/*
MySQL Backup
Source Server Version: 5.5.51
Source Database: ieledukh_dbweb
Date: 8/8/2017 11:50:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `ci_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `old_sch_class`
-- ----------------------------
DROP TABLE IF EXISTS `old_sch_class`;
CREATE TABLE `old_sch_class` (
  `classid` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) DEFAULT NULL,
  `grade_levelid` int(11) NOT NULL DEFAULT '0',
  `grade_labelid` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL COMMENT 'school_level',
  `handle_teacher` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `is_pt` int(11) NOT NULL COMMENT 'if =1 is partime else =0 fulltime',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=204 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `old_sch_day`
-- ----------------------------
DROP TABLE IF EXISTS `old_sch_day`;
CREATE TABLE `old_sch_day` (
  `dayid` int(11) NOT NULL AUTO_INCREMENT,
  `dayname` varchar(255) DEFAULT NULL,
  `enabled` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`dayid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `old_sch_dayrangedetail`
-- ----------------------------
DROP TABLE IF EXISTS `old_sch_dayrangedetail`;
CREATE TABLE `old_sch_dayrangedetail` (
  `dayrangedeid` int(11) NOT NULL AUTO_INCREMENT,
  `daysrangeid` int(11) DEFAULT NULL,
  `dayid` int(11) DEFAULT NULL,
  PRIMARY KEY (`dayrangedeid`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `old_sch_dayshift`
-- ----------------------------
DROP TABLE IF EXISTS `old_sch_dayshift`;
CREATE TABLE `old_sch_dayshift` (
  `shiftid` int(11) NOT NULL AUTO_INCREMENT,
  `shift` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`shiftid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `old_sch_daysrange`
-- ----------------------------
DROP TABLE IF EXISTS `old_sch_daysrange`;
CREATE TABLE `old_sch_daysrange` (
  `daysrangeid` int(11) NOT NULL AUTO_INCREMENT,
  `dayrange` varchar(255) DEFAULT NULL,
  `from_day` int(11) DEFAULT NULL,
  `to_day` int(11) DEFAULT NULL,
  `days` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`daysrangeid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `old_sch_emp_department`
-- ----------------------------
DROP TABLE IF EXISTS `old_sch_emp_department`;
CREATE TABLE `old_sch_emp_department` (
  `dep_id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(255) DEFAULT NULL,
  `department_kh` varchar(255) DEFAULT NULL,
  `vision` varchar(255) DEFAULT NULL,
  `mission` varchar(255) DEFAULT NULL,
  `goal` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `midified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `head_dep` int(11) DEFAULT NULL,
  PRIMARY KEY (`dep_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `old_sch_emp_position`
-- ----------------------------
DROP TABLE IF EXISTS `old_sch_emp_position`;
CREATE TABLE `old_sch_emp_position` (
  `posid` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(255) DEFAULT NULL,
  `position_kh` varchar(255) DEFAULT NULL,
  `description` text,
  `pos_order` int(11) DEFAULT NULL,
  `match_con_posid` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `dep_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`posid`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `old_sch_emp_profile`
-- ----------------------------
DROP TABLE IF EXISTS `old_sch_emp_profile`;
CREATE TABLE `old_sch_emp_profile` (
  `empid` int(11) NOT NULL AUTO_INCREMENT,
  `empcode` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name_kh` varchar(255) DEFAULT NULL,
  `last_name_kh` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `pob` text,
  `perm_adr` varchar(255) DEFAULT NULL,
  `village` varchar(255) DEFAULT NULL,
  `commune` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `zoon` varchar(255) NOT NULL DEFAULT '',
  `marital_status` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `emp_type` varchar(255) DEFAULT NULL,
  `pos_id` int(11) DEFAULT NULL,
  `dep_id` int(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `leave_school` varchar(255) DEFAULT NULL,
  `leave_school_reason` varchar(255) DEFAULT NULL,
  `idcard` varchar(255) DEFAULT NULL,
  `is_foreigner` int(11) DEFAULT '0',
  `employed_date` date DEFAULT NULL,
  `resigned_date` date DEFAULT NULL,
  `note` text,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `office_location` varchar(255) DEFAULT NULL,
  `contract_type` varchar(255) DEFAULT NULL,
  `annual_leave` double DEFAULT '0',
  `sick_leave` double DEFAULT '0',
  `al_taken` double DEFAULT '0',
  `sick_taken` double DEFAULT '0',
  `leave_reason` text,
  `con_id` int(11) DEFAULT NULL,
  `leave_type` int(11) DEFAULT NULL,
  `leave_comment` text,
  `kh_newyear_hol` double DEFAULT NULL,
  `endoftheyear_hol` double DEFAULT NULL,
  `worktimeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`empid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_absenty_entried_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_absenty_entried_kgp`;
CREATE TABLE `sch_absenty_entried_kgp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classid` varchar(50) NOT NULL,
  `trandate` varchar(50) NOT NULL,
  `exam_type` varchar(50) NOT NULL,
  `averag_coefficient` double NOT NULL,
  `num_month` varchar(50) NOT NULL,
  `type` int(11) NOT NULL,
  `typeno` int(11) NOT NULL,
  `studentid` varchar(50) NOT NULL,
  `yearid` int(11) NOT NULL,
  `schlevelid` int(11) NOT NULL,
  `grade_levelid` int(11) NOT NULL,
  `absenty_total` double NOT NULL,
  `absenty_with_permison` double NOT NULL,
  `score_total_line` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_assign_payment_method_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_assign_payment_method_detail`;
CREATE TABLE `sch_assign_payment_method_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `transno` int(50) DEFAULT NULL,
  `studentid` varchar(50) DEFAULT NULL,
  `trandate` datetime DEFAULT NULL,
  `from_to` varchar(20) NOT NULL,
  `is_paid` int(11) NOT NULL,
  `feetypeid` int(11) DEFAULT NULL,
  `term_sem_year_id` int(11) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `classid` varchar(50) DEFAULT NULL,
  `schooleleve` varchar(50) DEFAULT NULL,
  `programid` varchar(50) DEFAULT NULL,
  `acandemic` varchar(50) DEFAULT NULL,
  `ranglev` varchar(50) DEFAULT NULL,
  `schoolid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=146 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_assign_payment_method_order`
-- ----------------------------
DROP TABLE IF EXISTS `sch_assign_payment_method_order`;
CREATE TABLE `sch_assign_payment_method_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `typeno` int(50) DEFAULT NULL,
  `studentid` varchar(50) DEFAULT NULL,
  `trandate` datetime DEFAULT NULL,
  `classid` varchar(50) DEFAULT NULL,
  `schooleleve` varchar(50) DEFAULT NULL,
  `programid` varchar(50) NOT NULL,
  `acandemic` varchar(50) NOT NULL,
  `ranglev` varchar(50) NOT NULL,
  `schoolid` varchar(50) NOT NULL,
  `from_paymentmethod` varchar(50) NOT NULL,
  `to_paymentmethod` varchar(50) NOT NULL,
  `note` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `sch_attachment`;
CREATE TABLE `sch_attachment` (
  `attid` int(11) NOT NULL AUTO_INCREMENT,
  `techerid` int(11) DEFAULT NULL,
  `typeno` int(11) NOT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yare` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `coment` varchar(255) DEFAULT NULL,
  `treandate` date DEFAULT NULL,
  PRIMARY KEY (`attid`)
) ENGINE=MyISAM AUTO_INCREMENT=305 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `sch_attbase_schlevel`
-- ----------------------------
DROP TABLE IF EXISTS `sch_attbase_schlevel`;
CREATE TABLE `sch_attbase_schlevel` (
  `levattid` int(11) NOT NULL AUTO_INCREMENT,
  `baseattid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  PRIMARY KEY (`levattid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_class`
-- ----------------------------
DROP TABLE IF EXISTS `sch_class`;
CREATE TABLE `sch_class` (
  `classid` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) DEFAULT NULL,
  `grade_levelid` int(11) NOT NULL DEFAULT '0',
  `grade_labelid` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL COMMENT 'school_level',
  `handle_teacher` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `is_pt` int(11) NOT NULL COMMENT 'if =1 is partime else =0 fulltime',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=413 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_class_score_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_class_score_kgp`;
CREATE TABLE `sch_class_score_kgp` (
  `subject_score_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `adcademic_year_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `exam_type` varchar(100) DEFAULT NULL,
  `total_score` double DEFAULT NULL,
  `comments` varchar(300) DEFAULT NULL,
  `subject_groupid` int(11) NOT NULL,
  `subject_mainid` int(11) NOT NULL,
  `ranking_byclass` int(11) NOT NULL,
  `m_s_y` varchar(50) DEFAULT NULL COMMENT 'month,semester,year',
  `avg_score` double DEFAULT NULL,
  PRIMARY KEY (`subject_score_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_counselling`
-- ----------------------------
DROP TABLE IF EXISTS `sch_counselling`;
CREATE TABLE `sch_counselling` (
  `counsel_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `familyid` int(11) DEFAULT NULL,
  `c_type` varchar(255) DEFAULT NULL,
  `mem_id` int(11) DEFAULT NULL,
  `reason` text,
  `update_info` text,
  `observation` text,
  `session` text,
  `intervation` text,
  `plan_next_session` text,
  `plan_next_date` date DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  PRIMARY KEY (`counsel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `sch_counselling_staff`
-- ----------------------------
DROP TABLE IF EXISTS `sch_counselling_staff`;
CREATE TABLE `sch_counselling_staff` (
  `countsel_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `counsel_id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `remark` text,
  PRIMARY KEY (`countsel_staff_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `sch_dashboard_item`
-- ----------------------------
DROP TABLE IF EXISTS `sch_dashboard_item`;
CREATE TABLE `sch_dashboard_item` (
  `dashid` int(11) NOT NULL AUTO_INCREMENT,
  `dash_item` varchar(255) DEFAULT NULL,
  `moduleid` int(11) DEFAULT NULL,
  `link_pageid` int(11) DEFAULT NULL,
  `is_show` int(11) NOT NULL DEFAULT '1',
  `block` varchar(255) DEFAULT NULL COMMENT 'left_top,left_bottom',
  PRIMARY KEY (`dashid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_day`
-- ----------------------------
DROP TABLE IF EXISTS `sch_day`;
CREATE TABLE `sch_day` (
  `dayid` int(11) NOT NULL AUTO_INCREMENT,
  `dayname` varchar(255) DEFAULT NULL,
  `enabled` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`dayid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_dayrangedetail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_dayrangedetail`;
CREATE TABLE `sch_dayrangedetail` (
  `dayrangedeid` int(11) NOT NULL AUTO_INCREMENT,
  `daysrangeid` int(11) DEFAULT NULL,
  `dayid` int(11) DEFAULT NULL,
  PRIMARY KEY (`dayrangedeid`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_dayshift`
-- ----------------------------
DROP TABLE IF EXISTS `sch_dayshift`;
CREATE TABLE `sch_dayshift` (
  `shiftid` int(11) NOT NULL AUTO_INCREMENT,
  `shift` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`shiftid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_daysrange`
-- ----------------------------
DROP TABLE IF EXISTS `sch_daysrange`;
CREATE TABLE `sch_daysrange` (
  `daysrangeid` int(11) NOT NULL AUTO_INCREMENT,
  `dayrange` varchar(255) DEFAULT NULL,
  `from_day` int(11) DEFAULT NULL,
  `to_day` int(11) DEFAULT NULL,
  `days` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`daysrangeid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_emp_contract`
-- ----------------------------
DROP TABLE IF EXISTS `sch_emp_contract`;
CREATE TABLE `sch_emp_contract` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT,
  `contractid` varchar(255) DEFAULT NULL,
  `empid` varchar(255) DEFAULT NULL,
  `contract_type` varchar(255) DEFAULT NULL,
  `contract_attach` varchar(255) DEFAULT NULL,
  `begin_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `year` int(11) DEFAULT NULL COMMENT 'school_year',
  `job_type` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `decription` text,
  `status` int(11) DEFAULT '1',
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `duration_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`con_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_emp_department`
-- ----------------------------
DROP TABLE IF EXISTS `sch_emp_department`;
CREATE TABLE `sch_emp_department` (
  `dep_id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(255) DEFAULT NULL,
  `department_kh` varchar(255) DEFAULT NULL,
  `vision` varchar(255) DEFAULT NULL,
  `mission` varchar(255) DEFAULT NULL,
  `goal` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `midified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `head_dep` int(11) DEFAULT NULL,
  PRIMARY KEY (`dep_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_emp_leavetrans`
-- ----------------------------
DROP TABLE IF EXISTS `sch_emp_leavetrans`;
CREATE TABLE `sch_emp_leavetrans` (
  `takenid` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `empid` varchar(255) DEFAULT NULL,
  `taken_num` double DEFAULT NULL,
  `leave_type` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `requestid` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `is_auto` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`takenid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_emp_newresign`
-- ----------------------------
DROP TABLE IF EXISTS `sch_emp_newresign`;
CREATE TABLE `sch_emp_newresign` (
  `emptransid` int(11) NOT NULL AUTO_INCREMENT,
  `empid` int(11) DEFAULT NULL,
  `new_resign` varchar(255) DEFAULT NULL,
  `effective_date` date DEFAULT NULL COMMENT 'if new is hiredate,if resign is resign date',
  `reason` varchar(255) DEFAULT NULL,
  `con_id` int(11) DEFAULT NULL,
  `comment` text,
  `transdate` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`emptransid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_emp_permission_request`
-- ----------------------------
DROP TABLE IF EXISTS `sch_emp_permission_request`;
CREATE TABLE `sch_emp_permission_request` (
  `requestid` int(11) NOT NULL AUTO_INCREMENT,
  `empid` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL COMMENT 'school year',
  `request_type` varchar(255) DEFAULT NULL,
  `request_hour` double DEFAULT NULL,
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `medical_attach` varchar(255) DEFAULT NULL,
  `immediate_sup_name` int(255) DEFAULT NULL,
  `hr_respond` int(255) DEFAULT NULL,
  `date_request` date DEFAULT NULL,
  `comment` text,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `job_type` varchar(255) DEFAULT NULL,
  `total_day` double DEFAULT NULL,
  `am_pm` varchar(255) DEFAULT '',
  PRIMARY KEY (`requestid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_emp_position`
-- ----------------------------
DROP TABLE IF EXISTS `sch_emp_position`;
CREATE TABLE `sch_emp_position` (
  `posid` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(255) DEFAULT NULL,
  `position_kh` varchar(255) DEFAULT NULL,
  `description` text,
  `pos_order` int(11) DEFAULT NULL,
  `match_con_posid` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `dep_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`posid`)
) ENGINE=MyISAM AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_emp_profile`
-- ----------------------------
DROP TABLE IF EXISTS `sch_emp_profile`;
CREATE TABLE `sch_emp_profile` (
  `empid` int(11) NOT NULL AUTO_INCREMENT,
  `empcode` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name_kh` varchar(255) DEFAULT NULL,
  `last_name_kh` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `pob` text,
  `perm_adr` varchar(255) DEFAULT NULL,
  `village` varchar(255) DEFAULT NULL,
  `commune` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `zoon` varchar(255) NOT NULL DEFAULT '',
  `marital_status` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `emp_type` varchar(255) DEFAULT NULL,
  `pos_id` int(11) DEFAULT NULL,
  `dep_id` int(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `leave_school` varchar(255) DEFAULT NULL,
  `leave_school_reason` varchar(255) DEFAULT NULL,
  `idcard` varchar(255) DEFAULT NULL,
  `is_foreigner` int(11) DEFAULT '0',
  `employed_date` date DEFAULT NULL,
  `resigned_date` date DEFAULT NULL,
  `note` text,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `office_location` varchar(255) DEFAULT NULL,
  `contract_type` varchar(255) DEFAULT NULL,
  `annual_leave` double DEFAULT '0',
  `sick_leave` double DEFAULT '0',
  `al_taken` double DEFAULT '0',
  `sick_taken` double DEFAULT '0',
  `leave_reason` text,
  `con_id` int(11) DEFAULT NULL,
  `leave_type` int(11) DEFAULT NULL,
  `leave_comment` text,
  `kh_newyear_hol` double DEFAULT NULL,
  `endoftheyear_hol` double DEFAULT NULL,
  `worktimeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`empid`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_emp_working_day`
-- ----------------------------
DROP TABLE IF EXISTS `sch_emp_working_day`;
CREATE TABLE `sch_emp_working_day` (
  `workdayid` int(11) NOT NULL AUTO_INCREMENT,
  `empid` int(11) DEFAULT NULL,
  `mon` int(255) DEFAULT NULL,
  `tus` int(11) DEFAULT NULL,
  `wed` int(11) DEFAULT NULL,
  `thu` int(11) DEFAULT NULL,
  `fri` int(11) DEFAULT NULL,
  `sat` int(11) DEFAULT NULL,
  `sun` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`workdayid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_emp_working_time`
-- ----------------------------
DROP TABLE IF EXISTS `sch_emp_working_time`;
CREATE TABLE `sch_emp_working_time` (
  `worktimeid` int(11) NOT NULL AUTO_INCREMENT,
  `empid` int(11) DEFAULT NULL,
  `timeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`worktimeid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_eva_emp_evalutiont`
-- ----------------------------
DROP TABLE IF EXISTS `sch_eva_emp_evalutiont`;
CREATE TABLE `sch_eva_emp_evalutiont` (
  `evalid` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `eval_date` date DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  `review_from_date` date DEFAULT NULL,
  `review_to_date` date DEFAULT NULL,
  `eval_by_empid` int(11) DEFAULT NULL,
  `sup_comment` text,
  `sup_emp_id` int(11) DEFAULT NULL,
  `sup_comment_date` date DEFAULT NULL,
  `emp_comment` varchar(250) DEFAULT NULL,
  `emp_comment_date` date DEFAULT NULL,
  `gm_comment` varchar(250) DEFAULT NULL,
  `gm_emp_id` int(11) DEFAULT NULL,
  `gm_date` date DEFAULT NULL,
  `ove_com_id` int(11) DEFAULT NULL,
  `ove_comment` text,
  PRIMARY KEY (`evalid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_eva_emp_group`
-- ----------------------------
DROP TABLE IF EXISTS `sch_eva_emp_group`;
CREATE TABLE `sch_eva_emp_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `evalid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `grop_eval_id` int(11) DEFAULT NULL,
  `comment` varchar(250) DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_eva_emp_improvements`
-- ----------------------------
DROP TABLE IF EXISTS `sch_eva_emp_improvements`;
CREATE TABLE `sch_eva_emp_improvements` (
  `improvl_id` int(11) NOT NULL AUTO_INCREMENT,
  `evalid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `improment` varchar(255) DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  PRIMARY KEY (`improvl_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_eva_emp_streng`
-- ----------------------------
DROP TABLE IF EXISTS `sch_eva_emp_streng`;
CREATE TABLE `sch_eva_emp_streng` (
  `streng_id` int(11) NOT NULL AUTO_INCREMENT,
  `evalid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `strength` varchar(250) DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  PRIMARY KEY (`streng_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_evagroup`
-- ----------------------------
DROP TABLE IF EXISTS `sch_evagroup`;
CREATE TABLE `sch_evagroup` (
  `group_eval_id` int(11) NOT NULL AUTO_INCREMENT,
  `main_eval_id` int(11) DEFAULT NULL,
  `area_eval` varchar(250) DEFAULT NULL,
  `area_eval_kh` varchar(250) DEFAULT NULL,
  `rating_eval` int(11) DEFAULT NULL,
  PRIMARY KEY (`group_eval_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_evaluation_semester_iep_description`
-- ----------------------------
DROP TABLE IF EXISTS `sch_evaluation_semester_iep_description`;
CREATE TABLE `sch_evaluation_semester_iep_description` (
  `description_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`description_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_evaluation_semester_iep_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_evaluation_semester_iep_detail`;
CREATE TABLE `sch_evaluation_semester_iep_detail` (
  `eva_id` int(11) NOT NULL AUTO_INCREMENT,
  `evaluationid` int(11) DEFAULT NULL,
  `descrition_id` int(11) DEFAULT NULL,
  `seldom` int(11) DEFAULT '0',
  `sometimes` int(11) DEFAULT '0',
  `usually` int(11) DEFAULT '0',
  `consistently` int(11) DEFAULT '0',
  PRIMARY KEY (`eva_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_evaluation_semester_iep_order`
-- ----------------------------
DROP TABLE IF EXISTS `sch_evaluation_semester_iep_order`;
CREATE TABLE `sch_evaluation_semester_iep_order` (
  `evaluationid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `grade_levelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `semester` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `academicid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `techer_comment` varchar(255) DEFAULT NULL,
  `guardian_comment` varchar(255) DEFAULT NULL,
  `academic_date` datetime DEFAULT NULL,
  `techer_date` datetime DEFAULT NULL,
  `guardian_date` datetime DEFAULT NULL,
  `return_date` datetime DEFAULT NULL,
  PRIMARY KEY (`evaluationid`)
) ENGINE=MyISAM AUTO_INCREMENT=494 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_evamain`
-- ----------------------------
DROP TABLE IF EXISTS `sch_evamain`;
CREATE TABLE `sch_evamain` (
  `main_val_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) DEFAULT NULL,
  `id_main` int(100) DEFAULT NULL,
  PRIMARY KEY (`main_val_id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_evamention`
-- ----------------------------
DROP TABLE IF EXISTS `sch_evamention`;
CREATE TABLE `sch_evamention` (
  `mention_id` int(250) NOT NULL AUTO_INCREMENT,
  `score` varchar(250) DEFAULT NULL,
  `mention` varchar(250) DEFAULT NULL,
  `mention_kh` varchar(250) DEFAULT NULL,
  `min_rate` decimal(14,2) DEFAULT NULL,
  `max_rate` decimal(14,2) DEFAULT NULL,
  PRIMARY KEY (`mention_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_evaoverall_comments`
-- ----------------------------
DROP TABLE IF EXISTS `sch_evaoverall_comments`;
CREATE TABLE `sch_evaoverall_comments` (
  `ove_com_id` int(11) NOT NULL AUTO_INCREMENT,
  `description_overall` varchar(250) DEFAULT NULL,
  `description_overall_kh` varchar(250) DEFAULT NULL,
  `isfillout` int(11) DEFAULT '0',
  PRIMARY KEY (`ove_com_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_family`
-- ----------------------------
DROP TABLE IF EXISTS `sch_family`;
CREATE TABLE `sch_family` (
  `familyid` int(11) NOT NULL AUTO_INCREMENT,
  `family_code` varchar(255) DEFAULT NULL,
  `social_workerid` int(11) DEFAULT NULL,
  `family_name` varchar(255) DEFAULT NULL,
  `home_no` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `village` varchar(255) DEFAULT NULL,
  `commune` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `zoon` varchar(255) DEFAULT NULL,
  `permanent_adr` text,
  `housing_type` varchar(255) DEFAULT NULL,
  `drinking_water` varchar(255) DEFAULT NULL,
  `electricity` int(1) DEFAULT '0',
  `envirenment` varchar(255) DEFAULT NULL,
  `sleeping_place` varchar(255) DEFAULT NULL,
  `santitation` varchar(255) DEFAULT NULL,
  `property` text,
  `extra_information` text,
  `tel` varchar(255) DEFAULT NULL,
  `from_school` double DEFAULT NULL,
  `measure` varchar(255) DEFAULT NULL,
  `revenue` double DEFAULT NULL,
  `family_min_revenu` double DEFAULT NULL,
  `family_max_revenu` double DEFAULT NULL,
  `family_revenu_currcode` varchar(255) DEFAULT NULL,
  `family_revenu_period` varchar(255) DEFAULT NULL,
  `ex_rate` double DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `father_name_kh` varchar(255) DEFAULT NULL,
  `father_ocupation` varchar(255) DEFAULT NULL,
  `father_ocupation_kh` varchar(255) DEFAULT NULL,
  `father_dob` date DEFAULT NULL,
  `father_home` varchar(255) DEFAULT NULL,
  `father_phone` varchar(255) DEFAULT NULL,
  `father_street` varchar(255) DEFAULT NULL,
  `father_village` varchar(255) DEFAULT NULL,
  `father_commune` varchar(255) DEFAULT NULL,
  `father_district` varchar(255) DEFAULT NULL,
  `father_province` varchar(255) DEFAULT NULL,
  `father_revenu` double DEFAULT NULL,
  `father_min_revenu` double DEFAULT NULL,
  `father_max_revenu` double DEFAULT NULL,
  `father_revenu_currcode` varchar(255) DEFAULT NULL,
  `father_revenu_period` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `mother_name_kh` varchar(255) DEFAULT NULL,
  `mother_ocupation` varchar(255) DEFAULT NULL,
  `mother_ocupation_kh` varchar(255) DEFAULT NULL,
  `mother_dob` date DEFAULT NULL,
  `mother_home` varchar(255) DEFAULT NULL,
  `mother_phone` varchar(255) DEFAULT NULL,
  `mother_street` varchar(255) DEFAULT NULL,
  `mother_village` varchar(255) DEFAULT NULL,
  `mother_commune` varchar(255) DEFAULT NULL,
  `mother_district` varchar(255) DEFAULT NULL,
  `mother_province` varchar(255) DEFAULT NULL,
  `mother_revenu` varchar(255) DEFAULT NULL,
  `mother_min_revenu` double DEFAULT NULL,
  `mother_max_revenu` double DEFAULT NULL,
  `mother_revenu_currcode` varchar(255) DEFAULT NULL,
  `mother_revenu_period` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `login_user1` varchar(255) DEFAULT NULL,
  `login_user2` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fa_health` varchar(255) DEFAULT NULL,
  `ma_health` varchar(255) DEFAULT NULL,
  `fa_soc_status` varchar(255) DEFAULT NULL,
  `ma_soc_status` varchar(255) DEFAULT NULL,
  `adition_adr` text,
  `ma_education` text,
  `fa_education` text,
  `fa_civil_stat` varchar(255) DEFAULT NULL,
  `ma_civil_stat` varchar(255) DEFAULT NULL,
  `family_book_record_no` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`familyid`)
) ENGINE=MyISAM AUTO_INCREMENT=1418 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_family_distribute_plan`
-- ----------------------------
DROP TABLE IF EXISTS `sch_family_distribute_plan`;
CREATE TABLE `sch_family_distribute_plan` (
  `displanid` int(11) NOT NULL AUTO_INCREMENT,
  `familyid` int(11) DEFAULT NULL,
  `amt_rice` double DEFAULT NULL,
  `rice_uom` varchar(255) DEFAULT 'kg',
  `oil` double DEFAULT NULL,
  `oil_uom` varchar(255) DEFAULT 'lit',
  `is_invited` int(11) DEFAULT NULL,
  `remark` text,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `is_newadd` int(11) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `distrib_type` int(11) DEFAULT '0' COMMENT '12=every month/year,1=1times/year, 2=2times/year,4=4time/year',
  `year` varchar(255) DEFAULT NULL,
  `times_received` int(11) DEFAULT NULL,
  PRIMARY KEY (`displanid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_family_distributed`
-- ----------------------------
DROP TABLE IF EXISTS `sch_family_distributed`;
CREATE TABLE `sch_family_distributed` (
  `distribut_id` int(11) NOT NULL AUTO_INCREMENT,
  `dis_date` date DEFAULT NULL,
  `dis_adr` text,
  `from_time` time DEFAULT NULL,
  `to_time` time DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `total_family` int(11) DEFAULT NULL,
  `tota_rice` double DEFAULT NULL,
  `total_oil` double DEFAULT NULL,
  `note` text,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `distrib_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`distribut_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_family_distributed_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_family_distributed_detail`;
CREATE TABLE `sch_family_distributed_detail` (
  `disfamid` int(11) NOT NULL AUTO_INCREMENT,
  `distribut_id` int(11) DEFAULT NULL,
  `familyid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `dis_date` date DEFAULT NULL,
  `amt_rice` double DEFAULT NULL,
  `rice_uom` varchar(255) DEFAULT 'kg',
  `oil` double DEFAULT NULL,
  `oil_uom` varchar(255) DEFAULT 'lit',
  `received_date` date DEFAULT NULL,
  `received_by` varchar(255) DEFAULT NULL,
  `is_invited` int(11) DEFAULT NULL,
  `remark` text,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `is_newadd` int(11) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`disfamid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_family_social_infor`
-- ----------------------------
DROP TABLE IF EXISTS `sch_family_social_infor`;
CREATE TABLE `sch_family_social_infor` (
  `famnoteid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `familynote_type` varchar(255) DEFAULT NULL,
  `orders` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`famnoteid`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_family_sponsor`
-- ----------------------------
DROP TABLE IF EXISTS `sch_family_sponsor`;
CREATE TABLE `sch_family_sponsor` (
  `sponsorid` int(11) NOT NULL AUTO_INCREMENT,
  `sponsor_code` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `house_num` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `visit_sch_date` varchar(255) DEFAULT NULL,
  `budget` double DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `type_sponsor` varchar(255) DEFAULT NULL,
  `note` text,
  `title` varchar(255) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sponsorid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_family_visit`
-- ----------------------------
DROP TABLE IF EXISTS `sch_family_visit`;
CREATE TABLE `sch_family_visit` (
  `visitid` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `intervation_type` varchar(255) DEFAULT NULL,
  `visit_reason` text,
  `update_info` text,
  `sw_activities` text,
  `outcome` text,
  `familyid` int(11) DEFAULT NULL,
  `visit_times` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`visitid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_family_visit_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_family_visit_detail`;
CREATE TABLE `sch_family_visit_detail` (
  `visitdetid` int(11) NOT NULL AUTO_INCREMENT,
  `visitid` int(11) DEFAULT NULL,
  `studentid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  PRIMARY KEY (`visitdetid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_family_visit_sponsor`
-- ----------------------------
DROP TABLE IF EXISTS `sch_family_visit_sponsor`;
CREATE TABLE `sch_family_visit_sponsor` (
  `visitdetid` int(11) NOT NULL AUTO_INCREMENT,
  `visitid` int(11) DEFAULT NULL,
  `sponsorid` int(11) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`visitdetid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_family_visit_staff`
-- ----------------------------
DROP TABLE IF EXISTS `sch_family_visit_staff`;
CREATE TABLE `sch_family_visit_staff` (
  `visitdetid` int(11) NOT NULL AUTO_INCREMENT,
  `visitid` int(11) DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`visitdetid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_gep_total_score_entry`
-- ----------------------------
DROP TABLE IF EXISTS `sch_gep_total_score_entry`;
CREATE TABLE `sch_gep_total_score_entry` (
  `score_entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `schoolid` int(11) NOT NULL,
  `programid` int(11) NOT NULL,
  `schlevelid` int(11) NOT NULL,
  `yearid` int(11) NOT NULL,
  `termid` int(11) NOT NULL,
  `gradeid` int(11) NOT NULL,
  `classid` int(11) NOT NULL,
  `examtypeid` int(11) NOT NULL,
  `cousetype` int(11) NOT NULL,
  `studentid` int(11) NOT NULL,
  `attandance` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `class_participation` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `homework` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `total_subj` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `grand_total` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `is_change_class` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`score_entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7259346 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_grade_label`
-- ----------------------------
DROP TABLE IF EXISTS `sch_grade_label`;
CREATE TABLE `sch_grade_label` (
  `grade_labelid` int(11) NOT NULL AUTO_INCREMENT,
  `grade_label` varchar(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  PRIMARY KEY (`grade_labelid`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_grade_level`
-- ----------------------------
DROP TABLE IF EXISTS `sch_grade_level`;
CREATE TABLE `sch_grade_level` (
  `grade_levelid` int(11) NOT NULL AUTO_INCREMENT,
  `grade_level` varchar(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `next_grade_level` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  PRIMARY KEY (`grade_levelid`)
) ENGINE=MyISAM AUTO_INCREMENT=142 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_group_type_iep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_group_type_iep`;
CREATE TABLE `sch_group_type_iep` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grouptypeid` int(11) DEFAULT NULL,
  `group_type` varchar(50) DEFAULT NULL,
  `is_assessment` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Setup Fixed Data';

-- ----------------------------
--  Table structure for `sch_iep_total_score_entry`
-- ----------------------------
DROP TABLE IF EXISTS `sch_iep_total_score_entry`;
CREATE TABLE `sch_iep_total_score_entry` (
  `score_entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) NOT NULL,
  `schlevelid` int(11) NOT NULL,
  `yearid` int(11) NOT NULL,
  `semesterid` int(11) NOT NULL,
  `termid` int(11) NOT NULL,
  `gradeid` int(11) NOT NULL,
  `classid` int(11) NOT NULL,
  `examtypeid` int(11) NOT NULL,
  `studentid` int(11) NOT NULL,
  `attandance` double NOT NULL DEFAULT '0',
  `class_participation` double NOT NULL DEFAULT '0',
  `homework` double NOT NULL DEFAULT '0',
  `diligence` double NOT NULL DEFAULT '0',
  `subject` double NOT NULL DEFAULT '0',
  `total_score` double NOT NULL DEFAULT '0',
  `rank` int(11) DEFAULT NULL,
  `is_change_class` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`score_entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_level_subject_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_level_subject_detail`;
CREATE TABLE `sch_level_subject_detail` (
  `subj_level_id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_levelid` int(11) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`subj_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_medi_disease`
-- ----------------------------
DROP TABLE IF EXISTS `sch_medi_disease`;
CREATE TABLE `sch_medi_disease` (
  `diseaseid` int(11) NOT NULL AUTO_INCREMENT,
  `disease_code` varchar(255) DEFAULT NULL,
  `disease` varchar(255) DEFAULT NULL,
  `disease_kh` varchar(255) DEFAULT NULL,
  `illness_typeid` int(11) DEFAULT NULL,
  `disease_note` varchar(255) DEFAULT NULL,
  `available_occur` varchar(255) DEFAULT NULL,
  `prevent_note` varchar(255) DEFAULT NULL,
  `spread_note` varchar(255) DEFAULT NULL,
  `danger_level` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  PRIMARY KEY (`diseaseid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_medi_illness_type`
-- ----------------------------
DROP TABLE IF EXISTS `sch_medi_illness_type`;
CREATE TABLE `sch_medi_illness_type` (
  `illness_typeid` int(11) NOT NULL AUTO_INCREMENT,
  `illness_type` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `type_of_consultation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`illness_typeid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_medi_treatment`
-- ----------------------------
DROP TABLE IF EXISTS `sch_medi_treatment`;
CREATE TABLE `sch_medi_treatment` (
  `treatmentid` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `doctorid` int(11) DEFAULT NULL,
  `patientid` int(11) DEFAULT NULL,
  `patient_type` varchar(255) DEFAULT NULL COMMENT 'can be teacher,student,social service',
  `consult_typeid` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `is_internal_treat` int(11) DEFAULT '1',
  `external_hospital` varchar(255) DEFAULT NULL,
  `attach_ordernang` int(11) DEFAULT NULL,
  `next_visit_date` date DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `whcode` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  PRIMARY KEY (`treatmentid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='transaction';

-- ----------------------------
--  Table structure for `sch_medi_treatment_disease`
-- ----------------------------
DROP TABLE IF EXISTS `sch_medi_treatment_disease`;
CREATE TABLE `sch_medi_treatment_disease` (
  `treatedisid` int(11) NOT NULL AUTO_INCREMENT,
  `treatmentid` int(11) DEFAULT NULL,
  `diseaseid` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `sponfree` text,
  PRIMARY KEY (`treatedisid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='transaction';

-- ----------------------------
--  Table structure for `sch_medi_treatment_medicine`
-- ----------------------------
DROP TABLE IF EXISTS `sch_medi_treatment_medicine`;
CREATE TABLE `sch_medi_treatment_medicine` (
  `treatmedicid` int(11) NOT NULL AUTO_INCREMENT,
  `treatmentid` int(11) DEFAULT NULL,
  `stockid` int(11) DEFAULT NULL COMMENT 'medicine_code',
  `expire_date` date DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `unit_qty` double DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `how_to_use` varchar(255) DEFAULT NULL,
  `perday_use` varchar(255) DEFAULT NULL,
  `pertimes_use` varchar(255) DEFAULT NULL,
  `days_use` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  PRIMARY KEY (`treatmedicid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='transaction';

-- ----------------------------
--  Table structure for `sch_message`
-- ----------------------------
DROP TABLE IF EXISTS `sch_message`;
CREATE TABLE `sch_message` (
  `smsid` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `sent_from` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `sent_to` varchar(255) DEFAULT NULL,
  `cc_to` varchar(255) DEFAULT NULL,
  `bcc_to` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `description` longtext,
  `attach_file` varchar(255) DEFAULT NULL,
  `sent_date` datetime DEFAULT NULL,
  PRIMARY KEY (`smsid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_print_card`
-- ----------------------------
DROP TABLE IF EXISTS `sch_print_card`;
CREATE TABLE `sch_print_card` (
  `printid` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `academic_year` int(11) DEFAULT NULL,
  `card_id` varchar(255) DEFAULT NULL,
  `print_date` date DEFAULT NULL,
  `print_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`printid`)
) ENGINE=MyISAM AUTO_INCREMENT=894 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_rangelevel_period`
-- ----------------------------
DROP TABLE IF EXISTS `sch_rangelevel_period`;
CREATE TABLE `sch_rangelevel_period` (
  `rangelevel_periodid` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `typeid` int(11) DEFAULT NULL,
  `rangelevelid` int(11) DEFAULT NULL,
  PRIMARY KEY (`rangelevel_periodid`)
) ENGINE=MyISAM AUTO_INCREMENT=1121 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_school_feetype`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_feetype`;
CREATE TABLE `sch_school_feetype` (
  `feetypeid` int(11) NOT NULL AUTO_INCREMENT,
  `schoolfeetype` varchar(255) DEFAULT NULL,
  `schoolfeetype_kh` varchar(255) DEFAULT NULL,
  `maintype` int(11) NOT NULL COMMENT '1=term,2=semester,3=year',
  PRIMARY KEY (`feetypeid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_school_infor`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_infor`;
CREATE TABLE `sch_school_infor` (
  `schoolid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` text,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_tel` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `slogan` text,
  `vision` text,
  `mission` text,
  `gaol` text,
  `open_since` date DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  PRIMARY KEY (`schoolid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_school_level`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_level`;
CREATE TABLE `sch_school_level` (
  `schlevelid` int(11) NOT NULL AUTO_INCREMENT,
  `sch_level` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `orders` int(11) DEFAULT '0',
  `is_vtc` int(11) DEFAULT '0' COMMENT 'Vocaltional and Training',
  `schlv_director` int(11) NOT NULL,
  `programid` int(11) DEFAULT NULL,
  PRIMARY KEY (`schlevelid`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_school_program`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_program`;
CREATE TABLE `sch_school_program` (
  `programid` int(11) NOT NULL AUTO_INCREMENT,
  `program` varchar(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `program_kh` varchar(255) DEFAULT NULL,
  `program_short` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`programid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Setup Fixed Data';

-- ----------------------------
--  Table structure for `sch_school_promotion`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_promotion`;
CREATE TABLE `sch_school_promotion` (
  `promot_id` int(11) NOT NULL AUTO_INCREMENT,
  `proname` varchar(255) DEFAULT NULL,
  `proname_kh` varchar(255) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `num_student` int(11) DEFAULT NULL,
  `is_closed` int(11) DEFAULT '0',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`promot_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_school_rangelevel`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_rangelevel`;
CREATE TABLE `sch_school_rangelevel` (
  `rangelevelid` int(11) NOT NULL AUTO_INCREMENT,
  `rangelevelname` varchar(255) DEFAULT NULL,
  `min_level` int(11) DEFAULT NULL,
  `max_level` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rangelevelid`)
) ENGINE=MyISAM AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COMMENT='range level can use for payment';

-- ----------------------------
--  Table structure for `sch_school_rangelevelclass`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_rangelevelclass`;
CREATE TABLE `sch_school_rangelevelclass` (
  `clsrangeid` int(11) NOT NULL AUTO_INCREMENT,
  `rangelevelid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  PRIMARY KEY (`clsrangeid`)
) ENGINE=MyISAM AUTO_INCREMENT=1020 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_school_rangelevelfee`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_rangelevelfee`;
CREATE TABLE `sch_school_rangelevelfee` (
  `ranglevfeeid` int(11) NOT NULL AUTO_INCREMENT,
  `rangelevelid` int(11) DEFAULT NULL,
  `feetypeid` int(11) DEFAULT NULL,
  `fee` decimal(14,2) DEFAULT NULL,
  `book_price` decimal(14,2) DEFAULT NULL,
  `admin_fee` decimal(14,2) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `term_sem_year_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ranglevfeeid`)
) ENGINE=MyISAM AUTO_INCREMENT=644 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_school_rangelevtime`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_rangelevtime`;
CREATE TABLE `sch_school_rangelevtime` (
  `rangelevtimeid` int(11) NOT NULL AUTO_INCREMENT,
  `rangelevelid` int(11) DEFAULT NULL,
  `timeid` int(11) DEFAULT NULL,
  `daysrangeid` int(11) DEFAULT NULL,
  `shiftid` int(11) DEFAULT NULL,
  PRIMARY KEY (`rangelevtimeid`)
) ENGINE=MyISAM AUTO_INCREMENT=228 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_school_semester`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_semester`;
CREATE TABLE `sch_school_semester` (
  `semesterid` int(11) NOT NULL AUTO_INCREMENT,
  `semester` varchar(255) DEFAULT NULL,
  `semester_kh` varchar(255) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `isclosed` int(11) DEFAULT '0',
  `feetypeid` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `modify_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`semesterid`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_school_term`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_term`;
CREATE TABLE `sch_school_term` (
  `termid` int(11) NOT NULL AUTO_INCREMENT,
  `term` varchar(255) DEFAULT NULL,
  `term_kh` varchar(255) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `semesterid` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `isclosed` varchar(255) DEFAULT NULL,
  `feetypeid` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`termid`)
) ENGINE=MyISAM AUTO_INCREMENT=2851 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_school_termweek`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_termweek`;
CREATE TABLE `sch_school_termweek` (
  `weektermid` int(11) NOT NULL AUTO_INCREMENT,
  `termid` int(11) DEFAULT NULL,
  `weekid` int(11) DEFAULT NULL,
  `examtypeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`weektermid`)
) ENGINE=MyISAM AUTO_INCREMENT=543 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_school_year`
-- ----------------------------
DROP TABLE IF EXISTS `sch_school_year`;
CREATE TABLE `sch_school_year` (
  `yearid` int(11) NOT NULL AUTO_INCREMENT,
  `sch_year` varchar(255) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `feetypeid` int(11) DEFAULT NULL,
  `isclosed` int(11) NOT NULL DEFAULT '0',
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`yearid`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_score_entryed`
-- ----------------------------
DROP TABLE IF EXISTS `sch_score_entryed`;
CREATE TABLE `sch_score_entryed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` varchar(50) NOT NULL,
  `subjectid` varchar(50) NOT NULL,
  `subjecttypeid` varchar(50) NOT NULL,
  `score_num` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_score_gep_entryed_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_score_gep_entryed_detail`;
CREATE TABLE `sch_score_gep_entryed_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) NOT NULL,
  `subjectid` int(11) NOT NULL,
  `subjecttypeid` int(11) NOT NULL,
  `score_num` double NOT NULL,
  `type` int(11) DEFAULT NULL,
  `typeno` int(11) DEFAULT NULL,
  `score_oderid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_score_gep_entryed_order`
-- ----------------------------
DROP TABLE IF EXISTS `sch_score_gep_entryed_order`;
CREATE TABLE `sch_score_gep_entryed_order` (
  `score_id` int(11) NOT NULL AUTO_INCREMENT,
  `averag_coefficient` double NOT NULL,
  `type` int(11) NOT NULL,
  `typeno` int(11) NOT NULL,
  `main_tranno` varchar(100) DEFAULT NULL COMMENT 'school_schlevel_year_grade_class_examtype_subexam_coretype',
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `gradelevelid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `exam_typeid` int(11) DEFAULT NULL,
  `termid` int(11) DEFAULT NULL,
  `course_type` int(11) DEFAULT '0' COMMENT '0 = Core, 1 = Skill',
  `tran_date` date DEFAULT NULL,
  `create_by` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_by` varchar(100) DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `subjecttypeid` int(11) DEFAULT NULL,
  `group_subject` int(11) DEFAULT NULL,
  `is_pt` int(11) DEFAULT NULL,
  `is_class_participate` int(11) DEFAULT NULL COMMENT 'assesment exam',
  PRIMARY KEY (`score_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_score_iep_entryed_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_score_iep_entryed_detail`;
CREATE TABLE `sch_score_iep_entryed_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) NOT NULL,
  `subjectid` int(11) NOT NULL,
  `group_subject` int(11) NOT NULL,
  `score_num` double NOT NULL,
  `type` int(11) DEFAULT NULL,
  `typeno` int(11) DEFAULT NULL,
  `score_oderid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=333 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_score_iep_entryed_order`
-- ----------------------------
DROP TABLE IF EXISTS `sch_score_iep_entryed_order`;
CREATE TABLE `sch_score_iep_entryed_order` (
  `score_id` int(11) NOT NULL AUTO_INCREMENT,
  `averag_coefficient` double NOT NULL,
  `type` int(11) NOT NULL,
  `typeno` int(11) NOT NULL,
  `main_tranno` varchar(100) DEFAULT NULL COMMENT 'school_schlevel_year_grade_class_examtype_subexam',
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `gradelevelid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `exam_typeid` int(11) DEFAULT NULL,
  `group_subject` int(11) DEFAULT NULL,
  `tran_date` date DEFAULT NULL,
  `create_by` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_by` varchar(100) DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `weekid` int(11) DEFAULT NULL,
  `termid` int(11) DEFAULT NULL,
  `count_subject` int(11) DEFAULT NULL,
  PRIMARY KEY (`score_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_score_mention`
-- ----------------------------
DROP TABLE IF EXISTS `sch_score_mention`;
CREATE TABLE `sch_score_mention` (
  `menid` int(11) NOT NULL AUTO_INCREMENT,
  `mention` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `min_score` double DEFAULT '0',
  `max_score` double DEFAULT '0',
  `avg_score` double DEFAULT '0',
  `note` text CHARACTER SET latin1,
  `schlevelid` int(11) DEFAULT NULL,
  `mention_kh` varchar(255) DEFAULT NULL,
  `is_final_ses` int(11) DEFAULT '1',
  `men_type` int(11) NOT NULL DEFAULT '1' COMMENT '1=subject,2=level',
  PRIMARY KEY (`menid`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_score_mention_gep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_score_mention_gep`;
CREATE TABLE `sch_score_mention_gep` (
  `menid` int(11) NOT NULL AUTO_INCREMENT,
  `mention` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `mention_kh` varchar(255) DEFAULT NULL,
  `men_type` int(11) NOT NULL DEFAULT '1' COMMENT '1=subject,2=level',
  `schlevelid` int(11) DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `min_score` double DEFAULT '0',
  `max_score` double DEFAULT '0',
  `avg_score` double DEFAULT '0',
  `meaning` text,
  `is_final_ses` int(11) DEFAULT '1',
  `is_past` int(11) DEFAULT '0',
  PRIMARY KEY (`menid`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_setup_area`
-- ----------------------------
DROP TABLE IF EXISTS `sch_setup_area`;
CREATE TABLE `sch_setup_area` (
  `areaid` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`areaid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_setup_bus`
-- ----------------------------
DROP TABLE IF EXISTS `sch_setup_bus`;
CREATE TABLE `sch_setup_bus` (
  `busid` int(11) NOT NULL AUTO_INCREMENT,
  `busno` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`busid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_setup_busarea`
-- ----------------------------
DROP TABLE IF EXISTS `sch_setup_busarea`;
CREATE TABLE `sch_setup_busarea` (
  `busareaid` int(11) NOT NULL AUTO_INCREMENT,
  `busid` int(11) DEFAULT NULL,
  `areaid` int(11) DEFAULT NULL,
  PRIMARY KEY (`busareaid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_setup_busfee`
-- ----------------------------
DROP TABLE IF EXISTS `sch_setup_busfee`;
CREATE TABLE `sch_setup_busfee` (
  `buspriceid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'term,month,away',
  `buswaytype` varchar(255) DEFAULT NULL,
  `areaid` int(11) DEFAULT NULL,
  `price` decimal(14,2) DEFAULT NULL,
  PRIMARY KEY (`buspriceid`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_setup_driver`
-- ----------------------------
DROP TABLE IF EXISTS `sch_setup_driver`;
CREATE TABLE `sch_setup_driver` (
  `driverid` int(11) NOT NULL AUTO_INCREMENT,
  `driver_name` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `areaid` int(11) DEFAULT NULL,
  `busid` int(11) DEFAULT NULL,
  PRIMARY KEY (`driverid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_setup_location`
-- ----------------------------
DROP TABLE IF EXISTS `sch_setup_location`;
CREATE TABLE `sch_setup_location` (
  `loc_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL COMMENT '1=Province, 2=District, 3=Commune, 4=Village',
  `name` varchar(255) DEFAULT NULL,
  `province_id` int(11) DEFAULT '0',
  `district_id` int(11) DEFAULT '0',
  `commune_id` int(11) DEFAULT '0',
  `village_id` int(11) DEFAULT '0',
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`loc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_setup_otherfee`
-- ----------------------------
DROP TABLE IF EXISTS `sch_setup_otherfee`;
CREATE TABLE `sch_setup_otherfee` (
  `otherfeeid` int(11) NOT NULL AUTO_INCREMENT,
  `otherfee` varchar(250) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `prices` double DEFAULT NULL,
  `fromdate` date NOT NULL,
  `todate` date NOT NULL,
  `typefee` int(11) NOT NULL,
  `bus_way_type` int(11) DEFAULT NULL,
  `areacode` varchar(50) NOT NULL,
  `stockid` int(11) DEFAULT NULL,
  PRIMARY KEY (`otherfeeid`)
) ENGINE=MyISAM AUTO_INCREMENT=167 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_sponsor_box`
-- ----------------------------
DROP TABLE IF EXISTS `sch_sponsor_box`;
CREATE TABLE `sch_sponsor_box` (
  `boxid` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `sponsorid` int(11) DEFAULT NULL,
  `spr_ques_fr` text,
  `spr_ques_eng` text,
  `num_box` int(11) DEFAULT NULL,
  `visite_date` date DEFAULT NULL,
  `reception_date` date DEFAULT NULL,
  `sending_box_date` date DEFAULT NULL,
  `date_answer_toques` date DEFAULT NULL,
  `date_thanks` date DEFAULT NULL,
  `date_sendthanks_paris` date DEFAULT NULL,
  `langue` varchar(255) DEFAULT NULL,
  `detail` text,
  `num_gift` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `lettertype` text,
  `is_visit` int(11) DEFAULT NULL,
  `note` text,
  `letter_other` text,
  `gift` text,
  PRIMARY KEY (`boxid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_sponsor_boxdetail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_sponsor_boxdetail`;
CREATE TABLE `sch_sponsor_boxdetail` (
  `bodexid` int(11) NOT NULL AUTO_INCREMENT,
  `boxid` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `des_fr` text,
  `des_eng` text,
  PRIMARY KEY (`bodexid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stock`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stock`;
CREATE TABLE `sch_stock` (
  `stockid` int(11) NOT NULL AUTO_INCREMENT,
  `stockcode` varchar(255) DEFAULT NULL,
  `descr_eng` varchar(255) DEFAULT NULL,
  `descr_kh` varchar(255) DEFAULT NULL,
  `reorder_qty` double DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `is_con_serial` int(11) DEFAULT '0',
  `used_expired_date` int(11) DEFAULT '0',
  `is_active` int(11) DEFAULT '1',
  `specification` varchar(250) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `sale_price` double DEFAULT NULL,
  PRIMARY KEY (`stockid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stock_balance`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stock_balance`;
CREATE TABLE `sch_stock_balance` (
  `balanceid` int(11) NOT NULL AUTO_INCREMENT,
  `stockid` int(11) DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `whcode` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `categoryid` int(11) DEFAULT NULL,
  PRIMARY KEY (`balanceid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stock_category`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stock_category`;
CREATE TABLE `sch_stock_category` (
  `categoryid` int(11) NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `stocktype` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`categoryid`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stock_opening_balance`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stock_opening_balance`;
CREATE TABLE `sch_stock_opening_balance` (
  `openid` int(11) NOT NULL AUTO_INCREMENT,
  `stockid` int(11) DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `transdate` date DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `qty_unit` double(255,0) DEFAULT '1',
  `cost` double DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `ref_no` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `whcode` int(11) DEFAULT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `selling_price` double DEFAULT NULL,
  PRIMARY KEY (`openid`)
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stock_opening_master`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stock_opening_master`;
CREATE TABLE `sch_stock_opening_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `tran_date` datetime DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `typeno` int(11) DEFAULT NULL,
  `note` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stock_stockmove`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stock_stockmove`;
CREATE TABLE `sch_stock_stockmove` (
  `moveid` int(11) NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) DEFAULT NULL,
  `stockid` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `whcode` varchar(255) DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `unit_qty` double DEFAULT '1',
  `created_by` varchar(255) DEFAULT NULL,
  `oldcost` double DEFAULT NULL,
  `oldqty` double DEFAULT NULL,
  `buying_cost` double DEFAULT NULL,
  `add_cost` double DEFAULT NULL,
  `selling_price` double DEFAULT NULL,
  `avg_cost` double DEFAULT NULL,
  `stocktype` varchar(255) DEFAULT NULL COMMENT 'can be medicine,',
  PRIMARY KEY (`moveid`)
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COMMENT='transaction';

-- ----------------------------
--  Table structure for `sch_stock_type`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stock_type`;
CREATE TABLE `sch_stock_type` (
  `stocktypeid` int(11) NOT NULL AUTO_INCREMENT,
  `stocktype` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`stocktypeid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='setup';

-- ----------------------------
--  Table structure for `sch_stock_uom`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stock_uom`;
CREATE TABLE `sch_stock_uom` (
  `uomid` int(11) NOT NULL AUTO_INCREMENT,
  `uom` varchar(255) DEFAULT NULL,
  `desciption` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `unit_qty` double DEFAULT NULL,
  `desciption_kh` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uomid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stock_wharehouse`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stock_wharehouse`;
CREATE TABLE `sch_stock_wharehouse` (
  `whcode` int(11) NOT NULL AUTO_INCREMENT,
  `wharehouse` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_tel` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`whcode`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stud_academic_background`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stud_academic_background`;
CREATE TABLE `sch_stud_academic_background` (
  `acadid` int(11) NOT NULL AUTO_INCREMENT,
  `acadtypeid` int(11) DEFAULT NULL,
  `acadtype` varchar(255) DEFAULT NULL,
  `educationid` int(11) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `studentid` int(11) DEFAULT NULL,
  PRIMARY KEY (`acadid`)
) ENGINE=MyISAM AUTO_INCREMENT=19133 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stud_attachdetails`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stud_attachdetails`;
CREATE TABLE `sch_stud_attachdetails` (
  `stuattachid` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `attachid` int(11) DEFAULT NULL,
  PRIMARY KEY (`stuattachid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stud_attnote`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stud_attnote`;
CREATE TABLE `sch_stud_attnote` (
  `attnoteid` int(11) NOT NULL AUTO_INCREMENT,
  `attnote` varchar(255) DEFAULT NULL,
  `symbol` varchar(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`attnoteid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stud_attrule`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stud_attrule`;
CREATE TABLE `sch_stud_attrule` (
  `attruleid` int(11) NOT NULL AUTO_INCREMENT,
  `attnoteid` int(11) DEFAULT NULL,
  `discription` varchar(255) DEFAULT NULL,
  `withdraw_point` decimal(14,2) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`attruleid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stud_evalsemester`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stud_evalsemester`;
CREATE TABLE `sch_stud_evalsemester` (
  `semid` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `semester` varchar(255) DEFAULT NULL,
  `months` varchar(255) DEFAULT NULL,
  `sem_exam_avg` double DEFAULT NULL,
  `months_avg` double DEFAULT '0',
  `month_num` int(11) DEFAULT NULL,
  `sem_avg` double DEFAULT '0',
  `sem_mention` varchar(255) DEFAULT NULL,
  `sem_result` varchar(255) DEFAULT '0',
  `sem_rank` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  PRIMARY KEY (`semid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stud_gifdonate`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stud_gifdonate`;
CREATE TABLE `sch_stud_gifdonate` (
  `donateid` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `donate_type` varchar(255) DEFAULT NULL COMMENT 'Annual Gif,Ordering Gif',
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `modify_by` varchar(255) DEFAULT NULL,
  `modify_date` date DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  PRIMARY KEY (`donateid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stud_gifdonate_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stud_gifdonate_detail`;
CREATE TABLE `sch_stud_gifdonate_detail` (
  `donatdeid` int(11) NOT NULL AUTO_INCREMENT,
  `donateid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `studentid` int(11) DEFAULT NULL,
  `gifid` int(11) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `remark` text,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modify_date` date DEFAULT NULL,
  `modify_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`donatdeid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stud_partner`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stud_partner`;
CREATE TABLE `sch_stud_partner` (
  `partnerid` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(100) DEFAULT NULL,
  `contact_name` varchar(100) DEFAULT NULL,
  `business_type` varchar(100) DEFAULT NULL,
  `tel` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `partner_since` date DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` varchar(100) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  PRIMARY KEY (`partnerid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stud_vtcpromot`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stud_vtcpromot`;
CREATE TABLE `sch_stud_vtcpromot` (
  `vtcpromot_id` int(11) NOT NULL AUTO_INCREMENT,
  `proname` varchar(255) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `num_student` int(11) DEFAULT NULL,
  `is_closed` int(11) DEFAULT '0',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`vtcpromot_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stud_vtcstar`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stud_vtcstar`;
CREATE TABLE `sch_stud_vtcstar` (
  `starid` int(11) NOT NULL AUTO_INCREMENT,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `partnerid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `promot_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`starid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_stud_vtcstar_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_stud_vtcstar_detail`;
CREATE TABLE `sch_stud_vtcstar_detail` (
  `stardetail_id` int(11) NOT NULL AUTO_INCREMENT,
  `starid` int(11) DEFAULT NULL,
  `studentid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  PRIMARY KEY (`stardetail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_studatt_daily`
-- ----------------------------
DROP TABLE IF EXISTS `sch_studatt_daily`;
CREATE TABLE `sch_studatt_daily` (
  `dailyattid` int(11) NOT NULL AUTO_INCREMENT,
  `baseattid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `termid` int(11) DEFAULT NULL,
  `weekid` int(11) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `subject_grouop` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`dailyattid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_studatt_dailydetail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_studatt_dailydetail`;
CREATE TABLE `sch_studatt_dailydetail` (
  `dailyattdetid` int(11) NOT NULL AUTO_INCREMENT,
  `baseattid` int(11) DEFAULT NULL,
  `dailyattid` int(11) NOT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `studentid` int(11) DEFAULT NULL,
  `termid` int(11) DEFAULT NULL,
  `weekid` int(11) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `subject_grouop` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `attnoteid` int(11) DEFAULT NULL,
  `present_day` decimal(14,2) DEFAULT NULL,
  `permission_day` decimal(14,2) DEFAULT NULL,
  `permissin_hour` decimal(14,2) DEFAULT NULL,
  `absent_day` decimal(14,2) DEFAULT NULL,
  `absent_hour` decimal(14,2) DEFAULT NULL,
  `lateday` decimal(14,2) DEFAULT '0.00',
  `attend_score` decimal(14,2) DEFAULT NULL,
  `permis_score` decimal(14,2) DEFAULT NULL,
  `minus_absentscore` decimal(14,2) DEFAULT NULL,
  `latetime` datetime DEFAULT NULL,
  `leave_early_time` datetime DEFAULT NULL,
  `minus_permscore` decimal(14,2) DEFAULT NULL,
  `witdraw_late_point` decimal(14,2) DEFAULT NULL,
  `widrleave_early_point` decimal(14,2) DEFAULT NULL,
  `full_score` decimal(14,2) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dailyattdetid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_studend_com_ann_aca_iep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_studend_com_ann_aca_iep`;
CREATE TABLE `sch_studend_com_ann_aca_iep` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `gradelevelid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `termid` int(11) DEFAULT NULL,
  `command_teacher` varchar(250) DEFAULT NULL,
  `academicid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `directorid` int(11) DEFAULT NULL,
  `teacherid` int(11) DEFAULT NULL,
  `date_academic` date DEFAULT NULL,
  `date_teacher` date DEFAULT NULL,
  `date_director` date DEFAULT NULL,
  `date_create` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_studend_command_gep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_studend_command_gep`;
CREATE TABLE `sch_studend_command_gep` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `gradelevelid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `termid` int(11) DEFAULT NULL,
  `command` varchar(250) DEFAULT NULL,
  `is_partime` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `academicid` int(11) DEFAULT NULL,
  `date_academic` date DEFAULT NULL,
  `date_teacher` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_studend_command_iep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_studend_command_iep`;
CREATE TABLE `sch_studend_command_iep` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `gradelevelid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `termid` int(11) DEFAULT NULL,
  `command_teacher` varchar(250) DEFAULT NULL,
  `command_guardian` varchar(250) DEFAULT NULL,
  `examp_type` int(11) DEFAULT NULL,
  `academicid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `date_academic` date DEFAULT NULL,
  `date_teacher` date DEFAULT NULL,
  `date_return` date DEFAULT NULL,
  `date_guardian` date DEFAULT NULL,
  `date_create` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student`;
CREATE TABLE `sch_student` (
  `studentid` int(11) NOT NULL AUTO_INCREMENT,
  `student_num` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name_kh` varchar(255) DEFAULT NULL,
  `last_name_kh` varchar(255) DEFAULT NULL,
  `first_name_ch` varchar(255) DEFAULT NULL,
  `last_name_ch` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `commune` varchar(255) DEFAULT NULL,
  `village` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `home_no` varchar(255) DEFAULT NULL,
  `zoon` varchar(255) DEFAULT NULL,
  `permanent_adr` text,
  `from_school` double DEFAULT NULL,
  `measure` varchar(255) DEFAULT NULL,
  `boarding_school` int(1) DEFAULT '0',
  `boarding_reason` varchar(255) DEFAULT NULL,
  `leave_school` int(1) DEFAULT '0',
  `leave_reason` varchar(255) DEFAULT NULL,
  `family_revenue` double DEFAULT NULL,
  `comments` text,
  `familyid` int(11) DEFAULT NULL,
  `respondid` int(11) DEFAULT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `login_username` varchar(255) DEFAULT NULL,
  `pob` text,
  `is_active` int(1) DEFAULT '1',
  `match_memid` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `temp_name` varchar(255) DEFAULT NULL,
  `is_vtc` int(11) DEFAULT NULL COMMENT 'Vocational Training',
  `promot_id` int(11) DEFAULT NULL,
  `moeys_id` varchar(255) DEFAULT NULL COMMENT 'ID for Mistry of Education',
  `leave_sch_date` date DEFAULT NULL,
  `boarding_date` date DEFAULT NULL,
  `register_date` date DEFAULT NULL,
  `staywith` varchar(255) DEFAULT NULL,
  `id_card_no` varchar(255) DEFAULT NULL,
  `interview_by` varchar(255) DEFAULT NULL,
  `introduced_by` varchar(255) DEFAULT NULL,
  `transporttype` varchar(255) DEFAULT NULL,
  `is_owner_by` varchar(255) DEFAULT '0' COMMENT 'is 0= No, 1= Yes',
  `is_sch_car_no` varchar(255) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `current_programid` int(11) DEFAULT NULL,
  `current_schoollevelid` int(11) DEFAULT NULL,
  `current_yearid` int(11) DEFAULT NULL,
  `current_ranglevelid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `is_new_old` int(11) NOT NULL COMMENT '5=is new else paymentmethodid',
  `registered_number` int(11) DEFAULT NULL,
  `registered_discount` double DEFAULT NULL,
  `consultant_by` varchar(255) DEFAULT NULL,
  `get_info_from` varchar(255) DEFAULT NULL,
  `dob_province` varchar(255) DEFAULT NULL,
  `dob_district` varchar(255) DEFAULT NULL,
  `dob_commune` varchar(255) DEFAULT NULL,
  `dob_village` varchar(255) DEFAULT NULL,
  `dob_street` varchar(255) DEFAULT NULL,
  `dob_home_no` varchar(255) DEFAULT NULL,
  `province_eng` varchar(255) DEFAULT NULL,
  `district_eng` varchar(255) DEFAULT NULL,
  `commune_eng` varchar(255) DEFAULT NULL,
  `village_eng` varchar(255) DEFAULT NULL,
  `street_eng` varchar(255) DEFAULT NULL,
  `home_no_eng` varchar(255) DEFAULT NULL,
  `dob_province_eng` varchar(255) DEFAULT NULL,
  `dob_district_eng` varchar(255) DEFAULT NULL,
  `dob_commune_eng` varchar(255) DEFAULT NULL,
  `dob_village_eng` varchar(255) DEFAULT NULL,
  `dob_street_eng` varchar(255) DEFAULT NULL,
  `dob_home_no_eng` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`studentid`)
) ENGINE=MyISAM AUTO_INCREMENT=10426 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_area`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_area`;
CREATE TABLE `sch_student_area` (
  `studareaid` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `areaid` int(11) DEFAULT NULL,
  `latlng` varchar(255) DEFAULT NULL,
  `longlng` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`studareaid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_assign_class`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_assign_class`;
CREATE TABLE `sch_student_assign_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `from_rangelevelid` int(11) DEFAULT NULL,
  `from_classid` int(11) DEFAULT NULL,
  `to_rangle` int(11) NOT NULL,
  `to_class` int(11) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `tran_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1607 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_enrollment`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_enrollment`;
CREATE TABLE `sch_student_enrollment` (
  `enrollid` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `enroll_date` date DEFAULT NULL,
  `type_of_enroll` varchar(255) DEFAULT NULL,
  `is_rollover` int(1) DEFAULT NULL,
  `rollover_date` date DEFAULT NULL,
  `rollover_by` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `termid` int(11) DEFAULT NULL,
  `semesterid` int(11) DEFAULT NULL,
  `rangelevelid` int(11) DEFAULT NULL,
  `feetypeid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `is_closed` int(11) NOT NULL,
  `is_paid` int(11) NOT NULL,
  `term_sem_year_id` int(11) NOT NULL,
  `is_pass` int(11) NOT NULL,
  `score` double(11,0) NOT NULL,
  `rank` double(11,0) DEFAULT NULL,
  PRIMARY KEY (`enrollid`)
) ENGINE=MyISAM AUTO_INCREMENT=6741 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_enrollment_register`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_enrollment_register`;
CREATE TABLE `sch_student_enrollment_register` (
  `enrollid` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `is_rollover` int(1) DEFAULT NULL,
  `rollover_date` date DEFAULT NULL,
  `rollover_by` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_by` varchar(255) DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`enrollid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_evaluate_setup_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_evaluate_setup_kgp`;
CREATE TABLE `sch_student_evaluate_setup_kgp` (
  `evaluate_id` int(11) NOT NULL AUTO_INCREMENT,
  `evaluate_name_kh` varchar(255) DEFAULT NULL,
  `evaluate_name_eng` varchar(255) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`evaluate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_evaluate_trans_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_evaluate_trans_kgp`;
CREATE TABLE `sch_student_evaluate_trans_kgp` (
  `evaluate_tran_id` int(11) NOT NULL AUTO_INCREMENT,
  `evaluate_id` int(11) DEFAULT NULL,
  `seldom` int(11) DEFAULT '0',
  `sometimes` int(11) DEFAULT '0',
  `usuall` int(11) DEFAULT '0',
  `consistenly` int(11) DEFAULT '0',
  `student_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `academic_year_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `semester` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`evaluate_tran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_evaluated`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_evaluated`;
CREATE TABLE `sch_student_evaluated` (
  `evaluateid` int(11) NOT NULL AUTO_INCREMENT,
  `class_comment` text,
  `studentid` int(11) NOT NULL,
  `classid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `month` varchar(55) DEFAULT NULL,
  `evaluate_type` varchar(55) DEFAULT NULL,
  `kh_teacher_comment` text,
  `forign_teacher_comment` text,
  `kh_teacher` int(11) DEFAULT NULL,
  `forign_teacher` int(11) DEFAULT NULL,
  `created_by` varchar(55) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(55) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `type` int(11) DEFAULT '2',
  `transno` int(11) DEFAULT '0',
  `eval_semester` varchar(255) DEFAULT NULL,
  `eng_teach_com` text,
  `eng_teach_name` int(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `technic_com` text,
  `supple_com` text,
  `schlevelid` int(11) DEFAULT NULL,
  `avg_coefficient` double DEFAULT NULL,
  `promotion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`evaluateid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `sch_student_evaluated_mention`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_evaluated_mention`;
CREATE TABLE `sch_student_evaluated_mention` (
  `evaluate_menid` int(11) NOT NULL AUTO_INCREMENT,
  `evaluateid` int(11) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `mention` varchar(11) DEFAULT NULL,
  `score` double DEFAULT NULL,
  `studentid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  PRIMARY KEY (`evaluate_menid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `sch_student_exam_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_exam_schedule`;
CREATE TABLE `sch_student_exam_schedule` (
  `examid` int(11) NOT NULL AUTO_INCREMENT,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `grade_levelid` int(11) DEFAULT NULL,
  `examtypeid` int(11) DEFAULT NULL,
  `start_exam` date DEFAULT NULL,
  `end_exam` date DEFAULT NULL,
  `start_entry_score` date DEFAULT NULL,
  `end_entry_score` date DEFAULT NULL,
  PRIMARY KEY (`examid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_examtype`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_examtype`;
CREATE TABLE `sch_student_examtype` (
  `examtypeid` int(11) NOT NULL AUTO_INCREMENT,
  `exam_test` varchar(255) DEFAULT NULL,
  `maintype` int(11) NOT NULL DEFAULT '1' COMMENT '1=monthly,2=term,3=semester,4=final_grade',
  `programid` int(11) DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`examtypeid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Setup Fixed Data';

-- ----------------------------
--  Table structure for `sch_student_examtype_gep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_examtype_gep`;
CREATE TABLE `sch_student_examtype_gep` (
  `examtypeid` int(11) NOT NULL AUTO_INCREMENT,
  `exam_test` varchar(255) DEFAULT NULL,
  `maintype` int(11) NOT NULL DEFAULT '1' COMMENT '1=mid-term,2=final-term,3=semester,4=final_grade,5=Quiz,6 =assignment,7=Cp,8=Home Work',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `is_assissment` int(11) DEFAULT '0' COMMENT '1=assissment,0=subject',
  PRIMARY KEY (`examtypeid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Setup Fixed Data';

-- ----------------------------
--  Table structure for `sch_student_examtype_iep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_examtype_iep`;
CREATE TABLE `sch_student_examtype_iep` (
  `examtypeid` int(11) NOT NULL AUTO_INCREMENT,
  `exam_test` varchar(255) DEFAULT NULL,
  `maintype` int(11) NOT NULL DEFAULT '1' COMMENT '1=mid-term,2=final-term,3=semester,4=final_grade,5=Quiz,6 =assignment,7=Cp,8=Home Work',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `is_assissment` int(11) DEFAULT '0' COMMENT '1=assissment,0=subject',
  PRIMARY KEY (`examtypeid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Setup Fixed Data';

-- ----------------------------
--  Table structure for `sch_student_fee`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_fee`;
CREATE TABLE `sch_student_fee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `typeno` int(50) DEFAULT NULL,
  `studentid` varchar(50) DEFAULT NULL,
  `trandate` date DEFAULT NULL,
  `classid` varchar(50) DEFAULT NULL,
  `schooleleve` varchar(50) DEFAULT NULL,
  `termid` varchar(50) DEFAULT NULL,
  `amt_total` double DEFAULT NULL,
  `amt_paid` double DEFAULT NULL,
  `amt_balance` double DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `programid` varchar(50) NOT NULL,
  `acandemic` varchar(50) NOT NULL,
  `ranglev` varchar(50) NOT NULL,
  `schoolid` varchar(50) NOT NULL,
  `paymentmethod` varchar(50) NOT NULL,
  `paymenttype` varchar(50) NOT NULL,
  `areaid` varchar(50) NOT NULL,
  `busfeetypid` varchar(50) NOT NULL,
  `is_status` int(11) NOT NULL,
  `enrollid` int(50) NOT NULL,
  `note` varchar(50) NOT NULL,
  `timeid` varchar(10) NOT NULL,
  `is_returned` int(11) NOT NULL,
  `type_inv` int(11) NOT NULL,
  `typeno_inv` int(11) NOT NULL,
  `locationcode` varchar(255) DEFAULT NULL,
  `fromdate` date DEFAULT NULL,
  `todate` date DEFAULT NULL,
  `referno` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3508 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_fee_deleted`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_fee_deleted`;
CREATE TABLE `sch_student_fee_deleted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `typeno` int(50) DEFAULT NULL,
  `studentid` varchar(50) DEFAULT NULL,
  `trandate` date DEFAULT NULL,
  `classid` varchar(50) DEFAULT NULL,
  `schooleleve` varchar(50) DEFAULT NULL,
  `termid` varchar(50) DEFAULT NULL,
  `amt_total` double DEFAULT NULL,
  `amt_paid` double DEFAULT NULL,
  `amt_balance` double DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `programid` varchar(50) NOT NULL,
  `acandemic` varchar(50) NOT NULL,
  `ranglev` varchar(50) NOT NULL,
  `schoolid` varchar(50) NOT NULL,
  `paymentmethod` varchar(50) NOT NULL,
  `paymenttype` varchar(50) NOT NULL,
  `areaid` varchar(50) NOT NULL,
  `busfeetypid` varchar(50) NOT NULL,
  `is_status` int(11) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_byuser` varchar(50) DEFAULT NULL,
  `type_inv_rec` int(11) DEFAULT NULL,
  `typeno_inv_rec` int(11) DEFAULT NULL,
  `add_note` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_fee_inv_detail_deleted`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_fee_inv_detail_deleted`;
CREATE TABLE `sch_student_fee_inv_detail_deleted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `typeno` int(50) DEFAULT NULL,
  `studentid` varchar(50) DEFAULT NULL,
  `trandate` date DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `not` varchar(50) DEFAULT NULL,
  `amt_line` double NOT NULL,
  `prices` double NOT NULL,
  `amt_dis` double NOT NULL,
  `qty` int(11) NOT NULL,
  `type_inv_rec` int(11) NOT NULL,
  `typeno_inv_rec` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=377 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_fee_rec_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_fee_rec_detail`;
CREATE TABLE `sch_student_fee_rec_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `typeno` int(50) DEFAULT NULL,
  `studentid` varchar(50) DEFAULT NULL,
  `trandate` date DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `not` varchar(50) DEFAULT NULL,
  `amt_line` double NOT NULL,
  `prices` double NOT NULL,
  `amt_dis` double NOT NULL,
  `qty` int(11) NOT NULL,
  `type_inv` int(11) NOT NULL,
  `typeno_inv` int(11) NOT NULL,
  `otherfee_id` varchar(50) NOT NULL,
  `is_bus` int(11) NOT NULL,
  `areaid` varchar(50) NOT NULL,
  `busid` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7411 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_fee_rec_order`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_fee_rec_order`;
CREATE TABLE `sch_student_fee_rec_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `typeno` int(50) DEFAULT NULL,
  `studentid` varchar(50) DEFAULT NULL,
  `trandate` date DEFAULT NULL,
  `classid` varchar(50) DEFAULT NULL,
  `schooleleve` varchar(50) DEFAULT NULL,
  `termid` varchar(50) DEFAULT NULL,
  `amt_total` double NOT NULL,
  `amt_paid` double DEFAULT NULL,
  `amt_balance` double DEFAULT NULL,
  `programid` varchar(50) NOT NULL,
  `acandemic` varchar(50) NOT NULL,
  `ranglev` varchar(50) NOT NULL,
  `schoolid` varchar(50) NOT NULL,
  `paymentmethod` varchar(50) NOT NULL,
  `paymenttype` varchar(50) NOT NULL,
  `areaid` varchar(50) NOT NULL,
  `busfeetypid` varchar(50) NOT NULL,
  `type_inv` int(11) NOT NULL,
  `typeno_inv` int(11) NOT NULL,
  `enrollid` varchar(50) NOT NULL,
  `note` varchar(50) NOT NULL,
  `timeid` varchar(20) NOT NULL,
  `is_paywith_inv` int(11) NOT NULL,
  `duedate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3457 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_fee_type`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_fee_type`;
CREATE TABLE `sch_student_fee_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `typeno` int(50) DEFAULT NULL,
  `studentid` varchar(50) DEFAULT NULL,
  `trandate` date DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `not` varchar(50) DEFAULT NULL,
  `amt_line` double NOT NULL,
  `prices` double NOT NULL,
  `amt_dis` double NOT NULL,
  `qty` int(11) NOT NULL,
  `otherfee_id` varchar(50) NOT NULL,
  `is_bus` int(11) NOT NULL,
  `areaid` varchar(50) NOT NULL,
  `busid` varchar(50) NOT NULL,
  `driverid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7683 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_gif`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_gif`;
CREATE TABLE `sch_student_gif` (
  `gifid` int(11) NOT NULL AUTO_INCREMENT,
  `gifname` varchar(255) DEFAULT NULL,
  `gifname_kh` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `min_level` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `modify_by` varchar(255) DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `unit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`gifid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_link_othstudent`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_link_othstudent`;
CREATE TABLE `sch_student_link_othstudent` (
  `stlinkid` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `link_studentid` int(11) DEFAULT NULL,
  PRIMARY KEY (`stlinkid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_mem_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_mem_detail`;
CREATE TABLE `sch_student_mem_detail` (
  `studmemid` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `memberid` int(11) DEFAULT NULL,
  `relationship` varchar(255) DEFAULT NULL,
  `grade_level` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`studmemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_member`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_member`;
CREATE TABLE `sch_student_member` (
  `memid` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name_kh` varchar(255) DEFAULT NULL,
  `first_name_kh` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `dob` date NOT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `tel1` varchar(255) DEFAULT NULL,
  `tel2` varchar(255) DEFAULT NULL,
  `note` text,
  `social_status` varchar(255) DEFAULT NULL,
  `health` varchar(255) DEFAULT NULL,
  `civil_status` varchar(255) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `grade_level` varchar(255) DEFAULT NULL,
  `leave_school` int(1) DEFAULT NULL,
  `familyid` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `studentid` int(11) NOT NULL,
  `is_internal_student` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `relationship` varchar(255) DEFAULT NULL,
  `revenue` double DEFAULT '0',
  `currcode` varchar(255) DEFAULT 'USD',
  `ex_rate` double DEFAULT '1',
  `leave_school_reason` varchar(255) DEFAULT NULL,
  `temp_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`memid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_moeysid`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_moeysid`;
CREATE TABLE `sch_student_moeysid` (
  `stdid` int(11) NOT NULL AUTO_INCREMENT,
  `moeys_id` varchar(255) DEFAULT NULL,
  `studentid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) NOT NULL,
  PRIMARY KEY (`stdid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_permission`;
CREATE TABLE `sch_student_permission` (
  `permisid` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `studentid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL,
  `permis_status` int(1) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(55) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `modified_by` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`permisid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_resp_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_resp_detail`;
CREATE TABLE `sch_student_resp_detail` (
  `sturespid` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `respondid` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `note` text,
  `relationship` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sturespid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_responsible`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_responsible`;
CREATE TABLE `sch_student_responsible` (
  `respondid` int(11) NOT NULL AUTO_INCREMENT,
  `respon_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name_kh` varchar(255) DEFAULT NULL,
  `first_name_kh` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `revenue` double DEFAULT NULL,
  `tel1` varchar(255) DEFAULT NULL,
  `tel2` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `home_no` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `village` varchar(255) DEFAULT NULL,
  `commune` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `social_status` varchar(255) DEFAULT NULL,
  `health` varchar(255) DEFAULT NULL,
  `civil_status` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `respond_type` varchar(255) DEFAULT NULL,
  `familyid` int(11) DEFAULT NULL,
  `studentid` int(11) DEFAULT NULL,
  `relationship` varchar(255) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `respon_name_eng` varchar(255) DEFAULT NULL,
  `occupation_eng` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`respondid`)
) ENGINE=MyISAM AUTO_INCREMENT=1032 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_rollover`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_rollover`;
CREATE TABLE `sch_student_rollover` (
  `rolloverid` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `from_yearid` int(11) DEFAULT NULL,
  `to_yearid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `is_completed` int(11) DEFAULT '0',
  `note` text,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `is_verified` int(11) DEFAULT '0',
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`rolloverid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_student_sponsor_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_student_sponsor_detail`;
CREATE TABLE `sch_student_sponsor_detail` (
  `stdspid` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `sponsorid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `start_sp_date` date DEFAULT NULL,
  `end_sp_date` date DEFAULT NULL,
  PRIMARY KEY (`stdspid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject`;
CREATE TABLE `sch_subject` (
  `subjectid` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `subj_type_id` int(11) DEFAULT NULL,
  `short_sub` varchar(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `subject_kh` varchar(255) DEFAULT NULL,
  `is_trimester_sub` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `is_eval` int(11) DEFAULT '1',
  `orders` int(11) DEFAULT '0',
  `coefficient` decimal(14,2) DEFAULT NULL,
  `max_score` double DEFAULT NULL,
  `subject_mainid` int(11) DEFAULT NULL,
  `score_percence` double NOT NULL,
  PRIMARY KEY (`subjectid`,`is_trimester_sub`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_`;
CREATE TABLE `sch_subject_` (
  `subjectid` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `subject_kh` varchar(255) DEFAULT NULL,
  `subj_type_id` int(11) DEFAULT NULL,
  `short_sub` varchar(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `is_eval` int(11) DEFAULT '1',
  `is_trimester_sub` int(11) NOT NULL,
  `orders` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `coefficient` decimal(14,2) DEFAULT NULL,
  PRIMARY KEY (`subjectid`,`is_trimester_sub`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_assessment_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_assessment_kgp`;
CREATE TABLE `sch_subject_assessment_kgp` (
  `assess_id` int(11) NOT NULL AUTO_INCREMENT,
  `assess_name_kh` varchar(255) DEFAULT NULL,
  `assess_name_en` varchar(255) DEFAULT NULL,
  `def_percentag` double(3,2) DEFAULT '0.00',
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`assess_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_assessment_percent_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_assessment_percent_kgp`;
CREATE TABLE `sch_subject_assessment_percent_kgp` (
  `ass_percent_id` int(11) NOT NULL AUTO_INCREMENT,
  `assessment_id` int(11) DEFAULT NULL,
  `percentage` double DEFAULT '0',
  `subject_id` int(11) DEFAULT NULL,
  `subject_type_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `adcademic_year_id` int(11) NOT NULL,
  `maxscore` int(11) DEFAULT NULL,
  `ass_max_score` double NOT NULL,
  PRIMARY KEY (`ass_percent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_detail`;
CREATE TABLE `sch_subject_detail` (
  `subj_grad_id` int(11) NOT NULL AUTO_INCREMENT,
  `subjecttypeid` int(11) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `grade_levelid` int(11) DEFAULT NULL,
  `is_core` int(11) DEFAULT NULL,
  `is_skill` int(11) DEFAULT NULL,
  `is_assessment` int(11) DEFAULT NULL,
  `exam_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`subj_grad_id`)
) ENGINE=MyISAM AUTO_INCREMENT=462 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_detail_iep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_detail_iep`;
CREATE TABLE `sch_subject_detail_iep` (
  `subj_grad_id` int(11) NOT NULL AUTO_INCREMENT,
  `subjecttypeid` int(11) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `grade_levelid` int(11) DEFAULT NULL,
  `is_core` int(11) DEFAULT NULL,
  `is_skill` int(11) DEFAULT NULL,
  `is_assessment` int(11) DEFAULT NULL,
  `exam_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`subj_grad_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_gep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_gep`;
CREATE TABLE `sch_subject_gep` (
  `subjectid` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `subj_type_id` int(11) DEFAULT NULL,
  `short_sub` varchar(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `subject_kh` varchar(255) DEFAULT NULL,
  `is_trimester_sub` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `is_eval` int(11) DEFAULT '1',
  `orders` int(11) DEFAULT '0',
  `coefficient` decimal(14,2) DEFAULT NULL,
  `max_score` double DEFAULT NULL,
  `is_core` int(11) DEFAULT '0',
  `is_skill` int(11) DEFAULT '0',
  `is_assessment` int(11) DEFAULT '0',
  `calc_score` double DEFAULT '0',
  PRIMARY KEY (`subjectid`,`is_trimester_sub`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_gep_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_gep_detail`;
CREATE TABLE `sch_subject_gep_detail` (
  `subj_grad_id` int(11) NOT NULL AUTO_INCREMENT,
  `subjecttypeid` int(11) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `grade_levelid` int(11) DEFAULT NULL,
  `is_core` int(11) DEFAULT NULL,
  `is_skill` int(11) DEFAULT NULL,
  `is_assessment` int(11) DEFAULT NULL,
  `exam_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`subj_grad_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_iep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_iep`;
CREATE TABLE `sch_subject_iep` (
  `subjectid` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `subj_type_id` int(11) DEFAULT NULL,
  `short_sub` varchar(255) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `subject_kh` varchar(255) DEFAULT NULL,
  `is_trimester_sub` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `is_eval` int(11) DEFAULT '1',
  `orders` int(11) DEFAULT '0',
  `coefficient` decimal(14,2) DEFAULT NULL,
  `max_score` double DEFAULT NULL,
  `is_assessment` int(11) DEFAULT '0',
  PRIMARY KEY (`subjectid`,`is_trimester_sub`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_main`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_main`;
CREATE TABLE `sch_subject_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_mention`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_mention`;
CREATE TABLE `sch_subject_mention` (
  `submenid` int(11) NOT NULL AUTO_INCREMENT,
  `schoolid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `gradelevelid` int(11) DEFAULT NULL,
  `mentionid` int(11) DEFAULT NULL,
  `max_score` double DEFAULT '0',
  `minscrore` double DEFAULT '0',
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`submenid`)
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_detail_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_detail_kgp`;
CREATE TABLE `sch_subject_score_detail_kgp` (
  `subject_score_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_score_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `percentage` double DEFAULT NULL,
  `input_score` double DEFAULT NULL,
  `output_score` double DEFAULT NULL,
  `is_subject` int(11) NOT NULL COMMENT '1=subject,0=assessment',
  PRIMARY KEY (`subject_score_detail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_final_detail_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_final_detail_kgp`;
CREATE TABLE `sch_subject_score_final_detail_kgp` (
  `sub_final_score_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_final_score_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `percentage` double DEFAULT NULL,
  `input_score` double DEFAULT NULL,
  `output_score` double DEFAULT NULL,
  `is_subject` int(11) NOT NULL COMMENT '1=subject,0=assessment',
  PRIMARY KEY (`sub_final_score_detail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_final_detail_kgp_multi`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_final_detail_kgp_multi`;
CREATE TABLE `sch_subject_score_final_detail_kgp_multi` (
  `sub_final_score_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_final_score_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `percentage` double DEFAULT NULL,
  `input_score` double DEFAULT NULL,
  `output_score` double DEFAULT NULL,
  `is_subject` int(11) NOT NULL COMMENT '1=subject,0=assessment',
  PRIMARY KEY (`sub_final_score_detail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_final_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_final_kgp`;
CREATE TABLE `sch_subject_score_final_kgp` (
  `sub_final_score_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `adcademic_year_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `exam_final` varchar(100) DEFAULT NULL,
  `get_semester_id` varchar(100) DEFAULT NULL,
  `total_score_s1` double DEFAULT NULL,
  `total_avg_score_s1` double DEFAULT NULL,
  `total_score_s2` double DEFAULT NULL,
  `total_avg_score_s2` double DEFAULT NULL,
  `total_score` double DEFAULT NULL,
  `average_score` double DEFAULT NULL,
  `comments` varchar(300) DEFAULT NULL,
  `subject_groupid` int(11) NOT NULL,
  `subject_mainid` int(11) NOT NULL,
  `ranking_bysubj` int(11) NOT NULL,
  `averag_coefficient` double NOT NULL,
  PRIMARY KEY (`sub_final_score_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_final_kgp_multi`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_final_kgp_multi`;
CREATE TABLE `sch_subject_score_final_kgp_multi` (
  `sub_final_score_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `adcademic_year_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `exam_final` varchar(100) DEFAULT NULL,
  `get_semester_id` varchar(100) DEFAULT NULL,
  `total_score_s1` double DEFAULT NULL,
  `total_avg_score_s1` double DEFAULT NULL,
  `total_score_s2` double DEFAULT NULL,
  `total_avg_score_s2` double DEFAULT NULL,
  `total_score` double DEFAULT NULL,
  `average_score` double DEFAULT NULL,
  `comments` varchar(300) DEFAULT NULL,
  `subject_groupid` int(11) NOT NULL,
  `subject_mainid` int(11) NOT NULL,
  `ranking_bysubj` int(11) NOT NULL,
  `averag_coefficient` double NOT NULL,
  `absent` double DEFAULT NULL,
  `present` double DEFAULT NULL,
  PRIMARY KEY (`sub_final_score_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_monthly_detail_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_monthly_detail_kgp`;
CREATE TABLE `sch_subject_score_monthly_detail_kgp` (
  `sub_month_score_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_monthly_score_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `percentage` double DEFAULT NULL,
  `input_score` double DEFAULT NULL,
  `output_score` double DEFAULT NULL,
  `is_subject` int(11) NOT NULL COMMENT '1=subject,0=assessment',
  PRIMARY KEY (`sub_month_score_detail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=239 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_monthly_detail_kgp_multi`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_monthly_detail_kgp_multi`;
CREATE TABLE `sch_subject_score_monthly_detail_kgp_multi` (
  `sub_month_score_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `adcademic_year_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `subject_mainid` int(11) DEFAULT NULL,
  `subject_groupid` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `exam_monthly` varchar(100) DEFAULT NULL,
  `total_score` double DEFAULT NULL,
  `average_score` double DEFAULT NULL COMMENT 'calculate average score for monthly by subject',
  `averag_coefficient` double DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `sub_monthly_score_id` int(11) DEFAULT NULL,
  `is_subject` int(11) DEFAULT NULL COMMENT '1=subject,0=assessment',
  `output_score` double DEFAULT NULL,
  `input_score` double DEFAULT NULL,
  `percentage` double DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `percentage_subject` double DEFAULT NULL,
  `input_score_subject` double DEFAULT NULL,
  PRIMARY KEY (`sub_month_score_detail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3952 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
--  Table structure for `sch_subject_score_monthly_kg`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_monthly_kg`;
CREATE TABLE `sch_subject_score_monthly_kg` (
  `subject_score_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `adcademic_year_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `exam_monthly` varchar(100) DEFAULT NULL,
  `total_score` double DEFAULT NULL,
  `comments` varchar(300) DEFAULT NULL,
  `subject_groupid` int(11) NOT NULL,
  `subject_mainid` int(11) NOT NULL,
  PRIMARY KEY (`subject_score_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_monthly_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_monthly_kgp`;
CREATE TABLE `sch_subject_score_monthly_kgp` (
  `sub_monthly_score_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `adcademic_year_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `exam_monthly` varchar(100) DEFAULT NULL,
  `total_score` double DEFAULT NULL,
  `average_score` double DEFAULT NULL COMMENT 'calculate average score for monthly by subject',
  `comments` varchar(300) DEFAULT NULL,
  `subject_groupid` int(11) NOT NULL,
  `subject_mainid` int(11) NOT NULL,
  `ranking_bysubj` int(11) NOT NULL,
  `averag_coefficient` double NOT NULL,
  `none_subject` int(11) DEFAULT NULL,
  PRIMARY KEY (`sub_monthly_score_id`)
) ENGINE=MyISAM AUTO_INCREMENT=234 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_monthly_kgp_multi`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_monthly_kgp_multi`;
CREATE TABLE `sch_subject_score_monthly_kgp_multi` (
  `sub_monthly_score_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `adcademic_year_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `subject_mainid` int(11) DEFAULT NULL,
  `subject_groupid` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `exam_monthly` varchar(100) DEFAULT NULL,
  `total_score` double DEFAULT NULL,
  `average_score` double DEFAULT NULL COMMENT 'calculate average score for monthly by subject',
  `averag_coefficient` double DEFAULT NULL,
  `present` double DEFAULT NULL,
  `absent` double DEFAULT NULL,
  `mentions` varchar(255) DEFAULT NULL,
  `comments` varchar(300) DEFAULT NULL,
  `ranking_bysubj` int(11) DEFAULT NULL,
  `none_subject` int(11) DEFAULT NULL,
  PRIMARY KEY (`sub_monthly_score_id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_semester_detail_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_semester_detail_kgp`;
CREATE TABLE `sch_subject_score_semester_detail_kgp` (
  `sub_semes_score_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_sesmester_score_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `percentage` double DEFAULT NULL,
  `input_score` double DEFAULT NULL,
  `output_score` double DEFAULT NULL,
  `is_subject` int(11) NOT NULL COMMENT '1=subject,0=assessment',
  PRIMARY KEY (`sub_semes_score_detail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_semester_detail_kgp_multi`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_semester_detail_kgp_multi`;
CREATE TABLE `sch_subject_score_semester_detail_kgp_multi` (
  `sub_month_score_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `adcademic_year_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `subject_mainid` int(11) DEFAULT NULL,
  `subject_groupid` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `exam_monthly` varchar(100) DEFAULT NULL,
  `total_score` double DEFAULT NULL,
  `average_score` double DEFAULT NULL COMMENT 'calculate average score for monthly by subject',
  `averag_coefficient` double DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `sub_monthly_score_id` int(11) DEFAULT NULL,
  `is_subject` int(11) DEFAULT NULL COMMENT '1=subject,0=assessment',
  `output_score` double DEFAULT NULL,
  `input_score` double DEFAULT NULL,
  `percentage` double DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `percentage_subject` double DEFAULT NULL,
  `input_score_subject` double DEFAULT NULL,
  PRIMARY KEY (`sub_month_score_detail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
--  Table structure for `sch_subject_score_semester_kgp`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_semester_kgp`;
CREATE TABLE `sch_subject_score_semester_kgp` (
  `sub_sesmester_score_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `adcademic_year_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `exam_semester` varchar(100) DEFAULT NULL,
  `get_monthly` varchar(100) DEFAULT NULL,
  `total_avg_monthly_score` double DEFAULT NULL,
  `average_monthly_score` double DEFAULT NULL,
  `total_score` double DEFAULT NULL,
  `average_score` double DEFAULT NULL COMMENT 'calculate average score for semester by subject',
  `total_average_score` double DEFAULT NULL COMMENT 'calculate average score for monthly and semester by subject',
  `comments` varchar(300) DEFAULT NULL,
  `subject_groupid` int(11) NOT NULL,
  `subject_mainid` int(11) NOT NULL,
  `ranking_bysubj` int(11) NOT NULL,
  `averag_coefficient` double NOT NULL,
  PRIMARY KEY (`sub_sesmester_score_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_score_semester_kgp_multi`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_score_semester_kgp_multi`;
CREATE TABLE `sch_subject_score_semester_kgp_multi` (
  `sub_sesmester_score_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `school_level_id` int(11) DEFAULT NULL,
  `adcademic_year_id` int(11) DEFAULT NULL,
  `grade_level_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `subject_mainid` int(11) DEFAULT NULL,
  `subject_groupid` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `exam_semester` varchar(100) DEFAULT NULL,
  `get_monthly` varchar(100) DEFAULT NULL,
  `total_score` double DEFAULT NULL,
  `average_score` double DEFAULT NULL COMMENT 'calculate average score for semester by subject',
  `averag_coefficient` double DEFAULT NULL,
  `present` double DEFAULT NULL,
  `absent` double DEFAULT NULL,
  `comments` varchar(300) DEFAULT NULL,
  `ranking_bysubj` int(11) DEFAULT NULL,
  `total_average_score` double DEFAULT NULL COMMENT 'calculate average score for monthly and semester by subject',
  `average_monthly_score` double DEFAULT NULL,
  `total_avg_monthly_score` double DEFAULT NULL,
  PRIMARY KEY (`sub_sesmester_score_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_type`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_type`;
CREATE TABLE `sch_subject_type` (
  `subj_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_type` varchar(255) DEFAULT NULL,
  `subject_type_kh` varchar(255) DEFAULT NULL,
  `main_type` varchar(255) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `is_moeys` int(11) DEFAULT '0',
  `orders` int(11) DEFAULT '0',
  `rangelevelid` int(11) DEFAULT NULL,
  `is_report_display` int(11) DEFAULT '1' COMMENT '1= yes , 0= No',
  `is_group_calc` int(11) DEFAULT NULL,
  `is_group_calc_percent` int(11) DEFAULT NULL,
  `is_class_participation` int(11) DEFAULT NULL COMMENT 'Class-Participation',
  `is_attendance_qty` double DEFAULT NULL,
  PRIMARY KEY (`subj_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_type_gep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_type_gep`;
CREATE TABLE `sch_subject_type_gep` (
  `subj_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_type` varchar(255) DEFAULT NULL,
  `subject_type_kh` varchar(255) DEFAULT NULL,
  `main_type` varchar(255) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `is_moeys` int(11) DEFAULT '0',
  `orders` int(11) DEFAULT '0',
  `rangelevelid` int(11) DEFAULT NULL,
  `is_report_display` int(11) DEFAULT '1' COMMENT '1= yes , 0= No',
  `is_group_calc` int(11) DEFAULT NULL,
  `is_group_calc_percent` double DEFAULT NULL,
  `is_class_participation` int(11) DEFAULT NULL COMMENT 'Class-Participation',
  `is_attendance_qty` double DEFAULT NULL,
  PRIMARY KEY (`subj_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_subject_type_iep`
-- ----------------------------
DROP TABLE IF EXISTS `sch_subject_type_iep`;
CREATE TABLE `sch_subject_type_iep` (
  `subj_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_type` varchar(255) DEFAULT NULL,
  `subject_type_kh` varchar(255) DEFAULT NULL,
  `main_type` varchar(255) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `is_moeys` int(11) DEFAULT '0',
  `orders` int(11) DEFAULT '0',
  `rangelevelid` int(11) DEFAULT NULL,
  `is_report_display` int(11) DEFAULT '1' COMMENT '1= yes , 0= No',
  `is_group_calc` int(11) DEFAULT NULL,
  `is_group_calc_percent` int(11) DEFAULT NULL,
  `is_class_participation` int(11) DEFAULT NULL COMMENT 'Class-Participation',
  `is_attendance_qty` double DEFAULT NULL,
  `type_subject` int(11) DEFAULT NULL,
  PRIMARY KEY (`subj_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_teacher_class`
-- ----------------------------
DROP TABLE IF EXISTS `sch_teacher_class`;
CREATE TABLE `sch_teacher_class` (
  `tch_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `gradelevelid` int(11) DEFAULT NULL,
  PRIMARY KEY (`tch_class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_teacher_classhandle`
-- ----------------------------
DROP TABLE IF EXISTS `sch_teacher_classhandle`;
CREATE TABLE `sch_teacher_classhandle` (
  `tch_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` varchar(100) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `gradelevelid` int(11) DEFAULT NULL,
  PRIMARY KEY (`tch_class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_teacher_subject`
-- ----------------------------
DROP TABLE IF EXISTS `sch_teacher_subject`;
CREATE TABLE `sch_teacher_subject` (
  `tch_subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `programid` int(11) DEFAULT NULL,
  `gradelevelid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  PRIMARY KEY (`tch_subject_id`)
) ENGINE=MyISAM AUTO_INCREMENT=365 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_teacher_summary`
-- ----------------------------
DROP TABLE IF EXISTS `sch_teacher_summary`;
CREATE TABLE `sch_teacher_summary` (
  `tch_transno` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `yearid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `gradelevelid` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`tch_transno`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_time`
-- ----------------------------
DROP TABLE IF EXISTS `sch_time`;
CREATE TABLE `sch_time` (
  `timeid` int(11) NOT NULL AUTO_INCREMENT,
  `from_time` time DEFAULT NULL,
  `to_time` time DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `minute_relax` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `for_tran_type` int(11) DEFAULT NULL COMMENT 'can be 1=study,2=teach,3=off_work',
  `am_pm` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`timeid`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_time_table`
-- ----------------------------
DROP TABLE IF EXISTS `sch_time_table`;
CREATE TABLE `sch_time_table` (
  `time_tableid` int(11) NOT NULL AUTO_INCREMENT,
  `dayid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `timeid` int(11) DEFAULT NULL,
  `teacherid` int(11) DEFAULT NULL,
  `note` text,
  `year` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `is_leave_time` int(11) DEFAULT NULL COMMENT 'Leave',
  `create_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `transno` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`time_tableid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_uploadfile`
-- ----------------------------
DROP TABLE IF EXISTS `sch_uploadfile`;
CREATE TABLE `sch_uploadfile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filetype` varchar(250) DEFAULT NULL,
  `typeno` int(11) DEFAULT NULL,
  `teacherid` int(11) DEFAULT NULL,
  `filename` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=176 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `sch_user`
-- ----------------------------
DROP TABLE IF EXISTS `sch_user`;
CREATE TABLE `sch_user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `last_visit_ip` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `is_admin` int(11) DEFAULT '0',
  `schoolid` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `def_sch_level` varchar(255) DEFAULT NULL,
  `def_dashboard` varchar(255) DEFAULT NULL,
  `def_open_page` varchar(255) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `match_con_posid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM AUTO_INCREMENT=322 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_user_schlevel`
-- ----------------------------
DROP TABLE IF EXISTS `sch_user_schlevel`;
CREATE TABLE `sch_user_schlevel` (
  `accid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `schlevelid` int(11) DEFAULT NULL,
  PRIMARY KEY (`accid`)
) ENGINE=MyISAM AUTO_INCREMENT=453 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_using_stock_master`
-- ----------------------------
DROP TABLE IF EXISTS `sch_using_stock_master`;
CREATE TABLE `sch_using_stock_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `tran_date` datetime DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `typeno` int(11) DEFAULT NULL,
  `note` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_att_base`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_att_base`;
CREATE TABLE `sch_z_att_base` (
  `baseattid` int(11) NOT NULL AUTO_INCREMENT,
  `base_att` varchar(255) NOT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`baseattid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_currency`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_currency`;
CREATE TABLE `sch_z_currency` (
  `curid` int(11) NOT NULL AUTO_INCREMENT,
  `currcode` varchar(255) DEFAULT NULL,
  `curr_name` varchar(255) DEFAULT NULL,
  `symbol` varchar(255) DEFAULT NULL,
  `is_default` int(11) DEFAULT NULL,
  `ex_rate` double DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`curid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_health_type`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_health_type`;
CREATE TABLE `sch_z_health_type` (
  `healthtypeid` int(11) NOT NULL AUTO_INCREMENT,
  `health_type` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`healthtypeid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_leave_type`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_leave_type`;
CREATE TABLE `sch_z_leave_type` (
  `leavetypeid` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `default_value` double DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  PRIMARY KEY (`leavetypeid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_module`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_module`;
CREATE TABLE `sch_z_module` (
  `moduleid` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) DEFAULT NULL,
  `sort_mod` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `mod_position` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`moduleid`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_page`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_page`;
CREATE TABLE `sch_z_page` (
  `pageid` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `moduleid` int(11) DEFAULT '1',
  `order` int(11) DEFAULT NULL,
  `is_insert` int(11) DEFAULT NULL,
  `is_update` int(11) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `is_show` int(11) DEFAULT NULL,
  `is_print` int(11) DEFAULT NULL,
  `is_export` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `icon` varchar(255) DEFAULT 'fa-bars',
  PRIMARY KEY (`pageid`)
) ENGINE=MyISAM AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_period_type`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_period_type`;
CREATE TABLE `sch_z_period_type` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `period_code` varchar(255) DEFAULT NULL,
  `period_name` varchar(255) DEFAULT NULL,
  `per_group` varchar(255) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_postion_group`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_postion_group`;
CREATE TABLE `sch_z_postion_group` (
  `match_con_posid` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`match_con_posid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_role`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_role`;
CREATE TABLE `sch_z_role` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  `is_admin` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  PRIMARY KEY (`roleid`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_role_module_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_role_module_detail`;
CREATE TABLE `sch_z_role_module_detail` (
  `mod_rol_id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) DEFAULT NULL,
  `moduleid` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`mod_rol_id`)
) ENGINE=MyISAM AUTO_INCREMENT=449 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_role_page`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_role_page`;
CREATE TABLE `sch_z_role_page` (
  `role_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) DEFAULT NULL,
  `pageid` int(11) DEFAULT NULL,
  `moduleid` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `is_read` int(1) DEFAULT '1',
  `is_insert` int(1) DEFAULT '1',
  `is_delete` int(1) DEFAULT '1',
  `is_update` int(1) DEFAULT '1',
  `is_print` int(1) DEFAULT '1',
  `is_export` int(1) DEFAULT '1',
  `is_import` int(1) DEFAULT '1',
  PRIMARY KEY (`role_page_id`)
) ENGINE=MyISAM AUTO_INCREMENT=830 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_systype`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_systype`;
CREATE TABLE `sch_z_systype` (
  `typeid` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT '0',
  PRIMARY KEY (`typeid`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sch_z_week`
-- ----------------------------
DROP TABLE IF EXISTS `sch_z_week`;
CREATE TABLE `sch_z_week` (
  `weekid` int(11) NOT NULL AUTO_INCREMENT,
  `week` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`weekid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
