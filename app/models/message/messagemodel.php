<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Messagemodel extends CI_Model{

		function __construct()
		{
			parent::__construct();
		}
		public function getUserName()
		{
			return $this->session->userdata('user_name');
		}
		public function setWhere($tablename,$uname='',$sfrom='',$sdate='')
		{
			$this->db->select("*")->from($tablename);
			if($this->green->getActiveRole()!=1)
			{	
				$this->db->where("user_name = '".$this->getUserName()."' OR sent_from ='".$this->getUserName()."' OR sent_to ='".$this->getUserName()."'");
			}
			if($uname!='' || empty($uname)){
				$this->db->like('user_name',$uname);	
			}else if($sfrom!='' || empty($sfrom)){
				$this->db->like('sent_from',$sfrom);
			}else if($sdate!='' || empty($sdate)){
				$this->db->like("DATE_FORMAT(sent_date, '%d-%m-%Y %h:%m:%s')",$sdate);
			}
			return  $this->db->order_by("smsid", "desc");
		}
		public function setPagination($tablename,$url='',$uname='',$sfrom='',$sdate='')
		{
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];

			$config['base_url']=$url;
			$config['page_query_string'] = TRUE;
			$config['per_page']=50;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$config['total_rows']=$this->setWhere($tablename,$uname,$sfrom,$sdate)->get()->num_rows();
			$this->setWhere($tablename,$uname,$sfrom,$sdate)->limit($config['per_page'],$page);
			$this->pagination->initialize($config);
			return $this->db->get()->result();	
		}
		function getpagination()
		{	 
			return $this->setPagination("sch_message",site_url("message/message/index?index=index"),'','','');	
		}	
		function searchs($uname,$sfrom,$sdate)
		{
			return $this->setPagination("sch_message",site_url("message/message/searchs?un=$uname&sf=$sfrom&sd=$sdate"),$uname,$sfrom,$sdate);	
		}
		function getmessagerow($id)
		{
			$this->db->where('smsid',$id);
			$query=$this->db->get('sch_message');
			return $query->row();
		}
		function countinbox()
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_message');

		    return  $this->db->count_all_results();
		}
		public function sqlGetUser($whereclause='')
		{
			$where='';
			if($whereclause!="")
			{
				$where.=" AND su.last_name like '".$whereclause."%'
						  OR su.first_name like '".$whereclause."%'
						";

			}
			$sql= "SELECT
						su.user_name,
						su.last_name,
						su.first_name,
						CONCAT(su.last_name,' ', su.first_name) AS fullname,
						su.email
					FROM
						sch_user su
					WHERE
						1 = 1 {$where}
					AND su.is_active = 1 AND su.user_name !='0' AND su.user_name !=''";
			return $this->db->query($sql)->result();
		}
		public function saveData($user_name,$sent_from,$emailto,$emailcc,$subject,$message,$fulldate,$reply_to='')
		{
			$sqlQuery= "INSERT INTO 
									sch_message SET
									user_name = '".$user_name."',
									sent_from = '".$sent_from."',
									reply_to = '".$reply_to."',
									sent_to = '".$emailto."',
									cc_to = '".$emailcc."',
									bcc_to = '',
									`subject` = '".$subject."',
									description = '".$message."',
									attach_file = '',
									sent_date = '".$fulldate."'
						";
			return $this->db->query($sqlQuery);
		}
		public function loadingHtmlData($arrData=array()){
			$i=1;
		    $tr="";
		    foreach ($arrData as $row) {
		     	$tr.="		      
			        <tr>
			          <td><input type='checkbox' id='check' name='check' class='selected' rel='".$row->smsid."'/></td>
			          <td align='center'>".$i."</td>
			          <td>".$row->user_name."</td>
			          <td>".$this->getFullName($row->sent_from)."</td>
			          <td>".$this->getFullName($row->sent_to)."</td>";
			    if($row->reply_to!="")
			    {
					$tr.="<td><strong>".$row->reply_to.':</strong> '.$row->subject."</td>";
			    }else{
					$tr.="<td>".$row->subject."</td>";
			    }
			     $tr.="<td>".date('d/m/Y h:i A',strtotime($row->sent_date))."</td>
			          <td align='center'>";
			          	  //if(!$this->green->gAction("P")){
			              	$tr.="<a target='_blank' href='".site_url('message/message/preview/'.$row->smsid)."'><img src='".site_url('../assets/images/icons/preview.png')."' style='width:24px !important;' /></a> ";
			              //}
			             // if(!$this->green->gAction("D")){
			                $tr.="<a><img onclick='deletes(event);' rel='$row->smsid' src='".site_url('../assets/images/icons/delete.png')."' /></a>";
			             //}
			           "</td>
			        </tr>";
			        $i++;
		    }
		    $tr.="<tr>
     				<td colspan='8' id='pgt' align='center'>
     					<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
     				</td>
    		   </tr>";
    		return $tr;
		}
		public function getEmail($user_name)
		{
			return $this->green->getValue("SELECT u.email 
										FROM sch_user u 
										WHERE u.user_name='".$user_name."'
								   ");
		}
		public function getFullName($user_name)
		{
			return $this->green->getValue("SELECT
												CONCAT(
													u.last_name,
													' ',
													u.first_name
												) fullusername
											FROM
												sch_user u
											WHERE
												1 = 1
											AND u.user_name = '".$user_name."'
										");
		}
	}
?>