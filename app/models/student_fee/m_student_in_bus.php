<?php
    class m_student_in_bus extends CI_Model{

    	function allarea(){
            return $this->db->query("SELECT DISTINCT
												a.areaid,
												a.area
											FROM
												sch_setup_area AS a
											ORDER BY
												a.area")->result();

		}
		// get_bus ----------------------------------------------
		function get_bus($areaid = ''){
	        $qr_schbus = $this->db->query("SELECT
													b.busid,
													b.busno,
													ba.areaid
												FROM
													sch_setup_busarea AS ba
												LEFT JOIN sch_setup_bus AS b ON b.busid = ba.busid
												WHERE
													ba.areaid = '{$areaid}'
												ORDER BY
													b.busno ASC ")->result();        
	        
	        $arr = array('bus' => $qr_schbus);
	        return json_encode($arr);
	    }
	    // get_driver --------------------------------------------
	    function get_driver($busid = ''){
	        $qr_schdriver = $this->db->query("SELECT DISTINCT
												d.driverid,
												d.driver_name
											FROM
												sch_setup_driver AS d
											WHERE
	                                            d.busid = '{$busid}' - 0
											ORDER BY
												d.driver_name ASC ")->result();        
	        
	        $arr = array('dri' => $qr_schdriver);
	        return json_encode($arr);
	    }
	    
	    // get_busid ----------------------------------------------
		function get_busid($area_id = ''){
	        $qr_schbusid = $this->db->query("SELECT
													b.busid,
													b.busno,
													ba.areaid
												FROM
													sch_setup_busarea AS ba
												LEFT JOIN sch_setup_bus AS b ON b.busid = ba.busid
												WHERE
													ba.areaid = '{$area_id}'
												ORDER BY
													b.busno ASC ")->result();        
	        
	        $arr = array('bus' => $qr_schbusid);
	        return json_encode($arr);
	    }
	    // get_driverid --------------------------------------------
	    function get_driverid($bus_id = ''){
	        $qr_schdriverid = $this->db->query("SELECT DISTINCT
												d.driverid,
												d.driver_name
											FROM
												sch_setup_driver AS d
											WHERE
	                                            d.busid = '{$bus_id}' - 0
											ORDER BY
												d.driver_name ASC ")->result();        
	        
	        $arr = array('dri' => $qr_schdriverid);
	        return json_encode($arr);
	    }

	    // save -----------------------------------------------------
        function savedata($studentid){ 
        	date_default_timezone_set("Asia/Bangkok");
        	$user = $this->session->userdata('user_name');
            $area_id = $this->input->post('area_id');
            $bus_id = $this->input->post('bus_id');
            $driver_id = $this->input->post('driver_id');
            $to_date = date('Y-m-d h:m:s');
            $arr = $this->input->post('arr_check');

            if(!empty($arr)){
	            foreach($arr as $r){

        		$data = ['areaid' => $area_id,
						'busid' => $bus_id, 
						'trandate' => $to_date,
						'driverid' => $driver_id,
						'otherfee_id' => $r['othefeeid']
    					];
            	$i = $this->db->update('sch_student_fee_type', $data, array('typeno' => $r['studenttype']));	            	
	            }
            }                   
           
            return $i;
        }
    }