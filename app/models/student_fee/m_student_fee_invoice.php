<?php
class M_student_fee_invoice extends CI_Model{

	function __construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__construct();

	}

	function grid(){
		$this->green->setActiveRole($this->input->post('roleid'));
  		$sortby=$this->input->post("sortby");
		$sorttype=$this->input->post("sorttype");

		$offset = $this->input->post('offset') - 0;
        $limit_record = $this->input->post('limit_record') - 0;
		$student_num = trim($this->input->post('student_num', TRUE));
		$full_name = trim($this->input->post('full_name', TRUE));
		$full_kh = trim($this->input->post('full_kh', TRUE));
		$gender = trim($this->input->post('gender', TRUE));
		$schoolid = trim($this->input->post('schoolid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelid = trim($this->input->post('schlevelid', TRUE));
		$term_sem_year_id = trim($this->input->post('term_sem_year_id', TRUE));
		$feetypeid = trim($this->input->post('feetypeid', TRUE));
		$yearid = trim($this->input->post('yearid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$is_paid = trim($this->input->post('is_paid', TRUE));
		$is_closed = trim($this->input->post('is_closed', TRUE));		
		$from_date = trim($this->input->post('from_date', TRUE));
		$to_date = trim($this->input->post('to_date', TRUE));	
		$sortstr="";
		$where = '';
		// if($student_num!= ""){
		//     		$where .= "AND v_student_fee_create_inv.student_num LIKE '%".$student_num."%' ";
		//      	}

		if($student_num != ''){
			$where .= "AND student_num LIKE '%{$student_num}%' ";
			//$this->db->like('student_num', $student_num, 'both'); 
		}
		
		if($full_name != ''){
				$where .= "AND CONCAT(
									first_name,
									' ',
									last_name
								) LIKE '%{$full_name}%' ";
			//$this->db->like("CONCAT(first_name, ' ', last_name)", $full_name, "both");
		}	
		if($full_kh != ''){
				$where .= "AND CONCAT(
									first_name_kh,
									' ',
									last_name_kh
								) LIKE '%{$full_kh}%' ";
			//$this->db->like("CONCAT(first_name_kh, ' ', last_name_kh)", $full_kh, "both");
		}		
		if($gender != ''){
			 $where .= "AND gender = '{$gender}' ";
			//$this->db->where("gender", $gender);
		}
		// if($schoolid != ''){
		// 	 $where .= "AND schoolid = '{$schoolid}' ";
		// 	//$this->db->where("schoolid", $schoolid);
		// }
		if($programid != ''){
			$where .= "AND programid = '{$programid}' ";
			//$this->db->where("programid", $programid);
		}
		if($schlevelid != ''){
			$where .= "AND schlevelid = '{$schlevelid}' ";
			//$this->db->where("schlevelid", $schlevelid);
		}
		if($yearid != ''){
			$where .= "AND year = '{$yearid}' ";
			//$this->db->where("year", $yearid);
		}		
		if($term_sem_year_id != ''){
			$where .= "AND term_sem_year_id = '{$term_sem_year_id}' ";
			//$this->db->where("term_sem_year_id", $term_sem_year_id);
		}
		if($feetypeid != ''){
			 $where .= "AND feetypeid = '{$feetypeid}' ";
			//$this->db->where("feetypeid", $feetypeid);
		}
		if($rangelevelid != ''){
			$where .= "AND rangelevelid = '{$rangelevelid}' ";
			//$this->db->where("rangelevelid", $rangelevelid);
		}
		if($classid != ''){
			$where .= "AND classid = '{$classid}' ";
			//$this->db->where("classid", $classid);
		}
		if($is_paid != ''){
			$where .= "AND is_paid = '{$is_paid}' ";
			//$this->db->where("is_paid", $is_paid);
		}
		
		
	    (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";
		// result =======
		// $str='';
		// if($sortstr!=""){
		// 	$str.="";
		// }
		$totalRecord = $this->db->query("SELECT COUNT(s.enrollid) AS c FROM v_student_fee_create_inv AS s WHERE 1=1 {$where} ")->row()->c - 0;
		$totalPage = ceil($totalRecord/$limit_record- 0) - 0;

		$qr = $this->db->query("SELECT * FROM v_student_fee_create_inv WHERE 1=1 {$where} {$sortstr} LIMIT $offset, $limit_record "); 
		$result = $qr->result();
		$sqls=count($result);
		$tr="";
		if(isset($result) && count($result)>0){
			$i=1;
			
			foreach($result as $row){
				
				$no_imgs = base_url()."assets/upload/students/NoImage.png";	
				$have_img = base_url()."assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg';
				$img = '<img src="'.$no_imgs.'" class="img-circle" alt="No Image" style="width: 70px;height: 70px;">';
				if (file_exists(FCPATH . "assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg')) {				
				$img = '<img src="'.$have_img.'" class="img-circle" alt="No Image" style="width: 70px;height: 70px;">';
				}

				
				$liks="";
				$lik_create="";
				$attr_is_pt=0;
				if($row->programid==3){
					$attr_is_pt=1;
				}
				
				if($row->is_paid==0){
					$add_class='class="active valign_top"';
					$liks='<div class="tr_studentcode create_inv" isaddnewinv="0" attr_paymethod="'.$row->feetypeid.'" attr_is_pt="'.$attr_is_pt.'"  attr_term_sem_year_id="'.$row->term_sem_year_id.'" attr_period="'.$row->term_sem_year_id.'"  attr_student_num="'.$row->student_num.'" attr_enroid="'.$row->enrollid.'" attr_stuid="'.$row->studentid.'" data-target="#mdinvoice" data-toggle="modal">Create<br>Invoice($)</div>';
					$cl_trad='inactive';
				}else if($row->is_paid==1){
					$prints=site_url("student_fee/c_student_print_invoice");
					$add_class='class="inactive valign_top"';
					
					$liks='<div class="tr_studentcode addnew_inv" isaddnewinv="1"  attr_paymethod="'.$row->feetypeid.'" attr_is_pt="'.$attr_is_pt.'" attr_term_sem_year_id="'.$row->term_sem_year_id.'" attr_period="'.$row->term_sem_year_id.'"  attr_student_num="'.$row->student_num.'" attr_enroid="'.$row->enrollid.'" attr_stuid="'.$row->studentid.'" data-target="#mdinvoice" data-toggle="modal">Add&nbsp;More<br>Invoice($)</div>';
					$cl_trad='success';
				}
				
				if($this->green->gAction("D")){	
					// $lik_create=$liks;
				}	
						
				$lik_create=$liks;
				$tr.='<tr class="'.$cl_trad.'">
						<td>'.($i++ + $offset).'</td>
						<td style="text-align: center;">'.$img.'</td>
						<td>
							'.$row->student_num.'<br>
							'.$row->last_name_kh.' '.$row->first_name_kh.' <br>
							'.$row->last_name.' '.$row->first_name.' <br>
							'.$row->gender.'<br>
						</td>
						<td>'.$row->program.'</td>
						<td>'.$row->sch_level.'</td>
						<td>'.$row->sch_year.'</td>
						<td>'.$row->rangelevelname.'</td>
						<td>'.$row->class_name.'</td>
						<td>'.$row->period.'</td>
						<td  class="remove_tag no_wrap" align="center">'.$lik_create.'</td>
						
					 </tr>';
			}	
			// $arr['tr']=$tr;
		}else{
			// $arr['tr']
			$tr .='<tr><td colspan="10" align="center">We did not find anything to show here.</td></tr>';
			}
		
		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
       	return json_encode($arr);	
		//------------
		
		
	}
//--------------------------------------------------

public function FMupdate($attr_typeno){  
      $datas="SELECT
        sch_student_fee_type.typeno,
        sch_student_fee_type.type,
        sch_student_fee_type.description,
        sch_student_fee_type.`not`,
        sch_student_fee_type.amt_line,
        sch_student_fee_type.prices,
        sch_student_fee_type.amt_dis,
        sch_student_fee_type.otherfee_id,
        sch_student_fee_type.qty
       FROM
        sch_student_fee_type
       where 1=1
       AND sch_student_fee_type.typeno='".$attr_typeno."'
       AND sch_student_fee_type.type=17      
      ";
      // echo $datas; exit();
      
      $result = $this->db->query($datas)->result();
      // print_r($classes);
      $trdata=""; 
      $amttotal=0;
      $ii=1;
	  $trs="";
      $arr=array();
      if(isset($result) && count($result)>0){        
       foreach($result as $rowde){
		   //--------------
			$query=$this->db->get('sch_setup_otherfee')->result();
			
			$oppaylist="";
			$oppaylist="<option attr_price='0' value=''></option><option attr_price='create_new' value='create_new'><span style='color:blue'>--- Create New ---</span></option>";
			
			foreach ($query as $rowotherfee) {				
				$oppaylist.="<option ".(($rowde->otherfee_id)==($rowotherfee->otherfeeid)?"selected=selected":"")." attr_typefee='".$rowotherfee->typefee."' attr_datetran='".$this->green->convertSQLDate($rowotherfee->fromdate)."  -  ". $this->green->convertSQLDate($rowotherfee->todate)."' attr_price='".$rowotherfee->prices."' value='".$rowotherfee->otherfeeid."'>".$rowotherfee->otherfee."</option>";
			}			
			$photodel="<img src='".site_url('../assets/images/icons/delete.png')."'/>";
		   //--------------		  
		   $trdata.='<tr class="tr_addrow">
						<td>
							<select id="paymenttype_list" class="form-control paymenttype_list gform" required="">'.$oppaylist.'</select>
							<input id="paytypeid" class="form-control payment_des" type="text" value="'.$rowde->description.'" style="text-align:left; display:none">
							<input id="is_bus" class="form-control is_bus" type="text" value="" style="text-align:left; display:none">
						</td>
						<td>
							<input id="paytypeid" class="form-control payment-note"  value="'.($rowde->not).'" type="text" placeholder="" style="text-align:right">
						</td>
						<td>
							<input id="numqty" class="form-control numqty" type="text" placeholder="" value="'.($rowde->qty).'"  style="text-align:right">
						</td>
						
						<td>
							<input id="paytypeid" class="form-control payment-unitprice" type="text" placeholder=""  value="'.($rowde->prices).'"  style="text-align:right">
						</td>
						<td>
							<input id="paytypeid" class="form-control payment-dis" type="text" placeholder=""  value="'.($rowde->amt_dis).'"  style="text-align:right">
						</td>
						<td>
							<input id="paytypeid" class="form-control payment-amt" type="text" placeholder=""  value="'.($rowde->amt_line).'"  style="text-align:right">
						</td>
						<td style="text-align:center">&nbsp;&nbsp;&nbsp;<a id="a_delete" class="a_delete a_delete1" style="text-align:center">'.$photodel.'</a></td>"+
				  </tr>';
		   //--------------
      //  $trdata.='<tr class="tr_addrow"><td>'.($rowde->description).'</td><td>'.($rowde->description).'</td><td>'.($rowde->description).'</td></tr>';
       }
      }
     $arr['tr_loop']=$trdata;     
    header("Content-type:text/x-json");
    echo json_encode($arr); 
   exit();
 }
 //--------------------------------------------------

//--------------------------------------------------

	function get_schlevel($programid = ''){
        $qr_schlevel = $this->db->query("SELECT DISTINCT
                                                l.schlevelid,
                                                l.sch_level
                                            FROM
                                                sch_school_level AS l
                                            WHERE
                                                l.programid = '{$programid}' - 0
                                            ORDER BY
                                                l.sch_level ASC ")->result();        
        
        $arr = array('schlevel' => $qr_schlevel);
        return json_encode($arr);
    }

    function get_year($programid = '', $schlevelid = ''){
        $qr_year = $this->db->query("SELECT DISTINCT
                                            y.yearid,
                                            y.sch_year
                                        FROM
                                            sch_school_year AS y
                                        WHERE
                                            y.programid = '{$programid}' - 0 AND y.schlevelid = '{$schlevelid}' - 0
                                        ORDER BY
                                            y.sch_year ASC ")->result();

        $qr_class = $this->db->query("SELECT
													c.classid,
													c.class_name
												FROM
													sch_school_level AS l
												INNER JOIN sch_class AS c ON l.schlevelid = c.schlevelid
												WHERE
													l.schlevelid = '{$schlevelid}'
												ORDER BY
													c.class_name ASC ")->result();
        
        $arr = array('year' => $qr_year, 'class' => $qr_class);
        return json_encode($arr);
    }

    // get rangelevel =========
    function get_rangelevel($yearid = ''){
        $qr_rangelevel = $this->db->query("SELECT DISTINCT
                                                rl.rangelevelid,
                                                rl.rangelevelname,
                                                rp.rangelevelid AS rangelevelid_period
                                            FROM
                                                sch_school_rangelevel AS rl
                                            INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
                                            AND rl.programid = y.programid
                                            LEFT JOIN sch_rangelevel_period AS rp ON rl.rangelevelid = rp.rangelevelid
                                            WHERE
                                                y.yearid = '{$yearid}' 
                                            ORDER BY rl.rangelevelname ASC ")->result();

        $arr = array('rangelevel' => $qr_rangelevel);
        return json_encode($arr);
    }
	
    // get_paymentType =========
    public function get_paymentType($feetypeid = '', $programid = '', $schlevelid = '', $yearid = ''){
    	if($feetypeid != ''){
    		$this->db->where("payment_type", $feetypeid);
    	}
    	if($programid != ''){
    		$this->db->where("programid", $programid);
    	}
    	if($schlevelid != ''){
    		$this->db->where("schlevelid", $schlevelid);
    	}
    	if($yearid != ''){
    		$this->db->where("yearid", $yearid);
    	}    	

        $qr_get_paymentType = $this->db->get("v_study_peroid")->result();
        return json_encode($qr_get_paymentType);
    }
	
	
	public function Fcstudentinfo($enrollid,$isaddnewinv,$studentid,$attr_is_pt){
		
		
    	$this->db->where("enrollid", $enrollid);
        $getrowInf = $this->db->get("v_student_fee_create_inv")->row();
		// print_r($getrowInf); exit();
		//----------------------------
		
		$sqltime="SELECT
							sch_time.from_time,
							sch_school_rangelevtime.rangelevelid,
							sch_time.am_pm,
							sch_time.timeid,
							sch_time.to_time
							FROM
							sch_school_rangelevtime
							INNER JOIN sch_time ON sch_school_rangelevtime.timeid = sch_time.timeid
							where 1=1 
							AND rangelevelid='".$getrowInf->rangelevelid."'
						  ";
		// echo $sqltime; exit();
			
		$result = $this->db->query($sqltime)->result();
		 // print_r($classes);
		
		$pttime="";	
		if(isset($result) && count($result)>0){		
			foreach($result as $myrows){
				$pttime.="<option value='".$myrows->timeid."'>".$myrows->from_time."-".$myrows->to_time."&nbsp;".$myrows->am_pm."</option>";
			}
		}
		$arr['studytime']=$pttime;
		
		//----------------------------
		
		$countmethod="SELECT
					COUNT(studentid) AS studentid
					FROM
						sch_student_fee
					WHERE 1=1
					AND studentid='".$studentid."'
					AND programid='".$getrowInf->programid."'
					AND schooleleve='".$getrowInf->schlevelid."'
					AND acandemic='".$getrowInf->year."'
					AND ranglev='".$getrowInf->rangelevelid."'
					AND classid='".$getrowInf->classid."'
					AND paymentmethod <> ''
					";
		// echo $countmethod; exit();
		
		$ispayexist = $this->db->query($countmethod)->row()->studentid;
		$arr['ispayexist']=$ispayexist;	
		$arr['is_pt']=$attr_is_pt;	
				
		$photodel="<img src='".site_url('../assets/images/icons/delete.png')."'/>";
		
		$selesql="SELECT
						v_student_fee_create_receipt.student_num,
						v_student_fee_create_receipt.typeno,
						v_student_fee_create_receipt.type,
						v_student_fee_create_receipt.first_name,
						v_student_fee_create_receipt.last_name,
						v_student_fee_create_receipt.first_name_kh,
						v_student_fee_create_receipt.last_name_kh,
						v_student_fee_create_receipt.phone1,
						v_student_fee_create_receipt.is_status,
						v_student_fee_create_receipt.amt_total,
						v_student_fee_create_receipt.amt_paid,
						v_student_fee_create_receipt.amt_balance,
						v_student_fee_create_receipt.note,
						v_student_fee_create_receipt.paymentmethod,
						v_student_fee_create_receipt.trandate,
						v_student_fee_create_receipt.paymenttype,
						v_student_fee_create_receipt.typeno_inv,
						v_student_fee_create_receipt.studentid
					FROM
						v_student_fee_create_receipt
					WHERE 1=1
					AND studentid='".$studentid."'
					ORDER BY v_student_fee_create_receipt.typeno DESC
					";
				// echo $selesql;
		$invOrders=$this->db->query($selesql)->result();
		
		
		$trinv="";
		// print_r($invOrders);
		$gtotal=0;
		$sub_total=0;
		if(isset($invOrders) && count($invOrders)>0)
		{
			$i=1;
			$theads="";
			
			foreach($invOrders as $row)
			{
				if($row->type!=32){
					$sch_z_systype="SELECT
								sch_z_systype.type
								FROM
								sch_z_systype
								WHERE 1=1
								AND typeid='".$row->type."'
					
					";
				// echo $countmethod; exit();
		
				$systype = $this->db->query($sch_z_systype)->row()->type;
		
				$fclassact='fclassact';
				$is_status=($row->is_status);
				$note=($row->note);
				$typeno_inv=($row->typeno_inv);
				$trandate=($row->trandate);
				$dtrandate=($this->green->convertSQLDate($row->trandate));
				
				if($this->green->gAction("D")){	
				//$theads=$deleteid;
				}
				
				//------------------------------------------------------------------------
					$count_receiptno="SELECT
										COUNT(enrollid) AS countis_paywith_inv
									FROM
										sch_student_fee_rec_order
									WHERE
										1 = 1
									AND is_paywith_inv =0
									AND sch_student_fee_rec_order.typeno_inv='".$row->typeno."'
									AND sch_student_fee_rec_order.type_inv='17'
									";
				$ispay_ready = $this->db->query($count_receiptno)->row()->countis_paywith_inv;
				
				$edite="";
				$deleteid="";
				if($ispay_ready > 0 ){// paid
						$edite='<input class="btn btn-disabled" value="Edit" name="btn_edite" type="button">';
						$deleteid='<input class="btn btn-disabled" value="Delete" name="btn_edite" type="button">';
								
				}else{
					if($row->type==17){
						$edite='<input id="btn_edite" attr_method="'.$row->paymentmethod.'" attr_study_period="'.$row->paymenttype.'" attr_type="'.$row->type.'" attr_typeno="'.$row->typeno.'" class="btn btn-success btn_edite" value="Edit" name="btn_edite" type="button">';
						$deleteid='<a id="a_delete_inv" class="a_delete_inv btn btn-primary btn-danger" attr_type='.$row->type.'  attr_id='.$row->typeno.' style="text-align:center"><span class="glyphicon glyphicon-trash"></span></a>';
					}else{
						$edite="";
						$deleteid='<a id="a_delete_inv" class="a_delete_inv btn btn-primary btn-danger" attr_type='.$row->type.'  attr_id='.$row->typeno.' style="text-align:center"><span class="glyphicon glyphicon-trash"></span></a>';
					
						}
				}
				//------------------------------------------------------------------------
				$add_note="";
				$istranstype="";
					if($note!=""){
						$add_note='<span style="color:green"><u>Add Note:</u><br>'.$note.'<span>';	
					}	
					$istranstype='<span style="color:green"><strong>'.$systype.'</strong></span>';
					$linkprint="<a class='btn btn-success' target='_blank' href='".site_url('student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno.'')."'><span class='glyphicon glyphicon-print'></span> Inv#".$row->typeno."</a>";
				$returnQUERY = $this->db->query("SELECT type,typeno FROM sch_student_fee WHERE type_inv='".$row->type."' AND typeno_inv='".$row->typeno."'")->row();
				$returnType = (count($returnQUERY) > 0 ? $returnQUERY->type :0)-0;
				$returnTypeno = (count($returnQUERY) > 0 ? $returnQUERY->typeno :0)-0;

				$totalReturnAmountQuery = <<<QUERY
																			SELECT IFNULL((
																				SELECT
																					IFNULL(SUM(return_amount), 0)
																				FROM
																					sch_student_fee_rec_detail
																				WHERE
																					type = 32
																				AND type = $returnType
																				AND typeno = $returnTypeno
																				GROUP BY
																					typeno_inv,
																					typeno_inv
																				), 0) AS 'return_amount';
QUERY;
				$totalReturnAmount = $this->db->query($totalReturnAmountQuery)->row()->return_amount;
				$linkprint_return="<a class='btn btn-success' target='_blank' href='".site_url('student_fee/c_student_receipt_cash_return?param_reno='.$returnTypeno.'')."' ><span class='glyphicon glyphicon-print'></span> <span style='color:#f00;'>Return#".$returnTypeno."</span></a>";
				$Html_ReturnAmount = '<span style="color:green"><strong  style="color:#f00;">Return Amount '.number_format($totalReturnAmount,2).' $ </strong></span>';
				$trinv.='<tr class="active cl_inv">
							<td class="">
								<table border="0" width="100%">
									<tr>
										<td><span style="color:red;display:none">'.($i++).'</span>
										'.$linkprint.'
										<br>
										'.$istranstype.'
										
										<br>
										<span style="color:green"><strong>Date:'.$dtrandate.'</strong></span>
										</td>
										<td valign="top" width="20%">'.($returnTypeno>0?$linkprint_return:'').'<br>
										'.($returnTypeno>0?$Html_ReturnAmount:'').'
										</td>
										<td></td>
									</tr>					
								</table>
							</td>
							<td colspan="2"></td>
							<td align="right">'.$edite.'</td>
							<td colspan="1" align="right">'.$deleteid.'</td>
							
						 </tr>';
						 
						$datas="SELECT
								sch_student_fee_type.typeno,
								sch_student_fee_type.type,
								sch_student_fee_type.description,
								sch_student_fee_type.`not`,
								sch_student_fee_type.amt_line,
								sch_student_fee_type.prices,
								sch_student_fee_type.amt_dis,
								sch_student_fee_type.qty
							FROM
								sch_student_fee_type
							where 1=1
							AND sch_student_fee_type.typeno='".$row->typeno."'
							AND prices <> ''
						
						";
						// echo $datas; exit();
						
						$result = $this->db->query($datas)->result();
						// print_r($classes);
						$trinvd="";	
						$amttotal=0;
						$ii=1;
					
						if(isset($result) && count($result)>0){	
							$des="";	
								foreach($result as $rowde){
									if($rowde->not !=""){
										$des=$rowde->description.'-'.$rowde->not;	
									}else{
										$des=$rowde->description;
									}
									$trinv.='<tr class="info">
										<td width="15%">'.$des.'</td>
										<td width="15%">'.$rowde->not.'</td>
										<td align="right">'.$rowde->qty.'</td>
										<td align="right">'.number_format($rowde->prices,2).'&nbsp;$</td>
										<td align="right">'.$rowde->amt_dis.' %</td>
										<td align="right">'.number_format($rowde->amt_line,2).'&nbsp;$</td>
									 </tr>';
									 $amttotal+=($rowde->amt_line);
								}
								// end for each loop
								$trinv.='<tr>
										<td></td>
										<td></td>
										
										<td></td>
										<td align="right" class="'.$fclassact.'">Total</td>
										<td align="right" class="'.$fclassact.'">'.number_format($row->amt_total,2).'&nbsp;$</td>
									 </tr>';
								$trinv.='<tr>
										<td>'.$add_note.'</td>
										<td style="text-align:left"></td>
										
										<td></td>
										<td align="right" class="'.$fclassact.'">Desposit</td>
										<td align="right" class="'.$fclassact.'">'.number_format($row->amt_paid,2).'&nbsp;$</td>
									 </tr>';
								$trinv.='<tr class="">
										<td></td>
										<td></td>
										
										<td></td>
										<td align="right" class="'.$fclassact.'">Sub Total</td>
										<td align="right" class="'.$fclassact.'">'.number_format($row->amt_balance,2).'&nbsp;$</td>
									 </tr>';
							$gtotal+=($row->amt_total)-0;
							$sub_total+=$row->amt_balance-0;		 
						}// if(isset($result) && count($result)>0){
			}
			}// foreach($invOrders as $row){

				$trinv.='<tr>
							<td></td>
							<td></td>
							
							<td></td>
							<td align="right" class="'.$fclassact.'"><strong style="display:none">Grand Total 677</strong></td>
							<td align="right" class="'.$fclassact.'"><strong style="display:none">'.number_format($gtotal,2).'&nbsp;$</strong></td>
						 </tr>';
			$arr['trinv']='<fieldset>
								<legend class="btn-success">
										<span class="glyphicon glyphicon-stats"></span> History of Student Fee</legend>
										<table class="tbl_history" style="width:100% !important;">
										<tr>
											<td class="td_history">Description</td>
											<td class="td_history">Note</td>
											<td class="td_history" align="right">Quantity</td>
											<td class="td_history" align="right">Unit Price</td>
											<td class="td_history" align="right">Dis(%)</td>
											<td class="td_history" align="right">Amount</td>
										</tr>
								'.$trinv.'
							</table></fieldset>';
		}// if(isset($invOrders) && count($invOrders)>0){
					   
		if(isset($getrowInf) && count($getrowInf)>0){
			
			$have_img = base_url()."assets/upload/students/".$getrowInf->year.'/'.$getrowInf->studentid.'.jpg';		
			$no_imgs = base_url()."assets/upload/students/NoImage.png";	
			
			$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:155px;height:155px;">';
			if (file_exists(FCPATH . "assets/upload/students/".$getrowInf->year.'/'.$getrowInf->studentid.'.jpg')) {				
				$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:155px;height:155px;">';
			}
			
		 $arr['image_stu']=$img;
		
		 $arr['getRow']=$getrowInf;
		 $arr['sub_total']=number_format($sub_total,2);
		  return json_encode($arr);
		}
		//header("Content-type:text/x-json");
		// echo json_encode($arr);
	}
	
	public function MFsave($typeno = ''){
		$schoolid=$this->session->userdata("schoolid");
		$isupdate_save = $this->input->post('isupdate_save');
		$isupdate_type = $this->input->post('isupdate_type');
		$student_num = $this->input->post('student_num');
		$arr_inv = $this->input->post('arr_inv');
		$duedate = $this->input->post('duedate');
		$enrollid = $this->input->post('enrollid');
		$opstudytime = $this->input->post('opstudytime');
		$is_condic=$this->input->post('h_iscond_save');		
		$h_isaddnewinv=$this->input->post('h_isaddnewinv');		
		
		
		$programid=$this->input->post('opprpid');
		$yearid = $this->input->post('acandemicid');
		$schoolid=$this->session->userdata('schoolid');
		$schlevelid = $this->input->post('shlevelid');
		
		$text_note = $this->input->post('text_note');
		$is_parttime = $this->input->post('is_generalenglish');
		
		$trandate = $this->green->formatSQLDate($this->input->post('trandate'));
		$typenore=0;
		$type=17;
		$typere=18;
		
		if($isupdate_save==""){						
			$oppaymentmethod = $this->input->post('oppaymentmethod');
			$studyperiod = $this->input->post('paymenttype');
			
			$nextTran=$this->green->nextTran($type,"Create Invoice");
			
			$created_date = date('Y-m-d H:i:s');
		//print_r($arr_inv);die();
			
			if(($this->input->post('amt_paid'))>0){					
					$nextTranre=$this->green->nextTran($typere,"Create Receipt");
			};
			
		}else{
			//=========isupdate_save
			//$typenore="";	
			$count_typeno=0;	
			$nextTran=$isupdate_save;			
			$oppaymentmethod = $this->input->post('isupdate_paymentmethod');
			$studyperiod = $this->input->post('isupdate_studyperiod');
			
			$counttypeno_re=$this->db->query("SELECT
											sch_student_fee_rec_order.typeno,
											COUNT(typeno) as count_typeno
											FROM
											sch_student_fee_rec_order
											WHERE 1=1
											AND sch_student_fee_rec_order.typeno_inv ='".$nextTran."'
											");
		/*$program_id=1;
		$school_level_id=1;
		$adcademic_year_id=1;
		$school_id=1;
		$subject_id=1;
		$classid=1;		
		// $this->green->RankSubjMonthKgp($program_id="",$school_level_id="",$adcademic_year_id="",$school_id="",$classid="",$subject_id="",$studenid="",$exam_type="");
		*/
		
		$created_date = date('Y-m-d H:i:s');
		//print_r($arr_inv);die();
		
		if(($this->input->post('amt_paid'))>0){
				$typere=18;
				$nextTranre=$this->green->nextTran($typere,"Create Receipt");
		};

		if($counttypeno_re->num_rows() > 0 ){
			foreach($counttypeno_re->result() as $row_r){
				$typenore = $row_r->typeno;
				$count_typeno = $row_r->count_typeno;
			}
			$this->db->where('typeno_inv', $nextTran);
			$this->db->delete('sch_student_fee_rec_order');
					   
			$this->db->where('typeno_inv', $nextTran);
			$this->db->delete('sch_student_fee_rec_detail');	
		}
		 $wdelete="";
		 $wdelete = array('type' => $isupdate_type,'typeno' => $nextTran);	
		 $this->db->delete('sch_student_fee',$wdelete);
		 $this->db->delete('sch_student_fee_type',$wdelete);
			
	}// }else{
		
		if($arr_inv != ""){								  
		foreach($arr_inv as $arr_detail){			
			$datas = array(
				'type' => $type,
				'typeno' =>$nextTran,
				'studentid' =>$this->input->post('stuid'),				
				'trandate'=>$trandate,
                'otherfee_id' => $arr_detail['paymenttype_list'],
				'description' => $arr_detail['payment_des'],
				'not' => $arr_detail['payment_note'],
                'prices' => $arr_detail['prices'],
                'amt_dis' => $arr_detail['amt_dis'],
                'qty' => $arr_detail['numqty'],
                'is_bus' => $arr_detail['is_bus'],
                'areaid' => $this->input->post('oparea'),
                'busid' => $this->input->post('opbusfeetype'),
               
				'amt_line' => $arr_detail['payment_amt']
			);	
		
			// print_r($datas); exit();
			
			$this->db->insert('sch_student_fee_type', $datas);
			
			if(($this->input->post('amt_paid'))>0){
				$datarece = array(
				'type' => $typere,
				'typeno' =>$nextTranre,
				'type_inv' => $type,
				'typeno_inv' =>$nextTran,
				'studentid' =>$this->input->post('stuid'),
				
				'trandate'=>$trandate,
				'description' => $arr_detail['payment_des'],
				'not' => $arr_detail['payment_note'],
                'prices' => $arr_detail['prices'],
                'amt_dis' => $arr_detail['amt_dis'],
                'qty' => $arr_detail['numqty'],
				'otherfee_id' => $arr_detail['paymenttype_list'],
				'is_bus' => $arr_detail['is_bus'],
                'areaid' => $this->input->post('oparea'),
                'busid' => $this->input->post('opbusfeetype'),
				'amt_line' => $arr_detail['payment_amt']
				);		
				$this->db->insert('sch_student_fee_rec_detail', $datarece);	
			};
			
		};// end foreach($arr_inv as $arr_detail){ ==================
		}// if($arr_inv != ""){
			
		$data = array(
				'type' => $type,
				'typeno' =>$nextTran,
				'studentid' =>$this->input->post('stuid'),
				'enrollid' =>$this->input->post('enrollid'),
				'trandate'=>$trandate,
				
				'amt_total' => $this->input->post('amt_total'),
				
				'amt_balance' => $this->input->post('amt_balance'),
				'duedate' => $this->green->formatSQLDate($duedate),
				
				'schoolid' => $schoolid,
				'programid' => $this->input->post('opprpid'),
				'schooleleve' => $this->input->post('shlevelid'),
				'acandemic' => $this->input->post('acandemicid'),
				'ranglev' => $this->input->post('ranglevid'),
				'classid' => $this->input->post('classe'),
				
				'paymentmethod' => $oppaymentmethod,
				'paymenttype' => $studyperiod,
				'areaid' => $this->input->post('oparea'),
				'note' => $this->input->post('text_note'),
				 'timeid' => $opstudytime,
				'busfeetypid' => $this->input->post('opbusfeetype')
       			);
			$this->db->insert('sch_student_fee', $data);
			
			if(($this->input->post('amt_paid'))>0){	
				$datarecipt = array(
				'type' => $typere,
				'typeno' =>$nextTranre,
				'type_inv' => $type,
				'typeno_inv' =>$nextTran,			
				'studentid' =>$this->input->post('stuid'),		
				'enrollid' =>$this->input->post('enrollid'),
				'trandate'=>$trandate,
				'amt_paid' => $this->input->post('amt_paid'),
				'schoolid' => $schoolid,
				'programid' => $this->input->post('opprpid'),
				'schooleleve' => $this->input->post('shlevelid'),
				'acandemic' => $this->input->post('acandemicid'),
				'ranglev' => $this->input->post('ranglevid'),
				'classid' => $this->input->post('classe'),				
				'paymentmethod' => $oppaymentmethod,
				'paymenttype' => $studyperiod,
				'areaid' => $this->input->post('oparea'),				
				'note' => $this->input->post('text_note'),
				 'timeid' => $opstudytime,
				'busfeetypid' => $this->input->post('opbusfeetype'),
				'is_paywith_inv' => 1
				);
				$this->db->insert('sch_student_fee_rec_order', $datarecipt);
			}
			
			
			if($h_isaddnewinv==0){
				// pay with school fee is relation with table enroment===========================================================
				// ==============================================================================================================
				// ==============================================================================================================
				
				if($is_condic==1 and $is_parttime==0){ 
					// 1-insert new record by select from paymentmethod 
					// 2-is full time only ($is_parttime==0)
					//---------------------------------------------------------------------------------------------------------
					$this->db->where('studentid', $this->input->post('stuid'));
					$this->db->where('programid',$programid);
					$this->db->where('schlevelid',$schlevelid);
					$this->db->where('year',$yearid);
					$this->db->where('rangelevelid', $this->input->post('ranglevid'));
					$this->db->where('classid', $this->input->post('classe'));	
								
					$this->db->delete('sch_student_enrollment');
					
					
					//---------------------------------------------------------------------------------------------------------			
					$datas="SELECT
								v_study_peroid.term_sem_year_id
								FROM
								v_study_peroid
							where 1=1
							AND v_study_peroid.programid='".$programid."'
							AND v_study_peroid.yearid='".$yearid."'
							AND v_study_peroid.schoolid='".$schoolid."'
							AND v_study_peroid.schlevelid='".$schlevelid."'
							AND v_study_peroid.payment_type='".$oppaymentmethod."'
							AND v_study_peroid.rangelevelid='".$this->input->post('ranglevid')."'
							";
					// echo $datas; exit();
					$result = $this->db->query($datas)->result();
					$datapayment="";	
					if(isset($result) && count($result)>0){		
						foreach($result as $rowbusfee){
							 $da = array(									
										'studentid' =>$this->input->post('stuid'),
										'schoolid'=>$schoolid,
										'year' => $this->input->post('acandemicid'),
										'classid' => $this->input->post('classe'),
										'schlevelid' => $this->input->post('shlevelid'),
										'programid' => $this->input->post('opprpid'),
										'feetypeid' => $oppaymentmethod,
										'rangelevelid' => $this->input->post('ranglevid'),
										'term_sem_year_id' => $rowbusfee->term_sem_year_id,
										'enroll_date' => $created_date
										);
								$this->db->insert('sch_student_enrollment', $da);
						}
					}// foreach($result as $rowbusfee){
					
				}else if($is_condic ==2 and $is_parttime==0){ //  end new creade
					// 1-update old recorded. 
					// 2-insert new record by select from paymentmethod
					// it  is change payment method
					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------					
					$datas="SELECT
								v_study_peroid.term_sem_year_id
								FROM
								v_study_peroid
							where 1=1
							AND v_study_peroid.programid='".$programid."'
							AND v_study_peroid.yearid='".$yearid."'
							AND v_study_peroid.schoolid='".$schoolid."'
							AND v_study_peroid.schlevelid='".$schlevelid."'
							AND v_study_peroid.payment_type='".$oppaymentmethod."'
							AND v_study_peroid.rangelevelid='".$this->input->post('ranglevid')."'
							";
					// echo $datas; exit();
					$result = $this->db->query($datas)->result();
					$datapayment="";	
					if(isset($result) && count($result)>0){		
						foreach($result as $rowbusfee){
							 $da = array(									
										'studentid' =>$this->input->post('stuid'),
										'schoolid'=>$schoolid,
										'year' => $this->input->post('acandemicid'),
										'classid' => $this->input->post('classe'),
										'schlevelid' => $this->input->post('shlevelid'),
										'programid' => $this->input->post('opprpid'),
										'feetypeid' => $oppaymentmethod,
										'rangelevelid' => $this->input->post('ranglevid'),
										'term_sem_year_id' => $rowbusfee->term_sem_year_id,
										'enroll_date' => $created_date
										);
								$this->db->insert('sch_student_enrollment', $da);
						}
					}// foreach($result as $rowbusfee){
						
				$updatsql="UPDATE sch_student_enrollment SET 
							is_closed=1
							WHERE 1=1
							AND studentid='".$this->input->post('stuid')."'
							AND schoolid='".$schoolid."'
							AND year='".$this->input->post('acandemicid')."'
							AND classid='".$this->input->post('classe')."'
							AND schlevelid='".$this->input->post('shlevelid')."'
							AND enrollid='".$enrollid."'
							AND rangelevelid='".$this->input->post('ranglevid')."'
							AND feetypeid <>'".$oppaymentmethod."'
						  ";
						//  echo $updatsql;
						  $this->green->runSQL($updatsql);
						  
				} // }else if($is_condic ==2){
				
				//---------------------------------------------------------------------------------------------------------	---------------------------------------------------------------------------------------------------------	---------------------------------------------------------------------------------------------------------	
				//---------------------------------------------------------------------------------------------------------	---------------------------------------------------------------------------------------------------------	---------------------------------------------------------------------------------------------------------	
				//---------------------------------------------------------------------------------------------------------	---------------------------------------------------------------------------------------------------------	---------------------------------------------------------------------------------------------------------	
				
				// For all payment 
				if($is_parttime==1){
				$updatsql="UPDATE sch_student_enrollment SET 
							type='".$type."',
							transno='".$nextTran."',
							feetypeid='".$oppaymentmethod."',
							term_sem_year_id='".$studyperiod."',
							is_paid=1
							WHERE 1=1
							AND studentid='".$this->input->post('stuid')."'
							AND schoolid='".$schoolid."'
							AND year='".$this->input->post('acandemicid')."'
							AND classid='".$this->input->post('classe')."'
							AND schlevelid='".$this->input->post('shlevelid')."'
							AND rangelevelid='".$this->input->post('ranglevid')."'
							AND enrollid='".$enrollid."'
						  ";
			$updatsql1="UPDATE sch_assign_payment_method_detail SET
							is_paid=2
							WHERE 1=1
							AND studentid='".$this->input->post('stuid')."'
							AND schoolid='".$schoolid."'
							AND acandemic='".$this->input->post('acandemicid')."'
							AND classid='".$this->input->post('classe')."'
							AND schooleleve='".$this->input->post('shlevelid')."'
							AND ranglev='".$this->input->post('ranglevid')."'
						  ";
						  
				// echo $updatsql;
				
				}else{
					$updatsql="UPDATE sch_student_enrollment SET 
							type='".$type."',
							transno='".$nextTran."',
							is_paid=1
							WHERE 1=1
							AND studentid='".$this->input->post('stuid')."'
							AND schoolid='".$schoolid."'
							AND year='".$this->input->post('acandemicid')."'
							AND classid='".$this->input->post('classe')."'
							AND schlevelid='".$this->input->post('shlevelid')."'
							AND rangelevelid='".$this->input->post('ranglevid')."'
							AND feetypeid='".$oppaymentmethod."'
							AND term_sem_year_id='".$studyperiod."'
						  ";
				 $updatsql1="UPDATE sch_assign_payment_method_detail SET
							is_paid=2
							WHERE 1=1
							AND studentid='".$this->input->post('stuid')."'
							AND classid='".$this->input->post('classe')."'
							AND schooleleve='".$this->input->post('shlevelid')."'
							AND schoolid='".$schoolid."'
							AND acandemic='".$this->input->post('acandemicid')."'
							AND ranglev='".$this->input->post('ranglevid')."'
							AND feetypeid='".$oppaymentmethod."'
							AND term_sem_year_id='".$studyperiod."'
						  ";
					}
				$this->green->runSQL($updatsql);
				$this->green->runSQL($updatsql1);
				
				}//end pay with school fee is relation with table enroment=======================================================
				// ==============================================================================================================
				// ==============================================================================================================
				
				
			if(($this->input->post('amt_paid'))>0){
				$print_receipt=1;
			}else{
				$print_receipt=0;
				}
			
			
			$gettypeno['nextTran']=$nextTran;
			$gettypeno['print_receipt']=$print_receipt;
			
			header("Content-type:text/x-json");
			echo json_encode($gettypeno);
			exit();
	}
	
	public function FMVoid($typeno="",$dl_text_note="")
	{	
	//--------- stor history Void Invoice
	$dl_text_note=$this->input->post('dl_text_note');
	$type=20;	
	$nextTran=$this->green->nextTran($type,"Void Invoice");
		
	$curdate=date('Y-m-d H:i:s');
	$user=$this->session->userdata('user_name');
		$mo = '';
		$arraycon=array('type'=>17,'typeno'=>$typeno);		
		$mo = $this->db->where($arraycon)->get('sch_student_fee')->row();
		// print_r($mo); exit();
		$da = array(
					'deleted_byuser'=>$user,
					'deleted_date'=>$curdate,
					'type'=>$type,					
					'typeno'=>$nextTran,
					'type_inv_rec'=>$mo->type,					
					'typeno_inv_rec'=>$mo->typeno,
					'ranglev'=>$mo->ranglev,
					'studentid'=>$mo->studentid,
					'trandate'=>$mo->trandate,
					'classid'=>$mo->classid,
					'schooleleve'=>$mo->schooleleve,
					'termid'=>$mo->termid,
					'amt_total'=>$mo->amt_total,
					'amt_paid'=>$mo->amt_paid,
					'amt_balance'=>$mo->amt_balance,
					'duedate'=>$mo->duedate,
					'programid'=>$mo->programid,
					'acandemic'=>$mo->acandemic,
					'schoolid'=>$mo->schoolid,
					'paymentmethod'=>$mo->paymentmethod,
					'paymenttype'=>$mo->paymenttype,
					'areaid'=>$mo->areaid,
					'add_note'=>$dl_text_note,
					'busfeetypid'=>$mo->busfeetypid
					);
			  $this->db->insert('sch_student_fee_deleted', $da);
			   
			   $wdelete = array('type' => 17,'typeno' => $typeno);
        	   $this->db->delete('sch_student_fee',$wdelete);
		$datas="SELECT
					sch_student_fee_type.type,
					sch_student_fee_type.typeno,
					sch_student_fee_type.studentid,
					sch_student_fee_type.trandate,
					sch_student_fee_type.description,
					sch_student_fee_type.`not`,
					sch_student_fee_type.amt_line,
					sch_student_fee_type.prices,
					sch_student_fee_type.amt_dis,
					sch_student_fee_type.qty
				FROM
					sch_student_fee_type
				where 1=1
				AND sch_student_fee_type.typeno='".$typeno."'
				AND sch_student_fee_type.type='17'				
				";
		// echo $datas; exit();

	$result = $this->db->query($datas)->result();
	// print_r($result); exit();
	if(isset($result) && count($result)>0){	
		foreach($result as $rowdel){
			$SQL_DM = "INSERT INTO sch_student_fee_inv_detail_deleted";			
				$SQL = " SET studentid='".$rowdel->studentid."',
						 trandate='".$rowdel->trandate."',
						 type='".$type."',
						 typeno='".$nextTran."',
						 type_inv_rec='".$rowdel->type."',					
						 typeno_inv_rec='".$rowdel->typeno."',					 
						 description='".$rowdel->description."',
						 `not`='".$rowdel->not."',
						 qty='".$rowdel->qty."',
						 prices='".$rowdel->prices."',
						 amt_dis='".$rowdel->amt_dis."',
						 amt_line='".$rowdel->amt_line."'
						 ";
			$counts = $this->green->runSQL($SQL_DM . $SQL);
		}
	};	
	
	//---- end stor history deleted			
		// $this->db->where('typeno', $typeno);
      	$this->db->delete('sch_student_fee_type',$wdelete);		
		$updatsql="UPDATE sch_student_enrollment SET 
						is_closed=2
						WHERE 1=1
						AND type='17'
						AND transno='".$typeno."'
					  ";
			// echo $updatsql;
			$this->green->runSQL($updatsql);
			
	}
	
	public function FrestorDatas($type="",$typeno="",$attr_type="",$attr_typeno=""){
		$curdate=date('Y-m-d H:i:s');
		$user=$this->session->userdata('user_name');
		//---------------------------------------------		
		$mo = '';
		$arraycon=array('type'=>$type,'typeno'=>$typeno);
		
		$mo = $this->db->where($arraycon)->get('sch_student_fee_deleted')->row();
		//print_r($mo); exit();
		$da = array(					
					'type'=>$mo->type_inv_rec,					
					'typeno'=>$mo->typeno_inv_rec,					
					'ranglev'=>$mo->ranglev,					
					'studentid'=>$mo->studentid,
					'trandate'=>$mo->trandate,
					'classid'=>$mo->classid,
					'schooleleve'=>$mo->schooleleve,
					'termid'=>$mo->termid,
					'amt_total'=>$mo->amt_total,
					'amt_paid'=>$mo->amt_paid,
					'amt_balance'=>$mo->amt_balance,
					'duedate'=>$mo->duedate,
					'programid'=>$mo->programid,
					'acandemic'=>$mo->acandemic,
					'schoolid'=>$mo->schoolid,
					'paymentmethod'=>$mo->paymentmethod,
					'paymenttype'=>$mo->paymenttype,
					'areaid'=>$mo->areaid,
					'busfeetypid'=>$mo->busfeetypid
					);		
		//$this->db->where($arraycon);		
       //  $this->db->where($arraycon)->delete('sch_student_fee_deleted');	
	 
		$arraycon_d=array('type'=>$type,'typeno'=>$typeno);	   
		$this->db->where($arraycon_d);
		$sql="SELECT
				sch_student_fee_inv_detail_deleted.type,
				sch_student_fee_inv_detail_deleted.typeno,
				sch_student_fee_inv_detail_deleted.studentid,
				sch_student_fee_inv_detail_deleted.trandate,
				sch_student_fee_inv_detail_deleted.description,
				sch_student_fee_inv_detail_deleted.`not`,
				sch_student_fee_inv_detail_deleted.amt_line,
				sch_student_fee_inv_detail_deleted.prices,
				sch_student_fee_inv_detail_deleted.amt_dis,
				sch_student_fee_inv_detail_deleted.qty,
				sch_student_fee_inv_detail_deleted.type_inv_rec,
				sch_student_fee_inv_detail_deleted.typeno_inv_rec
				FROM
				sch_student_fee_inv_detail_deleted				
				";
		$mo_detail = $this->db->query($sql)->result();
	//	print_r($mo_detail);exit();
		$da_detail=array();
		if(isset($mo_detail) && count($mo_detail)>0){			
			foreach($mo_detail as $mo_detail){
			$da_detail[] =array(					
					'type'=>$mo_detail->type_inv_rec,					
					'typeno'=>$mo_detail->typeno_inv_rec,
					'studentid'=>$mo_detail->studentid,									
					'trandate'=>$mo_detail->trandate,
					'description'=>$mo_detail->description,					
					'not'=>$mo_detail->not,
					'amt_line'=>$mo_detail->amt_line,
					'prices'=>$mo_detail->prices,
					'amt_dis'=>$mo_detail->amt_dis,
					'qty'=>$mo_detail->qty
					);	
			}
		};	// if(isset($result) && count($result)>0){	
		//print_r($da_detail); exit();
        //$this->db->where($arraycon)->delete('sch_student_fee_inv_detail_deleted');
		
		// 20	Void Invoice
		// 21	Deleted Invoice
		// 22	Deleted Receipt
		// 42	Deleted Cash Return
		
		if($type==20){			
		}elseif($type==21){// inv
			$this->db->insert('sch_student_fee', $da);
			if(isset($da_detail)){
				foreach($da_detail as $v){
					$this->db->insert('sch_student_fee_type',$v);
				}
			}			
		}elseif($type==22){// receipt
			$this->db->insert('sch_student_fee_rec_order', $da);
			if(isset($da_detail)){				
				foreach($da_detail as $v){
					$this->db->insert('sch_student_fee_rec_detail',$v);
				}
			}
				
		}elseif($type==42){// Cash Return
			$this->db->insert('sch_student_fee', $da);
			if(isset($da_detail)){
				foreach($da_detail as $v){
					$this->db->insert('sch_student_fee_type',$v);
				}
			}
			
			$updatsql="UPDATE sch_student_enrollment SET 
					type='".$attr_type."',
					transno='".$attr_typeno."',
					is_paid=1
					WHERE 1=1
					AND type='17'
					AND transno='".$attr_typeno."'
					";
			// echo $updatsql;
			$this->green->runSQL($updatsql);
		}			   

		$wdelete = array('type' => $type,'typeno' => $typeno);	
        $this->db->delete('sch_student_fee_deleted',$wdelete);

		$wdelete = array('type' => $type,'typeno' => $typeno);	
        $this->db->delete('sch_student_fee_inv_detail_deleted',$wdelete);

		//----------------------------------------------
		$arr['tr_loop']="1010"; 
		header("Content-type:text/x-json");
		echo json_encode($arr); 
	   exit();
	}
	
	public function FMDelinv($attr_type="",$typeno="",$text_note="")
	{
		// echo "this:".$attr_type.'-----'.$typeno.'->text_note:'.$text_note; exit();
			
		if($attr_type==17){
			$type="";
			$type=21;
			$nextTran=$this->green->nextTran(21,"Deleted Invoice");
		}else if($attr_type==32){
			$type="";
			$type=42;
			$nextTran=$this->green->nextTran(42,"Deleted Cash Return");
		}
		
		$curdate=date('Y-m-d H:i:s');
		$user=$this->session->userdata('user_name');
		
		$mo = '';
		$arraycon=array('type'=>$attr_type,'typeno'=>$typeno);
		$mo = $this->db->where($arraycon)->get('sch_student_fee')->row();
		// print_r($mo); exit();
		
		$da = array(
					'deleted_byuser'=>$user,
					'deleted_date'=>$curdate,
					'type'=>$type,					
					'typeno'=>$nextTran,
					'type_inv_rec'=>$mo->type,									
					'typeno_inv_rec'=>$mo->typeno,
					'ranglev'=>$mo->ranglev,					
					'studentid'=>$mo->studentid,
					'trandate'=>$mo->trandate,
					'classid'=>$mo->classid,
					'schooleleve'=>$mo->schooleleve,
					'termid'=>$mo->termid,
					'amt_total'=>$mo->amt_total,
					'amt_paid'=>$mo->amt_paid,
					'amt_balance'=>$mo->amt_balance,
					'duedate'=>$mo->duedate,
					'programid'=>$mo->programid,
					'acandemic'=>$mo->acandemic,
					'schoolid'=>$mo->schoolid,
					'paymentmethod'=>$mo->paymentmethod,
					'paymenttype'=>$mo->paymenttype,
					'areaid'=>$mo->areaid,
					'add_note'=>$text_note,
					'busfeetypid'=>$mo->busfeetypid
					);
			   $this->db->insert('sch_student_fee_deleted', $da);
			   // $this->db->where('typeno', $typeno);
				
				$this->db->where($arraycon);
        	   $this->db->delete('sch_student_fee');
			   //------------------------------------
			   $this->db->where($arraycon);
			   $this->db->delete('sch_student_fee_rec_order');
			   
			   $this->db->where($arraycon);
			   $this->db->delete('sch_student_fee_rec_detail');
			   //------------------------------------
			   
		$datas="SELECT
					sch_student_fee_type.type,
					sch_student_fee_type.typeno,
					sch_student_fee_type.studentid,
					sch_student_fee_type.trandate,
					sch_student_fee_type.description,
					sch_student_fee_type.`not`,
					sch_student_fee_type.amt_line,
					sch_student_fee_type.prices,
					sch_student_fee_type.amt_dis,
					sch_student_fee_type.qty					
				FROM
					sch_student_fee_type
				where 1=1
				AND sch_student_fee_type.type='".$attr_type."'
				AND sch_student_fee_type.typeno='".$typeno."'
				";
		// echo $datas; exit();
			
		$result = $this->db->query($datas)->result();
		if(isset($result) && count($result)>0){	
			foreach($result as $rowdel){
			$SQL_DM = "INSERT INTO sch_student_fee_inv_detail_deleted";			
			$SQL = " SET studentid='".$rowdel->studentid."',
						 trandate='".$rowdel->trandate."',
						 type='".$type."',
						 typeno='".$nextTran."',
						 type_inv_rec='".$rowdel->type."',					
						 typeno_inv_rec='".$rowdel->typeno."',											 
						 description='".$rowdel->description."',
						 `not`='".$rowdel->not."',
						 qty='".$rowdel->qty."',
						 prices='".$rowdel->prices."',
						 amt_dis='".$rowdel->amt_dis."',
						 amt_line='".$rowdel->amt_line."'
						 ";
			$counts = $this->green->runSQL($SQL_DM . $SQL);
			}
		};	
		//---- end stor history deleted		
		
		//$this->db->where($arraydel);
		$this->db->where($arraycon);
      	$this->db->delete('sch_student_fee_type');
		if($attr_type==17){
		$updatsql="UPDATE sch_student_enrollment SET
						is_paid=0,
						is_closed=0
					WHERE 1=1
					AND type='17'
					AND transno='".$typeno."'
					";
			// echo $updatsql;
			$this->green->runSQL($updatsql);
		}
			
	}
	
	public function FMDelRec($typeno="",$dl_text_note="")
	{	
	//--------- stor history deleted receipt
	// echo $typeno."-----:dl_text_note->".$dl_text_note; exit();	
	$type=22;	
	$nextTran=$this->green->nextTran($type,"Deleted Receipt");
	
	$curdate=date('Y-m-d H:i:s');
	$user=$this->session->userdata('user_name');
		$mo = '';
        $mo = $this->db->where('typeno', $typeno)->get('sch_student_fee_rec_order')->row();
		$da = array(
					'deleted_byuser'=>$user,
					'deleted_date'=>$curdate,
					'type'=>$type,					
					'typeno'=>$nextTran,
					'type_inv_rec'=>$mo->type,					
					'typeno_inv_rec'=>$mo->typeno,
					'ranglev'=>$mo->ranglev,
					'studentid'=>$mo->studentid,
					'trandate'=>$mo->trandate,
					'classid'=>$mo->classid,
					'schooleleve'=>$mo->schooleleve,
					'termid'=>$mo->termid,
					'amt_paid'=>$mo->amt_paid,					
					'programid'=>$mo->programid,
					'acandemic'=>$mo->acandemic,
					'schoolid'=>$mo->schoolid,
					'paymentmethod'=>$mo->paymentmethod,
					'paymenttype'=>$mo->paymenttype,
					'areaid'=>$mo->areaid,
					'add_note'=>$dl_text_note,
					'busfeetypid'=>$mo->busfeetypid
					);
					// print_r($da);
			   $this->db->insert('sch_student_fee_deleted', $da);
			   $wdelete = array('type' => 18,'typeno' => $typeno);
			   
			   // $this->db->where('typeno', $typeno);
        	   $this->db->delete('sch_student_fee_rec_order',$wdelete);
			  			   
		$datas="SELECT
					sch_student_fee_rec_detail.type,
					sch_student_fee_rec_detail.typeno,
					sch_student_fee_rec_detail.studentid,
					sch_student_fee_rec_detail.trandate,
					sch_student_fee_rec_detail.description,
					sch_student_fee_rec_detail.`not`,
					sch_student_fee_rec_detail.amt_line,
					sch_student_fee_rec_detail.prices,
					sch_student_fee_rec_detail.amt_dis,
					sch_student_fee_rec_detail.qty
				FROM
					sch_student_fee_rec_detail
				where 1=1
				AND sch_student_fee_rec_detail.typeno='".$typeno."'
				";
		// echo $datas; exit();
			
		$result = $this->db->query($datas)->result();
		if(isset($result) && count($result)>0){	
			foreach($result as $rowdel){
			$SQL_DM = "INSERT INTO sch_student_fee_inv_detail_deleted";			
			$SQL = " SET studentid='".$rowdel->studentid."',
						 trandate='".$rowdel->trandate."',
						 type='".$type."',
						 typeno='".$nextTran."',
						 type_inv_rec='".$rowdel->type."',					
						 typeno_inv_rec='".$rowdel->typeno."',						 
						 description='".$rowdel->description."',
						 `not`='".$rowdel->not."',
						 qty='".$rowdel->qty."',
						 prices='".$rowdel->prices."',
						 amt_dis='".$rowdel->amt_dis."',
						 amt_line='".$rowdel->amt_line."'
						 ";
			$counts = $this->green->runSQL($SQL_DM . $SQL);
			}
		};	
		//$this->db->where('typeno', $typeno);
		
      	$this->db->delete('sch_student_fee_rec_detail',$wdelete);
		
		//---- end stor history deleted		
		
	}
}// main 