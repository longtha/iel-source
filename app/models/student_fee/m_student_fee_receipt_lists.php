<?php
class M_student_fee_receipt_lists extends CI_Model{

	function __construct(){
		parent::__construct();

	}
public function FgetDelhis(){	
		$selesql="SELECT
					sch_student_fee_deleted.id,
					sch_student_fee_deleted.type,
					sch_student_fee_deleted.typeno,
					sch_student_fee_deleted.studentid,
					sch_student_fee_deleted.trandate,
					sch_student_fee_deleted.classid,
					sch_student_fee_deleted.schooleleve,
					sch_student_fee_deleted.termid,
					sch_student_fee_deleted.amt_total,
					sch_student_fee_deleted.amt_paid,
					sch_student_fee_deleted.amt_balance,
					sch_student_fee_deleted.duedate,
					sch_student_fee_deleted.programid,
					sch_student_fee_deleted.acandemic,
					sch_student_fee_deleted.ranglev,
					sch_student_fee_deleted.schoolid,
					sch_student_fee_deleted.paymentmethod,
					sch_student_fee_deleted.paymenttype,
					sch_student_fee_deleted.areaid,
					sch_student_fee_deleted.busfeetypid,
					sch_student_fee_deleted.is_status,
					sch_student_fee_deleted.deleted_date,
					sch_student_fee_deleted.deleted_byuser,
					sch_student_fee_deleted.type_inv_rec,
					sch_student_fee_deleted.typeno_inv_rec
					FROM
					sch_student_fee_deleted
					";
				// echo $selesql;
		$invOrders=$this->db->query($selesql)->result();
		
		// print_r($invOrders);
		
		$trinv="";
		$gtotal=0;
		if(isset($invOrders) && count($invOrders)>0){
			$i=1;
			$theads="";
			foreach($invOrders as $row){
				$dtrandate=($this->green->convertSQLDate($row->trandate));
				$typeno=($row->typeno);
				$trinv.='<tr class="active cl_inv">
							<td><img class="img-circle img-responsive-" src="http://192.168.168.35:8080/greensim_v2_iel/assets/upload/students/NoImage.png" alt="No Image" style="width:70px;height:70px;"></td>
							
							<td class="">
								<table>
									<tr>
										<td><span style="color:red;display:noned">0-'.($i).'</span></td>
									</tr>
									<tr>
										<td><span style="color:red;display:noned">1-</span></td>
									</tr>
									<tr>
										<td><span style="color:red;display:noned">2-'.($i).'</span></td>
									</tr>
														
								</table>
							</td>
							<td colspan="2"></td>
							<td align="right">xx</td>
							<td colspan="1" align="right"><input id="btn_restore" class="btn btn-success btn_restore" attr_method="1" value="Restore" name="btn_restore" type="button"></td>
							
						 </tr>';
						 	$datas="SELECT
										sch_student_fee_inv_detail_deleted.id,
										sch_student_fee_inv_detail_deleted.type,
										sch_student_fee_inv_detail_deleted.typeno,
										sch_student_fee_inv_detail_deleted.studentid,
										sch_student_fee_inv_detail_deleted.trandate,
										sch_student_fee_inv_detail_deleted.description,
										sch_student_fee_inv_detail_deleted.`not`,
										sch_student_fee_inv_detail_deleted.amt_line,
										sch_student_fee_inv_detail_deleted.prices,
										sch_student_fee_inv_detail_deleted.amt_dis,
										sch_student_fee_inv_detail_deleted.qty,
										sch_student_fee_inv_detail_deleted.type_inv_rec,
										sch_student_fee_inv_detail_deleted.typeno_inv_rec
										FROM
										sch_student_fee_inv_detail_deleted
										where 1=1
										AND typeno='".$typeno."'
										";
						//  echo $datas; exit();
						
						$result = $this->db->query($datas)->result();
						// print_r($classes);
						$trinvd="";	
						$amttotal=0;
						$ii=1;
						if(isset($result) && count($result)>0){							
								foreach($result as $rowde){
								$trinv.='<tr class="info">
									<td align="right">'.$rowde->qty.'</td>
									<td align="right">'.number_format($rowde->prices,2).'&nbsp;$</td>
									<td align="right">'.$rowde->amt_dis.' %</td>
									<td align="right">'.number_format($rowde->amt_line,2).'&nbsp;$</td>
								 </tr>';
							}
								 
							}
							
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td>Total</td>
									<td align="right" class="">&nbsp;$</td>
								 </tr>';
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="">Desposit</td>
									<td align="right" class="">&nbsp;$</td>
								 </tr>';
							$trinv.='<tr class="">
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="">Sub&nbsp;Total</td>
									<td align="right" class="">&nbsp;$</td>
								 </tr>';	
								$i++; 
				}// if(isset($result) && count($result)>0){
			}// foreach($invOrders as $row){
				$trinv.='<tr>
							<td></td>
							<td></td>
							
							<td></td>
							<td align="right" class=""><strong>Grand&nbsp;Total</strong></td>
							<td align="right" class=""><strong>&nbsp;$</strong></td>
						 </tr>';
			$arr['trinv']='<table class="tbl_history" style="width:100% !important;">
								<tr>
									<td class="td_history">Description</td>
									<td class="td_history" align="right">Quantity</td>
									<td class="td_history" align="right">Unit Price</td>
									<td class="td_history" align="right">Dis(%)</td>
									<td class="td_history" align="right">Amount</td>
								</tr>
								'.$trinv.'
							</table>';
	 return json_encode($arr);	
}// end function 

	function grid(){
		$this->green->setActiveRole($this->input->post('roleid'));
		
  		$sortby=$this->input->post("sortby");
		$sorttype=$this->input->post("sorttype");

		$offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;
        $limit_record = $this->input->post('limit_record') - 0;        
		$typeno = trim($this->input->post('typeno', TRUE));       
		$typeno_inv = trim($this->input->post('typeno_inv', TRUE));
		$student_num = trim($this->input->post('student_num', TRUE));
		$student_name = trim($this->input->post('student_name', TRUE));
		$gender = trim($this->input->post('gender', TRUE));
		$schoolid = trim($this->input->post('schoolid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelid = trim($this->input->post('schlevelid', TRUE));		
		$yearid = trim($this->input->post('yearid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$term_sem_year_id = trim($this->input->post('term_sem_year_id', TRUE));
		$feetypeid = trim($this->input->post('feetypeid', TRUE));
		$from_date = trim($this->input->post('from_date', TRUE));
		$to_date = trim($this->input->post('to_date', TRUE));	
		$report_type = $this->input->post('report_type', TRUE);
		$sortstr="";
		$where = '';

		if($typeno != ''){
			$where .= "AND typeno LIKE '%{$typeno}%' ";
			// $this->db->like('student_num', $student_num, 'both'); 
		}
		if($typeno_inv != ''){
			$where .= "AND typeno_inv LIKE '%{$typeno_inv}%' ";
			// $this->db->like('student_num', $student_num, 'both'); 
		}
		
		if($student_num != ''){
			$where .= "AND student_num LIKE '%{$student_num}%' ";
			// $this->db->like('student_num', $student_num, 'both'); 
		}
				
		if($student_name != ''){
			$where .= "AND ( CONCAT(
									first_name,
									' ',
									last_name
								) LIKE '%{$student_name}%' ";
			$where .= "or CONCAT(
									first_name_kh,
									' ',
									last_name_kh
								) LIKE '%{$student_name}%' ) ";			

			// $this->db->like("CONCAT(first_name, ' ', last_name)", $full_name, "both");
		}		
		if($gender != ''){
			$where .= "AND gender = '{$gender}' ";
			// $this->db->where("gender", $gender);
		}
		if($schoolid != ''){
			$where .= "AND schoolid = '{$schoolid}' ";
			// $this->db->where("schoolid", $schoolid);
		}
		if($programid != ''){
			$where .= "AND programid = '{$programid}' ";
			// $this->db->where("programid", $programid);
		}
		if($schlevelid != ''){
			$where .= "AND schooleleve = '{$schlevelid}' ";
			// $this->db->where("schlevelid", $schlevelid);
		}
		if($yearid != ''){
			$where .= "AND acandemic = '{$yearid}' ";
			// $this->db->where("year", $yearid);
		}
		if($rangelevelid != ''){
			$where .= "AND ranglev = '{$rangelevelid}' ";
			// $this->db->where("rangelevelid", $rangelevelid);
		}
		if($classid != ''){
			$where .= "AND classid = '{$classid}' ";
			// $this->db->where("classid", $classid);
		}
		if($feetypeid != ''){
			$where .= "AND paymentmethod = '{$feetypeid}' ";
			// $this->db->where("feetypeid", $feetypeid);
		}
		if($term_sem_year_id != ''){
			$where .= "AND paymenttype = '{$term_sem_year_id}' ";
			// $this->db->where("term_sem_year_id", $term_sem_year_id);
		}
				
		/*if($from_date != ''){
			$where .= "AND DATE_FORMAT(trandate,'%d/%m/%Y') >= '{$from_date}' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') >=", $from_date);
		}
		if($to_date != ''){
			$where .= "AND DATE_FORMAT(trandate,'%d/%m/%Y') <= '{$to_date}' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') <=", $to_date);
		}*/
		//-----------
		if($from_date != ''){
			$where .= "AND date(trandate) >= '".$this->green->formatSQLDate($from_date)."' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') >=", $from_date);
		}
		if($to_date != ''){
			$where .= "AND date(trandate) <= '".$this->green->formatSQLDate($to_date)."' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') <=", $to_date);
		}
		//-----------
		$sortstr.= ((isset($sortby) && $sortby!="")?" ORDER BY `".$sortby."` ".$sorttype:" ORDER BY typeno DESC");
		// count ==========
		$qr_c = $this->db->query("SELECT COUNT(student_num) AS c FROM v_student_fee_receipt_list
									WHERE 1=1 {$where} {$sortstr} ")->row()->c - 0;
		$totalRecord = $qr_c - 0;		
		$totalPage = ceil($totalRecord/$limit);

		// result =======
		$qr = $this->db->query("SELECT
										*,
										DATE_FORMAT(trandate, '%d-%m-%Y') AS trandate
									FROM
										v_student_fee_receipt_list 
									WHERE 1=1 {$where} {$sortstr}
									  
									LIMIT $offset, $limit ");

		$i = 1;
		$tr = '';
		$t_receipt_amount=0;
		$gamt=0;
		$t_return_amount=0;
		if($qr->num_rows() > 0){			
			foreach($qr->result() as $row){
				$returnQUERY = $this->db->query("SELECT type,typeno FROM sch_student_fee WHERE type_inv='".$row->type_inv."' AND typeno_inv='".$row->typeno_inv."'")->row();
				$returnType = (count($returnQUERY) > 0 ? $returnQUERY->type :0)-0;
				$returnTypeno = (count($returnQUERY) > 0 ? $returnQUERY->typeno :0)-0;
				$totalReturnAmountQuery = <<<QUERY
																		SELECT IFNULL((
																			SELECT
																				IFNULL(SUM(return_amount), 0)
																			FROM
																				sch_student_fee_rec_detail
																			WHERE
																				type = 32
																			AND type = $returnType
																			AND typeno = $returnTypeno
																			GROUP BY
																				typeno_inv,
																				typeno_inv
																			), 0) AS 'return_amount';
QUERY;
				$totalReturnAmount = $this->db->query($totalReturnAmountQuery)->row()->return_amount;
				// $amt_balance_line= ($row->amt_paid-$totalReturnAmount)-0;
				if($totalReturnAmount>0){
					$amt_balance_line=($row->amt_paid + $totalReturnAmount)-0;	
				}else{
					$amt_balance_line=($row->amt_paid - ($totalReturnAmount*(-1)))-0;
				}
				if($report_type == 0){

					$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
					$no_imgs = base_url()."assets/upload/students/NoImage.png";	
					
					$photodel='';
					$delet="<img  class='img-circle' style='width:15px; height:15px;' src='".site_url('../assets/images/icons/deletedis.png')."'/>";
					if($row->type=='18'){
					$photodel="<img class='img-circle' src='".site_url('../assets/images/icons/delete.png')."'/>";	
						$delet='<span class="cl_del" data-toggle="modal" data-target="#dl_viod" attr_del='.$row->typeno.'>'.$photodel.'</span>';
					
					
					$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
					if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
					$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
					}
					
					
					$tr .= '<tr>
								<td>'.($i++).'</td>
								<td style="text-align: center;">
									'.$img.'
								</td>
								<td>
								'.$row->student_num.'<br>
								'.$row->last_name_kh.' '.$row->first_name_kh.' <br>
								'.$row->last_name.' '.$row->first_name.' <br>
								</td>							
								<td>'.$row->trandate.'</td>
								<td>'.$row->sch_level.'</td>
								<td>'.$row->sch_year.'</td>
								<td>'.$row->rangelevelname.'</td>
								<td>'.$row->class_name.'</td>
								<td>'.$row->period.'</td>
								<td>
 									<a href="'.site_url().'/student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno_inv.'" target="_blank">#'.$row->typeno_inv.'</a>
 								</td>
								<td>
 									<a href="'.site_url().'/student_fee/c_student_print_receipt?param_reno='.$row->typeno.'" target="_blank">#'.$row->typeno.'</a>
 								</td>
 								<td style="text-align: right;">'.number_format($row->amt_paid, 2).'</td>
								<td style="text-align: center;">
									<a href="'.($returnTypeno>0? site_url()."/student_fee/c_student_receipt_cash_return?param_reno=".$returnTypeno."" :"#").'" '.($returnTypeno>0? 'target="_blank"':"").'>#'.($returnTypeno>0? $returnTypeno :"N/A").'</a>
								</td>
								<td style="text-align: right;">'.number_format($totalReturnAmount, 2).'</td>
								<td style="text-align: right;">'.number_format($amt_balance_line, 2).'</td>
								<td style="text-align: right;">'.$delet.'</td>
							</tr>';	
					}
				}else{
					
					$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
					$no_imgs = base_url()."assets/upload/students/NoImage.png";	
					
					$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
					if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
					$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
					}
					
					$tr .= '<tr>
								<td>'.($i++).'</td>
								<td style="text-align: center;">
									'.$img.'
								</td>
								<td>
									<a href="'.site_url().'/student_fee/c_student_print_receipt?param_reno='.$row->typeno.'" target="_blank">#'.$row->typeno.'</a><br>
									'.$row->trandate.'<br>
									'.$row->student_num.'<br>
									'.$row->first_name_kh.' '.$row->last_name_kh.'<br>
								</td>							
								<td>'.$row->sch_level.'</td>
								<td>'.$row->sch_year.'</td>
								<td>'.$row->rangelevelname.'</td>
								<td>'.$row->class_name.'</td>
								<td>'.$row->period.'</td>								
								<td style="text-align: right;">'.number_format($row->amt_paid, 2).'$</td>
							</tr>';

					// detail =========
					$qr_detail = $this->db->query("SELECT DISTINCT
															*,
															DATE_FORMAT(trandate, '%d-%m-%Y') AS trandate
														FROM
															sch_student_fee_rec_detail AS st
														WHERE
															1=1
														AND st.typeno = '{$row->typeno}'
													");

					$sub_to = 0;
					if($qr_detail->num_rows() > 0){
						$tr .= '<tr style="text-align: right;">'.
									'<td colspan="1" style="border-top: 0;">&nbsp;</td>'.
									'<td colspan="2" style="border-top: 0;text-align: left;background: #DDD;">Description</td>'.
									'<td colspan="1" style="border-top: 0;background: #DDD;">Quantity</td>'.
									'<td colspan="2" style="border-top: 0;background: #DDD;">Price</td>'.
									'<td colspan="1" style="border-top: 0;background: #DDD;">Dis.(%)</td>'.
									'<td colspan="2" style="border-top: 0;background: #DDD;">Amount</td>'.
								'</tr>';
						foreach ($qr_detail->result() as $row_detail) {
							$tr .= '<tr style="text-align: right;">'.
										'<td colspan="1" style="border-top: 0;">&nbsp;</td>'.
										'<td colspan="2" style="border-top: 0;text-align: left;">- '.$row_detail->description.'</td>'.
										'<td colspan="1" style="border-top: 0;">'.$row_detail->qty.'</td>'.
										'<td colspan="2" style="border-top: 0;">'.number_format($row_detail->prices, 2).'</td>'.
										'<td colspan="1" style="border-top: 0;">'.number_format($row_detail->amt_dis, 2).'%</td>'.
										'<td colspan="2" style="border-top: 0;">'.number_format($row_detail->amt_line, 2).'</td>'.
									'</tr>';
							$sub_to += $row_detail->amt_line - 0;
						}// foreach detail =====
						
					}

					$tr .= '<tr style="font-weight: bold;">'.
								'<td colspan="8" style="text-align: right;border-top: 0;">Sub Total</td>'.
								'<td colspan="1" style="border-top: 0;text-align: right;">'.number_format($sub_to, 2).'</td>'.
							'</tr>';
				
				}// report type =====
			$t_receipt_amount+=$row->amt_paid-0;
			$t_return_amount+=$totalReturnAmount;
			$gamt+=$amt_balance_line;
			}// foreach master =====
		$tr .= '<tr>
					<td colspan="11" style="font-weight: bold;text-align: right;" class="gamt">Total</td>
					<td class="gamt">'.number_format($t_receipt_amount,2).' $</td>
					<td  class="gamt"></td>
					<td class="gamt">'.number_format($t_return_amount,2).' $</td>
					<td class="gamt" style="width:80px">'.number_format($gamt,2).' $</td>
					<td  class="gamt"></td>
				</tr>';
							
		}else{
			$tr .= '<tr><td colspan="14" style="font-weight: bold;text-align: center;">We did not find anything to show here.</td></tr>';
		}

		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage); 
		// $arr['tr'] = $tr;
        return json_encode($arr);		
	}

	function get_schlevel($programid){
        $qr_schlevel = $this->db->query("SELECT DISTINCT
                                                l.schlevelid,
                                                l.sch_level
                                            FROM
                                                sch_school_level AS l
                                            WHERE
                                                l.programid = '{$programid}'
                                            ORDER BY
                                                l.sch_level ASC ")->result();        
        
        $arr = array('schlevel' => $qr_schlevel);
        return json_encode($arr);
    }

    function get_year($programid, $schlevelid = ''){

    	$where = '';
    	if($schlevelid != ''){
    		$where .= "AND y.schlevelid = '{$schlevelid}' ";
    	}

        $qr_year = $this->db->query("SELECT
											y.yearid,
											y.sch_year
										FROM
											sch_school_year AS y
										WHERE
											y.programid = '{$programid}' {$where}
										ORDER BY
											y.sch_year ASC ")->result();

        $qr_class = $this->db->query("SELECT
													c.classid,
													c.class_name
												FROM
													sch_school_level AS l
												INNER JOIN sch_class AS c ON l.schlevelid = c.schlevelid
												WHERE
													l.schlevelid = '{$schlevelid}'
												ORDER BY
													c.class_name ASC ")->result();
        
        $arr = array('year' => $qr_year, 'class' => $qr_class);
        return json_encode($arr);
    }

    // get rangelevel =========
    function get_rangelevel($programid = '', $schlevelid){

    	$where = '';
    	if($programid != ''){
    		$where .= "AND rl.programid = '{$programid}' ";
    	}

        $qr_rangelevel = $this->db->query("SELECT
													rl.rangelevelid,
													rl.rangelevelname
												FROM
													sch_school_rangelevel AS rl
												WHERE
													rl.schlevelid = '{$schlevelid}' {$where}
												ORDER BY
													rl.rangelevelname ASC ")->result();

        $arr = array('rangelevel' => $qr_rangelevel);
        return json_encode($arr);
    }

    // get class =========
    function get_class($programid = '', $schlevelid, $yearid = ''){

    	$where = '';
    	if($programid != ''){
    		$where .= "AND c.programid = '{$programid}' ";
    	}
    	if($yearid != ''){
    		$where .= "AND c.yearid = '{$yearid}' ";
    	}    	

        $qr_class = $this->db->query("SELECT
												c.classid,
												c.class_name
											FROM
												sch_class AS c
											WHERE
												c.schlevelid = '{$schlevelid}'
											ORDER BY
												c.class_name ASC ")->result();

        $arr = array('class' => $qr_class);
        return json_encode($arr);
    }

    // get_paymentType =========
    public function get_paymentType($feetypeid = '', $programid = '', $schlevelid = '', $yearid = ''){
    	if($feetypeid != ''){
    		$this->db->where("payment_type", $feetypeid);
    	}
    	if($programid != ''){
    		$this->db->where("programid", $programid);
    	}
    	if($schlevelid != ''){
    		$this->db->where("schlevelid", $schlevelid);
    	}
    	if($yearid != ''){
    		$this->db->where("yearid", $yearid);
    	}    	

        $qr_get_paymentType = $this->db->get("v_study_peroid")->result();
        return json_encode($qr_get_paymentType);
    }	
	
	
}