<?php
	class m_student_restor extends CI_Model{

		function __construct(){
			parent::__construct();

		}

	    // get school level ----------------------------------------
    	function get_schlevel($programid = ''){
	        $qr_schlevel = $this->db->query("SELECT DISTINCT
	                                                l.schlevelid,
	                                                l.sch_level
	                                            FROM
	                                                sch_school_level AS l
	                                            WHERE
	                                                l.programid = '{$programid}' - 0
	                                            ORDER BY
	                                                l.sch_level ASC ")->result();        
	        
	        $arr = array('schlevel' => $qr_schlevel);
	        return json_encode($arr);
	    }	
	    

	    // get_schyera -------------------------------------------------------------
	    function get_schyera($schlevelid = ''){
	        $qr_schyear = $this->db->query("SELECT DISTINCT
	                                                y.yearid,
	                                                y.sch_year,
	                                                y.schlevelid
	                                            FROM
	                                                sch_school_year AS y
	                                            WHERE
	                                                y.schlevelid = '{$schlevelid}' - 0
	                                            ORDER BY
	                                                y.sch_year ASC ")->result();        
	        
	        $arr = array('schyear' => $qr_schyear);
	        return json_encode($arr);
	    }

	    //from ranglavel--------------------------------------------------
	     function schranglavel($yearid = ''){
	        $qr_schraglavele = $this->db->query("SELECT DISTINCT
													rl.rangelevelid,
													rl.rangelevelname
												FROM
													sch_school_rangelevel AS rl
												INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
												AND rl.programid = y.programid
												WHERE
													y.yearid = '{$yearid}' - 0
												ORDER BY
													rl.rangelevelname ASC")->result();       
	        
	        $arr = array('schranglavel' => $qr_schraglavele);
	        return json_encode($arr);
	    }
	    //from classname ------------------------------------------------------
	    function get_schclass($rangelevelid = ''){
	        $qr_schclass = $this->db->query("SELECT DISTINCT
												ran.rangelevelid,
												ran.rangelevelname,
												racl.classid,
												cla.class_name
											FROM
												sch_school_rangelevelclass AS racl
											LEFT JOIN sch_class AS cla ON racl.classid = cla.classid
											LEFT JOIN sch_school_rangelevel AS ran ON ran.rangelevelid = racl.rangelevelid
											WHERE
												ran.rangelevelid = '{$rangelevelid}' - 0
											ORDER BY
												cla.class_name ASC")->result();        
	        
	        $arr = array('schclass' => $qr_schclass);
	        return json_encode($arr);
	    }

	    // get_paymentType --------------------------------------------
	    function get_paymentType($feetypeid = ''){
	        $qr_get_paymentType = $this->db->query("SELECT DISTINCT
														v_study_peroid.term_sem_year_id,
														v_study_peroid.period,
														v_study_peroid.payment_type
													FROM
														v_study_peroid
													WHERE
	                                                	v_study_peroid.payment_type = '{$feetypeid}' - 0")->result();
	        	
	        return json_encode($qr_get_paymentType);
	    }

	   // get_systype -----------------------------------------------
	    function get_systype(){
	        return $this->db->query("SELECT DISTINCT
										sch_z_systype.type,
										sch_z_systype.typeid
									FROM
										sch_student_fee_deleted
									INNER JOIN sch_z_systype ON sch_z_systype.typeid = sch_student_fee_deleted.type")->result();
	    }
	    
	    
	}