<?php
    class m_treansport extends CI_Model{
    // allarea -------------------------------------------------	
		function allarea(){
            return $this->db->query("SELECT DISTINCT
												a.areaid,
												a.area
											FROM
												sch_setup_area AS a
											ORDER BY
												a.area")->result();

		}
		// get_bus ----------------------------------------------
		function get_bus($areaid = ''){
			$where="";
			if($areaid != ''){
				$where.=" AND ba.areaid = '{$areaid}'";
			}
	        $qr_schbus = $this->db->query("SELECT
													b.busid,
													b.busno,
													ba.areaid
												FROM
													sch_setup_busarea AS ba
												LEFT JOIN sch_setup_bus AS b ON b.busid = ba.busid
												WHERE
													1=1 {$where}
												ORDER BY
													b.busno ASC ")->result();        
	        
	        $arr = array('bus' => $qr_schbus);
	        return json_encode($arr);
	    }
	    function getschoolyear()
		{
			return $this->db->get('sch_school_year')->result();
		}		
	    // get_driver --------------------------------------------
	    function get_driver($busid = ''){
	    	$where="";
			if($busid != ''){
				$where.=" AND d.busid = '{$busid}' - 0";
			}
	        $qr_schdriver = $this->db->query("SELECT DISTINCT
	        									d.busid,
												d.driverid,
												d.driver_name
											FROM
												sch_setup_driver AS d
											WHERE
	                                            1=1 {$where}
											ORDER BY
												d.driver_name ASC ")->result();        
	        
	        $arr = array('dri' => $qr_schdriver);
	        return json_encode($arr);
	    }
	    public function update_student_transport($receipt_no,$data){
			return $this->db->where('typeno',$receipt_no)->update('sch_student_fee_rec_order',$data);
	    }
    public function get_bus_name($busid){
    	$this->db->select("busno");
		$this->db->where("busid", $busid);
		$query = $this->db->get("sch_setup_bus",1,0);

		if($query->num_rows() > 0) {
		    $variable = $query->row("busno");
		    return $variable;    
		} else {
		    return FALSE;
		}
    }
    public function get_area_name($areaid){
    	$this->db->select("area");
		$this->db->where("areaid", $areaid);
		$query = $this->db->get("sch_setup_area",1,0);

		if($query->num_rows() > 0) {
		    $variable = $query->row("area");
		    return $variable;    
		} else {
		    return FALSE;
		}
    }
    public function get_driver_name($driverid){
    	$this->db->select("driver_name");
		$this->db->where("driverid", $driverid);
		$query = $this->db->get("sch_setup_driver",1,0);

		if($query->num_rows() > 0) {
		    $variable = $query->row("driver_name");
		    return $variable;    
		} else {
		    return FALSE;
		}
    }
    }