<?php
class M_student_fee_invoice_lists extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function grid(){
		$this->green->setActiveRole($this->input->post('roleid'));
  		$sortby=$this->input->post("sortby");
		$sorttype=$this->input->post("sorttype");

		$offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;
        $limit_record = $this->input->post('limit_record') - 0;        
		$typeno = trim($this->input->post('typeno', TRUE));
		$student_num = trim($this->input->post('student_num', TRUE));
		$student_name = trim($this->input->post('student_name', TRUE));
		$gender = trim($this->input->post('gender', TRUE));
		$schoolid = trim($this->input->post('schoolid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelid = trim($this->input->post('schlevelid', TRUE));		
		$yearid = trim($this->input->post('yearid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$term_sem_year_id = trim($this->input->post('term_sem_year_id', TRUE));
		$feetypeid = trim($this->input->post('feetypeid', TRUE));
		$from_date = trim($this->input->post('from_date', TRUE));
		$to_date = trim($this->input->post('to_date', TRUE));	
		$report_type = $this->input->post('report_type', TRUE);
		$sortstr ='';
		$where = '';

		if($typeno != ''){
			$where .= "AND typeno LIKE '%{$typeno}%' ";
			// $this->db->like('student_num', $student_num, 'both'); 
		}
		if($student_num != ''){
			$where .= "AND student_num LIKE '%{$student_num}%' ";
			// $this->db->like('student_num', $student_num, 'both'); 
		}		
		if($student_name != ''){
			$where .= "AND ( CONCAT(
									first_name,
									' ',
									last_name
								) LIKE '%{$student_name}%' ";
			$where .= "or CONCAT(
									first_name_kh,
									' ',
									last_name_kh
								) LIKE '%{$student_name}%' ) ";			

			// $this->db->like("CONCAT(first_name, ' ', last_name)", $full_name, "both");
		}		
		if($gender != ''){
			$where .= "AND gender = '{$gender}' ";
			// $this->db->where("gender", $gender);
		}
		if($schoolid != ''){
			$where .= "AND schoolid = '{$schoolid}' ";
			// $this->db->where("schoolid", $schoolid);
		}
		if($programid != ''){
			$where .= "AND programid = '{$programid}' ";
			// $this->db->where("programid", $programid);
		}
		if($schlevelid != ''){
			$where .= "AND schooleleve = '{$schlevelid}' ";
			// $this->db->where("schlevelid", $schlevelid);
		}
		if($yearid != ''){
			$where .= "AND acandemic = '{$yearid}' ";
			// $this->db->where("year", $yearid);
		}
		if($rangelevelid != ''){
			$where .= "AND ranglev = '{$rangelevelid}' ";
			// $this->db->where("rangelevelid", $rangelevelid);
		}
		if($classid != ''){
			$where .= "AND classid = '{$classid}' ";
			// $this->db->where("classid", $classid);
		}
		if($feetypeid != ''){
			$where .= "AND paymentmethod = '{$feetypeid}' ";
			// $this->db->where("feetypeid", $feetypeid);
		}
		
		if($term_sem_year_id != ''){
			$where .= "AND paymenttype = '{$term_sem_year_id}' ";
			// $this->db->where("term_sem_year_id", $term_sem_year_id);
		}	
			
		if($report_type != ''){
			$where .= "AND is_status = '{$report_type}' ";
			// $this->db->where("term_sem_year_id", $term_sem_year_id);
		}	
			
		if($from_date != ''){
			$where .= "AND date(trandate) >= '".$this->green->formatSQLDate($from_date)."' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') >=", $from_date);
		}
		if($to_date != ''){
			$where .= "AND date(trandate) <= '".$this->green->formatSQLDate($to_date)."' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') <=", $to_date);
		}
		$where_log ='';
		if($this->session->userdata('match_con_posid')=='stu'){
			$where_log.=" AND studentid ='".$this->session->userdata('emp_id')."'";
		}
		$sortstr.= ((isset($sortby) && $sortby!="")?"`".$sortby."` ".$sorttype:" ORDER BY typeno DESC");
		// count ==========
		$qr_c = $this->db->query("SELECT COUNT(student_num) AS c FROM v_student_fee_inv_list
									WHERE 1=1 {$where} {$sortstr}")->row()->c - 0;
		$totalRecord = $qr_c - 0;		
		$totalPage = ceil($totalRecord/$limit);

		// result =======
		$qr = $this->db->query("SELECT
										*,
										DATE_FORMAT(trandate, '%d-%m-%Y') AS trandate
									FROM
										v_student_fee_inv_list 
									WHERE 1=1 {$where} {$where_log} {$sortstr}
									LIMIT $offset,$limit");

		$i = 1;
		$tr = '';
		$gamt_total=0;
		$gamt_paid=0;
		$gamt_balance=0;
			
		if($qr->num_rows() > 0){			
			foreach($qr->result() as $row){
				
				$counrow="SELECT
								COUNT(typeno) as typeno
							FROM
								sch_student_fee_rec_order
							WHERE
								1=1
								AND type=18
								AND type_inv='".$row->type."'
								AND typeno_inv='".$row->typeno."'
							";
		// echo $counrow; exit();
		
		$ispayexist = $this->db->query($counrow)->row()->typeno;
			$tradd="";
			
			$photodel="<img class='img-circle' src='".site_url('../assets/images/icons/delete.png')."'/>";	
		if($ispayexist>0){
			$photodel="<img  class='img-circle' style='width:15px; height:15px;' src='".site_url('../assets/images/icons/deletedis.png')."'/>";
			$tradd='<a>'.$photodel.'</a>';
			
		}else{
			$tradd='<a id="a_delete_inv"  data-toggle="modal" data-target="#dl_viod"  class="a_delete_inv" attr_id='.$row->typeno.' style="text-align:center">'.$photodel.'</a>';
		
		}
		
				//if($report_type == 0){
				
				
				if($this->green->gAction("D")){	
				$dele=$tradd;
				}
				
				$dele=$tradd;
				
				if(1 == 1){
					$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
					$no_imgs = base_url()."assets/upload/students/NoImage.png";	
					
					$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
					if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
					$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
					}
					
					$tr .= '<tr>
								<td>'.($i++).'</td>
								<td style="text-align: center;">
									'.$img.'
								</td>
								<td>
								'.$row->student_num.'<br>
								 '.$row->last_name_kh.' '.$row->first_name_kh.'<br>
								'.$row->last_name.' '.$row->first_name.' <br>
								'.ucfirst($row->gender).'
								 <br>
								
								</td>
								<td>'.$row->trandate.'</td>	
								<td>'.$row->sch_level.'</td>
								<td>'.$row->sch_year.'</td>
								<td>'.$row->rangelevelname.'</td>
								<td>'.$row->class_name.'</td>
								<td>'.$row->period.'</td>
								<td>
									<a href="'.site_url().'/student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno.'" target="_blank">#'.$row->typeno.'</a>
								</td>
								<td align="right">'.number_format($row->amt_total, 2).'</td>
								<td align="right">'.number_format($row->amt_paid, 2).'</td>
								<td align="right">'.number_format($row->amt_balance, 2).'</td>
								<td style="display:none">'.$dele.'</td>
								<td class="" style="display:none">
									<div class="relative">
										<div class="absolute" style="display:none">
											<div>1 Print</div>
											<div>2 Close</div>
											<div>3 Delete</div>
										</div>
									</div>
								</td>
							</tr>';	

				}else{
					$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
					$no_imgs = base_url()."assets/upload/students/NoImage.png";	
					
					$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
					if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
					$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
					}
					
					$tr .= '<tr>
								<td>'.($i++).'</td>
								<td style="text-align: center;">
									'.$img.'
								</td>
								<td>
									<a href="'.site_url().'/student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno.'" target="_blank">#'.$row->typeno.'</a><br>
									'.$row->trandate.'<br>
									'.$row->student_num.'<br>
									'.$row->first_name_kh.' '.$row->last_name_kh.'<br>
								</td>							
								<td>'.$row->sch_level.'</td>
								<td>'.$row->sch_year.'</td>
								<td>'.$row->rangelevelname.'</td>
								<td>'.$row->class_name.'</td>
								<td>'.$row->period.'</td>
								<td>'.number_format($row->amt_total, 2).'$</td>
								<td>'.number_format($row->amt_paid, 2).'$</td>
								<td>'.number_format($row->amt_balance, 2).'$</td>
							</tr>';

					// detail =========
					$qr_detail = $this->db->query("SELECT DISTINCT
															*,
															DATE_FORMAT(trandate, '%d-%m-%Y') AS trandate
														FROM
															sch_student_fee_type AS st
														WHERE
															1=1
														AND st.typeno = '{$row->typeno}' ");

					$sub_to = 0;
					if($qr_detail->num_rows() > 0){
						$tr .= '<tr style="text-align: right;">'.
									'<td colspan="1" style="border-top: 0;">&nbsp;</td>'.
									'<td colspan="2" style="border-top: 0;text-align: left;background: #DDD;">Description</td>'.
									'<td colspan="2" style="border-top: 0;background: #DDD;">Quantity</td>'.
									'<td colspan="2" style="border-top: 0;background: #DDD;">Price</td>'.
									'<td colspan="2" style="border-top: 0;background: #DDD;">Dis.(%)</td>'.
									'<td colspan="2" style="border-top: 0;background: #DDD;">Amount</td>'.
								'</tr>';
						foreach ($qr_detail->result() as $row_detail) {
							$tr .= '<tr style="text-align: right;">'.
										'<td colspan="1" style="border-top: 0;">&nbsp;</td>'.
										'<td colspan="2" style="border-top: 0;text-align: left;">- '.$row_detail->description.'</td>'.
										'<td colspan="2" style="border-top: 0;">'.$row_detail->qty.'</td>'.
										'<td colspan="2" style="border-top: 0;">'.number_format($row_detail->prices, 2).'</td>'.
										'<td colspan="2" style="border-top: 0;">'.number_format($row_detail->amt_dis, 2).'%</td>'.
										'<td colspan="2" style="border-top: 0;">'.number_format($row_detail->amt_line, 2).'</td>'.
									'</tr>';
							$sub_to += $row_detail->amt_line - 0;
						}// foreach detail =====
					}

					$tr .= '<tr style="font-weight: bold;">'.
								'<td colspan="10" style="text-align: right;border-top: 0;">Sub Total</td>'.
								'<td colspan="1" style="border-top: 0;text-align: right;">'.number_format($sub_to, 2).'</td>'.
							'</tr>';

				}// report type =====
			$gamt_total+=$row->amt_total;
			$gamt_paid+=$row->amt_paid;
			$gamt_balance+=$row->amt_balance;			
			}// foreach master =====
			$tr .= '<tr><td colspan="10" style="font-weight: bold;text-align: right;" class="gamt">Total</td>
					<td class="gamt">'.number_format($gamt_total,2).'</td>
					<td class="gamt">'.number_format($gamt_paid,2).'</td>
					<td class="gamt">'.number_format($gamt_balance,2).'</td>
					
					<td  class="gamt"></td></tr>';
						
		}else{
			$tr .= '<tr><td colspan="13" style="font-weight: bold;text-align: center;">We did not find anything to show here.</tr>';
		}

		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage); 
		// $arr['tr'] = $tr;
        return json_encode($arr);		
	}

	function get_schlevel($programid){
        $qr_schlevel = $this->db->query("SELECT DISTINCT
                                                l.schlevelid,
                                                l.sch_level
                                            FROM
                                                sch_school_level AS l
                                            WHERE
                                                l.programid = '{$programid}'
                                            ORDER BY
                                                l.sch_level ASC ")->result();        
        
        $arr = array('schlevel' => $qr_schlevel);
        return json_encode($arr);
    }

    function get_year($programid, $schlevelid = ''){

    	$where = '';
    	if($schlevelid != ''){
    		$where .= "AND y.schlevelid = '{$schlevelid}' ";
    	}

        $qr_year = $this->db->query("SELECT
											y.yearid,
											y.sch_year
										FROM
											sch_school_year AS y
										WHERE
											y.programid = '{$programid}' {$where}
										ORDER BY
											y.sch_year ASC ")->result();

        $qr_class = $this->db->query("SELECT
													c.classid,
													c.class_name
												FROM
													sch_school_level AS l
												INNER JOIN sch_class AS c ON l.schlevelid = c.schlevelid
												WHERE
													l.schlevelid = '{$schlevelid}'
												ORDER BY
													c.class_name ASC ")->result();
        
        $arr = array('year' => $qr_year, 'class' => $qr_class);
        return json_encode($arr);
    }

    // get rangelevel =========
    function get_rangelevel($programid = '', $schlevelid){

    	$where = '';
    	if($programid != ''){
    		$where .= "AND rl.programid = '{$programid}' ";
    	}

        $qr_rangelevel = $this->db->query("SELECT
													rl.rangelevelid,
													rl.rangelevelname
												FROM
													sch_school_rangelevel AS rl
												WHERE
													rl.schlevelid = '{$schlevelid}' {$where}
												ORDER BY
													rl.rangelevelname ASC ")->result();

        $arr = array('rangelevel' => $qr_rangelevel);
        return json_encode($arr);
    }

    // get class =========
    function get_class($programid = '', $schlevelid, $yearid = ''){

    	$where = '';
    	if($programid != ''){
    		$where .= "AND c.programid = '{$programid}' ";
    	}
    	if($yearid != ''){
    		$where .= "AND c.yearid = '{$yearid}' ";
    	}    	

        $qr_class = $this->db->query("SELECT
												c.classid,
												c.class_name
											FROM
												sch_class AS c
											WHERE
												c.schlevelid = '{$schlevelid}'
											ORDER BY
												c.class_name ASC ")->result();

        $arr = array('class' => $qr_class);
        return json_encode($arr);
    }

    // get_paymentType =========
    public function get_paymentType($feetypeid = '', $programid = '', $schlevelid = '', $yearid = ''){
    	if($feetypeid != ''){
    		$this->db->where("payment_type", $feetypeid);
    	}
    	if($programid != ''){
    		$this->db->where("programid", $programid);
    	}
    	if($schlevelid != ''){
    		$this->db->where("schlevelid", $schlevelid);
    	}
    	if($yearid != ''){
    		$this->db->where("yearid", $yearid);
    	}    	

        $qr_get_paymentType = $this->db->group_by('term_sem_year_id')->get("v_study_peroid")->result();
        return json_encode($qr_get_paymentType);
    }	
	
	
}