<?php
class m_assign_payment_method extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function grid(){
		$total_display =$this->input->post('sort_num');
	  	$total_display = $this->input->post('total_display');
		$this->green->setActiveRole($this->input->post('roleid'));
  		$sortby=$this->input->post("sortby");
		$sorttype=$this->input->post("sorttype");

		$offset = $this->input->post('offset') - 0;
        $limit_record = $this->input->post('limit_record') - 0;
		$student_num = trim($this->input->post('student_num', TRUE));
		$full_name = trim($this->input->post('full_name', TRUE));
		$full_kh = trim($this->input->post('full_kh', TRUE));
		$gender = trim($this->input->post('gender', TRUE));
		$schoolid = trim($this->input->post('schoolid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelid = trim($this->input->post('schlevelid', TRUE));
		$term_sem_year_id = trim($this->input->post('term_sem_year_id', TRUE));
		$feetypeid = trim($this->input->post('feetypeid', TRUE));
		$yearid = trim($this->input->post('yearid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$is_paid = trim($this->input->post('is_paid', TRUE));
		$is_closed = trim($this->input->post('is_closed', TRUE));		
		$from_date = trim($this->input->post('from_date', TRUE));
		$to_date = trim($this->input->post('to_date', TRUE));	
		$sortstr= '';
		$where = '';

		if($student_num != ''){
			 $where .= "AND student_num LIKE '%{$student_num}%' ";
			//$this->db->like('student_num', $student_num, 'both'); 
		}
		
		if($full_name != ''){
				$where .= "AND CONCAT(
									st.first_name,
									' ',
									st.last_name
								) LIKE '%{$full_name}%' ";
			//$this->db->like("CONCAT(first_name, ' ', last_name)", $full_name, "both");
		}	
		if($full_kh != ''){
				$where .= "AND CONCAT(
									st.first_name,
									' ',
									st.last_name
								) LIKE '%{$full_kh}%' ";
			//$this->db->like("CONCAT(first_name_kh, ' ', last_name_kh)", $full_kh, "both");
		}		
		if($gender != ''){
			 $where .= "AND gender = '{$gender}' ";
			//$this->db->where("gender", $gender);
		}
		if($schoolid != ''){
			 $where .= "AND schoolid = '{$schoolid}' ";
			//$this->db->where("schoolid", $schoolid);
		}
		if($programid != ''){
			 $where .= "AND programid = '{$programid}' ";
			//$this->db->where("programid", $programid);
		}
		if($schlevelid != ''){
			 $where .= "AND schlevelid = '{$schlevelid}' ";
			//$this->db->where("schlevelid", $schlevelid);
		}
		if($yearid != ''){
			 $where .= "AND year = '{$yearid}' ";
			//$this->db->where("year", $yearid);
		}		
		if($term_sem_year_id != ''){
			 $where .= "AND term_sem_year_id = '{$term_sem_year_id}' ";
			//$this->db->where("term_sem_year_id", $term_sem_year_id);
		}
		if($feetypeid != ''){
			 $where .= "AND feetypeid = '{$feetypeid}' ";
			//$this->db->where("feetypeid", $feetypeid);
		}
		if($rangelevelid != ''){
			 $where .= "AND rangelevelid = '{$rangelevelid}' ";
			//$this->db->where("rangelevelid", $rangelevelid);
		}
		if($classid != ''){
			 $where .= "AND classid = '{$classid}' ";
			//$this->db->where("classid", $classid);
		}
		(isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

		// count ==========
		// $qr_c = $this->db->get("v_student_fee_reminder_period");
		// $totalRecord = $qr_c->num_rows() - 0;		
		// $totalPage = ceil($totalRecord/$limit);
		// $limit_record=8;

		if($limit_record!=''){
	      $this->db->limit($limit_record,0);	       
	    }	

		// result =======
		//$qr = $this->db->get("v_assign_payment_method");
		
		$qr = $this->db->query("SELECT * FROM v_assign_payment_method WHERE 1=1 {$where} {$sortstr} LIMIT $offset, $limit_record");
		// echo "v_student_fee_create_inv:".$qr; exit();
		$result = $qr->result();
		$totalRecord = $qr->num_rows() - 0;		
		$totalPage = 1;
		$sqls=count($result);
		// echo $sqls; exit();
		if(isset($result) && count($result)>0){
			$i=1;
			$tr="";
			foreach($result as $row){
				
				$no_imgs = base_url()."assets/upload/students/NoImage.png";	
				$have_img = base_url()."assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg';
				$img = '<img src="'.$no_imgs.'" class="img-circle" alt="No Image" style="width:70px;height:70px;">';
				if (file_exists(FCPATH . "assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg')) {				
				$img = '<img src="'.$have_img.'" class="img-circle" alt="No Image" style="width:70px;height:70px;">';
				}

				
				$liks="";
				
				$add_class='class="active valign_top"';
				$liks='<div class="tr_studentcode create_inv" attr_coninv="0" attr_paymethod="'.$row->feetypeid.'" attr_term_sem_year_id="'.$row->term_sem_year_id.'" attr_period="'.$row->term_sem_year_id.'"  attr_student_num="'.$row->student_num.'" attr_schlevel="'.$row->schlevelid.'" attr_class="'.$row->classid.'" attr_stuid="'.$row->studentid.'" data-target="#mdinvoice" data-toggle="modal">Assign<br>Payment Method</div>';
				$cl_trad='inactive';
					
				$_paymentype='';
				if($row->feetypeid==1){
					$_paymentype="Pay on Term";
				}else if($row->feetypeid==2){
					$_paymentype="Pay on Semister";
				}else if($row->feetypeid==3){
					$_paymentype="Pay on Year";
				}
				// if($row->feetypeid!=5){
				$tr.='<tr class="'.$cl_trad.'">
						<td>'.($i++).'</td>
						<td style="text-align: center;">'.$img.'</td>
						<td>
							'.$row->student_num.'<br>
							'.$row->first_name_kh.' '.$row->last_name_kh.'<br>
							'.$row->first_name.' '.$row->last_name.'<br>
							'.$row->gender.'<br>
						</td>
						<td>'.$row->program.'</td>
						<td>'.$row->sch_level.'</td>
						<td>'.$row->sch_year.'</td>
						<td>'.$row->rangelevelname.'</td>
						<td>'.$row->class_name.'</td>
						<td>'.$_paymentype.'</td>						
						<td>'.$liks.'</td>
					 </tr>';
				// }
					 
			}	
			$arr['tr']=$tr;
		}else{
			$arr['tr']='<tr><td colspan="10" align="center">We did not find anything to show here.</td></tr>';
			}
		
       		return json_encode($arr);	
		//------------
		// $arr = array('result' => $result, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
		
	}

	function get_schlevel($programid = ''){
        $qr_schlevel = $this->db->query("SELECT DISTINCT
                                                l.schlevelid,
                                                l.sch_level
                                            FROM
                                                sch_school_level AS l
                                            WHERE
                                                l.programid = '{$programid}' - 0
                                            ORDER BY
                                                l.sch_level ASC ")->result();        
        
        $arr = array('schlevel' => $qr_schlevel);
        return json_encode($arr);
    }

    function get_year($programid = '', $schlevelid = ''){
        $qr_year = $this->db->query("SELECT DISTINCT
                                            y.yearid,
                                            y.sch_year
                                        FROM
                                            sch_school_year AS y
                                        WHERE
                                            y.programid = '{$programid}' - 0 AND y.schlevelid = '{$schlevelid}' - 0
                                        ORDER BY
                                            y.sch_year ASC ")->result();

        $qr_class = $this->db->query("SELECT
													c.classid,
													c.class_name
												FROM
													sch_school_level AS l
												INNER JOIN sch_class AS c ON l.schlevelid = c.schlevelid
												WHERE
													l.schlevelid = '{$schlevelid}'
												ORDER BY
													c.class_name ASC ")->result();
        
        $arr = array('year' => $qr_year, 'class' => $qr_class);
        return json_encode($arr);
    }

    // get rangelevel =========
    function get_rangelevel($yearid = ''){
        $qr_rangelevel = $this->db->query("SELECT DISTINCT
                                                rl.rangelevelid,
                                                rl.rangelevelname,
                                                rp.rangelevelid AS rangelevelid_period
                                            FROM
                                                sch_school_rangelevel AS rl
                                            INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
                                            AND rl.programid = y.programid
                                            LEFT JOIN sch_rangelevel_period AS rp ON rl.rangelevelid = rp.rangelevelid
                                            WHERE
                                                y.yearid = '{$yearid}' 
                                            ORDER BY rl.rangelevelname ASC ")->result();

        $arr = array('rangelevel' => $qr_rangelevel);
        return json_encode($arr);
    }

    // get_paymentType =========
    public function get_paymentType($feetypeid = '', $programid = '', $schlevelid = '', $yearid = ''){
    	if($feetypeid != ''){
    		$this->db->where("payment_type", $feetypeid);
    	}
    	if($programid != ''){
    		$this->db->where("programid", $programid);
    	}
    	if($schlevelid != ''){
    		$this->db->where("schlevelid", $schlevelid);
    	}
    	if($yearid != ''){
    		$this->db->where("yearid", $yearid);
    	}    	

        $qr_get_paymentType = $this->db->get("v_study_peroid")->result();
        return json_encode($qr_get_paymentType);
    }
	
	public function Fcstudentinfo($attr_schlevel,$attr_class,$studentid){
		
    		$this->db->where("schlevelid", $attr_schlevel);
			$this->db->where("classid", $attr_class);
			$this->db->where("studentid", $studentid);
			
    	
        $getrowInf = $this->db->get("v_student_fee_create_inv")->row();
		// print_r($getrowInf); exit();
		
		
		$countmethod="SELECT
					COUNT(studentid) AS studentid
					FROM
						sch_student_fee
					WHERE 1=1
					AND studentid='".$studentid."'
					AND programid='".$getrowInf->programid."'
					AND schooleleve='".$getrowInf->schlevelid."'
					AND acandemic='".$getrowInf->year."'
					AND ranglev='".$getrowInf->rangelevelid."'
					AND paymentmethod <> ''
					";
		// echo $countmethod; exit();
		
		$ispayexist = $this->db->query($countmethod)->row()->studentid;
		$arr['ispayexist']=$ispayexist;			
		$photodel="<img src='".site_url('../assets/images/icons/delete.png')."'/>";
		
		
					
		$selesql="SELECT DISTINCT
					sch_student_enrollment.enrollid,
					sch_student_enrollment.studentid,
					sch_student_enrollment.transno,
					sch_student_enrollment.feetypeid,
					sch_student_enrollment.is_paid,
					sch_student_enrollment.term_sem_year_id,
					sch_student_enrollment.type,
					v_study_peroid.period,
					sch_student_enrollment.schoolid,
					sch_student_enrollment.`year`,
					sch_student_enrollment.classid,
					sch_student_enrollment.schlevelid,
					sch_student_enrollment.rangelevelid,
					sch_student_enrollment.programid
				FROM
					sch_student_enrollment
				INNER JOIN v_study_peroid ON sch_student_enrollment.feetypeid = v_study_peroid.payment_type
				AND sch_student_enrollment.term_sem_year_id = v_study_peroid.term_sem_year_id
			WHERE 1=1
					AND sch_student_enrollment.studentid='".$studentid."'
					AND sch_student_enrollment.programid='".$getrowInf->programid."'
					AND sch_student_enrollment.schlevelid='".$getrowInf->schlevelid."'
					AND sch_student_enrollment.year='".$getrowInf->year."'
					AND sch_student_enrollment.rangelevelid='".$getrowInf->rangelevelid."'
					AND sch_student_enrollment.is_closed=0
			ORDER BY term_sem_year_id
				";
			// echo $selesql;
		$invOrders=$this->db->query($selesql)->result();
		
		
		$trinv="";
		// print_r($invOrders);
		$gtotal=0;
		if(isset($invOrders) && count($invOrders)>0){
			$i=1;
			$theads="";
			if($this->green->gAction("D")){	
				// $theads=$tradd;
			}
			
			foreach($invOrders as $row){
				$is_paid=$row->is_paid;
				
				if($row->feetypeid==1){
					$_paymentype="By Term";
				}else if($row->feetypeid==2){
					$_paymentype="By Semister";
				}else if($row->feetypeid==3){
					$_paymentype="By Year";
				}
				if($is_paid==1){
					$ispaid="Paid";
				}else{
					$ispaid="Not yet";
				}
				$trinv.='<tr>
							<td style="display:none">
								<input type="text" id="term_sem_year_id" class="term_sem_year_id" value="'.($row->term_sem_year_id).'">
								<input type="text" id="payment_type" class="payment_type" value="'.($row->feetypeid).'">
								<input type="text" id="from_to" class="from_to" value="From">
								<input type="text" id="ispaid" class="ispaid" value="'.$is_paid.'">
								<input type="text" id="description" class="description" value="'.$row->period.'">
							</td>
							
							<td>'.($i++).'</td>
							<td>'.$_paymentype.'</td>
							<td>'.$row->period.'</td>
							<td align="right">'.$ispaid.'</td>
							
						 </tr>';
			}// foreach($invOrders as $row){
			$arr['trinv']='<tr><th>No</th>
							<th>Payment Method Name</th>
							<th>Study Period</th>
							<th style="text-align:right" align="right">Status</th></tr>
							<tr class="success"><th colspan="4">From</th>
							</tr>'.$trinv;	
		}// if(isset($invOrders) && count($invOrders)>0){
					   
		if(isset($getrowInf) && count($getrowInf)>0){
			
			$have_img = base_url()."assets/upload/students/".$getrowInf->year.'/'.$getrowInf->studentid.'.jpg';		
			$no_imgs = base_url()."assets/upload/students/NoImage.png";	
			
			$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:155px;height:155px;">';
			if (file_exists(FCPATH . "assets/upload/students/".$getrowInf->year.'/'.$getrowInf->studentid.'.jpg')) {				
				$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:155px;height:155px;">';
			}
			
		 $arr['image_stu']=$img;
		
		 $arr['getRow']=$getrowInf;
		  return json_encode($arr);
		}
		//header("Content-type:text/x-json");
		// echo json_encode($arr);
	}
	
	public function MFsave($typeno = ''){
		$schoolid=$this->session->userdata("schoolid");
		
		$student_num = $this->input->post('student_num');
		$arr_inv = $this->input->post('arr_inv');
		$attr_all = $this->input->post('attr_all');
		$duedate = $this->input->post('duedate');
		$enrollid = $this->input->post('enrollid');
		
		
		
		$is_condic=$this->input->post('h_iscond_save');		
		
		
		$programid=$this->input->post('opprpid');
		$yearid = $this->input->post('acandemicid');
		$schoolid=$this->session->userdata('schoolid');
		$schlevelid = $this->input->post('shlevelid');
		$oppaymentmethod = $this->input->post('oppaymentmethod');
		$op_frompayment = $this->input->post('op_frompayment');
		$text_note = $this->input->post('text_note');
		
		$type=19;
		
		$nextTran=$this->green->nextTran($type,"Assign Payment Method");
		
		$created_date = date('Y-m-d H:i:s');
		
		if($arr_inv != ""){								  
			foreach($arr_inv as $arr_valinv){
			 $da = array(									
						'studentid' =>$this->input->post('stuid'),
						'schoolid'=>$schoolid,
						'type'=>$type,
						'transno'=>$nextTran,
						'year' => $this->input->post('acandemicid'),
						'classid' => $this->input->post('classe'),
						'schlevelid' => $this->input->post('shlevelid'),
						'programid' => $this->input->post('opprpid'),
						'rangelevelid' => $this->input->post('ranglevid'),
						'is_paid' => $arr_valinv['ispaid'],
						'feetypeid' => $arr_valinv['payment_type'],
						'term_sem_year_id' => $arr_valinv['term_sem_year_id'],
						'enroll_date' => $created_date
						);
				$this->db->insert('sch_student_enrollment', $da);// 1
			};// end foreach($arr_inv as $arr_valinv){ ==================
		}// if($arr_inv != ""){
		if($attr_all != ""){								  
			foreach($attr_all as $myrows){
			 $da2 = array(									
						'studentid' =>$this->input->post('stuid'),
						'type'=>$type,
						'transno'=>$nextTran,
						'is_paid' => $myrows['ispaid'],
						'feetypeid' => $myrows['payment_type'],
						'term_sem_year_id' => $myrows['term_sem_year_id'],
						'from_to' =>$myrows['from_to'],
						'description' =>$myrows['description'],
						'schoolid' => $schoolid,
						'programid' => $this->input->post('opprpid'),
						'schooleleve' => $this->input->post('shlevelid'),
						'acandemic' => $this->input->post('acandemicid'),
						'ranglev' => $this->input->post('ranglevid'),
						'classid' => $this->input->post('classe'),
						'trandate'=> $created_date
						);
				$this->db->insert('sch_assign_payment_method_detail', $da2);// 2
			};// end foreach($arr_inv as $arr_valinv){ ==================
		}// if($arr_inv != ""){	
		$data = array(
				'type' => $type,
				'typeno' =>$nextTran,
				'studentid' =>$this->input->post('stuid'),
				'trandate'=>$created_date,
				'schoolid' => $schoolid,
				'programid' => $this->input->post('opprpid'),
				'schooleleve' => $this->input->post('shlevelid'),
				'acandemic' => $this->input->post('acandemicid'),
				'ranglev' => $this->input->post('ranglevid'),
				'classid' => $this->input->post('classe'),
				'from_paymentmethod' => $this->input->post('op_frompayment'),
				'to_paymentmethod' => $this->input->post('oppaymentmethod'),
				'note' => $this->input->post('text_note')
       			);
			$this->db->insert('sch_assign_payment_method_order', $data);// 3
			
			$updatsql="UPDATE sch_student_enrollment SET 
						is_closed=1
						WHERE 1=1
						AND studentid='".$this->input->post('stuid')."'
						AND schoolid='".$schoolid."'
						AND year='".$this->input->post('acandemicid')."'
						AND classid='".$this->input->post('classe')."'
						AND schlevelid='".$this->input->post('shlevelid')."'
						AND rangelevelid='".$this->input->post('ranglevid')."'
						AND feetypeid <> '".$oppaymentmethod."'
					  ";
					//  echo $updatsql;
			$this->green->runSQL($updatsql);
					  
			//---------------------------------------------------------------------------------------------------------	
			
			
			
			$gettypeno['nextTran']=$nextTran;
			header("Content-type:text/x-json");
			echo json_encode($gettypeno);
	}
	
	
}// main 