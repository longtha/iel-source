<?php 
class M_evaluation_semester_iep extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function countdata(){
        $studentid=$this->input->post('studentid'); 
		$cout_student_id="SELECT COUNT(studentid)
							 FROM sch_evaluation_semester_iep_order
							 WHERE studentid='".$studentid."'
							 ";
	}

	function list_description(){
         $studentid=$this->input->post('studentid');    
		$sql = "SELECT
					sch_evaluation_semester_iep_description.description_id,
					sch_evaluation_semester_iep_description.description,
					sch_evaluation_semester_iep_detail.descrition_id,
					sch_evaluation_semester_iep_detail.seldom,
					sch_evaluation_semester_iep_detail.sometimes,
					sch_evaluation_semester_iep_detail.usually,
					sch_evaluation_semester_iep_detail.consistently,					
					sch_evaluation_semester_iep_order.evaluationid,
					sch_evaluation_semester_iep_order.studentid
					FROM
						sch_evaluation_semester_iep_order
					RIGHT JOIN sch_evaluation_semester_iep_detail ON sch_evaluation_semester_iep_order.evaluationid = sch_evaluation_semester_iep_detail.evaluationid
					LEFT JOIN sch_evaluation_semester_iep_description ON sch_evaluation_semester_iep_detail.descrition_id=sch_evaluation_semester_iep_description.description_id
					WHERE
						sch_evaluation_semester_iep_order.studentid = '".$studentid."'";
		$sql_description = "SELECT
							sch_evaluation_semester_iep_description.description_id,
							sch_evaluation_semester_iep_description.description
							FROM
							sch_evaluation_semester_iep_description
							";
				if($this->db->query($sql)->num_rows() >0){
					$arr = array('sql' => $sql);
					return $arr;
				}else{
					$arr = array('sql' => $sql_description);
					return $arr;		  			
				}			 
	}

	function show_student(){
		date_default_timezone_set("Asia/Bangkok");
		 $studentid = $this->input->post('id');
		 $arr=array();
         $sql=$this->db->query("SELECT *
								FROM 	v_student_profile as vs								
								LEFT JOIN sch_school_level as sl
								ON vs.schlevelid = sl.schlevelid 
								LEFT JOIN sch_evaluation_semester_iep_order as sor
								ON vs.studentid = sor.studentid
								LEFT JOIN sch_school_semester as sem
								ON vs.semesterid=sem.semesterid
								WHERE vs.studentid = '".$studentid."'")->row();
         	$arr['semester']=$sql->semester;
			$arr['name'] = $sql->fullname;
			$arr['namekh'] = $sql->fullname_kh;
			$arr['sch_level'] = $sql->sch_level;
			$arr['sch_levelkh'] = $sql->sch_level;					
         	$arr['year'] = $sql->sch_year;
         	$arr['techer_comment'] = $sql->techer_comment;
			$arr['guardian_comment'] = $sql->guardian_comment;
            $arr['techer_date'] =   (!empty($sql->techer_date)?date('d-m-Y',strtotime($sql->techer_date)): date('d-m-Y'));
            $arr['academic_date'] = (!empty($sql->academic_date)?date('d-m-Y',strtotime($sql->academic_date)): date('d-m-Y'));
            $arr['guardian_date'] = (!empty($sql->guardian_date)?date('d-m-Y',strtotime($sql->guardian_date)): date('d-m-Y'));
            $arr['return_date'] = (!empty($sql->return_date)?date('d-m-Y',strtotime($sql->return_date)): date('d-m-Y'));
            header("Content-type:text/x-json");
            echo json_encode($arr);
	}

	function save_evaluation(){
		$arr_detail = $this->input->post('arr_detail');
		$teacher_date = $this->input->post('teacher_date');
		$academic_date =$this->input->post('academic_date');
		$guardian_date = $this->input->post('guardian_date');
		$return_date = $this->input->post('return_date');
		$t_comment=$this->input->post('t_comment');
		$g_comment =$this->input->post('g_comment');
		$studentid=$this->input->post('studentid');
		$schoolid =$this->input->post('schoolid');
		$programid=$this->input->post('programid');
		$schlevelid= $this->input->post('schlevelid');
		$gradelevelid=$this->input->post('gradelevelid');
		$yearid=$this->input->post('yearid');
		$classid=$this->input->post('classid');
		$semesterid = $this->input->post('semesterid');
		$data = array('studentid' => $studentid ,
					  'schoolid' => $schoolid,
					  'schlevelid' =>$schlevelid ,
					  'grade_levelid' =>$gradelevelid ,
					  'yearid' =>$yearid,
					  'semester' =>$semesterid,
					  'classid' =>$classid ,
					  'techer_comment' => $t_comment,
					  'guardian_comment' =>$g_comment ,
					  'academic_date' => $this->green->convertSQLDate($academic_date),
					  'techer_date' =>$this->green->convertSQLDate($teacher_date),
					  'guardian_date' =>$this->green->convertSQLDate($guardian_date),
					  'return_date' =>$this->green->convertSQLDate($return_date) 
					 );		

		$sql_des=$this->db->query("	SELECT
										sch_evaluation_semester_iep_order.evaluationid,
										sch_evaluation_semester_iep_order.studentid
										FROM
										sch_evaluation_semester_iep_order
										WHERE studentid = '".$studentid."'");

		if($sql_des->num_rows() > 0){			
			 $eva_id =$sql_des->row()->evaluationid;
			$this->db->update('sch_evaluation_semester_iep_order',$data,array('studentid' => $studentid));
			$this->db->delete('sch_evaluation_semester_iep_detail',array('evaluationid' => $eva_id));
				if(!empty($arr_detail)){
				foreach ($arr_detail as $row) {
					$data_detail = array("descrition_id"=> $row["des_id"], 
			                              "evaluationid"=> $eva_id, 
			                              "seldom"=> $row["seldom"],
			                              "sometimes"=>$row["sometimes"],
			                              "usually"=>$row["usaully"],
			                              "consistently"=>$row["consistently"]
									    );
					$this->db->insert('sch_evaluation_semester_iep_detail',$data_detail);
				}				
			}
			$arr['insert']="success";
		}
		else{
			$this->db->insert('sch_evaluation_semester_iep_order',$data);
			$last_evaluationid = $this->db->insert_id();
			foreach ($arr_detail as $row) {
				$data_detail = array("descrition_id"=> $row["des_id"], 
                              "evaluationid"=> $last_evaluationid, 
                              "seldom"=> $row["seldom"],
                              "sometimes"=>$row["sometimes"],
                              "usually"=>$row["usaully"],
                              "consistently"=>$row["consistently"]
						    );
				$this->db->insert('sch_evaluation_semester_iep_detail',$data_detail);

			}
			$arr['insert']="success";		
		}
		return json_encode($arr);
	}

	 function getsch_level($programid="")
		{
	        if($programid !=""){
	            $this->db->where("programid",$programid);
	        }
	        return $this->db->get("sch_school_level")->result();
		}	

		function getschoolyear($sch_program="",$schoolid="",$schlevelid="")
		{
		    if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($sch_program!=""){
                $this->db->where('programid',$sch_program);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
			return $this->db->get('sch_school_year')->result();
		}
	

		function getgradelevel($schoolid="",$programid="",$schlevelid="")
		{
            if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
			return $this->db->get('sch_grade_level')->result();
		}

		function get_class($schoolid="",$grad_level="",$schlevelid="")
		{
            if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($grad_level!=""){
                $this->db->where('grade_levelid',$grad_level);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
             	   $this->db->order_by("class_name","asc");
			return $this->db->get('sch_class')->result();
		}	

		function show_list(){
			$page = $this->input->post('page');     	
			$perpage=$this->input->post('perpage');
			$s_sortby=$this->input->post('s_sortby');
	        $s_sorttype=$this->input->post('s_sorttype');
	        $program=$this->input->post('program');
	        $sch_level	= $this->input->post('sch_level');
	        $years = $this->input->post('years');
	        $classid = $this->input->post('classid');
	        $gradlevel=$this->input->post('gradlevel');
	        $from_date=$this->input->post('from_date');
	        $to_date = $this->input->post('to_date');
	        $gender = $this->input->post('gender');
	        $s_student_id=$this->input->post('s_student_id');
	        $s_full_name =$this->input->post('s_full_name');
	        $studentid = $this->input->post('studentid');
			$gradelevelid = $this->input->post('gradelevelid');
			$schlevelid = $this->input->post('schlevelid');
			$classid = $this->input->post('classid');
			$yearid = $this->input->post('yearid');
			$programid = $this->input->post('programid');
	        $where='';
	        $sortstr="";

	        if($s_full_name != ''){
				$where .= "AND ( CONCAT(
										last_name,
										' ',
										first_name
									) LIKE '%{$s_full_name}%' ";
				$where .= "or CONCAT(
										last_name_kh,
										' ',
										first_name_kh
									) LIKE '%{$s_full_name}%' ) ";			
			}		
	        isset($program) && $program!=""?$where.= " AND v_student_profile.programid like '".$program."%' ":$where.= $where;
	        isset($gradlevel) && $gradlevel!=""?$where.= " AND v_student_profile.grade_levelid like '".$gradlevel."%' ":$where.= $where;
	        isset($s_student_id) && $s_student_id!=""?$where.= " AND v_student_profile.student_num like '".$s_student_id."%' ":$where.= $where;
	        isset($sch_level) && $sch_level!=""?$where.= " AND v_student_profile.schlevelid like '".$sch_level."%' ":$where.= $where;
	        isset($years) && $years!=""?$where.= " AND v_student_profile.yearid like '".$years."%' ":$where.= $where;
	        isset($classid) && $classid!=""?$where.= " AND v_student_profile.classid like '".$classid."%' ":$where.= $where;
	        isset($gender) && $gender!=""?$where.= " AND v_student_profile.gender like '".$gender."%' ":$where.= $where;
	        isset($s_sortby) && $s_sortby!=""?$sortstr.= " ORDER BY ".$s_sortby." ".$s_sorttype:$sortstr.=$sortstr;
	             
			$sql = "SELECT DISTINCT
					v_student_profile.studentid,					
					v_student_profile.schlevelid,
					v_student_profile.schoolid,					
					v_student_profile.programid,
					v_student_profile.yearid,
					v_student_profile.sch_year,
					v_student_profile.classid,					
					v_student_profile.semesterid,
					v_student_profile.grade_levelid,
					v_student_profile.student_num,
					v_student_profile.first_name,
					v_student_profile.last_name,
					v_student_profile.fullname,
					v_student_profile.first_name_kh,
					v_student_profile.last_name_kh,
					v_student_profile.fullname_kh,
					v_student_profile.first_name_ch,
					v_student_profile.last_name_ch,
					v_student_profile.fullname_ch,
					v_student_profile.gender,
					v_student_profile.is_active,
					v_student_profile.class_name,
					v_student_profile.rangelevelid,
					sch_school_level.sch_level,
					sch_grade_level.grade_level
					FROM 
					  v_student_profile
					LEFT JOIN sch_school_level
					ON v_student_profile.schlevelid=sch_school_level.schlevelid
					LEFT JOIN sch_grade_level
					ON v_student_profile.grade_levelid=sch_grade_level.grade_levelid							
					WHERE 1=1 {$where}
					AND v_student_profile.is_active=1
					ORDER BY studentid DESC";


			$table='';
			$pagina='';
			$getperpage=0;
			if($perpage==''){
				$getperpage=10;
			}else{
				$getperpage=$perpage;
			}
			$paging=$this->green->ajax_pagination(count($this->db->query($sql)->result()),site_url("student/c_quick_update_student/get_student_list"),$getperpage,"icon");
			
			$i=1;
			$getlimit=10;
			if($paging['limit']!=''){
				$getlimit=$paging['limit'];
			}
			$limit=" LIMIT {$paging['start']}, {$getlimit}";   

	        if($sortstr!=""){
	            $sql.=$sortstr;
	        }
			$sql.=" {$limit}";
			 // return $sql;
			  $arr = array('sql' => $sql, 'paging' => $paging);
			  return $arr;
		}
}

?>