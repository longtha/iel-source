<?php 
	class Mod_score_final_mention extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}
		
        function delete_gep(){
        	$id = $this ->input->post('id');
	        $sql_delete=$this->db->delete('sch_score_mention_gep',array('menid'=> $id));  
	        $_delete=0;
	        if($sql_delete > 0){
	            $_delete=1;
	        }
	        echo ($_delete);
        }
        function vaidate($menid, $schlevelid, $mention, $mention_kh){
        $where = '';
        if($menid - 0 > 0){
            $where .= "AND m.menid != '{$menid}' ";
        }
        
        $c = $this->db->query("SELECT
                                    COUNT(*) AS c
                                FROM
                                    sch_score_mention_gep AS m
                                WHERE
                                    	m.schlevelid 	= '{$schlevelid}' 
                                    AND m.mention 		= {$mention}' 
                                    AND m.mention_kh 	= ".quotes_to_entities($mention_kh)."
                                    {$where} ")->row()->c - 0;
        return $c;
    }

        function getdata_mention(){
        	
			$perpage=$this->input->post('perpage');
			$s_sortby=$this->input->post('s_sortby');
	        $s_sorttype=$this->input->post('s_sorttype');
	        $s_schlevelid=$this->input->post('s_schlevelid');
	        $s_mention	= $this->input->post('s_mention');
	        $s_mentionkh = $this->input->post('s_mentionkh');
	        $where='';
	        $sortstr="";

	        isset($s_mention) && $s_mention!=""?$where.= " AND sch_score_mention_gep.mention like '".$s_mention."%' ":$where.= $where;
	        isset($s_mentionkh) && $s_mentionkh!=""?$where.= " AND sch_score_mention_gep.mention_kh like '".$s_mentionkh."%' ":$where.= $where;
	        isset($s_schlevelid) && $s_schlevelid!=""?$where.= " AND sch_score_mention_gep.schlevelid like '".$s_schlevelid."%' ":$where.= $where;
	        isset($s_sortby) && $s_sortby!=""?$sortstr.= " ORDER BY ".$s_sortby." ".$s_sorttype:$sortstr.=$sortstr;
	             
			$sql = "SELECT * FROM 
									sch_score_mention_gep						
					INNER JOIN sch_school_level 
					ON sch_score_mention_gep.schlevelid = sch_school_level.schlevelid
					WHERE 1=1 {$where}";


			$table='';
			$pagina='';
			$getperpage=0;
			if($perpage==''){
				$getperpage=10;
			}else{
				$getperpage=$perpage;
			}
			$paging=$this->green->ajax_pagination(count($this->db->query($sql)->result()),site_url("school/c_scor_mention_gep/getdata_mention"),$getperpage,"icon");
			
			$i=1;
			$getlimit=10;
			if($paging['limit']!=''){
				$getlimit=$paging['limit'];
			}
			$limit=" LIMIT {$paging['start']}, {$getlimit}";   

	        if($sortstr!=""){
	            $sql.=$sortstr;
	        }
			$sql.=" {$limit}";
			 // return $sql;
			  $arr = array('sql' => $sql, 'paging' => $paging);
			  return $arr;
		    }

	   
	}

 ?>