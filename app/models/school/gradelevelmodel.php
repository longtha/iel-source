<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class gradelevelmodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url('school/gradelevel/index?');
			$config['page_query_string'] = TRUE;
			// $config['per_page']=100;
			// $config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$config['total_rows']=$this->db->get('sch_grade_level')->num_rows();
			$this->pagination->initialize($config);
			//$this->db->where('is_active',1);
			//return $this->db->get('sch_subject', $config['per_page'],$page)->result();
			
			//------ Join ------
			$this->db->select("*");
			$this->db->from("sch_grade_level g");
			$this->db->join("sch_school_level sl","g.schlevelid=sl.schlevelid","inner");
			$this->db->join("sch_school_infor s","g.schoolid=s.schoolid","inner");
			$this->db->order_by("grade_levelid", "desc");
			//$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			
			return $query->result();		
		}

		function getschinfor()
		{
			//$this->db->where('status',1);
			$query=$this->db->get('sch_grade_level');
			return $query->result();
		}	

		function getschool()
		{
			$this->db->where('is_active',1);
			$query=$this->db->get('sch_school_infor');
			return $query->result();
		}

		function getschlevel()
		{
			$this->db->where('schoolid',$this->session->userdata('schoolid'));
			$query=$this->db->get('sch_school_level');
			return $query->result();
		}	

		function getgradelevel_row($gradelevel_id)
		{
			$this->db->where('grade_levelid',$gradelevel_id);
			$query=$this->db->get('sch_grade_level');
			return $query->row();
		}
        function getgradelevels($schlevelid="")
        {
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
            $query=$this->db->get('sch_grade_level');
            return $query->result();
        }
		function getvalidate($schoolid, $glevel, $schlevelid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_grade_level');
		    $this->db->where('grade_level',$glevel);
		    $this->db->where('schoolid',$schoolid);
		    $this->db->where('schlevelid',$schlevelid);

		    return  $this->db->count_all_results();
		}

		function getvalidateup($glevel, $grade_levelid, $schoolid, $schlevelid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_grade_level');
		    $this->db->where('grade_level',$glevel);
		    $this->db->where('schoolid',$schoolid);
		    $this->db->where('schlevelid',$schlevelid);
			$this->db->where_not_in('grade_levelid',$grade_levelid);
			return  $this->db->count_all_results();
		}

		function searchgrade_level($schoolid,$schoollevelid='')
		{

			$this->db->select("*");
			$this->db->from("sch_grade_level g");
			$this->db->join("sch_school_level sl","g.schlevelid=sl.schlevelid","inner");
			$this->db->join("sch_school_infor s","g.schoolid=s.schoolid","inner");
			
			if($schoolid!='0' && $schoolid!='')
				$this->db->where('sl.schoolid',$schoolid);
			if($schoollevelid!='0' && $schoollevelid!='')
			 	$this->db->where('g.schlevelid',$schoollevelid);
			$this->db->order_by("g.grade_levelid", "desc");
			//$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			return $query->result();		
		}
	}
?>