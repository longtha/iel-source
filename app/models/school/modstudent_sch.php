<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class ModStudent_sch extends CI_Model{
		function getdays(){
			return $this->db->where('enabled','1')->get('sch_day')->result();
		}
		function getmorningtimes($time){
			return $this->db->query("SELECT * 
									FROM (`sch_time`) 
									WHERE `am_pm` = '$time' 
									AND (for_tran_type='1' OR for_tran_type=3)
									ORDER BY `from_time` ASC									
									")->result();
		}
		function getteacherlist($key='',$limit=''){
			$where='';
			$yearid=$this->session->userdata('year');
			if($key!='')
				$where.=" AND(CONCAT(last_name,' ',first_name ) LIKE '%$key%' 
							OR last_name LIKE '%$key%' 
							OR first_name LIKE '%$key%' 	
							OR empcode LIKE '%$key%' 
							OR phone LIKE '%$key%' 	
							OR position LIKE '%$key%' 						
							)";
			return $this->db->query("SELECT DISTINCT
										sch.year,
										teacherid,
										emp.empid,
										emp.empcode,
										emp.last_name,
										emp.first_name,	
										emp.phone,
										pos.position,
										DATE_FORMAT(emp.dob,'%d-%m-%Y') as dob
									FROM
										sch_time_table sch
									INNER JOIN sch_emp_profile emp ON emp.empid = sch.teacherid
									LEFT JOIN sch_emp_position pos ON pos.posid = emp.pos_id									
									WHERE match_con_posid='tch' 
									AND sch.year='$yearid' 
									{$where} 
									{$limit}")->result();

		}
		function getsubject($gradelevelid,$year=''){
			if($year=='')
				$year=$this->session->userdata('year');
			$school=$this->session->userdata('schoolid');
			return $this->db->query("SELECT * FROM sch_level_subject_detail lsd
									INNER JOIN sch_subject s
									ON(lsd.subjectid=s.subjectid)
									INNER JOIN sch_subject_type st
									on(s.subj_type_id=st.subj_type_id)
									WHERE lsd.grade_levelid='$gradelevelid'
									AND lsd.year='$year'
									AND lsd.schoolid='$school'
									AND s.is_trimester_sub='0'")->result();
		}
		function getsubjectrow($subjectid){
			return $this->db->query("SELECT * FROM sch_subject s
									INNER JOIN sch_subject_type st
									on(s.subj_type_id=st.subj_type_id)
									WHERE s.subjectid='$subjectid'")->row();
		}
		function getteacher($subjectid,$yearid,$classid){

			/*return $this->db->query("SELECT *
									FROM sch_teacher_subject tc
									INNER JOIN sch_emp_profile emp
									ON(tc.teacher_id=emp.empid)
									WHERE tc.subject_id='$subjectid'
									AND tc.yearid='$yearid'
									")->result();*/

			return $this->db->query("SELECT
										tcsub.teacher_id,
										emp.first_name,
										emp.last_name,
										cls.classid,
										cls.schlevelid,
										emp.first_name_kh,
										emp.last_name_kh,
										emp.sex,
										tcsub.yearid,
										tcsub.subject_id,
										tcsub.transno,
										tcsub.type
									FROM
										sch_teacher_subject tcsub
									INNER JOIN sch_class cls ON cls.schlevelid = tcsub.schlevelid
									INNER JOIN sch_emp_profile emp ON (tcsub.teacher_id = emp.empid)
									WHERE
										classid = '$classid'									
									AND tcsub.subject_id='$subjectid'
									AND tcsub.yearid='$yearid'
									")->result();

		}
		function getclasslist($class_name='',$yearid=""){
			$where='';

			if($yearid==""){
				$yearid=$this->session->userdata('year');
			}

			if($class_name!='')
				$where.=" AND sch_class.class_name LIKE '$class_name%'";

			$sql="SELECT DISTINCT
										sch_class.class_name,
										sch.classid,
										sch.`year`,
										sch.title,
										sch.transno,
										sch_level
									FROM
										sch_time_table sch
									INNER JOIN sch_class ON sch.classid = sch_class.classid
									INNER JOIN sch_school_level schl ON (
										sch_class.schlevelid = schl.schlevelid
									)
									WHERE year={$yearid}
									{$where}
									ORDER BY
										classid";
			
			return $this->db->query($sql)->result();
		}
		function ishaveteacher($teacher_id,$classid,$yearid){
			return $this->db->query("SELECT COUNT(teacher_id) as count 
									FROM sch_teacher_class 
									WHERE teacher_id='$teacher_id'
									AND class_id='$classid'
									AND yearid='$yearid'									
									")->row();
		}
		function ishavesubject($dayid,$classid,$subjectid,$timeid,$yearid,$transno){
			$schoolid=$this->session->userdata('schoolid');
			return $this->db->query("SELECT COUNT(time_tableid) as count,teacherid
									FROM sch_time_table
									WHERE dayid='$dayid'
									AND classid='$classid'
									AND subjectid='$subjectid'
									AND timeid='$timeid'
									AND year='$yearid'
									AND schoolid='$schoolid'
									AND transno='$transno'
									GROUP BY teacherid
									")->row();
		}
		function isteacherbc($teacher_id,$timeid,$dayid,$yearid,$t=''){

			return $this->db->query("SELECT COUNT(teacherid) as count,teacherid,classid,subjectid,title 
									FROM sch_time_table 
									WHERE teacherid='$teacher_id'
									AND timeid='$timeid'
									AND dayid='$dayid'
									AND year='$yearid'
									AND teacherid <> '$t'
									GROUP BY teacherid,classid,subjectid
									");
		}
		function search($name,$empcod,$sort_num,$m,$p,$yearid="")
		{	
			if($yearid==""){
				$yearid=$this->session->userdata('year');
			}			

			$page=0;
			$where='';
			if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
		    if($name!='')
		    	$where.="  AND CONCAT(last_name,' ',first_name ) LIKE '%$name%'";
		    if($empcod!='')
		    	$where.="  AND empcode LIKE '%$empcod%'";
		    if($yearid)
		    	$where.=" AND sch.year='$yearid'";
		    $sql="SELECT DISTINCT		sch.year,
										teacherid,
										emp.empid,
										emp.empcode,
										emp.last_name,
										emp.first_name,	
										emp.phone,
										pos.position,
										DATE_FORMAT(emp.dob,'%d-%m-%Y') as dob
									FROM
										sch_time_table sch
									INNER JOIN sch_emp_profile emp ON emp.empid = sch.teacherid
									LEFT JOIN sch_emp_position pos ON pos.posid = emp.pos_id									
									WHERE match_con_posid='tch' AND sch.year='$yearid' {$where}";
			// echo $sql;							
		    $config['base_url'] =site_url()."/school/Student_sched/search?pg=1&m=$m&p=$p&n=$name&ecode=$empcod&s_num=$sort_num&y=$yearid";	
			$config['total_rows'] =  $this->green->getTotalRow($sql);
			$config['per_page'] =$sort_num;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);
			$limi=" limit ".$config['per_page'];
			if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}
			$sql.=$limi;
			return $this->db->query($sql)->result();
		}
		function istchinclass($teacherid,$yearid,$classid){
			return $this->green->getValue("SELECT COUNT(*) FROM sch_teacher_class 
											WHERE teacher_id='{$teacherid}' 
											AND class_id='{$classid}' 
											AND yearid={$yearid}");
		}
	}