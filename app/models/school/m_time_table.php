<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_time_table extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function save($timeTable) {
      $result = $this->db->insert('sch_class_time_table', $timeTable) ? 1 : 0;

      return $result;
    }

    function delete($id) {

      $this->db->delete('sch_class_time_table', array('id' => $id));

      if ($this->db->_error_message()) {
          $result['delete'] = 'Error! ['.$this->db->_error_message().']';
      } else if (!$this->db->affected_rows()) {
          $result['delete'] = 'Error! ID ['. $id .'] not found';
      } else {
          $result['delete'] = 'Success';
      }

      return $result;
    }

    function reload($parameters, $isParameter) {
      if($this->session->userdata('match_con_posid')!='stu'){
      $time_table_query = <<<QUERY
                            SELECT
                              tt.id,
                              si.`name` AS 'school',
                              sl.sch_level AS 'school_level',
                              sy.sch_year AS 'academic_year',
                              g.grade_level AS 'grade_level',
                              c.class_name AS 'class',
                              tt.title,
                              tt.description,
                              tt.attach_file,
                              tt.file_type,
                              tt.transaction_date,
                              u.user_name AS 'created_by'
                            FROM
                              sch_class_time_table tt
                            INNER JOIN sch_school_infor si ON si.schoolid = tt.school_id
                            INNER JOIN sch_school_level sl ON sl.schlevelid = tt.school_level_id
                            INNER JOIN sch_school_year sy ON sy.yearid = tt.academic_year_id
                            INNER JOIN sch_class c ON c.classid = tt.class_id
                            INNER JOIN sch_grade_level g ON g.grade_levelid = tt.grade_level_id
                            INNER JOIN sch_user u ON u.userid = tt.created_by
                            ORDER BY tt.id DESC
QUERY;
    }else{
  $time_table_query = "
                              SELECT
                                tt.id,
                                si.`name` AS 'school',
                                sl.sch_level AS 'school_level',
                                sy.sch_year AS 'academic_year',
                                g.grade_level AS 'grade_level',
                                c.class_name AS 'class',
                                tt.title,
                                tt.description,
                                tt.attach_file,
                                tt.file_type,
                                tt.transaction_date,
                                u.user_name AS 'created_by'
                              FROM
                                sch_class_time_table tt
                              INNER JOIN sch_school_infor si ON si.schoolid = tt.school_id
                              INNER JOIN sch_school_level sl ON sl.schlevelid = tt.school_level_id
                              INNER JOIN sch_school_year sy ON sy.yearid = tt.academic_year_id
                              INNER JOIN sch_class c ON c.classid = tt.class_id
                              INNER JOIN sch_grade_level g ON g.grade_levelid = tt.grade_level_id
                              INNER JOIN sch_user u ON u.userid = tt.created_by
                              WHERE 1=1  AND tt.class_id IN(SELECT DISTINCT classid FROM v_student_profile WHERE 1=1 AND studentid='".($this->session->userdata('emp_id'))."')
                              ORDER BY tt.id DESC
  ";
}
      if ($isParameter == 1) {
        $this->db->where($parameters);
      }

      $homeworks = $this->db->query($time_table_query)->result();

      return $homeworks;
    }

    
    function filter($parameters) {
        
        
        $where='';
        $i=0;
        foreach($parameters as $key => $val)
        {
            if($i==0){
                if($key=="from_date"){
                    
                    list ($day, $month, $year) = explode('-', $val);
                    
                    $startdate = $year."-".$month."-".$day." 00:00:00";
                    
                    $where=$where.' WHERE UNIX_TIMESTAMP(tt.transaction_date)>=UNIX_TIMESTAMP("'.$startdate.'")';
                    
                }else if($key=="to_date"){
                    
                    list ($day, $month, $year) = explode('-', $val);
                    
                    $enddate = $year."-".$month."-".$day." 00:00:00";
                    
                    $where=$where.' WHERE UNIX_TIMESTAMP(tt.transaction_date)<=UNIX_TIMESTAMP("'.$enddate.'")';
                    
                }else{
                    $where=$where.' WHERE tt.'.$key.'="'.$val.'"';
                }
            }else{
                if($key=="from_date"){
                    
                    list ($day, $month, $year) = explode('-', $val);
                    
                    $startdate = $year."-".$month."-".$day." 00:00:00";
                    
                    $where=$where.' AND UNIX_TIMESTAMP(tt.transaction_date)>=UNIX_TIMESTAMP("'.$startdate.'")';
                    
                    
                }else if($key=="to_date"){
                    
                    list ($day, $month, $year) = explode('-', $val);
                    
                    $enddate = $year."-".$month."-".$day." 00:00:00";
                    
                    $where=$where.' AND UNIX_TIMESTAMP(tt.transaction_date)<=UNIX_TIMESTAMP("'.$enddate.'")';
                    
                    
                }else{
                    $where=$where.' AND tt.'.$key.'="'.$val.'"';
                }
            }
            $i=$i+1;
            
        }
        
        $titmetableQuery2 = 'SELECT
                              tt.id,
                              si.`name` AS school,
                              sl.sch_level AS school_level,
                              sy.sch_year AS academic_year,
                              g.grade_level AS grade_level,
                              c.class_name AS class,
                              tt.title,
                              tt.description,
                              tt.attach_file,
                              tt.file_type,
                              tt.transaction_date,
                              u.user_name AS created_by
                            FROM
                              sch_class_time_table tt
                            INNER JOIN sch_school_infor si ON si.schoolid = tt.school_id
                            INNER JOIN sch_school_level sl ON sl.schlevelid = tt.school_level_id
                            INNER JOIN sch_school_year sy ON sy.yearid = tt.academic_year_id
                            INNER JOIN sch_class c ON c.classid = tt.class_id
                            INNER JOIN sch_grade_level g ON g.grade_levelid = tt.grade_level_id
                            INNER JOIN sch_user u ON u.userid = tt.created_by
                            '.$where;
        //if (is_object($parameters)) {
        //    $this->db->where($parameters);
        //}
        
        $timetable2 = $this->db->query($titmetableQuery2)->result();
        return $timetable2 ;
        
    }
    
  
   
 
    function getSchoolLevel($schoolId) {
        return $this->db->query("SELECT lv.schlevelid, lv.sch_level, lv.programid FROM sch_school_level lv INNER JOIN zz_tb_program z ON z.id_program = lv.programid AND lv.schoolid = $schoolId")->result();
    }
    function callClass($school_level_id, $grade_level_id){
        $this->db->order_by("schlevelid", "ASC");
        $this->db->order_by("grade_levelid", "ASC");
        $this->db->order_by("grade_labelid", "ASC");
        
        // $this->db->order_by("class_name", "ASC");
        return $this->db->query("SELECT classid, class_name FROM sch_class WHERE schlevelid = $school_level_id AND grade_levelid = $grade_level_id")->result();
    }
}
