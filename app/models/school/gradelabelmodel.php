<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class gradelabelmodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url('school/gradelabel/index?');
			$config['page_query_string'] = TRUE;
			// $config['per_page']=100;
			// $config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><span>';
			$config['cur_tag_close'] = '</span></a>';
			$config['total_rows']=$this->db->get('sch_grade_label')->num_rows();
			$this->pagination->initialize($config);
			//$this->db->where('is_active',1);
			//return $this->db->get('sch_subject', $config['per_page'],$page)->result();
			
			//------ Join ------
			$this->db->select("*");
			$this->db->from("sch_grade_label g");
			$this->db->join("sch_school_infor s","g.schoolid=s.schoolid","inner");
            $this->db->join("sch_school_level lev","lev.schlevelid=g.schlevelid","left");
            $this->db->join("sch_school_program pro","pro.programid=g.programid","left");
			$this->db->order_by("g.schlevelid", "desc");
			//$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			
			return $query->result();		
		}

		function getschinfor()
		{
			$query=$this->db->get('sch_grade_label');
			return $query->result();
		}	

		function getschool()
		{
			$this->db->where('is_active',1);
			$query=$this->db->get('sch_school_infor');
			return $query->result();
		}	

		function getgradelabel_row($gradelabel_id)
		{
			$this->db->where('grade_labelid',$gradelabel_id);
			$query=$this->db->get('sch_grade_label');
			return $query->row();
			echo $query;
		}
      
	    function getgradelabels($schlevelid="")
        {
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
            $query=$this->db->get('sch_grade_label');
            $this->db->order_by('schlevelid',"ASC");
            $this->db->order_by('grade_label',"ASC");
            return $query->result();
        }
		function getvalidate($schoolid, $glabel,$sclevelid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_grade_label');
		    $this->db->where('grade_label',$glabel);
		    $this->db->where('schoolid',$schoolid);
            $this->db->where('schlevelid',$sclevelid);

		    return  $this->db->count_all_results();
		}

		function getvalidateup($glabel, $grade_labelid, $schoolid,$sclevelid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_grade_label');
		    $this->db->where('grade_label', $glabel);
		    $this->db->where('schoolid', $schoolid);
            $this->db->where('schlevelid', $sclevelid);
		    $this->db->where_not_in('grade_labelid', $grade_labelid);

			return  $this->db->count_all_results();
		}

		function searchgrade_label($schoolid)
		{
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url("school/gradelabel/searchs?l=$schoolid");
			$config['page_query_string'] = TRUE;
			// $config['per_page']=5;
			// $config['num_link']=3;
			$config['full_tag_open'] ='<li>';
			$config['full_tag_close'] ='</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
						
			//$config['total_rows']=$this->db->get('sch_grade_level')->num_rows();
			//$this->pagination->initialize($config);
			$this->db->select("*");
			$this->db->from("sch_grade_label sl");
			$this->db->join("sch_school_infor si","sl.schoolid=si.schoolid","inner");
            $this->db->join("sch_school_level lev","lev.schlevelid=sl.schlevelid","left");
            $this->db->join("sch_school_program pro","pro.programid=sl.programid","left");
			//$this->db->like('sl.schoolid',$schoolid);
			
			if($schoolid!=0)
				$this->db->where('sl.schoolid',$schoolid);

            $this->db->order_by("sl.schlevelid", "desc");
			//$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			return $query->result();		
		}
	}
?>