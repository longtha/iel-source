<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class levelsubjectdetailmodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url('school/levelsubjectdetail/index?');
			$config['page_query_string'] = TRUE;
			// $config['per_page']=100;
			// $config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><span>';
			$config['cur_tag_close'] = '</span></a>';
			$config['total_rows']=$this->db->get('sch_level_subject_detail')->num_rows();
			$this->pagination->initialize($config);
			//$this->db->where('is_active',1);
			//return $this->db->get('sch_subject', $config['per_page'],$page)->result();
			
			//------ Join ------
			$this->db->select("*");
			$this->db->from("sch_level_subject_detail ls");
			$this->db->join("sch_subject s","ls.subjectid=s.subjectid","inner");
			$this->db->join("sch_grade_level gl","ls.grade_levelid=gl.grade_levelid","inner");
			$this->db->join("sch_school_infor si","ls.schoolid=si.schoolid","inner");
			$this->db->order_by("ls.subj_level_id", "desc");
			//$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			
			return $query->result();		
		}

		function getsubjects($sj_type)
		{
			return $this->db->query("select * from sch_subject Where subj_type_id='$sj_type'")->result();
			
		}	
		function searchsubjects($sj_type,$subject,$trim,$is_eval=0)
		{
			$where='';
			if($trim!='')
				$where=" AND is_trimester_sub='$trim'";
			if($is_eval!=""){
				$where.=" AND is_eval='{$is_eval}'";
			}
			return $this->db->query("SELECT * from sch_subject Where subj_type_id='$sj_type' AND subject LIKE '%$subject%' {$where}")->result();
			
		}	
		function getsubject()
		{
			return $this->db->where('schoolid', $this->session->userdata('schoolid'))
					->get('sch_subject')->result();
			
		}	

		function getschool()
		{
			$this->db->where('is_active',1);
			$query=$this->db->get('sch_school_infor');
			return $query->result();
		}

		function getgradelevel()
		{
			$query=$this->db->get('sch_grade_level');
			return $query->result();
		}

		function getschoolyear()
		{
			$query=$this->db->get('sch_school_year');
			return $query->result();
		}	

		function getsubjlevel_row($subj_level_id)
		{
			$this->db->where('subj_level_id',$subj_level_id);
			$query=$this->db->get('sch_level_subject_detail');
			return $query->row();
		}

		function getschoolrow($schoolid)
		{
			$this->db->where('schoolid',$schoolid);
			$query=$this->db->get('sch_school_infor');
			return $query->row();
		}

		function getschoolyearrow($yearid)
		{
			$this->db->where('yearid',$yearid);
			$query=$this->db->get('sch_school_year');
			return $query->row();	
		}

		function getvalidate($schoolid, $subjectid, $year)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_level_subject_detail');
		    $this->db->where('schoolid',$schoolid);
		    // $this->db->where('grade_levelid',$glevelid);
		    $this->db->where('subjectid',$subjectid);
		    $this->db->where('year',$year);
		    return $this->db->count_all_results();
		}

		function getvalidateup($schoolid, $subjectid, $year,$v_school,$v_sub,$v_year)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_level_subject_detail');
		    $this->db->where('schoolid',$schoolid);
		    $this->db->where('year',$year);
		    $this->db->where('subjectid',$subjectid);
		    // $this->db->where('schoolid <>',$v_school);
		    // $this->db->where('year <>',$v_year);
		    // $this->db->where('subjectid <>',$v_sub);
			return  $this->db->count_all_results();
		}

		function searchlevelsubject($subject,$trim)
		{
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url("school/levelsubjectdetail/searchs?l=$subject");
			$config['page_query_string'] = TRUE;
			// $config['per_page']=5;
			// $config['num_link']=3;
			$config['full_tag_open'] ='<li>';
			$config['full_tag_close'] ='</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
						
			//$config['total_rows']=$this->db->get('sch_grade_level')->num_rows();
			//$this->pagination->initialize($config);

			$this->db->select("*");
			$this->db->from("sch_level_subject_detail ls");
			$this->db->join("sch_subject s","ls.subjectid=s.subjectid","inner");
			$this->db->join("sch_grade_level gl","ls.grade_levelid=gl.grade_levelid","inner");
			$this->db->join("sch_school_infor si","ls.schoolid=si.schoolid","inner");
			
			$this->db->where("ls.schoolid",$this->session->userdata('schoolid'));
			$this->db->where("ls.year",$this->session->userdata('year'));
			//$this->db->order_by("ls.subj_level_id", "desc");

			//$this->db->like('sl.schoolid',$schoolid);		
			// if($schoolid!=0)
			// 	$this->db->where('sl.schoolid',$schoolid);
			// 	$this->db->order_by("grade_labelid", "desc");
			//$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			return $query->result();		
		}

		public function getlevelsubject($levelid,$subjectid,$yearid,$school)
		{	
			$this->db->select('count(*)');
			$this->db->from('sch_level_subject_detail');
			$this->db->where('grade_levelid',$levelid);
			$this->db->where('subjectid',$subjectid);
			$this->db->where('year',$yearid);
			$this->db->where('schoolid',$school);
			return $this->db->count_all_results();
		}

		public function editlevelsubject($subjectid,$yearid,$school){
			$row=$this->db->query(" SELECT * 
									FROM sch_level_subject_detail
									Where subjectid='$subjectid'
									AND year='$yearid'
									AND schoolid='$school'
									AND subj_level_id=( SELECT max(subj_level_id)
													    FROM sch_level_subject_detail
														Where subjectid='$subjectid'
														AND year='$yearid'
														AND schoolid='$school')")->row();
			return $row;
		}
	}
?>