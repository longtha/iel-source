<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modacademic_year extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function vaidate($yearid, $sch_year, $schoolid, $programid, $schlevelid, $from_date, $to_date, $isclosed){//
        $where = '';
        if($yearid - 0 > 0){
            $where .= "AND y.yearid <> '{$yearid}' ";
        }
        
        $c = $this->db->query("SELECT
                                    COUNT(*) AS c
                                FROM
                                    sch_school_year AS y
                                WHERE
                                    y.sch_year = '".quotes_to_entities($sch_year)."' AND y.schoolid = '{$schoolid}' AND y.programid = '{$programid}' AND y.schlevelid = '{$schlevelid}' AND y.from_date = '{$from_date}' AND y.to_date = '{$to_date}'AND y.isclosed = '{$isclosed}' {$where} ")->row()->c - 0; 
        return $c;
    }

    function semester($semesterid){
        if($semesterid!=""){
            $this->db->where("semesterid",$semesterid);
        }
        return $this->db->get("sch_school_semester")->row() ;
    }

    function semesters($schoolid="",$yearid="",$programid="",$schlevelid,$semesterid=""){
        if($schoolid!=""){
            $this->db->where("schoolid",$schoolid);
        }
        if($yearid!=""){
            $this->db->where("yearid",$yearid);
        }
        if($programid!=""){
            $this->db->where("programid",$programid);
        }
        if($schlevelid!=""){
            $this->db->where("schlevelid",$schlevelid);
        }
        if($semesterid!=""){
            $this->db->where("semesterid",$semesterid);
        }
        return $this->db->get("sch_school_semester")->result() ;
    }

    function save(){
        $yid = $this->input->post('yid') - 0;
        $sch_year = quotes_to_entities(trim($this->input->post('sch_year')));
        // $year_kh = quotes_to_entities(trim($this->input->post('year_kh')));
        $programid = trim($this->input->post('programid'));
        $yearid = trim($this->input->post('yearid'));
        $schoolid = trim($this->input->post('schoolid'));
        $schlevelid = trim($this->input->post('schlevelid'));
        $from_date = $this->green->formatSQLDate($this->input->post('from_date'));
        $to_date = $this->green->formatSQLDate($this->input->post('to_date'));
        $isclosed = $this->input->post('isclosed');
        $feetypeid = $this->input->post('feetypeid');

        $created_date = date("Y-m-d H:i:s");
        $created_by = $this->session->userdata('user_name');
        $modified_date = date("Y-m-d H:i:s");
        $modified_by = $this->session->userdata('user_name');    

        $data = array('sch_year' => $sch_year,
                        'programid' => $programid,
                        'schoolid' => $schoolid,
                        'schlevelid' => $schlevelid,
                        'from_date' => $from_date,
                        'to_date' => $to_date,
                        'isclosed' => $isclosed,
                        'feetypeid' => $feetypeid,
                        'created_date' => $created_date,
                        'created_by' => $created_by,
                        'modified_date' => $modified_date,
                        'modified_by' => $modified_by                                                
                    );

        $i = 0;

        // vaidate($yearid, $sch_year, $schoolid, $programid, $schlevelid, $from_date, $to_date, $isclosed)
        if($this->vaidate($yid, $sch_year, $schoolid, $programid, $schlevelid, $from_date, $to_date, $isclosed) > 0){
            return $i;
        }else{
            if($yid - 0 > 0){
                unset($data['created_date']);
                unset($data['created_by']);                
                $i = $this->db->update('sch_school_year', $data, array('yearid' => $yid));
                if($i - 0 > 0){
                    // rangelevel =======
                    $this->db->delete('sch_rangelevel_period', array('typeid' => $yid, 'type' => '3'));
                    $rangelevelid = $this->input->post('rangelevelid');
                    if(!empty($rangelevelid)){
                        foreach ($rangelevelid as $row) {
                            $data1 = array('type' => $feetypeid,
                                            'typeid' => $yid,
                                            'rangelevelid' => $row['rangelevelid']
                                        );
                            $i = $this->db->insert('sch_rangelevel_period', $data1);
                        }
                    }
                } 
                
            }else{
                unset($data['modified_date']);
                unset($data['modified_by']);
                $i = $this->db->insert('sch_school_year', $data);
                if($i - 0 > 0){
                    // rangelevel =======   
                    $last_yearid = $this->db->insert_id();
                    $rangelevelid = $this->input->post('rangelevelid');                             
                    if(!empty($rangelevelid)){                    
                        foreach ($rangelevelid as $row) {
                            $data1 = array('type' => $feetypeid,
                                            'typeid' => $last_yearid,
                                            'rangelevelid' => $row['rangelevelid']
                                        );
                            $i = $this->db->insert('sch_rangelevel_period', $data1);
                        }
                    }
                }            

            }// save ======

        }// validate =====        

        return $i;
    }

    function edit($yearid = ''){
        $row = $this->db->query("SELECT
                                        y.yearid,
                                        y.sch_year,
                                        DATE_FORMAT(y.from_date,'%d/%m/%Y') AS from_date,
                                        DATE_FORMAT(y.to_date,'%d/%m/%Y') AS to_date,
                                        y.schoolid,
                                        y.schlevelid,
                                        y.programid,
                                        y.feetypeid,
                                        y.isclosed,
                                        y.created_by,
                                        y.created_date,
                                        y.modified_by,
                                        y.modified_date
                                    FROM
                                        sch_school_year AS y
                                    WHERE
                                        y.yearid = '{$yearid}' ")->row();

        return json_encode($row);
    }

    function delete($yearid = ''){
        $i = 0;
        $i = $this->db->delete('sch_school_year', array('yearid' => $yearid));
        if($i - 0 > 0){
            $i = $this->db->delete('sch_rangelevel_period', array('typeid' => $yearid, 'type' => '3'));
        }
        return json_encode($i);
    }

    function grid(){
        $offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;        
        $sch_year_search = quotes_to_entities(trim($this->input->post('sch_year_search')));
        $programid_search = trim($this->input->post('programid_search'));
        $yearid_search = trim($this->input->post('yearid_search'));
        $schoolid_search = trim($this->input->post('schoolid_search'));
        $schlevelid_search = trim($this->input->post('schlevelid_search'));
        $from_date_search = trim($this->input->post('from_date_search'));
        $to_date_search = trim($this->input->post('to_date_search'));
        $isclosed_search = $this->input->post('isclosed_search');
        $rangelevelid_search = $this->input->post('rangelevelid_search');

        $where = '';

        if($sch_year_search != ''){
            $where .= "AND y.sch_year LIKE '%{$sch_year_search}%' ";
        }

        if($programid_search != ''){
            $where .= "AND y.programid = '{$programid_search}' ";
        }

        if($yearid_search != ''){
            $where .= "AND y.yearid = '{$yearid_search}' ";
        }

        if($schoolid_search != ''){
            $where .= "AND y.schoolid = '{$schoolid_search}' ";
        }

        if($schlevelid_search != ''){
            $where .= "AND y.schlevelid = '{$schlevelid_search}' ";
        }

        // date =======
        if($from_date_search != ''){
            $where .= "AND ( DATE_FORMAT(y.from_date,'%d/%m/%Y') >= '{$from_date_search}' ) AND ( DATE_FORMAT(y.from_date,'%d/%m/%Y') <= '{$to_date_search}' ) ";      
        }

        if($to_date_search != ''){
            $where .= "AND ( DATE_FORMAT(y.to_date,'%d/%m/%Y') >= '{$from_date_search}' ) AND ( DATE_FORMAT(y.to_date,'%d/%m/%Y') <= '{$to_date_search}' ) ";        
        }

        if($isclosed_search != ''){
            $where .= "AND y.isclosed = '{$isclosed_search}' - 0 ";
        }

        if($rangelevelid_search != ''){
            $where .= "AND rp.rangelevelid = '{$rangelevelid_search}' ";
        }

        $where .= "AND rp.type = '3' ";

        $totalRecord = $this->db->query("SELECT
                                                COUNT(cnt) AS c
                                            FROM
                                                (
                                                    SELECT
                                                        COUNT(*) cnt
                                                    FROM
                                                        sch_school_year AS y
                                                    LEFT JOIN sch_rangelevel_period AS rp ON y.yearid = rp.typeid
                                                    LEFT JOIN sch_school_rangelevel AS r ON rp.rangelevelid = r.rangelevelid
                                                    WHERE
                                                        1 = 1 {$where}
                                                    GROUP BY
                                                        y.yearid
                                                ) AS t ")->row()->c - 0;

        $totalPage = ceil($totalRecord/$limit);

        $result = $this->db->query("SELECT DISTINCT
                                            y.yearid,
                                            y.sch_year,
                                            DATE_FORMAT(y.from_date, '%d/%m/%Y') AS from_date,
                                            DATE_FORMAT(y.to_date, '%d/%m/%Y') AS to_date,
                                            info.`name`,
                                            p.program,
                                            lv.sch_level,
                                            y.feetypeid,
                                            y.isclosed,
                                            -- er.`year` AS year_of_enroll,
                                            rp.rangelevelid,
                                            GROUP_CONCAT(
                                                r.rangelevelname SEPARATOR ', '
                                            ) AS rangelevelname
                                        FROM
                                            sch_school_year AS y
                                        LEFT JOIN sch_school_program AS p ON p.programid = y.programid
                                        LEFT JOIN sch_school_infor AS info ON y.schoolid = info.schoolid
                                        LEFT JOIN sch_school_level AS lv ON lv.schlevelid = y.schlevelid
                                        -- LEFT JOIN sch_student_enrollment AS er ON y.yearid = er.`year`
                                        LEFT JOIN sch_rangelevel_period AS rp ON y.yearid = rp.typeid
                                        LEFT JOIN sch_school_rangelevel AS r ON rp.rangelevelid = r.rangelevelid
                                        WHERE
                                            1 = 1 {$where}
                                        GROUP BY
                                            y.yearid
                                        ORDER BY
                                            y.sch_year ASC
                                        LIMIT $offset, $limit ")->result();
        
        $arr = array('result' => $result, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
        return json_encode($arr);
    }

    function get_schlevel($programid = ''){
        $qr_schlevel = $this->db->query("SELECT DISTINCT
                                                l.schlevelid,
                                                l.sch_level
                                            FROM
                                                sch_school_level AS l
                                            WHERE
                                                l.programid = '{$programid}'
                                            ORDER BY
                                                l.sch_level ASC ")->result();        
        
        $arr = array('schlevel' => $qr_schlevel);
        return json_encode($arr);
    }

    function get_year($programid = '', $schlevelid ){
        $sch_where="";
        if($programid!=""){
            $sch_where.=" AND y.programid = '{$programid}'";
        }
        $qr_year = $this->db->query("SELECT DISTINCT
                                            y.yearid,
                                            y.sch_year
                                        FROM
                                            sch_school_year AS y
                                        WHERE
                                             y.schlevelid = '{$schlevelid}'
                                        {$sch_where}
                                        ORDER BY
                                            y.sch_year ASC ")->result();
        $arr = array('year' => $qr_year);
        return json_encode($arr);
    }

    function get_rangelevel($schlevelid = "", $yid = ""){
        $qr_rangelevel = $this->db->query("SELECT DISTINCT
                                                    r.rangelevelid,
                                                    r.rangelevelname
                                                FROM
                                                    sch_school_rangelevel AS r
                                                WHERE
                                                    r.schlevelid = '{$schlevelid}'
                                                ORDER BY
                                                    r.rangelevelname ASC ")->result();

        $qr_rangelevel_chk = $this->db->query("SELECT
                                                        rp.rangelevel_periodid,
                                                        rp.rangelevelid
                                                    FROM
                                                        sch_rangelevel_period AS rp
                                                    WHERE
                                                        rp.type = '3'
                                                    AND rp.typeid = '{$yid}' ")->result();

        $arr = array('rangelevel' => $qr_rangelevel, 'rangelevel_chk' => $qr_rangelevel_chk);
        return json_encode($arr);
    }

}