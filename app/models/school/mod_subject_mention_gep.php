<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_subject_mention_gep extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	// get subject ======
	function get_subject($schlevelid = ""){
		$where = "";
		if($schlevelid != ""){
			$where .= "AND s.schlevelid = '{$schlevelid}' ";
		}
		$where .= "AND s.is_assessment = '0' ";

		$subject = $this->db->query("SELECT DISTINCT
											s.subjectid,
											s.`subject`
										FROM
											sch_subject_gep AS s
										WHERE 1 = 1 {$where}
										ORDER BY
											s.`subject` ASC ")->result();
		$arr = array('subject' => $subject);
        return json_encode($arr);
	}

	// get grade ======
	function get_grade($schlevelid = ""){
		$where = "";
		if($schlevelid != ""){
			$where .= "AND g.schlevelid = '{$schlevelid}' ";
		}
		$grade = $this->db->query("SELECT DISTINCT
											g.grade_levelid,
											g.grade_level
										FROM
											sch_grade_level AS g
										WHERE
											1 = 1 {$where}										
										ORDER BY
											g.grade_level ASC ")->result();
        return json_encode($grade);
	}

	// save ======
	function save(){
		$schoolid = $this->input->post('schoolid');
		$schlevelid = $this->input->post('schlevelid');
		$arr = $this->input->post('arr');	
		
		$i = 0;
		if(!empty($arr)){
			foreach ($arr as $r1) {

				if(!empty($r1["subjectid"]) > 0){					
					foreach ($r1["subjectid"] as $r2) {

						if(!empty($r2["subjectid"]) > 0){
							foreach ($r2["subjectid"] as $r) {

								if($r["menid_list_id"] - 0 > 0 && trim($r["max_score"]) - 0 > 0 && trim($r["min_score"]) - 0 > 0){
									$data = array("schoolid" => $schoolid,
													"schlevelid" => $schlevelid,
													"subjectid" => $r["subjectid"],
													"gradelevelid" => $r["grade_levelid_list_id"],
													"mentionid" => $r["menid_list_id"],
													"max_score" => $r["max_score"],
													"minscrore" => $r["min_score"]
												);

									$i = $this->db->insert('sch_subject_mention', $data);
								}
							}
						}
					}
				}

			}
		}

        return json_encode($i);
	}

	// grid ======
	function grid(){
        // $offset = $this->input->post('offset') - 0;
        // $limit = $this->input->post('limit') - 0;

        $schlevelid = trim($this->input->post('schlevelid'));
        $subjectid = trim($this->input->post('subjectid'));
        $grade_levelid = trim($this->input->post('grade_levelid'));

        $where = '';
        if($schlevelid != ''){
            $where .= "AND s.schlevelid = '{$schlevelid}' ";
        }
        if($subjectid != ''){
            $where .= "AND s.subjectid = '{$subjectid}' ";
        }        

        $where1 = "";
        if($grade_levelid != ''){
            $where1 .= "AND g.grade_levelid = '{$grade_levelid}' ";
        }

        $totalRecord = $this->db->query("SELECT
												COUNT(s.subjectid) AS c
											FROM
												sch_subject_gep AS s
											WHERE
											1 = 1 {$where} ")->row()->c - 0;
        /*$totalPage = ceil($totalRecord/$limit);*/

        $q = $this->db->query("SELECT
										s.subjectid,
										s.`subject`,
										s.schlevelid
									FROM
										sch_subject_gep AS s
									WHERE
										1 = 1 {$where}
									ORDER BY
										s.`subject` ASC ");//LIMIT $offset, $limit

        $tr = "";
        $i = 1;
        if($q->num_rows() > 0){
        	foreach ($q->result() as $row) {
        		$j = 1;
        		$tr .= '<tr>'.
        					'<td name="no[]" class="no">'.$i++.'</td>'.
        					'<td data-subjectid="'.$row->subjectid.'" name="subjectid_list[]" class="subjectid_list">'.$row->subject.'</td>';

				// grade, mention & min... =======				
				$q_grade = $this->db->query("SELECT
													g.grade_levelid,
													g.grade_level,
													COUNT(g.grade_levelid) AS c
												FROM
													sch_grade_level AS g
												WHERE
													g.schlevelid = '{$row->schlevelid}' {$where1}
												GROUP BY
													g.grade_levelid
												ORDER BY
													g.grade_level ASC ");				
				$tr .= '<td style="text-align: right;" colspan="2">
							<table cellpadding="0" cellspacing="0" style="width: 100%;">';

				$n = 1;
				if($q_grade->num_rows() > 0){
		        	foreach ($q_grade->result() as $row_grade) {
						$tr .= '<tr class="tr_">
									<td style="width: 23%;">
										<div class="col-sm-12">
											<label>
												<input type="checkbox" name="grade_levelid_list[]" class="grade_levelid_list" data-grade="'.$row_grade->grade_levelid.'"> '.$row_grade->grade_level.'
											</label>
										</div>
									</td>';
									
									$tr .= '<td align="left" data-top="'.$n++.'">
												<table cellpadding="0" cellspacing="0" style="width: 100%;">';	
									$tr .= '<tr class="tr__">
												<td style="padding: 8px 0 1px 0;">
													<div class="col-sm-5" style="border-bottom: 1px solid #CCC;">Mention</div>		
													<div class="col-sm-4" style="border-bottom: 1px solid #CCC;">Max Score</div>
													<div class="col-sm-3" style="border-bottom: 1px solid #CCC;">Min Score</div>
												</td>	
											</tr>';

												$q_men = $this->db->query("SELECT
																					sm.menid,
																					sm.mention
																				FROM
																					sch_score_mention AS sm
																				WHERE
																					sm.schlevelid = '{$row->schlevelid}'
																				ORDER BY
																					sm.mention ASC ");	
												if($q_men->num_rows() > 0){
										        	foreach ($q_men->result() as $row_men) {
														$tr .= '<tr class="tr__">
																	<td style="padding: 1px;">
																		<div class="col-sm-5">
																			<div>
																				<label style="font-weight: normal !important;">
																					<input type="checkbox" name="menid_list[]" class="menid_list" data-menid="'.$row_men->menid.'"> '.$row_men->mention.'
																				</label>
																			</div>	
																		</div>
																		<div class="col-sm-3">
					 			 											<input decimal type="text" name="max_score[]" class="form-control max_score input-sm" placeholder="0" style="text-align: right;">
					 			 										</div>					
													 			 		<div class="col-sm-1">-</div>
													 			 		<div class="col-sm-3">
													 			 			<input decimal type="text" name="min_score[]" class="form-control min_score input-sm" placeholder="0" style="text-align: right;">
													 			 		</div>
																	</td>
																</tr>';
										        	}

									        	}

									    $tr .=	'</table>';    	

								$tr .=	'</td>
								</tr>';
		        	}		        	
	        	}
				$tr .= '</table>';

				// close =======
				$tr .= '<td colspan="1"><a href="javascript:;" name="del[]" class="del"><button type="button" class="btn btn-link"><img src="'.base_url('assets/images/icons/delete.png').'"></button></a></td>'.        					
	        			'</tr>';
        	}
        }else{
        	$tr .= '<tr>'.
    					'<td colspan="5" style="text-align: center;font-weight: bold;">No Results</td>'.
    				'</tr>';
        }

        // chk mention =====
		$score_mn = $this->db->query("SELECT
									sm.menid
								FROM
									sch_score_mention AS sm
								WHERE
									sm.schlevelid = '{$schlevelid}' ")->result();
                                                
        $arr = array('tr' => $tr, 'score_mn' => $score_mn, 'totalRecord' => $totalRecord);//, 'totalPage' => $totalPage
        return json_encode($arr);
    }

}