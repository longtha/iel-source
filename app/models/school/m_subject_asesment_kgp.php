<?php
    class m_subject_asesment_kgp extends CI_Model{
    	// vallidate --------------------------------
    	function validate($asesment_kh,$assess_id){
            $where='';
            if($assess_id!='')
                $where.=" AND assess_id<>'$assess_id'";
            return $this->db->query("SELECT COUNT(*) as count FROM sch_subject_assessment_kgp where assess_name_kh='$asesment_kh' {$where}")->row()->count;
        }
        // save -------------------------------------------
		function save($assess_id){ 
			date_default_timezone_set("Asia/Bangkok");
			$asesment_kh = $this->input->post('asesment_kh');
			$asesment_eng = $this->input->post('asesment_eng');
			//$percentag = ($this->input->post('percentag') - 0)/100;
			$status = $this->input->post('status');
	  		$user   = $this->session->userdata('user_name');
	  		$data   = array('assess_name_kh'	 => $asesment_kh,
	  						'assess_name_en' => $asesment_eng,
							//'def_percentag'	 => $percentag,
							'is_active' => $status
				 			);
	  		 	
		    if($assess_id !=''){ 
		    	$this->db->where('assess_id',$assess_id);
		    	$this->db->update('sch_subject_assessment_kgp',array_merge($data));		    	
		    }else{ 
		    	$this->db->insert('sch_subject_assessment_kgp', array_merge($data));
		    }	  	
		   	return $assess_id;
		}

		// editdata ----------------------------------------
		function editdata(){
			$assess_id = $this->input->post('assess_id');
			$sql = $this->db->query("SELECT
										sch_subject_assessment_kgp.assess_id,
										sch_subject_assessment_kgp.assess_name_kh,
										sch_subject_assessment_kgp.assess_name_en,
										sch_subject_assessment_kgp.def_percentag,
										sch_subject_assessment_kgp.is_active
									FROM
										sch_subject_assessment_kgp
	 								WHERE assess_id = '".$assess_id."'")->row();
			return  $sql;
			
		}


    }
   	