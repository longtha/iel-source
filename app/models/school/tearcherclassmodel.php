<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class tearcherclassmodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}
		//----------
		function getTeacherOpt(){
			$sql_teacher = $this->db->query("SELECT
												sep.empid,
												sep.empcode,
												sep.first_name,
												sep.last_name,
												sepo.match_con_posid
											FROM sch_emp_profile as sep 
											INNER JOIN sch_emp_position as sepo ON sep.pos_id=sepo.posid
											WHERE sepo.match_con_posid='tch'");
			// $sql_teacher = $this->db->query("SELECT
												// sch_user.userid,
												// sch_user.user_name,
												// sch_user.last_name,
												// sch_user.first_name,
												// sch_user.match_con_posid,
												// sch_user.emp_id
												// FROM
												// sch_user
												// WHERE match_con_posid='tch'");								
			$show_teacher = "";
			if($sql_teacher->num_rows() > 0){
				$show_teacher = $sql_teacher->result();
			}
			else{
				$show_teacher = "No have data";
			}
			return $show_teacher;
		}
		function grandOpt(){
			$schlavelid   = $this->input->post("schlavelid");
			$grandlavelid = $this->input->post("grandlavelid");
			$sql_grandlavel = $this->db->query("SELECT
												sch_grade_level.grade_levelid,
												sch_grade_level.grade_level,
												sch_grade_level.schoolid,
												sch_grade_level.next_grade_level,
												sch_grade_level.schlevelid
												FROM
												sch_grade_level
												WHERE schlevelid='".$schlavelid."'")->result();
			return $sql_grandlavel;
		}
		function getGrandOpt(){
			$schlavelid   = $this->input->post("schlavelid");
			$grandlavelid = $this->input->post("grandlavelid");
			$teacherid    = $this->input->post("teacherid");
			$programid    = $this->input->post("programid");
			$yearid    = $this->input->post("yearid");
			
			$ch_classhandle = $this->db->query("SELECT
													sch_teacher_classhandle.teacher_id,
													sch_teacher_classhandle.schlevelid,
													sch_teacher_classhandle.class_id,
													sch_teacher_classhandle.gradelevelid
													FROM
													sch_teacher_classhandle
													WHERE teacher_id='".$teacherid."'
													AND schlevelid='".$schlavelid."'
													AND gradelevelid='".$grandlavelid."'
													AND yearid='".$yearid."'");
			$arr_ch_h = "";
			if($ch_classhandle->num_rows() > 0){
				foreach($ch_classhandle->result() as $rch_h){
					$arr_ch_h['ch_classh_'.$rch_h->class_id]=$rch_h->class_id;
				}
			}
			$ch_class = $this->db->query("SELECT
										sch_teacher_class.teacher_id,
										sch_teacher_class.schlevelid,
										sch_teacher_class.class_id,
										sch_teacher_class.gradelevelid,
										sch_teacher_class.transno
										FROM
										sch_teacher_class
										WHERE teacher_id='".$teacherid."'
										AND schlevelid='".$schlavelid."'
										AND gradelevelid='".$grandlavelid."'
										AND yearid='".$yearid."'");
			$arr_ch_c = "";
			if($ch_class->num_rows() > 0){
				foreach($ch_class->result() as $rch_c){
					$arr_ch_c['ch_classc_'.$rch_c->class_id]=$rch_c->class_id;
				}
			}
			$ch_subj = $this->db->query("SELECT
												sch_teacher_subject.tch_subject_id,
												sch_teacher_subject.teacher_id,
												sch_teacher_subject.subject_id,
												sch_teacher_subject.schlevelid,
												sch_teacher_subject.gradelevelid,
												sch_teacher_subject.transno,
												sch_teacher_subject.classid
												FROM
												sch_teacher_subject
												WHERE teacher_id='".$teacherid."'
												AND schlevelid='".$schlavelid."'
												AND gradelevelid='".$grandlavelid."'
												AND yearid='".$yearid."'");
			$arr_ch_s = array();
			$classinsubject = array();
			if($ch_subj->num_rows() > 0){
				foreach($ch_subj->result() as $rch_s){
					$arr_ch_s[$rch_s->classid]['ch_classs_'.$rch_s->subject_id]=$rch_s->subject_id;
					if(!in_array($rch_s->classid,$classinsubject)){
						$classinsubject[$rch_s->classid] = $rch_s->classid;
					}
					
				}
			}


			$sql_class = $this->db->query("SELECT
												sch_class.classid,
												sch_class.class_name,
												sch_class.grade_levelid,
												sch_class.grade_labelid,
												sch_class.schoolid,
												sch_class.schlevelid
												FROM
												sch_class
												WHERE schlevelid='".$schlavelid."'
												ORDER BY grade_levelid,class_name")->result();
			$sql_subj = "";
			if($programid == 1){
				$sql_subj = $this->db->query("SELECT
													subjectid,
													`subject` as subject_en,
													subj_type_id,
													short_sub,
													subject_kh,
													subject_type
													FROM
													v_school_subject 
													WHERE schlevelid='".$schlavelid."'
													AND grade_levelid='".$grandlavelid."' 
													ORDER BY subj_type_id ASC")->result();
			}else if($programid == 3){
				$sql_subj = $this->db->query("SELECT subjectid,
													short_sub,
													`subject` as subject_en,
													subject_kh,
													subj_type_id,
													subject_type,
													is_core,
													is_skill,
													is_assessment
													FROM v_school_subject_gep
													WHERE schlevelid='".$schlavelid."'
													ORDER BY is_assessment,subj_type_id ASC")->result();
			}else if($programid == 2){
				$sql_subj = $this->db->query("SELECT subjectid,
													short_sub,
													`subject` as subject_en,
													subject_kh,
													subj_type_id,
													subject_type
													FROM v_school_subject_iep 
													WHERE schlevelid='".$schlavelid."'")->result();
			}
			$arr = array('grandclass'=>$sql_class,'subject'=>$sql_subj,'check_h'=>array($arr_ch_h,$arr_ch_c,$arr_ch_s,$classinsubject));
			return $arr;
		}
		function save_data()
		{
			date_default_timezone_set("Asia/Bangkok");
			$yearid     =  $this->input->post("yearid");
	        $teacherid  =  $this->input->post("teacherid");
	        $typeno_edit =  $this->input->post("typeno");
	        $description =  $this->input->post("description");
	        $schoollavel =  $this->input->post("schoollavel");
	        $grandlavel  =  $this->input->post("grandlavel");
	        $programid  =  $this->input->post("programid");
	        $arr_handle  =  $this->input->post("arr_handle");
	        $arr_subject =  $this->input->post("arr_subject");
	        $arr_class   =  $this->input->post("arr_class");
	        $ii = 0;
	        $tranno_arr = "";
	        if($typeno_edit != ""){
	        	$this->db->delete("sch_teacher_summary",array("tch_transno"=>$typeno_edit));
	        	$this->db->delete("sch_teacher_class",array("transno"=>$typeno_edit));
	        	$this->db->delete("sch_teacher_classhandle",array("transno"=>$typeno_edit));
	        	$this->db->delete("sch_teacher_subject",array("transno"=>$typeno_edit));
	        }
	        
    		if($typeno_edit != ""){
    			$tranno = $typeno_edit;
    		}else{
    			$tranno   = $this->green->nextTran(23,'Assign Teacher');
    		}
    		
    		$typenon_summary = $this->db->query("SELECT tch_transno FROM sch_teacher_summary 
    																WHERE 1=1
																	AND teacher_id='".$teacherid."'
																	AND yearid ='".$yearid."'
																	AND schlevelid='".$schoollavel."'
																	AND gradelevelid='".$grandlavel."'")->row();
    		if(isset($typenon_summary->tch_transno) AND $typenon_summary->tch_transno != ""){
    			$this->db->delete("sch_teacher_summary",array("tch_transno"=>$typenon_summary->tch_transno));
	        	$this->db->delete("sch_teacher_class",array("transno"=>$typenon_summary->tch_transno));
	        	$this->db->delete("sch_teacher_classhandle",array("transno"=>$typenon_summary->tch_transno));
	        	$this->db->delete("sch_teacher_subject",array("transno"=>$typenon_summary->tch_transno));
	        	$tranno  = $typenon_summary->tch_transno;
    		}


    		$tranno_arr.=$tranno."##";
			$data_sum = array("tch_transno" =>$tranno,
	        			"teacher_id"=>$teacherid,
	        			"modified_by"=>$this->session->userdata('userid'),
	        			"created_by"=> $this->session->userdata('userid'),
				  		"created_date"=> date('Y-m-d H:i:s'),
				  		"modified_date"=> date('Y-m-d H:i:s'),
				  		"yearid"=> $yearid,
				  		"schlevelid"=>$schoollavel,
				  		"gradelevelid"=>$grandlavel,
				  		"description"=>$description
				  	);
			$this->db->insert("sch_teacher_summary",$data_sum);
			if(isset($arr_handle)){
		    		if(count($arr_handle) > 0 )
		    		{
		    			foreach($arr_handle as $row_handle)
		    			{
							$data_h = array("teacher_id" => $teacherid,
						        			"schlevelid"=>$schoollavel,
						        			"class_id"=>$row_handle,
						        			"yearid"=>$yearid,
						        			"created_by"=> $this->session->userdata('userid'),
									  		"created_date"=> date('Y-m-d H:i:s'),
									  		"modified_by"=> $this->session->userdata('userid'),
									  		"modified_date"=> date('Y-m-d H:i:s'),
									  		'type'=>23,
									  		"transno"=>$tranno,
									  		"gradelevelid"=>$grandlavel
									  		);
			        		$this->db->insert("sch_teacher_classhandle",$data_h);
			        		
		    			}
		    		}
		    }
    		if(count($arr_subject) > 0)
    		{
    			
    			foreach($arr_subject as $subj_id_f)
    			{
    				foreach($subj_id_f as $row_subject){
						$data_s=array(
									'teacher_id'=> $teacherid,
									'schlevelid'=> $schoollavel,
									'subject_id'=> $row_subject['subjid'],
							  		'yearid'=> $yearid,
							  		'created_by'=> $this->session->userdata('userid'),
							  		'created_date'=> date('Y-m-d H:i:s'),
							  		'modified_by'=> $this->session->userdata('userid'),
							  		'modified_date'=> date('Y-m-d H:i:s'),
							  		'type'=>23,
							  		'transno'=>$tranno,
							  		'gradelevelid'=>$grandlavel,
							  		'classid'=>$row_subject['classid'],
							  		'programid'=>$programid
							  		);
		        		$this->db->insert("sch_teacher_subject",$data_s);
		        	}
    			}
    		}
    		if($arr_class > 0)
    		{
    			
    			foreach($arr_class as $row_class)
    			{
    				$data_c=array(
								'teacher_id'=> $teacherid,
								'schlevelid'=> $schoollavel,
								'class_id'=> $row_class,
						  		'yearid'=> $yearid,
						  		'created_by'=> $this->session->userdata('userid'),
						  		'created_date'=> date('Y-m-d H:i:s'),
						  		'modified_by'=> $this->session->userdata('userid'),
						  		'modified_date'=> date('Y-m-d H:i:s'),
						  		'gradelevelid'=>$grandlavel,
						  		'type'=>23,
						  		'transno'=>$tranno
						  		);
	        		$this->db->insert("sch_teacher_class",$data_c);
    			}
    		}
    		$ii++;
		    // end foreach arr_schoollavel
	        
	        return $tranno_arr;
	        //return $sss;
		}
		function show_data()
		{
			$teachid = $this->input->post("teacherid");
			$wh = "";
			if($teachid != ""){
				$wh.= " AND CONCAT(sch_emp_profile.first_name,' ',sch_emp_profile.last_name) LIKE '%".$teachid."%'";
			}
			$sql_summary = $this->db->query("SELECT
												sts.tch_transno,
												sts.teacher_id,
												sts.created_by,
												sts.created_date,
												sts.modified_by,
												sts.modified_date,
												sts.yearid,
												sts.schlevelid,
												sts.gradelevelid,
												sch_emp_profile.first_name,
												sch_emp_profile.last_name,
												sch_emp_profile.first_name_kh,
												sch_emp_profile.last_name_kh,
												sch_grade_level.grade_level
												FROM
												sch_teacher_summary as sts
												INNER JOIN sch_emp_profile ON sts.teacher_id = sch_emp_profile.empid
												INNER JOIN sch_grade_level ON sts.gradelevelid = sch_grade_level.grade_levelid
												WHERE 1=1 {$wh}
											");
			$tr = "";
			if($sql_summary->num_rows() > 0){
				$kk = 1;
				foreach($sql_summary->result() as $row_summ){
					$tr .= '<tr><td style="vertical-align:top;">'.($kk++).'</td><td style="vertical-align:top;">'.$row_summ->first_name.' '.$row_summ->last_name.'</td>';
					$sql_h = $this->db->query("SELECT
													stch.class_id,
													stch.transno,
													sch_class.class_name
													FROM
													sch_teacher_classhandle as stch
													INNER JOIN sch_class ON stch.class_id = sch_class.classid
													WHERE stch.transno='".$row_summ->tch_transno."'
													ORDER BY sch_class.class_name ASC
											");
					$sql_c = $this->db->query("SELECT
													sch_teacher_class.tch_class_id,
													sch_teacher_class.transno,
													sch_teacher_class.class_id,
													sch_teacher_class.schlevelid,
													sch_class.class_name
													FROM
													sch_teacher_class
													INNER JOIN sch_class ON sch_teacher_class.class_id = sch_class.classid
													WHERE sch_teacher_class.transno='".$row_summ->tch_transno."'
													ORDER BY sch_class.class_name ASC
											");
					$sql_s = $this->db->query("SELECT
													sch_teacher_subject.tch_subject_id,
													sch_teacher_subject.transno,
													sch_teacher_subject.subject_id,
													sch_teacher_subject.classid,
													sch_teacher_subject.programid,
													sch_class.class_name
													FROM
													sch_teacher_subject
													INNER JOIN sch_class ON sch_teacher_subject.classid = sch_class.classid
													WHERE sch_teacher_subject.transno='".$row_summ->tch_transno."'
													ORDER BY sch_class.class_name ASC
											");
					
					if($sql_h->num_rows() >0 ){
						$j = 1;
						$tr.='<td><ul style="list-style:none;padding:0px;margin:0px;">';
						$tr.='<li><b>Grade Lavel&nbsp;'.$row_summ->grade_level.'</b></li>';
						foreach($sql_h->result() as $row_h){
							$tr.='<li class="classh_'.$row_summ->tch_transno.'" att_h_classid="'.$row_h->class_id.'">'.($j++).'.&nbsp;'.$row_h->class_name.'</li>';
						}
						$tr.='</ul></td>';
					}else{
						$tr.='<td>&nbsp;</td>';
					}
					
					
					if($sql_c->num_rows() > 0 ){
						$tr.='<td><ul style="list-style:none;padding:0px;margin:0px;">';
						$tr.='<li><b>Grade Lavel&nbsp;'.$row_summ->grade_level.'</b></li>';
						$k = 1;
						foreach($sql_c->result() as $row_c){
							$tr.='<li class="classc_'.$row_summ->tch_transno.'" att_c_classid="'.$row_c->class_id.'">'.($k++).'.&nbsp;'.$row_c->class_name.'</li>';
						}
						$tr.='</ul></td>';
					}else{
						$tr.='<td>&nbsp;</td>';
					}
					
					
					if($sql_s->num_rows() >0 ){
						$tr.='<td><ul style="list-style:none;padding:0px;margin:0px;">';
						$tr.='<li><b>Grade Lavel&nbsp;:'.$row_summ->grade_level.'</b></li>';
						$s = 1; 
						$c = 1;
						$arr_class = array();
						foreach($sql_s->result() as $row_s){
							if(!in_array($row_s->classid,$arr_class)){
								$tr.='<li><b>'.($c++).'.&nbsp;Class :&nbsp;'.$row_s->class_name.'</b></li>';
								$s=1;
							}
							$arr_class[$s] = $row_s->classid;
							if($row_s->programid == 3){
								$subj_name = $this->db->query("SELECT
																	sch_subject_gep.subjectid,
																	sch_subject_gep.`subject` as subject_en,
																	sch_subject_gep.subject_kh
																	FROM
																	sch_subject_gep
																	WHERE subjectid='".$row_s->subject_id."'")->row();
								$subject_en = "";
								$subject_kh = "";
								if(isset($subj_name->subject_en)){
									$subject_en = $subj_name->subject_en;
								}
								if(isset($subj_name->subject_kh)){
									$subject_kh = $subj_name->subject_kh;
								}
								$tr.='<li class="classs_'.$row_summ->tch_transno.'" att_s_classid="'.$row_s->subject_id.'" att_classid="'.$row_s->classid.'">'.($s++).'.&nbsp;'.$subject_en.'&nbsp;('.$subject_kh.')</li>';
							}else if($row_s->programid == 1){
								$subj_name = $this->db->query("SELECT
																	sch_subject.subjectid,
																	sch_subject.`subject` as subject_en,
																	sch_subject.subject_kh
																	FROM
																	sch_subject
																	WHERE subjectid='".$row_s->subject_id."'")->row();
								$subject_en = "";
								$subject_kh = "";
								if(isset($subj_name->subject_en)){
									$subject_en = $subj_name->subject_en;
								}
								if(isset($subj_name->subject_kh)){
									$subject_kh = $subj_name->subject_kh;
								}
								$tr.='<li class="classs_'.$row_summ->tch_transno.'" att_s_classid="'.$row_s->subject_id.'" att_classid="'.$row_s->classid.'">'.($s++).'.&nbsp;'.$subject_en.'&nbsp;('.$subject_kh.')</li>';
							}else if($row_s->programid == 2){
								$subj_name = $this->db->query("SELECT
																	sch_subject_iep.subjectid,
																	sch_subject_iep.`subject` as subject_en,
																	sch_subject_iep.subject_kh
																	FROM
																	sch_subject_iep
																	WHERE subjectid='".$row_s->subject_id."'")->row();
								$subject_en = "";
								$subject_kh = "";
								if(isset($subj_name->subject_en)){
									$subject_en = $subj_name->subject_en;
								}
								if(isset($subj_name->subject_kh)){
									$subject_kh = $subj_name->subject_kh;
								}
								$tr.='<li class="classs_'.$row_summ->tch_transno.'" att_s_classid="'.$row_s->subject_id.'" att_classid="'.$row_s->classid.'">'.($s++).'.&nbsp;'.$subject_en.'&nbsp;('.$subject_kh.')</li>';
							}
							
						}
						$tr.='</u></td>';
					}else{
						$tr.='<td>&nbsp;</td>';
					}
					$tr.='</u></td><td class="remove_tag">
							<input type="hidden" id="schlavelid_edit" value="'.$row_summ->schlevelid.'">
							<input type="hidden" id="grandlavel_edit" value="'.$row_summ->gradelevelid.'">
							<input type="hidden" id="teacherid_edit" value="'.$row_summ->teacher_id.'">
							<input type="hidden" id="yearid_edit" value="'.$row_summ->yearid.'">
							<a href="javascript:void(0)" id="delete_assign" typeno_del="'.$row_summ->tch_transno.'"><img src="'.base_url("assets/images/icons/delete.png").'"></a>&nbsp;
							<a href="javascript:void(0)" id="edite_assign" typeno_edit="'.$row_summ->tch_transno.'"><img src="'.base_url("assets/images/icons/edit.png").'"></a>
							</td>';
					$tr.='</tr>';
				}
			}
			return $tr;
		}

		// ---- end sophorn ----
		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url('school/teacherclass/index?');
			$config['page_query_string'] = TRUE;
			$config['per_page']=50;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><span>';
			$config['cur_tag_close'] = '</span></a>';
			$config['total_rows']=3;
			$this->pagination->initialize($config);
			//$this->db->where('is_active',1);
			//return $this->db->get('sch_subject', $config['per_page'],$page)->result();
			
			//------ Join ------
			// $this->db->select(" ep.first_name, 
			// 					ep.last_name, 
			// 					sl.sch_level, 
			// 					c.class_name, 
			// 					s.subject,
			// 					tc.note,
			// 					tc.transno,
			// 					c.handle_teacher"
			// 				);
			$this->db->select("*,tc.note,tc.transno");
			$this->db->from("sch_teacher_class tc");
			$this->db->join("sch_teacher_subject ts","tc.transno = ts.transno","inner");
			$this->db->join("sch_class c","tc.class_id = c.classid");
			$this->db->join("sch_subject s","ts.subject_id = s.subjectid","inner");
			$this->db->join("sch_school_level sl","tc.schlevelid = sl.schlevelid","inner");
			$this->db->join("sch_emp_profile ep","tc.teacher_id = ep.empid","inner");
			$this->db->where('tc.is_active',1);
			$this->db->distinct();
			$this->db->group_by("c.class_name,s.subject");
			$this->db->order_by("ep.empid", "desc");

			$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			
			return $query->result();		
		}

		function getteacher($teacherid='',$yearid='',$schlevelid="",$limit=''){
			$where='';		
			if($teacherid!='' && $teacherid>0)
				$where.=" AND tc.teacher_id ='$teacherid' ";
			if($yearid!='')
				$where.=" AND tc.yearid ='$yearid' ";
			if($schlevelid!='')
				$where.=" AND tc.schlevelid ='$schlevelid' ";

			$sql="SELECT
						DISTINCT emp.last_name,emp.first_name,tc.teacher_id,tc.transno,tc.yearid,tc.schlevelid
					FROM
						(sch_teacher_class tc)
					INNER JOIN sch_emp_profile emp 
					ON(tc.teacher_id = emp.empid)
					WHERE 1=1 AND tc.is_active=1 {$where} {$limit}";	

			return $this->db->query($sql)->result();
		}

		function getschoolyear()
		{
			$schlavelid = $this->input->post("schlavelid");
			// $this->db->where("schlevelid",$schlavelid);
			// $this->db->order_by("yearid","desc");
			// $query=$this->db->get('sch_school_year');
			$query = $this->db->query("SELECT
											sch_school_year.yearid,
											sch_school_year.sch_year,
											sch_school_year.schoolid,
											sch_school_year.schlevelid
											FROM
											sch_school_year
											WHERE schlevelid='".$schlavelid."'");
			$sql_result = "";
			if($query->num_rows() > 0){
				$sql_result=$query->result();
			}

			return $sql_result;
		}
		
		
		function getschlevel()
		{
			$this->db->where('schoolid',$this->session->userdata('schoolid'));
			$query=$this->db->get('sch_school_level');
			return $query->result();
		}

		
		
		
	}

