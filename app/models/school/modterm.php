<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Modterm extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function term($termid){
        if($termid!=""){
            $this->db->where("termid",$termid);
        }
        return $this->db->get("sch_school_term")->row() ;
    }

    function terms($schoolid="",$yearid="",$programid="",$schlevelid,$termid=""){
        if($schoolid!=""){
            $this->db->where("schoolid",$schoolid);
        }
        if($yearid!=""){
            $this->db->where("yearid",$yearid);
        }
        if($programid!=""){
            $this->db->where("programid",$programid);
        }
        if($schlevelid!=""){
            $this->db->where("schlevelid",$schlevelid);
        }
        if($termid!=""){
            $this->db->where("termid",$termid);
        }
        return $this->db->get("sch_school_term")->result() ;
    }

    function vaidate($termid, $term, $schlevelid, $yearid, $semesterid, $rangelevelid){//
        $where = '';
        if($termid - 0 > 0){
            $where .= "AND t.termid != '{$termid}' ";
        }

        // string 1 ======
        $str = "";
        if(COUNT($rangelevelid) > 0){
            foreach ($rangelevelid as $k) {
                $str .= $k["rangelevelid"].",";
            }
        }

        // string 2 ======
        $q = $this->db->query("SELECT
                                        rp.type,
                                        rp.typeid,
                                        rp.rangelevelid
                                    FROM
                                        sch_rangelevel_period AS rp
                                    WHERE
                                        rp.type = '1'
                                    AND rp.typeid = '{$termid}'
                                    ORDER BY rp.rangelevelid ASC ");
        $str1 = "";
        if($q->num_rows() > 0){            
            foreach ($q->result() as $row){
                $str1 .= $row->rangelevelid.",";
            }
        }
        
        $c = $this->db->query("SELECT
                                        COUNT(t.termid) AS c
                                    FROM
                                        sch_school_term AS t
                                    INNER JOIN sch_rangelevel_period AS rp ON rp.typeid = t.termid
                                    WHERE
                                        rp.type = '1'
                                    AND t.term = '{$term}'
                                    AND t.schlevelid = '{$schlevelid}'
                                    AND t.yearid = '{$yearid}'
                                    AND t.semesterid = '{$semesterid}'
                                    AND '".$str."' = '".$str1."' {$where} ")->row()->c - 0;
        return ("SELECT
                                        COUNT(t.termid) AS c
                                    FROM
                                        sch_school_term AS t
                                    INNER JOIN sch_rangelevel_period AS rp ON rp.typeid = t.termid
                                    WHERE
                                        rp.type = '1'
                                    AND t.term = '{$term}'
                                    AND t.schlevelid = '{$schlevelid}'
                                    AND t.yearid = '{$yearid}'
                                    AND t.semesterid = '{$semesterid}'
                                    AND '".$str."' = '".$str1."' {$where} ");
    }

    function save(){
        $termid = $this->input->post('termid') - 0;
        $term = quotes_to_entities(trim($this->input->post('term')));
        $term_kh = quotes_to_entities(trim($this->input->post('term_kh')));
        $programid = trim($this->input->post('programid'));
        $yearid = trim($this->input->post('yearid'));
        $schoolid = trim($this->input->post('schoolid'));
        $schlevelid = trim($this->input->post('schlevelid'));
        $start_date = $this->green->formatSQLDate($this->input->post('start_date'));
        $end_date = $this->green->formatSQLDate($this->input->post('end_date'));
        $isclosed = $this->input->post('isclosed');
        $feetypeid = $this->input->post('feetypeid');
        $semesterid = $this->input->post('semesterid');        
        $rangelevelid = $this->input->post('rangelevelid');
        $created_date = date("Y-m-d H:i:s");
        $created_by = $this->session->userdata('user_name');
        $modified_date = date("Y-m-d H:i:s");
        $modified_by = $this->session->userdata('user_name');

        $weekids=$this->input->post('weekids');

        $data = array('term' => $term,
                        'term_kh' => $term_kh,
                        'programid' => $programid,
                        'yearid' => $yearid,
                        'schoolid' => $schoolid,
                        'schlevelid' => $schlevelid,
                        'semesterid' => $semesterid,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'isclosed' => $isclosed,
                        'feetypeid' => $feetypeid,
                        'created_date' => $created_date,
                        'created_by' => $created_by,
                        'modified_date' => $modified_date,
                        'modified_by' => $modified_by                                                
                    );
        $i = 0;

        // if($this->vaidate($termid, $term, $schlevelid, $yearid, $semesterid, $rangelevelid) - 0 > 0){
        //     $i = 2;
        // }else{
        if($termid - 0 > 0){
            unset($data['created_date']);
            unset($data['created_by']);
            $i = $this->db->update('sch_school_term', $data, array('termid' => $termid));
            // rangelevel =======
            if($i - 0 > 0){                
                $this->db->delete('sch_rangelevel_period', array('typeid' => $termid, 'type' => '1'));
                if(!empty($rangelevelid)){
                    foreach ($rangelevelid as $row) {
                        $data1 = array('type' => $feetypeid,
                                        'typeid' => $termid,
                                        'rangelevelid' => $row['rangelevelid']
                                    );
                        $i = $this->db->insert('sch_rangelevel_period', $data1);
                    }
                }
            } 
        }else{
            unset($data['modified_date']);
            unset($data['modified_by']);
            $i = $this->db->insert('sch_school_term', $data);
            $termid = $this->db->insert_id();

            // rangelevel =======
            if($i - 0 > 0){                 
                if(!empty($rangelevelid)){                    
                    foreach ($rangelevelid as $row) {
                        $data1 = array('type' => $feetypeid,
                                        'typeid' => $termid,
                                        'rangelevelid' => $row['rangelevelid']
                                    );
                        $i = $this->db->insert('sch_rangelevel_period', $data1);
                    }
                }
            }

        }
        // term week
        if($termid!="" && count($weekids)>0 && $weekids!=""){
           $this->savetermweek($termid,$weekids);
        }
        // save ======

        // }// validate =====        

        return $i;
    }

    function grid(){
        $offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;        
        $term_search = quotes_to_entities(trim($this->input->post('term_search')));
        $programid_search = trim($this->input->post('programid_search'));
        $yearid_search = trim($this->input->post('yearid_search'));
        $semesterid_search = trim($this->input->post('semesterid_search'));
        $schoolid_search = trim($this->input->post('schoolid_search'));
        $schlevelid_search = trim($this->input->post('schlevelid_search'));
        $start_date_search = trim($this->input->post('start_date_search'));
        $end_date_search = trim($this->input->post('end_date_search'));
        $isclosed_search = $this->input->post('isclosed_search');
        $rangelevelid_search = trim($this->input->post('rangelevelid_search'));

        $where = '';
        if($term_search != ''){
            $where .= "AND t.term LIKE '%".$term_search."%' ";
        }
        if($programid_search != ''){
            $where .= "AND t.programid = '{$programid_search}' ";
        }
        if($yearid_search != ''){
            $where .= "AND t.yearid = '{$yearid_search}' ";
        }
        if($semesterid_search != ''){
            $where .= "AND t.semesterid = '{$semesterid_search}' ";
        }
        if($schoolid_search != ''){
            $where .= "AND t.schoolid = '{$schoolid_search}' ";
        }
        if($schlevelid_search != ''){
            $where .= "AND t.schlevelid = '{$schlevelid_search}' ";
        }
        // date =======
        if($start_date_search != ''){
            $where .= "AND ( DATE_FORMAT(t.start_date,'%d/%m/%Y') >= '{$start_date_search}' ) AND ( DATE_FORMAT(t.start_date,'%d/%m/%Y') <= '{$end_date_search}' ) ";      
        }
        if($end_date_search != ''){
            $where .= "AND ( DATE_FORMAT(t.end_date,'%d/%m/%Y') >= '{$start_date_search}' ) AND ( DATE_FORMAT(t.end_date,'%d/%m/%Y') <= '{$end_date_search}' ) ";        
        }
        if($isclosed_search != ''){
            $where .= "AND t.isclosed = '{$isclosed_search}' ";
        }
        if($rangelevelid_search != ''){
            $where .= "AND rp.rangelevelid = '{$rangelevelid_search}' ";
        }

        $where .= "AND rp.type = '1' ";

        $totalRecord = $this->db->query("SELECT
                                                COUNT(cnt) AS c
                                            FROM
                                                (
                                                    SELECT
                                                        COUNT(*) cnt
                                                    FROM
                                                        sch_school_term AS t
                                                    LEFT JOIN sch_rangelevel_period AS rp ON t.termid = rp.typeid
                                                    LEFT JOIN sch_school_rangelevel AS r ON rp.rangelevelid = r.rangelevelid
                                                    WHERE
                                                        1 = 1 {$where}
                                                    GROUP BY
                                                        t.termid
                                                ) AS t ")->row()->c - 0;

        $totalPage = ceil($totalRecord/$limit);

        $result = $this->db->query("SELECT
                                            t.termid,
                                            t.term,
                                            t.term_kh,
                                            t.programid,
                                            t.yearid,
                                            t.schoolid,
                                            t.schlevelid,
                                            t.semesterid,
                                            DATE_FORMAT(t.start_date, '%d/%m/%Y') AS start_date,
                                            DATE_FORMAT(t.end_date, '%d/%m/%Y') AS end_date,
                                            t.isclosed,
                                            p.program,
                                            y.sch_year,
                                            info.`name`,
                                            lv.sch_level,
                                            s.semester,
                                            rp.rangelevelid,
                                            GROUP_CONCAT(
                                                r.rangelevelname SEPARATOR ', '
                                            ) AS rangelevelname
                                        FROM
                                            sch_school_term AS t
                                        LEFT JOIN sch_school_program AS p ON t.programid = p.programid
                                        LEFT JOIN sch_school_year AS y ON t.yearid = y.yearid
                                        LEFT JOIN sch_school_semester AS s ON t.semesterid = s.semesterid
                                        LEFT JOIN sch_school_infor AS info ON t.schoolid = info.schoolid
                                        LEFT JOIN sch_school_level AS lv ON t.schlevelid = lv.schlevelid
                                        LEFT JOIN sch_rangelevel_period AS rp ON t.termid = rp.typeid
                                        LEFT JOIN sch_school_rangelevel AS r ON rp.rangelevelid = r.rangelevelid
                                        WHERE
                                            1 = 1 {$where}
                                        GROUP BY
                                            t.termid
                                        ORDER BY
                                            t.term ASC
                                        LIMIT $offset, $limit ")->result();
                                                
        $arr = array('result' => $result, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
        return json_encode($arr);
    }


    function edit($termid = ''){

        $row = $this->db->query("SELECT
                                        t.termid,
                                        t.term,
                                        t.term_kh,
                                        t.programid,
                                        t.yearid,
                                        t.schoolid,
                                        t.schlevelid,
                                        DATE_FORMAT(t.start_date,'%d/%m/%Y') AS start_date,
                                        DATE_FORMAT(t.end_date,'%d/%m/%Y') AS end_date,
                                        t.isclosed,
                                        t.feetypeid,
                                        t.semesterid
                                    FROM
                                        sch_school_term AS t
                                    WHERE
                                        t.termid = '{$termid}' ")->row();

        return json_encode($row);
    }

    function delete($termid = ''){
        $i = 0;
        $i = $this->db->delete('sch_school_term', array('termid' => $termid));
        if($i - 0 > 0){
            $i = $this->db->delete('sch_rangelevel_period', array('typeid' => $termid, 'type' => '1'));
            $i = $this->db->delete('sch_school_termweek',array('termid' => $termid));
        }
        return json_encode($i);
    }

    function get_schlevel($programid = ''){
        $arr = array('schlevel' => $this->level->getsch_level($programid));
        return json_encode($arr);
    }

    function get_year($programid = '', $schlevelid = ''){
        $qr_year = $this->db->query("SELECT DISTINCT
                                            y.yearid,
                                            y.sch_year
                                        FROM
                                            sch_school_year AS y
                                        WHERE
                                            y.programid = '{$programid}' - 0 AND y.schlevelid = '{$schlevelid}' - 0
                                        ORDER BY
                                            y.sch_year ASC ")->result();
        
        $arr = array('year' => $qr_year);
        return json_encode($arr);
    }

    // semesters($schoolid="",$yearid="",$programid="",$schlevelid,$semesterid="")
    function get_semester($yearid = "", $programid = "", $schlevelid){
        $qr_semester = $this->se->semesters("", $yearid, $programid, $schlevelid ,"");
        $arr = array('semester' => $qr_semester);
        return json_encode($arr);
    }

    function get_rangelevel($termid = "", $schlevelid = ""){//
        // if($schlevelid != ""){
        //     $this->db->where("schlevelid", $schlevelid);
        // }
        // $qr_rangelevel= $this->db->get("sch_school_rangelevel")->result();

        // if($semesterid != ''){
        //     $this->db->where('type', '2');  
        //     $typeid=  $semesterid  ;       
        // }
        // if($termid != ''){
        //     $this->db->where('type', '1');    
        //     $typeid=  $termid  ;           
        // }

        $q = $this->db->query("SELECT DISTINCT
                                        r.rangelevelid,
                                        r.rangelevelname
                                    FROM
                                        sch_school_rangelevel AS r
                                    WHERE
                                        r.schlevelid = '{$schlevelid}'
                                    ORDER BY
                                        r.rangelevelname ASC ")->result();

        $q_chk = $this->db->query("SELECT
                                            rp.rangelevel_periodid,
                                            rp.rangelevelid
                                        FROM
                                            sch_rangelevel_period AS rp
                                        WHERE
                                            rp.type = '1'
                                        AND rp.typeid = '{$termid}' ")->result();               

        $arr = array('rangelevel' => $q, 'rangelevel_chk' => $q_chk);
        return json_encode($arr);
    }

    function get_rangelevel_search($schlevelid = ""){
        if($schlevelid != ""){
            $this->db->where("schlevelid", $schlevelid);
        }
        $get_rangelevel_search= $this->db->get("sch_school_rangelevel")->result();   

        $arr = array('rangelevel' => $get_rangelevel_search);
        return json_encode($arr);
    }

    function savetermweek($termid,$weekids){

        if($termid!="" && count($weekids)>0){
            $this ->db-> where('termid', $termid);
            $this ->db-> delete('sch_school_termweek');
             if(count($weekids)>0){
                 foreach($weekids as $weekid){
                     $data=array("termid"=>$termid,"weekid"=>$weekid['weekid'],"examtypeid"=>$weekid['examid']);
                     $this->db->insert("sch_school_termweek",$data);
                 }
             }
        }
    }

    function getallweeks(){
        return $this->db->get("sch_z_week")->result();
    }
    function gettermweek($termid=""){
        $this->db->select('termweek.weekid,sch_z_week.week,termweek.termid');
        $this->db->from('sch_school_termweek termweek');
        $this->db->join('sch_z_week', 'sch_z_week.weekid = termweek.weekid');
        if($termid!=""){
            $this->db->where("termid", $termid);
        }
        return $this->db->get()->result();
    }

    function getCkWeekTerm($termid)
    {
        $json = array();
        $json['allweeks'] = $this->getallweeks();
        $json['savedweek'] = $this->gettermweek($termid);
        return $json;
    }

    function week(){
        $sql_week = "SELECT
                            sch_z_week.weekid,
                            sch_z_week.`week`
                            FROM
                            sch_z_week
                            ";
        $arr = array('sql_week' => $sql_week);
        return $arr;          
    }

    function show_weeks($termid){
        $sql_weeks = $this->db->query("SELECT
                            sch_school_termweek.termid,
                            sch_school_termweek.weekid,
                            sch_school_termweek.examtypeid
                            FROM
                            sch_school_termweek
                            WHERE termid='".$termid."'");  
        return $sql_weeks;         
    }
}