<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class modweek extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

        function allweeks(){
            return $this->db->get("sch_z_week")->result();
        }
        function weekbyterm($termid=""){
            if($termid!=""){
                $this->db->where("termid",$termid);
            }
            return $this->db->get("v_sch_weekbyterm")->result();
        }

	}
