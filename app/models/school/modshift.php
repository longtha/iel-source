<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modshift extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    function shifts(){
        return $this->db->get("sch_dayshift")->result() ;
    }
}