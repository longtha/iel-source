<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modsemester extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function vaidate($semesterid, $semester, $schoolid, $programid, $schlevelid, $yearid, $from_date, $to_date, $isclosed){
        $where = '';
        if($semesterid - 0 > 0){
            $where .= "AND s.semesterid <> '{$semesterid}' ";
        }
        
        $c = $this->db->query("SELECT
                                    COUNT(*) AS c
                                FROM
                                    sch_school_semester AS s
                                WHERE
                                    s.semester = '".quotes_to_entities($semester)."' AND s.schoolid = '{$schoolid}' AND s.programid = '{$programid}' AND s.schlevelid = '{$schlevelid}' AND s.yearid = '{$yearid}' AND s.from_date = '{$from_date}' AND s.to_date = '{$to_date}'AND s.isclosed = '{$isclosed}' {$where} ")->row()->c - 0; 
        return $c;
    }

    function semester($semesterid){
        if($semesterid!=""){
            $this->db->where("semesterid",$semesterid);
        }
        return $this->db->get("sch_school_semester")->row() ;
    }

    function semesters($schoolid="",$yearid="",$programid="",$schlevelid,$semesterid=""){
        if($schoolid!=""){
            $this->db->where("schoolid",$schoolid);
        }
        if($yearid!=""){
            $this->db->where("yearid",$yearid);
        }
        if($programid!=""){
            $this->db->where("programid",$programid);
        }
        if($schlevelid!=""){
            $this->db->where("schlevelid",$schlevelid);
        }
        if($semesterid!=""){
            $this->db->where("semesterid",$semesterid);
        }
        return $this->db->get("sch_school_semester")->result() ;
    }

    function save(){
        $semesterid = $this->input->post('semesterid') - 0;
        $semester = quotes_to_entities(trim($this->input->post('semester')));
        $semester_kh = quotes_to_entities(trim($this->input->post('semester_kh')));
        $programid = trim($this->input->post('programid'));
        $yearid = trim($this->input->post('yearid'));
        $schoolid = trim($this->input->post('schoolid'));
        $schlevelid = trim($this->input->post('schlevelid'));
        $from_date = $this->green->formatSQLDate($this->input->post('from_date'));//.' '.date('H:i:s')
        $to_date = $this->green->formatSQLDate($this->input->post('to_date'));
        $isclosed = $this->input->post('isclosed');
        $feetypeid = $this->input->post('feetypeid');

        $created_date = date("Y-m-d H:i:s");
        $created_by = $this->session->userdata('user_name');
        $modified_date = date("Y-m-d H:i:s");
        $modified_by = $this->session->userdata('user_name');    

        $data = array('semester' => $semester,
                        'semester_kh' => $semester_kh,
                        'programid' => $programid,
                        'yearid' => $yearid,
                        'schoolid' => $schoolid,
                        'schlevelid' => $schlevelid,
                        'from_date' => $from_date,
                        'to_date' => $to_date,
                        'isclosed' => $isclosed,
                        'feetypeid' => $feetypeid,
                        'created_date' => $created_date,
                        'created_by' => $created_by,
                        'modified_date' => $modified_date,
                        'modified_by' => $modified_by                                                
                    );

        $i = 0;

        // vaidate($semesterid, $semester, $schoolid, $programid, $schlevelid, $yearid, $from_date, $to_date, $feetypeid, $isclosed)
        // if($this->vaidate($semesterid, $semester, $schoolid, $programid, $schlevelid, $yearid, $from_date, $to_date, $isclosed) > 0){
        //     return $i;
        // }else{
        if($semesterid - 0 > 0){
            unset($data['created_date']);
            unset($data['created_by']);                
            $i = $this->db->update('sch_school_semester', $data, array('semesterid' => $semesterid));

            // rangelevel =======
            if($i - 0 > 0){
                $this->db->delete('sch_rangelevel_period', array('typeid' => $semesterid, 'type' => '2'));
                $rangelevelid = $this->input->post('rangelevelid');
                if(!empty($rangelevelid)){
                    foreach ($rangelevelid as $row) {
                        $data1 = array('type' => $feetypeid,
                                        'typeid' => $semesterid,
                                        'rangelevelid' => $row['rangelevelid']
                                    );
                        $this->db->insert('sch_rangelevel_period', $data1);
                    }
                }
            }
        }else{
            unset($data['modified_date']);
            unset($data['modified_by']);
            $i = $this->db->insert('sch_school_semester', $data);

            // rangelevel =======  
            if($i - 0 > 0){ 
                $last_semesterid = $this->db->insert_id();
                $rangelevelid = $this->input->post('rangelevelid');                             
                if(!empty($rangelevelid)){                    
                    foreach ($rangelevelid as $row) {
                        $data1 = array('type' => $feetypeid,
                                        'typeid' => $last_semesterid,
                                        'rangelevelid' => $row['rangelevelid']
                                    );
                        $this->db->insert('sch_rangelevel_period', $data1);
                    }
                }
            }            

        }// save ======

        // }// validate =====
        

        return $i;
    }

    function edit($semesterid = ''){
        $row = $this->db->query("SELECT
                                        se.semesterid,
                                        se.semester,
                                        se.semester_kh,
                                        se.programid,
                                        se.yearid,
                                        se.schoolid,
                                        se.schlevelid,
                                        DATE_FORMAT(se.from_date,'%d/%m/%Y') AS from_date,
                                        DATE_FORMAT(se.to_date,'%d/%m/%Y') AS to_date,
                                        se.isclosed,
                                        se.feetypeid
                                    FROM
                                        sch_school_semester AS se
                                    WHERE
                                        se.semesterid = '{$semesterid}' ")->row();

        return json_encode($row);
    }

    function delete($semesterid = ''){
        $i = 0;
        $i = $this->db->delete('sch_school_semester', array('semesterid' => $semesterid));
        if($i - 0 > 0){
            $i = $this->db->delete('sch_rangelevel_period', array('typeid' => $semesterid, 'type' => '2'));
        }
        return json_encode($i);
    }

    function grid(){
        $offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;        
        $semester_search = quotes_to_entities(trim($this->input->post('semester_search')));
        $programid_search = trim($this->input->post('programid_search'));
        $yearid_search = trim($this->input->post('yearid_search'));
        $schoolid_search = trim($this->input->post('schoolid_search'));
        $schlevelid_search = trim($this->input->post('schlevelid_search'));
        $rangelevelid_search = trim($this->input->post('rangelevelid_search'));
        $from_date_search = trim($this->input->post('from_date_search'));
        $to_date_search = trim($this->input->post('to_date_search'));
        $isclosed_search = $this->input->post('isclosed_search');

        $where = '';

        if($semester_search != ''){
            $where .= "AND se.semester LIKE '%".$semester_search."%' ";
        }
        if($programid_search != ''){
            $where .= "AND se.programid = '{$programid_search}' ";
        }
        if($yearid_search != ''){
            $where .= "AND se.yearid = '{$yearid_search}' ";
        }
        if($schoolid_search != ''){
            $where .= "AND se.schoolid = '{$schoolid_search}' ";
        }
        if($schlevelid_search != ''){
            $where .= "AND se.schlevelid = '{$schlevelid_search}' ";
        }
        // date =======
        if($from_date_search != ''){
            $where .= "AND ( DATE_FORMAT(se.from_date,'%d/%m/%Y') >= '{$from_date_search}' ) AND ( DATE_FORMAT(se.from_date,'%d/%m/%Y') <= '{$to_date_search}' ) ";      
        }
        if($to_date_search != ''){
            $where .= "AND ( DATE_FORMAT(se.to_date,'%d/%m/%Y') >= '{$from_date_search}' ) AND ( DATE_FORMAT(se.to_date,'%d/%m/%Y') <= '{$to_date_search}' ) ";        
        }
        if($isclosed_search != ''){
            $where .= "AND se.isclosed = '{$isclosed_search}' ";
        }
        if($rangelevelid_search != ''){
            $where .= "AND rp.rangelevelid = '{$rangelevelid_search}' ";
        }

        $where .= "AND rp.type = '2' ";

        $totalRecord = $this->db->query("SELECT
                                                COUNT(cnt) AS c
                                            FROM
                                                (
                                                    SELECT
                                                        COUNT(*) cnt
                                                    FROM
                                                        sch_school_semester AS se
                                                    LEFT JOIN sch_rangelevel_period AS rp ON se.semesterid = rp.typeid
                                                    LEFT JOIN sch_school_rangelevel AS r ON rp.rangelevelid = r.rangelevelid
                                                    WHERE
                                                        1 = 1 {$where}
                                                    GROUP BY
                                                        se.semesterid
                                                ) AS t ")->row()->c - 0;

        $totalPage = ceil($totalRecord/$limit);

        $result = $this->db->query("SELECT DISTINCT
                                            se.semesterid,
                                            se.semester,
                                            se.semester_kh,
                                            se.programid,
                                            se.yearid,
                                            se.schoolid,
                                            se.schlevelid,
                                            DATE_FORMAT(se.from_date, '%d/%m/%Y') AS from_date,
                                            DATE_FORMAT(se.to_date, '%d/%m/%Y') AS to_date,
                                            se.isclosed,
                                            p.program,
                                            y.sch_year,
                                            info.`name`,
                                            lv.sch_level,
                                            rp.rangelevelid,
                                            GROUP_CONCAT(
                                                r.rangelevelname SEPARATOR ', '
                                            ) AS rangelevelname
                                        FROM
                                            sch_school_semester AS se
                                        LEFT JOIN sch_school_program AS p ON se.programid = p.programid
                                        LEFT JOIN sch_school_year AS y ON se.yearid = y.yearid
                                        LEFT JOIN sch_school_infor AS info ON se.schoolid = info.schoolid
                                        LEFT JOIN sch_school_level AS lv ON se.schlevelid = lv.schlevelid
                                        LEFT JOIN sch_rangelevel_period AS rp ON se.semesterid = rp.typeid
                                        LEFT JOIN sch_school_rangelevel AS r ON rp.rangelevelid = r.rangelevelid
                                        WHERE
                                            1 = 1 {$where}
                                        GROUP BY
                                            se.semesterid
                                        ORDER BY
                                            se.semester ASC
                                        LIMIT $offset, $limit ")->result();
        
        $arr = array('result' => $result, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
        return json_encode($arr);
    }

    function get_schlevel($programid = ''){
        $qr_schlevel = $this->db->query("SELECT DISTINCT
                                                l.schlevelid,
                                                l.sch_level
                                            FROM
                                                sch_school_level AS l
                                            WHERE
                                                l.programid = '{$programid}' - 0
                                            ORDER BY
                                                l.sch_level ASC ")->result();        
        
        $arr = array('schlevel' => $qr_schlevel);// , 'year' => $qr_year, 'rangelevel' => $qr_rangelevel
        return json_encode($arr);
    }

    function get_year($programid = '', $schlevelid = ''){
        $qr_year = $this->db->query("SELECT DISTINCT
                                            y.yearid,
                                            y.sch_year
                                        FROM
                                            sch_school_year AS y
                                        WHERE
                                            y.programid = '{$programid}' - 0 AND y.schlevelid = '{$schlevelid}' - 0
                                        ORDER BY
                                            y.sch_year ASC ")->result();
        $arr = array('year' => $qr_year);
        return json_encode($arr);
    }

    function get_rangelevel($schlevelid = "", $semesterid = ""){
        $qr_rangelevel = $this->db->query("SELECT DISTINCT
                                                    r.rangelevelid,
                                                    r.rangelevelname
                                                FROM
                                                    sch_school_rangelevel AS r
                                                WHERE
                                                    r.schlevelid = '{$schlevelid}'
                                                ORDER BY
                                                    r.rangelevelname ASC ")->result();

        $qr_rangelevel_chk = $this->db->query("SELECT
                                                        rp.rangelevel_periodid,
                                                        rp.rangelevelid
                                                    FROM
                                                        sch_rangelevel_period AS rp
                                                    WHERE
                                                        rp.type = '2'
                                                    AND rp.typeid = '{$semesterid}' ")->result();

        $arr = array('rangelevel' => $qr_rangelevel, 'rangelevel_chk' => $qr_rangelevel_chk);
        return json_encode($arr);
    }

}