<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modrangelevelfee extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    function rangelevelfee($yearid="",$schlevelid="",$rangelevelid="",$feetypeid="",$term_sem_year_id=""){
        if($yearid!=""){
            $this->db->where("yearid",$yearid);
        }
        if($schlevelid!=""){
            $this->db->where("schlevelid",$schlevelid);
        }
        if($rangelevelid!=""){
            $this->db->where("rangelevelid",$rangelevelid);
        }
        if($feetypeid!=""){
            $this->db->where("feetypeid",$feetypeid);
        }
        if($term_sem_year_id!=""){
            $this->db->where("term_sem_year_id",$term_sem_year_id);
        }
        return $this->db->get("sch_school_rangelevelfee")->row();
    }
 	
	function rangelevfee($yearid,$schlevelid,$rangelevelid,$feetypeid,$term_sem_year_id){
        $this->db->where("yearid",$yearid);
            $this->db->where("schlevelid",$schlevelid);
       
            $this->db->where("rangelevelid",$rangelevelid);
       
            $this->db->where("feetypeid",$feetypeid);
       
            $this->db->where("term_sem_year_id",$term_sem_year_id);
        return $this->db->get("sch_school_rangelevelfee")->row();
    }
	
    function getvalidate($schoolid,$schlevelid,$rangelevelid,$feetypeid,$term_sem_year_id)
    {
        $this->db->select('count(*)');
        $this->db->from('sch_school_rangelevelfee');
        $this->db->where('schoolid',$schoolid);
        $this->db->where('schlevelid',$schlevelid);
        $this->db->where('rangelevelid',$rangelevelid);
        $this->db->where('feetypeid',$feetypeid);
        $this->db->where('term_sem_year_id',$term_sem_year_id);
        return $this->db->count_all_results();
    }
    function save($ranglevfeeid=""){

        date_default_timezone_set("Asia/Bangkok");
        $schoolid=$this->input->post('schoolid');
        $schlevelid=$this->input->post('sclevelid');
        $rangelevelid=$this->input->post('rangelevelid');
        $feetypeid=$this->input->post('feetypeid');
        $yearid=$this->input->post('yearid');

        $term_sem_year_ids=$this->input->post('term_sem_year_ids');
        $fees=$this->input->post('fees');
        $book_prices=$this->input->post('book_prices');
        $admin_fees=$this->input->post('admin_fees');

        $count=0;
        $programid= $this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='".$schlevelid."'")->row()->programid;

        if(count($term_sem_year_ids)>0) {

            /*$this->db->delete('sch_school_rangelevelfee', array('rangelevelid' => $rangelevelid,
                                                                'schlevelid' => $schlevelid,
                                                                'programid' => $programid,
                                                                'schoolid' => $schoolid,
                                                                'feetypeid'=>$feetypeid,
                                                                'yearid'=>$yearid
                                                                ));*/
            $i=0;
            foreach($term_sem_year_ids as $term_sem_year_id){
                $yearid= $this->db->query("SELECT yearid FROM v_study_peroid WHERE term_sem_year_id ='".$term_sem_year_id."'
                AND payment_type='".$feetypeid."'")->row()->yearid;

                $del_where=array('rangelevelid' => $rangelevelid,
                    'schlevelid' => $schlevelid,
                    'schoolid' => $schoolid,
                    'feetypeid'=>$feetypeid,
                    'yearid'=>$yearid,
                    'term_sem_year_id'=>$term_sem_year_id
                );

                $this->db->delete("sch_school_rangelevelfee",$del_where);

                $data=array(
                    'schoolid'=>$schoolid,
                    'schlevelid'=>$schlevelid,
                    'rangelevelid'=>$rangelevelid,
                    'programid'=>$programid,
                    'feetypeid'=>$feetypeid,
                    'term_sem_year_id'=>$term_sem_year_id,
                    'fee'=>$fees[$i],
                    'book_price'=>$book_prices[$i],
                    'admin_fee'=>$admin_fees[$i],
                    'yearid'=>$yearid
                );
                if($count==0) {
                    $this->db->insert('sch_school_rangelevelfee', $data);
                }
                $i++;
            }
        }
        $m='';
        $p='';
        if(isset($_GET['m'])){
            $m=$_GET['m'];
        }
        if(isset($_GET['p'])){
            $p=$_GET['p'];
        }
        redirect("school/rangeslevelfee/?save&m=".$m."&p=".$p);
    }

    function search($start=0){

        $s_sclevelid=(isset($_POST['s_sclevelid']))?$_POST['s_sclevelid']:"";

        $WHERE="";
        if($s_sclevelid!=""){
            $WHERE.=" AND schlevelid = '".$s_sclevelid."'";
        }
        $sql="SELECT * FROM v_rangelevelfee WHERE 1=1 {$WHERE} ORDER BY schoolid,programid,schlevelid,yearid,rangelevelid,feetypeid";
        $total_row=$this->green->getValue("select count(*) as numrow FROM ($sql) as cc ");
        $paging=$this->green->ajax_pagination($total_row,site_url("school/rangeslevelfee/search"),50);

        if(isset($start) && $start>0){
            $paging['start']=($start-1)*$paging['limit'];
        }
        $data=$this->green->getTable("$sql limit {$paging['start']}, {$paging['limit']}");
        $arrJson['paging']=$paging;

        $arrJson['datas']=$data;
        header("Content-type:text/x-json");

        echo json_encode($arrJson);
        exit();
    }
    function delete($ranglevfeeid){
        $result['del']=0;
        if($ranglevfeeid!=""){
            $this->db->delete('sch_school_rangelevelfee', array('ranglevfeeid' => $ranglevfeeid));
            if ($this->db->_error_message()) {
                $result['del'] = 'Error! ['.$this->db->_error_message().']';
            } else if (!$this->db->affected_rows()) {
                $result['del'] = 'Error! ID ['.$ranglevfeeid.'] not found';
            } else {
                $result['del'] = 'Success';
            }
        }
        echo json_encode($result);
        exit();
    }
    function edit($ranglevfeeid){
        $rngdata['rnglevelfeerow']=$this->getrangelevelfee($ranglevfeeid);
        return $rngdata;
    }
    function getrangelevelfee($ranglevfeeid){
        $this->db->where("ranglevfeeid",$ranglevfeeid);
        return $this->db->get("v_rangelevelfee")->row();
    }

}