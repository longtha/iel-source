<?php
    class m_subject_asesment_percentag_kgp extends CI_Model{
        
        // getsubjecttype -------------------------------------------------
        function getsubjecttype(){
            $schoolid    = $this->input->post("schoolid");
            $programid   = $this->input->post("programid");
            $schlevelid  = $this->input->post("schlevelid");
            $qr_main_sub = $this->db->query("SELECT subj_type_id,subject_type 
                                                FROM sch_subject_type
                                                WHERE 1=1 
                                                AND schoolid='".$schoolid."'
                                                AND schlevelid='".$schlevelid."'
                                                ");
            return $qr_main_sub;
        }
        function getsubject(){
            $schoolid    = $this->input->post("schoolid");
            $programid   = $this->input->post("programid");
            $schlevelid  = $this->input->post("schlevelid");
            $groupsubject= $this->input->post("groupsubject");
            $gradelevelid= $this->input->post("gradelevelid");
            $qr_sub = $this->db->query("SELECT
                                        ss.subjectid,
                                        ss.subject_kh
                                        FROM
                                        sch_subject as ss
                                        INNER JOIN sch_subject_detail as ssd ON ss.subjectid = ssd.subjectid
                                        WHERE 1 = 1
                                        AND ss.schoolid ='".$schoolid ."'
                                        AND ss.subj_type_id ='".$groupsubject."'
                                        AND ss.programid ='".$programid."'
                                        AND ss.schlevelid ='".$schlevelid."'
                                        AND ssd.grade_levelid ='".$gradelevelid."'
                                        ");
            return $qr_sub;
        }
        function show_school(){
             $qr_school = $this->db->query("SELECT 
                                                    schoolid,
                                                    name
                                                FROM
                                                    sch_school_infor")->result();        
            return $qr_school;
        }
        function show_program(){
             $qr_program = $this->db->query("SELECT 
                                                    programid,
                                                    program
                                                FROM
                                                    sch_school_program
                                                WHERE programid=1")->result();        
            return $qr_program;
        }
        // get_schyera ---------------------------------------------------
        function get_schyera($schlevel_id = ''){
            $qr_schyear = $this->db->query("SELECT DISTINCT
                                                    y.yearid,
                                                    y.sch_year
                                                FROM
                                                    sch_school_year AS y
                                                WHERE
                                                    y.schlevelid = '{$schlevel_id}' - 0
                                                ORDER BY
                                                    y.sch_year ASC ")->result();        
            
            $arr = array('schyear' => $qr_schyear);
            return json_encode($arr);
        }
    	// getsubject ----------------------------------------------------
        // function getsubject($subj_type_id = ''){
        //     $qr_subject = $this->db->query("SELECT 
        //                                         sb.subjectid,
        //                                         sb.subject_kh,
        //                                         sb.subj_type_id
        //                                     FROM
        //                                         sch_subject AS sb
        //                                     WHERE
        //                                         sb.subj_type_id = '{$subj_type_id}' - 0
        //                                     GROUP BY
        //                                         sb.subject_kh
        //                                     ORDER BY
        //                                         sb.subject_kh ASC ")->result();        
            
        //     $arr = array('subject' => $qr_subject);
        //     return json_encode($arr);
        // }
   		// getschoollavel------------------------------------------------
    	function getschoollavel($programid= 1){
	        if($programid!=""){
	            $this->db->where("programid",$programid);
	        }
        	return $this->db->get("sch_school_level")->result();
   		}
   		// getgradelevels -----------------------------------------------
   		function gradelevel(){
            $schlevel_id = $this->input->post("schlevel_id");
            $schoolid    = $this->input->post("schoolid");
            $programid   = $this->input->post("programid");
            $qr_gradlevel = $this->db->query("SELECT
                                                sch_grade_level.grade_levelid,
                                                sch_grade_level.grade_level
                                                FROM
                                                sch_grade_level
                                                WHERE schlevelid='".$schlevel_id."' 
                                                AND schoolid='".$schoolid."'
                                                ");
            return $qr_gradlevel;
        }
        //percentag
        // getasesment --------------------------------------------------
        function checkasesment($assessment_id, $grade_level_id,$subj_type_id,$subjectid){
            $this->db->select('asp.percentage, 
                               asp.maxscore,
                               asp.ass_percent_id,
                               asp.assessment_id');
            $this->db->from('sch_subject_assessment_percent_kgp as asp');
            $this->db->where('asp.assessment_id',$assessment_id);
            $this->db->where('asp.grade_level_id',$grade_level_id);
            $this->db->where('asp.subject_type_id',$subj_type_id);
            $this->db->where('asp.subject_id',$subjectid);
            $query=$this->db->get();
            return $query;
        }
        // getasesmentname -----------------------------------------------
        function getasesmentname(){ 
            $this->db->select('*');
            $this->db->from('sch_subject_assessment_kgp');
            $this->db->where('is_active', 1); 
            $query = $this->db->get()->result();
            return $query;

        }
        // save --------------------------------------------------------
        function save(){ 
            date_default_timezone_set("Asia/Bangkok");
            $hid_assessment = $this->input->post('hid_assessment');
            $schoolid = $this->input->post('schoolid');
            $programid = $this->input->post('programid');
            $schlevelid = $this->input->post('schlevelid');
            $groupsubject = $this->input->post('groupsubject');
            $user   = $this->session->userdata('userid');
            $subjectexamp = $this->input->post('subjectexamp'); 
            $subject_assesment = $this->input->post('subject_assesment');
            $max_score = $this->input->post('max_score');
            $percentage = $this->input->post('percentage');
            $gradlevel = $this->input->post('gradlevel');
            try{
                $data = array(
                            "assess_name"=>$subject_assesment,
                            "percentag"=>$percentage,
                            "max_score"=>$max_score,
                            "schoolid"=>$schoolid,
                            "programid"=>$programid,
                            "schoollevelid"=>$schlevelid,
                            "grade_level_id"=>$gradlevel,
                            "group_subj_id"=>$groupsubject,
                            "subject_exam_id"=>$subjectexamp,
                            "userid"=>$user
                            );
                
                $assementid = "";
                if($hid_assessment != ""){
                    $this->db->update("sch_subject_assessment_kgp",$data,array("assess_id"=>$hid_assessment));
                    // $data_detail = array("subj_assem_id"=>$hid_assessment,
                    //                     "subj_examp_id"=>$subjectexamp,
                    //                     "subject_type_id"=>$groupsubject,
                    //                     "grade_level_id"=>$gradlevel
                    //                     );
                    // $this->db->update("sch_subject_assessment_detail_kgp",$data_detail,array("subj_assem_id"=>$hid_assessment));
                    //$this->db->delete("sch_subject_assessment_detail_kgp",array("subj_assem_id"=>$hid_assessment));
                    $assementid = $hid_assessment;
                }else{
                    $this->db->insert("sch_subject_assessment_kgp",$data);
                    // $assementid = $this->db->insert_id();
                   //  $data_detail = array("subj_assem_id"=>$assementid,
                   //                      "subj_examp_id"=>$subjectexamp,
                   //                      "subject_type_id"=>$groupsubject,
                   //                      "grade_level_id"=>$gradlevel
                   //                      );
                   // $this->db->insert("sch_subject_assessment_detail_kgp",$data_detail);
                }
                return "OK";
            }catch(Exception $e){
                echo 'Message: ' .$e->getMessage();
            }
            
        }
        
    }

