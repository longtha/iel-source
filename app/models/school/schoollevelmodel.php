<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class schoollevelmodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url('school/schoollevel/index?');
			$config['page_query_string'] = TRUE;
			$config['per_page']=100;
			$config['num_link']=5;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			//$this->db->where('status',1);
			$config['total_rows']=$this->db->get('sch_school_level')->num_rows();
			$this->pagination->initialize($config);
			//$this->db->where('status',1);
			//return $this->db->get('sch_subject', $config['per_page'],$page)->result();
			
			//------ Join ------
			$this->db->select("*");
			$this->db->from("sch_school_level sl");
            $this->db->join("sch_school_program pro","pro.programid=sl.programid","inner");
			$this->db->join("sch_school_infor si","sl.schoolid=si.schoolid","inner");
			$this->db->join("sch_emp_profile emp","sl.schlv_director=emp.empid","LEFT");
			$this->db->order_by("sl.schlevelid", "desc");
			$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			
			return $query->result();		
		}

		function getsch_level_kgp($programid="1")
		{
            if($programid !=""){
                $this->db->where("programid",$programid);
            }
            return $this->db->get("sch_school_level")->result();
		}
		function getsch_level($programid="")
		{
			$userid = $this->session->userdata('userid');
			if($programid !=""){
				$this->db->where("programid",$programid);
			}
			$this->db->select("*");
			$this->db->from("sch_school_level");
			$this->db->where('schlevelid in','(SELECT schlevelid FROM sch_user_schlevel WHERE userid = '.$userid.')',false);
			return $this->db->get()->result();
		}
		function getsch_levels($programid="")
				{
					if($programid !=""){
						$this->db->where("programid",$programid);
					}					
					return $this->db->get("sch_school_level")->result();
				}
		function getsch_level_login($programid="")
		{
			$userid = $this->session->userdata('userid');
			if($programid !=""){
				$this->db->where("programid",$programid);
			}
			$this->db->select("*");
			$this->db->from("sch_school_level");
			$this->db->where('schlevelid in','(SELECT schlevelid FROM sch_user_schlevel WHERE userid = '.$userid.')',false);
			return $this->db->get()->result();
		}
		function getschool()
		{
			//$this->db->where('status',1);
			$query=$this->db->get('sch_school_infor');
			return $query->result();
		}	

		function getsch_level_row($sch_level_id)
		{
			$this->db->where('schlevelid',$sch_level_id);
			$query=$this->db->get('sch_school_level');
			return $query->row();
		}

		function getvalidate($sch_level, $sch_level_id)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_school_level');
		    $this->db->where('sch_level',$sch_level);
		    $this->db->where('schoolid',$sch_level_id);

		    return  $this->db->count_all_results();
		}

		function getvalidateup($sch_level, $school_id, $sch_level_id)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_school_level');
		    $this->db->where('sch_level',$sch_level);
		    $this->db->where('schoolid',$school_id);
			$this->db->where_not_in('schlevelid',$sch_level_id);
			//$this->db->where('status',1);
			return  $this->db->count_all_results();
		}

		function searchsch_levels($sch_level, $school,$is_vtc='')
		{
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url("school/schoollevel/searchs?l=$sch_level&sch=$school");
			$config['page_query_string'] = TRUE;
			$config['per_page']=5;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$this->db->like('sch_level',$sch_level);
			$this->db->like('schoolid',$school);
			// if($s_type!=0)
			// 	$this->db->where('subj_type_id',$s_type);
			if($school!=0)
				$this->db->where('schoolid',$school);
			$config['total_rows']=$this->db->get('sch_school_level')->num_rows();
			$this->pagination->initialize($config);
			$this->db->select("*");
			$this->db->from("sch_school_level sl");
            $this->db->join("sch_school_program pro","pro.programid=sl.programid","inner");
			$this->db->join("sch_school_infor si","sl.schoolid=si.schoolid","inner");
			$this->db->join("sch_emp_profile emp","sl.schlv_director=emp.empid","inner");
			$this->db->like('sch_level',$sch_level);
			if($is_vtc!=0)
				$this->db->where('sl.is_vtc',$is_vtc);
			if($school!=0)
				$this->db->where('si.schoolid',$school);
			$this->db->order_by("sl.schlevelid", "desc");

			$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			return $query->result();		
		}

	}
