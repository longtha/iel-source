<?php
class Modsetupexam extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	// valid. update =====
	function vaidate_upd($examid, $schlevelid, $yearid, $grade_levelid, $examtypeid){
        $where = '';
        if($examid - 0 > 0){
            $where .= "AND e.examid != '{$examid}' ";
        }
        
        $c = $this->db->query("SELECT
                                    COUNT(*) AS c
                                FROM
                                    sch_student_exam_schedule AS e
                                WHERE
                                    e.schlevelid = '{$schlevelid}' AND e.yearid = '{$yearid}' AND e.grade_levelid = '{$grade_levelid}' AND e.examtypeid = '{$examtypeid}' {$where} ")->row()->c - 0; 
        return $c;
    }

	function examtype($examtypeid = ''){		
        if($examtypeid != ''){
            $this->db->where("examtypeid", $examtypeid);
        }
        return $this->db->get("sch_student_examtype")->result();
        
    }
	function examtype_gep($examtypeid = ''){		
        if($examtypeid != ''){
            $this->db->where("examtypeid", $examtypeid);
        }
        return $this->db->get("sch_student_examtype_gep")->result();
        
    }
    function update(){
     	// update =======
     	$examid_list = trim($this->input->post('examid_list'));
     	$grade_levelid_list = trim($this->input->post('grade_levelid_list'));
     	$schoolid_list = trim($this->input->post('schoolid_list'));
     	$schlevelid_list = trim($this->input->post('schlevelid_list'));
     	$yearid_list = trim($this->input->post('yearid_list'));
     	$examtypeid_list = trim($this->input->post('examtypeid_list'));
     	$start_exam_list = trim($this->input->post('start_exam_list'));
     	$end_exam_list = trim($this->input->post('end_exam_list'));
     	$start_entry_score_list = trim($this->input->post('start_entry_score_list'));
     	$end_entry_score_list = trim($this->input->post('end_entry_score_list'));

     	$i = 0;
     	// vaidate_upd($examid, $schlevelid, $yearid, $grade_levelid, $examtypeid)
		if($this->vaidate_upd($examid_list, $schlevelid_list, $yearid_list, $grade_levelid_list, $examtypeid_list) - 0 > 0){
            $i = 2;
        }else{ 
	     	if($examid_list - 0 > 0){
		     	$data = array('grade_levelid' => $grade_levelid_list,
		        					'schoolid' => $schoolid_list,
		        					'schlevelid' => $schlevelid_list,
		        					'yearid' => $yearid_list,
		        					'examtypeid' => $examtypeid_list,
		        					'start_exam' => $this->green->formatSQLDate($start_exam_list),
		        					'end_exam' => $this->green->formatSQLDate($end_exam_list),
		        					'start_entry_score' => $this->green->formatSQLDate($start_entry_score_list),
		        					'end_entry_score' => $this->green->formatSQLDate($end_entry_score_list)
		                        );
		    	$i = $this->db->update('sch_student_exam_schedule', $data, array('examid' => $examid_list));
	    	}
	    }

    	return $i;
    }

    function save(){
        $schoolid = trim($this->input->post('schoolid'));
     	$programid = trim($this->input->post('programid'));
     	$schlevelid = trim($this->input->post('schlevelid'));
     	$yearid = trim($this->input->post('yearid'));
     	$examtypeid = trim($this->input->post('examtypeid'));

     	// sub data =======
     	$arr = $this->input->post('arr');
        $i = 0;

        if(COUNT($arr) > 0){
        	foreach ($arr as $row){
            	if($this->vaidate_upd($row['examid'], $schlevelid, $yearid, $row['grade_levelid'], $examtypeid) > 0){
            		$i = 2;
            		break;
            	}
            }

            if($i != 2){
	            foreach ($arr as $row){
	            	if($row['examid'] - 0 == 0){
		            	if($examtypeid - 0 == 1){
	                		$data1 = array('schoolid' => $schoolid,// main ======
					                        'schoolid' => $schoolid,
					                        'programid' => $programid,
					                        'schlevelid' => $schlevelid,
					                        'yearid' => $yearid,
					                        'examtypeid' => $examtypeid,
	                        				'grade_levelid' => $row['grade_levelid'],// sub =====
	                        				'start_exam' => $this->green->formatSQLDate($row['start_exam']),
	                                        'end_exam' => $this->green->formatSQLDate($row['end_exam']),
	                                        'start_entry_score' => $this->green->formatSQLDate($row['start_entry_score']),
	                                        'end_entry_score' => $this->green->formatSQLDate($row['end_entry_score']) 
	                                    );
	                		$i = $this->db->insert('sch_student_exam_schedule', $data1);

		            	}else if(($row['start_exam'] != '') && $examtypeid != 1){
		            		$data1 = array('schoolid' => $schoolid,// main ======
					                        'schoolid' => $schoolid,
					                        'programid' => $programid,
					                        'schlevelid' => $schlevelid,
					                        'yearid' => $yearid,
					                        'examtypeid' => $examtypeid,
	                        				'grade_levelid' => $row['grade_levelid'],// sub =====
	                        				'start_exam' => $this->green->formatSQLDate($row['start_exam']),
	                                        'end_exam' => $this->green->formatSQLDate($row['end_exam']),
	                                        'start_entry_score' => $this->green->formatSQLDate($row['start_entry_score']),
	                                        'end_entry_score' => $this->green->formatSQLDate($row['end_entry_score']) 
	                                    );
	                		$i = $this->db->insert('sch_student_exam_schedule', $data1);
		            	}
	            	}

	            } 
            }                   
        }
        return $i;
    }

    function edit($examid = ''){
        $row = $this->db->query("SELECT
										e.examid,
										e.schoolid,
										e.programid,
										e.schlevelid,
										e.yearid,
										e.grade_levelid,
										e.examtypeid,
										DATE_FORMAT(e.start_exam, '%d/%m/%Y') AS start_exam,
										DATE_FORMAT(e.end_exam, '%d/%m/%Y') AS end_exam,
										DATE_FORMAT(e.start_entry_score, '%d/%m/%Y') AS start_entry_score,
										DATE_FORMAT(e.end_entry_score, '%d/%m/%Y') AS end_entry_score
									FROM
										sch_student_exam_schedule AS e
									WHERE
										e.examid = '{$examid}' ")->row();
        return json_encode($row);
    }

    function edit_grade($schlevelid_list){
        $opt = '';		
		foreach($this->g->getgradelevels($schlevelid_list) as $row_grade){
			$opt .= '<option value="'.$row_grade->grade_levelid.'" '.($row_grade->grade_levelid == $this->input->post('grade_levelid_list') ? 'selected="selected"' : '').'>'.$row_grade->grade_level.'</option>';
		}

		$arr = array('opt' => $opt);
        return json_encode($arr);
    }

    function edit_year($schlevelid_list){
        $opt = '';		
		foreach($this->y->getschoolyear('', '', $schlevelid_list) as $row_y){
			$opt .= '<option value="'.$row_y->yearid.'" '.($row_y->yearid == $this->input->post('yearid_list') ? 'selected="selected"' : '').'>'.$row_y->sch_year.'</option>';
		}

		$arr = array('opt' => $opt);
        return json_encode($arr);
    }

    function delete($examid = ''){
    	$i = 0;
		$i = $this->db->delete('sch_student_exam_schedule', array('examid' => $examid));
        return json_encode($i);
	}

	function grid(){
		$offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;
		$grade_list_search = trim($this->input->post('grade_list_search', TRUE));
		$schoolid_list_search = trim($this->input->post('schoolid_list_search', TRUE));
		$schlevelid_list_search = trim($this->input->post('schlevelid_list_search', TRUE));
		$yearid_list_search = trim($this->input->post('yearid_list_search', TRUE));		
		$examtypeid_list_search = trim($this->input->post('examtypeid_list_search', TRUE));

		$where = '';

		if($grade_list_search != ''){
			$where .= "AND e.grade_levelid = '{$grade_list_search}' ";
		}
		if($schoolid_list_search != ''){
			$where .= "AND e.schoolid = '{$schoolid_list_search}' ";
		}
		if($schlevelid_list_search != ''){
			$where .= "AND e.schlevelid = '{$schlevelid_list_search}' ";
		}
		if($yearid_list_search != ''){
			$where .= "AND e.yearid = '{$yearid_list_search}' ";
		}
		if($examtypeid_list_search != ''){
			$where .= "AND e.examtypeid = '{$examtypeid_list_search}' ";
		}

		// count ==========
		$qr_c = $this->db->query("SELECT COUNT(e.examid) AS c FROM sch_student_exam_schedule AS e
									WHERE 1=1 {$where} ")->row()->c - 0;

		$totalRecord = $qr_c - 0;		
		$totalPage = ceil($totalRecord/$limit);

		// result =======
		$qr = $this->db->query("SELECT
										e.examid,
										e.schoolid,
										e.programid,
										e.schlevelid,
										e.yearid,
										e.grade_levelid,
										e.examtypeid,
										DATE_FORMAT(e.start_exam, '%d/%m/%Y') AS start_exam,
										DATE_FORMAT(e.end_exam, '%d/%m/%Y') AS end_exam,
										DATE_FORMAT(
											e.start_entry_score,
											'%d/%m/%Y'
										) AS start_entry_score,
										DATE_FORMAT(
											e.end_entry_score,
											'%d/%m/%Y'
										) AS end_entry_score,
										g.grade_level,
										info.`name`,
										l.sch_level,
										y.sch_year,
										et.exam_test
									FROM
										sch_student_exam_schedule AS e
									LEFT JOIN sch_grade_level AS g ON e.grade_levelid = g.grade_levelid
									LEFT JOIN sch_school_infor AS info ON e.schoolid = info.schoolid
									LEFT JOIN sch_school_level AS l ON e.schlevelid = l.schlevelid
									LEFT JOIN sch_school_year AS y ON e.yearid = y.yearid
									LEFT JOIN sch_student_examtype AS et ON e.examtypeid = et.examtypeid
									WHERE 1 = 1 {$where}
									ORDER BY
										g.grade_level ASC
									LIMIT {$offset},
									 {$limit} ");

		$i = 1;
		$tr = '';

		if($qr->num_rows() > 0){			
			foreach($qr->result() as $row){

				// school option ========
				$opt1 = '';
				foreach ($this->info->getschinfor() as $row_s) {
					$opt1 .= '<option value="'.$row_s->schoolid.'">'.$row_s->name.'</option>';
				}

				// school level option =======
				$opt2 = '';
				foreach ($this->level->getsch_level() as $row_schlevel) {
					$opt2 .= '<option value="'.$row_schlevel->schlevelid.'">'.$row_schlevel->sch_level.'</option>';
				}

				// exam type =======
				$q_exam_type = $this->db->query("SELECT
														t.examtypeid,
														t.exam_test
													FROM
														sch_student_examtype AS t
													ORDER BY
														t.exam_test ASC ");
				$opt3 = '';
				if($q_exam_type->num_rows() > 0){
					foreach ($q_exam_type->result() as $row_t) {
						$opt3 .= '<option value="'.$row_t->examtypeid.'">'.$row_t->exam_test.'</option>';
					}
				}
				

				$tr .= '<tr>'.
							'<td style="height: 45px;text-align: center;">'.($i++ + $offset).'</td>'.
							'<td style="text-align: center;">
								<select class="form-control input-sm grade_levelid_list remove_tag" name="grade_levelid_list[]" style="display: none;"></select>
								<span class="sp_grade_levelid_list">'.$row->grade_level.'</span>
							</td>'.
							'<td>
								<select class="form-control input-sm schoolid_list remove_tag" name="schoolid_list[]" style="display: none;">'.$opt1.'</select>
								<span class="sp_schoolid_list">'.$row->name.'</span>
							</td>'.
							'<td>
								<select class="form-control input-sm schlevelid_list remove_tag" name="schlevelid_list[]" style="display: none;">'.$opt2.'</select>
								<span class="sp_schlevelid_list">'.$row->sch_level.'</span>
							</td>'.
							'<td>
								<select class="form-control input-sm yearid_list remove_tag" name="yearid_list[]" style="display: none;"></select>
								<span class="sp_yearid_list">'.$row->sch_year.'</span>
							</td>'.
							'<td style="">
								<select class="form-control input-sm examtypeid_list remove_tag" name="examtypeid_list[]" style="display: none;">'.$opt3.'</select>
								<span class="sp_examtypeid_list">'.$row->exam_test.'</span>
							</td>'.							
							'<td>
								<input type="text" class="form-control input-sm start_exam_list remove_tag" name="start_exam_list[]" placeholder="dd/mm/yyyy" style="display: none;">
								<span class="sp_start_exam_list">'.($row->start_exam != '00/00/0000' && $row->start_exam != null ? $row->start_exam : '').'</span>
							</td>'.
							'<td>
								<input type="text" class="form-control input-sm end_exam_list remove_tag" name="end_exam_list[]" placeholder="dd/mm/yyyy" style="display: none;">
								<span class="sp_end_exam_list">'.($row->end_exam != '00/00/0000' && $row->end_exam != null ? $row->end_exam : '').'</span>
							</td>'.
							'<td>
	                            <input type="text" class="form-control input-sm start_entry_score_list remove_tag" name="start_entry_score_list[]" placeholder="dd/mm/yyyy" style="display: none;">
								<span class="sp_start_entry_score_list">'.($row->start_entry_score != '00/00/0000' && $row->start_entry_score != null ? $row->start_entry_score : '').'</span>
							</td>'.
							'<td>
	                            <input type="text" class="form-control input-sm end_entry_score_list remove_tag" name="end_entry_score_list[]" placeholder="dd/mm/yyyy" style="display: none;">
								<span class="sp_end_entry_score_list">'.($row->end_entry_score != '00/00/0000' && $row->end_entry_score != null ? $row->end_entry_score : '').'</span>
							</td>'.
							'<td style="text-align: center;width: 7%;" class="remove_tag">
								<a href="javascript:;" class="edit" data-show_hide="0" style="text-decoration: none;" data-id="'.$row->examid.'">
									<img src="'.base_url('assets/images/icons/edit.png').'" class="img_eidt">
									<button type="button" class="btn btn-primary btn-sm btn_edit" style="display: none;">Update</button>
								</a>
							</td>'.
							'<td style="text-align: center;width: 3%;" class="remove_tag">
								<a href="javascript:;" class="delete" data-id="'.$row->examid.'">
									<img src="'.base_url('assets/images/icons/delete.png').'">
								</a>
							</td>'.
						'</tr>';
			}
							
		}else{
			$tr .= '<tr><td colspan="11" style="font-weight: bold;text-align: center;">'."We din't find data".'</tr>';
		}

		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage); 
        return json_encode($arr);		
	}

	// get school level =====
	function get_schlevel($programid){
        $qr_schlevel = $this->db->query("SELECT DISTINCT
                                                l.schlevelid,
                                                l.sch_level
                                            FROM
                                                sch_school_level AS l
                                            WHERE
                                                l.programid = '{$programid}'
                                            ORDER BY
                                                l.sch_level ASC ")->result();        
        
        $arr = array('schlevel' => $qr_schlevel);
        return json_encode($arr);
    }

    // get year =========
    function get_year($programid = '', $schlevelid = ''){
    	$opt = '';
    	// $opt .= '<option value=""></option>';
		foreach ($this->y->getschoolyear('', $programid, $schlevelid) as $row) {
			$opt .= '<option value="'.$row->yearid.'">'.$row->sch_year.'</option>';
		}
        return json_encode(array('opt' => $opt));
    }

    // get program =========
    function get_program($schlevelid){
        $qr_program = $this->db->query("SELECT DISTINCT
												p.programid,
												p.program
											FROM
												sch_school_program AS p
											RIGHT JOIN sch_school_level AS l ON l.programid = p.programid
											WHERE
												l.schlevelid = '{$schlevelid}'
											ORDER BY
												p.program ASC ");		
		$opt = '';
        if($qr_program->num_rows() > 0){
        	foreach ($qr_program->result() as $row) {
        		$opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
        	}
        }
        return json_encode(array('opt' => $opt));
    }

    // get grade =========
    function get_grade($schlevelid){
        $opt = '';
        // $opt .= '<option value=""></option>';		
		foreach($this->g->getgradelevels($schlevelid) as $row_grade){
			$opt .= '<option value="'.$row_grade->grade_levelid.'">'.$row_grade->grade_level.'</option>';
		}
        return json_encode(array('opt' => $opt));
    }	
	
}