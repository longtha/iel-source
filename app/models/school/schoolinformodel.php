<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class schoolinformodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url('school/schoolinfor/index?');
			$config['page_query_string'] = TRUE;
			// $config['per_page']=100;
			// $config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$config['total_rows']=$this->db->get('sch_school_infor')->num_rows();
			$this->pagination->initialize($config);
			//$this->db->where('is_active',1);
			//return $this->db->get('sch_subject', $config['per_page'],$page)->result();
			
			//------ Join ------
			$this->db->select("*");
			$this->db->from("sch_school_infor");
			//$this->db->join("sch_school_infor si","sl.schoolid=si.schoolid","inner");
			$this->db->order_by("schoolid", "desc");
			//$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			
			return $query->result();		
		}

		function getschinfor()
		{
			//$this->db->where('status',1);
			$query=$this->db->get('sch_school_infor');
			return $query->result();
		}		

		function getschinfor_row($sch_infor_id)
		{
			$this->db->where('schoolid',$sch_infor_id);
			$query=$this->db->get('sch_school_infor');
			return $query->row();
		}

		function getvalidate($name)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_school_infor');
		    $this->db->where('name',$name);
		    $this->db->where('is_active',1);

		    return  $this->db->count_all_results();
		}

		function getvalidateup($name, $sch_infor_id)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_school_infor');
		    $this->db->where('name',$name);
			$this->db->where_not_in('schoolid',$sch_infor_id);
			$this->db->where('is_active',1);
			return  $this->db->count_all_results();
		}

		function searchsch_infors($name,$showall)
		{
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url("school/schoolinfor/searchs?l=$name");
			$config['page_query_string'] = TRUE;
			$config['per_page']=5;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$this->db->like('name',$name);
			//if($showall>0)
				//$this->db->where('is_active',$is_active);
			$config['total_rows']=$this->db->get('sch_school_infor')->num_rows();
			$this->pagination->initialize($config);
			$this->db->select("*");
			$this->db->from("sch_school_infor");
			$this->db->like('name',$name);
			if($showall==0)
				$this->db->where('is_active',1);
			$this->db->order_by("schoolid", "desc");
			$query=$this->db->get();
			return $query->result();		
		}
	}
?>