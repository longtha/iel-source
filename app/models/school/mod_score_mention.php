<?php
	class mod_score_mention extends CI_Model{
		function getscoremention(){
				$page=0;
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				$m='';
				$p='';
				if(isset($_GET['m'])){
		        	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }	

				$sql="SELECT sl.sch_level,sm.* FROM sch_score_mention AS sm 
						INNER JOIN sch_school_level AS sl ON sm.schlevelid=sl.schlevelid
						ORDER BY sm.menid DESC";	
				$config['base_url'] =site_url()."/school/score_mention/index?pg=1&m=$m&p=$p";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =10;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				
				return $this->db->query($sql)->result();
		}
		function search($schlevelid,$s_num,$m,$p){
				$page=0;
				$where='';
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				if($schlevelid!='' || $schlevelid!=0)
					$where.=" AND sl.schlevelid LIKE '%".$schlevelid."%'";
					$sql="SELECT sl.sch_level,sm.* FROM sch_score_mention AS sm 
							INNER JOIN sch_school_level AS sl ON sm.schlevelid=sl.schlevelid
							WHERE 1=1 {$where} ORDER BY sm.menid DESC";
					$config['base_url'] = site_url()."/school/score_mention/search?pg=1&m=$m&p=$p&schlevelid=$schlevelid&s_num=$s_num";	
					$config['total_rows'] = $this->green->getTotalRow($sql);
					$config['per_page'] =$s_num;
					$config['num_link']=5;
					$config['page_query_string'] = TRUE;
					$config['full_tag_open'] = '<li>';
					$config['full_tag_close'] = '</li>';
					$config['cur_tag_open'] = '<a><u>';
					$config['cur_tag_close'] = '</u></a>';
					$this->pagination->initialize($config);
					$limi=" limit ".$config['per_page'];
					if($page>0){
							$limi=" limit ".$page.",".$config['per_page'];
						}		
					$sql.=" {$limi}";
					return $this->db->query($sql)->result();
		}
	}