<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class classmodel extends CI_Model{

		function __construct()
		{
			parent::__construct();
		}

		function getglevellabel($glevelid,$glabelid,$schoolid)
		{

		    return $this->db->query("SELECT
										COUNT(*) as count
									FROM
										(`sch_class`)
									WHERE
										`grade_labelid` = '$glabelid'
									AND `grade_levelid` = '$glevelid'
									AND `schoolid` = '$schoolid'")->row()->count;
											}

		function getschool()
		{
			$this->db->where('is_active',1);
			$query=$this->db->get('sch_school_infor');
			return $query->result();
		}

		function getgradelevel($schlevelid="")
		{
            if($schlevelid!=""){
                $this->db->where("schlevelid",$schlevelid);
            }
			$query=$this->db->get('sch_grade_level');
			return $query->result();
		}
		function getgradelabel($schlevelid="")
		{
            if($schlevelid!=""){
                $this->db->where("schlevelid",$schlevelid);
            }
			$query=$this->db->get('sch_grade_label');
			return $query->result();
		}

		function getgradelabels($grade_labelid)
		{
			$this->db->where('schoolid', $this->session->userdata('schoolid'))
					->LIKE('grade_label',$grade_labelid);

			$query=$this->db->get('sch_grade_label');
			return $query->result();
		}

		function getlabelrow($glableid)
		{	$this->db->select('grade_label');
			$this->db->where('grade_labelid',$glableid);
			$query=$this->db->get('sch_grade_label');
			return $query->result();
		}

		function getschoolrow($schoolid)
		{
			$this->db->where('schoolid',$schoolid);
			$query=$this->db->get('sch_school_infor');
			return $query->row();
		}

		function getvalidate($schoolid,$schlevelid,$class_name)
		{

			$this->db->select('count(*)');
		    $this->db->from('sch_class');
		    $this->db->where('schoolid',$schoolid);
            $this->db->where('schlevelid',$schlevelid);
		    $this->db->where('class_name',$class_name);
		    return $this->db->count_all_results();
		}

		function getvalidateup($schoolid, $subjectid, $year,$v_school,$v_sub,$v_year)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_class');
		    $this->db->where('schoolid',$schoolid);
		    $this->db->where('year',$year);
		    $this->db->where('subjectid',$subjectid);
			return  $this->db->count_all_results();
		}

		function searchclass($glabel)
		{
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url("school/classes/searchs?l=$glabel");
			$config['page_query_string'] = TRUE;

			$config['full_tag_open'] ='<li>';
			$config['full_tag_close'] ='</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';


			$this->db->select("*");
			$this->db->from("v_sch_class c");
			$this->db->where('c.is_active',1);
			$query=$this->db->get();
			return $query->result();
		}

		function getschoolyear()
		{
			$query=$this->db->get('sch_school_year');
			return $query->result();
		}

		function getschoolyearrow($yearid)
		{
			$this->db->where('yearid',$yearid);
			$query=$this->db->get('sch_school_year');
			return $query->row();
		}

		public function getclass($levelid="",$labelid="",$school="",$inactive="")
		{
			$this->db->select('count(*)');
			$this->db->from('sch_class');
			$this->db->where('grade_levelid',$levelid);
			$this->db->where('grade_labelid',$labelid);
			$this->db->where('schoolid',$school);
			if($inactive ==''){
				$this->db->where('is_active',1);
			}

			return $this->db->count_all_results();
		}

		public function getclassname($levelid="",$labelid="",$schoolid="")
		{
			$this->db->select('class_name');
			$this->db->where('grade_levelid',$levelid);
			$this->db->where('grade_labelid',$labelid);
			$this->db->where('schoolid',$schoolid);
			//$this->db->where('is_active',1);
			$query=$this->db->get('sch_class');

			return $query->row();
		}

		public function getschlevel($grade_levelid)
		{
			$this->db->select('schlevelid');
			$this->db->where('grade_levelid',$grade_levelid);
			$query=$this->db->get('sch_grade_level');

			return $query->row();
		}

        function updateclass($classid,$classname,$idseis_pt){
            $clsinfo=$this->getclassrow($classid);
            $count=$this->getvalidate($clsinfo->schoolid,$clsinfo->schlevelid,$classname);
            if($classid!=""){
			//if($classid!="" && $count==0){
                $data=array(
                    'class_name'=>$classname,
                    'is_pt'=>$idseis_pt,
                    'modified_by'=>$this->session->userdata('userid'),
                    'modified_date'=>date('Y-m-d H:i:s'),
                );
				// print_r($data);
                $this->db->where('classid',$classid);
                $this->db->update("sch_class",$data);
                return true;
            }else{
                return false;
            }
        }
        function search($start=0){

            $s_sclevelid=(isset($_POST['s_sclevelid']))?$_POST['s_sclevelid']:"";

            $WHERE="";
            if($s_sclevelid!=""){
                $WHERE.=" AND schlevelid = '".$s_sclevelid."'";
            }
            $sql="SELECT * FROM v_sch_class WHERE 1=1 {$WHERE} ORDER BY schoolid,programid,schlevelid,grade_levelid,grade_labelid";
            $total_row=$this->green->getValue("select count(*) as numrow FROM ($sql) as cc ");
            $paging=$this->green->ajax_pagination($total_row,site_url("school/classes/search"),50);

            if(isset($start) && $start>0){
                $paging['start']=($start-1)*$paging['limit'];
            }
            $data=$this->green->getTable("$sql limit {$paging['start']}, {$paging['limit']}");
            $arrJson['paging']=$paging;

            $arrJson['datas']=$data;
            header("Content-type:text/x-json");
            echo json_encode($arrJson);
            exit();
        }
        function getclassrow($classid){
            if($classid!=""){
                $this->db->where("classid",$classid);
            }
            return $this->db->get("sch_class")->row();
        }

       function allclass($schlevelid="",$gradlevel=""){
       		$where="";
            if($schlevelid!=""){
                $where.=" AND schlevelid='".$schlevelid."'";
            }
            if($gradlevel!=""){
                $where.=" AND grade_levelid='".$gradlevel."'";
            }
            $where_stu_log="";
            if($this->session->userdata('match_con_posid')=='stu'){
            	$where_stu_log.=" AND classid IN(SELECT DISTINCT classid FROM v_student_profile WHERE 1=1 AND studentid='".($this->session->userdata('emp_id'))."')";
            }
            $sql="SELECT
					*
				FROM
					sch_class
				WHERE 1=1 {$where} {$where_stu_log}
				ORDER BY
					`schlevelid` ASC,
					`grade_levelid` ASC,
					`grade_labelid` ASC
			";
            
            return $this->db->query($sql)->result();

        }

        function callClass($school_level_id, $grade_level_id){
             $this->db->order_by("schlevelid", "ASC");
             $this->db->order_by("grade_levelid", "ASC");
             $this->db->order_by("grade_labelid", "ASC");

             // $this->db->order_by("class_name", "ASC");
             return $this->db->query("SELECT classid, class_name FROM sch_class WHERE schlevelid = $school_level_id AND grade_levelid = $grade_level_id")->result();
         }

        function deleteclass($classid){
            $arrJson['del']=0;
            $checkstate=$this->checkClass($classid);
            if($checkstate>0){
                $arrJson['del']=$checkstate;
            }else{
                $this->db->delete('sch_class', array('classid' => $classid));
                $arrJson['del']=9;
            }
            return $arrJson;
        }
        function checkClass($classid){
            $result=0;
            if($classid!=""){
                /*check in enrollment*/
                $this->db->where("classid",$classid);
                $cls_ent=$this->db->get("sch_student_enrollment")->row();
                if(isset($cls_ent->studentid)){
                    $result=1;
                }
                /*check in schedule*/
            }
            return $result;
        }

        public function getClassData($levelid="",$inactive="")
		{
			$this->db->where('grade_levelid',$levelid);
			if($inactive ==''){
				$this->db->where('is_active',1);
			}
			return $this->db->get("sch_class")->result();
		}

	}
