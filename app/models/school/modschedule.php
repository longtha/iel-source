<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Modschedule extends CI_Model
{
	public function getdays(){
		return $this->db->where('enabled','1')->get('sch_day')->result();
	}
	function getmorningtimes($time){
		return $this->db->query("SELECT * 
								FROM (`sch_time`) 
								WHERE `am_pm` = '$time' 
								AND for_tran_type='1'
								ORDER BY `from_time` ASC									
								")->result();
	}
	public function setClassInfo($classid='')
	{
		return $this->db->where('classid',$classid)->get('sch_class')->row();
	}
	public function setClassListview($sy='',$slid='')
	{
		$where="";
		if($slid!=''){
			$where.=" AND sch_class.schlevelid ='".$slid."'";
		}
		if($sy!=''){
			$where.=" AND sch.`year` = $sy";
		}
		$sqlStr = "SELECT DISTINCT
								sch_class.classid,
								sch_class.class_name,
								sch.`year`,
								sch.title,
								sch.transno
							FROM
								sch_time_table sch
							INNER JOIN sch_class ON sch.classid = sch_class.classid
							WHERE
								1 = 1  {$where} ORDER BY sch_class.classid
					";
		return $this->db->query($sqlStr)->result();
	}
	function getsubject($gradelevelid,$year=''){
		if($year=='')
			$year=$this->session->userdata('year');
		$school=$this->session->userdata('schoolid');
		return $this->db->query("SELECT DISTINCT
										lsd.subjectid
									FROM sch_level_subject_detail lsd
									INNER JOIN sch_subject s
									ON(lsd.subjectid=s.subjectid)
									INNER JOIN sch_subject_type st
									ON(s.subj_type_id=st.subj_type_id)
									WHERE lsd.grade_levelid='".$gradelevelid."'
									AND lsd.year='".$year."'
									AND lsd.schoolid='".$school."'
									AND s.is_trimester_sub='0'")->result();
	}
	function ishavesubject($dayid,$classid,$subjectid,$timeid,$yearid,$transno){
		$schoolid=$this->session->userdata('schoolid');
		return $this->db->query("SELECT COUNT(time_tableid) as count,teacherid
										FROM sch_time_table
										WHERE dayid='".$dayid."'
										AND classid='".$classid."'
										AND subjectid='".$subjectid."'
										AND timeid='".$timeid."'
										AND year='".$yearid."'
										AND schoolid='".$schoolid."'
										AND transno='".$transno."'
										GROUP BY teacherid
								")->row();
	}
	function getsubjectrow($subjectid){
		$sql= "SELECT
					st.subject_type,
					s.`subject`
				FROM
					sch_subject s
				INNER JOIN sch_subject_type st ON (
					s.subj_type_id = st.subj_type_id
				)
				WHERE
					1 = 1 AND s.subjectid='".$subjectid."'
			";
		return $this->db->query($sql)->row();
	}
	public function setSchoolName(){
		return $this->green->getValue("SELECT schi.name FROM sch_school_infor schi WHERE schoolid = '".$this->session->userdata('schoolid')."'");
	}
	public function getBlockTimeTable($classinf,$classid,$yearid,$transno){
		$_tr="<table class='table table-bordered table-striped' id='addcss'>";
		$_tr.="<thead>";
			$_tr.="<th style='text-align:center !important;'>Subject/Days</th>";
			foreach ($this->getdays() as $day) {
				$_tr.="<th style='text-align:center !important;'>".$day->dayname."</th>";
			}
		$_tr.="</thead>";
		$_tr.="<tbody>";
			$_tr.="<tr>";
				$_tr.="<th colspan='8' style='background:#EEEEEE; text-align:center !important;'>MORNING TIMES</th>";
			$_tr.="</tr>";
			$_tr.=$this->setBlockTimeTable($this->getmorningtimes('AM'),$classinf,$classid,$yearid,$transno);
			$_tr.="<tr>";
				$_tr.="<th colspan='8' style='background:#EEEEEE; text-align:center !important;'>EVENING TIMES</th>";
			$_tr.="</tr>";
			$_tr.=$this->setBlockTimeTable($this->getmorningtimes('PM'),$classinf,$classid,$yearid,$transno);
		$_tr.="</tbody>";
		$_tr.="</table>";
		return $_tr;
	}
	public function setBlockTimeTable($setTime=array(),$classinf,$classid,$yearid,$transno){
		$_tr="";
		foreach ($setTime as $m) {
			$_tr.="<tr>";
					$_tr.="<td style='vertical-align: middle !important'>".$m->from_time.'-'.$m->to_time."</td>";
			foreach ($this->getdays() as $day) {
				$ishavclass='';
				if(isset($classinf)){
					foreach ($this->getsubject($classinf->grade_levelid,$yearid) as $s) {
						$ishavesub=$this->ishavesubject($day->dayid,$classid,$s->subjectid,$m->timeid,$yearid,$transno);
						if(count($ishavesub)>0){
							$subrow=$this->getsubjectrow($s->subjectid);
							$ishavclass='yes';
							$_tr.="<td width='150' height='50' align='center'  style='vertical-align: middle !important'>";
								$_tr.="<span id='subj'>".$subrow->subject."</span><br/><span id='sub_type'>(".$subrow->subject_type.")</span><br/>";
							$_tr.="</td>";
						}	
					}
					if($ishavclass=='')
						$_tr.="<td width='150' height='50' align='center'  style='vertical-align: middle !important;background: #CCCCCC !important'>X</td>";
				}			
			}
			$_tr.="</tr>";
		}
		return $_tr;
	}
	public function scheduleHeader($school_name='',$class_name='',$title='',$year=''){
		$tr="<div style='width:300px; float:right;'>
				<img src='".base_url('assets/images/logo/logo.png')."' style='width:250px;  height:60px;' />
			</div>";
		$tr.="<div style='float:left; '>
				<div style=' font-weight:bold; text-transform: uppercase;'>".$school_name."</div>
				<div style='text-align:center !important; font-weight:bold;'></div> 
			</div>";
		$tr.="<p style='clear:both'></p>";
		$tr.="<table align='center' style='width:100%' border='0'>
				<thead>
					<tr>
						<th valign='top' align='center' style='width:80%'><h4 align='center'>STUDENT SCHEDULE</h4></th>
					</tr>
				</thead>
			</table>";
		$tr.="<table align='center' style='width:100%' border='0'>
				<thead>
					<tr>
						<th>Schedule for Class :".$class_name."</th>
					</tr>
					<tr>
						<th style='text-align:left !important; font-weight:bold;'>Title :".$title."</th>
					</tr>
					<tr>
						<th style='text-align:left !important; font-weight:bold;'>Academy year :".$this->green->GetYear($year)."</th>
					</tr>
					<tr>
						<th style='text-align:left !important; font-weight:bold;'>&nbsp;</th>
					</tr>
				</thead>
			</table>";
		return $tr;
	}
}
