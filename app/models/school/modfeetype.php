<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modfeetype extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    function getfeetype($feetypeid){
        $this->db->where("feetypeid",$feetypeid);
        return $this->db->get("sch_school_feetype")->row() ;
    }
    function getfeetypes(){
        return $this->db->get("sch_school_feetype")->result() ;
    }

    function getStudyPeriod($programid="",$yearid="",$schoolid="",$schlevelid="",$payment_type="",$rangelevelid=""){

        if($programid!=""){
            $this->db->where("programid",$programid);
        }
        if($yearid!=""){
            $this->db->where("yearid",$yearid);
        }

        if($schoolid!=""){
            $this->db->where("schoolid",$schoolid);
        }
        if($schlevelid!=""){
            $this->db->where("schlevelid",$schlevelid);
        }
        if($payment_type!=""){
            $this->db->where("payment_type",$payment_type);
        }
        if($rangelevelid!=""){
            $this->db->where("rangelevelid",$rangelevelid);
        }
        return $this->db->get("v_study_peroid")->result() ;
    }


}