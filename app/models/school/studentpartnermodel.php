<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class studentpartnermodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m='';
			$p='';
			if(isset($_GET['m'])){
	        	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			    
			$config['base_url']=site_url("school/studentpartner/index?m=$m&p=$p");
			$config['per_page']=10;
			$config['page_query_string'] = TRUE;
			
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';

			$config['total_rows']=$this->db->get('sch_stud_partner')->num_rows();
			$this->pagination->initialize($config);
			
			//------ Join ------
			$this->db->select("*");
			$this->db->from("sch_stud_partner");
			$this->db->order_by("partnerid", "desc");
			$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			
			return $query->result();		
		}	
		function getparnerrow($partnerid)
		{
			$this->db->where('partnerid',$partnerid);
			$query=$this->db->get('sch_stud_partner');
			return $query->row();
		}
		function getvalidate($cname)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_stud_partner');
		    $this->db->where('company_name',$cname);

		    return  $this->db->count_all_results();
		}
		function getvalidateup($cname, $spid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_stud_partner');
		    $this->db->where('company_name',$cname);
			$this->db->where_not_in('partnerid',$spid);
			
			return  $this->db->count_all_results();
		}
		function searchs($cname)
		{
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];

			$m='';
			$p='';
			if(isset($_GET['m'])){
	        	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			$config['base_url']=site_url("school/studentpartner/searchs?c=$cname&m=$m&p=$p");
			$config['page_query_string'] = TRUE;
			$config['per_page']=5;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$this->db->select("*");
			$this->db->from("sch_stud_partner");
			$this->db->order_by("partnerid", "desc");
			$this->db->like('company_name',$cname);
			$config['total_rows']=$this->db->get()->num_rows();
			$this->pagination->initialize($config);

			$this->db->select("*");
			$this->db->from("sch_stud_partner");
			$this->db->order_by("partnerid", "desc");
			$this->db->like('company_name',$cname);
			
			$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			return $query->result();		
		}
	}
?>