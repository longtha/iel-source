<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class timemodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url('school/time/index?');
			$config['page_query_string'] = TRUE;
			// $config['per_page']=100;
			// $config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><span>';
			$config['cur_tag_close'] = '</span></a>';
			$config['total_rows']=$this->db->get('sch_time')->num_rows();
			$this->pagination->initialize($config);
			//$this->db->where('is_active',1);
			//return $this->db->get('sch_subject', $config['per_page'],$page)->result();
			
			//------ Join ------
			$this->db->select("*");
			$this->db->from("sch_time t");
			$this->db->join("sch_school_infor s","t.schoolid=s.schoolid","inner");
			$this->db->order_by("t.am_pm", "ASC");
            $this->db->order_by("t.from_time", "ASC");
			//$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			
			return $query->result();		
		}

		// function getschinfor()
		// {
		// 	$query=$this->db->get('sch_grade_label');
		// 	return $query->result();
		// }	

		function getschool()
		{
			$this->db->where('is_active',1);
			$query=$this->db->get('sch_school_infor');
			return $query->result();
		}	

		function gettimerow($time_id)
		{
			$this->db->where('timeid',$time_id);
			$query=$this->db->get('sch_time');
			return $query->row();
			echo $query;
		}

		function getvalidate($from_time,$to_time,$tran_type,$schoolid,$ampm)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_time');
		    $this->db->where('from_time',$from_time);
		    $this->db->where('to_time',$to_time);
		    $this->db->where('for_tran_type',$tran_type);
		    $this->db->where('schoolid',$schoolid);
		    $this->db->where('am_pm',$ampm);

		    return  $this->db->count_all_results();
		}

		function getvalidateup($timeid,$from_time,$to_time,$tran_type,$schoolid,$ampm) 					   
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_time');
		    $this->db->where('from_time',$from_time);
		    $this->db->where('to_time',$to_time);
		    $this->db->where('for_tran_type',$tran_type);
		    $this->db->where('schoolid',$schoolid);
		    $this->db->where('am_pm',$ampm);
		    $this->db->where_not_in('timeid', $timeid);

			return  $this->db->count_all_results();
		}

		function searchtime($schoolid)
		{
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url("school/time/searchs?l=$schoolid");
			$config['page_query_string'] = TRUE;
			// $config['per_page']=5;
			// $config['num_link']=3;
			$config['full_tag_open'] ='<li>';
			$config['full_tag_close'] ='</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
						
			//$config['total_rows']=$this->db->get('sch_grade_level')->num_rows();
			//$this->pagination->initialize($config);
			$this->db->select("*");
			$this->db->from("sch_time t");
			$this->db->join("sch_school_infor s","t.schoolid=s.schoolid","inner");
			//$this->db->like('sl.schoolid',$schoolid);
			
			if($schoolid!=0)
				$this->db->where('s.schoolid',$schoolid);
				$this->db->order_by("t.timeid", "desc");
			//$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			return $query->result();		
		}
        function  times(){
            return $this->db->get("sch_time")->result();
        }
	}
