<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class subjecttypemodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}
		function  save($subjectid_update=''){

			$subjecttype=$this->input->post('subjecttype');
			$maintype=$this->input->post('maintype');
			$description=$this->input->post('description');
	        $orders=$this->input->post('orders');
	        $is_moeys=$this->input->post('is_moeys');
			$report_display=$this->input->post('report_display');
	        $schoolid=$this->input->post('schoolid');
	        $schlevelid=$this->input->post('schlevelid');
	        $rangelevelid=$this->input->post('rangelevelid');
	        $is_cp=$this->input->post('is_cp');
	        $is_group_calc=$this->input->post('is_group_calc');
	        $is_group_calc_percent=$this->input->post('is_group_calc_percent');
	        $is_at_qty=$this->input->post('is_at_qty');	        
	        
	        if($subjectid_update ==""){
				$count=$this->getvalidate($subjecttype,$schoolid,$schlevelid);
			}else{
				$count=$this->getvalidateup($subjecttype,$subjectid_update);
			}		

			if ($count!=0){
				$save_res =0;
				$this->load->view('header');
				$data['exist']="<p style='color:red;'>Your data has already exist...!</p>";
				$data['query']=$this->subjecttypes->getpagination();
				$this->load->view('school/subjecttype/add',$data);
				$this->load->view('school/subjecttype/view',$data);
				$this->load->view('footer');
			}else{
				$data=array(
			  		'subject_type'=>$subjecttype,
			  		'main_type'=>$maintype,
					'note'=>$description,
                    'orders'=>$orders,
                    'schoolid'=>$schoolid,
                    'schlevelid'=>$schlevelid,
                    'rangelevelid'=>$rangelevelid,
                    'is_report_display'=>$report_display,
                    'is_group_calc'=>$is_group_calc,
                    'is_group_calc_percent'=>$is_group_calc_percent,
                    'is_class_participation'=>$is_cp,
                    'is_attendance_qty'=>$is_at_qty
				);
				if($subjectid_update ==""){
					$this->db->insert('sch_subject_type',$data);
					$save_res =1;
				}else{
					$this->db->where('subj_type_id',$subjectid_update);
					$this->db->update('sch_subject_type',$data);
					$save_res =2;
				}
				
			}
			return  $save_res;
		}
		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m=$this->input->post('m');
		    $p=$this->input->post('p');
		   //$this->green->setActiveRole($this->input->post('roleid'));
		    if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }

			$config['base_url']=site_url('school/subjecttype/index?m=$m&p=$p');
			$config['page_query_string'] = TRUE;
			$config['per_page']=100;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$config['total_rows']=$this->db->get('sch_subject_type')->num_rows();
			$this->pagination->initialize($config);

			$this->db->order_by("subj_type_id", "asc");
            $this->db->order_by("orders", "asc");
			return $this->db->get('v_school_subjecttype',$config['per_page'],$page)->result();
		}

		function searchsubjecttype($subject_type,$main_type,$schoolid="",$schlevelid="",$yearid="",$rangelevelid="")
		{
		   $per_page='';	
		   if(isset($_GET['per_page']))
		   $per_page=$_GET['per_page'];
		   $config['base_url']=site_url("school/subjecttype/search?main_type=$main_type&subjecttype=$subject_type");
		   $config['per_page']=100;
		   $config['full_tag_open'] = '<li>';
		   $config['full_tag_close'] = '</li>';
		   $config['cur_tag_open'] = '<a><u>';
		   $config['cur_tag_close'] = '</u></a>';
		   $config['page_query_string']=TRUE;
		   $config['num_link']=3;

            if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
            if($yearid!=""){
                $this->db->where('yearid',$yearid);
            }
            if($rangelevelid!=""){
                $this->db->where('rangelevelid',$rangelevelid);
            }

		   $this->db->like('subject_type',$subject_type);
		   $this->db->like('main_type',$main_type);
		  
		   $config['total_rows']=$this->db->get('v_school_subjecttype')->num_rows();
		   $this->pagination->initialize($config);
		   $this->db->select('*');
		   $this->db->from('v_school_subjecttype');
		  //$this->db->join('sch_z_role r','u.roleid=r.roleid','inner');
		   $this->db->like('subject_type',$subject_type);
		   $this->db->like('main_type',$main_type);
		   $this->db->order_by("subj_type_id", "asc");
           $this->db->order_by("orders", "asc");
           $this->db->limit($config['per_page'],$per_page);
		   $query=$this->db->get();
		  return $query->result();
		 }

		function getsubjecttype($schoolid="",$schlevelid="",$yearid="",$rangelevelid="")
		{
			//$this->db->where('status',1);
            if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
            if($yearid!=""){
                $this->db->where('yearid',$yearid);
            }
            if($rangelevelid!=""){
                $this->db->where('rangelevelid',$rangelevelid);
            }

            $this->db->like('subject_type',$subject_type);
            $this->db->order_by("orders", "asc");
            $query=$this->db->get('v_school_subjecttype');
			return $query->result();
		}

		function getsubjecttyperow($subj_type_id)
		{
			$this->db->where('subj_type_id',$subj_type_id);
			$query=$this->db->get('v_school_subjecttype');
			return $query->row();	
		}

		function getvalidate($subject_type_name,$schoolid="",$schlevelid="")
		{
			$this->db->select('count(*)');
			$this->db->from('sch_subject_type');
			$this->db->where('subject_type',$subject_type_name);
            if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }            
			return  $this->db->count_all_results();
		}

		function getvalidateup($subject_type_name,$id,$schoolid="",$schlevelid="")
		{
			$this->db->select('count(*)');
			$this->db->from('sch_subject_type');
			$this->db->where('subject_type',$subject_type_name);

            if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
           
			$this->db->where_not_in('subj_type_id',$id);
			return  $this->db->count_all_results();
		}

		function ssearchsubjecttype($subject_type,$main_type)
		{
			$this->db->select('*');
			$this->db->from('sch_subject_type');
			$this->db->like('subject_type',$subject_type);
			$this->db->like('main_type',$main_type);
            $this->db->order_by("orders", "asc");
			$query=$this->db->get();
			 return $query->result();
		}
	}
