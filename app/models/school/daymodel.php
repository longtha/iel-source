<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class daymodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url('school/gradelabel/index?');
			$config['page_query_string'] = TRUE;
			// $config['per_page']=100;
			// $config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><span>';
			$config['cur_tag_close'] = '</span></a>';
			$config['total_rows']=$this->db->get('sch_day')->num_rows();
			$this->pagination->initialize($config);
			//$this->db->where('is_active',1);
			//return $this->db->get('sch_subject', $config['per_page'],$page)->result();
			
			//------ Join ------
			$this->db->select("*");
			$this->db->from("sch_day");
			// $this->db->join("sch_school_infor s","g.schoolid=s.schoolid","inner");
			// $this->db->order_by("g.grade_labelid", "desc");
			//$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			
			return $query->result();		
		}

		function getdayrow($dayid)
		{
			$this->db->where('dayid',$dayid);
			$query=$this->db->get('sch_day');
			return $query->row();
			echo $query;
		}

		function getvalidate($dayid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_day');
		    $this->db->where('dayid',$dayid);

		    return  $this->db->count_all_results();
		}

		function getvalidateup($dayid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_day');
		    $this->db->where('dayid', $dayid);
		    $this->db->where_not_in('dayid', $dayid);

			return  $this->db->count_all_results();
		}

		function searchday($dayid)
		{
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url("school/day/searchs?l=$schoolid");
			$config['page_query_string'] = TRUE;
			// $config['per_page']=5;
			// $config['num_link']=3;
			$config['full_tag_open'] ='<li>';
			$config['full_tag_close'] ='</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
						
			//$config['total_rows']=$this->db->get('sch_grade_level')->num_rows();
			//$this->pagination->initialize($config);
			$this->db->select("*");
			$this->db->from("sch_day");
			//$this->db->join("sch_school_infor si","sl.schoolid=si.schoolid","inner");
			//$this->db->like('sl.schoolid',$schoolid);
			
			// if($schoolid!=0)
			// 	$this->db->where('sl.schoolid',$schoolid);
			// 	$this->db->order_by("grade_labelid", "desc");
			//$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			return $query->result();		
		}
	}
?>