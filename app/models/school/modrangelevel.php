<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modrangelevel extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function rangelevel($rangelevelid){
        if($rangelevelid!=""){
            $this->db->where("rangelevelid",$rangelevelid);
        }
        return $this->db->get("sch_school_rangelevel")->row();
    }
    function rangelevels($schoolid="",$programid="",$schlevelid=""){
        if($schoolid!=""){
            $this->db->where("schoolid",$schoolid);
        }
        if($programid!=""){
            $this->db->where("programid",$programid);
        }
        if($schlevelid!=""){
            $this->db->where("schlevelid",$schlevelid);
        }
        return $this->db->get("sch_school_rangelevel")->result();
    }
    function getvalidate($schoolid,$schlevelid,$rangelevelname,$rangelevelid)
    {
        $this->db->select('count(*)');
        $this->db->from('sch_school_rangelevel');
        $this->db->where('schoolid',$schoolid);
        $this->db->where('schlevelid',$schlevelid);
        $this->db->where('rangelevelname',$rangelevelname);
        if($rangelevelid!=""){
            $this->db->where_not_in('rangelevelid',$rangelevelid);
        }

        return $this->db->count_all_results();
    }
    function save($rangelevelid=""){
        date_default_timezone_set("Asia/Bangkok");
        $schoolid=$this->input->post('schoolid');
        $schlevelid=$this->input->post('sclevelid');
        $rangelevelname=$this->input->post('rangelevelname');
        $classids=$this->input->post('classids');
        $rangelevelname=$this->input->post('rangelevelname');
        //print_r($classids);
        $daysrangeids=$this->input->post('daysrangeids');
        $shiftids=$this->input->post('shiftids');
        $timeids=$this->input->post('timeids');

        $count=0;
        $programid= $this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='".$schlevelid."'")->row()->programid;

        $count=$this->getvalidate($schoolid,$schlevelid,$rangelevelname,$rangelevelid);

        $data=array(
            'schoolid'=>$schoolid,
            'schlevelid'=>$schlevelid,
            'rangelevelname'=>$rangelevelname,
            'programid'=>$programid,
        );
        if($rangelevelname!="" && $count==0) {
            $sqltrack = array();
            if ($rangelevelid == "") {
                $sqltrack = array('created_by' => $this->session->userdata('userid'),
                    'created_date' => date('Y-m-d H:i:s')
                );
                $this->db->insert('sch_school_rangelevel', array_merge($data, $sqltrack));
                $rangelevelid=$this->db->insert_id();

            } else {
                $sqltrack = array(  'modified_by' => $this->session->userdata('userid'),
                    'modified_date' => date('Y-m-d H:i:s')
                );
                $where = array("rangelevelid"=>$rangelevelid);
                $this->db->update('sch_school_rangelevel', array_merge($data, $sqltrack), $where);

            }
            /*---------------- classes in range level -------------*/
            $this->db->delete('sch_school_rangelevelclass', array('rangelevelid' => $rangelevelid));
            if(isset($classids)){
                if(count($classids)>0) {
                    foreach($classids as $classid) {
                        $this->saveRangeClass($rangelevelid, $classid);
                    }
                }
            }
            /*---------------- time in range level -------------*/
            $this->db->delete('sch_school_rangelevtime', array('rangelevelid' => $rangelevelid));
            if(count($daysrangeids)>0){
                $i=0;
                foreach($daysrangeids as $daysrangeid){
                    $this->saveRangeTime($rangelevelid,$daysrangeid,$shiftids[$i],$timeids[$i]);
                    $i++;
                }
            }

        }
        $m='';
        $p='';
        if(isset($_GET['m'])){
            $m=$_GET['m'];
        }
        if(isset($_GET['p'])){
            $p=$_GET['p'];
        }
        redirect("school/rangeslevel/?save&m=".$m."&p=".$p);
    }

    function search($start=0){

        $s_sclevelid=(isset($_POST['s_sclevelid']))?$_POST['s_sclevelid']:"";

        $WHERE="";
        if($s_sclevelid!=""){
            $WHERE.=" AND schlevelid = '".$s_sclevelid."'";
        }
        $sql="SELECT * FROM v_rangelevel WHERE 1=1 {$WHERE} ORDER BY schoolid,programid,schlevelid";
        $total_row=$this->green->getValue("select count(*) as numrow FROM ($sql) as cc ");
        $paging=$this->green->ajax_pagination($total_row,site_url("school/rangeslevel/search"),50);

        if(isset($start) && $start>0){
            $paging['start']=($start-1)*$paging['limit'];
        }
        $data=$this->green->getTable("$sql limit {$paging['start']}, {$paging['limit']}");
        $arrJson['paging']=$paging;

        $arrJson['datas']=$data;
        header("Content-type:text/x-json");
        echo json_encode($arrJson);
        exit();
    }
    function delete($rangelevelid){
        $result['del']=0;
        if($rangelevelid!=""){
            $this->db->delete('sch_school_rangelevel', array('rangelevelid' => $rangelevelid));
            $this->db->delete('sch_school_rangelevelclass', array('rangelevelid' => $rangelevelid));
            if ($this->db->_error_message()) {
                $result['del'] = 'Error! ['.$this->db->_error_message().']';
            } else if (!$this->db->affected_rows()) {
                $result['del'] = 'Error! ID ['.$rangelevelid.'] not found';
            } else {
                $result['del'] = 'Success';
            }
        }
        echo json_encode($result);
        exit();
    }
    function edit($rangelevelid){
        $rngdata['rngclsrows']=array();
        $rngdata['rnglevelrow']=$this->rangelevel($rangelevelid);
        $rngdata['rngtimerows']=array();
        $arr_rngclass=$this->getRangeClass($rangelevelid);
        if(count($arr_rngclass)>0){
            $j=0;
            foreach($arr_rngclass as $cls){
                $rngdata['rngclsrows'][$j]=$cls->classid;
                $j++;
            }
        }
        $arr_rngtime=$this->getRangeTime($rangelevelid);
        if(count($arr_rngtime)>0){
            $j=0;
            foreach($arr_rngtime as $tms){
                foreach($tms as $field=>$val){
                    $rngdata['rngtimerows'][$j][$field]=$val;
                }
                $j++;
            }
        }

        return $rngdata;

    }
    function saveRangeClass($rangelevelid,$classid){
        $count=$this->db->query("SELECT COUNT(*) AS nums FROM sch_school_rangelevelclass
                                WHERE rangelevelid='".$rangelevelid."'
                                AND classid='".$classid."'")->row()->nums-0;
        if($count==0){
            $data=array("rangelevelid"=>$rangelevelid,"classid"=>$classid);
            $this->db->insert("sch_school_rangelevelclass",$data);
        }
    }

    function getRangeClass($rangelevelid){
        $this->db->where("rangelevelid",$rangelevelid);
        return $this->db->get("sch_school_rangelevelclass")->result();
    }
    function saveRangeTime($rangelevelid,$daysrangeid,$shiftid,$timeid){
        $count=$this->db->query("SELECT COUNT(*) AS nums FROM sch_school_rangelevtime
                                WHERE rangelevelid='".$rangelevelid."'
                                AND daysrangeid='".$daysrangeid."'
                                AND shiftid='".$shiftid."'
                                AND timeid='".$timeid."'")->row()->nums-0;
        if($count==0){
            $data=array("rangelevelid"=>$rangelevelid,
                        "daysrangeid"=>$daysrangeid,
                        "shiftid"=>$shiftid,
                        "timeid"=>$timeid
                        );
            $this->db->insert("sch_school_rangelevtime",$data);
        }
    }
    function getRangeTime($rangelevelid){
        $this->db->where("rangelevelid",$rangelevelid);
        return $this->db->get("v_school_rangelevtime")->result();
    }
    function getRangeByClass($classid=""){
        if($classid!=""){
            $this->db->where("classid",$classid);
        }
        return $this->db->get("sch_school_rangelevelclass")->row();
    }
}