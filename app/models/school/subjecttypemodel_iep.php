<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class subjecttypemodel_iep extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}
		function  save(){

			$subjectid_update = $this->input->post('hgroupid');
			$subjecttype   = $this->input->post('subjecttype');
			$maintype      = $this->input->post('maintype');
			$description   = $this->input->post('description');
	        $orders        = $this->input->post('orders');
	        $is_moeys      = $this->input->post('is_moeys');
			$report_display = $this->input->post('report_display');
	        $schoolid      = $this->input->post('schoolid');
	        $schlevelid    = $this->input->post('schlevelid');
	        $rangelevelid  = $this->input->post('rangelevelid');
	        $is_cp         = $this->input->post('is_cp');
	        $is_group_calc = $this->input->post('is_group_calc');
	        $is_group_calc_percent = $this->input->post('is_group_calc_percent');
	        $is_at_qty     = $this->input->post('is_at_qty');	        
	        $type_sub     = $this->input->post('type_sub');	 
	        $data=array(
				  		'subject_type'=>$subjecttype,
				  		'main_type'=>$maintype,
						'note'=>$description,
	                    'orders'=>$orders,
	                    'schoolid'=>$schoolid,
	                    'schlevelid'=>$schlevelid,
	                    'rangelevelid'=>$rangelevelid,
	                    'is_report_display'=>$report_display,
	                    'is_group_calc'=>$is_group_calc,
	                    'is_group_calc_percent'=>$is_group_calc_percent,
	                    'is_class_participation'=>$is_cp,
	                    'is_attendance_qty'=>$is_at_qty,
	                    'type_subject'=>$type_sub  
					);
			if($subjectid_update ==""){
				$this->db->insert('sch_subject_type_iep',$data);
				$save_res =1;
			}else{
				$this->db->where('subj_type_id',$subjectid_update);
				$this->db->update('sch_subject_type_iep',$data);
				$save_res =2;
			}
			return  $save_res;
		}
		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m=$this->input->post('m');
		    $p=$this->input->post('p');
		   //$this->green->setActiveRole($this->input->post('roleid'));
		    if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }

			$config['base_url']=site_url('school/c_subjecttype_iep/index?m=$m&p=$p');
			$config['page_query_string'] = TRUE;
			$config['per_page']=100;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$config['total_rows']=$this->db->get('sch_subject_type_iep')->num_rows();
			$this->pagination->initialize($config);

			$this->db->order_by("subj_type_id", "asc");
            $this->db->order_by("orders", "asc");
			return $this->db->get('sch_subject_type_iep',$config['per_page'],$page)->result();
		}

		function searchsubjecttype($subject_type,$main_type,$schoolid="",$schlevelid="",$yearid="",$rangelevelid="")
		{
		   $per_page='';	
		   if(isset($_GET['per_page']))
		   $per_page=$_GET['per_page'];
		   $config['base_url']=site_url("school/c_subjecttype_iep/search?main_type=$main_type&subjecttype=$subject_type");
		   $config['per_page']=100;
		   $config['full_tag_open'] = '<li>';
		   $config['full_tag_close'] = '</li>';
		   $config['cur_tag_open'] = '<a><u>';
		   $config['cur_tag_close'] = '</u></a>';
		   $config['page_query_string']=TRUE;
		   $config['num_link']=3;

            if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
            if($yearid!=""){
                $this->db->where('yearid',$yearid);
            }
            if($rangelevelid!=""){
                $this->db->where('rangelevelid',$rangelevelid);
            }

		   $this->db->like('subject_type',$subject_type);
		   $this->db->like('main_type',$main_type);
		  
		   $config['total_rows']=$this->db->get('v_school_subject_group_iep')->num_rows();
		   $this->pagination->initialize($config);
		   $this->db->select('*');
		   $this->db->from('v_school_subject_group_iep');
		  //$this->db->join('sch_z_role r','u.roleid=r.roleid','inner');
		   $this->db->like('subject_type',$subject_type);
		   $this->db->like('main_type',$main_type);
		   $this->db->order_by("subj_type_id", "asc");
           $this->db->order_by("orders", "asc");
           $this->db->limit($config['per_page'],$per_page);
		   $query=$this->db->get();
		  return $query->result();
		 }
		
	}
