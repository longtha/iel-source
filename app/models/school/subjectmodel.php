<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class subjectmodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']) && $_GET['per_page'] != ""){
				$page=$_GET['per_page'];
			}else{
				$page=0;	
			}
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			$config['base_url']=site_url("school/subject/index?m=$m&p=$p");
			$config['page_query_string'] = TRUE;
			$config['per_page']=100;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			//$this->db->where('status',1);
			$config['total_rows']=$this->db->get('v_school_subject')->num_rows();
			$this->pagination->initialize($config);
			//$this->db->where('status',1);
			//return $this->db->get('sch_subject', $config['per_page'],$page)->result();
			
			//------ Join ------
			$query = $this->db->query("SELECT
								v_sub.subjectid,
								v_sub.`subject`,
								v_sub.subj_type_id,
								v_sub.short_sub,
								v_sub.schlevelid,
								v_sub.subject_kh,
								v_sub.orders,
								v_sub.sch_level,
								v_sub.subject_type,
								v_sub.grade_level,
								v_sub.subj_grad_id,
								v_sub.exam_type_id,
								v_sub.grade_levelid,
								v_sub.is_trimester_sub,
								v_sub.is_eval,
								v_sub.exam_name
							FROM
								v_school_subject AS v_sub
							ORDER BY v_sub.`subject`, v_sub.subjectid,v_sub.grade_levelid
							-- LIMIT {$page},{$config['per_page']}
							");
			return $query->result();		
		}

		function getsubject()
		{
			//$this->db->where('status',1);
			$query=$this->db->get('sch_subject');
			return $query->result();
		}
		function getsubjecttype($schoollevel="")
		{
			$this->db->where('schlevelid',$schoollevel);
			$query=$this->db->get('sch_subject_type');
			return $query->result();
		}
		function getschool()
		{
			//$this->db->where('status',$schoollevel);
			$query=$this->db->get('sch_school_infor');
			return $query->result();
		}

		function getsubjectrow($subjectid)
		{
			$this->db->where('subjectid',$subjectid);
			$query=$this->db->get('sch_subject');
			return $query->row();
		}
		function getsubjectdetail($subjectid="",$gradelevel="" ,$examtype="")
		{
			if($subjectid !=""){
				$this->db->where('subjectid',$subjectid);
			}
			if($gradelevel !=""){
				$this->db->where('grade_levelid',$gradelevel);
			}
			if($examtype !=""){
				$this->db->where('exam_type_id',$examtype);
			}
			$query=$this->db->get('sch_subject_detail');
			return $query->row();
		}
		function getvalidate($subject)
		{
			$this->db->select('count(*)');
			$this->db->from('sch_subject');
			$this->db->where('subject',$subject);
			return  $this->db->count_all_results();
		}

		function getvalidateup($subject="",$subjecttypeid="",$school_id="", $subjectid="")
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_subject');
		    //$this->db->where('subject',$subject);
		    $this->db->where('schoolid',$school_id);
		    $this->db->where('subj_type_id',$subjecttypeid);
			$this->db->where_not_in('subjectid',$subjectid);
			return  $this->db->count_all_results();
		}

		function subjectvalidate($subject,$subjecttype,$school_id,$schlevelid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_subject');
		    //$this->db->where('subject',$subject);
		    $this->db->where('schlevelid',$schlevelid);
		    $this->db->where('schoolid',$school_id);
		    $this->db->where('subj_type_id',$subjecttype);
		    return  $this->db->count_all_results();
		}
        function deletesubject($subjectid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_score_entryed');
		    $this->db->where('subjectid',$subjectid);
		    $deleted = $this->db->count_all_results();
		    if($deleted>0){
		    	$del_re = 0;
		    }else{
		    	$this->db->where('subjectid', $subjectid);
        		$this->db->delete('sch_subject');

				$this->db->where('subjectid', $subjectid);
				$this->db->delete('sch_subject_detail');
        		$del_re = 1;
		    }
			
			return $del_re;
		}
		function searchsubjects($subject="",$subjecttype="",$shortcut="",$schlevelid="",$gradelevelid="",$m="",$p="")
		{		//-------- Pagination ----------
			
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url("school/subject/search?m=$m&p=$p&subj=$subject&subj_type=$subjecttype&short=$shortcut&schlid=$schlevelid&gradlid=$gradelevelid");
			$config['page_query_string'] = TRUE;
			$config['per_page']=100;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$where ="";
	
			if($subject!=""){
				$where .=" AND subject like '%".$subject."%' OR subject_kh like '%".$subject."%'";
				$this->db->like('subject',$subject);
				//$this->db->like('subject_kh',$subject);
			}
			if($subjecttype!=""){
				$where .=" AND subj_type_id='".$subjecttype."'";
				$this->db->where('subj_type_id',$subjecttype);
			}

			if($shortcut!=""){
				$where .=" AND short_sub like'%".$shortcut."%'";
				$this->db->like('short_sub',$shortcut);
			}
			if($schlevelid!=""){
				$where .=" AND schlevelid='".$schlevelid."'";
				$this->db->where('schlevelid',$schlevelid);
			}
			if($gradelevelid!=""){
				$where .=" AND grade_levelid='".$gradelevelid."'";
				$this->db->where('grade_levelid',$gradelevelid);
			}			
			
			// if($examtype!=""){
			// 	$where .=" AND exam_type_id='".$examtype."'";
			// 	$this->db->where('exam_type_id',$examtype);
			// }
			//echo $where;
			$config['total_rows']=$this->db->get('v_school_subject')->num_rows();
			$this->pagination->initialize($config);

			$query = $this->db->query("SELECT
								v_sub.subjectid,
								v_sub.`subject`,
								v_sub.subj_type_id,
								v_sub.short_sub,
								v_sub.schlevelid,
								v_sub.subject_kh,
								v_sub.orders,
								v_sub.sch_level,
								v_sub.subject_type,
								v_sub.grade_level,
								v_sub.subj_grad_id,
								v_sub.exam_type_id,
								v_sub.grade_levelid,
								v_sub.is_trimester_sub,
								v_sub.is_eval,
								v_sub.exam_name,
                                v_sub.max_score
							FROM
								v_school_subject AS v_sub
							WHERE 1=1 {$where}
							ORDER BY v_sub.`subject`, v_sub.subjectid,v_sub.grade_levelid
							-- LIMIT {$page},{$config['per_page']}
							");
										
			return $query->result();

		}

        function  save($subjectid_ups=''){
			$subjectid_up =$this->input->post('subjectid');
            $subject =$this->input->post('subject');            
			$subjecttype =$this->input->post('subjecttypeid');
			$maintype = $this->input->post('maintype');
			$short_sub =$this->input->post('short_sub');
			$schoolid =$this->input->post('schoolid');
			$subject_kh=$this->input->post('subject_kh');
			$schlevelid =$this->input->post('schlevelid');
			$max_score =$this->input->post('max_score');
			$arrgradelevel = $this->input->post('arrgradelevel');
			$orders =$this->input->post('orders');
			$checkgroupsubject =$this->input->post('checkgroupsubject');

            $programid=$this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='".$schlevelid."'")->row()->programid;

			$is_trim =0;
			$is_eval =0;
			if(isset($_POST['is_trimester']))
				$is_trim=1;
			if(isset($_POST['is_eval']))
				$is_eval=1;
			$count = 0;
			// if($subjectid_up !=""){
			// 	$count=$this->getvalidateup($subject,$subjecttype,$schoolid,$subjectid_up);
			// }else{
			// 	$count=$this->subjectvalidate($subject,$subjecttype,$schoolid,$schlevelid);
			// }
			$save_res = "";
			if ($count!=0){
				$save_res =2;

				$this->load->view('header');
				$data['exist']="<p style='color:red;'>Your data has already exist...! $count $is_trim</p>";
				$data['query']=$this->subjects->getpagination();
				$this->load->view('school/subject/add',$data);
				$this->load->view('school/subject/view',$data);
				$this->load->view('footer');
			}else{
				$data=array(
				  		'subject'=>$subject,
				  		'subj_type_id'=>$subjecttype,
				  		'short_sub'=>$short_sub,
						'schoolid'=>$schoolid,
						'schlevelid'=>$schlevelid,
						'is_trimester_sub'=>$is_trim,
						'is_eval'=>$is_eval,
						'subject_kh'=>$subject_kh,
						'orders'=>$orders,
						'score_percence'=>($orders/100),						
						'max_score'=>$max_score,
						'created_by'=>$this->session->userdata('userid'),
						'created_date'=>date('Y-m-d H:i:s'),
						'modified_by'=>$this->session->userdata('userid'),
						'modified_date'=>date('Y-m-d H:i:s'),
                        'programid'=>$programid,
						'subject_mainid'=>$maintype,
						'score_group'=>$checkgroupsubject
				  );
				if($subjectid_up !=""){
					$subjectid = $subjectid_up;
					$this->db->where('subjectid',$subjectid);
					$this->db->update('sch_subject',$data);

					$this->db->where('subjectid', $subjectid);
					$this->db->delete('sch_subject_detail');
					$save_res =3;
				}else{
					$this->db->insert('sch_subject',$data);
					$subjectid = $this->db->insert_id();
					$save_res =1;
				}
				
				//print_r($arrgradelevel);
				if(count($arrgradelevel) >0){
			        foreach($arrgradelevel as $row_gradelevel){
			        	//$arrexamtype = $row_gradelevel["arrexamtype"];
			        	if($row_gradelevel != ""){
			        		$detail =array(
			        				'subjecttypeid'=>$subjecttype,
				        			'subjectid'=>$subjectid,
							  		'grade_levelid'=>$row_gradelevel,
									'exam_type_id'=>""
								);
			        		$this->db->insert('sch_subject_detail',$detail);
			        	}
			        }
			    }
			    			
			}
            return  $save_res;
        }

        function isvalidsub($subject,$is_trimester=0){
            $state=true;
            $count=$this->green->getValue("SELECT count(*) FROM sch_subject WHERE subject='".$subject."' AND is_trimester_sub='".$is_trimester."'");
            if($count>0){
                $state=false;
            }
            return $state;
        }

        function  getSubinfo($subjectid){
            if($subjectid!=""){
                $sql="SELECT
                            subtype.subj_type_id,
                            subtype.subject_type,
                            subtype.main_type,
                            sub.subjectid,
                            sub.`subject`,
                            sub.short_sub,
                            sub.schoolid,
                            sub.subject_kh,
                            sub.is_trimester_sub,
                            sub.is_eval,
                            sub.orders,
                            sub.max_score
                        FROM
                            sch_subject AS sub
                        INNER JOIN sch_subject_type AS subtype ON sub.subj_type_id = subtype.subj_type_id
                        WHERE  subjectid='".$subjectid."'
                         ";
                $row=$this->green->getOneRow($sql);
                return $row;
            }
        }
        function subjectskgp($userid="",$grade_levelid,$exam_type_id="",$suj_group=""){
            $roleid=$this->green->getActiveRole();
			//$roleid="";
            $wh="";
            if($exam_type_id!=""){
                $wh=" AND  sub.exam_type_id='".$exam_type_id."'";
            }
            if($roleid!=1 && $userid!=""){
                $wh=" AND sub.teacher_id='{$userid}'";
            }
			if($suj_group !=""){
				$wh=" AND  sub.subj_type_id='".$suj_group."'";
			}

            $sql="SELECT DISTINCT
                                sub.subjectid,
                                sub.subj_type_id,
                                sub.`subject`,
                                sub.subject_type,
                                sub.schoolid,
                                sub.programid,
                                sub.schlevelid,
                                sub.yearid,
                                sub.subject_kh,
                                sub.max_score
                            FROM
                                v_subject_byteacher_kgp AS sub
                            WHERE 1=1  {$wh}
                            ";
            $data=$this->db->query($sql)->result();
            return $data;
        }
	}
?>