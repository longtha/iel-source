<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class moddayrange extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function dayranges(){
        return $this->db->get("sch_daysrange")->result();
    }

}
