<?php
class Modcard extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function grid(){
		$offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;

		$student_num = trim($this->input->post('student_num', TRUE));
		$full_name = trim($this->input->post('full_name', TRUE));
		$full_name_kh = trim($this->input->post('full_name_kh', TRUE));
		$card_type = trim($this->input->post('card_type', TRUE));

		$schlevelid = trim($this->input->post('schlevelid', TRUE));
		$yearid = trim($this->input->post('yearid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));

		$where = '';

		if($student_num != ''){
			$where .= "AND s.student_num LIKE '%{$student_num}%' ";
		}
		if($full_name != ''){
			$where .= "AND CONCAT(s.first_name, ' ', s.last_name) LIKE '%{$full_name}%' ";
		}
		if($full_name_kh != ''){
			$where .= "AND CONCAT(s.first_name_kh, ' ', s.last_name_kh) LIKE '%{$full_name_kh}%' ";
		}

		if($schlevelid != ''){
			$where .= "AND s.schlevelid = '{$schlevelid}' ";
		}
		if($yearid != ''){
			$where .= "AND s.yearid = '{$yearid}' ";
		}
		if($classid != ''){
			$where .= "AND s.classid = '{$classid}' ";
		}

		// count ==========
		$qr_c = $this->db->query("SELECT COUNT(s.studentid) AS c FROM v_student_profile AS s
									WHERE 1=1 {$where} ")->row()->c - 0;

		$totalRecord = $qr_c - 0;		
		$totalPage = ceil($totalRecord/$limit);

		// result =======
		$qr = $this->db->query("SELECT
										*,
										CONCAT(first_name, ' ', last_name) AS full_name,
										CONCAT(first_name_kh, ' ', last_name_kh) AS full_name_kh,
										DATE_FORMAT(dateofbirth, '%d-%m-%Y') AS dateofbirth
									FROM
										v_student_profile AS s
									WHERE 1 = 1 {$where}
									ORDER BY
										s.student_num ASC
									LIMIT {$offset},
									 {$limit} ");

		$i = 1;
		$tr = '';

		if($qr->num_rows() > 0){			
			foreach($qr->result_array() as $row){
				$no_imgs = base_url()."assets/upload/students/NoImage.png";	
				$have_img = base_url()."assets/upload/students/".$row["yearid"].'/thumbs/'.$row["studentid"].'.jpg';
				$img = '<img src="'.$no_imgs.'" class="rounded" alt="No Image" style="width: 25mm !important;height: 29.1mm !important;">';
				if (file_exists("assets/upload/students/".$row["yearid"].'/thumbs/'.$row["studentid"].'.jpg')) {
					$img = '<img src="'.$have_img.'" class="rounded" alt="No Image" style="width: 25mm !important;height: 29.1mm !important;">';
				}

				if($card_type - 0 == 1){
					$tr .= '<tr style="font-weight: normal;">
								<td style="font-weight: normal;text-align: center;" class="remove_tag">'.($i++ + $offset).'</td>
								<td style="text-align: center;">
								   <div style="display: inline-block;border: 0;width: 53.98mm;height: 85.5mm;">
								      <table border="0"​ cellspacing="0" cellpadding="0" align="center" style="width: 100%;font-size: 10px;margin-top: 1.74cm;font-family: Arial;">
								      <tr>
								           <td style="text-align: center;padding-bottom:4px;" colspan="2">Student Identity</td>
								      </tr>
								      
								      <tr>
								           <td style="text-align: center;" colspan="2">'.$img.'</td>
								      </tr>
										<tr style="text-align: left;">
								           <td style="padding-left: 12px;padding-top: 2px;" colspan="2">Student ID : '.($row['student_num'] != null ? $row['student_num'] : '').'</td>
								        </tr>
								        <tr style="text-align: left;">
								           <td style="padding-left: 12px;">Name : '.($row['full_name'] != null ? $row['full_name'] : '').'</td>
								        	<td style="width: 45px;font-size: 9px;">Sex : '.($row['gender'] == 'female' ? 'F' : 'M').'</td>
								        </tr>
								        <tr style="text-align: left;">
								           <td style="padding-left: 12px;font-size: 9px;" colspan="2">Grade : '.($row['class_name'] != null ? $row['class_name'] : '').'</td>
								        </tr>
								        <tr style="text-align: left;">
								           <td colspan="2" style="padding-left: 12px;font-size: 9px;">DOB : '.($row['dateofbirth'] != null && $row['dateofbirth'] != '00-00-0000' ? $row['dateofbirth'] : '').'</td>
								        </tr>
								        <tr style="text-align: left;">
								           <td colspan="2" style="padding-left: 12px;font-size: 9px;">Aca.Year : '.($row['sch_year'] != null ? $row['sch_year'] : '').'</td>
								        </tr>								        
								      </table>
								   </div>
								</td>                        
							</tr>';	
				}else{
					$tr .= '<tr style="font-weight: normal;">
								<td style="font-weight: normal;text-align: center;" class="remove_tag">'.($i++ + $offset).'</td>
								<td style="text-align: center;">
								   <div style="display: inline-block;border: 0;width: 53.98mm;height: 85.5mm;">
								      <table border="0"​ cellspacing="0" cellpadding="0" align="center" style="width: 100%;font-size: 10px;margin-top: 1.74cm;font-family: khmer os battambang;">
										<tr>
								           <td style="text-align: center;padding-bottom: 4px;" colspan="2">Student ID : '.($row['student_num'] != null ? $row['student_num'] : '').'</td>
								        </tr>
								        <tr>
								           <td style="text-align: center;" colspan="2">'.$img.'</td>
								        </tr>
								        <tr style="text-align: left;">
								           <td style="padding-left: 10px;padding-top: 2px;">ឈ្មោះ : '.($row['full_name_kh'] != null ? $row['full_name_kh'] : '').'</td>
								           <td style="width: 55px;font-size: 10px;">ភេទ : '.($row['gender'] == 'female' ? 'ស្រី' : 'ប្រុស').'</td>
								        </tr>
								        <tr style="text-align: left;">
								           <td style="padding-left: 10px;">Name : '.($row['full_name'] != null ? $row['full_name'] : '').'</td>
											<td style="width: 55px;font-size: 10px;">Sex : '.($row['gender'] == 'female' ? 'F' : 'M').'</td>
								        </tr>								        
								        <tr style="text-align: left;">
								           <td style="padding-left: 10px;font-size: 10px;" colspan="2">ថ្នាក់/Grade : '.($row['class_name'] != null ? $row['class_name'] : '').'</td>	
								        </tr>								        
								        <tr style="text-align: left;">
								           <td colspan="2" style="padding-left: 10px;font-size: 10px;">ថ្ងៃខែឆ្នាំកំណើត/DOB : '.($row['dateofbirth'] != null && $row['dateofbirth'] != '00-00-0000' ? $row['dateofbirth'] : '').'</td>
								        </tr>
								        <tr style="text-align: left;">
								           <td colspan="2" style="padding-left: 10px;font-size: 8px;">ឆ្នាំសិក្សា/Aca.Year : '.($row['sch_year'] != null ? $row['sch_year'] : '').'</td>
								        </tr>
								        
								      </table>
								   </div>
								</td>                        
							</tr>';
				}
			}
			
		}else{
			$tr .= '<tr><td colspan="2" style="font-weight: bold;text-align: center;background: #F2F2F2;">'."We din't find data".'</tr>';
		}

		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage); 
        return json_encode($arr);	
	}

    // get grade =========
    function get_grade($schlevelid){
        $opt = '';
        // $opt .= '<option value=""></option>';		
		foreach($this->g->getgradelevels($schlevelid) as $row_grade){
			$opt .= '<option value="'.$row_grade->grade_levelid.'">'.$row_grade->grade_level.'</option>';
		}
        return json_encode(array('opt' => $opt));
    }	

    // get school level ========
    function get_schlevel($programid = ''){
	    $opt = '';
		$opt .= '<option value="">All</option>';
		foreach ($this->level->getsch_level($programid) as $row) {
			$opt .= '<option value="'.$row->schlevelid.'">'.$row->sch_level.'</option>';
		}
	    return json_encode(array('opt' => $opt));
	}

    // get year ========
    function get_year($programid = '', $schlevelid = ''){
	    $opt = '';
        $opt .= '<option value=""></option>';		
		foreach($this->y->getschoolyear('', $programid, $schlevelid) as $row_year){
			$opt .= '<option value="'.$row_year->yearid.'">'.$row_year->sch_year.'</option>';
		}
	    return json_encode(array('opt' => $opt));
	}

	// get class ========
    function get_class($schlevelid = ''){
	    $opt = '';
        $opt .= '<option value=""></option>';		
		foreach($this->cl->allclass($schlevelid) as $row_class){
			$opt .= '<option value="'.$row_class->classid.'">'.$row_class->class_name.'</option>';
		}
	    return json_encode(array('opt' => $opt));
	}
}