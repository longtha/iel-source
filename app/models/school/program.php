<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class program extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    function getprogram($programid){
        if($programid!=""){
            $this->db->where("programid",$programid);
        }
        return $this->db->get("sch_school_program")->row() ;
    }
    function getprograms($schoolid="",$programid=""){
        if($schoolid!=""){
            $this->db->where("schoolid",$schoolid);
        }
        if($programid!=""){
            $this->db->where("programid",$programid);
        }
        return $this->db->get("sch_school_program")->result() ;
    }
}