<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class schoolyearmodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url('school/schoolyear/index?');
			$config['page_query_string'] = TRUE;
			$config['per_page']=50;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$config['total_rows']=$this->db->get('sch_school_year')->num_rows();
			$this->pagination->initialize($config);
			//return $this->db->get('sch_subject', $config['per_page'],$page)->result();
			
			//------ Join ------
			$this->db->select("sy.yearid,sy.sch_year,sy.from_date,sy.to_date,si.name,lev.sch_level");
			$this->db->from("sch_school_year sy");
            $this->db->join("sch_school_level lev","lev.schlevelid=sy.schlevelid","inner");
			$this->db->join("sch_school_infor si","sy.schoolid=si.schoolid","inner");
			$this->db->order_by("yearid", "desc");
			$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			
			return $query->result();		
		}

		function getschoolyear($schoolid="",$programid="",$schlevelid="")
		{

            if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($programid!=""){
                $this->db->where('programid',$programid);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
            //$query=$this->db->get('sch_subject');
			return $this->db->get('sch_school_year')->result();
		}		
		function getschool()
		{
			$query=$this->db->get('sch_school_infor');
			return $query->result();
		}

		function getschoolyearrow($schoolyearid)
		{
			$this->db->where('yearid',$schoolyearid);
			$query=$this->db->get('sch_school_year');
			return $query->row();
		}

		function getvalidate($year,$schlevelid)
		{
			$this->db->select('count(*)');
			$this->db->from('sch_school_year');
			$this->db->where('sch_year',$year);
            $this->db->where('schlevelid',$schlevelid);
			return  $this->db->count_all_results();
		}

		function getvalidateup($year,$fromdate,$school_id, $todate,$yearid,$schlevelid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_school_year');
		    $this->db->where('sch_year',$year);
		    $this->db->where('schoolid',$school_id);
		    $this->db->where('from_date',$fromdate);
		    $this->db->where('to_date',$todate);
            $this->db->where('schlevelid',$schlevelid);
			$this->db->where_not_in('yearid',$yearid);

			return  $this->db->count_all_results();
		}

		function schoolyearvalidate($year,$fromdate,$todate,$school_id,$schlevelid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_school_year');
		    $this->db->where('sch_year',$year);
		    $this->db->where('from_date',$fromdate);
		    $this->db->where('to_date',$todate);
		    $this->db->where('schoolid',$school_id);
            $this->db->where('schlevelid',$schlevelid);
		    return  $this->db->count_all_results();
		}

		function searchschoolyears($year,$school,$yearid='')
		{
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url("school/schoolyear/search?y=$year&sch=$school&yi=$yearid");
			$config['page_query_string'] = TRUE;
			$config['per_page']=50;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$this->db->like('sch_year',$year);
			$this->db->like('schoolid',$school);

			$config['total_rows']=$this->db->get('sch_school_year')->num_rows();
			$this->pagination->initialize($config);
			$this->db->select("sy.yearid, sy.sch_year, sy.from_date, sy.to_date, si.name,lev.sch_level");
			$this->db->from("sch_school_year sy");
            $this->db->join("sch_school_level lev","lev.schlevelid=sy.schlevelid","LEFT");
			$this->db->join("sch_school_infor si","sy.schoolid=si.schoolid","LEFT");
			$this->db->like('sy.sch_year',$year);
			if($yearid!=0)
			 	$this->db->where('sy.yearid',$yearid);
			if($school!=0)
				$this->db->where('si.schoolid',$school);
			$this->db->order_by("sy.yearid", "desc");
			$this->db->limit($config['per_page'],$page);
			$query=$this->db->get();
			return $query->result();		
		}
	}
?>