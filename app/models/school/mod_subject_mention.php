<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_subject_mention extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}

	// get subject ======
	function get_subject($schlevelid,$programid,$groupsubjectid){
		//$where = "";
		// if($schlevelid != ""){
		// 	$where .= "AND s.schlevelid = '{$schlevelid}' ";
		// }
		
		if($programid =="1"){
			$where= "";
			if($groupsubjectid != ""){
				$where.= " AND s.subj_type_id = '{$groupsubjectid}' ";
			}
			if($schlevelid != ""){
				$where.= " AND s.schlevelid = '{$schlevelid}' ";
			}
			$subject = $this->db->query("SELECT DISTINCT
											s.subjectid,
											s.`subject`
										FROM
											sch_subject AS s
										WHERE 1 = 1 {$where}
										ORDER BY
											s.`subject` ASC ")->result();
		}
		if($programid =="2"){
			$where= "";
			if($groupsubjectid != ""){
				$where.= " AND s.subj_type_id = '{$groupsubjectid}' ";
			}
			if($schlevelid != ""){
				$where.= " AND s.schlevelid = '{$schlevelid}' ";
			}
			$subject = $this->db->query("SELECT DISTINCT
											s.subjectid,
											s.`subject`
										FROM
											sch_subject_iep AS s
										WHERE 1 = 1 {$where}
										ORDER BY
											s.`subject` ASC ")->result();
		}
		if($programid =="3"){
			$where= "";
			if($groupsubjectid != ""){
				$where.= " AND s.subj_type_id = '{$groupsubjectid}' ";
			}
			if($schlevelid != ""){
				$where.= " AND s.schlevelid = '{$schlevelid}' ";
			}
			$course = $this->input->post("course");
			if($course != ""){
				if($course == 1){
					$where.= " AND s.is_skill = '1' ";
				}else{
					$where.= " AND s.is_core = '1' ";
				}
			}
			$subject = $this->db->query("SELECT DISTINCT
											s.subjectid,
											s.`subject`
										FROM
											sch_subject_gep AS s
										WHERE 1 = 1 {$where}
										ORDER BY
											s.`subject` ASC ")->result();
		}
		if($programid ==""){
			$subject ="";
		}
		$arr = array('subject' => $subject);
        return json_encode($arr);
	}
	function m_group_subject($schlevelid,$programid){
		$sql_g = "";
		if($programid == 1){
			if($schlevelid != ""){
				$where= " AND sch_subject_type.schlevelid = '{$schlevelid}' ";
			}
			$sql_g = $this->db->query("SELECT
											sch_subject_type.subj_type_id,
											sch_subject_type.subject_type
										FROM sch_subject_type
										WHERE 1=1 {$where}");
		}
		if($programid == 2){
			if($schlevelid != ""){
				$where= " AND sch_subject_type_iep.schlevelid = '{$schlevelid}' ";
			}
			$sql_g = $this->db->query("SELECT
											sch_subject_type_iep.subj_type_id,
											sch_subject_type_iep.subject_type,
											sch_subject_type_iep.is_class_participation
										FROM sch_subject_type_iep
										WHERE 1=1 {$where}");
		}
		if($programid == 3){
			if($schlevelid != ""){
				$where= " AND sch_subject_type_gep.schlevelid = '{$schlevelid}' ";
			}
			$sql_g = $this->db->query("SELECT
											sch_subject_type_gep.subj_type_id,
											sch_subject_type_gep.subject_type
										FROM sch_subject_type_gep
										WHERE 1=1 {$where}");
		}
		return $sql_g;
	}
	// get grade ======
	function get_grade($schlevelid = ""){
		$where = "";
		if($schlevelid != ""){
			$where .= "AND g.schlevelid = '{$schlevelid}' ";
		}
		$grade = $this->db->query("SELECT DISTINCT
											g.grade_levelid,
											g.grade_level
										FROM
											sch_grade_level AS g
										WHERE
											1 = 1 {$where}										
										ORDER BY
											g.grade_level ASC ")->result();
        return json_encode($grade);
	}

	// save ======
	function save(){
		$schoolid = $this->input->post('schoolid');
		$schlevelid = $this->input->post('schlevelid');
		$arr = $this->input->post('arr');	
		$arr_subj_edit = $this->input->post('arr_subj_edit');
		if(count($arr_subj_edit) > 0){
			$k=0;
			foreach($arr_subj_edit as $arrsub){
				foreach($arrsub as $rowedit){
					$data_del = array("schoolid"   => $schoolid,
									"schlevelid"   => $schlevelid,
									"subjectid"    => $rowedit["subjectid"],
									"gradelevelid" => $rowedit["gradid"]
									);
					$this->db->delete("sch_subject_mention",$data_del);
					//$k++;
				}
			}
		}
		$i = 0;
		if(!empty($arr))
		{
			foreach ($arr as $rsubjectid) 
			{
				if(!empty($rsubjectid) > 0){					
					foreach ($rsubjectid as $rgrandid) {

						if(!empty($rgrandid) > 0){
								
							foreach ($rgrandid as $r) {
								
								if($r["mentionid"] - 0 > 0 && trim($r["max_score"]) - 0 >= 0 && trim($r["max_score"]) !="" && trim($r["min_score"]) - 0 >= 0 && trim($r["min_score"]) != ""){
									
									$data = array("schoolid" => $schoolid,
													"schlevelid" => $schlevelid,
													"subjectid" => $r["subjectid"],
													"gradelevelid" => $r["grandid"],
													"mentionid" => $r["mentionid"],
													"max_score" => $r["max_score"],
													"minscrore" => $r["min_score"]
												);

									$i = $this->db->insert('sch_subject_mention', $data);
								}
							}
						}
					}
				}

			}
		}
        return json_encode($i);
	}

	// grid ======
	function grid(){
        // $offset = $this->input->post('offset') - 0;
        // $limit = $this->input->post('limit') - 0;

        $schlevelid = trim($this->input->post('schlevelid'));
		$programid = trim($this->input->post('programid'));
		$subj_group = trim($this->input->post('subj_group'));
		$is_cp = trim($this->input->post('is_cp'));
        $subjectid = trim($this->input->post('subjectid'));
        $grade_levelid = trim($this->input->post('grade_levelid'));
		$course = trim($this->input->post('course'));
        $where = '';
        // if($schlevelid != ''){
        //     $where .= " AND s.schlevelid = '{$schlevelid}' ";
        // }
        if($subjectid != ''){
            $where .= " AND s.subjectid = '{$subjectid}' ";
        }        

        // $where1 = "";
        // if($grade_levelid != ''){
        //     $where1 .= " AND g.grade_levelid = '{$grade_levelid}' ";
        // }
        $q = "";
		if($programid =="1"){
			$q = $this->db->query("SELECT
										s.subjectid,
										s.`subject`,
										s.schlevelid
									FROM
										sch_subject AS s
									WHERE 1 = 1 
									AND schlevelid = '{$schlevelid}'
									AND s.subj_type_id='".$subj_group."'
									{$where}
									ORDER BY
										s.`subject` ASC ");//LIMIT $offset, $limit
		}
		if($programid =="2"){
			if($is_cp == 0){
				$q = $this->db->query("SELECT
											s.subjectid,
											s.`subject`,
											s.schlevelid
										FROM
											sch_subject_iep AS s
										WHERE 1 = 1 
										AND schlevelid = '{$schlevelid}'
										AND s.subj_type_id='".$subj_group."'
										{$where}
										ORDER BY s.`subject` ASC ");//LIMIT $offset, $limit
			}else{
				$q = $this->db->query("SELECT
										sch_subject_type_iep.subj_type_id as subjectid,
										sch_subject_type_iep.schlevelid,
										sch_subject_type_iep.subject_type as subject
										FROM sch_subject_type_iep
									WHERE subj_type_id='".$subj_group."'
									AND is_class_participation='".$is_cp."'");
			}
		}
		if($programid =="3"){
			if($course != ""){
				if($course == 1){
					$where.= " AND s.is_skill = '1' ";
				}else{
					$where.= " AND s.is_core = '1' ";
				}
			}
			
			$q = $this->db->query("SELECT
										s.subjectid,
										s.`subject`,
										s.schlevelid
									FROM
										sch_subject_gep AS s
									WHERE 1 = 1 
									AND schlevelid = '{$schlevelid}'
									AND s.subj_type_id='".$subj_group."'	
									{$where}
									ORDER BY
										s.`subject` ASC ");//LIMIT $offset, $limit
		}

        $tr = "";
        $i = 1;
        $value_check = 0;
        if($q != ""){
	        if($q->num_rows() > 0)
	        {
	        	$value_check = 1;
	        	foreach ($q->result() as $row) {
	        		$j = 1;
	        		$tr .= '<tr>'.
	        					'<td name="no[]" class="no">'.$i++.'</td>'.
	        					'<td data-subjectid="'.$row->subjectid.'" name="subjectid_list[]" class="subjectid_list">'.$row->subject.'</td>';

					// grade, mention & min... =======				
					$q_grade = $this->db->query("SELECT
														g.grade_levelid,
														g.grade_level,
														COUNT(g.grade_levelid) AS c
													FROM
														sch_grade_level AS g
													WHERE
														g.schlevelid = '{$row->schlevelid}'
													AND	g.grade_levelid = '{$grade_levelid}'
													GROUP BY
														g.grade_levelid
													ORDER BY
														g.grade_level ASC ");				
					$tr .= '<td style="text-align: right;" colspan="2">
								<table cellpadding="0" cellspacing="0" style="width: 100%;">';

					$n = 1;
					if($q_grade->num_rows() > 0){
			        	foreach ($q_grade->result() as $row_grade) {
							$tr .= '<tr class="tr_">
										<td style="width: 23%;">
											<div class="col-sm-12">
												<label>
													<input type="checkbox" name="grade_levelid_list[]" class="grade_levelid_list" data-grade="'.$row_grade->grade_levelid.'"> '.$row_grade->grade_level.'
												</label>
											</div>
										</td>';
										
										$tr .= '<td align="left" data-top="'.$n++.'">
													<table cellpadding="0" cellspacing="0" style="width: 100%;">';	
										$tr .= '<tr class="tr__">
													<td style="padding: 8px 0 1px 0;">
														<div class="col-sm-5" style="border-bottom: 1px solid #CCC;">Mention</div>		
														<div class="col-sm-4" style="border-bottom: 1px solid #CCC;">Max Score</div>
														<div class="col-sm-3" style="border-bottom: 1px solid #CCC;">Min Score</div>
													</td>	
												</tr>';

										$sql_score_mention = $this->db->query("SELECT
																					sm.submenid,
																					sm.schoolid,
																					sm.schlevelid,
																					sm.subjectid,
																					sm.gradelevelid,
																					sm.mentionid,
																					sm.max_score,
																					sm.minscrore,
																					cm.mention
																			FROM
																			sch_subject_mention AS sm
																			LEFT JOIN sch_score_mention AS cm ON sm.mentionid = cm.menid
																			WHERE 1=1 AND sm.subjectid='".$row->subjectid."'
																			AND sm.schlevelid='".$row->schlevelid."'
																			AND sm.gradelevelid='".$grade_levelid."'");
										$arr_sc_m = array();
										if($sql_score_mention->num_rows() > 0){
											foreach($sql_score_mention->result() as $row_sc){
												$arr_sc_m[$row_sc->mentionid] = array("Max_sc"=>$row_sc->max_score,"Min_sc"=>$row_sc->minscrore);
											}
										}


										$q_men = $this->db->query("SELECT
																			sm.menid,
																			sm.mention
																		FROM
																			sch_score_mention AS sm
																		WHERE
																			sm.schlevelid = '{$row->schlevelid}'
																		ORDER BY
																			sm.mention ASC ");	
										if($q_men->num_rows() > 0){
								        	foreach ($q_men->result() as $row_men) {
								        		$max_s = "";
								        		$min_s = "";
								        		if(isset($arr_sc_m[$row_men->menid])){
								        			$max_s = $arr_sc_m[$row_men->menid]['Max_sc'];
								        			$min_s = $arr_sc_m[$row_men->menid]['Min_sc'];
								        		}
												$tr .= '<tr class="tr__">
															<td style="padding: 1px;">
																<div class="col-sm-5">
																	<div>
																		<label style="font-weight: normal !important;">
																			<input type="checkbox" name="menid_list[]" class="menid_list" data-menid="'.$row_men->menid.'"> '.$row_men->mention.'
																		</label>
																	</div>	
																</div>
																<div class="col-sm-3">
			 			 											<input decimal type="text" name="max_score[]" class="form-control max_score input-sm" placeholder="0" value="'.$max_s.'" style="text-align: right;">
			 			 										</div>					
											 			 		<div class="col-sm-1">-</div>
											 			 		<div class="col-sm-3">
											 			 			<input decimal type="text" name="min_score[]" class="form-control min_score input-sm" placeholder="0" value="'.$min_s.'" style="text-align: right;">
											 			 		</div>
															</td>
														</tr>';
								        	}

							        	}

										$tr .=	'</table>';    	

									$tr .=	'</td>
									</tr>';
			        	}		        	
		        	}
					$tr .= '</table>';

					// close =======
					$tr .= '<td colspan="1"><a href="javascript:;" name="del[]" class="del"><button type="button" class="btn btn-link"><img src="'.base_url('assets/images/icons/delete.png').'"></button></a></td>'.        					
		        			'</tr>';
	        	}
	        }else{
	        	$tr.= '<tr><td colspan="5" style="text-align: center;font-weight: bold;">No Results</td></tr>';
	        }
	    }
	    else{
	    	$value_check = 0;
        	$tr.= '<tr><td colspan="5" style="text-align: center;font-weight: bold;">No Results</td></tr>';
	    }

        // chk mention =====
		$score_mn = $this->db->query("SELECT
									sm.menid
								FROM
									sch_score_mention AS sm
								WHERE
									sm.schlevelid = '{$schlevelid}' ")->result();
                                                
        //$arr = array('tr' => $tr, 'score_mn' => $score_mn, 'totalRecord' => $totalRecord);//, 'totalPage' => $totalPage
        $arr = array('tr' => $tr,'value_check'=>$value_check);
        return json_encode($arr);
    }

}