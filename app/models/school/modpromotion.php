<?php
	class Modpromotion extends CI_Model{
		function getpromotion(){
				$page=0;
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				$m='';
				$p='';
				if(isset($_GET['m'])){
		        	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }	
			    	$school=$this->session->userdata('schoolid');
					$sql="SELECT * 
							FROM sch_school_promotion schp
							INNER JOIN sch_school_level schl
							ON(schp.schlevelid=schl.schlevelid) WHERE schp.schoolid='$school'";	
					$config['base_url'] =site_url()."/school/promotion/index?pg=1&m=$m&p=$p";	
					$config['total_rows'] = $this->green->getTotalRow($sql);
					$config['per_page'] =10;
					$config['num_link']=5;
					$config['page_query_string'] = TRUE;
					$config['full_tag_open'] = '<li>';
					$config['full_tag_close'] = '</li>';
					$config['cur_tag_open'] = '<a><u>';
					$config['cur_tag_close'] = '</u></a>';
					$this->pagination->initialize($config);
					$limi=" limit ".$config['per_page'];
					if($page>0){
							$limi=" limit ".$page.",".$config['per_page'];
						}		
					$sql.=" {$limi}";
					return $this->db->query($sql)->result();
		}
		function search($promotion,$schlevelid,$s_num,$m,$p,$yearid){
				$page=0;
				$where='';
				$school=$this->session->userdata('schoolid');
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				if($promotion!='')
					$where.=" AND schp.proname LIKE '%".$promotion."%'";
				if($schlevelid!='')
					$where.=" AND schp.schlevelid = '".$schlevelid."'";
				if($yearid!='')
					$where.=" AND schp.yearid='".$yearid."'";
				$sql="SELECT * 
						FROM sch_school_promotion schp
						INNER JOIN sch_school_level schl
						ON(schp.schlevelid=schl.schlevelid) WHERE schp.schoolid='$school' {$where} ";	
				$config['base_url'] =site_url()."/school/promotion/search?pg=1&m=$m&p=$p&pro=$promotion&l=$schlevelid&s_num=$s_num";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =$s_num;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				return $this->db->query($sql)->result();
		}
	}