<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_calendar extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function save($calendar) {
      $result = $this->db->insert('sch_school_calendar', $calendar) ? 1 : 0;

      return $result;
    }

    function delete($id) {

      $this->db->delete('sch_school_calendar', array('id' => $id));

      if ($this->db->_error_message()) {
          $result['delete'] = 'Error! ['.$this->db->_error_message().']';
      } else if (!$this->db->affected_rows()) {
          $result['delete'] = 'Error! ID ['. $id .'] not found';
      } else {
          $result['delete'] = 'Success';
      }

      return $result;
    }

    function reload($parameters, $isParameter) {
      if($this->session->userdata('match_con_posid')!='stu'){
      $homework_query = <<<QUERY
                            SELECT
                              sc.id,
                              si.`name` AS 'school',
                              sl.sch_level AS 'school_level',
                              sy.sch_year AS 'academic_year',
                              (SELECT y.from_date FROM `sch_school_year` AS y WHERE y.yearid =sc.academic_year_id limit 0, 1) as from_date,
                              (SELECT y.to_date FROM `sch_school_year` AS y WHERE y.yearid =sc.academic_year_id limit 0, 1) as to_date,
                              sc.title,
                              sc.description,
                              sc.attach_file,
                              sc.file_type,
                              sc.transaction_date,
                              u.user_name AS 'created_by'
                              FROM
                              sch_school_calendar sc
                              INNER JOIN sch_school_infor si ON si.schoolid = sc.school_id
                              INNER JOIN sch_school_level sl ON sl.schlevelid = sc.school_level_id
                              INNER JOIN sch_school_year sy ON sy.yearid = sc.academic_year_id
                              INNER JOIN sch_user u ON u.userid = sc.created_by
                              ORDER BY  sc.id DESC
QUERY;
}else{
  $homework_query = "
                      SELECT
                        sc.id,
                        si.`name` AS 'school',
                        sl.sch_level AS 'school_level',
                        sy.sch_year AS 'academic_year',
                        (SELECT y.from_date FROM `sch_school_year` AS y WHERE y.yearid =sc.academic_year_id limit 0, 1) as from_date,
                        (SELECT y.to_date FROM `sch_school_year` AS y WHERE y.yearid =sc.academic_year_id limit 0, 1) as to_date,
                        sc.title,
                        sc.description,
                        sc.attach_file,
                        sc.file_type,
                        sc.transaction_date,
                        u.user_name AS 'created_by'
                        FROM
                        sch_school_calendar sc
                        INNER JOIN sch_school_infor si ON si.schoolid = sc.school_id
                        INNER JOIN sch_school_level sl ON sl.schlevelid = sc.school_level_id
                        INNER JOIN sch_school_year sy ON sy.yearid = sc.academic_year_id
                        INNER JOIN sch_user u ON u.userid = sc.created_by
                        WHERE 1=1 AND sc.school_level_id IN(SELECT DISTINCT schlevelid FROM v_student_profile WHERE 1=1 AND studentid='".($this->session->userdata('emp_id'))."')
                        ORDER BY  sc.id DESC
                      ";
}
      if ($isParameter == 1) {
        $this->db->where($parameters);
      }

      $homeworks = $this->db->query($homework_query)->result();

      return $homeworks;
    }

    
    function filter($parameters) {
        
        
        $where='';
        $i=0;
        foreach($parameters as $key => $val)
        {
            if($i==0){
                if($key=="from_date"){
                    
                    list ($day, $month, $year) = explode('-', $val);
                    
                    $startdate = $year."-".$month."-".$day." 00:00:00";
                    
                    $where=$where.' WHERE UNIX_TIMESTAMP(sc.from_date)>=UNIX_TIMESTAMP("'.$startdate.'")';
                    
                }else if($key=="to_date"){
                    
                    list ($day, $month, $year) = explode('-', $val);
                    
                    $enddate = $year."-".$month."-".$day." 00:00:00";
                    
                    $where=$where.' WHERE UNIX_TIMESTAMP(sc.to_date)<=UNIX_TIMESTAMP("'.$enddate.'")';
                    
                }else{
                    $where=$where.' WHERE sc.'.$key.'="'.$val.'"';
                }
            }else{
                if($key=="from_date"){
                    
                    list ($day, $month, $year) = explode('-', $val);
                    
                    $startdate = $year."-".$month."-".$day." 00:00:00";
                    
                    $where=$where.' AND UNIX_TIMESTAMP(sc.from_date)>=UNIX_TIMESTAMP("'.$startdate.'")';
                    
                    
                }else if($key=="to_date"){
                    
                    list ($day, $month, $year) = explode('-', $val);
                    
                    $enddate = $year."-".$month."-".$day." 00:00:00";
                    
                    $where=$where.' AND UNIX_TIMESTAMP(sc.to_date)<=UNIX_TIMESTAMP("'.$enddate.'")';
                    
                    
                }else{
                    $where=$where.' AND sc.'.$key.'="'.$val.'"';
                }
            }
            $i=$i+1;
            
        }
        
        $calendarQuery2 = 'SELECT
                              sc.id,
                              si.`name` AS school,
                              sl.sch_level AS school_level,
                              sy.sch_year AS academic_year,
                              (SELECT y.from_date FROM `sch_school_year` as y WHERE yearid =sc.academic_year_id) as from_date,
                              (SELECT y.to_date FROM `sch_school_year` as y WHERE yearid =sc.academic_year_id) as to_date,
                              sc.title,
                              sc.description,
                              sc.attach_file,
                              sc.file_type,
                              sc.transaction_date,
                              u.user_name AS created_by
                              FROM
                              sch_school_calendar sc
                              INNER JOIN sch_school_infor si ON si.schoolid = sc.school_id
                              INNER JOIN sch_school_level sl ON sl.schlevelid = sc.school_level_id
                              INNER JOIN sch_school_year sy ON sy.yearid = sc.academic_year_id
                              INNER JOIN sch_user u ON u.userid = sc.created_by
                            '.$where;
        //if (is_object($parameters)) {
        //    $this->db->where($parameters);
        //}
        
        $calendar2 = $this->db->query($calendarQuery2)->result();
        return $calendar2 ;
        
    }
    
    function getSubject($schoolLevel, $programId) {

      $subject = $this->db->query("SELECT subjectid, subject FROM sch_subject WHERE schlevelid = $schoolLevel AND programid = $programId")->result();

      return $subject;
    }

   
    function getSchoolLevel($schoolId) {
        return $this->db->query("SELECT lv.schlevelid, lv.sch_level, lv.programid FROM sch_school_level lv INNER JOIN zz_tb_program z ON z.id_program = lv.programid AND lv.schoolid = $schoolId")->result();
    }
    function getProgram(){
        return $this->db->query("SELECT programid,	program	schoolid,	program_kh,	program_short FROM `sch_school_program`")->result();
    }
}
