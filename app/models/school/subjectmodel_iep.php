<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class subjectmodel_iep extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		function getpagination()
		{	
			//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			$config['base_url']=site_url("school/subject_iep/index?m=$m&p=$p");
			$config['page_query_string'] = TRUE;
			$config['per_page']=100;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			//$this->db->where('status',1);
			$config['total_rows']=$this->db->get('v_school_subject_iep')->num_rows();
			$this->pagination->initialize($config);
			//$this->db->where('status',1);
			//return $this->db->get('sch_subject', $config['per_page'],$page)->result();
			
			//------ Join ------
			$query = $this->db->query("SELECT
											subiep.subjectid,
											subiep.`subject`,
											subiep.subj_type_id,
											subiep.short_sub,
											subiep.schoolid,
											subiep.programid,
											subiep.schlevelid,
											subiep.yearid,
											subiep.subject_kh,
											subiep.is_trimester_sub,
											subiep.created_by,
											subiep.created_date,
											subiep.modified_by,
											subiep.modified_date,
											subiep.is_eval,
											subiep.orders,
											subiep.coefficient,
											subiep.max_score,
											subiep.subject_type,
											subiep.sch_level,
											subiep.is_assessment
											FROM
											v_school_subject_iep AS subiep
										ORDER BY subiep.`subject`, subiep.subjectid
										LIMIT {$page},{$config['per_page']}");
			return $query->result();		
		}

		function getsubject()
		{
			//$this->db->where('status',1);
			$query=$this->db->get('v_school_subject_iep');
			return $query->result();
		}
		function getsubjecttype($schoollevel="")
		{
			if($schoollevel != ""){
				$this->db->where('schlevelid',$schoollevel);
			}
			$query=$this->db->get('sch_subject_type_iep');
			return $query->result();
		}
		function getschool()
		{
			//$this->db->where('status',1);
			$query=$this->db->get('sch_school_infor');
			return $query->result();
		}

		function getsubjectrow($subjectid)
		{
			$this->db->where('subjectid',$subjectid);
			$query=$this->db->get('sch_subject_iep');
			return $query->row();
		}
		function getvalidate($subject)
		{
			$this->db->select('count(*)');
			$this->db->from('sch_subject_gep');
			$this->db->where('subject',$subject);
			return  $this->db->count_all_results();
		}
		function deletesubject($subjectid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_score_gep_entryed_detail');
		    $this->db->where('subjectid',$subjectid);
		    $deleted = $this->db->count_all_results();
		    if($deleted>0){
		    	$del_re = 0;
		    }else{
		    	$this->db->where('subjectid', $subjectid);
        		$this->db->delete('sch_subject_iep');
        		$del_re = 1;
		    }
			
			return $del_re;
		}
		function getvalidateup($subject,$subjecttypeid,$school_id, $subjectid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_subject_gep');
		    $this->db->where('subject',$subject);
		    $this->db->where('schoolid',$school_id);
		    $this->db->where('subj_type_id',$subjecttypeid);
			$this->db->where_not_in('subjectid',$subjectid);
			return  $this->db->count_all_results();
		}

		function subjectvalidate($subject,$subjecttype,$school_id,$schlevelid)
		{
			$this->db->select('count(*)');
		    $this->db->from('sch_subject_gep');
		    $this->db->where('subject',$subject);
		    $this->db->where('schlevelid',$schlevelid);
		    $this->db->where('schoolid',$school_id);
		    $this->db->where('subj_type_id',$subjecttype);

		    return  $this->db->count_all_results();
		}

		function searchsubjects($subject="",$subjecttype="",$shortcut="",$schlevelid="",$is_assessment="",$m="",$p="")
		{		//-------- Pagination ----------
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$config['base_url']=site_url("school/subject_iep/search?m=$m&p=$p&s=$subject&s_type=$subjecttype&sh=$shortcut");
			$config['page_query_string'] = TRUE;
			$config['per_page']=100;
			$config['num_link']=3;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] ='<a><span>';
			$config['cur_tag_close'] ='</span></a>';
			$where ="";
	
			if($subject!=""){
				$where .=" AND subjectid ='".$subject."'";
				//$this->db->like('subject',$subject);
				//$this->db->like('subject_kh',$subject);
			}
			if($subjecttype!=""){
				$where .=" AND subj_type_id='".$subjecttype."'";
				$this->db->where('subj_type_id',$subjecttype);
			}

			if($shortcut!=""){
				$where .=" AND short_sub like'%".$shortcut."%'";
				//$this->db->like('short_sub',$shortcut);
			}
			if($schlevelid!=""){
				$where .=" AND schlevelid='".$schlevelid."'";
				//$this->db->where('schlevelid',$schlevelid);
			}			
			
			if($is_assessment!=""){
				$where .=" AND is_assessment='".$is_assessment."'";
				//$this->db->where('is_assessment',$is_assessment);
			}			
			//echo $where;
			$config['total_rows']=$this->db->get('v_school_subject_iep')->num_rows();
			$this->pagination->initialize($config);

			$query = $this->db->query("SELECT
											subiep.subjectid,
											subiep.`subject`,
											subiep.subj_type_id,
											subiep.short_sub,
											subiep.schoolid,
											subiep.programid,
											subiep.schlevelid,
											subiep.yearid,
											subiep.subject_kh,
											subiep.is_trimester_sub,
											subiep.created_by,
											subiep.created_date,
											subiep.modified_by,
											subiep.modified_date,
											subiep.is_eval,
											subiep.orders,
											subiep.coefficient,
											subiep.max_score,
											subiep.subject_type,
											subiep.sch_level,
											subiep.is_assessment
											FROM
											v_school_subject_iep AS subiep
											WHERE 1=1 {$where}
										ORDER BY subiep.`subject`, subiep.subjectid
										LIMIT {$page},{$config['per_page']}");
										
			return $query->result();		
		}
		
        function  save(){
        	$subjectid=$this->input->post('subjectid');
            $subject =$this->input->post('subject');            
			$subjecttype =$this->input->post('subjecttypeid');
			$short_sub =$this->input->post('short_sub');
			$schoolid =$this->input->post('schoolid');
			$subject_kh=$this->input->post('subject_kh');
			$schlevelid =$this->input->post('schlevelid');
			$programid =$this->input->post('programid');
			$orders =$this->input->post('orders');
			$is_assessment=$this->input->post('is_assessment');
			$max_score=$this->input->post('max_score');
			//$calc_score=$this->input->post('calc_score');

			$is_trim =0;
			$is_eval =0;
			$count=$this->subjectvalidate($subject,$subjecttype,$schoolid,$schlevelid);

			if ($count!=0){
				$save_res =2;				
			}else{
				$data=array(
				  		'subject'=>$subject,
				  		'subj_type_id'=>$subjecttype,
				  		'short_sub'=>$short_sub,
						'schoolid'=>$schoolid,
						'schlevelid'=>$schlevelid,
						'programid'=>$programid,
						'is_trimester_sub'=>$is_trim,
						'is_eval'=>$is_eval,
						'subject_kh'=>$subject_kh,						
						'orders'=>$orders,
						'is_assessment'=>$is_assessment,
						'max_score'=>$max_score,
						'created_by'=>$this->session->userdata('userid'),
						'created_date'=>date('Y-m-d H:i:s')
				  );	
				$this->db->insert('sch_subject_iep',$data);
				//print_r($arrgradelevel);
			    $save_res =1;
			}
            return  $save_res;
        }

        function updatesubject(){
        	$subjectid=$this->input->post('subjectid');
        	$subject=$this->input->post('subject');
			$subjecttypeid=$this->input->post('subjecttypeid');
			$shortcut=$this->input->post('short_sub');
			$school=$this->input->post('schoolid');
			$subjkh=$this->input->post('subject_kh');
			$schlevelid =$this->input->post('schlevelid');
			$is_assessment=$this->input->post('is_assessment');
			$orders=$this->input->post('orders');
			$max_score=$this->input->post('max_score');
			//$calc_score=$this->input->post('calc_score');
			$is_trim=0;
			$is_eval=0;
			$count=$this->getvalidateup($subject,$subjecttypeid,$school,$subjectid);
			
			if ($count!=0){
				$save_res =2;
			}else{
					$this->db->where('subjectid',$subjectid);
					$data=array(
								'subject'=>$subject,
								'subject_kh'=>$subjkh,
								'subj_type_id'=>$subjecttypeid,
								'short_sub'=>$shortcut,
								'schoolid'=>$school,
								'schlevelid'=>$schlevelid,
								'orders'=>$orders,
								'is_assessment'=>$is_assessment,
								'max_score'=>$max_score,
								'is_eval'=>$is_eval,
								'modified_by'=>$this->session->userdata('userid'),
								'modified_date'=>date('Y-m-d H:i:s')

					);
					
				$this->db->update('sch_subject_iep',$data);
				$save_res =1;
			}
			return  $save_res;
        }

        function isvalidsub($subject,$is_trimester=0){
            $state=true;
            $count=$this->green->getValue("SELECT count(*) FROM sch_subject_gep WHERE subject='".$subject."' AND is_trimester_sub='".$is_trimester."'");
            if($count>0){
                $state=false;
            }
            return $state;
        }

        function  getSubinfo($subjectid){
            if($subjectid!=""){
                $sql="SELECT
                            subtype.subj_type_id,
                            subtype.subject_type,
                            subtype.main_type,
                            sub.subjectid,
                            sub.`subject`,
                            sub.short_sub,
                            sub.schoolid,
                            sub.subject_kh,
                            sub.is_trimester_sub,
                            sub.is_eval,
                            sub.orders,                            
							sub.max_score,
							sub.calc_score
                        FROM
                            sch_subject_gep AS sub
                        INNER JOIN sch_subject_type AS subtype ON sub.subj_type_id = subtype.subj_type_id
                        WHERE  subjectid='".$subjectid."'
                         ";
                $row=$this->green->getOneRow($sql);
                return $row;
            }
        }
		function subjectsiep($userid=""){
			$roleid=$this->green->getActiveRole();
			$wh="";
			if($roleid!=1 && $userid!=""){
				$wh=" AND sub.teacher_id='{$userid}'";
			}
			$sql="SELECT DISTINCT
                                sub.subjectid,
                                sub.subj_type_id,
                                sub.`subject`,
                                sub.subject_type,
                                sub.schoolid,
                                sub.programid,
                                sub.schlevelid,
                                sub.yearid,
                                sub.subject_kh,
                                sub.max_score
                            FROM
                                v_subject_byteacher_iep AS sub
                            WHERE 1=1 {$wh}
                            ";
			$data=$this->db->query($sql)->result();
			return $data;
		}

	}
?>