<?php
    class ModStock extends CI_Model {
        public function getAll($id='')
        {
            	
        }
       
		function saveImport(){
			$is_imported=false;
			if (isset($_FILES['file']) && $_FILES['file']["tmp_name"][0]!="") {
				$_xfile = $_FILES['file']["tmp_name"];	
				function str($str){
					return str_replace("'","''",$str);
				}
				if($_FILES['file']['tmp_name']==""){
					$error="<font color='red'>Please Choose your Excel file!</font>";					
				}
				else{
					$html="";					
					require_once(APPPATH.'libraries/simplexlsx.php' );								
					foreach($_xfile as $index => $value){					
						$xlsx = new SimpleXLSX($value);				
						$_data = $xlsx->rows();					
						array_splice($_data,0,1);
						$error_record_exist="";
						foreach( $_data as $k => $r) {					
							$_check_exist = $this->green->getTable("SELECT * FROM sch_stock WHERE stockcode = '{$r[0]}'");					
							$seachspance = strpos(trim(str($r[0]))," ");
							if (count($_check_exist)> 0)
							{
								$error_record_exist .='<div class="success" style=" padding:5px; margin-top:5px;">
														<font style="color:red;">Code already exist'.$r[0].' exist aleady !</font><br>
														</div>';					
							}else if($seachspance !== false){
								$error_record_exist .='<div class="success" style=" padding:5px; margin-top:5px;">
													<font style="color:red;">Error Code'.str($r[0]).' is incorrect. Please try again !</font></div>';
							}
							else {
								
								$stockcode=trim(str($r[0]));
								$descr_eng=trim(str($r[1]));
								$descr_kh=trim(str($r[2]));
								$reorder_qty=trim(str($r[3]));
								$categoryid=trim(str($r[4]));
								$uom=trim(str($r[6]));
								$is_con_serial=trim(str($r[7]));								
								$used_expired_date=trim(str($r[8]));
								
								$SQL_DM="INSERT INTO sch_stock";
																
								$SQL=" SET stockcode='".$stockcode."',
											descr_eng='".$descr_eng."',
											descr_kh='".$descr_kh."',
											reorder_qty='".$reorder_qty."',
											categoryid='".$categoryid."',
											uom='".$uom."',
											is_con_serial='".$is_con_serial."',			
											used_expired_date='".$used_expired_date."'			
										";
								$counts=$this->green->runSQL($SQL_DM.$SQL);
								$is_imported=true;		
								
							}					
						}
					}				
				}
			}
			return $is_imported;
		}		

		function save($stockid=''){	

			$stockcode=$this->input->post('stockcode');
			$descr_eng=$this->input->post('descr_eng');
			$descr_kh=$this->input->post('descr_kh');
			$reorder_qty=$this->input->post('reorder_qty');
			$categoryid=$this->input->post('categoryid');
			$uom=$this->input->post('uom');
			$is_con_serial=$this->input->post('is_con_serial');
			$used_expired_date=$this->input->post('used_expired_date');
			$is_active=$this->input->post('is_active');


			$created_by=$this->session->userdata('user_name');
			$created_date=date('Y-m-d H:i:s');
			$SQL_WHERE=" ,created_date='".$created_date."', created_by='".$created_by."'";
			$SQL_DM="";
			if($stockid!=""){
				$SQL_DM="UPDATE sch_stock ";				
				$last_modified_by=$this->session->userdata('user_name');
				$last_modified_date=date('Y-m-d H:i:s');
				$SQL_WHERE =" ,modified_by='".$last_modified_by."',modified_date='".$last_modified_date."'  WHERE stockid = '".$stockid."'"; 

			}else{
				$SQL_DM="INSERT INTO sch_stock";				
			}
			
			$SQL=" SET stockcode='".$stockcode."',
						descr_eng='".$descr_eng."',
						descr_kh='".$descr_kh."',
						reorder_qty='".$reorder_qty."',
						categoryid='".$categoryid."',
						uom='".$uom."',
						is_con_serial='".$is_con_serial."',			
						used_expired_date='".$used_expired_date."',
						is_active='".$is_active."'				
					";
			
			if($this->validStock($stockcode,$stockid)){
				$this->green->runSQL($SQL_DM.$SQL.$SQL_WHERE);
				if($stockid==""){
					$respond['result_save']="Stock was saved success.";	
				}else{
					$respond['result_save']="Stock was updated success.";
				}
							
			}else{
				$stockid="";
				$respond['result_save']="Stock not save.";
			}			
			#====== Get all wherehouse ;	
			$allwh=	$this->getWherehouse()->result();

			if($stockid==''){
				$stockid=$this->db->insert_id();	
				//++++++++++++++++++ Save to upStockBalance +++++++							
				foreach($allwh as $wh){
					$sql_wh="";
					$sql_wh="INSERT INTO sch_stock_balance SET stockid='".$stockid."',
															expired_date='0000-00-00',
															quantity='0',
															uom='".$uom."',
															whcode='".$wh->whcode."'";
					$this->green->runSQL($sql_wh);										
				}
				//+++++++++++++++++++++++++++++++++++++++++++				
			}else{
				foreach($allwh as $wh){
					$sql_wh="";
					$sql_wh="UPDATE sch_stock_balance SET uom='".$uom."' WHERE  stockid='".$stockid."' AND whcode='".$wh->whcode."'";
					$this->green->runSQL($sql_wh);										
				}
			}		
			$respond['stockid']=$stockid;
			return $respond;
		}
		function upStockBalance($stockid,$expire_date,$uom){
			
		}		

		function validStock($stockcode,$stockid=""){
			$this->db->select('count(*)');
			$this->db->from('sch_stock');			
			$this->db->where('stockcode',$stockcode);

			if($stockid!=""){
				$this->db->where_not_in('stockid', $stockid);
			}

			$data=$this->db->count_all_results();
			if($data>0){
				return false;
			}else{
				return true;
			}
		}
		
		
		function getuserrow($user_name){
			$this->db->where('user_name',$user_name); 
			$query=$this->db->get('sch_user u');
			return $query->row();
		}

		function getmaxid(){
			$this->db->select_max('stockcode','max');
			$this->db->from('sch_stock');
			$query=$this->db->get();
			return $query->row()->max+1;
		}
		
		function searchstock($stockcode,$descr_eng,$category,$sort,$sort_num,$m,$p){
				$page=0;
				$sql='';
				if(isset($_GET['per_page'])) $page=$_GET['per_page'];
				$this->load->library('pagination');	
				$config['base_url'] = site_url()."/stock/stock/search?s_id=$stockcode&des=$descr_eng&cat=$category&s_num=$sort_num&m=$m&p=$p";
				
				$where="";
				if($stockcode!=""){
					$where=" AND stockcode like '%".$stockcode."%'";
				}
				if($descr_eng!=""){
					$where.=" AND descr_eng like '%".$descr_eng."%'";
				}
				if($category!=""){
					$where.=" AND category like '%".$category."%'";
				}
				

				$sql.="SELECT stock.stockid,
							stock.stockcode,
							stock.descr_eng,
							stock.expired_date,
							stock.reorder_qty,
							stock.quantity,
							stock.uom,
							stock.categoryid,
							stock.category,
							stock.modified_date,
							stock.modified_by
						FROM
							v_stock_profile AS stock	
						WHERE 1=1 
						{$where}					
						";		
					
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] = $sort_num;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';	
				$this->pagination->initialize($config);	
				$limi=" limit ".$config['per_page'];
				if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}		
				$sql.=" $sort {$limi}";				
				$query=$this->green->getTable($sql);					
				return $query;
		}
		function getOpUOM(){
			return $this->db->get('sch_stock_uom');			
		}
		function getCategory(){
			return $this->db->get('sch_stock_category');
		}
		function getStockRow($stockid){
			$this->db->where('stockid',$stockid);
			$query=$this->db->get('sch_stock');
			return $query->row_array();
		}
		function getStockTrans($stockid){
			$this->db->where('stockid',$stockid);
			$query=$this->db->get('v_stock_profile');
			return $query->row_array();
		}
		function getPreview($stockid){
			$this->db->where('stockid',$stockid);
			$query=$this->db->get('v_stock_profile');
			return $query->row_array();
		}
		function getStockBl($stockid){
			$this->db->select('stockcode,expired_date,quantity,uom');			
			$this->db->where('stockid',$stockid);			
			return $this->db->get('v_stock_profile');
		}		
		function getWherehouse(){			
			return $this->db->get('sch_stock_wharehouse');			
		}


    }