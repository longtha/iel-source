<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModeOpenStock extends CI_Model {

	public function getopenstock(){
		$page=0;
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				$m='';
				$p='';
				if(isset($_GET['m'])){
			        $m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }
					$sql="SELECT * 
						FROM sch_stock_opening_balance sob
						INNER JOIN sch_stock s
						ON(sob.stockid=s.stockid)
						INNER JOIN sch_stock_wharehouse sw
						ON(sob.whcode=sw.whcode)						
						ORDER BY sob.transdate DESC												 
						";
					$config['base_url'] =site_url()."/stock/openstock?pg=1&m=$m&p=$p";	
					$config['total_rows'] = $this->green->getTotalRow($sql);
					$config['per_page'] =50;
					$config['num_link']=5;
					$config['page_query_string'] = TRUE;
					$config['full_tag_open'] = '<li>';
					$config['full_tag_close'] = '</li>';
					$config['cur_tag_open'] = '<a><u>';
					$config['cur_tag_close'] = '</u></a>';
					$this->pagination->initialize($config);
					$limi=" limit ".$config['per_page'];
					if($page>0){
							$limi=" limit ".$page.",".$config['per_page'];
						}		
						$sql.=" {$limi}";
					return $this->db->query($sql)->result();
	}
	function getmidicat(){
		return $this->db->where('stocktype','medecine')->get('sch_stock_category')->result();
	}
	function getstockopen($type,$transno){
		return $this->db->select('*')
					->from('sch_stock_opening_balance so')
					->join('sch_stock s','so.stockid=s.stockid','inner')
					->join('sch_stock_wharehouse sw','so.whcode=sw.whcode','inner')
					->where('type',$type)
					->where('transno',$transno)
                    ->order_by('so.stockid')
					->get()->result();
	}
	function getstockbalancerow($stockid,$expired_date){
		return $this->db->where('stockid',$stockid)
				->where('expired_date',$expired_date)
				->get('sch_stock_balance')->row();
	}
	function removestockmove($type,$tran,$whcode){
		$stock_move=$this->db->where('type',$type)
				->where('transno',$tran)
				->get('sch_stock_stockmove')->result();
		foreach ($stock_move as $move) {
			$this->db->query("UPDATE sch_stock_balance SET quantity=quantity - $move->quantity WHERE stockid='$move->stockid' AND whcode='$whcode' AND expired_date='$move->expired_date'");
		}
		$this->db->where('type',$type)
				->where('transno',$tran)
				->delete('sch_stock_stockmove');
	}
	function save($ref_no,$stockid,$ex_date,$qty,$uom,$tranno,$openid,$updatestock,$date,$user,$whcode){
		$m_user='';
		$m_date='';
		if($openid!='' && $openid!=0){
			$m_user=$this->session->userdata('user_name');
			$m_date=$date=date('Y-m-d');
		}
		$data=array(
					'stockid'=>$stockid,
					'expire_date'=>$ex_date,
					'qty'=>$qty,
					'uom'=>$uom,
					'ref_no'=>$ref_no,
					'transdate'=>$date,
					'type'=>8,
					'transno'=>$tranno,
					'whcode'=>$whcode,
					'created_by'=>$user,
					'created_date'=>$date,
					'modified_by'=>$m_user,
					'modified_date'=>$m_date
					);
		$this->db->insert('sch_stock_opening_balance',$data);
		$this->addstockmove($stockid,$qty,$uom,$ex_date,$date,8,$tranno,$updatestock,$whcode);
	}
	function addstockmove($stockid,$qty,$uom,$expire_date,$date,$typeid,$tranno,$updatestock,$whcode){
		$created_by=$this->session->userdata('user_name'); 
		$data=array(
					'stockid'=>$stockid,
					'date'=>$date,
					'quantity'=>$qty,
					'whcode'=>$whcode,
					'uom'=>$uom,
					'expired_date'=>$expire_date,
					'created_by'=>$created_by,
					'type'=>$typeid,
					'transno'=>$tranno
					);
		$this->db->insert('sch_stock_stockmove',$data);
		if($updatestock==1)
			$this->updatestockbalance($stockid,$expire_date,$qty,$whcode);
	}
	function updatestockbalance($stockid,$expire_date,$qty,$whcode){
        $sql="SELECT
                SUM(quantity) AS amount
            FROM
                sch_stock_stockmove
            WHERE stockid='{$stockid}' AND whcode='{$whcode}' AND expired_date='{$expire_date}'
            GROUP BY
                stockid,
                expired_date,
                whcode";
        $qoh=$this->db->query($sql)->row();

		$this->db->query("UPDATE sch_stock_balance SET quantity='".$qoh->amount."' WHERE stockid='$stockid' AND whcode='$whcode' AND expired_date='".$expire_date."'");
			
	}
	function updatetran($sequence,$typeid){
		$this->db->set('sequence',$sequence )
				->where('typeid',$typeid)
				->update('sch_z_systype');
	}
	function newstockbalance($stockid,$ex_date,$qty,$uom,$whcode){
		$data=array('stockid'=>$stockid,
					'expired_date'=>$ex_date,
					'quantity'=>$qty,
					'whcode'=>$whcode,
					'uom'=>$uom,
					'is_active'=>1);
		$this->db->insert('sch_stock_balance',$data);
	}
	function getexpire($stockid,$whcode){
		return $this->db->select('*')
					->from('sch_stock_balance sb')
					->join('sch_stock s','s.stockid=sb.stockid','inner')
					->where('sb.stockid',$stockid)
					->where('sb.whcode',$whcode)
					->get()->result();
	}
	public function search($s_name,$s_date,$e_date,$s_num,$m,$p,$schlevelid){
		$page=0;
		if(isset($_GET['per_page']))
			$page=$_GET['per_page'];
		$where = '';
			if($s_date!='' && $e_date!='')
				$where.=" WHERE sob.transdate BETWEEN '".$s_date."' AND '".$e_date."'
						AND s.descr_eng LIKE '%$s_name%'";
			else
				$where.=" WHERE s.descr_eng LIKE '%$s_name%'";
			if ($schlevelid!='')
				$where.=" AND sob.whcode='$schlevelid'";
			$sql="SELECT * 
						FROM sch_stock_opening_balance sob
						INNER JOIN sch_stock s
						ON(sob.stockid=s.stockid)
						INNER JOIN sch_stock_wharehouse sw
						ON(sob.whcode=sw.whcode) {$where}
						ORDER BY sob.transdate DESC";
				
			
			$config['base_url'] =site_url()."/stock/openstock/search?pg=1&s_name=$s_name&s_date=$s_date&e_date=$e_date&s_num=$s_num&m=$m&p=$p&sl=$schlevelid";	
			$config['total_rows'] = $this->green->getTotalRow($sql);
			$config['per_page'] =$s_num;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);
			$limi=" limit ".$config['per_page'];
			if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}		
				$sql.=" {$limi}";
			return $this->db->query($sql)->result();
	}
}