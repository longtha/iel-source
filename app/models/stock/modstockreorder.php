<?php
    class Modstockreorder extends CI_Model {
        public function getAll($id='')
        {
            	
        }  
		
		function searchstock($stockcode,$descr_eng,$category,$sort,$sort_num,$m,$p){
				$page=0;
				$sql='';
				if(isset($_GET['per_page'])) $page=$_GET['per_page'];
				$this->load->library('pagination');	
				$config['base_url'] = site_url()."/stock/stockreorder/search?s_id=$stockcode&des=$descr_eng&cat=$category&s_num=$sort_num&m=$m&p=$p";
				
				$where="";
				if($stockcode!=""){
					$where=" AND stockcode like '%".$stockcode."%'";
				}
				if($descr_eng!=""){
					$where.=" AND descr_eng like '%".$descr_eng."%'";
				}
				if($category!=""){
					$where.=" AND category like '%".$category."%'";
				}
				

				$sql.="SELECT stock.stockid,
							stock.stockcode,
							stock.descr_eng,
							stock.expired_date,
							stock.reorder_qty,
							stock.quantity,
							stock.uom,
							stock.categoryid,
							stock.category,
							stock.modified_date,
							stock.modified_by
						FROM
							v_stock_reorder AS stock	
						WHERE 1=1 
						{$where}					
						";		
					
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] = $sort_num;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';	
				$this->pagination->initialize($config);	
				$limi=" limit ".$config['per_page'];
				if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}		
				$sql.=" $sort {$limi}";				
				$query=$this->green->getTable($sql);					
				return $query;
		}
		function getOpUOM(){
			return $this->db->get('sch_stock_uom');			
		}
		function getCategory(){
			return $this->db->get('sch_stock_category');
		}
		function getStockRow($stockid){
			$this->db->where('stockid',$stockid);
			$query=$this->db->get('sch_stock');
			return $query->row_array();
		}		
		function getPreview($stockid){
			$this->db->where('stockid',$stockid);
			$query=$this->db->get('v_stock_reorder');
			return $query->row_array();
		}
		function getStockBl($stockid){
			$this->db->select('stockcode,expired_date,quantity,uom');			
			$this->db->where('stockid',$stockid);			
			return $this->db->get('v_stock_reorder');
		}		
		function getWherehouse(){			
			return $this->db->get('sch_stock_wharehouse');			
		}


    }