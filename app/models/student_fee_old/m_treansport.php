<?php
    class m_treansport extends CI_Model{
    // allarea -------------------------------------------------	
		function allarea(){
            return $this->db->query("SELECT DISTINCT
												a.areaid,
												a.area
											FROM
												sch_setup_area AS a
											ORDER BY
												a.area")->result();

		}
		// get_bus ----------------------------------------------
		function get_bus($areaid = ''){
	        $qr_schbus = $this->db->query("SELECT
													b.busid,
													b.busno,
													ba.areaid
												FROM
													sch_setup_busarea AS ba
												LEFT JOIN sch_setup_bus AS b ON b.busid = ba.busid
												WHERE
													ba.areaid = '{$areaid}'
												ORDER BY
													b.busno ASC ")->result();        
	        
	        $arr = array('bus' => $qr_schbus);
	        return json_encode($arr);
	    }
	    // get_driver --------------------------------------------
	    function get_driver($busid = ''){
	        $qr_schdriver = $this->db->query("SELECT DISTINCT
												d.driverid,
												d.driver_name
											FROM
												sch_setup_driver AS d
											WHERE
	                                            d.busid = '{$busid}' - 0
											ORDER BY
												d.driver_name ASC ")->result();        
	        
	        $arr = array('dri' => $qr_schdriver);
	        return json_encode($arr);
	    }
    
    }