<?php
    class m_student_in_grades extends CI_Model{


    	// getprogram ----------------------------------------------
    	function getprograms($programid='3'){
	        if($programid!=""){
	            $this->db->where("programid",$programid);
	        }
        	return $this->db->get("sch_school_program")->result() ;
   		}

   		// get school level ----------------------------------------
    	function get_schlevel($programid = ''){
	        $qr_schlevel = $this->db->query("SELECT DISTINCT
	                                                l.schlevelid,
	                                                l.sch_level
	                                            FROM
	                                                sch_school_level AS l
	                                            WHERE
	                                                l.programid = '{$programid}' - 0
	                                            ORDER BY
	                                                l.sch_level ASC ")->result();        
	        
	        $arr = array('schlevel' => $qr_schlevel);
	        return json_encode($arr);
	    }
	    // get_schyera -------------------------------------------------------------
	    function get_schyera($schlevelid = ''){
	        $qr_schyear = $this->db->query("SELECT DISTINCT
	                                                y.yearid,
	                                                y.sch_year,
	                                                y.schlevelid
	                                            FROM
	                                                sch_school_year AS y
	                                            WHERE
	                                                y.schlevelid = '{$schlevelid}' - 0
	                                            ORDER BY
	                                                y.sch_year ASC ")->result();        
	        
	        $arr = array('schyear' => $qr_schyear);
	        return json_encode($arr);
	    }

	    //from ranglavel--------------------------------------------------
	     function schranglavel($yearid = ''){
	        $qr_schraglavele = $this->db->query("SELECT DISTINCT
													rl.rangelevelid,
													rl.rangelevelname
												FROM
													sch_school_rangelevel AS rl
												INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
												AND rl.programid = y.programid
												WHERE
													y.yearid = '{$yearid}' - 0
												ORDER BY
													rl.rangelevelname ASC")->result();       
	        
	        $arr = array('schranglavel' => $qr_schraglavele);
	        return json_encode($arr);
	    }
	    

	    //from classname ------------------------------------------------------
	    function get_schclass($rangelevelid = ''){
	        $qr_schclass = $this->db->query("SELECT DISTINCT
												ran.rangelevelid,
												ran.rangelevelname,
												racl.classid,
												cla.class_name
											FROM
												sch_school_rangelevelclass AS racl
											LEFT JOIN sch_class AS cla ON racl.classid = cla.classid
											LEFT JOIN sch_school_rangelevel AS ran ON ran.rangelevelid = racl.rangelevelid
											WHERE
												ran.rangelevelid = '{$rangelevelid}' - 0
											ORDER BY
												cla.class_name ASC")->result();        
	        
	        $arr = array('schclass' => $qr_schclass);
	        return json_encode($arr);
	    }
//---------form 2------------------
	    // get school level ----------------------------------------
    	function to_get_schlevel($to_programid = ''){
	        $qr_to_schlevel = $this->db->query("SELECT DISTINCT
	                                                l.schlevelid,
	                                                l.sch_level
	                                            FROM
	                                                sch_school_level AS l
	                                            WHERE
	                                                l.programid = '{$to_programid}' - 0
	                                            ORDER BY
	                                                l.sch_level ASC ")->result();        
	        
	        $arr = array('to_schlevel' => $qr_to_schlevel);
	        return json_encode($arr);
	    }

	    // get_schyera -------------------------------------------------------------
	    function to_get_schyera($to_schlevelids = ''){
	        $qr_to_schyear = $this->db->query("SELECT DISTINCT
	                                                y.yearid,
	                                                y.sch_year,
	                                                y.schlevelid
	                                            FROM
	                                                sch_school_year AS y
	                                            WHERE
	                                                y.schlevelid = '{$to_schlevelids}' - 0
	                                            ORDER BY
	                                                y.sch_year ASC ")->result();        
	        
	        $arr = array('to_schyear' => $qr_to_schyear);
	        return json_encode($arr);
	    }

		//from classname ------------------------------------------------------
		function to_get_schclass($to_raglevelids = ''){
			        $qr_to_schclass = $this->db->query("SELECT DISTINCT
															ran.rangelevelid,
															ran.rangelevelname,
															racl.classid,
															cla.class_name
														FROM
															sch_school_rangelevelclass AS racl
														LEFT JOIN sch_class AS cla ON racl.classid = cla.classid
														LEFT JOIN sch_school_rangelevel AS ran ON ran.rangelevelid = racl.rangelevelid
														WHERE
															ran.rangelevelid = '{$to_raglevelids}' - 0
														ORDER BY
															cla.class_name ASC")->result();        
			        
			        $arr = array('to_schclass' => $qr_to_schclass);
			        return json_encode($arr);
			    }
	    //from ranglavel--------------------------------------------------
	     function get_schranglavel($to_yearid = ''){
	        $qr_schraglavel = $this->db->query("SELECT DISTINCT
													rl.rangelevelid,
													rl.rangelevelname
												FROM
													sch_school_rangelevel AS rl
												INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
												AND rl.programid = y.programid
												WHERE
													y.yearid = '{$to_yearid}' - 0
												ORDER BY
													rl.rangelevelname ASC")->result();        
	        
	        $arr = array('schrang' => $qr_schraglavel);
	        return json_encode($arr);
	    }
	    
	    // save --------------------------------------------------------
        function savedata($enrollid ){ 
        	date_default_timezone_set("Asia/Bangkok");
        	$user = $this->session->userdata('user_name');
            date_default_timezone_set("Asia/Bangkok");
            $enrollid = $this->input->post('enrollid');
            $to_programid = $this->input->post('to_programid');
            $to_schlevelids = $this->input->post('to_schlevelids');
            $to_yearid = $this->input->post('to_yearid');
            $to_raglevelids = $this->input->post('to_raglevelids'); 
            $to_classid = $this->input->post('to_classid');
            $to_date = date('Y-m-d h:m:s');
            $arr = $this->input->post('arr_check');

            if(!empty($arr)){
	            foreach($arr as $r){
	            	$i = $this->db->update('sch_student_enrollment', array('is_pass' => 1), array('studentid' => $r['studentId'])) - 0;

	            	if($i > 0){
		                $data = array('programid'=>$to_programid,     
		             				  'schlevelid'=>$to_schlevelids,     
		             				  'year'=>$to_yearid,         
		             				  'rangelevelid'=>$to_raglevelids,
		             				  'classid'=>$to_classid,
		             				  'enroll_date'=>$to_date,
		             				  'studentid'=>$r['studentId'],
		             				  'schoolid'=>$r['schoolid'],
		             				  'type_of_enroll' => '1',
		             				  'feetypeid' =>'5',
		                   			  'is_rollover' => '0'
		                            );
		                    $this->db->insert('sch_student_enrollment', $data);  
                    }
	                                   
	            }    
            }                   
           
            return $enrollid;
        }
    }