<?php
    class m_history_void_invoice extends CI_Model{
    	function schooinfor($schoolid='1'){
    		return $this->db->query("SELECT DISTINCT
											inf.schoolid,
											inf.`name`
										FROM
											sch_school_infor AS inf
										WHERE
											inf.schoolid = '{$schoolid}' - 0
										ORDER BY
											inf.`name` ASC")->result();
        }

         // paymentmethod ---------------------------------------------------------------
        function paymentmethod(){
            return $this->db->query("SELECT DISTINCT
										f.feetypeid,
										f.schoolfeetype
									FROM
										sch_school_feetype AS f
									WHERE
										1 = 1
									ORDER BY
										f.schoolfeetype ASC")->result();
        }
        
        // getprogram -------------------------------------------------------------
        function getprograms($programid=""){
	        if($programid!=""){
	            $this->db->where("programid",$programid);
	        }
        	return $this->db->get("sch_school_program")->result() ;
   		}


   		// get school level ---------------------------------------------------------
    	function get_schlevel($programid = ''){
	        $qr_schlevel = $this->db->query("SELECT DISTINCT
	                                                l.schlevelid,
	                                                l.sch_level
	                                            FROM
	                                                sch_school_level AS l
	                                            WHERE
	                                                l.programid = '{$programid}' - 0
	                                            ORDER BY
	                                                l.sch_level ASC ")->result();        
	        
	        $arr = array('schlevel' => $qr_schlevel);
	        return json_encode($arr);
	    }
	    // get_schyera -------------------------------------------------------------
	    function get_schyera($schlevelid = ''){
	        $qr_schyear = $this->db->query("SELECT DISTINCT
	                                                y.yearid,
	                                                y.sch_year
	                                            FROM
	                                                sch_school_year AS y
	                                            WHERE
	                                                y.schlevelid = '{$schlevelid}' - 0
	                                            ORDER BY
	                                                y.sch_year ASC ")->result();        
	        
	        $arr = array('schyear' => $qr_schyear);
	        return json_encode($arr);
	    }
	    //from ranglavel---------------------------------------------------------
	     function get_schranglavel($yearid = ''){
	        $qr_schraglavel = $this->db->query("SELECT DISTINCT
													rl.rangelevelid,
													rl.rangelevelname
												FROM
													sch_school_rangelevel AS rl
												INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
												AND rl.programid = y.programid
												WHERE
													y.yearid = '{$yearid}' - 0
												ORDER BY
													rl.rangelevelname ASC")->result();        
	        
	        $arr = array('schrang' => $qr_schraglavel);
	        return json_encode($arr);
	    }
	    //from classname ------------------------------------------------------
	    function get_schclass($schlevelid = ''){
	        $qr_schclass = $this->db->query("SELECT DISTINCT
												sl.schlevelid,
												cl.class_name,
												cl.classid
											FROM
												sch_school_level AS sl
											INNER JOIN sch_class AS cl ON sl.schlevelid = cl.schlevelid
											AND sl.schlevelid = cl.schlevelid
											WHERE
												sl.schlevelid = '{$schlevelid}' - 0
											ORDER BY
												cl.class_name ASC")->result();        
	        
	        $arr = array('schclass' => $qr_schclass);
	        return json_encode($arr);
	    }

    	// get_paymentType --------------------------------------------------------
	  	function get_paymentType($feetypeid = '', $programid = '', $schlevelid = '', $yearid = ''){
	    	if($feetypeid != ''){
	    		$this->db->where("payment_type", $feetypeid);
	    	}
	    	if($programid != ''){
	    		$this->db->where("programid", $programid);
	    	}
	    	if($schlevelid != ''){
	    		$this->db->where("schlevelid", $schlevelid);
	    	}
	    	if($yearid != ''){
	    		$this->db->where("yearid", $yearid);
	    	}  
	        $qr_get_paymentType = $this->db->get("v_study_peroid")->result();
	        return json_encode($qr_get_paymentType);
	    }	



   }