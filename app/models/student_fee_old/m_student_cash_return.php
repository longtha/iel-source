<?php
class m_student_cash_return extends CI_Model{

	function __construct(){
		parent::__construct();

	}


public function FMupdate_return($attr_typeno){  
 
      $datas="SELECT
				sch_student_fee_type.typeno,
				sch_student_fee_type.type,
				sch_student_fee_type.description,
				sch_student_fee_type.`not`,
				sch_student_fee_type.amt_line,
				sch_student_fee_type.prices,
				sch_student_fee_type.amt_dis,
				sch_student_fee_type.otherfee_id,
				sch_student_fee_type.qty
			   FROM
				sch_student_fee_type
			   where 1=1
			   AND sch_student_fee_type.typeno='".$attr_typeno."'
			   AND type=32					
			  ";
     //  echo $datas; exit();
      
      $result = $this->db->query($datas)->result();
      // print_r($classes);
      $trdata=""; 
      $amttotal=0;
      $ii=1;
	  $trs="";
      $arr=array();
      if(isset($result) && count($result)>0){        
       foreach($result as $rowde){
		   //--------------
			$query=$this->db->get('sch_setup_otherfee')->result();
			
			$oppaylist="";
			$oppaylist="<option attr_price='0' value=''></option><option attr_price='create_new' value='create_new'><span style='color:blue'>--- Create New ---</span></option>";
			
			foreach ($query as $rowotherfee) {				
				$oppaylist.="<option ".(($rowde->otherfee_id)==($rowotherfee->otherfeeid)?"selected=selected":"")." attr_typefee='".$rowotherfee->typefee."' attr_datetran='".$this->green->convertSQLDate($rowotherfee->fromdate)."  -  ". $this->green->convertSQLDate($rowotherfee->todate)."' attr_price='".$rowotherfee->prices."' value='".$rowotherfee->otherfeeid."'>".$rowotherfee->otherfee."</option>";
			}
			
			$photodel="<a id='a_delete' class='btn btn-xs btn-danger'><span class='glyphicon glyphicon-trash'></span></a>";

		   //--------------
		  
		   $trdata.='<tr class="tr_addrow">
					<td>
						<select id="paymenttype_list" class="form-control paymenttype_list gform" required="">'.$oppaylist.'</select>
						<input id="paytypeid" class="form-control payment_des" type="text" value="'.$rowde->description.'" style="text-align:left; display:none">
						<input id="is_bus" class="form-control is_bus" type="text" value="" style="text-align:left; display:none">
					</td>
					<td>
						<input id="paytypeid" class="form-control payment-note"  value="'.($rowde->not).'" type="text" placeholder="" style="text-align:right">
					</td>
					<td>
						<input id="numqty" class="form-control numqty" type="text" placeholder="" value="'.($rowde->qty).'"  style="text-align:right">
					</td>
					
					<td>
						<input id="paytypeid" class="form-control payment-unitprice" type="text" placeholder=""  value="'.(-$rowde->prices).'"  style="text-align:right">
					</td>
					<td>
						<input id="paytypeid" class="form-control payment-dis" type="text" placeholder=""  value="'.($rowde->amt_dis).'"  style="text-align:right">
					</td>
					<td>
						<input id="paytypeid" class="form-control payment-amt" type="text" placeholder=""  value="'.(-$rowde->amt_line).'"  style="text-align:right">
					</td>
					<td>&nbsp;&nbsp;&nbsp;<a id="a_delete" class="a_delete a_delete1" >'.$photodel.'</a></td>"+
				</tr>';
		   //--------------
      //  $trdata.='<tr class="tr_addrow"><td>'.($rowde->description).'</td><td>'.($rowde->description).'</td><td>'.($rowde->description).'</td></tr>';
       }
      }
     $arr['tr_loop']=$trdata;     
    header("Content-type:text/x-json");
    echo json_encode($arr); 
   exit();
 }
 
 
function grid(){
		$this->green->setActiveRole($this->input->post('roleid'));
  		$sortby=$this->input->post("sortby");
		$sorttype=$this->input->post("sorttype");
		$offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;
        $limit_record = $this->input->post('limit_record') - 0;        
		$typeno = trim($this->input->post('typeno', TRUE));
		$student_num = trim($this->input->post('student_num', TRUE));
		$student_name = trim($this->input->post('student_name', TRUE));
		$gender = trim($this->input->post('gender', TRUE));
		$schoolid = trim($this->input->post('schoolid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelid = trim($this->input->post('schlevelid', TRUE));		
		$yearid = trim($this->input->post('yearid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$term_sem_year_id = trim($this->input->post('term_sem_year_id', TRUE));
		$feetypeid = trim($this->input->post('feetypeid', TRUE));
		$from_date = trim($this->input->post('from_date', TRUE));
		$to_date = trim($this->input->post('to_date', TRUE));	
		$report_type = $this->input->post('report_type', TRUE);
		$sortstr ='';
		$where = '';

		if($typeno != ''){
			$where .= "AND typeno LIKE '%{$typeno}%' ";
			// $this->db->like('student_num', $student_num, 'both'); 
		}
		if($student_num != ''){
			$where .= "AND student_num LIKE '%{$student_num}%' ";
			// $this->db->like('student_num', $student_num, 'both'); 
		}		
		if($student_name != ''){
			$where .= "AND ( CONCAT(
									first_name,
									' ',
									last_name
								) LIKE '%{$student_name}%' ";
			$where .= "or CONCAT(
									first_name_kh,
									' ',
									last_name_kh
								) LIKE '%{$student_name}%' ) ";			

			// $this->db->like("CONCAT(first_name, ' ', last_name)", $full_name, "both");
		}		
		if($gender != ''){
			$where .= "AND gender = '{$gender}' ";
			// $this->db->where("gender", $gender);
		}
		if($schoolid != ''){
			$where .= "AND schoolid = '{$schoolid}' ";
			// $this->db->where("schoolid", $schoolid);
		}
		if($programid != ''){
			$where .= "AND programid = '{$programid}' ";
			// $this->db->where("programid", $programid);
		}
		if($schlevelid != ''){
			$where .= "AND schooleleve = '{$schlevelid}' ";
			// $this->db->where("schlevelid", $schlevelid);
		}
		if($yearid != ''){
			$where .= "AND acandemic = '{$yearid}' ";
			// $this->db->where("year", $yearid);
		}
		if($rangelevelid != ''){
			$where .= "AND ranglev = '{$rangelevelid}' ";
			// $this->db->where("rangelevelid", $rangelevelid);
		}
		if($classid != ''){
			$where .= "AND classid = '{$classid}' ";
			// $this->db->where("classid", $classid);
		}
		if($feetypeid != ''){
			$where .= "AND paymentmethod = '{$feetypeid}' ";
			// $this->db->where("feetypeid", $feetypeid);
		}
		
		if($term_sem_year_id != ''){
			$where .= "AND paymenttype = '{$term_sem_year_id}' ";
			// $this->db->where("term_sem_year_id", $term_sem_year_id);
		}	
			
		if($report_type != ''){
			$where .= "AND is_returned = '{$report_type}' ";
			// $this->db->where("term_sem_year_id", $term_sem_year_id);
		}	
			
		if($from_date != ''){
			$where .= "AND date(trandate) >= '".$this->green->formatSQLDate($from_date)."' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') >=", $from_date);
		}
		if($to_date != ''){
			$where .= "AND date(trandate) <= '".$this->green->formatSQLDate($to_date)."' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') <=", $to_date);
		}

		(isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";
		// count ==========
		$qr_c = $this->db->query("SELECT COUNT(student_num) AS c FROM v_student_cash_return
									WHERE 1=1 {$where} {$sortstr}")->row()->c - 0;
		$totalRecord = $qr_c - 0;		
		$totalPage = ceil($totalRecord/$limit);

		// result =======  
		$qr = $this->db->query("SELECT
									v_student_cash_return.student_num,
									v_student_cash_return.first_name,
									v_student_cash_return.last_name,
									v_student_cash_return.first_name_kh,
									v_student_cash_return.last_name_kh,
									v_student_cash_return.program,
									v_student_cash_return.rangelevelname,
									v_student_cash_return.class_name,
									v_student_cash_return.sch_level,
									v_student_cash_return.type,
									v_student_cash_return.typeno,
									v_student_cash_return.studentid,
									DATE_FORMAT(trandate, '%d/%m/%Y') AS trandate,
									v_student_cash_return.schooleleve,
									v_student_cash_return.amt_balance,
									v_student_cash_return.duedate,
									v_student_cash_return.amt_total,
									v_student_cash_return.amt_paid,
									v_student_cash_return.programid,
									v_student_cash_return.schoolid,
									v_student_cash_return.acandemic,
									v_student_cash_return.classid,
									v_student_cash_return.ranglev,
									v_student_cash_return.is_status,
									v_student_cash_return.enrollid,
									v_student_cash_return.sch_year,
									v_student_cash_return.gender,
									v_student_cash_return.period,
									v_student_cash_return.paymentmethod,
									v_student_cash_return.paymenttype,
									v_student_cash_return.termid,
									v_student_cash_return.typeno_inv,
									v_student_cash_return.type_inv,
									v_student_cash_return.is_returned,
									v_student_cash_return.timeid,
									v_student_cash_return.note
								FROM
									v_student_cash_return
										
								WHERE 1=1 AND type=17 {$where} {$sortstr}

								LIMIT $offset,$limit");

		$i = 1;
		$tr = '';
		$gamt_total=0;
		$gamt_paid=0;
		$gamt_balance=0;
			
		if($qr->num_rows() > 0){			
			foreach($qr->result() as $row){
				
				$counrow="SELECT
								COUNT(typeno) as typeno
							FROM
								sch_student_fee_rec_order
							WHERE
								1=1
								AND type=18
								AND type_inv='".$row->type."'
								AND typeno_inv='".$row->typeno."'
							";
		// echo $counrow; exit();
		
				$ispayexist = $this->db->query($counrow)->row()->typeno;

				
					$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
					$no_imgs = base_url()."assets/upload/students/NoImage.png";	
					
					$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
					if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
						$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
					}
					
					$liks='<div class="tr_studentcode create_inv" isaddnewinv="0" attr_student_num="'.$row->student_num.'" attr_enroid="'.$row->enrollid.'" attr_stuid="'.$row->studentid.'" data-target="#mdinvoice" data-toggle="modal"><b class="btn btn-xs btn-success">Return</b></div>';
					
					$tr .= '<tr style="color: '.($row->is_returned == 1 ? 'red' : '').';">
								<td>'.($i++).'</td>
								<td style="text-align: center;">
									'.$img.'
								</td>
								<td>
									'.$row->student_num.'<br>
									 '.$row->last_name_kh.' '.$row->first_name_kh.'<br>
									'.$row->last_name.' '.$row->first_name.' <br>
									'.ucfirst($row->gender).'
									 <br>
								</td>
								<td>'.$row->trandate.'</td>	
								<td>'.$row->sch_level.'</td>
								<td>'.$row->sch_year.'</td>
								<td>'.$row->rangelevelname.'</td>
								<td>'.$row->class_name.'</td>
								<td>'.$row->period.'</td>
								<td style="text-align:center;">
									<a href="'.site_url().'/student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno.'" target="_blank" data-typeno="'.$row->typeno.'" class="a_">#'.$row->typeno.'</a>
								</td>
								<td style="text-align:center;">'.number_format($row->amt_total, 2).'</td>
								<td style="text-align:center;">'.number_format($row->amt_paid, 2).'</td>
								<td style="text-align:center;">'.number_format($row->amt_balance, 2).'</td>
								<td class="remove_tag no_wrap">'.$liks.'</td>
							</tr>';	

				$gamt_total+=$row->amt_total;
				$gamt_paid+=$row->amt_paid;
				$gamt_balance+=$row->amt_balance;			
			}
			// foreach master =====
			$tr .= '<tr><td colspan="10" style="font-weight: bold;text-align: right;" class="gamt">Total : </td>
					<td class="gamt">'.number_format($gamt_total,2).'$</td>
					<td class="gamt">'.number_format($gamt_paid,2).'$</td>
					<td class="gamt">'.number_format($gamt_balance,2).'$</td>
					<td  class="gamt"></td></tr>';
						
		}else{
			$tr .= '<tr><td colspan="13" style="font-weight: bold;text-align: center;">We did not find anything to show here...</tr>';
		}

		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage); 
		// $arr['tr'] = $tr;
        return json_encode($arr);		
	}

	public function Fcstudentinfo($enrollid,$isaddnewinv,$studentid,$attr_is_pt){
			$typeno_h =$this->input->post('typeno_h');
    		//$this->db->where("enrollid", $enrollid);
    		 $this->db->where("studentid", $studentid);
        $getrowInf = $this->db->get("v_student_fee_create_inv")->row();
		// print_r($getrowInf); exit();
		//----------------------------
		
		$sqltime="SELECT
							sch_time.from_time,
							sch_school_rangelevtime.rangelevelid,
							sch_time.am_pm,
							sch_time.timeid,
							sch_time.to_time
							FROM
							sch_school_rangelevtime
							INNER JOIN sch_time ON sch_school_rangelevtime.timeid = sch_time.timeid
							where 1=1 
							AND rangelevelid='".$getrowInf->rangelevelid."'
						  ";
		// echo $sqltime; exit();
			
		$result = $this->db->query($sqltime)->result();
		 // print_r($classes);
		
		$pttime="";	
		if(isset($result) && count($result)>0){		
			foreach($result as $myrows){
				$pttime.="<option value='".$myrows->timeid."'>".$myrows->from_time."-".$myrows->to_time."&nbsp;".$myrows->am_pm."</option>";
			}
		}
		$arr['studytime']=$pttime;
		
		//----------------------------
		
		$countmethod="SELECT
					COUNT(studentid) AS studentid
					FROM
						sch_student_fee
					WHERE 1=1
					AND studentid='".$studentid."'
					AND programid='".$getrowInf->programid."'
					AND schooleleve='".$getrowInf->schlevelid."'
					AND acandemic='".$getrowInf->year."'
					AND ranglev='".$getrowInf->rangelevelid."'
					AND classid='".$getrowInf->classid."'
					AND paymentmethod <> ''
					";
		// echo $countmethod; exit();
		
		$ispayexist = $this->db->query($countmethod)->row()->studentid;
		$arr['ispayexist']=$ispayexist;	
		$arr['is_pt']=$attr_is_pt;	
				
		$photodel='<a class="btn btn-xs btn-danger" id="a_delete"><span class="a_delete glyphicon glyphicon-trash"></span></a>';
		
		$selesql="SELECT
						v_student_fee_create_receipt.student_num,
						v_student_fee_create_receipt.typeno,
						v_student_fee_create_receipt.type,
						v_student_fee_create_receipt.first_name,
						v_student_fee_create_receipt.last_name,
						v_student_fee_create_receipt.first_name_kh,
						v_student_fee_create_receipt.last_name_kh,
						v_student_fee_create_receipt.phone1,
						v_student_fee_create_receipt.is_status,
						v_student_fee_create_receipt.amt_total,
						v_student_fee_create_receipt.amt_paid,
						v_student_fee_create_receipt.amt_balance,
						v_student_fee_create_receipt.studentid
					FROM
						v_student_fee_create_receipt
					WHERE 1=1
					AND studentid='".$studentid."'
					AND type=32
					-- AND type_inv=17
					-- AND typeno_inv='".$typeno_h."'
					";
				// echo $selesql;
		$invOrders=$this->db->query($selesql)->result();
		
		
		$trinv="";
		// print_r($invOrders);
		$gtotal=0;
		if(isset($invOrders) && count($invOrders)>0){
			$i=1;
			$theads="";
			$update="";
			foreach($invOrders as $row){
				$fclassact='fclassact';
				$is_status=($row->is_status);
				$edite ='<a  id="a_edite" attr_id='.$row->typeno.' class="btn btn-xs btn-success"><span class="glyphicon glyphicon-pencil" data-placement="top" title="Edite..."></span> </a>';
				$delete='<a  id="a_delete_inv"  attr_id='.$row->typeno.'  attr_type='.$row->type.' style="text-align:center" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash" data-placement="top" title="Delete..."></span></a>';

				if($this->green->gAction("D")){	
				$theads=$delete;
				}
				if($this->green->gAction("D")){	
				$update=$edite;
				}
				$theads=$delete;
				$update=$edite;
				$linkprint=($row->typeno);
				$trinv.='<tr class="active cl_inv">
							<td>
								<table  >
									<tr>
										<td><strong><span style="color:red;">'.($i++).'</span></strong>. <a href='.site_url('student_fee/c_print_cash_return?FCprint_inv='.$row->typeno.'').' class="btn btn-xs btn-success"><span class="glyphicon glyphicon-print" data-placement="top" title="Print..."></span> # '.$linkprint.'</a></td>
										
									</tr>
									
									<tr class="tr_inv">
										<td colspan="2"></td>
									</tr>								
								</table>
							</td>
							<td colspan="3" align="right">'.$update.'</td>
							<td colspan="1" align="right">'.$theads.'</td>
						 </tr>';
						 
						$datas="SELECT
								sch_student_fee_type.typeno,
								sch_student_fee_type.type,
								sch_student_fee_type.description,
								sch_student_fee_type.`not`,
								sch_student_fee_type.amt_line,
								sch_student_fee_type.prices,
								sch_student_fee_type.amt_dis,
								sch_student_fee_type.qty
							FROM
								sch_student_fee_type
							where 1=1
							AND sch_student_fee_type.typeno='".$row->typeno."'
							AND prices <> ''
						
						";
						// echo $datas; exit();
						
						$result = $this->db->query($datas)->result();
						// print_r($classes);
						$trinvd="";	
						$amttotal=0;
						$ii=1;
						if(isset($result) && count($result)>0){	
						$des="";	
							foreach($result as $rowde){
								if($rowde->not !=""){
									$des=$rowde->description.'-'.$rowde->not;	
								}else{
									$des=$rowde->description;
								}
								$trinv.='<tr class="info">
									<td>'.$des.'</td>
									<td align="right">'.$rowde->qty.'</td>
									<td align="right">'.number_format($rowde->prices,2).'&nbsp;$</td>
									<td align="right">'.$rowde->amt_dis.' %</td>
									<td align="right">'.number_format($rowde->amt_line,2).'&nbsp;$</td>
								 </tr>';
								 $amttotal+=($rowde->amt_line);
							}
							
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="'.$fclassact.'">Total</td>
									<td align="right" class="'.$fclassact.'">'.number_format($amttotal,2).'&nbsp;$</td>
								 </tr>';
							
						$gtotal+=($amttotal)-0;		 
				}// if(isset($result) && count($result)>0){
					
			}// foreach($invOrders as $row){
				$trinv.='<tr>
							<td></td>
							<td></td>
							
							<td></td>
							<td align="right" class="'.$fclassact.'"><strong>Grand&nbsp;Total</strong></td>
							<td align="right" class="'.$fclassact.'"><strong>'.number_format($gtotal,2).'&nbsp;$</strong></td>
						 </tr>';
			$arr['trinv']='<a style=" text-align:left;"  class="btn btn-block btn-sm btn-success"><span class="glyphicon glyphicon-signal"></span> History of Student Fee</a>
							<table class="tbl_history " style="width:100% !important;">
								
								<tr>
									<th style="text-align:left"  class="td_history">Description</th>
									<th style="text-align:right" class="td_history" align="right">Quantity</th>
									<th style="text-align:right" class="td_history" align="right">Unit Price</th>
									<th style="text-align:right" class="td_history" align="right">Dis(%)</th>
									<th style="text-align:right" class="td_history" align="right">Amount</th>
								</tr>

								'.$trinv.'
							</table>';
		}// if(isset($invOrders) && count($invOrders)>0){
					   
		if(isset($getrowInf) && count($getrowInf)>0){
			
			$have_img = base_url()."assets/upload/students/".$getrowInf->year.'/'.$getrowInf->studentid.'.jpg';		
			$no_imgs = base_url()."assets/upload/students/NoImage.png";	
			
			$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:155px;height:155px;">';
			if (file_exists(FCPATH . "assets/upload/students/".$getrowInf->year.'/'.$getrowInf->studentid.'.jpg')) {				
				$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:155px;height:155px;">';
			}
			
		 $arr['image_stu']=$img;
		
		 $arr['getRow']=$getrowInf;
		  return json_encode($arr);
		}

		//header("Content-type:text/x-json");
		// echo json_encode($arr);
	}


	public function MFsave($typeno = ''){
		$schoolid=$this->session->userdata("schoolid");
		$typeno_h =$this->input->post('typeno_');		
		$trandate = $this->input->post('trandate');
		$arr_inv = $this->input->post('arr_inv');
		$curdate=date('Y-m-d H:i:s');
		$user=$this->session->userdata('user_name');
		$amt_balance = $this->input->post('amt_balance');
		$editehiden = $this->input->post('editehiden');	
		$text_note = $this->input->post('text_note');
	
		$mo = '';
		$wh=array('type'=>17,'typeno'=>$typeno_h);	
        $mo = $this->db->where($wh)->get('sch_student_fee')->row();

		$arrD=array('type' => 32, 'typeno' => $editehiden);	
		$this->db->where($arrD);	
		$this->db->delete('sch_student_fee_type');

		$this->db->where($arrD);
		$this->db->delete('sch_student_fee');
      //print_r($mo);
      // die();
		$created_date = date('Y-m-d H:i:s');
		//---------
		$type=32;	
		
		if($editehiden !=""){// update
			$nextTran=$editehiden;
		}else{			
			$nextTran=$this->green->nextTran($type,"Create Cash Return");
		}
		$da = array(					
					'type'=>$type,					
					'typeno'=>$nextTran,
					'type_inv'=>$mo->type,									
					'typeno_inv'=>$mo->typeno,
					'ranglev'=>$mo->ranglev,					
					'studentid'=>$mo->studentid,
					'trandate'=>$mo->trandate,
					'classid'=>$mo->classid,
					'schooleleve'=>$mo->schooleleve,
					'termid'=>$mo->termid,
					'amt_total'=>($amt_balance),
					'amt_paid'=>($amt_balance),
					'duedate'=>$mo->duedate,
					'programid'=>$mo->programid,
					'acandemic'=>$mo->acandemic,
					'schoolid'=>$mo->schoolid,
					'paymentmethod'=>$mo->paymentmethod,
					'paymenttype'=>$mo->paymenttype,
					'areaid'=>$mo->areaid,
					'busfeetypid'=>$mo->busfeetypid,
					'note'=>$text_note
					);
		// echo $mo->typeno;
		// die();
				$this->db->insert('sch_student_fee', $da);
				$updateold = array('is_returned'=>1);
				// $this->db->where($wh);				
				$this->db->where($wh)->update('sch_student_fee',$updateold);
				// db->where($wh)
				
		if($arr_inv != ""){			  
			foreach($arr_inv as $arr_detail){			
				$datas = array(
					'type' => $type,
					'typeno' =>$nextTran,
					'studentid' =>$this->input->post('stuid'),				
					'trandate'=>$created_date,
	                'otherfee_id' => $arr_detail['paymenttype_list'],
					'description' => $arr_detail['payment_des'],
					'not' => $arr_detail['payment_note'],
	                'prices' => ($arr_detail['prices']),
	                'amt_dis' => ($arr_detail['amt_dis']),
	                'qty' => $arr_detail['numqty'],
	                'is_bus' => $arr_detail['is_bus'],
	                'areaid' => $this->input->post('oparea'),
	                'busid' => $this->input->post('opbusfeetype'),	               
					'amt_line' => ($arr_detail['payment_amt'])					
				);

				$this->db->insert('sch_student_fee_type', $datas);
			};// end foreach($arr_inv as $arr_detail){ ==================
		}// if($arr_inv != ""){
			$gettypeno['nextTran']=$nextTran;
			header("Content-type:text/x-json");
			echo json_encode($gettypeno);
			exit();
	}
}
?>

