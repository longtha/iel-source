<?php

class M_student_fee extends CI_Model
{
    function getresponrow($respon_id)
    {
        $sql_page = "SELECT * FROM sch_student_responsible WHERE respondid=$respondid";
    }

    function saveImport()
    {
        $is_imported = false;
        if (isset($_FILES['file']) && $_FILES['file']["tmp_name"][0] != "") {
            $_xfile = $_FILES['file']["tmp_name"];
            function str($str)
            {
                return str_replace("'", "''", $str);
            }

            if ($_FILES['file']['tmp_name'] == "") {
                $error = "<font color='red'>Please Choose your Excel file!</font>";
            } else {
                $html = "";
                require_once(APPPATH . 'libraries/simplexlsx.php');
                foreach ($_xfile as $index => $value) {
                    $xlsx = new SimpleXLSX($value);
                    $_data = $xlsx->rows();
                    array_splice($_data, 0, 1);
                    $error_record_exist = "";
                    foreach ($_data as $k => $r) {
                        $_check_exist = $this->green->getTable("SELECT * FROM sch_student WHERE student_num = '{$r[0]}'");
                        $seachspance = strpos(trim(str($r[0])), " ");
                        if (count($_check_exist) > 0) {
                            $error_record_exist .= '<div class="success" style=" padding:5px; margin-top:5px;">
														<font style="color:red;">Code already exist' . $r[0] . ' exist aleady !</font><br>
														</div>';
                        } else if ($seachspance !== false) {
                            $error_record_exist .= '<div class="success" style=" padding:5px; margin-top:5px;">
													<font style="color:red;">Error Code' . str($r[0]) . ' is incorrect. Please try again !</font></div>';
                        } else {

                            $student_num = trim(str($r[0]));
                            $first_name = trim(str($r[1]));
                            $last_name = trim(str($r[2]));
                            $first_name_kh = trim(str($r[3]));
                            $last_name_kh = trim(str($r[4]));
                            $dob = trim(str($r[6]));
                            $classid = trim(str($r[7]));

                            $nationality = trim(str($r[8]));
                            $religion = trim(str($r[9]));
                            $province = trim(str($r[10]));
                            $district = trim(str($r[11]));
                            $commune = trim(str($r[12]));

                            $village = trim(str($r[13]));
                            $zoon = trim(str($r[14]));
                            $permanent_adr = trim(str($r[15]));

                            $from_school = trim(str($r[16]));
                            $measure = trim(str($r[17]));

                            $boarding_school = trim(str($r[18]));
                            $boarding_reason = trim(str($r[19]));
                            $leave_school = trim(str($r[20]));
                            $leave_reason = trim(str($r[21]));
                            $family_revenue = trim(str($r[22]));
                            $comments = trim(str($r[23]));
                            $familyid = trim(str($r[24]));
                            $phone1 = trim(str($r[25]));
                            $email = trim(str($r[26]));
                            $login_username = trim(str($r[27]));
                            $SQL_DM = "INSERT INTO sch_student";

                            $SQL = " SET student_num='" . $student_num . "',
											first_name='" . $first_name . "',
											last_name='" . $last_name . "',
											last_name_kh='" . $last_name_kh . "',
											first_name_kh='" . $first_name_kh . "',
											dob='" . $dob . "',
											classid='" . $classid . "',
											nationality='" . $nationality . "',
											religion='" . $religion . "',
											province='" . $province . "',
											commune='" . $commune . "',
											district='" . $district . "',
											village='" . $village . "',
											zoon='" . $zoon . "',
											permanent_adr='" . $permanent_adr . "',
											from_school='" . $from_school . "',
											measure='" . $measure . "',
											boarding_school='" . $boarding_school . "',
											boarding_reason='" . $boarding_reason . "',
											leave_school='" . $leave_school . "',
											leave_reason='" . $leave_reason . "',
											family_revenue='" . $family_revenue . "',
											comments='" . $comments . "',
											familyid='" . $familyid . "',
											phone1='" . $phone1 . "',
											`email`='" . $email . "',
											login_username='" . $login_username . "'
										";
                            $counts = $this->green->runSQL($SQL_DM . $SQL);
                            $is_imported = true;

                        }
                    }
                }
            }
        }
        return $is_imported;
    }

    function saveImportenroll()
    {
        $is_imported = false;
        if (isset($_FILES['file']) && $_FILES['file']["tmp_name"][0] != "") {
            $_xfile = $_FILES['file']["tmp_name"];
            function str($str)
            {
                return str_replace("'", "''", $str);
            }

            if ($_FILES['file']['tmp_name'] == "") {
                $error = "<font color='red'>Please Choose your Excel file!</font>";
            } else {
                $html = "";
                require_once(APPPATH . 'libraries/simplexlsx.php');
                foreach ($_xfile as $index => $value) {
                    $xlsx = new SimpleXLSX($value);
                    $_data = $xlsx->rows();
                    array_splice($_data, 0, 1);
                    $error_record_exist = "";
                    foreach ($_data as $k => $r) {
                        $_check_exist = $this->green->getTable("SELECT * FROM sch_student_enrollment WHERE studentid = '{$r[0]}' AND year = '{$r[2]}' AND classid = '{$r[3]}'");
                        $seachspance = strpos(trim(str($r[0])), " ");
                        if (count($_check_exist) > 0) {
                            $error_record_exist .= '<div class="success" style=" padding:5px; margin-top:5px;">
														<font style="color:red;">Code already exist' . $r[0] . ' exist aleady !</font><br>
														</div>';
                        } else if ($seachspance !== false) {
                            $error_record_exist .= '<div class="success" style=" padding:5px; margin-top:5px;">
													<font style="color:red;">Error Code' . str($r[0]) . ' is incorrect. Please try again !</font></div>';
                        } else {

                            $studentid = trim(str($r[0]));
                            $schoolid = trim(str($r[1]));
                            $year = trim(str($r[2]));
                            $classid = trim(str($r[3]));
                            $enroll_date = trim(str($r[4]));
                            $SQL_DM = "INSERT INTO sch_student_enrollment";

                            $SQL = " SET studentid='" . $studentid . "',
											schoolid='" . $schoolid . "',
											year='" . $year . "',
											classid='" . $classid . "',
											enroll_date='" . $enroll_date . "'
										";
                            $counts = $this->green->runSQL($SQL_DM . $SQL);
                            $is_imported = true;

                        }
                    }
                }
            }
        }
        return $is_imported;
    }

    
	function MFsave($typeno = ''){
		$schoolid=$this->session->userdata("schoolid");
		
		$student_num = $this->input->post('student_num');
		$arr_inv = $this->input->post('arr_inv');
		$duedate = $this->input->post('duedate');
		
		
		// $is_condic=$this->input->post('is_condic');
		$is_condic=$this->input->post('h_iscond_save');		
		
		
		$programid=$this->input->post('opprpid');
		$yearid = $this->input->post('acandemicid');
		$schoolid=$this->session->userdata('schoolid');
		$schlevelid = $this->input->post('shlevelid');
		$oppaymentmethod = $this->input->post('oppaymentmethod');
		
		$type=17;
		
		$nextTran=$this->green->nextTran($type,"Student Fee");
		
		$created_date = date('Y-m-d H:i:s');
		
		if($arr_inv != ""){								  
		foreach($arr_inv as $arr_valinv){			
			$datas = array(
				'type' => $type,
				'typeno' =>$nextTran,
				'studentid' =>$this->input->post('stuid'),
				'trandate'=>$created_date,
				'description' => $arr_valinv['payment_des'],
				'not' => $arr_valinv['payment_note'],
                'prices' => $arr_valinv['prices'],
                'amt_dis' => $arr_valinv['amt_dis'],
                'qty' => $arr_valinv['numqty'],
                'otherfee_id' => $arr_valinv['paymenttype_list'],
				'is_bus' => $arr_valinv['is_bus'],
				'areaid' => $this->input->post('oparea'),
				'busid' => $this->input->post('opbusfeetype'),
				'amt_line' => $arr_valinv['payment_amt']
			);	
			$this->db->insert('sch_student_fee_type', $datas);
			
			if(($this->input->post('amt_paid'))>0){
				
				$datarece = array(
				'type' => $type,
				'typeno' =>$nextTran,
				'type_inv' => $type,
				'typeno_inv' =>$nextTran,
				'studentid' =>$this->input->post('stuid'),
				'trandate'=>$created_date,
				'description' => $arr_valinv['payment_des'],
				'not' => $arr_valinv['payment_note'],
                'prices' => $arr_valinv['prices'],
                'amt_dis' => $arr_valinv['amt_dis'],
                'qty' => $arr_valinv['numqty'],
				'otherfee_id' => $arr_valinv['paymenttype_list'],
				'is_bus' => $arr_valinv['is_bus'],
				'areaid' => $this->input->post('oparea'),
				'busid' => $this->input->post('opbusfeetype'),
				'amt_line' => $arr_valinv['payment_amt']
				);		
				$this->db->insert('sch_student_fee_rec_detail', $datarece);	
			};
			
		};// end foreach($arr_inv as $arr_valinv){ ==================
		}// if($arr_inv != ""){
			
		$data = array(
				'type' => $type,
				'typeno' =>$nextTran,
				'studentid' =>$this->input->post('stuid'),
				'trandate'=>$created_date,
				
				'amt_total' => $this->input->post('amt_total'),
				
				'amt_balance' => $this->input->post('amt_balance'),
				'duedate' => $this->green->formatSQLDate($duedate),
				
				'schoolid' => $schoolid,
				'programid' => $this->input->post('opprpid'),
				'schooleleve' => $this->input->post('shlevelid'),
				'acandemic' => $this->input->post('acandemicid'),
				'ranglev' => $this->input->post('ranglevid'),
				'classid' => $this->input->post('classe'),
				
				'paymentmethod' => $this->input->post('oppaymentmethod'),
				'paymenttype' => $this->input->post('paymenttype'),
				'areaid' => $this->input->post('oparea'),
				'busfeetypid' => $this->input->post('opbusfeetype')
       			);
			$this->db->insert('sch_student_fee', $data);
			
			if(($this->input->post('amt_paid'))>0){	
				$datarecipt = array(
				'type' => $type,
				'typeno' =>$nextTran,
				'type_inv' => $type,
				'typeno_inv' =>$nextTran,			
				'studentid' =>$this->input->post('stuid'),
				'trandate'=>$created_date,
				'amt_paid' => $this->input->post('amt_paid'),
				'schoolid' => $schoolid,
				'programid' => $this->input->post('opprpid'),
				'schooleleve' => $this->input->post('shlevelid'),
				'acandemic' => $this->input->post('acandemicid'),
				'ranglev' => $this->input->post('ranglevid'),
				'classid' => $this->input->post('classe'),
				
				'paymentmethod' => $this->input->post('oppaymentmethod'),
				'paymenttype' => $this->input->post('paymenttype'),
				'areaid' => $this->input->post('oparea'),
				'busfeetypid' => $this->input->post('opbusfeetype')
				);
				$this->db->insert('sch_student_fee_rec_order', $datarecipt);
			}
			
			// update student info
			// $this->db->set('classid',$this->input->post('classe'));
			// $this->db->set('zoon',$this->input->post('oparea'));
        	// $this->db->where('studentid',$this->input->post('stuid'));
        	// $this->db->update('sch_student');
			
			
			 if($oppaymentmethod !=0){
			$upinfo="UPDATE sch_student SET
						is_new_old='".$oppaymentmethod."'
						WHERE 1=1
						AND studentid='".$this->input->post('stuid')."'
						AND current_yearid='".$this->input->post('acandemicid')."'
						AND classid='".$this->input->post('classe')."'
						AND current_schoollevelid='".$this->input->post('shlevelid')."'
						AND current_ranglevelid='".$this->input->post('ranglevid')."'
					  ";
					//  echo $upinfo;
					  $this->green->runSQL($upinfo);
			 }
			if($is_condic==1){ 
				// 1-insert new record by select for paymentmethod
				//---------------------------------------------------------------------------------------------------------
				$this->db->where('studentid', $this->input->post('stuid'));
				$this->db->where('programid',$programid);
				$this->db->where('schlevelid',$schlevelid);
				$this->db->where('year',$yearid);
				$this->db->where('rangelevelid', $this->input->post('ranglevid'));
				$this->db->where('classid', $this->input->post('classe'));	
							
       			$this->db->delete('sch_student_enrollment');
				
				
				/*$whselec = '';
				$mo = $this->db->where('studentid',$this->input->post('stuid'))
				->where('programid',$programid)
				->where('schlevelid',$schlevelid)
				->where('year',$yearid)
				->where('rangelevelid',$this->input->post(''))
				->where('',$this->input->post(''))				
				->get('sch_student_enrollment')->row();*/
				//---------------------------------------------------------------------------------------------------------			
				$datas="SELECT
							v_study_peroid.term_sem_year_id
							FROM
							v_study_peroid
						where 1=1
						AND v_study_peroid.programid='".$programid."'
						AND v_study_peroid.yearid='".$yearid."'
						AND v_study_peroid.schoolid='".$schoolid."'
						AND v_study_peroid.schlevelid='".$schlevelid."'
						AND v_study_peroid.payment_type='".$oppaymentmethod."'
						";
				// echo $datas; exit();
				$result = $this->db->query($datas)->result();
				$datapayment="";	
				if(isset($result) && count($result)>0){		
					foreach($result as $rowbusfee){
						 $da = array(									
									'studentid' =>$this->input->post('stuid'),
									'schoolid'=>$schoolid,
									'year' => $this->input->post('acandemicid'),
									'classid' => $this->input->post('classe'),
									'schlevelid' => $this->input->post('shlevelid'),
									'programid' => $this->input->post('opprpid'),
									'feetypeid' => $oppaymentmethod,
									'rangelevelid' => $this->input->post('ranglevid'),
									'term_sem_year_id' => $rowbusfee->term_sem_year_id,
									'enroll_date' => $created_date
									);
							$this->db->insert('sch_student_enrollment', $da);
					}
				}// foreach($result as $rowbusfee){
				
			}else if($is_condic ==2){ //  end new creade
				// 1-update old recorded. 2-insert new record by select for paymentmethod
				//---------------------------------------------------------------------------------------------------------					
				$datas="SELECT
							v_study_peroid.term_sem_year_id
							FROM
							v_study_peroid
						where 1=1
						AND v_study_peroid.programid='".$programid."'
						AND v_study_peroid.yearid='".$yearid."'
						AND v_study_peroid.schoolid='".$schoolid."'
						AND v_study_peroid.schlevelid='".$schlevelid."'
						AND v_study_peroid.payment_type='".$oppaymentmethod."'
						";
				// echo $datas; exit();
				$result = $this->db->query($datas)->result();
				$datapayment="";	
				if(isset($result) && count($result)>0){		
					foreach($result as $rowbusfee){
						 $da = array(									
									'studentid' =>$this->input->post('stuid'),
									'schoolid'=>$schoolid,
									'year' => $this->input->post('acandemicid'),
									'classid' => $this->input->post('classe'),
									'schlevelid' => $this->input->post('shlevelid'),
									'programid' => $this->input->post('opprpid'),
									'feetypeid' => $oppaymentmethod,
									'rangelevelid' => $this->input->post('ranglevid'),
									'term_sem_year_id' => $rowbusfee->term_sem_year_id,
									'enroll_date' => $created_date
									);
							$this->db->insert('sch_student_enrollment', $da);
					}
				}// foreach($result as $rowbusfee){
					
				$updatsql="UPDATE sch_student_enrollment SET 
						is_closed=1
						WHERE 1=1
						AND studentid='".$this->input->post('stuid')."'
						AND schoolid='".$schoolid."'
						AND year='".$this->input->post('acandemicid')."'
						AND classid='".$this->input->post('classe')."'
						AND schlevelid='".$this->input->post('shlevelid')."'
						AND rangelevelid='".$this->input->post('ranglevid')."'
						AND feetypeid <>'".$oppaymentmethod."'
					  ";
					//  echo $updatsql;
					  $this->green->runSQL($updatsql);
					  
			} // }else if($is_condic ==2){
			//---------------------------------------------------------------------------------------------------------	
			$updatsql="UPDATE sch_student_enrollment SET 
						type='".$type."',
						transno='".$nextTran."',
						is_paid=1
						WHERE 1=1
						AND studentid='".$this->input->post('stuid')."'
						AND schoolid='".$schoolid."'
						AND year='".$this->input->post('acandemicid')."'
						AND classid='".$this->input->post('classe')."'
						AND schlevelid='".$this->input->post('shlevelid')."'
						AND rangelevelid='".$this->input->post('ranglevid')."'
						AND feetypeid='".$oppaymentmethod."'
						AND term_sem_year_id='".$this->input->post('paymenttype')."'
					  ";
			// echo $updatsql;
			$this->green->runSQL($updatsql);
			
			if(($this->input->post('amt_paid'))>0){
				$print_receipt=1;
			}else{
				$print_receipt=0;
				}
			
			
			$gettypeno['nextTran']=$nextTran;
			$gettypeno['print_receipt']=$print_receipt;
			
			header("Content-type:text/x-json");
			echo json_encode($gettypeno);
	}
	
	//=============================================================================================================================================================================================================
	
	//=============================================================================================================================================================================================================
	
	function save($studentid = '', $update_mem = '')
    {
        $student_num = $this->input->post('student_num');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $last_name_kh = $this->input->post('last_name_kh');
        $first_name_kh = $this->input->post('first_name_kh');
        $dob = $this->green->convertSQLDate($this->input->post('dob'));
        $classid = $this->input->post('classid');

        $moeys_id = $this->input->post('moeys_id');
        $leave_school_date = $this->input->post('leave_sch_date');

        $nationality = $this->input->post('nationality');
        $religion = $this->input->post('religion');

        $province = $this->input->post('province');
        $commune = $this->input->post('commune');
        $district = $this->input->post('district');
        $village = $this->input->post('village');
        $zoon = $this->input->post('zoon');
        $permanent_adr = $this->input->post('permanent_adr');

        $from_school = $this->input->post('from_school');
        $measure = $this->input->post('measure');
        $boarding_school = $this->input->post('boarding_school');
        $boarding_reason = $this->input->post('boarding_reason');
        $leave_school = $this->input->post('leave_school');
        $leave_reason = $this->input->post('leave_reason');
        $family_revenue = $this->input->post('family_revenue');
        $comments = $this->input->post('comments');
        $familyid = $this->input->post('familyid');
        $phone1 = $this->input->post('phone1');
        $email = $this->input->post('email');
        $promo_id = $this->input->post('promot_id');
        $boarding_date = $this->input->post('boarding_date');

        $is_vtc = 0;
        if ($promo_id != '')
            $is_vtc = 1;

        $login_username = $this->input->post('login_username');
        $password = $this->input->post('password');
        $memberid = $this->input->post('student');
        $year = $this->input->post('year');
        $created_by = $this->session->userdata('user_name');
        $created_date = date('Y-m-d H:i:s');
        $SQL_WHERE = " ,created_date='" . $created_date . "', created_by='" . $created_by . "'";
        $SQL_DM = "";
        if ($studentid != "") {

            $SQL_DM = "UPDATE sch_student ";
            // $this->saveuser($login_username);
            $last_modified_by = $this->session->userdata('user_name');
            $last_modified_date = date('Y-m-d H:i:s');
            $SQL_WHERE = " ,last_modified_by='" . $last_modified_by . "', last_modified_date='" . $last_modified_date . "'  WHERE studentid = '" . $studentid . "'";

        } else {
            $SQL_DM = "INSERT INTO sch_student";
            // $this->saveuser();
        }
        $SQL = " SET student_num='" . $student_num . "',
					first_name='" . $first_name . "',
					last_name='" . $last_name . "',
					last_name_kh='" . $last_name_kh . "',
					first_name_kh='" . $first_name_kh . "',
					dob='" . $dob . "',
					classid='" . $classid . "',
					nationality='" . $nationality . "',
					religion='" . $religion . "',
					province='" . $this->db->escape_str($province) . "',
					commune='" . $this->db->escape_str($commune) . "',
					district='" . $this->db->escape_str($district) . "',
					village='" . $this->db->escape_str($village) . "',
					zoon='" . $this->db->escape_str($zoon) . "',
					permanent_adr='" . $this->db->escape_str($permanent_adr) . "',
					from_school='" . $from_school . "',
					measure='" . $measure . "',
					boarding_school='" . $boarding_school . "',
					boarding_reason='" . $this->db->escape_str($boarding_reason) . "',
					leave_school='" . $leave_school . "',
					leave_reason='" . $this->db->escape_str($leave_reason) . "',
					family_revenue='" . $family_revenue . "',
					comments='" . $this->db->escape_str($comments) . "',
					familyid='" . $familyid . "',
					phone1='" . $phone1 . "',
					promot_id='" . $promo_id . "',
					is_vtc='" . $is_vtc . "',
					moeys_id='" . $moeys_id . "',
					leave_sch_date='" . $this->green->formatSQLDate($leave_school_date) . "',
					`email`='" . $email . "',
					login_username='" . $login_username . "',
					match_memid='" . $memberid . "',
					boarding_date='" . $this->green->formatSQLDate($boarding_date) . "'
						
				";
        $this->green->runSQL($SQL_DM . $SQL . $SQL_WHERE);
        if ($studentid == '') {
            $studentid = $this->db->insert_id();
            //++++++++++++++++++++++++save enrollment ++++++++++++++++++++++
            $this->saveenrollment($studentid, $year);
            //+++++++++++++++++++++++++++++++++++++++++++++++
            if ($update_mem == 'yes') {
                $this->updatestdmember($studentid, $last_name, $first_name, $last_name_kh, $first_name_kh, $dob);
            } else {
                $this->updatestdinmember($memberid, $studentid);
            }

        } else {
            $this->updateenrollment($studentid, $year);
            if ($update_mem == 'yes') {
                $this->updatestdmember($studentid, $last_name, $first_name, $last_name_kh, $first_name_kh, $dob);
            }
            $memcount = $this->db->query("SELECT count(memid) as count FROM sch_student_member where studentid='$studentid'")->row()->count;
            if ($memcount == 0)
                $this->updatestdmember($studentid, $last_name, $first_name, $last_name_kh, $first_name_kh, $dob, $familyid);
            $this->db->set('familyid', $familyid)->where('studentid', $studentid)->update('sch_student_member');
            $this->clearmember($studentid);
            // $this->clearrespon($studentid);
            $this->clearstdlink($studentid);
        }
        $this->moeys($studentid, $classid, $moeys_id, $year);
        $arrmem = $this->input->post('mem_id');//fetch member data
        // $arrres=$this->input->post('res_id') ;//fecth respondsible data
        $arrlink = $this->input->post('link_id');//fetch student link data
        //++++++++++++++++++++++++read member / respondsible / student link from form+++++++++++++++++++++++
        for ($i = 0; $i < count($arrmem); $i++) {
            $g_level = '';
            $mem_id = $this->input->post('mem_id');
            $mem_relat = $this->input->post('mem_relationship');
            if ($mem_id[$i] != 0 || $mem_id[$i] != '')
                $g_level = $this->getmemberow($mem_id[$i])->grade_level;
            $this->savemember($studentid, $mem_id[$i], $mem_relat[$i], $g_level);
        }
        // for($j=0;$j<count($arrres);$j++){
        // 		$res_id=$this->input->post('res_id');
        // 		$res_relat=$this->input->post('res_relationship');
        // 		$this->saverespon($studentid,$res_id[$j],$res_relat[$j]);
        // }
        for ($k = 0; $k < count($arrlink); $k++) {
            $std_id = $this->input->post('link_id');
            $this->savestdlink($studentid, $std_id[$k]);
        }
        //++++++++++++++++++++++++ end of read member / respondsible / student link from form+++++++++++++++++++++++
        return $studentid;
    }

    function saveenrollment($studentid, $year)
    {
        $schoolid = $this->session->userdata('schoolid');
        $enroll_date = date('Y-m-d H:i:s');
        $year = $year;
        $type_of_enroll = 1;
        $is_rollover = 0;
        $data = array(
            'studentid' => $studentid,
            'schoolid' => $schoolid,
            'year' => $year,
            'classid' => $this->input->post('classid'),
            'enroll_date' => $enroll_date,
            'type_of_enroll' => $type_of_enroll,
            'is_rollover' => $is_rollover,
        );
        $this->db->insert('sch_student_enrollment', $data);
    }

    function updatestdmember($studentid, $last_name, $first_name, $last_name_kh, $first_name_kh, $dob, $familyid = '')
    {
        $sex = 'F';
        $data = array(
            'studentid' => $studentid,
            'last_name' => $last_name,
            'first_name' => $first_name,
            'last_name_kh' => $last_name_kh,
            'first_name_kh' => $first_name_kh,
            'dob' => $dob,
            'is_active' => 1
        );
        if ($familyid != '') {
            $data2 = array("familyid" => $familyid,
                "sex" => $sex);
            $this->db->insert('sch_student_member', array_merge($data, $data2));
        } else {
            $this->db->where('studentid', $studentid);
            $this->db->update('sch_student_member', $data);
        }

    }

    function updatestdinmember($memberid, $studentid)
    {
        $this->db->set('studentid', $studentid);
        $this->db->where('memid', $memberid);
        $this->db->update('sch_student_member');
    }

    function updateenrollment($studentid, $year)
    {
        $schoolid = 1;
        $enroll_date = date('Y-m-d H:i:s');
        $year = $year;
        $type_of_enroll = 1;
        $is_rollover = 0;
        $data = array(
            'studentid' => $studentid,
            'schoolid' => $schoolid,
            'year' => $year,
            'classid' => $this->input->post('classid'),
            'enroll_date' => $enroll_date,
            'type_of_enroll' => $type_of_enroll,
            'is_rollover' => $is_rollover,
        );
        $this->db->where('studentid', $studentid)
            ->where('schoolid', $schoolid)
            ->where('year', $year)->update('sch_student_enrollment', $data);
    }

    function clearmember($studentid)
    {
        $this->db->where('studentid', $studentid);
        $this->db->delete('sch_student_mem_detail');
    }

    function clearstdlink($studentid)
    {
        $this->db->where('studentid', $studentid);
        $this->db->delete('sch_student_link_othstudent');
    }

    function moeys($studentid, $classid, $moeys, $yearid)
    {
        $classrow = $this->db->where('classid', $classid)->get('sch_class')->row();
        $count = $this->green->getValue("SELECT count(*) as count
									FROM sch_student_moeysid
									WHERE studentid='$studentid'
									AND schlevelid='$classrow->schlevelid'
									AND yearid='$yearid'
									");
        $data = array('studentid' => $studentid,
            'schlevelid' => $classrow->schlevelid,
            'yearid' => $yearid,
            'moeys_id' => $moeys);
        if ($count > 0)
            $this->db->where('stdid', $studentid)->update('sch_student_moeysid', $data);
        else
            $this->db->insert('sch_student_moeysid', $data);
    }

    function getmemberow($memberid)
    {
        $this->db->where('memid', $memberid);
        $query = $this->db->get('sch_student_member');
        return $query->row();
    }

    function savemember($stu_id, $memid, $relation, $g_level)
    {
        $data = array(
            'studentid' => $stu_id,
            'memberid' => $memid,
            'relationship' => $relation,
            'grade_level' => $g_level
        );
        if ($memid != 0)
            $this->db->insert('sch_student_mem_detail', $data);
    }

    function savestdlink($studentid, $stdlink_id)
    {
        $data = array(
            'studentid' => $studentid,
            'link_studentid' => $stdlink_id,
        );
        if ($stdlink_id != 0)
            $this->db->insert('sch_student_link_othstudent', $data);
    }

    function getmoeys($studentid, $schlevelid, $yearid)
    {
        $mo = '';
        $mo = $this->db->where('studentid', $studentid)
            ->where('schlevelid', $schlevelid)
            ->where('yearid', $yearid)
            ->get('sch_student_moeysid')->row();
        return $mo;
    }

    function getsponsor($studentid)
    {
        return $this->db->select('sd.sponsorid,sd.studentid,sd.stdspid,sd.start_sp_date,sd.end_sp_date,fs.last_name,fs.first_name')
            ->from('sch_student_sponsor_detail sd')
            ->join('sch_family_sponsor fs', 'sd.sponsorid=fs.sponsorid', 'inner')
            ->where('sd.studentid', $studentid)->get()->result();
    }

    function saverespon($stu_id, $res_id, $relat)
    {
        $data = array(
            'studentid' => $stu_id,
            'respondid' => $res_id,
            'relationship' => $relat,
            'created_date' => date('Y-m-d H:i:s'),
            'created_by' => 1
        );
        if ($res_id != 0)
            $this->db->insert('sch_student_resp_detail', $data);
    }

    function clearrespon($studentid)
    {
        $this->db->where('studentid', $studentid);
        $this->db->delete('sch_student_resp_detail');
    }

    function getrelation($studentid, $memberid)
    {
        $this->db->where('memberid', $memberid);
        $this->db->where('studentid', $studentid);
        $select_relat = $this->db->get('sch_student_mem_detail');
        if ($select_relat->num_rows() > 0) {
            $row = $select_relat->row();
            return $row->relationship;
        }
        return '';
    }

    function getfather($studentid)
    {
        $relat = "father's child";
        $query = $this->db->query("SELECT rp.respondid,rp.last_name,rp.first_name,rp.last_name_kh,rp.first_name_kh,DATE_FORMAT(rp.dob,'%d/%m/%Y') as dob,rp.occupation,rp.revenue,rpd.relationship FROM sch_student_responsible rp
					INNER JOIN sch_student_resp_detail rpd
					ON(rp.respondid=rpd.respondid)
					where rpd.studentid='$studentid'
					AND rpd.relationship='father`s child'
					");
        return $query->row_array();
    }

    function getmother($studentid)
    {
        $relat = "father's child";
        $this->db->select('*');
        $this->db->from('sch_student_responsible rp');
        $this->db->join('sch_student_resp_detail rpd', 'rp.respondid=rpd.respondid', 'right');
        $this->db->where('rpd.studentid', $studentid);
        $this->db->where('rpd.relationship', 'mother`s child');
        $query = $this->db->get();
        return $query->row_array();
    }

    function getresbystudent($studentid)
    {
        $this->db->select('*');
        $this->db->from('sch_student_responsible stdr');
        $this->db->join('sch_student_resp_detail stdrd', 'stdr.respondid=stdrd.respondid', 'inner');
        $this->db->where('studentid', $studentid);
        $query = $this->db->get();
        return $query->result();
    }

    function getstdlinkrowbystd($studentid)
    {
        $this->db->select('s.studentid,s.last_name,s.first_name,s.last_name_kh,c.class_name');
        $this->db->from('sch_student s');
        $this->db->join('sch_class c', 's.classid=c.classid', 'left');
        $this->db->join('sch_student_link_othstudent lstd', 's.studentid=lstd.link_studentid', 'inner');
        $this->db->where('lstd.studentid', $studentid);
        $query = $this->db->get();
        return $query->result();
    }

    function getmembystudent($studentid)
    {
        $this->db->select('*');
        $this->db->from('sch_student_member stdm');
        $this->db->join('sch_student_mem_detail stdmd', 'stdm.memid=stdmd.memberid', 'inner');
        $this->db->where('studentid', $studentid);
        $query = $this->db->get();
        return $query->result();
    }

    function getvalidateuser($username)
    {
        $this->db->select('count(*)');
        $this->db->from('sch_user');
        $this->db->where('user_name', $username);
        return $this->db->count_all_results();
    }

    function getstudentrow($studentid)
    {
        $this->db->where('studentid', $studentid);
        $query = $this->db->get('sch_student');
        return $query->row_array();
    }

    function previewstd($studentid = '', $yearid, $classid = '')
    {
        $where = '';
        if ($studentid != '')
            $where .= " AND s.studentid='$studentid'";
        if ($classid != '')
            $where .= " AND c.classid='$classid'";
        $query = $this->db->query("SELECT f.family_code,
											s.match_memid,
											s.studentid,
											s.nationality,
											DATE_FORMAT(s.dob,'%d-%m-%Y') as dob,
											s.student_num,
											s.last_name,
											s.first_name,
											s.last_name_kh,
											s.first_name_kh,
											s.permanent_adr,
											s.familyid,
											s.promot_id,
											pro.proname,
											c.classid,
											c.class_name
									FROM sch_student s 
									LEFT JOIN sch_family f 
									ON (s.familyid=f.familyid)
									LEFT JOIN sch_student_enrollment se
									ON(s.studentid=se.studentid)
									INNER JOIN sch_class c 
									ON (se.classid=c.classid)
									LEFT JOIN sch_school_promotion pro ON pro.promot_id=s.promot_id
									WHERE se.year='$yearid' {$where}");

        //$query=$this->db->get($query);
        if ($studentid != '')
            return $query->row_array();
        else
            return $query->result_array();
    }

    function saveuser($userlogin = '')
    {
        $schoolid = 1;
        $creat_date = date('Y-m-d H:i:s');
        $year = date('Y');
        $f_name = $this->input->post('first_name');
        $l_name = $this->input->post('last_name');
        $username = $this->input->post('login_username');
        $schoolid = 1;
        $pwd = md5($this->input->post('password'));
        if ($userlogin != '') {
            $u_row = $this->getuserrow($username);
            $pass = $this->input->post('password');
            if ($u_row->password != $pass) {
                $data = array(
                    'first_name' => $f_name,
                    'last_name' => $l_name,
                    'password' => $pwd,
                    'roleid' => 5,
                    'created_date' => $creat_date,
                    'year' => $year,
                    'is_active' => 1
                );
            } else {
                $data = array(
                    'first_name' => $f_name,
                    'last_name' => $l_name,
                    'roleid' => 5,
                    'created_date' => $creat_date,
                    'year' => $year,
                    'is_active' => 1
                );
            }
        } else {
            $data = array(
                'first_name' => $f_name,
                'last_name' => $l_name,
                'user_name' => $username,
                'password' => $pwd,
                'schoolid' => $schoolid,
                'roleid' => 5,
                'created_date' => $creat_date,
                'year' => $year,
                'is_active' => 1
            );
        }
        if ($userlogin != '') {
            $this->db->where('user_name', $userlogin);
            $this->db->update('sch_user', $data);
        } else {
            $this->db->insert('sch_user', $data);
        }
    }

    function getuserrow($user_name)
    {
        $this->db->where('user_name', $user_name);
        $query = $this->db->get('sch_user u');
        return $query->row();
    }

    function validatestudent($familyid, $memberid)
    {
        $this->db->select('count(*)');
        $this->db->from('sch_student');
        $this->db->where('familyid', $familyid);
        $this->db->where('match_memid', $memberid);
        return $this->db->count_all_results();
    }

    function validatestudentupdate($familyid, $memberid, $studentid)
    {
        $this->db->select('count(*)');
        $this->db->from('sch_student');
        $this->db->where('familyid', $familyid);
        $this->db->where('match_memid', $memberid);
        $this->db->where_not_in('studentid', $studentid);
        return $this->db->count_all_results();
    }

    function getclass()
    {
        return $this->db->get('sch_class');
    }

    function getfamilyrow($familyid)
    {
        $this->db->where('familyid', $familyid);
        $query = $this->db->get('sch_family');
        return $query->row();
    }

    function getmaxid()
    {
        $this->db->select_max('student_num', 'max');
        $this->db->from('sch_student');
        $query = $this->db->get();
        return $query->row()->max + 2;
    }

    function getresponfamily($familyid)
    {
        $this->db->where('familyid', $familyid);
        $this->db->where('is_active', 1);
        return $this->db->get('sch_student_responsible')->result();
    }

    function getmemberfamily($familyid, $memberid, $studentid)
    {
        if ($studentid > 0) {
            $mem = $this->db->query("SELECT memid FROM sch_student_member WHERE studentid='$studentid'")->row_array();
            if (isset($mem['memid']))
                $memberid = $mem['memid'];
        }
        $this->db->select("*");
        $this->db->from("sch_student_member m");
        $this->db->where('m.familyid', $familyid);
        $this->db->where('m.is_active', 1);
        if ($memberid != '')
            $this->db->where('m.memid !=', $memberid);
        return $this->db->get()->result();
    }

    function getexp($studentid, $full_name, $full_name_kh, $classid, $sort, $year, $schlevelid, $level, $sort_num, $page, $is_all)
    {

        $sql = "SELECT *
    					FROM
    						v_student_profile s	
    					LEFT JOIN sch_family f
    					ON(s.familyid=f.familyid)						
    					where student_num like '%$studentid%' 
    					AND (last_name LIKE '%" . trim($full_name) . "%'
    						OR first_name LIKE '%" . trim($full_name) . "%'
    						OR s.fullname LIKE '%" . trim($full_name) . "%'
    						OR s.fullname_kh LIKE '%" . trim($full_name) . "%'
    						)
    					AND s.last_name_kh LIKE '%" . trim($full_name_kh) . "%'
    					AND s.is_active=1	AND s.yearid=$year ";
        if ($classid != '')
            $sql .= " AND s.classid=$classid ";
        if ($schlevelid != '')
            $sql .= "  AND s.schlevelid=$schlevelid ";
        if ($level != '')
            $sql .= "  AND s.grade_levelid=$level ";
        $limi = '';
        if ($is_all == 0) {
            $limi = " limit $sort_num";
            if ($page != '' && $page > 0) {
                $limi = " limit " . $page . "," . $sort_num;
            }
        }

        $sql .= " $sort {$limi}";
        return $this->db->query($sql)->result_array();
    }

    function searchstudent($boarding, $studentid, $full_name, $full_name_kh, $class_id, $sort, $sort_num, $year, $schlevelid, $level, $m, $p, $ag, $promot_id)
    {
        $page = 0;
        $sql = '';
        if (isset($_GET['per_page'])) $page = $_GET['per_page'];
        $this->load->library('pagination');
        $config['base_url'] = site_url() . "/student/student/search?s_id=$studentid&fn=$full_name&fnk=$full_name_kh&class=$class_id&s_num=$sort_num&year=$year&m=$m&p=$p&l=$schlevelid&b=$boarding&le=$level&ag=$ag&pro=$promot_id";

        $where="";
        if($full_name!=""){
            $where.=" AND (last_name LIKE '%". trim($full_name) . "%'
                            OR first_name LIKE '%". trim($full_name) . "%'
                            OR fullname LIKE '%". trim($full_name) . "%'
                            )";
        }
        if($full_name_kh!=""){
            $where.=" AND (last_name_kh LIKE '%". trim($full_name_kh) . "%'
                        OR first_name_kh LIKE '%". trim($full_name_kh) . "%'
                        OR fullname_kh LIKE '%". trim($full_name_kh) . "%'
                        )";
        }


        $sql .= "SELECT s.studentid,
						s.student_num,
						s.last_name,
						s.first_name,
						s.last_name_kh,
						s.first_name_kh,
						s.dob as dob,
						DATE_FORMAT(s.dob,'%Y/%m/%d') as dobs,
						s.nationality,
						s.class_name,
						s.classid,
						s.dateofbirth,
						s.sch_year,
						s.familyid,
						s.yearid,
                        s.promot_id
					FROM
						v_student_profile s							
					where student_num like '%$studentid%' 
					{$where}
					AND is_active=1  AND s.yearid=$year ";
        if ($class_id != '')
            $sql .= " AND s.classid=$class_id ";
        if ($boarding != '')
            $sql .= " AND boarding_school='$boarding' ";
        if ($schlevelid != '')
            $sql .= "  AND s.schlevelid=$schlevelid ";
        if ($level != '')
            $sql .= "  AND s.grade_levelid=$level ";
        if ($ag != '') {
            $str_age = explode("__", $ag);
            if ($str_age[0] !== "") {
                $sql .= "  AND s.age >=$str_age[0]";
            }
            if ($str_age[1] !== "") {
                $sql .= "  AND s.age <=$str_age[1]";
            }
        }
        if ($promot_id != '' && $promot_id != '0'){
            $sql .= "  AND s.promot_id='$promot_id'  AND is_vtc=1 ";

        }
        
        $config['total_rows'] = $this->green->getTotalRow($sql);
		
        $config['per_page'] = $sort_num;
        $config['num_link'] = 5;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<li>';
        $config['full_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<a><u>';
        $config['cur_tag_close'] = '</u></a>';
        $this->pagination->initialize($config);
        $limi = '';
        if ($sort_num != 'all') {
            $limi = " limit " . $config['per_page'];
            if ($page > 0) {
                $limi = " limit " . $page . "," . $config['per_page'];
            }
        }

        $sql .= " $sort {$limi}";
        $query = $this->green->getTable($sql);
        
        return $query;
    }
	//--------------
	function getstudent()
    {
    	$studentid = $this->input->post('stuid_pos');
		$typeno = $this->input->post('typeno');
				
		$this->db->where("studentid",$studentid);
        return $this->db->get('sch_student')->row();

    }
	
	function getpaymentmethod(){
		return $this->db->get('sch_school_feetype')->result();
	}
	
    function getstudents($student_num="",$classid="",$studentid="")
    {
    	if($student_num!=""){
    		$this->db->where("student_num",$student_num);
    	}
    	if($classid!=""){
    		$this->db->where("classid",$classid);
    	}
    	if($studentid!=""){
    		$this->db->where("studentid",$studentid);
    	}
        return $this->db->get('v_student_info')->result();
    }
	
	
    function MgetReceiptlist($student_num="",$paymentmethod="",$studentid="")
    {
    	if($student_num!=""){
    		$this->db->where("student_num",$student_num);
    	}
    	if($paymentmethod!=""){
    		$this->db->where("paymentmethod",$paymentmethod);
    	}
    	if($studentid!=""){
    		$this->db->where("typeno_inv",$typeno_inv);
    	}
        return $this->db->get('v_sch_student_receip_list')->result();
    }
	
	
	function MgetfeeList($studentid="",$classid="")
    {
    	if($studentid!=""){
    		$this->db->where("studentid",$studentid);
    	}
		
    	if($classid!=""){
    		$this->db->where("classid",$classid);
    	}
    	
        return $this->db->get('sch_student_fee')->result();

    }
	//--------------
	function FMbusfee($areaid="")
	{
		if($areaid!=""){
        $this->db->where("areaid",$areaid);
        }
		$query=$this->db->get('sch_setup_busfee');
		return $query->result();
	}
	
	function FMinvoiclist($studentid="")
	{
		if($studentid!=""){
        $this->db->where("studentid",$studentid);
        }
		$query=$this->db->get('v_sch_student_fee_inv');
		return $query->result();
	}
	
	function FMinvOrders($studentid="")
	{
		if($studentid!=""){
        $this->db->where("studentid",$studentid);
        }
		$query=$this->db->get('v_inv_order');
		return $query->result();
	}
	
	function FMDelinv($typeno="")
	{	
	//--------- stor history deleted	
	$curdate=date('Y-m-d H:i:s');
	$user=$this->session->userdata('user_name');
		$mo = '';
        $mo = $this->db->where('typeno', $typeno)->get('sch_student_fee')->row();
		$da = array(
					'deleted_byuser'=>$user,
					'deleted_date'=>$curdate,
					'type'=>$mo->type,					
					'typeno'=>$mo->typeno,
					'studentid'=>$mo->studentid,
					'trandate'=>$mo->trandate,
					'classid'=>$mo->classid,
					'schooleleve'=>$mo->schooleleve,
					'termid'=>$mo->termid,
					'amt_total'=>$mo->amt_total,
					'amt_paid'=>$mo->amt_paid,
					'amt_balance'=>$mo->amt_balance,
					'duedate'=>$mo->duedate,
					'programid'=>$mo->programid,
					'acandemic'=>$mo->acandemic,
					'schoolid'=>$mo->schoolid,
					'paymentmethod'=>$mo->paymentmethod,
					'paymenttype'=>$mo->paymenttype,
					'areaid'=>$mo->areaid,
					'busfeetypid'=>$mo->busfeetypid
					);
			   $this->db->insert('sch_student_fee_deleted', $da);
			   
		$datas="SELECT
					sch_student_fee_type.type,
					sch_student_fee_type.typeno,
					sch_student_fee_type.studentid,
					sch_student_fee_type.trandate,
					sch_student_fee_type.description,
					sch_student_fee_type.`not`,
					sch_student_fee_type.amt_line,
					sch_student_fee_type.prices,
					sch_student_fee_type.amt_dis,
					sch_student_fee_type.qty
					FROM
					sch_student_fee_type
					where 1=1
					AND sch_student_fee_type.typeno='".$typeno."'
				";
		// echo $datas; exit();
			
		$result = $this->db->query($datas)->result();
		if(isset($result) && count($result)>0){	
			foreach($result as $rowdel){
			$SQL_DM = "INSERT INTO sch_student_fee_type_deleted";			
			$SQL = " SET studentid='".$rowdel->studentid."',
						 trandate='".$rowdel->trandate."',
						 type='".$rowdel->type."',
						 typeno='".$rowdel->typeno."',						 
						 description='".$rowdel->description."',
						 `not`='".$rowdel->not."',
						 qty='".$rowdel->qty."',
						 prices='".$rowdel->prices."',
						 amt_dis='".$rowdel->amt_dis."',
						 amt_line='".$rowdel->amt_line."'
						 ";
			$counts = $this->green->runSQL($SQL_DM . $SQL);
			}
		};	
		//---- end stor history deleted		
		$this->db->where('typeno', $typeno);
        $this->db->delete('sch_student_fee');
	}
	//--------------
	//------------------
	function getinvlists($student_num="",$is_status="",$studentid="")
    {
    	if($student_num!=""){
    		$this->db->where("student_num",$student_num);
    	}
    	if($is_status!=""){
    		$this->db->where("is_status",$is_status);
    	}
    	if($studentid!=""){
    		$this->db->where("studentid",$studentid);
    	}
        return $this->db->get('v_inv_order')->result();
    }	
	//------------------
}