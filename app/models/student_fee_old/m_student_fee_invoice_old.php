<?php
class M_student_fee_invoice extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function grid(){
		$offset = $this->input->post('offset') - 0;
        $limit_record = $this->input->post('limit_record') - 0;

		$student_num = trim($this->input->post('student_num', TRUE));
		$full_name = trim($this->input->post('full_name', TRUE));
		$full_kh = trim($this->input->post('full_kh', TRUE));
		$gender = trim($this->input->post('gender', TRUE));
		$schoolid = trim($this->input->post('schoolid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelid = trim($this->input->post('schlevelid', TRUE));
		$term_sem_year_id = trim($this->input->post('term_sem_year_id', TRUE));
		$feetypeid = trim($this->input->post('feetypeid', TRUE));
		$yearid = trim($this->input->post('yearid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$is_paid = trim($this->input->post('is_paid', TRUE));
		$is_closed = trim($this->input->post('is_closed', TRUE));		
		$from_date = trim($this->input->post('from_date', TRUE));
		$to_date = trim($this->input->post('to_date', TRUE));	

		$where = '';

		if($student_num != ''){
			// $where .= "AND st.student_num LIKE '%{$student_num}%' ";
			$this->db->like('student_num', $student_num, 'both'); 
		}
		
		if($full_name != ''){
			// 	$where .= "AND CONCAT(
			// 						st.first_name,
			// 						' ',
			// 						st.last_name
			// 					) LIKE '%{$full_name}%' ";
			$this->db->like("CONCAT(first_name, ' ', last_name)", $full_name, "both");
		}	
		if($full_kh != ''){
			// 	$where .= "AND CONCAT(
			// 						st.first_name,
			// 						' ',
			// 						st.last_name
			// 					) LIKE '%{$full_name}%' ";
			$this->db->like("CONCAT(first_name_kh, ' ', last_name_kh)", $full_kh, "both");
		}		
		if($gender != ''){
			// $where .= "AND er.gender = '{$gender}' ";
			$this->db->where("gender", $gender);
		}
		if($schoolid != ''){
			// $where .= "AND er.schoolid = '{$schoolid}' ";
			$this->db->where("schoolid", $schoolid);
		}
		if($programid != ''){
			// $where .= "AND er.programid = '{$programid}' ";
			$this->db->where("programid", $programid);
		}
		if($schlevelid != ''){
			// $where .= "AND er.schlevelid = '{$schlevelid}' ";
			$this->db->where("schlevelid", $schlevelid);
		}
		if($yearid != ''){
			// $where .= "AND er.year = '{$yearid}' ";
			$this->db->where("year", $yearid);
		}		
		if($term_sem_year_id != ''){
			// $where .= "AND er.term_sem_year_id = '{$term_sem_year_id}' ";
			$this->db->where("term_sem_year_id", $term_sem_year_id);
		}
		if($feetypeid != ''){
			// $where .= "AND er.feetypeid = '{$feetypeid}' ";
			$this->db->where("feetypeid", $feetypeid);
		}
		if($rangelevelid != ''){
			// $where .= "AND er.rangelevelid = '{$rangelevelid}' ";
			$this->db->where("rangelevelid", $rangelevelid);
		}
		if($classid != ''){
			// $where .= "AND er.classid = '{$classid}' ";
			$this->db->where("classid", $classid);
		}
		if($is_paid != ''){
			// $where .= "AND er.is_paid = '{$is_paid}' ";
			$this->db->where("is_paid", $is_paid);
		}
		/*if($is_closed != ''){
			// $where .= "AND er.is_closed = '{$is_closed}' ";
			$this->db->where("is_closed", $is_closed);
		}*/

		if($from_date != ''){
			// $where .= "AND DATE_FORMAT(er.enroll_date,'%d/%m/%Y') >= '{$from_date}' ";
			$this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') >=", $from_date);
		}
		if($to_date != ''){
			// $where .= "AND DATE_FORMAT(er.enroll_date,'%d/%m/%Y') <= '{$to_date}' ";
			$this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') <=", $to_date);
		}

		// count ==========
		// $qr_c = $this->db->get("v_student_fee_reminder_period");
		// $totalRecord = $qr_c->num_rows() - 0;		
		// $totalPage = ceil($totalRecord/$limit);
		// $limit_record=8;
		if($limit_record!=''){
	      $this->db->limit($limit_record,0);	       
	    }	

		// result =======
		$qr = $this->db->get("v_student_fee_create_inv");
		
		// echo "v_student_fee_create_inv:".$qr; exit();
		$result = $qr->result();		

		$totalRecord = $qr->num_rows() - 0;		
		$totalPage = 1;

		$arr = array('result' => $result, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
        return json_encode($arr);
	}

	function get_schlevel($programid = ''){
        $qr_schlevel = $this->db->query("SELECT DISTINCT
                                                l.schlevelid,
                                                l.sch_level
                                            FROM
                                                sch_school_level AS l
                                            WHERE
                                                l.programid = '{$programid}' - 0
                                            ORDER BY
                                                l.sch_level ASC ")->result();        
        
        $arr = array('schlevel' => $qr_schlevel);
        return json_encode($arr);
    }

    function get_year($programid = '', $schlevelid = ''){
        $qr_year = $this->db->query("SELECT DISTINCT
                                            y.yearid,
                                            y.sch_year
                                        FROM
                                            sch_school_year AS y
                                        WHERE
                                            y.programid = '{$programid}' - 0 AND y.schlevelid = '{$schlevelid}' - 0
                                        ORDER BY
                                            y.sch_year ASC ")->result();

        $qr_class = $this->db->query("SELECT
													c.classid,
													c.class_name
												FROM
													sch_school_level AS l
												INNER JOIN sch_class AS c ON l.schlevelid = c.schlevelid
												WHERE
													l.schlevelid = '{$schlevelid}'
												ORDER BY
													c.class_name ASC ")->result();
        
        $arr = array('year' => $qr_year, 'class' => $qr_class);
        return json_encode($arr);
    }

    // get rangelevel =========
    function get_rangelevel($yearid = ''){
        $qr_rangelevel = $this->db->query("SELECT DISTINCT
                                                rl.rangelevelid,
                                                rl.rangelevelname,
                                                rp.rangelevelid AS rangelevelid_period
                                            FROM
                                                sch_school_rangelevel AS rl
                                            INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
                                            AND rl.programid = y.programid
                                            LEFT JOIN sch_rangelevel_period AS rp ON rl.rangelevelid = rp.rangelevelid
                                            WHERE
                                                y.yearid = '{$yearid}' 
                                            ORDER BY rl.rangelevelname ASC ")->result();

        $arr = array('rangelevel' => $qr_rangelevel);
        return json_encode($arr);
    }

    // get_paymentType =========
    public function get_paymentType($feetypeid = '', $programid = '', $schlevelid = '', $yearid = ''){
    	if($feetypeid != ''){
    		$this->db->where("payment_type", $feetypeid);
    	}
    	if($programid != ''){
    		$this->db->where("programid", $programid);
    	}
    	if($schlevelid != ''){
    		$this->db->where("schlevelid", $schlevelid);
    	}
    	if($yearid != ''){
    		$this->db->where("yearid", $yearid);
    	}    	

        $qr_get_paymentType = $this->db->get("v_study_peroid")->result();
        return json_encode($qr_get_paymentType);
    }
	
	public function Fcstudentinfo($enrollid,$studentid){
		
		
    		$this->db->where("enrollid", $enrollid);
    	
    	
    		// $this->db->where("studentid", $studentid);
    	
        $getrowInf = $this->db->get("v_student_fee_create_inv")->row();
		// print_r($getrowInf); exit();
		
		
		$countmethod="SELECT
					COUNT(studentid) AS studentid
					FROM
						sch_student_fee
					WHERE 1=1
					AND studentid='".$studentid."'
					AND programid='".$getrowInf->programid."'
					AND schooleleve='".$getrowInf->schlevelid."'
					AND acandemic='".$getrowInf->year."'
					AND ranglev='".$getrowInf->rangelevelid."'
					AND paymentmethod <> ''
					";
		// echo $countmethod; exit();
		
		$ispayexist = $this->db->query($countmethod)->row()->studentid;
		$arr['ispayexist']=$ispayexist;		
		
		
		$photodel="<img src='".site_url('../assets/images/icons/delete.png')."'/>";
		
		$this->db->where("enrollid", $enrollid);
		$this->db->where("studentid", $studentid);
		$invOrders=$this->db->get("v_sch_student_fee_inv")->result();
		/*$sqldate="SELECT
					v_sch_student_fee_inv.enrollid,
					v_sch_student_fee_inv.studentid,
					v_sch_student_fee_inv.student_num
					FROM
					v_sch_student_fee_inv
					WHERE  enrollid='".$enrollid."'					
					AND studentid='".$studentid."' 
					";
		echo $sqldate;	*/	
		// print_r($invOrders);
		// $invOrders=$this->mstu_fee->FMinvOrders($studentid);
		
		$trinv="";
		// print_r($invOrders);
		
		if(isset($invOrders) && count($invOrders)>0){
			$i=1;
			$theads="";
			foreach($invOrders as $row){
				$fclassact='fclassact';
				$is_status=($row->is_status);
				
				
				if($is_status==0){// panding
					$tradd='<a id="a_delete_inv" class="a_delete_inv" attr_id='.$row->typeno.' style="text-align:center">'.$photodel.'</a>';
					$classact='panding';
					$ispaid="";
				}if($is_status==2){// partial
					$tradd='<span class="partial">Partial</samp>';
					$classact='partial';
				}else if($is_status==1){// completed
					$tradd='<span class="completed">Completed</samp>';
					$classact='completed';
				}
				if($this->green->gAction("D")){	
				// $theads=$tradd;
				}
				$theads=$tradd;
				$linkprint="<a target='_blank' href='".site_url('student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno.'')."'>".$row->typeno."</a>";
				$trinv.='<tr class="active cl_inv">
							<td class="'.$classact.'">
								<table>
									<tr>
										<td><strong><span style="color:red;">'.($i++).'</span></strong>. Invoice#</td>
										<td>:</td><td style="text-align:right">'.$linkprint.'</td>
									</tr>
									<tr>
										<td>Invoice Date</td>
										<td>:</td><td style="text-align:right">'.$this->green->convertSQLDate($row->trandate).'</td>
									</tr>
								
									<tr class="tr_inv">
										<td colspan="2"></td>
									</tr>								
								</table>
							</td>
							<td colspan="4" align="right">'.$theads.'</td>
						 </tr>';
						 
						$datas="SELECT
								sch_student_fee_type.typeno,
								sch_student_fee_type.type,
								sch_student_fee_type.description,
								sch_student_fee_type.`not`,
								sch_student_fee_type.amt_line,
								sch_student_fee_type.prices,
								sch_student_fee_type.amt_dis,
								sch_student_fee_type.qty
							FROM
								sch_student_fee_type
							where 1=1
							AND sch_student_fee_type.typeno='".$row->typeno."'
							AND prices <> ''
						
						";
						// echo $datas; exit();
						
						$result = $this->db->query($datas)->result();
						// print_r($classes);
						$trinvd="";	
						$amttotal=0;
						$ii=1;
						if(isset($result) && count($result)>0){	
						$des="";	
							foreach($result as $rowde){
								if($rowde->not !=""){
									$des=$rowde->description.'-'.$rowde->not;	
								}else{
									$des=$rowde->description;
								}
								$trinv.='<tr class="info">
									<td>'.$des.'</td>
									<td align="right">'.$rowde->qty.'</td>
									<td align="right">'.$rowde->prices.'</td>
									<td align="right">'.$rowde->amt_dis.' %</td>
									<td align="right">'.number_format($rowde->amt_line,2).'&nbsp;$</td>
								 </tr>';
								 $amttotal+=($rowde->amt_line);
							}
							
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="'.$fclassact.'">Total</td>
									<td align="right" class="'.$fclassact.'">'.number_format($row->amt_total,2).'&nbsp;$</td>
								 </tr>';
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="'.$fclassact.'">Desposit</td>
									<td align="right" class="'.$fclassact.'">'.number_format($row->amt_paid,2).'&nbsp;$</td>
								 </tr>';
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="'.$fclassact.'">Grand&nbsp;Total</td>
									<td align="right" class="'.$fclassact.'">'.number_format($row->amt_balance,2).'&nbsp;$</td>
								 </tr>';
							
								 
						}// if(isset($result) && count($result)>0){
						
			}// foreach($invOrders as $row){
		}
		
		if(isset($invOrders) && count($invOrders)>0){
		$tr_inv=' <table style="width:98%" align="center" class="btl_history">
							<thead>
							<tr class="gform">
							<th style="text-align:left !important; width:70%">Description</th>
							<th style="text-align:right !important; width:10%">Qty</th>
							<th style="text-align:right !important; width:5%">Unit Price</th>
							<th style="text-align:right !important; width:5%">Dis(%)</th>
							<th style="text-align:right !important; width:10%">Amount</th>
							</tr>
							</thead>
							<tbody>
								'.$trinv.'
							</tbody>
					   </table>';
					   $arr['tr_inv']=$tr_inv;
					  
		}
		// print_r($getrowInf);
		// echo "this".count($getrowInf); exit();
		if(isset($getrowInf) && count($getrowInf)>0){
		 $arr['image_stu']="<img class='img-responsive img-circle' style='border: 1px solid; width:250px; height:175px; border-radius: 3px;' rel='' src='".site_url('../assets/images/'.$studentid.'.jpg')."'/>";
		 $arr['getRow']=$getrowInf;
		  return json_encode($arr);
		}
		//header("Content-type:text/x-json");
		// echo json_encode($arr);
	}
	
	public function MFsave($typeno = ''){
		$schoolid=$this->session->userdata("schoolid");
		
		$student_num = $this->input->post('student_num');
		$arr_inv = $this->input->post('arr_inv');
		$duedate = $this->input->post('duedate');
		$enrollid = $this->input->post('enrollid');
		
		
		// $is_condic=$this->input->post('is_condic');
		$is_condic=$this->input->post('h_iscond_save');		
		
		
		$programid=$this->input->post('opprpid');
		$yearid = $this->input->post('acandemicid');
		$schoolid=$this->session->userdata('schoolid');
		$schlevelid = $this->input->post('shlevelid');
		$oppaymentmethod = $this->input->post('oppaymentmethod');
		
		$type=17;
		
		$nextTran=$this->green->nextTran($type,"Student Fee");
		
		$created_date = date('Y-m-d H:i:s');
		
		if($arr_inv != ""){								  
		foreach($arr_inv as $arr_valinv){			
			$datas = array(
				'type' => $type,
				'typeno' =>$nextTran,
				'studentid' =>$this->input->post('stuid'),
				
				'trandate'=>$created_date,
				'description' => $arr_valinv['payment_des'],
				'not' => $arr_valinv['payment_note'],
                'prices' => $arr_valinv['prices'],
                'amt_dis' => $arr_valinv['amt_dis'],
                'qty' => $arr_valinv['numqty'],
				'amt_line' => $arr_valinv['payment_amt']
			);	
			$this->db->insert('sch_student_fee_type', $datas);
			
			if(($this->input->post('amt_paid'))>0){
				
				$datarece = array(
				'type' => $type,
				'typeno' =>$nextTran,
				'type_inv' => $type,
				'typeno_inv' =>$nextTran,
				'studentid' =>$this->input->post('stuid'),
				
				'trandate'=>$created_date,
				'description' => $arr_valinv['payment_des'],
				'not' => $arr_valinv['payment_note'],
                'prices' => $arr_valinv['prices'],
                'amt_dis' => $arr_valinv['amt_dis'],
                'qty' => $arr_valinv['numqty'],
				'amt_line' => $arr_valinv['payment_amt']
				);		
				$this->db->insert('sch_student_fee_rec_detail', $datarece);	
			};
			
		};// end foreach($arr_inv as $arr_valinv){ ==================
		}// if($arr_inv != ""){
			
		$data = array(
				'type' => $type,
				'typeno' =>$nextTran,
				'studentid' =>$this->input->post('stuid'),
				'enrollid' =>$this->input->post('enrollid'),
				'trandate'=>$created_date,
				
				'amt_total' => $this->input->post('amt_total'),
				
				'amt_balance' => $this->input->post('amt_balance'),
				'duedate' => $this->green->formatSQLDate($duedate),
				
				'schoolid' => $schoolid,
				'programid' => $this->input->post('opprpid'),
				'schooleleve' => $this->input->post('shlevelid'),
				'acandemic' => $this->input->post('acandemicid'),
				'ranglev' => $this->input->post('ranglevid'),
				'classid' => $this->input->post('classe'),
				
				'paymentmethod' => $this->input->post('oppaymentmethod'),
				'paymenttype' => $this->input->post('paymenttype'),
				'areaid' => $this->input->post('oparea'),
				'busfeetypid' => $this->input->post('opbusfeetype')
       			);
			$this->db->insert('sch_student_fee', $data);
			
			if(($this->input->post('amt_paid'))>0){	
				$datarecipt = array(
				'type' => $type,
				'typeno' =>$nextTran,
				'type_inv' => $type,
				'typeno_inv' =>$nextTran,			
				'studentid' =>$this->input->post('stuid'),		
				'enrollid' =>$this->input->post('enrollid'),
				'trandate'=>$created_date,
				'amt_paid' => $this->input->post('amt_paid'),
				'schoolid' => $schoolid,
				'programid' => $this->input->post('opprpid'),
				'schooleleve' => $this->input->post('shlevelid'),
				'acandemic' => $this->input->post('acandemicid'),
				'ranglev' => $this->input->post('ranglevid'),
				'classid' => $this->input->post('classe'),
				
				'paymentmethod' => $this->input->post('oppaymentmethod'),
				'paymenttype' => $this->input->post('paymenttype'),
				'areaid' => $this->input->post('oparea'),
				'busfeetypid' => $this->input->post('opbusfeetype')
				);
				$this->db->insert('sch_student_fee_rec_order', $datarecipt);
			}
			
			// update student info
			// $this->db->set('classid',$this->input->post('classe'));
			// $this->db->set('zoon',$this->input->post('oparea'));
        	// $this->db->where('studentid',$this->input->post('stuid'));
        	// $this->db->update('sch_student');
			
			
			 if($oppaymentmethod !=0){
			$upinfo="UPDATE sch_student SET
						is_new_old='".$oppaymentmethod."'
						WHERE 1=1
						AND studentid='".$this->input->post('stuid')."'
						AND current_yearid='".$this->input->post('acandemicid')."'
						AND classid='".$this->input->post('classe')."'
						AND current_schoollevelid='".$this->input->post('shlevelid')."'
						AND current_ranglevelid='".$this->input->post('ranglevid')."'
					  ";
					//  echo $upinfo;
					  $this->green->runSQL($upinfo);
			 }
			if($is_condic==1){ 
				// 1-insert new record by select for paymentmethod
				//---------------------------------------------------------------------------------------------------------
				$this->db->where('studentid', $this->input->post('stuid'));
				$this->db->where('programid',$programid);
				$this->db->where('schlevelid',$schlevelid);
				$this->db->where('year',$yearid);
				$this->db->where('rangelevelid', $this->input->post('ranglevid'));
				$this->db->where('classid', $this->input->post('classe'));	
							
       			$this->db->delete('sch_student_enrollment');
				
				
				/*$whselec = '';
				$mo = $this->db->where('studentid',$this->input->post('stuid'))
				->where('programid',$programid)
				->where('schlevelid',$schlevelid)
				->where('year',$yearid)
				->where('rangelevelid',$this->input->post(''))
				->where('',$this->input->post(''))				
				->get('sch_student_enrollment')->row();*/
				//---------------------------------------------------------------------------------------------------------			
				$datas="SELECT
							v_study_peroid.term_sem_year_id
							FROM
							v_study_peroid
						where 1=1
						AND v_study_peroid.programid='".$programid."'
						AND v_study_peroid.yearid='".$yearid."'
						AND v_study_peroid.schoolid='".$schoolid."'
						AND v_study_peroid.schlevelid='".$schlevelid."'
						AND v_study_peroid.payment_type='".$oppaymentmethod."'
						";
				// echo $datas; exit();
				$result = $this->db->query($datas)->result();
				$datapayment="";	
				if(isset($result) && count($result)>0){		
					foreach($result as $rowbusfee){
						 $da = array(									
									'studentid' =>$this->input->post('stuid'),
									'schoolid'=>$schoolid,
									'year' => $this->input->post('acandemicid'),
									'classid' => $this->input->post('classe'),
									'schlevelid' => $this->input->post('shlevelid'),
									'programid' => $this->input->post('opprpid'),
									'feetypeid' => $oppaymentmethod,
									'rangelevelid' => $this->input->post('ranglevid'),
									'term_sem_year_id' => $rowbusfee->term_sem_year_id,
									'enroll_date' => $created_date
									);
							$this->db->insert('sch_student_enrollment', $da);
					}
				}// foreach($result as $rowbusfee){
				
			}else if($is_condic ==2){ //  end new creade
				// 1-update old recorded. 
				// 2-insert new record by select for paymentmethod
				//---------------------------------------------------------------------------------------------------------					
				$datas="SELECT
							v_study_peroid.term_sem_year_id
							FROM
							v_study_peroid
						where 1=1
						AND v_study_peroid.programid='".$programid."'
						AND v_study_peroid.yearid='".$yearid."'
						AND v_study_peroid.schoolid='".$schoolid."'
						AND v_study_peroid.schlevelid='".$schlevelid."'
						AND v_study_peroid.payment_type='".$oppaymentmethod."'
						";
				// echo $datas; exit();
				$result = $this->db->query($datas)->result();
				$datapayment="";	
				if(isset($result) && count($result)>0){		
					foreach($result as $rowbusfee){
						 $da = array(									
									'studentid' =>$this->input->post('stuid'),
									'schoolid'=>$schoolid,
									'year' => $this->input->post('acandemicid'),
									'classid' => $this->input->post('classe'),
									'schlevelid' => $this->input->post('shlevelid'),
									'programid' => $this->input->post('opprpid'),
									'feetypeid' => $oppaymentmethod,
									'rangelevelid' => $this->input->post('ranglevid'),
									'term_sem_year_id' => $rowbusfee->term_sem_year_id,
									'enroll_date' => $created_date
									);
							$this->db->insert('sch_student_enrollment', $da);
					}
				}// foreach($result as $rowbusfee){
					
				$updatsql="UPDATE sch_student_enrollment SET 
						is_closed=1
						WHERE 1=1
						AND studentid='".$this->input->post('stuid')."'
						AND schoolid='".$schoolid."'
						AND year='".$this->input->post('acandemicid')."'
						AND classid='".$this->input->post('classe')."'
						AND schlevelid='".$this->input->post('shlevelid')."'
						AND rangelevelid='".$this->input->post('ranglevid')."'
						AND feetypeid <>'".$oppaymentmethod."'
					  ";
					//  echo $updatsql;
					  $this->green->runSQL($updatsql);
					  
			} // }else if($is_condic ==2){
			//---------------------------------------------------------------------------------------------------------	
			$updatsql="UPDATE sch_student_enrollment SET 
						type='".$type."',
						transno='".$nextTran."',
						is_paid=1
						WHERE 1=1
						AND studentid='".$this->input->post('stuid')."'
						AND schoolid='".$schoolid."'
						AND year='".$this->input->post('acandemicid')."'
						AND classid='".$this->input->post('classe')."'
						AND schlevelid='".$this->input->post('shlevelid')."'
						AND rangelevelid='".$this->input->post('ranglevid')."'
						AND feetypeid='".$oppaymentmethod."'
						AND term_sem_year_id='".$this->input->post('paymenttype')."'
					  ";
			// echo $updatsql;
			$this->green->runSQL($updatsql);
			
			if(($this->input->post('amt_paid'))>0){
				$print_receipt=1;
			}else{
				$print_receipt=0;
				}
			
			
			$gettypeno['nextTran']=$nextTran;
			$gettypeno['print_receipt']=$print_receipt;
			
			header("Content-type:text/x-json");
			echo json_encode($gettypeno);
	}
	
	public function FMDelinv($typeno="")
	{	
	//--------- stor history deleted	
	$curdate=date('Y-m-d H:i:s');
	$user=$this->session->userdata('user_name');
		$mo = '';
        $mo = $this->db->where('typeno', $typeno)->get('sch_student_fee')->row();
		$da = array(
					'deleted_byuser'=>$user,
					'deleted_date'=>$curdate,
					'type'=>$mo->type,					
					'typeno'=>$mo->typeno,
					'studentid'=>$mo->studentid,
					'trandate'=>$mo->trandate,
					'classid'=>$mo->classid,
					'schooleleve'=>$mo->schooleleve,
					'termid'=>$mo->termid,
					'amt_total'=>$mo->amt_total,
					'amt_paid'=>$mo->amt_paid,
					'amt_balance'=>$mo->amt_balance,
					'duedate'=>$mo->duedate,
					'programid'=>$mo->programid,
					'acandemic'=>$mo->acandemic,
					'schoolid'=>$mo->schoolid,
					'paymentmethod'=>$mo->paymentmethod,
					'paymenttype'=>$mo->paymenttype,
					'areaid'=>$mo->areaid,
					'busfeetypid'=>$mo->busfeetypid
					);
			   $this->db->insert('sch_student_fee_deleted', $da);
			   
		$datas="SELECT
					sch_student_fee_type.type,
					sch_student_fee_type.typeno,
					sch_student_fee_type.studentid,
					sch_student_fee_type.trandate,
					sch_student_fee_type.description,
					sch_student_fee_type.`not`,
					sch_student_fee_type.amt_line,
					sch_student_fee_type.prices,
					sch_student_fee_type.amt_dis,
					sch_student_fee_type.qty
					FROM
					sch_student_fee_type
					where 1=1
					AND sch_student_fee_type.typeno='".$typeno."'
				";
		// echo $datas; exit();
			
		$result = $this->db->query($datas)->result();
		if(isset($result) && count($result)>0){	
			foreach($result as $rowdel){
			$SQL_DM = "INSERT INTO sch_student_fee_type_deleted";			
			$SQL = " SET studentid='".$rowdel->studentid."',
						 trandate='".$rowdel->trandate."',
						 type='".$rowdel->type."',
						 typeno='".$rowdel->typeno."',						 
						 description='".$rowdel->description."',
						 `not`='".$rowdel->not."',
						 qty='".$rowdel->qty."',
						 prices='".$rowdel->prices."',
						 amt_dis='".$rowdel->amt_dis."',
						 amt_line='".$rowdel->amt_line."'
						 ";
			$counts = $this->green->runSQL($SQL_DM . $SQL);
			}
		};	
		//---- end stor history deleted		
		$this->db->where('typeno', $typeno);
        $this->db->delete('sch_student_fee');
	}
}// main 