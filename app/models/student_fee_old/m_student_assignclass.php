<?php
class M_student_assignclass extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function grid(){
		$offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;

		// offset:0
		// limit:10
		// student_num:1
		// english_name:2
		// khmer_name:3
		// gender:Female
		// schoolid:1
		// programid:1
		// schlevelid:6
		// yearid:2
		// rangelevelid:18
		// classid:101

		$student_num = trim($this->input->post('student_num', TRUE));
		$english_name = trim($this->input->post('english_name', TRUE));
		$khmer_name = trim($this->input->post('khmer_name', TRUE));
		$gender = trim($this->input->post('gender', TRUE));
		$schoolid = trim($this->input->post('schoolid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelid = trim($this->input->post('schlevelid', TRUE));
		$yearid = trim($this->input->post('yearid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$limit_record = trim($this->input->post('limit_record', TRUE));

		$where = '';

		if($student_num != ''){
			$where .= "AND v_assign_class.student_num LIKE '%{$student_num}%' ";
			// $this->db->like('student_num', $student_num, 'both'); 
		}
		if($english_name != ''){
			$where .= "AND CONCAT(
								v_assign_class.first_name,
								' ',
								v_assign_class.last_name
							) LIKE '%{$english_name}%' ";
			//$this->db->like("CONCAT(first_name, ' ', last_name)", $english_name, "both");
		}
		if($khmer_name != ''){
			$where .= "AND CONCAT(
								v_assign_class.first_name_kh,
								' ',
								v_assign_class.last_name_kh
							) LIKE '%{$khmer_name}%' ";
			//$this->db->like("CONCAT(first_name_kh, ' ', last_name_kh)", $khmer_name, "both");
		}
				
		if($gender != ''){
			$where .= "AND v_assign_class.gender = '{$gender}' ";
			// $this->db->where("gender", $gender);
		}
		if($schoolid != ''){
			$where .= "AND v_assign_class.schoolid = '{$schoolid}' ";
			// $this->db->where("schoolid", $schoolid);
		}
		if($programid != ''){
			$where .= "AND v_assign_class.programid = '{$programid}' ";
			// $this->db->where("programid", $programid);
		}
		if($schlevelid != ''){
			$where .= "AND v_assign_class.schlevelid = '{$schlevelid}' ";
			// $this->db->where("schlevelid", $schlevelid);
		}
		if($yearid != ''){
			$where .= "AND v_assign_class.year = '{$yearid}' ";
			// $this->db->where("year", $yearid);
		}
		if($rangelevelid != ''){
			$where .= "AND v_assign_class.rangelevelid = '{$rangelevelid}' ";
			// $this->db->where("rangelevelid", $rangelevelid);
		}
		if($classid != ''){
			$where .= "AND v_assign_class.classid = '{$classid}' ";
			// $this->db->where("classid", $classid);
		}
	
		if($limit_record!=""){
			$limits="limit 0,{$limit_record}";
		}
		
		
		// result =======
		// $result = $this->db->get("v_assign_class")->result();
		$sqls="SELECT
				v_assign_class.student_num,
				v_assign_class.first_name,
				v_assign_class.last_name,
				v_assign_class.first_name_kh,
				v_assign_class.last_name_kh,
				v_assign_class.gender,
				v_assign_class.phone1,
				v_assign_class.class_name,
				v_assign_class.studentid,
				v_assign_class.schoolid,
				v_assign_class.`year`,
				v_assign_class.classid,
				v_assign_class.schlevelid,
				v_assign_class.rangelevelid,
				v_assign_class.feetypeid,
				v_assign_class.programid,
				v_assign_class.sch_level,
				v_assign_class.rangelevelname,
				v_assign_class.program,
				v_assign_class.sch_year
			FROM
				v_assign_class
			WHERE 1=1 {$where}
			{$limits}
			
			";
		 // echo $sqls; exit();
		$result = $this->db->query($sqls);

		$arr_lavel = array();
		$tr='';
		$i=1;
		if($result->num_rows > 0){
			foreach($result->result() as $row_result){
					
				$no_imgs = base_url()."assets/upload/students/NoImage.png";	
				$have_img = base_url()."assets/upload/students/".$row_result->year.'/'.$row_result->studentid.'.jpg';
				$img = '<img src="'.$no_imgs.'" class="img-circle" alt="No Image" width="70" height="70">';
				if (file_exists(FCPATH . "assets/upload/students/".$row_result->year.'/'.$row_result->studentid.'.jpg')) {				
				$img = '<img src="'.$have_img.'" class="img-circle" alt="No Image" width="70" height="70">';
				}


				$where = '';
		    	
				if($row_result->programid != ''){
		    		$where .= "AND rl.programid = '{$row_result->programid}' ";
		    	}
		    	if($row_result->schlevelid != ''){
		    		$where .= "AND rl.schlevelid = '{$row_result->schlevelid}' ";
		    	}
				
				
		    	$sql="SELECT
						rl.rangelevelid,
						rl.rangelevelname
						FROM
						sch_school_rangelevel AS rl
						WHERE
						1 = 1 {$where}
						ORDER BY
						rl.rangelevelname ASC ";
				//echo $sql; exit();
		    	$qr_rangelevel = $this->db->query($sql);
		    	$opt = '';
		    	if($qr_rangelevel->num_rows() > 0){
					foreach($qr_rangelevel->result() as $row_rl){						
						$opt .='<option value="'.$row_rl->rangelevelid.'"'.($row_result->rangelevelid != $row_rl->rangelevelid?"":"selected").'>'.$row_rl->rangelevelname.'</option>';
					}
				}
				
				
				$where1 = '';		    	
		    	if($row_result->schlevelid != ''){
		    		$where1 .= "AND sch_class.schlevelid = '{$row_result->schlevelid}' ";
		    	}
				
				$sql1="SELECT
						sch_class.classid,
						sch_class.class_name,
						sch_class.schlevelid
						FROM
						sch_class
						WHERE
						1 = 1 {$where1}
						ORDER BY
						class_name ASC ";
				//echo $sql; exit();
		    	$qr_rangelevel = $this->db->query($sql1);
		    	$opt_class = '';
		    	if($qr_rangelevel->num_rows() > 0){
					foreach($qr_rangelevel->result() as $row_rl){						
						$opt_class .='<option value="'.$row_rl->classid.'"'.($row_result->classid != $row_rl->classid?"":"selected").'>'.$row_rl->class_name.'</option>';
					}
				}
				
				$tr.='<tr>
					<td>'.($i++).'</td>
					<td style="display:none">
						
						<input type="text" class="studentid" value="'.$row_result->studentid.'">
						<input type="text" class="programid" value="'.$row_result->programid.'">
						<input type="text" class="schoolid" value="'.$row_result->schoolid.'">
						<input type="text" class="schlevelid" value="'.$row_result->schlevelid.'">
						<input type="text" class="yearid" value="'.$row_result->year.'">
						<input type="text" class="rangelevelid" value="'.$row_result->rangelevelid.'">
						<input type="text" class="classid" value="'.$row_result->classid.'">
						
					</td>
					<td>'.$img.'</td>
					<td>
						<div>'.$row_result->student_num.'</div>
						<div>'.$row_result->first_name_kh.' '.$row_result->last_name_kh.'</div>
						<div>'.$row_result->last_name.' '.$row_result->first_name.'</div>
						<div>'.$row_result->gender.'</div>
					</td>
					<td>'.$row_result->program.'</td>
					<td>'.$row_result->sch_level.'</td>
					<td>'.$row_result->sch_year.'</td>
					<td>
							<input type="hidden" class="cur_range_class" id="cur_range_class" value="'.$row_result->rangelevelid.'_'.$row_result->classid.'">
							
							<div class="iscur">'.$row_result->rangelevelname.'</div>
					</td>
					
					<td><select id="new_range" class="form-control new_range">'.$opt.'</select></td>
					<td align="right">
						<input type="hidden" class="new_range_class" id="new_range_class" value="'.$row_result->rangelevelid.'_'.$row_result->classid.'">
							
						<div class="iscur">
							'.$row_result->class_name.'
						</div>
						</td>
					<td><select id="new_class" class="form-control new_class">'.$opt_class.'</select></td>
					
					<td>
						<button id="btn_update" class="btn disabled btn-sm btn_update" name="btn_update" type="button">Disabled</button>
					</td>
				</tr>';	
		}
	}else{// if($result->num_rows > 0){	
		$tr.='<tr>
					<td></td>
				</tr>';
	}
		$arr['arr_tr']=$tr;		
        return json_encode($arr);
	}


    // get rangelevel list =========
    function get_rangelevel_list($programid = '', $schlevelid = '', $yearid = ''){

    	$where = '';
    	if($programid != ''){
    		$where .= "AND rl.programid = '{$programid}' ";
    	}
    	if($schlevelid != ''){
    		$where .= "AND rl.schlevelid = '{$schlevelid}' ";
    	}
    	
    	$qr_rangelevel = $this->db->query("SELECT
													rl.rangelevelid,
													rl.rangelevelname
												FROM
													sch_school_rangelevel AS rl
												WHERE
													1 = 1 {$where}
												ORDER BY
													rl.rangelevelname ASC ");
    	$opt = '';
    	if($qr_rangelevel->num_rows() > 0){
			foreach($qr_rangelevel->result() as $row_rl){
				$opt .= "<option value='".$row_rl->rangelevelid."'>".$row_rl->rangelevelname."</option>";
			}
		}

		$arr_opt[$programid] = array($opt,$programid,$yearid );

		return $arr_opt;

    }

	function get_schlevel($programid = ''){
        $qr_schlevel = $this->db->query("SELECT DISTINCT
                                                l.schlevelid,
                                                l.sch_level
                                            FROM
                                                sch_school_level AS l
                                            WHERE
                                                l.programid = '{$programid}' - 0
                                            ORDER BY
                                                l.sch_level ASC ")->result();        
        
        $arr = array('schlevel' => $qr_schlevel);
        return json_encode($arr);
    }

    function get_year($programid = '', $schlevelid){
    	
    	$where = '';
    	if($programid != ''){
    		$where .= "AND y.programid = '{$programid}' ";
    	}

        $qr_year = $this->db->query("SELECT DISTINCT
                                            y.yearid,
                                            y.sch_year
                                        FROM
                                            sch_school_year AS y
                                        WHERE
                                            y.schlevelid = '{$schlevelid}' '{$where}'
                                        ORDER BY
                                            y.sch_year ASC ")->result();

        $qr_class = $this->db->query("SELECT
													c.classid,
													c.class_name
												FROM
													sch_school_level AS l
												INNER JOIN sch_class AS c ON l.schlevelid = c.schlevelid
												WHERE
													l.schlevelid = '{$schlevelid}'
												ORDER BY
													c.class_name ASC ")->result();
        
        $arr = array('year' => $qr_year, 'class' => $qr_class);
        return json_encode($arr);
    }

    // get rangelevel =========
    function get_rangelevel($programid = '', $yearid = ''){
        $qr_rangelevel = $this->db->query("SELECT DISTINCT
                                                rl.rangelevelid,
                                                rl.rangelevelname,
                                                rp.rangelevelid AS rangelevelid_period
                                            FROM
                                                sch_school_rangelevel AS rl
                                            INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
                                            AND rl.programid = y.programid
                                            LEFT JOIN sch_rangelevel_period AS rp ON rl.rangelevelid = rp.rangelevelid
                                            WHERE
                                                y.yearid = '{$yearid}' 
                                            ORDER BY rl.rangelevelname ASC ")->result();

        $arr = array('rangelevel' => $qr_rangelevel);
        return json_encode($arr);
    }

}