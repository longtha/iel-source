<?php
class M_student_fee_reminder_period extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function grid(){
		$offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;

		$student_num = trim($this->input->post('student_num', TRUE));
		$full_name = trim($this->input->post('full_name', TRUE));
		$gender = trim($this->input->post('gender', TRUE));
		$schoolid = trim($this->input->post('schoolid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelid = trim($this->input->post('schlevelid', TRUE));
		$term_sem_year_id = trim($this->input->post('term_sem_year_id', TRUE));
		$feetypeid = trim($this->input->post('feetypeid', TRUE));
		$yearid = trim($this->input->post('yearid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$is_paid = trim($this->input->post('is_paid', TRUE));
		$is_closed = trim($this->input->post('is_closed', TRUE));		
		$from_date = trim($this->input->post('from_date', TRUE));
		$to_date = trim($this->input->post('to_date', TRUE));	

		$where = '';

		if($student_num != ''){
			$where .= "AND st.student_num LIKE '%{$student_num}%' ";
			// $this->db->like('student_num', $student_num, 'both'); 
		}
		if($full_name != ''){
			$where .= "AND CONCAT(
								st.first_name,
								' ',
								st.last_name
							) LIKE '%{$full_name}%' ";
			//$this->db->like("CONCAT(first_name, ' ', last_name)", $full_name, "both");

		}		
		if($gender != ''){
			$where .= "AND st.gender = '{$gender}' ";
			// $this->db->where("gender", $gender);
		}
		if($schoolid != ''){
			$where .= "AND er.schoolid = '{$schoolid}' ";
			// $this->db->where("schoolid", $schoolid);
		}
		if($programid != ''){
			$where .= "AND er.programid = '{$programid}' ";
			// $this->db->where("programid", $programid);
		}
		if($schlevelid != ''){
			$where .= "AND er.schlevelid = '{$schlevelid}' ";
			// $this->db->where("schlevelid", $schlevelid);
		}
		if($yearid != ''){
			$where .= "AND er.year = '{$yearid}' ";
			// $this->db->where("year", $yearid);
		}		
		if($term_sem_year_id != ''){
			$where .= "AND er.term_sem_year_id = '{$term_sem_year_id}' ";
			// $this->db->where("term_sem_year_id", $term_sem_year_id);
		}
		if($feetypeid != ''){
			$where .= "AND er.feetypeid = '{$feetypeid}' ";
			// $this->db->where("feetypeid", $feetypeid);
		}
		if($rangelevelid != ''){
			$where .= "AND er.rangelevelid = '{$rangelevelid}' ";
			// $this->db->where("rangelevelid", $rangelevelid);
		}
		if($classid != ''){
			$where .= "AND er.classid = '{$classid}' ";
			// $this->db->where("classid", $classid);
		}
		if($is_paid != ''){
			$where .= "AND er.is_paid = '{$is_paid}' ";
			// $this->db->where("is_paid", $is_paid);
		}
		if($is_closed != ''){
			// $where .= "AND er.is_closed = '{$is_closed}' ";
			$this->db->where("is_closed", $is_closed);
		}

		if($from_date != ''){
			$where .= "AND DATE_FORMAT(er.enroll_date,'%d/%m/%Y') >= '{$from_date}' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') >=", $from_date);
		}
		if($to_date != ''){
			$where .= "AND DATE_FORMAT(er.enroll_date,'%d/%m/%Y') <= '{$to_date}' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') <=", $to_date);
		}

		// count =========
		// $totalRecord = $this->db->count_all_results('v_student_fee_reminder_period') - 0;	
		$totalRecord = $this->db->query("SELECT
												COUNT(er.studentid) AS c
											FROM
												(
													(
														(
															(
																(
																	(
																		(
																			sch_student_enrollment AS er
																			JOIN sch_student AS st ON (
																				(er.studentid = st.studentid)
																			)
																		)
																		JOIN sch_school_program ON (
																			(
																				er.programid = sch_school_program.programid
																			)
																		)
																	)
																	JOIN sch_school_level ON (
																		(
																			er.schlevelid = sch_school_level.schlevelid
																		)
																	)
																)
																JOIN sch_school_rangelevel ON (
																	(
																		er.rangelevelid = sch_school_rangelevel.rangelevelid
																	)
																)
															)
															JOIN sch_school_year ON (
																(
																	er.`year` = sch_school_year.yearid
																)
															)
														)
														JOIN sch_class ON (
															(
																er.classid = sch_class.classid
															)
														)
													)
													JOIN v_study_peroid ON (
														(
															(
																er.term_sem_year_id = v_study_peroid.term_sem_year_id
															)
															AND (
																er.feetypeid = v_study_peroid.payment_type
															)
														)
													)
												)
											WHERE 1=1 {$where} ")->row()->c - 0;

		$totalPage = ceil($totalRecord/$limit - 0) - 0;

		// $this->db->limit($limit, $offset);
		
		// result =======
		// $result = $this->db->get("v_student_fee_reminder_period")->result();
		$result = $this->db->query("SELECT
											er.studentid AS studentid,
											er.schoolid AS schoolid,
											er.`year` AS `year`,
											er.classid AS classid,
											er.enroll_date AS enroll_date,
											er.schlevelid AS schlevelid,
											er.rangelevelid AS rangelevelid,
											er.feetypeid AS feetypeid,
											er.programid AS programid,
											er.is_closed AS is_closed,
											er.is_paid AS is_paid,
											er.term_sem_year_id AS term_sem_year_id,
											sch_school_program.program AS program,
											sch_school_level.sch_level AS sch_level,
											sch_school_year.sch_year AS sch_year,
											sch_school_rangelevel.rangelevelname AS rangelevelname,
											sch_class.class_name AS class_name,
											st.student_num AS student_num,
											st.first_name AS first_name,
											st.last_name AS last_name,
											st.last_name_kh AS last_name_kh,
											st.first_name_kh AS first_name_kh,
											st.gender AS gender,
											st.phone1 AS phone1,
											v_study_peroid.period AS period
										FROM
											(
												(
													(
														(
															(
																(
																	(
																		sch_student_enrollment AS er
																		JOIN sch_student AS st ON (
																			(er.studentid = st.studentid)
																		)
																	)
																	JOIN sch_school_program ON (
																		(
																			er.programid = sch_school_program.programid
																		)
																	)
																)
																JOIN sch_school_level ON (
																	(
																		er.schlevelid = sch_school_level.schlevelid
																	)
																)
															)
															JOIN sch_school_rangelevel ON (
																(
																	er.rangelevelid = sch_school_rangelevel.rangelevelid
																)
															)
														)
														JOIN sch_school_year ON (
															(
																er.`year` = sch_school_year.yearid
															)
														)
													)
													JOIN sch_class ON (
														(
															er.classid = sch_class.classid
														)
													)
												)
												JOIN v_study_peroid ON (
													(
														(
															er.term_sem_year_id = v_study_peroid.term_sem_year_id
														)
														AND (
															er.feetypeid = v_study_peroid.payment_type
														)
													)
												)
											) 
										WHERE 1=1 {$where}
										ORDER BY er.enroll_date DESC
										LIMIT {$offset}, {$limit} ")->result();	

		$arr = array('result' => $result, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage, 'test' => ($offset.' | '.$limit));
        return json_encode($arr);
	}

	function get_schlevel($programid = ''){
        $qr_schlevel = $this->db->query("SELECT DISTINCT
                                                l.schlevelid,
                                                l.sch_level
                                            FROM
                                                sch_school_level AS l
                                            WHERE
                                                l.programid = '{$programid}' - 0
                                            ORDER BY
                                                l.sch_level ASC ")->result();        
        
        $arr = array('schlevel' => $qr_schlevel);
        return json_encode($arr);
    }

    function get_year($programid = '', $schlevelid = ''){
        $qr_year = $this->db->query("SELECT DISTINCT
                                            y.yearid,
                                            y.sch_year
                                        FROM
                                            sch_school_year AS y
                                        WHERE
                                            y.programid = '{$programid}' - 0 AND y.schlevelid = '{$schlevelid}' - 0
                                        ORDER BY
                                            y.sch_year ASC ")->result();

        $qr_class = $this->db->query("SELECT
													c.classid,
													c.class_name
												FROM
													sch_school_level AS l
												INNER JOIN sch_class AS c ON l.schlevelid = c.schlevelid
												WHERE
													l.schlevelid = '{$schlevelid}'
												ORDER BY
													c.class_name ASC ")->result();
        
        $arr = array('year' => $qr_year, 'class' => $qr_class);
        return json_encode($arr);
    }

    // get rangelevel =========
    function get_rangelevel($yearid = ''){
        $qr_rangelevel = $this->db->query("SELECT DISTINCT
                                                rl.rangelevelid,
                                                rl.rangelevelname,
                                                rp.rangelevelid AS rangelevelid_period
                                            FROM
                                                sch_school_rangelevel AS rl
                                            INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
                                            AND rl.programid = y.programid
                                            LEFT JOIN sch_rangelevel_period AS rp ON rl.rangelevelid = rp.rangelevelid
                                            WHERE
                                                y.yearid = '{$yearid}' 
                                            ORDER BY rl.rangelevelname ASC ")->result();

        $arr = array('rangelevel' => $qr_rangelevel);
        return json_encode($arr);
    }

    // get_paymentType =========
    public function get_paymentType($feetypeid = '', $programid = '', $schlevelid = '', $yearid = ''){
    	if($feetypeid != ''){
    		$this->db->where("payment_type", $feetypeid);
    	}
    	if($programid != ''){
    		$this->db->where("programid", $programid);
    	}
    	if($schlevelid != ''){
    		$this->db->where("schlevelid", $schlevelid);
    	}
    	if($yearid != ''){
    		$this->db->where("yearid", $yearid);
    	}    	

        $qr_get_paymentType = $this->db->get("v_study_peroid")->result();
        return json_encode($qr_get_paymentType);
    }

}