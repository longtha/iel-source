<?php
class M_student_cash_return__ extends CI_Model{

	function __construct(){
		parent::__construct();

	}


public function FMupdate_return($attr_typeno){  
 
      $datas="SELECT
        sch_student_fee_type.typeno,
        sch_student_fee_type.type,
        sch_student_fee_type.description,
        sch_student_fee_type.`not`,
        sch_student_fee_type.amt_line,
        sch_student_fee_type.prices,
        sch_student_fee_type.amt_dis,
        sch_student_fee_type.otherfee_id,
        sch_student_fee_type.qty
       FROM
        sch_student_fee_type
       where 1=1
       AND sch_student_fee_type.typeno='".$attr_typeno."'
	   AND type=32
            
      ";
     //  echo $datas; exit();
      
      $result = $this->db->query($datas)->result();
      // print_r($classes);
      $trdata=""; 
      $amttotal=0;
      $ii=1;
	  $trs="";
      $arr=array();
      if(isset($result) && count($result)>0){        
       foreach($result as $rowde){
		   //--------------
			$query=$this->db->get('sch_setup_otherfee')->result();
			
			$oppaylist="";
			$oppaylist="<option attr_price='0' value=''></option><option attr_price='create_new' value='create_new'><span style='color:blue'>--- Create New ---</span></option>";
			
			foreach ($query as $rowotherfee) {				
				$oppaylist.="<option ".(($rowde->otherfee_id)==($rowotherfee->otherfeeid)?"selected=selected":"")." attr_typefee='".$rowotherfee->typefee."' attr_datetran='".$this->green->convertSQLDate($rowotherfee->fromdate)."  -  ". $this->green->convertSQLDate($rowotherfee->todate)."' attr_price='".$rowotherfee->prices."' value='".$rowotherfee->otherfeeid."'>".$rowotherfee->otherfee."</option>";
			}
			
			$photodel="<img src='".site_url('../assets/images/icons/delete.png')."'/>";
		   //--------------
		  
		   $trdata.='<tr class="tr_addrow">
					<td>
						<select id="paymenttype_list" class="form-control paymenttype_list gform" required="">'.$oppaylist.'</select>
						<input id="paytypeid" class="form-control payment_des" type="text" value="'.$rowde->description.'" style="text-align:left; display:none">
						<input id="is_bus" class="form-control is_bus" type="text" value="" style="text-align:left; display:none">
					</td>
					<td>
						<input id="paytypeid" class="form-control payment-note"  value="'.($rowde->not).'" type="text" placeholder="" style="text-align:right">
					</td>
					<td>
						<input id="numqty" class="form-control numqty" type="text" placeholder="" value="'.($rowde->qty).'"  style="text-align:right">
					</td>
					
					<td>
						<input id="paytypeid" class="form-control payment-unitprice" type="text" placeholder=""  value="'.($rowde->prices).'"  style="text-align:right">
					</td>
					<td>
						<input id="paytypeid" class="form-control payment-dis" type="text" placeholder=""  value="'.($rowde->amt_dis).'"  style="text-align:right">
					</td>
					<td>
						<input id="paytypeid" class="form-control payment-amt" type="text" placeholder=""  value="'.($rowde->amt_line).'"  style="text-align:right">
					</td>
					<td style="text-align:center">&nbsp;&nbsp;&nbsp;<a id="a_delete" class="a_delete a_delete1" style="text-align:center">'.$photodel.'</a></td>"+
				</tr>';
		   //--------------
      //  $trdata.='<tr class="tr_addrow"><td>'.($rowde->description).'</td><td>'.($rowde->description).'</td><td>'.($rowde->description).'</td></tr>';
       }
      }
     $arr['tr_loop']=$trdata;     
    header("Content-type:text/x-json");
    echo json_encode($arr); 
   exit();
 }
 
 
	function grid(){
		$this->green->setActiveRole($this->input->post('roleid'));
  		$sortby=$this->input->post("sortby");
		$sorttype=$this->input->post("sorttype");

		$offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;
        $limit_record = $this->input->post('limit_record') - 0;        
		$typeno = trim($this->input->post('typeno', TRUE));
		$student_num = trim($this->input->post('student_num', TRUE));
		$student_name = trim($this->input->post('student_name', TRUE));
		$gender = trim($this->input->post('gender', TRUE));
		$schoolid = trim($this->input->post('schoolid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelid = trim($this->input->post('schlevelid', TRUE));		
		$yearid = trim($this->input->post('yearid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$term_sem_year_id = trim($this->input->post('term_sem_year_id', TRUE));
		$feetypeid = trim($this->input->post('feetypeid', TRUE));
		$from_date = trim($this->input->post('from_date', TRUE));
		$to_date = trim($this->input->post('to_date', TRUE));	
		$report_type = $this->input->post('report_type', TRUE);
		$sortstr ='';
		$where = '';

		if($typeno != ''){
			$where .= "AND typeno LIKE '%{$typeno}%' ";
			// $this->db->like('student_num', $student_num, 'both'); 
		}
		if($student_num != ''){
			$where .= "AND student_num LIKE '%{$student_num}%' ";
			// $this->db->like('student_num', $student_num, 'both'); 
		}		
		if($student_name != ''){
			$where .= "AND ( CONCAT(
									first_name,
									' ',
									last_name
								) LIKE '%{$student_name}%' ";
			$where .= "or CONCAT(
									first_name_kh,
									' ',
									last_name_kh
								) LIKE '%{$student_name}%' ) ";			

			// $this->db->like("CONCAT(first_name, ' ', last_name)", $full_name, "both");
		}		
		if($gender != ''){
			$where .= "AND gender = '{$gender}' ";
			// $this->db->where("gender", $gender);
		}
		if($schoolid != ''){
			$where .= "AND schoolid = '{$schoolid}' ";
			// $this->db->where("schoolid", $schoolid);
		}
		if($programid != ''){
			$where .= "AND programid = '{$programid}' ";
			// $this->db->where("programid", $programid);
		}
		if($schlevelid != ''){
			$where .= "AND schooleleve = '{$schlevelid}' ";
			// $this->db->where("schlevelid", $schlevelid);
		}
		if($yearid != ''){
			$where .= "AND acandemic = '{$yearid}' ";
			// $this->db->where("year", $yearid);
		}
		if($rangelevelid != ''){
			$where .= "AND ranglev = '{$rangelevelid}' ";
			// $this->db->where("rangelevelid", $rangelevelid);
		}
		if($classid != ''){
			$where .= "AND classid = '{$classid}' ";
			// $this->db->where("classid", $classid);
		}
		if($feetypeid != ''){
			$where .= "AND paymentmethod = '{$feetypeid}' ";
			// $this->db->where("feetypeid", $feetypeid);
		}
		
		if($term_sem_year_id != ''){
			$where .= "AND paymenttype = '{$term_sem_year_id}' ";
			// $this->db->where("term_sem_year_id", $term_sem_year_id);
		}	
			
		if($report_type != ''){
			$where .= "AND is_status = '{$report_type}' ";
			// $this->db->where("term_sem_year_id", $term_sem_year_id);
		}	
			
		if($from_date != ''){
			$where .= "AND date(trandate) >= '".$this->green->formatSQLDate($from_date)."' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') >=", $from_date);
		}
		if($to_date != ''){
			$where .= "AND date(trandate) <= '".$this->green->formatSQLDate($to_date)."' ";
			// $this->db->where("DATE_FORMAT(enroll_date,'%d/%m/%Y') <=", $to_date);
		}

		(isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";
		// count ==========
		$qr_c = $this->db->query("SELECT COUNT(student_num) AS c FROM v_student_fee_inv_list
									WHERE 1=1 {$where} {$sortstr}")->row()->c - 0;
		$totalRecord = $qr_c - 0;		
		$totalPage = ceil($totalRecord/$limit);

		// result =======
		$qr = $this->db->query("SELECT
										*,
										DATE_FORMAT(trandate, '%d-%m-%Y') AS trandate
									FROM
										v_student_fee_inv_list 
									WHERE 1=1 {$where} {$sortstr}
									AND type=17
									LIMIT $offset,$limit");

		$i = 1;
		$tr = '';
		$gamt_total=0;
		$gamt_paid=0;
		$gamt_balance=0;
			
		if($qr->num_rows() > 0){			
			foreach($qr->result() as $row){
				
				$counrow="SELECT
								COUNT(typeno) as typeno
							FROM
								sch_student_fee_rec_order
							WHERE
								1=1
								AND type=18
								AND type_inv='".$row->type."'
								AND typeno_inv='".$row->typeno."'
							";
		// echo $counrow; exit();
		
		$ispayexist = $this->db->query($counrow)->row()->typeno;
			$tradd="";
			
							//$photodel="<img class='img-circle' src='".site_url('../assets/images/icons/delete.png')."'/>";	
							// if($ispayexist>0){
							// 	$photodel="<img  class='img-circle' style='width:15px; height:15px;' src='".site_url('../assets/images/icons/deletedis.png')."'/>";
							// 	$tradd='<a>'.$photodel.'</a>';
								
							// }else{
							// 	$tradd='<a id="a_delete_inv" class="a_delete_inv" attr_id='.$row->typeno.' style="text-align:center">'.$photodel.'</a>';
							
							// }
		
				//if($report_type == 0){
				
				
				if($this->green->gAction("D")){	
				$dele=$tradd;
				}
				
				$dele=$tradd;
				
				if(1 == 1){
					$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
					$no_imgs = base_url()."assets/upload/students/NoImage.png";	
					
					$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
					if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
						$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
					}

					$liks='<div class="tr_studentcode create_inv" isaddnewinv="0" attr_student_num="'.$row->student_num.'" attr_enroid="'.$row->enrollid.'" attr_stuid="'.$row->studentid.'" data-target="#mdinvoice" data-toggle="modal"><a href="#" class="btn btn-success">Return</a></div>';
					$tr .= '<tr>
								<td>'.($i++).'</td>
								<td style="text-align: center;">
									'.$img.'
								</td>
								<td>
									'.$row->student_num.'<br>
									 '.$row->last_name_kh.' '.$row->first_name_kh.'<br>
									'.$row->last_name.' '.$row->first_name.' <br>
									'.ucfirst($row->gender).'
									 <br>
								</td>
								<td>'.$row->trandate.'</td>	
								<td>'.$row->sch_level.'</td>
								<td>'.$row->sch_year.'</td>
								<td>'.$row->rangelevelname.'</td>
								<td>'.$row->class_name.'</td>
								<td>'.$row->period.'</td>
								<td>
									<a href="'.site_url().'/student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno.'" target="_blank" data-typeno="'.$row->typeno.'" class="a_">#'.$row->typeno.'</a>
								</td>
								<td>'.number_format($row->amt_total, 2).'</td>
								<td>'.number_format($row->amt_paid, 2).'</td>
								<td>'.number_format($row->amt_balance, 2).'</td>
								<td>'.$liks.'</td>
							</tr>';	

				}else{
					$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
					$no_imgs = base_url()."assets/upload/students/NoImage.png";	
					
					$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
					if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
						$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
					}
					
					$tr .= '<tr>
								<td>'.($i++).'</td>
								<td style="text-align: center;">
									'.$img.'
								</td>
								<td>
									<a href="'.site_url().'/student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno.'" target="_blank">#'.$row->typeno.'</a><br>
									'.$row->trandate.'<br>
									'.$row->student_num.'<br>
									'.$row->first_name_kh.' '.$row->last_name_kh.'<br>
								</td>							
								<td>'.$row->sch_level.'</td>
								<td>'.$row->sch_year.'</td>
								<td>'.$row->rangelevelname.'</td>
								<td>'.$row->class_name.'</td>
								<td>'.$row->period.'</td>
								<td>'.number_format($row->amt_total, 2).'$</td>
								<td>'.number_format($row->amt_paid, 2).'$</td>
								<td>'.number_format($row->amt_balance, 2).'$</td>
							</tr>';

					// detail =========
					$qr_detail = $this->db->query("SELECT DISTINCT
															*,
															DATE_FORMAT(trandate, '%d-%m-%Y') AS trandate
														FROM
															sch_student_fee_type AS st
														WHERE
															1=1
														AND st.typeno = '{$row->typeno}' ");

					$sub_to = 0;
					if($qr_detail->num_rows() > 0){
						$tr .= '<tr style="text-align: right;">'.
									'<td colspan="1" style="border-top: 0;">&nbsp;</td>'.
									'<td colspan="2" style="border-top: 0;text-align: left;background: #DDD;">Description</td>'.
									'<td colspan="2" style="border-top: 0;background: #DDD;">Quantity</td>'.
									'<td colspan="2" style="border-top: 0;background: #DDD;">Price</td>'.
									'<td colspan="2" style="border-top: 0;background: #DDD;">Dis.(%)</td>'.
									'<td colspan="2" style="border-top: 0;background: #DDD;">Amount</td>'.
								'</tr>';
						foreach ($qr_detail->result() as $row_detail) {
							$tr .= '<tr style="text-align: right;">'.
										'<td colspan="1" style="border-top: 0;">&nbsp;</td>'.
										'<td colspan="2" style="border-top: 0;text-align: left;">- '.$row_detail->description.'</td>'.
										'<td colspan="2" style="border-top: 0;">'.$row_detail->qty.'</td>'.
										'<td colspan="2" style="border-top: 0;">'.number_format($row_detail->prices, 2).'</td>'.
										'<td colspan="2" style="border-top: 0;">'.number_format($row_detail->amt_dis, 2).'%</td>'.
										'<td colspan="2" style="border-top: 0;">'.number_format($row_detail->amt_line, 2).'</td>'.
									'</tr>';
							$sub_to += $row_detail->amt_line - 0;
						}// foreach detail =====
					}

					$tr .= '<tr style="font-weight: bold;">'.
								'<td colspan="9" style="text-align: right;border-top: 0;">Sub Total</td>'.
								'<td colspan="1" style="border-top: 0;text-align: right;">'.number_format($sub_to, 2).'</td>'.
							'</tr>';

				}// report type =====
			$gamt_total+=$row->amt_total;
			$gamt_paid+=$row->amt_paid;
			$gamt_balance+=$row->amt_balance;			
			}// foreach master =====
			$tr .= '<tr><td colspan="9" style="font-weight: bold;text-align: right;" class="gamt">Total</td>
					<td class="gamt">'.number_format($gamt_total,2).'</td>
					<td class="gamt">'.number_format($gamt_paid,2).'</td>
					<td class="gamt">'.number_format($gamt_balance,2).'</td>
					
					<td  class="gamt"></td></tr>';
						
		}else{
			$tr .= '<tr><td colspan="13" style="font-weight: bold;text-align: center;">We did not find anything to show here...</tr>';
		}

		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage); 
		// $arr['tr'] = $tr;
        return json_encode($arr);		
	}

	public function Fcstudentinfo($enrollid,$isaddnewinv,$studentid,$attr_is_pt){
			$typeno_h =$this->input->post('typeno_h');
    		//$this->db->where("enrollid", $enrollid);
    		 $this->db->where("studentid", $studentid);
        $getrowInf = $this->db->get("v_student_fee_create_inv")->row();
		// print_r($getrowInf); exit();
		//----------------------------
		
		$sqltime="SELECT
							sch_time.from_time,
							sch_school_rangelevtime.rangelevelid,
							sch_time.am_pm,
							sch_time.timeid,
							sch_time.to_time
							FROM
							sch_school_rangelevtime
							INNER JOIN sch_time ON sch_school_rangelevtime.timeid = sch_time.timeid
							where 1=1 
							AND rangelevelid='".$getrowInf->rangelevelid."'
						  ";
		// echo $sqltime; exit();
			
		$result = $this->db->query($sqltime)->result();
		 // print_r($classes);
		
		$pttime="";	
		if(isset($result) && count($result)>0){		
			foreach($result as $myrows){
				$pttime.="<option value='".$myrows->timeid."'>".$myrows->from_time."-".$myrows->to_time."&nbsp;".$myrows->am_pm."</option>";
			}
		}
		$arr['studytime']=$pttime;
		
		//----------------------------
		
		$countmethod="SELECT
					COUNT(studentid) AS studentid
					FROM
						sch_student_fee
					WHERE 1=1
					AND studentid='".$studentid."'
					AND programid='".$getrowInf->programid."'
					AND schooleleve='".$getrowInf->schlevelid."'
					AND acandemic='".$getrowInf->year."'
					AND ranglev='".$getrowInf->rangelevelid."'
					AND classid='".$getrowInf->classid."'
					AND paymentmethod <> ''
					";
		// echo $countmethod; exit();
		
		$ispayexist = $this->db->query($countmethod)->row()->studentid;
		$arr['ispayexist']=$ispayexist;	
		$arr['is_pt']=$attr_is_pt;	
				
		$photodel="<img src='".site_url('../assets/images/icons/delete.png')."'/>";
		
		$selesql="SELECT
						v_student_fee_create_receipt.student_num,
						v_student_fee_create_receipt.typeno,
						v_student_fee_create_receipt.type,
						v_student_fee_create_receipt.first_name,
						v_student_fee_create_receipt.last_name,
						v_student_fee_create_receipt.first_name_kh,
						v_student_fee_create_receipt.last_name_kh,
						v_student_fee_create_receipt.phone1,
						v_student_fee_create_receipt.is_status,
						v_student_fee_create_receipt.amt_total,
						v_student_fee_create_receipt.amt_paid,
						v_student_fee_create_receipt.amt_balance,
						v_student_fee_create_receipt.studentid
					FROM
						v_student_fee_create_receipt
					WHERE 1=1
					AND studentid='".$studentid."'
					AND type=32
					-- AND type_inv=17
					-- AND typeno_inv='".$typeno_h."'
					";
				// echo $selesql;
		$invOrders=$this->db->query($selesql)->result();
		
		
		$trinv="";
		// print_r($invOrders);
		$gtotal=0;
		if(isset($invOrders) && count($invOrders)>0){
			$i=1;
			$theads="";
			foreach($invOrders as $row){
				$fclassact='fclassact';
				$is_status=($row->is_status);
				if($is_status==0){// panding  
					//$tradd='<a id="a_delete_inv" class="a_delete_inv" attr_id='.$row->typeno.' style="text-align:center">'.$photodel.'</a>';
					$tradd='<a href="#" id="a_delete_inv"  attr_id='.$row->typeno.' style="text-align:center" class="a_delete_inv btn btn-sm btn-success"><span class="glyphicon glyphicon-trash"></span> Delete</a>';

					$classact='panding';
					$ispaid="";
				}if($is_status==2){// partial
					$tradd='<span class="partial">Partial</samp>';
					$classact='partial';
				}else if($is_status==1){// completed
					$tradd='<a href="#" class="btn btn-sm btn-success"><span class="completed glyphicon glyphicon-ok"></span>Completed</a>';
					$classact='completed';
				}
				if($this->green->gAction("D")){	
				$theads=$tradd;
				}
				$theads=$tradd;
				$linkprint=($row->typeno);
				$trinv.='<tr class="active cl_inv">
							<td class="'.$classact.'">
								<table>
									<tr>
										<td><strong><span style="color:red;">'.($i++).'</span></strong>. <a href='.site_url('student_fee/c_print_cash_return?FCprint_inv='.$row->typeno.'').' class="btn btn-sm btn-success"><span class="glyphicon glyphicon-print"></span> Cash Return</a>  '.$linkprint.'</td>
										
									</tr>
									
									<tr class="tr_inv">
										<td colspan="2"></td>
									</tr>								
								</table>
							</td>
							<td colspan="4" align="right">'.$theads.'</td>
						 </tr>';
						 
						$datas="SELECT
								sch_student_fee_type.typeno,
								sch_student_fee_type.type,
								sch_student_fee_type.description,
								sch_student_fee_type.`not`,
								sch_student_fee_type.amt_line,
								sch_student_fee_type.prices,
								sch_student_fee_type.amt_dis,
								sch_student_fee_type.qty
							FROM
								sch_student_fee_type
							where 1=1
							AND sch_student_fee_type.typeno='".$row->typeno."'
							AND prices <> ''
						
						";
						// echo $datas; exit();
						
						$result = $this->db->query($datas)->result();
						// print_r($classes);
						$trinvd="";	
						$amttotal=0;
						$ii=1;
						if(isset($result) && count($result)>0){	
						$des="";	
							foreach($result as $rowde){
								if($rowde->not !=""){
									$des=$rowde->description.'-'.$rowde->not;	
								}else{
									$des=$rowde->description;
								}
								$trinv.='<tr class="info">
									<td>'.$des.'</td>
									<td align="right">'.$rowde->qty.'</td>
									<td align="right">'.number_format($rowde->prices,2).'&nbsp;$</td>
									<td align="right">'.$rowde->amt_dis.' %</td>
									<td align="right">'.number_format($rowde->amt_line,2).'&nbsp;$</td>
								 </tr>';
								 $amttotal+=($rowde->amt_line);
							}
							
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="'.$fclassact.'">Total</td>
									<td align="right" class="'.$fclassact.'">'.number_format($row->amt_total,2).'&nbsp;$</td>
								 </tr>';
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="'.$fclassact.'">Desposit</td>
									<td align="right" class="'.$fclassact.'">'.number_format($row->amt_paid,2).'&nbsp;$</td>
								 </tr>';
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="'.$fclassact.'">Sub&nbsp;Total</td>
									<td align="right" class="'.$fclassact.'">'.number_format($row->amt_balance,2).'&nbsp;$</td>
								 </tr>';
						$gtotal+=($row->amt_total)-0;		 
				}// if(isset($result) && count($result)>0){
					
			}// foreach($invOrders as $row){
				$trinv.='<tr>
							<td></td>
							<td></td>
							
							<td></td>
							<td align="right" class="'.$fclassact.'"><strong>Grand&nbsp;Total</strong></td>
							<td align="right" class="'.$fclassact.'"><strong>'.number_format($gtotal,2).'&nbsp;$</strong></td>
						 </tr>';
			$arr['trinv']='<fieldset> <legend class="btn-success"><a href="#" class="class_history"><span class="glyphicon glyphicon-stats"></span> History of Student Fee</a></legend>
							<table class="tbl_history" style="width:100% !important;">
								
								<tr>
									<td class="td_history">Description</td>
									<td class="td_history" align="right">Quantity</td>
									<td class="td_history" align="right">Unit Price</td>
									<td class="td_history" align="right">Dis(%)</td>
									<td class="td_history" align="right">Amount</td>
								</tr>
								'.$trinv.'
							</table></fieldset>';
		}// if(isset($invOrders) && count($invOrders)>0){
					   
		if(isset($getrowInf) && count($getrowInf)>0){
			
			$have_img = base_url()."assets/upload/students/".$getrowInf->year.'/'.$getrowInf->studentid.'.jpg';		
			$no_imgs = base_url()."assets/upload/students/NoImage.png";	
			
			$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:155px;height:155px;">';
			if (file_exists(FCPATH . "assets/upload/students/".$getrowInf->year.'/'.$getrowInf->studentid.'.jpg')) {				
				$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:155px;height:155px;">';
			}
			
		 $arr['image_stu']=$img;
		
		 $arr['getRow']=$getrowInf;
		  return json_encode($arr);
		}
		//header("Content-type:text/x-json");
		// echo json_encode($arr);
	}


	public function MFsave($typeno = ''){
		$schoolid=$this->session->userdata("schoolid");
		$typeno_h =$this->input->post('typeno_');
		$student_num = $this->input->post('student_num');
		$arr_inv = $this->input->post('arr_inv');
		
		$trandate = $this->input->post('trandate');
		//---------
		$type=32;
		
		$nextTran=$this->green->nextTran($type,"Create Cash Return");
			
	
	$curdate=date('Y-m-d H:i:s');
	$user=$this->session->userdata('user_name');
		$mo = '';
		$wh=array('typeno'=>$typeno_h,'type'=>17);
        $mo = $this->db->where($wh)->get('sch_student_fee')->row();
		$created_date = date('Y-m-d H:i:s');
		$da = array(
					
					'type'=>$type,					
					'typeno'=>$nextTran,
					'type_inv'=>$mo->type,									
					'typeno_inv'=>$mo->typeno,
					'ranglev'=>$mo->ranglev,					
					'studentid'=>$mo->studentid,
					'trandate'=>$mo->trandate,
					'classid'=>$mo->classid,
					'schooleleve'=>$mo->schooleleve,
					'termid'=>$mo->termid,
					'amt_total'=>(-$mo->amt_total),
					'amt_paid'=>(-$mo->amt_total),
					'duedate'=>$mo->duedate,
					'programid'=>$mo->programid,
					'acandemic'=>$mo->acandemic,
					'schoolid'=>$mo->schoolid,
					'paymentmethod'=>$mo->paymentmethod,
					'paymenttype'=>$mo->paymenttype,
					'areaid'=>$mo->areaid,
					'busfeetypid'=>$mo->busfeetypid
					);
				$dataold = array(	
					'is_returned'=>1
					);	
			   $this->db->insert('sch_student_fee', $da);
				$this->db->update('sch_student_fee', $dataold,$wh);
				
		if($arr_inv != ""){								  
		foreach($arr_inv as $arr_detail){			
				$datas = array(
					'type' => $type,
					'typeno' =>$nextTran,
					'studentid' =>$this->input->post('stuid'),				
					'trandate'=>$created_date,
	                'otherfee_id' => $arr_detail['paymenttype_list'],
					'description' => $arr_detail['payment_des'],
					'not' => $arr_detail['payment_note'],
	                'prices' => $arr_detail['prices'],
	                'amt_dis' => $arr_detail['amt_dis'],
	                'qty' => $arr_detail['numqty'],
	                'is_bus' => $arr_detail['is_bus'],
	                'areaid' => $this->input->post('oparea'),
	                'busid' => $this->input->post('opbusfeetype'),	               
					'amt_line' => $arr_detail['payment_amt']					
				);
				$this->db->insert('sch_student_fee_type', $datas);
			};// end foreach($arr_inv as $arr_detail){ ==================
		}// if($arr_inv != ""){
			
			$gettypeno['nextTran']=$nextTran;
			header("Content-type:text/x-json");
			echo json_encode($gettypeno);
			exit();
	}


public function FMDelinv($typeno=""){
	//--------- stor history deleted invoice
	$type=21;	
	$nextTran=$this->green->nextTran($type,"Deleted Invoice");
	$curdate=date('Y-m-d H:i:s');
	$user=$this->session->userdata('user_name');
		$mo = '';
        $mo = $this->db->where('typeno', $typeno)->get('sch_student_fee')->row();
		
		//$typenore = $this->db->where('typeno_inv', $type)->get('sch_student_fee_type')->row()->typeno_inv;
		
		$da = array(
					'deleted_byuser'=>$user,
					'deleted_date'=>$curdate,
					'type'=>$type,					
					'typeno'=>$nextTran,
					'type_inv_rec'=>$mo->type,									
					'typeno_inv_rec'=>$mo->typeno,
					'ranglev'=>$mo->ranglev,					
					'studentid'=>$mo->studentid,
					'trandate'=>$mo->trandate,
					'classid'=>$mo->classid,
					'schooleleve'=>$mo->schooleleve,
					'termid'=>$mo->termid,
					'amt_total'=>$mo->amt_total,
					'amt_paid'=>$mo->amt_paid,
					'amt_balance'=>$mo->amt_balance,
					'duedate'=>$mo->duedate,
					'programid'=>$mo->programid,
					'acandemic'=>$mo->acandemic,
					'schoolid'=>$mo->schoolid,
					'paymentmethod'=>$mo->paymentmethod,
					'paymenttype'=>$mo->paymenttype,
					'areaid'=>$mo->areaid,
					'busfeetypid'=>$mo->busfeetypid
					);
			   $this->db->insert('sch_student_fee_deleted', $da);
			   $this->db->where('typeno', $typeno);
        	   $this->db->delete('sch_student_fee');
			   //------------------------------------
			   
			   // $this->db->where('typeno_inv', $typenore);
			   // $this->db->delete('sch_student_fee_rec_order');
			   
			   // $this->db->where('typeno_inv', $typenore);
			   // $this->db->delete('sch_student_fee_rec_detail');
			   //------------------------------------
			   
		$datas="SELECT
					sch_student_fee_type.type,
					sch_student_fee_type.typeno,
					sch_student_fee_type.studentid,
					sch_student_fee_type.trandate,
					sch_student_fee_type.description,
					sch_student_fee_type.`not`,
					sch_student_fee_type.amt_line,
					sch_student_fee_type.prices,
					sch_student_fee_type.amt_dis,
					sch_student_fee_type.qty					
				FROM
					sch_student_fee_type
				where 1=1
				AND sch_student_fee_type.typeno='".$typeno."'
				";
		// echo $datas; exit();
			
		$result = $this->db->query($datas)->result();
		if(isset($result) && count($result)>0){	
			foreach($result as $rowdel){
			$SQL_DM = "INSERT INTO sch_student_fee_inv_detail_deleted";			
			$SQL = " SET studentid='".$rowdel->studentid."',
						 trandate='".$rowdel->trandate."',
						 type='".$type."',
						 typeno='".$nextTran."',
						 type_inv_rec='".$rowdel->type."',					
						 typeno_inv_rec='".$rowdel->typeno."',											 
						 description='".$rowdel->description."',
						 `not`='".$rowdel->not."',
						 qty='".$rowdel->qty."',
						 prices='".$rowdel->prices."',
						 amt_dis='".$rowdel->amt_dis."',
						 amt_line='".$rowdel->amt_line."'
						 ";
			$counts = $this->green->runSQL($SQL_DM . $SQL);
			}
		};	
		//---- end stor history deleted		
		
		$this->db->where('typeno', $typeno);
      	$this->db->delete('sch_student_fee_type');
		
		$updatsql="UPDATE sch_student_enrollment SET 
						type='',
						transno='',
						is_paid=0,
						is_closed=0
					WHERE 1=1
					AND type='17'
					AND transno='".$typeno."'
					  ";
			// echo $updatsql;
			$this->green->runSQL($updatsql);
			
	}

}