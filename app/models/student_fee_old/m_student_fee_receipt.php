<?php
    class m_student_fee_receipt extends CI_Model{

    	// get school level =======
    	function get_schlevel($programid = ''){
	        $qr_schlevel = $this->db->query("SELECT DISTINCT
	                                                l.schlevelid,
	                                                l.sch_level
	                                            FROM
	                                                sch_school_level AS l
	                                            WHERE
	                                                l.programid = '{$programid}' - 0
	                                            ORDER BY
	                                                l.sch_level ASC ")->result();        
	        
	        $arr = array('schlevel' => $qr_schlevel);
	        return json_encode($arr);
	    }
	    // get_schyera ==========
	    function get_schyera($schlevelid = ''){
	        $qr_schyear = $this->db->query("SELECT DISTINCT
	                                                y.yearid,
	                                                y.sch_year
	                                            FROM
	                                                sch_school_year AS y
	                                            WHERE
	                                                y.schlevelid = '{$schlevelid}' - 0
	                                            ORDER BY
	                                                y.sch_year ASC ")->result();        
	        
	        $arr = array('schyear' => $qr_schyear);
	        return json_encode($arr);
	    }
	    // ranglavel===============
	     function get_schranglavel($yearid = ''){
	        $qr_schraglavel = $this->db->query("SELECT DISTINCT
												rl.rangelevelid,
												rl.rangelevelname
											FROM
												sch_school_rangelevel AS rl
											INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
											AND rl.programid = y.programid
											WHERE
												y.yearid = '{$yearid}' - 0
											ORDER BY
												rl.rangelevelname ASC")->result();        
	        
	        $arr = array('schrang' => $qr_schraglavel);
	        return json_encode($arr);
	    }
	    //classname==============
	    function get_schclass($schlevelid = ''){
	        $qr_schclass = $this->db->query("SELECT DISTINCT
												sl.schlevelid,
												cl.class_name,
												cl.classid
											FROM
												sch_school_level AS sl
											INNER JOIN sch_class AS cl ON sl.schlevelid = cl.schlevelid
											AND sl.schlevelid = cl.schlevelid
											WHERE
												sl.schlevelid = '{$schlevelid}' - 0
											ORDER BY
												cl.class_name ASC")->result();        
	        
	        $arr = array('schclass' => $qr_schclass);
	        return json_encode($arr);
	    }
	    // schollinfor
	    function schooinfor($schoolid='1'){
            return $this->db->query("SELECT DISTINCT
											inf.schoolid,
											inf.`name`
										FROM
											sch_school_infor AS inf
										WHERE
											inf.schoolid = '{$schoolid}' - 0
										ORDER BY
											inf.`name` ASC")->result();
        }
        // status
		function status(){
            return $this->db->query("SELECT DISTINCT
										st.is_status
									FROM
										sch_student_fee AS st
									ORDER BY
										st.is_status")->result();
		}


		
	function MgetfeeList($schooleleve="",$classid="",$termid="",$studentid=""){
	    	if($studentid!=""){
	    		$this->db->where("studentid",$studentid);
	    	}
			if($schooleleve!=""){
	    		$this->db->where("schooleleve",$schooleleve);
	    	}
			
	    	if($classid!=""){
	    		$this->db->where("classid",$classid);
	    	}
	    	if($termid!=""){
	    		$this->db->where("termid",$termid);
	    	}
	        return $this->db->get('sch_student_fee')->result();

	}
	
	function getstudent()
    {
    	$studentid = $this->input->post('stuid_pos');				
		$this->db->where("studentid",$studentid);
        return $this->db->get('sch_student')->row();

    }
		
function MFsaveRec(){
		$text_note=$this->input->post("text_note");
		$schoolid=$this->session->userdata("schoolid");		
		$student_num = $this->input->post('student_num');	
		$typeno = $this->input->post('h_typeno');	
		$trandate = $this->green->formatSQLDate($this->input->post('trandate'));
		$type=18;
		
		$nextTran=$this->green->nextTran($type,"Student Receipt");
		$created_date = date('Y-m-d H:i:s');
		// echo "this:".$typeno; exit();
		
		if($typeno!=""){
			$invorder="";
	    	$invorder=$this->db->where("typeno",$typeno)->get('sch_student_fee')->row();
			$type_inv=$invorder->type;
			$typeno=$invorder->typeno;
			$stuid=$invorder->studentid;
			
			$schooleleve=$invorder->schooleleve;
			$acandemic=$invorder->acandemic;
			$ranglev=$invorder->ranglev;
			$classid=$invorder->classid;
			$paymentmethod=$invorder->paymentmethod;
			
			$paymenttype=$invorder->paymenttype;
			$areaid=$invorder->areaid;
			$busfeetypid=$invorder->busfeetypid;
			// $trandate=$invorder->trandate;
			$programid=$invorder->programid;
			$timeid=$invorder->timeid;
			$note=$invorder->note;
			
			$datarecipt = array(
						'type' => $type,
						'typeno' =>$nextTran,
						
						'type_inv' => $type_inv,
						'typeno_inv' =>$typeno,									
						'studentid' =>$stuid,
						'trandate'=>$trandate,
						'amt_paid' => $this->input->post('amt_deposit'),
						
						'schoolid' => $schoolid,
						'programid' => $programid,
						'schooleleve' => $schooleleve,
						'acandemic' => $acandemic,
						'ranglev' => $ranglev,
						'classid' => $classid,
						'paymentmethod' => $paymentmethod,
						'paymenttype' => $paymenttype,
						'areaid' => $areaid,
						'note' => $text_note,
						'timeid' => $timeid,
						'busfeetypid' => $busfeetypid
				);
				$this->db->insert('sch_student_fee_rec_order', $datarecipt);
	   		} 
			
			
			//------------- Detail
			$studentfee="";
			if($typeno!=""){
	    		$studentfee=$this->db->where("typeno",$typeno)->get('sch_student_fee_type')->result();
	    	}
			$i=1;
			foreach($studentfee as $rowstufee){
				$type=($rowstufee->type);
				$typeno=($rowstufee->typeno);
				$studentid=($rowstufee->studentid);
				$description=($rowstufee->description);
				$not=($rowstufee->not);
				$prices=($rowstufee->prices);
				$amt_dis=($rowstufee->amt_dis);
				$qty=($rowstufee->qty);
				$amt_line=($rowstufee->amt_line);
				$otherfee_id=($rowstufee->otherfee_id);
				$is_bus=($rowstufee->is_bus);
				$areaid=($rowstufee->areaid);
				$busid=($rowstufee->busid);

				if($rowstufee->prices > 0){
					$datas = array(
									'type' => 18,
									'typeno' =>$nextTran,
									'type_inv' => $type_inv,
									'typeno_inv' =>$typeno,	
									'studentid' =>$studentid,
									'trandate'=>$trandate,
									'description' => $description,
									'not' => $not,
									'prices' => $prices,
									'amt_dis' => $amt_dis,
									'qty' => $qty,
									'otherfee_id' => $otherfee_id,
									'is_bus' => $is_bus,
									'areaid' => $areaid,
									'busid' => $busid,
									'amt_line' => $amt_line
								 );	
					$this->db->insert('sch_student_fee_rec_detail', $datas);
				}
			}
			// ------------	
			
			$gettypeno['nextTran']=$nextTran;
			header("Content-type:text/x-json");
			echo json_encode($gettypeno);
	}
	
 
 }
?>