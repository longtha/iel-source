<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_score_rank_by_subject extends CI_Model {
	public function sqlGetResult($sql)
	{
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result();
		}else{
			return NULL;
		}
	}
	public function sqlGetRow($sql){
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row();
		}else{
			return NULL;
		}
	}
	public function modGetSchoolYear($schoolid,$programid,$schlevelid,$yearid){
		$set_where='';
		if($schoolid!=''){
			$set_where.=" AND sy.schoolid = '".$schoolid."'";
		}
		if($schlevelid!=''){
			$set_where.=" AND sy.schlevelid = '".$schlevelid."'";
		}
		if($programid!=''){
			$set_where.=" AND sy.programid = '".$programid."'";
		}
		if($yearid!=''){
			$set_where.=" AND sy.yearid = '".$yearid."'";
		}
		$sqlyear = "SELECT
						sy.sch_year,
						sy.from_date,
						sy.to_date,
						CONCAT(
							YEAR(sy.from_date),
							'-',
							YEAR(sy.to_date)
						) AS schyear
					FROM
						sch_school_year AS sy
					WHERE
						1 = 1 {$set_where}
					";
		$result=$this->db->query($sqlyear);
		if($result->num_rows()>0){
			return $result->row()->schyear;
		}else{
			return NULL;
		}
	}
	public function modGetClass($school_id,$school_level_id,$grade_level_id){
		$set_where='';
		if($school_id!=''){
			$set_where.=" AND c.schoolid = '".$school_id."'";
		}
		if($school_level_id!=''){
			$set_where.=" AND c.schlevelid = '".$school_level_id."'";
		}
		if($grade_level_id!=''){
			$set_where.=" AND c.grade_levelid = '".$grade_level_id."'";
		}

		$sql_class = "SELECT
						c.classid,
						c.class_name
					FROM
						sch_class AS c
					WHERE
						1 = 1 {$set_where}";	  
        return $this->sqlGetResult($sql_class);
	}
	function getStudentInformationSearch($schoolid,$programid,$schlavelid,$yearid,$gradlavelid,$class_id){
		$set_where='';
		if($schoolid!=''){
			$set_where.=" AND s_score.school_id = '".$schoolid."'";
		}
		if($programid!=''){
			$set_where.=" AND s_score.program_id = '".$programid."'";
		}
		if($schlavelid!=''){
			$set_where.=" AND s_score.school_level_id = '".$schlavelid."'";
		}
		if($yearid!=''){
			$set_where.=" AND s_score.adcademic_year_id = '".$yearid."'";
		}
		if($gradlavelid!=''){
			$set_where.=" AND s_score.grade_level_id = '".$gradlavelid."'";
		}
		if($class_id!=''){
			$set_where.=" AND s_score.class_id = '".$class_id."'";
		}
			$sql_student = "SELECT
							s_score.student_id,
							vst.student_num,
							vst.fullname,
							s_score.class_id
						FROM
							sch_subject_score_semester_kgp_multi AS s_score
						INNER JOIN v_student_enroment AS vst ON vst.studentid = s_score.student_id
						AND vst.classid = s_score.class_id
						WHERE
							1 = 1 {$set_where}";
    		return $this->sqlGetResult($sql_student);
    	}
    function getSubjectName($schoolid,$programid,$schlevelid,$grade_levelid){
    	$set_where='';
		if($schoolid!=''){
			$set_where.=" AND v_sub.schoolid = '".$schoolid."'";
		}
		if($programid!=''){
			$set_where.=" AND v_sub.programid = '".$programid."'";
		}
		if($schlevelid!=''){
			$set_where.=" AND v_sub.schlevelid = '".$schlevelid."'";
		}
		if($grade_levelid!=''){
			$set_where.=" AND v_sub.grade_levelid = '".$grade_levelid."'";
		}
    	$sql_subject = "SELECT
							v_sub.subjectid,
							v_sub.`subject`,
							v_sub.subj_type_id,
							v_sub.short_sub,
							v_sub.schlevelid,
							v_sub.subject_kh,
							v_sub.orders,
							v_sub.sch_level,
							v_sub.subject_type,
							v_sub.grade_level,
							v_sub.subj_grad_id,
							v_sub.exam_type_id,
							v_sub.grade_levelid,
							v_sub.is_trimester_sub,
							v_sub.is_eval,
							v_sub.exam_name,
							v_sub.max_score
						FROM
							v_school_subject AS v_sub
						WHERE
							1 = 1 {$set_where}
						ORDER BY
							v_sub.`subject`,
							v_sub.subjectid,
							v_sub.schlevelid";
		return $this->sqlGetResult($sql_subject);
    	}
    function getStudentInformationBySubject($schoolid,$programid,$schlavelid,$yearid,$gradlavelid,$class_id,$studentid,$check_semeste){
		$set_where='';
		if($schoolid!=''){
			$set_where.=" AND s_score.school_id = '".$schoolid."'";
		}
		if($programid!=''){
			$set_where.=" AND s_score.program_id = '".$programid."'";
		}
		if($schlavelid!=''){
			$set_where.=" AND s_score.school_level_id = '".$schlavelid."'";
		}
		if($yearid!=''){
			$set_where.=" AND s_score.adcademic_year_id = '".$yearid."'";
		}
		if($gradlavelid!=''){
			$set_where.=" AND s_score.grade_level_id = '".$gradlavelid."'";
		}
		if($class_id!=''){
			$set_where.=" AND s_score.class_id = '".$class_id."'";
		}
		if($studentid!=''){
			$set_where.=" AND s_score.student_id = '".$studentid."'";
		}
			$sqlStd = "SELECT
							s_score.sub_sesmester_score_id,
							s_score.student_id,
							vst.student_num,
							vst.fullname,
							vst.gender,
							vst.sch_year,
							vst.class_name,
							s_score.school_id,
							s_score.program_id,
							s_score.school_level_id,
							s_score.adcademic_year_id,
							s_score.grade_level_id,
							s_score.class_id,
							s_score.get_monthly,
							s_score.total_score,
							s_score.average_score,
							s_score.averag_coefficient,
							s_score.average_monthly_score,
							s_score.exam_semester
						FROM
							sch_subject_score_semester_kgp_multi AS s_score
						INNER JOIN v_student_enroment AS vst ON vst.studentid = s_score.student_id
						AND vst.classid = s_score.class_id
						WHERE
							1 = 1 {$set_where}
						 AND s_score.exam_semester ='".$check_semeste."'
					";
    		return $this->sqlGetResult($sqlStd);
    	}
    
    function getSubjectPoint($schoolid,$programid,$schlavelid,$yearid,$gradlavelid,$class_id,$studentid,$subject_id,$check_semeste){
		$set_where='';
		if($schoolid!=''){
			$set_where.=" AND ss_detail.school_id = '".$schoolid."'";
		}
		if($programid!=''){
			$set_where.=" AND ss_detail.program_id = '".$programid."'";
		}
		if($schlavelid!=''){
			$set_where.=" AND ss_detail.school_level_id = '".$schlavelid."'";
		}
		if($yearid!=''){
			$set_where.=" AND ss_detail.adcademic_year_id = '".$yearid."'";
		}
		if($gradlavelid!=''){
			$set_where.=" AND ss_detail.grade_level_id = '".$gradlavelid."'";
		}
		if($class_id!=''){
			$set_where.=" AND ss_detail.class_id = '".$class_id."'";
		}
		if($studentid!=''){
			$set_where.=" AND ss_detail.student_id = '".$studentid."'";
		}
		if($check_semeste!=''){
			$set_where.=" AND ss_detail.exam_semester ='".$check_semeste."'";
		}
    	$sql_subject = "SELECT
								 s_score.sub_sesmester_score_id,
								 s_score.student_id,
								 s_score.get_monthly,
								 s_score.total_score AS m_total_score,
								 s_score.average_score AS m_average_score,
								 s_score.averag_coefficient  AS m_averag_coefficient,
								 s_score.average_monthly_score,
								 s_score.exam_semester AS m_exam_semester,
								 ss_detail.sub_month_score_detail_id,
								 ss_detail.subject_mainid,
								 ss_detail.subject_groupid,
								 ss_detail.subject_id,
								 ss_detail.exam_monthly,
								 ss_detail.total_score,
								 ss_detail.average_score,
								 ss_detail.averag_coefficient,
								 ss_detail.exam_semester,
								 FIND_IN_SET(
									ss_detail.total_score,
									(
										SELECT
											GROUP_CONCAT(
												ss_detail.total_score
												ORDER BY ss_detail.total_score DESC
											)
										FROM
											sch_subject_score_semester_detail_kgp_multi AS ss_detail
										WHERE 1=1 {$set_where} ORDER BY ss_detail.total_score DESC
									)
								) AS rank
							FROM
								sch_subject_score_semester_kgp_multi AS s_score
							INNER JOIN sch_subject_score_semester_detail_kgp_multi AS ss_detail ON ss_detail.class_id = s_score.class_id
							AND ss_detail.student_id = s_score.student_id
							AND ss_detail.grade_level_id = s_score.grade_level_id
							AND ss_detail.exam_semester = s_score.exam_semester
							WHERE
								1 = 1
							{$set_where}
							AND ss_detail.subject_id IN(".trim($subject_id,',').")
							ORDER BY ss_detail.total_score DESC
							";
		return $this->sqlGetResult($sql_subject);
    	}
    	
 		function getStudentGetScoreFinalKGP($schoolid,$programid,$schlavelid,$yearid,$gradlavelid,$class_id,$studentid){
			$set_where='';
			if($schoolid!=''){
				$set_where.=" AND s.school_id = '".$schoolid."'";
			}
			if($programid!=''){
				$set_where.=" AND s.program_id = '".$programid."'";
			}
			if($schlavelid!=''){
				$set_where.=" AND s.school_level_id = '".$schlavelid."'";
			}
			if($yearid!=''){
				$set_where.=" AND s.adcademic_year_id = '".$yearid."'";
			}
			if($gradlavelid!=''){
				$set_where.=" AND s.grade_level_id = '".$gradlavelid."'";
			}
			if($class_id!=''){
				$set_where.=" AND s.class_id = '".$class_id."'";
			}
			if($studentid!=''){
				$set_where.=" AND s.student_id = '".$studentid."'";
			}
			$sqlSubFin = "SELECT
							s.sub_final_score_id,
							s.student_id,
							vst.student_num,
							vst.fullname,
							vst.gender,
							vst.sch_year,
							vst.class_name,
							s.class_id,
							s.grade_level_id,
							s.adcademic_year_id,
							s.school_level_id,
							s.program_id,
							s.school_id,
							s.total_avg_score_s1,
							s.total_avg_score_s2,
							s.total_average_score,
							s.ranking,
							s.averag_coefficient,
							s.absent,
							s.present
						FROM
							sch_subject_score_final_kgp_multi AS s
						INNER JOIN v_student_enroment AS vst ON vst.studentid = s.student_id
						AND vst.classid = s.class_id
						WHERE
							1 = 1 {$set_where}
					";
    		return $this->sqlGetResult($sqlSubFin);
    	}
    	function getSubjectScoreFinalKGPDetail($schoolid,$programid,$schlavelid,$yearid,$gradlavelid,$class_id,$studentid){
			$set_where='';
			if($schoolid!=''){
				$set_where.=" AND sd.schoolid = '".$schoolid."'";
			}
			if($programid!=''){
				$set_where.=" AND sd.programid = '".$programid."'";
			}
			if($schlavelid!=''){
				$set_where.=" AND sd.schlevelid = '".$schlavelid."'";
			}
			if($yearid!=''){
				$set_where.=" AND sd.yearid = '".$yearid."'";
			}
			if($gradlavelid!=''){
				$set_where.=" AND sd.grade_level_id = '".$gradlavelid."'";
			}
			if($class_id!=''){
				$set_where.=" AND sd.classid = '".$class_id."'";
			}
			if($studentid!=''){
				$set_where.=" AND sd.student_id = '".$studentid."'";
			}
			$sqlSubFinDetail = "SELECT
							sd.sub_final_score_detail_id,
							sd.student_id,
							sd.schoolid,
							sd.programid,
							sd.schlevelid,
							sd.yearid,
							sd.grade_level_id,
							sd.classid,
							sd.subject_mainid,
							sd.subject_group,
							sd.subjectid,
							sd.total_score,
							sd.rank
						FROM
							sch_subject_score_final_detail_kgp_multi AS sd
						WHERE
							1 = 1 {$set_where}
					";
    		return $this->sqlGetResult($sqlSubFinDetail);
    	} 
    	function modMetion($schlevelid){
    		$sql_mention  = "SELECT
									sch_score_mention_gep.menid,
									sch_score_mention_gep.mention_kh,
									sch_score_mention_gep.schlevelid,
									sch_score_mention_gep.min_score,
									sch_score_mention_gep.max_score
							FROM
							sch_score_mention_gep
							WHERE schlevelid='".$schlevelid."'";
			return $this->sqlGetResult($sql_mention);
    	}     
}