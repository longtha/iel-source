<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModeThreatment extends CI_Model {

	public function getdoctor()
	{	
		$doc_con= "(emp_p.match_con_posid='doc' OR emp_p.match_con_posid='nur')";
		
		return $this->db->select('*')
				->from('sch_emp_profile emp')
				->join('sch_emp_position emp_p','emp.pos_id=emp_p.posid','inner')
				->where($doc_con)->get()->result();
		
	}
	function getschoollevel(){
		$query=$this->db->get('sch_school_level');
		return $query->result();
	}
	function save($threatmentid=''){
		$date 			   = $this->green->formatSQLDate($this->input->post('date'));
		$doctorid 		   = $this->input->post('doctor');
		$patientid 		   = $this->input->post('patientid');
		$patient_type  	   = $this->input->post('patient_type');
		$next_visit_date   = $this->green->formatSQLDate($this->input->post('next_visit_date'));
		$is_internal_threat= 1;
		
		$schoolid='';
		$yearid='';
		$tranno=0;
		if($patient_type=='student'){
			$schoolid=$this->session->userdata('schoolid');
			$yearid=$this->session->userdata('year');
		}		
		$classid 		   = $this->input->post('classid');
		$schlevelid		   = $this->input->post('schlevel');
		$sponfree  		   = $this->input->post('sponfree');
		$external_hospital = $this->input->post('external_hospital');

		if($external_hospital!=""){
			$is_internal_threat=0;			
		}

		// echo $schlevelid;
		// die();
		
		$data=array(
				"date" 				=> $date,
				"doctorid" 			=> $doctorid,
				"patientid" 		=> $patientid,
				"patient_type"      => $patient_type,
				"next_visit_date"   => $next_visit_date,
				"is_internal_treat" => $is_internal_threat,
				"schoolid" 			=> $schoolid,
				"yearid" 			=> $yearid,
				"external_hospital" => $external_hospital,
				"classid" 			=> $classid,
				"schlevelid"		=> $schlevelid			
			);
		if($threatmentid!=''){
				$last_modified_by=$this->session->userdata('user_name');
				$last_modified_date=date('Y-m-d H:i:s');
				$data2=array('modified_by'=>$last_modified_by,
							 'modified_date'=>$last_modified_date);
				$this->db->where('treatmentid',$threatmentid)
						->update('sch_medi_treatment',array_merge($data, $data2));
				$tranno=$this->db->select('transno')
						->from('sch_medi_treatment')
						->where('treatmentid',$threatmentid)
						->get()->row()->transno;
				$this->cleartreatdis($threatmentid);
				$this->removestockmove(7,$tranno,$threatmentid);

		}else{
				$created_by=$this->session->userdata('user_name');
				$created_date=date('Y-m-d H:i:s');
				$tranno=$this->db->select('sequence')
						->from('sch_z_systype')
						->where('typeid',7)
						->get()->row()->sequence;
				$data2=array('created_by'=>$created_by,
							 'created_date'=>$created_date,
							 'transno'=>$tranno,
							 'type'=>7);
				$this->db->insert('sch_medi_treatment',array_merge($data, $data2));
				$threatmentid=$this->db->insert_id();
				$this->updatetran($tranno+1,7);
		}

		$this->savedisease($threatmentid);
		$this->savemedicine($threatmentid,$date,7,$tranno);
		return $threatmentid;
	}
	function updatetran($sequence,$typeid){
		$this->db->set('sequence',$sequence)
				->where('typeid',$typeid)
				->update('sch_z_systype');
	}
	function removestockmove($type,$tran,$treatmentid){
		$stock_move=$this->db->where('type',$type)
				->where('transno',$tran)
				->get('sch_stock_stockmove')->result();
				
		foreach ($stock_move as $move) {
			$this->db->query("UPDATE sch_stock_balance SET quantity=quantity - $move->quantity WHERE stockid='$move->stockid' AND whcode='$move->whcode' AND expired_date='$move->expired_date'");
		}
		$this->db->where('type',$type)
				->where('transno',$tran)
				->delete('sch_stock_stockmove');
		$this->cleartreatmidecin($treatmentid);
	}
	function cleartreatdis($treatmentid){
		$this->db->where('treatmentid',$treatmentid)->delete('sch_medi_treatment_disease');
	}
	function cleartreatmidecin($treatmentid){
		$this->db->where('treatmentid',$treatmentid)->delete('sch_medi_treatment_medicine');
	}
	function savedisease($threatmentid){
		$diseaseid=$this->input->post('diseaseid');
		$sponfrees=$this->input->post('sponfrees');
		$i=0;
		foreach($diseaseid as $diseaseid) {
			$data=array("treatmentid"=>$threatmentid,"diseaseid"=>$diseaseid,"sponfree"=>$sponfrees[$i]);
			$this->db->insert('sch_medi_treatment_disease',$data);
			$i++;
		}
	}
	function savemedicine($treatmentid,$date,$typeid,$tranno){
		$stock_id=$this->input->post('r_stockid');
		$i=0;
		
		foreach($stock_id as $stock_id) {
			$expire_date=$this->input->post('r_expire_date');
			$qty=$this->input->post('r_qty');
			$days_use=$this->input->post('r_day_use');
			$perday_use=$this->input->post('r_perday_use');
			$pertimes_use=$this->input->post('r_pertime_use');
			$note=$this->input->post('r_note');
			$uom=$this->input->post('r_uom');
			$whcode=$this->input->post('r_whcode');
			$data=array(
					"treatmentid"=>$treatmentid,
					"stockid"=>$stock_id,
					"expire_date"=>$this->green->formatSQLDate($expire_date[$i]),
					"qty"=>$qty[$i],
					"days_use"=>$days_use[$i],
					"perday_use"=>$perday_use[$i],
					"pertimes_use"=>$pertimes_use[$i],
					"note"=>$note[$i],
					"uom"=>$uom[$i],
					);
			$this->db->insert('sch_medi_treatment_medicine',$data);
			$this->addstockmove($stock_id,$qty[$i],$uom[$i],$this->green->formatSQLDate($expire_date[$i]),$date,$typeid,$tranno,$whcode[$i]);
		$i++;
		}
		
	}
	function addstockmove($stockid,$qty,$uom,$expire_date,$date,$typeid,$tranno,$whcode){
		$created_by=$this->session->userdata('user_name');
		$data=array('stockid'=>$stockid,
					'date'=>$date,
					'quantity'=>"-$qty",
					'uom'=>$uom,
					'whcode'=>$whcode,
					'expired_date'=>$expire_date,
					'created_by'=>$created_by,
					'type'=>$typeid,
					'transno'=>$tranno
					);
		$this->db->insert('sch_stock_stockmove',$data);
		$this->updatestockbalance($stockid,$expire_date,$qty,$whcode);
	}
	function updatestockbalance($stockid,$expire_date,$qty,$whcode){
			$this->db->query("UPDATE sch_stock_balance SET quantity=quantity-($qty) WHERE stockid='$stockid' AND whcode='$whcode' AND expired_date='".$expire_date."'");
	}
	function getmidicat(){
		return $this->db->where('stocktype','medecine')->get('sch_stock_category')->result();
	}
	function gettreatmentrow($treatmentid){
		return $this->db->where('treatmentid',$treatmentid)->get('sch_medi_treatment')->row_array();
	}
	function gettreatmentbypatient($patientid,$patient_type){
		return $this->db->where('patientid',$patientid)
					->where('patient_type',$patient_type)
					->get('sch_medi_treatment')
					->result();
	}
	function getmedicinebycat($catid){
		return $this->db->where('categoryid',$catid)->get('sch_stock')->result();
	}
	function getmedicinebycatname($catid,$key){
		$this->db->like('descr_eng',$key);
		if($catid!='' && $catid !=0)
			$this->db->where('categoryid',$catid);
		return $this->db->get('sch_stock')->result();
	}
	function getexpire($stockid,$whcode){
		return $this->db->where('stockid',$stockid)->where('whcode',$whcode)->get('sch_stock_balance')->result();
	}
	function getillness($consult){
		return $this->db->where('type_of_consultation',$consult)->get('sch_medi_illness_type')->result();
	}
	function getdisease($illnessid){
		return $this->db->where('illness_typeid',$illnessid)->get('sch_medi_disease')->result();
	}
	function gettreatdisease($treatid){
		return $this->db->select('*')
					->from('sch_medi_treatment_disease td')
					->join('sch_medi_disease d','d.diseaseid=td.diseaseid','LEFT')
					->where('td.treatmentid',$treatid)->get()->result();
	}
	function gettreatmidecine($treatid){
		return $this->db->select('*')
					->from('sch_medi_treatment_medicine tm')
					->join('sch_stock s','tm.stockid=s.stockid','inner')
					->where('tm.treatmentid',$treatid)
					->get()->result();
	}
	function getstocktreat($treatid,$stockid){
		return $this->db->select('*')
					->from('sch_medi_treatment_medicine tm')
					->join('sch_stock s','tm.stockid=s.stockid','inner')
					->where('tm.treatmentid',$treatid)
					->where('tm.stockid',$stockid)
					->get()->result();
	}
	function gettreatmidecinegroup($treatid){
		return $this->db->select('*')
					->from('sch_medi_treatment_medicine tm')
					->join('sch_stock s','tm.stockid=s.stockid','inner')
					->where('tm.treatmentid',$treatid)
					->group_by('s.descr_eng')
					->get()->result();
	}
	function gettreatment(){
			$page=0;
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				$m='';
				$p='';
				if(isset($_GET['m'])){
			        $m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }	
				$sql="SELECT DATE_FORMAT(mt.date,'%d-%m-%Y') as date,
						mt.treatmentid,
						mt.patientid,
						s.last_name,
						s.first_name,
						s.sex,
						DATE_FORMAT(s.dob,'%d-%m-%Y') as dob,
						p.position,
						c.class_name
						-- doc.last_name as doc_last,
						-- doc.first_name as doc_first
					FROM sch_medi_treatment mt
					LEFT JOIN v_se_health s
					ON(mt.patientid=s.studentid)
					LEFT JOIN sch_emp_position p
					ON(s.pos_id=p.posid)
					LEFT JOIN sch_class c
					ON(mt.classid=c.classid)									
					where s.type=mt.patient_type 
					ORDER BY mt.treatmentid DESC ";

					$config['base_url'] =site_url()."/health/threatment?pg=1&m=$m&p=$p";	
					$config['total_rows'] = $this->green->getTotalRow($sql);
					$config['per_page'] =200;
					$config['num_link']=5;
					$config['page_query_string'] = TRUE;
					$config['full_tag_open'] = '<li>';
					$config['full_tag_close'] = '</li>';
					$config['cur_tag_open'] = '<a><u>';
					$config['cur_tag_close'] = '</u></a>';
					$this->pagination->initialize($config);
					$limi=" limit ".$config['per_page'];
					if($page>0){
							$limi=" limit ".$page.",".$config['per_page'];
						}		
						$sql.=" {$limi}";
			return $this->db->query($sql)->result();
	}
	function search($doctorid,$patient_name,$posid,$schlevelid,$classid,$s_type,$sort_num,$m,$p,$yearid){
		$page=0;
		if(isset($_GET['per_page']))
			$page=$_GET['per_page'];
		$where='';
		if($doctorid!='')
			$where.=" AND mt.doctorid='$doctorid'";
		if($posid!='')
			$where.=" AND s.pos_id='$posid'";
		if($classid!='')
			$where.=" AND mt.classid='$classid'";
		if($schlevelid!='' && $s_type=='student')
			$where.=" AND c.schlevelid='$schlevelid'";
		if($schlevelid!='' && $s_type=='emp')
			$where.=" AND mt.schlevelid='$schlevelid'";
		if($s_type!='')
			$where.=" AND mt.patient_type='$s_type'";
		if($patient_name!='')
			$where.=" AND (CONCAT(s.last_name,' ',s.first_name) LIKE '%".$patient_name."%'
						    OR CONCAT(s.first_name,' ',s.last_name) LIKE '%".$patient_name."%'
					)";
		if($yearid!=''){
			$where.=" AND mt.yearid='$yearid'";
		}

		$sql="SELECT DATE_FORMAT(mt.date,'%d-%m-%Y') as date,
				mt.treatmentid,
				mt.patientid,
				s.last_name,
				s.first_name,
				s.sex,
				DATE_FORMAT(s.dob,'%d-%m-%Y') as dob,
				p.position,
				c.class_name,
				doc.last_name as doc_last,
				doc.first_name as doc_first
			FROM sch_medi_treatment mt
			LEFT JOIN v_se_health s
			ON(mt.patientid=s.studentid)
			LEFT JOIN sch_emp_position p
			ON(s.pos_id=p.posid)
			LEFT JOIN sch_class c
			ON(mt.classid=c.classid)
			INNER JOIN sch_emp_profile doc
			ON(mt.doctorid=doc.empid)
			WHERE s.type=mt.patient_type
			{$where}
			ORDER BY mt.treatmentid DESC";
		
		$config['base_url'] = site_url()."/health/threatment/search?pg=1&doc=$doctorid&pat=$patient_name&pos=$posid&class=$classid&s_type=$s_type&s_num=$sort_num&m=$m&p=$p&l=$schlevelid&y=$yearid";		
		$config['total_rows'] = $this->green->getTotalRow($sql);
		$config['per_page'] =$sort_num;
		$config['num_link']=5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';
		$this->pagination->initialize($config);
		$limi=" limit ".$config['per_page'];
		if($page>0){
				$limi=" limit ".$page.",".$config['per_page'];
			}		
			$sql.=" {$limi}";
		return $this->db->query($sql)->result();
	}
}