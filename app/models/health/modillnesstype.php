<?php
	class modillnesstype extends CI_Model{
		function getillness($m,$p){
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
				$sql="SELECT * FROM sch_medi_illness_type ORDER BY illness_typeid DESC";	
				$config['base_url'] =site_url()."/health/illnesstype?pg=1&m=$m&p=$p";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =10;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				return $this->db->query($sql)->result();
		}
		function validate($illness_type,$illness_typeid=''){
			$this->db->select('count(*) as count')
					->from('sch_medi_illness_type')
					->where('illness_type',$illness_type);
			if($illness_typeid!='')
				$this->db->where_not_in('illness_typeid',$illness_typeid);
			return $this->db->count_all_results();
		}
		function save($illnesstypeid=''){
			$illness_type=$this->input->post('illness_type');
			$color=$this->input->post('color');
			$ctype=$this->input->post('ctype');
			$desc=$this->input->post('description');

			$data=array(
						'illness_type'		   => $illness_type,
						'color'		  		   => $color,
						'type_of_consultation' => $ctype,
						'description' 		   => $desc
						);
			if($illnesstypeid!=''){
				$this->db->where('illness_typeid',$illnesstypeid)
						->update('sch_medi_illness_type',$data);
			}else{
				$this->db->insert('sch_medi_illness_type',$data);
			}
		}
		function search($illness_type,$ctype,$s_num,$m,$p){
			$page=0;
			$where='';
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			if($illness_type!=""){
				$where=" AND illness_type LIKE '%".$illness_type."%'";
			}
			if($ctype!=""){
				$where=" AND type_of_consultation LIKE '%".$ctype."%'";
			}

			$sql="SELECT * FROM sch_medi_illness_type WHERE 1=1 {$where} ORDER BY illness_typeid DESC";	
			$config['base_url'] =site_url()."/health/illnesstype/search?pg=1&dc=$illness_type&d=$ctype&s_num=$s_num&m=$m&p=$p";	
			$config['total_rows'] = $this->green->getTotalRow($sql);
			$config['per_page'] =$s_num;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);
			$limi=" limit ".$config['per_page'];
			if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}		
			$sql.=" {$limi}";
			return $this->db->query($sql)->result();
		}
}