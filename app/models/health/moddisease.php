<?php
	class ModDiSease extends CI_Model{
		function getdisease($m,$p){
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
				$where='';
				if(isset($_GET['illness_typeid']))	
					$where="WHERE md.illness_typeid='".$_GET['illness_typeid']."'";
				$sql="SELECT * 
						FROM sch_medi_disease md
						INNER JOIN sch_medi_illness_type it
						ON(md.illness_typeid=it.illness_typeid)
						{$where}
						ORDER BY md.diseaseid DESC";	
				$config['base_url'] =site_url()."/health/disease?pg=1&m=$m&p=$p";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =30;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				return $this->db->query($sql)->result();
		}
		function validate($disease,$diseaseid=''){
			$this->db->select('count(*)')
					->from('sch_medi_disease')
					->where('disease',$disease);
			if($diseaseid!='')
				$this->db->where_not_in('diseaseid',$diseaseid);
			return $this->db->count_all_results();
		}
		function save($diseaseid=''){
			$illness_type=$this->input->post('illness_type');
			$disease_code=$this->input->post('disease_code');
			$disease=$this->input->post('disease');
			$date=date('Y-m-d');
			$user=$this->session->userdata('user_name');
			$data=array(
						'disease_code'=>$disease_code,
						'disease'=>$disease,
						'illness_typeid'=>$illness_type
						);
			if($diseaseid!=''){
				$data2=array('modified_date'=>$date,
							'modified_by'=>$user);
				$this->db->where('diseaseid',$diseaseid)
						->update('sch_medi_disease',array_merge($data,$data2));
			}else{
				$data2=array('created_date'=>$date,
							'created_by'=>$user);
				$this->db->insert('sch_medi_disease',array_merge($data,$data2));
			}
		}
		function search($disease_code,$disease,$illness_typeid,$s_num,$m,$p){
			$page=0;
			$where='';
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			if($disease_code!=""){
				$where=" AND md.diseaseid LIKE '%".$disease_code."%'";
			}
			if($disease!=""){
				$where=" AND md.disease LIKE '%".$disease."%'";
			}
			if($illness_typeid!=""){
				$where=" AND md.illness_typeid='".$illness_typeid."'";
			}
				$sql="SELECT * 
						FROM sch_medi_disease md
						INNER JOIN sch_medi_illness_type it
						ON(md.illness_typeid=it.illness_typeid) WHERE 1=1
						{$where} ORDER BY md.diseaseid DESC";	
				$config['base_url'] =site_url()."/health/disease/search?pg=1&dc=$disease_code&d=$disease&it=$illness_typeid&s_num=$s_num&m=$m&p=$p";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =$s_num;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				return $this->db->query($sql)->result();
		}
}