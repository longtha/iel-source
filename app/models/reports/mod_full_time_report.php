<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mod_full_time_report extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function get_assessment(){
       $this->db->select("*");
       $this->db->from("sch_subject_gep");
	   $this->db->where('is_assessment',1);
       $query=$this->db->get();
       return $query->result();
	}

	function get_core(){
		$q = $this->db->query("SELECT
										sch_subject_gep.subjectid,
										sch_subject_gep.`subject`,
										sch_subject_gep.subj_type_id,
										sch_subject_gep.short_sub,
										sch_subject_gep.schoolid,
										sch_subject_gep.programid,
										sch_subject_gep.schlevelid,
										sch_subject_gep.is_core,
										sch_subject_gep.is_skill,
										sch_subject_gep.is_assessment,
										sch_subject_gep.max_score,
										sch_subject_gep.calc_score,
										sch_subject_type_gep.is_class_participation,
										sch_subject_type_gep.is_group_calc_percent as gr_percent
									FROM
										sch_subject_gep
									INNER JOIN sch_subject_type_gep ON sch_subject_gep.subj_type_id = sch_subject_type_gep.subj_type_id
									WHERE 1= 1 AND is_core = 1
									ORDER BY
									sch_subject_gep.subjectid,sch_subject_gep.subj_type_id ASC");
		return $q->result();
	}

	function get_skill(){
       $q = $this->db->query("SELECT
									sch_subject_gep.subjectid,
									sch_subject_gep.`subject`,
									sch_subject_gep.subj_type_id,
									sch_subject_gep.short_sub,
									sch_subject_gep.schoolid,
									sch_subject_gep.programid,
									sch_subject_gep.schlevelid,
									sch_subject_gep.is_core,
									sch_subject_gep.is_skill,
									sch_subject_gep.is_assessment,
									sch_subject_gep.max_score,
									sch_subject_gep.calc_score,
									sch_subject_type_gep.is_class_participation,
									sch_subject_type_gep.is_group_calc_percent as gr_percent
								FROM
									sch_subject_gep
								INNER JOIN sch_subject_type_gep ON sch_subject_gep.subj_type_id = sch_subject_type_gep.subj_type_id
								WHERE 1= 1 AND is_skill = 1
								ORDER BY
								sch_subject_gep.subjectid,sch_subject_gep.subj_type_id ASC ");
       return $q->result();
	}

	function get_grad(){
		//$schlevel = $this->input->post("schoollavel");
		$this->db->select("*");
		$this->db->from("sch_score_mention_gep");
		//$this->db->where("schlevelid",$schleve);
		$query_grad=$this->db->get();
       return $query_grad->result();
	}

}