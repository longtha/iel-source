<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Generalkhmerlanguagemodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		//get class data
		public function getClassData($program_id, $school_level_id, $grade_level_id, $adcademic_year_id, $class_id){

			$this->db->select('c.classid, c.class_name');
			$this->db->from('sch_class AS c');

			if($class_id > 0){
				$this->db->where('c.classid', $class_id);
			}

			if($program_id > 0){
				$this->db->where('c.programid', $program_id);
			}
			
			if($school_level_id > 0){
				$this->db->where('c.schlevelid', $school_level_id);
			}

			if($grade_level_id > 0){
				$this->db->where('c.grade_levelid', $grade_level_id);
			}

			if($adcademic_year_id > 0){
				$this->db->where('c.yearid', $adcademic_year_id);
			}

			$result = $this->db->get()->result();
			return $result;
		}

		function getStudent($where){
    		// $sql = $this->db->query("SELECT DISTINCT
						// 				sl.student_id,
						// 				s.last_name,
						// 				s.first_name,
						// 			CONCAT(s.last_name,'  ',s.first_name) AS studentname,
						// 				s.gender,
						// 				sl.class_id,
						// 				sc.class_name,
						// 				y.sch_year,
						// 				sl.program_id,
						// 				sl.school_level_id,
						// 				sl.adcademic_year_id,
						// 				sl.grade_level_id
						// 			FROM
						// 				sch_subject_score_monthly_kgp_multi AS sl
						// 			INNER JOIN sch_student AS s ON sl.student_id = s.studentid
						// 			INNER JOIN sch_class AS sc ON sl.class_id = sc.classid
						// 			INNER JOIN sch_school_year AS y ON sl.adcademic_year_id = y.yearid
						// 			WHERE 1 = 1". $where ." ORDER BY s.last_name ASC ");
			$sql = $this->db->query("SELECT
										v_student_enroment.student_num,
										v_student_enroment.first_name,
										v_student_enroment.last_name,
										v_student_enroment.first_name_kh,
										v_student_enroment.last_name_kh,
										v_student_enroment.gender,
										v_student_enroment.class_name,
										v_student_enroment.studentid
										FROM
										v_student_enroment
										WHERE {$where}");
    		return $sql->result();
    	}


    	function getStudentfinal($where){
    		$sql = $this->db->query("SELECT DISTINCT
										s.first_name_kh,
										s.last_name_kh,
										CONCAT(s.last_name_kh,'  ',s.first_name_kh) AS studentname,
										f.sub_final_score_id,
										c.class_name,
										y.sch_year,
										f.present,
										f.absent,
										f.average_score,
										f.total_avg_score_s2,
										f.total_avg_score_s1,
										f.class_id,
										f.adcademic_year_id,
										f.student_id,
										s.student_num,
										f.ranking_bysubj,
										f.averag_coefficient
									FROM
										sch_subject_score_final_kgp_multi AS f
									LEFT JOIN sch_class AS c ON f.class_id = c.classid
									LEFT JOIN sch_student AS s ON f.student_id = s.studentid
									LEFT JOIN sch_school_year AS y ON f.adcademic_year_id = y.yearid
									WHERE 1 = 1 ". $where ." ORDER BY s.student_num ASC ")->row();
    		return $sql;
    	}

		function getStudentInformation($where)	{  
    		$sql = $this->db->query("SELECT DISTINCT
										sc.class_name,
										s.last_name,
										s.first_name,
										s.first_name_kh,
										s.last_name_kh,
										s.gender,
										sp.program,
										sy.sch_year,
										sl.sub_monthly_score_id,
										sl.present,
										sl.absent,
										sl.student_id,
										sl.average_score,
										sl.exam_monthly,
										sl.averag_coefficient,
										sl.class_id,
										sl.adcademic_year_id,
										sl.program_id,
										sl.total_score,
										sl.ranking_bysubj,
										sl.school_level_id
									FROM
										sch_subject_score_monthly_kgp_multi AS sl
									INNER JOIN sch_class AS sc ON sl.class_id = sc.classid
									INNER JOIN sch_school_program AS sp ON sl.program_id = sp.programid
									INNER JOIN sch_school_year AS sy ON sl.adcademic_year_id = sy.yearid
									INNER JOIN sch_student AS s ON sl.student_id = s.studentid
									WHERE
										1 = 1 ". $where ."
									ORDER BY
										sl.ranking_bysubj ASC ");
								
    		return $sql->result();
    	}

		//calculate average score of a student in a class
		public function calculateAverageScoreMonthly($student_id, $class_id, $grade_level_id,$adcademic_year_id, $school_level_id, $program_id, $get_result_type){
	
			$sql = "SELECT
						ms.total_score,
						ms.average_score,
						ms.ranking_bysubj,
						ms.averag_coefficient,
						ms.student_id,
						ms.teacher_id,
						ms.class_id,
						ms.grade_level_id,
						ms.adcademic_year_id,
						ms.school_level_id,
						ms.program_id,
						ms.school_id,
						ms.exam_monthly
					FROM
						sch_subject_score_monthly_kgp_multi AS ms
					WHERE 1=1
					AND ms.student_id = ". $student_id ." 
				    AND ms.class_id = ". $class_id ." 
					AND ms.grade_level_id = ". $grade_level_id ." 
					AND ms.adcademic_year_id = ". $adcademic_year_id ." 
					AND ms.school_level_id = ". $school_level_id ." 
					AND ms.program_id = ". $program_id ." 
					AND ms.exam_monthly = ". $get_result_type."
					ORDER BY
						ms.ranking_bysubj ASC ";
			//print_r($sql);		
			$query = $this->db->query($sql);
            $num_row = $query->num_rows();
            $row = 0;
            if($num_row > 0){
                return $row = $query->row();
            }else{
                return $row;
            }	
		}

	}
?>