<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Generalkhmerlanguagemodel extends CI_Model{
		
		function __construct()
		{
			parent::__construct();
		}

		//get class data
		public function getClassData($program_id, $school_level_id, $grade_level_id, $adcademic_year_id, $class_id){
			$where = "";
			if($class_id != ""){
				$where.=" AND c.classid='".$class_id."'";
			}
			$sql_class = $this->db->query("SELECT c.classid, c.class_name 
											FROM sch_class AS c
											WHERE 1=1 {$where}
											AND c.schlevelid='".$school_level_id."'
											AND c.grade_levelid='".$grade_level_id."'");
			// $class_name = "";
			// if($sql_class->num_rows() > 0){
			// 	//$row = $sql_class->row_array();
			// 	//$class_name = $row['class_name'];
				
			// }
			return $sql_class;
		}

		function getStudent($where){
    		// $sql = $this->db->query("SELECT DISTINCT
						// 				sl.student_id,
						// 				s.last_name,
						// 				s.first_name,
						// 			CONCAT(s.last_name,'  ',s.first_name) AS studentname,
						// 				s.gender,
						// 				sl.class_id,
						// 				sc.class_name,
						// 				y.sch_year,
						// 				sl.program_id,
						// 				sl.school_level_id,
						// 				sl.adcademic_year_id,
						// 				sl.grade_level_id
						// 			FROM
						// 				sch_subject_score_monthly_kgp_multi AS sl
						// 			INNER JOIN sch_student AS s ON sl.student_id = s.studentid
						// 			INNER JOIN sch_class AS sc ON sl.class_id = sc.classid
						// 			INNER JOIN sch_school_year AS y ON sl.adcademic_year_id = y.yearid
						// 			WHERE 1 = 1". $where ." ORDER BY s.last_name ASC ");
			if($this->session->userdata('match_con_posid')!='stu'){
				$sql = $this->db->query("SELECT
											v_student_enroment.student_num,
											v_student_enroment.first_name,
											v_student_enroment.last_name,
											v_student_enroment.first_name_kh,
											v_student_enroment.last_name_kh,
											v_student_enroment.gender,
											v_student_enroment.class_name,
											v_student_enroment.studentid
											FROM
											v_student_enroment
											WHERE 1=1 {$where}");
			}else{
				$sql = $this->db->query("SELECT
											v_student_enroment.student_num,
											v_student_enroment.first_name,
											v_student_enroment.last_name,
											v_student_enroment.first_name_kh,
											v_student_enroment.last_name_kh,
											v_student_enroment.gender,
											v_student_enroment.class_name,
											v_student_enroment.studentid
											FROM
											v_student_enroment
											WHERE 1=1 AND v_student_enroment.studentid='".($this->session->userdata('emp_id'))."' {$where}");
			}
    		return $sql->result();
    	}


    	function getStudentfinal($where){
    		$sql = $this->db->query("SELECT DISTINCT
										s.first_name_kh,
										s.last_name_kh,
										CONCAT(s.last_name_kh,'  ',s.first_name_kh) AS studentname,
										f.sub_final_score_id,
										c.class_name,
										y.sch_year,
										f.present,
										f.absent,
										f.average_score,
										f.total_avg_score_s2,
										f.total_avg_score_s1,
										f.class_id,
										f.adcademic_year_id,
										f.student_id,
										s.student_num,
										f.ranking_bysubj,
										f.averag_coefficient
									FROM
										sch_subject_score_final_kgp_multi AS f
									LEFT JOIN sch_class AS c ON f.class_id = c.classid
									LEFT JOIN sch_student AS s ON f.student_id = s.studentid
									LEFT JOIN sch_school_year AS y ON f.adcademic_year_id = y.yearid
									WHERE 1 = 1 ". $where ." ORDER BY s.student_num ASC ")->row();
    		return $sql;
    	}

		// function getStudentInformation($where)	{  
  //   		$sql = $this->db->query("SELECT DISTINCT
		// 								sc.class_name,
		// 								s.last_name,
		// 								s.first_name,
		// 								s.first_name_kh,
		// 								s.last_name_kh,
		// 								s.gender,
		// 								sp.program,
		// 								sy.sch_year,
		// 								sl.sub_monthly_score_id,
		// 								sl.present,
		// 								sl.absent,
		// 								sl.student_id,
		// 								sl.average_score,
		// 								sl.exam_monthly,
		// 								sl.averag_coefficient,
		// 								sl.class_id,
		// 								sl.adcademic_year_id,
		// 								sl.program_id,
		// 								sl.total_score,
		// 								sl.ranking_bysubj,
		// 								sl.school_level_id
		// 							FROM
		// 								sch_subject_score_monthly_kgp_multi AS sl
		// 							INNER JOIN sch_class AS sc ON sl.class_id = sc.classid
		// 							INNER JOIN sch_school_program AS sp ON sl.program_id = sp.programid
		// 							INNER JOIN sch_school_year AS sy ON sl.adcademic_year_id = sy.yearid
		// 							INNER JOIN sch_student AS s ON sl.student_id = s.studentid
		// 							WHERE
		// 								1 = 1 ". $where ."
		// 							ORDER BY
		// 								sl.ranking_bysubj ASC ");
								
  //   		return $sql->result();
    		
  //   	}
		function getStudentInformationSemester($where)	{  
    		$sql = $this->db->query("SELECT
									vse.student_num,
									vse.first_name,
									vse.last_name,
									vse.first_name_kh,
									vse.last_name_kh,
									vse.gender,
									vse.phone1,
									vse.class_name,
									vse.studentid,
									vse.schoolid,
									vse.`year`,
									vse.classid,
									vse.schlevelid,
									vse.rangelevelid,
									vse.feetypeid,
									vse.programid,
									vse.sch_level,
									vse.rangelevelname,
									vse.program,
									vse.sch_year,
									vse.grade_levelid,
									vse.is_pt,
									vse.from_date,
									vse.to_date
									FROM
									v_student_enroment AS vse
									WHERE 1=1 {$where}
									ORDER BY studentid ASC");
			return $sql->result();
			
    	}
		//calculate average score of a student in a class
		// Monthly
		public function calculateAverageScoreMonthly($class_id, $grade_level_id,$adcademic_year_id, $school_level_id, $program_id, $month_show)
		{
			// Monthly
			if($this->session->userdata('match_con_posid')!='stu'){
				$sql = "SELECT
							ms.total_score,
							ms.average_score,
							ms.ranking_bysubj,
							ms.averag_coefficient,
							ms.student_id,
							ms.teacher_id,
							ms.class_id,
							ms.grade_level_id,
							ms.adcademic_year_id,
							ms.school_level_id,
							ms.program_id,
							ms.school_id,
							ms.exam_monthly
						FROM
							sch_subject_score_monthly_kgp_multi AS ms
						WHERE 1=1
					    AND ms.class_id = ". $class_id ." 
						AND ms.grade_level_id = ". $grade_level_id ." 
						AND ms.adcademic_year_id = ". $adcademic_year_id ." 
						AND ms.school_level_id = ". $school_level_id ." 
						AND ms.program_id = ". $program_id ." 
						AND ms.exam_monthly = ".$month_show."
						ORDER BY
							ms.ranking_bysubj ASC ";
			}else{
				$sql = "SELECT
							ms.total_score,
							ms.average_score,
							ms.ranking_bysubj,
							ms.averag_coefficient,
							ms.student_id,
							ms.teacher_id,
							ms.class_id,
							ms.grade_level_id,
							ms.adcademic_year_id,
							ms.school_level_id,
							ms.program_id,
							ms.school_id,
							ms.exam_monthly
						FROM
							sch_subject_score_monthly_kgp_multi AS ms
						WHERE 1=1
					    AND ms.class_id = ". $class_id ." 
						AND ms.grade_level_id = ". $grade_level_id ." 
						AND ms.adcademic_year_id = ". $adcademic_year_id ." 
						AND ms.school_level_id = ". $school_level_id ." 
						AND ms.program_id = ". $program_id ." 
						AND ms.exam_monthly = ".$month_show."
						AND ms.student_id='".($this->session->userdata('emp_id'))."'
						ORDER BY
							ms.ranking_bysubj ASC ";
			}			
			$query = $this->db->query($sql);
            $arr_average = array();
            if($query->num_rows() > 0){
            	foreach($query->result() as $row_avg){
            		$arr_average[$row_avg->student_id] = $row_avg->average_score;
            	}
            }
		    return $arr_average;
		}
		// Semester
		public function calculateAverageScoreSemester($student_id, $class_id, $grade_level_id,$adcademic_year_id, $school_level_id, $program_id, $select_result_type)
		{
			if($this->session->userdata('match_con_posid')!='stu'){
				$sql = "SELECT
							ssmd.sub_sesmester_score_id,
							ssmd.student_id,
							ssmd.school_id,
							ssmd.program_id,
							ssmd.school_level_id,
							ssmd.adcademic_year_id,
							ssmd.grade_level_id,
							ssmd.class_id,
							ssmd.exam_semester,
							ssmd.get_monthly,
							ssmd.total_score,
							ssmd.average_score,
							ssmd.averag_coefficient,
							ssmd.present,
							ssmd.absent,
							ssmd.comments,
							ssmd.ranking_bysubj
							FROM
							sch_subject_score_semester_kgp_multi AS ssmd
							WHERE 1=1
							AND ssmd.student_id = ". $student_id ." 
						    AND ssmd.class_id = ". $class_id ." 
							AND ssmd.grade_level_id = ". $grade_level_id ." 
							AND ssmd.adcademic_year_id = ". $adcademic_year_id ." 
							AND ssmd.school_level_id = ". $school_level_id ." 
							AND ssmd.program_id = ". $program_id ." 
							AND ssmd.exam_semester = ".$select_result_type."
							ORDER BY
								ssmd.ranking_bysubj ASC ";
			}else{
				$sql = "SELECT
							ssmd.sub_sesmester_score_id,
							ssmd.student_id,
							ssmd.school_id,
							ssmd.program_id,
							ssmd.school_level_id,
							ssmd.adcademic_year_id,
							ssmd.grade_level_id,
							ssmd.class_id,
							ssmd.exam_semester,
							ssmd.get_monthly,
							ssmd.total_score,
							ssmd.average_score,
							ssmd.averag_coefficient,
							ssmd.present,
							ssmd.absent,
							ssmd.comments,
							ssmd.ranking_bysubj
							FROM
							sch_subject_score_semester_kgp_multi AS ssmd
							WHERE 1=1
							AND ssmd.student_id = ". $student_id ." 
						    AND ssmd.class_id = ". $class_id ." 
							AND ssmd.grade_level_id = ". $grade_level_id ." 
							AND ssmd.adcademic_year_id = ". $adcademic_year_id ." 
							AND ssmd.school_level_id = ". $school_level_id ." 
							AND ssmd.program_id = ". $program_id ." 
							AND ssmd.exam_semester = ".$select_result_type."
							ORDER BY
								ssmd.ranking_bysubj ASC ";
			}
			//print_r($sql);		
			$query = $this->db->query($sql);
            $num_row = $query->num_rows();
            $row = 0;
            if($num_row > 0){
                return $row = $query->row();
            }else{
                return $row;
            }
		}
	}
?>