<?php
	class Mod_iep_entryed_order_report extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function getnumberpage()
	    {
	        $perpage = $this->db->select('perpage')->get_where('z_record_perpage', array('id' => 3))->row()->perpage;
	        return $perpage;
	    }

	    function getsch_level($programid="")
		{
	        if($programid !=""){
	            $this->db->where("programid",$programid);
	        }
	        return $this->db->get("sch_school_level")->result();
		}

		function getschoolyear($sch_program="",$schoolid="",$schlevelid="")
		{
		    if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($sch_program!=""){
                $this->db->where('programid',$sch_program);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
			return $this->db->get('sch_school_year')->result();
		}	

		function getgradelevel($schoolid="",$programid="",$schlevelid="")
		{
            if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
			return $this->db->get('sch_grade_level')->result();
		}

		function get_class($schoolid="",$grad_level="",$schlevelid="")
		{
            if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($grad_level!=""){
                $this->db->where('grade_levelid',$grad_level);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
             	   $this->db->order_by("class_name","asc");
			return $this->db->get('sch_class')->result();
		}					
	}
?>