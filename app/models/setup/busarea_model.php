<?php
class Busarea_model extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function vaidate($id = 0, $busid = '', $areaid = ''){
        $where = '';
        if($id - 0 > 0){
            $where .= "AND ba.busareaid <> '{$id}' ";
        }
        $c = $this->db->query("SELECT
									COUNT(*) AS c
								FROM
									sch_setup_busarea AS ba
								WHERE
									ba.busid = '{$busid}' AND ba.areaid = '{$areaid}' {$where} ")->row()->c - 0;
        return $c;
    }

	function save(){
		$busareaid = $this->input->post('busareaid') - 0;
		$busno = trim($this->input->post('busno'));
		$area = trim($this->input->post('area'));

		$i = 0;
		if($this->vaidate($busareaid, $busno, $area) > 0){
			return $i;
		}else{
			$data = array(
					'busid' => $busno,
					'areaid' => $area
					 
				);
			
			if($busareaid > 0){
				$i = $this->db->update('sch_setup_busarea', $data, array('busareaid' => $busareaid));							
			}else{
				$i = $this->db->insert('sch_setup_busarea', $data);			
			}
		}

		return $i;
	}

	function grid(){
		$busarea = trim($this->input->post('search'));

		$where = '';
		if($busarea != ''){
			$where .= "AND ( b.busno LIKE '%{$busarea}%' )";
		}

		$totalRecord = $this->db->query("SELECT
											COUNT(b.busid) AS c
										FROM
											sch_setup_bus AS b
										INNER JOIN sch_setup_busarea AS ba ON b.busid = ba.busid
										INNER JOIN sch_setup_area AS a ON a.areaid = ba.areaid
										WHERE
											1 = 1 {$where} ")->row()->c;

		$result = $this->db->query("SELECT
											b.busid,
											b.busno,
											a.areaid,
											a.area,
											ba.busareaid
										FROM
											sch_setup_bus AS b
										INNER JOIN sch_setup_busarea AS ba ON b.busid = ba.busid
										INNER JOIN sch_setup_area AS a ON a.areaid = ba.areaid
										WHERE
											1 = 1 {$where}
										ORDER BY
											a.area ASC ")->result();
		
        $arr = array('result' => $result, 'totalRecord' => $totalRecord);
        return json_encode($arr);
	}

	function edit($busareaid = ''){
		$row = $this->db->query("SELECT
										ba.busareaid,
										ba.busid,										
										ba.areaid
									FROM
										sch_setup_busarea AS ba
									WHERE
										ba.busareaid = '{$busareaid}' ")->row();

        return json_encode($row);
	}

	function delete($busareaid = ''){
		$this->db->delete('sch_setup_busarea', array('busareaid' => $busareaid));
		$data['msg'] = 'deleted';
        return json_encode($data);
	}
	
}// end main