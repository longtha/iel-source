<?php
class Area_model extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function vaidate($id = 0, $f = ''){
        $where = '';
        if($id - 0 > 0){
            $where .= "AND a.areaid <> '{$id}' ";
        }
        $c = $this->db->query("SELECT
									COUNT(*) AS c
								FROM
									sch_setup_area AS a
								WHERE
									a.area = '{$f}' {$where} ")->row()->c - 0;
        return $c;
    }

	function save(){
		$areaid = $this->input->post('areaid') - 0;
		$area = trim($this->input->post('area'));

		$i = 0;
		if($this->vaidate($areaid, $area) > 0){
			return $i;
		}else{
			$data = array(
						'area' => $area 
					);

			if($areaid > 0){
				$i = $this->db->update('sch_setup_area', $data, array('areaid' => $areaid));
			}else{			
				$i = $this->db->insert('sch_setup_area', $data);		
			}
		}

		return $i;		
	}

	function grid(){
		$area = trim($this->input->post('search'));

		$where = '';
		if($area != ''){
			$where .= "AND ( a.area LIKE '%{$area}%' )";
		}

		$totalRecord = $this->db->query("SELECT
												COUNT(a.areaid) AS c
											FROM
												sch_setup_area AS a
											WHERE
												1 = 1 {$where}")->row()->c;

		$result = $this->db->query("SELECT DISTINCT
											a.areaid,
											a.area,
											ba.areaid AS ba_areaid
										FROM
											sch_setup_area AS a
										LEFT JOIN sch_setup_busarea AS ba ON a.areaid = ba.areaid
										WHERE
											1 = 1 {$where}
										ORDER BY
											a.area ASC ")->result();
		
        $arr = array('result' => $result, 'totalRecord' => $totalRecord);
        return json_encode($arr);
	}

	function edit($areaid = ''){
		$row = $this->db->query("SELECT
										a.areaid,
										a.area
									FROM
										sch_setup_area AS a
									WHERE
										a.areaid = '{$areaid}' ")->row();

        return json_encode($row);
	}

	function delete($areaid = ''){
		$this->db->delete('sch_setup_area', array('areaid' => $areaid));
		$data['msg'] = 'deleted';
        return json_encode($data);
	}
	
	function FMareas($areaid="")
	{
		if($areaid!=""){
        $this->db->where("areaid",$areaid);
        }
		$query=$this->db->get('sch_setup_area');
		return $query->result();
	}
	
}