<?php
class Assign_bus_area_model extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function vaidate($id = 0, $busid, $areaid){
        $where = '';
        if($id - 0 > 0){
            $where .= "AND ba.busareaid <> '{$id}' ";
        }
        $c = $this->db->query("SELECT
									COUNT(*) AS c
								FROM
									sch_setup_busarea AS ba
								WHERE
									ba.busid = '{$busid}' AND ba.areaid = '{$areaid}' {$where} ")->row()->c - 0;
        return $c;
    }

	function save(){
		$busareaid = $this->input->post('busareaid') - 0;
		$busid = trim($this->input->post('busid'));		
		$areaid = trim($this->input->post('areaid'));		

		$i = 0;
		if($this->vaidate($busareaid, $busid, $areaid) > 0){
			return $i = 2;
		}else{
			$data = array(
						'busareaid' => $busareaid,
						'busid' => $busid,
						'areaid' => $areaid												 
					);

			if($busareaid > 0){
				$i = $this->db->update('sch_setup_busarea', $data, array('busareaid' => $busareaid));					
			}else{
				$i = $this->db->insert('sch_setup_busarea', $data);
			}
		}

		return $i;
	}

	function edit($busareaid = ''){
		$row = $this->db->query("SELECT
										ba.busareaid,
										ba.busid,
										ba.areaid
									FROM
										sch_setup_busarea AS ba
									WHERE ba.busareaid = '{$busareaid}' ")->row();

        return json_encode($row);
	}

	function delete($busareaid = ''){
		$i = 0;
		$i = $this->db->delete('sch_setup_busarea', array('busareaid' => $busareaid));
        return json_encode($i);
	}
	
	function FMareas($areaid=""){
		if($areaid!=""){
        $this->db->where("areaid",$areaid);
        }
		$query=$this->db->get('sch_setup_area');
		return $query->result();
	}

	function get_bus(){
		$qr_bus = $this->db->query("SELECT
											b.busid,
											b.busno
										FROM
											sch_setup_bus AS b
										ORDER BY
		
											b.busno ASC ")->result();
		$arr = array('bus' => $qr_bus);
		return json_encode($arr);
	}

	function get_area(){
		$qr_area = $this->db->query("SELECT
											a.areaid,
											a.area
										FROM
											sch_setup_area AS a
										ORDER BY
											a.area ASC ")->result();

		$arr = array('area' => $qr_area);
		return json_encode($arr);
	}

	/*function get_area($term = ''){
		$qr_area = $this->db->query("SELECT
											a.areaid,
											a.area
										FROM
											sch_setup_area AS a
										WHERE
											a.area LIKE '%{$term}%'
										ORDER BY
											a.area ASC
										LIMIT 0, 10 ");

		$arr = [];
		if($qr_area->num_rows() > 0){
			foreach ($qr_area->result() as $row) {
				$arr[] = ['id' => $row->areaid, 'value' => $row->area];
			}
		}

		return json_encode($arr);
	}*/

	function grid(){
		$busno_area = trim($this->input->post('search'));

		$where = '';
		if($busno_area != ''){
			$where .= "AND ( b.busno LIKE '%{$busno_area}%' ";
			$where .= "or a.area LIKE '%{$busno_area}%' ) ";
			
		}

		$totalRecord = $this->db->query("SELECT
												COUNT(ba.busareaid) AS c
											FROM
												sch_setup_bus AS b
											RIGHT JOIN sch_setup_busarea AS ba ON b.busid = ba.busid
											LEFT JOIN sch_setup_area AS a ON ba.areaid = a.areaid
											WHERE 1=1 {$where} ")->row()->c;

		$result = $this->db->query("SELECT
											ba.busareaid,
											b.busno,
											a.area
										FROM
											sch_setup_bus AS b
										RIGHT JOIN sch_setup_busarea AS ba ON b.busid = ba.busid
										LEFT JOIN sch_setup_area AS a ON ba.areaid = a.areaid
										WHERE
											1 = 1 {$where}
										ORDER BY
											b.busno ASC ")->result();
		
        $arr = array('result' => $result, 'totalRecord' => $totalRecord);//
        return json_encode($arr);
	}
	
}