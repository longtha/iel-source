<?php
class Location_model extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function validateLoc($type="",$name="",$loc_id="", $province_id=""){
        $where = '';
        if($type !=""){
            $where .= " AND l.type = '{$type}' ";
        }
        if($name !=""){
			$where .= " AND l.name = '{$name}' ";
        }
        if($loc_id !=""){
			$where .= " AND l.loc_id <> '{$loc_id}' ";
        }
        if($province_id !=""){
			$where .= " AND l.province_id = '{$province_id}' ";
        }
        $val = $this->db->query("SELECT
									COUNT(*) AS c
								FROM
									sch_setup_location AS l
								WHERE 1=1 
									{$where} ")->row()->c - 0;
        return $val;
    }

	function save(){
		$location_id = $this->input->post('location_id') - 0;
		$location_type = $this->input->post('location_type') - 0;
		$type_name = trim($this->input->post('type_name'));
		$province_id = $this->input->post('province_id');
		$district_id = $this->input->post('district_id');
		$commune_id = $this->input->post('commune_id');				
		$i = 0;			
		$data_update = array(
					'type' => $location_type,
					'name' => $type_name,
					'province_id' => $province_id,
					'district_id' => $district_id,
					'commune_id' => $commune_id			 
				);
		if($location_id > 0){

			$i = $this->db->update('sch_setup_location', $data_update, array('loc_id' => $location_id));
			$i = 1;					
		}else{
			if($location_type=='1'){
				$data = array(
					'type' => $location_type,
					'name' => $type_name,
					'province_id' => $province_id);	
				$i = $this->db->insert('sch_setup_location', $data);
				$loc_id_auto = $this->db->insert_id();
				
				$data1 = array(					
					'province_id' => $loc_id_auto	 
				);
				$this->db->update('sch_setup_location', $data1, array('loc_id' => $loc_id_auto));
			}
			if($location_type=='2'){
				$data1 = array(
					'type' => $location_type,
					'name' => $type_name,
					'province_id' => $province_id,
					'district_id' => $district_id		 
				);
				$i = $this->db->insert('sch_setup_location', $data1);
				$loc_id_auto = $this->db->insert_id();

				$data2 = array(					
					'district_id' => $loc_id_auto	 
				);
				$this->db->update('sch_setup_location', $data2, array('loc_id' => $loc_id_auto));
			}
			if($location_type=='3'){
				$data2 = array(
					'type' => $location_type,
					'name' => $type_name,
					'province_id' => $province_id,
					'district_id' => $district_id,
					'commune_id' => $commune_id			 
				);
				$i = $this->db->insert('sch_setup_location', $data2);
				$loc_id_auto = $this->db->insert_id();

				$data3 = array(					
					'commune_id' => $loc_id_auto	 
				);
				$this->db->update('sch_setup_location', $data3, array('loc_id' => $loc_id_auto));
			}
			if($location_type=='4'){
				$data3 = array(
					'type' => $location_type,
					'name' => $type_name,
					'province_id' => $province_id,
					'district_id' => $district_id,
					'commune_id' => $commune_id			 
				);
				$i = $this->db->insert('sch_setup_location', $data3);
				$loc_id_auto = $this->db->insert_id();

				$data4 = array(					
					'village_id' => $loc_id_auto	 
				);
				$this->db->update('sch_setup_location', $data4, array('loc_id' => $loc_id_auto));
			}
		}
		return $i;
	}

	function getprovince($province_id = "")
	{
		if($province_id != ""){
        	$this->db->where("province_id", $province_id);
        }

        $this->db->where("type", "1");
		$query = $this->db->get('sch_setup_location');		
		return $query->result();
	}

	function getdistrict($province_id = "", $district_id ="")
	{
		if($province_id != ""){
        	$this->db->where("province_id", $province_id);
        }
        if($district_id != ""){
        	$this->db->where("district_id", $district_id);
        }
        $this->db->where("type", "2");
		$district = $this->db->get('sch_setup_location')->result();		
        return $district;
	}
	function getcommune($province_id = "", $district_id = "", $commune_id="")
	{
		if($province_id != ""){
        	$this->db->where("province_id", $province_id);
        }
        if($district_id != ""){
        	$this->db->where("district_id", $district_id);
        }
		if($commune_id != ""){
        	$this->db->where("commune_id", $commune_id);
        }
        $this->db->where("type", "3");
		$commune = $this->db->get('sch_setup_location')->result();		
		return $commune;
	}
	
	function getvillage($province_id = "", $district_id = "", $commune_id="", $village_id="")
	{
		if($province_id != ""){
        	$this->db->where("province_id", $province_id);
        }
        if($district_id != ""){
        	$this->db->where("district_id", $district_id);
        }
		if($commune_id != ""){
        	$this->db->where("commune_id", $commune_id);
        }        
		if($village_id != ""){
        	$this->db->where("village_id", $village_id);
        }
        $this->db->where("type", "4");
		$village = $this->db->get('sch_setup_location')->result();		
		return $village;
	}
	function getlocations($province_id="", $district_id="", $commune_id="", $village_id=""){
		$where ="";
		if($province_id != ""){
        	$where .=" AND loc.province_id = '".$province_id."'";	
        }
        if($district_id != ""){
        	$where .=" AND loc.district_id = '".$district_id."'";
        }
        if($commune_id != ""){
        	$where .=" AND loc.commune_id = '".$commune_id."'";
        }
        if($village_id != ""){
        	$where .=" AND loc.village_id = '".$village_id."'";
        }

		$result = $this->db->query("SELECT
									loc.loc_id,
									loc.type,
									loc.loc_name,
									loc.province_id,
									loc.district_id,
									loc.commune_id,
									loc.village_id,
									loc.province_name,
									loc.district_name,
									loc.commune_name,
									loc.village_name,
									loc.note
								FROM
									v_location_info AS loc 
								WHERE 1=1 {$where}")->result();
								
        return $result;
	}

	function getlocation($loc_id="", $loc_type="", $province_id="", $district_id="", $commune_id="", $village_id=""){
		$where ="";
		if($province_id != ""){
        	$where .=" AND loc.province_id = '".$province_id."'";	
        }
        if($district_id != ""){
        	$where .=" AND loc.district_id = '".$district_id."'";
        }
        if($commune_id != ""){
        	$where .=" AND loc.commune_id = '".$commune_id."'";
        }
        if($village_id != ""){
        	$where .=" AND loc.village_id = '".$village_id."'";
        }
 		if($loc_type != ""){
        	$where .=" AND loc.type = '".$loc_type."'";
        }			
 		if($loc_id != ""){
        	$where .=" AND loc.loc_id = '".$loc_id."'";
        }
        $result = $this->db->query("SELECT
									loc.loc_id,
									loc.type,
									loc.loc_name,
									loc.province_id,
									loc.district_id,
									loc.commune_id,
									loc.village_id,
									loc.province_name,
									loc.district_name,
									loc.commune_name,
									loc.village_name,
									loc.note
								FROM
									v_location_info AS loc 
								WHERE 1=1 {$where}")->row();
								
        return $result;
	}

	function delete($loc_id="", $loc_type=""){
		$this->db->where('loc_id', $loc_id);
		$this->db->where('type', $loc_type);
		$this->db->delete('sch_setup_location');
		$data['msg'] = 'deleted';
        return json_encode($data);
	}
}