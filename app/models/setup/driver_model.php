<?php
class Driver_model extends CI_Model{

	function __construct(){
		parent::__construct();

		$this->load->model('setup/area_model', 'a');
	}

	function save(){
		$driverid = $this->input->post('driverid') - 0;
		$busid = $this->input->post('busid');		
		$driver_name = trim($this->input->post('driver_name'));
		$tel = trim($this->input->post('tel'));
		$address = trim($this->input->post('address'));
	
		$data = array(
					'driver_name' => $driver_name,
					'tel' => $tel,
					'address' => $address,
					'busid' => $busid										 
				);

		if($driverid > 0){
			$i = $this->db->update('sch_setup_driver', $data, array('driverid' => $driverid));
			return $i;
						
		}else{		
			$i = $this->db->insert('sch_setup_driver', $data);
			return $i;			
		}
	}

	function grid(){
		$driver = trim($this->input->post('search'));

		$where = '';
		if($driver != ''){
			$where .= "AND ( d.driver_name LIKE '%{$driver}%' ";
			$where .= "or d.tel LIKE '%{$driver}%' ) ";			
		}

		$totalRecord = $this->db->query("SELECT
												COUNT(d.driverid) AS c
											FROM
												sch_setup_driver AS d
											LEFT JOIN sch_setup_bus AS b ON b.busid = d.busid
											WHERE
												1 = 1 {$where} ")->row()->c;

		$result = $this->db->query("SELECT
											d.driverid,
											d.driver_name,
											d.tel,
											d.address,
											b.busno
										FROM
											sch_setup_driver AS d
										LEFT JOIN sch_setup_bus AS b ON b.busid = d.busid
										WHERE
											1 = 1 {$where}
										ORDER BY
											d.driver_name ASC ")->result();
		
        $arr = array('result' => $result, 'totalRecord' => $totalRecord);
        return json_encode($arr);
	}

	function edit($driverid = ''){
		$row = $this->db->query("SELECT
										d.driverid,
										d.driver_name,
										d.tel,
										d.address,
										d.busid										
									FROM
										sch_setup_driver AS d
									WHERE
										d.driverid = '{$driverid}' ")->row();

        return json_encode($row);
	}

	function delete($driverid = ''){
		$this->db->delete('sch_setup_driver', array('driverid' => $driverid));
		$data['msg'] = 'deleted';
        return json_encode($data);
	}
	
}