<?php
class Old_busfee_model extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function vaidate($id = 0, $buswaytype = '', $areaid = ''){
        $where = '';
        if($id - 0 > 0){
            $where .= "AND b.buspriceid <> '{$id}' ";
        }
        $c = $this->db->query("SELECT
									COUNT(*) AS c
								FROM
									sch_setup_busfee AS b
								WHERE
									b.buswaytype = '{$buswaytype}' AND b.areaid = '{$areaid}' {$where} ")->row()->c - 0;
        return $c;
    }

	function save(){
		$buspriceid = $this->input->post('buspriceid') - 0;
		$buswaytype = trim($this->input->post('buswaytype'));
		$area = trim($this->input->post('area'));
		$price = trim($this->input->post('price'));

        $created_date = date("Y-m-d H:i:s");
        $created_by = $this->session->userdata('user_name');
        $modified_date = date("Y-m-d H:i:s");
        $modified_by = $this->session->userdata('user_name');

		$i = 0;
		if($this->vaidate($buspriceid, $buswaytype, $area) > 0){
			return $i;
		}else{
			$data = array('buswaytype' => $buswaytype,
							'areaid' => $area,
							'price' => $price
							// ,
							// 'created_date' => $created_date,
							// 'created_by' => $created_by,
							// 'modified_date' => $modified_date,
							// 'modified_by' => $modified_by				
						);	

			if($buspriceid > 0){
				// unset($data['created_date']);
				// unset($data['created_by']);
				$i = $this->db->update('sch_setup_busfee', $data, array('buspriceid' => $buspriceid));			
			}else{
				// unset($data['modified_date']);
				// unset($data['modified_by']);								
				$i = $this->db->insert('sch_setup_busfee', $data);					
			}
		}

		return $i;

	}

	function grid(){
		$buswaytype = trim($this->input->post('search'));
		$area = trim($this->input->post('area'));

		$where = '';
		if($buswaytype != ''){
			$where .= "AND ( s.description LIKE '%{$buswaytype}%' )";
		}

		if($area != ''){
			$where .= "AND ( a.area LIKE '%{$area}%' )";
		}
		

		$totalRecord = $this->db->query("SELECT
											COUNT(bf.buspriceid) AS c
										FROM
											sch_setup_busfee AS bf
										LEFT JOIN sch_family_social_infor AS s ON bf.buswaytype = s.famnoteid
										LEFT JOIN sch_setup_area AS a ON bf.areaid = a.areaid
										WHERE
											1 = 1 {$where} ")->row()->c;

		$result = $this->db->query("SELECT
											bf.buspriceid,
											bf.buswaytype,
											bf.areaid,
											bf.price,
											a.area,
											s.description
										FROM
											sch_setup_busfee AS bf
										LEFT JOIN sch_family_social_infor AS s ON bf.buswaytype = s.famnoteid
										LEFT JOIN sch_setup_area AS a ON bf.areaid = a.areaid
										WHERE
											1 = 1 {$where} ")->result();
		
        $arr = array('result' => $result, 'totalRecord' => $totalRecord);
        return json_encode($arr);
	}

	function edit($buspriceid){
		$row = $this->db->query("SELECT
										bf.buspriceid,
										bf.buswaytype,
										bf.areaid,
										bf.price
									FROM
										sch_setup_busfee AS bf
									WHERE
										bf.buspriceid = '{$buspriceid}' ")->row();		
        return json_encode($row);
	}

	function getbusway()
    { 
    	$this->db->where('familynote_type', 'busway');
    	return $this->db->get('sch_family_social_infor');
    }

	function delete($buspriceid = ''){
		$this->db->delete('sch_setup_busfee', array('buspriceid' => $buspriceid));
		$data['msg'] = 'deleted';
        return json_encode($data);
	}
	
}