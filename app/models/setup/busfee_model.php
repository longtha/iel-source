<?php
class Busfee_model extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function vaidate($id = 0, $buswaytype = '', $areaid = ''){
        $where = '';
        if($id - 0 > 0){
            $where .= "AND b.buspriceid <> '{$id}' ";
        }
        $c = $this->db->query("SELECT
									COUNT(*) AS c
								FROM
									sch_setup_busfee AS b
								WHERE
									b.buswaytype = '{$buswaytype}' AND b.areaid = '{$areaid}' {$where} ")->row()->c - 0;
        return $c;
    }

	function save(){
		$otherfeeid = $this->input->post('otherfeeid') - 0;
		$bus_way_type = trim($this->input->post('bus_way_type'));
		$area = trim($this->input->post('area'));
		$prices = trim($this->input->post('prices'));		
		$otherfee = trim($this->input->post('otherfee'));
		$fromdate = $this->green->formatSQLDate(trim($this->input->post('fromdate')));
		$todate = $this->green->formatSQLDate(trim($this->input->post('todate')));

        $created_date = date("Y-m-d H:i:s");
        $created_by = $this->session->userdata('user_name');
        $modified_date = date("Y-m-d H:i:s");
        $modified_by = $this->session->userdata('user_name');

		$i = 0;
		// if($this->vaidate($buspriceid, $buswaytype, $area) > 0){
		// 	return $i;
		// }else{
			$data = array('otherfee' => $otherfee,							
							'created_date' => $created_date,
							'created_by' => $created_by,
							'modified_date' => $modified_date,
							'modified_by' => $modified_by,
							'prices' => $prices,
							'fromdate' => $fromdate,
							'todate' => $todate,
							'typefee' => 1,
							'bus_way_type' => $bus_way_type,
							'areacode' => $area
						);	

			if($otherfeeid > 0){
				unset($data['created_date']);
				unset($data['created_by']);		
				$i = $this->db->update('sch_setup_otherfee', $data, array('otherfeeid' => $otherfeeid ));
				
			}else{
				unset($data['modified_date']);
				unset($data['modified_by']);
				$i = $this->db->insert('sch_setup_otherfee', $data);						
			}
		// }

		return $i;

	}

	function grid(){
		$otherfee_search = trim($this->input->post('otherfee_search'));
		$area_search = trim($this->input->post('area_search'));

		$where = '';
		if($otherfee_search != ''){
			$where .= "AND of.otherfee LIKE '%{$otherfee_search}%' ";
		}

		if($area_search != ''){
			$where .= "AND a.area LIKE '%{$area_search}%' ";
		}
		
		$where .= "AND of.typefee = '1' ";

		$totalRecord = $this->db->query("SELECT
											COUNT(of.otherfeeid) AS c
										FROM
											sch_setup_otherfee AS of
										LEFT JOIN sch_setup_area AS a ON of.areacode = a.areaid
										WHERE
											1 = 1 {$where} ")->row()->c;

		$result = $this->db->query("SELECT
											of.otherfeeid,
											of.otherfee,
											of.created_by,
											of.created_date,
											of.modified_by,
											of.modified_date,
											of.prices,
											DATE_FORMAT(of.fromdate, '%d/%m/%Y') AS fromdate,
                                            DATE_FORMAT(of.todate, '%d/%m/%Y') AS todate,
											of.todate,
											of.typefee,
											of.bus_way_type,
											of.areacode,
											a.area
										FROM
											sch_setup_otherfee AS of
										LEFT JOIN sch_setup_area AS a ON of.areacode = a.areaid
										WHERE
											1 = 1 {$where}
										ORDER BY of.otherfee ASC ")->result();
		
        $arr = array('result' => $result, 'totalRecord' => $totalRecord);
        return json_encode($arr);
	}

	function edit($otherfeeid){
		$row = $this->db->query("SELECT
										of.otherfeeid,
										of.otherfee,
										of.created_by,
										of.created_date,
										of.modified_by,
										of.modified_date,
										of.prices,
										DATE_FORMAT(of.fromdate, '%d/%m/%Y') AS fromdate,
                                        DATE_FORMAT(of.todate, '%d/%m/%Y') AS todate,
										of.typefee,
										of.bus_way_type,
										of.areacode,
										a.areaid,
										a.area
										
									FROM
										sch_setup_otherfee AS of
									LEFT JOIN sch_setup_area AS a ON of.areacode = a.areaid
									WHERE
										of.otherfeeid = '{$otherfeeid}' ")->row();


        return json_encode($row);
	}

	function getbusway()
    { 
    	$this->db->where('familynote_type', 'busway');
    	return $this->db->get('sch_family_social_infor');

    }

	function delete($otherfeeid = ''){
		$i = 0;
		$i = $this->db->delete('sch_setup_otherfee', array('otherfeeid' => $otherfeeid));
        return json_encode($i);
	}
	
}