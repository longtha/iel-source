<?php
class modotherfee extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function vaidate($id = 0, $s = ''){
        $where = '';
        if($id - 0 > 0){
            $where .= "AND f.otherfeeid <> '{$id}' ";
        }
        $c = $this->db->query("SELECT
         COUNT(*) AS c
        FROM
         sch_setup_otherfee AS f
        WHERE 1=1
        AND f.otherfee = '{$s}' {$where} ")->row()->c - 0;
        return $c;
    }

 	function save(){
		$otherfeeid = $this->input->post('otherfeeid') - 0;
		$otherfee = trim($this->input->post('otherfee'));
		$prices = $this->input->post('prices');
		$c_date = date('Y-m-d h:m:s');
	   	$user   = $this->session->userdata('user_name');
		$i = 0;
		if($this->vaidate($otherfeeid, $otherfee) > 0){
		return $i;
		}else{
		  	$data = array(
		      'otherfee' => $otherfee,
		      'prices' => $prices
		     );
		$qr1 = $this->db->query("SELECT
									f.otherfee
								FROM
									sch_setup_otherfee AS f
								WHERE
									f.otherfeeid = '{$otherfeeid}' ");
		if($otherfeeid > 0){
			$data = array(
					'otherfee' => $otherfee,
					'prices' => $prices,
					'modified_date'=> $c_date,
	 		        'modified_by'  =>  $user );
		    $i = $this->db->update('sch_setup_otherfee', $data, array('otherfeeid' => $otherfeeid));     
		}else{
			$data = array(
					'otherfee' => $otherfee,
					'prices' => $prices,
	 				'created_date'=> $c_date,
	 		        'created_by'  =>  $user );
		    $i = $this->db->insert('sch_setup_otherfee', $data); 
			}

		}

	  return $i;
	}

	function gatedata(){
		$other = trim($this->input->post('search'));
		$where = '';
		if($other != ''){
			$where .= "AND ( f.otherfee LIKE '%{$other}%' )";
		}

		$totalRecord = $this->db->query("SELECT
										COUNT(f.otherfeeid) AS c
										FROM
											sch_setup_otherfee AS f
										WHERE 1=1 AND f.typefee = 0
										{$where} ")->row()->c;

		$result = $this->db->query("SELECT
										f.otherfeeid,
										f.otherfee,
										f.prices,
										f.created_by,
										f.created_date,
										f.modified_by,
										f.modified_date
									FROM
										sch_setup_otherfee AS f
									WHERE 1=1 
									AND typefee = 0
									{$where} ")->result();
		
        $arr = array('result' => $result, 'totalRecord' => $totalRecord);
        return json_encode($arr);
	}

	function edit($otherfeeid = ''){
		$row = $this->db->query("SELECT
										f.otherfeeid,
										f.otherfee,
										f.prices
									FROM
										sch_setup_otherfee AS f
									WHERE f.otherfeeid = '{$otherfeeid}' ")->row();

        return json_encode($row);
	}

	function delete($otherfeeid = ''){
		$this->db->delete('sch_setup_otherfee', array('otherfeeid' => $otherfeeid));
		$data['msg'] = 'deleted';
        return json_encode($data);
	}
	
	function FMotherfee($otherfeeid="")
	{
		if($otherfeeid!=""){
        $this->db->where("otherfeeid",$otherfeeid);
        }
		$query=$this->db->get('sch_setup_otherfee');
		return $query->result();
	}
}