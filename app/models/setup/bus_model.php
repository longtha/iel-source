<?php
class Bus_model extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function vaidate($id = 0, $f = ''){
        $where = '';
        if($id - 0 > 0){
            $where .= "AND b.busid <> '{$id}' ";
        }
        $c = $this->db->query("SELECT
									COUNT(*) AS c
								FROM
									sch_setup_bus AS b
								WHERE
									b.busno = '{$f}' {$where} ")->row()->c - 0;
        return $c;
    }

    function fBus($busid = "")
	{
		if($busid != ""){
        	$this->db->where("busid", $busid);
        }
		$query = $this->db->get('sch_setup_bus');
		return $query->result();
	}

	function save(){
		$busid = $this->input->post('busid') - 0;
		$busno = trim($this->input->post('busno'));		

		$i = 0;
		if($this->vaidate($busid, $busno) > 0){
			return $i;
		}else{
			$data = array(
						'busno' => $busno						 
					);

			if($busid > 0){
				$i = $this->db->update('sch_setup_bus', $data, array('busid' => $busid));					
			}else{
				$i = $this->db->insert('sch_setup_bus', $data);
			}
		}

		return $i;
	}

	function grid(){
		$busno = trim($this->input->post('search'));

		$where = '';
		if($busno != ''){
			$where .= "AND ( b.busno LIKE '%{$busno}%' )";
		}

		$totalRecord = $this->db->query("SELECT
												COUNT(b.busid) AS c
											FROM
												sch_setup_bus AS b
											WHERE 1=1 {$where} ")->row()->c;

		$result = $this->db->query("SELECT DISTINCT
										b.busid,
										b.busno,
										ba.busid AS ba_busid
									FROM
										sch_setup_bus AS b
									LEFT JOIN sch_setup_busarea AS ba ON b.busid = ba.busid	
									WHERE 1=1 {$where}									
									ORDER BY b.busno ASC ")->result();
		
        //$arr = array('result' => $result, 'totalRecord' => $totalRecord);

        $arr['result']= $result;
        $arr['totalRecord']= $totalRecord;
        return json_encode($arr);
	}

	function edit($busid = ''){
		$row = $this->db->query("SELECT
										b.busid,
										b.busno
									FROM
										sch_setup_bus AS b
									WHERE b.busid = '{$busid}' ")->row();

        return json_encode($row);
	}

	function delete($busid = ''){
		$this->db->delete('sch_setup_bus', array('busid' => $busid));
		$data['msg'] = 'deleted';
        return json_encode($data);
	}
	
}