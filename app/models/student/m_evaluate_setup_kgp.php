<?php
    class m_evaluate_setup_kgp extends CI_Model{
    	// val
    	function validate($evaluate_kh,$evaluate_id){
            $where='';
            if($evaluate_id!='')
                $where.=" AND evaluate_id<>'$evaluate_id'";
            return $this->db->query("SELECT COUNT(*) as count FROM sch_student_evaluate_setup_kgp where evaluate_name_kh='$evaluate_kh' {$where}")->row()->count;
        }
        // save -------------------------------------------
		function save($evaluate_id){ 
			date_default_timezone_set("Asia/Bangkok");
			$evaluate_eng = $this->input->post('evaluate_eng');
			$evaluate_kh = $this->input->post('evaluate_kh');
			$status = $this->input->post('status');
			//$CKcheckbox = $this->input->post('CKcheckbox');
	  		$user   = $this->session->userdata('user_name');
	  		$data   = array('evaluate_name_eng' => $evaluate_eng,
							'evaluate_name_kh'	 => $evaluate_kh,
							'is_active' => $status
				 			);
	  		 	
		    if($evaluate_id !=''){ 
		    	$this->db->where('evaluate_id',$evaluate_id);
		    	$this->db->update('sch_student_evaluate_setup_kgp',array_merge($data));		    	
		    }else{ 
		    	$this->db->insert('sch_student_evaluate_setup_kgp', array_merge($data));
		    }	  	
		   	return $evaluate_id;
		}

		// editdata ----------------------------------------
		function editdata(){
			$evaluate_id = $this->input->post('evaluate_id');
			$sql = $this->db->query("SELECT
										sch_student_evaluate_setup_kgp.evaluate_id,
										sch_student_evaluate_setup_kgp.evaluate_name_kh,
										sch_student_evaluate_setup_kgp.evaluate_name_eng,
										sch_student_evaluate_setup_kgp.is_active
									FROM
										sch_student_evaluate_setup_kgp WHERE evaluate_id = '".$evaluate_id."'")->row();
			return  $sql;
			
		}


    }