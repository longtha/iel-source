<?php if(!defined('BASEPATH')) exit('No direct script access allowed.!');
	class Modstusponsor  extends CI_Model
	{
		public function setSqlStuSponsor($yearid='',$schlevelid='',$stu_code='',$stu_fullname='',$stu_level='',$stu_classid='')
		{
			$where="";
			if($yearid!=''){
				$where.=" AND vsp.yearid ='".$yearid."'";
			}
			if($schlevelid!=''){
				$where.=" AND vsp.schlevelid='".$schlevelid."'";
			}
			if($stu_code!=""){
				$where.=" AND (vsp.student_num like '%".$stu_code."%')";
			}		
			if($stu_fullname!=""){
				$where.=" AND (vsp.fullname like '%".$stu_fullname."%')";
			}
			if($stu_level!=""){
				$where.=" AND (vsp.grade_levelid ='".$stu_level."')";
			}		
			if($stu_classid!=""){
				$where.=" AND (vsp.classid = '".$stu_classid."')";
			}
			$sqlText = "SELECT
							vsp.studentid,
							vsp.student_num,
							vsp.fullname,
							vsp.class_name,
							vsp.sch_year,
							vsp.classid,
							vsp.yearid,
							vsp.familyid,
							vsp.schlevelid,
							vsp.grade_levelid
						FROM
							v_student_profile vsp
						WHERE 1=1 {$where} ORDER BY vsp.grade_levelid ASC,vsp.classid ASC,vsp.fullname ASC
						";
			return $sqlText;
		}
		
		public function GetTotalRow($yearid='',$schlevelid='',$stu_code='',$stu_fullname='',$stu_level='',$stu_classid='')
		{
			return $this->green->getTotalRow($this->setSqlStuSponsor($yearid,$schlevelid,$stu_code,$stu_fullname,$stu_level,$stu_classid));
		}
		public function listbody($no,$student_num,$fullname,$level,$class,$year,$studentid='')
		{
			$tr="";
			$sp= '<div class="" style="border:0px solid #f00;width:17px;height: 15px;margin:0 auto;" data-toggle="collapse" data-target=".collapseDiv'.$no.'" title="View Sponsor">
		          <i style="cursor:pointer" class="glyphicon glyphicon-large fa-lg glyphicon-triangle-bottom open_sponsor"  attr="'.$studentid.'"></i>
		          </div>
		      	';
			$pre="";
			if(!$this->green->gAction("P"))
				 $pre.="<a attr='".$studentid."' class='StuSponsor' href='JavaScript:void(0);'><img src='".site_url('../assets/images/icons/a_preview.png')."'/></a>"; 
			$tr.= "<tr>
				 	 <td class='no' style='vertical-align:middle;text-align:center;'>".$no."</td>
					 <td class='studentid' style='vertical-align:middle'>".$student_num."</td>
					 <td class='fullname' style='vertical-align:middle'>".$fullname."</td>
					 <td class='level' style='vertical-align:middle'>".$level."</td>
					 <td class='class' style='vertical-align:middle'>".$class."</td>
					 <td class='year remove_tag' width='50' style='vertical-align:middle; text-align:center;'>".$sp."</td>
					 <td class='action remove_tag' align='center' style='vertical-align:middle'>".$pre."</td>
				</tr>";
			$tr.= "<tr class='collapse collapseAll collapseDiv".$no."'>
					<td colspan='4' class='no' style='vertical-align:middle'></td>
					<td colspan='2' class='no' style='vertical-align:middle'>
			      		<table id='preview' class='table table-bordered table-striped' style='paddingm:0px; margin:0px;' border='1'>
							<thead class='tab_head'>
								<tr align='center' >
									<th width='10'>No</th>
									<th>Sponsor Name</th>
								</tr>
							</thead>
							<tbody id='listsponsor'>
								".($this->subsponinstu($studentid))."
							</tbody>
						</table>
			      	</td>
			      	<td class='no' style='vertical-align:middle'></td>
				</tr>";	
			return $tr;
		}
		public function subsponinstu($studentid='')
		{
			$tr="";
			$j=1;
			foreach($this->setSqlSponsor($studentid) as $_row){
				$tr.="<tr>
					<td width='10'>".$j."</td>
					<td>".($_row->sponsor_fullname)."</td>
				</tr>";
				$j++;
			}
			
			return $tr;
		}
		public function getclass(){
			return $this->db->get('sch_class');
		}
		public function setSqlStu($studentid)
		{
			$sqlStr = "SELECT
							vsp.student_num,
							vsp.fullname,
							vsp.fullname_kh,
							vsp.familyid,
							vsp.class_name,
							vsp.dateofbirth,
							vsp.nationality,
							vsp.permanent_adr
						FROM
							v_student_profile AS vsp
						WHERE vsp.studentid = '".$studentid."'
					";
			return $this->green->getOneRow($sqlStr);			
		}
		public function setSqlSponsor($studentid){
			$sqlStr = "SELECT
							sfc.sponsorid,
							sfc.sponsor_code,
							sfc.last_name,
							sfc.first_name,
							concat(
								sfc.last_name,
								_utf8 ' ',
								sfc.first_name
							) AS `sponsor_fullname`,
							sfc.position,
							sfc.phone,
							sfc.mail,
							sfc.title
						FROM
							sch_family_sponsor sfc
						INNER JOIN sch_student_sponsor_detail sssd ON sssd.sponsorid = sfc.sponsorid
					WHERE
						sssd.studentid = '".$studentid."'
			";
			return $this->db->query($sqlStr)->result();
		}
		public function schinfo(){
			return $this->green->getValue("SELECT ssi.name FROM sch_school_infor ssi WHERE ssi.schoolid='1'");
		}
		public function listsponinfo($i,$sponsor_code,$fullname,$position,$phone)
		{
			$tr ="<tr>
				<td>".$i."</td>
				<td>".$sponsor_code."</td>
				<td>".$fullname."</td>
				<td>".$position."</td>
				<td>".$phone."</td>
			</tr>";
			return $tr;
		}
	}