<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_home_work extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function save($homeworkData) {
      $result = $this->db->insert('sch_student_home_work', $homeworkData) ? 1 : 0;

      return $result;
    }
  
    function getEditData($id) {
      $homeworkQuery = <<<QUERY
                            SELECT
                            	id,
                            	school_id,
                            	school_level_id,
                            	academic_year_id,
                            	class_id,
                            	grade_level_id,
                            	subject_id,
                            	title,
                            	description,
                            	deadline
                            FROM
                            	sch_student_home_work
                            WHERE
                            	id = $id
QUERY;

      $homeworkData = $this->db->query($homeworkQuery)->row();

      return $homeworkData;
    }
    
function filter($parameters) {
    
    
    $where='';
    $i=0;
    foreach($parameters as $key => $val)
    {
        if($i==0){
            if($key=="from_date"){
                
                list ($day, $month, $year) = explode('-', $val);
         
                $startdate = $year."-".$month."-".$day." 00:00:00";
                
                $where=$where.' WHERE UNIX_TIMESTAMP(stw.transaction_date)>=UNIX_TIMESTAMP("'.$startdate.'")';
                
            }else if($key=="to_date"){
                
                list ($day, $month, $year) = explode('-', $val);
                
                $enddate = $year."-".$month."-".$day." 00:00:00";
                
                $where=$where.' WHERE UNIX_TIMESTAMP(stw.transaction_date)<=UNIX_TIMESTAMP("'.$enddate.'")';
                
            }else{
                $where=$where.' WHERE stw.'.$key.'="'.$val.'"';
            }
        }else{
            if($key=="from_date"){
                
                list ($day, $month, $year) = explode('-', $val);
            
                $startdate = $year."-".$month."-".$day." 00:00:00";
                
                $where=$where.' AND UNIX_TIMESTAMP(stw.transaction_date)>=UNIX_TIMESTAMP("'.$startdate.'")';
          
                
            }else if($key=="to_date"){
                
                list ($day, $month, $year) = explode('-', $val);
                
                $enddate = $year."-".$month."-".$day." 00:00:00";
                
                $where=$where.' AND UNIX_TIMESTAMP(stw.transaction_date)<=UNIX_TIMESTAMP("'.$enddate.'")';
             
                
            }else{
                $where=$where.' AND stw.'.$key.'="'.$val.'"';
            }
        }
        $i=$i+1;
       
    }
    if($this->session->userdata('match_con_posid')!='stu'){
        $homeworkQuery2 = 'SELECT
                              stw.id,
                            	si.`name` as school,
                            	sl.sch_level as school_level,
                            	sy.sch_year as academic_year,
                            	g.grade_level as grade_level,
                            	c.class_name as class,
                            	s.`subject`,
                            	stw.title,
                            	stw.description,
                            	stw.attach_file,
                              stw.file_type,
                            	stw.transaction_date,
                              u.user_name as created_by
                            FROM
                            	sch_student_home_work stw
                            INNER JOIN sch_school_infor si ON si.schoolid = stw.school_id
                            INNER JOIN sch_school_level sl ON sl.schlevelid = stw.school_level_id
                            INNER JOIN sch_school_year sy ON sy.yearid = stw.academic_year_id
                            INNER JOIN sch_class c ON c.classid = stw.class_id
                            INNER JOIN sch_grade_level g ON g.grade_levelid = stw.grade_level_id
                            INNER JOIN sch_subject s ON s.subjectid = stw.subject_id
                            INNER JOIN sch_user u ON u.userid = stw.created_by
                            '.$where;
        }else{
           $homeworkQuery2 = "SELECT
                              stw.id,
                                si.`name` as school,
                                sl.sch_level as school_level,
                                sy.sch_year as academic_year,
                                g.grade_level as grade_level,
                                c.class_name as class,
                                s.`subject`,
                                stw.title,
                                stw.description,
                                stw.attach_file,
                              stw.file_type,
                                stw.transaction_date,
                              u.user_name as created_by
                            FROM
                                sch_student_home_work stw
                            INNER JOIN sch_school_infor si ON si.schoolid = stw.school_id
                            INNER JOIN sch_school_level sl ON sl.schlevelid = stw.school_level_id
                            INNER JOIN sch_school_year sy ON sy.yearid = stw.academic_year_id
                            INNER JOIN sch_class c ON c.classid = stw.class_id
                            INNER JOIN sch_grade_level g ON g.grade_levelid = stw.grade_level_id
                            INNER JOIN sch_subject s ON s.subjectid = stw.subject_id
                            INNER JOIN sch_user u ON u.userid = stw.created_by
                            WHERE 1=1 AND stw.created_by='".$this->session->userdata('userid')."'
                            "; 
        }
        $homeworks = $this->db->query($homeworkQuery2)->result();
        return $homeworks ;

}
    
    function reload($parameters, $isParameter) {
        
        if($this->session->userdata('match_con_posid')!='stu'){
      $homeworkQuery = <<<QUERY
                            SELECT
                              stw.id,
                            	si.`name` as 'school',
                            	sl.sch_level as 'school_level',
                            	sy.sch_year as 'academic_year',
                            	g.grade_level as 'grade_level',
                            	c.class_name as 'class',
                            	s.`subject`,
                            	stw.title,
                            	stw.description,
                            	stw.attach_file,
                              stw.file_type,
                            	stw.transaction_date,
                              u.user_name as 'created_by'
                            FROM
                            	sch_student_home_work stw
                            INNER JOIN sch_school_infor si ON si.schoolid = stw.school_id
                            INNER JOIN sch_school_level sl ON sl.schlevelid = stw.school_level_id
                            INNER JOIN sch_school_year sy ON sy.yearid = stw.academic_year_id
                            INNER JOIN sch_class c ON c.classid = stw.class_id
                            INNER JOIN sch_grade_level g ON g.grade_levelid = stw.grade_level_id
                            INNER JOIN sch_subject s ON s.subjectid = stw.subject_id
                            INNER JOIN sch_user u ON u.userid = stw.created_by
                            ORDER BY stw.id DESC
QUERY;
    }else{
       $homeworkQuery = "
                            SELECT
                              stw.id,
                                si.`name` as 'school',
                                sl.sch_level as 'school_level',
                                sy.sch_year as 'academic_year',
                                g.grade_level as 'grade_level',
                                c.class_name as 'class',
                                s.`subject`,
                                stw.title,
                                stw.description,
                                stw.attach_file,
                              stw.file_type,
                                stw.transaction_date,
                              u.user_name as 'created_by'
                            FROM
                                sch_student_home_work stw
                            INNER JOIN sch_school_infor si ON si.schoolid = stw.school_id
                            INNER JOIN sch_school_level sl ON sl.schlevelid = stw.school_level_id
                            INNER JOIN sch_school_year sy ON sy.yearid = stw.academic_year_id
                            INNER JOIN sch_class c ON c.classid = stw.class_id
                            INNER JOIN sch_grade_level g ON g.grade_levelid = stw.grade_level_id
                            INNER JOIN sch_subject s ON s.subjectid = stw.subject_id
                            INNER JOIN sch_user u ON u.userid = stw.created_by
                            WHERE 1=1 AND stw.class_id IN(SELECT DISTINCT classid FROM v_student_profile WHERE 1=1 AND studentid='".($this->session->userdata('emp_id'))."')
                            ORDER BY stw.id DESC 
                    ";
    }
      if ($isParameter == 1) {
        $this->db->where($parameters);
      }

      $homeworks = $this->db->query($homeworkQuery)->result();

      return $homeworks;
    }

    function delete($id) {

      $this->db->delete('sch_student_home_work', array('id' => $id));

      if ($this->db->_error_message()) {
          $result['delete'] = 'Error! ['.$this->db->_error_message().']';
      } else if (!$this->db->affected_rows()) {
          $result['delete'] = 'Error! ID ['. $id .'] not found';
      } else {
          $result['delete'] = 'Success';
      }

      return $result;
    }
    
    function getSubject($schoolLevel,$programId) {
        $result_sch_level=$this->db->query("SELECT * from zz_tb_program WHERE id_program=$programId limit 0,1")->row();
        if($result_sch_level!=null && !empty($result_sch_level) ){
            $subject = $this->db->query("SELECT subjectid, subject,subject_kh FROM ".$result_sch_level->label_table." WHERE schlevelid = '$schoolLevel'")->result();
            return $subject;
        }else{
            return null;
        }
        
    }
    function getSchoolLevel($schoolId) {
      return $this->db->query("SELECT lv.schlevelid, lv.sch_level, lv.programid FROM sch_school_level lv INNER JOIN zz_tb_program z ON z.id_program = lv.programid AND lv.schoolid = $schoolId")->result();
    }
    function callClass($school_level_id, $grade_level_id){
        $this->db->order_by("schlevelid", "ASC");
        $this->db->order_by("grade_levelid", "ASC");
        $this->db->order_by("grade_labelid", "ASC");
        
        // $this->db->order_by("class_name", "ASC");
        return $this->db->query("SELECT classid, class_name FROM sch_class WHERE schlevelid = $school_level_id AND grade_levelid = $grade_level_id")->result();
    }
}
