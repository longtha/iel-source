<?php
	class Modevaluate_moyes extends CI_Model{

		function getevaluate(){
			$page=0;
			$yearid=$this->session->userdata('year');
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m='';
			$p='';
			if(isset($_GET['m'])){
		        $m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }	
				$sql="SELECT * FROM v_moeys_evaluate se
						INNER JOIN v_student_profile s
						ON(se.studentid=s.studentid AND s.yearid=se.yearid)
						WHERE  se.yearid='$yearid'
						";
				$config['base_url'] =site_url()."/student/evaluate_moyes?pg=1&m=$m&p=$p";
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =150;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.="  ORDER BY se.transno desc,s.last_name_kh {$limi}";
				return $this->db->query($sql)->result();
		}
		function getresult(){
				$page=0;
				$yearid=$this->session->userdata('year');
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				$m='';
				$p='';
				if(isset($_GET['m'])){
			        $m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }	
				$sql="SELECT eval.studentid,
								eval.classid,
								s.class_name,
								s.fullname,
								eval.yearid,
								count(eval.studentid) as stdtimes,
								group_concat(eval.sem_avg) as avg,
								sum(eval.sem_avg) as total
					FROM sch_stud_evalsemester eval
					INNER JOIN v_student_profile s
					ON(eval.studentid=s.studentid AND eval.classid=s.classid)
					WHERE eval.yearid='$yearid' 
					GROUP BY eval.studentid";	
				$config['base_url'] =site_url()."/student/evaluate_moyes?pg=1&m=$m&p=$p";
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =50;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				return $this->db->query($sql)->result();
		}
		function getevasemester(){
			$page=0;
			$yearid=$this->session->userdata('year');
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m='';
			$p='';
			if(isset($_GET['m'])){
		        $m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }	
				$sql="SELECT * FROM sch_stud_evalsemester se
						INNER JOIN v_student_profile s
						ON(se.studentid=s.studentid AND s.yearid=se.yearid)
						WHERE  se.yearid='$yearid'";	
				$config['base_url'] =site_url()."/student/evaluate_moyes/semesterlist?pg=1&m=$m&p=$p";
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =100;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.="  Order By se.transno desc {$limi}";
				return $this->db->query($sql)->result();
		}
		function save(){
			$c_date=date('Y-m-d');
			$schoolid=$this->session->userdata('schoolid');
			$user=$this->session->userdata('user_name');
			$studentid=$this->input->post('studentid');
			$upd=$this->input->post('upd');
			$classid=$this->input->post('classid');
			$date=$this->green->convertSQLDate($this->input->post('eva_date'));
			$month=$this->input->post('eva_months');
			$evaluate_type=$this->input->post('eva_type');

			$kh_teacher=$this->input->post('teacher_khid');
			$forign_teacher=$this->input->post('teacher_enid');
			$fra_teacher=$this->input->post('teacher_frid');

			$transno=$this->input->post('transno');
			if($transno==""){
				$transno=$this->green->nextTran("2","Evaluate Student");
			}			

			$other_month_tran=$this->input->post('eva_transno');
            $avg_coefficient=$this->input->post('avg_coefficient');

			$eval_semester='';

			if($evaluate_type!='month'){
				$eval_semester=$this->input->post('eva_semester');
			}

			$sem_transno=0;
			for($i=0;$i<count($studentid);$i++){
				$yearid=$this->db->select('year')->from('sch_student_enrollment')->where('studentid',$studentid[$i])->where('classid',$classid)->get()->row()->year;
				$level=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
				$data=array('studentid'=>$studentid[$i],
							'classid'=>$classid,
							'yearid'=>$yearid,
							'date'=>$date,
							'month'=>trim($month,','),
							'evaluate_type'=>$evaluate_type,
							'eng_teach_name'=>$fra_teacher,
							'kh_teacher'=>$kh_teacher,
							'eval_semester'=>$eval_semester,
							'schoolid'=>$schoolid,
							'forign_teacher'=>$forign_teacher,
							'schlevelid'=>$level,
                            'avg_coefficient'=>$avg_coefficient
							);
				$count=$this->evaluate->validate($studentid[$i],$transno);
				if($count>0){
					$data2=array('modified_date'=>$c_date,'modified_by'=>$user);
					$this->db->where('studentid',$studentid[$i])->where('transno',$transno)->update('sch_student_evaluated',array_merge($data,$data2));
					$evaluateid=$this->db->where('transno',$transno)->where('studentid',$studentid[$i])->get('sch_student_evaluated')->row()->evaluateid;
					$this->clearmention($evaluateid);

				}else{
					$data2=array('created_date'=>$c_date,'created_by'=>$user,'transno'=>$transno);
					$this->db->insert('sch_student_evaluated',array_merge($data,$data2));
					$evaluateid=$this->db->insert_id();

					//$this->updatetran($transno+1);
				}
				$this->savesubjment($transno,$evaluateid,$i,$studentid[$i],$classid,$yearid,$evaluate_type,$eval_semester,trim($month,','),$other_month_tran);
				$evaluateid='';
			}
		}
		function savesubjment($transno,$evaluateid,$i,$studentid,$classid,$yearid,$type,$eval_semester,$months,$month_tran){
			$countsub=0;
			$totalscore=0;
			$months_avg=0;
			$each_months_avg=0;
			$trimester=0;
	   		if($type=='Three Month')
				$trimester=1;
			foreach ($this->gets_type($classid,$yearid,$trimester) as $sub) {
				foreach ($this->getsubject($classid,$sub->subj_type_id,$yearid,$trimester) as $subject) {
					$mention='';
					$countsub++;
					$score=$this->input->post("$subject->subjectid");
					$level=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;

					if($type=='Three Month'){
						$mention=$score[$i];
					}else{

                        if(isset($score[$i])){
                            $objmen=$this->getstdmention($score[$i],$level);
                            if(isset($objmen->mention)){
                                $mention=$objmen->mention;
                            }else{
                                $mention='';
                            }

                        }else{
                            $mention='';
                        }
						$totalscore+=$score[$i];
					}// echo "+ $subject->subject:".$mention[$i].'<br/>';
					$datamen=array('evaluateid'=>$evaluateid,
									'subjectid'=>$subject->subjectid,
									'mention'=>$mention,
									'score'=>$score[$i],
									'type'=>2,
									'transno'=>$transno,
									'studentid'=>$studentid,
									'classid'=>$classid,
									'yearid'=>$yearid,
									'schlevelid'=>$level
									);
					$this->db->insert('sch_student_evaluated_mention',$datamen);
				}
			}
			if($type=='semester'){
				$srtM=$month_tran;
				$month=explode(",", $srtM);
				// print_r($month);
				$month_num=count($month)-1;//get number of months
				for($i=0;$i<$month_num;$i++) {
					$each_months_avg+=($this->evaluate->getstdscore($month[$i],$studentid)->score)/$countsub;
					//echo "$countsub each month :".$each_months_avg."/tr:".$month[$i]." /std/$studentid <br>";
				}
				$months_avg=number_format($each_months_avg/$month_num, 2, '.', '');//get month average
				$sem_exam_avg=number_format($totalscore/$countsub, 2, '.', '');//get average of semester exam
				$sem_avg=number_format(($months_avg+$sem_exam_avg)/2, 2, '.', '');
				$this->savesemester($transno,$studentid,$classid,$yearid,$eval_semester,$months,$sem_exam_avg,$months_avg,$month_num,$sem_avg);
			}
		}
		function savesemester($sem_transno,$studentid,$classid,$yearid,$semester,$months,$sem_exam_avg,$months_avg,$month_num,$sem_avg,$sem_rank=''){
			$count=$this->validatesem($studentid,$sem_transno);
			$sem_result='';
			$sem_mention='';
			$level=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
			$mention=$this->getstdmention($sem_avg,$level)->mention;
			//echo $mention.'<br>';
			if($sem_avg>=5)
				$sem_result='Pass';
			else
				$sem_result='Fail';
			$c_date=date('Y-m-d');
			$user=$this->session->userdata('user_name');
			$data=array('studentid'=>$studentid,
						'classid'=>$classid,
						'yearid'=>$yearid,
						'semester'=>$semester,
						'months'=>$months,
						'sem_exam_avg'=>$sem_exam_avg,
						'months_avg'=>$months_avg,
						'month_num'=>$month_num,
						'sem_avg'=>$sem_avg,
						'sem_mention'=>$mention,
						'sem_result'=>$sem_result,
						'sem_rank'=>$sem_rank,
						'type'=>2,
						'schlevelid'=>$level
						);
			if($count>0){
				$data2=array('modified_date'=>$c_date,'modified_by'=>$user);
				$this->db->where('studentid',$studentid)->where('transno',$sem_transno)->update('sch_stud_evalsemester',array_merge($data,$data2));
			}else{
				$data2=array('created_date'=>$c_date,'created_by'=>$user,'transno'=>$sem_transno);
				$this->db->insert('sch_stud_evalsemester',array_merge($data,$data2));
			}
		}
		function validatesem($studentid,$tran){
			return $this->db->select('count(*)')
						->from('sch_stud_evalsemester')
						->where('transno',$tran)
						->where('studentid',$studentid)
						->count_all_results();
		}
		function updatetran($val){
			$this->db->set('sequence',$val)->where('typeid',2)->update('sch_z_systype');
		}
		function gettransno(){
			return $this->db->where('typeid',2)->get('sch_z_systype')->row()->sequence;
		}
		function getscoremention($schlevelid){
			return $this->db->where('schlevelid',$schlevelid)
						->where('is_final_ses',1)
						->order_by('mention','asc')
						->get('sch_score_mention')->result();
		}
		function getteacherbyclass($classid,$yearid){
			return $this->db->select('emp.empid,emp.last_name,emp.first_name')
							->from('sch_teacher_class tc')
							->join('sch_emp_profile emp','tc.teacher_id=emp.empid','inner')
							->where('tc.class_id',$classid)->where('tc.yearid',$yearid)->get()->result();

		}
		function getmention($evaluateid,$subjectid){
			return $this->db->where('evaluateid',$evaluateid)
							->where('subjectid',$subjectid)
							->get('sch_student_evaluated_mention')->row();
		}
		function clearmention($evaluateid){
			$this->db->where('evaluateid',$evaluateid)->delete('sch_student_evaluated_mention');
		}
		function getrank($transno,$evaluateid){
			$data="SELECT sum(em.score) as score,em.evaluateid
					FROM sch_student_evaluated se
					INNER JOIN sch_student_evaluated_mention em
					ON(se.evaluateid=em.evaluateid)
					WHERE se.transno='".$transno."'
					GROUP BY em.evaluateid
					ORDER BY score desc";
			$rank=1;
			$i=0;
			$score=0;
			foreach ($this->db->query($data)->result() as $row) {
				$i++;
				if($row->score<$score){
					$rank=$i;
				}
				if($row->evaluateid==$evaluateid)
						return $rank;
				$score=$row->score;
			}
		}
		function getsemrank($transno,$semid){
			$data="SELECT sem_avg as score,semid
					FROM sch_stud_evalsemester
					WHERE transno='".$transno."'
					ORDER BY score desc";
			$rank=1;
			$i=0;
			$score=0;
			foreach ($this->db->query($data)->result() as $row) {
				$i++;
				if($row->score<$score){
					$rank=$i;
				}
				if($row->semid==$semid)
						return $rank;
				$score=$row->score;
			}
		}
		function getaverage($transno,$evaluateid){
			$data="SELECT 
						SUM(em.score) as score,
						AVG(em.score) AS average,
						se.avg_coefficient as avg_coefficient
					FROM sch_student_evaluated se
					INNER JOIN sch_student_evaluated_mention em
					ON(se.evaluateid=em.evaluateid)
					WHERE se.transno='".$transno."'
					AND em.evaluateid='".$evaluateid."'
					Group by avg_coefficient
				";
			$query = $this->db->query($data);
			if ($query->num_rows() > 0)
			{
			   return $query->row();
			} 
			return NULL;
		}
		function getmonthpermis($studentid,$month){
			return $this->db->query("SELECT 
									studentid,
									permis_status,
								 	SUM(DATEDIFF(to_date,from_date) +1 )as days
								FROM
								sch_student_permission
								WHERE DATE_FORMAT(sch_student_permission.to_date,'%b')='$month'
								AND studentid='$studentid'
								GROUP BY permis_status")->result();
		}
		function getstdscore($transno,$studentid){
			$data="SELECT SUM(em.score) as score
						FROM sch_student_evaluated se
						INNER JOIN sch_student_evaluated_mention em
						ON(se.evaluateid=em.evaluateid)
						WHERE se.transno='".$transno."'
						AND em.studentid='".$studentid."'";
			return $this->db->query($data)->row();
		}
		function khmonth($month){
			$khmonth='';
			if($month=='Jan')
				$khmonth='មករា';
			if($month=='Feb')
				$khmonth='គុម្ភ:';
			if($month=='Mar')
				$khmonth='មិនា';
			if($month=='Apr')
				$khmonth='មេសា';
			if($month=='May')
				$khmonth='ឧសភា';
			if($month=='jun')
				$khmonth='មិថុនា';
			if($month=='Jul')
				$khmonth='កក្កដា';
			if($month=='Aug')
				$khmonth='សីហា';
			if($month=='Sep')
				$khmonth='កញ្ញា';
			if($month=='Oct')
				$khmonth='តុលា';
			if($month=='Nov')
				$khmonth='វិច្ឆិកា';
			if($month=='Dec')
				$khmonth='ធ្នូ';
			return $khmonth;
		}
		
		function getedit($transno){
			return $this->db->query("SELECT DISTINCT transno,
													date,
													yearid,
													evaluate_type,
													modified_by,
													modified_date,
													eval_semester,
													classid,
													kh_teacher,
													forign_teacher,
													class_comment,
													eng_teach_name,
													month 
													FROM sch_student_evaluated
													Where transno='$transno'
													")->row();
		}
		function validate($stu,$tra){
			return $this->db->select('count(*)')
						->from('sch_student_evaluated')
						->where('transno',$tra)
						->where('studentid',$stu)
						->count_all_results();
		}
		function getmonthclass($classid,$yearid){
			return $this->db->query("SELECT DISTINCT(month),transno 
					FROM sch_student_evaluated 
					where classid='".$classid."'
					AND yearid='".$yearid."'
					AND evaluate_type='month'")->result_array();
		}
		
		function getstdmention($score,$grade_levelid){
			return $this->db->where('min_score <=',$score)
							->where('max_score >=',$score)
							->where('schlevelid',$grade_levelid)
							->get('sch_score_mention')->row();
		}
		function gets_type($classid,$yearid,$trim){
			$sch=$this->session->userdata('schoolid');
			return $this->db->query("SELECT st.subject_type,st.subj_type_id,count(sb.subject) s_total FROM sch_class c
									INNER JOIN sch_level_subject_detail sld
									ON(c.grade_levelid=sld.grade_levelid)
									INNER JOIN sch_subject sb ON(sb.subjectid=sld.subjectid)
									INNER JOIN sch_subject_type st ON(sb.subj_type_id=st.subj_type_id)
									WHERE c.classid='$classid'
									AND sld.year='$yearid'
									AND sld.schoolid='$sch'
									AND st.is_moeys='1'
									AND sb.is_eval='1'
									AND sb.is_trimester_sub=0
									GROUP BY st.subject_type,st.subj_type_id
									ORDER BY st.orders
									")->result();
		}
		function getsubject($classid,$subject_type,$yearid,$trimester){
			$sch=$this->session->userdata('schoolid');
			return $this->db->query("SELECT sb.subject,sb.subject_kh,sb.short_sub,sb.subjectid FROM sch_class c
									INNER JOIN sch_level_subject_detail sld ON(c.grade_levelid=sld.grade_levelid)
									INNER JOIN sch_subject sb ON(sb.subjectid=sld.subjectid)
									WHERE c.classid='$classid'
									AND sld.year='$yearid'
									AND sld.schoolid='$sch'
									AND sb.is_eval='1'
									AND sb.is_trimester_sub=0
									AND sb.subj_type_id='$subject_type'
									ORDER  BY sb.orders
									")->result();
		}
		function getstudent($classid,$year){
			return $this->db->query("SELECT * 
									from v_student_profile 
									WHERE classid='$classid'
									AND yearid='$year'
									ORDER BY last_name_kh
									")->result();
		}
		function getrow($evaluateid,$transno=''){
			$where='';
			if($evaluateid!='')
				$where.=" AND evaluateid='$evaluateid'";
			if($transno!='')
				$where.=" AND transno='$transno'";
			$data=$this->db->query("SELECT * FROM v_moeys_evaluate where 1=1 {$where}");
			if($evaluateid!='')
				return $data->row();
			else
				return $data->result();
		}
		function getsemrow($semid){
			return $this->db->query("SELECT * FROM 
								v_moeys_evaluate ste
								INNER JOIN sch_stud_evalsemester sem
								ON(ste.studentid=sem.studentid AND ste.transno=sem.transno)
								WHERE sem.semid='".$semid."'")->row();
		}
		function searchsemester($student_name,$schlevelid,$classid,$yearid,$semester,$s_num,$m,$p,$sort=''){
			$page=0;
			$where='';
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			if($student_name!=""){
				$where=" AND (s.last_name like '%".$student_name."%' OR s.first_name like '%".$student_name."%' OR s.fullname LIKE '%".$student_name."%')";
			}
			if($classid!=""){
				$where.=" AND se.classid = '".$classid."'";
			}
			if($schlevelid!=""){
				$where.=" AND s.schlevelid = '".$schlevelid."'";
			}
			if($semester!=""){
				$where.=" AND se.semester = '".$semester."'";
			}
				$sql="SELECT * FROM sch_stud_evalsemester se
						INNER JOIN v_student_profile s
						ON(se.studentid=s.studentid AND s.yearid=se.yearid)
						WHERE s.yearid='$yearid' {$where}
						";
				$config['base_url'] =site_url()."/student/evaluate_moyes/searchsemester?pg=1&n=$student_name&c=$classid&y=$yearid&&se=$semester&s_num=$s_num&m=$m,&p=$p&l=$schlevelid";
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =50;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" $sort  {$limi}";
				return $this->db->query($sql)->result();
		}
		function search($student_name,$eva_type,$semester,$schlevelid,$classid,$month,$yearid,$s_num,$m,$p,$sort=''){
			$page=0;
			$where="";

			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
            if($yearid!=""){
                $where.=" AND stinf.yearid = {$yearid}";
            }
			if($student_name!=""){
                $where.=" AND (fullname LIKE '%".$student_name."%')";
			}
			if($eva_type!=""){
                $where.=" AND evaluate_type = '".$eva_type."'";
			}
			if($semester!=''){
				$where.=" AND eval_semester='$semester'";
			}

			if($classid!=""){
                $where.=" AND stinf.classid = '".$classid."'";
			}
			if($schlevelid!=""){
                $where.=" AND stinf.schlevelid = '".$schlevelid."'";
			}
			if($month!=""){
                $where.= " AND month = '".$month."'";
			}
			
			#========== temporary tab ======= 1

			$sql="SELECT
					eval.evaluateid,
					stinf.student_num,
					stinf.first_name,
					stinf.last_name,
					stinf.fullname,
					stinf.first_name_kh,
					stinf.last_name_kh,
					stinf.fullname_kh,
					stinf.yearid,
					stinf.studentid,
					stinf.classid,
					stinf.sch_year,
					stinf.class_name,
					stinf.is_active,
					stinf.familyid,
					stinf.schlevelid,
					stinf.promot_id,
					stinf.is_vtc,
					eval.evaluate_type,
					eval.date,
					eval.`month`,
					eval.kh_teacher_comment,
					eval.forign_teacher_comment,
					eval.kh_teacher,
					eval.forign_teacher,
					eval.type,
					eval.transno,
					eval.eval_semester,
					eval.eng_teach_com,
					eval.eng_teach_name,
					eval.schoolid,
					eval.technic_com,
					eval.supple_com,
					eval.avg_coefficient
				FROM
					v_student_profile AS stinf
				INNER JOIN v_moeys_evaluate AS eval ON stinf.studentid = eval.studentid
				WHERE 1=1
				{$where} GROUP BY eval.evaluateid ";

				$config['base_url'] =site_url()."/student/evaluate_moyes/search?pg=1&n=$student_name&type=$eva_type&l=$schlevelid&sem=$semester&c=$classid&mo=$month&y=$yearid&s_num=$s_num&m=$m&p=$p";
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =$s_num;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}
                if($sort!=""){
                    $sort.=" ,last_name_kh ";
                }else{
                    $sort=" ORDER BY eval.transno desc ,last_name_kh ";
                }

				$sql.=" $sort {$limi}";
				return $this->db->query($sql)->result();
		}
		function searchresult($name,$schlevelid,$classid,$yearid,$s_result,$sort_num,$sort=''){
				$page=0;
				$passscore=5;
				$where='';
				$having='';
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				if($name!='')
					$where.=" AND s.fullname LIKE '%".$name."%'";
				if($classid!='')
					$where.=" AND eval.classid='".$classid."'";
				if($schlevelid!='')
					$where.=" AND s.schlevelid='".$schlevelid."'";
				if($s_result!=''){
					if($s_result=='pass')
						$having.=" AND (total/2)>='".$passscore."'";
					else
						$having.=" AND (total/2)<'".$passscore."'";
				}

					
				$sql="SELECT eval.studentid,
								eval.classid,
								s.class_name,
								s.fullname,
								eval.yearid,
								count(eval.studentid) as stdtimes,
								group_concat(eval.sem_avg) as avg,
								sum(eval.sem_avg) as total
					FROM sch_stud_evalsemester eval
					INNER JOIN v_student_profile s
					ON(eval.studentid=s.studentid AND eval.classid=s.classid)
					WHERE eval.yearid='$yearid' {$where} 
					GROUP BY eval.studentid
					HAVING stdtimes>1 {$having}";	
				$config['base_url'] =site_url()."/student/evaluate_moyes/searchresult?pg=1&n=$name&l=$schlevelid&c=$classid&y=$yearid&r=$s_result&s_num=$sort_num";
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =$sort_num;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$sort} {$limi}";
				return $this->db->query($sql)->result();
		}
		public function getStudentInfo($yearid='',$classid='',$schlevelid='',$transno)
		{
			$where ="";
			if($classid!=''){
				$where.=" AND s.classid = '".$classid."'";
			}
			if($schlevelid!=''){
				$where.=" AND s.schlevelid = '".$schlevelid."'";
			}

			$sqlgsi= "SELECT * FROM v_moeys_evaluate se
						INNER JOIN v_student_profile s
						ON(se.studentid=s.studentid AND s.yearid=se.yearid)
						WHERE
						se.yearid='{$yearid}'
						AND se.transno='{$transno}'
						{$where}
					    ORDER BY s.fullname_kh
						";
			//echo $sqlgsi;
			return $this->db->query($sqlgsi)->result();
		}

        function getMoYScoreByTran($transno){
            $sql="SELECT
                        score.studentid,
                        score.subjectid,
                        score.score
                    FROM
                        sch_student_evaluated_mention score
                    WHERE score.transno = '{$transno}'
                ";
            $score_data=$this->green->getTable($sql);
            $arrScores=array();
            if(count($score_data)>0){
                foreach($score_data as $row){
                    $arrScores[$transno][$row['studentid']][$row['subjectid']]= $row['score'];
                }
            }
            return $arrScores;
        }


		public function getScoreOfSubject($stuID='',$subjID='',$evaluateid='')
		{

			$where ="";
			if($stuID!='')
			{
				$where.=" AND vsp.studentid ='".$stuID."'";
			}
			if($subjID!='')
			{
				$where.=" AND ssem.subjectid ='".$subjID."'";
			}
			if($evaluateid!='')
			{
				$where.=" AND ssem.evaluateid = '".$evaluateid."'";
			}
			$sqlgsos= "SELECT DISTINCT
							ssem.studentid,
							vsp.fullname,
							ssem.evaluate_menid,
							ssem.evaluateid,
							ssem.subjectid,
							ssem.mention,
							ssem.score,
							ssem.type,
							ssem.transno,
							ssem.classid,
							ssem.yearid,
							ssem.schlevelid	
						FROM
							v_student_profile vsp
						INNER JOIN sch_student_evaluated_mention ssem ON ssem.studentid=vsp.studentid
						WHERE 1=1  {$where} ORDER BY ssem.score DESC";

			return $this->db->query($sqlgsos)->result();
		}
		
		public function setClassName($class_name='')
		{
			$where="";
			if($class_name!='')
				$where.=" AND sc.classid='".$class_name."'";	
			return $this->db->query("SELECT sc.class_name FROM sch_class sc WHERE 1=1 {$where}")->row()->class_name;
		}


        public function getCountRowEvaluate()
        {
            $this->db->select('subject_kh');
            $this->db->from('sch_subject');
            $this->db->where('is_trimester_sub', 1);
            $this->db->where('is_eval', 1);
            $this->db->where('subject_kh <>', '');
            $total_sold = $this->db->count_all_results();
            if ($total_sold > 0)
            {
                return $total_sold;
            }
            return NULL;
        }
        public function countsubjectevaluate($yearid,$classid,$schlevelid)
        {
			$this->db->select("sb.subjectid");
		    $this->db->from('sch_class c');
		    $this->db->join('sch_level_subject_detail sld', "sld.grade_levelid = c.grade_levelid");
		    $this->db->join('sch_subject sb', "sb.subjectid = sld.subjectid");
		    $this->db->where('sb.is_trimester_sub', 0);
            $this->db->where('sb.is_eval', 1);
            $this->db->where('c.classid', $classid);
            $this->db->where('sld.`year`', $yearid);
            $this->db->where('sld.schoolid', $schlevelid);
            $this->db->order_by('orders','desc');
		    return $this->db->count_all_results();
        }
        public function saveImportStudentScore()
        {
            $is_imported=false;
            if (isset($_FILES['file_import']) && $_FILES['file_import']["tmp_name"][0]!="")
            {
                $_xfile = $_FILES['file_import']["tmp_name"];
                function str($str){
                    return str_replace("'","''",$str);
                }
                if($_FILES['file_import']['tmp_name']==""){
                    $error="<font color='red'>Please Choose your Excel file!</font>";
                }
                else{
                    $html="";
                    require_once(APPPATH.'libraries/simplexlsx.php' );
                    $transno=$this->green->nextTran("2","Evaluate Student");

                    foreach($_xfile as $index => $value){
                        $xlsx = new SimpleXLSX($value);
                        $_data = $xlsx->rows();
                        array_splice($_data,0,5);
                        $j=1;
                        $subject_row=11;
                        $data_row=13;
                        $studentid='';
                        $classid=0;
                        $yearid=0;
                        $date='';
                        $month='';
                        $evaluate_type='';
                        $schoolid='';
                        $level="";
                        $avg_coefficient="";
                        $eval_semester="";
                        $arrSubjectids=array();
                        foreach( $_data as $k => $r) {
                            if($j==1) $yearid=trim(str($r[1]));
                            if($j==2) $month=trim(str($r[1]));
                            if($j==3) $date=trim(str($r[2]));
                            if($j==4) $evaluate_type=trim(str($r[1]));
                            if($j==5) $classid=trim(str($r[1]));
                            if($j==6) $schoolid=trim(str($r[1]));
                            if($j==7) $level=trim(str($r[1]));
                            if($j==8) $eval_semester=trim(str($r[1]));
                            if($j==9){$avg_coefficient=trim(str($r[1]));}
                            if($j==$subject_row){
                                for ($i=1; $i <=$this->countsubjectevaluate($yearid,$classid,$level); $i++) {
                                    $arrSubjectids[]=trim(str($r[(2+$i)]));
                                }
                            }
                            $fra_teacher="";
                            $kh_teacher="";
                            $forign_teacher="";
                            $type=2;
                            if($j>=$data_row){
                                $studentid=trim(str($r[(2)]));
                                if($studentid > 0){
	                                $data=array('studentid'=>$studentid,
			                                    'classid'=>$classid,
			                                    'yearid'=>$yearid,
			                                    'date'=>$date,
			                                    'month'=>trim($month,','),
			                                    'evaluate_type'=>$evaluate_type,
			                                    'eng_teach_name'=>$fra_teacher,
			                                    'kh_teacher'=>$kh_teacher,
			                                    'eval_semester'=>$eval_semester,
			                                    'schoolid'=>$schoolid,
			                                    'forign_teacher'=>$forign_teacher,
			                                    'schlevelid'=>$level,
			                                    'avg_coefficient'=>$avg_coefficient,
			                                    'transno'=>$transno,
			                                    'type'=>2
			                                );
	                                $this->db->insert('sch_student_evaluated',$data);
	                                $evaluateid=$this->db->insert_id();
	                                if(count($arrSubjectids)>0){
	                                    $sub_count=1;
	                                    foreach ($arrSubjectids as $subjectid) {
	                                        $score=trim(str($r[(2+$sub_count)]));
	                                        $mention='';
	                                        if(isset($score)){
	                                            $objmen=$this->getstdmention($score,$level);
	                                            if(isset($objmen->mention)){
	                                                $mention=$objmen->mention;
	                                            }
	                                        }
	                                        $datamen=array('evaluateid'=>$evaluateid,
				                                            'subjectid'=>$subjectid,
				                                            'mention'=>$mention,
				                                            'score'=>$score,
				                                            'type'=>2,
				                                            'transno'=>$transno,
				                                            'studentid'=>$studentid,
				                                            'classid'=>$classid,
				                                            'yearid'=>$yearid,
				                                            'schlevelid'=>$level
				                                        );
	                                        $this->db->insert('sch_student_evaluated_mention',$datamen);
	                                        $sub_count++;
	                                    }
	                                }
                                }
                            }
                            $j++;
                            $is_imported=TRUE;
                        }
                    }
                }
            }
            return $is_imported;
        }
	}

?>