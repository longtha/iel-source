<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class ModVtcStar extends CI_Model{
		function save(){
			$starid=$this->input->post('starid');
			$f_date=$this->green->convertSQLDate($this->input->post('f_date'));
			$to_date=$this->green->convertSQLDate($this->input->post('e_date'));
			$partnerid=$this->input->post('partnerid');
			$promot_id=$this->input->post('promot_id');
			$studentid=$this->input->post('student_id');
			$classid=$this->input->post('class_id');
			$user=$this->session->userdata('user_name');
			$yearid=$this->session->userdata('year');
			$schoolid=$this->session->userdata('schoolid');
			//echo $partnerid;
			$transno=$this->green->nextTran('13','VTC Intern');
			$c_date=date('Y-m-d H:i:s');
			$data=array('from_date'=>$f_date,
						'to_date'=>$to_date,
						'partnerid'=>$partnerid,
						'promot_id'=>$promot_id,
						'yearid'=>$yearid,
						'schoolid'=>$schoolid,
						'type'=>13
						);
			if($starid!=''){
				$data2=array('modified_by'=>$user,
								'modified_date'=>$c_date);

				$this->db->where('starid',$starid);
				$this->db->update('sch_stud_vtcstar',array_merge($data, $data2));
				$this->cleasdetail($starid);
			}else{
				$data2=array('created_by'=>$user,
							'created_date'=>$c_date,
							'transno'=>$transno);
				$this->db->insert('sch_stud_vtcstar',array_merge($data, $data2));
				$starid=$this->db->insert_id();
				$this->updatetran($transno+1);
				
			}
			for ($i=0; $i < count($studentid); $i++) { 
				$this->savedetail($starid,$studentid[$i],$classid[$i],$transno);
			}
		}
		function savedetail($starid,$studentid,$classid,$transno){
			$data=array('starid'=>$starid,
						'studentid'=>$studentid,
						'classid'=>$classid,
						'transno'=>$transno,
						'type'=>13);
			$this->db->insert('sch_stud_vtcstar_detail',$data);
		}
		function cleasdetail($starid){
			$this->db->where('starid',$starid)->delete('sch_stud_vtcstar_detail');
		}
		function updatetran($transno){
			$this->db->where('typeid',13)->set('sequence',$transno)->update('sch_z_systype');
		}
		function getstar($yearid){
			$m='';
			$p='';
			$where='';
			if(isset($_GET['m'])){
		        $m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
		    $page=0;
			if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
			$sql="SELECT * 
					FROM sch_stud_vtcstar sv
					LEFT JOIN sch_school_promotion sp
					ON(sv.promot_id=sp.promot_id)
					INNER JOIN sch_stud_partner part 
					ON(sv.partnerid=part.partnerid)";
			$config['base_url'] =site_url()."/student/vtcstar/index?pg=1&m=$m&p=$p";	
			$config['total_rows'] =  $this->green->getTotalRow($sql);
			$config['per_page'] =20;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);
			$limi=" limit ".$config['per_page'];
			if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}
			$sql.=$limi;
			return $this->db->query($sql)->result();
		}
		function getvtcclass(){
			$year=$this->session->userdata('year');
			return $this->db->query("SELECT c.class_name,c.classid
									FROM sch_class c
									INNER JOIN sch_school_level l
									ON(c.schlevelid=l.schlevelid)
									WHERE is_vtc='1'")->result();
		}
		function getpromomte(){
			$schoolid=$this->session->userdata('schoolid');
			return $this->db->where('schoolid',$schoolid)->get("sch_school_promotion")->result();
		}
		function getstdlist($promot_id,$classid,$studentid){
			$yearid=$this->session->userdata('year');
			$where='';
			if($promot_id!='0')
				$where.=" AND promot_id ='$promot_id'";
			if($classid!='0')
				$where.=" AND classid='$classid'";
			if($studentid!='')
				$where.=" AND studentid='$studentid'";
			return $this->db->query("SELECT * 
									FROM v_student_profile
									WHERE is_vtc='1' AND yearid='$yearid'
									{$where}")->result();
		}
		function geteditlist($starid){
			$yearid=$this->session->userdata('year');
			return $this->db->query("SELECT * 
									FROM sch_stud_vtcstar_detail sv
									INNER JOIN v_student_profile s
									ON(sv.studentid=s.studentid)
									WHERE s.is_vtc='1' AND s.yearid='$yearid'
									AND sv.starid='$starid'")->result();
		}
		function search($promot_id,$partnerid,$sort_num,$m,$p,$yearid){			
		    $page=0;
		    $where='';
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			if($promot_id!='')
				$where.=" AND sv.promot_id='".$promot_id."'";
			if($partnerid!='')
				$where.=" AND sv.partnerid='".$partnerid."'";
			if($yearid!='')
				$where.=" AND sv.yearid='".$yearid."'";

			$sql="SELECT * 
					FROM sch_stud_vtcstar sv
					LEFT JOIN sch_school_promotion sp ON sv.promot_id=sp.promot_id
					LEFT JOIN sch_stud_partner part ON sv.partnerid=part.partnerid
					WHERE 1=1 {$where}";
			$config['base_url'] =site_url()."/student/vtcstar/index?pg=1&m=$m&p=$p&pr=$promot_id&pa=$partnerid&s_num=$sort_num&y=$yearid";	
			$config['total_rows'] =  $this->green->getTotalRow($sql);
			$config['per_page'] =$sort_num;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);
			$limi=" limit ".$config['per_page'];
			if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}
			$sql.=$limi;
			return $this->db->query($sql)->result();
		}
	}