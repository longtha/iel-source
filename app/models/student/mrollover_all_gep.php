<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mrollover_all_gep extends CI_Model{
	function schoolid(){
		$sql_school = $this->db->query("SELECT schoolid,`name` FROM sch_school_infor");
		return $sql_school;
	}
	
	function program(){
		$get_data = $this->input->post("arr_data");
		$sql_program = $this->db->query("SELECT programid,program FROM sch_school_program WHERE 1=1 AND programid=3 AND schoolid='".$get_data['schoolid']."'");
		$opt_program = "<option value=''></option>";
		if($sql_program->num_rows() > 0){
			foreach($sql_program->result() as $rprogram){
				$opt_program.='<option value="'.$rprogram->programid.'">'.$rprogram->program.'</option>';
			}
		}
		return $opt_program;
	}
	function schlevel(){
		$get_data = $this->input->post("arr_data");
		$sql_schlevel = $this->db->query("SELECT schlevelid,sch_level FROM sch_school_level WHERE 1=1 AND schoolid='".$get_data['schoolid']."' AND programid='".$get_data['program']."'");
		$opt_schlevel = "<option value=''></option>";
		if($sql_schlevel->num_rows() > 0){
			foreach($sql_schlevel->result() as $rschlevel){
				$opt_schlevel.='<option value="'.$rschlevel->schlevelid.'">'.$rschlevel->sch_level.'</option>';
			}
		}
		return $opt_schlevel;
	}
	function schyear(){
		$get_data = $this->input->post("arr_data");
		$sql_schyear = $this->db->query("SELECT yearid,sch_year FROM sch_school_year WHERE 1=1 AND schoolid='".$get_data['schoolid']."' AND programid='".$get_data['program']."' AND schlevelid='".$get_data['schoollevel']."'");
		$opt_year = "<option value=''></option>";
		if($sql_schyear->num_rows() > 0){
			foreach($sql_schyear->result() as $ryear){
				$opt_year.='<option value="'.$ryear->yearid.'">'.$ryear->sch_year.'</option>';
			}
		}
		return $opt_year;
	}
	function ranglevel(){
		$get_data = $this->input->post("arr_data");
		$sql_ranglevel = $this->db->query("SELECT
											sch_school_rangelevel.rangelevelid,
											sch_school_rangelevel.rangelevelname,
											sch_school_rangelevel.schlevelid,
											sch_school_rangelevel.schoolid,
											sch_school_rangelevel.programid
											FROM
											sch_school_rangelevel
											WHERE programid='".$get_data['program']."'
											AND schoolid='".$get_data['schoolid']."'
											AND schlevelid='".$get_data['schoollevel']."'
										");
		$opt_range = "<option value=''></option>";
		if($sql_ranglevel->num_rows() > 0){
			foreach($sql_ranglevel->result() as $r_rang){
				$opt_range.= "<option value='".$r_rang->rangelevelid."'>".$r_rang->rangelevelname."</option>";
			}
		}
		return $opt_range;
	}
	function grandlevel(){
		$get_data = $this->input->post("arr_data");
		$sql_schgrade = $this->db->query("SELECT grade_levelid,grade_level FROM sch_grade_level WHERE 1=1 AND schoolid='".$get_data['schoolid']."' AND schlevelid='".$get_data['schoollevel']."'");
		$opt_grand = "<option value=''></option>";
		if($sql_schgrade->num_rows() > 0){
			foreach($sql_schgrade->result() as $rgrad){
				$opt_grand.='<option value="'.$rgrad->grade_levelid.'">'.$rgrad->grade_level.'</option>';
			}
		}
		return $opt_grand;
	}
	function classname(){
		$get_data = $this->input->post("arr_data");
		$sql_class = $this->db->query("SELECT classid,class_name FROM sch_class WHERE 1=1 AND schoolid='".$get_data['schoolid']."' AND schlevelid='".$get_data['schoollevel']."' AND grade_levelid='".$get_data['gradelavel']."'");
		$opt_class = "<option value=''></option>";
		if($sql_class->num_rows() > 0){
			foreach($sql_class->result() as $rclass){
				$opt_class.='<option value="'.$rclass->classid.'">'.$rclass->class_name.'</option>';
			}
		}
		return $opt_class;
	}
	function mshow_student(){
		$obj_var = $this->input->post("arr_data");
		$student = $this->db->query("SELECT
										vse.student_num,
										vse.pass_mobile,
										vse.first_name,
										vse.last_name,
										vse.first_name_kh,
										vse.last_name_kh,
										vse.gender,
										vse.phone1,
										vse.class_name,
										vse.studentid,
										vse.schoolid,
										vse.`year`,
										vse.classid,
										vse.schlevelid,
										vse.rangelevelid,
										vse.feetypeid,
										vse.programid,
										vse.sch_level,
										vse.rangelevelname,
										vse.program,
										vse.sch_year,
										vse.grade_levelid,
										vse.is_pt,
										vse.from_date,
										vse.to_date,
										sgl.grade_level
										FROM
										v_student_enroment AS vse
										INNER JOIN sch_grade_level AS sgl ON vse.grade_levelid = sgl.grade_levelid
										WHERE 1=1
										AND vse.schoolid='".$obj_var['schoolid']."' 
										AND vse.schlevelid='".$obj_var['schoollevel']."' 
										AND vse.`year`='".$obj_var['yearid']."' 
										AND vse.classid='".$obj_var['classid']."'
										AND vse.rangelevelid='".$obj_var['rangelevelid']."'	
										AND vse.grade_levelid='".$obj_var['gradelavel']."'
									");
		

		$result = "";
		if($student->num_rows() >0){
			$result = $student->result();
		}
		return $result;
	}
}
?>