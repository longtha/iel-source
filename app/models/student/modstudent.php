<?php

class ModStudent extends CI_Model
{
    public function getAll($id = '')
    {

    }

    function getresponrow($respon_id)
    {
        $sql_page = "SELECT * FROM sch_student_responsible WHERE respondid=$respondid";
    }

    function saveImport()
    {
        $is_imported = false;
        if (isset($_FILES['file']) && $_FILES['file']["tmp_name"][0] != "") {
            $_xfile = $_FILES['file']["tmp_name"];
            function str($str)
            {
                return str_replace("'", "''", $str);
            }

            if ($_FILES['file']['tmp_name'] == "") {
                $error = "<font color='red'>Please Choose your Excel file!</font>";
            } else {
                $html = "";
                require_once(APPPATH . 'libraries/simplexlsx.php');
                foreach ($_xfile as $index => $value) {
                    $xlsx = new SimpleXLSX($value);
                    $_data = $xlsx->rows();
                    array_splice($_data, 0, 1);
                    $error_record_exist = "";
                    foreach ($_data as $k => $r) {
                        $_check_exist = $this->green->getTable("SELECT * FROM sch_student WHERE student_num = '{$r[0]}'");
                        $seachspance = strpos(trim(str($r[0])), " ");
                        if (count($_check_exist) > 0) {
                            $error_record_exist .= '<div class="success" style=" padding:5px; margin-top:5px;">
														<font style="color:red;">Code already exist' . $r[0] . ' exist aleady !</font><br>
														</div>';
                        } else if ($seachspance !== false) {
                            $error_record_exist .= '<div class="success" style=" padding:5px; margin-top:5px;">
													<font style="color:red;">Error Code' . str($r[0]) . ' is incorrect. Please try again !</font></div>';
                        } else {

                            $student_num = trim(str($r[0]));
                            $first_name = trim(str($r[1]));
                            $last_name = trim(str($r[2]));
                            $first_name_kh = trim(str($r[3]));
                            $last_name_kh = trim(str($r[4]));
                            $dob = trim(str($r[6]));
                            $classid = trim(str($r[7]));

                            $nationality = trim(str($r[8]));
                            $religion = trim(str($r[9]));
                            $province = trim(str($r[10]));
                            $district = trim(str($r[11]));
                            $commune = trim(str($r[12]));

                            $village = trim(str($r[13]));
                            $zoon = trim(str($r[14]));
                            $permanent_adr = trim(str($r[15]));

                            $from_school = trim(str($r[16]));
                            $measure = trim(str($r[17]));

                            $boarding_school = trim(str($r[18]));
                            $boarding_reason = trim(str($r[19]));
                            $leave_school = trim(str($r[20]));
                            $leave_reason = trim(str($r[21]));
                            $family_revenue = trim(str($r[22]));
                            $comments = trim(str($r[23]));
                            $familyid = trim(str($r[24]));
                            $phone1 = trim(str($r[25]));
                            $email = trim(str($r[26]));
                            $login_username = trim(str($r[27]));
                            $SQL_DM = "INSERT INTO sch_student";

                            $SQL = " SET student_num='" . $student_num . "',

											first_name='" . $first_name . "',
											last_name='" . $last_name . "',
											last_name_kh='" . $last_name_kh . "',
											first_name_kh='" . $first_name_kh . "',
											dob='" . $dob . "',
											classid='" . $classid . "',
											nationality='" . $nationality . "',
											religion='" . $religion . "',
											province='" . $province . "',
											commune='" . $commune . "',
											district='" . $district . "',
											village='" . $village . "',
											zoon='" . $zoon . "',
											permanent_adr='" . $permanent_adr . "',
											from_school='" . $from_school . "',
											measure='" . $measure . "',
											boarding_school='" . $boarding_school . "',
											boarding_reason='" . $boarding_reason . "',
											leave_school='" . $leave_school . "',
											leave_reason='" . $leave_reason . "',
											family_revenue='" . $family_revenue . "',
											comments='" . $comments . "',
											familyid='" . $familyid . "',
											phone1='" . $phone1 . "',
											`email`='" . $email . "',
											login_username='" . $login_username . "'
										";
                            $counts = $this->green->runSQL($SQL_DM . $SQL);
                            $is_imported = true;

                        }
                    }
                }
            }
        }
        return $is_imported;
    }

    function saveImportenroll()
    {
        $is_imported = false;
        if (isset($_FILES['file']) && $_FILES['file']["tmp_name"][0] != "") {
            $_xfile = $_FILES['file']["tmp_name"];
            function str($str)
            {
                return str_replace("'", "''", $str);
            }

            if ($_FILES['file']['tmp_name'] == "") {
                $error = "<font color='red'>Please Choose your Excel file!</font>";
            } else {
                $html = "";
                require_once(APPPATH . 'libraries/simplexlsx.php');
                foreach ($_xfile as $index => $value) {
                    $xlsx = new SimpleXLSX($value);
                    $_data = $xlsx->rows();
                    array_splice($_data, 0, 1);
                    $error_record_exist = "";
                    foreach ($_data as $k => $r) {
                        $_check_exist = $this->green->getTable("SELECT * FROM sch_student_enrollment WHERE studentid = '{$r[0]}' AND year = '{$r[2]}' AND classid = '{$r[3]}'");
                        $seachspance = strpos(trim(str($r[0])), " ");
                        if (count($_check_exist) > 0) {
                            $error_record_exist .= '<div class="success" style=" padding:5px; margin-top:5px;">
														<font style="color:red;">Code already exist' . $r[0] . ' exist aleady !</font><br>
														</div>';
                        } else if ($seachspance !== false) {
                            $error_record_exist .= '<div class="success" style=" padding:5px; margin-top:5px;">
													<font style="color:red;">Error Code' . str($r[0]) . ' is incorrect. Please try again !</font></div>';
                        } else {

                            $studentid = trim(str($r[0]));
                            $schoolid = trim(str($r[1]));
                            $year = trim(str($r[2]));
                            $classid = trim(str($r[3]));
                            $enroll_date = trim(str($r[4]));
                            $SQL_DM = "INSERT INTO sch_student_enrollment";

                            $SQL = " SET studentid='" . $studentid . "',
											schoolid='" . $schoolid . "',
											year='" . $year . "',
											classid='" . $classid . "',
											enroll_date='" . $enroll_date . "'
										";
                            $counts = $this->green->runSQL($SQL_DM . $SQL);
                            $is_imported = true;

                        }
                    }
                }
            }
        }
        return $is_imported;
    }

    // school level =======
    function saveteacherschl($schlevelid, $userid){
        $data = array('userid' => $userid, 'schlevelid' => $schlevelid);
        $this->db->insert('sch_user_schlevel', $data);
    }

    function save($studentid = '')
    {   
    	$id_year = $this->input->post('id_year');
        $aut_save = 0;
    	if($studentid ==""){
            $student_in_num = $this->input->post('id_number');
            $is_change = $this->input->post('is_change');
            if($is_change !=0){
                if($student_in_num !=""){
                    $student_num = $id_year.'-'.$student_in_num;
                }else{
                    $getAutoid = $this->getAutoid();
                    $student_num = $id_year.'-'.$getAutoid;
                }
            }else{
                $getAutoid = $this->getAutoid();
                $student_num = $id_year.'-'.$getAutoid;
            }

             $getvalidatstudid = $this->getvalidatstudid($student_num,$studentid="");
             if($getvalidatstudid>0){
                $aut_save =1;
             }else{
                 $aut_save =0;
             }
    	}else{
    		$student_num = $this->input->post('id_number');
    	}

        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $last_name_kh = $this->input->post('last_name_kh');
        $first_name_kh = $this->input->post('first_name_kh');
        $first_name_ch = $this->input->post('first_name_ch');
        $last_name_ch = $this->input->post('last_name_ch');
        $dob = $this->green->formatSQLDate($this->input->post('dob'));
        $register_date = $this->green->formatSQLDate($this->input->post('register_date'));
        $nationality = $this->input->post('nationality');
        $religion = $this->input->post('religion');
        $province = $this->input->post('province');
        $commune = $this->input->post('commune');
        $district = $this->input->post('district');
        $village = $this->input->post('village');
        $street = $this->input->post('street');
        $home_no = $this->input->post('home_no');
        $province_eng = $this->input->post('province_eng');
        $commune_eng = $this->input->post('commune_eng');
        $district_eng = $this->input->post('district_eng');
        $village_eng = $this->input->post('village_eng');
        $street_eng = $this->input->post('street_eng');
        $home_no_eng = $this->input->post('home_no_eng');
        $dob_province = $this->input->post('dob_province');
        $dob_commune = $this->input->post('dob_commune');
        $dob_district = $this->input->post('dob_district');
        $dob_village = $this->input->post('dob_village');
        $dob_street = $this->input->post('dob_street');
        $dob_home_no = $this->input->post('dob_home_no');
        $dob_province_eng = $this->input->post('dob_province_eng');
        $dob_commune_eng = $this->input->post('dob_commune_eng');
        $dob_district_eng = $this->input->post('dob_district_eng');
        $dob_village_eng = $this->input->post('dob_village_eng');
        $dob_street_eng = $this->input->post('dob_street_eng');
        $dob_home_no_eng = $this->input->post('dob_home_no_eng');
        $phone1 = $this->input->post('phone1');
        $email = $this->input->post('email');
        $gender = $this->input->post('gender');
        $staywith = $this->input->post('stu_relationship');
        $school_carno = $this->input->post('school_carno');
        $owner_tran_by = $this->input->post('owner_tran_by');
        $interviewby = $this->input->post('interviewby');
        $introducedby = $this->input->post('introducedby');
        $introduced = $this->input->post('introduced');

        if($introducedby=="intro_other" && $introduced !=""){
        	$introduced_by = $introduced;
        }else{
        	$introduced_by = $introducedby;
        }

        $transporttype = $this->input->post('transportation');
        $id_card_no = $this->input->post('id_card');
        $leave_school_date = $this->green->formatSQLDate($this->input->post('leave_date'));
        $leave_school = $this->input->post('leave_school');
        $leave_reason = $this->input->post('leave_reason');
        $familyid = $this->input->post('familyid');
        $registered_number = $this->input->post('registered_number');
        $registered_discount = $this->input->post('register_discount');
		$get_info_from = $this->input->post('getinfor');
        $consultant_by = $this->input->post('consultant');

        $created_by = $this->session->userdata('user_name');

        if($consultant_by ==""){
            $consultant_by = $created_by;
        }else{
            $consultant_by = $this->input->post('consultant');
        }

        //$login_username = $this->input->post('login_username');
        //$password = $this->input->post('password');
        // ///////////////////////////////////////////////////
        $guard_name = $this->input->post('guard_name');
        $guard_name_eng = $this->input->post('guard_name_eng');
        $guard_occupation = $this->input->post('guard_occupation');
        $guard_occupation_eng = $this->input->post('guard_occupation_eng');
        $guard_province = $this->input->post('guard_province');
        $guard_commune = $this->input->post('guard_commune');
        $guard_district = $this->input->post('guard_district');
        $guard_village = $this->input->post('guard_village');
        $guard_street = $this->input->post('guard_street');
        $guard_home = $this->input->post('guard_home');
        $guard_tel1 = $this->input->post('guard_tel1');
        $guard_tel2 = $this->input->post('guard_tel2');
        $guard_email = $this->input->post('guard_email');
        $guard_relationship = $this->input->post('stu_relationship');
        /////////////////////////////////////////////////////
		$schoolid = $this->session->userdata('schoolid');

        $created_date = date('Y-m-d H:i:s');

        $last_modified_by = $this->session->userdata('user_name');
        $last_modified_date = date('Y-m-d H:i:s');

        $SQL_WHERE = " ,created_date='" . $created_date . "', created_by='" . $created_by . "'";
        $SQL_DM = "";

        // users =======
        $userid = $this->input->post("userid");
        $txtu_name = $this->input->post("txtu_name");
        $txtpwd = $this->input->post("txtpwd");
        $cborole = $this->input->post("cborole");
        $schlevel = $this->input->post("cboschlevel");
        $dashboard = $this->input->post("dashboard");
        $defpage = $this->input->post("defpage");
        $year = date('Y');
        $modified_date = date("Y-m-d H:i:s");
        $midified_by = $this->session->userdata('user_name');
        if($cborole == 1)
            $admin = 1;
        else
            $admin = 0;
        $stu = 'stu';


    if($aut_save==0){
        // HSC: Force to set default password is iel007 when it's empty
        // 09/10/2017
        if ($txtpwd == "") {
            $txtpwd = "iel007";
        }
                if ($studentid != "") {
                    $SQL_DM = "UPDATE sch_student ";
                    $SQL_WHERE = " ,last_modified_by='" . $last_modified_by . "', last_modified_date='" . $last_modified_date . "'  WHERE studentid = '" . $studentid . "'";

                	$res_event = " , 'last_modified_by'=>".$last_modified_by.", 'last_modified_date' =>".$last_modified_date;
                	$where_is_update = "1";


                    // user ==========
                    if($txtu_name != '' && $txtpwd != ''){
                        if($userid - 0 > 0){// userid > 0; => update ===
                            $data = array('first_name' => $first_name,
                                            'last_name' => $last_name,
                                            'user_name' => $txtu_name,
                                            'password' => md5($txtpwd),
                                            'roleid' => $cborole,
                                            'schoolid' => '1',
                                            'def_dashboard' => $dashboard,
                                            'modified_date' => $modified_date,
                                            'modified_by' => $midified_by,
                                            'year' => $year,
                                            'def_open_page' => $defpage,
                                            'is_admin' => $admin,
                                            'is_active' => '1',
                                            'match_con_posid' => $stu
                                        );
                            $u_row = $this->getuserrow_($studentid);

                            // HSC 09/10/2017
                            /*
                            if($u_row['password'] != $txtpwd){

                            }else{
                                unset($data['password']);
                            }
                            */

                            // HSC
                            if ($this->input->post('chkReset') === FALSE) {
                                // Unchecked: Don't chage password
                                unset($data['password']);
                            }

                            $this->db->update("sch_user", $data, array("userid" => $userid));

                            // school level ===
                            $this->db->delete("sch_user_schlevel", array("userid" => $userid));
                            if($schlevel){
                                foreach ($schlevel as $schlevelid) {
                                    $this->saveteacherschl($schlevelid, $userid);
                                }
                            }

                        }
                        else{// userid = 0; => insert ===
                            // user ======
                            $data = array(
                                        'first_name' => $first_name,
                                        'last_name' => $last_name,
                                        'user_name' => $txtu_name,
                                        'password' => md5($txtpwd),
                                        'roleid' => $cborole,
                                        'schoolid' => '1',
                                        'def_dashboard' => $dashboard,
                                        'created_date' => $created_date,
                                        'created_by' => $created_by,
                                        'year' => $year,
                                        'def_open_page' => $defpage,
                                        'is_admin' => $admin,
                                        'is_active' => '1',
                                        'emp_id' => $studentid,
                                        'match_con_posid' => $stu
                                    );
                            $this->db->insert("sch_user", $data);
                            $userid_last = $this->db->insert_id();

                            // school level ====
                            $this->db->delete("sch_user_schlevel", array("userid" => $userid_last));
                            if($schlevel){
                                foreach ($schlevel as $schlevelid) {
                                    $this->saveteacherschl($schlevelid, $userid_last);
                                }
                            }
                        }
                    }


                } else {
                    $SQL_DM = "INSERT INTO sch_student";

                    $where_is_update = "0";
                    $res_event = ", 'created_date' =>".$created_date.", 'created_by' =>".$created_by;
                }


                $SQL = " SET student_num='" . $student_num . "',
                  pass_mobile = '" . md5($txtpwd) . "',
        					first_name='" . $first_name . "',
        					last_name='" . $last_name . "',
        					last_name_kh='" . $last_name_kh . "',
        					first_name_kh='" . $first_name_kh . "',
        					last_name_ch='" . $last_name_ch . "',
        					first_name_ch='" . $first_name_ch . "',
        					dob='" . $dob . "',
        					id_card_no='" . $id_card_no . "',
        					register_date='" . $register_date . "',
        					nationality='" . $nationality . "',
        					religion='" . $religion . "',
        					gender='" . $gender . "',
        					province='" . $this->db->escape_str($province) . "',
        					commune='" . $this->db->escape_str($commune) . "',
        					district='" . $this->db->escape_str($district) . "',
        					village='" . $this->db->escape_str($village) . "',
        					home_no='" . $this->db->escape_str($home_no) . "',
                            province_eng='" . $this->db->escape_str($province_eng) . "',
                            commune_eng='" . $this->db->escape_str($commune_eng) . "',
                            district_eng='" . $this->db->escape_str($district_eng) . "',
                            village_eng='" . $this->db->escape_str($village_eng) . "',
                            home_no_eng='" . $this->db->escape_str($home_no_eng) . "',
                            street='" . $this->db->escape_str($street) . "',
                            dob_province='" . $this->db->escape_str($dob_province) . "',
                            dob_commune='" . $this->db->escape_str($dob_commune) . "',
                            dob_district='" . $this->db->escape_str($dob_district) . "',
                            dob_village='" . $this->db->escape_str($dob_village) . "',
                            dob_home_no='" . $this->db->escape_str($dob_home_no) . "',
                            dob_province_eng='" . $this->db->escape_str($dob_province_eng) . "',
                            dob_commune_eng='" . $this->db->escape_str($dob_commune_eng) . "',
                            dob_district_eng='" . $this->db->escape_str($dob_district_eng) . "',
                            dob_village_eng='" . $this->db->escape_str($dob_village_eng) . "',
                            dob_home_no_eng='" . $this->db->escape_str($dob_home_no_eng) . "',
                            street_eng='" . $this->db->escape_str($street_eng) . "',
        					phone1='" . $phone1 . "',
        					`email`='" . $email . "',
        					staywith ='" . $staywith . "',
        					interview_by='".$interviewby."',
        					introduced_by='".$introduced_by."',
        					transporttype='".$transporttype."',
        					leave_school='" .$leave_school . "',
        					leave_reason='" .$this->db->escape_str($leave_reason) . "',
        					familyid='" .$familyid . "',
        					leave_sch_date='".$leave_school_date. "',
        					is_sch_car_no='".$school_carno . "',
        					is_owner_by='".$owner_tran_by . "',
        					registered_number='" . $registered_number . "',
        					registered_discount='" . $registered_discount . "',
        					consultant_by='" . $consultant_by . "',
        					get_info_from='" . $get_info_from . "',
                  is_active = '1'";



                $this->green->runSQL($SQL_DM . $SQL . $SQL_WHERE);

                if ($studentid == '') {        	//============================GET AUTO=============
                    $studentid = $this->db->insert_id();

                    // user ======
                    if($txtu_name != '' && $txtpwd != ''){
                        $data = array(
                                    'first_name' => $first_name,
                                    'last_name' => $last_name,
                                    'user_name' => $txtu_name,
                                    'password' => md5($txtpwd),
                                    'roleid' => $cborole,
                                    'schoolid' => '1',
                                    'def_dashboard' => $dashboard,
                                    'created_date' => $created_date,
                                    'created_by' => $created_by,
                                    'year' => $year,
                                    'def_open_page' => $defpage,
                                    'is_admin' => $admin,
                                    'is_active' => '1',
                                    'emp_id' => $studentid,
                                    'match_con_posid' => $stu
                                );
                        $this->db->insert("sch_user", $data);
                        $userid_last = $this->db->insert_id();

                        // school level ====
                        $this->db->delete("sch_user_schlevel", array("userid" => $userid_last));
                        if($schlevel){
                            foreach ($schlevel as $schlevelid) {
                                $this->saveteacherschl($schlevelid, $userid_last);
                            }
                        }
                    }


                } ///END ($studentid !="")
        	    ////////////////////////// sch_student_responsible \\\\\\\\\\\\\\\\\\\\\\\\

                $data = array(
                    'studentid' => $studentid,
                    'respon_name' => $guard_name,
                    'respon_name_eng' => $guard_name_eng,
                    'relationship' => $guard_relationship,
                    'occupation' => $guard_occupation,
                    'occupation_eng' => $guard_occupation_eng,
                    'tel1' => $guard_tel1,
                    'tel2' => $guard_tel2,
                    'email'=> $guard_email,
                    'home_no' => $guard_home,
                    'street' => $guard_street,
                    'village' => $guard_village,
                    'commune' => $guard_commune,
                    'district' => $guard_district,
                    'province' => $guard_province
        	    );

        	    if($where_is_update =='1'){
        	    	$row_respon = $this->getresponstudent($studentid);
        	    	if(count($row_respon)>0){
        				$this->db->where('studentid', $studentid);
        				$this->db->update('sch_student_responsible',$data);
        	    	}else{
        	    		$this->db->insert('sch_student_responsible',$data);
        	    	}

        	    }else{
        	    	$this->db->insert('sch_student_responsible',$data);
        	    }


        	        ///////////////////// ENROLEMENT REGISTER \\\\\\\\\\\\\\\\\\\\
        		$enrollid = $this->input->post('enrollid');
        		$allstucture = $this->input->post('allstucture');
                $sch_programid = $this->input->post('sch_programid');
                $sch_levelid = $this->input->post('sch_levelid');
                $sch_yearid = $this->input->post('sch_yearid');
                $sch_rangelevelid = $this->input->post('sch_rangelevelid');
                $sch_classid = $this->input->post('sch_classid');
                $is_new = $this->input->post('is_new');

                if(count($allstucture) >0){
        	        for($j=0;$j<count($allstucture);$j++) {

                        if($is_new[$j]==1){
            	        	$data2=array(
            								'studentid'=>$studentid,
            								'schoolid'=>$schoolid,
            								'programid'=>$sch_programid[$j],
            								'schlevelid'=>$sch_levelid[$j],
            								'year'=>$sch_yearid[$j],
            								'rangelevelid'=>$sch_rangelevelid[$j],
            								'classid'=>$sch_classid[$j],
            								'enroll_date'=>$created_date,
            								'type_of_enroll' => '1',
            								'feetypeid' =>'5',
                    						'is_rollover' => '0'
            					);
            	        	$this->db->insert('sch_student_enrollment', $data2);
                        }
        	        }
        	    }
        	        /////////////////////////////// Academic Background\\\\\\\\\\\\\\\\\\\\\\\\\\
        	       	$acadtype1 = "General Education";

        			$edu_primary = $this->input->post('edu_primary');
        			$primary_sch_name = $this->input->post('primary_sch_name');
        			$primary_sch_ayear = $this->input->post('primary_sch_ayear');

        			$edu_Lower = $this->input->post('edu_Lower');
        			$lower_sch_name = $this->input->post('lower_sch_name');
        			$lower_sch_ayear = $this->input->post('lower_sch_ayear');

        			$edu_upper = $this->input->post('edu_upper');
        			$upper_sch_name = $this->input->post('upper_sch_name');
        			$upper_sch_ayear = $this->input->post('upper_sch_ayear');

        			$acadtype2 = "General English";
        			$general_currentlevel = $this->input->post('general_currentlevel');
        			$general_sch_name = $this->input->post('general_sch_name');
        			$general_sch_ayear = $this->input->post('general_sch_ayear');

                	$data_acadtype11 = array(
                	'acadtypeid' => 1,
                    'educationid' => 1,
                    'acadtype' => $acadtype1,
                    'education' => $edu_primary,
                    'school' => $primary_sch_name,
                    'year' => $primary_sch_ayear,
                    'studentid' => $studentid
                	);
        			if($where_is_update=='1'){
        		    	$this->db->where('studentid', $studentid);
        				$this->db->delete('sch_stud_academic_background');
        		    }

                    $this->db->insert('sch_stud_academic_background', $data_acadtype11);

                	$data_acadtype12 = array(
                	'acadtypeid' => 1,
                    'educationid' => 2,
                    'acadtype' => $acadtype1,
                    'education' => $edu_Lower,
                    'school' => $lower_sch_name,
                    'year' => $lower_sch_ayear,
                    'studentid' => $studentid
                	);

        			$this->db->insert('sch_stud_academic_background', $data_acadtype12);

                	$data_acadtype13 = array(
                	'acadtypeid' => 1,
                    'educationid' => 3,
                    'acadtype' => $acadtype1,
                    'education' => $edu_upper,
                    'school' => $upper_sch_name,
                    'year' => $upper_sch_ayear,
                    'studentid' => $studentid
                	);

        			$this->db->insert('sch_stud_academic_background', $data_acadtype13);

                	$data_acadtype2 = array(
                	'acadtypeid' => 2,
                    'educationid' => 4,
                    'acadtype' => $acadtype2,
                    'education' => $general_currentlevel,
                    'school' => $general_sch_name,
                    'year' => $general_sch_ayear,
                    'studentid' => $studentid
                	);

                    $this->db->insert('sch_stud_academic_background', $data_acadtype2);

                	$arrlink = $this->input->post('link_id');//fetch student link data

                for ($k = 0; $k < count($arrlink); $k++) {
                    $std_id = $this->input->post('link_id');
                    $this->savestdlink($studentid, $std_id[$k]);
                }

                $savestudent = $studentid;
        }else{
            $savestudent =0;
        }//++++++++++++++++++++++++ end of read member / respondsible / student link from form+++++++++++++++++++++++
        return  $savestudent;
    }
    
    function booksave($data){
        $this->db->insert('sch_relationship_book', $data);
    }
    
    function bookupdate($id, $data){        
        $this->db->where('id', $id);
        $this->db->update('sch_relationship_book', $data);       
    }

    function saveenrollment($studentid, $year)
    {
        $schoolid = $this->session->userdata('schoolid');
        $enroll_date = date('Y-m-d H:i:s');
        $year = $year;
        $type_of_enroll = 1;
        $is_rollover = 0;
        $data = array(
            'studentid' => $studentid,
            'schoolid' => $schoolid,
            'year' => $year,
            'classid' => $this->input->post('classid'),
            'enroll_date' => $enroll_date,
            'type_of_enroll' => $type_of_enroll,
            'is_rollover' => $is_rollover
        );
        $this->db->insert('sch_student_enrollment', $data);
    }

    function updatestdmember($studentid, $last_name, $first_name, $last_name_kh, $first_name_kh, $dob, $familyid = '')
    {
        $sex = 'F';
        $data = array(
            'studentid' => $studentid,
            'last_name' => $last_name,
            'first_name' => $first_name,
            'last_name_kh' => $last_name_kh,
            'first_name_kh' => $first_name_kh,
            'dob' => $dob,
            'is_active' => 1
        );
        if ($familyid != '') {
            $data2 = array("familyid" => $familyid,
                "sex" => $sex);
            $this->db->insert('sch_student_member', array_merge($data, $data2));
        } else {
            $this->db->where('studentid', $studentid);
            $this->db->update('sch_student_member', $data);
        }

    }

    function updatestdinmember($memberid, $studentid)
    {
        $this->db->set('studentid', $studentid);
        $this->db->where('memid', $memberid);
        $this->db->update('sch_student_member');
    }

    function updateenrollment($studentid, $year)
    {
        $schoolid = 1;
        $enroll_date = date('Y-m-d H:i:s');
        $year = $year;
        $type_of_enroll = 1;
        $is_rollover = 0;
        $data = array(
            'studentid' => $studentid,
            'schoolid' => $schoolid,
            'year' => $year,
            'classid' => $this->input->post('classid'),
            'enroll_date' => $enroll_date,
            'type_of_enroll' => $type_of_enroll,
            'is_rollover' => $is_rollover,
        );
        $this->db->where('studentid', $studentid)
            ->where('schoolid', $schoolid)
            ->where('year', $year)->update('sch_student_enrollment', $data);
    }

    function clearmember($studentid)
    {
        $this->db->where('studentid', $studentid);
        $this->db->delete('sch_student_mem_detail');
    }

    function clearstdlink($studentid)
    {
        $this->db->where('studentid', $studentid);
        $this->db->delete('sch_student_link_othstudent');
    }

    function moeys($studentid, $classid, $moeys, $yearid)
    {
        $classrow = $this->db->where('classid', $classid)->get('sch_class')->row();
        $count = $this->green->getValue("SELECT count(*) as count
									FROM sch_student_moeysid
									WHERE studentid='$studentid'
									AND schlevelid='$classrow->schlevelid'
									AND yearid='$yearid'
									");
        $data = array('studentid' => $studentid,
            'schlevelid' => $classrow->schlevelid,
            'yearid' => $yearid,
            'moeys_id' => $moeys);
        if ($count > 0)
            $this->db->where('stdid', $studentid)->update('sch_student_moeysid', $data);
        else
            $this->db->insert('sch_student_moeysid', $data);
    }

    function getmemberow($memberid)
    {
        $this->db->where('memid', $memberid);
        $query = $this->db->get('sch_student_member');
        return $query->row();
    }

    function savemember($stu_id, $memid, $relation, $g_level)
    {
        $data = array(
            'studentid' => $stu_id,
            'memberid' => $memid,
            'relationship' => $relation,
            'grade_level' => $g_level
        );
        if ($memid != 0)
            $this->db->insert('sch_student_mem_detail', $data);
    }

    function savestdlink($studentid, $stdlink_id)
    {
        $data = array(
            'studentid' => $studentid,
            'link_studentid' => $stdlink_id,
        );
        if ($stdlink_id != 0)
            $this->db->insert('sch_student_link_othstudent', $data);
    }

    function getmoeys($studentid, $schlevelid, $yearid)
    {
        $mo = '';
        $mo = $this->db->where('studentid', $studentid)
            ->where('schlevelid', $schlevelid)
            ->where('yearid', $yearid)
            ->get('sch_student_moeysid')->row();
        return $mo;
    }

    function getsponsor($studentid)
    {
        return $this->db->select('sd.sponsorid,sd.studentid,sd.stdspid,sd.start_sp_date,sd.end_sp_date,fs.last_name,fs.first_name')
            ->from('sch_student_sponsor_detail sd')
            ->join('sch_family_sponsor fs', 'sd.sponsorid=fs.sponsorid', 'inner')
            ->where('sd.studentid', $studentid)->get()->result();
    }

    function saverespon($stu_id, $res_id, $relat)
    {
        $data = array(
            'studentid' => $stu_id,
            'respondid' => $res_id,
            'relationship' => $relat,
            'created_date' => date('Y-m-d H:i:s'),
            'created_by' => 1
        );
        if ($res_id != 0)
            $this->db->insert('sch_student_resp_detail', $data);
    }

    function clearrespon($studentid)
    {
        $this->db->where('studentid', $studentid);
        $this->db->delete('sch_student_resp_detail');
    }

    function getrelation($studentid, $memberid)
    {
        $this->db->where('memberid', $memberid);
        $this->db->where('studentid', $studentid);
        $select_relat = $this->db->get('sch_student_mem_detail');
        if ($select_relat->num_rows() > 0) {
            $row = $select_relat->row();
            return $row->relationship;
        }
        return '';
    }

    function getfather($studentid)
    {
        $relat = "father's child";
        $query = $this->db->query("SELECT rp.respondid,rp.last_name,rp.first_name,rp.last_name_kh,rp.first_name_kh,DATE_FORMAT(rp.dob,'%d/%m/%Y') as dob,rp.occupation,rp.revenue,rpd.relationship FROM sch_student_responsible rp
					INNER JOIN sch_student_resp_detail rpd
					ON(rp.respondid=rpd.respondid)
					where rpd.studentid='$studentid'
					AND rpd.relationship='father`s child'
					");
        return $query->row_array();
    }

    function getmother($studentid)
    {
        $relat = "father's child";
        $this->db->select('*');
        $this->db->from('sch_student_responsible rp');
        $this->db->join('sch_student_resp_detail rpd', 'rp.respondid=rpd.respondid', 'right');
        $this->db->where('rpd.studentid', $studentid);
        $this->db->where('rpd.relationship', 'mother`s child');
        $query = $this->db->get();
        return $query->row_array();
    }

    function getresbystudent($studentid)
    {
        $this->db->select('*');
        $this->db->from('sch_student_responsible stdr');
        $this->db->join('sch_student_resp_detail stdrd', 'stdr.respondid=stdrd.respondid', 'inner');
        $this->db->where('studentid', $studentid);
        $query = $this->db->get();
        return $query->result();
    }

    function getstdlinkrowbystd($studentid)
    {
        $this->db->select('s.studentid,s.last_name,s.first_name,s.last_name_kh,c.class_name');
        $this->db->from('sch_student s');
        $this->db->join('sch_class c', 's.classid=c.classid', 'left');
        $this->db->join('sch_student_link_othstudent lstd', 's.studentid=lstd.link_studentid', 'inner');
        $this->db->where('lstd.studentid', $studentid);
        $query = $this->db->get();
        return $query->result();
    }

    function getmembystudent($studentid)
    {
        $this->db->select('*');
        $this->db->from('sch_student_member stdm');
        $this->db->join('sch_student_mem_detail stdmd', 'stdm.memid=stdmd.memberid', 'inner');
        $this->db->where('studentid', $studentid);
        $query = $this->db->get();
        return $query->result();
    }

    function getvalidateuser($username)
    {
        $this->db->select('count(*)');
        $this->db->from('sch_user');
        $this->db->where('user_name', $username);
        return $this->db->count_all_results();
    }

    function getvalidatstudid($student_num="",$studentid="")
    {

        $this->db->select('count(*)');
        $this->db->from('sch_student');
        $this->db->where('student_num', $student_num);
        $this->db->where('is_active', '1');
        if($studentid !=""){
            $is_student = "";
            $this->db->where('studentid <>', $studentid);
        }
        return $this->db->count_all_results();
    }

    function getstudentrow($studentid)
    {
        $this->db->where('studentid', $studentid);
        $query = $this->db->get('sch_student');
        return $query->row_array();
    }

    function getuserrow_($studentid){
        // HSC: 06/Oct/2017
        // Mark: remove u.`password`,
        $row_user = $this->db->query("SELECT DISTINCT
                                                r.role,
                                                r.roleid,
                                                u.userid,
                                                u.user_name,
                                                u.email,
                                                u.last_visit,
                                                u.last_visit_ip,
                                                u.created_date,
                                                u.created_by,
                                                u.modified_by,
                                                u.modified_date,
                                                u.roleid,
                                                u.last_name,
                                                u.first_name,
                                                u.is_admin,
                                                u.schoolid,
                                                u.`year`,
                                                u.is_active,
                                                u.def_sch_level,
                                                u.def_dashboard,
                                                u.def_open_page,
                                                u.emp_id,
                                                u.match_con_posid
                                            FROM
                                                sch_user AS u
                                            INNER JOIN sch_z_role AS r ON u.roleid = r.roleid
                                            WHERE
                                                u.is_active = '1'
                                            AND u.emp_id = '{$studentid}'
                                            AND u.match_con_posid = 'stu' ")->row_array();
        return $row_user;
    }

    function previewstd($studentid = '', $yearid, $classid = '')
    {
        $where = '';
        if ($studentid != '')
            $where .= " AND std.studentid ='$studentid'";
        if ($classid != '')
            $where .= " AND std.classid='$classid'";
        $query = $this->db->query("SELECT
        								*
									FROM v_student_profile AS std
									WHERE 1=1 {$where}");

        //$query=$this->db->get($query);
        if ($studentid != '')
            return $query->row_array();
        else
            return $query->result_array();
    }
    
    function getbook($studentid , $yearid){
        $query = $this->db->query("SELECT b.*,m.month_kh,w.week_kh FROM sch_relationship_book AS b
                                    LEFT JOIN  sch_zz_month AS m ON(m.id = b.month) 
                                    LEFT JOIN  sch_zz_week AS w ON(w.id = b.week)
                                    WHERE b.is_active = 1 AND b.id_student = $studentid AND b.id_year = $yearid");
        return $query->result();
    }
    
    function getbookbyid($studentid , $yearid,$idtable){
        $query = $this->db->query("SELECT b.*,m.id AS monthid,w.id AS weekid FROM sch_relationship_book AS b
                                    LEFT JOIN  sch_zz_month AS m ON(m.id = b.month)
                                    LEFT JOIN  sch_zz_week AS w ON(w.id = b.week)
                                    WHERE b.is_active = 1 AND b.id_student = $studentid AND b.id_year = $yearid AND b.id = $idtable");
        return $query->row_array();
    }
    
    function deletebook($id){      
        $this->db->where('id', $id);
        $this->db->delete('sch_relationship_book');
        $del_re = 1;       
        return $del_re;
    }
    
    function getbookbymonth($studentid , $yearid,$month){
        $query = $this->db->query("SELECT b.*,m.month_kh,w.week_kh FROM sch_relationship_book AS b
                                    LEFT JOIN  sch_zz_month AS m ON(m.id = b.month)
                                    LEFT JOIN  sch_zz_week AS w ON(w.id = b.week)
                                    WHERE b.is_active = 1 AND b.id_student = $studentid AND b.id_year = $yearid AND b.month = $month ");
        return $query->result();
    }
    
    function getmonth(){        
        $query = $this->db->query("SELECT * FROM sch_zz_month WHERE 1=1");           
        return $query->result();
    }
    
    function getmonthfilter($studentid , $yearid){
        $query = $this->db->query("SELECT m.* FROM sch_zz_month AS m  
                                    LEFT JOIN  sch_relationship_book AS b ON(m.id = b.month)
                                    WHERE b.is_active = 1 AND b.id_student = $studentid AND b.id_year = $yearid GROUP BY m.month_kh ORDER BY m.id");
        return $query->result();
    }
    
    function getweek(){
        $query = $this->db->query("SELECT * FROM sch_zz_week WHERE 1=1");
        return $query->result();
    }

    function saveuser($userlogin = '')
    {
        $schoolid = 1;
        $creat_date = date('Y-m-d H:i:s');
        $year = date('Y');
        $f_name = $this->input->post('first_name');
        $l_name = $this->input->post('last_name');
        $username = $this->input->post('login_username');
        $schoolid = 1;
        $pwd = md5($this->input->post('password'));
        if ($userlogin != '') {
            $u_row = $this->getuserrow($username);
            $pass = $this->input->post('password');
            if ($u_row->password != $pass) {
                $data = array(
                    'first_name' => $f_name,
                    'last_name' => $l_name,
                    'password' => $pwd,
                    'roleid' => 5,
                    'created_date' => $creat_date,
                    'year' => $year,
                    'is_active' => 1
                );
            } else {
                $data = array(
                    'first_name' => $f_name,
                    'last_name' => $l_name,
                    'roleid' => 5,
                    'created_date' => $creat_date,
                    'year' => $year,
                    'is_active' => 1
                );
            }
        } else {
            $data = array(
                'first_name' => $f_name,
                'last_name' => $l_name,
                'user_name' => $username,
                'password' => $pwd,
                'schoolid' => $schoolid,
                'roleid' => 5,
                'created_date' => $creat_date,
                'year' => $year,
                'is_active' => 1
            );
        }
        if ($userlogin != '') {
            $this->db->where('user_name', $userlogin);
            $this->db->update('sch_user', $data);
        } else {
            $this->db->insert('sch_user', $data);
        }
    }

    function getuserrow($user_name)
    {
        $this->db->where('user_name', $user_name);
        $query = $this->db->get('sch_user u');
        return $query->row();
    }

    function validatestudent($student_first, $student_lastname, $student_dob)
    {
        $this->db->select('count(*)');
        $this->db->from('sch_student');
        $this->db->where('is_active','1');
        $this->db->where('first_name', $student_first);
        $this->db->where('last_name', $student_lastname);
        $this->db->where('dob', $student_dob);
        return $this->db->count_all_results();
    }

    function validatestudentupdate($studentid,$student_first, $student_lastname, $student_dob)
    {
        $this->db->select('count(*)');
        $this->db->from('sch_student');
        $this->db->where('is_active', '1');
        $this->db->where('first_name', $student_first);
        $this->db->where('last_name', $student_lastname);
        $this->db->where('dob', $student_dob);
        $this->db->where_not_in('studentid', $studentid);

        return $this->db->count_all_results();
    }

    function getprogramlevel()
    {
        $query = $this->db->query('SELECT
				sch_program.programid,
				sch_program.program,
				sch_level.schlevelid,
				sch_level.sch_level
				FROM
				sch_school_program AS sch_program
				INNER JOIN sch_school_level AS sch_level ON sch_program.programid = sch_level.programid
				ORDER BY sch_program.programid');
        return $query->result();
    }

	function getschoolyear()
	{
		$query=$this->db->get('sch_school_year');
		return $query->result();
	}
    function getfamilyrow($familyid="")
    {
        $this->db->where('familyid', $familyid);
        $query = $this->db->get('sch_family');
        return $query->row();
    }

    function getmaxid()
    {
        $this->db->select_max('studentid', 'max');
        $this->db->from('sch_student');
        $query = $this->db->get();
        return $query->row()->max + 1;
    }
    function displayAutoid()
    {
        $this->db->where('typeid', '30');
        $this->db->from('sch_z_systype');
        $query = $this->db->get();
        return $query->row()->sequence;
    }
    function getAutoid()
    {
        $tran_next_id = $this->green->nextTran(30,'Student AutoID');
        return $tran_next_id;
    }

    function getresponfamily($familyid="")
    {
        $this->db->where('familyid', $familyid);
        $this->db->where('is_active', 1);
        return $this->db->get('sch_student_responsible')->result();
    }
 	function getresponstudent($studentid="")
    {
        $this->db->where('studentid', $studentid);
        $this->db->where('is_active', 1);
        return $this->db->get('sch_student_responsible')->row();
    }
    function getmemberfamily($familyid, $memberid, $studentid)
    {
        if ($studentid > 0) {
            $mem = $this->db->query("SELECT memid FROM sch_student_member WHERE studentid='$studentid'")->row_array();
            if (isset($mem['memid']))
                $memberid = $mem['memid'];
        }
        $this->db->select("*");
        $this->db->from("sch_student_member m");
        $this->db->where('m.familyid', $familyid);
        $this->db->where('m.is_active', 1);
        if ($memberid != '')
            $this->db->where('m.memid !=', $memberid);
        return $this->db->get()->result();
    }

    function searchstudent($boarding, $studentid, $full_name, $phone1, $class_id, $sort, $sort_num, $year,$schlevelid, $level, $m, $p, $ag, $promot_id)
    {
        $userid = $this->session->userdata('userid');
        $page = 0;
        $sql = '';
        if (isset($_GET['per_page'])) $page = $_GET['per_page'];

        $this->load->library('pagination');
        $config['base_url'] = site_url() . "/student/student/search?s_id=$studentid&fn=$full_name&fone=$phone1&class=$class_id&s_num=$sort_num&year=$year&m=$m&p=$p&l=$schlevelid&b=$boarding&le=$level&ag=$ag&pro=$promot_id";

        $where="";
        if($full_name!=""){
            $where.=" AND (s.last_name LIKE '%". trim($full_name) . "%'
                            OR s.first_name LIKE '%". trim($full_name) . "%'
                            OR s.fullname LIKE '%". trim($full_name) . "%'
                            OR s.last_name_kh LIKE '%". trim($full_name) . "%'
                            OR s.first_name_kh LIKE '%". trim($full_name) . "%'
                            OR s.fullname_kh LIKE '%". trim($full_name) . "%'
                        )";
        }
        if($studentid!=""){
            $where.=" AND s.student_num like '%$studentid%'";
        }
        if ($class_id != ''){
            $where.= " AND s.classid='".$class_id."' ";
        }
        if ($schlevelid != ''){
            $where.= "  AND s.schlevelid ='".$schlevelid."'";
        }
        if ($level != ''){
            $where.= "  AND s.grade_levelid='".$level."'";
        }
        
        if ($ag != '') {
            $str_age = explode("__", $ag);
            if ($str_age[0] !== "") {
                $where.= "  AND s.age >=$str_age[0]";
            }
            if ($str_age[1] !== "") {
                $where.= "  AND s.age <=$str_age[1]";
            }
        }
        // $where='';
        if($this->session->userdata('match_con_posid')=='stu'){
            $where.=" AND s.studentid='".$this->session->userdata('emp_id')."'";
        }else{
            if ($year != ''){
                $where.= "  AND s.yearid='".$year."'";
            }
        }
        $sql .= "SELECT DISTINCT
                        s.studentid,
                        s.sch_year,
                        s.familyid,
                        s.yearid,
						s.student_num,
						s.last_name,
						s.first_name,
						s.last_name_kh,
						s.first_name_kh,
						s.dob,
                        s.register_date,
						s.nationality,
						s.class_name,
						s.classid,
						s.dateofbirth,
                        s.phone1
					FROM
						v_student_profile s
					WHERE 1=1
					{$where}
                    AND s.is_active=1";

       
        $config['total_rows'] = $this->green->getTotalRow($sql);
        $config['per_page'] = $sort_num;
        $config['num_link'] = 5;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<li>';
        $config['full_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<a><u>';
        $config['cur_tag_close'] = '</u></a>';
        $this->pagination->initialize($config);
        $limi = '';
        if ($sort_num != 'all') {
            $limi = " limit " . $config['per_page'];
            if ($page > 0) {
                $limi = " limit " . $page . "," . $config['per_page'];
            }
        }

        $sql .= " $sort {$limi}";
        $query = $this->green->getTable($sql);

        return $query;
    }

    function getclass()
	{
        $this->db->order_by("class_name","asc");
		$query=$this->db->get('sch_class');
		return $query;
	}

    function getyear(){
        $userid = $this->session->userdata('userid');
        $this->db->select("*");
        $this->db->from("sch_school_year");
        $this->db->where('schlevelid in','(SELECT schlevelid FROM sch_user_schlevel WHERE userid = '.$userid.')',false);

        $query=$this->db->get();
        return $query;
    }
	function getfamaily($familyid)
	{
		$this->db->where('familyid',$familyid);
		return $this->db->get('sch_family')->row();
	}
	function getsocailinfo($familynote_type="")
    {
    	$this->db->where('familynote_type', $familynote_type);
    	return $this->db->get('sch_family_social_infor')->result();
    }
    function getstudent($studentid)
    {
    	$this->db->where("studentid",$studentid);
        return $this->db->get('sch_student')->row();
    }
    function getacademic_back($studentid="",$acadtypeid="",$educationid="")
    {
    	$this->db->where("studentid",$studentid);
    	$this->db->where("acadtypeid",$acadtypeid);
    	$this->db->where("educationid",$educationid);
        return $this->db->get('sch_stud_academic_background')->row();
    }
    function getstudents($yearid="",$classid="",$studentid="")
    {
    	if($yearid!=""){
    		$this->db->where("yearid",$yearid);
    	}
    	if($classid!=""){
    		$this->db->where("yearid",$classid);
    	}
    	if($studentid!=""){
    		$this->db->where("studentid",$studentid);
    	}
        return $this->db->get('v_student_profile')->result();

    }
    function getenrollments($studentid="")
    {
    	$student = "";
    	if($studentid !=""){
    		$student = " sch_student_enrollment.studentid='".$studentid."'";
    	}

    	$results = $this->green->getTable("SELECT DISTINCT
									sch_student_enrollment.programid,
									sch_school_program.program,
									sch_student_enrollment.schlevelid,
									sch_school_level.sch_level,
									sch_student_enrollment.`year`,
									sch_school_year.sch_year,
									sch_student_enrollment.rangelevelid,
									sch_school_rangelevel.rangelevelname,
									sch_student_enrollment.classid,
									sch_class.class_name,
                                    sch_grade_level.grade_level,
                                    sch_student_enrollment.grade_levelid,
                                    sch_student_enrollment.is_active
								FROM
									sch_student_enrollment
								INNER JOIN sch_school_program ON sch_student_enrollment.programid = sch_school_program.programid
								INNER JOIN sch_school_level ON sch_student_enrollment.schlevelid = sch_school_level.schlevelid
                                INNER JOIN sch_school_year ON sch_student_enrollment.`year` = sch_school_year.yearid
								LEFT JOIN sch_school_rangelevel ON sch_student_enrollment.rangelevelid = sch_school_rangelevel.rangelevelid
								LEFT JOIN sch_class ON sch_student_enrollment.classid = sch_class.classid
                                LEFT JOIN sch_grade_level ON sch_student_enrollment.grade_levelid = sch_grade_level.grade_levelid
								WHERE {$student}");
    	return $results;
    }
    function get_checkpayment($studentid="", $sch_programid="", $sch_levelid="", $sch_yearid="", $sch_ranglevid="", $sch_classid="")
    {
        $schoolid = $this->session->userdata('schoolid');
        $where = " sch_student_fee.studentid='".$studentid."'";
        $where .= " AND sch_student_fee.schoolid='".$schoolid."'";
        $where .= " AND sch_student_fee.programid='".$sch_programid."'";
        $where .= " AND sch_student_fee.schooleleve='".$sch_levelid."'";
        $where .= " AND sch_student_fee.acandemic='".$sch_yearid."'";
        $where .= " AND sch_student_fee.ranglev='".$sch_ranglevid."'";
        $where .= " AND sch_student_fee.classid='".$sch_classid."'";

        $results = $this->green->getValue("SELECT
                                    count(sch_student_fee.studentid) as val
                                FROM
                                    sch_student_fee
                                WHERE {$where}");
        return $results;
    }
    function delete_acadimice($studentid="", $sch_programid="", $sch_levelid="", $sch_yearid="", $sch_ranglevid="", $sch_classid="")
    {
        $schoolid = $this->session->userdata('schoolid');

        $where = " sch_student_enrollment.studentid='".$studentid."'";
        $where .= " AND sch_student_enrollment.schoolid='".$schoolid."'";
        $where .= " AND sch_student_enrollment.programid='".$sch_programid."'";
        $where .= " AND sch_student_enrollment.schlevelid='".$sch_levelid."'";
        $where .= " AND sch_student_enrollment.year='".$sch_yearid."'";
        //$where .= " AND sch_student_enrollment.rangelevelid '".$sch_ranglevid."'";
        //$where .= " AND sch_student_enrollment.classid '".$sch_classid."'";

        if($sch_ranglevid =="0"){
            $where .= " AND IFNULL(sch_student_enrollment.rangelevelid,0) = 0";
        }else{
            $where .= " AND sch_student_enrollment.rangelevelid ='".$sch_ranglevid."'";
        }
        if($sch_classid =="0"){
            $where .= " AND IFNULL(sch_student_enrollment.classid,0) = 0";
        }else{
            $where .= " AND sch_student_enrollment.classid ='".$sch_classid."'";
        }

       $this->green->runSQL("DELETE FROM sch_student_enrollment WHERE {$where}");
       //echo "DELETE FROM sch_student_enrollment WHERE {$where}";
       return $studentid;
    }
    function getcardhistory($studentid="")
    {
        if($studentid !=""){
            $WHERE = " AND card.studentid='".$studentid."'";
        }

        $results = $this->green->getTable("SELECT
                                            card.printid,
                                            card.studentid,
                                            card.card_id,
                                            card.print_date,
                                            card.print_by,
                                            acc_y.sch_year as academic_year
                                        FROM
                                            sch_print_card AS card
                                        INNER JOIN sch_school_year AS acc_y ON card.academic_year = acc_y.yearid
                                        WHERE 1=1 {$WHERE}");

        return $results;
    }
}
