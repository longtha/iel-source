<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
    class m_subject_score_entry_kgp extends CI_Model{

    	//get teacher infor
    	public function getTeacherInfo($teacher_id = "", $school_level_id, $adcademic_year_id,
                                        $grade_level_id){
            if($teacher_id != ""){
                $this->db->where('t.teacher_id', $teacher_id);   
            }

    		$this->db->select(' e.first_name,
								e.last_name,
								e.first_name_kh,
								e.last_name_kh,
								e.sex,
								t.teacher_id');
    		$this->db->from('sch_teacher_summary as t');
    		$this->db->join('sch_emp_profile as e', 'e.empid = t.teacher_id');
    		$this->db->where('t.schlevelid', $school_level_id);
    		$this->db->where('t.yearid', $adcademic_year_id);
    		$this->db->where('t.gradelevelid', $grade_level_id);
    		return $this->db->get()->result();
    	}

    	//get class by teacher
    	public function getClassByTeacher($teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id){
            
    		$sql_class = $this->db->query("SELECT 
                                                sts.classid,
                                                sch_class.class_name
                                                FROM
                                                sch_teacher_subject as sts
                                                INNER JOIN sch_class ON sts.classid = sch_class.classid
                                                WHERE 1=1 
                                                AND sts.schlevelid='".$school_level_id."'
                                                AND sts.gradelevelid='".$grade_level_id."'
                                                AND sts.yearid='".$adcademic_year_id."'
                                                -- AND sts.teacher_id='".$teacher_id."'
                                                GROUP BY sts.classid
                                                ");
    		
            return $sql_class;
    	}

        //get subject by teacher
        public function getSubjectByTeacher($subject_id = "", $class_id, $teacher_id, $adcademic_year_id,
                                            $grade_level_id, $school_level_id){
            if($subject_id != ""){
                $this->db->where('ts.subject_id', $subject_id);
            }

            $this->db->distinct();
            $this->db->select('s.subjectid, 
                                s.`subject` as subject_name, 
                                s.subject_kh, 
                                s.max_score, 
                                s.score_percence, 
                                st.subj_type_id, 
                                st.main_type as subj_main_id
                                ');
            $this->db->from('sch_teacher_subject as ts');
            $this->db->join('sch_subject as s', 's.subjectid = ts.subject_id');
            $this->db->join('sch_subject_type as st', 's.subj_type_id = st.subj_type_id');
            //$this->db->join('sch_subject_main as sm', 'sm.id = st.main_type');
            $this->db->where('ts.classid', $class_id);
            $this->db->where('ts.teacher_id', $teacher_id);
            $this->db->where('ts.yearid', $adcademic_year_id);
            $this->db->where('ts.gradelevelid', $grade_level_id);
            $this->db->where('ts.schlevelid', $school_level_id);
            return $this->db->get()->result();
        }

        //get assessment by subject
        public function getAssessmentBySubject($subject_id, $schoolid,$school_level_id,$grade_level_id){

            $this->db->select('a.assess_id,a.assess_name, a.percentag, a.max_score');
            $this->db->from('sch_subject_assessment_kgp as a');
            //$this->db->join('sch_subject_assessment_detail_kgp as ad', 'a.assess_id = ad.subj_assem_id AND a.subject_exam_id = ad.subj_examp_id');
            $this->db->where('a.subject_exam_id', $subject_id);
            $this->db->where('a.schoolid', $schoolid);
            $this->db->where('a.schoollevelid', $school_level_id);
            $this->db->where('a.grade_level_id', $grade_level_id);
            return $this->db->get()->result();
        }

        //get semester data
        public function getSemesterData($school_id, $program_id, $school_level_id, $adcademic_year_id){
            $this->db->select('se.semesterid, se.semester, se.semester_kh, se.from_date, se.to_date');
            $this->db->from('sch_school_semester as se');
            $this->db->where('se.schoolid', $school_id);
            $this->db->where('se.programid', $program_id);
            $this->db->where('se.schlevelid', $school_level_id);
            $this->db->where('se.yearid', $adcademic_year_id);
            return $this->db->get()->result();
        }

        //check student exist in table of score entry
        public function checkStudentMonthlyExsist($school_id, $program_id, $school_level_id, $grade_level_id, $adcademic_year_id, $class_id, $exam_type,$subjctid){

            $table = "";
            $fields = "";
            $where = "";
            //exam type is monthly-----------------------
            $arr_return = array();
            if($exam_type["get_exam_type"] == "monthly"){
                // $exam_monthly = $exam_type["exam_monthly"];
                // $table = 'sch_subject_score_monthly_kgp as sc';
                // $fields = 'sc.sub_monthly_score_id as subject_score_id, sc.none_subject';
                // $where = '`sc`.exam_monthly = "'.$exam_monthly.'"';

                $qr_monthly_ord = $this->db->query("SELECT 
                                                    sch_subject_score_monthly_kgp.sub_monthly_score_id,
                                                    sch_subject_score_monthly_kgp.student_id,
                                                    sch_subject_score_monthly_kgp.teacher_id,
                                                    sch_subject_score_monthly_kgp.subject_id,
                                                    sch_subject_score_monthly_kgp.exam_monthly,
                                                    sch_subject_score_monthly_kgp.subject_groupid,
                                                    sch_subject_score_monthly_kgp.score_subject,
                                                    sch_subject_score_monthly_kgp.total_score,
                                                    sch_subject_score_monthly_kgp.average_score,
                                                    sch_subject_score_monthly_kgp.subject_mainid,
                                                    sch_subject_score_monthly_kgp.ranking_bysubj
                                                    FROM
                                                    sch_subject_score_monthly_kgp
                                                    WHERE 1=1 
                                                    AND school_id='".$program_id."'
                                                    AND program_id='".$school_id."'
                                                    AND school_level_id='".$school_level_id."'
                                                    AND grade_level_id='".$grade_level_id."'
                                                    AND adcademic_year_id='".$adcademic_year_id."'
                                                    AND class_id='".$class_id."' 
                                                    AND exam_monthly='".$exam_type['exam_monthly']."'
                                                    AND subject_id = '".$subjctid."'
                                                    ");
                $arr_score_assement = array();
                if($qr_monthly_ord->num_rows() > 0){
                    foreach($qr_monthly_ord->result() as $row_ord){
                        $sql_assement = $this->db->query("SELECT
                                                            sch_subject_score_monthly_detail_kgp.sub_monthly_score_id,
                                                            sch_subject_score_monthly_detail_kgp.subj_assem_id,
                                                            sch_subject_score_monthly_detail_kgp.percentage,
                                                            sch_subject_score_monthly_detail_kgp.score_assem,
                                                            sch_subject_score_monthly_detail_kgp.max_score_assem
                                                            FROM
                                                            sch_subject_score_monthly_detail_kgp
                                                            WHERE 1=1
                                                            AND sub_monthly_score_id='".$row_ord->sub_monthly_score_id."'");
                        if($sql_assement->num_rows() > 0){
                            foreach($sql_assement->result() as $row_ass_d){
                                $arr_score_assement[$row_ass_d->subj_assem_id] = $row_ass_d->score_assem;
                            }
                        }
                        $arr_return[$row_ord->student_id] = array("hcheckdata"=>$row_ord->sub_monthly_score_id,"score_subject"=>$row_ord->score_subject,"total_score"=>$row_ord->total_score,'ass_detail'=>$arr_score_assement);
                    }
                }else{
                    $arr_return[] = "1000";
                }
            }
            //exam type is semester-----------------------
            else if($exam_type["get_exam_type"] == "semester"){
                $exam_semester = $exam_type["exam_semester"];
                $get_monthly_in = implode(',', $exam_type["get_monthly_in"]);

                // $table = 'sch_subject_score_semester_kgp as sc';
                // $fields = 'sc.sub_sesmester_score_id as subject_score_id';
                // $where = 'sc.exam_semester ='. $exam_semester .' AND sc.get_monthly in('. $get_monthly_in .')';
                $qr_ass_core_m = $this->db->query('SELECT 
                                                    studentid,
                                                    subjectid,
                                                    subj_assem_id,
                                                    SUM(score_assem) AS score_assem,
                                                    COUNT(*) AS amt_c
                                                    FROM sch_subject_score_monthly_detail_kgp
                                                    WHERE 1=1
                                                    AND schoolid="'.$school_id.'"
                                                    AND programid="'.$program_id.'"
                                                    AND schlevelid="'.$school_level_id.'"
                                                    AND gradlevelid="'.$grade_level_id.'"
                                                    AND yearid="'.$adcademic_year_id.'"
                                                    AND classid="'.$class_id.'"
                                                    AND subjectid="'.$subjctid.'"
                                                    AND exam_monthly in('.$get_monthly_in.')
                                                    GROUP BY studentid,subjectid,subj_assem_id');
                $arr_ass = array();
                if($qr_ass_core_m->num_rows() > 0){
                    foreach($qr_ass_core_m->result() as $rass){
                        $total_score_ass = $rass->score_assem;
                        $arr_ass[$rass->studentid][$rass->subj_assem_id] = $total_score_ass;
                    }
                }
                $qr_ord = $this->db->query("SELECT
                                            semord.sub_sesmester_score_id,
                                            semord.student_id,
                                            semord.teacher_id,
                                            semord.subject_id,
                                            semord.class_id,
                                            semord.grade_level_id,
                                            semord.adcademic_year_id,
                                            semord.school_level_id,
                                            semord.program_id,
                                            semord.school_id,
                                            semord.exam_semester,
                                            semord.get_monthly,
                                            semord.total_avg_monthly_score,
                                            semord.average_monthly_score,
                                            semord.score_subject,
                                            semord.total_score,
                                            semord.average_score,
                                            semord.subject_groupid,
                                            semord.subject_mainid,
                                            semord.ranking_bysubj
                                            FROM
                                            sch_subject_score_semester_kgp AS semord
                                            WHERE 1=1
                                            AND school_id='".$program_id."'
                                            AND program_id='".$school_id."'
                                            AND school_level_id='".$school_level_id."'
                                            AND grade_level_id='".$grade_level_id."'
                                            AND adcademic_year_id='".$adcademic_year_id."'
                                            AND class_id='".$class_id."' 
                                            AND exam_semester='".$exam_semester."' 
                                            AND subject_id = '".$subjctid."'
                                            AND get_monthly in(". $get_monthly_in .")");
                $arr_score_assement = array();
                if($qr_ord->num_rows() > 0){
                    foreach($qr_ord->result() as $row_ord){
                        $qr_det = $this->db->query("SELECT
                                                        score_det.sub_semes_score_detail_id,
                                                        score_det.sub_sesmester_score_id,
                                                        score_det.subj_assem_id,
                                                        score_det.percentage,
                                                        score_det.score_assem,
                                                        score_det.max_score_assem,
                                                        score_det.is_subject
                                                        FROM
                                                        sch_subject_score_semester_detail_kgp AS score_det
                                                        WHERE 1=1 AND sub_sesmester_score_id='".$row_ord->sub_sesmester_score_id."'");
                        if($qr_det->num_rows() > 0){
                            foreach($qr_det->result() as $row_ass_d){
                                $arr_score_assement[$row_ass_d->subj_assem_id] = $row_ass_d->score_assem;
                            }
                        }
                        
                        $arr_return[$row_ord->student_id] = array("hcheckdata"=>$row_ord->sub_sesmester_score_id,"score_subject"=>$row_ord->score_subject,"total_score"=>$row_ord->total_score,'ass_detail'=>isset($arr_ass[$row_ord->student_id])?$arr_ass[$row_ord->student_id]:'');
                    }
                }else{
                    $arr_return[] = "1000";
                }
            }
            //exam type is semester-----------------------
            else if($exam_type["get_exam_type"] == "final"){
                $get_semester_in = implode(',', $exam_type["get_semester_in"]);

                $table = 'sch_subject_score_final_kgp as sc';
                $fields = 'sc.sub_final_score_id as subject_score_id,
                           sc.total_avg_score_s1,
                           sc.total_avg_score_s2';
                $where = "sc.get_semester_id in(". $get_semester_in .")";
            }
            return $arr_return;
            //query to get data---------------------------
            // $this->db->select(' s.first_name,
            //                     s.last_name,
            //                     s.first_name_kh,
            //                     s.last_name_kh,
            //                     s.gender,
            //                     sc.student_id,
            //                     sc.total_score,
            //                     sc.averag_coefficient,
            //                     sc.comments,'.$fields
            //                  );

            // $this->db->from($table);
            // $this->db->join('sch_student as s', 's.studentid = sc.student_id');
            // $this->db->where('sc.student_id', $student_id);
            // $this->db->where('sc.subject_id', $subject_id);
            // $this->db->where('sc.teacher_id', $teacher_id);
            // $this->db->where('sc.class_id', $class_id);
            // $this->db->where('sc.adcademic_year_id', $adcademic_year_id);
            // $this->db->where($where);
            //return $this->db->get();    
            
        }

        //get final score from both semester
        public function getSemesterScore($student_id, $subject_id, $teacher_id, $class_id, 
                                        $adcademic_year_id, $exam_type){

            $get_semester_score = array();
            if($exam_type["get_exam_type"] == "final")
            {
                $get_num_semester = count($exam_type["get_semester_in"]);
                if($get_num_semester > 0)
                {                    
                    $t = 0;
                    for($i = 1; $i <= $get_num_semester; $i++)
                    {
                        $this->db->select('sc.total_average_score');
                        $this->db->from('sch_subject_score_semester_kgp as sc');
                        $this->db->where('sc.student_id', $student_id);
                        $this->db->where('sc.subject_id', $subject_id);
                        $this->db->where('sc.teacher_id', $teacher_id);                        
                        $this->db->where('sc.class_id', $class_id);
                        $this->db->where('sc.adcademic_year_id', $adcademic_year_id);
                        $this->db->where('sc.exam_semester', $exam_type["get_semester_in"][$t]);
                        $result = $this->db->get();
                        if($result->num_rows() > 0)
                        {
                            $row = $result->row();
                            $get_semester_score[] = $row;
                        }
                        $t++;
                    }
                }               
            }
            return $get_semester_score;
        }

        //check subject assessment if it exists
        public function checkSubjectAssessmentIfExsits($subject_score_id, $exam_type){
            $table_detail = "";
            $where = "";

            if($exam_type["get_exam_type"] == "monthly"){
                $table_detail = 'sch_subject_score_monthly_detail_kgp';
                $where = 'sub_monthly_score_id ='. $subject_score_id; 
            }
            else if($exam_type["get_exam_type"] == "semester"){
                $table_detail = 'sch_subject_score_semester_detail_kgp';
                $where = 'sub_sesmester_score_id ='. $subject_score_id;
            }
            else if($exam_type["get_exam_type"] == "final"){
                $table_detail = 'sch_subject_score_final_detail_kgp';
                $where = 'sub_final_score_id ='. $subject_score_id;
            }

            $this->db->select('*');
            $this->db->from($table_detail);
            $this->db->where($where);            
            return $this->db->get()->result();
        }

        public function calculateAverageMonthly($student_id, $subject_id, $teacher_id, $class_id, $adcademic_year_id, $exam_monthly){
            $sql = 'SELECT
                        SUM(sc.average_score) AS total_avg_monthly_score,
                        SUM(sc.average_score)/COUNT(sc.exam_monthly) AS average_monthly_score
                    FROM
                        sch_subject_score_monthly_kgp AS sc
                    WHERE
                        sc.student_id ='. $student_id .'
                        AND sc.subject_id ='. $subject_id .'
                        AND sc.teacher_id ='. $teacher_id .'
                        AND sc.class_id ='. $class_id .'
                        AND sc.adcademic_year_id ='. $adcademic_year_id .'
                        AND sc.exam_monthly IN ('. $exam_monthly .')';
            
            $query = $this->db->query($sql);
            $num_row = $query->num_rows();
            $row = "";
            if($num_row > 0){
                return $row = $query->row();
            }else{
                return $row;
            }
        }

        //save evaluate for each student
        function saveScoreEntry(){  
			//echo "OK";
            //die();
            $school_id = $this->input->post('school_id');
            $program_id = $this->input->post('program_id');
            $school_level_id = $this->input->post('school_level_id');
            $adcademic_year_id = $this->input->post('adcademic_year_id');
            $grade_level_id = $this->input->post('grade_level_id');
            $teacher_id = $this->input->post('teacher_id');
            $class_id = $this->input->post('class_id');    
            $subject_id = $this->input->post('subject_id');
            $subject_group_id = $this->input->post('subject_group_id');
            $subject_main_id = $this->input->post('subject_main_id');
            $exam_type = $this->input->post('exam_type');
            $student_arr_inf = $this->input->post('student_arr_inf');
            $mess = "";
            $total_average_score = 0;

            if(count($student_arr_inf) > 0 ){
                foreach($student_arr_inf as $r_stu){
                    $getData = "";
                    $table = ""; $table_detail = "";
                    $fields = ""; $fields_detail = "";                    

                    $subject_score_id = ($r_stu["subject_score_id"] > 0?$r_stu["subject_score_id"] : 0);
                    
                    $average_score = ""; 
                    $averag_coefficient = "";

                    // if($exam_type["get_exam_type"] != "final"){
                    //     $averag_coefficient = $r_stu["averag_coefficient"];
                    //     if($r_stu["averag_coefficient"] != 0){
                    //        $average_score = $r_stu["total_score"] / $r_stu["averag_coefficient"]; 
                    //     }
                    // }
                    // else
                    // {
                    //     $average_score = ($r_stu["total_avg_score_s1"] + $r_stu["total_avg_score_s2"]) / 2;
                    // } 

                    //get data when exam_type is monthly
                    if($exam_type["get_exam_type"] == "monthly"){
                            $exam_monthly = $exam_type["exam_monthly"];
                            $data = array(
                                        'school_id' => $school_id,
                                        'exam_monthly' => $exam_monthly,
                                        'program_id' => $program_id,
                                        'school_level_id' => $school_level_id,
                                        'adcademic_year_id' => $adcademic_year_id,
                                        'grade_level_id' => $grade_level_id,
                                        'teacher_id' => $teacher_id,
                                        'class_id' => $class_id,
                                        'subject_id' => $subject_id,
                                        'subject_groupid' => $subject_group_id,
                                        'subject_mainid' => $subject_main_id,
                                        'student_id' => $r_stu["student_id"],
                                        'score_subject' => $r_stu["score_subj"],
                                        'total_score' => $r_stu["total_score"]
                                        );
                            $id_order_monthly = "";
                            if($r_stu['hcheckdata'] != ""){
                                $this->db->where("sub_monthly_score_id",$r_stu['hcheckdata']);
                                $this->db->where("student_id",$r_stu["student_id"]);
                                $this->db->update('sch_subject_score_monthly_kgp', $data); 
                                $id_order_monthly = $r_stu['hcheckdata'];
                                $this->db->delete("sch_subject_score_monthly_detail_kgp",array("sub_monthly_score_id"=>$id_order_monthly));
                                
                            }else{
                                $this->db->insert('sch_subject_score_monthly_kgp', $data); 
                                $id_order_monthly = $this->db->insert_id();
                            }
                            if(isset($r_stu["assement_array"])){
                                foreach($r_stu["assement_array"] as $row){
                                        $data_detail = array(
                                                            'sub_monthly_score_id'=> $id_order_monthly,
                                                            'subj_assem_id' => $row["assessment_id"],
                                                            'percentage' => $row["percentage"],
                                                            'score_assem' => $row["ass_score"],
                                                            'max_score_assem' => $row["max_score"],
                                                            'schoolid'=>$school_id,
                                                            'programid'=>$program_id,
                                                            'schlevelid'=>$school_level_id,
                                                            'yearid'=>$adcademic_year_id,
                                                            'gradlevelid'=>$grade_level_id,
                                                            'classid'=>$class_id,
                                                            'exam_monthly'=>$exam_monthly,
                                                            'subjectid'=>$subject_id,
                                                            'studentid'=>$r_stu["student_id"]
                                                        );
                                        $this->db->insert('sch_subject_score_monthly_detail_kgp', $data_detail);  
                                }
                            }
                    }
                    //get data when exam_type is semester
                    else if($exam_type["get_exam_type"] == "semester"){
                        $exam_semester = $exam_type["exam_semester"];
                        $get_monthly_in = implode(',', $exam_type["get_monthly_in"]);
                        $data = array(
                                        'school_id' => $school_id,
                                        'program_id' => $program_id,
                                        'school_level_id' => $school_level_id,
                                        'adcademic_year_id' => $adcademic_year_id,
                                        'grade_level_id' => $grade_level_id,
                                        'teacher_id' => $teacher_id,
                                        'class_id' => $class_id,
                                        'subject_id' => $subject_id,
                                        'subject_groupid' => $subject_group_id,
                                        'subject_mainid' => $subject_main_id,
                                        'student_id' => $r_stu["student_id"],
                                        'score_subject' => $r_stu["score_subj"],
                                        'total_score' => $r_stu["total_score"],
                                        'exam_semester'=>$exam_semester,
                                        'get_monthly' => $get_monthly_in
                                    );
                        $id_order_semester = "";
                        if($r_stu['hcheckdata'] != ""){
                            $this->db->where("sub_sesmester_score_id",$r_stu['hcheckdata']);
                            $this->db->where("student_id",$r_stu["student_id"]);
                            $this->db->update('sch_subject_score_semester_kgp', $data); 
                            $id_order_semester = $r_stu['hcheckdata'];
                             $this->db->delete("sch_subject_score_semester_detail_kgp",array("sub_sesmester_score_id"=>$id_order_semester));
                        }else{
                            $this->db->insert('sch_subject_score_semester_kgp', $data); 
                            $id_order_semester = $this->db->insert_id();
                        }
                        if(isset($r_stu["assement_array"])){
                            foreach($r_stu["assement_array"] as $row){
                                $data_detail = array(
                                                    'sub_sesmester_score_id'=> $id_order_semester,
                                                    'subj_assem_id' => $row["assessment_id"],
                                                    'percentage' => $row["percentage"],
                                                    'score_assem' => $row["ass_score"],
                                                    'max_score_assem' => $row["max_score"]
                                                );
                                $this->db->insert('sch_subject_score_semester_detail_kgp', $data_detail);  
                            }
                        }

                        // ==========================
                        // $fields = array(
                        //             'exam_semester' => $exam_semester,
                        //             'get_monthly' => $get_monthly_in
                        //           );
                        // $table = 'sch_subject_score_semester_kgp';

                        // $fields_detail = 'sub_sesmester_score_id';
                        // $table_detail = 'sch_subject_score_semester_detail_kgp';

                        // //calculate score of total and verage monthly in one subject
                        // $row = $this->calculateAverageMonthly($r_stu["student_id"], $subject_id, $teacher_id, $class_id, $adcademic_year_id, $get_monthly_in);
                        // $total_average_score = ($row->average_monthly_score + $average_score)/2;
                        // $score = array(
                        //                 'total_avg_monthly_score' => $row->total_avg_monthly_score,
                        //                 'average_monthly_score' => $row->average_monthly_score,
                        //                 'total_average_score' => $total_average_score
                        //             );
                        // $getData = array_merge($data, $fields, $score);
                    }
                    //get data when exam_type is final
                    else if($exam_type["get_exam_type"] == "final"){
                        $exam_final = $exam_type["get_exam_type"]."_year_id_".$adcademic_year_id;
                        $get_semester_in = implode(',', $exam_type["get_semester_in"]);                

                        $fields = array(
                                    'exam_final' => $exam_final,
                                    'get_semester_id' => $get_semester_in,
                                    'total_avg_score_s1' => $r_stu["total_avg_score_s1"],
                                    'total_avg_score_s2' => $r_stu["total_avg_score_s2"]
                                  );
                        $table = 'sch_subject_score_final_kgp';

                        $fields_detail = 'sub_final_score_id';
                        //$table_detail = 'sch_subject_score_final_detail_kgp';

                        $getData = array_merge($data, $fields);
                    }

                    //update data-------------------------------------------------
                    if($subject_score_id > 0){
                        //update main table
                        //$this->db->where($fields_detail, $subject_score_id);
                        //$this->db->update($table,$getData);

                        // if(!empty($r_stu["getScoreDetail"]) > 0){
                        //     //delete detail table and insert new data
                        //     $this->db->where($fields_detail, $subject_score_id);
                        //     $this->db->delete($table_detail);

                        //     foreach($r_stu["getScoreDetail"] as $row){
                        //             $data_detail = array(
                        //                                 $fields_detail => $subject_score_id,
                        //                                 'assessment_id' => $row["assessment_id"],
                        //                                 'is_subject' => $row["is_subject"],
                        //                                 'percentage' => $row["percentage"],
                        //                                 'input_score' => $row["input_score"],
                        //                                 'output_score' => $row["output_score"]
                        //                             );
                        //             $this->db->insert($table_detail, $data_detail);  
                        //     }
                        // }

                        $mess = 2;
                    }
                    //insert data--------------------------------------------------
                    else
                    {
                        // $this->db->insert($table,$getData);
                        // $getId = $this->db->insert_id();

                        // if(!empty($r_stu["getScoreDetail"]) > 0){
                        //     foreach($r_stu["getScoreDetail"] as $row){
                        //         //if($row["input_score"] > 0){
                        //             $data_detail = array(
                        //                                 $fields_detail => $getId,
                        //                                 'assessment_id' => $row["assessment_id"],
                        //                                 'is_subject' => $row["is_subject"],
                        //                                 'percentage' => $row["percentage"],
                        //                                 'input_score' => $row["input_score"],
                        //                                 'output_score' => $row["output_score"]
                        //                             );
                        //             $this->db->insert($table_detail, $data_detail);  
                        //         //}                                
                        //     }
                        // }
                        
                        $mess = 1;
                    }
                }               
            }
// $this->green->getRankresult_by_class(1,6,2,1,101,1);
            return $mess;
        }


    }
?>