<?php
class Modstudy_record extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function grid(){
		$offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;

		$student_num = trim($this->input->post('student_num', TRUE));
		$full_name = trim($this->input->post('full_name', TRUE));
		$full_name_kh = trim($this->input->post('full_name_kh', TRUE));
		$card_type = trim($this->input->post('card_type', TRUE));

		$programid = trim($this->input->post('programid', TRUE));
		$schlevelid = trim($this->input->post('schlevelid', TRUE));		
		$yearid = trim($this->input->post('yearid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));

		$where = '';

		if($student_num != ''){
			$where .= "AND s.student_num LIKE '%{$student_num}%' ";
		}
		if($full_name != ''){
			$where .= "AND CONCAT(s.last_name, ' ', s.first_name) LIKE '%{$full_name}%' ";
		}
		if($full_name_kh != ''){
			$where .= "AND CONCAT(s.last_name_kh, ' ', s.first_name_kh) LIKE '%{$full_name_kh}%' ";
		}

		if($programid != ''){
			$where .= "AND s.programid = '{$programid}' ";
		}
		if($schlevelid != ''){
			$where .= "AND s.schlevelid = '{$schlevelid}' ";
		}
		if($yearid != ''){
			$where .= "AND s.yearid = '{$yearid}' ";
		}
		if($classid != ''){
			$where .= "AND s.classid = '{$classid}' ";
		}

		// count ==========
		$qr_c = $this->db->query("SELECT COUNT(s.studentid) AS c FROM v_student_profile AS s
									WHERE 1=1 {$where} ")->row()->c - 0;

		$totalRecord = $qr_c - 0;		
		$totalPage = ceil($totalRecord/$limit);

		// result =======
		$qr = $this->db->query("SELECT
										*,
										CONCAT(s.last_name, ' ', s.first_name) AS full_name,
										CONCAT(s.last_name_kh, ' ', s.first_name_kh) AS full_name_kh,
										DATE_FORMAT(dateofbirth, '%d-%m-%Y') AS dateofbirth
									FROM
										v_student_profile AS s
									WHERE 1 = 1 {$where}
									GROUP BY s.studentid
									ORDER BY
										s.student_num ASC
									LIMIT {$offset},
									 {$limit} ");

		$i = 1;
		$tr = '';

		if($qr->num_rows() > 0){			
			foreach($qr->result_array() as $row){

				// family ==========
				$family_code ='';
				$family_name ='';
				$family_code ='';
				$family_name ='';
				$family_book_no ='';
				$family_home_no = "";
			    $family_street = "";
			    $family_village = "";
			    $family_commune = "";
			    $family_district = "";
			    $family_province = "";
			    $family_tel = "";
				$father_name ='';
				$father_name_kh ='';
				$father_ocupation ='';
				$father_ocupation_kh ='';
				$father_dob ='';
				$father_home ='';
				$father_phone ='';
				$father_street ='';
				$father_village ='';
				$father_commune ='';
				$father_district ='';
				$father_province ='';
				$mother_name ='';
				$mother_name_kh ='';
				$mother_ocupation ='';
				$mother_ocupation_kh ='';
				$mother_dob ='';
				$mother_home ='';
				$mother_phone ='';
				$mother_street ='';
				$mother_village ='';
				$mother_commune ='';
				$mother_district ='';
				$mother_province ='';

				if($row['familyid'] - 0 > 0){
					$family = $this->family->getfamilyrow($row['familyid']);
					if(count($family) > 0){
						$family_code = $family["family_code"];
						$family_name = $family["family_name"];
						$family_book_no = $family["family_book_record_no"];
						$family_home_no = $family["home_no"];
						$family_street = $family["street"];
						$family_village = $family["village"];
						$family_commune = $family["commune"];
						$family_district = $family["district"];
						$family_province = $family["province"];
						$family_tel = $family["tel"];
						$father_name = $family["father_name"];
						$father_name_kh = $family["father_name_kh"];
						$father_ocupation = $family["father_ocupation"];
						$father_ocupation_kh = $family["father_ocupation_kh"];
						$father_dob = $this->green->convertSQLDate($family["father_dob"]);
						$father_home = $family["father_home"];
						$father_phone = $family["father_phone"];
						$father_street = $family["father_street"];
						$father_village = $family["father_village"];
						$father_commune = $family["father_commune"];
						$father_district = $family["father_district"];
						$father_province = $family["father_province"];
						$mother_name = $family["mother_name"];
						$mother_name_kh = $family["mother_name_kh"];
						$mother_ocupation = $family["mother_ocupation"];
						$mother_ocupation_kh = $family["mother_ocupation_kh"];
						$mother_dob = $this->green->convertSQLDate($family["mother_dob"]);
						$mother_home = $family["mother_home"];
						$mother_phone = $family["mother_phone"];
						$mother_street = $family["mother_street"];
						$mother_village = $family["mother_village"];
						$mother_commune = $family["mother_commune"];
						$mother_district = $family["mother_district"];
						$mother_province = $family["mother_province"];
					}
					
				}

				$responsible = $this->std->getresponstudent($row['studentid']);
				if(count($responsible) > 0){
					$guard_name = $responsible->respon_name;
					$guard_name_eng = $responsible->respon_name_eng;
			        $guard_relationship = $responsible->relationship;
			        $guard_occupation = $responsible->occupation;
			        $guard_occupation_eng = $responsible->occupation_eng;
			        $guard_tel1 = $responsible->tel1;
			        $guard_tel2 = $responsible->tel2;
			        $guard_email = $responsible->email;
			        $guard_home = $responsible->home_no;
			        $guard_street = $responsible->street;
			        $guard_village = $responsible->village;
			        $guard_commune = $responsible->commune;
			        $guard_district = $responsible->district;
			        $guard_province = $responsible->province;
				}else{
					$guard_name = "";
					$guard_name_eng = "";
			        $guard_occupation_eng = "";
			        $guard_occupation= "";
			        $guard_relationship = "";
			        $guard_tel1= "";
			        $guard_tel2= "";
			        $guard_email = "";
			        $guard_home= "";
			        $guard_street= "";
			        $guard_village= "";
			        $guard_commune= "";
			        $guard_district= "";
			        $guard_province= "";
				}

				$no_imgs = base_url()."assets/upload/students/NoImage.png";	
				$have_img = base_url()."assets/upload/students/".$row["yearid"].'/thumbs/'.$row["studentid"].'.jpg';

				$img = '<img src="'.$no_imgs.'" class="rounded" alt="No Image" style="width: 25mm !important;height: 29.1mm !important;">';
				if (file_exists("assets/upload/students/".$row["yearid"].'/thumbs/'.$row["studentid"].'.jpg')) {
					$img = '<img src="'.$have_img.'" class="rounded" alt="No Image" style="width: 25mm !important;height: 29.1mm !important;">';
				}

				if($card_type - 0 == 1){
					$tr .= '
							<tr style="font-family: khmer mef2;font-size: 35px;text-align: center;vertical-align: middle;">
		                        <td colspan="5" style="padding-top: 25px;">
		                           <div>សៀវភៅតាមដានការសិក្សា</div>
		                           <div style="font-size: 25px;font-weight: bold;">Student&apos;s Study Record</div>
		                           <div style="padding: 0;"><img src="'.base_url()."assets/images/sambol-line.png".'" class="rounded" alt="No Image" style="width: 200px !important;height: 18px !important;"></div>		                           
		                           <div>                              
		                              <table border="0"​ cellspacing="0" cellpadding="0" align="right" style="width: 100%;">
		                                 <tr>
		                                    <td style="font-family: arial;width: 80%;">&nbsp;</td>
		                                    <td style="text-align: right;">'.$img.'</td>
		                                 </tr>
		                              </table>
		                           </div>
		                        </td>
		                     </tr>
							<tr>
		                        <td style="white-space: nowrap; width:23%;">
		                           <div>នាមត្រកូល និង នាមខ្លួន​</div>
		                           <div>Student Name</div></td>                       
		                        <td colspan ="2" style="width: 45%;font-weight: bold;"><input type="text" class="form-control" name="full_name_" id="full_name_" value="'.$row['full_name'].'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly></td>	                        
		                        <td>
		                           <div>ភេទ</div>
		                           <div>Sex</div></td>
		                        <td>
		                           <input type="text" class="form-control" name="gender_" id="gender_" value="'.ucfirst($row['gender']).'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly></td>
		                     </tr>
		                     <tr>
		                        <td>
		                           <div>ថ្ងៃខែឆ្នាំកំណើត</div>
		                           <div>Date of birth</div>                           
		                        </td>                        
		                        <td colspan="4">
		                           <input type="text" class="form-control" name="dateofbirth_" id="dateofbirth_" value="'.($row['dateofbirth'] != null && $row['dateofbirth'] != '00-00-0000' ? $row['dateofbirth'] : '').'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
		                        </td>
		                     </tr>
		                     <tr>
		                        <td>
		                           <div>ទីកន្លែងកំណើត</div>
		                           <div>Place of birth</div>                           
		                        </td>                        
		                        <td colspan="4">
		                           <input type="text" class="form-control" name="placeofbirth_" id="placeofbirth_" value="Sangkat '.$row['dob_commune_eng'].', Khan '.$row['dob_district_eng'].', '.$row['dob_province_eng'].'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
		                        </td>
		                     </tr>
		                     <tr>
		                        <td>
		                           <div>ឳពុកឈ្មោះ</div>
		                           <div>Father&apos;s Name</div>                           
		                        </td>                        
		                        <td>
		                           <input type="text" class="form-control" name="father_name_" id="father_name_" value="'.$father_name.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
		                        </td>

		                        <td>
		                           <div>ទូរស័ព្ទ</div>
		                           <div>Tel.</div>                           
		                        </td>                        
		                        <td  colspan ="2">
		                           <input type="text" class="form-control" name="tel_" id="tel_" value="'.$father_phone.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
		                        </td>                        
		                     </tr>                     
		                     <tr>
		                        <td>
		                           <div>មុខរបរ</div>
		                           <div>Occupation</div>                           
		                        </td>                        
		                        <td colspan="4">
		                           <input type="text" class="form-control" name="occupation_" id="occupation_"  value="'.$father_ocupation.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
		                        </td>
		                     </tr>


		                     <tr>
		                        <td>
		                           <div>ម្តាយឈ្មោះ</div>
		                           <div>Mother&apos;s Name</div>                           
		                        </td>                        
		                        <td>
		                           <input type="text" class="form-control" name="mother_name_" id="mother_name_" value="'.$mother_name.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
		                        </td>
		                        <td>
		                           <div>ទូរស័ព្ទ</div>
		                           <div>Tel.</div>                           
		                        </td>                        
		                        <td colspan ="2">
		                           <input type="text" class="form-control" name="tel_mother_" id="tel_mother_" value="'.$mother_phone.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
		                        </td>                        
		                     </tr>                     
		                     <tr>
		                        <td>
		                           <div>មុខរបរ</div>
		                           <div>Occupation</div>                           
		                        </td>                        
		                        <td colspan="4">
		                           <input type="text" class="form-control" name="occupation_mother_" value="'.$mother_ocupation.'" id="occupation_mother_" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
		                        </td>
		                     </tr>


		                     <tr>
		                        <td>
		                           <div>អាណាព្យាបាល</div>
		                           <div>Guardian&apos;s Name</div></td>                        
		                        <td>
		                           <input type="text" class="form-control" name="guardian_name_" id="guardian_name_" value="'.$guard_name_eng.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly></td>
		                        <td>
		                           <div>ទូរស័ព្ទ</div>
		                           <div>Tel.</div></td>                        
		                        <td colspan ="2">
		                           <input type="text" class="form-control" name="tel_guardian_" id="tel_guardian_" value="'.$guard_tel1.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly></td>                        
		                     </tr>                     
		                     <tr>
		                        <td>
		                           <div>មុខរបរ</div>
		                           <div>Occupation</div></td>                        
		                        <td colspan="3">
		                           <input type="text" class="form-control" name="occupation_guardian_" id="occupation_guardian_" value="'.$guard_occupation_eng.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly></td>
		                     </tr>
		                     <tr>
		                        <td>
		                           <div>អាសយដ្ឋានសព្វថ្ងៃ</div>
		                           <div>Current address</div></td>                        
		                        <td colspan="4">
		                           <input type="text" class="form-control" name="current_address_" id="current_address_" value="N#'.$row['home_no_eng'].', St'.$row['street_eng'].', Sangkat '.$row['commune_eng'].', Khan '.$row['district_eng'].', '.$row['province_eng'].'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly></td>
		                     </tr>
		                     <tr>
		                        <td colspan="5">';
		                          $tr .= "<div>គំរូហេត្ថលេខាមាតាបិតា ឬ អាណាព្យាបាលសិស្ស: Parent/ Guardian's signature</div>	";	                  
		                       $tr .= '</td>                        
		                     </tr>
		                     
		                     <tr style="text-align: center;height: 160px;">
		                        <td colspan="2" style="vertical-align: top; text-align:center;">មាតាបិតា: Parent</td>                       
		                                                
		                        <td  colspan ="4" style="vertical-align: top;">អាណាព្យាបាល(បើមាន): Guardian</td>
		                     </tr>
		                     <tr>                     
		                        <td colspan="6">
		                           <span style="font-family: khmer mef2;"><img src="'.base_url()."assets/images/point-01.png".'" style="width: 20px !important;height: 8px !important;"> សំគាល់ៈ </span>
		                           <span style="font-family: khmer mef1; font-size:15px;">ស្នើមាតាបិតា ឬអាណាព្យាបាលសិស្សចុះគំរូហត្ថលេខាកុំបីខាន។ Note: Signature is compusory</span>
		                        </td>
	                     </tr>';
	                }else{
	                	$tr .= '
	                			<tr style="font-family: khmer mef2;font-size: 35px;text-align: center;vertical-align: middle;">
			                        <td colspan="6" style="padding-top: 25px;">
			                           <div>សៀវភៅតាមដានការសិក្សា</div>
			                           <div style="font-size: 25px;font-weight: bold;">Student&apos;s Study Record</div>
			                           <div style="padding: 0;"><img src="'.base_url()."assets/images/sambol-line.png".'" class="rounded" alt="No Image" style="width: 200px !important;height: 18px !important;"></div>		                           
			                           <div>                              
			                              <table border="0"​ cellspacing="0" cellpadding="0" align="right" style="width: 100%;">
			                                 <tr>
			                                    <td style="font-family: arial;width: 80%;">&nbsp;</td>
			                                    <td style="text-align: right;">'.$img.'</td>
			                                 </tr>
			                              </table>
			                           </div>
			                        </td>
			                     </tr>

	                			<tr>
			                        <td style="white-space:nowrap; width:5%;">
			                           <div>នាមត្រកូល និង នាមខ្លួន​</div>
			                           <div>Student Name</div></td>                        
			                        <td colspan="5">
			                        	<div style="width: 38%; float:left;">
			                        		<input type="text" class="form-control" name="full_name_kh_" id="full_name_kh_" value="'.$row['full_name_kh'].'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px; font-family: khmer mef2;" readonly>
			                        	</div>
			                        	<div style="width: 38%; float:left; padding-left:1px;">
			                        		<input type="text" class="form-control" name="full_name_" id="full_name_" value="'.$row['full_name'].'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
			                        	</div>
			                        
			                           <div style="width: 7%; float:left;">ភេទ</div>
			                           <div style="width: 7%; float:left;">Sex</div>
			                           <div style="width: 10%; float:right;">			                        
			                           	<input type="text" class="form-control" name="gender_" id="gender_" value="'.($row['gender']=="male"?"ប្រុស":"ស្រី").'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
			                           </div>
			                           </td>
			                     </tr>

			                    <tr>
			                        <td>
			                           <div>ថ្ងៃខែឆ្នាំកំណើត</div>
			                           <div>Date of birth</div>                           
			                        </td>                        
			                        <td colspan="5">
			                           <input type="text" class="form-control" name="dateofbirth_" id="dateofbirth_" value="'.($row['dateofbirth'] != null && $row['dateofbirth'] != '00-00-0000' ? $row['dateofbirth'] : '').'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
			                        </td>
			                     </tr>


			                     <tr>
			                        <td>
			                           <div>ទីកន្លែងកំណើត</div>
			                           <div>Place of birth</div>                           
			                        </td>                        
			                        <td colspan="5">
			                           <input type="text" class="form-control" name="placeofbirth_" id="placeofbirth_" value="ឃុំ/សង្កាត់ '.$row['dob_commune'].' ស្រុក/ខណ្ឌ '.$row['dob_district'].' ខេត្ត/ក្រុង '.$row['dob_province'].'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
			                        </td>
			                     </tr>
			                     

			                     <tr>
			                        <td>
			                           <div>ឳពុកឈ្មោះ</div>
			                           <div>Father&apos;s Name</div>                           
			                        </td>                        
			                        <td>
			                           <input type="text" class="form-control" name="father_name_" id="father_name_" value="'.$father_name_kh.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
			                        </td>
			                        <td>
			                           <div>ទូរស័ព្ទ</div>
			                           <div>Tel.</div>                           
			                        </td>                        
			                        <td colspan="3">
			                           <input type="text" class="form-control" name="tel_" id="tel_" value="'.$father_phone.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
			                        </td>                        
			                     </tr>                     
			                     <tr>
			                        <td>
			                           <div>មុខរបរ</div>
			                           <div>Occupation</div>                           
			                        </td>                        
			                        <td colspan="5">
			                           <input type="text" class="form-control" name="occupation_" id="occupation_"  value="'.$father_ocupation_kh.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
			                        </td>
			                     </tr>
			                     <tr>
			                        <td>
			                           <div>ម្តាយឈ្មោះ</div>
			                           <div>Mother&apos;s Name</div>                           
			                        </td>                        
			                        <td>
			                           <input type="text" class="form-control" name="mother_name_" id="mother_name_" value="'.$mother_name_kh.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
			                        </td>
			                        <td>
			                           <div>ទូរស័ព្ទ</div>
			                           <div>Tel.</div>                           
			                        </td>                        
			                        <td colspan="3">
			                           <input type="text" class="form-control" name="tel_mother_" id="tel_mother_" value="'.$mother_phone.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
			                        </td>                        
			                     </tr>                     
			                     <tr>
			                        <td>
			                           <div>មុខរបរ</div>
			                           <div>Occupation</div>                           
			                        </td>                        
			                        <td colspan="5">
			                           <input type="text" class="form-control" name="occupation_mother_" value="'.$mother_ocupation_kh.'" id="occupation_mother_" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly>
			                        </td>
			                     </tr>
			                     <tr>
			                        <td>
			                           <div>អាណាព្យាបាល</div>
			                           <div>Guardian&apos;s Name</div></td>                        
			                        <td>
			                           <input type="text" class="form-control" name="guardian_name_" id="guardian_name_" value="'.$guard_name.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly></td>
			                        <td>
			                           <div>ទូរស័ព្ទ</div>
			                           <div>Tel.</div></td>                        
			                        <td colspan="3">
			                           <input type="text" class="form-control" name="tel_guardian_" id="tel_guardian_" value="'.$guard_tel1.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly></td>                        
			                     </tr>                     
			                     <tr>
			                        <td>
			                           <div>មុខរបរ</div>
			                           <div>Occupation</div></td>                        
			                        <td colspan="5">
			                           <input type="text" class="form-control" name="occupation_guardian_" id="occupation_guardian_" value="'.$guard_occupation.'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly></td>
			                     </tr>


			                     <tr>
			                        <td>
			                           <div>អាសយដ្ឋានសព្វថ្ងៃ</div>
			                           <div>Current address</div></td>                        
			                        <td colspan="5">
			                           <input type="text" class="form-control" name="current_address_" id="current_address_" value="ផ្ទះលេខ​'.$row['home_no'].' ផ្លូវលេខ '.$row['street'].' ឃុំ/សង្កាត់ '.$row['commune'].', ស្រុក/ខណ្ឌ '.$row['district'].', '.$row['province'].'" style="border: 1px solid black;border-radius: 1px;background: #FFFFFF;padding: 0 5px 0 5px;" readonly></td>
			                     </tr>
			                     <tr>
			                        <td colspan="6">
			                           <div>គំរូហេត្ថលេខាមាតាបិតា ឬ អាណាព្យាបាលសិស្ស</div>                         
			                        </td>                        
			                     </tr>
			                     
			                     <tr style="text-align: center;height: 160px;">
			                        <td style="vertical-align: top;">មាតា</td>                        
			                        <td colspan="2" style="vertical-align: top;">បិតា</td>                        
			                        <td colspan="3" style="vertical-align: top;">អាណាព្យាបាល (បើមាន)</td>
			                     </tr>


			                     <tr>                     
			                        <td colspan="6">
			                           <span style="font-family: khmer mef2;"><img src="'.base_url()."assets/images/point-01.png".'" style="width: 20px !important;height: 8px !important;"> សំគាល់ៈ </span>
			                           <span>ស្នើមាតាបិតា ឬអាណាព្យាបាលសិស្សចុះគំរូហត្ថលេខាកុំបីខាន ។</span>
			                        </td>
		                     </tr>';
	                }

                 	/*if($totalRecord - 0 > 1){
						$tr .=  '<tr style="page-break-after: always;">                    
				                    <td colspan="4"></td>
			                     </tr>';
                     }*/

			}
			
		}else{
			$tr .= '<tr><td colspan="2" style="font-weight: bold;text-align: center;background: #F2F2F2;">'."We din't find data".'</tr>';
		}

		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage); 
        return json_encode($arr);	
	}

    // get grade =========
    function get_grade($schlevelid){
        $opt = '';
        // $opt .= '<option value=""></option>';		
		foreach($this->g->getgradelevels($schlevelid) as $row_grade){
			$opt .= '<option value="'.$row_grade->grade_levelid.'">'.$row_grade->grade_level.'</option>';
		}
        return json_encode(array('opt' => $opt));
    }	

    // get school level ========
    function get_schlevel($programid = ''){
	    $opt = '';
		$opt .= '<option value=""></option>';
		foreach ($this->level->getsch_level($programid) as $row) {
			$opt .= '<option value="'.$row->schlevelid.'">'.$row->sch_level.'</option>';
		}
	    return json_encode(array('opt' => $opt));
	}

    // get year ========

    function get_year($programid = '', $schlevelid = ''){
	    $opt = '';
        $opt .= '<option value=""></option>';		
		foreach($this->y->getschoolyear('', $programid, $schlevelid) as $row_year){
			$opt .= '<option value="'.$row_year->yearid.'">'.$row_year->sch_year.'</option>';
		}
	    return json_encode(array('opt' => $opt));
	}

	// get class ========
    function get_class($schlevelid = ''){
	    $opt = '';
        $opt .= '<option value=""></option>';		
		foreach($this->cl->allclass($schlevelid) as $row_class){
			$opt .= '<option value="'.$row_class->classid.'">'.$row_class->class_name.'</option>';
		}
	    return json_encode(array('opt' => $opt));
	}
}