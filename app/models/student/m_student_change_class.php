<?php
class M_student_change_class extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function grid(){
		$offset = $this->input->post('offset') - 0;
        $limit = $this->input->post('limit') - 0;

		$student_num = trim($this->input->post('student_num', TRUE));
		$full_name = trim($this->input->post('full_name', TRUE));
		$schoolid = trim($this->input->post('schoolid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelid = trim($this->input->post('schlevelid', TRUE));
		$term_sem_year_id = trim($this->input->post('term_sem_year_id', TRUE));
		$feetypeid = trim($this->input->post('feetypeid', TRUE));
		$yearid = trim($this->input->post('yearid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$is_paid = trim($this->input->post('is_paid', TRUE));
		$is_closed = trim($this->input->post('is_closed', TRUE));		
		$from_date = trim($this->input->post('from_date', TRUE));
		$to_date = trim($this->input->post('to_date', TRUE));	

		$where = '';

		if($student_num != ''){
			$where .= "AND st.student_num LIKE '%{$student_num}%' ";
		}
		if($full_name != ''){
			$where .= "AND CONCAT(
								st.first_name,
								' ',
								st.last_name
							) LIKE '%{$full_name}%' ";
		}
		// if($full_name != ''){
		// 	$where .= "AND CONCAT(
		// 						st.first_name_kh,
		// 						' ',
		// 						st.last_name_kh
		// 					) LIKE '%{$full_name}%' ";
		// }
		
		if($schoolid != ''){
			$where .= "AND er.schoolid = '{$schoolid}' ";
		}
		if($programid != ''){
			$where .= "AND er.programid = '{$programid}' ";
		}
		if($schlevelid != ''){
			$where .= "AND er.schlevelid = '{$schlevelid}' ";
		}
		if($yearid != ''){
			$where .= "AND er.year = '{$yearid}' ";
		}		
		// if($term_sem_year_id != ''){
		// 	$where .= "AND term_sem_year_id LIKE '%{$term_sem_year_id}%' ";
		// }
		if($feetypeid != ''){
			$where .= "AND er.feetypeid = '{$feetypeid}' ";
		}
		if($rangelevelid != ''){
			$where .= "AND er.rangelevelid = '{$rangelevelid}' ";
		}
		if($classid != ''){
			$where .= "AND er.classid = '{$classid}' ";
		}
		if($is_paid != ''){
			$where .= "AND er.is_paid = '{$is_paid}' ";
		}
		if($is_closed != ''){
			$where .= "AND er.is_closed = '{$is_closed}' ";
		}

		// if($from_date != ''){
		// 	$where .= "AND student_num LIKE '%{$from_date}%' ";
		// }
		// if($to_date != ''){
		// 	$where .= "AND student_num LIKE '%{$to_date}%' ";
		// }

		$totalRecord = $this->db->query("SELECT
												COUNT(er.enrollid) AS c
											FROM
												(
													(
														(
															(
																(
																	(
																		sch_student_enrollment AS er
																		JOIN sch_student AS st ON (
																			(er.studentid = st.studentid)
																		)
																	)
																	JOIN sch_school_program AS p ON ((er.programid = p.programid))
																)
																JOIN sch_school_level AS lv ON (
																	(
																		er.schlevelid = lv.schlevelid
																	)
																)
															)
															JOIN sch_school_rangelevel AS r ON (
																(
																	er.rangelevelid = r.rangelevelid
																)
															)
														)
														JOIN sch_school_year AS y ON ((er.`year` = y.yearid))
													)
													JOIN sch_class AS c ON ((er.classid = c.classid))
												) 
											WHERE 1=1 {$where} ")->row()->c;

		$totalPage = ceil($totalRecord/$limit);

		$datas ="SELECT
						v_student_enroment.student_num,
						v_student_enroment.first_name,
						v_student_enroment.last_name,
						v_student_enroment.first_name_kh,
						v_student_enroment.last_name_kh,
						v_student_enroment.gender,
						v_student_enroment.phone1,
						v_student_enroment.class_name,
						v_student_enroment.studentid,
						v_student_enroment.schoolid,
						v_student_enroment.`year`,
						v_student_enroment.classid,
						v_student_enroment.schlevelid,
						v_student_enroment.rangelevelid,
						v_student_enroment.feetypeid,
						v_student_enroment.programid,
						v_student_enroment.sch_level,
						v_student_enroment.rangelevelname,
						v_student_enroment.program,
						v_student_enroment.sch_year
					FROM
						v_student_enroment
				WHERE 1=1 
				ORDER BY
					v_student_enroment.student_num - 0 ASC
				LIMIT $offset, $limit
				";	
		$result = $this->db->query($datas)->result();
						// print_r($classes);
						$tr="";	
						$amttotal=0;
						$ii=1;
						if(isset($result) && count($result)>0){	
						$des="";	
							foreach($result as $rowde){
								
								$getclass="SELECT
												sch_class.classid,
												sch_class.class_name
											FROM
												sch_class
											WHERE 1=1
											AND sch_class.schlevelid='".$rowde->schlevelid."'
											ORDER BY classid,class_name 
											";
								$result_cl = $this->db->query($getclass)->result();
								$dataclass="<option value=''>None</option>";
									
								if(isset($result_cl) && count($result_cl)>0){		
									foreach($result_cl as $myrow){
										$dataclass.="<option value='".$myrow->classid."'>".$myrow->class_name."</option>";
									}
								}
								
								$getrang="SELECT
												sch_school_rangelevel.rangelevelid,
												sch_school_rangelevel.rangelevelname
												
											FROM
												sch_school_rangelevel
											WHERE 1=1
											AND sch_school_rangelevel.programid='".$rowde->programid."'
											AND sch_school_rangelevel.schlevelid='".$rowde->schlevelid."'											
												ORDER BY rangelevelid,rangelevelname
											";
								$result_rang = $this->db->query($getrang)->result();
								$datacrang="<option value=''>None</option>";
									
								if(isset($result_rang) && count($result_rang)>0){		
									foreach($result_rang as $myrowr){
										$datacrang.="<option value='".$myrowr->rangelevelid."'>".$myrowr->rangelevelname."</option>";
									}
								}
								
								//-------
								$path=base_url('assets/upload/students/'.$rowde->year);
								
								/*if(file_exists($path.'/'.$rowde->student_num.'.jpg')){
									$imangs='<img src="'.base_url('assets/upload/students/'.$rowde->year.'/'.$rowde->student_num.'.jpg').'" class="img-circle" alt="No image" width="70" height="70">';	
								
								}else{
									$imangs='<img src="'.base_url('assets/upload/students/'.$rowde->year.'/nonephoto.png').'" class="img-circle" alt="No image" width="70" height="70">';	
								}*/
								
								$imangs='<img src="'.base_url('assets/upload/students/'.$rowde->year.'/'.$rowde->student_num.'.jpg').'" class="img-circle" alt="No image" width="70" height="70">';	
								
								$tr.='<tr>
										<td>'.($ii++).'</td>
										<td>'.$imangs.'</td>
										<td><strong>'.$rowde->student_num.'</strong><br>'.$rowde->first_name_kh.' '.$rowde->first_name.'<br>'.$rowde->first_name.' '.$rowde->first_name.'<br>'.$rowde->gender.'</td>
										
										<td>'.$rowde->program.'</td>
										<td>'.$rowde->sch_level.'</td>
										<td>'.$rowde->sch_year.'</td>
										<td>'.$rowde->rangelevelname.'</td>
										<td>'.$rowde->class_name.'s</td>
										<td><select class="datacrang">'.$datacrang.'</select></td>
										<td><select class="dataclass">'.$dataclass.'</select></td>
										<td class="tr_studentid" attr_studentid='.$rowde->studentid.'>Add</td>										
										
									</tr>';
							}
						}
		$arr['tr']=$tr;		
		// $arr = array('totalRecord' => $totalRecord, 'totalPage' => $totalPage);
        return json_encode($arr);
	}

	function get_schlevel($programid = ''){
        $qr_schlevel = $this->db->query("SELECT DISTINCT
                                                l.schlevelid,
                                                l.sch_level
                                            FROM
                                                sch_school_level AS l
                                            WHERE
                                                l.programid = '{$programid}' - 0
                                            ORDER BY
                                                l.sch_level ASC ")->result();        
        
        $arr = array('schlevel' => $qr_schlevel);
        return json_encode($arr);
    }

    function get_year($programid = '', $schlevelid = ''){
        $qr_year = $this->db->query("SELECT DISTINCT
                                            y.yearid,
                                            y.sch_year
                                        FROM
                                            sch_school_year AS y
                                        WHERE
                                            y.programid = '{$programid}' - 0 AND y.schlevelid = '{$schlevelid}' - 0
                                        ORDER BY
                                            y.sch_year ASC ")->result();

        $qr_class = $this->db->query("SELECT
													c.classid,
													c.class_name
												FROM
													sch_school_level AS l
												INNER JOIN sch_class AS c ON l.schlevelid = c.schlevelid
												WHERE
													l.schlevelid = '{$schlevelid}'
												ORDER BY
													c.class_name ASC ")->result();
        
        $arr = array('year' => $qr_year, 'class' => $qr_class);
        return json_encode($arr);
    }

    // get rangelevel =========
    function get_rangelevel($yearid = ''){
        $qr_rangelevel = $this->db->query("SELECT DISTINCT
                                                rl.rangelevelid,
                                                rl.rangelevelname,
                                                rp.rangelevelid AS rangelevelid_period
                                            FROM
                                                sch_school_rangelevel AS rl
                                            INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
                                            AND rl.programid = y.programid
                                            LEFT JOIN sch_rangelevel_period AS rp ON rl.rangelevelid = rp.rangelevelid
                                            WHERE
                                                y.yearid = '{$yearid}' 
                                            ORDER BY rl.rangelevelname ASC ")->result();

        $arr = array('rangelevel' => $qr_rangelevel);
        return json_encode($arr);
    }

}