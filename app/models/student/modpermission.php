<?php
	class ModPermission extends CI_Model{
		var $sid;
		var $num_perm;
		function getstdpermis(){
			$this->load->library('pagination');	
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m='';
			$p='';
			if(isset($_GET['m'])){
		        $m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }	

		    // ------------------------------------ start long tha working---------------------------------------
		    if(isset($_GET['sid'])){
		        if($_GET['sid'] > 0)
		        {
		        	$this->sid = " AND s.studentid ='".$_GET['sid']."'";
		        }
		    }
		    // ------------------------------------ end long tha working---------------------------------------
			
			if(isset($_GET['n'])){
				if($_GET['n']==2 || $_GET['n']==3){
					$this->num_perm=" AND MONTH (stdp.to_date) =".date('m');
				}else{
					$this->num_perm='';
				}
			}
		    $yearid=$this->session->userdata('year');
				$sql="SELECT s.student_num,
							s.studentid,
							stdp.permisid,
							s.fullname,
							s.class_name,
							stdp.date,
							stdp.from_date,
							stdp.to_date,
							stdp.reason,
							emp.last_name,
							emp.first_name
					FROM sch_student_permission stdp
					INNER JOIN v_student_profile s ON(stdp.studentid=s.studentid AND stdp.yearid=s.yearid)
					INNER JOIN sch_emp_profile emp ON(stdp.approve_by=emp.empid)
					WHERE stdp.yearid='".$yearid."' {$this->sid} {$this->num_perm}";	
				$config['base_url'] =site_url()."/student/permission?pg=1&m=$m&p=$p";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =50;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				return $this->db->query($sql)->result();
		}
		function save($permisid=''){
			$date=$this->input->post('date');
			$classid=$this->input->post('classid');
			$yearid=$this->session->userdata('year');
			$studentid=$this->input->post('studentid');
			$from_date=$this->input->post('from_date');
			$to_date=$this->input->post('to_date');
			$approve_by=$this->input->post('approve_by');
			$reason=$this->input->post('reason');
			$permis_status=$this->input->post('permis_status');
			$c_date=date('Y-m-d');
			$user=$this->session->userdata('user_name');
			$data=array('date'=>$this->green->formatSQLDate($date),
						'classid'=>$classid,
						'yearid'=>$yearid,
						'studentid'=>$studentid,
						'from_date'=>$this->green->formatSQLDate($from_date),
						'to_date'=>$this->green->formatSQLDate($to_date),
						'approve_by'=>$approve_by,
						'permis_status'=>$permis_status,
						'reason'=>$reason);
			if($permisid!=''){
				$data2=array('modified_date'=>$c_date,
							'modified_by'=>$user);
				$this->db->where('permisid',$permisid);
				$this->db->update('sch_student_permission',array_merge($data,$data2));
			}else{
				$data2=array('created_date'=>$c_date,
							'created_by'=>$user);
				$this->db->insert('sch_student_permission',array_merge($data,$data2));
			}
		}
		function dateDiff ($d1, $d2) {
		// Return the number of days between the two dates:

		  return round(abs(strtotime($d1)-strtotime($d2))/86400);

		}  // end function dateDiff
		function getallstdpermis($studentid,$yearid){
			return $this->db->query("SELECT perm.permisid,
										perm.date,
										perm.studentid,
										perm.classid,
										perm.yearid,
										perm.from_date,
										perm.to_date,
										perm.approve_by,
										perm.permis_status,
										perm.reason,
										perm.created_date,
										perm.created_by,
										perm.modified_date,
										perm.modified_by,
										DATEDIFF(to_date, from_date) + 1 AS num_day
									FROM sch_student_permission perm
									where studentid='$studentid' AND yearid='$yearid'")->result();
		}
		function getstdpermisrow($permisid){
			return $this->db->query("SELECT s.student_num,
										s.studentid,
										s.yearid,
										stdp.permisid,
										s.fullname,
										s.class_name,
										stdp.date,
										stdp.permis_status,
										stdp.classid,
										stdp.from_date,
										stdp.to_date,
										stdp.modified_by,
										stdp.modified_date,
										stdp.approve_by,
										stdp.reason,
										emp.last_name,
										emp.first_name
								FROM sch_student_permission stdp
								INNER JOIN v_student_profile s
								ON(stdp.studentid=s.studentid AND stdp.yearid=s.yearid)
								INNER JOIN sch_emp_profile emp
								ON(stdp.approve_by=emp.empid)
								WHERE stdp.permisid='".$permisid."'")->row();
		}
		function search($student_num,$s_name,$classid,$schlevelid,$emp_name,$sort_num,$m,$p,$sort=''){
			$this->load->library('pagination');	
			$page=0;
			$where='';
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			if($student_num!='')
				$where.=" AND s.student_num LIKE '%".$student_num."%'";
			if($s_name!='')
				$where.=" AND s.fullname LIKE '%".$s_name."%'";
			if($classid!='')
				$where.=" AND stdp.classid='".$classid."'";
			if($schlevelid!='')
				$where.=" AND s.schlevelid='".$schlevelid."'";
			if($emp_name!='')
				$where.=" AND (emp.last_name LIKE '%".$emp_name."%' OR emp.first_name LIKE '%".$emp_name."%')";

		    $yearid=$this->session->userdata('year');
				$sql="SELECT s.student_num,
							s.studentid,
							stdp.permisid,
							s.fullname,
							s.class_name,
							stdp.date,
							stdp.from_date,
							stdp.to_date,
							stdp.reason,
							emp.last_name,
							emp.first_name,
							s.yearid
					FROM sch_student_permission stdp
					INNER JOIN v_student_profile s
					ON(stdp.studentid=s.studentid AND stdp.yearid=s.yearid)
					INNER JOIN sch_emp_profile emp
					ON(stdp.approve_by=emp.empid)
					WHERE stdp.yearid='".$yearid."' {$where}";	
				$config['base_url'] =site_url()."/student/permission/search?pg=1&n=$s_name&num=$student_num&c=$classid&en=$emp_name&s_num=$sort_num&m=$m&p=$p&l=$schlevelid";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =$sort_num;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" $sort {$limi}";
				return $this->db->query($sql)->result();
		}

		// ------------------------------------ start long tha working---------------------------------------
		function getMothlyPerm($yearid,$month,$num_perm){
			/*$sql="SELECT
					DISTINCT
						stuinf.studentid,
						stuinf.classid,
						stuinf.yearid,
						per_count.num_perm
					FROM
						sch_student_permission as stuinf
					LEFT JOIN (SELECT studentid,COUNT(*) as num_perm 
										FROM sch_student_permission att 															
										WHERE MONTH(att.to_date)= '".$month."'
										AND yearid= '".$yearid."' 
										AND permis_status=0 
										GROUP BY studentid 
										HAVING num_perm>= '".$num_perm."') as per_count
					 ON stuinf.studentid=per_count.studentid
					WHERE per_count.num_perm>= '".$num_perm."'
					";*/

			$sql="SELECT
					studentid,
					classid,
					yearid,
					SUM(DATEDIFF(to_date, from_date) + 1) AS num
				FROM
					sch_student_permission att
				WHERE
					yearid = ".$yearid."
				AND MONTH (att.to_date) =".$month."
				-- AND permis_status = 0	
				GROUP BY studentid,
				classid,
				yearid
				HAVING num>=".$num_perm;

			return $this->green->getTable($sql);

		}
		function getYearlyPerm($yearid,$num_perm){
			

			/*$sql="SELECT
					DISTINCT
						stuinf.studentid,
						stuinf.classid,
						stuinf.yearid,
						per_count.num_perm
					FROM
						sch_student_permission as stuinf
					LEFT JOIN (SELECT studentid,COUNT(*) as num_perm 
										FROM sch_student_permission att 															
										WHERE yearid= '".$yearid."' 
										AND permis_status=0 
										GROUP BY studentid 
										HAVING num_perm>= '".$num_perm."') as per_count
					 ON stuinf.studentid=per_count.studentid
					WHERE per_count.num_perm>= '".$num_perm."'
					";*/
			$sql="SELECT
					studentid,
					classid,
					yearid,
					SUM(DATEDIFF(to_date, from_date) + 1) AS num_perm
				FROM
					sch_student_permission att
				WHERE
					yearid = ".$yearid."
				-- AND permis_status = 0						
				GROUP BY studentid,
				classid,
				yearid
				HAVING num_perm>=".$num_perm;
				
			return $this->green->getTable($sql);

		}
		function getMothlyPermOver2($yearid,$month,$num_perm){
			/*SELECT DISTINCT
						att.studentid,
						att.classid,
						att.yearid
					FROM
						sch_student_permission att
					WHERE
						yearid = '".$yearid."'
					AND MONTH (att.to_date) = '".$month."'
					AND DATEDIFF(to_date, from_date) + 1 >= '".$num_perm."'
					GROUP BY
						studentid*/
			$sql="SELECT
					studentid,
					classid,
					yearid,
					SUM(DATEDIFF(to_date, from_date) + 1) AS num
				FROM
					sch_student_permission att
				WHERE
					yearid = ".$yearid."
				AND MONTH (att.to_date) =".$month."
				-- AND permis_status = 0	
				GROUP BY studentid,
				classid,
				yearid
				HAVING num>=".$num_perm;
			//echo $sql;		
			return $this->green->getTable($sql);

		}
	// ------------------------------------ end long tha working---------------------------------------
}