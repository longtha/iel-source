<?php
	class ModGiftrpt extends CI_Model{
		function getalltotal(){
			$yearid=$this->session->userdata('year');
			return $this->db->query("SELECT SUM(quantity) as total,gifname,sgd.unit
					FROM sch_stud_gifdonate_detail sgd
					INNER JOIN sch_student_gif sg 
					ON(sgd.gifid=sg.gifid)
					WHERE sgd.yearid='$yearid'
					GROUP BY sgd.gifid
					ORDER BY total DESC")->result();
		}
		function getdonate(){
				$page=0;
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				$m='';
				$p='';
				if(isset($_GET['m'])){
		        	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }	
			    $yearid=$this->session->userdata('year');
					$sql="SELECT sg.date,
								sg.donate_type,
								sg.donateid,
								sg.transno,
								sg.yearid,
								sgd.studentid,
								s.fullname,
								s.class_name,
								s.sch_year
							FROM sch_stud_gifdonate sg
							INNER JOIN sch_stud_gifdonate_detail sgd
							ON(sg.donateid=sgd.donateid)
							INNER JOIN v_student_profile s
							ON(sgd.studentid=s.studentid AND sgd.yearid=s.yearid)
							WHERE sgd.yearid='$yearid' 
							AND quantity>0
							GROUP BY sgd.studentid,sgd.transno 
							ORDER BY sgd.transno DESC";	
					$config['base_url'] =site_url()."/student/gift_report/listgifstd?pg=1&m=$m&p=$p";	
					$config['total_rows'] = $this->green->getTotalRow($sql);
					$config['per_page'] =50;
					$config['num_link']=5;
					$config['page_query_string'] = TRUE;
					$config['full_tag_open'] = '<li>';
					$config['full_tag_close'] = '</li>';
					$config['cur_tag_open'] = '<a><u>';
					$config['cur_tag_close'] = '</u></a>';
					$this->pagination->initialize($config);
					$limi=" limit ".$config['per_page'];
					if($page>0){
							$limi=" limit ".$page.",".$config['per_page'];
						}		
					$sql.=" {$limi}";
					return $this->db->query($sql)->result();
		}
		function search($s_date,$e_date){
			$where='';
			if($s_date!='' && $e_date!='')
				$where=" AND sgd.date BETWEEN '$s_date' AND '$e_date'";
			return $this->db->query("SELECT SUM(quantity) as total,gifname,sgd.unit
					FROM sch_stud_gifdonate_detail sgd
					INNER JOIN sch_student_gif sg 
					ON(sgd.gifid=sg.gifid)
					WHERE 1=1 {$where}
					GROUP BY sgd.gifid
					ORDER BY total DESC")->result();
		}
		function searchstd($s_date,$e_date,$schlevelid,$classid,$yearid,$gifid,$sort_num,$m,$p){
				$page=0;
				$where='';
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				if($schlevelid!='')
					$where.=" AND sgd.schlevelid='$schlevelid'";
				if($classid!='')
					$where.=" AND sgd.classid='$classid'";
				if($gifid!='')
					$where.=" AND sgd.gifid='$gifid'";
				if($s_date!='' && $e_date!='')
					$where.=" AND sgd.date BETWEEN '$s_date' AND '$e_date'";
				if($yearid!='')
					$where.=" AND sgd.yearid='$yearid'";
				$sql="SELECT sg.date,
							sg.donate_type,
							sg.donateid,
							sg.transno,
							sg.yearid,
							sgd.studentid,
							s.fullname,
							s.class_name,
							s.sch_year
						FROM sch_stud_gifdonate sg
						INNER JOIN sch_stud_gifdonate_detail sgd
						ON(sg.donateid=sgd.donateid)
						INNER JOIN v_student_profile s
						ON(sgd.studentid=s.studentid AND sgd.yearid=s.yearid)
						WHERE 1=1 {$where}
						GROUP BY sgd.studentid,sgd.transno 
						ORDER BY sgd.transno DESC";	
				$config['base_url'] =site_url()."/student/gift_report/searchstd?pg=1&m=$m&p=$p&s=$s_date&e=$e_date&l=$schlevelid&c=$classid&y=$yearid&gid=$gifid&s_num=$sort_num";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =$sort_num;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				return $this->db->query($sql)->result();
		}
	}