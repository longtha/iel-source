<?php
	class modrollover extends CI_Model{
		function getrollover(){
				$page=0;
				$schoolid=$this->session->userdata('schoolid');
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
					$sql="SELECT sr.created_date,
										y.sch_year as from_year,
										y2.sch_year as to_year,
										slv.sch_level,
										sr.schlevelid,
										sr.rolloverid,
										sr.is_verified,
										sr.verified_by,
										sr.verified_date,
										sr.transno,
										c.class_name,
										c.classid,
										schi.name
						FROM sch_student_rollover sr
						INNER JOIN sch_school_year y
						ON(sr.from_yearid=y.yearid)
						INNER JOIN sch_school_year y2
						ON(sr.to_yearid=y2.yearid)
						LEFT JOIN sch_school_level slv
						ON(sr.schlevelid=slv.schlevelid)
						LEFT JOIN sch_class c
						ON(sr.classid=c.classid)
						INNER JOIN sch_school_infor schi
						ON(sr.schoolid=schi.schoolid)
						WHERE sr.schoolid='".$schoolid."'";	
					$config['base_url'] =site_url().'/student/rollover?pg=1';	
					$config['total_rows'] = $this->green->getTotalRow($sql);
					$config['per_page'] =50;
					$config['num_link']=5;
					$config['page_query_string'] = TRUE;
					$config['full_tag_open'] = '<li>';
					$config['full_tag_close'] = '</li>';
					$config['cur_tag_open'] = '<a><u>';
					$config['cur_tag_close'] = '</u></a>';
					$this->pagination->initialize($config);
					$limi=" limit ".$config['per_page'];
					if($page>0){
							$limi=" limit ".$page.",".$config['per_page'];
						}		
					$sql.=" {$limi}";
					return $this->db->query($sql)->result();
		}
		function save(){
			$c_date=date('Y:m:d');
			$user=$this->session->userdata('user_name');
			$from_yearid=$this->session->userdata('year');
			$to_yearid=$this->input->post('to_year');
			$schlevelid=null;
			$classid=null;
			$roll_type=$this->input->post('roll_type');
			$schoolid=$this->session->userdata('schoolid');
			$transno=$this->db->query("SELECT sequence FROM sch_z_systype WHERE typeid=9")->row()->sequence;
			if($roll_type=='schlevel')
				$schlevelid=$this->input->post('roll_schlevel');
			if($roll_type=='class'){
				$classid=$this->input->post('roll_class');
			}
			if(isset($_POST['chkcontract']))
				$this->copycontract($from_yearid,$to_yearid,$transno);
			if(isset($_POST['chktimetable']))
				$this->copytimetable($from_yearid,$to_yearid,$classid,$schlevelid,$transno);//echo 'chktimetable: check';
			//if(isset($_POST['chkdistribute']))
				//$this->copytimetable();//echo 'chktimetable: check';

			$this->copystdclass($from_yearid,$to_yearid,$classid,$schlevelid,$transno);	
			$data=array('created_date'=>$c_date,
						'created_by'=>$user,
						'from_yearid'=>$from_yearid,
						'to_yearid'=>$to_yearid,
						'schoolid'=>$schoolid,
						'schlevelid'=>$schlevelid,
						'type'=>9,
						'transno'=>$transno,
						'classid'=>$classid);
			$this->db->insert('sch_student_rollover',$data);
			$this->updatetran($transno+1);
			
		}
		function updatetran($transno){
			$this->db->where('typeid',9)
					->set('sequence',$transno)
					->update('sch_z_systype');
		}
		function getyear(){
			return $this->db->get('sch_school_year')->result();
		}
		function getschlevel(){
			return $this->db->get('sch_school_level')->result();
		}
		function copystdclass($fromyear,$toyear,$class,$schlevelid,$transno){
			$minscore=5;
			$must_final_exam=0;	
			foreach ($this->getstdenroll($fromyear,$class,$schlevelid) as $std) {
				$stdinfo=$this->getyearaverage($std->classid,$std->year,$std->studentid);
				$newclass=$this->getnewclassid($std->classid);
				$newclassid=$newclass->classid;
				#=== check student pass or fail ========
				
				/*$avg=number_format($stdinfo->avg/2, 2, '.', '');				
				if($avg<$minscore)
					$newclassid=$std->classid;*/
				#=======================================
				if($must_final_exam==0){
					if($this->validatestd($toyear,$newclassid,$std->studentid)==0 ){
						$this->savenewstd($std->studentid,$std->schoolid,$toyear,$newclassid,$newclass->schlevelid,$transno);
					}
				}else {
					if($this->validatestd($toyear,$newclassid,$std->studentid)==0 && $stdinfo->count>=2){
						$this->savenewstd($std->studentid,$std->schoolid,$toyear,$newclassid,$newclass->schlevelid,$transno);
					}
				}
			}
		}
		function getyearaverage($classid,$yearid,$studentid){
			return $this->db->query("SELECT SUM(sev.sem_avg) as avg,T1.count 
									FROM sch_stud_evalsemester sev,
											(select count(semid) as count 
											from sch_stud_evalsemester 
											where studentid='$studentid'
											AND yearid='$yearid'
											AND classid='$classid'
											GROUP BY studentid,yearid,classid	
											) as T1 
									WHERE sev.studentid='$studentid'
									AND sev.yearid='$yearid'
									AND sev.classid='$classid'
									GROUP BY studentid,yearid,classid
									")->row();
		}
		function copycontract($fromyear,$toyear,$transno){
			foreach ($this->db->where('year',$fromyear)->get('sch_emp_contract')->result() as $emp) {
				if($this->validateemp($emp->empid,$toyear)==0)
					$this->savecontract($emp->contractid,$emp->empid,$emp->contract_type,$emp->contract_attach,$emp->begin_date,$emp->end_date,$toyear,$emp->job_type,$emp->decription,$transno);
			}
		}
		function copytimetable($fromyear,$toyear,$class,$schlevelid,$transno){
			foreach ($this->gettimetable($fromyear,$class,$schlevelid) as $time) {
				$data=array('dayid'=>$time->dayid,
							'classid'=>$time->classid,
							'subjectid'=>$time->subjectid,
							'timeid'=>$time->timeid,
							'teacherid'=>$time->teacherid,
							'note'=>$time->note,
							'year'=>$toyear,
							'schlevelid'=>$time->schlevelid,
							'schoolid'=>$time->schoolid,
							'type'=>9,
							'transno'=>$transno,
							'is_leave_time'=>$time->is_leave_time);
				$count=$this->validatetime($time->classid,$time->schlevelid,$time->schoolid,$toyear,$time->subjectid,$time->dayid,$time->timeid);
				if($count==0)
					$this->db->insert('sch_time_table',$data);
			}
		}
		function validatetime($classid,$schlevelid,$schoolid,$year,$subjectid,$dayid,$timeid){
			return $this->db->select('count(*)')
						->from('sch_time_table')
						->where('classid',$classid)
						->where('schlevelid',$schlevelid)
						->where('year',$year)
						->where('subjectid',$subjectid)
						->where('dayid',$dayid)
						->where('timeid',$timeid)
						->where('schoolid',$schoolid)->count_all_results();
		}
		function copydistribut(){

		}
		function savecontract($contract_id,$empid,$contract_type,$contract_attach,$begin_date,$end_date,$year,$job_type,$desc,$transno){
			$data=array('contractid'=>$contract_id,
						'empid'=>$empid,
						'contract_type'=>$contract_type,
						'contract_attach'=>$contract_attach,
						'begin_date'=>$begin_date,
						'end_date'=>$end_date,
						'job_type'=>$job_type,
						'type'=>9,
						'transno'=>$transno,
						'decription'=>$desc,
						'year'=>$year);
			$this->db->insert('sch_emp_contract',$data);

		}
		function validateemp($empid,$yearid){
			return $this->db->select('count(*)')->from('sch_emp_contract')->where('empid',$empid)->where('year',$yearid)->count_all_results();
		}
		function getstdenroll($year,$class,$schlevelid){
			$this->db->where('year',$year);
			if($class>0 || $class!='')
				$this->db->where('classid',$class);
			if($schlevelid>0 || $schlevelid!='')
				$this->db->where('schlevelid',$schlevelid);
			return $this->db->get('sch_student_enrollment')->result();
		}
		function gettimetable($year,$class,$schlevelid){
			$this->db->where('year',$year);
			if($class>0 || $class!='')
				$this->db->where('classid',$class);
			if($schlevelid>0 || $schlevelid!='')
				$this->db->where('schlevelid',$schlevelid);
			return $this->db->get('sch_time_table')->result();
		}
		function validatestd($year,$classid,$studentid){
			return $this->db->select('count(*)')->from('sch_student_enrollment')->where('studentid',$studentid)->where('year',$year)->where('classid',$classid)->count_all_results();
		}
		function getnewclassid($classid){
			$class=$this->db->where('classid',$classid)->get('sch_class')->row();
			$graderow=$this->db->where('grade_levelid',$class->grade_levelid)->get('sch_grade_level')->row();
			return $this->db->where('grade_levelid',$graderow->next_grade_level)->where('grade_labelid',$class->grade_labelid)->get('sch_class')->row();
		}
		function savenewstd($studentid,$schoolid,$yearid,$classid,$schlevelid,$transno){
			$data=array('studentid'=>$studentid,
						'schoolid'=>$schoolid,
						'year'=>$yearid,
						'classid'=>$classid,
						'schlevelid'=>$schlevelid,
						'type'=>9,
						'transno'=>$transno,
						'enroll_date'=>date('Y:m:d'));
			$this->db->insert('sch_student_enrollment',$data);
		}
		function search($f_year,$t_year,$schlevel,$classid,$m,$p,$sort_num){
				$page=0;
				$where='';
				$schoolid=$this->session->userdata('schoolid');
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				if($f_year!='')
					$where.=" AND sr.from_yearid='".$f_year."'";
				if($t_year!='')
					$where.=" AND sr.to_yearid='".$t_year."'";
				if($schlevel!='')
					$where.=" AND (sr.schlevelid='".$schlevel."' OR cl.schlevelid='$schlevel')";
				
				if($classid!='')
					$where.=" AND sr.classid='".$classid."'";
					$sql="SELECT sr.created_date,
										y.sch_year as from_year,
										y2.sch_year as to_year,
										slv.sch_level,
										sr.schlevelid,
										sr.rolloverid,
										c.class_name,
										c.classid,
										schi.name
						FROM sch_student_rollover sr
						INNER JOIN sch_school_year y
						ON(sr.from_yearid=y.yearid)
						INNER JOIN sch_school_year y2
						ON(sr.to_yearid=y2.yearid)
						LEFT JOIN sch_school_level slv
						ON(sr.schlevelid=slv.schlevelid)
						LEFT JOIN sch_class c
						ON(sr.classid=c.classid)
						INNER JOIN sch_school_level cl
						ON(c.schlevelid=cl.schlevelid)
						INNER JOIN sch_school_infor schi
						ON(sr.schoolid=schi.schoolid)
						WHERE sr.schoolid='".$schoolid."' {$where}";	
					$config['base_url'] =site_url()."/student/rollover?pg=1&fy=$f_year&ty=$t_year&sl=$schlevel&c=classid&s_num=$sort_num&m=$m&p=$p";	
					$config['total_rows'] = $this->green->getTotalRow($sql);
					$config['per_page'] =$sort_num;
					$config['num_link']=5;
					$config['page_query_string'] = TRUE;
					$config['full_tag_open'] = '<li>';
					$config['full_tag_close'] = '</li>';
					$config['cur_tag_open'] = '<a><u>';
					$config['cur_tag_close'] = '</u></a>';
					$this->pagination->initialize($config);
					$limi=" limit ".$config['per_page'];
					if($page>0){
							$limi=" limit ".$page.",".$config['per_page'];
						}		
					$sql.=" {$limi}";
					return $this->db->query($sql)->result();
		}

	}