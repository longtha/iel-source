<?php
	class m_student_attendent extends CI_Model{

		function __construct(){
			parent::__construct();

		}

		// getprograms --------------------------------------
		function getprograms($schoolid="",$programid=""){
	        if($schoolid!=""){
	            $this->db->where("schoolid",$schoolid);
	        }
	        if($programid!=""){
	            $this->db->where("programid",$programid);
	        }
	        return $this->db->get("sch_school_program")->result() ;
	    }

	    // get_schlevel --------------------------------------
	    function get_schlevel($programid){
	        $qr_schlevel = $this->db->query("SELECT DISTINCT
	                                                l.schlevelid,
	                                                l.sch_level
	                                            FROM
	                                                sch_school_level AS l
	                                            WHERE
	                                                l.programid = '{$programid}'
	                                            ORDER BY
	                                                l.sch_level ASC ")->result();        
	        
	        $arr = array('schlevel' => $qr_schlevel);
	        return json_encode($arr);
	    }

	    // get_schyera --------------------------------------
	    function get_year($schlevelids = ''){
	        $qr_schyear = $this->db->query("SELECT DISTINCT
	                                                y.yearid,
	                                                y.sch_year
	                                            FROM
	                                                sch_school_year AS y
	                                            WHERE
	                                                y.schlevelid = '{$schlevelids}' - 0
	                                            ORDER BY
	                                                y.sch_year ASC ")->result();        
	        
	        $arr = array('year' => $qr_schyear);
	        return json_encode($arr);
	    }

	    //from classname -----------------------------------
	    function get_class($schlevelids = ''){
	        $qr_schclass = $this->db->query("SELECT DISTINCT
													sl.schlevelid,
													cl.class_name,
													cl.classid
												FROM
													sch_school_level AS sl
												LEFT  JOIN sch_class AS cl ON sl.schlevelid = cl.schlevelid
												AND sl.schlevelid = cl.schlevelid
												WHERE
													sl.schlevelid = '{$schlevelids}'
												ORDER BY
													cl.class_name ASC ")->result();        
	        
	        $arr = array('schclass' => $qr_schclass);
	        return json_encode($arr);
	    }

	    // get_school --------------------------------------
	    function get_school($program){
	        $qr_schlevel = $this->db->query("SELECT DISTINCT
	                                                l.schlevelid,
	                                                l.sch_level
	                                            FROM
	                                                sch_school_level AS l
	                                            WHERE
	                                                l.programid = '{$program}'
	                                            ORDER BY
	                                                l.sch_level ASC ")->result();        
	        
	        $arr = array('school' => $qr_schlevel);
	        return json_encode($arr);
	    }

	    // get_schyera --------------------------------------
	    function get_yearid($school = ''){
	        $qr_schyear = $this->db->query("SELECT DISTINCT
	                                                y.yearid,
	                                                y.sch_year
	                                            FROM
	                                                sch_school_year AS y
	                                            WHERE
	                                                y.schlevelid = '{$school}' - 0
	                                            ORDER BY
	                                                y.sch_year ASC ")->result();        
	        
	        $arr = array('yearid' => $qr_schyear);
	        return json_encode($arr);
	    }

	    //from classname -----------------------------------
	    function get_toclass($school = ''){
	        $qr_schclass = $this->db->query("SELECT DISTINCT
													sl.schlevelid,
													cl.class_name,
													cl.classid
												FROM
													sch_school_level AS sl
												LEFT  JOIN sch_class AS cl ON sl.schlevelid = cl.schlevelid
												AND sl.schlevelid = cl.schlevelid
												WHERE
													sl.schlevelid = '{$school}'
												ORDER BY
													cl.class_name ASC ")->result();        
	        
	        $arr = array('toschclass' => $qr_schclass);
	        return json_encode($arr);
	    }
	    // delete --------------------------------------------
	    function delete($permisid = ''){
			$this->db->delete('sch_student_permission', array('permisid' => $permisid));
			$data['msg'] = 'deleted';
	        return json_encode($data);
		}

		// edit ----------------------------------------------
		function edit($permisid = ''){

			$row = $this->db->query("SELECT
										atd.permisid,
										atd.schlevelid,
										CONCAT(last_name_,'  ',first_name_) AS student,
										atd.studentid,
										CONCAT(last_name,'  ',first_name) AS Approveby,
										atd.classid,
										atd.programid,
										atd.permis_status,
										atd.reason,
										DATE_FORMAT(from_date, '%d/%m/%Y') AS from_date,
										DATE_FORMAT(to_date, '%d/%m/%Y') AS to_date,
										atd.yearid,
										atd.program,
										atd.sch_level,
										atd.sch_year,
										atd.class_name
									FROM
										v_student_attendent AS atd
									WHERE 
										atd.permisid = '{$permisid}' ")->row();

	        return json_encode($row);
		}
		
	    // save ---------------------------------------
	    function save($permisid=''){
			$programid=$this->input->post('programid');
			$schlevelids=$this->input->post('schlevelids');
			$yearid=$this->input->post('yearid');
			$classid=$this->input->post('classid');
			$studentid=$this->input->post('studentid');
			$approve_by=$this->input->post('approve_by');
			$permis_status=$this->input->post('permis_status');
			$fromdate=trim($this->input->post('fromdate'));
			$todate=trim($this->input->post('todate'));
			$reason=$this->input->post('reason');
			$c_date=date('Y-m-d');
			$date = date('y-m-d');


			$from_m = date("d", strtotime($fromdate)) - 0;
			$to_m = date("d", strtotime($todate)) - 0;

			$fromdate_ = explode(' ', $fromdate);
			$fromdate__ = explode('-', $fromdate_[0]);

			//print_r($fromdate_[0]);

			for($i = 0; $i < ($to_m - $from_m + 1) - 0; $i++) {
		
				$user=$this->session->userdata('user_name');

				$data=array('date'=>$date,
							'programid'=>$programid,
							'schlevelid'=>$schlevelids,
							'classid'=>$classid,
							'yearid'=>$yearid,
							'studentid'=>$studentid,
							'from_date'=> $fromdate,
							'to_date'=>$todate,
							'approve_by'=>$approve_by,
							'permis_status'=>$permis_status,
							'reason'=>$reason);

				if($permisid!=''){
					$data2=array('modified_date'=>$c_date,
								'modified_by'=>$user);
					$this->db->where('permisid',$permisid);
					$this->db->update('sch_student_permission',array_merge($data,$data2));
				}else{
					$data2=array('created_date'=>$c_date,
								'created_by'=>$user);
					$this->db->insert('sch_student_permission',array_merge($data,$data2));
				}


				 return $permisid;

			}// for ====

		}// fSave ======



	}