<?php
    class m_evaluate_trans_kgp extends CI_Model{

    	//get student information
    	function getStudentInformation($where)
    	{
    		$sql = $this->db->query("SELECT DISTINCT								
										s.studentid,
										s.student_num,
										s.first_name,
										s.last_name,
										s.first_name_kh,
										s.last_name_kh,
										s.gender
									FROM
										sch_school_level AS sl
										INNER JOIN sch_school_program AS sp ON sp.programid = sl.programid
										INNER JOIN sch_school_year AS sy ON sl.schlevelid = sy.schlevelid
										INNER JOIN sch_class AS sc ON sl.schlevelid = sc.schlevelid
										INNER JOIN sch_student_enrollment AS se ON sc.classid = se.classid 
										INNER JOIN sch_student AS s ON s.studentid = se.studentid
									WHERE 1 = 1 ". $where ." ORDER BY s.studentid");
    		return $sql->result();
    	}

    	//get evaluate name for each student
    	function getEvaluateName()
    	{
    		$this->db->select('*');
			$this->db->from('sch_student_evaluate_setup_kgp');
			$this->db->where('is_active', 1); 
			$query = $this->db->get()->result();
			return $query;
    	}

    	//check student evaluate
    	function checkStudentEvaluate($academic_year_id, $class_id, $student_id, $semester){
    		$this->db->select('e.evaluate_name_kh,
							   	e.evaluate_name_eng,
							   	s.evaluate_tran_id,
							   	s.evaluate_id,
								s.class_id,
								s.student_id,								
								s.seldom,
								s.sometimes,
								s.usuall,
								s.consistenly,
								s.comment');
    		$this->db->from('sch_student_evaluate_trans_kgp as s');
    		$this->db->join('sch_student_evaluate_setup_kgp as e', 's.evaluate_id = e.evaluate_id');
    		$this->db->where('s.academic_year_id', $academic_year_id);
    		$this->db->where('s.class_id', $class_id);
    		$this->db->where('s.student_id', $student_id);
    		$this->db->where('s.semester', $semester);
    		return $this->db->get()->result();
		}

    	//save evaluate for each student
    	function saveEvaluate(){    		
			$class_id = $this->input->post('class_id');
			$academic_year_id = $this->input->post('academic_year_id');
			$grade_level_id = $this->input->post('grade_level_id');
			$school_level_id = $this->input->post('school_level_id');
			$program_id = $this->input->post('program_id');
			$semester = $this->input->post('semester');
			$student_array = $this->input->post('student_array');
			//print_r($student_array);

			$mess = "";
			if(count($student_array) > 0 ){
				foreach($student_array as $r_stu){
					$evaluate_tran_id = ($r_stu["evaluate_tran_id"] > 0?$r_stu["evaluate_tran_id"] : 0);
					$data = array(
									'evaluate_id' => $r_stu["evaluate_id"],
									'seldom' => $r_stu["seldom"],
									'sometimes' => $r_stu["sometimes"],
									'usuall' => $r_stu["usuall"],
									'consistenly' => $r_stu["consistenly"],
									'student_id' => $r_stu["student_id"],
									'class_id' => $class_id,
									'academic_year_id' => $academic_year_id,
									'grade_level_id' => $grade_level_id,
									'school_level_id' => $school_level_id,
									'program_id' => $program_id,
									'semester' => $semester,
									'comment' => $r_stu["comment"]
								);
					if($evaluate_tran_id > 0){
						$this->db->where('evaluate_tran_id', $evaluate_tran_id);
						$this->db->update('sch_student_evaluate_trans_kgp',$data);
						$mess = 2;
					}else{
						$this->db->insert('sch_student_evaluate_trans_kgp',$data);
						$mess = 1;
					}
				}				
			}

			return $mess;
    	}

    }
?>