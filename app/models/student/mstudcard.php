<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mstudcard extends CI_Model {
	public function priviewstudcard($studentid='',$yearid,$classid='',$multi_class=0)
	{
		$where='';
		if($studentid!=''){
			$where.=" AND (s.student_num like '".$studentid."%'
							OR s.last_name like '".$studentid."%'
							OR s.first_name like '".$studentid."%'
							OR s.last_name_kh like '".$studentid."%'
							OR s.first_name_kh like '".$studentid."%'
						)";
		}
		if($classid!='' && $multi_class==0){
			$where.=" AND c.classid='".$classid."'";
		}
		if($yearid !=""){
			$where.=" AND se.year='".$yearid."'";
		}

		return $this->makesqlsyntax($where);
	}
	public function makesqlsyntax($where)
	{
		$sql="SELECT 
					f.family_code,
					s.studentid,
					s.student_num,
					s.last_name,
					s.first_name,
					s.last_name_kh,
					s.first_name_kh,
					s.familyid,
					c.classid,
					c.class_name,
					LENGTH(CONCAT(s.last_name,' ',s.first_name)) as len
			FROM sch_student s 
			LEFT JOIN sch_family f 
			ON (s.familyid=f.familyid)
			LEFT JOIN sch_student_enrollment se
			ON(s.studentid=se.studentid)
			INNER JOIN sch_class c 
			ON (se.classid=c.classid)
			WHERE 1=1 {$where}
			ORDER BY s.last_name_kh ASC";
		return $this->db->query($sql);
	}
}