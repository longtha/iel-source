<?php
	class Modevaluate extends CI_Model{

		function getevaluate(){
			$page=0;
			$yearid=$this->session->userdata('year');
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m='';
			$p='';
			if(isset($_GET['m'])){
		        $m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }	
				$sql="SELECT * FROM sch_student_evaluated se
						INNER JOIN v_student_profile s
						ON(se.studentid=s.studentid AND s.yearid=se.yearid)
						WHERE  se.yearid='$yearid' AND se.evaluate_type='Three Month'
						";
				$config['base_url'] =site_url()."/student/evaluate?pg=1&m=$m&p=$p";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =50;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.="  ORDER BY se.transno DESC, s.last_name_kh,se.transno desc {$limi}";
				return $this->db->query($sql)->result();
		}
		function getresult(){
				$page=0;
				$yearid=$this->session->userdata('year');
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				$m='';
				$p='';
				if(isset($_GET['m'])){
			        $m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }	
				$sql="SELECT eval.studentid,
								eval.classid,
								s.class_name,
								s.fullname,
								eval.yearid,
								count(eval.studentid) as stdtimes,
								group_concat(eval.sem_avg) as avg,
								sum(eval.sem_avg) as total
					FROM sch_stud_evalsemester eval
					INNER JOIN v_student_profile s
					ON(eval.studentid=s.studentid AND eval.classid=s.classid)
					WHERE eval.yearid='$yearid' 
					GROUP BY eval.studentid";	
				$config['base_url'] =site_url()."/student/evaluate?pg=1&m=$m&p=$p";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =50;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				return $this->db->query($sql)->result();
		}
		function getevasemester(){
			$page=0;
			$yearid=$this->session->userdata('year');
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m='';
			$p='';
			if(isset($_GET['m'])){
		        $m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }	
				$sql="SELECT * FROM sch_stud_evalsemester se
						INNER JOIN v_student_profile s
						ON(se.studentid=s.studentid AND s.yearid=se.yearid)
						WHERE  se.yearid='$yearid'";	
				$config['base_url'] =site_url()."/student/evaluate/semesterlist?pg=1&m=$m&p=$p";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =50;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.="  Order By se.transno desc {$limi}";
				return $this->db->query($sql)->result();
		}
		function save(){
			$c_date 			   = date('Y-m-d');
			$schoolid 			   = $this->session->userdata('schoolid');
			$user 				   = $this->session->userdata('user_name');
			$studentid 			   = $this->input->post('studentid');
			$cmd 				   = $this->input->post('cmd');
			$upd    			   = $this->input->post('upd');

			$kh_teacher_comment    = $this->input->post('teacher_kh_cmd');
			$forign_techer_comment = $this->input->post('teacher_f_cmd');
			$france_techer_comment = $this->input->post('teacher_fr_cmd');
			$technical_cmd     	   = $this->input->post('technical_cmd');
			$sp_cmd 		  	   = $this->input->post('sp_cmd');

			$cbpmention 	  	   = $this->input->post('txtmention');
			$classid 		  	   = $this->input->post('classid');
			$promot_id 		  	   = $this->input->post('promotionid');
			// echo "Pro:".$promot_id;
			// die();
			$date 			  	   = $this->green->convertSQLDate($this->input->post('eva_date'));
			$month 			  	   = $this->input->post('eva_months');
			$evaluate_type    	   = $this->input->post('eva_type');

			$kh_teacher       	   = $this->input->post('teacher_khid');
			$forign_teacher   	   = $this->input->post('teacher_enid');
			$fra_teacher	  	   = $this->input->post('teacher_frid');
			$transno 		  	   = $this->input->post('transno');
			if($transno==""){
				$transno=$this->green->nextTran("2","Evaluate Student");
			}
			$other_month_tran 	   = $this->input->post('eva_transno');

			$eval_semester='';
			
			if($evaluate_type!='month'){
				$eval_semester=$this->input->post('eva_semester');
			}
			$sem_transno=0;
			for($i=0;$i<count($studentid);$i++){
				$yearid=$this->db->select('year')->from('sch_student_enrollment')->where('studentid',$studentid[$i])->where('classid',$classid)->get()->row()->year;
				$level=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
				$data=array('class_comment'			 => $cmd,
							'studentid'				 => $studentid[$i],
							'classid'				 => $classid,
							'promotion_id'			 => $promot_id,
							'yearid'				 => $yearid,
							'date'					 => $date,
							'month'					 => trim($month,','),
							'evaluate_type'			 => $evaluate_type,
							'kh_teacher_comment' 	 => $kh_teacher_comment[$i],
							'forign_teacher_comment' => $forign_techer_comment[$i],
							'eng_teach_com'			 => $france_techer_comment[$i],
							'technic_com'			 => $technical_cmd[$i],
							'supple_com'			 => $sp_cmd[$i],
							'eng_teach_name'		 => $fra_teacher,
							'kh_teacher'			 => $kh_teacher,
							'eval_semester'			 => $eval_semester,
							'schoolid'				 => $schoolid,
							'forign_teacher'		 => $forign_teacher,
							'schlevelid'			 => $level
							);
				$count=$this->evaluate->validate($studentid[$i],$transno);
				if($count>0){
					$data2=array('modified_date'=>$c_date,'modified_by'=>$user);
					$this->db->where('studentid',$studentid[$i])->where('transno',$transno)->update('sch_student_evaluated',array_merge($data,$data2));
					$evaluateid=$this->db->where('transno',$transno)->where('studentid',$studentid[$i])->get('sch_student_evaluated')->row()->evaluateid;
					$this->clearmention($evaluateid);
				}else{
					$data2=array('created_date'=>$c_date,'created_by'=>$user,'transno'=>$transno);
					$this->db->insert('sch_student_evaluated',array_merge($data,$data2));
					$evaluateid=$this->db->insert_id();
					//if($upd=='')
						//$this->updatetran($transno+1);
				}
				$this->savesubjment($transno,$evaluateid,$i,$studentid[$i],$classid,$yearid,$evaluate_type,$eval_semester,trim($month,','),$other_month_tran);

			}
            return $transno;
		}
		function savesubjment($transno,$evaluateid,$i,$studentid,$classid,$yearid,$type,$eval_semester,$months,$month_tran){
			$countsub=0;
			$totalscore=0;
			$months_avg=0;
			$each_months_avg=0;
			$trimester=0;
	   		if($type=='Three Month')
				$trimester=1;
			foreach ($this->gets_type($classid,$yearid,$trimester) as $sub) {
				foreach ($this->getsubject($classid,$sub->subj_type_id,$yearid,$trimester) as $subject) {
					$mention='';
					$countsub++;
					$score=$this->input->post("$subject->subjectid");
					$level=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
					if($type=='Three Month'){
						$mention=$score[$i];
					}else{
						$mention=$this->getstdmention($score[$i],$level)->mention;
						$totalscore+=$score[$i];
					}// echo "+ $subject->subject:".$mention[$i].'<br/>';
					$datamen=array('evaluateid'=>$evaluateid,
									'subjectid'=>$subject->subjectid,
									'mention'=>$mention,
									'score'=>$score[$i],
									'type'=>2,
									'transno'=>$transno,
									'studentid'=>$studentid,
									'classid'=>$classid,
									'yearid'=>$yearid,
									'schlevelid'=>$level
									);
					$this->db->insert('sch_student_evaluated_mention',$datamen);
				}
			}
			if($type=='semester'){
				$srtM=$month_tran;
				$month=explode(",", $srtM);
				// print_r($month);
				$month_num=count($month)-1;//get number of months
				for($i=0;$i<$month_num;$i++) {
					$each_months_avg+=($this->evaluate->getstdscore($month[$i],$studentid)->score)/$countsub;
					//echo "$countsub each month :".$each_months_avg."/tr:".$month[$i]." /std/$studentid <br>";
				}
				$months_avg=number_format($each_months_avg/$month_num, 2, '.', '');//get month average
				$sem_exam_avg=number_format($totalscore/$countsub, 2, '.', '');//get average of semester exam
				$sem_avg=number_format(($months_avg+$sem_exam_avg)/2, 2, '.', '');
				$this->savesemester($transno,$studentid,$classid,$yearid,$eval_semester,$months,$sem_exam_avg,$months_avg,$month_num,$sem_avg);
			}
		}
		function savesemester($sem_transno,$studentid,$classid,$yearid,$semester,$months,$sem_exam_avg,$months_avg,$month_num,$sem_avg,$sem_rank=''){
			$count=$this->validatesem($studentid,$sem_transno);
			$sem_result='';
			$sem_mention='';
			$level=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
			$mention=$this->getstdmention($sem_avg,$level)->mention;
			//echo $mention.'<br>';
			if($sem_avg>=5)
				$sem_result='Pass';
			else
				$sem_result='Fail';
			$c_date=date('Y-m-d');
			$user=$this->session->userdata('user_name');
			$data=array('studentid'=>$studentid,
						'classid'=>$classid,
						'yearid'=>$yearid,
						'semester'=>$semester,
						'months'=>$months,
						'sem_exam_avg'=>$sem_exam_avg,
						'months_avg'=>$months_avg,
						'month_num'=>$month_num,
						'sem_avg'=>$sem_avg,
						'sem_mention'=>$mention,
						'sem_result'=>$sem_result,
						'sem_rank'=>$sem_rank,
						'type'=>2,
						'schlevelid'=>$level
						);
			if($count>0){
				$data2=array('modified_date'=>$c_date,'modified_by'=>$user);
				$this->db->where('studentid',$studentid)->where('transno',$sem_transno)->update('sch_stud_evalsemester',array_merge($data,$data2));
			}else{
				$data2=array('created_date'=>$c_date,'created_by'=>$user,'transno'=>$sem_transno);
				$this->db->insert('sch_stud_evalsemester',array_merge($data,$data2));
			}
		}
		function validatesem($studentid,$tran){
			return $this->db->select('count(*)')
						->from('sch_stud_evalsemester')
						->where('transno',$tran)
						->where('studentid',$studentid)
						->count_all_results();
		}
		function updatetran($val){
			$this->db->set('sequence',$val)->where('typeid',2)->update('sch_z_systype');
		}
		function gettransno(){
			return $this->db->where('typeid',2)->get('sch_z_systype')->row()->sequence;
		}
		function getscoremention($schlevelid){
			return $this->db->where('schlevelid',$schlevelid)
						->where('is_final_ses',1)
						->order_by('mention','asc')
						->get('sch_score_mention')->result();
		}
		function getteacherbyclass($classid,$yearid){
			return $this->db->select('emp.empid,emp.last_name,emp.first_name')
							->from('sch_teacher_class tc')
							->join('sch_emp_profile emp','tc.teacher_id=emp.empid','inner')
							->where('tc.class_id',$classid)->where('tc.yearid',$yearid)->get()->result();

		}
		function getmention($evaluateid,$subjectid){
			return $this->db->where('evaluateid',$evaluateid)
							->where('subjectid',$subjectid)
							->get('sch_student_evaluated_mention')->row();
		}
		function clearmention($evaluateid){
			$this->db->where('evaluateid',$evaluateid)->delete('sch_student_evaluated_mention');
		}
		function getrank($transno,$evaluateid){
			$data="SELECT sum(em.score) as score,em.evaluateid
					FROM sch_student_evaluated se
					INNER JOIN sch_student_evaluated_mention em
					ON(se.evaluateid=em.evaluateid)
					WHERE se.transno='".$transno."'
					GROUP BY em.evaluateid
					ORDER BY score desc";
			$rank=1;
			$i=0;
			$score=0;
			foreach ($this->db->query($data)->result() as $row) {
				$i++;
				if($row->score<$score){
					$rank=$i;
				}
				if($row->evaluateid==$evaluateid)
						return $rank;
				$score=$row->score;
			}
		}
		function getsemrank($transno,$semid){
			$data="SELECT sem_avg as score,semid
					FROM sch_stud_evalsemester
					WHERE transno='".$transno."'
					ORDER BY score desc";
			$rank=1;
			$i=0;
			$score=0;
			foreach ($this->db->query($data)->result() as $row) {
				$i++;
				if($row->score<$score){
					$rank=$i;
				}
				if($row->semid==$semid)
						return $rank;
				$score=$row->score;
			}
		}
		function getaverage($transno,$evaluateid){
			$data="SELECT SUM(em.score) as score
						FROM sch_student_evaluated se
						INNER JOIN sch_student_evaluated_mention em
						ON(se.evaluateid=em.evaluateid)
						WHERE se.transno='".$transno."'
						AND em.evaluateid='".$evaluateid."'";
			return $this->db->query($data)->row();
		}
		function getmonthpermis($studentid,$month){
			return $this->db->query("SELECT 
									studentid,
									permis_status,
								 	SUM(DATEDIFF(to_date,from_date) +1 )as days
								FROM
								sch_student_permission
								WHERE DATE_FORMAT(sch_student_permission.to_date,'%b')='$month'
								AND studentid='$studentid'
								GROUP BY permis_status")->result();
		}
		function getstdscore($transno,$studentid){
			$data="SELECT SUM(em.score) as score
						FROM sch_student_evaluated se
						INNER JOIN sch_student_evaluated_mention em
						ON(se.evaluateid=em.evaluateid)
						WHERE se.transno='".$transno."'
						AND em.studentid='".$studentid."'";
			return $this->db->query($data)->row();
		}
		function khmonth($month){
			$khmonth='';
			if($month=='Jan')
				$khmonth='មករា';
			if($month=='Feb')
				$khmonth='គុម្ភ:';
			if($month=='Mar')
				$khmonth='មិនា';
			if($month=='Apr')
				$khmonth='មេសា';
			if($month=='May')
				$khmonth='ឧសភា';
			if($month=='jun')
				$khmonth='មិថុនា';
			if($month=='Jul')
				$khmonth='កក្កដា';
			if($month=='Aug')
				$khmonth='សីហា';
			if($month=='Sep')
				$khmonth='កញ្ញា';
			if($month=='Oct')
				$khmonth='តុលា';
			if($month=='Nov')
				$khmonth='វិច្ឆិកា';
			if($month=='Dec')
				$khmonth='ធ្នូ';
			return $khmonth;
		}
		function getedit($transno){
			return $this->db->query("SELECT DISTINCT transno,
													date,
													yearid,
													evaluate_type,
													modified_by,
													modified_date,
													eval_semester,
													classid,
													kh_teacher,
													forign_teacher,
													class_comment,
													eng_teach_name,
													month,
													promotion_id
													FROM sch_student_evaluated
													Where transno='$transno'")->row();
		}
		function validate($stu,$tra){
			return $this->db->select('count(*)')
						->from('sch_student_evaluated')
						->where('transno',$tra)
						->where('studentid',$stu)
						->count_all_results();
		}
		function getmonthclass($classid,$yearid){
			return $this->db->query("SELECT DISTINCT(month),transno 
					FROM sch_student_evaluated 
					where classid='".$classid."'
					AND yearid='".$yearid."'
					AND evaluate_type='month'")->result_array();
		}
		
		function getstdmention($score,$grade_levelid){
			return $this->db->where('min_score <=',$score)
							->where('max_score >=',$score)
							->where('schlevelid',$grade_levelid)
							->get('sch_score_mention')->row();
		}
		function gets_type($classid,$yearid,$trim){
			$sch=$this->session->userdata('schoolid');
			$where='';
			$where.=" AND sb.is_trimester_sub='$trim'";
			if($trim==0)
				$where.=" AND sb.is_eval='1'";
			return $this->db->query("SELECT st.subject_type,st.subj_type_id,count(sb.subject) s_total FROM sch_class c
									INNER JOIN sch_level_subject_detail sld
									ON(c.grade_levelid=sld.grade_levelid)
									INNER JOIN sch_subject sb
									ON(sb.subjectid=sld.subjectid)
									INNER JOIN sch_subject_type st
									ON(sb.subj_type_id=st.subj_type_id)
									WHERE c.classid='$classid'
									AND sld.year='$yearid'
									AND sld.schoolid='$sch'
									{$where}
									GROUP BY st.subj_type_id
									ORDER BY st.orders
									")->result();
		}
		function getsubject($classid,$subject_type,$yearid,$trimester){
			$sch=$this->session->userdata('schoolid');
			$where='';
			$where.=" AND sb.is_trimester_sub='$trimester'";
			if($trimester==0)
				$where.=" AND sb.is_eval='1'";
			return $this->db->query("SELECT sb.subject,sb.short_sub,sb.subjectid FROM sch_class c
									INNER JOIN sch_level_subject_detail sld
									ON(c.grade_levelid=sld.grade_levelid)
									INNER JOIN sch_subject sb
									ON(sb.subjectid=sld.subjectid)
									WHERE c.classid='$classid'
									AND sld.year='$yearid'
									AND sld.schoolid='$sch'
									{$where}
									AND sb.subj_type_id='$subject_type'
									ORDER  BY sb.orders
									")->result();
		}
		function getstudent($classid,$year,$promot_id=''){
			$where='';
			if($promot_id!=''){
				$where .=" AND promot_id='$promot_id'";
			}
			return $this->db->query("SELECT * 
									from v_student_profile 
									WHERE classid='$classid'
									AND yearid='$year'
									AND is_active='1' {$where}
									ORDER BY last_name_kh
									")->result();
		}
		function getrow($evaluateid,$transno=''){
			$where='';
			if($evaluateid!='')
				$where.=" AND evaluateid='$evaluateid'";
			if($transno!='')
				$where.=" AND transno='$transno'";
			$data=$this->db->query("SELECT * FROM sch_student_evaluated WHERE 1=1 {$where}");
			if($evaluateid!='')
				return $data->row();
			else
				return $data->result();
		}
		function getsemrow($semid){
			return $this->db->query("SELECT * FROM 
								sch_student_evaluated ste
								INNER JOIN sch_stud_evalsemester sem
								ON(ste.studentid=sem.studentid AND ste.transno=sem.transno)
								WHERE sem.semid='".$semid."'")->row();
		}
		function searchsemester($student_name,$schlevelid,$classid,$yearid,$semester,$s_num,$m,$p,$sort=''){
			$page=0;
			$where='';
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			if($student_name!=""){
				$where=" AND (s.last_name like '%".$student_name."%' OR s.first_name like '%".$student_name."%' OR s.fullname LIKE '%".$student_name."%')";
			}
			if($classid!=""){
				$where.=" AND se.classid = '".$classid."'";
			}
			if($schlevelid!=""){
				$where.=" AND s.schlevelid = '".$schlevelid."'";
			}
			if($semester!=""){
				$where.=" AND se.semester = '".$semester."'";
			}
				$sql="SELECT * FROM sch_stud_evalsemester se
						INNER JOIN v_student_profile s
						ON(se.studentid=s.studentid AND s.yearid=se.yearid)
						WHERE s.yearid='$yearid' {$where}
						";
				$config['base_url'] =site_url()."/student/evaluate/searchsemester?pg=1&n=$student_name&c=$classid&y=$yearid&&se=$semester&s_num=$s_num&m=$m,&p=$p&l=$schlevelid";		
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =50;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" $sort  {$limi}";
				return $this->db->query($sql)->result();
		}
		function search($student_name,$eva_type,$semester,$schlevelid,$classid,$month,$yearid,$s_num,$m,$p,$sort='',$promotion_id=''){
			$page=0;
			$where="";
			$WH_TEM="";

			$wh_stdeval="";
			$wh_vinf="";
			$wh_month="";

			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			if($student_name!=""){
				$wh_vinf=" AND (fullname LIKE '%".$student_name."%')";
			}
			if($eva_type!=""){				
				$wh_stdeval=" AND evaluate_type = '".$eva_type."'";
			}
			if($semester!=''){
				$where.=" AND eval_semester='$semester'";
			}
			$promotion = "";
			if($promotion_id!=''){
				$promotion=" AND promotion_id='$promotion_id'";
			}
			if($classid!=""){				
				$WH_TEM.=" AND classid = '".$classid."'";	
				// $where.= " AND se.classid='$classid'";
			}
			if($schlevelid!=""){				
				$WH_TEM.=" AND schlevelid = '".$schlevelid."'";
			}
			if($month!=""){				
				$wh_month= " AND month = '".$month."'";
			}
			#========== temporary tab ======= 1			

			$this->green->drop_temp("tmpstupro");
			$this->green->drop_temp("tmpstueval");
			$this->green->drop_temp("tmpstuevalmen");


			$this->green->create_temp("CREATE TEMPORARY TABLE IF NOT EXISTS tmpstupro 
										SELECT * 
										FROM v_student_profile as vinf 
										WHERE vinf.yearid='$yearid'
										$WH_TEM
										$wh_vinf
									");
			$this->green->create_temp("CREATE TEMPORARY TABLE IF NOT EXISTS tmpstueval 
										SELECT * 
										FROM sch_student_evaluated as stdeval 
										WHERE stdeval.yearid='$yearid' AND stdeval.evaluate_type='Three Month'
										$WH_TEM
										$wh_month
										$wh_stdeval
										$promotion
									");
			$this->green->create_temp("CREATE TEMPORARY TABLE IF NOT EXISTS tmpstuevalmen 
										SELECT * 
										FROM sch_student_evaluated_mention as stuevalmen 
										WHERE stuevalmen.yearid='$yearid'
										$WH_TEM
									");
			// print_r($this->db->query("SELECT * FROM tmpstueval WHERE classid='150'")->result()); die();
			$sql="SELECT sum(em.score) as score,
								se.evaluateid,
								se.transno,
								s.fullname,
								s.class_name,
								s.last_name,
								s.first_name,
								se.classid,
								se.yearid,
								se.evaluate_type,
								se.studentid,
								se.month,
								se.date
					FROM tmpstueval se
					INNER JOIN tmpstupro s ON(se.studentid=s.studentid AND s.yearid=se.yearid)
					LEFT JOIN tmpstuevalmen em ON(se.evaluateid=em.evaluateid )
					WHERE se.yearid='$yearid'
					{$where} GROUP BY se.evaluateid ";	
				
				$config['base_url'] =site_url()."/student/evaluate/search?pg=1&n=$student_name&type=$eva_type&l=$schlevelid&sem=$semester&c=$classid&mo=$month&y=$yearid&s_num=$s_num&m=$m&p=$p&pro=$promotion_id";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =$s_num;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}
                if($sort!=""){
                    $sort.=" ,last_name_kh ";
                }else{
                    $sort=" ORDER BY last_name_kh ";
                }
				$sql.=" $sort {$limi}";
				return $this->db->query($sql)->result();
				$this->green->drop_temp("tmpstupro");
				$this->green->drop_temp("tmpstueval");
				$this->green->drop_temp("tmpstuevalmen");
		}
		function searchresult($name,$schlevelid,$classid,$yearid,$s_result,$sort_num,$sort=''){
			$page=0;
			$passscore=5;
			$where='';
			$having='';
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			if($name!='')
				$where.=" AND s.fullname LIKE '%".$name."%'";
			if($classid!='')
				$where.=" AND eval.classid='".$classid."'";
			if($schlevelid!='')
				$where.=" AND s.schlevelid='".$schlevelid."'";
			if($s_result!=''){
				if($s_result=='pass')
					$having.=" AND (total/2)>='".$passscore."'";
				else
					$having.=" AND (total/2)<'".$passscore."'";
			}

				
			$sql="SELECT eval.studentid,
							eval.classid,
							s.class_name,
							s.fullname,
							eval.yearid,
							count(eval.studentid) as stdtimes,
							group_concat(eval.sem_avg) as avg,
							sum(eval.sem_avg) as total
				FROM sch_stud_evalsemester eval
				INNER JOIN v_student_profile s
				ON(eval.studentid=s.studentid AND eval.classid=s.classid)
				WHERE eval.yearid='$yearid' {$where} 
				GROUP BY eval.studentid
				HAVING stdtimes>1 {$having}";	
			$config['base_url'] =site_url()."/student/evaluate/searchresult?pg=1&n=$name&l=$schlevelid&c=$classid&y=$yearid&r=$s_result&s_num=$sort_num";	
			$config['total_rows'] = $this->green->getTotalRow($sql);
			$config['per_page'] =$sort_num;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);
			$limi=" limit ".$config['per_page'];
			if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}		
			$sql.=" {$sort} {$limi}";
			return $this->db->query($sql)->result();
		}
	}
?>