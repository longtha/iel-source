<?php
	class ModDonation extends CI_Model{
		function getdonate(){
				$page=0;
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				$m='';
				$p='';
				if(isset($_GET['m'])){
		        	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }	
					$sql="SELECT sg.date,
								sg.donate_type,
								sg.donateid,
								sg.transno,
								sg.yearid,
								sgd.studentid,
								s.fullname,
								s.class_name,
								s.sch_year
							FROM sch_stud_gifdonate sg
							INNER JOIN sch_stud_gifdonate_detail sgd
							ON(sg.donateid=sgd.donateid)
							INNER JOIN v_student_profile s
							ON(sgd.studentid=s.studentid AND sgd.yearid=s.yearid)
							GROUP BY sgd.studentid,sgd.transno 
							ORDER BY sgd.transno DESC";	
					$config['base_url'] =site_url()."/student/studentgift/index?pg=1&m=$m&p=$p";	
					$config['total_rows'] = $this->green->getTotalRow($sql);
					$config['per_page'] =50;
					$config['num_link']=5;
					$config['page_query_string'] = TRUE;
					$config['full_tag_open'] = '<li>';
					$config['full_tag_close'] = '</li>';
					$config['cur_tag_open'] = '<a><u>';
					$config['cur_tag_close'] = '</u></a>';
					$this->pagination->initialize($config);
					$limi=" limit ".$config['per_page'];
					if($page>0){
							$limi=" limit ".$page.",".$config['per_page'];
						}		
					$sql.=" {$limi}";
					return $this->db->query($sql)->result();
		}
		function save(){
			$transno=$this->input->post('transno');
			$donate_id=$this->input->post('donateid');
			$date=$this->input->post('d_date');
			$is_up=$this->input->post('is_up');
			$donate_type=$this->input->post('d_type');
			$yearid=$this->session->userdata('year');
			$school=$this->session->userdata('schoolid');
			$c_date=date('Y-m-d');
			$user=$this->session->userdata('user_name');
            if($transno==""){
                $transno=$this->green->nextTran("12","Gift Donate");
            }
			$data=array('date'=>$this->green->convertSQLDate($date),
						'donate_type'=>$donate_type,
						'type'=>12,
						'transno'=>$transno,
						'yearid'=>$yearid,
						'schoolid'=>$school);
			if($is_up=='upd'){
				$data2=array('modify_by'=>$user,'modify_date'=>$c_date);
				$this->db->where('donateid',$donate_id)->update('sch_stud_gifdonate',array_merge($data,$data2));
				$this->clearstdgif($transno);
			}else{
				if($donate_id==''){
					$data2=array('created_by'=>$user,'created_date'=>$c_date);
					$this->db->insert('sch_stud_gifdonate',array_merge($data,$data2));
					$donate_id=$this->db->insert_id();
				}
				$this->updatetran($transno+1);
			}
			$this->savestdgif($donate_id,$date,$transno,$yearid);
			
		}
		function savestdgif($donateid,$date,$transno,$yearid){
			$studentid=$this->input->post('studentid');
			for($i=0;$i<count($studentid);$i++){
				$gifid=$this->input->post('gifid_'.$studentid[$i]);
				$qty=$this->input->post('qty_'.$studentid[$i]);
				$unit=$this->input->post('unit_'.$studentid[$i]);
				$remark=$this->input->post('remark_'.$studentid[$i]);
				for($j=0;$j<count($gifid);$j++){
					$this->savedetail($donateid,$transno,$date,$studentid[$i],$gifid[$j],$qty[$j],$unit[$j],$remark[$j],$yearid);
				}
			}
		}
		function clearstdgif($transno){
			$this->db->where('transno',$transno)->delete('sch_stud_gifdonate_detail');
		}
		function getstdgifbytran($transno){
			return $this->db->query("SELECT DISTINCT sgd.studentid,s.fullname,s.class_name 
									FROM sch_stud_gifdonate_detail sgd
									INNER JOIN v_student_profile s
									ON(sgd.studentid=s.studentid AND sgd.yearid=s.yearid)
									WHERE sgd.transno='$transno'")->result();
		}
		function getgifbystd($studentid,$transno){
			return $this->db->query("SELECT sgd.gifid,
											sgd.quantity,
											sgd.unit,
											sgd.remark,
											sg.gifname
									FROM sch_stud_gifdonate_detail sgd
									INNER JOIN sch_student_gif sg
									ON(sgd.gifid=sg.gifid)
									WHERE sgd.transno='$transno'
									AND sgd.studentid='$studentid'")->result();
		}
		function savedetail($donateid,$transno,$date,$studentid,$gifid,$qty,$unit,$remark,$yearid){
			$schoolid=$this->session->userdata('schoolid');
			$stdrow=$this->db->query("SELECT * 
										FROM v_student_profile
										WHERE studentid='$studentid'
										AND yearid='$yearid'")->row();
			$data=array('donateid'=>$donateid,
						'transno'=>$transno,
						'type'=>12,
						'date'=>$this->green->convertSQLDate($date),
						'studentid'=>$studentid,
						'gifid'=>$gifid,
						'quantity'=>$qty,
						'unit'=>$unit,
						'remark'=>$remark,
						'yearid'=>$yearid,
						'schlevelid'=>$stdrow->schlevelid,
						'classid'=>$stdrow->classid,
						'schoolid'=>$schoolid);
			$this->db->insert('sch_stud_gifdonate_detail',$data);
		}
		function updatetran($val){
			$this->db->set('sequence',$val)->where('typeid',12)->update('sch_z_systype');
		}

		function search($name,$type,$classid,$s_num,$m,$p,$slv='',$yearid){
				$page=0;
				$where='';
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				if($name!='')
					$where.=" AND s.fullname LIKE '%".$name."%'";
				if($type!='')
					$where.=" AND sg.donate_type='".$type."'";
				if($classid!='')
					$where.=" AND sgd.classid='".$classid."'";
				if($slv!='')
					$where.=" AND sgd.schlevelid='".$slv."'";
				if($yearid!='')
					$where.=" AND sg.yearid='".$yearid."'";
				$sql="SELECT sg.date,
							sg.donate_type,
							sg.donateid,
							sg.transno,
							sg.yearid,
							sgd.studentid,
							s.fullname,
							s.class_name,
							s.sch_year
						FROM sch_stud_gifdonate sg
						INNER JOIN sch_stud_gifdonate_detail sgd
						ON(sg.donateid=sgd.donateid)
						INNER JOIN v_student_profile s
						ON(sgd.studentid=s.studentid AND sgd.yearid=s.yearid)
						WHERE 1=1 {$where}
						GROUP BY sgd.studentid,sgd.transno 
						ORDER BY sgd.transno DESC";	
				$config['base_url'] =site_url()."/student/donation/search?pg=1&m=$m&p=$p&n=$name&c=$classid&t=$type&s_num=$s_num&slv=$slv&y=$yearid";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =50;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				return $this->db->query($sql)->result();
		}
	}