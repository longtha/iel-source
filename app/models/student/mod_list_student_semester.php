<?php 
class Mod_list_student_semester extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function show_student(){
		date_default_timezone_set("Asia/Bangkok");
		 $studentid = $this->input->post('id');
		 $arr=array();
         $sql=$this->db->query("SELECT *
								FROM 	v_student_profile as vs								
								LEFT JOIN sch_school_level as sl
								ON vs.schlevelid = sl.schlevelid 
								LEFT JOIN sch_evaluation_semester_iep_order as sor
								ON vs.studentid = sor.studentid
								LEFT JOIN sch_school_semester as sem
								ON vs.semesterid=sem.semesterid
								WHERE vs.studentid = '".$studentid."'")->row();
         	$arr['semester']=$sql->semester;
			$arr['name'] = $sql->fullname;
			$arr['namekh'] = $sql->fullname_kh;
			$arr['sch_level'] = $sql->sch_level;
			$arr['sch_levelkh'] = $sql->sch_level;					
         	$arr['year'] = $sql->sch_year;
         	$arr['techer_comment'] = $sql->techer_comment;
			$arr['guardian_comment'] = $sql->guardian_comment;
            $arr['techer_date'] =   (!empty($sql->techer_date)?date('d-m-Y',strtotime($sql->techer_date)): date('d-m-Y'));
            $arr['academic_date'] = (!empty($sql->academic_date)?date('d-m-Y',strtotime($sql->academic_date)): date('d-m-Y'));
            $arr['guardian_date'] = (!empty($sql->guardian_date)?date('d-m-Y',strtotime($sql->guardian_date)): date('d-m-Y'));
            $arr['return_date'] = (!empty($sql->return_date)?date('d-m-Y',strtotime($sql->return_date)): date('d-m-Y'));
            header("Content-type:text/x-json");
            echo json_encode($arr);
	}
	
	function get_program($schoolid=""){  
		if($schoolid !=""){
            $this->db->where("schoolid",$schoolid);
        }
        $this->db->where("programid",2);
        return $this->db->get("sch_school_program")->result();
	}
	
	function getsch_level($programid=""){
        if($programid !=""){
            $this->db->where("programid",$programid);
        }
        return $this->db->get("sch_school_level")->result();
	}	

	function getschoolyear($sch_program="",$schoolid="",$schlevelid=""){
	    // if($schoolid!=""){
     //        $this->db->where('schoolid',$schoolid);
     //    }
     //    if($sch_program!=""){
     //        $this->db->where('programid',$sch_program);
     //    }
     //    if($schlevelid!=""){
     //        $this->db->where('schlevelid',$schlevelid);
     //    }
		$sql_y = $this->db->query("SELECT
									sch_school_year.yearid,
									sch_school_year.sch_year,
									CONCAT(DATE_FORMAT(from_date,'%Y'),'-',DATE_FORMAT(to_date,'%Y')) AS fyear
									FROM
									sch_school_year
									WHERE schoolid='".$schoolid."' 
									AND schlevelid='".$schlevelid."' 
									AND programid='".$sch_program."'")->result();
		return $sql_y;
	}	

	function getgradelevel($schoolid="",$programid="",$schlevelid=""){
        if($schoolid!=""){
            $this->db->where('schoolid',$schoolid);
        }
        if($schlevelid!=""){
            $this->db->where('schlevelid',$schlevelid);
        }
		return $this->db->get('sch_grade_level')->result();
	}

	function get_class($schoolid="",$grad_level="",$schlevelid=""){
        if($schoolid!=""){
            $this->db->where('schoolid',$schoolid);
        }
        if($grad_level!=""){
            $this->db->where('grade_levelid',$grad_level);
        }
        if($schlevelid!=""){
            $this->db->where('schlevelid',$schlevelid);
        }
         	   $this->db->order_by("class_name","asc");
		return $this->db->get('sch_class')->result();
	}	

	function show_list(){
		$page        = $this->input->post('page');     	
		$perpage     = $this->input->post('perpage');
		$s_sortby    = $this->input->post('s_sortby');
        $s_sorttype  = $this->input->post('s_sorttype');
        $program     = $this->input->post('program');
        $sch_level	 = $this->input->post('sch_level');
        $years       = $this->input->post('years');
        $classid     = $this->input->post('classid');
        $gradlevel   = $this->input->post('gradlevel');
        $s_student_id= $this->input->post('s_student_id');
        $s_full_name = $this->input->post('s_full_name');
        //$studentid   = $this->input->post('studentid');
		//$schlevelid = $this->input->post('schlevelid');
		$semesterid     = $this->input->post('semesterid');
		$programid   = $this->input->post('programid');
		$schoolid    = $this->input->post('schoolid');
        $where   = '';
        $sortstr = "";

        if($s_full_name != ''){
			$where .= " AND ( CONCAT(
									last_name,
									' ',
									first_name
								) LIKE '%{$s_full_name}%' ";
			$where .= " or CONCAT(
									last_name_kh,
									' ',
									first_name_kh
								) LIKE '%{$s_full_name}%' ) ";			
		}

		isset($termid) && $termid!=""?$where.= " AND tt.termid = '".$termid."' ":"";
        isset($s_student_id) && $s_student_id!=""?$where.= " AND vp.student_num ='".$s_student_id."' ":"";
        isset($years) && $years!=""?$where.= " AND vp.year ='".$years."' ":"";
        isset($semesterid) && $semesterid!=""?$where.= " AND tt.semesterid ='".$semesterid."' ":"";
        //isset($classid) && $classid!=""?$where.= " AND vp.classid ='".$classid."' ":"";
        isset($s_sortby) && $s_sortby!=""?$sortstr.= " ORDER BY ".$s_sortby." ".$s_sorttype:$sortstr.=$sortstr;
        if($this->session->userdata('match_con_posid')!='stu'){
	        $sql ="SELECT
							vp.student_num,
							vp.first_name,
							vp.last_name,
							CONCAT(vp.last_name,' ',vp.first_name) AS fullname,
							vp.first_name_kh,
							vp.last_name_kh,
							CONCAT(vp.last_name_kh,' ',vp.first_name_kh) AS fullname_kh,
							vp.gender,
							vp.class_name,
							vp.studentid,
							vp.schoolid,
							vp.`year`,
							vp.classid,
							vp.schlevelid,
							vp.rangelevelid,
							vp.feetypeid,
							vp.programid,
							vp.sch_level,
							vp.rangelevelname,
							vp.program,
							vp.sch_year,
							vp.grade_levelid,
							g.grade_level,
							tt.yearid,
							tt.semesterid,
							sm.semester
							FROM
							v_student_enroment AS vp
							INNER JOIN sch_school_term AS tt ON tt.yearid = vp.`year` AND vp.schlevelid = tt.schlevelid
							INNER JOIN sch_grade_level AS g ON vp.grade_levelid = g.grade_levelid
							INNER JOIN sch_school_semester sm ON tt.semesterid = sm.semesterid
							WHERE 1=1
							AND vp.schoolid =".($schoolid==''?'NULL':$schoolid)."
							AND vp.programid ='".$program."'
							AND vp.schlevelid ='".$sch_level."'
							AND vp.grade_levelid ='".$gradlevel."' 
							AND vp.classid ='".$classid."' 
							{$where}
							GROUP BY vp.studentid,tt.semesterid
							ORDER BY vp.studentid ASC";

		}else{
			$sql ="SELECT
						vp.student_num,
						vp.first_name,
						vp.last_name,
						CONCAT(vp.last_name,' ',vp.first_name) AS fullname,
						vp.first_name_kh,
						vp.last_name_kh,
						CONCAT(vp.last_name_kh,' ',vp.first_name_kh) AS fullname_kh,
						vp.gender,
						vp.class_name,
						vp.studentid,
						vp.schoolid,
						vp.`year`,
						vp.classid,
						vp.schlevelid,
						vp.rangelevelid,
						vp.feetypeid,
						vp.programid,
						vp.sch_level,
						vp.rangelevelname,
						vp.program,
						vp.sch_year,
						vp.grade_levelid,
						g.grade_level,
						tt.yearid,
						tt.semesterid,
						sm.semester
						FROM
						v_student_enroment AS vp
						INNER JOIN sch_school_term AS tt ON tt.yearid = vp.`year` AND vp.schlevelid = tt.schlevelid
						INNER JOIN sch_grade_level AS g ON vp.grade_levelid = g.grade_levelid
						INNER JOIN sch_school_semester sm ON tt.semesterid = sm.semesterid
						WHERE 1=1 AND vp.studentid='".($this->session->userdata('emp_id'))."' AND vp.programid=2
						GROUP BY vp.studentid,tt.semesterid
						ORDER BY vp.studentid ASC";
		}
		$table='';
		$pagina='';
		$getperpage=0;
		if($perpage==''){
			$getperpage=10;
		}else{
			$getperpage=$perpage;
		}
		$paging=$this->green->ajax_pagination(count($this->db->query($sql)->result()),site_url("student/c_list_student_semester/show_list"),$getperpage,"icon");
		
		$i=1;
		$getlimit=10;
		if($paging['limit']!=''){
			$getlimit=$paging['limit'];
		}
		$limit=" LIMIT {$paging['start']}, {$getlimit}";   

        if($sortstr!=""){
            $sql.=$sortstr;
        }
		$sql.=" {$limit}";
		$path_print_all = "<a href='".site_url("student/c_list_student_semester/veiw_all_report")."?limitpage=".$perpage."&startpage=".$paging['start']."&programid=".$program."&schoolid=".$schoolid."&schlid=".$sch_level."&grandlevel=".$gradlevel."&yearid=".$years."&classid=".$classid."&semesterid=".$semesterid."' target='_blank'>Print ALL</a>";
		$arr = array('sql' => $sql, 'paging' => $paging,"showpage"=>$path_print_all);
	  	return $arr;
	}
}

?>