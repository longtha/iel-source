<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modattendance extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
     public function sqlGetResult($sql)
    {
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result();
        }else{
            return NULL;
        }
    }
    function getvalidate($schoolid,$schlevelid,$yearid,$classid,$termid,$date,$baseattid,$subject_grouop,$subjectid="")
    {
        $this->db->select('count(*)');
        $this->db->from('sch_studatt_daily');
        $this->db->where('schoolid',$schoolid);
        $this->db->where('schlevelid',$schlevelid);
        $this->db->where('yearid',$yearid);
        $this->db->where('classid',$classid);
        $this->db->where('termid',$termid);
        $this->db->where('date',$date);
        $this->db->where('baseattid',$baseattid);

        if($baseattid==1 && $subjectid!=""){
            $this->db->where('subjectid',$subjectid);
            $this->db->where('subject_grouop',$subject_grouop);
        }

        return $this->db->count_all_results();
    }
    function save($transno=""){

        date_default_timezone_set("Asia/Bangkok");
        $dailyattid=$this->input->post('dailyattid');

        $schoolid=$this->input->post('schoolid');
        $schlevelid=$this->input->post('sclevelid');
        $yearid=$this->input->post('yearid');

        $weekid=$this->input->post('weekid');
        $termid=$this->input->post('termid');
        $att_date=$this->input->post('att_date');
        $subjectid=$this->input->post('subjectid');
        $subject_grouop=$this->input->post('subject_grouop');
        $studentids=$this->input->post('studentids');
        $baseattid=$this->input->post('baseattid');
        $classid=$this->input->post('classid');

       // $attnoteids=(!isset($this->input->post('attnoteids'))?0:$this->input->post('attnoteids'));
        $present_days=$this->input->post('present_days');
        $permission_days=$this->input->post('permission_days');
        $absent_days=$this->input->post('absent_days');
        $latedays=$this->input->post('latedays');
        $reason_note =$this->input->post('reason_note');

        $count=$this->getvalidate($schoolid,$schlevelid,$yearid,$classid,$termid,$this->green->formatSQLDate($att_date),$baseattid,$subject_grouop,$subjectid);
        $programid= $this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='".$schlevelid."'")->row()->programid;

        if(count($studentids)>0) {

            $type='31';
            $created_date = date("Y-m-d H:i:s");
            $created_by = $this->session->userdata('user_name');
            if($transno=="" && $count==""){

                $transno=$this->green->nextTran("31","Student Attendance");
                $attdaily=array('termid'=>$termid,
                                'weekid'=>$weekid,
                                'subjectid'=>$subjectid,
                                'subject_grouop'=>$subject_grouop,
                                'date'=>$this->green->formatSQLDate($att_date),
                                'yearid'=>$yearid,
                                'schoolid'=>$schoolid,
                                'programid'=>$programid,
                                'schlevelid'=>$schlevelid,
                                'baseattid'=>$baseattid,
                                'type'=>$type,
                                'transno'=>$transno,
                                'classid'=>$classid,
                                'created_date'=>$created_date,
                                'created_by'=>$created_by
                                );
                $this->db->insert('sch_studatt_daily', $attdaily);
                $dailyattid=$this->db->insert_id();
            }else{

                $upwh=array('type'=>$type,'transno'=>$transno);
                $attdaily=array('termid'=>$termid,
                                'weekid'=>$weekid,
                                'subjectid'=>$subjectid,
                                'subject_grouop'=>$subject_grouop,
                                'date'=>$this->green->formatSQLDate($att_date),
                                'yearid'=>$yearid,
                                'schoolid'=>$schoolid,
                                'programid'=>$programid,
                                'schlevelid'=>$schlevelid,
                                'baseattid'=>$baseattid,
                                'classid'=>$classid,
                                'modified_date'=>$created_date,
                                'modified_by'=>$created_by
                                );
                $this->db->update('sch_studatt_daily',$attdaily,$upwh);
                $arrdelwh=array("transno"=>$transno);
                $this->db->delete("sch_studatt_dailydetail",$arrdelwh);
            }
            $i=0;
            foreach($studentids as $studentid){
                if($present_days[$i] ==0) {
                    $data = array('dailyattid' => $dailyattid,
                        'studentid' => $studentid,
                        'present_day' => $present_days[$i],
                        'permission_day' => $permission_days[$i],
                        'absent_day' => $absent_days[$i],
                        'lateday' => $latedays[$i],
                        'termid' => $termid,
                        'weekid' => $weekid,
                        'subjectid' => $subjectid,
                        'subject_grouop' => $subject_grouop,
                        'date' => $this->green->formatSQLDate($att_date),
                        'yearid' => $yearid,
                        'schoolid' => $schoolid,
                        'programid' => $programid,
                        'schlevelid' => $schlevelid,
                        'baseattid' => $baseattid,
                        'type' => $type,
                        'transno' => $transno,
                        'classid' => $classid,
                        'note' => $reason_note[$i]
                    );

                    $this->db->insert('sch_studatt_dailydetail', $data);
                }
                $i++;
            }
        }
        $m='';
        $p='';
        if(isset($_GET['m'])){
            $m=$_GET['m'];
        }
        if(isset($_GET['p'])){
            $p=$_GET['p'];
        }
        $res=0;
        if($transno!=""){
            $res=1;
        }
        redirect("student/attendance/add?res=".$res."&m=".$m."&p=".$p);
    }

    function search($start=0){

        $schoolid=(isset($_POST['schoolid']))?$_POST['schoolid']:"";
        $sclevelid=(isset($_POST['sclevelid']))?$_POST['sclevelid']:"";
        $yearid=(isset($_POST['yearid']))?$_POST['yearid']:"";
        $classid=(isset($_POST['classid']))?$_POST['classid']:"";

        $termid=(isset($_POST['termid']))?$_POST['termid']:"";
        $weekid=(isset($_POST['weekid']))?$_POST['weekid']:"";
        $baseattid=(isset($_POST['baseattid']))?$_POST['baseattid']:"";

        $from_date=(isset($_POST['from_date']))?$_POST['from_date']:"";
        $to_date=(isset($_POST['to_date']))?$_POST['to_date']:"";
        $subjectid=(isset($_POST['subjectid']))?$_POST['subjectid']:"";
        $subject_grouop=(isset($_POST['subject_grouop']))?$_POST['subject_grouop']:"";

        $WHERE="";
        $programid="";
        if($schoolid!=""){
            $WHERE.=" AND schoolid = '".$schoolid."'";
        }
        if($sclevelid!=""){
            $WHERE.=" AND schlevelid = '".$sclevelid."'";
            $programid= $this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='".$sclevelid."'")->row()->programid;
        }
        if($yearid!=""){
            $WHERE.=" AND yearid = '".$yearid."'";
        }
        
        if($termid!=""){
            $WHERE.=" AND termid = '".$termid."'";
        }
        if($weekid!=""){
            $WHERE.=" AND weekid = '".$weekid."'";
        }
        if($baseattid!=""){
            $WHERE.=" AND baseattid = '".$baseattid."'";
        }
        if($subjectid!="" && $baseattid==1){
            $WHERE.=" AND subjectid = '".$subjectid."'";
        }
        if($subject_grouop!="" && $baseattid==1){
            $WHERE.=" AND subject_grouop = '".$subject_grouop."'";
        }
        if($from_date!="" && $to_date!=""){
            $WHERE.=" AND date >= '". $this->green->formatSQLDate($from_date)."'
                AND date <= '".$this->green->formatSQLDate($to_date)."'";
        }
        
        $v_attdaily="v_attdaily_kgp";

        if($programid==2){
            $v_attdaily="v_attdaily_iep";
        }elseif($programid==3){
            $v_attdaily="v_attdaily_gep";
        }
        if($this->session->userdata('match_con_posid')=='stu'){
            $WHERE.=" AND classid IN(SELECT
                                            vs.classid
                                        FROM
                                            v_student_profile vs
                                        WHERE
                                            1 = 1
                                        AND vs.studentid = '".$this->session->userdata('emp_id')."' GROUP BY vs.classid)";
        }else{
            if($classid!=""){
                $WHERE.=" AND classid = '".$classid."'";
            }
        }
        $sql="SELECT * FROM {$v_attdaily} WHERE 1=1 {$WHERE} ORDER BY schoolid,programid,schlevelid,yearid";

        $total_row=$this->green->getValue("select count(*) as numrow FROM ($sql) as cc ");
        $paging=$this->green->ajax_pagination($total_row,site_url("student/attendance/search"),100);

        if(isset($start) && $start>0){
            $paging['start']=($start-1)*$paging['limit'];
        }
        $data=$this->green->getTable("$sql limit {$paging['start']}, {$paging['limit']}");
        $arrJson['paging']=$paging;

        $arrJson['datas']=$data;
        header("Content-type:text/x-json");

        echo json_encode($arrJson);
        exit();
    }
    function search_detail(){
        $schoolid=(isset($_POST['schoolid']))?$_POST['schoolid']:"";
        $sclevelid=(isset($_POST['sclevelid']))?$_POST['sclevelid']:"";
        $yearid=(isset($_POST['yearid']))?$_POST['yearid']:"";
        $classid=(isset($_POST['classid']))?$_POST['classid']:"";

        $termid=(isset($_POST['termid']))?$_POST['termid']:"";
        $weekid=(isset($_POST['weekid']))?$_POST['weekid']:"";
        $baseattid=(isset($_POST['baseattid']))?$_POST['baseattid']:"";

        $from_date=(isset($_POST['from_date']))?$_POST['from_date']:"";
        $to_date=(isset($_POST['to_date']))?$_POST['to_date']:"";
        $subjectid=(isset($_POST['subjectid']))?$_POST['subjectid']:"";
        $subject_grouop=(isset($_POST['subject_grouop']))?$_POST['subject_grouop']:"";
        $t_date = $this->green->formatSQLDate($to_date);
        $f_date = $this->green->formatSQLDate($from_date);

        $WHERE="";
        $WHERE_DE ="";
        $programid="";
        if($schoolid!=""){
            $WHERE.=" AND schoolid = '".$schoolid."'";
        }
        if($sclevelid!=""){
            $WHERE.=" AND schlevelid = '".$sclevelid."'";
            $programid= $this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='".$sclevelid."'")->row()->programid;
        }
        if($yearid!=""){
            $WHERE.=" AND yearid = '".$yearid."'";
        }
        // if($classid!=""){
        //     $WHERE.=" AND classid = '".$classid."'";
        // }
        if($termid!=""){
            $WHERE_DE .=" AND termid = '".$termid."'";
        }
        if($weekid!=""){
            $WHERE_DE .=" AND weekid = '".$weekid."'";
        }
        if($baseattid!=""){
           // $WHERE.=" AND baseattid = '".$baseattid."'";
        }
        if($subjectid!="" && $baseattid==1){
            $WHERE_DE .= " AND subjectid = '".$subjectid."'";
        }
        if($subject_grouop!="" && $baseattid==1){
            $WHERE_DE .= " AND subject_grouop = '".$subject_grouop."'";
        }
        if($from_date!="" && $to_date!=""){
           // $WHERE_DE .= " AND trandate >= '". $f_date."' AND trandate <= '".$t_date."' OR trandate ='000'";
        }
        if($programid==1) {
            $WHERE .=" AND programid= 1 ";
            $v_attdaily = "v_attdailydetail_kgp";
        }elseif($programid==2){
            $WHERE .=" AND programid= 2 ";
            $v_attdaily="v_attdailydetail_iep";
        }elseif($programid==3){
            $WHERE .=" AND programid= 3 ";
            $v_attdaily="v_attdailydetail_gep";
        }
        $where_userlog="";
        // if($this->session->userdata('match_con_posid')=='stu'){
        //     $where_userlog.=" AND studentid='".($this->session->userdata('emp_id'))."'";
        // }
        if($this->session->userdata('match_con_posid')=='stu'){
            $where_userlog.=" AND classid IN(SELECT
                                            vs.classid
                                        FROM
                                            v_student_profile vs
                                        WHERE
                                            1 = 1
                                        AND vs.studentid = '".$this->session->userdata('emp_id')."' GROUP BY vs.classid)";
        }else{
            if($classid!=""){
                $WHERE.=" AND classid = '".$classid."'";
            }
        }
        $sql= $this->db->query("SELECT DISTINCT studentid, student_num, fullname, fullname_kh, fullname_ch, phone1, gender  FROM v_student_profile WHERE 1=1 {$WHERE} {$where_userlog} ORDER BY fullname ASC")->result();
        $stly_th = ' style="background-color: #8096b5 !important;"';
        $stly_th1 = ' style="background-color: #098ddf !important;"';

        $th ='<th '.$stly_th1.'>#</th>
              <th '.$stly_th1.'>ID</th>
              <th '.$stly_th1.'>Name</th>
              <th '.$stly_th1.'>Phone</th>
              <th '.$stly_th1.'>S</th>';
            $period = new DatePeriod(new DateTime($f_date), new DateInterval('P1D'), new DateTime($t_date ." +1 day"));
            $h_date ="";

            foreach ($period as $date) {
                $day = date_format($date, "w");
                $th .= '<th '.($day =="0"?$stly_th:($day =="6"?$stly_th:$stly_th1)).'>'.$date->format("d").'</th>';
                $dates[] = $date->format("Y-m-d");

            }
        $th .= '<th '.$stly_th1.'>Total P</th>';
        $th .= '<th '.$stly_th1.'>Total A</th>';
        $th .= '<th '.$stly_th1.'>Total L</th>';
        $tr ='';
        $total_a=0;
        $total_l=0;
        $total_p=0;
        if(count($sql)>0){
            $i =1;
            foreach($sql as $row){
                $tr .='<tr>
                        <td>'.$i.'</td>
                        <td>'.$row->student_num.'</td>
                        <td>'.($programid==1?$row->fullname_kh:$row->fullname).'</td>
                        <td>'.$row->phone1.'</td>
                        <td>'.($row->gender =="male"?"M":"F").'</td>';
                $i++;
                $t_total_a=0;
                $t_total_l=0;
                $t_total_p=0;
                if(count($dates)>0){
                    foreach($dates AS $con_date){
                       $days = "";//date_format($con_date,"w");
                       $W_DATE = " AND studentid='".$row->studentid."' AND trandate ='".$con_date."'";
                       $att_detail = "SELECT permission_day, lateday, absent_day FROM {$v_attdaily} WHERE 1=1 {$WHERE} {$WHERE_DE} {$W_DATE} ORDER BY fullname ASC";
                       // echo $att_detail;
                       $val = $this->db->query($att_detail)->row();
                       if(count($val)>0) {
                            $p = $val->permission_day - 0;
                            $l = $val->lateday - 0;
                            $a = $val->absent_day - 0;
                       }else{
                            $p = "";
                            $l = "";
                            $a = "";
                       }
                        if($a==1){
                            $pre = "A";
                        }elseif($l==1){
                            $pre = "L";
                        }elseif($p==1){
                            $pre = "P";
                        }else{
                            $pre = " ";
                        }
                        $total_a+=$a;
                        $total_l+=$l;
                        $total_p+=$p;
                        $t_total_a+=$a;
                        $t_total_l+=$l;
                        $t_total_p+=$p;
                        $tr .='<td>'.$pre.'</td>';
                    }
                }
                $tr.='<td align="right"><strong>'.$t_total_p.'</strong></td>
                       <td align="right"><strong>'.$t_total_a.'</strong></td>
                       <td align="right"><strong>'.$t_total_l.'</strong></td>
                    ';
            }
            $tr .=' </tr>';
        }else{
            $tr.='<tr><td></td></tr>';
        }

        $arrJson['data_h']=$th;
        $arrJson['data_d']=$tr;
        $arrJson['total_a']=$total_a;
        $arrJson['total_l']=$total_l;
        $arrJson['total_p']=$total_p;
        $arrJson['sql']=$sql;
        header("Content-type:text/x-json");
        echo json_encode($arrJson);
        exit();
    }
    function delete($transno){
        $result['del']=0;
        $candel=1;

        if($transno!="" && $candel==1){
            $this->db->delete('sch_studatt_daily', array('transno' => $transno));
            $this->db->delete('sch_studatt_dailydetail', array('transno' => $transno));
            if ($this->db->_error_message()) {
                $result['del'] = 'Error! ['.$this->db->_error_message().']';
            } else if (!$this->db->affected_rows()) {
                $result['del'] = 'Error! ID ['.transno.'] not found';
            } else {
                $result['del'] = 'Success';
            }
        }
        echo json_encode($result);
        exit();
    }
    
    function attnotes(){
        return $this->db->get("sch_stud_attnote")->result();
    }
    function attbase($schlevelid=""){
        $rec=array();
        if($schlevelid!=""){
            $this->db->where("schlevelid",$schlevelid);
            $rec=$this->db->get("v_attbaseschlevel")->result();
        }else{
            $this->db->where("isactive",1);
            $rec=$this->db->get("sch_z_att_base")->result();
        }

        return $rec;
    }
    function  getattdailyrow($transno,$programid=1){
        $this->db->where("transno",$transno);
        $v_attdaily="v_attdaily_kgp";
        if($programid=="2"){
            $v_attdaily="v_attdaily_iep";
        }elseif($programid=="3"){
            $v_attdaily="v_attdaily_gep";
        }
        return $this->db->get($v_attdaily)->result();
    }

    function getsubjecttype_iep($schoollevel="")
    {
        $key=$_GET['term'];
        $this->db->like('subject_type',$key);
        $this->db->where('schlevelid',$schoollevel);
        $query=$this->db->get('sch_subject_type_iep');
        $data= $query->result();
        return $data;
    }

    function  getattdailydetrow($transno="",$programid="",$yearid="",$classid=""){
        $WHERE ="";
        if($programid!=""){
            $WHERE .=" AND att_detail.programid='".$programid."'";
        }
        if($classid!=""){
            $yearid .=" AND att_detail.yearid='".$yearid."'";
        }
        if($classid!=""){
            $WHERE .=" AND att_detail.classid='".$classid."'";
        }
        $sql = "SELECT DISTINCT
                    vst.studentid,
                    vst.student_num,
                    vst.fullname,
                    vst.fullname_kh,
                    vst.fullname_ch,
                    vst.phone1,
                    IFNULL(att_detail.present_day, '1') AS present_day,
                    att_detail.permission_day,
                    att_detail.lateday,
                    att_detail.absent_day,
                    att_detail.dailyattdetid,
                    att_detail.baseattid,
                    att_detail.dailyattid,
                    att_detail.schoolid,
                    att_detail.programid,
                    att_detail.schlevelid,
                    att_detail.yearid,
                    att_detail.classid,
                    att_detail.studentid,
                    att_detail.termid,
                    att_detail.weekid,
                    att_detail.subjectid,
                    att_detail.subject_grouop,
                    att_detail.date,
                    att_detail.type,
                    att_detail.transno,
                    att_detail.attnoteid,
                    att_detail.present_day,
                    att_detail.permissin_hour,
                    att_detail.absent_hour,
                    att_detail.attend_score,
                    att_detail.permis_score,
                    att_detail.minus_absentscore,
                    att_detail.latetime,
                    att_detail.leave_early_time,
                    att_detail.minus_permscore,
                    att_detail.witdraw_late_point,
                    att_detail.widrleave_early_point,
                    att_detail.full_score,
                    att_detail.note
                FROM
                    v_student_profile vst
                INNER JOIN sch_studatt_dailydetail AS att_detail ON att_detail.studentid = vst.studentid
                AND att_detail.classid = vst.classid
                WHERE
                    1 = 1 {$WHERE}
                AND att_detail.transno='".$transno."'
                ORDER BY att_detail.dailyattdetid DESC
                ";
        return $this->sqlGetResult($sql);
    }
    function  getattdailydetrowpreview($transno="",$programid="",$yearid="",$classid=""){
        $WHERE ="";
        $WHERE .=" AND programid='".$programid."'";
        $WHERE .=" AND yearid='".$yearid."'";
        $WHERE .=" AND classid='".$classid."'";
        $where_userlog="";
        if($this->session->userdata('match_con_posid')=='stu'){
            $where_userlog.=" AND vst.studentid='".($this->session->userdata('emp_id'))."'";
        }
        $sql_stu = $this->db->query("SELECT DISTINCT 
                                        vst.studentid,
                                        vst.student_num,
                                        vst.fullname,
                                        vst.fullname_kh,
                                        vst.fullname_ch,
                                        vst.phone1,
                                        vst.gender  
                                    FROM 
                                        v_student_profile AS vst 
                                    WHERE 1=1 {$WHERE} {$where_userlog} 
                                    ORDER BY vst.fullname ASC");
            $tr='<thead class="tab_head">
                    <th>#-'.count($sql_stu).'</th>
                    <th>Student ID</th>
                    <th>Student Name</th>
                    <th>Phone</th>
                    <th>Present</th>
                    <th>Permission</th>
                    <th>Absent</th>
                    <th>Late</th>
                    <th>Note</th>
                </thead>';
            if(count($sql_stu)>0){
               
                $i=1;
                $image = '<img src="'.base_url('assets/images/icons/checked.png').'"/>';
                $tpresent_day=0;
                $totot_permission_day=0;
                $totot_absent_day=0;
                $totot_lateday=0;
                foreach ($sql_stu->result() as $stu_info){
                    $att_detail = "SELECT IFNULL(present_day,'1') AS present_day,
                                        permission_day, 
                                        lateday, 
                                        absent_day, 
                                        note,
                                        studentid 
                                    FROM sch_studatt_dailydetail 
                                    WHERE 1=1  
                                    AND transno ='".$transno."' 
                                    AND studentid='".$stu_info->studentid."' 
                                    {$WHERE}";
                    $attrowdet = $this->db->query($att_detail)->row();
                    if(count($attrowdet)>0) {
                        $present_day =number_format($attrowdet->present_day,0);
                        $permission_day =number_format($attrowdet->permission_day,0)-0;
                        $absent_day=number_format($attrowdet->absent_day,0)-0;
                        $lateday=number_format($attrowdet->lateday,0)-0;
                        $note = $attrowdet->note;

                    }else{
                        $present_day =0;
                        $permission_day =0;
                        $absent_day=0;
                        $lateday=0;
                        $note ="";
                    }
                    $tr .= '<tr>
                                <td>'.$i.'</td>
                                <td>'.$stu_info->student_num.'</td>
                                <td>'.($programid==1?$stu_info->fullname_kh:$stu_info->fullname).'</td>
                                <td class="left">'.$stu_info->phone1.'</td>
                                <td align="center">'.($present_day ==""?$image:"").'</td>
                                <td align="center">'.($permission_day==1?$image:"").'</td>
                                <td align="center">'.($absent_day==1?$image:"").'</td>
                                <td align="center">'.($lateday==1?$image:"").'</td>
                                <td align="left">'.$note.'</td>
                            </tr>';
                            $tpresent_day+=($present_day ==""?$present_day=1:$present_day=0);
                            $totot_permission_day+=$permission_day-0;
                            $totot_absent_day+=$absent_day-0;
                            $totot_lateday+=$lateday-0;
                    $i++;
                  
                }
                if($i>1){
                    $tr .= '<tr>
                                <td colspan="4" align="right"><strong style="font-size:14px; color:#000;">Total</strong></td>
                                <td align="center" style="font-size:14px; color:#000;">'.$tpresent_day.'</td>
                                <td align="center" style="font-size:14px; color:#000;">'.$totot_permission_day.'</td>
                                <td align="center" style="font-size:14px; color:#000;">'.$totot_absent_day.'</td>
                                <td align="center" style="font-size:14px; color:#000;">'.$totot_lateday.'</td>
                                <td align="center"></td>
                            </tr>';
                }else{
                    $tr.='<tr><td colspan="9" align="center">Nothing record to display here..!</td></tr>';
                }
                
        }else{
            $tr.='<tr><td colspan="9" align="center">Nothing record to display here..!</td></tr>';
        }
        return $tr;
    }

}