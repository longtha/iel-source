<?php
    class m_convert_student extends CI_Model{


    	// getprogram ----------------------------------------------
    	function getprograms($programid=''){
	        if($programid!=""){
	            $this->db->where("programid",$programid);
	        }
        	return $this->db->get("sch_school_program")->result() ;
   		}

   		// get school level ----------------------------------------
    	function get_schlevel($programid = ''){
	        $qr_schlevel = $this->db->query("SELECT DISTINCT
	                                                l.schlevelid,
	                                                l.sch_level
	                                            FROM
	                                                sch_school_level AS l
	                                            WHERE
	                                                l.programid = '{$programid}' - 0
	                                            ORDER BY
	                                                l.sch_level ASC ")->result();        
	        
	        $arr = array('schlevel' => $qr_schlevel);
	        return json_encode($arr);
	    }
	    // get_schyera -------------------------------------------------------------
	    function get_schyera($schlevelid = ''){
	        $qr_schyear = $this->db->query("SELECT DISTINCT
	                                                y.yearid,
	                                                y.sch_year,
	                                                y.schlevelid
	                                            FROM
	                                                sch_school_year AS y
	                                            WHERE
	                                                y.schlevelid = '{$schlevelid}' - 0
	                                            ORDER BY
	                                                y.sch_year ASC ")->result();        
	        
	        $arr = array('schyear' => $qr_schyear);
	        return json_encode($arr);
	    }

	    //from ranglavel--------------------------------------------------
	     function schranglavel($yearid = ''){
	        $qr_schraglavele = $this->db->query("SELECT DISTINCT
													rl.rangelevelid,
													rl.rangelevelname
												FROM
													sch_school_rangelevel AS rl
												INNER JOIN sch_school_year AS y ON rl.schlevelid = y.schlevelid
												AND rl.programid = y.programid
												WHERE
													y.yearid = '{$yearid}' - 0
												ORDER BY
													rl.rangelevelname ASC")->result();       
	        
	        $arr = array('schranglavel' => $qr_schraglavele);
	        return json_encode($arr);
	    }
	    
	    //from classname ------------------------------------------------------
	    function get_schclass($rangelevelid = ''){
	        $qr_schclass = $this->db->query("SELECT DISTINCT
												ran.rangelevelid,
												ran.rangelevelname,
												racl.classid,
												cla.class_name
											FROM
												sch_school_rangelevelclass AS racl
											LEFT JOIN sch_class AS cla ON racl.classid = cla.classid
											LEFT JOIN sch_school_rangelevel AS ran ON ran.rangelevelid = racl.rangelevelid
											WHERE
												ran.rangelevelid = '{$rangelevelid}' - 0
											ORDER BY
												cla.class_name ASC")->result();        
	        
	        $arr = array('schclass' => $qr_schclass);
	        return json_encode($arr);
	    }

	    // save --------------------------------------------------------
        function saved($enrollid){ 
        	date_default_timezone_set("Asia/Bangkok");
        	$user = $this->session->userdata('user_name');
            $to_classid = $this->input->post('to_classid');
            $classid = $this->input->post('classid');
            $rangelevelid = $this->input->post('rangelevelid');

            
            $ar = $this->input->post('arr_check');
            $countid=count($ar);
            if(!empty($ar)){
	            foreach($ar as $r){
	            	if($enrollid > 0){

						// $data=array('classid'=>$to_classid,
						// 			'studentid' => $r['studentId'],
						// 			'schoolid'=> $r['schoolid'],
						// 			'yearid'=> $r['yearid'],
						// 			'programid'=> $r['programid'],
						// 			'schlevelid'=> $r['schlevelid']									
						// 			);

						// $this->db->where('enrollid', $r['enrollID']);

						// $this->db->update('sch_student_enrollment',array_merge($data));

						
						$this->green->FconvertClaID($rangelevelid,$r['programid'],$r['schlevelid'],$r['yearid'],$classid,$to_classid,$r['studentId'],$r['enrollID']);
					}
	            }    
            }

            //return $enrollid;
           return $countid;
        }

    

    }