<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_quick_update_student extends CI_Model
{
	
	function __construct(){
		parent::__construct();
	}

	function getsch_level($programid=""){
        if($programid !=""){
            $this->db->where("programid",$programid);
        }
        return $this->db->get("sch_school_level")->result();
	}

	function getgradelevel($schoolid="",$programid="",$schlevelid="")
		{
            if($schoolid!=""){
                $this->db->where('schoolid',$schoolid);
            }
            if($schlevelid!=""){
                $this->db->where('schlevelid',$schlevelid);
            }
			return $this->db->get('sch_grade_level')->result();
		}

	function getschoolyear($sch_program="",$schoolid="",$schlevelid=""){
	    if($schoolid!=""){
            $this->db->where('schoolid',$schoolid);
        }
        if($sch_program!=""){
            $this->db->where('programid',$sch_program);
        }
        if($schlevelid!=""){
            $this->db->where('schlevelid',$schlevelid);
        }
		return $this->db->get('sch_school_year')->result();
	}

	function get_class($schoolid="",$grad_level="",$schlevelid=""){
        if($schoolid!=""){
            $this->db->where('schoolid',$schoolid);
        }
        if($grad_level!=""){
            $this->db->where('grade_levelid',$grad_level);
        }
        if($schlevelid!=""){
            $this->db->where('schlevelid',$schlevelid);
        }
         	   $this->db->order_by("class_name","asc");
		return $this->db->get('sch_class')->result();
	}

	function s_class(){
			    $this->db->order_by("class_name","asc");
		return $this->db->get('sch_class')->result();
	}

	function save_gender(){
		$studentid = $this->input->post('studentid');
		$gender = $this->input->post('gender');
		$data= array(
						'gender'=>$gender
					);
		$this->db->update('v_student_profile',$data, array('studentid' => $studentid));
        $arr['insert']="success";
		return json_encode($arr);
	}

	function get_student_list(){   
		$page = $this->input->post('page');     	
		$perpage=$this->input->post('perpage');
		$s_sortby=$this->input->post('s_sortby');
        $s_sorttype=$this->input->post('s_sorttype');
        $program=$this->input->post('program');
        $sch_level	= $this->input->post('sch_level');
        $years = $this->input->post('years');
        $classid = $this->input->post('classid');
        $gradlevel=$this->input->post('gradlevel');
        $gender = $this->input->post('gender');
        $s_student_id=$this->input->post('s_student_id');
        $s_full_name =$this->input->post('s_full_name');
        $where='';
        $sortstr="";

        if($s_full_name != ''){
			$where .= "AND ( CONCAT(
									last_name,
									' ',
									first_name
								) LIKE '%{$s_full_name}%' ";
			$where .= "or CONCAT(
									last_name_kh,
									' ',
									first_name_kh
								) LIKE '%{$s_full_name}%' ) ";			
		}		
        isset($program) && $program!=""?$where.= " AND v_student_profile.programid like '".$program."%' ":$where.= $where;
        isset($gradlevel) && $gradlevel!=""?$where.= " AND v_student_profile.grade_levelid like '".$gradlevel."%' ":$where.= $where;
        isset($s_student_id) && $s_student_id!=""?$where.= " AND v_student_profile.student_num like '".$s_student_id."%' ":$where.= $where;
        isset($sch_level) && $sch_level!=""?$where.= " AND v_student_profile.schlevelid like '".$sch_level."%' ":$where.= $where;
        isset($years) && $years!=""?$where.= " AND v_student_profile.yearid like '".$years."%' ":$where.= $where;
        isset($classid) && $classid!=""?$where.= " AND v_student_profile.classid like '".$classid."%' ":$where.= $where;
        isset($gender) && $gender!=""?$where.= " AND v_student_profile.gender like '".$gender."%' ":$where.= $where;
        isset($s_sortby) && $s_sortby!=""?$sortstr.= " ORDER BY ".$s_sortby." ".$s_sorttype:$sortstr.=$sortstr;
             
		$sql = "SELECT DISTINCT
				v_student_profile.studentid,
				v_student_profile.student_num,
				v_student_profile.first_name,
				v_student_profile.last_name,
				v_student_profile.fullname,
				v_student_profile.first_name_kh,
				v_student_profile.last_name_kh,
				v_student_profile.fullname_kh,
				v_student_profile.first_name_ch,
				v_student_profile.last_name_ch,
				v_student_profile.fullname_ch,
				v_student_profile.gender,
				v_student_profile.is_active,
				v_student_profile.programid,
				v_student_profile.yearid,
				v_student_profile.classid,
				v_student_profile.class_name,
				v_student_profile.rangelevelid
				FROM 
				  v_student_profile							
				WHERE 1=1 {$where}
				AND v_student_profile.is_active=1
				ORDER BY studentid DESC";


		$table='';
		$pagina='';
		$getperpage=0;
		if($perpage==''){
			$getperpage=10;
		}else{
			$getperpage=$perpage;
		}
		$paging=$this->green->ajax_pagination(count($this->db->query($sql)->result()),site_url("student/c_quick_update_student/get_student_list"),$getperpage,"icon");
		
		$i=1;
		$getlimit=10;
		if($paging['limit']!=''){
			$getlimit=$paging['limit'];
		}
		$limit=" LIMIT {$paging['start']}, {$getlimit}";   

        if($sortstr!=""){
            $sql.=$sortstr;
        }
		$sql.=" {$limit}";
		 // return $sql;
		  $arr = array('sql' => $sql, 'paging' => $paging);
		  return $arr;
    }		

}