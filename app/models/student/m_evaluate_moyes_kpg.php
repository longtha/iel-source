
<?php
    class m_evaluate_moyes_kpg extends CI_Model{

    	function allclass(){
            return $this->db->query("SELECT DISTINCT
										c.classid,
										c.class_name
									FROM
										sch_class AS c
									ORDER BY
										c.class_name")->result();

		}

		function getmonthclass($classid,$yearid){
			return $this->db->query("SELECT DISTINCT(month),transno 
					FROM sch_student_evaluated 
					where classid='".$classid."'
					AND yearid='".$yearid."'
					AND evaluate_type='month'")->result_array();
		}
		
		function getteacherbyclass($classid,$yearid){
			return $this->db->select('emp.empid,emp.last_name,emp.first_name')
							->from('sch_teacher_class tc')
							->join('sch_emp_profile emp','tc.teacher_id=emp.empid','inner')
							->where('tc.class_id',$classid)->where('tc.yearid',$yearid)->get()->result();

		}
		
		function allclass_gradeid($schlevelid,$gradeid){
			$sqlsel="SELECT DISTINCT
									c.classid,
									c.class_name
								FROM
									sch_class AS c
								WHERE 1=1 
								AND c.grade_levelid ='".$gradeid."' 
								AND c.schlevelid ='".$schlevelid."'
								ORDER BY
									c.class_name";
			// echo $sqlsel;
		return $this->db->query($sqlsel)->result();
		
		}
		
		function getsubtypeid($schlevelid,$yearid){
			$sqlsel="SELECT
						sch_subject_type.schlevelid,
						sch_subject_type.yearid,
						sch_subject_type.subject_type,
						sch_subject_type.subj_type_id
						FROM
						sch_subject_type
						WHERE 1=1 
						AND schlevelid ='".$schlevelid."' 
						AND yearid ='".$yearid."'
						ORDER BY
							subject_type";
			// echo $sqlsel;
		return $this->db->query($sqlsel)->result();
		
		}
		public function Fgetm_s_y(){
			
		}
				
    }// end 