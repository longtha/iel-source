<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modattendance extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function getvalidate($schoolid,$schlevelid,$yearid,$classid,$termid,$date,$baseattid,$subjectid="")
    {
        $this->db->select('count(*)');
        $this->db->from('sch_studatt_daily');
        $this->db->where('schoolid',$schoolid);
        $this->db->where('schlevelid',$schlevelid);
        $this->db->where('yearid',$yearid);
        $this->db->where('classid',$classid);
        $this->db->where('termid',$termid);
        $this->db->where('date',$date);
        $this->db->where('baseattid',$baseattid);

        if($baseattid==1 && $subjectid!=""){
            $this->db->where('subjectid',$subjectid);
        }

        return $this->db->count_all_results();
    }
    function save($transno=""){

        date_default_timezone_set("Asia/Bangkok");
        $dailyattid=$this->input->post('dailyattid');

        $schoolid=$this->input->post('schoolid');
        $schlevelid=$this->input->post('sclevelid');
        $yearid=$this->input->post('yearid');

        $weekid=$this->input->post('weekid');
        $termid=$this->input->post('termid');
        $att_date=$this->input->post('att_date');
        $subjectid=$this->input->post('subjectid');

        $studentids=$this->input->post('studentids');
        $baseattid=$this->input->post('baseattid');
        $classid=$this->input->post('classid');

        $attnoteid=$this->input->post('attnoteids');
        $attend_scores=$this->input->post('attend_scores');
        $permis_score=$this->input->post('permis_score');
        $minus_absentscore=$this->input->post('minus_absentscore');
        $witdraw_late_point=$this->input->post('witdraw_late_point');
        $widrleave_early_point=$this->input->post('widrleave_early_point');
        $minus_permscore=$this->input->post('minus_permscore');
        $total_att_score=$this->input->post('total_att_score');


        $count=$this->getvalidate($schoolid,$schlevelid,$yearid,$classid,$termid,$this->green->formatSQLDate($att_date),$baseattid,$subjectid);
        $programid= $this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='".$schlevelid."'")->row()->programid;

        if(count($studentids)>0) {

            $type='30';
            $created_date = date("Y-m-d H:i:s");
            $created_by = $this->session->userdata('user_name');
            if($transno=="" && $count==""){

                $transno=$this->green->nextTran("31","Student Attendance");
                $attdaily=array('termid'=>$termid,
                                'weekid'=>$weekid,
                                'subjectid'=>$subjectid,
                                'date'=>$this->green->formatSQLDate($att_date),
                                'yearid'=>$yearid,
                                'schoolid'=>$schoolid,
                                'programid'=>$programid,
                                'schlevelid'=>$schlevelid,
                                'baseattid'=>$baseattid,
                                'type'=>$type,
                                'transno'=>$transno,
                                'classid'=>$classid,
                                'created_date'=>$created_date,
                                'created_by'=>$created_by
                                );
                $this->db->insert('sch_studatt_daily', $attdaily);
                $dailyattid=$this->db->insert_id();
            }else{
                $upwh=array('type'=>$type,'transno'=>$transno);
                $attdaily=array('termid'=>$termid,
                                'weekid'=>$weekid,
                                'subjectid'=>$subjectid,
                                'date'=>$this->green->formatSQLDate($att_date),
                                'yearid'=>$yearid,
                                'schoolid'=>$schoolid,
                                'programid'=>$programid,
                                'schlevelid'=>$schlevelid,
                                'baseattid'=>$baseattid,
                                'classid'=>$classid,
                                'modified_date'=>$created_date,
                                'modified_by'=>$created_by
                                );
                $this->db->update('sch_studatt_daily',$attdaily,$upwh);

                $arrdelwh=array("transno"=>$transno);
                $this->db->delete("sch_studatt_dailydetail",$arrdelwh);
            }
            $i=0;
            foreach($studentids as $studentid){

                $data=array('dailyattid'=>$dailyattid,
                            'studentid'=>$studentid,
                            'attnoteid'=>$attnoteid[$i],
                            'attend_score'=>$attend_scores[$i],
                            'permis_score'=>$permis_score[$i],
                            'minus_absentscore'=>$minus_absentscore[$i],
                            'witdraw_late_point'=>$witdraw_late_point[$i],
                            'widrleave_early_point'=>$widrleave_early_point[$i],
                            'minus_permscore'=>$minus_permscore[$i],
                            'termid'=>$termid,
                            'weekid'=>$weekid,
                            'full_score'=>$total_att_score[$i],
                            'subjectid'=>$subjectid,
                            'date'=>$this->green->formatSQLDate($att_date),
                            'yearid'=>$yearid,
                            'schoolid'=>$schoolid,
                            'programid'=>$programid,
                            'schlevelid'=>$schlevelid,
                            'baseattid'=>$baseattid,
                            'type'=>$type,
                            'transno'=>$transno,
                            'classid'=>$classid
                            );

                $this->db->insert('sch_studatt_dailydetail', $data);

                $i++;
            }
        }
        $m='';
        $p='';
        if(isset($_GET['m'])){
            $m=$_GET['m'];
        }
        if(isset($_GET['p'])){
            $p=$_GET['p'];
        }
        $res=0;
        if($transno!=""){
            $res=1;
        }
        redirect("student/attendance/add?res=".$res."&m=".$m."&p=".$p);
    }

    function search($start=0){

        $schoolid=(isset($_POST['schoolid']))?$_POST['schoolid']:"";
        $sclevelid=(isset($_POST['sclevelid']))?$_POST['sclevelid']:"";
        $yearid=(isset($_POST['yearid']))?$_POST['yearid']:"";
        $classid=(isset($_POST['classid']))?$_POST['classid']:"";

        $termid=(isset($_POST['termid']))?$_POST['termid']:"";
        $weekid=(isset($_POST['weekid']))?$_POST['weekid']:"";
        $baseattid=(isset($_POST['baseattid']))?$_POST['baseattid']:"";

        $from_date=(isset($_POST['from_date']))?$_POST['from_date']:"";
        $to_date=(isset($_POST['to_date']))?$_POST['to_date']:"";
        $subjectid=(isset($_POST['subjectid']))?$_POST['subjectid']:"";


        $WHERE="";
        $programid="";
        if($schoolid!=""){
            $WHERE.=" AND schoolid = '".$schoolid."'";
        }
        if($sclevelid!=""){
            $WHERE.=" AND schlevelid = '".$sclevelid."'";
            $programid= $this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='".$sclevelid."'")->row()->programid;
        }
        if($yearid!=""){
            $WHERE.=" AND yearid = '".$yearid."'";
        }
        if($classid!=""){
            $WHERE.=" AND classid = '".$classid."'";
        }
        if($termid!=""){
            $WHERE.=" AND termid = '".$termid."'";
        }
        if($weekid!=""){
            $WHERE.=" AND weekid = '".$weekid."'";
        }
        if($baseattid!=""){
            $WHERE.=" AND baseattid = '".$baseattid."'";
        }
        if($subjectid!="" && $baseattid==1){
            $WHERE.=" AND subjectid = '".$subjectid."'";
        }
        if($from_date!="" && $to_date!=""){
            $WHERE.=" AND date between  '". $this->green->formatSQLDate($from_date)."'
            AND '".$this->green->formatSQLDate($to_date)."'";
        }
        
        $v_attdaily="v_attdaily_kgp";

        if($programid==2){
            $v_attdaily="v_attdaily_iep";
        }elseif($programid==3){
            $v_attdaily="v_attdaily_gep";
        }

        $sql="SELECT * FROM {$v_attdaily} WHERE 1=1 {$WHERE} ORDER BY schoolid,programid,schlevelid,yearid";

        $total_row=$this->green->getValue("select count(*) as numrow FROM ($sql) as cc ");
        $paging=$this->green->ajax_pagination($total_row,site_url("student/attendance/search"),100);

        if(isset($start) && $start>0){
            $paging['start']=($start-1)*$paging['limit'];
        }
        $data=$this->green->getTable("$sql limit {$paging['start']}, {$paging['limit']}");
        $arrJson['paging']=$paging;

        $arrJson['datas']=$data;
        header("Content-type:text/x-json");

        echo json_encode($arrJson);
        exit();
    }
    function delete($transno){
        $result['del']=0;
        $candel=1;

        if($transno!="" && $candel==1){
            $this->db->delete('sch_studatt_daily', array('transno' => $transno));
            $this->db->delete('sch_studatt_dailydetail', array('transno' => $transno));
            if ($this->db->_error_message()) {
                $result['del'] = 'Error! ['.$this->db->_error_message().']';
            } else if (!$this->db->affected_rows()) {
                $result['del'] = 'Error! ID ['.transno.'] not found';
            } else {
                $result['del'] = 'Success';
            }
        }
        echo json_encode($result);
        exit();
    }
    function edit($transno,$programid){
        $rngdata['attdailyrow']=$this->getattdailyrow($transno,$programid);
        $rngdata['attdailydetrow']=$this->getattdailydetrow($transno,$programid);
        $rngdata['attnote']=$this->db->get("sch_stud_attnote")->result();
        return $rngdata;
    }


    function attnotes(){
        return $this->db->get("sch_stud_attnote")->result();
    }
    function attbase($schlevelid=""){
        $rec=array();
        if($schlevelid!=""){
            $this->db->where("schlevelid",$schlevelid);
            $rec=$this->db->get("v_attbaseschlevel")->result();
        }else{
            $this->db->where("isactive",1);
            $rec=$this->db->get("sch_z_att_base")->result();
        }

        return $rec;
    }
    function  getattdailyrow($transno,$programid=1){
        $this->db->where("transno",$transno);
        $v_attdaily="v_attdaily_kgp";
        if($programid=="2"){

        }elseif($programid=="3"){
            $v_attdaily="v_attdaily_gep";
        }
        return $this->db->get($v_attdaily)->result();
    }
    function  getattdailydetrow($transno,$programid=1){
        $this->db->where("transno",$transno);
        $v_attdaily="v_attdailydetail_kgp";
        if($programid=="2"){

        }elseif($programid=="3"){
            $v_attdaily="v_attdailydetail_gep";
        }

        return $this->db->get($v_attdaily)->result();
    }

}