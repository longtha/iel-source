<?php
	class Modepermission extends CI_Model{
		
		function getperm_row($perm_id){
			$this->db->where('requestid',$perm_id);
			$query= $this->db->get('sch_emp_permission_request');
			return $query->row_array();
		}
		function getpermis($empid,$year,$reason){
			$where='';
			if($reason=='SL' || $reason=='AL')
				$where=" AND empr.reason='$reason'";
			else
				$where=" AND empr.reason<>'SL' AND empr.reason<>'AL'";
			$sql="SELECT * 
					FROM sch_emp_permission_request empr
					INNER JOIN sch_emp_profile emp
					ON(emp.empid=empr.immediate_sup_name)
					WHERE empr.empid='$empid' 
					AND empr.year='$year' {$where}";
			return $this->db->query($sql)->result();
		}
		function searchperm($sort_num,$empid,$request_type,$reason,$sort,$sort_ad,$m,$p,$year){
			$page=0;
			if(isset($_GET['per_page'])) $page=$_GET['per_page'];
			$this->load->library('pagination');	
			$config['base_url'] = site_url()."/employee/permission/search_perm?sort_num=$sort_num&empid=$empid&request_type=$request_type&sort=$sort&sort_ad=$sort_ad&m=$m&p=$p&leave_type=$reason&y=$year";
			
			$where = "";
			if( $empid !=""){
				$where .=" AND (CONCAT(emp.last_name,' ',emp.first_name) LIKE '$empid%' 
								OR CONCAT(emp.first_name,' ',emp.last_name) LIKE '%$empid%' 
								OR CONCAT(emp.first_name_kh,' ',emp.last_name_kh) LIKE '%$empid%' 
								OR CONCAT(emp.last_name_kh,' ',emp.first_name_kh) LIKE '%$empid%' 
								OR emp.empcode LIKE '%$empid%' 
							)";
			}
			if( $request_type !=""){
				$where .=" AND epr.request_type = '".$request_type."'";
			}
			if( $reason !=""){
				$where .=" AND epr.reason = '".$reason."'";
			}
			if ($year != ''){
				$where .= "AND epr.year = '".$year."'";
			}
			if($sort_ad!=''){
				$sql = "SELECT emp.last_name,
								emp.first_name,
								sup.last_name as sup_last_name,
								sup.first_name as sup_first_name,
								epr.from,
								epr.reason,
								epr.to,
								epr.date_request,
								epr.request_type,
								epr.requestid 
						FROM sch_emp_permission_request epr 
						LEFT JOIN sch_emp_profile emp 
						ON(epr.empid=emp.empid)
						LEFT JOIN sch_emp_profile sup
						ON(sup.empid=epr.immediate_sup_name) 
						WHERE 1=1 {$sort_ad}";
			}else{
				$sql = "SELECT emp.last_name,
								emp.first_name,
								sup.last_name as sup_last_name,
								sup.first_name as sup_first_name,
								epr.from,
								epr.reason,
								epr.to,
								epr.date_request,
								epr.request_type,
								epr.requestid 
						FROM sch_emp_permission_request epr 
						LEFT JOIN sch_emp_profile emp 
						ON(epr.empid=emp.empid) 
						LEFT JOIN sch_emp_profile sup
						ON(sup.empid=epr.immediate_sup_name)
						WHERE 1=1 {$where}";
			}
			$config['total_rows'] = $this->green->getTotalRow($sql);
		    $config['per_page'] =$sort_num;
		   	$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
		    $this->pagination->initialize($config); 
		    $limi=" limit ".$config['per_page'];
		    if($page>0){
		     $limi=" limit ".$page.",".$config['per_page'];
		    }  
		    $sql.="$sort {$limi}";
			$query=$this->green->getTable($sql);
			return $query;	
		}
	}
?>