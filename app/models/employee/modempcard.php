<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Modempcard extends CI_Model
{ //CI_Model
	public function setSql($where='')
	{
		$sql = "SELECT DISTINCT
						sep.dep_id,
						sep.empid,
						sep.empcode,
						sep.first_name,
						sep.last_name,
						sep.first_name_kh,
						sep.last_name_kh,
						sep.phone,
						sep.email,
						sep.perm_adr,
						epos.posid,
						epos.position
					FROM
						sch_emp_profile as sep
					LEFT JOIN sch_emp_position as epos ON epos.posid = sep.pos_id
					WHERE sep.is_active=1 {$where}
					GROUP BY
						sep.empcode
					ORDER BY
						sep.empid DESC";
		return $this->db->query($sql);
	}
	public function preempcard($emp_dep_id='')
	{
		$where='';
		if($emp_dep_id>0)
		{
			$where.= " AND sep.dep_id = '".$emp_dep_id."'";
		}else{
			$where.= " AND sep.dep_id = '-1'";
		}
		
		return $this->setSql($where);
	}
	public function getpreempcard($empid)
	{
		$where='';
		if($empid !='')
		{
			$where.= " 	AND sep.empid = '".$empid."'";
		}
		return $this->setSql($where);
	}
	public function urlimagesemp($yearid='',$empid)
	{
		$img_path=base_url('assets/upload/No_person.jpg');
		if(file_exists(FCPATH.'assets/upload/employees/photos/'.$empid.'.jpg')){
	        $img_path=base_url('assets/upload/employees/photos/'.$empid.'.jpg');
	       
	    } 
	    return $img_path;
	}

	public function emplistcard($img_path,$empname,$idcard,$position,$department,$fulladdress,$phoneline,$empmail)
	{
		$tr='<tr>
				<td>
					<table  border="1"  style="width:750px;margin:5px 7px;border:1px solid #ccc;" align="center">
						<tr>
							<td style="width:180px;border-right:none;" valign="top"> 
								<img src="'.$img_path.'" style="width:180px; padding:4px;"/>
							</td>
							<td style="border-left:none">
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<label class="control-label labelname">Name</label>
										</td>
										<td><label class="control-label shwo_labelname">:</label></td>
										<td>
											<label class="control-label shwo_labelname">&nbsp;'.$empname.'</label>	
										</td>
									</tr>
									<tr>
										<td>
											<label class="control-label labelname">ID</label>
										</td>
										<td><label class="control-label shwo_labelname">:</label></td>
										<td>
											<label class="control-label shwo_labelname">&nbsp;'.$idcard.'</label>	
										</td>
									</tr>
									<tr>
										<td>
											<label class="control-label labelname">Position</label>
										</td>
										<td><label class="control-label shwo_labelname">:</label></td>
										<td>
											<label class="control-label css_pos_dep">&nbsp;'.$position.'</label>
										</td>
									</tr>
									<tr>
										<td>
											<label class="control-label labelname"> Depart</label>
										</td>
										<td><label class="control-label shwo_labelname">:</label></td>
										<td>
											<label class="control-label css_pos_dep">&nbsp;'.$department.'</label>
										</td>
									</tr>
									<tr>
										<td  valign="top">
											<label class="labelname">Address</label>
										</td>
										<td  valign="top"><label class="shwo_labelname">:</label></td>
										<td>
											<label class="control-label  css_address">&nbsp;'.$fulladdress.'</label>
										</td>
									</tr>
									<tr>
										<td>
											<label class="control-label labelname">Mobile</label>
										</td>
										<td><label class="control-label shwo_labelname">:</label></td>
										<td>
											<label class="control-label css_phone">&nbsp;'.$phoneline.'</label>
										</td>
									</tr>
									<tr>
										<td valign="bottom">
											<label class="control-label labelname">E-mail</label>
										</td>
										<td><label class="control-label shwo_labelname">:</label></td>
										<td valign="middle">
											<label class="control-label labelname">&nbsp;<u>'.$empmail.'</u></label>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>';
		return $tr;	
	}
	public function showdataindialog($empid,$empcode,$empname,$empposition,$mobelephone)
	{
		$tr='<tr class="cleartbl">
				<td>
					<input type="checkbox" checked="checked" name="chempcode" id="chempcode" class="chempcode" value="'.$empid.'"/>
				</td>
        		<td>'.$empcode.'</td>
        		<td>'.$empname.'</td>
        		<td>'.$empposition.'</d>
        		<td>'.$mobelephone.'</td>
        	</tr>';
        return $tr;
	}
}