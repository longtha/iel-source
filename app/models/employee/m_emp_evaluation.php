<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_emp_evaluation extends CI_Model{
		
		// save =============================================================
		function save(){ 
			$transno = $this->green->nextTran("16","Staff Evaluation");
			$idhidden_evalmen    = $this->input->post('idhidden_evalmen')-0;
			$idhidden_emp        = $this->input->post('idhidden_emp');
			$emp_rev_formdate    = $this->input->post('emp_rev_formdate');
			$emp_rev_todate      = $this->input->post('emp_rev_todate');
			$idhidden_eval       = $this->input->post('idhidden_eval');
			$evaluator_ofdate    = $this->input->post('evaluator_ofdate');
			$supervisor_date     = $this->input->post('supervisor_date');
			$comments_supervisor = $this->input->post('comments_supervisor');
			$supervisor_date     = $this->input->post('supervisor_date');
			$comments_employee   = $this->input->post('comments_employee');
			$employee_date       = $this->input->post('employee_date');
			$comments_manager    = $this->input->post('comments_manager');
			$idhidden_gm     	 = $this->input->post('idhidden_gm');
			$gm_date             = $this->input->post('gm_date');
			$ove_comment         = $this->input->post('ove_comment');
			$CKcheckbox          = $this->input->post('CKcheckbox');
			$hidden_group_id     = $this->input->post('hidden_group_id');
			$hidden_streng       = $this->input->post('hidden_streng');
			$hidden_improment    = $this->input->post('hidden_improment');

			$data = array(
						'evalid'          => $idhidden_evalmen,
						'type'            => 16,
						'transno'		  => $transno,
						'eval_date'       => $evaluator_ofdate,
						'empid'           => $idhidden_emp,
						'review_from_date'=> $emp_rev_formdate,
						'review_to_date'  => $emp_rev_todate,
						'eval_by_empid'   => $idhidden_eval,
						'sup_comment'     => $comments_supervisor,
						'sup_emp_id'      => $idhidden_eval,
						'sup_comment_date'=> $supervisor_date,
						'emp_comment'     => $comments_employee,
						'emp_comment_date'=> $employee_date,
						'gm_comment'      => $comments_manager,
						'gm_emp_id'       => $idhidden_gm,
						'gm_date'         => $gm_date,
						'ove_comment'     => $ove_comment,
						'ove_com_id'      => $CKcheckbox
						);
				//	$this->db->insert('sch_eva_emp_evalutiont',$data);
			if($idhidden_evalmen > 0){
			   $this->db->update('sch_eva_emp_evalutiont',$data,array('evalid' => $idhidden_evalmen));
			}else{
			   $this->db->insert('sch_eva_emp_evalutiont',$data);
			}

			// sch_eva_emp_streng =================================
			//echo $idhidden_evalmen;
			//exit();
			if($idhidden_evalmen > 0){ // for update
				//======== Delete first ===========================
				$this->delete_emp_streng($idhidden_evalmen);
				$this->delete_emp_improvement($idhidden_evalmen);
				$this->delete_emp_group($idhidden_evalmen);
				//======== New insert delete_emp_streng ======================
				$evalid = $idhidden_evalmen;

				$strengths = $this->input->post('arr');
				if(is_array($strengths) > 0){
					foreach($strengths as $row){
						$data_strengths = array('streng_id' => $hidden_streng,
												'evalid'    => $evalid,
												'type'      => 16,
												'transno'   => $transno,
												'empid'     => $idhidden_emp,
												'strength'  => $row['strengths']);
						$this->db->insert('sch_eva_emp_streng', $data_strengths);
					}
				}

				// ====== New insert to sch_eva_emp_improvements =================
				$im_proment = $this->input->post('arr1');
				if(is_array($im_proment) > 0){
					foreach($im_proment as $row1){
						$data_im_proment = array('improvl_id'=> $hidden_improment,
												'evalid'     => $evalid,
												'type'       => 16,
												'transno'    => $transno,
												'empid'      => $idhidden_emp,
												'improment'  => $row1['im_proment']);
							$this->db->insert('sch_eva_emp_improvements', $data_im_proment);
						
					}
				}
				//============= New insert to sch_eva_emp_group ===================
				$comments_eval = $this->input->post('arr2');
				if(is_array($comments_eval) > 0){
					foreach($comments_eval as $row2){
						//$transno = $this->green->nextTran("16","Staff Evaluation");
						$data_comments_eval = array('group_id'    => $hidden_group_id,
													'evalid'      => $evalid,
													'type'        => 16,
													'transno'     => $transno ,
													'grop_eval_id'=> $row2['idhidden_geva'],
													'rate'        => $row2['radio_check'],
													'comment'     => $row2['comments_eval']);
						$this->db->insert('sch_eva_emp_group', $data_comments_eval);
						
					}
				}

			}else{ // end for update
				//========== Insert sch_eva_emp_streng ========================
				$evalid = $this->db->insert_id();

				$strengths = $this->input->post('arr');
				if(is_array($strengths) > 0){
					foreach($strengths as $row){
						$data_strengths = array('streng_id' => $hidden_streng,
												'evalid'    => $evalid,
												'type'      => 16,
												'transno'   => $transno,
												'empid'     => $idhidden_emp,
												'strength'  => $row['strengths']);
						$this->db->insert('sch_eva_emp_streng', $data_strengths);
						
					}
				}

				//=========== Insert sch_eva_emp_improvements ==================		
				$im_proment = $this->input->post('arr1');
				if(is_array($im_proment) > 0){
					foreach($im_proment as $row1){
						$data_im_proment = array('improvl_id'=> $hidden_improment,
												'evalid'     => $evalid,
												'type'       => 16,
												'transno'    => $transno,
												'empid'      => $idhidden_emp,
												'improment'  => $row1['im_proment']);
							$this->db->insert('sch_eva_emp_improvements', $data_im_proment);
						
					}
				}
				//=============Insert sch_eva_emp_group ===========================
				$comments_eval = $this->input->post('arr2');
				if(is_array($comments_eval) > 0){
					foreach($comments_eval as $row2){
						$data_comments_eval = array('group_id'    => $hidden_group_id,
													'evalid'      => $evalid,
													'type'        => 16,
													'transno'     => $transno ,
													'grop_eval_id'=> $row2['idhidden_geva'],
													'rate'        => $row2['radio_check'],
													'comment'     => $row2['comments_eval']);
						$this->db->insert('sch_eva_emp_group', $data_comments_eval);
						
					}
				}
			}
			die();
		}
		function delete_emp_streng($id){
			$this->db->where('evalid',$id);
			$this->db->delete('sch_eva_emp_streng');
		}
		function delete_emp_improvement($id){
			$this->db->where('evalid',$id);
			$this->db->delete('sch_eva_emp_improvements');
		}
		function delete_emp_group($id){
			$this->db->where('evalid',$id);
			$this->db->delete('sch_eva_emp_group');
		}
		function sch_eva_emp_evalutiont($transno){ 
			// echo $transno;
			$sql_query = $this->db->query("SELECT
											sch_eva_emp_evalutiont.evalid,
											sch_eva_emp_evalutiont.type,
											sch_eva_emp_evalutiont.transno,
											sch_eva_emp_evalutiont.eval_date,
											sch_eva_emp_evalutiont.empid,
											sch_eva_emp_evalutiont.review_from_date,
											sch_eva_emp_evalutiont.review_to_date,
											sch_eva_emp_evalutiont.eval_by_empid,
											sch_eva_emp_evalutiont.sup_comment,
											sch_eva_emp_evalutiont.sup_emp_id,
											sch_eva_emp_evalutiont.sup_comment_date,
											sch_eva_emp_evalutiont.emp_comment,
											sch_eva_emp_evalutiont.emp_comment_date,
											sch_eva_emp_evalutiont.gm_comment,
											sch_eva_emp_evalutiont.gm_emp_id,
											sch_eva_emp_evalutiont.gm_date,
											sch_eva_emp_evalutiont.ove_com_id,
											sch_eva_emp_evalutiont.ove_comment
										FROM
											sch_eva_emp_evalutiont 
										WHERE transno = '".$transno."'")->result();
			
			return $sql_query; 

		}
		
	}
?>