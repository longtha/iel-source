<?php
	class ModDepartment extends CI_Model{
		function getdepartment(){
				$page=0;
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				$m='';
				$p='';
				if(isset($_GET['m'])){
		        	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }	

				$sql="SELECT * FROM sch_emp_department ORDER BY dep_id DESC";	
				$config['base_url'] =site_url()."/employee/department/index?pg=1&m=$m&p=$p";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =50;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				
				return $this->db->query($sql)->result();
		}
		function search($department,$s_num,$m,$p){
				$page=0;
				$where='';
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				if($department!='')
					$where.=" AND department LIKE '%".$department."%'";
					$sql="SELECT * FROM sch_emp_department where 1=1 {$where} ORDER BY dep_id DESC";	
					$config['base_url'] =site_url()."/employee/department/search?pg=1&m=$m&p=$p&dep=$department&s_num=$s_num";	
					$config['total_rows'] = $this->green->getTotalRow($sql);
					$config['per_page'] =$s_num;
					$config['num_link']=5;
					$config['page_query_string'] = TRUE;
					$config['full_tag_open'] = '<li>';
					$config['full_tag_close'] = '</li>';
					$config['cur_tag_open'] = '<a><u>';
					$config['cur_tag_close'] = '</u></a>';
					$this->pagination->initialize($config);
					$limi=" limit ".$config['per_page'];
					if($page>0){
							$limi=" limit ".$page.",".$config['per_page'];
						}		
					$sql.=" {$limi}";
					return $this->db->query($sql)->result();
		}
		function getHoD($empid){
			return $this->green->getValue("SELECT	
									CONCAT(last_name, ' ', first_name) AS NAME
									FROM
										sch_emp_profile AS emp
									WHERE
										emp.empid = '{$empid}'
									");
		}
	}