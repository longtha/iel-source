<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_overal_comment extends CI_Model{

		// save =============================================================
		function save(){ 
			$idhidden   = $this->input->post('idhidden')- 0;
			$overall_comment = $this->input->post('overall_comment');
			$overall_comment_kh = $this->input->post('overall_comment_kh');
			$CKcheckbox = $this->input->post('CKcheckbox');
			$data = array(
						'ove_com_id'            => $idhidden,
						'description_overall'   => $overall_comment,
						'description_overall_kh'=> $overall_comment_kh,
						'isfillout'				=> $CKcheckbox
			 			);
			
			if($idhidden > 0){
			  $this->db->update('sch_evaoverall_comments',$data,array('ove_com_id' => $idhidden));
			}else{
			   $this->db->insert('sch_evaoverall_comments',$data);
			}
			die();
		}
		//  editdata =========================================================
		function editdata(){
			$idhidden = $this->input->post('idhidden');
			$sql = $this->db->query("SELECT
										sch_evaoverall_comments.ove_com_id,
										sch_evaoverall_comments.description_overall,
										sch_evaoverall_comments.description_overall_kh,
										sch_evaoverall_comments.isfillout
									FROM
										sch_evaoverall_comments WHERE ove_com_id = '".$idhidden."'")->row();
			
			return  $sql;
		}
		//  delete ============================================================
		function deletedata(){
			$idhidden = $this->input->post('deletedata');
			$this->db->delete('sch_evaoverall_comments',array('ove_com_id'=>$idhidden));
			die();
		}
		//  showdata ==========================================================
		function showdata(){
			$this->sql = $this->db->query("SELECT
										sch_evaoverall_comments.ove_com_id,
										sch_evaoverall_comments.description_overall,
										sch_evaoverall_comments.description_overall_kh,
										sch_evaoverall_comments.isfillout
									FROM
										sch_evaoverall_comments");
					return $this->sql;
			die();
		}
	}
?>