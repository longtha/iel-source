<?php
	class Modemployee extends CI_Model{

		//------Get Max ID of Employee---------
		function getmaxid(){
			$this->db->select_max('empid','max');
			$this->db->from('sch_emp_profile');
			$query=$this->db->get();
			return $query->row()->max+1;
		}
		//-----Get Position ID of Employee------
		function getposid($pos_id){
			$this->db->where('posid',$pos_id);
			return $this->db->get('sch_emp_position');
		}
	
		function get_pos($pos_id, $emp_type){
			$this->db->where('posid',$pos_id);
			$this->db->where('match_con_posid', $emp_type);
			$query = $this->db->get('sch_emp_position');
			//$query->row_array();
			$option ="";
			foreach($query->result() as $posid ){
			 '<option value="'.$posid->posid.'">'.$posid->position.'</option>';
		}
		echo $option;
		}
		function view_employee($empid){
			$query=$this->db->query("SELECT DISTINCT
										sch_emp_profile.dep_id,
										sch_emp_profile.empid,
										sch_emp_profile.empcode,
										sch_emp_profile.first_name,
										sch_emp_profile.last_name,
										sch_emp_profile.first_name_kh,
										sch_emp_profile.last_name_kh,
										sch_emp_profile.marital_status,
										sch_emp_profile.idcard,
										sch_emp_profile.nationality,
										sch_emp_profile.is_foreigner,
										sch_emp_profile.emp_type,
										sch_emp_profile.phone,
										sch_emp_profile.email,
										sch_emp_profile.sex,
										sch_emp_profile.village,
										sch_emp_profile.commune,
										sch_emp_profile.province,
										sch_emp_profile.zoon,
										sch_emp_profile.district,
										sch_emp_profile.perm_adr,
										sch_emp_profile.note,
										sch_emp_profile.dob,
										sch_emp_profile.pob,
										sch_emp_profile.employed_date,
										sch_emp_profile.resigned_date,
										sch_emp_profile.leave_school,
										sch_emp_profile.leave_school_reason,
										sch_emp_profile.office_location,
										sch_emp_profile.contract_type,
										sch_emp_profile.annual_leave,
										sch_emp_profile.sick_leave,										
										sch_emp_profile.al_taken,
										sch_emp_profile.sick_taken,
										sch_emp_profile.con_id,
										sch_emp_profile.leave_reason,
										sch_emp_profile.leave_type,
										sch_emp_profile.leave_comment,
										sch_emp_position.posid,
										sch_emp_position.position,										
										sch_emp_profile.last_modified_date,
										sch_emp_profile.last_modified_by,
										sch_emp_profile.kh_newyear_hol,
										sch_emp_profile.endoftheyear_hol,
										sch_emp_profile.is_active
									FROM
										sch_emp_profile
									LEFT JOIN sch_emp_position ON sch_emp_position.posid = sch_emp_profile.pos_id
									WHERE sch_emp_profile.empid='$empid'
									GROUP BY
										sch_emp_profile.empcode
									ORDER BY
										sch_emp_profile.empid DESC
						");
			//$query=$this->db->get($query);
			return $query->row_array();
		}

		function getuserrow($emp_id){
			$row_user = $this->db->query("SELECT DISTINCT
													r.role,
													r.roleid,
													u.userid,
													u.user_name,
													u.`password`,
													u.email,
													u.last_visit,
													u.last_visit_ip,
													u.created_date,
													u.created_by,
													u.modified_by,
													u.modified_date,
													u.roleid,
													u.last_name,
													u.first_name,
													u.is_admin,
													u.schoolid,
													u.`year`,
													u.is_active,
													u.def_sch_level,
													u.def_dashboard,
													u.def_open_page,
													u.emp_id,
													u.match_con_posid
												FROM
													sch_user AS u
												INNER JOIN sch_z_role AS r ON u.roleid = r.roleid
												WHERE
													u.is_active = '1'
												AND u.emp_id = '{$emp_id}'
												AND ( u.match_con_posid != 'stu' or   u.match_con_posid != 'parent') ")->row_array();
			return $row_user;
		}

		function saveImport(){
			$is_imported=false;
			if (isset($_FILES['file']) && $_FILES['file']["tmp_name"][0]!="") {

				$_xfile = $_FILES['file']["tmp_name"];
				function str($str){
					return str_replace("'","''",$str);
				}
				if($_FILES['file']['tmp_name']==""){

					$error="<font color='red'>Please Choose your Excel file!</font>";					
				}
				else{
					$html="";					
					require_once(APPPATH.'libraries/simplexlsx.php' );								
					foreach($_xfile as $index => $value){

						$xlsx = new SimpleXLSX($value);				
						$_data = $xlsx->rows();
						array_splice($_data,0,1);
						$error_record_exist="";
						foreach( $_data as $k => $r) {										
							$_check_exist = $this->green->getTable("SELECT * FROM sch_emp_profile WHERE empcode = '{$r[1]}'");					
							$seachspance = strpos(trim(str($r[1]))," ");
							if (count($_check_exist)> 0)
							{
								$error_record_exist .='<div class="success" style=" padding:5px; margin-top:5px;">
														<font style="color:red;">Code already exist'.$r[1].' exist aleady !</font><br>
														</div>';					
							}else if($seachspance !== false){
								$error_record_exist .='<div class="success" style=" padding:5px; margin-top:5px;">
													<font style="color:red;">Error Code'.str($r[1]).' is incorrect. Please try again !</font></div>';
							}
							else {
							//-----------------------------------------------------------	
								$empcode = trim(str($r[1]));
								
								$first_name = trim(str($r[3]));
								$last_name = trim(str($r[4]));
								$first_name_kh = trim(str($r[6]));
								$last_name_kh = trim(str($r[7]));

								$nationality = trim(str($r[8]));
								$sex = trim(str($r[9]));								
								$dep_id=trim(str($r[11]));
								$office_location= trim(str($r[12]));

								$pos_id = trim(str($r[15]));
								$dob = trim(str($r[16]));
								$employed_date = trim(str($r[18]));
								$contract_type= trim(str($r[19]));
								$phone = trim(str($r[20]));	
								$pob = trim(str($r[21]));								

								$perm_adr = trim(str($r[21]));
								$village = trim(str($r[21]));
								$commune = trim(str($r[21]));
								$district = trim(str($r[21]));
								$province = trim(str($r[21]));
								$zoon = trim(str($r[21]));


								$marital_status ="";								
								$emp_type = trim("FT");					
								
								$email ="";
								$leave_school ="";
								$leave_school_reason ="";
								$idcard = "";
								$is_foreigner =0;
								$note = "";
								
								
								$created_date = date("Y-m-d H:i:s");
								$created_by =$this->session->userdata('user_name');
								
								//-----------------------------------------------------------
								$SQL_DM="INSERT INTO sch_emp_profile";								
															
								$SQL=" SET empcode='".$empcode."',
											first_name='".$first_name."',
											last_name='".$last_name."',
											first_name_kh='".$first_name_kh."',
											last_name_kh='".$last_name_kh."',
											sex='".$sex."',
											dob='".$dob."',
											pob='".$pob."',
											perm_adr='".$this->db->escape_str($perm_adr)."',
											village='".$village."',
											commune='".$commune."',
											district='".$district."',
											province='".$province."',
											zoon='".$zoon."',
											marital_status='".$marital_status."',
											nationality='".$nationality."',
											emp_type='".$emp_type."',
											pos_id='".$pos_id."',
											phone='".$phone."',
											email='".$email."',
											leave_school='".$leave_school."',
											leave_school_reason='".$leave_school_reason."',
											idcard='".$idcard."',
											is_foreigner='".$is_foreigner."',
											note='".$this->db->escape_str($note)."',
											employed_date='".$employed_date."',
											contract_type='".$contract_type."',
											dep_id='".$dep_id."',
											office_location='".$office_location."',
											created_by='".$created_by."',
											created_date='".$created_date."'

										";
								$counts=$this->green->runSQL($SQL_DM.$SQL);
								$is_imported=true;								
							}					
						}
					}				
				}
			}
			return $is_imported;
		}
		//-------------------------------
		function searchemployee($sch_empId, $sch_fullname, $sch_fullname_kh, $sort_num,$sort,$sort_ad,$pos_id,$m,$p,$status=''){
			
			$page=0;
			if(isset($_GET['per_page'])) $page=$_GET['per_page'];
			$this->load->library('pagination');
			$config['base_url'] = site_url()."/employee/employee/search_emp?emp_id=$sch_empId&fn=$sch_fullname&fnk=$sch_fullname_kh&s_num=$sort_num&sort=$sort&sort_ad=$sort_ad&pos_id=$pos_id&m=$m&p=$p&st=$status";
			
			$where = "";
			if($sch_empId !=""){
				$where.=" AND sch_emp_profile.empcode LIKE '%$sch_empId%'";
			}
			if($sch_fullname !=""){
				$where .=" AND
							CONCAT(sch_emp_profile.last_name,' ',sch_emp_profile.first_name) LIKE '%".$sch_fullname."%'
							OR CONCAT(sch_emp_profile.first_name,' ',sch_emp_profile.last_name) LIKE '%".$sch_fullname."%'
						   ";
			}
			if($pos_id !=""){
				$where.=" AND sch_emp_position.position LIKE '%$pos_id%'";
			}

			if($sch_fullname_kh !=""){
				$where .=" AND
							CONCAT(sch_emp_profile.first_name_kh,' ',sch_emp_profile.last_name_kh) LIKE '%".$sch_fullname_kh."%'
							OR CONCAT(sch_emp_profile.last_name_kh,' ',sch_emp_profile.first_name_kh) LIKE '%".$sch_fullname_kh."%'
						    ";
			}
			if($status!=""){
				$where .=" AND sch_emp_profile.`is_active` = '".$status."'";
			}
			$sql = "SELECT DISTINCT
							sch_emp_profile.dep_id,
							sch_emp_profile.empid,
							sch_emp_profile.empcode,
							sch_emp_profile.first_name,
							sch_emp_profile.last_name,
							sch_emp_profile.first_name_kh,
							sch_emp_profile.last_name_kh,
							sch_emp_profile.dob,
							sch_emp_profile.employed_date,
							sch_emp_profile.resigned_date,
							sch_emp_profile.is_active,
							sch_emp_position.position
					FROM
						sch_emp_profile
					LEFT JOIN sch_emp_position ON sch_emp_position.posid = sch_emp_profile.pos_id
					WHERE 1=1
					";
				
			if($sort_ad!=''){
				$sql .= " $sort_ad";
			}else{
				$sql .= " $where";
			}

			$config['total_rows'] = $this->green->getTotalRow($sql);
		    $config['per_page'] = $sort_num;
		    $config['num_link']=5;
		    $config['page_query_string'] = TRUE;
		    $config['full_tag_open'] = '<li>';
		    $config['full_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<a><u>';
		    $config['cur_tag_close'] = '</u></a>'; 
		    $this->pagination->initialize($config); 
		    $limi=" limit ".$config['per_page'];
		    if($page>0){
		     	$limi=" limit ".$page.",".$config['per_page'];
		    }  
		    $sql.=" $sort {$limi} ";
			$query=$this->green->getTable($sql);
			return $query;
		}
		
		function getLastContInf($empid){
			$row="";		
			if($empid!=""){
				$row= $this->green->getOneRow("SELECT * FROM sch_emp_contract 
													WHERE empid ='".$empid."' 
													ORDER BY end_date DESC LIMIT 0,1");
			}
			// if($row['contractid']==""){
			// 	$row['contractid']="";
			// 	$row['con_id']="";
			// }
			return $row;				
		}
		function getexp($sch_empId,$sch_fullname,$sch_fullname_kh,$s_status,$pos_id,$sort,$sort_num,$page,$is_all){
				
				$WHERE="";
			    if($sch_empId!=""){
			     $WHERE.=" AND empcode like '%$sch_empId%' ";
			    }
			    if($sch_fullname!=""){
			     $WHERE.=" AND CONCAT(last_name,' ',first_name) like '%$sch_fullname%' 
			     			OR CONCAT(first_name,' ',last_name) like '%$sch_fullname%";
			    }
			    if($sch_fullname_kh!=""){
			     $WHERE.=" AND CONCAT(last_name_kh,' ',first_name_kh) like '%$sch_fullname_kh%' 
			     		    OR CONCAT(first_name_kh,' ',last_name_kh) like '%$sch_fullname_kh%";
			    }
			    if($s_status!=""){
			     $WHERE.=" AND ep.is_active = $s_status ";
			    }
			    if($pos_id!=""){
			     $WHERE.=" AND epo.position like '%$pos_id%' ";
			    }
				$sql="SELECT * FROM sch_emp_profile ep
							LEFT JOIN sch_emp_position epo ON ep.pos_id=epo.posid
							LEFT JOIN sch_emp_department ed ON ep.dep_id=ed.dep_id  WHERE 1=1 {$WHERE} ";
				$limi='';
				if($is_all==0){
					$limi=" limit $sort_num";
					if($page!='' && $page>0){
							$limi=" limit ".$page.",".$sort_num;
					}
				}

				$sql.=" $sort {$limi}";
				return $this->db->query($sql)->result_array();
		}
		function getContractData($empid,$status){
			$WHERE="";

			if($empid!=""){
				$WHERE.=" AND cont.empid='".$empid."'";				
			}
			if($status!=""){
				$WHERE.=" AND cont.status='".$status."'";				
			}
			$data=$this->green->getTable("SELECT con.con_id,
												con.contractid,
												con.empid,
												con.contract_type,
												con.contract_attach,
												con.begin_date,
												con.end_date,
												con.`year`,
												con.job_type,
												con.created_by,
												con.created_date,
												con.last_modified_by,
												con.last_modified_date,
												con.decription,
												con.`status` 
												FROM sch_emp_contract as cont 
												WHERE 1=1 {$WHERE}");
			return $data;
		}
	}
?>