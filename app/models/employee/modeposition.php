<?php
	class Modeposition extends CI_Model{

		function getpos_row($posid){
			$this->db->where('posid',$posid);
			$query=$this->db->get('sch_emp_position');
			return $query->row_array();
		}

		function view_pos($posid){
			$query = $this->db->query("SELECT 
										sch_emp_position.posid,
										sch_emp_position.position,
										sch_emp_position.position_kh,
										sch_emp_position.description
										FROM sch_emp_position WHERE is_active=1
									 ");
			return 	$query->row_array();
		}

		function getmatch(){
			$query=$this->db->get('sch_z_postion_group');
			return $query->result();
		}

		function searchpos($sort_num, $position, $position_kh, $description,$sort,$sort_ad,$m,$p){

			$page=0;
			if(isset($_GET['per_page'])) $page=$_GET['per_page'];
			$this->load->library('pagination');	
			$config['base_url'] = site_url()."/employee/position/search_pos?sort_num=$sort_num&position=$position&position_kh=$position_kh&description=$description&sort=$sort&sort_ad=$sort_ad&m=$m&p=$p";
			
			$where = "";
			if($position !=""){
				$where .="AND sch_emp_position.position LIKE '%$position%'";
			}
			if($position_kh !=""){
				$where .="AND sch_emp_position.position_kh LIKE '%$position_kh%'";
			}
			if($description !=""){
				$where .="AND sch_emp_position.description LIKE '%$description%'";
			}
			
			if($sort_ad!=''){
				$sql = "SELECT 
							sch_emp_position.posid,
							sch_emp_position.position,
							sch_emp_position.position_kh,
							sch_emp_position.description
						FROM sch_emp_position 
						WHERE is_active=1 {$sort_ad}";
			}else{
				$sql = "SELECT 
							sch_emp_position.posid,
							sch_emp_position.position,
							sch_emp_position.position_kh,
							sch_emp_position.description
						FROM sch_emp_position 
						WHERE is_active=1 {$where}";								
			}

			$config['total_rows'] = $this->green->getTotalRow($sql);
		    $config['per_page'] =$sort_num;
		   	$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
		    $this->pagination->initialize($config); 
		    $limi=" limit ".$config['per_page'];
		    if($page>0){
		     $limi=" limit ".$page.",".$config['per_page'];
		    }  
		    $sql.=" $sort {$limi}";
			$query=$this->green->getTable($sql);
			return $query;	
		}

	}
?>