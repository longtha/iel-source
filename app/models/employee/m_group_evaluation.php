<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_group_evaluation extends CI_Model{

	//  save =============================================================
		function save(){ 
			$idhidden        = $this->input->post('idhidden')- 0;
			$decritption_eva = $this->input->post('decritption_eva');
			$area_eva        = $this->input->post('area_eva');
			$area_kh         = $this->input->post('area_kh');
			$data = array('group_eval_id'  => $idhidden,
						'main_eval_id'     => $decritption_eva, 
						'area_eval'        => $area_eva,
						'area_eval_kh'     => $area_kh 
						);
			if($idhidden > 0){
				$this->db->update('sch_evagroup',$data,array('group_eval_id' => $idhidden));
			}else{
				$this->db->insert('sch_evagroup',$data);
			}
			die();
		}
		//  delete ========================================================
		function deletedata(){
			$idhidden = $this->input->post('deletedata');
			$this->db->delete('sch_evagroup',array('group_eval_id'=>$idhidden));
			die();
		}
		//  showdata ======================================================
		function showdata(){
			$sech_evaluation_group = $this->input->post('sech_evaluation_group');
			$search = "";
			if($sech_evaluation_group != ""){ 
    			$search .= "AND  sch_evamain.description  LIKE '".$sech_evaluation_group."%' ";
    		} 
			$sql = "SELECT
						sch_evagroup.group_eval_id,
						sch_evagroup.main_eval_id,
						sch_evagroup.area_eval,
						sch_evagroup.area_eval_kh,
						sch_evamain.description
					FROM
						sch_evamain
					INNER JOIN sch_evagroup ON sch_evagroup.main_eval_id = sch_evamain.main_val_id
					WHERE 1=1 {$search}";
				return $this->db->query($sql);
			die();
		}
		//edit ===========================================================
		function editdata(){

			$idhidden = $this->input->post('idhidden');
			$sql = $this->db->query("SELECT
									sch_evagroup.group_eval_id,
									sch_evagroup.main_eval_id,
									sch_evagroup.area_eval,
									sch_evagroup.area_eval_kh
								FROM sch_evagroup WHERE group_eval_id = '".$idhidden."'")->row();
			
			return  $sql;
		}

		//get_description ===========================================================
		function get_description($id=''){
			$where="";
			if($id!=''){
				$where.=" AND e.main_val_id = '".$id."'";
			}
			$qr = $this->db->query("SELECT
											e.main_val_id,
											e.description
										FROM
											sch_evamain AS e
										WHERE 1=1 {$where}
										ORDER BY
											e.description ASC ");
			
			return  $qr;
		}

	}
?>
					

                       