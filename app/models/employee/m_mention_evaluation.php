<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_mention_evaluation extends CI_Model{
		//  save =============================================================
		function save(){ 
			$idhidden       = $this->input->post('idhidden')- 0;
			$score_eva      = $this->input->post('score_eva');
			$mantion_eva    = $this->input->post('mantion_eva');
			$mantion_eva_kh = $this->input->post('mantion_eva_kh');
			$minrate        = $this->input->post('minrate');
			$maxrate        = $this->input->post('maxrate');
			$data = array('mention_id'  => $idhidden,
						'score'         => $score_eva, 
						'mention'       => $mantion_eva,
						'mention_kh'    => $mantion_eva_kh,
						'min_rate'      => $minrate,
						'max_rate'      => $maxrate
						);
			if($idhidden > 0){
			   $this->db->update('sch_evamention',$data,array('mention_id' => $idhidden));
			}else{
			   $this->db->insert('sch_evamention',$data);
			}
			die();
		}
		//  showdata ==========================================
		function showdata(){
			return $this->db->query("SELECT
										sch_evamention.mention_id,
										sch_evamention.score,
										sch_evamention.mention,
										sch_evamention.mention_kh,
										sch_evamention.min_rate,
										sch_evamention.max_rate
									FROM
										 sch_evamention ");
					die();
		}
		//  delete ===============================================
		function deletedata(){
			$idhidden = $this->input->post('deletedata');
			$this->db->delete('sch_evamention',array('mention_id'=>$idhidden));
			die();
		}
		//  edit =================================================
		function editdata(){
			$idhidden = $this->input->post('idhidden');
			$sql = $this->db->query("SELECT
										sch_evamention.mention_id,
										sch_evamention.score,
										sch_evamention.mention,
										sch_evamention.mention_kh,
										sch_evamention.min_rate,
										sch_evamention.max_rate
								FROM sch_evamention WHERE mention_id = '".$idhidden."'")->row();
			return  $sql;
		}
	}
?>
