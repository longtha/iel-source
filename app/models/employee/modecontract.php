<?php
	class Modecontract extends CI_Model{

		//------Get Max ID of Employee---------
		function getmaxid(){
			$this->db->select_max('contractid','max');
			$this->db->from('sch_emp_contract');
			$query=$this->db->get();
			return $query->row()->max+1;
		}

		function getcontr_row($con_id){
			$this->db->where('con_id',$con_id);
			$query=$this->db->get('sch_emp_contract');
			return $query->row_array();
		}
		function getempcode($empid){
			return $this->green->getValue("SELECT empcode FROM sch_emp_profile WHERE empid = '{$empid}' ");
		}
		// function validate($contractid,$con_id=''){
		// 	$this->db->select('count(*)')
		// 			->from('sch_emp_contract')
		// 			->where('contractid',$contractid);
		// 	if($con_id!='')
		// 		$this->db->where_not_in('con_id',$con_id);
		// 	return $this->db->count_all_results();
		// }
		function searchcontr($sort_num, $contr_id, $empid, $contract_type,$job_type,$sort,$sort_ad,$m='',$p='',$yearid){
			$page=0;
			if(isset($_GET['per_page'])) $page=$_GET['per_page'];
			$this->load->library('pagination');	
			$config['base_url'] = site_url()."/employee/contract/search_contr?sort_num=$sort_num&contr_id=$contr_id&empid=$empid&contract_type=$contract_type&job_type=$job_type&sort=$sort&sort_ad=$sort_ad&m=$m&p=$p&y=$yearid";
			
			$where = "";

			if($contr_id !=""){
				$where .=" AND sch_emp_contract.contractid LIKE '%$contr_id%'";
			}

			if($yearid!=""){
				$where .=" AND sch_emp_contract.year='".$yearid."'";
			}

			if($empid !=""){
				$strempid=$this->green->getEmpID($empid);
				if($strempid!=""){
					$where .=" AND sch_emp_contract.empid IN(".trim($strempid).")";
				}
				
			}

			if($contract_type !=""){
				$where .=" AND sch_emp_contract.contract_type = '".$contract_type."'";
			}
			
			if($job_type !=""){
				$where .=" AND sch_emp_contract.job_type = '".$job_type."'";
			}
			if($sort_ad!=''){
				$sql = "SELECT distinct contractid,begin_date,end_date,contract_type,job_type,empid,decription,con_id,empid FROM sch_emp_contract WHERE 1=1 group by contractid {$sort_ad}";
			}else{
				$sql = "SELECT distinct contractid,begin_date,end_date,contract_type,job_type,empid,decription,con_id,empid FROM sch_emp_contract WHERE 1=1  {$where} group by contractid";
			}
			$config['total_rows'] = $this->green->getTotalRow($sql);
		    $config['per_page'] =$sort_num;
		   	$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
		    $this->pagination->initialize($config); 
		    $limi=" limit ".$config['per_page'];
		    if($page>0){
		     $limi=" limit ".$page.",".$config['per_page'];
		    }  
		    if($sort_ad!=''){
				$sql="SELECT distinct contractid,begin_date,end_date,contract_type,job_type,empid,decription,con_id,empid FROM sch_emp_contract $sort_ad WHERE 1=1 group by contractid {$limi}";
				$query=$this->green->getTable($sql);
			}else{
				$sql="SELECT distinct contractid,begin_date,end_date,contract_type,job_type,empid,decription,con_id,empid FROM sch_emp_contract WHERE 1=1 {$where} group by contractid";
				 $sql.=" $sort {$limi}";
				$query=$this->green->getTable($sql);
			}
			return $query;
		}
	}
?>