<?php
    class Moddisplan extends CI_Model {
		function saveImport(){
			$is_imported=false;
			if (isset($_FILES['file']) && $_FILES['file']["tmp_name"][0]!="") {
				$_xfile = $_FILES['file']["tmp_name"];	
				function str($str){
					return str_replace("'","''",$str);
				}
				if($_FILES['file']['tmp_name']==""){
					$error="<font color='red'>Please choose your excel file!</font>";					
				}
				else{
					$html="";					
					require_once(APPPATH.'libraries/simplexlsx.php' );								
					foreach($_xfile as $index => $value){					
						$xlsx = new SimpleXLSX($value);				
						$_data = $xlsx->rows();					
						array_splice($_data,0,1);
						$error_record_exist="";
						foreach( $_data as $k => $r) {					
							$_check_exist = $this->green->getTable("SELECT * FROM sch_family_distribute_plan 
																			WHERE familyid = '{$r[0]}' 
																			AND year='{$r[5]}'
																			AND distrib_type='{$r[7]}'
																			");					
							$seachspance = strpos(trim(str($r[0]))," ");
							if (count($_check_exist)> 0)
							{
								$error_record_exist .='<div class="success" style=" padding:5px; margin-top:5px;">
														<font style="color:red;">Code already exist'.$r[0].' exist aleady !</font><br>
														</div>';					
							}else if($seachspance !== false){
								$error_record_exist .='<div class="success" style=" padding:5px; margin-top:5px;">
													<font style="color:red;">Error Code'.str($r[0]).' is incorrect. Please try again !</font></div>';
							}
							else {								
								
								$familyid=trim(str($r[0]));								
								$amt_rice=trim(str($r[1]));
								$rice_uom=trim(str($r[2]));
								$oil=trim(str($r[3]));
								$oil_uom=trim(str($r[4]));
								$remark=trim(str($r[6]));
								$distrib_type=trim(str($r[7]));
								$year=trim(str($r[5]));	

								$SQL_DM="INSERT INTO sch_family_distribute_plan";
																
								$SQL=" SET familyid='".$familyid."',
											amt_rice='".$amt_rice."',
											rice_uom='".$rice_uom."',
											oil='".$oil."',
											oil_uom='".$oil_uom."',
											remark='".$remark."',
											distrib_type='".$distrib_type."',
											`year`='".$year."'
										";
								$counts=$this->green->runSQL($SQL_DM.$SQL);
								$is_imported=true;		
								
							}					
						}
					}				
				}
			}
			return $is_imported;
		}		
		function getallrow($year){
			return	$this->db->where('year',$year)
				->get('sch_family_distribute_plan')
				->result();
		}
		function save($displanid=''){	

			$familyid=$this->input->post('familyid');
			$amt_rice=$this->input->post('amt_rice');
			$rice_uom="Kg";
			$oil=$this->input->post('oil');
			$oil_uom='L';
			$remark=$this->input->post('remark');
			$is_newadd=$this->input->post('is_newadd');
			$distrib_type=$this->input->post('distrib_type');
			$year=$this->input->post('year');


			$created_by=$this->session->userdata('user_name');
			$created_date=date('Y-m-d H:i:s');
			$SQL_WHERE=" ,created_date='".$created_date."', created_by='".$created_by."'";
			$SQL_DM="";
			if($displanid!=""){
				$SQL_DM="UPDATE sch_stock ";				
				$last_modified_by=$this->session->userdata('user_name');
				$last_modified_date=date('Y-m-d H:i:s');
				$SQL_WHERE =" ,modified_by='".$last_modified_by."',modified_date='".$last_modified_date."'  WHERE displanid = '".$displanid."'"; 

			}else{
				$SQL_DM="INSERT INTO sch_family_distribute_plan";				
			}
			
			$SQL=" SET familyid='".$familyid."',
						amt_rice='".$amt_rice."',
						rice_uom='".$rice_uom."',
						oil='".$oil."',
						oil_uom='".$oil_uom."',
						remark='".$remark."',
						distrib_type='".$distrib_type."',
						`year`='".$year."'			
					";
			
			if($this->validatePlan($familyid,$distrib_type,$year,$displanid)){
				$this->green->runSQL($SQL_DM.$SQL.$SQL_WHERE);
				if($displanid==""){
					$respond['result_save']="Distribute was saved success.";	
				}else{
					$respond['result_save']="Distribute was updated success.";
				}
							
			}else{
				$displanid="";
				$respond['result_save']="Distribute not save.";
			}			
					
			$respond['displanid']=$displanid;
			return $respond;
		}
		
		function validDisPlan($familyid,$distrib_type,$year,$displanid=""){
			
			$this->db->select('count(*)');
			$this->db->from('sch_family_distribute_plan');			
			$this->db->where('familyid',$familyid);
			$this->db->where('distrib_type',$distrib_type);
			$this->db->where('year',$year);

			if($displanid!=""){
				$this->db->where_not_in('displanid', $displanid);
			}

			$data=$this->db->count_all_results();
			if($data>0){
				return false;
			}else{
				return true;
			}
		}
		function searchdisplan($family_code,$father_name,$mother_name,$year,$distrib_type,$sort,$sort_num,$m,$p,$yearid){
				$page=0;
				$sql='';
				if(isset($_GET['per_page'])) $page=$_GET['per_page'];
				$this->load->library('pagination');	
				$config['base_url'] = site_url()."/social/distribplan/search?fid=$family_code&fn=$father_name&mn=$mother_name&dt=$distrib_type&y=$year&s_num=$sort_num&m=$m&p=$p";
				
				$where="";
				if($family_code!=""){
					$where=" AND family_code like '%".$family_code."%'";
				}
				if($father_name!=""){
					$where.=" AND (father_name like '%".$father_name."%' OR father_name_kh  like '%".$father_name."%')";
				}
				if($mother_name!=""){
					$where.=" AND (mother_name like '%".$mother_name."%' OR mother_name_kh  like '%".$mother_name."%')";
				}
				if($year!=""){
					$where.=" AND year = '".$year."'";
				}
				if($distrib_type!=""){
					$where.=" AND distrib_type = '".$distrib_type."'";
				}
				if($yearid!=''){
					$years=$this->db->query("SELECT year(to_date) AS years FROM sch_school_year WHERE yearid='$yearid'")->row()->years;
					$where.=" AND year='".$years."'";
				}
				$sql.="SELECT
						dsplan.displanid,
						dsplan.familyid,
						dsplan.family_code,
						dsplan.father_name,
						dsplan.father_name_kh,
						dsplan.mother_name,
						dsplan.mother_name_kh,
						dsplan.amt_rice,
						dsplan.rice_uom,
						dsplan.oil,
						dsplan.oil_uom,
						dsplan.`year`,
						dsplan.distrib_type,
						dsplan.modified_by,
						dsplan.modified_date,
						dsplan.remark,
						dsplan.created_date,
						dsplan.created_by
					FROM
						v_fam_distrib_plan AS dsplan
					WHERE 1=1 
					{$where}					
					";					
				
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] = $sort_num;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';	
				$this->pagination->initialize($config);	
				$limi=" limit ".$config['per_page'];
				if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}		
				$sql.=" $sort {$limi}";		
					
				$query=$this->green->getTable($sql);
				return $query;
		}
		function getpreview($family_code,$father_name,$mother_name,$year,$distrib_type){
				$page=0;
				$sql='';
				if(isset($_GET['per_page'])) $page=$_GET['per_page'];
				$where="";
				if($family_code!=""){
					$where=" AND family_code like '%".$family_code."%'";
				}
				if($father_name!=""){
					$where.=" AND (father_name like '%".$father_name."%' OR father_name_kh  like '%".$father_name."%')";
				}
				if($mother_name!=""){
					$where.=" AND (mother_name like '%".$mother_name."%' OR mother_name_kh  like '%".$mother_name."%')";
				}
				if($year!=""){
					$where.=" AND year = '".$year."'";
				}
				if($distrib_type!=""){
					$where.=" AND distrib_type = '".$distrib_type."'";
				}
				$sql.="SELECT
						dsplan.displanid,
						dsplan.familyid,
						dsplan.family_code,
						dsplan.father_name,
						dsplan.father_name_kh,
						dsplan.mother_name,
						dsplan.mother_name_kh,
						dsplan.amt_rice,
						dsplan.rice_uom,
						dsplan.oil,
						dsplan.oil_uom,
						dsplan.`year`,
						dsplan.distrib_type,
						dsplan.modified_by,
						dsplan.modified_date,
						dsplan.remark,
						dsplan.created_date,
						dsplan.created_by
					FROM
						v_fam_distrib_plan AS dsplan
					WHERE 1=1 
					{$where}";					
				
				
				$query=$this->green->getTable($sql);
				return $query;
		}
		
		function getDisRow($displanid){
			$this->db->where('displanid',$displanid);
			$query=$this->db->get('sch_family_distribute_plan');
			return $query->row_array();
		}		
		// function getPreview($displanid){
		// 	$this->db->where('displanid',$displanid);
		// 	$query=$this->db->get('sch_family_distribute_plan');
		// 	return $query->row_array();
		// }
		function getDistribType(){
			$arr=array(""=>"All","12"=>"Every Month","4"=>"4 times per year","2"=>"2 times per year");
			return $arr;
		}
		function getdatedis(){
			$year=Date('Y')+5;
			$arr=array();
			for($i=1;$i<=10;$i++){
				$arr[$year-$i]=$year-$i;
			}
			return $arr;
		}

    }