<?php if(!defined('BASEPATH')) exit('No direct script access allowed.!');
	class Modstusponsor  extends CI_Model
	{
		public function setSqlStuSponsor($yearid='',$schlevelid='',$stu_code='',$stu_fullname='')
		{
			$where="";
			if($yearid!=''){
				$where.=" AND vsp.yearid ='".$yearid."'";
			}
			if($schlevelid!=''){
				$where.=" AND vsp.schlevelid='".$schlevelid."'";
			}
			if($stu_code!=""){
				$where.=" AND (vsp.student_num like '%".$stu_code."%')";
			}		
			if($stu_fullname!=""){
				$where.=" AND (vsp.fullname like '%".$stu_fullname."%')";
			}
			$sqlText = "SELECT
							vsp.studentid,
							vsp.student_num,
							vsp.fullname,
							vsp.class_name,
							vsp.sch_year,
							vsp.classid,
							vsp.yearid,
							vsp.familyid,
							vsp.schlevelid,
							vsp.grade_levelid
						FROM
							v_student_profile vsp
						WHERE 1=1 {$where} ORDER BY vsp.studentid DESC
						";
			return $sqlText;
		}
		
		public function GetTotalRow($yearid='',$schlevelid='',$stu_code='',$stu_fullname='')
		{
			return $this->green->getTotalRow($this->setSqlStuSponsor($yearid,$schlevelid,$stu_code,$stu_fullname));
		}
		public function listbody($no,$student_num,$fullname,$level,$class,$year,$studentid='')
		{
			$pre="";
			if(!$this->green->gAction("P"))
				 $pre.="<a attr='".$studentid."' class='StuSponsor' href='JavaScript:void(0);'><img src='".site_url('../assets/images/icons/a_preview.png')."'/></a>"; 
			$tr = "<tr>
				 	 <td class='no' style='vertical-align:middle'>".$no."</td>
					 <td class='studentid' style='vertical-align:middle'>".$student_num."</td>
					 <td class='fullname' style='vertical-align:middle'>".$fullname."</td>
					 <td class='level' style='vertical-align:middle'>".$level."</td>
					 <td class='class' style='vertical-align:middle'>".$class."</td>
					 <td class='year' width='50' style='vertical-align:middle'>".$year."</td>
					 <td class='action remove_tag' align='center' style='vertical-align:middle'>".$pre."</td>
				</tr>";
			return $tr;
		}
		public function setSqlStu($studentid)
		{
			$sqlStr = "SELECT
							vsp.student_num,
							vsp.fullname,
							vsp.fullname_kh,
							vsp.familyid,
							vsp.class_name,
							vsp.dateofbirth,
							vsp.nationality,
							vsp.permanent_adr
						FROM
							v_student_profile AS vsp
						WHERE vsp.studentid = '".$studentid."'
					";
			return $this->green->getOneRow($sqlStr);			
		}
		public function setSqlSponsor($studentid){
			$sqlStr = "SELECT
						sfc.sponsorid,
						sfc.sponsor_code,
						sfc.last_name,
						sfc.first_name,
						sfc.position,
						sfc.phone,
						sfc.mail,
						sfc.title
					FROM
						sch_family_sponsor sfc
					INNER JOIN sch_student_sponsor_detail sssd ON sssd.sponsorid = sfc.sponsorid
					WHERE
						sssd.studentid = '".$studentid."'
			";
			return $this->db->query($sqlStr)->result();
		}
		public function schinfo(){
			return $this->green->getValue("SELECT ssi.name FROM sch_school_infor ssi WHERE ssi.schoolid='1'");
		}
		public function listsponinfo($i,$sponsor_code,$fullname,$position,$phone)
		{
			$tr ="<tr>
				<td>".$i."</td>
				<td>".$sponsor_code."</td>
				<td>".$fullname."</td>
				<td>".$position."</td>
				<td>".$phone."</td>
			</tr>";
			return $tr;
		}
	}