<?php
    class ModSocialInfor extends CI_Model {       
				

		function save($famnoteid=''){			
			$description=$this->input->post('description');			
			$familynote_type=$this->input->post('familynote_type');
			$created_by=$this->session->userdata('user_name');
			$created_date=date('Y-m-d H:i:s');
			$SQL_WHERE=" ,created_date='".$created_date."', created_by='".$created_by."'";
			$SQL_DM="";
			if($famnoteid!=""){
				$SQL_DM="UPDATE sch_family_social_infor ";				
				$last_modified_by=$this->session->userdata('user_name');
				$last_modified_date=date('Y-m-d H:i:s');
				$SQL_WHERE =" ,modified_by='".$last_modified_by."',modified_date='".$last_modified_date."'  WHERE famnoteid = '".$famnoteid."'"; 

			}else{
				$SQL_DM="INSERT INTO sch_family_social_infor";				
			}
			$orders=$this->green->getValue("SELECT MAX(orders)+1 FROM sch_family_social_infor WHERE familynote_type='".$familynote_type."'");
			$SQL=" SET  description='".$this->db->escape_str($description)."',
						familynote_type='".$familynote_type."',
						orders='".$orders."'
					";
			
			if($this->validStock($description,$familynote_type)){
				$this->green->runSQL($SQL_DM.$SQL.$SQL_WHERE);
				if($famnoteid==""){
					$respond['result_save']="Record was saved.";	
				}else{
					$respond['result_save']="Record was updated success.";
				}
							
			}else{
				$famnoteid="";
				$respond['result_save']="Record not save.";
			}			
					
			$respond['famnoteid']=$famnoteid;
			return $respond;
		}
		
		function validStock($description,$familynote_type){
			$data=$this->green->getValue("SELECT COUNT(*) FROM sch_family_social_infor WHERE familynote_type='".$familynote_type."' AND description='".$this->db->escape_str($description)."'");
			if($data>0){
				return false;
			}else{
				return true;
			}
		}
		
		function searchsocinf($famnoteid,$description,$familynote_type,$sort,$sort_num,$m,$p){
			$page=0;
			$sql='';
			if(isset($_GET['per_page'])) $page=$_GET['per_page'];
			$this->load->library('pagination');	
			$config['base_url'] = site_url()."/social/socialinfor/searchsocinf?s_id=$famnoteid&des=$description&fat=$familynote_type&s_num=$sort_num&m=$m&p=$p";
			
			$where="";
			if($famnoteid!=""){
				$where=" AND famnoteid like '%".$famnoteid."%'";
			}
			if($description!=""){
				$where.=" AND description like '%".$description."%'";
			}
			if($familynote_type!=""){
				$where.=" AND familynote_type like '%".$familynote_type."%'";
			}
			

			$sql.="SELECT
					socinfo.famnoteid,
					socinfo.description,
					socinfo.familynote_type,
					socinfo.orders
				FROM
					sch_family_social_infor AS socinfo						
				WHERE 1=1 {$where} ";
				
			$config['total_rows'] = $this->green->getTotalRow($sql);
			$config['per_page'] = $sort_num;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';	
			$this->pagination->initialize($config);	
			$limi=" limit ".$config['per_page'];
			if($page>0){
				$limi=" limit ".$page.",".$config['per_page'];
			}		
			$sql.=" $sort {$limi}";				
			$query=$this->green->getTable($sql);					
			return $query;
		}		

    }