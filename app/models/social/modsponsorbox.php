<?php
	class modsponsorbox extends CI_Model{
		function getsponsorbox(){
			$this->load->library('pagination');	
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m='';
			$p='';
			if(isset($_GET['m'])){
		        $m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
		    $yearid=$this->session->userdata('year');
	    	$sql="SELECT 
	    			sp.studentid,
	    			sp.boxid,
					CONCAT(sd.last_name,' ',sd.first_name) AS stu_fullname,
					sy.sch_year,
					c.class_name,
					CONCAT(fs.last_name,' ',fs.first_name) AS sp_fullname,
					sp.spr_ques_eng,
					sp.spr_ques_fr,
					sp.num_box,
					sp.visite_date,
					sp.reception_date,
					sp.sending_box_date,
					sp.date_answer_toques,
					sp.date_thanks,
					sp.date_sendthanks_paris 
					FROM sch_sponsor_box AS sp
					INNER JOIN sch_student AS sd ON sp.studentid=sd.studentid
					INNER JOIN sch_school_year AS sy ON sp.yearid=sy.yearid
					INNER JOIN sch_class AS c ON sp.classid=c.classid
					INNER JOIN sch_family_sponsor AS fs ON sp.sponsorid=fs.sponsorid
					WHERE sp.yearid='".$yearid."'
					ORDER BY boxid DESC";

			$config['base_url'] =site_url()."/social/sponsorbox?pg=1&m=$m&p=$p";	
			$config['total_rows'] = $this->green->getTotalRow($sql);
			$config['per_page'] =50;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);
			$limi=" limit ".$config['per_page'];
			if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}		
			$sql.=" {$limi}";
			return $this->db->query($sql)->result();
		}
		function getsponsorboxs(){
			$this->load->library('pagination');	
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m='';
			$p='';
			if(isset($_GET['m'])){
		        $m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
		    $yearid=$this->session->userdata('year');
	    	$sql="SELECT 
	    			sp.boxid,
					CONCAT(sd.last_name,' ',sd.first_name) AS stu_fullname,
					c.class_name,
					CONCAT(fs.last_name,' ',fs.first_name) AS sp_fullname,
					sp.spr_ques_eng,
					sp.spr_ques_fr,
					spd.des_eng,
					spd.des_fr,
					sp.gift,
					sp.lettertype
					FROM sch_sponsor_box AS sp
				  	INNER JOIN sch_sponsor_boxdetail AS spd ON sp.boxid=spd.boxid
					INNER JOIN sch_student AS sd ON sp.studentid=sd.studentid
					INNER JOIN sch_school_year AS sy ON sp.yearid=sy.yearid
					INNER JOIN sch_class AS c ON sp.classid=c.classid
					INNER JOIN sch_family_sponsor AS fs ON sp.sponsorid=fs.sponsorid
					WHERE sp.yearid='".$yearid."'
					ORDER BY boxid DESC";

			$config['base_url'] =site_url()."/social/sponsorboxs?pg=1&m=$m&p=$p";	
			$config['total_rows'] = $this->green->getTotalRow($sql);
			$config['per_page'] =50;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);
			$limi=" limit ".$config['per_page'];
			if($page>0){
				$limi=" limit ".$page.",".$config['per_page'];
			}		
			$sql.=" {$limi}";
			return $this->db->query($sql)->result();
		}
		//-------- Note use anymore -----------
		function getsponsor_old($studentid){
	    	$sql="SELECT 
	    			sb.reception_date,
					CONCAT(fs.last_name,' ',fs.first_name) AS sp_fullname,
					sb.spr_ques_eng,
					sb.spr_ques_fr,
					sbd.des_fr,
					sbd.des_eng,
				  	sb.visite_date
					FROM sch_sponsor_box sb
					INNER JOIN sch_family_sponsor fs ON sb.sponsorid=fs.sponsorid
					INNER JOIN sch_sponsor_boxdetail sbd ON sb.boxid=sbd.boxid
					WHERE sb.studentid='".$studentid."'";
			return $this->db->query($sql)->result();
		}
		//---------- end ------------------------
		function getsponsor($boxid){
	    	$sql="SELECT 
	    			sb.reception_date,
					CONCAT(fs.last_name,' ',fs.first_name) AS sp_fullname,
					sb.spr_ques_eng,
					sb.spr_ques_fr,
					sbd.des_fr,
					sbd.des_eng,
				  	sb.visite_date,
				  	sb.gift,
				  	sb.lettertype
					FROM sch_sponsor_box sb
					INNER JOIN sch_family_sponsor fs ON sb.sponsorid=fs.sponsorid
					INNER JOIN sch_sponsor_boxdetail sbd ON sb.boxid=sbd.boxid
					WHERE sb.boxid='".$boxid."'";
			return $this->db->query($sql)->row();
		}
		function getStudentName($studentid){
			return $this->db->query("SELECT 
										CONCAT(last_name,' ',first_name) AS stu_fullname 
										FROM sch_student 
										WHERE studentid='".$studentid."'")->row();
		}
		function getClassName($studentid){
			return $this->db->query("SELECT 
										MAX(c.class_name) AS classname FROM sch_student_enrollment se
										INNER JOIN sch_class c ON se.classid=c.classid
										WHERE se.studentid='".$studentid."'")->row();
		}
		function getSponsorName($sponsorid){
			return $this->db->query("SELECT CONCAT(fs.last_name,' ',fs.first_name) AS sp_fullname FROM sch_sponsor_box sb 
									INNER JOIN sch_family_sponsor fs ON fs.sponsorid=sb.sponsorid
									WHERE sb.sponsorid='".$sponsorid."'")->row();
		}
		function do_upload($id){
			$config['upload_path'] ='./assets/upload/sponsorbox/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name']  = "$id.jpg";
			$config['overwrite']=true;
			$config['file_type']='image/jpg';
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('userfile'))
			{
				$error = array('error' => $this->upload->display_errors());			
			}
			else
			{				
				//$data = array('upload_data' => $this->upload->data());			
				$config2['image_library'] = 'gd2';
	            $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
	            $config2['new_image'] = './assets/upload/sponsorbox/';
	            $config2['maintain_ratio'] = true;
	            $config2['create_thumb'] = true;
	            $config2['thumb_marker'] = false;
	            $config2['width'] = 120;
	            $config2['height'] = 150;
	            $config2['overwrite']=true;
	            $this->load->library('image_lib',$config2);

	            if ( !$this->image_lib->resize()){
	            	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
				}//else{
					//unlink('./assets/upload/sponsorbox/'.$id.'.jpg');
				// $m='';
				// $p='';
				// if(isset($_GET['m'])){
			    //    $m=$_GET['m'];
			    // }
			    // if(isset($_GET['p'])){
			    //    $p=$_GET['p'];
			    // }
				// redirect("social/sponsorbox/?m=$m&p=$p");
				//}	
				}
		}
		function save($boxid=''){
			//------- Student Info -------
			$sponsorid=$this->input->post('s_sponsorid');
			$studentid=$this->input->post('s_studentid');
			$yearid=$this->input->post('s_yearid');
			//$schlevelid=$this->input->post('s_schlevel');
			$classid=$this->input->post('hiddenclassid');
			$receptiondate=$this->input->post('receptiondate');
			
			//------- Gift -------
			//$numbergift=$this->input->post('numgift');
			$gift=$this->input->post('gift');
			$sendingboxdate=$this->input->post('sendingboxdate');
			$detailfr=$this->input->post('detailfr');
			$detailen=$this->input->post('detailen');

			//------- Response and thanks -------
			$answerdate=$this->input->post('dateanswertoques');
			$thankdate=$this->input->post('datethanks');

			//--------- Paris of dispatch ---------
			$replydate=$this->input->post('datesendthanksparis');

			//-------- Letter --------
			$letter_type=$this->input->post('letter_type');
			$other=$this->input->post('other');
			$is_visit=$this->input->post('isvisit');
			$language=$this->input->post('laguage');
			//$detail=$this->input->post('detail');
			$sprquesfr=$this->input->post('sp_fr');
			$sprquesen=$this->input->post('sp_en');
			$visitdate=$this->input->post('vdate');
			$note=$this->input->post('note');

			$data=array(
						'sponsorid'			   => $sponsorid,
						'studentid'			   => $studentid,
						'yearid'			   => $yearid,
						'classid'			   => $classid,
						//'schlevelid'		   => $schlevelid,
						
						//'num_gift'		   => $numbergift,
						'gift'				   => $gift,
						'reception_date'	   => $this->green->formatSQLDate($receptiondate),
						'sending_box_date'     => $this->green->formatSQLDate($sendingboxdate),

						//'date_answer_toques'   => $this->green->formatSQLDate($answerdate),
						//'date_thanks'		   => $this->green->formatSQLDate($thankdate),

						'date_sendthanks_paris'=> $this->green->formatSQLDate($replydate),

						'is_visit'			   => $is_visit,
						'lettertype'		   => $letter_type,
						'letter_other'		   => $other,
						'langue'			   => $language,
						//'detail'			   => $detail,
						'spr_ques_fr'		   => $sprquesfr,
						'spr_ques_eng'		   => $sprquesen,
						'visite_date'		   => $this->green->formatSQLDate($visitdate),
						//'num_box'			   => $numbox,
						'note'				   => $note,
						);

			if($boxid!=''){
				//-------- Update sponsor box -------------
				$this->db->where('boxid',$boxid);
				$this->db->update('sch_sponsor_box',$data);

				//-------- Delete first sponsor box detail ----------
				$this->DeleteSponsorDetail($boxid);
				
				$this->SaveSponsorDetail($boxid,$detailfr,$detailen);
				$this->do_upload($boxid);
				// //-------- New insert row sponsor box detail --------
				// $descen_de=$this->input->post('descen_de');
				// $descfr_de=$this->input->post('descfr_de');

				// for($j=0;$j<count($descen_de);$j++){
				// 	if ($descen_de!='')
				// 		$this->SaveSponsorDetail($boxid,$descfr_de[$j],$descen_de[$j]);
				// }
			}else{
				//------- Add new row sponsor box --------
				$this->db->insert('sch_sponsor_box',$data);
				$last_boxid=$this->db->insert_id();

				$this->SaveSponsorDetail($last_boxid,$detailfr,$detailen);
				$this->do_upload($last_boxid);
				// //------- Add new row sponsor box detail ----------
				// $descen_de=$this->input->post('descen_de');
				// $descfr_de=$this->input->post('descfr_de');

				// for($i=0;$i<count($descen_de);$i++){
				// 	if ($descen_de!='')
				// 		$this->SaveSponsorDetail($last_boxid,$descfr_de[$i],$descen_de[$i]);
				// }
			}	
		}
		function SaveSponsorDetail($boxid,$des_fr,$des_en){
			$data_detail=array(
							'boxid'  => $boxid,
							'transno'=> $this->green->nextTran(15,"Sponsor box"),
							'type'   => '15',
							'des_fr' => $des_fr,
							'des_eng'=> $des_en
						);
			$this->db->insert('sch_sponsor_boxdetail',$data_detail);
		}

		function DeleteSponsorDetail($boxid){
			$this->db->where('boxid',$boxid)->delete('sch_sponsor_boxdetail');
		}
		function sponsorboxrow($boxid){
			return $this->db->query("SELECT 
						    			sp.boxid,
						    			sp.sponsorid,
						    			sd.studentid,
										CONCAT(sd.last_name,' ',sd.first_name)AS stu_fullname,
										sy.sch_year,
										CONCAT(fs.last_name,' ',fs.first_name) AS sp_fullname,
										sp.spr_ques_eng,
										sp.spr_ques_fr,
										sp.num_box,
										sp.visite_date,
										sp.reception_date,
										sp.sending_box_date,
										sp.date_answer_toques,
										sp.date_thanks,
										sp.date_sendthanks_paris,
										sp.detail,
										sp.num_gift,
										sp.yearid,
										sp.classid,
										sp.is_visit,
										sp.letter_other,
										c.class_name,
										sp.lettertype,
										sp.langue,
										sp.gift,
										sp.note
										FROM sch_sponsor_box AS sp
										INNER JOIN sch_student AS sd ON sp.studentid=sd.studentid
										INNER JOIN sch_school_year AS sy ON sp.yearid=sy.yearid
										INNER JOIN sch_class AS c ON sp.classid=c.classid
										INNER JOIN sch_family_sponsor AS fs ON sp.sponsorid=fs.sponsorid
										WHERE sp.boxid='".$boxid."'")->row();
		}

		function sponsorboxdetailrow($boxid){
			return $this->db->query("SELECT * FROM sch_sponsor_boxdetail WHERE boxid='".$boxid."'")->row();
		}
		function search($s_name,$classid,$sponsorid,$sort_num,$m,$p,$sort='',$year,$sdate,$edate){
			$this->load->library('pagination');
			$page=0;
			$where='';
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			if($s_name!='')
				$where.=" AND CONCAT(sd.last_name,' ',sd.first_name) LIKE '%".$s_name."%'";
			if($classid!='')
				$where.=" AND c.classid='".$classid."'";
			if($sponsorid!='')
				$where.=" AND CONCAT(fs.last_name,' ',fs.first_name) LIKE '%".$sponsorid."%'";
			if($year!='')
				$where.=" AND sp.yearid='".$year."'";
			if($sdate!="" && $edate!="")
				$where.=" AND sp.reception_date BETWEEN '".$sdate."' AND '".$edate."'";
		
			$sql="SELECT 
					sp.studentid,
	    			sp.boxid,
					CONCAT(sd.last_name,' ',sd.first_name) AS stu_fullname,
					sy.sch_year,
					c.class_name,
					CONCAT(fs.last_name,' ',fs.first_name) AS sp_fullname,
					sp.spr_ques_eng,
					sp.spr_ques_fr,
					sp.num_box,
					sp.visite_date,
					sp.reception_date,
					sp.sending_box_date,
					sp.date_sendthanks_paris 
					FROM sch_sponsor_box AS sp
					INNER JOIN sch_student AS sd ON sp.studentid=sd.studentid
					INNER JOIN sch_school_year AS sy ON sp.yearid=sy.yearid
					INNER JOIN sch_class AS c ON sp.classid=c.classid
					INNER JOIN sch_family_sponsor AS fs ON sp.sponsorid=fs.sponsorid
				    WHERE 1=1 {$where}";	
			$config['base_url'] =site_url()."/social/sponsorbox/search?pg=1&n=$s_name&s_num=$sort_num&m=$m&p=$p&l=$classid&s=sponsorid&y=$year&sort=$sort&sd=$sdate&ed=$edate";	
			$config['total_rows'] = $this->green->getTotalRow($sql);
			$config['per_page'] =$sort_num;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);

			$limi = '';
	        if ($sort_num != 'all') {
	            $limi = " limit " . $config['per_page'];
	            if ($page > 0) {
	                $limi = " limit " . $page . "," . $config['per_page'];
	            }
	        }
	        $sql .= " $sort {$limi}";
	        
	        return $this->db->query($sql)->result();
		}
		function getexp($s_student_num,$s_classid,$s_sponsor,$sdate,$edate,$sort,$sort_num,$page,$is_all,$year){
				
			$WHERE="";
		    if($s_student_num!=""){
		     $WHERE.=" AND CONCAT(sd.last_name,' ',sd.first_name) like '%$s_student_num%' 
		     			OR CONCAT(sd.first_name,' ',sd.last_name) like '%$s_student_num%";
		    }
		    if($s_classid!=""){
		     $WHERE.=" AND c.classid ='$s_classid'";
		    }
		    if($s_sponsor!=""){
		     $WHERE.=" AND CONCAT(fs.last_name_kh,' ',fs.first_name_kh) like '%$s_sponsor%' 
		     		    OR CONCAT(fs.first_name_kh,' ',fs.last_name_kh) like '%$s_sponsor%";
		    }
		    if($sdate!="" && $edate!=""){
		     $WHERE.=" AND sp.reception_date BETWEEN '".$sdate."' AND '".$edate."'";
		    }
		    if($year!=""){
		     $WHERE.=" AND sp.yearid='".$year."'";
		    }
			$sql="SELECT 
		    			sp.boxid,
						CONCAT(sd.last_name,' ',sd.first_name) AS student,
						c.class_name AS class,
						CONCAT(fs.last_name,' ',fs.first_name) AS sponsor,
						sp.reception_date AS rdate,
						sp.date_sendthanks_paris AS tdate,
						sp.visite_date AS vdate,
						sp.gift AS gift,
						spd.des_eng AS den,
						spd.des_fr AS dfr,
						sp.lettertype AS ltype, 
						sp.langue AS lang,
						sp.spr_ques_eng AS qfr,
						sp.spr_ques_fr AS qen,
						sy.sch_year AS year,
						sp.note AS note
						FROM sch_sponsor_box AS sp
					  	INNER JOIN sch_sponsor_boxdetail AS spd ON sp.boxid=spd.boxid
						INNER JOIN sch_student AS sd ON sp.studentid=sd.studentid
						INNER JOIN sch_school_year AS sy ON sp.yearid=sy.yearid
						INNER JOIN sch_class AS c ON sp.classid=c.classid
						INNER JOIN sch_family_sponsor AS fs ON sp.sponsorid=fs.sponsorid
						WHERE 1=1 {$WHERE}";
			$limi='';
			if($is_all==0){
				$limi=" limit $sort_num";
				if($page!='' && $page>0){
						$limi=" limit ".$page.",".$sort_num;
				}
			}

			$sql.=" $sort {$limi}";
			return $this->db->query($sql)->result_array();
		}
		function searchs($s_name,$classid,$sponsorid,$sort_num,$m,$p,$sort='',$year,$sdate,$edate){
			$this->load->library('pagination');
			$page=0;
			$where='';
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			if($s_name!='')
				$where.=" AND CONCAT(sd.last_name,' ',sd.first_name) LIKE '%".$s_name."%'";
			if($classid!='')
				$where.=" AND c.classid='".$classid."'";
			if($sponsorid!='')
				$where.=" AND CONCAT(fs.last_name,' ',fs.first_name) LIKE '%".$sponsorid."%'";
			if($year!='')
				$where.=" AND sp.yearid='".$year."'";
			if($sdate!='' && $edate!='')
				$where.=" AND sp.reception_date BETWEEN '".$sdate."' AND '".$edate."'";
			$sql="SELECT 
	    			sp.boxid,
					CONCAT(sd.last_name,' ',sd.first_name) AS stu_fullname,
					c.class_name,
					CONCAT(fs.last_name,' ',fs.first_name) AS sp_fullname,
					sp.spr_ques_eng,
					sp.spr_ques_fr,
					spd.des_eng,
					spd.des_fr,
					sp.gift,
					sp.lettertype
					FROM sch_sponsor_box AS sp
				  	INNER JOIN sch_sponsor_boxdetail AS spd ON sp.boxid=spd.boxid
					INNER JOIN sch_student AS sd ON sp.studentid=sd.studentid
					INNER JOIN sch_school_year AS sy ON sp.yearid=sy.yearid
					INNER JOIN sch_class AS c ON sp.classid=c.classid
					INNER JOIN sch_family_sponsor AS fs ON sp.sponsorid=fs.sponsorid
				    WHERE 1=1 {$where} 
				    ORDER BY boxid DESC";	
			$config['base_url'] =site_url()."/social/sponsorboxs/search?pg=1&n=$s_name&s_num=$sort_num&m=$m&p=$p&l=$classid&s=sponsorid&y=$year&sd=$sdate&ed=$edate";	
			$config['total_rows'] = $this->green->getTotalRow($sql);
			$config['per_page'] =$sort_num;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);

			$limi = '';
	        if ($sort_num != 'all') {
	            $limi = " limit " . $config['per_page'];
	            if ($page > 0) {
	                $limi = " limit " . $page . "," . $config['per_page'];
	            }
	        }
	        $sql .= " $sort {$limi}";
	        
	        return $this->db->query($sql)->result();
		}
}