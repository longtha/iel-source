<?php
	class ModCounseling extends CI_Model{
		function getcountselling(){
				$page=0;
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				$m='';
				$p='';
				if(isset($_GET['m'])){
		        	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }	
					$sql="SELECT * FROM sch_student_counseling";	
					$config['base_url'] =site_url()."/social/counseling?pg=1&m=$m&p=$p";	
					$config['total_rows'] = $this->green->getTotalRow($sql);
					$config['per_page'] =100;
					$config['num_link']=5;
					$config['page_query_string'] = TRUE;
					$config['full_tag_open'] = '<li>';
					$config['full_tag_close'] = '</li>';
					$config['cur_tag_open'] = '<a><u>';
					$config['cur_tag_close'] = '</u></a>';
					$this->pagination->initialize($config);
					$limi=" limit ".$config['per_page'];
					if($page>0){
							$limi=" limit ".$page.",".$config['per_page'];
						}		
					$sql.=" {$limi}";
					return $this->db->query($sql)->result();
		}
                function getadvice(){
                    $sql = "SELECT empid,first_name,last_name FROM sch_emp_profile";
                    return $this->db->query($sql)->result();
                }
                function getstudentcounseling(){
                    $sqls="SELECT
				v_assign_class.student_num,
				v_assign_class.first_name,
				v_assign_class.last_name,
				v_assign_class.first_name_kh,
				v_assign_class.last_name_kh,
				v_assign_class.gender,
				v_assign_class.studentid,
				v_assign_class.`year`,
				v_assign_class.classid,
                                v_assign_class.class_name,
				v_assign_class.schlevelid,
				v_assign_class.rangelevelid,
				v_assign_class.sch_level,
				v_assign_class.sch_year
			FROM
				v_assign_class			
			";
                    return $this->db->query($sqls)->result();
                }
                        function getfamilyrow($familyid){
			return $this->db->where('familyid',$familyid)->get('sch_family')->row_array();
		}
		function getmember($familyid){
			return $this->db->where('familyid',$familyid)->where('studentid !=','')->get('sch_student_member')->result();
		}
		function getcounsulrow($id){
			return $this->db->where('id',$id)->get('sch_student_counseling')->row_array();
		}
		function getcounsulted($counsel_id){
			return $this->db->select('*')
							->from('sch_counselling_staff cs')
							->join('sch_emp_profile emp','cs.emp_id=emp.empid','inner')
							->join('sch_emp_position p','emp.pos_id=p.posid','inner')
							->where('cs.counsel_id',$counsel_id)->get()
							->result();
		}
                function getsch_level($programid="")
		{
			$userid = $this->session->userdata('userid');
			if($programid !=""){
				$this->db->where("programid",$programid);
			}
			$this->db->select("*");
			$this->db->from("sch_school_level");
			$this->db->where('schlevelid in','(SELECT schlevelid FROM sch_user_schlevel WHERE userid = '.$userid.')',false);
			return $this->db->get()->result();
		}
        function getsch_levelupdate(){
            $id=$this->input->post('id');
            $sql = "select * from sch_student_counseling where id=$id";
            return $this->db->query($sqls)->result();
        }
        function save(){
            $programid=$this->input->post('pro_id');
			$schoollevel=$this->input->post('schoollevel');
			$studentname=$this->input->post('studentname');
            $levelschid=$this->input->post('levelschid');
            $student_num=$this->input->post('studentnum');
            $classname=$this->input->post('classname');
			$studyyear=$this->input->post('studyyear');
			$grandlevel=$this->input->post('grandlevel');
			$teachername=$this->input->post('teachername');
            $studentGender=$this->input->post('studentGender');
            $teachersubjectname=$this->input->post('teachersubject');
			$mistake=$this->input->post('mistake');
			$advised=$this->input->post('advised');
            $advice=$this->input->post('advice');
            $id=$this->input->post('id');
            $teacherid=$this->input->post('teacherid');
            $classid=$this->input->post('classid');
            $studentid=$this->input->post('studentid');
            $teachersubjectid=$this->input->post('teachersubjectid');
			$date=date('Y-m-d');
			$data=array('create_at'=>$date,
						'student_name'=>$studentname,
						'student_id'=>$studentid,
						'student_gender'=>$studentGender,
						'student_class'=>$classname,
                        'teacher'=>$teachername,
                        'schlevel'=>$schoollevel,
                        'subject'=>$teachersubjectname,
                        'schlevelid'=>$levelschid,
                        'study_year'=>$studyyear,
                        'programid'=>$programid,
                        'grade_level_id'=>$grandlevel,
						'mistake'=>$mistake,
						'advised'=>$advised,
						'advice'=>$advice,
						'subjectid'=>$teachersubjectid,
						'teacherid'=>$teacherid,
						'classid'=>$classid,
						'student_num'=>$student_num
					);
                        
			if($id!=''){
				$this->db->where('id',$id)->update('sch_student_counseling',$data);

			}else{
				$this->db->insert('sch_student_counseling',$data);
			}
		}
                function getgradelevels($schlevelid="")
                {
                    if($schlevelid!=""){
                        $this->db->where('schlevelid',$schlevelid);
                    }
                    $query=$this->db->get('sch_grade_level');
                    return $query->result();
                }
                public function getClassData($school_level_id, $grade_level_id, $adcademic_year_id, $class_id){
			$where = "";
			if($class_id != ""){
				$where.=" AND c.classid='".$class_id."'";
			}
			$sql_class = $this->db->query("SELECT c.classid, c.class_name 
                                                        FROM sch_class AS c
                                                        WHERE 1=1 {$where}
                                                        AND c.schlevelid='".$school_level_id."'
                                                        AND c.grade_levelid='".$grade_level_id."'");
		
			return $sql_class;
		}
        function getStudent($where){

			$sql = $this->db->query("SELECT
                                                v_student_enroment.student_num,
                                                v_student_enroment.first_name,
                                                v_student_enroment.last_name,
                                                v_student_enroment.first_name_kh,
                                                v_student_enroment.last_name_kh,
                                                v_student_enroment.gender,
                                                v_student_enroment.class_name,
                                                v_student_enroment.studentid,
                                                v_student_enroment.student_num
                                                FROM
                                                v_student_enroment
                                                WHERE 1=1 {$where}");
                        return $sql->result();
        }
       	function getteachersubject($teacherid='',$schlevelid = ''){//,$gradelevelid='',$classid=''){
			
			$sql="SELECT
						sch_subject.`subjectid`,
		                sch_subject_type.subj_type_id,
		                sch_subject_type.subject_type,
		                sch_subject_type.yearid,
		                sch_subject_type.schlevelid,
		                sch_subject.`subject`,
		                sch_subject.short_sub,
		                sch_teacher_subject.programid,
		                sch_teacher_subject.gradelevelid,
		                sch_teacher_subject.classid,
		                sch_teacher_subject.teacher_id
	                FROM
	                	sch_subject_type
	                INNER JOIN sch_subject ON sch_subject_type.subj_type_id = sch_subject.subj_type_id
	                INNER JOIN sch_teacher_subject ON sch_teacher_subject.subject_id = sch_subject.subjectid
	                WHERE 1=1 
	                AND sch_teacher_subject.teacher_id={$teacherid} 
	                AND sch_teacher_subject.schlevelid={$schlevelid} 
	                GROUP BY subject_type";	
	                //AND gradelevelid={$gradelevelid} AND classid=${classid} GROUP BY subject_type

			return $this->db->query($sql)->result();
		}
		function savestaff($counsel_id,$remark,$empid){
			$data=array('counsel_id'=>$counsel_id,
						'remark'=>$remark,
						'emp_id'=>$empid);
			$this->db->insert('sch_counselling_staff',$data);
		}
		function deletestaff($counsel_id){
			$this->db->where("counsel_id",$counsel_id)->delete("sch_counselling_staff");
		}
		function getfamilycoun($familyid,$c_type,$mem_id){
			$where='';
			if($c_type=='member')
				$where.=" AND mem_id='$mem_id'";
			$year=$this->session->userdata('year');
			return $this->db->query("SELECT * 
									FROM sch_counselling 
									WHERE familyid='$familyid'
									AND yearid='$year'
									AND c_type='$c_type' {$where}
									ORDER BY `session` DESC ")->result();
		}
                        function search($student_name,$student_id,$m,$p){
				$page=0;
				$where='';
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				if($student_name!=""){
					$where=" AND student_name like '%".$student_name."%' ";
				}
				if($student_id!=""){
					$where.=" AND student_id like '%".$student_id."%'";
				}
				$id=$this->input->post('id');
				$sql="SELECT * 
						FROM sch_student_counseling
						WHERE 1=1 {$where} ";	
				$config['base_url'] =site_url()."/social/counseling/search?pg=1&f_id=$id&student_name=$student_name&student_id=$student_id&m=$m&p=$p";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =50;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				return $this->db->query($sql)->result();
		}

	}
?>