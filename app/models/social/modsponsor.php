<?php
	class ModSponSor extends CI_Model{
		function getsponsor(){
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$m='';
			$p='';
			if(isset($_GET['m'])){
		        $m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }	
				$sql="SELECT * FROM sch_family_sponsor";	
				$config['base_url'] =site_url()."/social/sponsor?pg=1&m=$m&p=$p";	
				$config['total_rows'] = $this->green->getTotalRow($sql);
				$config['per_page'] =100;
				$config['num_link']=5;
				$config['page_query_string'] = TRUE;
				$config['full_tag_open'] = '<li>';
				$config['full_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<a><u>';
				$config['cur_tag_close'] = '</u></a>';
				$this->pagination->initialize($config);
				$limi=" limit ".$config['per_page'];
				if($page>0){
						$limi=" limit ".$page.",".$config['per_page'];
					}		
				$sql.=" {$limi}";
				return $this->db->query($sql)->result();
		}
		function saveImport(){
	        $is_imported = false;
	        if (isset($_FILES['file']) && $_FILES['file']["tmp_name"][0] != "") {
	            $_xfile = $_FILES['file']["tmp_name"];
	            function str($str)
	            {
	                return str_replace("'", "''", $str);
	            }

	            if ($_FILES['file']['tmp_name'] == "") {
	                $error = "<font color='red'>Please Choose your Excel file!</font>";
	            } else {
	                $html = "";
	                require_once(APPPATH.'libraries/simplexlsx.php');
	                foreach ($_xfile as $index => $value) {
	                    $xlsx = new SimpleXLSX($value);
	                    $_data = $xlsx->rows();
	                    array_splice($_data, 0, 1);
	                    $error_record_exist = "";
	                    foreach ($_data as $k => $r) {
	                        $_check_exist = $this->green->getTable("SELECT * FROM sch_family_sponsor WHERE sponsor_code = '{$r[0]}'");
	                        $seachspance = strpos(trim(str($r[0])), " ");
	                        if (count($_check_exist) > 0) {
	                            $error_record_exist .= '<div class="success" style=" padding:5px; margin-top:5px;">
															<font style="color:red;">Code already exist' . $r[0] . ' exist aleady !</font><br>
															</div>';
	                        } else if ($seachspance !== false) {
	                            $error_record_exist .= '<div class="success" style=" padding:5px; margin-top:5px;">
														<font style="color:red;">Error Code' . str($r[0]) . ' is incorrect. Please try again !</font></div>';
	                        } else {

	                            $sponsor_code   = trim(str($r[0]));
	                            $last_name 	    = trim(str($r[1]));
	                            $first_name     = trim(str($r[2]));
	                            $sex            = trim(str($r[3]));
	                            $dob 		    = trim(str($r[4]));
	                            $position 	    = trim(str($r[5]));
	                            $country 	    = trim(str($r[6]));
	                            $city 	        = trim(str($r[7]));
	                            $street 	    = trim(str($r[8]));
	                            $house_num 	    = trim(str($r[9]));
	                            $start_date     = trim(str($r[10]));
	                            $end_date 	    = trim(str($r[11]));
	                            $phone 	        = trim(str($r[12]));
	                            $mail 	        = trim(str($r[13]));
	                            $visit_sch_date = trim(str($r[14]));
	                            $budget 		= trim(str($r[15]));
	                            $created_date   = trim(str($r[16]));
	                            $created_by     = trim(str($r[17]));
	                            $modified_date  = trim(str($r[18]));
	                            $modified_by    = trim(str($r[19]));
	                            $type_sponsor   = trim(str($r[20]));
	                            $note 			= trim(str($r[21]));
	                            $title 			= trim(str($r[22]));
	                            $website 		= trim(str($r[23]));

	                            $SQL_DM = "INSERT INTO sch_family_sponsor";

	                            $SQL = " SET sponsor_code      ='".$sponsor_code."',
												last_name      ='".$last_name ."',
												first_name     ='".$first_name."',
												sex            ='".$sex."',
												dob            ='".$dob."',
												position       ='".$position."',
												country        ='".$country."',
												city           ='".$city."',
												street         ='".$street."',
												house_num      ='".$house_num."',
												start_date     ='".$start_date."',
												end_date 	   ='".$end_date."',
												phone		   ='".$phone."',
												mail 		   ='".$mail."',
												visit_sch_date ='".$visit_sch_date."',
												budget		   ='".$budget."',
												created_date   ='".$created_date."',
												created_by     ='".$created_by."',
												modified_date  ='".$modified_date."',
												modified_by    ='".$modified_by."',
												type_sponsor   ='".$type_sponsor."',
												note           ='".$note."',
												title          ='".$title."',
												website        ='".$website."'
											";
	                            $counts = $this->green->runSQL($SQL_DM.$SQL);
	                            $is_imported = true;

	                        }
	                    }
	                }
	            }
	        }
	        return $is_imported;
    }
		function save($sponsorid=''){
			$date=date('Y-m-d');
			$user=$this->session->userdata('user_name');
			$data=array('sponsor_code'=>$this->input->post('sponsor_code'),
						'last_name'=>$this->input->post('last_name'),
						'first_name'=>$this->input->post('first_name'),
						'sex'=>$this->input->post('gender'),
						'dob'=>$this->green->formatSQLDate($this->input->post('dob')),
						'position'=>$this->input->post('position'),
						'country'=>$this->input->post('country'),
						'city'=>$this->input->post('city'),
						'street'=>$this->input->post('street'),
						'house_num'=>$this->input->post('house_no'),
						'start_date'=>$this->green->formatSQLDate($this->input->post('start_sp_date')),
						'end_date'=>$this->green->formatSQLDate($this->input->post('end_sp_date')),
						'phone'=>$this->input->post('phone'),
						'mail'=>$this->input->post('email'),
						'budget'=>$this->input->post('budget'),
						'Note'=>$this->input->post('note'),
						'website'=>$this->input->post('website')
				);
			if($sponsorid!=''){
				$data2=array('modified_date'=>$date,
								'modified_by'=>$user);
				$this->db->where('sponsorid',$sponsorid)->update('sch_family_sponsor',array_merge($data,$data2));
			
				//-------- Delete first --------
				$this->deleteStudentSponsor($sponsorid);
				//-------- New insert ---------- 
				$studentid=$this->input->post('student_id');
				$classid=$this->input->post('class_id');
				$yearid=$this->session->userdata('year');
				for($i=0;$i<count($studentid);$i++){
					if ($studentid!='')
						$this->saveStudentSponsor($sponsorid,$studentid[$i],$classid[$i],$yearid);
				}
			}else{
				$data2=array('created_date'=>$date,
								'created_by'=>$user);
				$this->db->insert('sch_family_sponsor',array_merge($data,$data2));

				$last_sponsorid=$this->db->insert_id();
				//-------- insert student ---------- 
				$studentid=$this->input->post('student_id');
				$classid=$this->input->post('class_id');
				$yearid=$this->session->userdata('year');
				for($i=0;$i<count($studentid);$i++){
					if ($studentid!='')
						$this->saveStudentSponsor($last_sponsorid,$studentid[$i],$classid[$i],$yearid);
				}
			}
		}
		function saveStudentSponsor($sponsorid,$studentid,$classid,$yearid){
			$data=array(
						'sponsorid' => $sponsorid,
						'studentid' => $studentid,
						'classid'	=> $classid,
						'yearid'	=> $yearid
				 );
			$this->db->insert('sch_student_sponsor_detail',$data);
		}
		function deleteStudentSponsor($sponsorid){
			$this->db->where('sponsorid',$sponsorid)->delete('sch_student_sponsor_detail');
		}
		function validate($last_name,$first_name,$dob,$sponsorid){
			$this->db->select('count(*)')
					->from('sch_family_sponsor')
					->where('last_name',$last_name)
					->where('first_name',$first_name)
					->where('dob',$dob);
			if($sponsorid!='')
				$this->db->where_not_in('sponsorid',$sponsorid);
			return $this->db->count_all_results();
		}
		function getsponsorrow($sponsorid){
			// return $this->db->where('sponsorid',$sponsorid)
			// 		->get('sch_family_sponsor')->row_array();
			return $this->db->query("SELECT * FROM sch_family_sponsor WHERE sponsorid='".$sponsorid."'")->row_array();
		}
		function getstdsponsor($sponsorid){
			return $this->db->query("SELECT * FROM sch_student_sponsor_detail sd
									INNER JOIN v_student_profile s 
									ON(sd.studentid=s.studentid)
									INNER JOIN sch_family f
									ON(s.familyid=f.familyid)
									WHERE s.yearid=sd.yearid
									AND sd.sponsorid='".$sponsorid."'

									")->result();
		}
		function search($sponsor_code,$name,$position,$country,$s_num,$m,$p){
				$page=0;
				$where='';
				if(isset($_GET['per_page']))
					$page=$_GET['per_page'];
				if($sponsor_code!=""){
					$where=" AND sponsor_code like '%".$sponsor_code."%'";
				}
				if($position!=""){
					$where=" AND position like '%".$position."%'";
				}
				if($country!=""){
					$where=" AND country like '%".$country."%'";
				}
				if($name!=""){
					$where=" AND (last_name like '%".$name."%' OR first_name like '%".$name."%')";
				}
					$sql="SELECT * FROM sch_family_sponsor WHERE 1=1 {$where}";	
					$config['base_url'] =site_url()."/social/sponsor/search?pg=1&s_code=$sponsor_code&name=$name&p=$position&c=$country&s_num=$s_num&m=$m&p=$p";	
					$config['total_rows'] = $this->green->getTotalRow($sql);
					$config['per_page'] =$s_num;
					$config['num_link']=5;
					$config['page_query_string'] = TRUE;
					$config['full_tag_open'] = '<li>';
					$config['full_tag_close'] = '</li>';
					$config['cur_tag_open'] = '<a><u>';
					$config['cur_tag_close'] = '</u></a>';
					$this->pagination->initialize($config);
					$limi=" limit ".$config['per_page'];
					if($page>0){
							$limi=" limit ".$page.",".$config['per_page'];
						}		
					$sql.=" {$limi}";
					return $this->db->query($sql)->result();
		}
		
	}