<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Modsocialdash extends CI_Model {
        function getStdbyFam($yearid){
        	$WH="";
        	if($yearid!=""){
        		$WH=" AND is_active=1
					AND yearid='".$yearid."'
					AND leave_school='0'
					AND (ISNULL(is_vtc) OR is_vtc=0)
					";
        	}	
			$sql="SELECT t1.num_stu as num_student,
						t2.num_fam as num_family,
						t3.stu_1_over as over_1student FROM 
				(SELECT count(studentid) as num_stu 
					FROM v_student_profile 
					WHERE 1=1 {$WH} ) as t1,
				(SELECT COUNT(familyid) as num_fam 
					FROM sch_family WHERE is_active=1 AND familyid <>'' 
					) as t2,
				(SELECT COUNT(*) as stu_1_over 
					FROM (SELECT familyid,COUNT(studentid) as num_stu 
						FROM v_student_profile 
						WHERE familyid <>'' 
						{$WH}
						GROUP BY familyid 
						HAVING  num_stu>=1) as stu_by_fam) as t3";
			$sql2="SELECT
						COUNT(*) AS num_fam,
						fam.number_stu
					FROM
						(
							SELECT
								COUNT(*) number_stu
							FROM
								v_student_profile stu
							INNER JOIN sch_family ON sch_family.familyid = stu.familyid
							WHERE
								stu.familyid <> ''
							AND stu.is_active = 1
							AND stu.leave_school='0'
							AND (ISNULL(is_vtc) OR is_vtc=0)
							AND sch_family.is_active = 1
							AND yearid=$yearid
							GROUP BY
								stu.familyid
						) AS fam
					GROUP BY
						fam.number_stu
					HAVING
						number_stu >= 1";
			$row=$this->green->getOneRow($sql);
			$row2=$this->green->getTable($sql2);
			$array['f_total']=$row;
			$array['h_child']=$row2;
			return $array;
		}
		function familyRice() {
				$year=date('Y');
				$f_total_get="SELECT COUNT(*) as f_total 
							from( Select DISTINCT familyid from sch_family_distributed_detail where year='".$year."') as num";
				$f_times_get="SELECT t1.num,count(*) as family 
							FROM (SELECT count(*) as num,familyid 
									from sch_family_distributed_detail 
									where year='$year'
									GROUP BY familyid) as t1 
														GROUP BY t1.num
														ORDER BY t1.num DESC";
				$f_times_total=$this->green->getOneRow($f_total_get);
				$family_total=$this->green->getTable($f_times_get);
				$arr['family_get']=$f_times_total;
				$arr['f_times']=$family_total;
				return $arr;
		}  
		function familyRicetotal() {
				$year=date('Y');
				$total_rice="SELECT sum(amt_rice) as total FROM sch_family_distributed_detail WHERE year='".$year."'";
				$detail="SELECT t1.f_rice,count(*) as family 
								FROM (SELECT sum(amt_rice) as f_rice,familyid 
								FROM sch_family_distributed_detail 
								WHERE year='".$year."' GROUP BY familyid) as t1 
										GROUP BY t1.f_rice Order By t1.f_rice DESC";
				$totalrice=$this->green->getOneRow($total_rice);
				$rice_detail=$this->green->getTable($detail);
				$arr['totalrice']=$totalrice;
				$arr['rice_detail']=$rice_detail;
				return $arr;
		}  	
    }