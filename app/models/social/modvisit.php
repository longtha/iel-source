<?php
    class Modvisit extends CI_Model {
    	function getfamilyrow($familyid){
    		$this->db->where('familyid',$familyid);
    		return $this->db->get('sch_family')->row();
    	}
    	function getstdbyfamily($familyid){
    		return $this->db->query("SELECT first_name,last_name,familyid,studentid,DATE_FORMAT(dob,'%d-%m-%Y') as dob FROM sch_student_member WHERE familyid='".$familyid."' AND studentid<>''")->result();
    	}
    	function getclassbystd($studentid){
    		return $this->db->query("SELECT * FROM v_student_profile s WHERE s.studentid='$studentid' AND s.yearid=(SELECT MAX(year) FROM sch_student_enrollment WHERE studentid='$studentid')")->row();
    	}
    	function getvisitrow($familyid){
    		return $this->db->query("SELECT DATE_FORMAT(fv.date,'%d-%m-%Y') as dates,
						fv.visitid,
						fv.date,
						fv.intervation_type,
						fv.visit_reason,
						fv.update_info,
						fv.sw_activities,
						fv.outcome,
						fv.familyid,
						fv.visit_times,
						fv.yearid,
						fv.schoolid
					FROM sch_family_visit fv
					WHERE fv.familyid='".$familyid."'
                    ORDER BY fv.date desc")->result();
    	}
       
    }