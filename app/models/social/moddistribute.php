<?php
    class ModDistribute extends CI_Model {

       public function getdistribute(){
		$page=0;
		if(isset($_GET['per_page']))   
			$page=$_GET['per_page'];
		$m='';
		$p='';
		if(isset($_GET['m'])){
			$m=$_GET['m'];
		}
		if(isset($_GET['p'])){
			$p=$_GET['p'];
		}
			$sql="SELECT * FROM sch_family_distributed fb
				INNER JOIN sch_family_distributed_detail fbd
				ON(fb.distribut_id=fbd.distribut_id)
				INNER JOIN sch_family f
				ON(fbd.familyid=f.familyid)
				ORDER BY fbd.transno DESC,fbd.familyid ASC";
			$config['base_url'] =site_url()."/social/distribute?pg=1&m=$m&p=$p";
			$config['total_rows'] = $this->green->getTotalRow($sql);
			$config['per_page'] =200;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);
			$limi=" limit ".$config['per_page'];
			if($page>0){
					$limi=" limit ".$page.",".$config['per_page'];
				}
				$sql.=" {$limi}";
			return $this->db->query($sql)->result();
	}
	function getfamilyplan($dis_type,$year){
			$sql_page="SELECT
					dsplan.displanid,
					dsplan.familyid,
					dsplan.family_code,
					dsplan.father_name,
					dsplan.father_name_kh,
					dsplan.mother_name,
					dsplan.mother_name_kh,
					dsplan.amt_rice,
					dsplan.rice_uom,
					dsplan.oil,
					dsplan.oil_uom,
					dsplan.`year`,
					dsplan.distrib_type,
					dsplan.modified_by,
					dsplan.modified_date,
					dsplan.remark,
					dsplan.created_date,
					dsplan.created_by
				FROM
					v_fam_distrib_plan AS dsplan
				WHERE dsplan.distrib_type='$dis_type'
				AND dsplan.year='$year'
				ORDER BY dsplan.`year`,dsplan.distrib_type,dsplan.familyid
				";
			return $this->db->query($sql_page)->result();
	}
	function getDistribType(){
			$arr=array("12"=>"Every Month","4"=>"4 times per year","2"=>"2 times per year");
			return $arr;
	}
	function getdistrrow($distribut_id){
		return $this->db->where('distribut_id',$distribut_id)
				->get('sch_family_distributed')->row_array();
	}
	function getfamilyvalidate($familyid,$year,$dis_type){
		return $this->db->where('familyid',$familyid)
				->where('year',$year)
                ->where('distrib_type',$dis_type)
				->from('sch_family_distribute_plan')->count_all_results();
	}
	function updatetran($sequence,$typeid){
		$this->db->set('sequence',$sequence)
				->where('typeid',$typeid)
				->update('sch_z_systype');
	}
	function getdisdetail($distribut_id){
		$sql="SELECT * FROM sch_family_distributed fb
						INNER JOIN sch_family_distributed_detail fbd
						ON(fb.distribut_id=fbd.distribut_id) 
						INNER JOIN sch_family f
						ON(fbd.familyid=f.familyid)
						WHERE fbd.distribut_id='$distribut_id'
						ORDER BY fbd.transno DESC,fbd.familyid ASC";	
		return $this->db->query($sql)->result();
	}
	public function search($familyid,$father_name,$mother_name,$s_date,$e_date,$sort_num,$m,$p,$distrib_type,$yearid){
		$page=0;
		if(isset($_GET['per_page']))
			$page=$_GET['per_page']-0;
			$where="";
		if($familyid!=""){
			$where=" AND family_code like '%".$familyid."%'";
		}
		if($father_name!=""){
			$where.=" AND (father_name like '%".$father_name."%' OR father_name_kh  like '%".$father_name."%')";
		}
		if($mother_name!=""){
			$where.=" AND (mother_name like '%".$mother_name."%' OR mother_name_kh  like '%".$mother_name."%')";
		}
		if($e_date!="" && $s_date!=""){
			$where.=" AND fb.dis_date BETWEEN '".$s_date."' AND '".$e_date."'";
		}
		if($distrib_type!=""){
			$where.=" AND fb.distrib_type='".$distrib_type."'";
		}
		if($yearid!=''){
			$year=$this->db->query("SELECT year(to_date) AS years FROM sch_school_year WHERE yearid='$yearid'")->row()->years;
			$where.=" AND fb.year='".$year."'";
		}

		$sql="SELECT * FROM sch_family_distributed fb
			LEFT JOIN sch_family_distributed_detail fbd
			ON(fb.distribut_id=fbd.distribut_id)
			LEFT JOIN sch_family f
			ON(fbd.familyid=f.familyid)
			WHERE fbd.distribut_id<>'' {$where} ORDER BY fbd.transno DESC,fbd.familyid ASC";
		
		$config['base_url'] =site_url()."/social/distribute/search?pg=1&f_id=$familyid&fn=$father_name&mm=$mother_name&s_date=$s_date&e_date=$e_date&s_num=$sort_num&m=$m&p=$p&dst=$distrib_type&y=$yearid";
		$config['total_rows'] = $this->green->getTotalRow($sql);
		$config['per_page'] =$sort_num;
		$config['num_link']=5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';
		$this->pagination->initialize($config);

		$limi=" limit ".$config['per_page'];
		if($page>0){
				$limi=" limit ".$page.",".$config['per_page'];
			}
			$sql.=" {$limi}";
		return $this->db->query($sql)->result();
	}
}