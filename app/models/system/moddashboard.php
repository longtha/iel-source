<?php
	class ModDashBoard extends CI_Model{
		function getstudentdash($year){
			$level222=$this->db->query("SELECT COUNT(*) te_grade,
											TG.grade,
											TG.gradeid,
											TG.is_vtc,
											sum(TG.total_stu) as total_stu 
											FROM
													(SELECT count(vs.studentid) total_stu,
																g.grade_level as grade,
																g.grade_levelid as gradeid,
																vs.is_vtc
														FROM v_student_profile vs 
														INNER JOIN sch_class c 
														ON(vs.classid=c.classid)
														INNER JOIN sch_grade_level g 
														ON(c.grade_levelid=g.grade_levelid) 
														WHERE vs.yearid='".$year."'
														AND vs.leave_school='0' 
														GROUP BY c.classid) as TG
											WHERE (ISNULL(TG.is_vtc) or TG.is_vtc=0)
											GROUP BY TG.gradeid")->result();
			$level=$this->db->query("SELECT
											COUNT(*) te_grade,
											TG.grade,
											TG.gradeid,
											TG.is_vtc,
											sum(TG.total_stu) AS total_stu,
											TG.term_sem_year_id,
											TG.yearid,
											TG.classid,
											TG.class_name,
											TG.is_vtc
										FROM
											(
										SELECT
											count(vs.studentid) total_stu,
											g.grade_level AS grade,
											g.grade_levelid AS gradeid,
											vs.term_sem_year_id,
											vs.yearid,
											vs.classid,
											vs.class_name,
											vs.is_vtc
										FROM
											v_student_fee_inv_list vs
										INNER JOIN sch_grade_level g ON (
											vs.grade_levelid = g.grade_levelid
										)
										WHERE
											1 = 1
										AND vs.yearid='".$year."'
										AND vs.leave_school='0'
										GROUP BY
											vs.classid
											) AS TG
										WHERE
											(ISNULL(TG.is_vtc) or TG.is_vtc=0)
										GROUP BY TG.gradeid")->result();
			$total=$this->db->query("SELECT count(studentid) as total  
							FROM v_student_profile  
							WHERE yearid='".$year."'
							AND leave_school='0'
							AND (ISNULL(is_vtc) OR is_vtc=0)")->row();
			$leave=$this->db->query("SELECT count(*) as total FROM sch_student 
									WHERE leave_school='1'
									AND YEAR(leave_sch_date)<=(SELECT YEAR(from_date)  FROM sch_school_year 
									WHERE yearid='".$year."')")->row();
			$vtc=$this->db->query("SELECT count(studentid) as total  
							FROM v_student_profile  
							WHERE yearid='".$year."'
							AND leave_school='0'
							AND is_vtc='1'")->row()->total;
		//	$vtc_promotion=$this->db->query("SELECT DISTINCT proname FROM sch_school_promotion WHERE yearid='".$year."'");
		//--- rothna  --- 
			$vtc_promotion=$this->db->query("SELECT
												vtc_stud.yearid,
												vtc_stud.promot_id,
												pro.proname,
												COUNT(vtc_stud.studentid) AS num_stu
											FROM
												v_student_profile AS vtc_stud
											INNER JOIN sch_school_promotion pro ON pro.promot_id = vtc_stud.promot_id
											WHERE
												vtc_stud.promot_id > 0
											AND vtc_stud.is_active = 1
											GROUP BY
												vtc_stud.promot_id,
												vtc_stud.yearid='".$year."',
												pro.proname");
			$had_sing=$this->green->getOneRow("SELECT
													sch_class.classid,
													sch_class.class_name,
													slevel.schlevelid,
													sch_class.grade_levelid
												FROM
													sch_class
												INNER JOIN sch_school_level slevel ON sch_class.schlevelid = slevel.schlevelid
												WHERE
													slevel.is_vtc = 1");
			$dash_student_name=$this->green->getOneRow("SELECT
															vs.fullname,
															vs.fullname_kh
														FROM
															v_student_profile vs
														WHERE
															1 = 1
														AND vs.studentid = '".$this->session->userdata('emp_id')."'
														AND vs.leave_school = '0'
														GROUP BY
															vs.studentid");
            if(isset($had_sing['class_name'])){
                $arr['had_sing']=$had_sing['class_name'];
                $arr['vtc_class_id']=$had_sing['classid'];
                $arr['vtc_grade_levelid']=$had_sing['grade_levelid'];
            }else{
                $arr['had_sing']="";
                $arr['vtc_class_id']="";
                $arr['vtc_grade_levelid']="";
            }
            
            if(!is_null($this->session->userdata('emp_id'))){
            	 $arr['fullname']=(isset($dash_student_name['fullname'])?$dash_student_name['fullname']:"");
            	 $arr['fullname_kh']=(isset($dash_student_name['fullname_kh'])?$dash_student_name['fullname_kh']:"");;
            }else{
            	$arr['fullname']="";
				$arr['fullname_kh']="";
            }
			$arr['level']=$level;
			$arr['vtc_promotion']= $vtc_promotion;
			$arr['total']=$total;
			$arr['vtc_total']=$vtc;
			$arr['t_leave']=$leave;
			return $arr;
		}
		// start longtha
		function getStudentInGrade($year= "",$s_term_type= "",$term_sem_year_id= ""){
			$cond_where="";
			if($s_term_type!=""){
				$cond_where.=" AND vs.paymentmethod='".$s_term_type."'";	
			}
			if($term_sem_year_id!=""){
				$cond_where.=" AND	vs.term_sem_year_id='".$term_sem_year_id."'";	
			}
		return $this->db->query("SELECT
									COUNT(*) te_grade,
									TG.grade,
									TG.gradeid,
									TG.is_vtc,
									sum(TG.total_stu) AS total_stu,
									TG.term_sem_year_id,
									TG.yearid,
									TG.classid,
									TG.class_name
								FROM
									(
								SELECT
									count(vs.studentid) total_stu,
									g.grade_level AS grade,
									g.grade_levelid AS gradeid,
									vs.term_sem_year_id,
									vs.yearid,
									vs.classid,
									vs.class_name,
									vs.is_vtc
								FROM
									v_student_fee_inv_list vs
								INNER JOIN sch_grade_level g ON (
									vs.grade_levelid = g.grade_levelid
								)
								WHERE
									1 = 1
								AND vs.yearid='".$year."'
								AND vs.leave_school='0'
								{$cond_where}
								GROUP BY
									vs.classid
									) AS TG
								WHERE
									(ISNULL(TG.is_vtc) or TG.is_vtc=0)
								GROUP BY
									TG.gradeid")->result();	
		}
		function getTotalStudentInGrade($year,$s_term_type='',$term_sem_year_id='',$grade_levelid=''){
			$cond_where="";
			if($s_term_type!=""){
				$cond_where.=" AND vs.feetypeid='".$s_term_type."'";	
			}
			if($term_sem_year_id!=""){
				$cond_where.=" AND	vs.term_sem_year_id='".$term_sem_year_id."'";	
			}
			if($grade_levelid!=""){
				$cond_where.=" AND	vs.grade_levelid='".$grade_levelid."'";	
			}
			return $this->db->query("SELECT count(studentid) as total  
							FROM v_student_profile as vs  
							WHERE vs.yearid='".$year."' {$cond_where}
							AND vs.leave_school='0'
							AND (ISNULL(vs.is_vtc) OR vs.is_vtc=0)")->row();
		}
		//---end rothna  ---
		function getsocialdash($s_date,$end_date){
			$data=$this->db->query("SELECT t1.dis,t2.visit,t3.counseling FROM 
									(SELECT COUNT(DISTINCT(dis_date)) AS dis FROM sch_family_distributed WHERE dis_date BETWEEN '".$s_date."' AND '".$end_date."') as t1,
									(SELECT COUNT(DISTINCT(sch_family_visit.date)) AS visit FROM sch_family_visit WHERE sch_family_visit.date BETWEEN '".$s_date."' AND '".$end_date."') as t2,
									(SELECT COUNT(DISTINCT(sch_counselling.date)) AS counseling FROM sch_counselling WHERE sch_counselling.date BETWEEN '".$s_date."' AND '".$end_date."') as t3")->row_array();
			return $data;
		}
		function getdistribut($s_date,$end_date){
			$data=$this->db->query("SELECT sum(total_oil) as oil_total,
											sum(tota_rice) as tota_rice 
									FROM sch_family_distributed
									WHERE dis_date BETWEEN '".$s_date."' AND '".$end_date."'")->row_array();
			return $data;
		}
		function getdistribution($sdate,$edate){
			$data=$this->db->query("SELECT sum(total_oil) as oil_total,
											sum(tota_rice) as tota_rice 
									FROM sch_family_distributed
									WHERE dis_date BETWEEN '".$sdate."' AND '".$edate."'");
			return $data->row();
		}
		//------ Rothna-----------
		function getvisit($sdate_visited,$edate_visited){
			$data=$this->db->query("SELECT COUNT(*) as number_visit FROM sch_family_visit
									WHERE date BETWEEN '".$sdate_visited."' AND '".$edate_visited."'
									");
			return $data->row();
		}
		//count visit
		function connamevi($sdate_visited,$edate_visited){
			$data=$this->db->query("SELECT e.empid,
										Count(vs.visitdetid) AS c_visited,
										CONCAT(
											e.last_name,
											' ',
											e.first_name
										) AS Fullname,
										fm.date,
										vs.empid
									FROM
										sch_family_visit_staff AS vs
									INNER JOIN sch_emp_profile AS e ON vs.empid = e.empid
									LEFT JOIN sch_family_visit AS fm ON fm.visitid = vs.visitid
									WHERE date 
									BETWEEN '".$sdate_visited."'AND '".$edate_visited."'
									GROUP BY
										e.empid
									ORDER BY
										fullname ASC");
			return $data->result();
		}
		// ----end rothna --------------------
		function getdis($sdate,$edate){
			return $this->db->query("SELECT distrib_type,
											SUM(total_family) AS total_family,
											SUM(total_oil) AS oil_total,
											SUM(tota_rice) AS tota_rice 
									FROM sch_family_distributed
									WHERE dis_date  BETWEEN '".$sdate."' AND '".$edate."'
									GROUP BY distrib_type 
									ORDER BY distrib_type DESC")->result();
		}
		function gethealthdash($s_date,$end_date){
			$data=$this->db->query("SELECT t1.total,t2.stu,t3.emp FROM 
			(SELECT count(treatmentid) as total FROM sch_medi_treatment WHERE sch_medi_treatment.date BETWEEN '".$s_date."' AND '".$end_date."') as t1,
			(SELECT count(treatmentid) as stu FROM sch_medi_treatment WHERE patient_type='student' AND sch_medi_treatment.date BETWEEN '".$s_date."' AND '".$end_date."') as t2,
			(SELECT count(treatmentid) as emp FROM sch_medi_treatment WHERE patient_type='emp' AND sch_medi_treatment.date BETWEEN '".$s_date."' AND '".$end_date."') as t3")->row_array();
			return $data;
		}
		function getsicknes($s_date,$end_date){
			return $this->db->query("SELECT count(mtd.treatedisid) as discount,mdis.disease
									FROM sch_medi_treatment mt
									INNER JOIN sch_medi_treatment_disease mtd
									ON(mt.treatmentid=mtd.treatmentid)
									INNER JOIN sch_medi_disease mdis
									ON(mtd.diseaseid=mdis.diseaseid)
									WHERE mt.date BETWEEN '".$s_date."' AND '".$end_date."' 
									GROUP BY mtd.diseaseid")->result();
		}
		function getrefer($s_date,$end_date){
			$total=$this->db->query("SELECT count(treatmentid) as total 
									FROM sch_medi_treatment 
									where is_internal_treat='0'
									AND date BETWEEN '".$s_date."' AND '".$end_date."'")->row()->total;
			$ex=$this->db->query("SELECT count(treatmentid) as count,external_hospital 
								FROM sch_medi_treatment 
								where is_internal_treat='0' AND date BETWEEN '".$s_date."' AND '".$end_date."' 
								GROUP BY external_hospital")->result();
			$patient=$this->green->getValue("SELECT count(DISTINCT patientid,patient_type) as count from sch_medi_treatment 
								WHERE is_internal_treat='0' AND date BETWEEN '".$s_date."' AND '".$end_date."' 
								GROUP BY external_hospital")-0;

			$data['total']=$total;
			$data['patient']=$patient;
			$data['ex']=$ex;
			return $data;
		}
		function getfmem(){
			$data=$this->db->query("SELECT count(T1.familyid) as f_total,T1.t_mem FROM
									(
										SELECT
											count(memid) AS t_mem,
											familyid
										FROM
											sch_student_member
										WHERE is_active=1
										GROUP BY
											familyid
									) AS T1
									
									GROUP BY T1.t_mem
									ORDER BY T1.t_mem DESC")->result();
			$total=$this->db->query("SELECT
										count(memid) AS total
									FROM
										sch_student_member
									WHERE is_active=1")->row()->total;
			$arr['data']=$data;
			$arr['total']=$total;
			return $arr;
		}
		function getempdash(){
			$data=$this->db->query("SELECT count(*) as total FROM sch_emp_profile where is_active=1")->row_array();
			return $data;
		}
		function getclassname($grade_levelid,$yearid){
			$data=$this->db->query("SELECT count(vs.studentid) total_stu,
									c.class_name,
									c.grade_levelid as grade 
									FROM v_student_profile vs 
									INNER JOIN sch_class c 
									ON(vs.classid=c.classid) 
									WHERE vs.yearid='".$yearid."'
									AND c.grade_levelid='".$grade_levelid."'
									AND vs.leave_school='0' 
									GROUP BY c.class_name")->result();
			return $data;
		}
		function dashboard_getclassname($emp_id){
			$data=$this->db->query("SELECT
											TG.grade,
											TG.gradeid,
											TG.is_vtc,
											TG.class_name
										FROM
											(
												SELECT
													count(vs.studentid) total_stu,
													g.grade_level AS grade,
													g.grade_levelid AS gradeid,
													vs.is_vtc,
													vs.class_name
												FROM
													v_student_profile vs
												INNER JOIN sch_class c ON (vs.classid = c.classid)
												INNER JOIN sch_grade_level g ON (
													c.grade_levelid = g.grade_levelid
												)
												WHERE
													1 = 1
												AND vs.studentid = '".$emp_id."'
												AND vs.leave_school = '0'
												GROUP BY
													c.classid
											) AS TG
										WHERE
											(
												ISNULL(TG.is_vtc)
												OR TG.is_vtc = 0
											)
										GROUP BY
											TG.gradeid")->result();
			return $data;
		}
		function getminmax($grade_levelid,$year){
			$min=$this->db->query("SELECT MIN(age) as min_age
									FROM v_student_profile s
									INNER JOIN sch_class c
									ON(s.classid=c.classid)
									WHERE c.grade_levelid='".$grade_levelid."'
									AND s.yearid='".$year."'
									AND s.leave_school='0' ")->row();
			$max=$this->db->query("SELECT MAX(age) as max_age
									FROM v_student_profile s
									INNER JOIN sch_class c
									ON(s.classid=c.classid)
									WHERE c.grade_levelid='".$grade_levelid."'
									AND s.yearid='".$year."'
									AND s.leave_school='0' ")->row();
			$arr['min']=$min;
			$arr['max']=$max;
			return $arr;
		}
		function getNumByAge($year,$minage,$maxage){
			$whage="";
			if($minage!=""){
				$whage.=" AND age >=$minage ";
			}
			if($maxage!=""){
				$whage.=" AND age <=$maxage ";
			}
			$sql="SELECT COUNT(studentid) as num
						FROM v_student_profile s
						INNER JOIN sch_class c
						ON(s.classid=c.classid)
						WHERE s.yearid='".$year."'
						AND s.leave_school='0' 
						AND (s.is_vtc=0 OR ISNULL(s.is_vtc))
						{$whage}";
			$num=$this->green->getValue($sql);						
			return $num;
		}

		function getNumStdByAgeLev($grade_levelid,$year,$minage,$maxage){
			$whage="";
			if($minage!=""){
				$whage.=" AND age >=$minage ";
			}
			if($maxage!=""){
				$whage.=" AND age <=$maxage ";
			}
			$sql="SELECT COUNT(studentid) as num
									FROM v_student_profile s
									INNER JOIN sch_class c
									ON(s.classid=c.classid)
									WHERE c.grade_levelid='".$grade_levelid."'
									AND s.yearid='".$year."'
									AND s.leave_school='0'
									AND (s.is_vtc=0 OR ISNULL(s.is_vtc))
									{$whage}";
			$num=$this->green->getValue($sql);						
			return $num;
		}

		
		function getempdetail(){
						$sql="SELECT
								count(emp.empid) AS total,
								p.posid,
								p.position,
								emp.nationality,
								emp.sex
							FROM
								sch_emp_profile emp
							LEFT JOIN sch_emp_position p ON (emp.pos_id = p.posid)
							WHERE emp.is_active=1
							GROUP BY
								emp.nationality,
								p.position,
								emp.sex
							ORDER BY
								p.position
					";
			$data=$this->green->getTable($sql);
			return $data;
		}
		function getdoctor()
		{	return $this->db->select('*')
					->from('sch_emp_profile emp')
					->join('sch_emp_position emp_p','emp.pos_id=emp_p.posid','inner')
					->where('emp_p.match_con_posid','doc')->get()->result();
			
		}
		
		function getallpostion(){
			$sql="SELECT DISTINCT p.posid,p.position,emp.nationality
			FROM sch_emp_profile emp			
			INNER JOIN sch_emp_position p ON(emp.pos_id=p.posid)
			ORDER BY p.position";
			return $this->green->getTable($sql);
		}
		
		function getFamRevenu(){
			$field="revenue";
			$arrRevParam=array("$field<50"=>"<50",
								"$field>=50 AND $field<100"=>"<100",
								"$field>=100 AND $field<150"=>"<150",
								"$field>=150"=>">150"
								);
			$tr="";
			foreach ($arrRevParam as $key => $value) {
				$num_fam=$this->green->getValue("SELECT COUNT(familyid) as fam_num 
												FROM sch_family
												WHERE revenue>0 
												AND is_active=1
												AND {$key}
												");
				$str_revenu=$key;
				$father_name="";$father_name_kh="";$father_ocupation="";$mother_name="";$mother_name_kh="";$mother_ocupation="";$sort_num="all";$family_code="";$num_stu="";$num_stu="";$nm="";$fm="";
				$link=(site_url()."/social/family/search?fn=$father_name&fnk=$father_name_kh&fc=$father_ocupation&mn=$mother_name&mnk=$mother_name_kh&mc=$mother_ocupation&s_num=$sort_num&fcode=$family_code&nst=$num_stu&srev=".urlencode($str_revenu)."&nm=$nm&fm=$fm");
				$tr.="<tr>
							<td>Family who have revenue{$value}</td>							
							<td  class='text-right'><a href='".$link."' target='_blank'>{$num_fam} Families</a></td>							
					  </tr>";				

			}
			return $tr;
		}
		function getFemaleMem(){
			$data=$this->db->query("SELECT COUNT(fembyfamily.familyid) totalFfami,fembyfamily.mem_num_F 
											FROM (SELECT familyid,COUNT(*) as mem_num_F 
													FROM sch_student_member 
													WHERE is_active=1 
													AND sex='F' 
													AND familyid<>0 
													GROUP BY familyid,sex)as fembyfamily
													GROUP BY fembyfamily.mem_num_F
												")->result();
			$total=$this->db->query("SELECT
										count(memid) AS total
									FROM
										sch_student_member
									WHERE is_active=1
									AND sex='F' 
									AND familyid<>0
									")->row()->total;
			$arr['data']=$data;
			$arr['total']=$total;
			return $arr;
		}
// tha
		public function countvillage($commune)
        {
            $this->db->distinct();
			$this->db->select('village');
			$this->db->where('commune', trim($commune));
			$this->db->where('is_active', 1);
			$this->db->group_by('village'); 
			$query = $this->db->get('sch_family');
            return $query->num_rows();
        }
        public function countcommunce(){
        	$sql="SELECT
						COUNT(*) AS countcommunce
					FROM
						(".$this->sqlcountcommunce().") 
					AS  tbl
				";
				
			return $this->green->getValue($sql);
        }
        public function sqlcountcommunce(){
        	$sql = "SELECT
						COUNT(f.family_code) AS family_num,
						f.commune
					FROM
						sch_family f
					WHERE f.commune <> '' 
					AND f.commune <> '0' 
					AND f.is_active = '1'
					GROUP BY
						f.commune
					ORDER BY
						f.commune";
			return $sql;
        }
		public function staticfamilylocation()
		{
			$resfl = $this->db->query($this->sqlcountcommunce())->result();
			if(count($resfl)> 0){
					$tr='';
					$i=1;
				foreach($resfl as $row){
					$countvillage = $this->countvillage($row->commune);
					$tr.='<tr>
	           				<td colspan="2">
	           					<label class="control-label">+ '.$row->commune.' Communce </label> 
	           					<label class="control-label">'.$row->family_num.' families from '.($countvillage > 0?$countvillage:'1').' villages</label> 
	           				</td>
	           			</tr>
	           			<tr>
	           				<td>
	           					<table class="table table-striped">'.$this->familyinvillage($row->commune).'</table>
	           				</td>
	           			</tr>';
	           		$i++;
				}
				return $tr;
			}else
			{
				return NULL;
			}
		}
		public function summarystatfamloc()
		{
			$resfl = $this->db->query($this->sqlcountcommunce());
			$i=1;
			$tr="";
  			foreach ($resfl->result() as $rowssfl) {
  				$countvillage = $this->countvillage($rowssfl->commune);
  				$tr.="<tr>
						<td>".$i."</td>
                  		<td>".($rowssfl->commune)."</td>
                  		<td align='right'>".($countvillage > 0?$countvillage:'1')." Villages</td>
                  		<td align='right'>".($rowssfl->family_num)." Families</td>
  					</tr>";
  				$i++;
  			}
  			return $tr;
		}
		public function familyinvillage($commune)
		{
			$sql = "SELECT DISTINCT
							f.village,
							count(f.family_code) AS family_num
						FROM
							sch_family f
						WHERE
							f.commune = '".trim($commune)."'
						AND f.is_active = '1'
						GROUP BY
							f.village
					";	
			$refv = $this->db->query($sql)->result();
			if(count($refv) > 0){
					$trv ="";
					$displayvalligename = "";
				foreach($refv as $rowfv){
					$villagename = $rowfv->village;
					if($villagename == '0' || $villagename == '') $displayvalligename="None"; else $displayvalligename=$villagename;
					$trv.='<tr>
								<td style="padding-left:15px;" width="60%">- '.$displayvalligename.' village </td>
								<td> &nbsp;:&nbsp;'.$rowfv->family_num.' families</td>
		           			</tr>';
				}
					return $trv;
			}else{
				return NULL;
			}	
		}
		
		// end tha
		
	function getmedicinebydate($s_year,$e_year){
		return $this->db->query("SELECT
									ss.descr_eng,
									sum(sssm.quantity) AS qty,
									smt.yearid,
									sssm.whcode
								FROM
									sch_medi_treatment smt
								LEFT JOIN sch_medi_treatment_medicine smtm ON smt.treatmentid = smtm.treatmentid
								LEFT JOIN sch_stock ss ON smtm.stockid = ss.stockid
								LEFT JOIN sch_stock_stockmove sssm ON ss.stockid = sssm.stockid
								WHERE
									sssm.quantity < 0 AND smt.yearid='$yearid'
								GROUP BY ss.descr_eng")->result();
	}
	function getmedicine($yearid){
		return $this->db->query("SELECT
								treat.yearid,
								mov.whcode,
								ssc.description,
								wh.wharehouse,
								mov.stockid,
								stk.descr_eng,
								ABS(SUM(mov.quantity)) AS qty,
								stk.uom
								FROM
									sch_stock_stockmove mov
								INNER JOIN sch_medi_treatment treat ON treat.transno = mov.transno
								INNER JOIN sch_stock_wharehouse wh ON wh.whcode = mov.whcode
								INNER JOIN sch_stock stk ON stk.stockid = mov.stockid
								INNER JOIN sch_stock_category ssc ON stk.categoryid=ssc.categoryid
								WHERE
								mov.quantity < 0 AND treat.yearid='$yearid'
										GROUP BY
								mov.whcode,
								mov.stockid
								ORDER BY qty DESC")->result();
	}
	function searchhealthdetail($sort_num,$location,$category,$medicine,$yearid,$s_date,$e_date){
			$page=0;
			if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
			$where = "";
			if($yearid !=""){
				$where .="AND treat.yearid LIKE '%$yearid%'";
			}
			if($location !=""){
				$where .="AND wh.wharehouse LIKE '%$location%'";
			}
			if($category !=""){
				$where .="AND ssc.description LIKE '%$category%'";
			}
			if($medicine !=""){
				$where .="AND stk.descr_eng LIKE '%$medicine%'";
			}

			if($s_date !=""){
				$where .="AND treat.date >= '".date_format(date_create($s_date), 'Y-m-d')."'";
			}
			if($e_date !=""){
				$where .="AND treat.date <= '".date_format(date_create($e_date), 'Y-m-d')."'";
			}
			
			$sql="SELECT
				treat.yearid,
				mov.whcode,
				ssc.description,
				wh.wharehouse,
				mov.stockid,
				stk.descr_eng,
				ABS(SUM(mov.quantity)) AS qty,
				stk.uom
				FROM
					sch_stock_stockmove mov
				INNER JOIN sch_medi_treatment treat ON treat.transno = mov.transno
				INNER JOIN sch_stock_wharehouse wh ON wh.whcode = mov.whcode
				INNER JOIN sch_stock stk ON stk.stockid = mov.stockid
				INNER JOIN sch_stock_category ssc ON stk.categoryid=ssc.categoryid
				WHERE
				mov.quantity < 0 {$where}
						GROUP BY
				mov.whcode,
				mov.stockid
				ORDER BY qty DESC";

			$this->load->library('pagination');
			$config['base_url'] =site_url()."/system/dashboard/search_health_detail?sort_num=$sort_num&location=$location&category=$category&medicine=$medicine&year=$yearid&s_date=$s_date&e_date=$e_date";	
			$config['total_rows'] = $this->green->getTotalRow($sql);
			$config['per_page'] =$sort_num;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';
			$this->pagination->initialize($config);
			$limi=" limit ".$config['per_page'];
			if($page>0){
				$limi=" limit ".$page.",".$config['per_page'];
			}
			$sql.=" {$limi} ";
			return $this->db->query($sql)->result();
			/* test */
	}		
}	