<style type="text/css"> 
 table tbody tr td img{
 	width: 20px;	
 }
 table th{
 	font-weight: normal;
 	
 }
.noborder{
	border: 0px !important;
}
.tr{
	border:1px solid black !important;
}

	  a{
	    cursor: pointer;
	  }
	  a:hover{
	  	text-decoration: none;
	  }
  	#myModal3 {
		top:15%;
		left:50%;
		outline: none;
	}
	 .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background-color: #337ab7 !important;
		border-color: #2e6da4 !important;
        cursor: inherit;
        display: block;
    }
    .require_input{
    	border:1px dashed #f00 !important;
    }
    </style>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info col-xs-10" style='width:100%'>
		      	<div class="col-xs-6">
		      		<strong>Message Description</strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right";>
		      	<span class="top_action_button">
		      		<span class='error' style='color:red;'></span>
		      	</span>
		      		Reply:
		      		<a href="#myModal3" data-toggle="modal" id="iconreply" title="Reply">
					    <img width='20px' height='20px'  src="<?php echo base_url('assets/images/icons/reply.png')?>" />
					</a>
		      	</div> 			      
		  </div>
		</div>
	</div>
</div>
<!-- <div class="row"> -->
 <div class="col-sm-12">  
 	<div class="panel panel-default">  	
	  <div class="panel-body" >       		 
		<div class="table-responsive" id="tab_print" >
			<!-------- Header ------------>		
			<table border="0"​ align="center" id='pre' class="table">
			    <thead>
			    <?php if($query->reply_to!=""){?>
			    	<th><strong><?php echo $query->reply_to.': '.$query->subject; ?></strong></th>
			    <?php }else{?>
			    	<th><strong><?php echo $query->subject; ?></strong></th>
			   <?php }?>
			    </thead>
			    <tbody>
			    	<tr >
			    		<td class="noborder">
			    		<?php if($query->reply_to!=""){?>
			    			<strong>Reply From : </strong>
			    			<?php echo $this->sms->getFullName($query->sent_from); ?>&nbsp;
			    			<?php 
			    			if($this->sms->getEmail($query->sent_from)!="")
			    			{
								echo '< '.$this->sms->getEmail($query->sent_from).' >';
			    			}
			    			?>
			    		<?php }else{?>
			    			<strong>Sent From : </strong>
			    			<?php echo $this->sms->getFullName($query->sent_from); ?>&nbsp;
			    			<?php 
			    			if($this->sms->getEmail($query->sent_from)!="")
			    			{
								echo '< '.$this->sms->getEmail($query->sent_from).' >';
			    			}
			    			?>
			    		<?php } ?>
			    		</td>
			    	</tr>
			    	<tr >
			    		<td class="noborder">
				    		<strong>Sent To : </strong>
				    		<?php echo $this->sms->getFullName($query->sent_to); ?>&nbsp;
				    		<?php
				    			if($this->sms->getEmail($query->sent_to)!=""){ 
				    				echo '< '.$this->sms->getEmail($query->sent_to).' >';
				    			}
				    		?> 
			    		</td>
			    	</tr>
			    	<?php if($query->reply_to!=""){?>
			    	<tr style="display:none">
			    		<td class="noborder">
			    		<strong>Reply To : </strong>
			    		<?php echo $this->sms->getFullName($query->sent_to); ?>&nbsp;
			    		<?php
			    			if($this->sms->getEmail($query->sent_to)!=""){ 
			    				echo '< '.$this->sms->getEmail($query->sent_to).' >';
			    			}
			    		?> 
			    		</td>
			    	</tr>
			    	<?php } ?>
			    	<?php if($query->cc_to!=""){?>
			    	<tr >
			    		<td class="noborder"><strong>CC : </strong><?php echo $query->cc_to; ?></td>
			    	</tr>
			    	<?php } ?>
			    	<?php if($query->bcc_to!=""){?>
			    	<tr >
			    		<td class="noborder"><strong>BCC : </strong><?php echo $query->bcc_to; ?></td>
			    	</tr>
			    	<?php } ?>
			    	<tr >
			    		<td class="noborder"><strong>Date : </strong><?php echo date('d/m/Y h:i A', strtotime($query->sent_date)); ?></td>
			    	</tr>
			    	<tr>
			    		<table border="1" width="100%"  style="padding:10px;margin:0px;height:400px;border:1px solid #ccc;">
			    			<tr ><strong>Description</strong>
			    				<td style="padding:10px;vertical-align: top;">
									<?php echo $query->description; ?>
								</td>
			    			</tr>
			    		</table>
			    	</tr>
			    </tbody>
			</table>			
		</div>
  	 </div>
  	</div>
  	<!-- start block dialog -->
      	<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="false">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title">Reply Message</h3>
					</div>
					<div class="modal-body">
						<!-- start from  -->
						<form class="form-horizontal" role="form" method="post" action="index.php" enctype="multipart/form-data" id='myForm'>
							<div class="form-group">
								<label for="emailto" class="col-sm-2 control-label">To *</label>
								<div class="col-sm-10">
									<input type="hidden" value="<?php echo $query->sent_from; ?>" class="form-control emailto" id="emailto"  name="emailto" placeholder="To"/>
									<input type="hidden" value="<?php echo $query->sent_to; ?>" class="form-control emailreplyto" id="emailreplyto"  name="emailreplyto" placeholder="To"/>
									<input type="text" value="<?php echo $this->sms->getFullName($query->sent_from);?>" class="form-control emailtoval" id="emailtoval" name="emailtoval" placeholder="To"/>
								</div>
							</div>
							<div class="form-group" style="display:none">
								<label for="emailcc" class="col-sm-2 control-label">Cc</label>
								<div class="col-sm-10">								 
										<select class="form-control emailcc" style="width:100% " id="emailcc" name="emailcc" multiple="multiple">
									</select>
									<input type="hidden" id="ecchide" class="ecchide">
								</div>
							</div>
							<div class="form-group">
								<label for="name" class="col-sm-2 control-label">Subject</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject to reply...!"/>
									<input type="hidden" id="replysms" value="Re">
								</div>
							</div>
							<div class="form-group">
								<label for="name" class="col-sm-2 control-label">Message</label>
								<div class="col-sm-10">
									 <textarea name="txtarmessage" id="txtarmessage" class="form-control txtarmessage" style="resize:none" maxlength="999" data-validation-minlength-message="Min 5 characters" minlength="5"  placeholder="Please enter a message."  cols="100" rows="15" aria-invalid="false"  required data-validation-required-message="Please enter a message."></textarea>
								</div>
							</div>
						</form>
						<!-- end form -->
					</div>
					<div class="modal-footer" style="display:none;"> 
						<button type="button" id="btnClose" class="btn btn-success" data-dismiss="modal">Close</button>
					</div>
					<div class="modal-footer" style="margin-top:0;">
					    <div style="float:right;color:#737373;font-style:italic; text-align:left;display:none">
					    	<strong>Attach files:</strong> - 
				    	    <span class="btn btn-default btn-file btn-primary">
						        Browse <input type="file">
						    </span>
					    </div>
					    <div style="float:left">
					        <button type="button" class="btn btn-primary" id="btnSent" data-loading-text="Submitting...">Send</button>
					    </div>
					</div>
				</div> 
			</div> 
		</div> 
	<!-- end block dialog -->
  </div>
<script type="text/javascript">
	$(function(){
		$('.txtarmessage').summernote({
	      height: 280.617,                 // set editor height
	      minHeight: null,             // set minimum height of editor
	      maxHeight: null,             // set maximum height of editor
	      focus: true,                 // set focus to editable area after initializing summernote    
	    });
	    $("#btnSent").on("click",function(){	
	    	saveData();
	    });
	});
	var showsms = function(sms=''){
		if(sms!=""){
			$('.error').html(sms).slideDown(1000).delay(3000).slideUp(500);
		}
	}
	var saveData = function(){
		var eto 	= $("#emailtoval").val();
		var subject = $("#subject").val();
		var replysms = $("#replysms").val();
		var emailreplyto = $("#emailreplyto").val();
		if(eto == "")//emailtoval
		{
			$("#emailtoval").addClass('require_input');
			$("#subject").removeClass( "require_input" );
			return false;
		}
		else if(subject =="")
		{
			$("#emailtoval").removeClass( "require_input" );
			$("#subject").addClass('require_input');
			return false;
		}
		else
		{
			$.ajax({
				url 	:"<?php echo site_url('message/message/saveReplyData')?>",
				type 	:"POST",
				dataType:"json",
				async 	:false,
				data 	:{
					emailto 	: $("#emailto").val(),//require_input
					emailcc 	: $(".emailcc").val(),
					subject 	: subject,
					message 	: $("#txtarmessage").code(),
					replysms 	: replysms,
					emailreplyto: emailreplyto
				},
				success:function(rs){
					if(rs.result==true)
					{
						showsms('Your message has been sent.');
						$('#btnClose').click();
						location.reload();
					}
					
				}
			});
		}
	}
</script>