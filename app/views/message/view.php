<style type="text/css"> 
	  a{
	    cursor: pointer;
	  }
	  a:hover{
	  	text-decoration: none;
	  }
  	#myModal3 {
		top:15%;
		left:50%;
		outline: none;
	}
	 .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background-color: #337ab7 !important;
		border-color: #2e6da4 !important;
        cursor: inherit;
        display: block;
    }
    .require_input{
    	border:1px dashed #f00 !important;
    }
  </style>
<div class="result_info">
        <div class="col-sm-6">
          <strong>Receive Message</strong>  
        </div>
        <div class="col-sm-6" style="text-align:right;">
        	<span class="top_action_button">
	      		<span class='error' style='color:red;'></span>
	      	</span>
        	<?php //if($this->green->gAction("D")){ ?>
	        	<a href="#" id="export" title="Export">
				    <img onclick='deleteall(event);' width='20px' height='20px' src="<?php echo base_url('assets/images/icons/deleteall.png')?>" />
				</a>
			<?php //} ?>
			<?php //if($this->green->gAction("C")){ ?>
				<a href="#myModal3" data-toggle="modal" id="iconadd" title="Send">
				    <img  src="<?php echo base_url('assets/images/icons/add.png')?>" />
				</a>
			<?php //} ?>
        </div> 
      </div> 
<!-- <div class="row"> -->          
 <div class="col-sm-12">    
 	<div class="panel panel-default">
 		  	
	  <div class="panel-body">	          		 
		<div class="table-responsive">	
			
			<table border="0"​ align="center" id='listsubject' class="table table-bordered">
			    <thead>
			    	<th width="10"></th>
			        <th align="center" width="40">No</th>
			        <th width="170">Use Name</th>
			        <th width="130">Sent From</th>
			        <th width="130">Sent To</th>
			        <th width="130">Subject</th>
			        <th width="200">Sent Date</th>
			        <th width="70">Action</th>
			    </thead>
			    <tr>
			        <td><input type='checkbox' id='checkall' name='checkall'/></td>
			        <td></td>
			        <td><input type="text" class="form-control input-sm" name="txtsearchuname" id="txtsearchuname" onkeyup='search(event);' value='<?php if(isset($_GET['un'])) echo $_GET['un']; ?>' class="form-control" /></td>
			        <td><input type="text" class="form-control input-sm" name="txtsearchsfrom" id="txtsearchsfrom" onkeyup='search(event);' value='<?php if(isset($_GET['sf'])) echo $_GET['sf']; ?>' class="form-control" /></td>
			        <td></td>
			        <td></td>
			        <td>
			        	<div class='input-group date' id='datetimepicker'>
                     	<input type='text' id="txtdate" class="form-control asign-color hasDatepicker txtdate" name="txtdate" onkeyup='search(event);' onSelect='search(event);' class="sdate"/>
                     	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></td>
			        <td></td>
			    </tr>
			    <tbody id='bodylist'><?php echo $this->sms->loadingHtmlData($query); ?></tbody>
			</table>	
		</div>
  	 </div>
  	</div>
  	<!-- start block dialog -->
      	<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="false">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title"><?php echo $page_header?></h3>
					</div>
					<div class="modal-body">
						<!-- start from  -->
						<form class="form-horizontal" role="form" method="post" action="index.php" enctype="multipart/form-data" id='myForm'>
							<div class="form-group">
								<label for="emailto" class="col-sm-2 control-label">To *</label>
								<div class="col-sm-10">
									<input type="hidden" class="form-control emailto" id="emailto" name="emailto" placeholder="To"/>
									<input type="text" class="form-control emailtoval" id="emailtoval" name="emailtoval" placeholder="To"/>
								</div>
							</div>
							<div class="form-group" style="display:none">
								<label for="emailcc" class="col-sm-2 control-label">Cc</label>
								<div class="col-sm-10">								 
										<select class="form-control emailcc" style="width:100% " id="emailcc" name="emailcc" multiple="multiple">
									</select>
									<input type="hidden" id="ecchide" class="ecchide">
								</div>
							</div>
							<div class="form-group">
								<label for="name" class="col-sm-2 control-label">Subject</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject"/>
								</div>
							</div>
							<div class="form-group">
								<label for="name" class="col-sm-2 control-label">Message</label>
								<div class="col-sm-10">
									 <textarea name="txtarmessage" id="txtarmessage" class="form-control txtarmessage" style="resize:none" maxlength="999" data-validation-minlength-message="Min 5 characters" minlength="5"  placeholder="Please enter a message."  cols="100" rows="15" aria-invalid="false"  required data-validation-required-message="Please enter a message."></textarea>
								</div>
							</div>
						</form>
						<!-- end form -->
					</div>
					<div class="modal-footer" style="display:none;"> 
						<button type="button" id="btnClose" class="btn btn-success" data-dismiss="modal">Close</button>
					</div>
					<div class="modal-footer" style="margin-top:0;">
					    <div style="float:right;color:#737373;font-style:italic; text-align:left;display:none">
					    	<strong>Attach files:</strong> - 
				    	    <span class="btn btn-default btn-file btn-primary">
						        Browse <input type="file">
						    </span>
					    </div>
					    <div style="float:left">
					        <button type="button" class="btn btn-primary" id="btnSent" data-loading-text="Submitting...">Send</button>
					    </div>
					</div>
				</div> 
			</div> 
		</div> 
	<!-- end block dialog -->
  </div>
<!-- </div> -->
<script type="text/javascript">
	$(function() {
        showcalendar();
        autoemail();
		$('.txtarmessage').summernote({
	      height: 280.617,                 // set editor height
	      minHeight: null,             // set minimum height of editor
	      maxHeight: null,             // set maximum height of editor
	      focus: true,                 // set focus to editable area after initializing summernote    
	    });
	    $("#emailtoval").on("change",function(){
	    	autoemail();
	    });

	    $("#btnSent").on("click",function(){	
	    	saveData();
	    });
	    $("#iconadd").on("click",function(){
	    	clearobj();
	    });
	    $('#checkall').click(function(event) {
		  if(this.checked) {
		      $(':checkbox').each(function() {
		          this.checked = true;
		      });
		  } else {
		    $(':checkbox').each(function() {
		          this.checked = false;
		      });
		  }
		});
    });
	var showcalendar = function()
	{ 
    	$(".txtdate").datepicker({
                format: "dd-mm-yyyy"
            }).on('changeDate', function(ev){
                var dateData = $('.txtdate').val();
               search(dateData)
            });
	}
	function preview(event){
    	var year=jQuery('#year').val();
    	window.open("<?php echo site_url('message/message/preview');?>/0/"+year,"_blank");
    }

	function deleteall(event){
		var r = confirm("Are you sure to delete all this item?");
		if (r==true){
			$(".selected:checked").each(function(){
				var smsid=$(this).attr("rel");
				 $.ajax({
			           url:"<?PHP echo site_url('message/message/deleteall/');?>/"+smsid,    
			           data: {},
			           type: "POST",
			           success: function(data){ 
			           		showsms('Your message has been deleted all.'); 
			           	  location.href="<?PHP echo site_url('message/message');?>";
			           }
		         });
			});
			
		}
	}

    function deletes(event){
           var r = confirm("Are you sure to delete this item?");
           if (r == true) {
               var id=jQuery(event.target).attr("rel");
               showsms('Your message has been deleted.'); 
               location.href="<?PHP echo site_url('message/message/deletes/');?>/"+id;
           } else {
               txt = "You pressed Cancel!";
           }      
    }
    function search(event,sldate=''){
        var uname=jQuery('#txtsearchuname').val(); 
        var sfrom=jQuery('#txtsearchsfrom').val();
        if(sldate==''){
			sldate =jQuery('#txtdate').val();
        }else{
        	sldate=sldate;
        }
  		
        $.ajax({
           url:"<?php echo base_url(); ?>index.php/message/message/searchs",
           data: {
                  'uname':uname,
                  'sfrom':sfrom,
                  'sdate':sldate
                 },
           type: "POST",
           success: function(data){
              jQuery('#bodylist').html(data);                   
           }
         });
     } 
   	var clearobj = function(){
		$("#emailto").val('');
		$("#emailtoval").val('');
		$("#emailcc").val('');
		$("#subject").val('');
		$("#message").code('');
		$("#emailtoval").removeClass( "require_input" );
		$("#subject").removeClass( "require_input" );
	}

	var autoemail = function(){
		$("#emailtoval").autocomplete({
			source:"<?php echo site_url('message/message/getautoemail')?>",
			minLenght:0,
			select:function(events, ui){
				var username = ui.item.username;
				$('#emailto').val(username);
			}
		});
	}
	var showsms = function(sms=''){
		if(sms!=""){
			$('.error').html(sms).slideDown(1000).delay(3000).slideUp(500);
		}
	}
	var saveData = function(){
		var eto 	= $("#emailtoval").val();
		var subject = $("#subject").val();
		if(eto == "")//emailtoval
		{
			$("#emailtoval").addClass('require_input');
			$("#subject").removeClass( "require_input" );
			return false;
		}
		else if(subject =="")
		{
			$("#emailtoval").removeClass( "require_input" );
			$("#subject").addClass('require_input');
			return false;
		}
		else
		{
			$.ajax({
				url:"<?php echo site_url('message/message/savedata')?>",
				type:"POST",
				dataType:"json",
				async:false,
				data:{
					emailto:$("#emailto").val(),//require_input
					emailcc:$(".emailcc").val(),
					subject:subject,
					message:$("#txtarmessage").code(),
					replysms 	: '',
					emailreplyto: ''
				},
				success:function(rs){
					if(rs.result==true)
					{
						showsms('Your message has been sent.');
						$('#btnClose').click();
						location.href="<?PHP echo site_url('message/message');?>";	
					}
					
				}
			});
		}
	}
</script>