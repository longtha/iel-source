<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }

 ?>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
    <div class="col-sm-12">
        <div class="row result_info">
            <div class="col-sm-6 "><strong><?php echo $title?></strong></div>
        </div>
        <form enctype="multipart/form-data" id="fschyear">
         <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6" style="padding-top:23px;">
                    <label class="control-label"><span>Evaluation Group</span></label>
                    <select type="text" name="decritption_eva" id="decritption_eva" class="form-control input-xs" required data-parsley-required-message="Enter name"></select> 
                    <input type="hidden" name="idhidden" id="idhidden" class="form-control input-xs"/>
                </div>
                <div class="col-sm-6" style="padding-top:23px;">
                    <label class="control-label"><span>Area</span></label>
                    <input type="text" name="area_eva" id="area_eva" class="form-control input-xs"/>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-6" style="padding-top:23px;">
                    <label class="control-label"><span>Area (KH)</span></label>
                    <input type="text" name="area_kh" id="area_kh" class="form-control input-xs"/>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="col-sm-12" style="padding-top:2px;">
         <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <label class="control-label"></label>
                    <?php if($this->green->gAction("C")){ ?>
                    <input type="button" id="save" value="Save" class="btn btn-primary"/>
                    <?php } ?>
                    <input type="button" id="clear" value="Clear" class="btn btn-warning"/>
                </div>
           </div> 
        </div>
        <div class="row result_info">
            <div class="col-sm-6"><strong>List of Evaluation Key</strong></div>
        </div>
        <!-- show header -->
        <div class="col-sm-12">
            <meta charset="Utf-8">
             <div class="table-responsive" id="dv-print">
                <table class="table table-condensed panel-body">
                    <thead>
                        <tr>
                            <th width="5%">N&deg;</th>
                            <th width="40%">Evaluation Group</th>
                            <th width="30%">Area</th>
                            <th style="text-align:center;" colspan="2" width="5%">Action</th>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2">
                                <input type="text" id="sech_evaluation_group" placeholder="Sech Evaluation Group" class="form-control input-xs" style="width:270px;"/>
                            </td>
                        </tr>
                    </thead>
                    <tbody id="show_tbl" class="panel-body"></tbody>
                </table>
            </div>
        </div>
    </div>
<!-- dialog delete -->
<div class="modal" id="myModal_delete" role="dialog" style="top:8%">
        <div class="modal-dialog" style="width:300px">
            <div class="modal-content">
                <div class="modal-header" style="text-align:left; font-size:14px; text-align:center; color:red;">Do you want to delete ? </div>
                <div class="modal-footer">
                    <button type="button" id="delete" data-dismiss="modal" class="btn btn-danger">Delete</button>
                    <button type="button" class=" btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- dialog save -->
    <div class="modal fade" id="myModal_msg">
    <div class="modal-dialog" style="width:400px">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="modal-title" id="title_msg" style="text-align:center;"></h5>
            </div>
            <div class="modal-footer" style="display:none" id="div_btn">
                <button type="button" class=" btn btn-danger" id="close_msg" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(function() {
        $('#fschyear').parsley();  
        cleadata();
        showdata(); 
        get_description(); 
        $("#clear").on("click",function(){
            cleadata(); 
            //$("#save").val('Save').show();
            //$("#save").val('Update').hide();
        });
        $("#sech_evaluation_group").on('keyup',function(){
            showdata();
        });
        // save =========================================================
        $("#save").on("click",function(){

            var idhidden=$('#idhidden').val();
            var decritption_eva = $('#decritption_eva option:selected');
            if(decritption_eva.val() ==0){
                $('#decritption_eva').css('border-color', 'red');
            }else{
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('employee/c_group_evaluation/save');?>",
                    data : {
                        idhidden        : $("#idhidden").val(),
                        decritption_eva : $("#decritption_eva").val(),
                        area_eva        : $("#area_eva").val(),
                        area_kh        : $("#area_kh").val(),
                        comments_eva    : $("#comments_eva").val()
                    },
                    success:function(data){
                        $('#myModal').modal('hide');
                        $('#div_btn').hide();
                        $('#myModal_msg').modal({show: true});
                        $('#title_msg').html('Save successfull...');
                        $('#title_msg').css('color','blue');
                        setTimeout(function (){ $('#myModal_msg').modal('hide'); }, 1500);
                        showdata();
                        cleadata();
                        $("#save").val('Save');
                    }
                });
                $('#decritption_eva').css('border-color', '#ccc');
            }  
        });
    }); //function
        // delete========================================================
        $(document).on('click','.delete_data',function(){
            var deletedata = $(this).attr('delete'); 
            $("#myModal_delete").modal();
            $("#delete").click(function(){
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('employee/c_group_evaluation/deletedata');?>",
                    data : { 
                        deletedata : deletedata
                    },
                    success : function(data){ 
                         cleadata();
                         showdata();
                    }
                });
             });
        });

        //  edit ======================================================
        $(document).on('click','.edit_data',function(){
            var idhidden = $(this).attr("edit");
            $.ajax({ 
                type : "POST",
                url  : "<?= site_url('employee/c_group_evaluation/editdata');?>",
                data : { 
                    idhidden : idhidden 
                },
                success:function(data){
                  var post = data.split('|');
                    $("#idhidden").val(post[0]);
                    $("#decritption_eva").val(post[1]);
                    $("#area_eva").val(post[2]);
                    $("#area_kh").val(post[3]);
                   $("#save").val('Update');
                }
            }); 
        });
        
        function get_description(){
            $.ajax({ 
                type : "POST",
                url  : "<?= site_url('employee/c_group_evaluation/get_description') ?>",
                dataType: "html",
                data : {
                    
                },
                success:function(data){
                    $("#decritption_eva").html(data);
                },
                error: function(){

                }
            });
        }

        function showdata(){
            var sech_evaluation_group = $("#sech_evaluation_group").val().toString();
            var roleid=<?php echo $this->session->userdata('roleid');?>;
            var m=<?php echo $m;?>;
            var p=<?php echo $p;?>;
            $.ajax({ 
                type : "POST",
                url  : "<?= site_url('employee/c_group_evaluation/showdata');?>",
                data : {
                    showdata : 1,
                    sech_evaluation_group : sech_evaluation_group,
                    roleid:roleid,
                    m:m,
                    p:p

                },
                success:function(data){
                    $("#show_tbl").html(data);
                }
            });
        }
        function cleadata(){
            $("#idhidden").val("");
            $("#decritption_eva").val(0);// $('#decritption_eva option:selected')
            $("#area_eva").val("");
            $("#area_kh").val("");
            $("#comments_eva").val("");
        }
</script>