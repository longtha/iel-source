

<div class='col-xs-13'>
	<div class="wrapper">
		<div class="clearfix" id="main_content_outer">
		    <div id="main_content">
		      <div class="row result_info col-xs-12">
			      	<div class="col-xs-5">
			      		<strong><?php echo $page_header;?></strong>
			      	</div>
			      	<div class="col-xs-7" align="right">
			      	<table border="0">
			      		<tr>
			      			<td>
			      				<a href="#" id="print" title="Print">
					    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
					    		</a>
					    		&nbsp;
			      			</td>
			      			<td style="padding-left:3px;">
			      				<button class="btn btn-primary"  data-toggle="modal" data-target="#myModal" style="height:31px; margin-top:-3px;">
			      					<i class="glyphicon glyphicon-shopping-cart"></i> Name 
			      				</button>
			      			</td>
			      			<td style="padding-left:2px;">&nbsp;&nbsp;Department :&nbsp;&nbsp;</td>
			      			<td>
			      				<select onchange='loadempcard();' class='form-control input-sm' id='emp_department' name='emp_department'>
									<?php
										$option ="<option value='-1' selected>Select Department</option>";
										foreach( $this->db->get('sch_emp_department')->result() as $dep ){
											//$dep->dep_id==1?$option .='<option value="'.$dep->dep_id.'" selected>'.$dep->department.'</option>':$option .='<option value="'.$dep->dep_id.'">'.$dep->department.'</option>';
											$dep->dep_id==1?$option .='<option value="'.$dep->dep_id.'">'.$dep->department.'</option>':$option .='<option value="'.$dep->dep_id.'">'.$dep->department.'</option>';
										}
										echo $option;
									 ?>
								</select>
			      			</td>
			      			
			      		</tr>
			      	</table>
			      	</div> 		      
			  </div>
			</div>
		</div>
	</div>
	<div class="wrapper col-sm-12">

	 <!-- <div class="row "> -->
		<div class="col-sm-12">
		    <div class="panel panel-default">
		      	<div class="panel-body">
			         <div class="table-responsive">
			    			<div id="tab_print" style="background-color:#fff;border:0px solid #f00;width:758px;overflow:hidden;margin:0 auto;">
							<style type="text/css">
								.labelname { 
									font-family: "TimesNewRoman","Times New Roman";;
									font-size:24px !important;
									color:#4BBABC;
									
								}
								.shwo_labelname {
								font-family: "TimesNewRoman","Times New Roman";; 
									font-size:26px !important;
									text-transform: uppercase;
									color:#4BBABC;
									font-weight:bold !important;

								}
								.css_pos_dep { 
									font-family: "TimesNewRoman","Times New Roman";;
									font-size:26px !important;
									color:#4BBABC;
									font-weight:bold !important;
								}
								.css_phone{
									font-family: "TimesNewRoman","Times New Roman";;
									font-size:26px !important;
									color:#4BBABC;									
									letter-spacing: 2px;
								}
								.css_address { 
									font-family: "TimesNewRoman","Times New Roman";;
									font-style: italic;
									font-variant: normal;
									font-size:20px !important;
									color:#4BBABC;
								}
								

									@media (min-height: 40em) {
									  .modal {
									    position: fixed;
									  }
									  .modal-body {
									    max-height: 35em; /* This should be less than the min-height in the media query. */
									    overflow-y: auto;
									  }
									}
									
								label{
									font-weight:normal !important;
									margin-bottom:0 !important;
									line-height:1.2em !important;
								}
								.ui-autocomplete{z-index: 9999 !important;}
        						.datepicker {z-index: 9999 !important;}	
							</style>
							<!-- Start Student Badge List Script-->
							<table border="0"  id="tblempcard"></table>
							<!-- End Student Badge List Script-->

							<!-- Start Student Badge List PHP-->
							<table border="0"  id="tdata_emp_card" class="tdata_emp_card" style="width:620px;" align="center">
								
							</table>
							<!-- End Student Badge List PHP-->
							</div>	
						</div>
					</div>
				</div>
			</div>
		<!-- </div> -->
		<!-- <p style="page-break-after:always;"></p> -->
		<?php //} ?>

       <!-- end block dialog -->
       <!-- start new dialog  -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo $page_header;?></h4>
                    </div>
                    <div class="modal-body ">
                    	<div class="stdscrollbar">
	                        <form method="post" accept-charset="utf-8" class="frmaddrespond" id="frmaddrespond">
					      		<div class="row">          
						       		<div class="col-sm-12">                       	
						              	<div class="panel-body">
						              		<div class="form-group" id="error_text" style="display:none"></div>
											  <div class="form-group">
											    <input type="text" class="form-control searchempamulti" id="searchempamulti" name="searchdatamulti"  placeholder="Search here..!">
											 	
											  </div>
							                <table   class="table table-bordered" border="0" width="100%">
							                	<thead>
								                	<tr>
								                		<th style="width:1px"></th>
								                		<th>ID</th>
								                		<th>Name</th>
								                		<th>Position</th>
								                		<th>Mobile</th>
								                	</tr>
							                	</thead>
							                	<tbody id="tdata"></tbody>
							                </table> 
							            </div>
						            </div>   
						        </div>
							</form>
						</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="btnclose">Close</button>
                        <button type="button" class="btn btn-primary" id="addemptocard">Add</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end new dialog  -->
	</div>
</div>

	<script type="text/javascript">
		$(function(){
			loadempcard();
			autoempcard();
			$("#print").on("click",function(){
				gPrint("tab_print");
			});
			$("#searchempamulti").on("change",function(){
					autoempcard();	
			});
			$('#myModal').on('click',function(){
				$("#searchempamulti").val('');
				$('#error_text').hide();
			});
			$('#addemptocard').on("click",function(){
				var yearid=$('#year').val();
				var arrayidcard = Array();
				$(".chempcode:checked").each(function(i){
					var idcard = $(this).val();
					arrayidcard[i]={"idcard":idcard};
				});
				$.ajax({
					url:"<?php echo site_url('employee/empcard/addtoempcardlist')?>",
					type:"POST",
					async:false,
					datatype:"Json",
					data:{
						yearid:yearid,
						arrayidcard:arrayidcard
					},
					success:function(res){
						if(res.dropcardlist!="")
						{
							$("#tdata_emp_card").hide();
							$("#tblempcard").html(res.dropcardlist);
							$("#searchempamulti").val('');
							$('#btnclose').click();
						}

					}
				});
			});
		});
		// $("#year").change(function(){
		// 	loadempcard();
		// })
		var loadempcard = function()
		{
			var yearid=$('#year').val();
			var emp_department=$("#emp_department").val();
			$.ajax({
				url:"<?php echo site_url('employee/empcard/loadempcard')?>",
				type:"POST",
				datatype:"Json",
				async:false,
				data:{
					yearid:yearid,
					emp_department:emp_department
				},
				success:function(reemp){
					$("#tblempcard").html('');
					if(reemp.emp!=""){
						$("#tblempcard").html(reemp.emp);
					}
				}
			});
		}
		var loadempcardindialog = function()
		{
			var yearid=$('#year').val();
			var searchempamulti = $("#searchempamulti").val();
			$.ajax({
				url:"<?php echo site_url('employee/empcard/loadingempcardindialog')?>",
				type:"POST",
				datatype:"Json",
				async: false,
				data:{
					yearid:yearid,
					searchempamulti:searchempamulti
				},
				success:function(res){
					if(res.emplist !="")
					{
						$("#tdata").append(res.emplist);
					}
				}
			});
		}
		var autoempcard = function()
		{
			$('#searchempamulti').autocomplete({
				source:"<?php echo site_url('employee/empcard/autoempcard');?>",
				minLength:0,
				select:function(events,ui){
					var datamulties = ui.item.empid;
					var exist = false;
					$('.searchempamulti').val(datamulties);
					$('.chempcode:checked').each(function(){
						var empcheck = $(this).val();
						if(empcheck == datamulties){
							exist=true;
						}
					});
					if(exist==true){
						$('#error_text').show();
						$('#error_text').html('Data is already exist, Please try again...!');
					}else{
						loadempcardindialog();
					}	
				}

			});
		}
	</script>

