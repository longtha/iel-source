
<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }

 ?>
<style type="text/css">
	.show_tbl{
		display: inline !important;
	}
</style>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
<form enctype="multipart/form-data" id="fschyear">
    <div class="col-sm-12">
        <div class="row result_info">
            <div class="col-sm-6 "><strong><?php echo $title?></strong></div>
        </div>
         <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6" style="padding-top:25px;">
                    <label class="control-label"><span>From Date :</span></label>
                    <input type="date" id="fromdate" placeholder="Search From Date" class="form-control input-xs"/>
               		<input type="hidden" id="idhidden">
                </div>
                <div class="col-sm-6" style="padding-top:25px;">
                    <label class="control-label"><span>To Date :</span></label>
                    <input type="date" id="todate" placeholder="Search To Date" class="form-control input-xs"/>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-6" style="padding-top:25px;">
                    <label class="control-label"><span>Name :</span></label>
                    <input type="text" id="search_name" placeholder="Search Name"  class="form-control input-xs"/>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12" style="padding-top:25px;">
         <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <label class="control-label"></label>
                    <input type="button" id="search" value="Search"  class="btn btn-primary"/>
                    <!-- <input type="button" id="clear"  value="Clear"   class="btn btn-warning"/> -->
                </div>
           </div> 
        </div>
        <div class="row result_info">
            <div class="col-sm-6"><strong>List of Staff Evaluation Report</strong></div>
        </div>
        <!-- show header -->
        <div class="col-sm-12">
            <meta charset="Utf-8">
             <div class="table-responsive" id="dv-print" style="border:#0ff solid 0px;">
                <table border="0" class="table table-condensed">
                    <thead>
                        <tr>
                            <th width="5%">N&deg;</th>
                            <th width="25%">Name</th>
                            <th width="7%">Sex</th>
                            <th width="20%">Position</th>
                            <th width="25%">Department</th>
                            <th width="15%">Date</th>
                            <th width="10%">Print</th>
                            <th style="text-align:center;" colspan="2" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody id="show_datatable"></tbody>
                </table>
				<div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
			        Display : <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
        						<?php
            						$num=10;
            						for($i=0;$i<10;$i++){?>
            							<option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
            							<?php $num+=20;
            						}
        						?>
					         </select>
		        </div>
		        <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
					<div class="pagination" style="text-align:center"></div>
				</div>
            </div>
        </div>
    </div>
</form>
<!-- dialog delete -->
    <div class="modal" id="myModal_delete" role="dialog" style="top:8%">
        <div class="modal-dialog" style="width:300px">
            <div class="modal-content">
                <div class="modal-header" style="text-align:left; font-size:14px; text-align:center; color:red;">Do you want to delete ? </div>
                <div class="modal-footer">
                    <button type="button" id="delete" data-dismiss="modal" class="btn btn-danger">Delete</button>
                    <button type="button" class=" btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
   $(function() {
        getDataTable(1,$('#sort_num').val());
    	$("#todate").datepicker({format:'yyyy-mm-dd'});
    	$("#fromdate").datepicker({format:'yyyy-mm-dd'});
    	$("#search_name").on('keyup',function(){
			var total_display = $("#sort_num").val();
            getDataTable(1,total_display);
		});
        $("#search").on("click",function(){
        	var total_display = $("#sort_num").val();
            getDataTable(1,total_display);
        });
        $('#sort_num').change(function(){
            var total_display = $(this).val();
            getDataTable(1,total_display);
        });
    //deletedata ===============================================
    $(document).on('click','.delete_data',function(){
        var deletedata = $(this).attr('delete'); 
        $("#myModal_delete").modal();
        $("#delete").click(function(){
            $.ajax({ 
                type : "POST",
                url  : "<?= site_url('employee/c_emp_search_evaluation/deletedata');?>",
                data : { 
                    deletedata : deletedata
                },
                success : function(data){ 
                    getDataTable();
                }
            });
        });
    });
    $("body").delegate(".pagenav","click",function(){
        var page          = $(this).attr("id");
        var total_display = $('#sort_num').val();
        getDataTable(page,total_display);
    });

}); // class ====================================================
    function getDataTable(page,total_display){
	     	var fromdate = $('#fromdate').val();
	     	var todate = $('#todate').val();
	     	var search_name = $('#search_name').val().toString();
			var y     = "<?php echo $_GET['y'];?>";
			var m     = <?php echo $_GET['m'];?>;
			var p     = <?php echo $_GET['p'];?>;
            var per   = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
            var s_num = $('#sort_num').val();
            var roleid = <?php echo $this->session->userdata('roleid');?>;

			$.ajax({
				type : "POST",
				url  : "<?php echo site_url('employee/c_emp_search_evaluation/setShowdata')?>",
				datatype:"Json",
				async: false,
				data : {
					y:y,
					m:m,
					p:p,
					page:page,
					fromdate:fromdate,
					todate:todate,
					'search_name':search_name,
					total_display: total_display,
                    roleid:roleid,
                    s_num:s_num,
                    p_page:per
				},
				success:function(data){
					$("#show_datatable").html(data['body']);
					$(".pagination").html(data['pagination']['pagination']);

				}
			});
		}  
        
</script>
