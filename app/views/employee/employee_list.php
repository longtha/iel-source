<?php
	$m='';
	$p='';
	if(isset($_GET['m'])){
	    $m=$_GET['m'];
	}
	if(isset($_GET['p'])){
	    $p=$_GET['p'];
	}
 ?>
<?php 
	$tr = ""; $tr_search=""; $num_emp = 1;
	if(count($tdata)>0){  
		// $i=1;
		// if(isset($_GET['per_page']))
		// 	$i=$_GET['per_page']+1;
		// echo $i;
		//exit();
		$tr_search .= '<tr class="remove_tag">
						<td class="col-xs-1">&nbsp;</td>
						<td class="col-xs-1"><input type="text" name="sch_empId" id="sch_empId" class="form-control" onkeyup="search();"></td>
						<td class="col-xs-2"><input type="text" name="sch_fullname" id="sch_fullname" class="form-control" onkeyup="search();"></td>
						<td class="col-xs-2"><input type="text" name="sch_fullname_kh" id="sch_fullname_kh" class="form-control" onkeyup="search();"></td>
						<td class="col-xs-2"><select name="s_status" id="s_status" class="form-control col-xs-1"><option value=1>Active Staff</option><option value=0>Resigned Staff</option></select></td>
						<td class="col-xs-2"><input type="text" name="pos_id" id="pos_id" class="form-control" onkeyup="search();"></td>
						<td colspan="3"></td>
					</tr>';
		$i=1;
		if(isset($_GET['per_page']))
			$i=$_GET['per_page']+1;
		foreach($tdata as $row_emp){

			if($row_emp['dob'] =="0000-00-00"){
				$date = "";
			}else{
				$cdate = date_create($row_emp['dob']);
				$date = date_format($cdate,"d/m/Y");
			}
			$tr .='<tr class="no_data" rel="1">
						<td class="pos_1">'.str_pad($i,2,"0",STR_PAD_LEFT).'</td>
						<td class="pos_2">
							<a target="_blank" href="'.site_url("employee/contract/?m=$m&p=$p&emp_id=".$row_emp['empid']).'">'.$row_emp['empcode'].'</a>
						</td>
						<td class="pos_3">'.($row_emp['last_name']." ".$row_emp['first_name']).'</td>
						<td class="pos_4">'.($row_emp['last_name_kh']." ".$row_emp['first_name_kh']).'</td>
			       		<td class="pos_5">'.$date.'</td>
			       		<td class="pos_6"><a target="_blank" href="'.site_url("employee/position/?m=$m&p=$p").'">'.$row_emp['position'].'</a></td>
			       		<td width="1%" class="remove_tag">';
			       		if($this->green->gAction("P")){
							$tr .= '<a target="_blank" title="View Employee">
										<img rel="'.$row_emp['empid'].'" src="'.site_url('../assets/images/icons/preview.png').'" onclick="view_employee(event);" style="width:20px;height:20px;">
									</a>';
							}
						$tr .= '
						</td>
						<td width="1%" class="remove_tag">';
						if($this->green->gAction("D")){
							$tr .= '<a title="Delete Employee">
										<img class="clk_del" rel="'.$row_emp['empid'].'" onclick="delete_employee(event);" src="'.site_url('../assets/images/icons/delete.png').'" style="width:20px;height:20px;">
								    </a>';
						}
						$tr .= '</td>
						<td width="1%" class="remove_tag">';
						if($this->green->gAction("U")){
							$tr .= '<a title="Edit Employee">
										<img rel="'.$row_emp['empid'].'" src="'.site_url('../assets/images/icons/edit.png').'" onclick="edit_employee(event);" style="width:20px;height:20px;">
									</a>';
						}
						$tr .= '</td>
			       </tr>';
			       $i++;
		}
	}else{
		$tr.="<tr class='no_data' rel='0' style='text-align:center;background:#FFFFFF;border-bottom:solid 2px #EEEEEE;'><td colspan='10'><b>No Employee</b></td></tr>";
	}
?>
<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	.table th td {border:1px solid gray;}
	.export {
	    border: 1px solid gray !important;
	}
	table tr:hover{cursor: pointer;}
	a,.sort{cursor: pointer;}
	a,.sort,.panel-heading span{cursor: pointer;}
	.panel-heading span{margin-left: 10px;}
	a,.sort{cursor: pointer;}
	.cur_sort_up{
		background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	.cur_sort_down{
		background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
</style>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
	      	<div class="col-xs-6">
		      	<strong class="emp_title"><?php echo $title = "Employee List";?></strong>
		    </div>
		    <div class="col-xs-6" style="text-align: right">
		      		<span class="top_action_button">
			    		<a>
			    			<img onclick="showsort(event);" src="<?php echo base_url('assets/images/icons/sort.png')?>" />
			    		</a>
			    	</span>
			    	<?php if($this->green->gAction("I")){ ?>
		      		<span class="top_action_button">
			    		<a href="<?php echo site_url('employee/employee/import')?>" >
			    			<img src="<?php echo base_url('assets/images/icons/import.png')?>" />
			    		</a>
			    	</span>
			    	<?php } ?>
			    	<?php if($this->green->gAction("E")){ ?>
			    	<span class="top_action_button">	
			    		<a href="JavaScript:void(0);" id="emp_export"  title="Export">
			    			<img src="<?php echo base_url('assets/images/icons/export.png')?>" />
			    		</a>
			    	</span>
			    	<?php } ?>
			    	<?php if($this->green->gAction("P")){ ?>
		      		<span class="top_action_button">
						<a href="JavaScript:void(0);" id="emp_print" title="Print">
			    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
			    		</a>
		      		</span>		    	
		      		<?php } ?>
		      		<?php if($this->green->gAction("C")){ ?>
			    	<span class="top_action_button">
			    		<a href="<?php echo site_url("employee/employee/add?m=$m&p=$p")?>" >
			    			<img src="<?php echo base_url('assets/images/icons/add.png')?>" />
			    		</a>
			    	</span>	      		
			    	<?php } ?>
		    </div>
	       </div>
	       <div class="panel panel-default" id="sort_ad" style="display:none">
			  <div class="panel-heading">
			  		<span class="label label-default" onclick="sortadvance(event);">A</span>
			  		<span class="label label-default" onclick="sortadvance(event);">B</span>
			  		<span class="label label-default" onclick="sortadvance(event);">C</span>
			  		<span class="label label-default" onclick="sortadvance(event);">D</span>
			  		<span class="label label-default" onclick="sortadvance(event);">E</span>
			  		<span class="label label-default" onclick="sortadvance(event);">F</span>
			  		<span class="label label-default" onclick="sortadvance(event);">G</span>
			  		<span class="label label-default" onclick="sortadvance(event);">H</span>
			  		<span class="label label-default" onclick="sortadvance(event);">I</span>
			  		<span class="label label-default" onclick="sortadvance(event);">J</span>
			  		<span class="label label-default" onclick="sortadvance(event);">K</span>
			  		<span class="label label-default" onclick="sortadvance(event);">L</span>
			  		<span class="label label-default" onclick="sortadvance(event);">M</span>
			  		<span class="label label-default" onclick="sortadvance(event);">N</span>
			  		<span class="label label-default" onclick="sortadvance(event);">O</span>
			  		<span class="label label-default" onclick="sortadvance(event);">P</span>
			  		<span class="label label-default" onclick="sortadvance(event);">Q</span>
			  		<span class="label label-default" onclick="sortadvance(event);">R</span>
			  		<span class="label label-default" onclick="sortadvance(event);">S</span>
			  		<span class="label label-default" onclick="sortadvance(event);">T</span>
			  		<span class="label label-default" onclick="sortadvance(event);">U</span>
			  		<span class="label label-default" onclick="sortadvance(event);">V</span>
			  		<span class="label label-default" onclick="sortadvance(event);">W</span>
			  		<span class="label label-default" onclick="sortadvance(event);">X</span>
			  		<span class="label label-default" onclick="sortadvance(event);">Y</span>
			  		<span class="label label-default" onclick="sortadvance(event);">Z</span>
			  		<!-- <span class="label label-default" onclick="sortadvance(event);">0-9</span> -->
			  </div>
			</div>
	       <!-------------Start Show Table------------>
	       
	       <!-- <div class="row"> -->
	       		<div class="col-sm-12">
	       			<div class="panel panel-default">
		       			<div class="panel-body">
		       				<div class="table-responsive" id="div_export_print">
		       					<table class="table" border="0" cellspacing="0" cellpadding="0">
		       						<thead>
		       							<tr>
		       								<th class="pos_1 sort">N&deg;</th>
		       								<th class="pos_2 sort" onclick="sort(event);" rel="empcode">EmployeeID</th>
		       								<th class="pos_3 sort" onclick="sort(event);" rel="last_name">Full Name</th>
		       								<th class="pos_4 sort" onclick="sort(event);" rel="last_name_kh">Full Name(Kh)</th>
		       								<th class="pos_5 sort" onclick="sort(event);" rel="dob">Date of Birth</th>
		       								<th class="pos_6 sort" onclick="sort(event);" rel="pos_id">Position</th>
		       								<th colspan="3" class="remove_tag">Action</th>
		       							</tr>
		       							<input type='hidden' value='ASC' name='sort' id='sort' style='width:80px;'/>
		       							<input type='hidden' value='' name='sortquery' id='sortquery' style='width:200px;'/>
		       							<?php echo $tr_search;?>
		       						</thead>
		       						<tbody class="listbody">
		       						<?php echo $tr;?>
		       						<?php if(count($tdata)>0){ ?>
		       						<tr class="remove_tag">
										<td colspan='12' id='pgt'>
											<div style='margin-top:20px; width:11%; float:left;'>
											Display : <select id="sort_num" onchange="search();" style='padding:5px; margin-right:0px;'>
															<?php
															$num=50;
															for($i=0;$i<10;$i++){?>
																<option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
																<?php $num+=50;
															}
															?>
														</select>
											</div>
											<div style='text-align:center; verticle-align:center; width:89%; float:right;'>
												<ul class='pagination' style='text-align:center'>
													
													<?php echo $this->pagination->create_links(); ?>
												</ul>
											</div>

										</td>
									</tr> 
									<?php } ?>
		       						</tbody>
		       					</table>
		       				</div>
		       			</div>
		       			
	       			</div>
	       		<!-- </div> -->
	        </div>

	       	<form id="excel-form" action="<?php echo base_url('app/libraries/Z_EXPORT_TO_EXCEL.PHP')?>" method="POST">
	             <input type="hidden" name="num_colspan" value="6" id="num_colspan"/>
	             <input type="hidden" name="exporttile" value="<?php echo $title ?>"/>
	             <input type="hidden" name="pagesecurity" value="PgSecurity"/>
	             <textarea id="excel-data" name="data" style="display:none;"></textarea>
	        </form>

	       <!-------------End Show Table------------>
	    </div>
	</div>
</div>
<!--Check Print Export-->
<div class="modal fade bs-example-modal-lg" id="check_export_print" data-backdrop=false>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><b>Print/Export Option</b></h4>
      </div>
      <div class="modal-body table-responsive">
      	<?php 
      		$thead = array(
      						"No"				=> "No",	
      						"EmployeeID" 		=> "empcode",
      						"Last Name"			=> "last_name",
      						"First Name"		=> "first_name",
      						"Last Name(Kh)" 	=> "last_name_kh",
      						"First Name(Kh)"	=> "first_name_kh",
      						"DOB"			 	=> "dob",
      						"Position" 			=> "position",
      						"Gender" 			=> "sex",
      						"DOB"				=> "dob",
      						"POB"				=> "pob",
      						"Addr."				=> "perm_adr",
      						"Village"			=> "village",
      						"Commune"			=> "commune",
      						"District"			=> "district",
      						"Province"			=> "province",
      						"Zoon"				=> "zoon",
      						"Marital Status"	=> "marital_status",
      						"Nationality"		=> "nationality",
      						"Employee Type"		=> "emp_type",
      						"Department"		=> "department",
      						"Phone"				=> "phone",
      						"Email"				=> "email",
      						"Leave"				=> "leave_school",
      						"Leave Reason"		=> "leave_school_reason",
      						"ID Card"			=> "idcard",
      						"Foreigner"			=> "is_foreigner",
      						"Employee Date"		=> "employed_date",
      						"Resigned Date"		=> "resigned_date",
      						"Note"				=> "note",
      						"Office Location"	=> "office_location",
      						"Contract Type"		=> "contract_type",
      						"Annual Leave"		=> "annual_leave",
      						"Sick Leave"		=> "sick_leave",
      						"AL Taken"			=> "al_taken",
      						"Sick Taken"		=> "sick_taken",
      						"Leave Reason"		=> "leave_reason",
      						"Con ID"			=> "con_id",
      						"Leave Type"		=> "leave_type",
      						"Leave Comment"		=> "leave_comment",
      						"KH New Year Hol"	=> "kh_newyear_hol",
      						"End Year Hol"		=> "endoftheyear_hol",
      						"Work Time"			=> "worktimeid"
      					  );
      	?>
        <table width="100%" align="center" class="table">
        	<tr>
        		<?php foreach ($thead as $value=>$key) {
        			echo "<th align='center'>$value</th>";
        		} ?>
        	</tr>
        	<tr>
        		<?php foreach ($thead as $value=>$key) {
        			echo '<td><input type="checkbox" class="chk" rel="'.$key.'" value="'.$value.'" checked="checked"></td>';
        		} ?>
        	</tr>
        </table>
      </div>
      
      <div class="modal-footer">
      	<label><input type="checkbox" name='is_all' id='is_all' value='0'>Print/Export All Page</label>
      	<button type="button" class="btn btn-success" id="btnexport" onclick='expdata(event);' data-dismiss="modal">Export</button>
        <button type="button" class="btn btn-success" id="btnprint" onclick='expdata(event);' data-dismiss="modal">Print</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<input type='text' id='page' class='hide' value="<?PHP if(isset($_GET['per_page'])) echo $_GET['per_page'] ?>" />
<div class="table-responsive hide"  id="tab_print">
  		<table class="export" id='exptbl' border='1'>

  		</table>
</div>
<!--Modal Export Print-->
<div class="modal fade" id="myModal_export_print" data-backdrop=false>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><b>Warning</b></h4>
      </div>
      <div class="modal-body">
        <b id="message_body"></b>
        <input type="hidden" name="get_rel" id="get_rel" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!--Modal Delete-->
<div class="modal fade" id="myModal_del" data-backdrop=false>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><b>Warning</b></h4>
      </div>
      <div class="modal-body">
        <b>Are you sure to delete this record !</b>
        <input type="hidden" name="get_rel" id="get_rel" value="">
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" onclick="delete_employee(event);">Yes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<script type="text/JavaScript">
	$(function(){
		$("#s_status").on("change",function(){
			search();
		});
		$("#emp_export").on("click", function(){
			var no_data = $(".no_data").attr('rel');
			var data = $("#div_export_print").html();
			$("a").removeAttr("href"); //remove link of href url when print

			//alert(no_data);return false;
			if(no_data == 1){
				$("#check_export_print").modal('show');
			}else{
				$("#message_body").text("We didn't find anything to Print.");
				$("#myModal_export_print").modal('show');
			}
		});
		//----------------Print Data--------
		$("#emp_print").on("click",function(){
			var no_data = $(".no_data").attr('rel');
			var data = $("#div_export_print").html();
			$("a").removeAttr("href"); //remove link of href url when print

			//alert(no_data);return false;
			if(no_data == 1){
				$("#check_export_print").modal('show');
			}else{
				$("#message_body").text("We didn't find anything to Print.");
				$("#myModal_export_print").modal('show');
			}
		});
		//---------------show number of record--------
		$(".chk").on("click", function(){
			var get_chk = $(this).attr('rel');
			if($(this).is(":checked")){
				$(".pos_"+get_chk).removeClass('remove_tag');
			}else{
				$(".pos_"+get_chk).addClass('remove_tag');
			}
		});
		$("#btnprint").on("click", function(){
			var data = $("#div_export_print").html();
			gsPrint('Employee List',data,'remove_tag');
		});
		$("#btnexport").on("click", function(){
			var no_data = $(".no_data").attr('rel');
			var data=$('.exptbl').attr('border',1);
				data = $("#tab_print").html().replace(/<img[^>]*>/gi,"");
			//var export_data=$("<center>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
			var export_data=$("<center><h3 align='center'><meta charset='utf-8'>Employee List</h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html(); 
			
			if(no_data ==1){
				window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
				$('.table').attr('border',0);
			}else{
				$("#message_body").text("We didn't find anything to Export.");
				$("#myModal_export_print").modal('show');
			}
		});
	});
	//-----------------End Main Function-----------------------
	function showsort(event){	
		$('#sort_ad').fadeToggle('3000');
	}
	function sortadvance(event){
		var value = $(event.target).text();
		
		var sql='';
		if($('.cur').attr('rel')==undefined){
			alert('please select field');
		}else {
			$(event.target).removeClass('label-default');
			$('.panel div span').removeClass('label-danger');
			$('.panel div span').addClass('label-default');
			$(event.target).addClass('label-danger');
			var field = $('.cur').attr('rel');
			
			if(value=='0-9')
				sql="ORDER BY Case When "+field+" Like '[0-9]%' Then 1 Else 0 End Asc, "+field;
			else
				sql="AND "+field+" LIKE '"+value+"%' ";
			search(sql);	
		}
	}
	function sort(event){
		var sort=$(event.target).attr('rel');

		var sorttype=$('#sort').val();
		if(sorttype=='ASC'){
			$('#sort').val('DESC');
			$('.sort').removeClass('cur_sort_down cur');
			$(event.target).addClass('cur_sort_up cur');
		}
		else{

			$('#sort').val('ASC');
			$('.sort').removeClass('cur_sort_up cur');
			$(event.target).addClass('cur_sort_down cur');
		}
		$('#sortquery').val('ORDER BY '+sort+' '+sorttype);
		search();
	}
	function search(sort){
		if(sort==''){
			$('.panel div span').removeClass('label-danger');
			$('.panel div span').addClass('label-default');
		}
		var sort_num = $("#sort_num").val();
		var sch_empId = $("#sch_empId").val();
		var pos_id		= $("#pos_id").val();
		var sch_fullname = $("#sch_fullname").val();
		var sch_fullname_kh = $("#sch_fullname_kh").val();
		var s_status = $("#s_status").val();
		var sortemp=$('#sortquery').val();

		var roleid=<?php echo $this->session->userdata('roleid');?>;
		var m=<?php echo $m;?>;
		var p=<?php echo $p;?>;

		if(sortemp == '')
			sortemp='ORDER BY empid DESC';
		
			$.ajax({
				type: "POST",
				url:"<?php echo base_url(); ?>index.php/employee/employee/search_emp",    
				data: {
					'sort_ad':sort,
					'sort':sortemp,
					'sort_num':sort_num,
					'pos_id':pos_id,
					'sch_empId':sch_empId,
					'sch_fullname':sch_fullname,
					'sch_fullname_kh':sch_fullname_kh,
					'roleid':roleid,
					's_status':s_status,
					'm':m,
					'p':p,
				},
				success: function(data){
		           $('.listbody').html(data);
				}
			});
		
	}
	//----load to model employee/employee/view_emp
	function view_employee(event){
		var employee_id=jQuery(event.target).attr("rel");
		window.open("<?PHP echo site_url('employee/employee/view_emp');?>/"+employee_id+"?<?php echo "m=$m&p=$p" ?>","_blank");
	}
	function edit_employee(event){
		var employee_id=jQuery(event.target).attr("rel");
		location.href="<?PHP echo site_url('employee/employee/get_emp');?>/"+employee_id+"?<?php echo "m=$m&p=$p" ?>";
	}
	//----load to model employee/employee/delete
	function delete_employee(event){
		var r = confirm("Are you sure to delete this record !");
		if(r == true){
			var emp_id= $(event.target).attr('rel');
			location.href="<?PHP echo site_url('employee/employee/delete');?>/"+emp_id+"?<?php echo "m=$m&p=$p" ?>";
		}
	}
	$('#emp_export').click(function(){
		//$('#exporttap').modal('show');
		$('#btnprint').hide();
		$('#btnexport').show();
	})
	$('#emp_print').click(function(){
		//$('#exporttap').modal('show');
		$('#btnprint').show();
		$('#btnexport').hide();
	})
	
	function expdata(event){
		var sch_empId 		= $('#sch_empId').val();
		var sch_fullname 	= $('#sch_fullname').val();
		var sch_fullname_kh = $('#sch_fullname_kh').val();
		var s_status 		= $('#s_status').val();
		var pos_id 			= $('#pos_id').val();
		var sort_num 		= $('#sort_num').val();
		var sortstd 		= $('#sortquery').val();
		var page 			= $('#page').val();
		var is_all=0;
		if($('#is_all').is(':checked'))
			is_all=1;
		if(sortstd=='')
				sortstd=' order by empid desc ';
		var field={};
		$('.chk').each(function(){
			if($(this).is(':checked')){
				var key=$(this).attr('rel');
				var val=$(this).val();
				field[key]=val;
			}	
		})
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/employee/employee/getexp",    
				data: {
						'page':page,
						'is_all':is_all,
						'f':field,
						'sort_num':sort_num,
						'sort':sortstd,
						'sch_empId':sch_empId,
						'sch_fullname':sch_fullname,
						'sch_fullname_kh':sch_fullname_kh,
						's_status':s_status,
						'pos_id':pos_id},
				type: "POST",
				dataType:'json',
				async:false,
				success: function(data){
					$('#exptbl').html(data.data);
					// var s=$(event.target).attr('rel');
					// if(s=='E')
					// 	exports();
					// else
					// 	prints();
			}
		});
	}
</script>