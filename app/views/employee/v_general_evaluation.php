<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }

 ?>
<div class="col-sm-12">
    <div class="row result_info">
        <div class="col-sm-6 "><strong>Evaluation Group</strong><?php echo $this->lang->line("cust_list")?></div>
    </div>

     <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-9" style="padding-top:25px;">
                <label class="control-label"><span>Evaluation Group</span></label>
                <input type="text" name="decritption_eval" id="decritption_eval" class="form-control input-xs"/>
                <input type="hidden" name="h_custcode" id="h_custcode" class="form-control input-xs"/>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12" style="padding-top:25px;">
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <label class="control-label"></label>
                <?php if($this->green->gAction("C")){ ?>
                <input type="button" id="save" value="Save" class="btn btn-primary"/>
                <?php } ?>
                <input type="button" id="clear" value="Clear" class="btn btn-warning"/>
            </div>
       </div> 
    </div>
    <div class="row result_info">
        <div class="col-sm-6"><strong>List of Evaluation Group</strong><?php echo $this->lang->line("cust_list")?></div>
    </div>
    <div class="col-sm-12">
        <meta charset="Utf-8">
         <div class="table-responsive" id="dv-print">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th width="5%">N&deg;</th>
                        <th width="80%">Evaluation Group</th>
                        <th style="text-align:center;" width="5%" colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody id="show_tbl"></tbody>
            </table>
        </div>
    </div>
    <div id="show_tbl"></div> 
</div>
    <div class="modal" id="myModal_delete" role="dialog" style="top:8%">
        <div class="modal-dialog" style="width:300px">
            <div class="modal-content">
                <div class="modal-header" style="text-align:left; font-size:14px; text-align:center; color:red;">Do you want to delete ? </div>
                <div class="modal-footer">
                    <button type="button" id="delete" data-dismiss="modal" class="btn btn-danger">Delete</button>
                    <button type="button" class=" btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal_msg">
        <div class="modal-dialog" style="width:400px">
            <div class="modal-content">
                <div class="modal-body">
                    <h5 class="modal-title" id="title_msg" style="text-align:center;"></h5>
                </div>
                <div class="modal-footer" style="display:none" id="div_btn">
                    <button type="button" class=" btn btn-danger" id="close_msg" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        showdata();
        $("#clear").on("click",function(){
            cleadata();
            $("#save").val('Save').shw();
            $("#save").val('Update').hide();
        });

        // save ==========================================
        $("#save").on("click",function(){
            if($("#decritption_eval").val() != ''){
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('employee/c_general_evaluation/save');?>",
                    data : {
                        h_custcode       : $("#h_custcode").val(),
                        decritption_eval : $("#decritption_eval").val()
                    },
                    success:function(data){
                        $('#myModal').modal('hide');
                        $('#div_btn').hide();
                        $('#myModal_msg').modal({show: true});
                        $('#title_msg').html('Save successfull...');
                        $('#title_msg').css('color','blue');
                        setTimeout(function (){ $('#myModal_msg').modal('hide'); }, 1500);
                        showdata();
                        cleadata();
                        $("#save").val('Save');
                    }
                });
            }else{
                $("#decritption_eval").select();
            }
        });
        //  delete===================================================
        $(document).on('click','.delete_data',function(){
            var deletedata = $(this).attr('delete'); 
            $("#myModal_delete").modal();
            $("#delete").click(function(){
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('employee/c_general_evaluation/deletedata');?>",
                    data : { 
                        deletedata : deletedata
                    },
                    success : function(data){ 
                         showdata();
                    }
                });
             });
        });
        //  edit ======================================================
        $(document).on('click','.edit_data',function(){
            var h_custcode = $(this).attr("edit");
            $.ajax({ 
                type : "POST",
                url  : "<?= site_url('employee/c_general_evaluation/editdata');?>",
                data : { 
                    h_custcode : h_custcode 
                },
                success:function(data){
                  var post = data.split('|');
                   $("#h_custcode").val(post[0]);
                   $("#decritption_eval").val(post[1]);
                   $("#save").val('Update');
                }
            }); 
        });

    });// function()===================================================
            function showdata(){
                var roleid=<?php echo $this->session->userdata('roleid');?>;
                var m=<?php echo $m;?>;
                var p=<?php echo $p;?>;
                    $.ajax({ 
                        type : "POST",
                        url  : "<?= site_url('employee/c_general_evaluation/showdata');?>",
                        data : {
                            showdata : 1,
                            roleid:roleid,
                            m:m,
                            p:p
                        },
                        success:function(data){
                            $("#show_tbl").html(data);
                        }
                    });
                }
            function cleadata(){
                $("#h_custcode").val("");
                $("#decritption_eval").val("");
            }
</script>
                   