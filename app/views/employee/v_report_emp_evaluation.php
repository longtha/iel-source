<?php 
	if(COUNT($sql_query) > 0){ 
		foreach($sql_query as $row){ 
			$evalid           = $row->evalid;
			$empid            = $row->empid;
			$review_from_date = $row->review_from_date;
			$review_to_date   = $row->review_to_date;
			$eval_date        = $row->eval_date;
			$eval_by_empid    = $row->eval_by_empid;
			$sup_comment      = $row->sup_comment;
			$sup_comment_date = $row->sup_comment_date;
			$emp_comment      = $row->emp_comment;
			$emp_comment_date = $row->emp_comment_date;
			$gm_comment       = $row->gm_comment;
			$ove_comment      = $row->ove_comment;
			$gm_emp_id        = $row->gm_emp_id;
			$gm_date          = $row->gm_date;
			$ove_com_id       = $row->ove_com_id;
			$typeno           = $row->transno;
		}
	}
	$query_emp = $this->db->query("SELECT
							sch_emp_profile.sex,
							sch_emp_profile.pos_id,
							sch_emp_profile.dep_id,
							sch_emp_profile.first_name,
							sch_emp_profile.last_name
						FROM
							sch_emp_profile 
						WHERE empid = '".$empid."'");
		if(COUNT($query_emp) > 0){ 
			foreach($query_emp->result() as $data){ 
				$first_name = $data->first_name;
				$last_name  = $data->last_name; 
				$sex        = $data->sex;
				$dep_id     = $data->dep_id;
				$pos_id     = $data->pos_id;
			}
		}
		$department = $this->db->query("SELECT sch_emp_department.department FROM sch_emp_department WHERE dep_id ='".$dep_id."'")->row()->department;
		$position   = $this->db->query("SELECT sch_emp_position.position FROM sch_emp_position WHERE posid ='".$pos_id."'")->row()->position;
?>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
	<meta charset="utf-8">
	<table border="0" align="center" width="" style=" border-collapse: collapse;">
		<tbody>
			<tr>
				<td  width="10%">
					<a class="logo.png" href="index.html">
						<image style="text-align:left;" width="150px;" height="45px;" src="<?= base_url('assets/images/logo/logo.png')?>">
					</a>
				</td>
				<td style="text-align:center" width="50%">
					<input type="hidden" >
					<span style="font-family: Times New Roman;font-size: 16px;font-weight:bold;"><u>PERFORMANCE & DEVELOPMENT EVALUATION FORM (PDE)</u></span><br/>
					<span style="font-family: khmer mef2;font-size: 16px;">ទំរង់វាយតម្លៃការងារ​ និង​ ការអភិវឌ្ឍន៍</span>
				</td>
			</tr>
		</tbody>
	</table><br/>
	<table border="0" width="100%">
		<tbody>
		<tr>
			<td colspan="10">
				<span style="font-family: Times New Roman; font-size: 12px;">(Note: This form is used for any evaluation, end of probation & regular staff)</span><br/>
			</td><br/>
		</tr>
		<tr style="text-align:justify;">
			<td colspan="3">
				<input type="hidden" id="idhidden_evalmen">
				<span style="font-family: Times New Roman; font-size: 12px;">Employee Name :</span>
			</td>
			<td  id="emp_name" style="width:160px; border: none;border-bottom: 1px dotted;font-family:Times New Roman;font-size:12px;"><?php echo $last_name.' '.$first_name; ?><input type="hidden" id="idhidden_emp" value="<?php echo $empid ;?>"></td>
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;">Sex :</span>
			</td>
			<td id="emp_sex" style="width:40px; border: none;border-bottom: 1px dotted;font-family:Times New Roman;font-size:12px;"><?php echo $sex; ?></td>
			
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;">Position :</span>
			</td>
			<td id="emp_position" style="width:200px; border: none;border-bottom: 1px dotted;font-family:Times New Roman;font-size:12px;"><?php echo $position; ?></td>
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;">Department/Unit :</span>
			</td>
			<td id="emp_department" style="width:200px; border: none;border-bottom: 1px dotted;font-family:Times New Roman;font-size:12px;"><?php echo $department; ?></td>
		</tr>
		<tr>
			<td colspan="3">
				<span style="font-family: Times New Roman; font-size: 12px;">Review Period / From :</span>
			</td>
			<td id="emp_rev_formdate" style="width:100px; border: none;border-bottom: 1px dotted;font-family:Times New Roman;font-size:12px;"><?php echo $review_from_date; ?></td>
			
			<td colspan="3">
				<span style="font-family: Times New Roman; font-size: 12px;">Review Period / To :</span>
			</td>
			<td id="emp_rev_todate" style="width:100px; border: none;border-bottom: 1px dotted;font-family:Times New Roman;font-size:12px;"><?php echo $review_to_date; ?></td>
			
		</tr>
		<tr>
			<td colspan="3">
				<span style="font-family: Times New Roman; font-size: 12px;">Evaluator Name :</span>
			</td>
			<?php 
					$fullname='';
					if (isset($eval_by_empid)){
						$fullname=$this->db->query("SELECT CONCAT(last_name,' ',first_name) AS fullname FROM sch_emp_profile WHERE empid='".$eval_by_empid."'")->row()->fullname;
					}
				?>
			<td id="evaluator_name" style="width:150px; border: none;border-bottom: 1px dotted;font-family:Times New Roman;font-size:12px;"><?php echo $fullname; ?><input type="hidden" id="idhidden_eval" value="<?php echo $eval_by_empid; ?>"/></td>
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;">Sex :</span>
			</td>
			<?php
				$sex='';
				if (isset($eval_by_empid)){
					$sex=$this->db->query("SELECT sep.sex FROM sch_emp_profile AS sep WHERE sep.empid='".$eval_by_empid."'")->row()->sex;
				}
			?>
			<td id="evaluator_sex" style="width:50px; border: none;border-bottom: 1px dotted;font-family: Times New Roman; font-size: 12px;"/><?php echo $sex;?></td>
			
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;">Position :</span>
			</td>
			<?php 
				$position='';
				if (isset($eval_by_empid)){
					$position=$this->db->query("SELECT ep.position FROM sch_emp_profile sep INNER JOIN sch_emp_position ep ON sep.pos_id=ep.posid WHERE sep.empid='".$eval_by_empid."'")->row()->position;
				}
			?>
			<td id="evaluator_psition" style="width:200px; border: none;border-bottom: 1px dotted; font-family: Times New Roman; font-size: 12px;"><?php echo $position; ?></td>
			
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;">Department :</span>
			</td>
			<?php 
				$department='';
				if (isset($eval_by_empid)){
					$department=$this->db->query("SELECT ed.department FROM sch_emp_profile sep INNER JOIN sch_emp_department ed ON sep.dep_id=ed.dep_id WHERE sep.empid='".$eval_by_empid."'")->row()->department;
				}
			?>
			<td id="evaluator_department" style="width:200px; border: none;border-bottom: 1px dotted;font-family: Times New Roman; font-size: 12px;"><?php echo $department; ?></td>
		</tr>
		<tr>
			<td colspan="3">
				<span style="font-family: Times New Roman; font-size: 12px;">Date of Evaluation :</span>
			</td>
			<td id="evaluator_ofdate" style="width:100px; border: none;border-bottom: 1px dotted;font-family: Times New Roman; font-size: 12px;"><?php echo $eval_date; ?></td>
		</tr>
		</tbody>
	</table><br/>
	<?php 
		$arr_main = array("1"=>"GENERAL EVALUATION",);
		$tr = "";
		$title = "";
		if(count($arr_main)>0){
			foreach($arr_main as $key=>$val){
				$title.='<span style="font-family: Times New Roman;font-size:12px;"style="width:20px;height:20px;font-weight: bold;">I.&nbsp;</b>'.$val.'<b></span>';
				$sql_main = $this->db->query("SELECT
													sch_evamain.main_val_id,
													sch_evamain.description
												FROM
													sch_evamain
												WHERE id_main='".$key."'")->result();
				$ii =0;
				if(count($sql_main)>0){
					foreach($sql_main as $row_main1){
						//$title.='<span style="font-family: Times New Roman;font-size:12px;"><b>&nbsp;&nbsp;&nbsp;</b>'.$row_main1->description.'</span><br/>';
						$tr.='<tr><td colspan="8" style="text-align:left;height: 40px;"><span style="font-family: Times New Roman;font-size:14px;"><b>&nbsp;&nbsp;&nbsp;'.$row_main1->description.'</b></span></td></tr>';
						$sql_sub = $this->db->query("SELECT
														sch_evagroup.group_eval_id,
														sch_evagroup.main_eval_id,
														sch_evagroup.area_eval,
														sch_evagroup.area_eval_kh
													FROM
														sch_evagroup
													WHERE main_eval_id='".$row_main1->main_val_id."'")->result();
						if(count($sql_sub)>0){
							$i = 1;
							foreach($sql_sub as $row_sub){
								$value_textarea = '';
								$value_checked  = '';
								$sql_com = $this->db->query("SELECT `comment`,rate,group_id FROM sch_eva_emp_group WHERE grop_eval_id='".$row_sub->group_eval_id."' AND transno='".$typeno."'")->row();
								if(count($sql_com) > 0){
									$value_textarea = $sql_com->comment;
									$value_checked  = $sql_com->rate;
								}
								$tr.=  '<tr>
											<td style="text-align:center;">
												<input type="hidden" class="idhidden_geva" id="idhidden_geva" value="'.$row_sub->group_eval_id.'">'.$i.'</td>
											<td style="text-align:left;" class="eval_area">
												<span style="font-family: Times New Roman; font-size: 12px;">'.$row_sub->area_eval.'</span><br>
												<span style="font-family: khmer mef1;font-size: 12px;">'.$row_sub->area_eval_kh.'</span><br>
												
											</td>
											
											<td class="comments_eval form-control input-xs" id="comments_eval" style="height:40px;font-family: Times New Roman; font-size: 12px;">'.$value_textarea.'<input type="hidden" id="hidden_group_id" value='.(isset($row_sub->group_id) ? $row_sub->group_id : '').' /></td>
									    	<td style="text-align:center"><input type="checkbox" '.($value_checked ==5?"checked":"").' class="radio_check radio_check_'.$row_sub->group_eval_id.'  radio_check5 chrating" attr_val="'.$row_sub->group_eval_id.'" value="5" name="fooby['.$i.'][]" style="border:1px;"/></td>
									    	<td style="text-align:center"><input type="checkbox" '.($value_checked ==4?"checked":"").' class="radio_check radio_check_'.$row_sub->group_eval_id.'  radio_check4 chrating" attr_val="'.$row_sub->group_eval_id.'" value="4" name="fooby['.$i.'][]" style="border:1px;"/></td>
									    	<td style="text-align:center"><input type="checkbox" '.($value_checked ==3?"checked":"").' class="radio_check radio_check_'.$row_sub->group_eval_id.'  radio_check3 chrating" attr_val="'.$row_sub->group_eval_id.'" value="3" name="fooby['.$i.'][]" style="border:1px;"/></td>
									    	<td style="text-align:center"><input type="checkbox" '.($value_checked ==2?"checked":"").' class="radio_check radio_check_'.$row_sub->group_eval_id.'  radio_check2 chrating" attr_val="'.$row_sub->group_eval_id.'" value="2" name="fooby['.$i.'][]" style="border:1px;"/></td>
									    	<td style="text-align:center"><input type="checkbox" '.($value_checked ==1?"checked":"").' class="radio_check radio_check_'.$row_sub->group_eval_id.'  radio_check1 chrating" attr_val="'.$row_sub->group_eval_id.'" value="1" name="fooby['.$i.'][]" style="border:1px;"/></td>
									    </tr>';
								$i++;
							}
						}
						
					}
				}
			}
		}
	?>
	<table  id="emp_evalutiont" border="1" style=" border-collapse: collapse;" width="100%">
		<thead>		
			<tr>
				<th rowspan="2" style="width:5%;border-right: 1px solid;text-align:center;background-color:#ccc;vertical-align: middle;">
					<span style="font-family:Times New Roman;font-size:12px;vertical-align: middle;">N&deg;</span>			
				</th>	
				<th rowspan="2" style="width:35%;border-right: 1px solid;text-align:center; background-color:#ccc;vertical-align: middle;">
					<span style="font-family:Times New Roman;font-size:12px;">AREA</span>
					<span style="font-family:khmer mef1;font-size:12px;">តំបន់</span>				
				</th>	
				<th rowspan="2" style="width:40%;border-right: 1px solid; text-align:center; background-color:#ccc;vertical-align: middle;">
					<span style="font-family:Times New Roman;font-size:12px;">COMMENTS</span>
                    <span style="font-family:khmer mef1;font-size:12px;">មតិយោបល់</span>
				</th>	
				<th colspan="5" style="width:20%;border-right: 1px solid;text-align:center; background-color:#ccc;">
					<span style="font-family:Times New Roman;font-size:12px;">RATING</span><br/>
				</th>
			</tr>
			<tr style=" background-color:#ccc;text-align:center;">
				<th>5</th><th>4</th><th>3</th><th>2</th><th>1</th>
			</tr>
		</thead>
		<tbody>
			<?php
				echo $title.$tr;
			?>
		</tbody>
			<tr style="border-right: 1px solid; background-color:#ccc;">
				<td colspan="3"style="text-align:right;font-family: Times New Roman;font-size:12px;font-weight:bold;">
					<span>Total Scores :</span>
				</td>
				 <td colspan="2"><input type="text" style="width:70px;background-color:#ccc;" class="total_score"/></td>
				<td>&nbsp;&nbsp;/&nbsp;&nbsp;</td>
					<?php 
						$count = $this->db->query("SELECT COUNT(*) as count_grop_eval
													FROM
													sch_eva_emp_group
													WHERE grop_eval_id >= 15 AND grop_eval_id <= 20 AND transno = '".$typeno."'")->row()->count_grop_eval;
						if($count > 0){ 
							$option = '19';
						}else{ 
							$option = '13';
						}
					?>
				<td colspan="2"><input type="text"  style="width:60px; background-color:#ccc;font-family: Times New Roman;font-size:12px;" class="total_rate" value="<?php echo $option; ?>" /></td>
			</tr>
			<tr style="border-right: 1px solid; background-color:#ccc;">
				<td colspan="3" style="text-align:right;font-family: Times New Roman;font-size:12px;font-weight:bold;">
					<span>Total Rating :</span>
				</td>
			<th colspan="5"><input type="text" class="total_rating" style="width:100%; background-color:#ccc;" /></th>
			</tr>
	</table><br/>
	<?php
		$tr = "";
		if(count($sql_main)>0){
			$sql_sub = $this->db->query("SELECT
											sch_evamention.mention_id,
											sch_evamention.score,
											sch_evamention.mention,
											sch_evamention.mention_kh
										FROM
											sch_evamention")->result();
							$td1="";	
							$td="";
							
							foreach($sql_sub as $row_sub){
									$td1.=  "<td style='text-align:center;width:15%;padding-bottom: 8px; padding-top:10px; font-family: Times New Roman;font-size:12px;'>".$row_sub->score."</td>";

									$td.= '<td style="text-align:center; width:15%;padding-bottom: 8px; padding-top:10px;">
													<div style="font-family:Times New Roman;font-size:12px;">'.$row_sub->mention.'</div>
													<div style="font-family:khmer mef1;font-size:12px;">'.$row_sub->mention_kh.'</div>
											</td>';
										    

							}
				}	
	?>
	<table  border="1" style=" border-collapse: collapse;" width="100%">
		<thead>		
			<tr>
				<th colspan="7" style="width: ;border-right: 1px solid;background-color:#ccc;">
					<span style="font-family: Times New Roman;font-size:13px;">Add the scores together and divide by 13 for normal employees and by 19 for management to get the overall performance rating. Below is a table showing you where to tick the overall performance level.</span>
					<span style="font-family: khmer mef1;font-size:11px;">បូកសរុបពិន្ទុទំាងអស់ហើយចែកនិង ១៣ សំរាប់បុគ្គលិកធម្មតា ឬចែកនឹង ១៩ សម្រាប់តួនាទីជាអ្នកគ្រប់គ្រង ដើម្បីឲ្យការវាយតម្លៃជាមធ្យមមួយ។ ខាងក្រោមនេះគឺជាតារាងដែលបង្ហាញអំពីកំរឹតនៃការវាយតម្លៃជារួម៖</span>
				</th>
			</tr>

		</thead>
		<tbody>
			<tr>
				<td style="width:15%; border-right: 1px solid;text-align:center">
					<span style="font-family: Times New Roman;font-size:12px;">Score</span>
					<span style="font-family: khmer mef1;font-size:12px;">ពីន្ទុ</span>
				</td>
				<?php
					echo $td1;
				?>
			</tr>
			<tr>
				<td style="width:15%; border-right: 1px solid;text-align:center">
					<span style="font-family: Times New Roman;font-size:12px;">Level</span>
					<span style="font-family: khmer mef1;font-size:12px;">កំរិត</span>
				</td>
				<?php
					echo $td;
				?>
			</tr>
		</tbody>
	</table><br/>
	<table  border="1" width="100%">
		<?php
			$arr_main = array("2"=>"PERSONAL DEVELOPMENT PLAN");
			$title1 = "";
			if(count($arr_main)>0){
				foreach($arr_main as $key=>$val){
					$title1.='<span style="font-family: Times New Roman;font-size:12px;"style="width:20px;height:20px;font-weight: bold;">II.&nbsp;</b>'.$val.'<b></span>';
				
				}
				echo $title1;	
			}
		?>
	</table>
	<table name="tbl_ment"  style=" border-collapse: collapse;" class=" control-label" border="1" width="100%">
		<tr>
			<td style=" vertical-align: text-bottom;">
				<table  name="tbl1" class=" table control-label" border="1" style=" border-collapse: collapse;" width="100%">
					<thead>		
						<tr style="background-color:#ccc;">
							<th rowspan="1" style="width: 3%;border-right: 1px solid; background-color:#ccc;">
								<span style="font-family: Times New Roman;font-size:12px;">N&deg;</span>			
							</th>
							<th colspan="1" style="width:50%;border-right: 1px solid;text-align:center;">
								<span style="font-family: Times New Roman;font-size:12px;">Strengths</span>
								<span style="font-family: khmer mef1;font-size:12px;">ចំនុចខ្លាំង</span>
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$sql=$this->db->query("SELECT
														sch_eva_emp_evalutiont.evalid,
														sch_eva_emp_streng.streng_id,
														sch_eva_emp_streng.strength
													FROM
														sch_eva_emp_evalutiont
													INNER JOIN sch_eva_emp_streng ON sch_eva_emp_evalutiont.evalid = sch_eva_emp_streng.evalid
													WHERE sch_eva_emp_evalutiont.evalid= '".$evalid."'");
							$tr = "";
							$i = 1;
							if($sql->num_rows() > 0){

								foreach ($sql->result() as $row) {

									$tr .= '<tr>'.
												"<td style='width:5%;background-color:#ccc; text-align:center;'>".$i++."<input type='hidden' id='hidden_streng' value='".$row->streng_id."'/></td>".
												'<td  id="strengths " class="strengths form-control "style="width:450px;Importen ;font-family: Times New Roman; font-size: 12px;"/>'.$row->strength.'</td>'.													
											'</tr>';
								}
							}
							echo $tr;							
						?>
					</tbody>
				</table>
			</td>
			<td style=" vertical-align: text-bottom;">
				<table name="tbl2"  class=" table control-label table-hover" border="1" style=" border-collapse: collapse;" width="100%">
					<thead>		
						<tr style="background-color:#ccc;">
							<th rowspan="1" style="width: 3%;border-right: 1px solid; background-color:#ccc;">
								<span style="font-family: Times New Roman;font-size:12px;">N&deg;</span>			
							</th>
							<th colspan="1" style="width:50%;border-right: 1px solid;text-align:center;">
								<span style="font-family: Times New Roman;font-size:12px;">Areas of Improvement</span>
								<span style="font-family: khmer mef1;font-size:12px;">ចំនុចដែលត្រូវកែប្រែឲ្យប្រសើរឡើង</span>
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$sql=$this->db->query("SELECT
														sch_eva_emp_evalutiont.evalid,
														sch_eva_emp_improvements.improvl_id,
														sch_eva_emp_improvements.improment
													FROM
														sch_eva_emp_evalutiont
													INNER JOIN sch_eva_emp_improvements ON sch_eva_emp_evalutiont.evalid = sch_eva_emp_improvements.evalid
													WHERE sch_eva_emp_evalutiont.evalid= '".$evalid."'");
							
							$tr1 = "";
							$j = 1;
							if($sql->num_rows() > 0){
								foreach ($sql->result() as $row){
									$tr1 .= '<tr>'.
												"<td style='width:5%;background-color:#ccc; text-align:center'>".$j++." <input type='hidden' id='hidden_improment' value='".$row->improvl_id."'/></td>".
												'<td id="im_proment" class="im_proment  form-control "style="width:450px; font-family: Times New Roman;font-size:12px;Importen"/>'.$row->improment.'</td>'.													
											'</tr>';
								}
							}
							echo $tr1;							
						?>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
	<table  border="0" width="100%">
		<tbody>
			<tr>
				<td colspan="5" class="col-sm-12">
                    <label class="control-label"><span style="font-family: Times New Roman; font-size: 12px;">Supervisor's comments: </span><span style="font-family: khmer mef1;font-size:12px;">យោបល់របស់ប្រធានផ្ទាល់ ៖</span></label><br/>
                </td>
			</tr>
			<tr>
				<td colspan="5" type="text" name="comments_supervisor" id="comments_supervisor" class="form-control input-xs"style="width:1245px;border: none;border-bottom: 1px dotted; font-family: Times New Roman; font-size: 12px;"><?php echo $sup_comment;?></td><br/>
			</tr>
			<tr>
				<td  style="padding-top:25px;">
					<span style="font-family: Times New Roman; font-size: 12px;">Supervisor's Name :</span><br/>
					<span style="font-family: khmer mef1;font-size:12px;text-align:justify; font-family: Times New Roman; font-size: 12px;">ឈ្មនោះ និង តួនាទី</span>
				</td>
				<td style="padding-top:25px;">
					<input type="text" id="subpervisors_name" style="width:210px;border:none;border-bottom: 1px dotted; font-family: Times New Roman; font-size: 12px;" value="<?php echo $fullname;?>"/><br/>  
					<input type="text" id="subpervisors"  style="width:210px;border: none;border-bottom: 1px dotted; font-family: Times New Roman; font-size: 12px;" value="<?php echo $position;?>"/>
				</td>
				<td style="padding-top:25px;">
					<span style="font-family: Times New Roman; font-size: 12px;">Signature :......................................</span><br/>
					<span style="font-family: khmer mef1;font-size:12px;text-align:justify; font-family: Times New Roman; font-size: 12px;">ហត្ថលេខា</span>
				</td>
				<td style="padding-top:25px;">
					<span style="font-family: Times New Roman; font-size: 12px;">Date :</span><br/>
					<span style="font-family: khmer mef1;font-size:12px;text-align:justify; font-family: Times New Roman; font-size: 12px;">កាលបរិច្ឆេទ</span>
				</td>
				<td ><input type="text" id="supervisor_date" style="width:90px; border: none;border-bottom: 1px dotted;font-family: Times New Roman; font-size: 12px;" value=" <?php echo $sup_comment_date; ?>" /></td>
			</tr>
			<tr>
				<td colspan="5" class="col-sm-12" style="padding-top:25px;">
                    <label class="control-label"><span style="font-family: Times New Roman; font-size: 12px;">Employee's comments: </span><span style="font-family: khmer mef1;font-size:12px;">យោបល់របស់បុគ្គិក៖</span></label>
                </td>
			</tr>

			<tr>
				<td colspan="5" type="text" name="comments_employee" id="comments_employee" class="form-control input-xs"style="width:1245px;border: none;border-bottom: 1px dotted;font-family: Times New Roman; font-size: 12px;"><?php echo $emp_comment; ?></td>
			</tr>
			<tr>
				<td>
					<span style="font-family: Times New Roman; font-size: 12px;">Employee's Name :</span><br/>
					<span style="font-family: khmer mef1;font-size:12px;text-align:justify; font-family: Times New Roman; font-size: 12px;">ឈ្មនោះ និង តួនាទី</span>
				</td>
				<td>
					<?php 
						$positions='';
						if (isset($empid)){
							$positions = $this->db->query("SELECT ep.position FROM sch_emp_profile sep INNER JOIN sch_emp_position ep ON sep.pos_id = ep.posid WHERE sep.empid='".$empid."'")->row()->position;
						}
					?>
					<input type="text" id="employee_name" style="width:210px;border: none;border-bottom: 1px dotted; font-family: Times New Roman; font-size: 12px;" value="<?php echo $last_name.' '.$first_name; ?>"/><br/>  
					<input type="text" id="employee"  style="width:210px;border: none;border-bottom: 1px dotted; font-family: Times New Roman; font-size: 12px;" value="<?php echo $positions; ?>"/><br/>
					
				</td>
				<td>
					<span style="font-family: Times New Roman; font-size: 12px;">Signature :.......................................</span><br/>
					<span style="font-family: khmer mef1;font-size:10px;text-align:justify; font-family: Times New Roman; font-size: 12px;">ហត្ថលេខា</span>
				</td>
				<td>
					<span style="font-family: Times New Roman; font-size: 12px;">Date : </span><br/>
					<span style="font-family: khmer mef1;font-size:12px;text-align:justify; font-family: Times New Roman; font-size: 12px;">កាលបរិច្ឆេទ</span>
				</td>
				<td><input type="text" id="employee_date"  style="width:100px;border: none;border-bottom: 1px dotted;font-family: Times New Roman; font-size: 12px; " value="<?php echo $gm_date;?>"/></td>
			</tr>
			<tr>
				<td colspan="5" class="col-sm-12" style="padding-top:25px;">
                    <label class="control-label"><span style="font-family: Times New Roman; font-size: 12px;">General Manager's comments (if any): </span><span style="font-family: khmer mef1;font-size:12px;">យោបល់របស់ប្រធានគ្រប់គ្រងទូទៅ៖</span></label>
                </td>
			</tr>
			<tr>
				<td colspan="5" type="text" name="comments_manager" id="comments_manager" class="form-control input-xs"style="width:1245px;border: none;border-bottom: 1px dotted;font-family: Times New Roman; font-size: 12px;"><?php echo $gm_comment; ?></td>
			</tr>
		</tbody>
	</table><br/>
	<table border="0" width="100%">
		<thead>
			<tr>
				<td>
					<span style="font-family: Times New Roman; font-size: 12px;">Next management level has to tick one of the boxes below to show the result of this evaluation. </span>
				    <span style="font-family: khmer mef1;font-size:12px;">ប្រធានបន្ទាប់ទ្រូវគូសលើប្រអប់ណាមួយខាងក្រោម សម្រាប់ការវាវតម្លៃលើបុគ្គលិករូបនេះ</span>
				</td>
			</tr>
		</thead>
	</table>
		<?php
			if(count($sql_main)>0){
				$sql_sub = $this->db->query("SELECT
												sch_evaoverall_comments.ove_com_id,
												sch_evaoverall_comments.description_overall,
												sch_evaoverall_comments.description_overall_kh,
												sch_evaoverall_comments.isfillout

											FROM
												sch_evaoverall_comments")->result();	
					$td="";
					$i  = 1;
					$tdbreak='';
					
				foreach($sql_sub as $row_sub){
					$other_com="";
					if($row_sub->isfillout==1){
						$other_com="&nbsp;&nbsp;<input type='text' name='ove_comment' id='ove_comment'  style=' padding-top: 2px; width:px;border:none;border-bottom: 1px dotted' value='".$ove_comment."'/>";	
					}
					$checked = '';
					if($ove_com_id == $row_sub->ove_com_id){ 
						$checked .= 'checked="checked"';
					}

					$td.= '<td style="text-align:left;padding-left:12px; padding-top:10px;font-family: Times New Roman; font-size: 12px;padding-bottom: 10px;"'.$tdbreak.'>
									<span><input type="checkbox" id="checkboxid" class="radio CKcheckbox" value="'.$row_sub->ove_com_id.'" '.$checked.' name="CKcheckbox" style="float:left;font-family: Times New Roman; font-size: 12px;"/>&nbsp;&nbsp;'.$i.'-</span>
									<span style="font-family: Times New Roman; font-size: 12px;">'.$row_sub->description_overall.'</span>
									<div style="font-family: khmer mef1;font-size:12px;">'.$row_sub->description_overall_kh.''.$other_com.'</div>
							</td>';
				if($i%4==0){
					$td.= '<tr>';
				}
				$i ++;	
				}
			}	
		?>
	<table border="1" style=" border-collapse: collapse;" width="100%">
		<thead>		
			<tr​​​>
				<th colspan="4" style="border-right: 1px solid;text-align:center;background-color:#ccc;">
					<span style="font-family: Times New Roman;font-size:12px;">OVERALL COMMENTS </span>
					<span style="font-family: khmer mef1;font-size:12px;">មតិយោបល់ជារួម</span>
				</th>
			</tr>
			<tr>
				<?php
					echo $td;
				?>
			</tr>
		</thead>
	</table><br/>

	<table  border="0" width="100%">
		<tr>
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;text-align:justify;width:40px;">Name :</span>
			</td>
				<?php 
					$fullname ='';
					if (isset($gm_emp_id)){
						$fullname = $this->db->query("SELECT CONCAT(last_name,' ',first_name) AS fullname FROM sch_emp_profile WHERE empid='".$gm_emp_id."'")->row()->fullname;
					}
				?>
			<td id="gm_name"  style="width:200px;border: none;border-bottom: 1px dotted;font-family: Times New Roman; font-size: 12px;"><input type="hidden" id="idhidden_gm"><?php echo $fullname;?></td>
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;text-align:justify;">Position :</span>
			</td>
				<?php 
					$position='';
					if (isset($gm_emp_id)){
						$position = $this->db->query("SELECT ep.position FROM sch_emp_profile sep INNER JOIN sch_emp_position ep ON sep.pos_id = ep.posid WHERE sep.empid='".$gm_emp_id."'")->row()->position;
					}
				?>
			<td id="gm_position"  style="width:200px;border: none;border-bottom: 1px dotted;font-family: Times New Roman; font-size: 12px;"><?php echo $position?></td>
			
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;text-align:justify;">Signature :.......................................</span>
			</td>
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;text-align:justify;">Date : </span>
			</td>
			<td>
				<input type="date" id="gm_date"  style="width:100px;border: none;border-bottom: 1px dotted; font-family: Times New Roman; font-size: 12px;" value="<?php echo $gm_date; ?>"/>
			</td>
		</tr>
		</table>
    </div>
<style type="text/css">
	.tdbreak {
		page-break-after: always;
	}
</style>
<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js')?>"></script>	
<script type="text/javascript">
	$(function(){
		 total_rating();
		// read only ============================================
		$('.total_rate').attr("readonly", "readonly");
		$('.total_score').attr("readonly", "readonly");
		$('.total_rating').attr("readonly", "readonly");
		$('#employee').attr("readonly", "readonly");
		$('#employee_name').attr("readonly", "readonly");
		$('#employee_date').attr("readonly", "readonly");
		$('#employee').attr("readonly", "readonly");
		$('#subpervisors_name').attr("readonly", "readonly");
		$('#supervisor_date').attr("readonly", "readonly");
		$('#gm_date').attr("readonly", "readonly");
		$('#subpervisors').attr("readonly", "readonly");
		$('#ove_comment').attr("readonly", "readonly");
		$(".radio_check").prop('disabled','disabled');
		$(".CKcheckbox").prop('disabled','disabled');
		// checkbox only ==========================================
		$("table tr.row_manage_check").each(function () {
		   var tr = $(this).find(".radio_check").size();
		});
		$(".chrating").on('click', function() {	
		  	var cur_row=$(this).closest("tr");
		  	var area_id=$(this).attr("attr_val");
		  	var ch_rating=cur_row.find(".radio_check_"+area_id);
		  	var rating_checked=cur_row.find(".radio_check_"+area_id+":checked");//cur_row.find(".radio_check_1:checked")
		  	if(rating_checked.size()>1){
		  		ch_rating.prop("checked",false);
		  		if($(this).prop("checked")==false){
					$(this).prop("checked",true);
					// alert($(this).val());	
		  		}else{
		  			$(this).prop("checked",false);	
		  		}	  			  			
		  	}
			// total rating  =====================================
			var total_rate = 0;
			$(".chrating:checked").each(function(){
				total_rate += $(this).val() - 0;
			});
			$('.total_score').val(total_rate);
			var total_rating = $('.total_rate').val() - 0 > 0 ? total_rate/$('.total_rate').val() : '';
			$(".total_rating").val(total_rating.toFixed(2));
		});
		
	}); //   function  ============================================
		function total_rating(){
		var total_rate = 0;
			$(".chrating:checked").each(function(){
				total_rate += $(this).val() - 0;
			});
			$('.total_score').val(total_rate);
			var total_rating = $('.total_rate').val() - 0 > 0 ? total_rate/$('.total_rate').val() : '';
			$(".total_rating").val(total_rating.toFixed(2));//val.toFixed(2) //returns 2489.82
	}
</script>