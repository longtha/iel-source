
<style type="text/css">
    #preview td{
        padding: 3px ;
    }
    td img{border:1px solid #CCCCCC; padding: 2px;}
    #preview_wr{
        margin: 10px auto !important;
    }
    #tblmention td,#tblmention th{padding: 3px 5px !important;}
    #tblmention th{width: 30%; text-align: left; text-align:right !important; vertical-align:middle;}

    span.set,.tab_head th,label.control-label{font-family:"Times New Roman", Times, serif !important; font-size: 14px !important;}
    td,th{font-family:"Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important; font-size: 14px;}

</style>
<?php
        $school=$this->db->where('schoolid',$this->session->userdata('schoolid'))->get('sch_school_infor')->row();
            $school_name=$school->name;
            $school_adr=$school->address;
        $contract=$this->db->where('con_id',$eva['con_id'])->get('v_sch_emp_contract')->row();
        
  ?>
<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
        <div id="main_content">
          <div class="row result_info col-xs-10">
                <div class="col-xs-6">
                    <strong>Contract List Preview</strong>
                </div>
                <div class="col-xs-6" style="text-align: right">
                    <span class="top_action_button">    
                        <a href="#" id="export" title="Export">
                            <img src="<?php echo base_url('assets/images/icons/export.png')?>" />
                        </a>
                    </span>
                    <span class="top_action_button">
                        <a href="#"  title="Print">
                            <img id="print" src="<?php echo base_url('assets/images/icons/print.png')?>" />
                        </a>
                    </span>     
                </div>                
          </div>
        </div>
    </div>
</div>

<div class="row" id='export_tap'>
    <div class="col-sm-10" id='preview_wr'>
        <div class="panel panel-default">
            <div class="panel-body">
                 <div class="table-responsive" id="tab_print">
                        <style type="text/css">
                            #preview td{
                                padding: 3px ;
                            }
                            td img{border:1px solid #CCCCCC; padding: 2px;}
                            #preview_wr{
                                margin: 10px auto !important;
                            }
                            #tblmention td,#tblmention th{padding: 3px 5px !important; text-align: left;}
                            .tab_head th,label.control-label{font-family:"Times New Roman", Times, serif !important; font-size: 14px; font-weight: bold !important;}
                            td,th,label.control-label{font-family:"Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important; font-size: 14px;}

                        </style>
                        
                        <div style='width:300px; float:right;'>
                            <img src="<?php echo base_url('assets/images/logo/logo.png')?>" style='width:250px;  height:60px;' />
                            
                        </div>
                        <div style='float:left; '>
                            <div style=' font-weight:bold; text-transform: uppercase;'><?php echo $school_name ?></div>
                            <div style='text-align:center !important; font-weight:bold;'></div>
                        </div>
                        <p style="clear:both"></p>
                        <table align='center'>
                            <thead>
                                <th valign='top' align="center" style='width:100%'><h4 align="center" >CONTRACTS REPORT</h4></th> 
                            </thead>
                        </table></br>
                        <table align='center' id='preview' style='width:100%'>
                                <tr>
                                    <td ><label class="control-label">Employee ID : </label> </td>
                                    <td> <?php echo $contract->empid; ?></td>
                                    <td ><label class="control-label">Contract ID : </label> </td>
                                    <td> <?php echo $contract->contractid; ?></td>
                                    <td><label class="control-label">Start Date : </label></td>
                                    <td> <?php echo $contract->begin_date; ?></td>
                                    <td><label class="control-label">End Date : </label></td>
                                    <td> <?php echo $contract->end_date; ?></td>
                                </tr>          
                        </table><br>
                        <table style='width:100%;' id='tblmention' border='1' cellspacing='0' cellpadding='0'>
                            <thead>
                                <tr class="odd"> 
                                    <th>Contract Duration</th>
                                    <td> <?php echo $contract->duration_type; ?> </td>
                                </tr>
                                <tr class="even"> 
                                    <th>Contract Type</th>
                                    <td> <?php echo $contract->contract_type; ?> </td>
                                </tr>
                                <tr class="odd"> 
                                    <th>Job Type</th>
                                    <td> <?php echo $contract->job_type; ?> </td>
                                </tr>
                                <tr class="even"> 
                                    <th>Contract Status</th>
                                    <td> <?php echo $contract->status; ?> </td>
                                </tr>
                                <tr class="odd"> 
                                    <th>Description</th>
                                    <td> <?php echo $contract->decription; ?> </td>
                                </tr>
                                <tr class="even"> 
                                    <th>Upload</th>
                                    <td>
                                        <img alt="Your uploaded image" src="<?=base_url(). './assets/upload/employees/contracts/' . $uploadInfo['file_name'];?>"> 
                                        <?php echo anchor('./assets/upload/employees/contracts/', 'Download File');?>
        
                            </thead>
                        
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){       
            $("#print").on("click",function(){
                gPrint("tab_print");
            });
        })
        $("#export").on("click",function(e){
                var data=$('.table').attr('border',1);
                    data = $("#export_tap").html().replace(/<img[^>]*>/gi,"");
                var export_data = $("<center><h3 align='center'></h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
                e.preventDefault();
                $('.table').attr('border',0);
        });

    </script>

