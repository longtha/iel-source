<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
	      	<div class="col-xs-6">
		      	<strong>Position Information</strong>
		    </div>
		    <div class="col-xs-6" style="text-align: right">
		      		<center class='exist' style='color:red;'></center>	      		
		    </div>
	      </div>
	      <?php 
	      	$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
	      ?>
	      <!---Start form-->
	      <form id="position_register" method="POST" enctype="multipart/form-data"  action="<?php echo site_url("employee/position/save?&save=add&m=$m&p=$p")?>">
	      		<!-- <div class="row"> -->
	      	<div class="panel panel-default">
	      		<div class="panel-heading">
	     			<h4 class="panel-title">Position Details</h4>
	     		</div>
	     		<div class="panel-body">
	      		<div class="col-sm-6">           
                <div class="panel-body">
                    <div class="form_sep">
                        <label>Position</label>
	     				<input type="text" name="position" id="position" class="form-control parsley-validated" data-required="true" required data-parsley-required-message="Enter Position">
                    </div>
                    <div class="form_sep">
                        <label>Position (Kh)</label>
	     				<input type="text" name="position_kh" id="position_kh" class="form-control">
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6">                          
              <div class="panel-body">
                  <div class="form_sep">
                        <label>Match</label>
						<!-- <input type="text" name="match" id="match" class="form-control"> -->
						<select class="form-control" id='match' name='match' required data-parsley-required-message="Select any position match">
	                    <option value=''>Select match</option>
	                    <?php
	                    foreach ($this->position->getmatch() as $row) {
	                        echo "<option value='$row->match_con_posid'>$row->description</option>";
	                    }
	                    ?>
					</select>
                  </div>
                  <div class="form_sep">
                      <label>Job Description</label>
	     			  <textarea name="description" id="description" class="form-control"></textarea>
                  </div>
              </div> 
            </div>
            <div class="col-sm-12">  
            <div class="panel-body">
                <div class="form_sep">
                 <?php if($this->green->gAction("C")){ ?>
                 <input type='button' class="btn btn-success" name="btnsave" id="btnsave" value='Save'/>
                 <!-- <button class="btn btn-success" id="btnsave" name="btnsave">Save</button> -->
                 <?php } ?>
                </div>                               
            </div>
          </div>
          </div>
          </div>
	     <!-- </div> -->
	      </form>
	      <!--End Form-->
	    </div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#position_register").parsley();
	});
	$('#btnsave').click(function(){
		var position=$('#position').val();
		var posid='';
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/employee/position/validate",    
				data: {'position':position,
					   'posid':posid},
				type: "POST",
				success: function(data){
                  if(data>0)
                  	$('.exist').html('Position has already exist...!');
                 else
                  	$('#position_register').submit();
			}
		});		
	})
</script>