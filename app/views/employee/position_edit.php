<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
	      	<div class="col-xs-6">
		      	<strong>Position Information</strong>
		    </div>
		    <div class="col-xs-6" style="text-align: right">
		      		<center class='exist' style='color:red;'></center> 		
		    </div>
	      </div>
	      <?php 
	      	$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
	      ?>
	      <!---Start form-->
	      <form id="position_register" method="POST" enctype="multipart/form-data"  action="<?php echo site_url("employee/position/save?&save=edit&m=$m&p=$p")?>">
	      		<div class="panel panel-default">
	      		<div class="panel-heading">
	     			<h4 class="panel-title">Position Detail
	     			<label style="float:right !important; font-size:11px !important; color:red;"><?php if($pos_row['last_modified_by']!='') echo "Last Modified Date: ".date_format(date_create($pos_row['last_modified_date']),'d-m-Y H:i:s')." By : $pos_row[last_modified_by]"; ?></label>	
	     			</h4>
	     		</div>
	     		<div class="panel-body">
	      		<div class="col-sm-6">           
                <div class="panel-body">
                    <div class="form_sep">
                        <label>Position</label>
						<input type="hidden" id="posid" name="posid" value="<?php echo $pos_row['posid'];?>">
						<input type="text" name="position" value="<?php echo $pos_row['position'];?>" id="position" class="form-control parsley-validated" data-required="true" required data-parsley-required-message="Enter Position">
                    </div>
                    <div class="form_sep">
                        <label>Position (Kh)</label>
	     				<input type="text" name="position_kh" value="<?php echo $pos_row['position_kh'];?>" id="position_kh" class="form-control">
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6">                          
              <div class="panel-body">
                  <div class="form_sep">
                       <label>Match</label>
						<!-- <input type="text" name="match" value="<?php //echo $pos_row['match_con_posid'];?>" id="match" class="form-control"> -->
						<select class="form-control" id='match' name='match' required data-parsley-required-message="Select any position match">
	                    <option value=''>Select match</option>
	                    <?php
	                        foreach ($this->position->getmatch() as $row) {?>
	                            <option value='<?php echo $row->match_con_posid; ?>' <?php if($row->match_con_posid==$pos_row['match_con_posid']) echo "selected";?>><?php echo $row->description;?></option>	                    		
	                    	<?php echo $row->match_con_posid; ?>
	                    <?php } ?>
	                </select>
                  </div>
                  <div class="form_sep">
                      <label>Job Description</label>
	     			  <textarea name="description" id="description" class="form-control"><?php echo $pos_row['description'];?></textarea>
                  </div>
              </div> 
            </div>
            <div class="col-sm-12">  
            <div class="panel-body">
                <div class="form_sep">
                 <?php if($this->green->gAction("C")){ ?>
                 <input type='button' class="btn btn-success" name="btnsave" id="btnsave" value='Save'/>
                 <!-- <button class="btn btn-success">Save</button> -->
                 <?php } ?>
                </div>                               
            </div>
          </div>
          </div>
          </div>
	      </form>
	      <!--End Form-->
	    </div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#position_register").parsley();
	});
	$('#btnsave').click(function(){
		var position=$('#position').val();
		var posid=$('#posid').val();
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/employee/position/validate",    
				data: {'position':position,
					   'posid':posid},
				type: "POST",
				success: function(data){
                  if(data>0)
                  	$('.exist').html('Position has already exist...!');
                 else
                  	$('#position_register').submit();
			}
		});		
	})
</script>