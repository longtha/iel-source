<?php
     								
	if(isset($_POST['act_empcode_val'])==1){
		$empcode_val = $this->input->post("empcode_val");

		$this->db->select('count(*)');
		$this->db->from('sch_emp_profile');
		$this->db->where('empcode',$empcode_val);
		$count = $this->db ->count_all_results();
		echo $count;
		exit();
	}

	if(isset($_POST['act_idcard_val'])==1){
		$idcard_val = $this->input->post("idcard_val");

		$this->db->select('count(*)');
		$this->db->from('sch_emp_profile');
		$this->db->where('idcard',$idcard_val);
		$count = $this->db ->count_all_results();
		echo $count;
		exit();
	}

	if(isset($_POST['act_employee_type'])==1){
		$post_id = $this->input->post("pos_id");
		$option = "";
		foreach( $this->emp->getposid($post_id)->result() as $posid ){
			$option .='<option value="'.$posid->posid.'">'.$posid->position.'</option>';
		}
		echo $option;
		exit();
	}
	$option ="<option value=''>Select Department</option>";
	foreach( $this->db->get('sch_emp_department')->result() as $dep ){
		$option .='<option value="'.$dep->dep_id.'">'.$dep->department.'</option>';
	}

	$m='';
	$p='';
	if(isset($_GET['m'])){
    	$m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }
?>

<?php
	$dash= array('Full'=>'/system/dashboard',
				'Socail'=>'system/dashboard/view_soc',
				'Health'=>'/system/dashboard/view_health',
				'Employee'=>'/system/dashboard/view_staff',
				'Student'=>'/system/dashboard/view_std');
?>

<style type="text/css">
	.disable{display: none;}
</style>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
	      	<div class="col-xs-6">
		      	<strong>Employee Information</strong>
		    </div>
		    <div class="col-xs-6" style="text-align: right">
		      		      		
		    </div>
	      </div>
	      <!--Start Form Employee---->
	      	<form id="emp_register" method="POST" enctype="multipart/form-data"  action="<?php echo site_url("employee/employee/save?&save=add&m=$m&p=$p")?>">
	      		<div class="row">
	      			<div class="col-sm-6">
	      				<div class="panel panel-default">
	      					<div class="panel-heading">
	      						<h4 class="panel-title">Personal Detail</h4>
	      					</div>
	      					<div class="panel-body">
	      						<div class="form_sep">
	      							<label>Employee ID</label>
	      							<input type="text" data-required="true" name="empcode" value="<?php echo $this->emp->getmaxid(); ?>" id="empcode" class="form-control parsley-validated" required data-parsley-required-message="Enter Employee ID">
	      							<input type="hidden" name="empcode_hidden" id="empcode_hidden" value="<?php echo $this->emp->getmaxid(); ?>">
	      						</div>
	      						<div class="form_sep">
		      						<label>ID Card</label>
		      						<input type="text" name="idcard" id="idcard" class="form-control">
		      					</div>
		      					<div class="form_sep">
			      					<label>Last Name</label>
			      					<input type="text" name="last_name" id="last_name" class="form-control parsley-validated" required data-required="true" data-parsley-required-message="Enter Last Name">
		      					</div>
		      					<div class="form_sep">
			      					<label>First Name</label>
			      					<input type="text" data-requried="true" name="first_name" id="first_name" class="form-control parsley-validated" required data-parsley-required-message="Enter First Name">
		      					</div>
		      					<div class="form_sep">
			      					<label>Last Name(kh)</label>
			      					<input type="text" name="last_name_kh" id="last_name_kh" class="form-control">
		      					</div>
		      					<div class="form_sep">
			      					<label>First Name(kh)</label>
			      					<input type="text" name="first_name_kh" id="first_name_kh" class="form-control">
		      					</div>
		      					<div class="form_sep">
		      						<label>Sex</label>
		      						<select name="sex" id="sex" class="form-control">
		      							<option value="M">Male</option>
		      							<option value="F">Female</option>
		      						</select>
		      					</div>
		      					<div class="form_sep">
	      							<label>Marital Status</label>
	      							<select name="marital_status" id="marital_status" class="form-control">
	      								<option value="S">Single</option>
	      								<option value="M">Marital</option>
	      							</select>
	      						</div>
		      					<div class="form_sep">
	      							<label>Phone</label>
	      							<input type="text" name="phone" id="phone" class="form-control">
	      						</div>
	      						<div class="form_sep">
	      							<label>Email</label>
	      							<input type="email" name="email" id="email" class="form-control">
	      						</div>
		      					<div class="form_sep">
			      					<label>Date of Birth</label>
			      					<div data-date-format="dd-mm-yyyy" class="input-group date dob">
						              <input type="text" value="<?php echo date("d-m-Y");?>" data-type="dateIso" class="form-control" id="dob" name="dob">
						              <span class="input-group-addon"><i class="icon-calendar"></i></span> 
						            </div>
		      					</div>
		      					<div class="form_sep">
			      					<label>Place of Birth</label>
			      					<textarea name="pob" id="pob" class=" form-control"></textarea>
		      					</div>		      					
		      					
	      						<div class="form_sep hide">
	      							<label>Employee Type</label>
	      							<select name="emp_type" id="emp_type" class="form-control">
	      								<option value="S">Office Staff</option>
	      								<option value="T">Teacher</option>
	      							</select>
	      						</div>
	      						<div class="form_sep">
	      							<label>Permanant Address</label>
	      							<textarea name="perm_adr" id="perm_adr" class="form-control">	      								
	      							</textarea>
	      						</div>
	      							      						
	      						<div class="form_sep hide">
	      							<label>Leave School</label>
	      							<select name="leave_school" id="leave_school" class="form-control">
	      								<option value="0">No</option>
	      								<option value="1">Yes</option>
	      							</select>
	      						</div>
	      						<div class="form_sep hide" id="sh_reason">
	      							<label>Leave School Reason</label>
	      							<textarea name="leave_school_reason" id="leave_school_reason" class="form-control"></textarea>
	      						</div>
	      					</div>
	      				</div>
	      			</div>
	      			<div class="col-sm-6">
	      				<div class="panel panel-default">

	      					<div class="panel-heading">
	      						<h4 class="panel-title">Contact Details</h4>
	      					</div>
	      					<div class="panel-body">
	      						
	      						<div class="form_sep">
	      							<label>Hire Date</label>
	      							<div data-date-format="dd-mm-yyyy" class="input-group date resigned_date">
						              <input type="text" value="<?php echo date('d-m-Y');?>" data-type="dateIso" class="form-control" id="employed_date" name="employed_date">
						              <span class="input-group-addon"><i class="icon-calendar"></i></span> 
						            </div>
	      						</div>
		      					
		      					<div class="form_sep">
			      					<label>Nationality</label>
			      					<input type="text" value="Cambodia" name="nationality" id="nationality" class="form-control nationality">
		      					</div>
		      					<div class="form_sep hide">
		      						<label>Foreigner</label>
		      						&nbsp;&nbsp;&nbsp;&nbsp;
		      						<input type="radio" value="1" name="is_foreigner" id="is_foreigner" class="is_foreigner is_yes">
		      						<label>Yes</label>&nbsp;&nbsp;
		      						<input type="radio" value="0" name="is_foreigner" id="is_foreigner" class="is_foreigner is_no" checked/>
		      						<label>No</label>&nbsp;&nbsp;
		      					</div>
	      						
	      						
	      						<div class="form_sep hide">
	      							<label>Village</label>
	      							<input ng-model="village" type="text" name="village" id="village" class="form-control">
	      						</div>
	      						<div class="form_sep  hide">
	      							<label>Khan / Communce</label>
	      							<input ng-model="commune" type="text" name="commune" id="commune" class="form-control">
	      						</div>
	      						<div class="form_sep  hide">
	      							<label>Sankat / District</label>
	      							<input ng-model="district" type="text" name="district" id="district" class="form-control">
	      						</div>
	      						<div class="form_sep  hide">
	      							<label>Province</label>
	      							<input ng-model="province" type="text" name="province" id="province" class="form-control">
	      						</div>
	      						<div class="form_sep  hide">
	      							<label>Zoon</label>
	      							<input type="text" name="zoon" id="zoon" class="form-control">
	      						</div>
	      						<div class="form_sep">
	      							<label>Department</label>
	      							<select class="form-control" name="dep_id" id="dep_id">
	      								<?php echo $option; ?>
	      							</select>
	      						</div>
	      						<div class="form_sep">
	      							<label>Position</label>
	      							<select name="pos_id" id="pos_id" class="form-control">
	      								
	      							</select>
	      						</div>
	      						<div class="form_sep hide">
	      							<label>note</label>
	      							<textarea name="note" id="note" class="form-control"></textarea>
	      						</div>
	      						<div class="form_sep">
	      							<img src="<?php echo site_url('../assets/upload/No_person.jpg') ?>" id="uploadPreview" style='width:120px; height:150px; margin-bottom:15px; border:1px solid #CCCCCC'>
									<input id="uploadImage" type="file" accept="image/gif, image/jpeg, image/jpg, image/png" name="userfile" onchange="PreviewImage();" style="visibility:hidden; display:none;" />
									<input type='button' class="btn btn-success" onclick="$('#uploadImage').click();" value='Browse'/>
		      					</div>
	      					</div>
	      					<div class="panel-heading">
	      						<h4 class="panel-title">Leave Details</h4>
	      					</div>
	      					<div class="panel-body">	
	      						<div class="form_sep">
	      							<label>Khmer New</label>	      							
	      							<input type="text" class="form-control" name="kh_newyear_hol" /> 
	      						</div>
	      						<div class="form_sep">
	      							<label>End of the year</label>	      							
	      							<input type="text" class="form-control" name="endoftheyear_hol"/> 
	      						</div>
	      						<div class="form_sep">
	      							<label>Annual Leave</label>	      							
	      							<input type="text" class="form-control" name="annual_leave"/> 
	      						</div>	      						
	      						<div class="form_sep">
	      							<label>Sick Leave</label>	      							
	      							<input type="text" class="form-control"  name="sick_leave"/> 
	      						</div>
	      						
	      					</div>
	      					<div class="panel-heading">
	      						<h4 class="panel-title">Contract Details</h4>
	      					</div>
	      					<div class="panel-body">
	      						
	      						<div class="form_sep">
	      							<label>Contract Status</label>
	      							<select name="cont_status" class="form-control">
	      								<option value="1">Active</option>
	      							</select>
	      						</div>
	      					</div>


	      					<!-- login -->
	      					<div class="panel-heading">
	      						<h4 class="panel-title">Login Options</h4>
	      					</div>
	      					<div class="panel-body">
	      						<div class="col-sm-6" style="padding: 2px;">
		      						<div class="form_sep">
		      							<label>User Login</label>	      							
		      							<input type="text" class="form-control" name="txtu_name" id="txtu_name" /> 
		      						</div>
									<div class="form_sep">
		      							<label>Password</label>	      							
		      							<!-- <input type="password" class="form-control" name="txtpwd" id="txtpwd" />  -->
		      							<div class="input-group">
		                                    <input type='password' class="form-control" name='txtpwd' id='txtpwd' placeholder="Password" />
		                                    <span class="input-group-btn">
		                                        <button type="button" id="btnShowPassword" class="btn btn-default">
		                                            <span class="glyphicon glyphicon-eye-close"></span>
		                                        </button>
		                                    </span>
                                		</div>
		      						</div>		      						
	      						</div>
	      						<div class="col-sm-6" style="padding: 2px;">
	      							<div class="form_sep">
		      							<label>Role</label>
		      							<select name='cborole' id='cborole' class="form-control">
											<?php
											foreach ($this->role->getallrole() as $role_row) {
												echo "<option value='$role_row->roleid'>$role_row->role</option>";
											}
											?>
										</select>
		      						</div>
		      						<div class="form_sep">
		      							<label>School Level</label>
		      							<select name="cboschlevel[]" id="cboschlevel" class="form-control" multiple="multiple">
		      								<?php foreach ($this->db->get('sch_school_level')->result() as $schl) {
												echo "<option value='$schl->schlevelid'>$schl->sch_level</option>";
											} ?>
		      							</select>
		      						</div>
	      						</div>

							</div>
							<!-- start up -->
							<div class="panel panel-default" style="">
								<div class="panel-heading" style="padding: 5px;">
									<h5 class="panel-title">Startup Page</h5>
								</div>
								<div class="panel-body">
									<div class="col-sm-4" style="padding: 2px;">
										<div class="form_sep">
											<label for="emailField">Dashboard</label>
			      							<select  class="form-control months"  name="dashboard" id="dashboard" >
												<?php foreach ($dash as $key => $value) {
													echo "<option value='$value'>$key</option>";
												} ?>
											</select>
			      						</div>
		      						</div>
		      						<div class="col-sm-4" style="padding: 2px;">
										<div class="form_sep">
											<label>Module</label>
			      							<select  class="form-control months" name="moduleallow[]" id="moduleallow" >
											</select>
			      						</div>
		      						</div>
		      						<div class="col-sm-4" style="padding: 2px;">
										<div class="form_sep">
											<label for="emailField">Def. Page</label>
			      							<select  class="form-control months"  name="defpage" id="defpage" >
											</select>
			      						</div>
		      						</div>
								</div>
	      					</div>
	      					

	      				</div>
	      			</div>
	      		</div>
	      		<div class="row">
      				<div class="col-sm-12">
      					<button type="submit" class="btn btn-success">Save</button>
      				</div>
      			</div>
	      	</form>
	      <!--End Form Employee---->
	    </div>
	</div>
</div>
<!--Modal  -->
<div class="modal fade" id="myModal_Emp_IDCard" data-backdrop=false>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><b>Warning</b></h4>
      </div>
      <div class="modal-body">
        <b class="message-body"></b>
      </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<script type="text/javascript">

function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
    oFReader.onload = function (oFREvent) {
        document.getElementById("uploadPreview").src = oFREvent.target.result;
        document.getElementById("uploadPreview").style.backgroundImage = "none";
    };
}

function LoadModule(){
	$.ajax({
		type : "POST",
		url  : "<?php echo site_url('setting/user/getModule')?>",
		dataType: 'json',
		async: false,
		data: {
			'roleid' : $("#cborole").val()
		},
		success:function(data){
			$('#moduleallow').html(data.op);
			$('#defpage').html(data.oppage);
		}
	});
}//End Load Module

$(function(){

	// get...=====
	LoadModule();
	$(document).on('change','#cborole',function(){
		LoadModule();
	});
	$(document).on('change','#moduleallow',function(){
		$.ajax({
    		type : "POST",
    		url  : "<?php echo site_url('setting/user/getPage')?>",
    		data: {
    			'moduleid' : $("#moduleallow").val()
    		},
    		success:function(data){
    			$('#defpage').html(data);
    		}
    	});
	});


	var showPassword = false;

    $("#btnShowPassword").click( function() {
        if (showPassword) {
            $("#btnShowPassword span:first-child").removeClass("glyphicon-eye-open");
            $("#btnShowPassword span:first-child").addClass("glyphicon-eye-close");
            $("#txtpwd").attr("type", "password");
            showPassword = false;
        } else {
            $("#btnShowPassword span:first-child").removeClass("glyphicon-eye-close");
            $("#btnShowPassword span:first-child").addClass("glyphicon-eye-open");
            $("#txtpwd").attr("type", "text");
            showPassword = true;
        }
        $("#txtpwd").focus();
    });


	$('#emp_register').parsley();
	getPosition();

	$("#dob,#employed_date,#resigned_date").datepicker({
  		language: 'en',
  		pick12HourFormat: true,
  		format:'dd-mm-yyyy'
	});

	$(".nationality").on("change", function(){
		var get_value = $(this).val();
		//alert(get_value);
		validate_foriegn(get_value);
		window.getSelection().removeAllRanges();
	});
	//------is_foreigner------------
	$(".is_foreigner").on("click", function(){
		var get_value = $("#nationality").val();
		validate_foriegn(get_value);
	});

	function validate_foriegn(get_value){
		var get_national = get_value.toLowerCase();
		var is_yes = $(".is_yes").val();
		var is_no = $(".is_no").val();
		if( get_national =="cambodia" || get_national =="cambodian" || get_national == "khmer" || get_national ==""  ) {
			$(".is_no").attr("checked", true);
			$(".is_yes").attr("checked", false);
		}else{
			$(".is_yes").attr("checked", true);
			$(".is_no").attr("checked", false);
		}
	}
	//------Check Duplicate Employee ID-------
	$("#empcode").on("change", function(){
		var empcode_val = $(this).val();
		$.ajax({
			type:"post",
			url:"<?php echo $_SERVER['PHP_SELF']?>",
			data:{
				act_empcode_val:1,
				empcode_val:empcode_val
			},
			success:function(data){
				if( data ==1 ){
					$(".message-body").text("You have enter Duplicate ID, Please try another Employee ID!");
					$("#myModal_Emp_IDCard").modal('show');
					var empcode = $("#empcode_hidden").val();
					$("#empcode").val(empcode).focus();
					return false;
				}
			}
		});
	});
	
	//------ Chage Employee Type-----------
	$("#emp_type").on("change", function(){		
		//getposition();
	});

	function getPosition(selected){
		$.ajax({
			type:"post",
			url:"<?php echo site_url('employee/employee/getPosition'); ?>",
			data:{
				posid:selected
			},
			success: function(data){				
				$("#pos_id").html(data);
			}
		});
	}
	//------Check Duplicate ID Card-----------
	$("#idcard").on("change", function(){
		var idcard_val = $(this).val();
		$.ajax({
			type:"post",
			url:"<?php echo $_SERVER['PHP_SELF'];?>",
			data:{
				act_idcard_val:1,
				idcard_val:idcard_val
			},
			success:function(data){
				if( data ==1 ){
					$(".message-body").text("You have enter ID Card, Please try another ID Card!");
					$("#myModal_Emp_IDCard").modal('show');
					$("#idcard").val('').select();
					return false;
				}
			}
		});
	});

	//----------Validate Leave School----
	$("#leave_school").on("change", function(){
		var get_val = $(this).val();
		if(get_val == 0 ){
			$("#sh_reason").fadeOut("slow");
		}else if(get_val == 1){
			$("#sh_reason").fadeIn("slow");
		}

	});

	$("#village,#commune,#district,#province").on("change", function(){
		perm_adr();
	});
});

function perm_adr(){
	var village = $("#village").val();
	var commune = $("#commune").val();
	var district = $("#district").val();
	var province = $("#province").val();
	var get_val = "";
	
	if( village !="" && commune !=""){
		get_val +=village+", ";
	}else{
		get_val +=village;
	}

	if( commune != "" && district !=""){
		get_val +=commune+", ";
	}else{
		get_val +=commune;
	}

	if( district != "" && province !="" ){
		get_val +=district+", ";
	}else{
		get_val +=district;
	}
	
	if( province != ""){
		get_val +=province;
	}
	$("#perm_adr").val(get_val);
}

</script>