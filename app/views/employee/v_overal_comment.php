<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }

 ?>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
<div class="col-sm-12">
    <div class="row result_info">
        <div class="col-sm-6 "><strong><?php echo $title?></strong></div>
    </div>
    <form enctype="multipart/form-data" id="fschyear">
     <div class="row">
        <div class="col-sm-12">
             <div class="col-sm-6" style="padding-top:25px;">
                <label class="control-label"><span>Overall Comment</span></label>
                <input type="hidden" name="idhidden" id="idhidden" class="form-control  input-xs"/>
                <input type="text" name="overall_comment" id="overall_comment" class="form-control input-xs"/>
            </div>
            <div class="col-sm-6" style="padding-top:25px;">
                <label class="control-label"><span>Overall Comment (KH)</span></label>
                <input type="text" name="overall_comment_kh" id="overall_comment_kh" class="form-control  input-xs"/>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6" style="padding-top:25px;">
                <label class="control-label"><span>Is fillout</span></label>
                <input  type="checkbox" id="checkbox_isfillout" class="radio CKcheckbox" value="1" />
            </div>
        </div>
    </div>
    </form>
</div>
<div class="col-sm-12" style="padding-top:25px;">
     <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <label class="control-label"></label>
                <?php if($this->green->gAction("C")){ ?>
                <input type="submit" id="save" value="Save" class="btn btn-primary"/>
                <?php } ?>
                <input type="button" id="clear" value="Clear" class="btn btn-warning"/>
            </div>
       </div> 
    </div>
    <div class="row result_info">
        <div class="col-sm-6"><strong>List of Overall Comment</strong></div>
    </div>
    <!-- show header -->
    <div class="col-sm-12">
        <meta charset="Utf-8">
         <div class="table-responsive" id="dv-print">
            <table  class="table table-condensed">
                <thead>
                    <tr>
                        <th>N&deg;</th>
                        <th>Overall Comment</th>
                        <th>Overall Comment (KH)</th>
                        <th style="text-align:center;" colspan="2" width="7%">Action</th>
                    </tr>
                </thead>
                <tbody id="show_tbl"></tbody>
            </table>
        </div>
    </div>
</div>
</div>
<!-- dialog delete -->
	<div class="modal" id="myModal_delete" role="dialog" style="top:8%">
        <div class="modal-dialog" style="width:300px">
            <div class="modal-content">
                <div class="modal-header" style="text-align:left; font-size:14px; text-align:center; color:red;">Do you want to delete ? </div>
                <div class="modal-footer">
                    <button type="button" id="delete" data-dismiss="modal" class="btn btn-danger">Delete</button>
                    <button type="button" class=" btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- dialog save -->
    <div class="modal fade" id="myModal_msg">
        <div class="modal-dialog" style="width:400px">
            <div class="modal-content">
                <div class="modal-body">
                    <h5 class="modal-title" id="title_msg" style="text-align:center;"></h5>
                </div>
                <div class="modal-footer" style="display:none" id="div_btn">
                    <button type="button" class=" btn btn-danger" id="close_msg" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
	$(function() {
		//--------- parseley validation -----
	         $('#fschyear').parsley();       
	});
    $(document).ready(function(){
    	   
    	showdata();
    	autoemployeename();
        // check box ==========================================
            $(".CKcheckbox").on('click', function() {
            var box = $(this);
            if (box.is(":checked")) {
                $('.CKcheckbox:checkbox[name="' + box.attr("name") + '"]').not(box).prop('checked',false);
                box.prop("checked",true);
                cmdId = box.val();
            //    alert(cmdId);
            }else{
                box.prop("checked",false);
            }
        });
    	// clear ==============================================
        $("#clear").on("click",function(){
            cleadata();
           // $("#save").val('Save').shw();
           // $("#save").val('Update').hide();
        });
    	// save ===============================================
    	$("#save").on("click",function(){
    		var idhidden=$('#idhidden').val();
    		var overall_comment = $('#overall_comment');
			if(overall_comment.val() ==""){
          		overall_comment.css('border-color', 'red');
          	}else{
    			$.ajax({ 
                type : "POST",
                url  : "<?= site_url('employee/c_overal_comment/save')?>",
                data : {
                	//ove_com_id 		   : $("#ove_com_id").val(), 
                    idhidden           : $("#idhidden").val(),
                    overall_comment    : $("#overall_comment").val(),
                    overall_comment_kh : $("#overall_comment_kh").val(),
                    CKcheckbox         : $(".CKcheckbox:checked").val()
                },
                success:function(data){
                    $('#myModal').modal('hide');
                    $('#div_btn').hide();
                    $('#myModal_msg').modal({show: true});
                    $('#title_msg').html('Save successfull...');
                    $('#title_msg').css('color','blue');
                    setTimeout(function (){ $('#myModal_msg').modal('hide'); }, 1500);
                    showdata();
                    cleadata();
                    $("#save").val('Save');
                }
            });
				overall_comment.css('border-color', '#ccc');
           }  

        });
		//  editdata ======================================================
        $(document).on('click','.edit_data',function(){
            var idhidden = $(this).attr("edit");
            $.ajax({ 
                type : "POST",
                url  : "<?= site_url('employee/c_overal_comment/editdata')?>",
                data : { 
                    idhidden : idhidden 
                },
                success:function(data){

                  var post = data.split('|');
                   	$("#idhidden").val(post[0]);
					$("#overall_comment").val(post[1]);
					$("#overall_comment_kh").val(post[2]);
                    if (post[3]==1){
                        $('.CKcheckbox').prop('checked', true);
                    }else{
                        $('.CKcheckbox').prop('checked', false);
                    }
                    $("#save").val('Update');
                    showdata();
                }
            }); 
        });
		// deletedata ===================================================
        $(document).on('click','.delete_data',function(){
            var deletedata = $(this).attr('delete'); 
            $("#myModal_delete").modal();
            $("#delete").click(function(){
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('employee/c_overal_comment/deletedata');?>",
                    data : { 
                        deletedata : deletedata
                    },
                    success : function(data){ 
                        cleadata();
                        showdata();
                    }
                });
             });
        });
    }); // function ======================================================
		function showdata(){
            var roleid=<?php echo $this->session->userdata('roleid');?>;
            var m=<?php echo $m;?>;
            var p=<?php echo $p;?>;
            $.ajax({ 
                type : "POST",
                url  : "<?= site_url('employee/c_overal_comment/showdata');?>",
                data : {
                    showdata   : 1,
                    roleid:roleid,
                    m:m,
                    p:p
                },
                success:function(data){
                    $("#show_tbl ").html(data);
                }
            });
        }
        // aoutocomeplite ===============================================
		function autoemployeename(){    
		  	var employeename="<?php echo site_url('employee/c_overal_comment/employeename')?>";
		    $("#employee_name").autocomplete({
		      	source: employeename,
		      	minLength:0,
		      	select: function(events,ui) { 

		        var f_id=ui.item.id;
		        //alert(f_id);
		        $("#idhidden").val(f_id); 
		      }           
		    });
		}
		// cleardata ======================================================
        function cleadata(){
        	$("#idhidden").val("");
			$("#overall_comment").val("");
			$("#overall_comment_kh").val("");
			$("#ove_com_id").val("");
            $('.CKcheckbox').attr('checked', false); // Unchecks it
        }
</script>