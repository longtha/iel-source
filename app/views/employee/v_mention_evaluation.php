<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }

 ?>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
<form enctype="multipart/form-data" id="fschyear">
    <div class="col-sm-12">
        <div class="row result_info">
            <div class="col-sm-6 "><strong><?php echo $title?></strong></div>
        </div>

         <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6" style="padding-top:25px;">
                    <label class="control-label"><span>Mention</span></label>
                    <input type="text" name="mantion_eva" id="mantion_eva" class="form-control input-xs  required data-parsley-required-message='Enter name'"/>
                </div>
                <div class="col-sm-6" style="padding-top:25px;">
                    <label class="control-label"><span>Mention (KH)</span></label>
                    <input type="text" name="mantion_eva_kh" id="mantion_eva_kh" class="form-control input-xs"/>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-6" style="padding-top:25px;">
                    <label class="control-label"><span>Description</span></label>
                    <input type="text" name="score_eva" id="score_eva" class="form-control input-xs"/>
                    <input type="hidden" name="idhidden" id="idhidden" class="form-control input-xs"/>
                </div>

                <div class="col-sm-3" style="padding-top:25px;">
                    <label class="control-label"><span>Min Rate</span></label>
                    <input type="text" name="minrate" id="minrate" class="form-control input-xs"/>
                </div>
                <div class="col-sm-3" style="padding-top:25px;">
                    <label class="control-label"><span>Max Rate</span></label>
                    <input type="text" name="maxrate" id="maxrate" class="form-control input-xs"/>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12" style="padding-top:25px;">
         <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <label class="control-label"></label>
                    <?php if($this->green->gAction("C")){ ?>
                    <input type="button" id="save" value="Save" class="btn btn-primary"/>
                     <?php } ?>
                    <input type="button" id="clear" value="Clear" class="btn btn-warning"/>
                </div>
           </div> 
        </div>
        <div class="row result_info">
            <div class="col-sm-6"><strong>List of Evaluation Mention</strong></div>
        </div>
        <!-- show header -->
        <div class="col-sm-12">
            <meta charset="Utf-8">
             <div class="table-responsive" id="dv-print">
                <table  class="table table-condensed">
                    <thead>
                        <tr>
                            <th width="5%">N&deg;</th>
                            <th width="20%">Description</th>
                            <th width="20%">Mention</th>
                            <th width="15%">Mention (KH)</th>
                            <th width="10%">Min Rate</th>
                            <th width="10%">Max Rate</th>
                            <th style="text-align:center;" colspan="2" width="5%">Action</th>
                        </tr>
                    </thead>
                    <tbody id="show_tbl"></tbody>
                </table>
            </div>
        </div>
    </div>
</form>
<!-- dialog delete -->
<div class="modal" id="myModal_delete" role="dialog" style="top:8%">
        <div class="modal-dialog" style="width:300px">
            <div class="modal-content">
                <div class="modal-header" style="text-align:left; font-size:14px; text-align:center; color:red;">Do you want to delete ? </div>
                <div class="modal-footer">
                    <button type="button" id="delete" data-dismiss="modal" class="btn btn-danger">Delete</button>
                    <button type="button" class=" btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- dialog save -->
    <div class="modal fade" id="myModal_msg">
    <div class="modal-dialog" style="width:400px">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="modal-title" id="title_msg" style="text-align:center;"></h5>
            </div>
            <div class="modal-footer" style="display:none" id="div_btn">
                <button type="button" class=" btn btn-danger" id="close_msg" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(function() {
        $('#fschyear').parsley(); 
        showdata();
        //get_group_eva();
        // clear =================================================
        $("#clear").on("click",function(){
            cleadata();
            //$("#save").val('Save').show();
            //$("#save").val('Update').hide();
        });
        // save =================================================
        $("#save").on("click",function(){
            var idhidden=$('#idhidden').val();
            var mantion_eva = $('#mantion_eva');
            if(mantion_eva.val() ==""){
                mantion_eva.css('border-color', 'red');
            }else{
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('employee/c_mention_evaluation/save')?>",
                    data : {
                        idhidden       : $("#idhidden").val(),
                        score_eva      : $("#score_eva").val(),
                        mantion_eva    : $("#mantion_eva").val(),
                        mantion_eva_kh : $("#mantion_eva_kh").val(),
                        minrate        : $("#minrate").val(),
                        maxrate        : $("#maxrate").val()
                    },
                    success:function(data){
                        $('#myModal').modal('hide');
                        $('#div_btn').hide();
                        $('#myModal_msg').modal({show: true});
                        $('#title_msg').html('Save successfull...');
                        $('#title_msg').css('color','blue');
                        setTimeout(function (){ $('#myModal_msg').modal('hide'); }, 1500);
                        showdata();
                        cleadata();
                        $("#save").val('Save');
                    }
                });
                mantion_eva.css('border-color', '#ccc');
            }
        });

        // deletedata ============================================
        $(document).on('click','.delete_data',function(){
            var deletedata = $(this).attr('delete'); 
            $("#myModal_delete").modal();
            $("#delete").click(function(){
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('employee/c_mention_evaluation/deletedata');?>",
                    data : { 
                        deletedata : deletedata
                    },
                    success : function(data){ 
                        cleadata();
                        showdata();
                    }
                });
             });
        });

        //  editdata ==========================================
        $(document).on('click','.edit_data',function(){
            var idhidden = $(this).attr("edit");
            $.ajax({ 
                type : "POST",
                url  : "<?= site_url('employee/c_mention_evaluation/editdata');?>",
                data : { 
                    idhidden : idhidden 
                },
                success:function(data){
                  var post = data.split('|');
                    $("#idhidden").val(post[0]);
                    $("#score_eva").val(post[1]);
                    $("#mantion_eva").val(post[2]);
                    $("#mantion_eva_kh").val(post[3]);
                    $("#minrate").val(post[4]);
                    $("#maxrate").val(post[5]);
                    $("#save").val('Update');
                    showdata();
                }
            }); 
        });
        // number only ==================================================
        $("#minrate").keydown(function (e) {
         if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                  return;
            }
         if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
             e.preventDefault();
            }

        });
        // number only =================================================
        $("#maxrate").keydown(function (e) {
         if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                  return;
            }
         if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
             e.preventDefault();
            }

        });

});  // function  =====================================================
            function showdata(){
                var roleid=<?php echo $this->session->userdata('roleid');?>;
                var m=<?php echo $m;?>;
                var p=<?php echo $p;?>;
                    $.ajax({ 
                        type : "POST",
                        url  : "<?= site_url('employee/c_mention_evaluation/showdata');?>",
                        data : {
                            showdata : 1,
                            roleid:roleid,
                            m:m,
                            p:p
                        },
                        success:function(data){
                            $("#show_tbl ").html(data);
                        }
                    });
            }
            function cleadata(){
                $("#idhidden").val("");
                $("#score_eva").val("");
                $("#mantion_eva").val("");
                $("#mantion_eva_kh").val("");
                $("#minrate").val("");
                $("#maxrate").val("");
            }
</script>