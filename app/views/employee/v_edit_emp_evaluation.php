
<?php 

	$m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }


	if(COUNT($sch_eva_emp_evalutiont) > 0){ 
		foreach($sch_eva_emp_evalutiont as $row_data){ 
				$empid            = $row_data->empid;
				$review_from_date = $row_data->review_from_date;
				$review_to_date   = $row_data->review_to_date;
				$eval_date        = $row_data->eval_date;
				$eval_by_empid    = $row_data->eval_by_empid;
				$sup_comment      = $row_data->sup_comment;
				$sup_comment_date = $row_data->sup_comment_date;
				$emp_comment      = $row_data->emp_comment;
				$emp_comment_date = $row_data->emp_comment_date;
				$gm_comment       = $row_data->gm_comment;
				$ove_comment      = $row_data->ove_comment;
				$gm_emp_id        = $row_data->gm_emp_id;
				$gm_date          = $row_data->gm_date;
				$ove_com_id       = $row_data->ove_com_id;
				$evalid           = $row_data->evalid;
				$typeno           = $row_data->transno;
		}
	}

	$query_emp = $this->db->query("SELECT
							sch_emp_profile.sex,
							sch_emp_profile.pos_id,
							sch_emp_profile.dep_id,
							sch_emp_profile.first_name,
							sch_emp_profile.last_name
						FROM
							sch_emp_profile 
						WHERE empid = '".$empid."'");
		if(COUNT($query_emp) > 0){ 
			foreach($query_emp->result() as $data_emp){ 
				$first_name = $data_emp->first_name;
				$last_name  = $data_emp->last_name; 
				$sex        = $data_emp->sex;
				$dep_id     = $data_emp->dep_id;
				$pos_id     = $data_emp->pos_id;
			}
		}
		$department = $this->db->query("SELECT sch_emp_department.department FROM sch_emp_department WHERE dep_id ='".$dep_id."'")->row()->department;
		$position   = $this->db->query("SELECT sch_emp_position.position FROM sch_emp_position WHERE posid ='".$pos_id."'")->row()->position;								
	
		
?>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
	<table class="control-label " border="0" align="center">
		<tbody>
			<tr>
				
				<td style="text-align:center;">
					<input type="hidden" name="h_typeno" id="h_typeno" class="h_typeno" value="<?=$typeno ?>">
					<span style="font-family: Times New Roman;font-size: 16px;font-weight:bold;"><u>PERFORMANCE & DEVELOPMENT EVALUATION FORM (PDE)</u></span><br/>
					<span style="font-family: khmer mef2;font-size: 16px;">ទំរង់វាយតម្លៃការងារ​ និង​ ការអភិវឌ្ឍន៍</span>
				</td>
			</tr>
		</tbody>
	</table><br/>
	
	<table class="control-label" border="0" width="100%">
		<tbody>
		<tr>
			<td colspan="10">
				<span style="font-family: Times New Roman; font-size: 12px;">(Note: This form is used for any evaluation, end of probation & regular staff)</span><br/>
			</td><br/>
		</tr>
			<input type="hidden" id="idhidden_evalmen" value="<?php echo $evalid ;?>">
		<tr style="text-align:justify;">
			<td colspan="3">
				<span style="font-family: Times New Roman; font-size: 14px;">Employee Name :</span>
			</td>
			<td>
				<input type="hidden" id="idhidden_emp" value="<?php echo $empid;?>"/>
				<input type="tex"  id="emp_name" style="width:160px; border: none;border-bottom: 1px dotted;" value="<?php echo $last_name.' '.$first_name; ?>"/>
			</td>
			<td>
				<span style="font-family: Times New Roman; font-size: 14px;">Sex :</span>
			</td>
			<td>
				<input type="tex"  id="emp_sex" style="width:40px; border: none;border-bottom: 1px dotted;"value="<?php echo $sex; ?>"/>
			</td>
			<td>
				<span style="font-family: Times New Roman; font-size: 14px;">Position :</span>
			</td>
			<td>
				<input type="tex"  id="emp_position" style="width:200px; border: none;border-bottom: 1px dotted;" value="<?php echo $position; ?>" />
			</td>
			<td>
				<span style="font-family: Times New Roman; font-size: 14px;">Department/Unit :</span>
			</td>
			<td>
				<input type="tex"  id="emp_department" style="width:200px; border: none;border-bottom: 1px dotted;" value="<?php echo $department; ?>" />
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<span style="font-family: Times New Roman; font-size: 14px;">Review Period / From :</span>
			</td>
			<td>
				<input type="tex"  id="emp_rev_formdate" style="width:100px; border: none;border-bottom: 1px dotted;" value="<?php echo $review_from_date; ?>">
			</td>
			<td colspan="3">
				<span style="font-family: Times New Roman; font-size: 14px;">Review Period / To :</span>
			</td>
			<td>
				<input type="tex"  id="emp_rev_todate" style="width:100px; border: none;border-bottom: 1px dotted;" value="<?php echo $review_to_date; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<span style="font-family: Times New Roman; font-size: 14px;">Evaluator Name :</span>
			</td>
			<td>
				<input type="hidden" id="idhidden_eval" value="<?php echo $eval_by_empid; ?>">
				<?php 
					$fullname='';
					if (isset($eval_by_empid)){
						$fullname=$this->db->query("SELECT CONCAT(last_name,' ',first_name) AS fullname FROM sch_emp_profile WHERE empid='".$eval_by_empid."'")->row()->fullname;
					}
				?>
				<input type="tex"  id="evaluator_name" style="width:150px; border: none;border-bottom: 1px dotted;" value="<?php echo $fullname; ?>">
			</td>
			<td>
				<span style="font-family: Times New Roman; font-size: 14px;">Sex :</span>
			</td>
			<td>
				<?php
					$sex='';
					if (isset($eval_by_empid)){
						$sex=$this->db->query("SELECT sep.sex FROM sch_emp_profile AS sep WHERE sep.empid='".$eval_by_empid."'")->row()->sex;
					}
				?>
				<input type="tex"  id="evaluator_sex" style="width:50px; border: none;border-bottom: 1px dotted;" value="<?php echo $sex;?>"/>
			</td>
			<td>
				<span style="font-family: Times New Roman; font-size: 14px;">Position :</span>
			</td>
			<td>
				<?php 
					$position='';
					if (isset($eval_by_empid)){
						$position=$this->db->query("SELECT ep.position FROM sch_emp_profile sep INNER JOIN sch_emp_position ep ON sep.pos_id=ep.posid WHERE sep.empid='".$eval_by_empid."'")->row()->position;
					}
				?>
				<input type="tex"  id="evaluator_psition" style="width:200px; border: none;border-bottom: 1px dotted;" value="<?php echo $position; ?>"/>
			</td>
			<td>
				<span style="font-family: Times New Roman; font-size: 14px;">Department :</span>
			</td>
			<td>
				<?php 
					$department='';
					if (isset($eval_by_empid)){
						$department=$this->db->query("SELECT ed.department FROM sch_emp_profile sep INNER JOIN sch_emp_department ed ON sep.dep_id=ed.dep_id WHERE sep.empid='".$eval_by_empid."'")->row()->department;
					}
				?>
				<input type="tex"  id="evaluator_department" style="width:200px; border: none;border-bottom: 1px dotted;" value="<?php echo $department; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<span style="font-family: Times New Roman; font-size: 14px;">Date of Evaluation :</span>
			</td>
			<td>
				<input type="tex"  id="evaluator_ofdate" style="width:100px; border: none;border-bottom: 1px dotted;" value="<?php echo $eval_date; ?>">
			</td>
		</tr>
		</tbody>
	</table><br/>
	<?php 
		$arr_main = array("1"=>"GENERAL EVALUATION",);
		$tr = "";
		$title = "";
		if(count($arr_main)>0){
			foreach($arr_main as $key=>$val){
				$title.='<span style="font-family: Times New Roman;font-size:12px;"style="width:20px;height:20px;font-weight: bold;">I.&nbsp;</b>'.$val.'<b></span>';
				$sql_main = $this->db->query("SELECT
													sch_evamain.main_val_id,
													sch_evamain.description
												FROM
													sch_evamain
												WHERE id_main='".$key."'")->result();
				$ii =0;
				if(count($sql_main)>0){
					foreach($sql_main as $row_main1){
						//$title.='<span style="font-family: Times New Roman;font-size:12px;"><b>&nbsp;&nbsp;&nbsp;</b>'.$row_main1->description.'</span><br/>';
						$tr.='<tr><td colspan="8" style="text-align:left;height: 40px;"><span style="font-family: Times New Roman;font-size:14px;"><b>&nbsp;&nbsp;&nbsp;'.$row_main1->description.'</b></span></td></tr>';
						$sql_sub = $this->db->query("SELECT
														sch_evagroup.group_eval_id,
														sch_evagroup.main_eval_id,
														sch_evagroup.area_eval,
														sch_evagroup.area_eval_kh
													FROM
														sch_evagroup
													WHERE main_eval_id='".$row_main1->main_val_id."'")->result();
						if(count($sql_sub)>0){
							$i = 1;
							foreach($sql_sub as $row_sub){
								$value_textarea = '';
								$value_checked  = '';
								$sql_com = $this->db->query("SELECT `comment`,rate,group_id FROM sch_eva_emp_group WHERE grop_eval_id='".$row_sub->group_eval_id."' AND transno='".$typeno."'")->row();
								if(count($sql_com) > 0){
									$value_textarea = $sql_com->comment;
									$value_checked  = $sql_com->rate; 
								//	$value_group_id = $sql_com->group_id;
								}
								$tr.=  '<tr>
											<td style="text-align:center;">
												<input type="hidden" class="idhidden_geva" id="idhidden_geva" value="'.$row_sub->group_eval_id.'">'.$i.'</td>
											<td style="text-align:left;" class="eval_area">
												<span style="font-family: Times New Roman; font-size: 12px;">'.$row_sub->area_eval.'</span><br>
												<span style="font-family: khmer mef1;font-size: 12px;">'.$row_sub->area_eval_kh.'</span><br>
											</td>
											<td ><textarea class="comments_eval form-control input-xs" id="comments_eval" style="height:40px;">'.$value_textarea.'</textarea><input type="hidden" id="hidden_group_id" value='.(isset($row_sub->group_id) ? $row_sub->group_id : '').' /></td>
									    	<td style="text-align:center"><input type="checkbox" '.($value_checked ==5?"checked":"").' class="radio_check  radio_check_'.$row_sub->group_eval_id.'  radio_check5 chrating" attr_val="'.$row_sub->group_eval_id.'" value="5" name="fooby['.$i.'][]"/></td>
									    	<td style="text-align:center"><input type="checkbox" '.($value_checked ==4?"checked":"").' class="radio_check  radio_check_'.$row_sub->group_eval_id.'  radio_check4 chrating" attr_val="'.$row_sub->group_eval_id.'" value="4" name="fooby['.$i.'][]"/></td>
									    	<td style="text-align:center"><input type="checkbox" '.($value_checked ==3?"checked":"").' class="radio_check  radio_check_'.$row_sub->group_eval_id.'  radio_check3 chrating" attr_val="'.$row_sub->group_eval_id.'" value="3" name="fooby['.$i.'][]"/></td>
									    	<td style="text-align:center"><input type="checkbox" '.($value_checked ==2?"checked":"").' class="radio_check  radio_check_'.$row_sub->group_eval_id.'  radio_check2 chrating" attr_val="'.$row_sub->group_eval_id.'" value="2" name="fooby['.$i.'][]"/></td>
									    	<td style="text-align:center"><input type="checkbox" '.($value_checked ==1?"checked":"").' class="radio_check  radio_check_'.$row_sub->group_eval_id.'  radio_check1 chrating" attr_val="'.$row_sub->group_eval_id.'" value="1" name="fooby['.$i.'][]"/></td>
									    </tr>';
								$i++;
							}
						}
						
					}
				}
			}
		}
	?>
	<table class="table control-label table-hover" id="emp_evalutiont" border="1" width="100%">
		<thead>		
			<tr>
				<th rowspan="2" style="width:5%;border-right: 1px solid;text-align:center;background-color:#ccc;vertical-align: middle;">
					<span style="font-family:Times New Roman;font-size:12px;vertical-align: middle;">N&deg;</span>			
				</th>	
				<th rowspan="2" style="width:35%;border-right: 1px solid;text-align:center; background-color:#ccc;vertical-align: middle;">
					<span style="font-family:Times New Roman;font-size:12px;">AREA</span>
					<span style="font-family:khmer mef1;font-size:12px;">តំបន់</span>				
				</th>	
				<th rowspan="2" style="width:40%;border-right: 1px solid; text-align:center; background-color:#ccc;vertical-align: middle;">
					<span style="font-family:Times New Roman;font-size:12px;">COMMENTS</span>
                    <span style="font-family:khmer mef1;font-size:12px;">មតិយោបល់</span>
				</th>	
				<th colspan="5" style="width:20%;border-right: 1px solid;text-align:center; background-color:#ccc;">
					<span style="font-family:Times New Roman;font-size:12px;">RATING</span><br/>
				</th>
			</tr>
			<tr style=" background-color:#ccc;text-align:center;">
				<th>5</th><th>4</th><th>3</th><th>2</th><th>1</th>
			</tr>
		</thead>
		<tbody>
			<?php
				echo $title.$tr;
			?>
		</tbody>
		<tfoot>
			<tr style="border-right: 1px solid; background-color:#ccc;">
				<td colspan="3"style="text-align:right;font-family: Times New Roman;font-size:12px;font-weight:bold;">
					<span>Total Scores :</span>
				</td>
				 <td colspan="2"><input type="text" style="width:70px;background-color:#ccc;" class="total_score"/></td>
				<td>&nbsp;&nbsp;/&nbsp;&nbsp;</td>
				<td colspan="2">
					<select style="width:70px;background-color:#ccc;" class="total_rate">
						<option selected="selected" value="13">13</option>
						<option value="19">19</option>
					</select>
				</td>
			</tr>
			<tr style="border-right: 1px solid; background-color:#ccc;">
				<td colspan="3" style="text-align:right;font-family: Times New Roman;font-size:12px;font-weight:bold;">
					<span>Total Rating :</span>
				</td>
			<th colspan="5"><input type="text" class="total_rating" style="width:100%; background-color:#ccc;" /></th>
			</tr>			
		</tfoot>
	</table><br/>
	<?php
		$tr = "";
		if(count($sql_main)>0){
			$sql_sub = $this->db->query("SELECT
											sch_evamention.mention_id,
											sch_evamention.score,
											sch_evamention.mention,
											sch_evamention.mention_kh
										FROM
											sch_evamention")->result();
							$td1="";	
							$td="";
							
							foreach($sql_sub as $row_sub){
									$td1.=  "<td style='text-align:center;width:15%;'>".$row_sub->score."</td>";

									$td.= '<td style="text-align:center; width:15%;">
													<div>'.$row_sub->mention.'</div>
													<div>'.$row_sub->mention_kh.'</div>
											</td>';
							}
				}	
	?>
	<table class=" table control-label table-hover" border="1" width="100%">
		<thead>		
			<tr>
				<th colspan="7" style="width: ;border-right: 1px solid;background-color:#ccc;">
					<span style="font-family: Times New Roman;font-size:13px;">Add the scores together and divide by 13 for normal employees and by 19 for management to get the overall performance rating. Below is a table showing you where to tick the overall performance level.</span>
					<span style="font-family: khmer mef1;font-size:11px;">បូកសរុបពិន្ទុទំាងអស់ហើយចែកនិង ១៣ សំរាប់បុគ្គលិកធម្មតា ឬចែកនឹង ១៩ សម្រាប់តួនាទីជាអ្នកគ្រប់គ្រង ដើម្បីឲ្យការវាយតម្លៃជាមធ្យមមួយ។ ខាងក្រោមនេះគឺជាតារាងដែលបង្ហាញអំពីកំរឹតនៃការវាយតម្លៃជារួម៖</span>
				</th>
			</tr>

		</thead>
		<tbody>
			<tr>
				<td style="width:15%; border-right: 1px solid;text-align:center">
					<span style="font-family: Times New Roman;font-size:12px;">Score</span>
					<span style="font-family: khmer mef1;font-size:12px;">ពីន្ទុ</span>
				</td>
				<?php
					echo $td1;
				?>
			</tr>
			<tr>
				<td style="width:15%; border-right: 1px solid;text-align:center">
					<span style="font-family: Times New Roman;font-size:12px;">Level</span>
					<span style="font-family: khmer mef1;font-size:12px;">កំរិត</span>
				</td>
				<?php
					echo $td;
				?>
			</tr>
		</tbody>
	</table>

	<table class="table control-label table-hover" border="1" width="100%">
		<?php
			$arr_main = array("2"=>"PERSONAL DEVELOPMENT PLAN");
			$title1 = "";
			if(count($arr_main)>0){
				foreach($arr_main as $key=>$val){
					$title1.='<span style="font-family: Times New Roman;font-size:12px;"style="width:20px;height:20px;font-weight: bold;">II.&nbsp;</b>'.$val.'<b></span>';
				}
				echo $title1;	
			}
		?>
	</table>
	<table name="tbl_ment"  class=" control-label" border="0" width="100%">
		<tr>
			<td style=" vertical-align: text-bottom;">
				<table  name="tbl1" class=" table control-label" border="1" width="50%">
					<thead>		
						<tr style="background-color:#ccc;">
							<th rowspan="1" style="width: 3%;border-right: 1px solid; background-color:#ccc;">
								<span style="font-family: Times New Roman;font-size:12px;">N&deg;</span>			
							</th>
							<th colspan="1" style="width:50%;border-right: 1px solid;text-align:center;">
								<span style="font-family: Times New Roman;font-size:13px;">Strengths</span>
								<span style="font-family: khmer mef1;font-size:12px;">ចំនុចខ្លាំង</span>
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$sql=$this->db->query("SELECT

														sch_eva_emp_evalutiont.evalid,
														sch_eva_emp_streng.streng_id,
														sch_eva_emp_streng.strength
													FROM
														sch_eva_emp_evalutiont
													INNER JOIN sch_eva_emp_streng ON sch_eva_emp_evalutiont.evalid = sch_eva_emp_streng.evalid
													WHERE sch_eva_emp_evalutiont.evalid= '".$evalid."'");
							
							$tr = "";
							$i = 1;
							if($sql->num_rows() > 0){

								foreach ($sql->result() as $row) {

									$tr .= '<tr>'.

												"<td style='width:5%;background-color:#ccc;'>".$i++."<input type='hidden' id='hidden_streng' value='".$row->streng_id."'/></td>".
												'<td><input type="text" id="strengths " class="strengths form-control "style="width:450px;Importen" value="'.$row->strength.'"/></td>'.													
											'</tr>';
								}
							}
							echo $tr;							
						?>
					</tbody>
				</table>
			</td>
			<td style=" vertical-align: text-bottom;">
				<table name="tbl2" class=" table control-label table-hover" border="1" width="50%">
					<thead>		
						<tr style="background-color:#ccc;">
							<th rowspan="1" style="width: 3%;border-right: 1px solid; background-color:#ccc;">
								<span style="font-family: Times New Roman;font-size:12px;">N&deg;</span>			
							</th>
							<th colspan="1" style="width:50%;border-right: 1px solid;text-align:center;">
								<span style="font-family: Times New Roman;font-size:13px;">Areas of Improvement</span>
								<span style="font-family: khmer mef1;font-size:12px;">ចំនុចដែលត្រូវកែប្រែឲ្យប្រសើរឡើង</span>
							</th>
						</tr>
					</thead>
					<tbody>
						<?php

							$sql=$this->db->query("SELECT
														sch_eva_emp_evalutiont.evalid,
														sch_eva_emp_improvements.improvl_id,
														sch_eva_emp_improvements.improment
													FROM
														sch_eva_emp_evalutiont
													INNER JOIN sch_eva_emp_improvements ON sch_eva_emp_evalutiont.evalid = sch_eva_emp_improvements.evalid
													WHERE sch_eva_emp_evalutiont.evalid= '".$evalid."'");
							
							$tr1 = "";
							$j = 1;
							
							if($sql->num_rows() > 0){

								foreach ($sql->result() as $row) {
									$tr1 .= '<tr>'.
												"<td style='width:5%;background-color:#ccc;'>".$j++." <input type='hidden' id='hidden_improment' value='".$row->improvl_id."'/></td>".
												'<td><input type="text" id="im_proment" class="im_proment  form-control "style="width:450px;Importen" value="'.$row->improment.'"/></td>'.
																											
											'</tr>';
								}

							}
							echo $tr1;							
						?>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
	<table class="control-label " border="0" width="100%">
		<tbody>
			<tr>
				<td colspan="5" class="col-sm-12">
                    <label class="control-label"><span style="font-family: Times New Roman; font-size: 12px;">Supervisor's comments: </span><span style="font-family: khmer mef1;font-size:12px;">យោបល់របស់ប្រធានផ្ទាល់</span></label>
                    <textarea type="text" name="comments_supervisor" id="comments_supervisor" class="form-control input-xs" style="height:70px;"/> <?php echo $sup_comment;?></textarea>
                </td>
			</tr>
			<tr>
				<td  style="padding-top:25px;">
					<span style="font-family: Times New Roman; font-size: 12px;">Supervisor's Name :</span><br/>
					<span style="font-family: khmer mef1;font-size:12px;text-align:justify; font-family: Times New Roman; font-size: 10px;">ឈ្មនោះ និង តួនាទី</span>
				</td>
				<td style="padding-top:25px;">
					<input type="text" id="subpervisors_name" style="width:210px;border:none;border-bottom: 1px dotted" value="<?php echo $fullname;?>"/><br/>  
					<input type="text" id="subpervisors"  style="width:210px;border: none;border-bottom: 1px dotted" value="<?php echo $position;?>"/>
				</td>
				<td style="padding-top:25px;">
					<span style="font-family: Times New Roman; font-size: 12px;">Signature :......................................</span><br/>
					<span style="font-family: khmer mef1;font-size:12px;text-align:justify; font-family: Times New Roman; font-size: 10px;">ហត្ថលេខា</span>
				</td>
				<td style="padding-top:25px;">
					<span style="font-family: Times New Roman; font-size: 12px;">Date :</span><br/>
					<span style="font-family: khmer mef1;font-size:12px;text-align:justify; font-family: Times New Roman; font-size: 10px;">កាលបរិច្ឆេទ</span>
				</td>
				<td>
					<input type="date"  id="supervisor_date" style="width:100px; border: none;border-bottom: 1px dotted;" value="<?php echo $sup_comment_date; ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="5" class="col-sm-12" style="padding-top:25px;">
                    <label class="control-label"><span style="font-family: Times New Roman; font-size: 12px;">Employee's comments: </span><span style="font-family: khmer mef1;font-size:12px;">យោបល់របស់បុគ្គិក</span></label>
                    <textarea type="text" name="comments_employee" id="comments_employee" class="form-control input-xs" style="height:70px;"><?php echo $emp_comment; ?></textarea><br/>
                </td>
			</tr>
			<tr>
				<td>
					<span style="font-family: Times New Roman; font-size: 12px;">Employee's Name :</span><br/>
					<span style="font-family: khmer mef1;font-size:12px;text-align:justify; font-family: Times New Roman; font-size: 10px;">ឈ្មនោះ និង តួនាទី</span>
				</td>
				<td>
					<?php 
						$positions='';
						if (isset($gm_emp_id)){
							$positions = $this->db->query("SELECT ep.position FROM sch_emp_profile sep INNER JOIN sch_emp_position ep ON sep.pos_id = ep.posid WHERE sep.empid='".$empid."'")->row()->position;
						}
					?>
					<input type="text" id="employee_name" style="width:210px;border: none;border-bottom: 1px dotted" value="<?php echo $last_name.' '.$first_name; ?>"/><br/>  
					<input type="text" id="employee"  style="width:210px;border: none;border-bottom: 1px dotted" value="<?php echo $positions; ?>"/><br/>
					
				</td>
				<td>
					<span style="font-family: Times New Roman; font-size: 12px;">Signature :.......................................</span><br/>
					<span style="font-family: khmer mef1;font-size:10px;text-align:justify; font-family: Times New Roman; font-size: 10px;">ហត្ថលេខា</span>
				</td>
				<td>
					<span style="font-family: Times New Roman; font-size: 12px;">Date : </span><br/>
					<span style="font-family: khmer mef1;font-size:10px;text-align:justify; font-family: Times New Roman; font-size: 10px;">កាលបរិច្ឆេទ</span>
				</td>
				<td>
					<input type="text" id="employee_date"  style="width:100px;border: none;border-bottom: 1px dotted" value="<?php echo $emp_comment_date; ?>" /><br/>
				</td>
			</tr>
			<tr>
				<td colspan="5" class="col-sm-12" style="padding-top:25px;">
                    <label class="control-label"><span style="font-family: Times New Roman; font-size: 12px;">General Manager's comments (if any): </span><span style="font-family: khmer mef1;font-size:12px;">យោបល់របស់ប្រធានគ្រប់គ្រងទូទៅ៖</span></label>
                    <textarea type="text" name="comments_manager" id="comments_manager" class="form-control input-xs" style="height:70px;"><?php echo $gm_comment; ?></textarea>
                </td>
			</tr>
		</tbody>
	</table><br/>
	<table class="control-label table-hover" border="0" width="100%">
		<thead>
			<tr>
				<td>
					<span style="font-family: Times New Roman; font-size: 12px;">Next management level has to tick one of the boxes below to show the result of this evaluation. </span>
				    <span style="font-family: khmer mef1;font-size:10px;">ប្រធានបន្ទាប់ទ្រូវគូសលើប្រអប់ណាមួយខាងក្រោម សម្រាប់ការវាវតម្លៃលើបុគ្គលិករូបនេះ</span>
				</td>
			</tr>
		</thead>
	</table><br/>
		<?php
			if(count($sql_main)>0){
				$sql_sub = $this->db->query("SELECT
												sch_evaoverall_comments.ove_com_id,
												sch_evaoverall_comments.description_overall,
												sch_evaoverall_comments.description_overall_kh,
												sch_evaoverall_comments.isfillout

											FROM
												sch_evaoverall_comments")->result();	
					$td="";
					$i  = 1;
					$tdbreak='';
					
				foreach($sql_sub as $row_sub){
					$other_com="";
					if($row_sub->isfillout==1){
						$other_com="&nbsp;&nbsp;<input type='text' name='ove_comment' id='ove_comment' value='".$ove_comment."'/>";	
					}
					$checked = '';
					if($ove_com_id == $row_sub->ove_com_id){ 
						$checked .= 'checked="checked"';
					}

					$td.= '<td style="text-align:left;padding-left:12px; padding-top:10px; padding-bottom: 10px;"'.$tdbreak.'>
									<span><input type="checkbox" id="checkboxid" class="radio CKcheckbox" value="'.$row_sub->ove_com_id.'" '.$checked.' name="CKcheckbox" style="float:left;"/>&nbsp;&nbsp;'.$i.'-</span>
									<span>'.$row_sub->description_overall.'</span>
									<div>'.$row_sub->description_overall_kh.''.$other_com.'</div>
							</td>';
				if($i%4==0){
					$td.= '<tr>';
				}
				$i ++;	
				}
			}	
		?>
	<table class="control-label table-hover" border="1" width="100%">
		<thead>		
			<tr​​​>
				<th colspan="4" style="border-right: 1px solid;text-align:center;background-color:#ccc;">
					<span style="font-family: Times New Roman;font-size:12px;">OVERALL COMMENTS </span>
					<span style="font-family: khmer mef1;font-size:12px;">មតិយោបល់ជារួម</span>
				</th>
			</tr>
			<tr>
				<?php
					echo $td;
				?>
			</tr>
		</thead>
	</table><br/>

	<table class="control-label" border="0" width="100%">
		<tr>
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;text-align:justify;width:40px;">Name :</span>
			</td>
			<td>
				<?php 
					$fullname ='';
					if (isset($gm_emp_id)){
						$fullname = $this->db->query("SELECT CONCAT(last_name,' ',first_name) AS fullname FROM sch_emp_profile WHERE empid='".$gm_emp_id."'")->row()->fullname;
					}
				?>
				<input type="text" id="gm_name"  style="width:200px;border: none;border-bottom: 1px dotted" value="<?php echo $fullname; ?>" />
				<input type="hidden" id="idhidden_gm" value="<?php echo $gm_emp_id;?>">
			</td>
			<td>
				<?php 
					$position='';
					if (isset($gm_emp_id)){
						$position = $this->db->query("SELECT ep.position FROM sch_emp_profile sep INNER JOIN sch_emp_position ep ON sep.pos_id = ep.posid WHERE sep.empid='".$gm_emp_id."'")->row()->position;
					}
				?>
				<span style="font-family: Times New Roman; font-size: 12px;text-align:justify;">Position :</span>
			</td>
			<td>
				<input type="text" id="gm_position"  style="width:200px;border: none;border-bottom: 1px dotted" value="<?php echo $position; ?>" />
			</td>
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;text-align:justify;">Signature :.......................................</span>
			</td>
			<td>
				<span style="font-family: Times New Roman; font-size: 12px;text-align:justify;">Date : </span>
			</td>
			<td>
				<input type="date" id="gm_date"  style="width:100px;border: none;border-bottom: 1px dotted" value="<?php echo $gm_date; ?>" />
			</td>
		</tr>
	</table>
</div>
<div class="col-sm-12" style="padding-top:25px;">
     <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <label class="control-label"></label>
                <?php if($this->green->gAction("U")){ ?>
                <input type="submit" id="save" value="Update" class="btn btn-primary"/>
                <?php } ?>
                <input type="button" id="clear" value="Clear" class="btn btn-warning"/>
            </div>
       </div> 
    </div>
    <!-- dialog save -->
    <div class="modal fade" id="myModal_msg">
        <div class="modal-dialog" style="width:400px">
            <div class="modal-content">
                <div class="modal-body">
                    <h5 class="modal-title" id="title_msg" style="text-align:center;"></h5>
                </div>
                <div class="modal-footer" style="display:none" id="div_btn">
                    <button type="button" class=" btn btn-danger" id="close_msg" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<style type="text/css">
	.tdbreak {
		page-break-after: always;
	}
</style>
<script type="text/javascript">
	$(function(){
		$("#evaluator_ofdate").datepicker({format:'yyyy-mm-dd'});
		$("#emp_rev_formdate").datepicker({format:'yyyy-mm-dd'});
		$("#emp_rev_todate").datepicker({format:'yyyy-mm-dd'});
		$("#employee_date").datepicker({format:'yyyy-mm-dd'});
		$("#supervisor_date").datepicker({format:'yyyy-mm-dd'});
		$("#gm_date").datepicker({format:'yyyy-mm-dd'});
		$('.total_rating').attr("readonly",true);
		$('.total_score').attr("readonly",true);
		autoemplete_gm();
		autoemplete_eval();
		autoemplete_emp();
		total_rating();
		// selec_gm ======================================================
		$('body').delegate('.ui-menu-item', 'click', function(){
			$.ajax({
				url: '<?= site_url("employee/c_emp_evaluation/show_employee") ?>',
				type: 'POST',
				dataType: 'JSON',
				async: false,
				data: {
					id: $("#idhidden_gm").val()
				},
				success: function(data){
					$('#gm_position').attr("readonly",true);
					$('#gm_position').val(data.position);
				},
				error: function(err){
				}
			});
		});	
		// select_eval ===================================================
		$('body').delegate('.ui-menu-item', 'click', function(){
			$.ajax({
				url: '<?= site_url("employee/c_emp_evaluation/show_employee") ?>',
				type: 'POST',
				dataType: 'JSON',
				async: false,
				data: {
					id: $("#idhidden_emp").val()
				},
				success: function(data){
					$('#emp_sex').attr("readonly",true);
					$('#emp_position').attr("readonly",true);
					$('#emp_department').attr("readonly",true);
					$('#employee_name').attr("readonly",true);
					$('#employee').attr("readonly",true);
					$('#emp_sex').val(data.sex);
					$('#emp_position').val(data.position);
					$('#emp_department').val(data.department);
					$('#employee_name').val(data.fullname);
					$('#employee').val(data.position);
				},
				error: function(err){
				}
			});
		});	
		//  select_emp ================================================
		$('body').delegate('.ui-menu-item', 'click', function(){
			$.ajax({
				url: '<?= site_url("employee/c_emp_evaluation/show_employee") ?>',
				type: 'POST',
				dataType: 'JSON',
				async: false,
				data: {
					id: $("#idhidden_eval").val()
				},
				success: function(data){
					$('#evaluator_sex').attr("readonly",true);
					$('#evaluator_psition').attr("readonly",true);
					$('#evaluator_department').attr("readonly",true);
					$('#subpervisors_name').attr("readonly",true);
					$('#subpervisors').attr("readonly",true);
					$('#evaluator_sex').val(data.sex);
					$('#evaluator_psition').val(data.position);
					$('#evaluator_department').val(data.department);
					$('#subpervisors_name').val(data.fullname);
					$('#subpervisors').val(data.position);
				},
				error: function(err){
				}
			});
		});
		$("#clear").on("click",function(){
			cleadata();
		});
		// save =========================================================
		$("#save").on("click",function(){
		 	var arr = [];
		 	$(".strengths").each(function(i){
		 		if($.trim($(this).val()) != ''){
					arr[i]= {'strengths': $(this).val()};	
		 		}
		 	});
		 	var arr1 = [];
		 	$(".im_proment").each(function(j){
		 		if($.trim($(this).val()) != ''){
					arr1[j]= {'im_proment': $(this).val()};	
		 		}
		 	});
			//console.log(arr1);
			//return false;

			var arr2 = [];
		 	$(".comments_eval").each(function(l){
		 		var radio_check = $(this).parent().parent().find('.radio_check:checked').val();	 		
		 		if($.trim($(this).val()) != ''){
		 		var idhidden_geva = $(this).parent().parent().find('.idhidden_geva').val();
					arr2[l]= {'comments_eval': $(this).val(), 'radio_check': radio_check,'idhidden_geva':idhidden_geva};
		 		}
		 	});

    		var idhidden_evalmen=$('#idhidden_evalmen').val();
    		var emp_name = $('#emp_name');
    		var emp_name = $('#evaluator_name');
			if(emp_name.val() ==""){
          		emp_name.css('border-color', 'red');
          	}else{
    			$.ajax({ 
		                type : "POST",
		                url  : "<?= site_url('employee/c_emp_evaluation/save')?>",
		                data : {
		                	idhidden_evalmen   : $("#idhidden_evalmen").val(),
		                	idhidden_emp  	   : $("#idhidden_emp").val(),
		                	emp_rev_formdate   : $("#emp_rev_formdate").val(),
							emp_rev_todate     : $("#emp_rev_todate").val(),
							idhidden_eval      : $("#idhidden_eval").val(),
							evaluator_ofdate   : $("#evaluator_ofdate").val(),
							supervisor_date    : $("#supervisor_date").val(),
							comments_supervisor: $("#comments_supervisor").val(),
		                    supervisor_date    : $("#supervisor_date").val(),
		                    comments_employee  : $("#comments_employee").val(),
		                    employee_date      : $("#employee_date").val(),
		                    comments_manager   : $("#comments_manager").val(),
		                    idhidden_gm        : $("#idhidden_gm").val(),
		                    gm_date            : $("#gm_date").val(),
		                 	ove_comment        : $("#ove_comment").val(),
							CKcheckbox         : $(".CKcheckbox:checked").val(),
		    				radio_check        : $(".radio_check:checked").val(),
		                    arr      		   : arr,
		                    arr1     		   : arr1,
		                    arr2     		   : arr2
                		}, 
		                success:function(data){
		                	cleadata();
		                	//alert(data);
		                	//console.log(data);
		                    $('#myModal').modal('hide');
		                    $('#div_btn').hide();
		                    $('#myModal_msg').modal({show: true});
		                    $('#title_msg').html('Save successfull...');
		                    $('#title_msg').css('color','blue');
		                    setTimeout(function (){ $('#myModal_msg').modal('hide'); }, 1500);
		                }
           		});
				emp_name.css('border-color', '#ccc');
           }
      });

		// checkbox only ==========================================
		$("table tr.row_manage_check").each(function () {
		   var tr = $(this).find(".radio_check").size();
		});
		$(".chrating").on('click', function() {	
		  	var cur_row=$(this).closest("tr");
		  	var area_id=$(this).attr("attr_val");
		  	var ch_rating=cur_row.find(".radio_check_"+area_id);
		  	var rating_checked=cur_row.find(".radio_check_"+area_id+":checked");//cur_row.find(".radio_check_1:checked")
		  	if(rating_checked.size()>1){
		  		ch_rating.prop("checked",false);
		  		if($(this).prop("checked")==false){
					$(this).prop("checked",true);
					// alert($(this).val());	
		  		}else{
		  			$(this).prop("checked",false);	
		  		}	  			  			
		  	}
			// total rating  ===================================
			var total_rate = 0;
			$(".chrating:checked").each(function(){
				total_rate += $(this).val() - 0;
			});
			$('.total_score').val(total_rate);
			var total_rating = $('.total_rate').val() - 0 > 0 ? total_rate/$('.total_rate').val() : '';
			$(".total_rating").val(total_rating.toFixed(2));
		});
		$(".total_rate").on('change', function() {
			total_rating();
		});

		// checkbox oreral comment =================================
		$(".CKcheckbox").on('click', function() {
			var box = $(this);
		  	if (box.is(":checked")) {
		  		$('.CKcheckbox:checkbox[name="' + box.attr("name") + '"]').not(box).prop('checked',false);
		    	box.prop("checked",true);
		    	cmdId = box.val();
		  	}else{
		    	box.prop("checked",false);
		  	}
		});

}); // class 

	function total_rating(){
		var total_rate = 0;
			$(".chrating:checked").each(function(){
				total_rate += $(this).val() - 0;
			});
			$('.total_score').val(total_rate);
			var total_rating = $('.total_rate').val() - 0 > 0 ? total_rate/$('.total_rate').val() : '';
			$(".total_rating").val(total_rating.toFixed(2));//val.toFixed(2) //returns 2489.82
	}


	// aoutocomeplite_emp =========================================
		function autoemplete_emp(){    
		  	var employeename="<?php echo site_url('employee/c_emp_evaluation/employeename')?>";
		    $("#emp_name").autocomplete({
		      	source: employeename,
		      	minLength:0,
		      	select: function(events,ui) { 
		        var f_id=ui.item.id;		        
		        $("#idhidden_emp").val(f_id); 
		      }           
		    });
		}

	// aoutocomeplite_eval =========================================
		function autoemplete_eval(){    
		  	var employeename="<?php echo site_url('employee/c_emp_evaluation/employeename')?>";
		    $("#evaluator_name").autocomplete({
		      	source: employeename,
		      	minLength:0,
		      	select: function(events,ui) { 
		        var f_id=ui.item.id;		        
		        $("#idhidden_eval").val(f_id); 
		      }           
		    });
		}

	// autocomplete_gm ==============================================
		function autoemplete_gm(){    
		  	var employeename="<?php echo site_url('employee/c_emp_evaluation/employeename')?>";
		    $("#gm_name").autocomplete({
		      	source: employeename,
		      	minLength:0,
		      	select: function(events,ui) { 
		        var f_id=ui.item.id;		        
		        $("#idhidden_gm").val(f_id); 
		      }           
		    });
		}

	// clrea data ===================================================
		function cleadata(){
			$("#emp_name").val("");
			$("#emp_sex").val("");
			$("#emp_position").val("");
			$("#emp_department").val("");
			$("#emp_rev_formdate").val("");
			$("#emp_rev_todate").val("");
			$("#evaluator_name").val("");
			$("#evaluator_sex").val("");
			$("#evaluator_psition").val("");
			$("#evaluator_department").val("");
			$("#evaluator_ofdate").val("");
			$(".comments_eval").val("");
			$('.radio_check').attr('checked', false); // Unchecks it
			$('.radio').attr('checked', false); // Unchecks it
			$(".total_score").val("");
			$(".total_rating").val("");
			$(".strengths").val("");
			$(".im_proment").val("");
			$("#comments_supervisor").val("");
			$("#subpervisors_name").val("");
			$("#subpervisors").val("");
			$("#supervisor_date").val("");
			$("#comments_employee").val("");
			$("#employee_name").val("");
			$("#employee").val("");
			$("#employee_date").val("");
			$("#comments_manager").val("");
			$("#ove_comment").val("");
			$("#gm_name").val("");
			$("#gm_position").val("");
			$("#gm_date").val("");
		}

		function edit_emp(){ 
			$.ajax({ 
				type : 'POST',
				url  : "<?= site_url('employee/c_emp_evaluation/edit')?>",
				dataType : 'json',
				async : false,
				data : { },
				success:function(data){
					console.log(data); 

				}
			});
		}
</script>