<div class="container-fluid">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
                <strong>Location</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add New">
                  <img src="<?= base_url('assets/images/icons/add.png') ?>">
               </a>
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>
   <div class="collapse in" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('setup/location/save') ?>" id="f_save">
         
         <div class="col-sm-3">
            <div class="form-group">
               <label class="" for="location_type">Location Type</label>
               <select name="location_type" id="location_type" class="form-control" data-parsley-required="true" data-parsley-required-message="This field require">
                  <option value="1">Province</option>
                  <option value="2">District</option>
                  <option value="3">Commune</option>
                  <option value="4">Village</option>
               </select>
               <input type="hidden" name="location_id" id="location_id" class="location_id">
            </div>
         </div>
         <div class="col-sm-3">
            <div class="form-group">
               <label class="" for="province_id">Province<span class="pro_dis" style="color:red; display:none;">*</span></label>
               <select name="province_id" id="province_id" class="form-control" placeholder="Bus Way Type" data-parsley-required-message="This field require">
                  <option value=""></option>
                  
               </select>
            </div>
         </div>

         <div class="col-sm-3">
            <div class="form-group">
               <label class="" for="district_id">District<span class="dis_riq" style="color:red; display:none;">*</span></label>
               <select name="district_id" id="district_id" class="form-control" data-parsley-required-message="This field require">
               </select>
            </div>
         </div>
         <div class="col-sm-3">
            <div class="form-group">
               <label class="" for="commune_id">Commune<span class="dis_riq" style="color:red; display:none;">*</span></label>
               <select name="commune_id" id="commune_id" class="form-control" data-parsley-required-message="This field require">
               </select>
            </div>
         </div>
         <div class="col-sm-3">
            <div class="form-group">
               <label class="" for="type_name">Name<span style="color:red;">*</span></label>
               <input required name="type_name" id="type_name" class="form-control" data-parsley-required="true" data-parsley-required-message="This field require">
            </div>
         </div>

         <div class="col-sm-12 col-sm-offset-0">
            <div class="form-group">           
               <button type="button" class="btn btn-primary btn-sm" name="save" id="save" value="save">Save</button>
               <button type="button" class="btn btn-warning btn-sm" name="clear" id="clear">Clear</button>
            </div>
         </div>

         <div class="row">
            <div class="col-sm-12">
               <div class="col-sm-12" style="border-bottom: 1px solid #CCC;">&nbsp;</div>
            </div>             
         </div><br>
      </form>      
   </div>

    
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <table border="0"​ align="center" id="location_list" class="table table-hover">
                  <thead>
                     <tr>
                        <th>Province</th>
                        <th>District</th>
                        <th>Commune</th>
                        <th>Village</th>
                        <th style="width: 10%;text-align: center;vertical-align: middle;">Action</th>
                     </tr>
                     <tr>
                        <td>
                           <select type="text" name="s_province_id" id="s_province_id" class="form-control" placeholder="Search province" aria-describedby="sizing-addon2" data-toggle="tooltip" data-placement="top" title="Search provicne...">
                           <option value="">Search province</option>
                           <?php
                           $proval = $this->m->getprovince(); 
                           if(count($proval)>0){
                              foreach ($proval as $pro) {
                                 echo '<option value="'.$pro->loc_id.'">'.$pro->name.'</option>';
                              }
                           }
                           ?>            
                        </select>
                        </td>
                        <td>
                           <select type="text" name="s_district_id" id="s_district_id" class="form-control" placeholder="Search district" aria-describedby="sizing-addon2" data-toggle="tooltip" data-placement="top" title="Search district...">
                              <option value="">Search district</option>
                           </select>
                        </td>
                        <td>
                           <select type="text" name="s_commune_id" id="s_commune_id" class="form-control" placeholder="Search commune" aria-describedby="sizing-addon2" data-toggle="tooltip" data-placement="top" title="Search commune...">
                              <option value="">Search commune</option>
                           </select>
                        </td>
                        <td>
                           <select type="text" name="s_village_id" id="s_village_id" class="form-control" placeholder="Search village" aria-describedby="sizing-addon2" data-toggle="tooltip" data-placement="top" title="Search village...">
                              <option value="">Search village</option>
                           </select>
                        </td>
                     </tr>
                  </thead>
                
               <tbody>

               </tbody>
               <tfoot>
                  
               </tfoot>
            </table>         
         </div>
      </div>
   </div>

</div>

<script type="text/javascript">
   $(function(){

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });
      clear();
      getprovince();
      grid();
      $('#s_province_id').change(function () {
            var province_id = $(this).val();
            if (province_id != "") {
                sgetdistrict(province_id);                
            } else {
                $('#s_district_id').html("");
                $('#s_commune_id').html("");
            }
      });
      $('#s_district_id').change(function () {
            var province_id = $('#s_province_id').val();
            var district_id = $(this).val();
            if (district_id != "") {
                sgetcommune(province_id,district_id);                
            } else {
                $('#s_commune_id').html("");
            }
      });
      $('#s_commune_id').change(function () {
            var province_id = $('#s_province_id').val();
            var district_id = $('#s_district_id').val();
            var commune_id = $(this).val();
            if (commune_id != "") {
                sgetvillage(province_id, district_id, commune_id);                
            } else {
                $('#s_village_id').html("");
            }
      });
      $('#province_id').change(function () {
            var province_id = $(this).val();
            if (province_id != "") {
                getdistrict(province_id);                
            } else {
                $('#district_id').html("");
                $('#commune_id').html("");
            }
      });
      $('#district_id').change(function () {
            var province_id = $('#province_id').val();
            var district_id = $(this).val();
            if (district_id != "") {
                getcommune(province_id,district_id);                
            } else {
                $('#commune_id').html("");
            }
      });
      
      // save =======      
      $('body').delegate('#save', 'click', function(){
         var location_id = $('#location_id').val();
         var location_type = $('#location_type').val();
         var province_id = $('#province_id').val();
         var district_id = $('#district_id').val();
         var commune_id  = $('#commune_id').val();
         var village_id  = $('#village_id').val();
         var type_name  = $('#type_name').val();

         var autho = 1;
         if(location_type =="1"){
            autho = 1;
         }
         if(location_type =="2"){
            if(province_id=="" || !province_id){
               toastr["warning"]('Please select province!');
               autho = 0;
            }
         }
         if(location_type =="3"){
            if(district_id=="" || !district_id){
               toastr["warning"]('Please select district!');
               autho = 0;
            }
         }
         if(location_type =="4"){
            if(commune_id=="" || !commune_id){
               toastr["warning"]('Please select commune!');
               autho = 0;
            }
         }
         if(autho >0){
            if($('#f_save').parsley().validate()){
               $.ajax({
                  url: '<?= site_url('setup/location/save') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     location_id: location_id,
                     location_type: location_type,
                     province_id: province_id,
                     district_id: district_id,
                     commune_id: commune_id,
                     village_id: village_id,
                     type_name: type_name                  
                  },
                  success: function(data){
                     if(data == 1){
                        toastr["success"]('Success!');
                        clear();
                        $('#save').html('Save');
                        grid();
                        getprovince();
                        getdistrict(province_id);
                        getcommune(province_id,district_id);
                     }else{
                        toastr["warning"]('Duplicate data!');
                     }         
                  
                  },
                  error: function() {

                  }
               });
            }/////// fsave \\\\\\\\\\\\\\\\\\\\\\\
         }//////////////////autho\\\\\\\\\\\\\\\\\\

      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#busno').select();
         $('#save').html('Save');
         $('#f_save').parsley().destroy();
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#busno').select();
         $('#save').html('Save');
      });

      // edit ========
      $('body').delegate('.edit', 'click', function(){
         var loc_id = $(this).data('locid');
         var loc_type = $(this).data('typeid');

         $.ajax({
            url: '<?= site_url('setup/location/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){

            },
            data: {
               loc_id : loc_id,
               loc_type : loc_type
            },
            success: function(data){
               $('#location_id').val(loc_id);
               $('#location_type').val(loc_type);
               $('#province_id').html(data.province);
               $('#district_id').html(data.district);
               $('#commune_id').html(data.commune);
               $('#village_id').val(data.Village);
               $('#type_name').val(data.loc_name);

               $('#save').html('Update'); 
               
               $('#collapseExample').removeAttr('style');               
               $('#collapseExample').attr('aria-expanded', 'true');
               $('#collapseExample').addClass('in'); 

               $('#f_save').parsley().destroy();        
            
            },
            error: function() {

            }
         });         
         
      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var loc_id = $(this).data('locid');
         var loc_type = $(this).data('typeid');

         if(window.confirm('Are your sure to delete?')){
            $.ajax({
               url: '<?= site_url('setup/location/delete') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  loc_id: loc_id,
                  loc_type: loc_type
               },
               success: function(data){
                  if(data.msg = 'deleted'){
                     toastr["success"]("Data deleted!");
                     grid();
                  }else{
                     toastr["warning"]("Can't delete!");
                  }
               },
               error: function() {

               }
            });
         }
        
         
      });

      // init. =====      

      // delete ========
      $('body').delegate('#s_province_id, #s_district_id, #s_commune_id, #s_village_id', 'change', function(){
         grid();
      });
      

   }); // ready ========

   // clear =======
   function clear(){
      $('#type_name').val('');
      $('#location_id').val('');
   }
   function getprovince(province_id) {
        $.ajax({
            url: '<?= site_url('setup/location/cgetprovince') ?>',
            data: {'province_id': province_id},
            async: false,
            type: "post",
            success: function (data) {
               $('#province_id').html(data.province);
               //$('#s_province_id').html(data.province);
               var province_id = $('#province_id').val();
               getdistrict(province_id);
               //var district_id = $('#district_id').val();
               //getcommune(province_id, district_id);
            }
        });
   }
   function getdistrict(province_id) {
        $.ajax({
            url: '<?= site_url('setup/location/cgetdistrict') ?>',
            data: {'province_id': province_id},
            async: false,
            type: "post",
            success: function (data) {
               $('#district_id').html(data.district);
               var district_id = $('#district_id').val();
               getcommune(province_id, district_id);
            }
        });
   }
   function sgetdistrict(province_id) {
        $.ajax({
            url: '<?= site_url('setup/location/cgetdistrict') ?>',
            data: {'province_id': province_id},
            async: false,
            type: "post",
            success: function (data) {
               $('#s_district_id').html(data.district);
               var district_id = $('#s_district_id').val();
               sgetcommune(province_id, district_id);
            }
        });
   }
   function getcommune(province_id, district_id) {
        $.ajax({
            url: '<?= site_url('setup/location/cgetcommune') ?>',
            data: {'province_id': province_id, 'district_id': district_id},
            async: false,
            type: "post",
            success: function (data) {
                $('#commune_id').html(data.commune);
            }
        });
   }
   function sgetcommune(province_id, district_id) {
        $.ajax({
            url: '<?= site_url('setup/location/cgetcommune') ?>',
            data: {'province_id': province_id, 'district_id': district_id},
            async: false,
            type: "post",
            success: function (data) {
               var province_id = $('#s_province_id').val();
               var district_id = $('#s_district_id').val();
               $('#s_commune_id').html(data.commune); 
               var commune_id = $('#s_commune_id').val();
               sgetvillage(province_id, district_id,commune_id);            
            }
        });

   }
   function sgetvillage(province_id, district_id,commune_id) {
        $.ajax({
            url: '<?= site_url('setup/location/cgetvillage') ?>',
            data: {'province_id': province_id, 'district_id': district_id,'commune_id':commune_id},
            async: false,
            type: "post",
            success: function (data) {
                $('#s_village_id').html(data.village);               
            }
        });
   }
   // grid =========
   function grid(){
      $.ajax({
         url: '<?= site_url('setup/location/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            s_province_id: $('#s_province_id').val(),
            s_district_id: $('#s_district_id').val(),
            s_commune_id : $('#s_commune_id').val(),
            s_village_id : $('#s_village_id').val()
         },
         success: function(data) {
            $('#location_list tbody').html(data.location);
         },
         error: function() {

         }
      });
   }
</script>