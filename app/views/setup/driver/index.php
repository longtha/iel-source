<div class="container-fluid">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
                <strong>Driver</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add New">
                  <img src="<?= base_url('assets/images/icons/add.png') ?>">
               </a>
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('setup/area/save') ?>" id="f_save">
         <input type="hidden" name="driverid" id="driverid">

         <div class="col-sm-3">
           <div class="form-group">           
               <label class="" for="driver_name">Driver Name<span style="color:red">*</span></label>
               <input type="text" name="driver_name" id="driver_name" class="form-control" placeholder="Driver Name" data-parsley-required="true" data-parsley-required-message="This field require">
            </div>
         </div>

         <div class="col-sm-3">
           <div class="form-group">           
               <label class="" for="busid">Bus<span style="color:red">*</span></label>
               <select name="busid" id="busid" class="form-control" data-parsley-required="true" data-parsley-required-message="This field require">
                  <?php
                     $opt = '';
                     $opt .= '<option></option>';
                     foreach ($this->b->fBus() as $row) {
                        $opt .= '<option value="'.$row->busid.'">'.$row->busno.'</option>';
                     }

                     echo $opt;
                  ?>
               </select>
            </div>
         </div>

         <div class="col-sm-3">
            <div class="form-group">
               <label class="" for="tel">Phone<span style="color:red">*</span></label>
               <input type="text" name="tel" id="tel" class="form-control" placeholder="Phone" data-parsley-required="true" data-parsley-required-message="This field require">
            </div>
         </div>

         <div class="col-sm-3">
            <div class="form-group">
               <label class="" for="address">Address<span style="color:red"></span></label>
               <textarea rows="3" name="address" id="address" class="form-control" placeholder="Address"></textarea>
            </div>
         </div>

         <div class="col-sm-5 col-sm-offset-4">
            <div class="form-group">           
               <button type="button" class="btn btn-primary btn-sm" name="save" id="save" value="save">Save</button>
               <button type="button" class="btn btn-warning btn-sm" name="clear" id="clear">Clear</button>
            </div>
         </div>
      </form>

      <div class="row">
         <div class="col-sm-12">
            <div class="col-sm-12" style="border-bottom: 1px solid #CCC;">&nbsp;</div>
         </div>             
      </div>
   </div><br>

   <div class="row">
      <div class="col-sm-3">
           <input type="text" name="driver_search" id="driver_search" class="form-control" placeholder="Search..." aria-describedby="sizing-addon2" data-toggle="tooltip" data-placement="top" title="Search driver...">
      </div>             
   </div>     
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <table border="0"​ align="center" id="driver_list" class="table table-hover">
                <thead>
                     <tr>
                        <th style="width: 10%;">No</th>
                        <th style="width: 20%;">Driver Name</th>
                        <th style="width: 20%;">Phone</th>
                        <th style="width: 20%;">Address</th>
                        <th style="width: 10%;">Bus Name</th>
                        <th style="width: 10%;text-align: center;vertical-align: middle;">Action</th>
                     </tr>
                </thead>
                
               <tbody>

               </tbody>
               <tfoot>
                  
               </tfoot>
            </table>         
         </div>
      </div>
   </div>

</div>

<script type="text/javascript">
   $(function(){

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#f_save').parsley().destroy();
      });

      // save =======      
      $('body').delegate('#save', 'click', function(){
         if($('#f_save').parsley().validate()){
            $.ajax({
               url: '<?= site_url('setup/driver/save') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  driverid: $('#driverid').val(),
                  busid: $('#busid').val(),                  
                  driver_name: $('#driver_name').val(),
                  tel: $('#tel').val(),
                  address: $('#address').val()
               },
               success: function(data){
                  if(data == 1){
                     toastr["success"]('Success!');
                     clear();
                     $('#save').html('Save');
                     grid();
                  }else{
                     toastr["warning"]('Duplicate!');
                  }         
               
               },
               error: function() {

               }
            });
         }
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#driver_name').select();
         $('#save').html('Save');
      });

      // edit ========
      $('body').delegate('.edit', 'click', function(){
         var driverid = $(this).data('id');

         $.ajax({
            url: '<?= site_url('setup/driver/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){

            },
            data: {
               driverid: driverid
            },
            success: function(data){
               $('#driverid').val(data.driverid);
               $('#driver_name').val(data.driver_name);
               $('#busid').val(data.busid);               
               $('#tel').val(data.tel);
               $('#address').val(data.address);
               
               $('#save').html('Update'); 
               
               $('#collapseExample').removeAttr('style');               
               $('#collapseExample').attr('aria-expanded', 'true');
               $('#collapseExample').addClass('in');

               $('#f_save').parsley().destroy();         
            
            },
            error: function() {

            }
         });         
         
      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var driverid = $(this).data('id') - 0;

         // if(areaid == $(this).data('ba_id') - 0){
         //    toastr["warning"]("Can't delete! data in process...");
         // }else{
            if(window.confirm('Are your sure to delete?')){
               $.ajax({
                  url: '<?= site_url('setup/driver/delete') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     driverid: driverid
                  },
                  success: function(data){
                     if(data.msg = 'deleted'){
                        toastr["success"]("Data deleted!");
                        grid();
                     }else{
                        toastr["warning"]("Can't delete!");
                     }               
                     
                  },
                  error: function() {

                  }
               });
            }
         // }
         
      });

      // init. =====
      grid();

      // delete ========
      $('body').delegate('#driver_search', 'keyup', function(){
         grid();
      });

   }); // ready ========

   // clear =======
   function clear(){
      $('#driverid').val('');
      $('#busid').val('');
      $('#driver_name').val('');
      $('#tel').val('');
      $('#address').val('');      
   }

   // grid =========
   function grid(){
      $.ajax({
         url: '<?= site_url('setup/driver/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            search: $('#driver_search').val()
         },
         success: function(data) {

            var tr = '';
            var total = '';

            if(data.result.length > 0){
               $.each(data.result, function(i, row){
                  tr += '<tr>'+
                           '<td>'+ (i + 1) +'</td>'+
                           '<td>'+ (row.driver_name != null ? row.driver_name : '') +'</td>'+
                           '<td>'+ (row.tel != null ? row.tel : '') +'</td>'+
                           '<td>'+ (row.address != null ? row.address : '') +'</td>'+
                           '<td>'+ (row.busno != null ? row.busno : '') +'</td>'+                           
                           '<td style="text-align: center;">\
                              <a href="javascript:;" class="edit" data-id="'+ row.driverid +'" data-toggle="tooltip" data-placement="left" title="Edit">\
                                 <img src="<?= base_url('assets/images/icons/edit.png') ?>" style="width: 16px;height: 16px;">\
                              </a> | \
                              <a href="javascript:;" class="delete" data-id="'+ row.driverid +'" data-ba_id="'+ 'row.driverid' +'" data-toggle="tooltip" data-placement="right" title="Delete">\
                                 <img src="<?= base_url('assets/images/icons/delete.png') ?>">\
                              </a>\
                           </td>'+                           
                        '</tr>';
               });

               total += '<tr>'+
                           '<td colspan="6" style="padding-top: 15px;">Total:<span style="font-weight: bold;">&nbsp;&nbsp;'+ data.totalRecord +'&nbsp;</span><span>Records</span></td>'+
                        '</tr>';

            }else{
               tr += '<tr><td colspan="6" style="text-align: center;font-weight: bold;">No Results</td></tr>';
               total += '';
            }

            $('#driver_list tbody').html(tr);
            $('#driver_list tfoot').html(total);
            
         },
         error: function() {

         }
      });
   }
</script>