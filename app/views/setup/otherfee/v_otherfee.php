<div class="class="container-fluid">     
   <div class="row">
      <div class="col-sm-12">
         <div class="result_info">
            <div class="col-sm-6">
                <strong>Fee Name</strong>  
            </div>
            <div class="col-sm-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add New">
                  <img src="<?= base_url('assets/images/icons/add.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('setup/otherfee/save') ?>" id="f_save">
         <input type="hidden" name="otherfeeid" id="otherfeeid">
         <div class="row">
           <div class="col-sm-6">           
               <label class="" for="otherfee">Fee Name<span style="color:red">*</span></label>
               <input type="text" name="otherfee" id="otherfee" class="form-control" placeholder="Fee Name" data-parsley-required="true" data-parsley-required-message="This field require">
            </div>
            <div class="col-sm-6"> 
               <label class="" for="prices">Prices</label>  
               <input type="text" name="prices" id="prices"​ onkeypress='return isNumberKey(event);' class="form-control" style=" text-align : right" placeholder="0">
            </div>         
         </div><br>

         <div class="row">
            <div class="col-sm-12">           
               <button type="button" class="btn btn-primary btn-sm" name="save" id="save" value="save">Save</button>
               <button type="button" class="btn btn-warning btn-sm" name="clear" id="clear">Clear</button>
            </div>
         </div>
      </form>

      <div class="row">
         <div class="col-sm-12">
            <div class="col-sm-12" style="border-bottom: 1px solid #CCC;">
               &nbsp;
            </div>
         </div>             
      </div>
   </div><br>

   <div class="row">
      <div class="col-sm-3">
         <div class="input-group">           
           <input type="text" name="Other_search" id="Other_search" class="form-control" placeholder="Search..." aria-describedby="sizing-addon2" data-toggle="tooltip" data-placement="top" title="Search Other Fee...">
           <span class="input-group-btn">
              <button id="refresh" class="btn btn-default" type="button" data-toggle="tooltip" data-placement="top" title="Refresh..."><i class="glyphicon glyphicon-refresh"></i></button>
           </span>
         </div>
      </div>             
   </div>     
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <table width="100%" border="0"​ align="center" id="bus_list" class="table table-hover">
                <thead>
                     <tr>
                        <th style="width: 5%;">No</th>
                        <th style="width: 60%;">Fee Name</th>
                        <th style="width: 20%;">Prices</th>
                        <th style="width: 5%;text-align: right;vertical-align: middle;">Action</th>
                     </tr>
                </thead>
                
               <tbody>

               </tbody>
               <tfoot>
                  
               </tfoot>
            </table>         
         </div>
      </div>
   </div>

</div>

<script type="text/javascript">
   $(function(){

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // save =======      
      $('body').delegate('#save', 'click', function(){
         if($('#f_save').parsley().validate()){
            $.ajax({
               url: '<?= site_url('setup/otherfee/save') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  otherfeeid: $('#otherfeeid').val(),
                  otherfee: $('#otherfee').val(),
                  prices: $('#prices').val(),
                  
               },
               success: function(data){
                  //  alert(data);
                  if(data == 1){
                     toastr["success"]('Success!');
                     clear();
                     $('#save').html('Save');
                     gatedata();
                     clear();
                  }else{
                     toastr["warning"]('Duplicate!');
                  }         
               
               },
               error: function() {

               }
            });
         }
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#prices').select();
         $('#otherfee').select();
         $('#save').html('Save');
         $('#f_save').parsley().destroy();
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#prices').select();
         $('#otherfee').select();
         $('#save').html('Save');
      });

      // edit ========
      $('body').delegate('.edit', 'click', function(){
         var otherfeeid = $(this).data('id');

         $.ajax({
            url: '<?= site_url('setup/otherfee/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){

            },
            data: {
               otherfeeid: otherfeeid
            },
            success: function(data){
               $('#otherfeeid').val(data.otherfeeid);
               $('#otherfee').val(data.otherfee);
               $('#prices').val(data.prices);
               $('#save').html('Update'); 
               
               $('#collapseExample').removeAttr('style');               
               $('#collapseExample').attr('aria-expanded', 'true');
               $('#collapseExample').addClass('in'); 

               $('#f_save').parsley().destroy();        
            
            },
            error: function() {

            }
         });         
         
      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var otherfeeid = $(this).data('id');

         if(otherfeeid == $(this).data('ba_id') - 0){
            toastr["warning"]("Can't delete! data in process...");
         }else{
            if(window.confirm('Are your sure to delete?')){
               $.ajax({
                  url: '<?= site_url('setup/otherfee/delete') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     otherfeeid: otherfeeid
                  },
                  success: function(data){
                     if(data.msg = 'deleted'){
                        toastr["success"]("Data deleted!");
                        gatedata();
                     }else{
                        toastr["warning"]("Can't delete!");
                     }               
                     
                  },
                  error: function() {

                  }
               });
            }
         }
         
      });

      // init. =====
      gatedata();

      // delete ========
      $('body').delegate('#Other_search', 'keyup', function(){
         gatedata();
      });

   }); // ready ========

   // clear =======
   function clear(){
      $('#otherfeeid').val('');
      $('#otherfee').val('');
      $('#prices').val('');
   }

   function isNumberKey(evt) {
        var e = window.event || evt; // for trans-browser compatibility 
        var charCode = e.which || e.keyCode;
        if ((charCode > 45 && charCode < 58) || charCode == 8) {
            return true;
        }
        return false;
    }

   // gatedata =========
   function gatedata(){
      $.ajax({
         url: '<?= site_url('setup/otherfee/gatedata') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            search: $('#Other_search').val()
         },
         success: function(data) {

            var tr = '';
            var total = '';
            if(data.result.length > 0){
               $.each(data.result, function(i, row){
                  tr += '<tr>'+
                           '<td>'+ (i + 1) +'</td>'+
                           '<td>'+ row.otherfee +'</td>'+
                           '<td>'+ row.prices +'</td>'+
                           '<td style="text-align: center;">\
                              <a href="javascript:;" class="edit" data-id="'+ row.otherfeeid +'" data-toggle="tooltip" data-placement="left" title="Edit">\
                                 <img src="<?= base_url('assets/images/icons/edit.png') ?>" style="width: 16px;height: 16px;">\
                              </a> | \
                              <a href="javascript:;" class="delete" data-id="'+ row.otherfeeid +'" data-ba_id="'+ row.fa_otherfeeid +'" data-toggle="tooltip" data-placement="right" title="Delete">\
                                 <img src="<?= base_url('assets/images/icons/delete.png') ?>">\
                              </a>\
                           </td>'+
                        '</tr>';
               });

               total += '<tr>'+
                           '<td colspan="6" style="padding-top: 15px;">Total:<span style="font-weight: bold;">&nbsp;&nbsp;'+ data.totalRecord +'&nbsp;</span><span>Items</span></td>'+                           
                        '</tr>';
            }else{
               tr += '<tr><td colspan="4" style="text-align: center;font-weight: bold;">We did not find anything to show here...</td></tr>';
               total += '';
            }

            $('#bus_list tbody').html(tr);
            $('#bus_list tfoot').html(total);
         },
         error: function() {

         }
      });
   }
</script>