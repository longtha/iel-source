<div class="container-fluid">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
                <strong>Assign Bus-Area</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add New">
                  <img src="<?= base_url('assets/images/icons/add.png') ?>">
               </a>
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('setup/bus/save') ?>" id="f_save">
         <input type="hidden" name="busareaid" id="busareaid">

         <div class="col-sm-4">
           <div class="form-group">           
               <label class="" for="busno">Bus No<span style="color:red">*</span></label>
              <select name="busid" id="busid" class="form-control" data-parsley-required="true" data-parsley-required-message="This field require">
                  <?php
                     $opt = '';
                     $opt .= '<option></option>';
                     foreach($this->b->fBus() as $row){                           
                        $opt .= '<option value="'.$row->busid.'">'.$row->busno.'</option>';
                     }

                     echo $opt;
                  ?>
               </select>
            </div>
         </div>

         <div class="col-sm-4">
           <div class="form-group">           
               <label class="" for="areaid">Area<span style="color:red">*</span></label>
               <select name="areaid" id="areaid" class="form-control" data-parsley-required="true" data-parsley-required-message="This field require">
                  <?php
                     $opt = '';
                     $opt .= '<option></option>';
                     foreach($this->a->FMareas() as $row){                           
                        $opt .= '<option value="'.$row->areaid.'">'.$row->area.'</option>';
                     }

                     echo $opt;
                  ?>
               </select>
            </div>
         </div>
         

         <div class="col-sm-12 col-sm-offset-0">
            <div class="form-group">           
               <button type="button" class="btn btn-primary btn-sm" name="save" id="save" value="save">Save</button>
               <button type="button" class="btn btn-warning btn-sm" name="clear" id="clear">Clear</button>
            </div>
         </div>

         <div class="row">
            <div class="col-sm-12">
               <div class="col-sm-12" style="border-bottom: 1px solid #CCC;">&nbsp;</div>
            </div>             
         </div><br>
      </form>      
   </div>

   <div class="row">
      <div class="col-sm-3">
         <input type="text" name="busno_area_search" id="busno_area_search" class="form-control" placeholder="Search..." aria-describedby="sizing-addon2" data-toggle="tooltip" data-placement="top" title="Search bus/area...">
      </div>             
   </div>
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <table border="0"​ align="center" id="bus_area" class="table table-hover">
                <thead>
                     <tr>
                        <th style="width: 10%;">No</th>
                        <th style="width: 30%;">Bus No</th>
                        <th style="width: 30%;">Area</th>
                        <th style="width: 10%;text-align: center;vertical-align: middle;">Action</th>
                     </tr>
                </thead>
                
               <tbody>

               </tbody>
               <tfoot>
                  
               </tfoot>
            </table>         
         </div>
      </div>
   </div>

</div>

<script type="text/javascript">
   $(function(){

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // save =======      
      $('body').delegate('#save', 'click', function(){
         if($('#f_save').parsley().validate()){
            $.ajax({
               url: '<?= site_url('setup/assign_bus_area/save') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  busareaid: $('#busareaid').val(),                 
                  busid: $('#busid').val(),
                  areaid: $('#areaid').val()
                  
               },
               success: function(data){
                  if(data == 1){
                     toastr["success"]('Success!');
                     clear();
                     $('#save').html('Save');
                     grid();
                  }else if(data == 2){
                     toastr["warning"]('Duplicate!');
                  }else{
                     toastr["warning"]("Can't save!");
                  }         
               
               },
               error: function() {

               }
            });
         }
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#busid').select();
         $('#save').html('Save');
         $('#f_save').parsley().destroy();
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#busid').select();
         $('#save').html('Save');
      });

      // edit ========
      $('body').delegate('.edit', 'click', function(){
         var busareaid = $(this).data('id');

         $.ajax({
            url: '<?= site_url('setup/assign_bus_area/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){

            },
            data: {
               busareaid: busareaid
            },
            success: function(data){
               $('#busareaid').val(data.busareaid);
               $('#busid').val(data.busid);
               $('#areaid').val(data.areaid);
               
               $('#save').html('Update');
               $('#collapseExample').removeAttr('style');               
               $('#collapseExample').attr('aria-expanded', 'true');
               $('#collapseExample').addClass('in'); 

               $('#f_save').parsley().destroy();        
            
            },
            error: function() {

            }
         });         
         
      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var busareaid = $(this).data('id');

         // if(busid == $(this).data('ba_id') - 0){
         //    toastr["warning"]("Can't delete! data in process...");
         // }else{
            if(window.confirm('Are your sure to delete?')){
               $.ajax({
                  url: '<?= site_url('setup/assign_bus_area/delete') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     busareaid: busareaid
                  },
                  success: function(data){
                     if(data == 1){
                        toastr["success"]("Data deleted!");
                        grid();
                     }else{
                        toastr["warning"]("Can't delete!");
                     }               
                     
                  },
                  error: function() {

                  }
               });
            }
         // }
         
      });

      // init. =====
      grid();

      // search ========
      $('body').delegate('#busno_area_search', 'keyup', function(){
         grid();
      });

   }); // ready ========

   // clear =======
   function clear(){
      $('#busareaid').val('');     
      $('#busid').val('');
      $('#areaid').val('');
   }


   // grid =========
   function grid(){
      $.ajax({
         url: '<?= site_url('setup/assign_bus_area/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            search: $('#busno_area_search').val()
         },
         success: function(data) {

            var tr = '';
            var total = '';
            if(data.result.length > 0){
               $.each(data.result, function(i, row){
                  tr += '<tr>'+
                           '<td>'+ (i + 1) +'</td>'+
                           '<td>'+ row.busno +'</td>'+ 
                           '<td>'+ row.area +'</td>'+
                                                   
                           '<td style="text-align: center;">\
                              <a href="javascript:;" class="edit" data-id="'+ row.busareaid +'" data-toggle="tooltip" data-placement="left" title="Edit">\
                                 <img src="<?= base_url('assets/images/icons/edit.png') ?>" style="width: 16px;height: 16px;">\
                              </a> | \
                              <a href="javascript:;" class="delete" data-id="'+ row.busareaid +'" data-ba_id="'+ row.ba_busid +'" data-toggle="tooltip" data-placement="right" title="Delete">\
                                 <img src="<?= base_url('assets/images/icons/delete.png') ?>">\
                              </a>\
                           </td>'+
                        '</tr>';
               });

               total += '<tr>'+
                           '<td colspan="6" style="padding-top: 15px;">Total:<span style="font-weight: bold;">&nbsp;&nbsp;'+ data.totalRecord +'&nbsp;</span><span>Items</span></td>'+                           
                        '</tr>';
            }else{
               tr += '<tr><td colspan="4" style="text-align: center;font-weight: bold;">No Results</td></tr>';
               total += '';
            }

            $('#bus_area tbody').html(tr);
            $('#bus_area tfoot').html(total);
         },
         error: function() {

         }
      });
   }
</script>