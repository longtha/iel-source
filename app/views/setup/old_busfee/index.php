<div class="container-fluid">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
                <strong>Bus Fee</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add New">
                  <img src="<?= base_url('assets/images/icons/add.png') ?>">
               </a>

               <a href="javascript:;" id="refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse in" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('setup/area/save') ?>" id="f_save">
         <input type="hidden" name="buspriceid" id="buspriceid">      

         <div class="row">
           <div class="col-sm-4">           
               <label class="" for="buswaytype">Bus Way Type<span style="color:red">*</span></label>
               <select name="buswaytype" id="buswaytype" class="form-control" placeholder="Bus Way Type" data-parsley-required="true" data-parsley-required-message="This field require">

                  <?php
                     $qr = $this->m->getbusway();
                     $opt = '';
                     $opt .= '<option></option>';
                     if($qr->num_rows() > 0){
                        foreach($qr->result() as $row){                           
                           $opt .= '<option value="'.$row->famnoteid.'">'.$row->description.'</option>';
                        }
                     }

                     echo $opt;
                  ?>
               </select>
            </div>
            <div class="col-sm-4">           
               <label class="" for="area">Area<span style="color:red">*</span></label>
               <select name="area" id="area" class="form-control" data-parsley-required="true" data-parsley-required-message="This field require">
                  <?php
                     // getoption($sql, $key, $display, $select = '', $istop = false)
                     echo getoption("SELECT
                                          a.areaid,
                                          a.area
                                       FROM
                                          sch_setup_area AS a
                                       WHERE
                                          1 = 1
                                       ORDER BY
                                          a.area ASC ", 'areaid', 'area', '', true)
                    ?>
               </select>
            </div>           
            <div class="col-sm-4">           
               <label class="" for="price">Price<span style="color:red"></span></label>
               <input type="text" name="price" id="price" class="form-control" placeholder="Price" data-parsley-type="number">
            </div>
            
         </div><br>

         <div class="row">
            <div class="col-sm-12">           
               <button type="button" class="btn btn-primary btn-sm" name="save" id="save" value="save">Save</button>
               <button type="button" class="btn btn-warning btn-sm" name="clear" id="clear">Clear</button>
            </div>
         </div><br>
      </form>  
      
      <div class="row">
         <div class="col-sm-12">
            <span style="border-top: 1px solid #CCC;width: 100%;display: block;">&nbsp;</span>

         </div>             
      </div>

   </div>  
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <table border="0"​ align="center" id="busfee_list" class="table table-hover">
                <thead>
                     <tr>
                        <th style="width: 10%;">No</th>
                        <th style="width: 20%;">Bus Way Type</th>
                        <th style="width: 20%;">Area</th>
                        <th style="width: 20%;">Price</th>
                        <th style="width: 10%;text-align: center;">&nbsp;</th>
                     </tr>
                     <tr>
                        <td style="width: 10%;">&nbsp;</td>
                        <td style="width: 20%;">
                           <input type="text" name="buswaytype_search" id="buswaytype_search" class="form-control" placeholder="Search Bus Way..." data-toggle="tooltip" data-placement="top" title="Search by Bus Way...">
                        </td>
                        <td style="width: 20%;">
                        <input type="text" name="area_search" id="area_search" class="form-control" placeholder="Search area..." data-toggle="tooltip" data-placement="top" title="Searc area...">
                        </td>
                        <td style="width: 20%;">&nbsp;</td>
                        <td style="width: 10%;text-align: center;vertical-align: middle;">&nbsp;</td>
                     </tr>
                </thead>
                
               <tbody>

               </tbody>
               <tfoot>
                  
               </tfoot>
            </table>         
         </div>
      </div>
   </div>

</div>

<script type="text/javascript">
   $(function(){

      $('[data-toggle="tooltip"]').tooltip()

      $('body').delegate('#a_up_down', 'click', function(){
         $(this).find('i').removeClass();
         if($( "#collapseExample" ).hasClass( "in" )){
            $(this).find('i').addClass("glyphicon glyphicon-menu-down");
         }else{
            $(this).find('i').addClass("glyphicon glyphicon-menu-up");
         }
     
         $('#collapseExample').collapse('toggle')
      });

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#areaid').select();
         $('#save').html('Save');
         $('#f_save').parsley().destroy();
      });

      // save =======      
      $('body').delegate('#save', 'click', function(){
         if($('#f_save').parsley().validate()){
            $.ajax({
               url: '<?= site_url('setup/old_busfee/save') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  buspriceid: $('#buspriceid').val(),
                  buswaytype: $('#buswaytype').val(),
                  area: $('#area').val(),
                  price: $('#price').val()                                    
               },
               success: function(data){
                  if(data == 1){
                     toastr["success"]('Success!');
                     clear();
                     $('#save').html('Save');
                     grid();
                  }else{
                     toastr["warning"]('Duplicate!');
                  }
               },
               error: function() {

               }
            });
         }
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#areaid').select();
         $('#save').html('Save');
      });

      // edit ========
      $('body').delegate('.edit', 'click', function(){
         var buspriceid = $(this).data('id');

         $.ajax({
            url: '<?= site_url('setup/old_busfee/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){

            },
            data: {
               buspriceid: buspriceid
            },
            success: function(data){
               $('#buspriceid').val(data.buspriceid);               
               $('#buswaytype').val(data.buswaytype);
               $('#area').val(data.areaid);
               $('#price').val(data.price - 0 > 0 ? data.price : '');               
               $('#save').html('Update'); 
               
               $('#collapseExample').removeAttr('style');               
               $('#collapseExample').attr('aria-expanded', 'true');
               $('#collapseExample').addClass('in');

               $('#f_save').parsley().destroy();         
            
            },
            error: function() {

            }
         });         
         
      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var buspriceid = $(this).data('id') - 0;

         // if(areaid == $(this).data('ba_id') - 0){
         //    toastr["warning"]("Can't delete! data in process...");
         // }else{
            if(window.confirm('Are your sure to delete?')){
               $.ajax({
                  url: '<?= site_url('setup/old_busfee/delete') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     buspriceid: buspriceid
                  },
                  success: function(data){
                     if(data.msg = 'deleted'){
                        toastr["success"]("Data deleted!");
                        grid();
                     }else{
                        toastr["warning"]("Can't delete!");
                     }               
                     
                  },
                  error: function() {

                  }
               });
            }
         // }
         
      });

      // init. =====
      grid();

      // search ========
      $('body').delegate('#buswaytype_search', 'keyup', function(){
         grid();
      });

      // search ========
      $('body').delegate('#area_search', 'keyup', function(){
         grid();
      });

   }); // ready ========

   // clear =======
   function clear(){
      $('#buspriceid').val('');
      $('#buswaytype').val('');
      $('#area').val('');
      $('#price').val('');
   }


   // grid =========
   function grid(){
      $.ajax({
         url: '<?= site_url('setup/old_busfee/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            search: $('#buswaytype_search').val(),
            area: $('#area_search').val()
            
         },
         success: function(data) {
            var tr = '';
            var total = '';

            if(data.result.length > 0){
               $.each(data.result, function(i, row){
                  tr += '<tr>'+
                           '<td>'+ (i + 1) +'</td>'+
                           '<td>'+ (row.description != null ? row.description : '') +'</td>'+
                           '<td>'+ (row.area != null ? row.area : '') +'</td>'+
                           '<td>'+ (row.price - 0 > 0  ? row.price : '') +'</td>'+                           
                           '<td style="text-align: center;">\
                              <a href="javascript:;" class="edit" data-id="'+ row.buspriceid +'" data-toggle="tooltip" data-placement="left" title="Edit">\
                                 <img src="<?= base_url('assets/images/icons/edit.png') ?>" style="width: 16px;height: 16px;">\
                              </a> | \
                              <a href="javascript:;" class="delete" data-id="'+ row.buspriceid +'" data-ba_id="'+ 'row.ba_areaid' +'" data-toggle="tooltip" data-placement="right" title="Delete">\
                                 <img src="<?= base_url('assets/images/icons/delete.png') ?>">\
                              </a>\
                           </td>'+                           
                        '</tr>';
               });

               total += '<tr>'+
                           '<td colspan="5" style="padding-top: 15px;">Total:<span style="font-weight: bold;">&nbsp;&nbsp;'+ data.totalRecord +'&nbsp;</span><span>Items</span></td>'+                           
                        '</tr>';

            }else{
               tr += '<tr><td colspan="5" style="text-align: center;font-weight: bold;">No Results</td></tr>';
               total += '';
            }

            $('#busfee_list tbody').html(tr);
            $('#busfee_list tfoot').html(total);
            
         },
         error: function() {

         }
      });
   }
</script>