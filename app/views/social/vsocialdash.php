
<div class="wrapper" >
	<div class="clearfix" id="main_content_outer" >
	    <div id="main_content">
	      <div class="result_info">
		      	<div class="col-xs-6">
		      		<strong id='title'>Social Report Summary</strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
		      		<span class="top_action_button">
			    		<a>
			    			<img onclick="showsort(event);" src="<?php echo base_url('assets/images/icons/sort.png')?>" />
			    		</a>
			    	</span>
		      		<span class="top_action_button">
			    		<a href="<?php echo site_url('student/student/import')?>" >
			    			<img src="<?php echo base_url('assets/images/icons/import.png')?>" />
			    		</a>
			    	</span>
			    	<span class="top_action_button">	
			    		<a href="#" id="export" title="Export">
			    			<img src="<?php echo base_url('assets/images/icons/export.png')?>" />
			    		</a>
			    	</span>
		      		<span class="top_action_button">
						<a href="#" id="print" title="Print">
			    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
			    		</a>
		      		</span>		    	
			    	<span class="top_action_button">
			    		<a href="<?php echo site_url('social/family/add')?>" >
			    			<img src="<?php echo base_url('assets/images/icons/add.png')?>" />
			    		</a>
			    	</span>	      		
		      	</div> 			      
		  </div>
		  <div id="tab_print">
	      	<div class="col-sm-6">
	      		<div class="panel panel-default">
	      			<div class="panel-body">
		           		<div class="form_sep">
			                  <label class="req" for="reg_input_name"><?php echo "1. Thear are ".$data['f_total']['num_student']." children from ".$data['f_total']['num_family']." families" ?></label>
			                  <label class="req" for="reg_input_name"><?php echo "&nbsp &nbsp &nbsp".$data['f_total']['over_1student']." of ".$data['f_total']['num_family']." families have more than 1 child at Happy Chandara Primary School" ?></label> 
			                  <table class='table table-bordered'>
			                  		<thead>
			                  			<th>No</th>
			                  			<th>Family</th>
			                  			<th>Have Child</th>
			                  		</thead>
			                  		<tbody>
			                  			<?php
				                  			$i=1;
				                  			foreach ($data['h_child'] as $h_child) {
				                  				$father_name='';$father_name_kh='';$father_ocupation='';$mother_name='';$mother_name_kh='';$mother_ocupation='';$sort_num='all';$family_code='';$num_stu=$h_child['number_stu'];$nm="";$fm="";
				                  				$link=site_url()."/social/family/search?fn=$father_name&fnk=$father_name_kh&fc=$father_ocupation&mn=$mother_name&srev=&mnk=$mother_name_kh&mc=$mother_ocupation&s_num=$sort_num&fcode=$family_code&nst=$num_stu&nm=$nm&fm=$fm";
				                  				echo "<tr>
				                  							<td>$i</td>
									                  		<td>".$h_child['num_fam'] ." of ".$data['f_total']['num_family']." families</td>
									                  		<td><a href='".$link."' target='_blank'>".$h_child['number_stu']." children </a></td>
				                  					</tr>";
				                  					$i++;
				                  			}
			                  			?>
			                  			
			                  		</tbody>
			                  </table>
		                </div> 
					</div>
					<div class="panel-body">
		           		<div class="form_sep">
		           			<?php 
		           				  $f_percent=number_format(($datarice['family_get']['f_total']*100)/$data['f_total']['num_family'],2) ;
		           				  $family_not_re=($data['f_total']['num_family']-$datarice['family_get']['f_total']);
		           				  $f_not_recieved_percent=(100-$f_percent);
		           			?>
			                  <label class="req" for="reg_input_name"><?php echo "2. ".$datarice['family_get']['f_total']." of ".$data['f_total']['num_family']." families = ".$f_percent." % had reveived different quantity of rice" ?></label>
			                  <label class="req" for="reg_input_name"><?php echo "&nbsp &nbsp &nbsp".$family_not_re." of ".$data['f_total']['num_family']." families = ". $f_not_recieved_percent." % had never received rice from the school" ?></label> 
			                  <table class='table table-bordered'>
			                  		<thead>
			                  			<th>No</th>
			                  			<th>Family</th>
			                  			<th>Percent</th>
			                  			<th>Had Received</th>
			                  		</thead>
			                  		<tbody>
			                  			<?php
				                  			$j=1;
				                  			foreach ($datarice['f_times'] as $times) {
				                  				echo "<tr>
				                  							<td>$j</td>
									                  		<td>".$times['family'] ." of ".$datarice['family_get']['f_total']." families</td>
									                  		<td>".number_format(($times['family']*100)/$datarice['family_get']['f_total'],2)." %</td>
									                  		<td>".$times['num']." Times</td>
				                  					</tr>";
				                  					$j++;
				                  			}
			                  			?>
			                  			
			                  		</tbody>
			                  </table>
		                </div> 
					</div>
					<div class="panel-body">
		           		<div class="form_sep">
			                  <label class="req" for="reg_input_name"><?php echo "3. Happy Chandara Primary School had distributed ".$totalrice['totalrice']['total']." Kg in ".date('Y') ?></label>
			                  <table class='table table-bordered'>
			                  		<thead>
			                  			<th>No</th>
			                  			<th>Family</th>
			                  			<th>Received(Kg)</th>
			                  			<th>Total(Kg)</th>
			                  		</thead>
			                  		<tbody>
			                  			<?php
				                  			$j=1;
				                  			foreach ($totalrice['rice_detail'] as $rice) {
				                  				echo "<tr>
				                  							<td>$j</td>
									                  		<td>".$rice['family'] ." of ".$datarice['family_get']['f_total']." families</td>
									                  		<td>".$rice['f_rice']." Kg</td>
									                  		<td>".$rice['family']*$rice['f_rice']." Kg</td>
				                  					</tr>";
				                  					$j++;
				                  			}
			                  			?>
			                  			
			                  		</tbody>
			                  </table>
		                </div> 
					</div>

					<div class="panel-heading">
		                <span class="panel-title">4.Family Revenue Static</span>
		                <span class='panel-title pull-right danger'>		                	
		                </span>
		            </div>	
		            <div class="panel-body">	           
			            <div class="form_sep">		                	
		           			<table class='table table-bordered'>
		                   		<tbody>
		                   			<?php echo $frevenue?>
		                   		</tbody>
		                   </table>
		                </div> 
					</div>

      		
	      		</div>
	      		<div class="panel panel-default">
      			<div class="panel-body">
	           		<div class="form_sep">
		                  <label class="req" for="reg_input_name">7. Distribution Statistic: From : <?php echo $disdate['sdate'];?> To : <?php echo $disdate['edate'];?></label>
		                  <table class='table table-bordered'>
		                  		<thead>
		                  			<th>Type</th>
		                  			<th>Family</th>
		                  			<th>Rice</th>
		                  			<th>Oil</th>
		                  		</thead>
		                  		<tbody>
		                  			<?php
		                  				foreach ($dis as $row) {
		                  					echo "<tr>
			                  			 				<td>";
			                  			 				if ($row->distrib_type==12)
			                  			 					echo "Every Year";
			                  			 				else if ($row->distrib_type==4)
			                  			 					echo "4 times/year";
			                  			 				else
			                  			 					echo "2 times/year";

			                  			 		  echo "</td>
								                   		<td>".$row->total_family."</td>
								                   		<td>".$row->tota_rice." Kg(s)</td>
			                  			 				<td>".$row->oil_total." Litre(s)</td>
			                  			 		</tr>";
		                  				}
		                  			?>	
		                  		</tbody>
		                  </table>
	              
				</div>
      		</div>      	
	      	</div>
	      	</div>

	      	<div class="col-sm-6">
	      		<div class="panel panel-default">
	      			<div class="panel-body">
		           		<div class="form_sep">
			                  <label class="req" for="reg_input_name"><?php echo "5. Thear are ".$member['total']." Member from ".$data['f_total']['num_family']." Families" ?></label>
			                  <table class='table table-bordered'>
			                  		<thead>
			                  			<th>No</th>
			                  			<th>Family</th>
			                  			<th>Have Member</th>
			                  		</thead>
			                  		<tbody>
			                  			<?php
				                  			$i=1;
				                  			foreach ($member['data'] as $m) {
				                  				$father_name='';$father_name_kh='';$father_ocupation='';$mother_name='';$mother_name_kh='';$mother_ocupation='';$sort_num='100';$family_code='';$num_stu='';$num_mem=$m->t_mem;$fm="";
				                  				$link=site_url()."/social/family/search?fn=$father_name&fnk=$father_name_kh&fc=$father_ocupation&mn=$mother_name&mnk=$mother_name_kh&srev=&mc=$mother_ocupation&s_num=$sort_num&fcode=$family_code&nst=$num_stu&nm=$num_mem&fm=$fm";
				                  				$adds=$m->t_mem>1?"s":"";
				                  				echo "<tr>
				                  							<td>$i</td>
									                  		<td>".$m->f_total ." of ".$data['f_total']['num_family']." Families</td>
									                  		<td><a href='".$link."' target='_blank'>".$m->t_mem." Member$adds </a></td>
				                  					</tr>";
				                  					$i++;
				                  			}
			                  			?>
			                  			
			                  		</tbody>
			                  </table>
		                </div> 
					</div>
					
	      		</div>
	      	</div>
	      	<div class="col-sm-6">
      		<div class="panel panel-default">
      			<div class="panel-body">
	           		<div class="form_sep">
		                  <label class="req" for="reg_input_name"><?php echo "6. Thear are ".$femalemem['total']." Female Member from ".$data['f_total']['num_family']." Families" ?></label>
		                  <table class='table table-bordered'>
		                  		<thead>
		                  			<th>No</th>
		                  			<th>Family</th>
		                  			<th>Female Member</th>
		                  		</thead>
		                  		<tbody>
		                  			<?php
			                  			$i=1;
			                  			foreach ($femalemem['data'] as $m) {

			                  				$father_name='';$father_name_kh='';$father_ocupation='';$mother_name='';$mother_name_kh='';$mother_ocupation='';$sort_num='100';$family_code='';$num_stu='';$num_mem="";$fm=$m->mem_num_F;
			                  				$link=site_url()."/social/family/search?fn=$father_name&fnk=$father_name_kh&fc=$father_ocupation&mn=$mother_name&mnk=$mother_name_kh&srev=&mc=$mother_ocupation&s_num=$sort_num&fcode=$family_code&nst=$num_stu&nm=$num_mem&fm=$fm";
			                  				$adds=$m->mem_num_F>1?"s":"";
			                  				echo "<tr>
			                  							<td>$i</td>
								                  		<td>".$m->totalFfami ." of ".$data['f_total']['num_family']." Families</td>
								                  		<td><a href='".$link."' target='_blank'>".$m->mem_num_F." Member$adds </a></td>
			                  					</tr>";
			                  					$i++;
			                  			}
		                  			?>	
		                  		</tbody>
		                  </table>
	                </div> 
				</div>
      		</div>      	
	      </div>
	      <!-- tha working -->
	      	<div class="col-sm-6">
      		<div class="panel panel-default">
      			<div class="panel-body">
	           		<div class="form_sep">
		                <span class="panel-title">8. <?php echo $statistic_header_tittle;?></span>
	                	<span class='panel-title pull-right danger'>
							<a href="<?php echo base_url('index.php/system/dashboard/vsflocation?y=5&m=4&p=59')?>"><img onclick="" src="<?php echo base_url('assets/images/icons/detail.png')?>" /></a>	                	
	                	</span>
		                  <table class='table table-bordered'>
		                  		<thead>
		                  			<th>No</th>
		                  			<th>Communces</th>
		                  			<th>Villages</th>
		                  			<th>Famillies</th>
		                  		</thead>
		                  		<tbody><?php echo $fetchcommunce;?></tbody>
		                  </table>
	                </div> 
				</div>
      		</div>      	
	      </div>
	      <!-- end tha working -->
	      </div>
      	</div>
   </div>
</div>

<script type="text/javascript">
	$(function(){
		$("#print").on("click",function(){					
				var title="<h4 align='center'>"+ $("#title").html()+"</h4>";
			   	var data = $("#tab_print").html().replace(/<img[^>]*>/gi,"");
			   	var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
			   	var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
			   		
			   	gsPrint(title,export_data);
			});
		$("#export").on("click",function(e){
				var data=$("#tab_print").html().replace(/<img[^>]*>/gi,"");
	   			var export_data = $("<center><h3 align='center'>"+$("#title").html()+"</h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
				window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
    			e.preventDefault();
		});
	});		
</script>