<?php
$m='';
$p='';
if(isset($_GET['m'])){
    $m=$_GET['m'];
}
if(isset($_GET['p'])){
    $p=$_GET['p'];
}
 ?>
<style>
.disable{display: none;}
.shide{
	display: none;
}
.hid{
	display: none;
}
</style>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">

	      	<div class="col-xs-6">
		      	<strong>Sponsor Box Information</strong>
		    </div>
		    <div class="col-xs-6" style="text-align: right">
		      	<strong>
				    <center class='c_exist' style='color:red;text-align:right;'></center>
				</strong>        		
		    </div>
	      </div>
	      <!---Start form-->
	      <form id="sponsor_register" method="POST" enctype="multipart/form-data" accept-charset="utf-8"  action="<?php echo site_url("social/sponsorbox/save?&save=add&m=$m&p=$p")?>">
	      		<!-- <div class="row"> -->
	      		<!-- Start Student Legend -->
	      		<div class="col-sm-12">
	     				<div class="panel panel-default">
	     					<div class="panel-heading">
	     						<h4 class="panel-title">Student Information
								    <label style="float:right !important; font-size:11px !important; color:red;">
								    	<?PHP 
							              echo "<p style='color:red;' id='i_exist'></p>";
							             ?>
							         </label>
	     						</h4>
	     					</div>
	     					<div class="panel-body">
	     						<div class="row">
	     							<div class="col-xs-6">
	     								<div class="form_sep hide">
								            <input type="text" id="s_boxid" name="s_boxid" value="<?php if(isset($row->boxid)) echo $row->boxid ?>">
	     								</div>
	     								<div class="form_sep">
	     									<label>Student</label>
	     									<input type="text" name="student" id="student" value="<?php if(isset($row->stu_fullname)) echo $row->stu_fullname ?>"  required  data-parsley-required-message="Enter student" class="student form-control">
	     									<input type="text" class="hide" name="s_studentid" id="s_studentid" value="<?php if(isset($row->studentid)) echo $row->studentid ?>">
	     								</div>
	     								<div class="form_sep">
	     									<label>Year</label>
	     									<select name="s_yearid" id="s_yearid" class="form-control" required  data-parsley-required-message="Please select any class">
	     										<!-- <option value="">Select Class</option> -->
	     										<!-- <?php foreach ($this->db->get('sch_school_year')->result() as $year) {?>
	     												<option value="<?php echo $year->yearid ?>"<?php if(isset($row->yearid)) if($row->yearid==$year->yearid ) echo "selected";?>><?php echo $year->sch_year ?></option>				
	     										<?php } ?>-->
												<!-- <option value=''>---Select year---</option> -->
	     										<?php foreach ($this->db->select("*")->from('sch_school_year')->order_by('yearid', 'desc')->get()->result() as $year) {?>
													<option value="<?php echo $year->yearid ?>"
														<?php if(isset($row->yearid)){
																if($row->yearid==$year->yearid){ 
																	echo "selected"; 
																}
															  }else{ 
																if ($this->session->userdata('year')==$year->yearid){
																	echo "selected";
																} 
															}?>

														><?php echo $year->sch_year ?></option>
												<?php } ?>
	     									</select>
	     								</div>
	     								<div class="form_sep">
     									<label>Receive Date</label>
     									<div data-date-format="dd-mm-yyyy" class="input-group date from">
							              <input type="text"  required  data-parsley-required-message="Enter From Date" value="<?php if(isset($row->reception_date)) echo $this->green->convertSQLDate($row->reception_date); else echo date('d-m-Y');?>" data-type="dateIso" class="form-control" id="receptiondate" name="receptiondate">
							              <span class="input-group-addon"><i class="icon-calendar"></i></span> 
							            </div>
	     							</div>
	     							</div>
	     							<div class="col-xs-6">
		     							<div class="form_sep">
	     									<label>Sponsor</label>
	     									<input type="text" name="sponsor" id="sponsor"  value="<?php if(isset($row->sp_fullname)) echo $row->sp_fullname ?>" required  data-parsley-required-message="Enter Sponsor" class="form-control" onkeypress='return nothingSponsor(event);'>
	     									<input type="text" class="hide" name="s_sponsorid" id="s_sponsorid" value="<?php if(isset($row->sponsorid)) echo $row->sponsorid ?>">
	     								</div>
	     								<div class="form_sep">
	     									<label>Class</label>
	     									<select name="s_classid" id="s_classid" class="form-control" required  data-parsley-required-message="Please select any class" disabled>
	     										<option value=''>---Select Class---</option>
	     										<?php foreach ($this->db->select("*")->from('sch_class')->order_by('classid', 'desc')->get()->result() as $class) {?>
													<option value="<?php echo $class->classid ?>"<?php if(isset($row->classid)) if($row->classid==$class->classid ) echo "selected";?>><?php echo $class->class_name ?></option>
												<?php } ?>
	     									</select>
	     									<input type="text" class="hide" name="hiddenclassid" id="hiddenclassid" value="<?php if(isset($row->classid)) echo $row->classid ?>">
	     								</div>
	     							</div>
	     						</div>
	     					</div>
	     				</div>
	     			</div>
	     			<!-- End Student Legend -->
	     			<!-- Start Gift -->
	     			<div class="col-sm-6">
			            <div class="panel panel-default">
				              <div class="panel-heading">
				                <h4 class="panel-title">Gift</h4>
				              </div>
				              <div class="panel-body">
					              <div class="form_sep">
	 									<label>Gift</label>
     									<textarea class="form-control"  name="gift" id="gift"><?php if(isset($row->gift)) echo $row->gift ?></textarea>
	 							  </div>
	     						  <div class="form_sep">
     									<label>Detail (Fr)</label>
     									<textarea class="form-control"  name="detailfr" id="detailfr"><?php if(isset($drow->des_fr)) echo $drow->des_fr ?></textarea>
     							  </div>
     							  <div class="form_sep">
     									<label>Detail (En)</label>
     									<textarea class="form-control"  name="detailen" id="detailen"><?php if(isset($drow->des_eng)) echo $drow->des_eng ?></textarea>
     							   </div>
     							   <!-- <div class="form_sep">
     									<label>Date of sending gift</label>
     									<div data-date-format="dd-mm-yyyy" class="input-group date to">
							              <input type="text"  required  data-parsley-required-message="Enter to sending date" value="<?php if(isset($row->sending_box_date)) echo $this->green->convertSQLDate($row->sending_box_date); else echo date('d-m-Y');?>" data-type="dateIso" class="form-control" id="sendingboxdate" name="sendingboxdate">
							              <span class="input-group-addon"><i class="icon-calendar"></i></span> 
							            </div>
	     							</div> -->
     						  </div>
     						  <!-- End Gift -->
     						  <!-- Start Response and Thanks -->
				              <div class="panel-heading hide">
				                <h4 class="panel-title">Response and Thanks</h4>
					          </div>
					          <div class="panel-body hide">     
					             	<div class="form_sep">
     									<label>Answer Date</label>
     									<div data-date-format="dd-mm-yyyy" class="input-group date to">
							              <input type="text"  required  data-parsley-required-message="Enter to date" value="<?php if(isset($row->date_answer_toques)) echo $this->green->convertSQLDate($row->date_answer_toques); else echo date('d-m-Y');?>" data-type="dateIso" class="form-control" id="dateanswertoques" name="dateanswertoques">
							              <span class="input-group-addon"><i class="icon-calendar"></i></span> 
							            </div>
     								</div>
					                <div class="form_sep">
     									<label>Thanks Date</label>
     									<div data-date-format="dd-mm-yyyy" class="input-group date to">
							              <input type="text"  required  data-parsley-required-message="Enter to date" value="<?php if(isset($row->date_thanks)) echo $this->green->convertSQLDate($row->date_thanks); else echo date('d-m-Y');?>" data-type="dateIso" class="form-control" id="datethanks" name="datethanks">
							              <span class="input-group-addon"><i class="icon-calendar"></i></span> 
							            </div>
     								</div>  
					         </div>
					         <!-- End Response and Thanks --> 
					          
				            </div>
				            <!-- Start Paris dispatch --> 
				            <div class="panel panel-default">
					            <div class="panel-heading">
					                <h4 class="panel-title">Reply</h4>
						        </div>
						        <div class="panel-body">     
					             	<div class="form_sep">
	 									<label>Reply Date</label>
	 									<div data-date-format="dd-mm-yyyy" class="input-group date to">
							              <input type="text" id="datesendthanksparis" name="datesendthanksparis"  required  data-parsley-required-message="Enter to date" value="<?php if(isset($row->date_sendthanks_paris)) echo $this->green->convertSQLDate($row->date_sendthanks_paris); else echo date('d-m-Y');?>" data-type="dateIso" class="form-control">
							              <span class="input-group-addon"><i class="icon-calendar"></i></span> 
							            </div>
	 								</div> 
					              <br />
					              <div class="form_sep">
		                              <div>
											<img src="<?php if(isset($row)) {if(@ file_get_contents(site_url('../assets/upload/sponsorbox/'.$row->boxid.'.jpg'))){ echo site_url('../assets/upload/sponsorbox/'.$row->boxid.'.jpg'); }else{ echo site_url('../assets/upload/NoImage.png');}}else{ echo site_url('../assets/upload/NoImage.png');} ?>" id="uploadPreview" style='width:120px; height:150px; margin-bottom:15px'>
					                    	(Size : 120px,150px)<input id="uploadImage" type="file" accept="image/gif, image/jpeg, image/jpg, image/png" name="userfile" onchange="PreviewImage();" style="visibility:hidden; display:none;" />
		                              </div>
		                              <div>
		                                    <input type='button' class="btn btn-success" onclick="$('#uploadImage').click();" value='Upload Photo'/>
		                              </div>
					                </div>
					             </div>
				          	</div>
				              <!-- Start Paris dispatch --> 
				        </div>

			        </div>
	     			<!-- Start Letter Legend -->
	     			<div class="col-sm-6">
			            <div class="panel panel-default">
				              <div class="panel-heading">
				                <h4 class="panel-title">Letter</h4>
				              </div>
				              <div class="panel-body">
				              		<div class="form_sep">   
					                    <label class="req" for="classid">Letter Type</label>
					                    <?php 
					                    	$lat = "";
					                    	$pcard = "";
					                    	$pho = "";
					                    	$oth = "";
					                    	$h = "shide";
					                    	if(isset($row->lettertype)){
					                    		if($row->lettertype=="Letter"){
					                    			$lat = "selected";
					                    		}
					                    		if($row->lettertype=="Post Card"){
					                    			$pcard = "selected";
					                    		}
					                    		if($row->lettertype=="Photo"){
					                    			$pho = "selected";
					                    		}
					                    		if($row->lettertype=="Other"){
					                    			$oth = "selected";
					                    			$h = "";
					                    		}
					                    	}
					                    ?>
					                    <select class="form-control" onchange='l_lettertype(event);' id='letter_type' name='letter_type'>
				                          <option value="Letter" <?php echo $lat; ?>>Letter</option>
				                          <option value="Post Card" <?php echo $pcard; ?>>Post Card</option>
				                          <option value="Photo" <?php echo $pho; ?>>Photo</option>
				                          <option value="Other" <?php echo $oth; ?>>Other</option>
					                    </select> 
					                </div>
					                <div class="form_sep <?php echo $h; ?>" id="l_other">
     									<label>Other</label>
     									<input type="text" name="other" id="other" value="<?php if(isset($row->letter_other)) echo $row->letter_other ?>" class="student form-control">
     								</div>
				              		<div class="form_sep">   
					                    <label class="req" for="classid">Language</label>
					                    <?php 
					                    	$en="";
					                    	$fr="";
					                    	if (isset($row->langue)){
					                    		if($row->langue=="EN"){
					                    			$en="selected";
					                    		}
					                    		if($row->langue=="FR"){
					                    			$fr="selected";
					                    		}
					                    	}
					                    ?>
					                    <select class="form-control" id='laguage'name='laguage'>
				                          <option value="FR" <?php echo $fr; ?>>FR</option>
				                          <option value="EN" <?php echo $en; ?>>EN</option>
					                    </select> 
					                </div>
				              		<!--<div class="form_sep">
     									<label>Detail</label>
     									<textarea class="form-control"  name="detail" id="detail"><?php if(isset($row->detail)) echo $row->detail ?></textarea>
     								</div>-->
				              		<div class="form_sep">
     									<label>Question (Fr)</label>
     									<textarea class="form-control"  name="sp_fr" id="sp_fr"><?php if(isset($row->spr_ques_fr)) echo $row->spr_ques_fr ?></textarea>
     								</div>
     								<div class="form_sep">
     									<label>Question (En)</label>
     									<textarea class="form-control"  name="sp_en" id="sp_en"><?php if(isset($row->spr_ques_eng)) echo $row->spr_ques_eng ?></textarea>
     								</div>
     								<div class="form_sep">
     									<?php 
     										$ch="";
     										$hi="hid";
     										if (isset($row->is_visit)){
     											if($row->is_visit=='1'){
     												$ch="checked";
     												$hi="";
     											}
     										}
     									?>
					                    <label class="req" for="cboschool">Is visit</label><br/>
					                    <input type='checkbox' value='1' <?php echo $ch; ?>  name='isvisit' id='isvisit' onchange='vcheck(event);'>
					                </div>
     								<div class="form_sep <?php echo $hi; ?>" id="v_date">
     									<label>Visit Date</label>
     									<div data-date-format="dd-mm-yyyy" class="input-group date date_request">
     										<?php 
     											$d="";
					              		 		if(isset($row->visite_date)) {
					              		 			 if($row->visite_date!="00-00-0000" && $row->visite_date!="0000-00-00"){ 
					              		 				$d=trim($this->green->convertSQLDate($row->visite_date));
					              		 			 }else{
					              		 			 	$d="";
					              		 			 } 
					              		 		}
					              		 	?>
							              <input type="text" 
							              		 value="<?php echo $d; ?>"
									             data-type="dateIso"
									             class="form-control"
									             id="vdate"
									             name="vdate">
									      <input type="hidden" 
							              		 value="<?php echo $d; ?>"
									             id="vdateh">
							              <span class="input-group-addon"><i class="icon-calendar"></i></span> 
							            </div>
	     							</div>
     								<!--<div class="form_sep">
     									<label>Number Box</label>
     									<input type="text" name="numbox" onkeypress='return isNumberKey(event);' value="<?php if(isset($row->num_box)) echo $row->num_box ?>" id="numbox" class="student form-control">
     								</div>-->
     								<div class="form_sep">
     									<label>Note</label>
     									<textarea class="form-control"  name="note" id="note"><?php if(isset($row->note)) echo $row->note ?></textarea>
	     							</div>
				              </div>  
				        </div>
			        </div>

	     			<!-- End Letter Legend -->
	     			<!-- Not use -->
	     			<div class="col-sm-12">
	     				<div class="panel panel-default hide">
	     					<div class="panel-heading">
	     						<p class="panel-title">Sponsor Box Detail</p>
	     						<p style="float:right;margin-top:-20px"><input type="button" value="+Add" onclick="getsponsordetailtolist();"/> </p>
	     					</div>
	     					<div class="panel-body">
	     						<div class="row">
	     							<div class='table-responsive'>
			                          <table class='table'>
			                            <thead class='thead'>
			                              <th>Description En</th>
			                              <th>Description Fr</th>
			                              <th style='text-align: right;'>Delete</th>
			                            </thead>
			                            <tbody id='listsponsordetail'>
			                                <?php
			                                	if(isset($row->boxid)){
			                                		$query=$this->db->query("SELECT * FROM sch_sponsor_boxdetail WHERE boxid='".$row->boxid."'")->result();
			                               			if (count($query)>0){
				                                		foreach ($query as $drow) {
				                                			echo "<tr style='border-bottom: 1px solid #DDDDDD !important;'>
																	<td><input style='display:none;' type='text' class='sponsordetailen' name='sponsordetailen[]' value='' />
																		<textarea class='form-control'  name='descen_de[]' required  data-parsley-required-message='Enter Descrition English'>".$drow->des_eng."</textarea>
																	</td>
																	<td><input style='display:none;' type='text' class='sponsordetailfr' name='sponsordetailfr[]' value='' />
																		<textarea class='form-control'  name='descfr_de[]' required  data-parsley-required-message='Enter Description France'>".$drow->des_fr."</textarea>
																	</td>
																	<td style='text-align: right;'>
															    		<a>
															    			<img onclick='removerow(event);' src='".base_url()."assets/images/icons/delete.png' />
															    		</a>
															    	</td>
																  </tr>";
				                                		}
				                                	}
			                                	}
			                                ?>
			                            </tbody>
			                          </table>
	     						</div>
	     					</div>
	     				</div>
	     			</div>
	     			<!-- End not use -->
	     		<!-- </div> -->
	     		<!-- <div class="row"> -->
		          <div class="col-sm-5">
		            <div class="form_sep">
		            	<input type="submit" value="Save" id="save" name="save" class="btn btn-success">
		            	<input type="button" value="Cancel" id="cancel" name="cancel" class="btn btn-warning">
		            	<!-- <button id="save" name="save" type="submit" class="btn btn-success">Save</button> -->
		            </div>
		          </div>
		        <!-- </div> -->
	     	</form>
	     	<!--End Form-->
	    </div>
	</div>
</div>
<style type="text/css">
	a{cursor: pointer;}	
</style>
<script type="text/javascript">
	function l_lettertype(event){
		var selected = $("#letter_type").val();
	    if(selected == 'Other'){
	      $('#l_other').show();
	      $("#other").focus();
	    }else{
			$("#l_other").hide();
	    }
	}
	function vcheck(event){
		if($('#isvisit').is(":checked")){
			$("#vdate").val($("#vdateh").val());
			$("#v_date").show();
		}else{
			$("#vdate").val('');
			$("#v_date").hide();
		}
    } 
	function isNumberKey(evt){
        var e = window.event || evt;
        var charCode = e.which || e.keyCode; 
        if ((charCode > 47 && charCode < 58 ) || charCode == 8 || charCode==37 || charCode==39 || charCode==46){  
            return true; 
        } 
        return false;  
	}
	function nothingSponsor(){
		var sponsor=$('#sponsor').val();
		if(sponsor==''){
			$('#s_sponsorid').val('');
		}
	}
	function nothingStudent(){
		var student=$('#student').val();
		if(student==''){
			$('#s_studentid').val('');
		}
	}
	$(function(){
		//$("#vdate").val(''); 
   		$("#save").click(function(e){
          	//----- validate nothing or not -----------
			e.preventDefault();
			var sponsorid=$('#s_sponsorid').val();
			var studentid=$('#s_studentid').val();
			var classid=$('#hiddenclassid').val();
			if(sponsorid!='' && studentid!='' && classid!=''){
			    $('#i_exist').html('');
				$('#sponsor_register').submit();
			  	////----- Validate exist or not -------------
			  	////----- Not validate cos it many to many
			    //  var boxid=$('#s_boxid').val();
			    //  $.ajax({
				// 		url:"<?php echo base_url(); ?>index.php/social/sponsorbox/validate",    
				// 		data: { 'sponsor':sponsorid,
		        //                   'student':studentid,
				// 			'boxid':boxid
				// 		},
				// 		type: "POST",
				// 		success: function(data){
			    //         if(data>0){
			    //            $('html,body').animate({ scrollTop: 0 }, 500);
			    //            $('.c_exist').html('Data is already exist...!');
			    //         }else{
			    //            $('#sponsor_register').submit();
			    //         }   
				// 		}
				// });
			}else{
				$('html,body').animate({ scrollTop: 0 }, 500);
				$('.c_exist').html('');
			    $('#i_exist').html('Please select any sponsor or student or class...!');
			}
     	});  

		// var boxid=$("#s_boxid").val();
		// if (boxid!='')
		autostudent();
		autosponsor();
		$('#sponsor_register').parsley();
		$("#receptiondate,#sendingboxdate,#vdate,#dateanswertoques,#datethanks,#datesendthanksparis").datepicker({
	  		language: 'en',
	  		pick12HourFormat: true,
	  		format:'dd-mm-yyyy'
		});

	})
	function PreviewImage() {
      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

      oFReader.onload = function (oFREvent) {
          document.getElementById("uploadPreview").src = oFREvent.target.result;
          document.getElementById("uploadPreview").style.backgroundImage = "none";
      };
  	};
	function get_year(){
		var get_year = $("#year").val();
		$(".get_year").val(get_year);
	}
	function autostudent(){	
		var fillrespon="<?php echo site_url("social/sponsorbox/fillstudent")?>";
    	$("#student").autocomplete({
			source: fillrespon,
			minLength:0,
			select: function(events,ui) {	
				$('#s_studentid').val(ui.item.id);
				getClass();
				autosponsor();
				$('#sponsor').val('');
				$('#s_sponsorid').val('');
			}						
		});
	}
	$("#s_yearid").change(function() {
	    getClass();
	});
	function getClass(){
		var studentid=$('#s_studentid').val();
		var yearid=$('#s_yearid').val();
        $.ajax({
            url:"<?php echo site_url("social/sponsorbox/getClass")?>/"+studentid+"/"+yearid,    
	        type:"post",
	        success: function(data){
	           $("#s_classid option[value='"+data+"']").prop('selected', true);
	           $("#hiddenclassid").val(data);
	        }
        });
	}
	function autosponsor(){	
		var studentid=$('#s_studentid').val();	
		var fillrespon="<?php echo site_url("social/sponsorbox/fillsponsor")?>/"+studentid;
    	$("#sponsor").autocomplete({
			source: fillrespon,
			minLength:0,
			select: function(events,ui) {	
				$('#s_sponsorid').val(ui.item.id);
				//autostudent();
				//$('#s_studentid').val("");
				//$('#student').val("");
			}						
		});
	}
	function getsponsordetailtolist(sponsorid){
    $.ajax({
            url:"<?php echo site_url(); ?>social/sponsorbox/getsponsortolist",    
            data: {'sponsor':sponsorid},
            type:"post",
            success: function(data){
              $('#listsponsordetail').append(data);
            }
        });
  	}
	function removerow(event){
	    //var transno=$(event.target).attr('tran');
	    var r = confirm("Are you sure to delete this item !");
	      if (r == true) {
	        //var row_class=$(event.target).closest('tr').remove();
	        $(event.target).closest('tr').remove();
	      }
	}
	$("#cancel").click(function(){
		/*
			1 clear all control in the form.
			2 when cancel we can save new although it was edit. 
		*/
		$("#s_boxid").val('');
		$("#sponsor").val('');
		$("#s_sponsorid").val('');
		$("#student").val('');
		$("#s_studentid").val('');
		$("#s_classid").val('');
		$("#hiddenclassid").val('');
		//$("#s_yearid").val('');
		$("#gift").val('');
		$("#detailfr").val('');
		$("#detailen").val('');
		$("#note").val('');
		$("#sp_fr").val('');
		$("#sp_en").val('');
		$("#other").val('');
		var d = new Date();
		var t = d.getDate()+'-'+(d.getMonth()+1)+'-'+d.getFullYear();
		$("#receptiondate").val(t);
		$("#sendingboxdate").val(t);
		$("#datesendthanksparis").val(t);
		$("#sendingboxdate").val(t);

		$('#isvisit').attr('checked', false);
        $("#v_date").hide();
        $("#vdate").val('');
        jQuery("#uploadPreview").attr('src','<?php echo site_url('../assets/upload/NoImage.png') ?>' );
    }); 
</script>