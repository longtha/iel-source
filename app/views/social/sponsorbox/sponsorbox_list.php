
<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	a,.sort{cursor: pointer;}
	.cur_sort_up{
		background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	.cur_sort_down{
		background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	
</style>
<?php
	$yearid=$this->session->userdata('year');
	$m='';
	$p='';
	if(isset($_GET['m'])){
	    $m=$_GET['m'];
	}
	if(isset($_GET['p'])){
	    $p=$_GET['p'];
	}
 ?>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
		      	<div class="col-xs-6">
		      		<strong id='title'>Sponsor Box List</strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
			    	<span class="top_action_button">	
			    		<?php if($this->green->gAction("E")){ ?>
				    		<a href="#" id="exports" title="Export">
				    			<img id='export' src="<?php echo base_url('assets/images/icons/export.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>
		      		<span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ ?>
							<a href="#" id="print" title="Print">
				    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
				    		</a>
				    	<?php } ?>
		      		</span>		    	
			    	<span class="top_action_button">
			    		<?php if($this->green->gAction("C")){ ?>
				    		<a href="<?php echo site_url("social/sponsorbox/add?m=$m&p=$p")?>" >
				    			<img src="<?php echo base_url('assets/images/icons/add.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>	      		
		      	</div> 			      
		  </div>
	      <!-- <div class="row"> -->
	      	<div class="col-sm-12">
	      		<div class="panel panel-default">
	      			<div class="panel-body">
		           		<div class="table-responsive" >
		           			<?php 
		           			$thr="";	
		           			$tr="";	
		           			$school=$this->db->where('schoolid',$this->session->userdata('schoolid'))->get('sch_school_infor')->row();
		           			$school_name=$school->name;
		           			$school_adr=$school->address;
		           			foreach($thead as $th=>$val){
		           				if($th=='No' || $th=='Nationality')
		           					$thr.="<th class='$val'>".$th."</th>";
		           				else if($th=='Action')
		           					$thr.="<th class='remove_tag'>".$th."</th>";
		           				else
		           					$thr.="<th class='sort $val' onclick='sort(event);' rel='$val'>".$th."</th>";								
		           			}
		           			if(count($tdata)>0){
		           				$i=1;
		           				if(isset($_GET['per_page']))
									$i=$_GET['per_page']+1;
								foreach ($tdata as $row) {
									$tr.="<tr>
											<td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
											<td class='student no_wrap'>".$row->stu_fullname."</td>
											<td class='class'>".$row->class_name."</td>
											<td class='sponsor'>".$row->sp_fullname."</td>";
											$tr .= "<td class='rdate'>".$this->green->convertSQLDate($row->reception_date)."</td>
											
											<td class='tdate'>".$this->green->convertSQLDate($row->date_sendthanks_paris)."</td>
											<td class='vdate'>";
												if ($row->visite_date == '00-00-0000' || $row->visite_date =='0000-00-00'){
												 	$tr .="";
												}else{
													$tr .= $this->green->convertSQLDate($row->visite_date);
												}
											$tr .= "</td>
											<td class='remove_tag no_wrap'>";
											if($this->green->gAction("P")){	
												$tr.="<a><img rel=".$row->boxid." onclick='preview(event);' src='".site_url('../assets/images/icons/preview.png')."'/></a>";
											}
											if($this->green->gAction("U")){	
												$tr.="<a><img  rel=".$row->boxid." onclick='update(event);' src='".site_url('../assets/images/icons/edit.png')."'/></a>";
											}
											if($this->green->gAction("D")){	
												$tr.="<a><img rel=".$row->boxid." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/></a>";
											}
										$tr.="</td>
											 </tr> ";	
										 $i++;		
								}
								
							}
													
		           			?>
		           			<input type='hidden' value='ASC' name='sort' id='sort' style='width:80px;'/>
		       				<input type='hidden' value='' name='sortquery' id='sortquery' style='width:200px;'/>
		           			<table class="table" border='0'>
		           				<thead ><?php echo $thr ?></thead>
		           				<thead class='remove_tag'>
		           					<td>
		           						<input type='text' value='asc' name='sort' id='sort' style='width:30px; display:none'/>
									</td>
		           					<td class='col-xs-1'>
		           						<input type='text' value="<?php if(isset($_GET['s_id'])) echo $_GET['s_id']; ?>" onkeyup='search(event);' class='form-control input-sm' id='s_student_num'/>
		           					</td>
		           					<td width='120'>
		           						<select class='form-control input-sm parsley-validated' onchange='search(event);' name='s_classid' id='s_classid'>
	                   						 <option value="">Select class</option>
						                    <?php
						                    	foreach ($this->db->select("*")->from('sch_class')->order_by('classid', 'asc')->get()->result() as $level) {?>
						                    		<option value="<?php echo $level->classid ;?>" <?php if(isset($_GET['class'])){ if($level->classid==$_GET['class']) echo 'selected'; }?> ><?php echo $level->class_name;?></option>
						                    <?php }
						                    ?>
					                 	</select>
		           					</td>
							 		<td width='150px'>
							 			<input type='text' id='s_sponsor' name='s_sponsor' value="<?php if(isset($_GET['s'])) echo $_GET['s']; ?>" onkeyup='search(event);' class='form-control input-sm'/>
							 		</td>
							 		<td colspan="4">
							 			<div class="row">
										  <div class="col-xs-5">	
												<table width='200px' class='table-responsive' style='border: 0px;'>
													<tr>
														<td class='no_wrap'>Receive From: </td>
														<td width='120px'>
															<div class='input-group new_ex' id='datetimepicker'>
															    <input value="<?php if(isset($_GET['s_name'])) echo $_GET['s_date'] ?>" type='text' class='form-control input-sm s_date' name='sdate' id='sdate'/>
															    <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'>
															</div>
														</td>
													</tr>
												</table>
										  </div>
										  <div class="col-xs-5">
										  		<table width='200px' class='table-responsive' style='border: 0px;'>
										  			<tr>
										  				<td class='no_wrap'>Receive To: </td>
										  				<td width='120px'>
										  					<div class='input-group new_ex' id='datetimepicker'>
															    <input value="<?php if(isset($_GET['s_name'])) echo $_GET['e_date'] ?>" type='text' class='form-control input-sm end_date' name='edate' id='edate'/>
															    <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'>
															</div>
										  				</td>
										  			</tr>
										  		</table>	
										  </div>
										  <div class="col-xs-1">
										   		<button type="button" onClick='search(event);' class="btn btn-default btn-sm">
												  	<span class="glyphicon glyphicon-search"  aria-hidden="true"></span> Search
												</button>	
										  </div>
										</div></td>
							 		<td></td>
							 		<input type='text' value='' name='sortquery' id='sortquery' style='width:80px; display:none'/></td>
							 		<td></td>
							 		<!--<td></td>
							 		<td></td> -->

		           				</thead>
		           				<tbody class='listbody'>
		           					<?php echo $tr ?>
		           					<tr class='remove_tag'>
										<td colspan='12' id='pgt'>
											<div style='margin-top:20px; width:11%; float:left;'>
											Display : <select id="sort_num"  onchange='search(event);' style='padding:5px; margin-right:0px;'>
															<?php
															$num=50;
															for($i=0;$i<10;$i++){?>
																<option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
																<?php $num+=50;
															}
															?>
															<option value='all' <?php if(isset($_GET['s_num'])){ if($_GET['s_num']=='all') echo 'selected'; }?>>All</option>
														</select>
											</div>
											<div style='text-align:center; verticle-align:center; width:89%; float:right;'>
												<ul class='pagination' style='text-align:center'>
													
													<?php echo $this->pagination->create_links(); ?>
												</ul>
											</div>

										</td>
									</tr> 
		           				</tbody>
		           			</table>
						</div>  
					</div>
	      		<!-- </div> -->
	      	</div>	      	
	      </div> 
	    </div>
   </div>
</div>
<div class="modal fade bs-example-modal-sm" id='loading' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
         <div class="modal-content">
			<img src="<?php echo base_url().'assets/images/icons/loading.gif'?>">
		</div>
	</div>
</div>
<!--Check Print Export-->
<input type='text' id='page' class='hide' value="<?PHP if(isset($_GET['per_page'])) echo $_GET['per_page'] ?>" />
<div class="table-responsive hide"  id="tab_print">
  		<table class="export" id='exptbl' border='1'>

  		</table>
</div>
<div class="modal fade bs-example-modal-lg" id="exporttap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="wrapper">
				<div class="clearfix" id="main_content_outer">
				    <div id="main_content">
					    <div class="result_info">
					    	<div class="col-sm-6">
					      		<strong>Choose Column To Export</strong>
					      	</div>
					      	<div class="col-sm-6" style="text-align: center">
					      		<strong>
					      			<center class='visit_error' style='color:red;'></center>
					      		</strong>	
					      	</div>
					    </div>
					      	<form enctype="multipart/form-data" name="frmvisit" id="frmvisit" method="POST">
						        <div class="row">
									<div class="col-sm-12">
							            	<div class="panel-body">
							            		<div class='modal-body table-responsive'>
							            			<?php 
							            				$thead=array(
															"No"          => 'no',
															"Student"     => 'student',
															"Class"       => 'class',
															"Sponsor"     => 'sponsor',
															"Receive Date"=> 'rdate',
															"Reply Date"  => 'tdate',
															"Visit Date"  => 'vdate',
															"Gift"        => 'gift',
															"Detail Fr"   => 'dfr',
															"Detail En"   => 'den',
															"Letter type" => 'ltype',
															"Language"    => 'lang',
															"Question Fr" => 'qfr',
															"Question En" => 'qen',
															"Year"        => 'year',
															"Note"        => 'note'
														);
							            			?>
									               <table width="100%" align="center" class="table">
									               		<tr>
											        		<?php foreach ($thead as $value=>$key) {
											        			echo "<th align='center'>$value</th>";
											        		} ?>
											        	</tr>
											        	<tr>
											        		<?php foreach ($thead as $value=>$key) {
											        			echo '<td><input type="checkbox" class="chk" rel="'.$key.'" value="'.$value.'" checked="checked"></td>';
											        		} ?>
											        	</tr>
									               </table>
											   </div>
								            </div>
								    </div> 
								</div>
					      </form>
					</div> 
			    </div>
			</div> 
            <div class="modal-footer">
            	<label><input type="checkbox" name='is_all' id='is_all' value='0'>Print/Export All Page</label>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id='btnprint' class="btn btn-primary" onclick='expdata(event);'>Print</button>
                <button type="button" id='btnexport' class="btn btn-primary" onclick='expdata(event);'>Export</button>
            </div>
        </div>                       <!-- /.modal-content -->
    </div>
                                <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
	// getviewpermis();
	// function getviewpermis(){
	// 	var schlevelid=$('#schlevelid').val();
	// 	if(schlevelid>0 || schlevelid!='')
	// 		search();
	// }
	$('#exports').click(function(){
		$('#exporttap').modal('show');
		$('#btnprint').hide();
		$('#btnexport').show();
	})
	$('#print').click(function(){
		$('#exporttap').modal('show');
		$('#btnprint').show();
		$('#btnexport').hide();
	})
	function gsPrint(emp_title,data){
		 var element = "<div>"+data+"</div>";
		 $("<center><p style='padding-top:15px;text-align:center;'><b>"+emp_title+"</b></p>"+element+"</center>").printArea({
		  mode:"popup",  //printable window is either iframe or browser popup              
		  popHt: 600 ,  // popup window height
		  popWd: 500,  // popup window width
		  popX: 0 ,  // popup window screen X position
		  popY: 0 , //popup window screen Y position
		  popTitle:"Sponsor List", // popup window title element
		  popClose: false,  // popup window close after printing
		  strict: false 
		  });
	}
	$('.colch').click(function(){
		if($(this).is(':checked')){
			var col=$(this).attr('rel');
			$('.'+col).removeClass(' remove_tag');
		}else{
			var col=$(this).attr('rel');
			$('.'+col).addClass(' remove_tag');
		}
	})
	$(function(){
		$("#sdate,#edate").datepicker({
	      		language: 'en',
	      		pick12HourFormat: true,
	      		format:'dd-mm-yyyy'
		});

		$("#btnprint").on("click",function(){
			var htmlToPrint = '' +
			        '<style type="text/css">' +
			        'table th, table td {' +
			        'border:1px solid #000 !important;' +
			        'padding;0.5em;' +
			        '}' +
			        '</style>';
			var title="<div style='width:300px; text-align:left;'><span style='font-weight:bold; font-size:16px;'><?php echo $school_name; ?></span><br>";
			var s_adr="<?php echo $school_adr; ?></div>";
				title+=s_adr;
				title +="<h4 align='center'>"+ $("#title").text()+"</h4>";
			var year =$("#year :selected").text();
				title+="Year : "+year;
			//if($('#s_levelid').val()!='')
				//title+="<br>Class : " + $('#s_levelid :selected').text();
		   	var data = $("#tab_print").html().replace(/<img[^>]*>/gi,"");
		   	var export_data = $("<center>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
		   		export_data+=htmlToPrint;
		   	gsPrint(title,export_data);
		});
		$("#btnexport").on("click",function(e){
			var title="<div style='width:300px; text-align:left;'><span style='font-weight:bold; font-size:16px;'><?php echo $school_name; ?></span><br>";
			var s_adr="<?php echo $school_adr; ?></div>";
					title+=s_adr;
					title +="<h4 align='center'>"+ $("#title").text()+"</h4>";
			var year =$("#year :selected").text();
					title+="Year : "+year;
				//if($('#s_levelid').val()!='')
					//title+="<br>Class : " + $('#s_levelid :selected').text();
			var data=$('.exptbl').attr('border',1);
				data = $("#tab_print").html().replace(/<img[^>]*>/gi,"");
   			var export_data = $("<center><h3 align='center'>"+title+"</h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
			window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
			e.preventDefault();
			$('.table').attr('border',0);
		});
	})
	$('#classid').change(function(){
		search();
	})
	function search(event){
		if(sort==''){
			$('.panel div span').removeClass('label-danger');
			$('.panel div span').addClass('label-default');
		}
		var roleid    = <?php echo $this->session->userdata('roleid');?>;
		var m         = <?php echo $m;?>;
		var p         = <?php echo $p;?>;
		var year      = $('#year').val();
		var s_name    = jQuery('#s_student_num').val();
		var classid   = $('#s_classid').val();
		var sponsor   = $('#s_sponsor').val();
		var sort_num  = $('#sort_num').val();
		var sortquery = $('#sortquery').val();
		var sdate     = $('#sdate').val();
		var edate     = $('#edate').val();
		if(sortquery  == '')
			sortquery = 'ORDER BY sp.boxid DESC';
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/social/sponsorbox/search",    
				data: {
						'roleid'   : roleid,
						'm'        : m,
						'p'        : p,
						'sort'     : sortquery,
						'sort_num' : sort_num,
						's_name'   : s_name,
						'year' 	   : year,
						'class'    : classid,
						'sponsor'  : sponsor,
						'sdate'    : sdate,
						'edate'    : edate
				    },
				type: "POST",
				success: function(data){
                   jQuery('.listbody').html(data);
				}
		});
	}
	$('#year').change(function(){
		var year_id=$(this).val();
		search();
	})
	function deletes(event){
		var r = confirm("Are you sure to delete this item !");
		if (r == true) {
		    var boxid=jQuery(event.target).attr("rel");
			location.href="<?PHP echo site_url('social/sponsorbox/delete');?>/"+boxid+"?<?php echo "m=$m&p=$p" ?>";
		} else {
		    txt = "You pressed Cancel!";
		}	
	}
	function loading(){
		$('#loading').modal('show');
        setTimeout(function() { $('#loading').modal('hide'); }, 2000);
	}
	function update(event){
		var boxid=jQuery(event.target).attr("rel");
		location.href="<?PHP echo site_url('social/sponsorbox/edit');?>/"+boxid+"?<?php echo "m=$m&p=$p" ?>";
	}
	function preview(event){
		var boxid=jQuery(event.target).attr("rel");
		//location.href="<?PHP echo site_url('social/sponsorbox/preview');?>/"+studentid+"?<?php echo "m=$m&p=$p" ?>";
		window.open("<?PHP echo site_url('social/sponsorbox/preview');?>/"+boxid+"?<?php echo "m=$m&p=$p" ?>","_blank");
	}
	function sort(event){
		var sort=$(event.target).attr('rel');
		var rel="";
		
		if (sort=='student'){
			rel='stu_fullname';
		}else if (sort=='class'){
			rel='c.class_name';
		}else if (sort=='sponsor'){
			rel='sp_fullname';
		}else if (sort=='vdate'){
			rel='sp.visite_date';
		}else if (sort=='rdate'){
			rel='sp.reception_date';
		}else if (sort=='sdate'){
			rel='sp.sending_box_date';
		}else if (sort=='tdate'){
			rel='sp.date_sendthanks_paris';
		}
		var sorttype=$('#sort').val();
		if(sorttype=='asc'){
			$('#sort').val('desc');
			$('.sort').removeClass('cur_sort_down');
			$(event.target).addClass('cur_sort_up');
		}
		else{
			$('#sort').val('asc');
			$('.sort').removeClass('cur_sort_up');
			$(event.target).addClass('cur_sort_down');
		}
		$('#sortquery').val('ORDER BY '+rel+' '+sorttype);
		search();
	}
	function expdata(event){
		var s_student_num 	= $('#s_student_num').val();
		var s_classid 		= $('#s_classid').val();
		var s_sponsor 		= $('#s_sponsor').val();
		var sdate 			= $('#sdate').val();
		var edate 			= $('#edate').val();
		var sort_num 		= $('#sort_num').val();
		var sortstd 		= $('#sortquery').val();
		var page 			= $('#page').val();
		var year 			= $('#year').val();
		var is_all=0;
		if($('#is_all').is(':checked'))
			is_all=1;
		if(sortstd=='')
				sortstd=' ORDER BY sp.boxid DESC ';
		var field={};
		$('.chk').each(function(){
			if($(this).is(':checked')){
				var key=$(this).attr('rel');
				var val=$(this).val();
				field[key]=val;
			}
		})
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/social/sponsorbox/getexp",    
				data: {
						'page' 		   : page,
						'is_all' 	   : is_all,
						'f' 		   : field,
						'sort_num'     : sort_num,
						'sort'         : sortstd,
						's_student_num': s_student_num,
						's_classid'    : s_classid,
						's_sponsor'    : s_sponsor,
						'sdate'        : sdate,
						'edate'        : edate,
						'year'         : year
				},
				type: "POST",
				dataType:'json',
				async:false,
				success: function(data){
					$('#exptbl').html(data.data);
					
					// var s=$(event.target).attr('rel');
					// if(s=='E')
					// 	exports();
					// else
					// 	prints();
			}
		});
	}
</script>