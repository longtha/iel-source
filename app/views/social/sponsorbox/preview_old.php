
<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	a,.sort{cursor: pointer;}
	.cur_sort_up{
		background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	.cur_sort_down{
		background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	
</style>
<?php

$yearid=$this->session->userdata('year');
$m='';
$p='';
if(isset($_GET['m'])){
    $m=$_GET['m'];
}
if(isset($_GET['p'])){
    $p=$_GET['p'];
}
 ?>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
		      	<div class="col-xs-6">
		      		<strong id='title'>Sponsor's Letter and Gift Report</strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
			    	<span class="top_action_button">	
			    		<?php if($this->green->gAction("E")){ ?>
				    		<a href="#" id="exports" title="Export">
				    			<img id='export' src="<?php echo base_url('assets/images/icons/export.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>
		      		<span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ ?>
							<a href="#" id="print" title="Print">
				    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
				    		</a>
				    	<?php } ?>
		      		</span>		      		
		      	</div> 			      
		  </div>
	      	<div class="col-sm-12">
	      		<div class="panel panel-default">
	      			<div class="panel-body">
		           		<div class="table-responsive" id="tab_print">
		           			<?php	
			            		$tr="";	
								if(count($tdata)>0){
									foreach ($tdata as $row) {
										$tr.="<tr>
												<td class='student'>".$this->green->convertSQLDate($row->reception_date)."</td>
												<td class='sponsor'>".$row->sp_fullname."</td>
												<td class='l_fr'>".$row->spr_ques_fr."</td>
												<td class='l_en'>".$row->spr_ques_eng."</td>
												<td class='g_fr'>".$row->des_fr."</td>
												<td class='g_en'>".$row->des_eng."</td>
												<td class='g_en'>";
													if($row->visite_date=='0000-00-00'){
														$tr.='';
													}else{
														$tr.=$this->green->convertSQLDate($row->visite_date);
													}
												"</td>
											</tr>";			
									}
								}					
		           			?>
		           			<h3 align="center"><u>Sponsor's Letter and Gift Report</u></h3></br>
		           			<p><strong>Student name :</strong>.................<?php echo $student->stu_fullname;?>.....................<strong>Class :</strong>.................<?php echo $class->classname;?>.....................</p>
		           			<table class="table table-bordered">
			           			<tr>
								    <th rowspan="2" style='vertical-align:middle;'>Date</th>
								    <th rowspan="2" style='vertical-align:middle;'>Sponsor</th>
								    <th colspan="2" style='text-align:center'>Letter</th>
								    <th colspan="2" style='text-align:center'>Gift</th>
								    <th rowspan="2" style='vertical-align:middle;'>Sponsor Visit Date</th>
								  </tr>
		           				<tr>
								    <th>French</th>
								    <th>English</th>
								    <th>French</th>
								    <th>English</th>
								</tr>
		           				<tbody class='listbody'>
		           					<?php echo $tr; ?> 
		           				</tbody>
		           			</table>
						</div>  
					</div>
	      		</div>
	      	</div>	      
	    </div>
   </div>
</div>
<script type="text/javascript">
	$(function(){			
		 $("#print").on("click",function(){
			 gPrint("tab_print");
		 });	
	  
	     $("#export").on("click",function(e){
			var title=$('#title').text();
			var data=$('.table').attr('border',1);
			var datas = $("#tab_print").html();
			data = $("#tab_print").html().replace(/<A[^>]*>|<\/A>/gi,"");
			var export_data = $("<div><center><h3 align='center'>"+title+"</h3>"+data+"</center></div>").clone().find(".remove_tag").remove().end().html();
			window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
			e.preventDefault();
			$('.table').attr('border',0);
		 });
	 });
</script>