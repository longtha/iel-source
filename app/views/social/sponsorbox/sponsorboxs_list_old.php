
<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	a,.sort{cursor: pointer;}
	.cur_sort_up{
		background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	.cur_sort_down{
		background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	
</style>
<?php

$yearid=$this->session->userdata('year');
$m='';
$p='';
if(isset($_GET['m'])){
    $m=$_GET['m'];
}
if(isset($_GET['p'])){
    $p=$_GET['p'];
}
 ?>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
		      	<div class="col-xs-6">
		      		<strong id='title'>Sponsor Box Report</strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
			    	<span class="top_action_button">	
			    		<?php if($this->green->gAction("E")){ ?>
				    		<a href="#" id="exports" title="Export">
				    			<img id='export' src="<?php echo base_url('assets/images/icons/export.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>
		      		<span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ ?>
							<a href="#" id="print" title="Print">
				    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
				    		</a>
				    	<?php } ?>
		      		</span>		      		
		      	</div> 			      
		  </div>
	      <div class="row">
	      	<div class="col-sm-12">
	      		<div class="panel panel-default">
	      			<div class="panel-body">
		           		<div class="table-responsive" id="tab_print">
		           			<?php 
			           			$thr="";
			           			$thr1="";	
			           			$tr="";	
			           			$school=$this->db->where('schoolid',$this->session->userdata('schoolid'))->get('sch_school_infor')->row();
			           			$school_name=$school->name;
			           			$school_adr=$school->address;

			           			foreach($thead as $th=>$val){
			           				if($th=='No'){
			           					$thr.="<th style='vertical-align:middle;' rowspan='2' class='$val'>".$th."</th>";
			           				}else if($th == 'Student'){
										$thr.="<th style='vertical-align:middle;' rowspan='2' class='$val'>".$th."</th>";
			           				}else if($th == 'Class'){
										$thr.="<th style='vertical-align:middle;' rowspan='2' class='$val'>".$th."</th>";
			           				}else if($th == 'Sponsor'){
										$thr.="<th style='vertical-align:middle;' rowspan='2' class='$val'>".$th."</th>";
			           				}else if($th == 'Letter'){
										$thr.="<th colspan='2' class='$val'>".$th."</th>";
			           				}else if($th == 'Gift'){
										$thr.="<th colspan='2' class='$val'>".$th."</th>";
			           				}

			           				if($th == 'French-1'){
										$thr1.="<th  class='$val'>French</th>";
			           				}else if($th == 'English-1'){
										$thr1.="<th class='$val'>English</th>";
			           				}else if($th == 'French-2'){
										$thr1.="<th class='$val'>French</th>";
			           				}else if($th == 'English-2'){
										$thr1.="<th class='$val'>English</th>";
			           				}					
			           			}

			           			
			           			if(count($tdata)>0){
			           				$i=1;
			           				if(isset($_GET['per_page']))
										$i=$_GET['per_page']+1;
									foreach ($tdata as $row) {
										$tr.="<tr>
												<td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
												<td class='student'>".$row->stu_fullname."</td>
												<td class='class'>".$row->class_name."</td>
												<td class='sponsor'>".$row->sp_fullname."</td>
												<td class='l_fr'>".$row->spr_ques_fr."</td>
												<td class='l_en'>".$row->spr_ques_eng."</td>
												<td class='g_fr'>".$row->des_fr."</td>
												<td class='g_en'>".$row->des_eng."</td>
												</tr>";	
											 $i++;		
									}
								}					
		           			?>
		           			<table class="table table-bordered">
		           				<thead>
		           					<tr><?php echo $thr; ?></tr>
		           					<tr><?php echo $thr1; ?></tr>
		           				</thead>
		           				<thead class='remove_tag' >
		           					<td width='5%'>
		           						<input type='text' value='asc' name='sort' id='sort' style='width:30px; display:none'/>
									</td>
		           					<td width='15%'>
		           						<input type='text' value="<?php if(isset($_GET['s_id'])) echo $_GET['s_id']; ?>" onkeyup='search(event);' class='form-control input-sm' id='s_student_num'/>
		           					</td>
		           					<td width='8%'>
		           						<select class='form-control input-sm parsley-validated' onchange='search(event);' name='s_classid' id='s_classid'>
	                   						<option value="">Select class</option>
						                    <?php
						                    	foreach ($this->db->select("*")->from('sch_class')->order_by('classid', 'desc')->get()->result() as $class) {?>
						                    		<option value="<?php echo $class->classid ;?>" <?php if(isset($_GET['class'])){ if($class->classid==$_GET['class']) echo 'selected'; }?> ><?php echo $class->class_name;?></option>
						                    <?php }
						                    ?>
					                 	</select>
		           					</td>
							 		<td width='15%'>
							 			<input type='text' id='s_sponsor' name='s_sponsor' value="<?php if(isset($_GET['s'])) echo $_GET['s']; ?>" onkeyup='search(event);' class='form-control input-sm'/>
							 		</td>
							 		<td colspan='4'>
							 			<div class="col-xs-5">
								 			<table width='200px' class='table-responsive' style='border: 0px;'>
												<tr>
													<td class='no_wrap'>Receive From: </td>
													<td width='120px'>
														<div class='input-group new_ex' id='datetimepicker'>
														    <input value="<?php if(isset($_GET['s_name'])) echo $_GET['s_date'] ?>" type='text' class='form-control input-sm s_date' name='sdate' id='sdate'/>
														    <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'>
														</div>
													</td>
												</tr>
											</table>
										</div>
										<div class="col-xs-5">
											<table width='200px' class='table-responsive' style='border: 0px;'>
										  			<tr>
										  				<td class='no_wrap'>Receive To: </td>
										  				<td width='120px'>
										  					<div class='input-group new_ex' id='datetimepicker'>
															    <input value="<?php if(isset($_GET['s_name'])) echo $_GET['e_date'] ?>" type='text' class='form-control input-sm end_date' name='edate' id='edate'/>
															    <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'>
															</div>
										  				</td>
										  			</tr>
										  		</table>
										</div>
										<div class="col-xs-1">
									   		<button type="button" onClick='search(event);' class="btn btn-default btn-sm">
											  	<span class="glyphicon glyphicon-search"  aria-hidden="true"></span> Search
											</button>	
										</div>	
							 		</td>
							 		<input type='text' value='' name='sortquery' id='sortquery' style='width:80px; display:none'/>
		           				</thead>
		           				<tbody class='listbody'>
		           					<?php echo $tr ?>
		           					<tr class='remove_tag'>
										<td colspan='12' id='pgt'>
											<div style='margin-top:20px; width:11%; float:left;'>
											Display : <select id="sort_num"  onchange='search(event);' style='padding:5px; margin-right:0px;'>
															<?php
															$num=50;
															for($i=0;$i<10;$i++){?>
																<option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
																<?php $num+=50;
															}
															?>
															<option value='all' <?php if(isset($_GET['s_num'])){ if($_GET['s_num']=='all') echo 'selected'; }?>>All</option>
														</select>
											</div>
											<div style='text-align:center; verticle-align:center; width:89%; float:right;'>
												<ul class='pagination' style='text-align:center'>
													
													<?php echo $this->pagination->create_links(); ?>
												</ul>
											</div>

										</td>
									</tr> 
		           				</tbody>
		           			</table>
						</div>  
					</div>
	      		</div>
	      	</div>	      	
	      </div> 
	    </div>
   </div>
</div>
<div class="modal fade bs-example-modal-sm" id='loading' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
         <div class="modal-content">
			<img src="<?php echo base_url().'assets/images/icons/loading.gif'?>">
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal-lg" id="exporttap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="wrapper">
				<div class="clearfix" id="main_content_outer">
				    <div id="main_content">
					    <div class="result_info">
					    	<div class="col-sm-6">
					      		<strong>Choose Column To Export</strong>
					      	</div>
					      	<div class="col-sm-6" style="text-align: center">
					      		<strong>
					      			<center class='visit_error' style='color:red;'></center>
					      		</strong>	
					      	</div>
					    </div>
					      	<form enctype="multipart/form-data" name="frmvisit" id="frmvisit" method="POST">
						        <div class="row">
									<div class="col-sm-12">
							            	<div class="panel-body">
							            		<div class='table-responsive'>
										               <table class='table'>
										               		<thead >
										               			<?php
										               			foreach($thead as $th=>$val){
										               				if($th!='Action')
											           					echo "<th>".$th."</th>";	
											           			}?>
										               		</thead>
										               		<tbody >
										               			<?php
										               			foreach($thead as $th=>$val){
										               				if($th!='Action')
											           					echo "<td align='center'><input type='checkbox' checked class='colch' rel='".$val."'></td>";	
											           			}?>
										               		</tbody>
										               </table>
											   </div>
								            </div>
								    </div> 
								</div>
					      </form>
					</div> 
			    </div>
			</div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id='btnprint' class="btn btn-primary">Print</button>
                <button type="button" id='btnexport' class="btn btn-primary">Export</button>
            </div>
        </div>                       <!-- /.modal-content -->
    </div>
                                <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
	$('#export').click(function(){
		$('#exporttap').modal('show');
		$('#btnprint').hide();
		$('#btnexport').show();
	})
	$('#print').click(function(){
		$('#exporttap').modal('show');
		$('#btnprint').show();
		$('#btnexport').hide();
	})
	function gsPrint(emp_title,data){
		 var element = "<div>"+data+"</div>";
		 $("<center><p style='padding-top:15px;text-align:center;'><b>"+emp_title+"</b></p><hr>"+element+"</center>").printArea({
		  mode:"popup",  //printable window is either iframe or browser popup              
		  popHt: 600 ,  // popup window height
		  popWd: 500,  // popup window width
		  popX: 0 ,  // popup window screen X position
		  popY: 0 , //popup window screen Y position
		  popTitle:"Sponsor Box List", // popup window title element
		  popClose: false,  // popup window close after printing
		  strict: false 
		  });
	}
	$('.colch').click(function(){
		if($(this).is(':checked')){
			var col=$(this).attr('rel');
			$('.'+col).removeClass(' remove_tag');
		}else{
			var col=$(this).attr('rel');
			$('.'+col).addClass(' remove_tag');
		}
	})
	$(function(){
		$("#sdate,#edate").datepicker({
      		language: 'en',
      		pick12HourFormat: true,
      		format:'dd-mm-yyyy'
		});			
		$("#btnprint").on("click",function(){
			var htmlToPrint = '' +
			        '<style type="text/css">' +
			        'table th, table td {' +
			        'border:1px solid #000 !important;' +
			        'padding;0.5em;' +
			        '}' +
			        '</style>';
			var title="<div style='width:300px; text-align:left;'><span style='font-weight:bold; font-size:16px;'><?php echo $school_name; ?></span><br>";
			var s_adr="<?php echo $school_adr; ?></div>";
				title+=s_adr;
				title +="<h4 align='center'>"+ $("#title").text()+"</h4>";
			var year =$("#year :selected").text();
				title+="Year : "+year;
			// if($('#s_levelid').val()!='')
			// 	title+="<br>Class : " + $('#s_levelid :selected').text();
		   	var data = $("#tab_print").html().replace(/<img[^>]*>/gi,"");
		   	var export_data = $("<center>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
		   		export_data+=htmlToPrint;
		   	gsPrint(title,export_data);
		});
		$("#btnexport").on("click",function(e){
			var title="<div style='width:300px; text-align:left;'><span style='font-weight:bold; font-size:16px;'><?php echo $school_name; ?></span><br>";
			var s_adr="<?php echo $school_adr; ?></div>";
					title+=s_adr;
					title +="<h4 align='center'>"+ $("#title").text()+"</h4>";
			var year =$("#year :selected").text();
					title+="Year : "+year;
				// if($('#s_levelid').val()!='')
				// 	title+="<br>Class : " + $('#s_levelid :selected').text();
			var data=$('.table').attr('border',1);
				data = $("#tab_print").html().replace(/<img[^>]*>/gi,"");
   			var export_data = $("<center><h3 align='center'>"+title+"</h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
			window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
			e.preventDefault();
			$('.table').attr('border',0);
		});
	})
	$('#classid').change(function(){
		search();
	})
	function search(event){
		var roleid   = <?php echo $this->session->userdata('roleid');?>;
		var m        = <?php echo $m;?>;
		var p        = <?php echo $p;?>;
		var year     = $('#year').val();
		var s_name   = $('#s_student_num').val();
		var classid  = $('#s_classid').val();
		var sponsor  = $('#s_sponsor').val();
		var sort_num = $('#sort_num').val();
		var sdate    = $('#sdate').val();
		var edate    = $('#edate').val();
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/social/sponsorboxs/search",    
				data: {
						'roleid'  : roleid,
						'm' 	  : m,
						'p' 	  : p,
						'sort_num': sort_num,
						's_name'  : s_name,
						'year'    : year,
						'class'   : classid,
						'sponsor' : sponsor,
						'sdate'   : sdate,
						'edate'   : edate
				    },
				type: "POST",
				success: function(data){
                   jQuery('.listbody').html(data);
				}
		});
	}
	$('#year').change(function(){
		var year_id=$(this).val();
		search();
	})
</script>