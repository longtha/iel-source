
<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	a,.sort{cursor: pointer;}
	.cur_sort_up{
		background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	.cur_sort_down{
		background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	
</style>
<?php

$yearid=$this->session->userdata('year');
$m='';
$p='';
if(isset($_GET['m'])){
    $m=$_GET['m'];
}
if(isset($_GET['p'])){
    $p=$_GET['p'];
}
 ?>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
		      	<div class="col-xs-6">
		      		<strong id='title'>Sponsor's Letter and Gift Report</strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
			    	<span class="top_action_button">	
			    		<?php if($this->green->gAction("E")){ ?>
				    		<a href="#" id="exports" title="Export">
				    			<img id='export' src="<?php echo base_url('assets/images/icons/export.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>
		      		<span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ ?>
							<a href="#" id="print" title="Print">
				    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
				    		</a>
				    	<?php } ?>
		      		</span>		      		
		      	</div> 			      
		  </div>
	      	<div class="col-sm-12">
	      		<div class="panel panel-default">
	      			<div class="panel-body">
		           		<div class="table-responsive" id="tab_print">
		           			<?php	
			            		$tr="";	
								if(count($tdata)>0){
									//foreach ($tdata as $row) {
										$tr.="<tr>
												<td class='l_fr' width=50%>".$tdata->gift."</td>
												<td class='l_en' width=50%>".$tdata->lettertype."</td>
											</tr>";	
											// <td class='g_fr' width=25%>".$tdata->des_fr."</td>
											// <td class='g_en' width=25%>".$tdata->des_eng."</td>		
									//}
								}					
		           			?>
		           			<h3 align="center"><u>Sponsor's Letter and Gift Report</u></h3></br>
		           			<p><strong>Date of Reply :</strong>................<?php echo $this->green->formatSQLDate($replydate);?>....................</p>
		           			<p><strong>Student name :</strong>................<?php echo $student->stu_fullname;?>.....................</p>
		           			<table class="table table-bordered">
			           			<tr>
			           				<th style='text-align:center'>Gift</th>
								    <th style='text-align:center'>Letter</th>
								</tr>
		           				<tbody class='listbody'>
		           					<?php echo $tr; ?> 
		           				</tbody>
		           			</table>
		           			<div style="float:left;" class="col-sm-6"><strong>From :</strong>.......<?php echo $sponsor->sp_fullname;?>.......</div>
							<div class="form_sep col-sm-6" style="float:right;">
								<img src="<?php if(isset($boxid)) {if(@ file_get_contents(site_url('../assets/upload/sponsorbox/'.$boxid.'.jpg'))){ echo site_url('../assets/upload/sponsorbox/'.$boxid.'.jpg'); }else{ echo site_url('../assets/upload/NoImage.png');}}else{ echo site_url('../assets/upload/NoImage.png');} ?>" id="uploadPreview" style='width:120px; height:150px; margin-bottom:15px'>
		                    	<input id="uploadImage" type="file" accept="image/gif, image/jpeg, image/jpg, image/png" name="userfile" onchange="PreviewImage();" style="visibility:hidden; display:none;" />
				            </div>
					        </div>
						</div>  
					</div>
	      		</div>
	      	</div>	      
	    </div>
   </div>
</div>
<script type="text/javascript">
	$(function(){			
		 $("#print").on("click",function(){
			 gPrint("tab_print");
		 });	
	  
	     $("#export").on("click",function(e){
			var title=$('#title').text();
			var data=$('.table').attr('border',1);
			//var datas = $("#tab_print").html();
			data = $("#tab_print").html().replace(/<A[^>]*>|<\/A>/gi,"");
			var export_data = $("<div><center><h3 align='center'>"+title+"</h3>"+data+"</center></div>").clone().find(".remove_tag").remove().end().html();
			window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
			e.preventDefault();
			$('.table').attr('border',0);
		 });
	 });
	function PreviewImage() {
      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

      oFReader.onload = function (oFREvent) {
          document.getElementById("uploadPreview").src = oFREvent.target.result;
          document.getElementById("uploadPreview").style.backgroundImage = "none";
      };
  	};
</script>