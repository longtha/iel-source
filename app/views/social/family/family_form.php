<style type="text/css">
	ul,ol{
		margin-bottom: 0px !important;
	}
	a{
		cursor: pointer;
	}
	.datepicker {z-index: 9999;}
</style>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
	      	<div class="result_info">
		      	<div class="col-sm-6">
		      		<strong>Family Information</strong>		      		
		      	</div>
		      	<div class="col-sm-6" style="text-align: center">
		      		<strong>
		      			<center class='error' style='color:red;'><?php if(isset($error)) echo "$error"; ?></center>
		      		</strong>	
		      	</div>
			</div> 
			<div id="stdregister_div"></div>
      	
	        <form enctype="multipart/form-data" class="gform" name="std_register" id="std_register" method="POST" action="<?php echo site_url('social/family/savefamily')?>">	        
	          	<div class="col-sm-12">
	          	<div class="col-sm-12 panel panel-default" style="padding-left: 0;padding-right: 0px">
                    <!-- Start Form  -->
                    <div class="panel-body">
						<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="family_id">Family ID<span style="color:red">*</span></label>
							</div>
							<div class="col-sm-2">
								<input type="text" required id="family_id" name="family_id"  class="form-control parsley-validated"value="">
								<input type="text" class="hide" name="familyid" value="" id="familyid">
							</div>
							<div class="col-sm-2">
								<label class="req" for="family_name">Family Name</label>
							</div>
							<div class="col-sm-2">
								<input type="text" required  class="form-control parsley-validated" name="family_name" value="" id="family_name">
							</div>												
							<div class="col-sm-2">
								<label class="req" for="family_book_no">Family Book Record No</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="family_book_no" value="" id="family_book_no">
							</div>												
				    	</div>	
						<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="family_home">N#</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_home" id="family_home"  class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="family_street">Street</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_street" id="family_street"  class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="family_village">Village</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_village" id="family_village"  class="form-control"/>	
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="family_commune">Commune</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_commune" id="family_commune"  class="form-control"  />
							</div>
							<div class="col-sm-2">
								<label class="req" for="family_district">District</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_district" id="family_district"  class="form-control" />
							</div>
							<div class="col-sm-2">
								<label class="req" for="family_province">Province</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_province" id="family_province"  class="form-control"  />
							</div>
				    	</div>
				    </div>					    
					<div class="panel-heading">
	                	<h4 class="panel-title">Father Information</h4>
	              	</div>
	              	<div class="panel-body">							              				<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="father_name">Father's Name(KH)</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="father_name" value="" id="father_name">
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_occupation">Occupation(KH)</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="father_occupation" value="" id="father_occupation">
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_dob">Father's DOB</label>
							</div>
							<div class="col-sm-2">
								<div data-date-format="dd-mm-yyyy" class="input-group date dob">
									<input type='text' id="father_dob" class="form-control" name="father_dob" placeholder="dd-mm-yyyy"/>
							    	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
							    </div>
							</div>
				    	</div>			              	
		            	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="father_name_eng">Father's Name</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="father_name_eng" value="" id="father_name_eng">
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_occupation_eng">Occupation</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="father_occupation_eng" value="" id="father_occupation_eng">
							</div>
							<div class="col-sm-2">								
							</div>
							<div class="col-sm-2">								
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="father_home">N#</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_home" id="father_home"  class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_street">Street</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_street" id="father_street"  class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_village">Village</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_village" id="father_village"  class="form-control"/>	
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="father_commune">Commune</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_commune" id="father_commune"  class="form-control"  />
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_district">District</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_district" id="father_district"  class="form-control" />
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_province">Province</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_province" id="father_province"  class="form-control"  />
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="father_phone">Phone</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_phone" id="d_father_phone"  class="form-control"  />
							</div>
							<div class="col-sm-8"></div>												
				    	</div>
				    </div>
			    	<div class="panel-heading">
	                	<h4 class="panel-title">Mother Information</h4>
	              	</div>
	              	<div class="panel-body">
	              		<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="mother_name">Mother's Name(KH)</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" id="mother_name" name="mother_name" value="">
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_occupation">Occupation(KH)</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="mother_occupation" id="mother_occupation" value="">
							</div>
							<div class="col-sm-2">								
							</div>
							<div class="col-sm-2">								
							</div>
				    	</div>
		              	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="mother_name_eng">Mother's Name</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" id="mother_name_eng" name="mother_name_eng" value="">
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_occupation_eng">Occupation</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="mother_occupation_eng" id="mother_occupation_eng" value="">
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_dob">Mother's DOB</label>
							</div>
							<div class="col-sm-2">
								<div data-date-format="dd-mm-yyyy" class="input-group date dob">
									<input type='text' id="mother_dob" class="form-control" name="mother_dob" placeholder="dd-mm-yyyy"/>
								    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
								</div>
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="mother_home">N#</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_home" id="mother_home"  class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_street">Street</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_street" id="mother_street"  class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_village">Village</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_village" id="mother_village"  class="form-control"/>	
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="mother_commune">Commune</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_commune" id="mother_commune"  class="form-control"  />
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_district">District</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_district" id="mother_district"  class="form-control" />
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_province">Province</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_province" id="mother_province"  class="form-control"  />
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="mother_phone">Phone</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_phone" id="mother_phone"  class="form-control"  />
							</div>
							<div class="col-sm-8">
								
							</div>
				    	</div>
				    </div>
			    	<!-- end form  -->


			    	<!-- login -->
  					<div class="panel-heading">
  						<h4 class="panel-title">Login Options</h4>
  					</div>      					
  					<div class="panel-body">
  						<div class="col-sm-6" style="padding: 2px;">
      						<div class="form_sep">
      							<label>User Login</label>	      							
      							<input type="text" class="form-control" name="txtu_name" id="txtu_name" /> 
      						</div>
							<div class="form_sep">
      							<label>Password</label>	      							
      							<input type="password" class="form-control" name="txtpwd" id="txtpwd" /> 
      						</div>		      						
  						</div>
  						<div class="col-sm-6" style="padding: 2px;">
  							<div class="form_sep">
      							<label>Role</label>
      							<select name='cborole' id='cborole' class="form-control">
									<?php
									foreach ($this->role->getallrole() as $role_row) {
										echo "<option value='$role_row->roleid'>$role_row->role</option>";
									}
									?>
								</select>
      						</div>
      						<div class="form_sep">
      							<label>School Level</label>
      							<select name="cboschlevel[]" id="cboschlevel" class="form-control" multiple="multiple">
      								<?php 
      								foreach ($this->db->get('sch_school_level')->result() as $schl) {
										echo "<option value='$schl->schlevelid'>$schl->sch_level</option>";
									} 
									?>
      							</select>
      						</div>
  						</div>

						<!-- start up -->
  						<div class="col-sm-12" style="padding: 5px;">
							<div class="panel panel-default">
								<div class="panel-heading" style="padding: 5px;">
									<h5 class="panel-title">Startup Page</h5>
								</div>
								<div class="panel-body">
									<div class="col-sm-4" style="padding: 2px;">
										<div class="form_sep">
											<label for="emailField">Dashboard</label>
			      							<select  class="form-control months"  name="dashboard" id="dashboard" >
												<?php 

												$dash= array('Full'=>'/system/dashboard',
															'Socail'=>'system/dashboard/view_soc',
															'Health'=>'/system/dashboard/view_health',
															'Employee'=>'/system/dashboard/view_staff',
															'Student'=>'/system/dashboard/view_std');

												foreach ($dash as $key => $value) {
													echo "<option value='$value'>$key</option>";
												}

												?>
											</select>
			      						</div>
		      						</div>
		      						<div class="col-sm-4" style="padding: 2px;">
										<div class="form_sep">
											<label>Module</label>
			      							<select  class="form-control months" name="moduleallow[]" id="moduleallow" >
											</select>
			      						</div>
		      						</div>
		      						<div class="col-sm-4" style="padding: 2px;">
										<div class="form_sep">
											<label for="emailField">Def. Page</label>
			      							<select  class="form-control months"  name="defpage" id="defpage" >
											</select>
			      						</div>
		      						</div>
								</div>
		  					</div>
						</div>
					</div>


		        </div>
	        </div>

	        
	       <div class="row">
	          <div class="col-sm-5">
	            <div class="form_sep">
	              <input type="hidden" name="y" value="<?php echo isset($_GET['y'])?$_GET['y']:"" ?>" name="y"> 
	              <input type="hidden" name="m" value="<?php echo isset($_GET['m'])?$_GET['m']:"" ?>" name="m"> 	
	              <input type="hidden" name="p" value="<?php echo isset($_GET['p'])?$_GET['p']:"" ?>" name="p"> 
	              <button id="std_reg_submit" name="std_reg_submit" type="submit" class="btn btn-success">Save</button>
	            </div>
	          </div>
	        </div>               
	      </form>
	 
    </div>
  </div>
</div>

<script type="text/javascript">
	
	function fillpermanent(event){
		var village=$('#village').val();
		if(village!='')
			village=$('#village').val()+' Village, ';
		var commune=$('#commune').val();
		if(commune!='')
			commune=$('#commune').val()+' Commune, ';
		var district=$('#district').val();
		if(district!='')
			district=$('#district').val()+' District, ';
		var adition_adr=$('#adition_adr').val();
		if(adition_adr!='')
			adition_adr=$('#adition_adr').val();
		var province=$('#province').val();
		if(province!='')
			province=$('#province').val()+' Province';
		var permanent_adr=village+commune+district+province+"/"+adition_adr;
		$('#permanent').val(permanent_adr);
	}
	
 	function isNumberKey(evt){
        var e = window.event || evt; // for trans-browser compatibility 
        var charCode = e.which || e.keyCode; 
        if ((charCode > 45 && charCode < 58) || charCode == 8){ 
            return true; 
        } 
        return false;  
     }

    function LoadModule(){
		$.ajax({
			type : "POST",
			url  : "<?php echo site_url('setting/user/getModule')?>",
			dataType: 'json',
			async: false,
			data: {
				'roleid' : $("#cborole").val()
			},
			success:function(data){
				$('#moduleallow').html(data.op);
				$('#defpage').html(data.oppage);
			}
		});
	}//End Load Module
	
	$(function(){
		// get...=====
		LoadModule();
		$(document).on('change','#cborole',function(){
			LoadModule();
		});
		$(document).on('change','#moduleallow',function(){
			$.ajax({
	    		type : "POST",
	    		url  : "<?php echo site_url('setting/user/getPage')?>",
	    		data: {
	    			'moduleid' : $("#moduleallow").val()
	    		},
	    		success:function(data){
	    			$('#defpage').html(data);
	    		}
	    	});
		});
	
		$('#std_register').parsley();
		$('#frmaddrespond').parsley();
		$('#frmaddmember').parsley();
		$('#frmvisit').parsley();
		
		$("#responsible_revenue").val(0);
		$("#family_revenue").val(0);
		$("#father_revenu").val(0);
		$("#mother_revenu").val(0);

		$.fn.datepicker.defaults.format = "dd-mm-yyyy";

		$('#add_date').datepicker();
		$('#mother_dob').datepicker();
		$('#father_dob').datepicker();
		$('#addres_dob').datepicker();
		$('#addmem_dob').datepicker();
		//++++++++++++++++++++++++++++++add respond submit++++++++++++++++++
		$("#btnsaverpb").click(function() { 
			var res_revenue=$("#addres_revenue").val();
			var res_r = $("#responsible_revenue").val();
			$("#responsible_revenue").val(Number(res_r)+Number(res_revenue)); 
        	totalrevenu();
        	$('#frmaddrespond').submit();           
        });
        
        $('#frmaddrespond').submit(function(e) { 
	    });	
			//++++++++++++++++++++++++++++++end save++++++++++++++++++	
			//++++++++++++++++++++++++++++++add member submit++++++++++++++++++
		
		$("#std_reg_submit").on("click",function(){
			
		});
	
		$("#dob,.res_dob,.mem_dob").datepicker({
      		language: 'en',
      		pick12HourFormat: true,
      		format:'dd-mm-yyyy'
    	});

		$('#family_home').change(function(){
			var myVal =$(this).val();
			$('#father_home').val(myVal);
			$('#mother_home').val(myVal);
		});
		$('#family_street').change(function(){
			var myVal =$(this).val();
			$('#father_street').val(myVal);
			$('#mother_street').val(myVal);
		});
		$('#family_village').change(function(){
			var myVal =$(this).val();
			$('#father_village').val(myVal);
			$('#mother_village').val(myVal);
		});
		$('#family_commune').change(function(){
			var myVal =$(this).val();
			$('#father_commune').val(myVal);
			$('#mother_commune').val(myVal);
		});
		$('#family_district').change(function(){
			var myVal =$(this).val();
			$('#father_district').val(myVal);
			$('#mother_district').val(myVal);
		});
		$('#family_province').change(function(){
			var myVal =$(this).val();
			$('#father_province').val(myVal);
			$('#mother_province').val(myVal);
		});

	});
	
	function removerow(event){
		var row_class=$(event.target).closest('tr').remove();
	}
	function removerowres(event){
		var r = confirm("Are you sure to delete this Respondsible it can't undo !");
		if (r == true) {
			var family_revenue=$("#family_revenue").val()-0;
			var res_revenue=$("#res_revenue").val()-0;
			var respon_revenu=$("#responsible_revenue").val()-0;
			var total=Number(family_revenue)-Number(res_revenue);

			$("#family_revenue").val(total);	
			$("#responsible_revenue").val(Number(respon_revenu)-Number(res_revenue));

			var row_class=$(event.target).closest('tr').remove();
		}
	}
	

</script>
	
		
	
