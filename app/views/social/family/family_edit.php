 <?php	
	//array("Single","Married","Divorce/widow");
	$adr=$family['permanent_adr'];
	if($adr==''){
		$adr=$family['village'].','.$family['commune'].','.$family['district'].','.$family['province'];
	}
	$father_dob ="";
	$mother_dob ="";
	if($family["father_dob"] !="" && $family["father_dob"] !="0000-00-00" ){
		$father_dob = $this->green->convertSQLDate($family["father_dob"]);
	}	
	if($family["mother_dob"] !="" && $family["mother_dob"]!="0000-00-00"){
		$mother_dob = $this->green->convertSQLDate($family["mother_dob"]);
	}
?>

<?php
	$dash= array('Full'=>'/system/dashboard',
				'Socail'=>'system/dashboard/view_soc',
				'Health'=>'/system/dashboard/view_health',
				'Employee'=>'/system/dashboard/view_staff',
				'Student'=>'/system/dashboard/view_std');
	$moduleid = '';
	if(isset($query['def_open_page'])){
		parse_str((isset($query['def_open_page']) ? $query['def_open_page'] : ''),$arr);
		$moduleid = $arr['m'];
	}
?>

<style type="text/css">
	ul,ol{
		margin-bottom: 0px !important;
	}
	td{
		 word-wrap: break-word !important;

	}
	.ui-autocomplete{z-index: 9999;}
	table tbody tr td img,th img{width: 20px; margin-right: 10px}
	a{
		cursor: pointer;
	}
	.datepicker {z-index: 9999;}
</style>
<head>
</head>
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
	      	<div class="result_info">
		      	<div class="col-sm-6">
		      		<strong>Update Family Information</strong>		      		
		      	</div>
		      	<div class="col-sm-6" style="text-align: center">
		      		<strong>
		      			<center class='error' style='color:red;'><?php if(isset($error)) echo "$error"; ?></center>
		      		</strong>	
		      	</div>
			</div> 
			<div id="stdregister_div"></div>
      	
	        <form enctype="multipart/form-data" class="gform" name="std_register" id="std_register" method="POST" action="<?php echo site_url('social/family/savefamily')?>">	        
	          	<div class="col-sm-12">
	          	<div class="col-sm-12 panel panel-default" style="padding-left: 0;padding-right: 0px">
                    <!-- Start Form  -->
                    <div class="panel-body">
						<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="family_id">Family ID<span style="color:red">*</span></label>
							</div>
							<div class="col-sm-2">
								<input type="text" required id="family_id" name="family_id"  class="form-control parsley-validated"value="<?php echo $family['family_code']; ?>">
								<input type="text" class="hide" name="familyid" value="<?php echo $family['familyid']; ?>" id="familyid">
							</div>
							<div class="col-sm-2">
								<label class="req" for="family_name">Family Name</label>
							</div>
							<div class="col-sm-2">
								<input type="text" required  class="form-control parsley-validated" name="family_name" value="<?php echo $family['family_name']; ?>" id="family_name">
							</div>												
							<div class="col-sm-2">
								<label class="req" for="family_book_no">Family Book Record No</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="family_book_no" value="<?php echo $family['family_book_record_no']; ?>" id="family_book_no">
							</div>												
				    	</div>	
						<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="family_home">N#</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_home" id="family_home" value="<?php echo $family['home_no']; ?>" class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="family_street">Street</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_street" id="family_street" value="<?php echo $family['street']; ?>" class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="family_village">Village</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_village" id="family_village" value="<?php echo $family['village']; ?>" class="form-control"/>	
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="family_commune">Commune</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_commune" id="family_commune" value="<?php echo $family['commune']; ?>" class="form-control"  />
							</div>
							<div class="col-sm-2">
								<label class="req" for="family_district">District</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_district" id="family_district" value="<?php echo $family['district']; ?>" class="form-control" />
							</div>
							<div class="col-sm-2">
								<label class="req" for="family_province">Province</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="family_province" id="family_province" value="<?php echo $family['province']; ?>" class="form-control"  />
							</div>
				    	</div>
				    </div>					    
					<div class="panel-heading">
	                	<h4 class="panel-title">Father Information</h4>
	              	</div>
	              	<div class="panel-body">							              							              	
		            	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="father_name">Father's Name(KH)</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="father_name" value="<?php echo $family['father_name_kh']; ?>" id="father_name">
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_occupation">Occupation(KH)</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="father_occupation" value="<?php echo $family['father_ocupation_kh']; ?>" id="father_occupation">
							</div>
							<div class="col-sm-2">
								
							</div>
							<div class="col-sm-2">
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="father_name">Father's Name</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="father_name_eng" value="<?php echo $family['father_name']; ?>" id="father_name_eng">
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_occupation_eng">Occupation</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="father_occupation_eng" value="<?php echo $family['father_ocupation']; ?>" id="father_occupation_eng">
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_dob">Father's DOB</label>
							</div>
							<div class="col-sm-2">
								
								<div data-date-format="dd-mm-yyyy" class="input-group date dob">
									<input type='text' id="father_dob" value="<?php echo $father_dob; ?>" class="form-control" name="father_dob" placeholder="dd-mm-yyyy"/>
							    	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
							    </div>
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="father_home">N#</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_home" id="father_home"  value="<?php echo $family['father_home']; ?>"  class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_street">Street</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_street" id="father_street"  value="<?php echo $family['father_street']; ?>"  class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_village">Village</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_village" id="father_village"  value="<?php echo $family['father_village']; ?>"  class="form-control"/>	
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="father_commune">Commune</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_commune" id="father_commune"  value="<?php echo $family['father_commune']; ?>"  class="form-control"  />
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_district">District</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_district" id="father_district"  value="<?php echo $family['father_district']; ?>"  class="form-control" />
							</div>
							<div class="col-sm-2">
								<label class="req" for="father_province">Province</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_province" id="father_province"  value="<?php echo $family['father_province']; ?>"  class="form-control"  />
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="father_phone">Phone</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="father_phone" id="d_father_phone"  value="<?php echo $family['father_phone']; ?>"  class="form-control"  />
							</div>
							<div class="col-sm-8"></div>												
				    	</div>
				    </div>
			    	<div class="panel-heading">
	                	<h4 class="panel-title">Mother Information</h4>
	              	</div>
	              	<div class="panel-body">
	              		<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="mother_name">Mother's Name(KH)</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" id="mother_name" name="mother_name" value="<?php echo $family['mother_name_kh']; ?>">
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_occupation">Occupation(KH)</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="mother_occupation" id="mother_occupation" value="<?php echo $family['mother_ocupation_kh'];?>">
							</div>
							<div class="col-sm-2">
							</div>
							<div class="col-sm-2">								
							</div>
				    	</div>
		              	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="mother_name_eng">Mother's Name</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" id="mother_name_eng" name="mother_name_eng" value="<?php echo $family['mother_name']; ?>">
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_occupation_eng">Occupation</label>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control parsley-validated" name="mother_occupation_eng" id="mother_occupation_eng" value="<?php echo $family['mother_ocupation'];?>">
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_dob">Mother's DOB</label>
							</div>
							<div class="col-sm-2">
								<div data-date-format="dd-mm-yyyy" class="input-group date dob">
									<input type='text' value="<?php echo $mother_dob;?>" id="mother_dob" class="form-control" name="mother_dob" placeholder="dd-mm-yyyy"/>
								    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
								</div>
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="mother_home">N#</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_home" id="mother_home"  value="<?php echo $family['mother_home'];?>"  class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_street">Street</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_street" id="mother_street"  value="<?php echo $family['mother_street'];?>"  class="form-control"/>
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_village">Village</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_village" id="mother_village"  value="<?php echo $family['mother_village'];?>" class="form-control"/>	
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="mother_commune">Commune</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_commune" id="mother_commune"  value="<?php echo $family['mother_commune'];?>" class="form-control"  />
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_district">District</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_district" id="mother_district" value="<?php echo $family['mother_district'];?>" class="form-control" />
							</div>
							<div class="col-sm-2">
								<label class="req" for="mother_province">Province</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_province" id="mother_province"  value="<?php echo $family['mother_province'];?>"  class="form-control"  />
							</div>
				    	</div>
				    	<div class="col-sm-12 form_sep">
							<div class="col-sm-2">
								<label class="req" for="mother_phone">Phone</label>
							</div>
							<div class="col-sm-2">
								<input type="text" name="mother_phone" id="mother_phone"  value="<?php echo $family['mother_phone'];?>" class="form-control"  />
							</div>
							<div class="col-sm-8">
								
							</div>
				    	</div>
				    </div>
			    	<!-- end form  -->	
		            

		            <!-- login -->
  					<div class="panel-heading">
  						<h4 class="panel-title">Login Options</h4>
  					</div>
  					<div class="panel-body">
  						<input type="hidden" id="userid" name="userid" value="<?= (isset($query['userid']) ? $query['userid'] : '') ?>">
  						<div class="col-sm-6" style="padding: 2px;">
      						<div class="form_sep">
      							<label>User Login</label>	      							
      							<input type="text" class="form-control" name="txtu_name" id="txtu_name" value="<?= (isset($query['user_name']) ? $query['user_name'] : '') ?>" /> 
      						</div>
							<div class="form_sep">
      							<label>Password</label>	      							
      							<input type="password" class="form-control" name="txtpwd" id="txtpwd" value="<?= (isset($query['password']) ? $query['password'] : '') ?>" /> 
      						</div>		      						
  						</div>
  						<div class="col-sm-6" style="padding: 2px;">

  							<div class="form_sep">
      							<label>Role</label>
      							<select name='cborole' id='cborole' class="form-control">
									<?php
									foreach ($this->role->getallrole() as $role_row) {?>
										<option value='<?php echo $role_row->roleid; ?>' <?php if((isset($query['roleid']) ? $query['roleid'] : '')==$role_row->roleid) echo 'selected';?>> <?php echo $role_row->role ; ?></option>
									<?php }
									?>
								</select>
      						</div>
      						<div class="form_sep">
      							<label>School Level</label>
      							<select name="cboschlevel[]" id="cboschlevel" class="form-control" multiple="multiple">
      								<?php foreach ($this->db->get('sch_school_level')->result() as $schl) {

										$selec = $this->db->query("SELECT
																			COUNT(u.userid) AS c
																		FROM
																			sch_user AS u
																		LEFT JOIN sch_user_schlevel AS l ON u.userid = l.userid
																		WHERE
																			u.emp_id = '{$query['emp_id']}'
																		AND l.schlevelid = '{$schl->schlevelid}' ")->row()->c - 0;
									 ?>
										<option value="<?php echo $schl->schlevelid ?>" <?php if($selec>0) echo 'selected' ?>><?php echo $schl->sch_level ?></option>";
									<?php } ?>
      							</select>
      						</div>
  						</div>
					</div>
					
					<!-- start up -->
					<div class="col-sm-12" style="padding-left: 20px;padding-right: 20px;">
						<div class="panel panel-default">
							<div class="panel-heading" style="padding: 5px;">
								<h5 class="panel-title">Startup Page</h5>
							</div>
							<div class="panel-body">
								<div class="col-sm-4" style="padding: 2px;">
									<div class="form_sep">
										<label for="emailField">Dashboard</label>
		      							<select  class="form-control months" name="dashboard" id="dashboard" >
											<?php foreach ($dash as $key => $value) { ?>
													<option value='<?PHP echo $value ?>' <?php if((isset($query['def_dashboard']) ? $query['def_dashboard'] : '')==$value) echo 'selected'; ?>><?php echo $key ?> </option>";
											<?php 	} ?>
										</select>
		      						</div>
	      						</div>
	      						<div class="col-sm-4" style="padding: 2px;">
									<div class="form_sep">
										<label>Module</label>
		      							<select  class="form-control months" name="moduleallow[]" id="moduleallow" >
											<option></option>
										</select>
		      						</div>
	      						</div>
	      						<div class="col-sm-4" style="padding: 2px;">
									<div class="form_sep">
										<label for="emailField">Def. Page</label>
		      							<select  class="form-control months"  name="defpage" id="defpage" >
										</select>
		      						</div>
	      						</div>
							</div>
	  					</div>
  					</div>


		        </div>
	        </div>
	        
	       <div class="row">
	          <div class="col-sm-5">
	            <div class="form_sep">
	              <input type="hidden" name="y" value="<?php echo isset($_GET['y'])?$_GET['y']:"" ?>" name="y"> 
	              <input type="hidden" name="m" value="<?php echo isset($_GET['m'])?$_GET['m']:"" ?>" name="m"> 	
	              <input type="hidden" name="p" value="<?php echo isset($_GET['p'])?$_GET['p']:"" ?>" name="p"> 
	              <button id="std_reg_submit" name="std_reg_submit" type="submit" class="btn btn-success">Save</button>
	            </div>
	          </div>
	        </div>               
	      </form>
	 
    </div>
  </div>
</div>
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<script type="text/javascript">	
	function loadPage(defpage=''){
		$.ajax({
			type : "POST",
			url  : "<?php echo site_url('setting/user/getPage')?>",
			data: {
				'moduleid' : $("#moduleallow").val(),
				'defpage'  : defpage
			},
			success:function(data){
				$('#defpage').html(data);
			}
		});
	}
	function LoadModule(moduleid=''){
		$.ajax({
			type : "POST",
			url  : "<?php echo site_url('setting/user/getModule')?>",
			dataType: 'json',
			async: false,
			data: {
				'roleid' : $("#cborole").val(),
				'moduleid': moduleid
			},
			success:function(data){
				$('#moduleallow').html(data.op);
				$('#defpage').html(data.oppage);
			}
		});
	}//End Load Module

	$(function(){

		LoadModule("<?php echo $moduleid?>");
		loadPage("<?php echo (isset($query['def_open_page']) ? $query['def_open_page'] : '') ?>");

		$(document).on('change','#cborole',function(){
			LoadModule();
		});
		$(document).on('change','#moduleallow',function(){
			loadPage();
		});
		
		// $("#responsible_revenue").val(0);
		// $("#family_revenue").val(0);
		// $("#father_revenu").val(0);
		// $("#mother_revenu").val(0);
		$("#dob,.res_dob,.mem_dob").datepicker({
      		language: 'en',
      		pick12HourFormat: true,
      		format:'dd-mm-yyyy'
    	});
    	$('#summernote').summernote({
    		defaultFontName: 'Times New Roman',
          	height: 100,               // set editor height
          	minHeight: null,             // set minimum height of editor
          	maxHeight: null,             // set maximum height of editor
          	focus: true                 // set focus to editable area after initializing summernote
        });
		$.fn.datepicker.defaults.format = "dd-mm-yyyy";
		$('#std_register').parsley();
		$('#frmvisit').parsley();
		$('#frmaddrespond').parsley();
		$('#frmaddmember').parsley();
		
		$('#mother_dob').datepicker();
		$('#add_date').datepicker();
		$('#father_dob').datepicker();
		$('#addres_dob').datepicker();
		$('#addmem_dob').datepicker();
		//++++++++++++++++++++++++++++++++++++++++++++++++
		
		$('#std_reg_submit').click(function(){			
			if(confirm("Are you sure? You want to upate !")){
				$('#std_register').submit();
			}else{				
				return false;
			}			
		});
       	
		$('#family_home').change(function(){
			var myVal =$(this).val();
			$('#father_home').val(myVal);
			$('#mother_home').val(myVal);
		});
		$('#family_street').change(function(){
			var myVal =$(this).val();
			$('#father_street').val(myVal);
			$('#mother_street').val(myVal);
		});
		$('#family_village').change(function(){
			var myVal =$(this).val();
			$('#father_village').val(myVal);
			$('#mother_village').val(myVal);
		});
		$('#family_commune').change(function(){
			var myVal =$(this).val();
			$('#father_commune').val(myVal);
			$('#mother_commune').val(myVal);
		});
		$('#family_district').change(function(){
			var myVal =$(this).val();
			$('#father_district').val(myVal);
			$('#mother_district').val(myVal);
		});
		$('#family_province').change(function(){
			var myVal =$(this).val();
			$('#father_province').val(myVal);
			$('#mother_province').val(myVal);
		});

		//++++++++++++++++++++++++++++++++++++++++++++++	
		
        
        $(".up_studinf").on("change",function(){
            var studentid=$("#h_studentid").val()-0;
            if(studentid!=0){
                $("#ht_upstud").val(1);
            }
        });
	});
	
	function uploads(visitid,familyid,formdata){
		//alert(visitid+'/'+familyid);
        $.ajax({
            type:'POST',
            url:"<?PHP echo site_url('social/visit/do_upload');?>/"+visitid+"/"+familyid,
            data:formdata,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                console.log("success");
                console.log(data);
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
	}
	
	function previewstudent(event){
		var year=$(event.target).attr('year');
		var yearid=$(event.target).attr('yearid');
		var student_id=jQuery(event.target).attr("rel");
		window.open("<?PHP echo site_url('student/student/preview');?>/"+student_id+"?yearid="+yearid,'_blank');
	}
	function previewvisit(event){
		var familyid=$(event.target).attr('rel');
		window.open("<?PHP echo site_url('social/visit/preview');?>/"+familyid+"?s=full",'_blank');
	}
	function previewimage(event){
		var familyid=$(event.target).attr('rel');
		window.open("<?PHP echo site_url('social/visit/previewimage');?>/"+familyid+"?s=full",'_blank');
	}
 	function isNumberKey(evt){
        var e = window.event || evt; // for trans-browser compatibility 
        var charCode = e.which || e.keyCode; 
        if ((charCode > 45 && charCode < 58) || charCode == 8){ 
            return true; 
        } 
        return false;  
    }
	function fillusername(event){
		var f_name=$('#first_name').val();
		var l_name=$('#last_name').val();
		var username=f_name+'.'+l_name;
		
		$.ajax({
                url:"<?php echo base_url(); ?>index.php/student/student/validateuser",    
                data: {'username':username},
                type:"post",
                success: function(data){
               if(data>0){
               		$('#login_username').val(username+'1');
               }else{
               		$('#login_username').val(username);
               }
            }
        });
	}
	
</script>