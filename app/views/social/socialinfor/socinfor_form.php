<?php
	
  $m='';
  $p='';
  if(isset($_GET['m'])){
      $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }
?>

<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	ul,ol{
		margin-bottom: 0px !important;
	}
	a{
		cursor: pointer;
	}
	.datepicker {z-index: 9999;}
</style>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
    <div id="main_content">
       <div class="result_info">
	      	<div class="col-sm-6">
	      		<strong>Stock Information</strong>	      		
	      	</div>
	      	<div class="col-sm-6" style="text-align: center">
	      		<strong>
	      			<center class='error' style='color:red;'><?php if(isset($error)) echo "$error"; ?></center>
	      		</strong>	
	      	</div>
		</div> 
      <div id="stdregister_div"></div>
      <!-- main content -->
       <form enctype="multipart/form-data" name="std_register" id="std_register" method="POST" action="<?php echo site_url("social/socialinfor/save?m=$m&p=$p")?>">
        
        <div class="row">
          <div class="col-xs-6">

            <div class="panel panel-default">                          
              <div class="panel-heading">
                <h4 class="panel-title">Stock Details
                      <label style="float:right !important; font-size:11px !important; color:red;"><?php if(isset($socinfo['modified_by'])) if($socinfo['modified_by']!='') echo "Last Modified Date: ".date_format(date_create($socinfo['modified_date']),'d-m-Y H:i:s')." By : $socinfo[modified_by]"; ?></label> 

                </h4>
              </div>
              <div class="panel-body">              	
              	<div class="form_sep">
                  <input type="text" style='display:none;' value='<?php echo (isset($socinfo['famnoteid'])?$socinfo['famnoteid']:""); ?>' id="famnoteid" name="famnoteid">
                </div> 

                <div class="form_sep">
                  <label class="req" for="classid">Type</label>
                  <select data-required="true" required data-parsley-required-message="Select" minlength='1' class="form-control parsley-validated" name="familynote_type" id="familynote_type">
                    <option value="">Select Type</option>
                    <?php
                      foreach ($soc_type as $soc => $val ) {?>
                        <option value="<?php echo $val?>" <?php if((isset($socinfo['familynote_type'])&&$socinfo['familynote_type'])==$soc) echo 'selected' ?>><?php echo $soc; ?></option>
                      <?php }
                    ?>
                  </select>
                </div> 

                <div class="form_sep">
                  <label class="req" for="stockcode">Description</label>
                  <input type="text" data-required="true" class="form-control parsley-validated" name="description" value='<?php echo isset($socinfo['description'])?$socinfo['description']:""; ?>' id="description">
                </div>                                
                               
              </div>
            </div>
          </div>
              
        </div>        
        <div class="row">
          <div class="col-sm-5">
            <div class="form_sep">
              <button id="std_reg_submit" name="std_reg_submit" type="submit" class="btn btn-primary">Save</button>
              <button id="btnback" name="btnback" type="botton" class="btn btn-info">Cancel</button>
            </div>
          </div>
        </div>        
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	
	

 	function isNumberKey(evt){
        var e = window.event || evt; // for trans-browser compatibility 
        var charCode = e.which || e.keyCode; 
        if ((charCode > 45 && charCode < 58) || charCode == 8){ 
            return true; 
        } 
        return false;  
        }    

	$(function(){

		$('#std_register').parsley();
	
		$("#dob,.res_dob,.mem_dob").datepicker({
      		language: 'en',
      		pick12HourFormat: true,
      		format:'yyyy-mm-dd'
    	});    

    
    
    $("#btnback").on("click",function(){
      window.history.back();
    });

	});
	
	
	
	
</script>
	
		
	
