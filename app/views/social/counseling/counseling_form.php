<style type="text/css">
	ul,ol{
		margin-bottom: 0px !important;
	}
	a{
		cursor: pointer;
	}
	.ui-autocomplete{z-index: 9999;}
	.datepicker {z-index: 9999;}
</style>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content" style="margin:10px 10px;">
	      
	      <!-- main content -->
	      <?php
		      $m='';
				$p='';
				if(isset($_GET['m'])){
			    	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }

	       ?>
	       <form enctype="multipart/form-data" id='frmtreatment' action="" method="POST" class="form-horizontal basic_validate">
                    <div class="row-1">
                        <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Counseling Form 
                                    <label style="float:right !important; font-size:11px !important; color:red;"><?php if(isset($data['modified_by'])) if($data['modified_by']!='') echo "Last Modified Date: ".date_format(date_create($data['modified_date']),'d-m-Y H:i:s')." By : $data[modified_by]"; ?></label>	
                                </h4>
                                 </div>
                                <div class="panel-body">
                                    <div class="form_sep">
                                        <label class="req" for="studentlevel">School Level</label>
                                        
                                        <select class="form-control required" id="studentlevel" name="studentlevel">
                                            <?php 
                                                foreach ($schevels as $sclev) {
                                                    echo '<option value="' . $sclev->schlevelid . '" '.($data['schlevelid']==$sclev->schlevelid?"selected='selected'":"").' data-schoollev="' . $sclev->sch_level . '"  data-pro_id="'. $sclev->programid .'">' . $sclev->sch_level . '</option>';
                                                }
                                            ?>
                                        </select>
                                        <?php 
                                            if (!empty($data['id'])){
                                        ?>
                                        <input type="hidden" id="idcounseling" name="idcounseling" value="<?php echo $data['id'];?>" />
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form_sep">
                                        <label for="adcademic_year_id">Academic Year</label>
                                        <select name="adcademic_year_id" id="adcademic_year_id" minlength='1' class="form-control required" >
                                             <?php
                                                if (isset($data['study_year'])){
                                                    echo '<option selected value="'.$data['study_year'].'" data-studyyear="'.$data['study_year'].'">'.$data['study_year'].'</option>';
                                                }
                                            ?>
                                        </select>
                                       
                                    </div>
                                    <div class="form_sep">
                                        <label for="grade_level_id">Grade Level</label>
                                        <select name="grade_level_id" id="grade_level_id" minlength='1' class="form-control required" required data-parsley-required-message="Select Grade Level">
                                         <?php
                                            if (isset($data['grade_level_id'])){
                                                echo '<option selected data-grandlevel="'.$data['grade_level_id'].'" value="'.$data['grade_level_id'].'">'.$data['grade_level_id'].'</option>';
                                            }
                                        ?>
                                        </select>
                                         
                                    </div>
                                    <div class="form_sep">
                                        <label for="class_id">Class Name</label>					        			
                                        <select name="class_id" id="class_id" minlength='1' class="form-control required" required data-parsley-required-message="Select Class Name">
                                        <?php
                                            if (isset($data['student_class'])){
                                                echo '<option selected data-classname="'.$data['student_class'].'" value="'.$data['student_class'].'">'.$data['student_class'].'</option>';
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="form_sep">
                                        <label for="student_id">Student Name</label>
                                        <select name="student_id" id="student_id" class="form-control studentinfo required">
                                        <?php
                                            if (isset($data['student_name'])){
                                                echo '<option selected data-studentname="'.$data['student_name'].'" data-studentnum="'.$data['student_num'].'" value="'.$data['student_id'].'">'.$data['student_name'].'</option>';
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="form_sep">
                                        <label class="req" readonly for="studentGender">Gender</label>
                                        <input type="text" readonly id="studentGender" value="<?php if(isset($data['student_gender'])) echo $data['student_gender']; ?>" class="soperations-tudentGender form-control required" name="studentGender">
                                    </div>
                                    <div class="form_sep">
                                        <label class="req" for="teachername">Teacher Name</label>
                                        <select data-required="true" class="form-control required"  required name="teachername" id="teacherid" >
                                        <?php 
                                            echo '<option data-teachername="" value="">--- Select Teacher Name ---</option>';
                                            if (isset($data['teacher'])){
                                                echo '<option selected="selected"  value="'.$data['teacherid'].'" data-teachername="'.$data['teacher'].'">'.$data['teacher'].'</option>';
                                            }
                                            if(count($getteacher)>0){
                                                foreach ($getteacher as $ad) {
                                                   echo '<option value="'.$ad->empid.'" data-teachername="'.$ad->last_name.' '.$ad->first_name.'">'. $ad->last_name.' '.$ad->first_name.'</option>';
                                                } 
                                            }
                                        ?>

                                        </select>
                                    </div>
                                    <div class="form_sep">
                                        <label for="teachersubject">Teacher Subject</label>
                                        <select name="teachersubject" id="teachersubject" class="form-control">
                                        <?php
                                            echo '<option  data-subjectname="" value="">--- Select Teacher Subject ---</option>';
                                            if (isset($data['subject'])){
                                                echo '<option selected value="'.$data['subjectid'].'" data-subjectname="'.$data['subject'].'">'.$data['subject'].'</option>';
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="form_sep">
                                        <label class="req" for="reg_input_name">Mistake</label>
                                        <textarea data-required="true" class="form-control parsley-validated required" name="mistake"  id="mistake"><?php if(isset($data['mistake'])) echo $data['mistake']; ?></textarea>
                                    </div>
                                    <div class="form_sep">
                                        <label class="req" for="reg_input_name">Advised</label>
                                        <textarea data-required="true" class="form-control parsley-validated required" name="advised"  id="advised"><?php if(isset($data['advised'])) echo $data['advised']; ?></textarea>
                                    </div>
                                    <div class="form_sep">
                                        <label class="req" for="advice">Advicer</label>
                                        <select data-required="true" class="form-control required"  required name="advice" value="" id="c_type" >
                                        <?php 
                                            if (isset($data['advice'])){
                                                echo '<option selected value="'.$data['advice'].'">'.$data['advice'].'</option>';
                                            }
                                            if(count($getadvice)>0){
                                                foreach ($getadvice as $ad) {
                                                   echo '<option value="'.$ad->last_name.' '.$ad->first_name.'">'. $ad->last_name.' '.$ad->first_name.'</option>';
                                                } 
                                            }
                                        ?>

                                        </select>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                   
                <div class="row-1">
		          <div class="col-sm-12">
		            <div class="form_sep" style="margin:10px 10px;">
		              <button id="std_reg_submit" name="std_reg_submit" type="submit" class="btn btn-success">Save</button>
                        <button id="btncancel" type="button" class="btn btn-danger">Cancel</button>
		            </div>
		          </div>
		        </div>  
		 </form>
              
	    </div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/jquery.validate.js')?>"></script>
<script type="text/javascript">
    

$(document).ready(function() {

    $('select.studentinfo').change(function() {
        var studentClass = $('select.studentinfo').find(':selected').data('studentclass');
        var studentId = $('select.studentinfo').find(':selected').data('studentid');
        var studentGender = $('select.studentinfo').find(':selected').data('studentgender');
        var sch_level = $('select.studentinfo').find(':selected').data('sch_level');
        var sch_year = $('select.studentinfo').find(':selected').data('sch_year');
        
        $('#studentClass').val(studentClass);
        $('#studentId').val(studentId);
        $('#studentGender').val(studentGender);
        $('#sch_level').val(sch_level);
        $('#sch_year').val(sch_year);
        
    });    
    $("body").delegate("#sch_year", "change", function () {
        var schlevelid = $("#sch_year").val();
        getClass(schlevelid);
    });
     $("body").delegate("#btncancel", "click", function () {
        var con=confirm('Are you Sure to Cancel...!');
        if(con==true)
            location.href="<?PHP echo site_url("social/counseling/add?m=$m&p=$p");?>";
    });
    //////////////
    $('#class_id').change(function(){
            var class_id = $(this).val();		
            var schoolid = $("#school_id").val();
            var programid = $('select#studentlevel').find(':selected').data('pro_id');
            var schlevelid = $("#studentlevel").val();
            var yearid = $("#adcademic_year_id").val();	
            var gradlavelid = $("#grade_level_id").val();	
            if(class_id == ""){
                    $('#student_id').html('');
            }else{
                    var url = "<?php echo site_url('social/counseling/get_student_data') ?>";
                    var data = {'class_id':class_id,
                                            'schoolid':schoolid,
                                            'programid':programid,
                                            'schlavelid':schlevelid,
                                            'yearid':yearid,
                                            'gradlavelid':gradlavelid
                                    };
                    getDataByAjax($('#student_id'), url, data);
            }
    });
    $('#studentlevel').change(function(){
            var studentlevel = $(this).val();
            var school_id = $('#school_id').val();
            var program_id = $('select#studentlevel').find(':selected').data('pro_id');

            if(studentlevel != ""){
                $.ajax({
                url: "<?php echo site_url('social/counseling/get_year_and_grade_data') ?>",	            
                type: "post",
                dataType: 'json',
                data: {'school_id': school_id, 'program_id': program_id, 'studentlevel': studentlevel},
                success: function (data) {
                    //console.log(data);
                    //get year--------------------------------
                    var getYear = '';	            	
                    $.each(data["year"], function(k,v){
                            getYear += '<option value="'+ v.yearid +'" data-studyyear="'+ v.sch_year +'">'+ v.sch_year+'</option>';		            		
                    }); $('#adcademic_year_id').html(getYear);

                    //get grade--------------------------------
                    var getGrade = '';
                    $.each(data["grade"], function(ke,re){
                            getGrade += '<option value="'+ re.grade_levelid +'" data-grandlevel="'+ re.grade_level +'">'+ re.grade_level+'</option>';
                    });	$('#grade_level_id').html(getGrade);

                    //get teacher------------------------------
                    var grade_level_id = $('#grade_level_id').val();
                    var adcademic_year_id = $('#adcademic_year_id').val();
                    var url = "<?php echo site_url('social/counseling/get_class_data') ?>";
                    var data = 'program_id='+program_id+'&studentlevel='+studentlevel+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
                               getDataByAjax($('#class_id'), url, data);
                                    $('#class_id').html('');
                                    $('#student_id').html('');

                                    //remove message require-------------------
                                    var aId = $('#adcademic_year_id').data('parsley-id');
                                    var gId = $('#grade_level_id').data('parsley-id');

                                    $('#parsley-id-'+aId).remove();	   
                                    $('#adcademic_year_id').removeClass('parsley-error');

                                    $('#parsley-id-'+gId).remove();	   
                                    $('#grade_level_id').removeClass('parsley-error');        	
                        }
            });	
            }else{
                    $('#adcademic_year_id').html('');
                    $('#grade_level_id').html('');
                    $('#class_id').html('');
                    $('#student_id').html('');
            }
    });
  
    
});
$(function(){ 
    $('#frmtreatment').parsley();   
    $("#frmtreatment").submit(function(e){
      e.preventDefault();
    }).validate({
        rules:{
          required:{
            required:true
          },
          url:{
            required:true,
            url: true
          }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
          $(element).parents('.form_sep').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form_sep').removeClass('has-error').addClass('has-success');
        },        
        submitHandler: function(form) {
          var url="<?php echo site_url('social/counseling/save?m='.$m.'&p='.$p.'')?>"; 
          var schoollevel = $('select#studentlevel').find(':selected').data('schoollev');
          var pro_id = $('select#studentlevel').find(':selected').data('pro_id');
          var classname = $('select#class_id').find(':selected').data('classname');
          var classid = $('select#class_id').find(':selected').val();
          var studentname = $('select#student_id').find(':selected').data('studentname');
          var studentnum = $('select#student_id').find(':selected').data('studentnum');
          var studentid = $('select#student_id').find(':selected').val();
          var studyyear = $('select#adcademic_year_id').find(':selected').data('studyyear');
          var grandlevel = $('select#grade_level_id').find(':selected').data('grandlevel');
          var teachername = $('select#teacherid').find(':selected').data('teachername');
          var teacherid = $('select#teacherid').find(':selected').val();
          var studentGender = $("#studentGender").val();
          var teachersubjectid = $("#teachersubject").val();
          var teachersubject = $('select#teachersubject').find(':selected').data('subjectname');
          var levelschid = $("#studentlevel").val();
          var mistake = $("#mistake").val();
          var advised = $("#advised").val();
          var advice = $("#c_type").val();
          var student_id = $('select#student_id').find(':selected').data('studentnum'); //$("#student_id").val();
          var idcounseling = $('#idcounseling').val(); 
          $.ajax({
            url:url,
            type:"POST",
            datatype:"Json",
            async:false,
            data:{          
                id:idcounseling,
                schoollevel:schoollevel,
                classname:classname,
                levelschid:levelschid,
                studentname:studentname,
                pro_id:pro_id,
                studyyear:studyyear,
                grandlevel:grandlevel,
                teachername:teachername,
                student_id:student_id,
                studentGender:studentGender,
                m:$("#m").val(),
                p:$("#p").val(),
                teachersubject : teachersubject,
                mistake:mistake,
                advised:advised,
                advice:advice,
                teacherid:teacherid,
                classid:classid,
                studentid:studentid,
                studentnum:studentnum,
                teachersubjectid:teachersubjectid
            },
            success:function(data) {
                var formdata = new FormData(form);
                location.href="<?PHP echo site_url('social/counseling?m='.$m.'&p='.$p.'');?>";
            }
          })
        }
      });
   });
////////////////////

function getDataByAjax(selector, url, data){
    $.ajax({
        url: url,	            
        type: "post",
        dataType: 'html',
        data: data,
        success: function (data) {
            selector.html(data);		            	
        }
    });	
}
$('#grade_level_id').change(function(){
        var program_id = $('select#studentlevel').find(':selected').data('pro_id');
        var studentlevel = $('#studentlevel').val();
        var adcademic_year_id = $('#adcademic_year_id').val();
        var grade_level_id = $(this).val();
        var url = "<?php echo site_url('social/counseling/get_class_data') ?>";
        var data = 'program_id='+program_id+'&studentlevel='+studentlevel+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
        getDataByAjax($('#class_id'), url, data);
});
 $('#teacherid').change(function(){
        var teacherid = $(this).val();
        var class_id = $("#class_id").val();	
        var gradlavelid = $("#grade_level_id").val();
        var schlevelid = $('#studentlevel').val();
        var url = "<?php echo site_url('social/counseling/getsubbyteacher') ?>";
        var data = 'teacherid='+teacherid+'&class_id='+class_id+'&gradlavelid='+gradlavelid+'&schlevelid='+schlevelid;
      //  alert(data);
        getDataByAjax($('#teachersubject'), url, data);
});   

///////////////////


function getSchYear(sclevelid) {

    $("#studentClass").html("");
    if (sclevelid != "") {
        $.ajax({
            url: "<?php echo site_url('social/counseling/getsubbyteacher'); ?>",
            type: "POST",
            dataType: 'json',
            async: false,
            data: {
                schlevelid: sclevelid
            },
            success: function (res) {
                var data = res.year;
                var tr = "<option value=''></option>";
                if (data.length > 0) {

                    for (var i = 0; i < data.length; i++) {
                        var row = data[i];
                        tr += '<option value="' + row.sch_year + '">' + row.sch_year + '</option>';
                    }
                }
                $("#studentClass").html(tr);
            }
        })
    }
}
    
</script>