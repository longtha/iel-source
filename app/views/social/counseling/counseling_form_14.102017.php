
<style type="text/css">
	ul,ol{
		margin-bottom: 0px !important;
	}
	a{
		cursor: pointer;
	}
	.ui-autocomplete{z-index: 9999;}
	.datepicker {z-index: 9999;}
</style>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      
	      <!-- main content -->
	      <?php
		      $m='';
				$p='';
				if(isset($_GET['m'])){
			    	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }

	       ?>
	       <form enctype="multipart/form-data" id='frmtreatment' action="<?php echo base_url()."index.php/social/counseling/save?m=$m&p=$p";?>" method="POST">
                    <div class="row">
                        <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Counseling Form 
                                    <label style="float:right !important; font-size:11px !important; color:red;"><?php if(isset($data['modified_by'])) if($data['modified_by']!='') echo "Last Modified Date: ".date_format(date_create($data['modified_date']),'d-m-Y H:i:s')." By : $data[modified_by]"; ?></label>	
                                </h4>
                                 </div>
                                <div class="panel-body">
                                    <div class="form_sep">
                                        <label class="req" for="student_name">Student</label>
                                        <select class="form-control studentinfo"  required name="student_name" id="student_name" >
                                        <?php 
                                            if (isset($data['student_name'])){
                                                echo '<option selectd value="'.$data['student_name'].'">'.$data['student_name'].'</option>';
                                            }
                                            if(count($studentsql)>0 && isset($studentsql)){
                                                foreach ($studentsql as $stu) {
                                                    echo '<option value="'.$stu->last_name.' '.$stu->first_name.'" data-studentClass="'.$stu->class_name.'" data-sch_level="'.$stu->sch_level.'" data-sch_year="'.$stu->sch_year.'" data-studentId="'.$stu->studentid.'" data-studentGender="'.$stu->gender.'">'.$stu->last_name.' '.$stu->first_name.'</option>' ;
                                                }     
                                            }
                                        ?>
                                        </select>
                                        <?php 
                                            if (!empty($data['id'])){
                                        ?>
                                        <input type="hidden" name="id" value="<?php echo $data['id'];?>" />
                                        <?php } ?>
                                    </div>
                                    <div class="form_sep">
                                        <label class="req" for="student_num">Student ID</label>
                                        <input type="text" readonly id="studentId" value="<?php if(isset($data['student_id'])) echo $data['student_id']; ?>" class="operations-studentId form-control" name="studentId">
                                    </div>
                                    <div class="form_sep">
                                        <label class="req" readonly for="studentGender">Gender</label>
                                        <input type="text" readonly id="studentGender" value="<?php if(isset($data['student_gender'])) echo $data['student_gender']; ?>" class="soperations-tudentGender form-control" name="studentGender">
                                    </div>
                                    <div class="form_sep">
                                        <label class="req"  for="studentClass">Class</label>
                                        <input type="text" id="studentClass" value="<?php if(isset($data['student_class'])) echo $data['student_class']; ?>" readonly class="operations-studentClass form-control" name="studentClass">
                                    </div>
                                    <div class="form_sep">
                                        <label class="req"  for="sch_level">School level</label>
                                        <input type="text" id="sch_level" value="<?php if(isset($data['sch_level'])) echo $data['sch_level']; ?>" readonly class="operations-studentClass form-control" name="sch_level">
                                    </div>
                                    <div class="form_sep">
                                        <label class="req"  for="sch_year">Year</label>
                                        <input type="text" id="sch_year" value="<?php if(isset($data['sch_year'])) echo $data['sch_year']; ?>" readonly class="operations-studentClass form-control" name="sch_year">
                                    </div>
                                    <div class="form_sep">
                                        <label class="req" for="reg_input_name">Mistake</label>
                                        <textarea data-required="true" class="form-control parsley-validated" name="mistake"  id="mistake"><?php if(isset($data['mistake'])) echo $data['mistake']; ?></textarea>
                                    </div>
                                    <div class="form_sep">
                                        <label class="req" for="reg_input_name">Advised</label>
                                        <textarea data-required="true" class="form-control parsley-validated" name="advised"  id="advised"><?php if(isset($data['advised'])) echo $data['advised']; ?></textarea>
                                    </div>
                                    <div class="form_sep">
                                        <label class="req" for="advice">Advice</label>
                                        <select data-required="true" class="form-control"  required name="advice" value="" id="c_type" >
                                        <?php 
                                            if (isset($data['advice'])){
                                                echo '<option selectd value="'.$data['advice'].'">'.$data['advice'].'</option>';
                                            }
                                            if(count($getadvice)>0){
                                                foreach ($getadvice as $ad) {
                                                   echo '<option value="'.$ad->last_name.' '.$ad->first_name.'">'. $ad->last_name.' '.$ad->first_name.'</option>';
                                                } 
                                            }
                                        ?>

                                        </select>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                   
                        <div class="row">
		          <div class="col-sm-5">
		            <div class="form_sep">
		              <button id="std_reg_submit" name="std_reg_submit" type="submit" class="btn btn-success">Save</button>
                              <button id="btncancel" type="reset" class="btn btn-danger">Cancel</button>
		            </div>
		          </div>
		        </div>  
		 </form>
              
	    </div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {

    $('select.studentinfo').change(function() {
        var studentClass = $('select.studentinfo').find(':selected').data('studentclass');
        var studentId = $('select.studentinfo').find(':selected').data('studentid');
        var studentGender = $('select.studentinfo').find(':selected').data('studentgender');
        var sch_level = $('select.studentinfo').find(':selected').data('sch_level');
        var sch_year = $('select.studentinfo').find(':selected').data('sch_year');
        
        $('#studentClass').val(studentClass);
        $('#studentId').val(studentId);
        $('#studentGender').val(studentGender);
        $('#sch_level').val(sch_level);
        $('#sch_year').val(sch_year);
        
    });    
});
</script>
