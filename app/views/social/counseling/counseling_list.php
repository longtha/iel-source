<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	a,.sort,.panel-heading span{cursor: pointer;}
	.panel-heading span{margin-left: 10px;}
	.cur_sort_up{
		background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	.cur_sort_down{
		background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
        #reportheadcounseling,.reportheadcounseling{
            display: inline-block;
            font-weight: bold;
            padding: 0 10px 10px;
            width:100%;
        }
        .report{
            border-bottom: 1px dashed #ccc;
            display: block;
            padding: 10px;
        }
        table.table tr td,table.table tr th ,table.table thead{
            border: 1px solid #ccc;
        }
</style>
<?php
$school=$this->db->where('schoolid',$this->session->userdata('schoolid'))->get('sch_school_infor')->row();
		           			$school_name=$school->name;
		           			$school_adr=$school->address;	
$m='';
$p='';
if(isset($_GET['m'])){
    $m=$_GET['m'];
}
if(isset($_GET['p'])){
    $p=$_GET['p'];
}
 ?>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      
              <div id="collapseExample" class="collapse in">
                  <form enctype="multipart/form-data" id='frmtreatment' action="" method="POST" class="basic_validate">
                    <div class="panel-body">
                        <div class="col-sm-4" style="">  
                            <div class="form_sep">
                                <label class="req" for="studentlevel">School Level</label>
                                <select class="form-control" id="studentlevel" name="studentlevel">
                                    <?php 
                                    if (isset($data['schlevel'])){
                                        echo '<option selected="selected" value="' . $data['schlevelid'] . '" data-schoollev="' .$data['schlevel'] . '"  pro_id="'. $data['programid'] .'">' . $data['schlevel'] . '</option>';
                                    }
                                    if (isset($schevels) && count($schevels) > 0) {
                                        foreach ($schevels as $sclev) {
                                            echo '<option value="' . $sclev->schlevelid . '" data-schoollev="' . $sclev->sch_level . '"  pro_id="'. $sclev->programid .'">' . $sclev->sch_level . '</option>';
                                        }
                                    } 
                                    
                                    ?>
                                </select>
                               
                            </div>
                                <?php 
                                    if (!empty($data['id'])){
                                ?>
                                <?php } ?>
                            <div class="form_sep">
                                <label for="adcademic_year_id">Academic Year</label>
                                <select name="adcademic_year_id" id="adcademic_year_id" minlength='1' class="form-control" >
                                     <?php
                                        if (isset($data['study_year'])){
                                            echo '<option selectd value="'.$data['study_year'].'">'.$data['study_year'].'</option>';
                                        }
                                    ?>
                                </select>

                            </div>
                           </div>
                           <div class="col-sm-4" style=""> 
                            <div class="form_sep">
                                <label for="grade_level_id">Grade Level</label>
                                <select name="grade_level_id" id="grade_level_id" minlength='1' class="form-control">
                                 <?php
                                    if (isset($data['grade_level_id'])){
                                        echo '<option selectd value="'.$data['grade_level_id'].'">'.$data['grade_level_id'].'</option>';
                                    }
                                ?>
                                </select>

                            </div>
                            <div class="form_sep">
                                <label for="class_id">Class Name</label>					        			
                                <select name="class_id" id="class_id" minlength='1' class="form-control">
                                <?php
                                    if (isset($data['student_class'])){
                                        echo '<option selectd value="'.$data['student_class'].'">'.$data['student_class'].'</option>';
                                    }
                                ?>
                                </select>
                            </div>
                       </div>
                      <div class="col-sm-4" style="">
                          <div class="form_sep">
                                <label class="req" for="teachername">Teacher Name</label>
                                <select data-required="true" placeholder="Select Teacher" class="form-control" name="teachername" value="" id="teacherid" >
                                    <option value="" data-teachername=""> Select Teacher </option>
                                <?php 
                                    if (isset($data['teacher'])){
                                        echo '<option selectd data-teachername="'.$data['teacher'].'" value="'.$data['teacher'].'">'.$data['teacher'].'</option>';
                                    }
                                    if(count($getadvice)>0){
                                        foreach ($getadvice as $ad) {
                                           echo '<option value="'.$ad->empid.'" data-teachername="'.$ad->last_name.' '.$ad->first_name.'">'. $ad->last_name.' '.$ad->first_name.'</option>';
                                        } 
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="form_sep">
                                <label for="teachersubject">Teacher Subject</label>
                                <select name="teachersubject" id="teachersubject" class="form-control">
                                <?php
                                    if (isset($data['subject'])){
                                        echo '<option selectd value="'.$data['subject'].'">'.$data['subject'].'</option>';
                                    }
                                ?>
                                </select>
                            </div>
                      </div>
                    </div>
                      <div style="text-align: center;"><input type="submit" value="Search" id="searchcounseling" class="btn btn-success" /></div>
                  </form>
                </div>
                <div class="row-1 result_info">
                    <div class="col-xs-6">
                            <strong id='title'>Counseling List</strong>
                    </div>

                    <div class="col-xs-6" style="text-align: right">

                            <span class="top_action_button">
                                    <?php if($this->green->gAction("P")){ ?>
                                                    <a href="#" id="print" title="Print">
                                                    <img src="<?php echo base_url('assets/images/icons/print.png')?>" />
                                            </a>
                                    <?php } ?>
                            </span>			    	
                            <span class="top_action_button">
                                    <?php if($this->green->gAction("C")){ ?>
                                            <a href="<?php echo site_url("social/counseling/add?m=$m&p=$p")?>" >
                                                    <img src="<?php echo base_url('assets/images/icons/add.png')?>" />
                                            </a>
                                    <?php } ?>
                            </span>	      		
                    </div> 			      
              </div>
                <br />
	      <div class="row-1">
	      	<div class="col-sm-12">
                    
	      		<div class="panel panel-default1" id="tab_print">
                            <div id="reportheadcounseling"></div>
	      			<div >
		           		<div class="table-responsive" >
                                            
		           			<?php 
		           			$thr="";
		           			$tr="";		
		           			foreach($thead as $th=>$val){
		           				if($th=='No')
		           					$thr.="<th class='$val' align='center' rel='$val'>".$th."</th>";
		           				else if($th=='Action')
		           					$thr.="<th class='remove_tag' align='center'>".$th."</th>";
		           				else
		           					$thr.="<th class='$val' align='center' class='sort' rel='$val'>".$th."</th>";								
		           			}
		           			
		           			if(count($tdata)>0){
		           				$i=1;
		           				if(isset($_GET['per_page']))
									$i=$_GET['per_page']+1;
								foreach($tdata as $row){
									$tr.="<tr class='no_data'>
                                                <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
                                                <td class='student_name'>".$row->student_name."</td> 
                                                <td class='student_id'>".$row->student_num."</td>
                                                <td class='student_gender'>".$row->student_gender."</td>
                                                <td class='student_class'>".$row->student_class."</td>
                                                <td class='mistake'>".$row->mistake."</td>
                                                <td class='advised'>".$row->advised."</td>
                                                <td class='advice'>".$row->advice."</td>
									            <td class='date'>".date('d-m-Y',strtotime($row->create_at))."</td>
                                                <td class='remove_tag no_wrap'>";
											
											if($this->green->gAction("D")){	
											 	$tr.="<a>
											 		<img rel=".$row->id." onclick='delete_counseling(event);' class='delcounseling' src='".site_url('../assets/images/icons/delete.png')."'/>
											 	</a>";
											}
											if($this->green->gAction("U")){	
											 	$tr.="<a>
											 		<img  rel=".$row->id." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
											 	</a>";
											 }
											$tr.="</td>
									 	</tr>
									 ";
									 $i++;
								}
							}?>
		           			<table class="table">
		           				<thead><?php echo $thr ?></thead>
		           				<tbody class='listbody'>
		           					<?php echo $tr ?>
		           					 
		           				</tbody>
		           			</table>
						</div>  
                                            
					</div>
	      		
	      	
           <!------------------------->
            <div class='remove_tag'>
                <div id='pgt'>
                    <div style='margin-top:20px; width:11%; float:left;'>
                    Display : <select id="sort_num"  onchange='search(event);' style='padding:5px; margin-right:0px;'>
                        <?php
                        $num=50;
                        for($i=0;$i<20;$i++){?>
                                <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                <?php $num+=50;
                        }
                        ?>
                        </select>
                    </div>
                    <div style='text-align:center; verticle-align:center; width:89%; float:right;'>
                            <ul class='pagination' style='text-align:center'>
                                    <?php echo $this->pagination->create_links(); ?>
                            </ul>
                    </div>

                </div>
            </div>
        </div>	
    </div>
            <!------------------------->
<!--------------------------------->
<div class="col-sm-12" style="display: none;" id="div_export_print">
    <style>
          table.table tr td,table.table tr th ,table.table thead{
            border: 1px solid #ccc;
        }
    </style>
    <div class="reportheadcounseling" style="display: inline-block; font-weight: bold; padding: 0px 10px 10px; line-height: 2.5em; margin-left: 100px;width: 80%;"></div>
    
    <div class="panel panel-default" style="border: 0;">
        <div class="panel-body" style="border: 0;">
            <div class="table-responsive" >
                    <?php 
                    $thr="";
                    $tr="";		
                    foreach($thead as $th=>$val){
                            if($th=='No')
                                    $thr.="<th class='$val' align='center' rel='$val'>".$th."</th>";
                            else if($th=='Action')
                                    $thr.="<th class='remove_tag' align='center'>".$th."</th>";
                            else
                                    $thr.="<th class='$val' align='center' class='sort' rel='$val'>".$th."</th>";								
                    }
                    if(count($tdata)>0){
                            $i=1;
                            if(isset($_GET['per_page']))
                                            $i=$_GET['per_page']+1;
                                    foreach($tdata as $row){
                                            $tr.="<tr class='no_data'>
                                                        <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
                                                        <td class='student_name'>".$row->student_name."</td> 
                                                        <td class='student_id'>".$row->student_id."</td>
                                                        <td class='student_gender'>".$row->student_gender."</td>
                                                        <td class='student_class'>".$row->student_class."</td>
                                                        <td class='mistake'>".$row->mistake."</td>
                                                        <td class='advised'>".$row->advised."</td>
                                                        <td class='advice'>".$row->advice."</td>
                                                        <td class='date'>".date('d-m-Y',strtotime($row->create_at))."</td>
                                                        <td class='remove_tag no_wrap'>";

                                                            $tr.="</td>
                                                    </tr>
                                             ";
                                             $i++;
                                    }
                            }?>
                        <table class="table">
                                <thead><?php echo $thr ?></thead>
                                <tbody class='listbody'>
                                        <?php echo $tr ?>
                                </tbody>
                        </table>
                    </div>  
            </div>
    </div>
</div>	
<!--------------------------------->
	      </div> 
	    </div>
   </div>
</div>
<div class="modal fade bs-example-modal-lg" id="exporttap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="wrapper">
				<div class="clearfix" id="main_content_outer">
				    <div id="main_content">
					    <div class="result_info">
					    	<div class="col-sm-6">
					      		<strong>Choose Column To Export</strong>
					      	</div>
					      	<div class="col-sm-6" style="text-align: center">
					      		<strong>
					      			<center class='visit_error' style='color:red;'></center>
					      		</strong>	
					      	</div>
					    </div>
                                            <form enctype="multipart/form-data" name="frmvisit" id="frmvisit" method="POST">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="panel-body">
                                                                <div class='table-responsive'>
                                                                    <table class='table'>
                                                                        <thead >
                                                                            <?php
                                                                            foreach($thead as $th=>$val){
                                                                                if($th!='Action')
                                                                                echo "<th>".$th."</th>";	
                                                                            }?>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            foreach($thead as $th=>$val){
                                                                                if($th!='Action')
                                                                                                echo "<td align='center'><input type='checkbox' checked class='colch' rel='".$val."'></td>";	
                                                                            }?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                        </div>
                                                    </div> 
                                                </div>
					      </form>
					</div> 
			    </div>
			</div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id='btnprint' onclick='expdata(event);' class="btn btn-primary">Print</button>
                <button type="button" id='btnexport' onclick='expdata(event);' class="btn btn-primary">Export</button>
            </div>
        </div>                       <!-- /.modal-content -->
    </div>
</div>
<div class="modal fade" id="myModal_del" data-backdrop=false>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><b>Warning</b></h4>
      </div>
      <div class="modal-body">
        <b>Are you sure to delete this record !</b>
        <input type="hidden" name="get_rel" id="get_rel" value="">
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" onclick="deletes(event);">Yes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<script src="<?php echo base_url('assets/js/jquery.validate.js')?>"></script>
<script type="text/javascript">
	
        $('#export').click(function(){
		$('#exporttap').modal('show');
		$('#btnprint').hide();
		$('#btnexport').show();
	})
	$('#print').click(function(){
		$('#exporttap').modal('show');
		$('#btnprint').show();
		$('#btnexport').hide();
	})
        $("#btnprint").on("click", function(){
                var data = $("#div_export_print").html();
                gsPrint('<img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="height:40px;float:left"/>   <div class="col-sm-12" style="text-align: center; font-family: Khmer M1; font-size: 20px;">  អប់រំតាមបែបចិត្តសាស្រ្ត</div>',data);
        });
	function gsPrint(emp_title,data){
		 var element = "<div>"+data+"</div>";
		 $("<center><div style='padding-top:10px;text-align:center;'>"+emp_title+"</div>"+element+"</center>").printArea({
		  mode:"popup",  //printable window is either iframe or browser popup              
		  popHt: 600 ,  // popup window height
		  popWd: 500,  // popup window width
		  popX: 0 ,  // popup window screen X position
		  popY: 0 , //popup window screen Y position
		  popTitle:"test", // popup window title element
		  popClose: false,  // popup window close after printing
		  strict: false 
		  });
	}
	$('.colch').click(function(){
		if($(this).is(':checked')){
			var col=$(this).attr('rel');
				$('.'+col).removeClass(' remove_tag');
		}else{
			var col=$(this).attr('rel');
			$('.'+col).addClass(' remove_tag');
		}
	})
	function preview(event){
				var countsel_id=jQuery(event.target).attr("rel");
				window.open("<?PHP echo site_url('social/counseling/preview');?>/"+id,'_blank');
		}
	function edit(event){
			var id=jQuery(event.target).attr("rel");
                //        alert(id);
			location.href="<?PHP echo site_url('social/counseling/edit');?>/"+id+"?<?php echo "m=$m&p=$p" ?>";
	}
	function delete_counseling(event){
                var id=jQuery(event.target).attr("rel");
                var con=confirm("Are you sure want to delete ...!");
                if(con==true)
                location.href="<?PHP echo site_url('social/counseling/deletes');?>/"+id+"?<?php echo "m=$m&p=$p" ?>";
	}
	$('#year').change(function(){
		search();
	})
	function search(event){
		var roleid=<?php echo $this->session->userdata('roleid');?>;
		var m=<?php echo $m;?>;
		var p=<?php echo $p;?>;
		var student_name=$("#namestudent").val();
		var student_id=$("#idstudent").val();
                var sort_num = $('#sort_num').val();
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/social/counseling/search",    
				data: {'student_name':student_name,
					'student_id':student_id,
                                        'id':roleid,
                                        'p':p,
                                        'm':m,
                                        'sort_num':sort_num
					},
				type: "POST",
				success: function(data){
                   //                 alert(data);
                   $('.listbody').html(data);
			}
		});
	}



$(document).ready(function() {
     $(".s_date,.end_date").datepicker({
            language: 'en',
            pick12HourFormat: true,
            format:'dd-mm-yyyy'
    });
    $('select.studentinfo').change(function() {
        var studentClass = $('select.studentinfo').find(':selected').data('studentclass');
        var studentId = $('select.studentinfo').find(':selected').data('studentid');
        var studentGender = $('select.studentinfo').find(':selected').data('studentgender');
        var sch_level = $('select.studentinfo').find(':selected').data('sch_level');
        var sch_year = $('select.studentinfo').find(':selected').data('sch_year');
        
        $('#studentClass').val(studentClass);
        $('#studentId').val(studentId);
        $('#studentGender').val(studentGender);
        $('#sch_level').val(sch_level);
        $('#sch_year').val(sch_year);
        
    });    
    $("body").delegate("#sch_year", "change", function () {
        var schlevelid = $("#sch_year").val();
        getClass(schlevelid);
    });
    //////////////
    $('#class_id').change(function(){
            var class_id = $(this).val();		
            var schoolid = $("#school_id").val();
            var programid = $("#program_id").val();
            var schlevelid = $("#studentlevel").val();
            var yearid = $("#adcademic_year_id").val();	
            var gradlavelid = $("#grade_level_id").val();	
            if(class_id == ""){
                    $('#student_id').html('');
            }else{
                    var url = "<?php echo site_url('social/counseling/get_student_data') ?>";
                    var data = {'class_id':class_id,
                                            'schoolid':schoolid,
                                            'programid':programid,
                                            'schlavelid':schlevelid,
                                            'yearid':yearid,
                                            'gradlavelid':gradlavelid
                                    };
                    getDataByAjax($('#student_id'), url, data);
            }
    });
    $("#studentlevel").val($("#studentlevel first").val());
    $('#studentlevel').change(function(){
            var studentlevel = $(this).val();
            var school_id = $('#school_id').val();
            var program_id = $('#program_id').val();

            if(studentlevel != ""){
                $.ajax({
                url: "<?php echo site_url('social/counseling/get_year_and_grade_data') ?>",	            
                type: "post",
                dataType: 'json',
                data: {'school_id': school_id, 'program_id': program_id, 'studentlevel': studentlevel},
                success: function (data) {
                    //console.log(data);
                    //get year--------------------------------
                    var getYear = '';	            	
                    $.each(data["year"], function(k,v){
                            getYear += '<option value="'+ v.yearid +'" data-studyyear="'+ v.sch_year +'">'+ v.sch_year+'</option>';		            		
                    }); $('#adcademic_year_id').html(getYear);

                    //get grade--------------------------------
                    var getGrade = '';
                    $.each(data["grade"], function(ke,re){
                            getGrade += '<option value="'+ re.grade_levelid +'" data-grandlevel="'+ re.grade_level +'">'+ re.grade_level+'</option>';
                    });	$('#grade_level_id').html(getGrade);

                    //get teacher------------------------------
                    var grade_level_id = $('#grade_level_id').val();
                    var adcademic_year_id = $('#adcademic_year_id').val();
                    var url = "<?php echo site_url('social/counseling/get_class_data') ?>";
                    var data = 'program_id='+program_id+'&studentlevel='+studentlevel+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
                               getDataByAjax($('#class_id'), url, data);
                                    $('#class_id').html('');
                                    $('#student_id').html('');

                                    //remove message require-------------------
                                    var aId = $('#adcademic_year_id').data('parsley-id');
                                    var gId = $('#grade_level_id').data('parsley-id');

                                    $('#parsley-id-'+aId).remove();	   
                                    $('#adcademic_year_id').removeClass('parsley-error');

                                    $('#parsley-id-'+gId).remove();	   
                                    $('#grade_level_id').removeClass('parsley-error');        	
                        }
            });	
            }else{
                    $('#adcademic_year_id').html('');
                    $('#grade_level_id').html('');
                    $('#class_id').html('');
                    $('#student_id').html('');
            }
    });
  
    
});
$(function(){    
    $("#frmtreatment").submit(function(e){
      e.preventDefault();
    })
    .validate({
        rules:{
          required:{
            required:true
          },
          url:{
            required:true,
            url: true
          }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
          $(element).parents('.form_sep').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form_sep').removeClass('has-error').addClass('has-success');
        },        
        submitHandler: function(form) {
          var url="<?php echo site_url('social/counseling/searchcounseling?m='.$m.'&p='.$p.'')?>"; 
          var schoollevel = $('select#studentlevel').find(':selected').data('schoollev');
          var classname = $('select#class_id').find(':selected').text();
          var studentname = $('select#student_id').find(':selected').data('studentname');
          var studyyear = $('select#adcademic_year_id').find(':selected').data('studyyear');
          var grandlevel = $('select#grade_level_id').find(':selected').data('grandlevel');
          var teachername = $('select#teacherid').find(':selected').data('teachername');
          var studentGender = $("#studentGender").val();
          var teachersubject = $("#teachersubject").text();
          var mistake = $("#mistake").val();
          var advised = $("#advised").val();
          var advice = $("#c_type").val();
          var student_id = $("#student_id").val();
          var idcounseling = $('#idcounseling').val();
          var roleid=<?php echo $this->session->userdata('roleid');?>;
          var m=<?php echo $m;?>;
          var p=<?php echo $p;?>;
          var sort_num = $('#sort_num').val();
          $.ajax({
            url:url,
            type:"POST",
            datatype:"Json",
            async:false,
            data:{          
                id:idcounseling,
                schoollevel:schoollevel,
                classname:classname,
                studentname:studentname,
                studyyear:studyyear,
                grandlevel:grandlevel,
                teachername:teachername,
                student_id:student_id,
                studentGender:studentGender,
                m:m,
                p:p,
                sort_num:sort_num,
                teachersubject : teachersubject,
                mistake:mistake,
                advised:advised,
                advice:advice
            },
            success:function(data) {
            var headreport = '<div class="col-sm-4"><div>មុខវិជ្ជា: '+(jQuery.type( teachersubject ) === "null"?'':teachersubject)+'</div><div>គ្រូឈ្មោះ: '+teachername+'</div></div><div class="col-sm-8"><div>ថ្នាក់ទី: '+(typeof classname == "undefined"?'':classname)+'</div><div>ឆ្នាំសិក្សា: '+(typeof studyyear == "undefined"?'':studyyear)+'</div></div>';
        	$('#reportheadcounseling').html(headreport);
                $('.reportheadcounseling').html(headreport);
                $('.listbody').html(data);
            },
            beforeSend: function() { $('.proccess_loading').show(); },
            complete: function() { $('.proccess_loading').hide();}
          })
        }
      });
   });
////////////////////

function getDataByAjax(selector, url, data){
    $.ajax({
        url: url,	            
        type: "post",
        dataType: 'html',
        data: data,
        success: function (data) {
            selector.html(data);		            	
        }
    });	
}
$('#grade_level_id').change(function(){
        var program_id = $('#program_id').val();
        var studentlevel = $('#studentlevel').val();
        var adcademic_year_id = $('#adcademic_year_id').val();
        var grade_level_id = $(this).val();
        var url = "<?php echo site_url('social/counseling/get_class_data') ?>";
        var data = 'program_id='+program_id+'&studentlevel='+studentlevel+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
        getDataByAjax($('#class_id'), url, data);
});
 $('#teacherid').change(function(){
        var teacherid = $(this).val();
        var class_id = $("#class_id").val();	
        var gradlavelid = $("#grade_level_id").val();
        var url = "<?php echo site_url('social/counseling/getsubbyteacher') ?>";
        var data = 'teacherid='+teacherid+'&class_id='+class_id+'&gradlavelid='+gradlavelid;
      //  alert(data);
        getDataByAjax($('#teachersubject'), url, data);
});   

///////////////////


function getSchYear(sclevelid) {

    $("#studentClass").html("");
    if (sclevelid != "") {
        $.ajax({
            url: "<?php echo site_url('social/counseling/getsubbyteacher'); ?>",
            type: "POST",
            dataType: 'json',
            async: false,
            data: {
                schlevelid: sclevelid
            },
            success: function (res) {
                var data = res.year;
                var tr = "<option value=''></option>";
                if (data.length > 0) {

                    for (var i = 0; i < data.length; i++) {
                        var row = data[i];
                        tr += '<option value="' + row.sch_year + '">' + row.sch_year + '</option>';
                    }
                }
                $("#studentClass").html(tr);
            }
        })
    }
}
    
</script>
