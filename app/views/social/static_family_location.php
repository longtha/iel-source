<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="col-xs-12 result_info">
		      	<div class="col-xs-6">
		      		<strong id='title' class="x_title"><?php echo $page_header;?></strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
			    	<span class="top_action_button">	
			    		<a href="#" id="export" title="Export">
			    			<img src="<?php echo base_url('assets/images/icons/export.png')?>" />
			    		</a>
			    	</span>
		      		<span class="top_action_button">
						<a href="#" id="print" title="Print">
			    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
			    		</a>
		      		</span>		    	     		
		      	</div> 			      
		  </div>
		  <!-- tha -->
	      	<div class="col-sm-12">
	      		<div class="panel panel-default" id="tab_print">
	      			<div class="panel-heading">
		                <span class="panel-title"><?php echo $page_header;?></span>
		                <span class='panel-title pull-right danger'> Total Communce: <?php echo $countcommunce;?> </span>
		            </div>	
		            <div class="panel-body">	           
			            <div class="form_sep">		                	
		           			<table class='table table-bordered table-striped' border="1">
		                   		<tbody><?php echo $sfl;?></tbody>
		                   </table>
		                </div> 
 					</div>
					
	      		</div>
	      	</div>
		    <!-- tha -->
	   </div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#print").on("click",function(){					
				var title="<h4 align='center'>"+ $("#title").html()+"</h4>";
			   	var data = $("#tab_print").html().replace(/<img[^>]*>/gi,"");
			   	var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
			   	var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
			   		
			   	gsPrint(title,export_data);
			});
		$("#export").on("click",function(e){
				var data=$("#tab_print").html().replace(/<img[^>]*>/gi,"");
	   			var export_data = $("<center><h3 align='center'>"+$("#title").html()+"</h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
				window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
    			e.preventDefault();
		});
	});		
</script>