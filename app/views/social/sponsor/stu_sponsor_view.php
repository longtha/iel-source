<style type="text/css">
    .loading {
          position: fixed;
          z-index: 999;
          height: 2em;
          width: 2em;
          overflow: show;
          margin: auto;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
          content: '';
          display: block;
          position: fixed;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
          /* hide "loading..." text */
          font: 0/0 a;
          color: transparent;
          text-shadow: none;
          background-color: transparent;
          border: 0;
        }

        .loading:not(:required):after {
          content: '';
          display: block;
          font-size: 10px;
          width: 1em;
          height: 1em;
          margin-top: -0.5em;
          
        }
</style>
<div class="wrapper">
<div id="wait" class="loading" align="center"><img src='<?php echo base_url('assets/images/icons/ajax-loader.gif')?>'/></div>
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
		      	<div class="col-xs-6">
		      		<strong id='title'><?php echo $page_header;?></strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
		      		
			    	<span class="top_action_button">	
			    		<?php if($this->green->gAction("E")){ ?>
				    		<a href="javaScript:void(0)" id="exports" title="Export to excel">
				    			<img id='export' src="<?php echo base_url('assets/images/icons/export.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>
		      		<span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ ?>
							<a href="javaScript:void(0)" id="print" title="Print">
				    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
				    		</a>
				    	<?php } ?>
		      		</span>	
		      	</div> 			      
		  </div>
	      	<div class="col-sm-12">
	      		<div class="panel panel-default">
	      			<div class="panel-body">
	      				<!-- start class table responsive -->
		           		<div class="table-responsive">
		           		<style type="text/css">
							#preview td{
								padding: 3px ;
							}
							th img{border:1px solid #CCCCCC; padding: 2px;}
							#preview_wr{
								margin: 10px auto !important;
							}
							.tab_head th,label.control-label{font-family:"Times New Roman", Times, serif !important; font-size: 14px;}
							td{font-family:"Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important; font-size: 14px;}

						</style>
							<div id="tab_print">
								<div style='width:250px; float:left;'>
									<img src="<?php echo base_url('assets/images/logo/logo.png')?>" style='width:200px;  height:60px;' />
									<div style=' font-weight:bold; text-transform: uppercase;'><?php echo $showschoolname;?></div>
								</div>
								<div style='float:right; margin:15px'>
								</div>
								<p style="clear:both"></p>
				                <table align='center'>
									<thead>
										<th valign='top' align="center" style='width:80%'><h4 align="center"><b><u>STUDENT DETAILS</u></b></h4></th>
									</thead>
								</table>
								<table align='center' id='preview' style='width:100%' border="0">
										<tr>
										<td width="13%"><label class="control-label">Student ID </label></td>
										<td width="1%"><label class="control-label">:</td>
										<td width="20%"><?php echo $stuinfo['student_num']?></td>
										<td width="13%"><label class="control-label">Family ID</label></td>
										<td width="1%"><label class="control-label">:</td>
										<td width="20%"><?php echo $stuinfo['familyid']?></td>
										<td><label class="control-label">Class : </label> 1A</td>
									</tr>
									<tr>
										<td><label class="control-label">Nom et Prénom</label></td>
										<td width="1%"><label class="control-label">:</td>
										<td><?php echo $stuinfo['fullname']?></td>
										<td><label class="control-label">En Khmer</label></td>
										<td width="1%"><label class="control-label">:</td>
										<td  colspan='2'><?php echo $stuinfo['fullname_kh']?></td>
									</tr>
									<tr>
										<td><label class="control-label">Date de naissance</label></td>
										<td width="1%"><label class="control-label">:</td>
										<td colspan='5'><?php echo date('d-m-Y',strtotime($stuinfo['dateofbirth']));?></td>
									</tr>
									<tr>
										<td><label class="control-label">Nationalité</label></td>
										<td width="1%"><label class="control-label">:</td>
										<td colspan='5'><?php echo $stuinfo['nationality']?></td>
									</tr>
									<tr>
										<td colspan='7'><label class="control-label">SPONSOR :</label></td>
									</tr>
									<tr>
										<td colspan='7'>
											<table id='preview' class='table table-bordered table-striped'>
												<thead class='tab_head'>
													<tr align='center' >
														<th width="10">No</th>
														<th width="10%">Sponsor ID</th>
														<th>Sponsor Name</th>
														<th>Position</th>
														<th>Phone</th>
													</tr>
												</thead>
												<tbody id="listsponsor"></tbody>
											</table>
										</td>
									</tr>	
								</table>
							</div>
						</div> 
						  <!-- end class table responsive -->
					</div>
	      		</div>
	      	</div>	      	 
	    </div>
   </div>
</div>
<script type="text/javascript">
	$(function(){
		views();
		$('#exports').on('click',function(e){
			e.preventDefault();
			window.open('data:application/vnd.ms-excel,' + encodeURIComponent(PrintingHtml()));
		});
		$('#print').on('click',function(){
			gsPrint(PrintingHtml());
		});
	});
	var PrintingHtml = function(){
		return $("<center>"+$("#tab_print").html()+"</center>").clone().find(".remove_tag").remove().end().html();a;
	}
	var views = function(){
		$("#wait").css("display", "block");
		var variables = {'stuid': "<?php echo $_GET['stu']?>"};	
		$.ajax({
			type:"POST",
			url:"<?php echo site_url('social/Stusponsor/sponsorlist');?>",
			async:false,
			dataType:"Json",
			data:{
				variables:variables
			},
			success:function(data){
				if(data.tr!=""){
					$('#listsponsor').html(data.tr);
				 }
				else{
					$('#listsponsor').html('<tr><td colspan="5" align="center">Data is empty display, Please try again..!</td></tr>');
				}
			}
		}).done(function( msg ) {
         	$("#wait").css("display", "none");
		}).fail(function() {
		  $('#tab_print').html('Failed to fetch data, Please contact administrator to solve this problem..!').css('text-align','center').show();
		});
	}
	var gsPrint = function(data){
		 var options = {
		 				mode:"popup",
		 				popHt: 600,
		 				popWd: 700,
		 				popX: 0,
		 				popY: 0,
		 				popTitle:"<?php echo $page_header;?>",
		 				popClose: false,
		 				strict: false 
		 			};
		 $("<div>"+data+"</div>").printArea(options);
	}	
</script>