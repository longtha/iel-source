<style type="text/css">
    .loading {
          position: fixed;
          z-index: 999;
          height: 2em;
          width: 2em;
          overflow: show;
          margin: auto;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
          content: '';
          display: block;
          position: fixed;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
          /* hide "loading..." text */
          font: 0/0 a;
          color: transparent;
          text-shadow: none;
          background-color: transparent;
          border: 0;
        }

        .loading:not(:required):after {
          content: '';
          display: block;
          font-size: 10px;
          width: 1em;
          height: 1em;
          margin-top: -0.5em;
          
        }
</style>
<div class="wrapper">
<div id="wait" class="loading" align="center"><img src='<?php echo base_url('assets/images/icons/ajax-loader.gif')?>'/></div>
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
		      	<div class="col-xs-6">
		      		<strong id='title'><?php echo $page_header;?></strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
		      		
			    	<span class="top_action_button">	
			    		<?php if($this->green->gAction("E")){ ?>
				    		<a href="javaScript:void(0)" id="exports" title="Export to excel">
				    			<img id='export' src="<?php echo base_url('assets/images/icons/export.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>
		      		<span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ ?>
							<a href="javaScript:void(0)" id="print" title="Print">
				    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
				    		</a>
				    	<?php } ?>
		      		</span>	
		      	</div> 			      
		  </div>
	      	<div class="col-sm-12">
	      		<div class="panel panel-default">
	      			<div class="panel-body">
	      				<!-- start class table responsive -->
		           		<div class="table-responsive">
							<div id="tab_print">
							
								<table class="table table-bordered table-striped">
		           				<thead>
		           				<tr>
							        <th data-field="no" data-radio="true" width="10">No</th>
							        <th data-field="studentid" data-switchable="false">StudentID</th>
							        <th data-field="fullname">Full Name</th>
							        <th data-field="level">Level</th>
							        <th data-field="class" data-visible="false">Class</th>
							        <th data-field="year" data-visible="false">Year</th>
							        <th data-field="action" data-switchable="false" width="20" class="remove_tag">Action</th>
							    </tr>
		           				</thead>
		           				<thead class="remove_tag">
		           					<th class="remove_tag"></th>
		           					<th class="remove_tag"><input type='text' onkeyup="views();" id="stu_code" class="form-control input-sm" /></th>
		           					<th class="remove_tag"><input type='text' onkeyup="views();" id="stu_fullname" class="form-control input-sm" /></th>
		           					<th class="remove_tag" colspan="3"></th>
		           					<th class="remove_tag"></th>
		           				</thead>
		           				<tbody class='listbody'></tbody>
		           				</table>
							</div>
							<!--  -->
							<table align="center" border="0" width="100%">
							<tr>
								<td align="center" width="6%">
								Display : 
									<select id="sort_num"  onchange='views();' style="padding:5px; margin-right:0px;">
										<?php
										$num=100;
										for($i=0;$i<10;$i++){?>
											<option value="<?php echo $num ;?>"><?php echo $num;?></option>
											<?php $num+=50;
										}
										?>
										<option value='all'>All</option>
									</select>
								</td>
								<td colspan="6" align="center">
									<div id="show_pagination" class="show_pagination"></div>
								</td>
							</tr> 
							</table>
							
						</div> 
						  <!-- end class table responsive -->
					</div>
	      		</div>
	      	</div>	      	 
	    </div>
   </div>
</div>
<script type="text/javascript">
	$(function(){
		views(1);
		$('#exports').on('click',function(e){
			e.preventDefault();
			window.open('data:application/vnd.ms-excel,' + encodeURIComponent(PrintingHtml()));
		});
		$(document).on('click', '.StuSponsor',function(){
			var stspid = $(this).attr('attr');
			// location.href="<?php echo site_url('social/stusponsor?y=').$_GET['y'].'&m='.$_GET['m'].'&p='.$_GET['p'].'&stu=';?>"+stspid;
			window.open("<?php echo site_url('social/stusponsor/views?y=').$_GET['y'].'&m='.$_GET['m'].'&p='.$_GET['p'].'&stu=';?>"+stspid, "_blank");
		});
		$(document).on('click', '.pagenav', function(){
		    var page = $(this).attr("id");
		    $(this).addClass( "active" );
			views(page);
			//$(this).removeClass( "active" )

		});
		$('#print').on('click',function(){
			gsPrint(PrintingHtml());
		});
	});
	var PrintingHtml = function(){
		return $("<center>"+$("#tab_print").html()+"</center>").clone().find(".remove_tag").remove().end().html();a;
	}
	var views = function(page=''){
		$("#wait").css("display", "block");
		var variables = {
						'yid'			: $('#year').val(),
						'slid'			: $('#schlevelid').val(),
						'stu_code'		: $('#stu_code').val(),
						'stu_fullname'	: $('#stu_fullname').val(),
						'sort_num'		: $('#sort_num').val()
						};	
		$.ajax({
			type:"POST",
			url:"<?php echo site_url('social/Stusponsor/stusponsor');?>",
			async:false,
			dataType:"Json",
			data:{
				variables:variables,
				'page':page
			},
			success:function(data){
				if(data.tr!=""){
					$('.listbody').html(data.tr);
					$('#show_pagination').html(data.paging.pagination);
				 }
				else{
					$('.listbody').html('<tr><td colspan="7" align="center">Data is empty display, Please try again..!</td></tr>');
					$('#show_pagination').html('');
					$('.listfooter').css("display", "none");
				}
			}
		}).done(function( msg ) {
         	$("#wait").css("display", "none");
		}).fail(function() {
		  $('#tab_print').html('Failed to fetch data, Please contact administrator to solve this problem..!').css('text-align','center').show();
		});
	}
	var gsPrint = function(data){
		 var options = {
		 				mode:"popup",
		 				popHt: 600,
		 				popWd: 700,
		 				popX: 0,
		 				popY: 0,
		 				popTitle:"<?php echo $page_header;?>",
		 				popClose: false,
		 				strict: false 
		 			};
		 $("<div>"+data+"</div>").printArea(options);
	}	
</script>