<style type="text/css">
    table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }
    .cssclass {
        width: 650px;
    }
    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }
    
    #se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	.to {color:#cc66ff; font-weight:bold;}
	.from {color:#FF0000; font-weight:bold;}
	
</style>
<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }

    // Program --------------------------------------------
    if(isset($getprogram) && count($getprogram)>0){
        $opprogram="";
        $opprogram="<option attr_price='0' value=''>--select--</option>";
        foreach($getprogram as $rowpro){
            $opprogram.="<option value='".$rowpro->programid."'>".$rowpro->program."</option>";
        }
        
    }
 ?>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
    <div class="col-sm-12">     
	    <div class="row">
	      	<div class="col-xs-12">
		         <div class="result_info">
		            <div class="col-xs-6">
		                <strong> Students in Grades  </strong>  
		            </div>
		            <div class="col-xs-6" style="text-align: right">
		            	<a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
		                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>" width="22px">
		               	</a>
		               	<a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add">
		                  <img src="<?= base_url('assets/images/icons/add.png') ?>" width="22px">
		               	</a>
		            </div>         
		         </div>
	      	</div>
	   	</div>
		<div class="collapse" id="collapseExample">             
		    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
		        
		     	<div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                   <h1 class="panel-title"  align="center">From Grades</h1>
                            </div>
                            <div class="panel-body">
                                <div class="form_sep">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="student_num">ID<span style="color:red"></span></label>
                                            <input onkeypress='return isNumberKey(event);' type="text" name="student_num" id="student_num" class="form-control" placeholder="ID">
                                             <input type="hidden" name="enrollid" id="enrollid"> 
                                        </div>
                                        <div class="form-group">
                                            <label for="full_name">Name English <span style="color:red"></span></label>
                                            <input type="text" name="full_name" id="full_name" class="form-control" placeholder="English">
                                        </div>
                                         <div class="form-group">
                                            <label for="full_Khmer">Name Khmer <span style="color:red"></span></label>
                                            <input type="text" name="full_Khmer" id="full_Khmer" class="form-control" placeholder="Khmer">
                                        </div>
                                        <div class="form-group">
                                            <label for="rangelevelid">Rang Level<span style="color:red"></span></label>
                                            <select name="rangelevelid" id="rangelevelid" class="form-control">
                                              
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group" style="display:none">
                                            <label for="schoolid">School Info<span style="color:red"></span></label>
                                            <select name="schoolid" id="schoolid" class="form-control">
                                                       
                                           </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="programid">Program<span style="color:red"></span></label>
                                            <select name="programid" id="programid" class="form-control">
                                                  <?php echo $opprogram;?>     
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="schlevelid">School Level<span style="color:red"></span></label>
                                            <select name="schlevelids" id="schlevelids" class="form-control">
                                              
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="yearid">Year<span style="color:red"></span></label>
                                            <select name="yearid" id="yearid" class="form-control">

                                           </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="classid">Class<span style="color:red"></span></label>
                                            <select name="classid" id="classid" class="form-control">
                                              
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
		     	<!-- form2 -->
                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title"  align="center">To Grades</h1>
                            </div>
                            <div class="panel-body">
                                <div class="form_sep">
                                   <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="to_programid">Program<span style="color:red"></span></label>
                                            <select name="to_programid" id="to_programid" class="form-control">
                                                <?php echo $opprogram;?>   
                                           </select>
                                        </div>
                                         <div class="form-group">
                                            <label for="to_schlevelids">School Level<span style="color:red"></span></label>
                                            <select name="to_schlevelids" id="to_schlevelids" class="form-control input-sm required">
                                              
                                           </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="to_yearid">Year<span style="color:red"></span></label>
                                            <select name="to_yearid" id="to_yearid" class="form-control">

                                            </select>
                                        </div>
                                    </div>
                                    <!-- form 2 -->
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="to_raglevelids">Rang Level<span style="color:red"></span></label>
                                            <select name="to_raglevelids" id="to_raglevelids" class="form-control">
                                              
                                            </select>
                                        </div>
                                        <div class="form-group" style="">
                                            <label for="to_classid">Class<span style="color:red"></span></label>
                                            <select name="to_classid" id="to_classid" class="form-control">
                                              
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="to_date">Date<span style="color:red"></span></label>
                                            <div class="input-group">
                                                <input type="text" name="to_date" id="to_date" class="form-control" placeholder="dd/mm/yyyy">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div style="padding-top:10px; text-align: center; " class="form-group">
                            <button id="serch" class="btn btn-warning btn-sm" type="button" name="serch">Serch</button>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <button id="save" class="btn btn-primary btn-sm" type="button" name="save" value="save">Save</button>
                        </div>            
                    </div>
		     	<div class="row">
			        <div class="col-sm-12">
			            <div class="form-group" style="border-bottom: 1px solid #CCC;">&nbsp;</div>           
			        </div>
		     	</div>
		  	</form>
		</div>
    <!-- show header -->
        <div class="col-sm-12">
            <div class="panel panel-default">
               <div class="table-responsive" id="div_export_print">
                    <div id="tab_print">
                        <table width="100%" border="0" class="table table-hover" id="setBorderTbl">
                            <thead>
	                             <tr>
		                              <?php
		                                 foreach ($thead as $th => $val) {
                                            if ($th == 'No'){
                                                echo "<th class='$val'width='5%'>".$th."</th>";
                                            }else if($th == "<input type='checkbox' name='checkAll[]'' class='checkAll ' value=''>"){
		                                      	echo "<th style='width:50px;'' class='$val'>".$th."</th>";
		                  					}else if($th == 'Photo'){
                                                echo"<th class='$val' width='8%'>".$th."</th>";
                                            }else{
		                                    	echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
											}
		                                 }
		                              ?>
	                           	</tr>
                            </thead>
                            <tbody id='listrespon'></tbody>
                        </table>
                     </div>
                 </div>
            </div>
            <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
                Display :   <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                                <?php
                                    $num=50;
                                    for($i=0;$i<10;$i++){?>
                                        <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                        <?php $num+=100;
                                    }
                                ?>
                            </select>
            </div>
            <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
                <div class="pagination" id="pagination" style="text-align:center"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#year,#schlevelid").hide();
        Getdata(1,$('#sort_num').val());

        // formatedate ---------------------------------
        $("#to_date").datepicker({
            language: 'en',
            pick12HourFormat: true,
            format:'dd-mm-yyyy'
        });
        // sort_num ------------------------------------
        $('#sort_num').change(function(){
            var  total_display= $(this).val();
            Getdata(1,total_display);
        });
        // pagenav --------------------------------------
        $("body").delegate(".pagenav","click",function(){
            var page = $(this).attr("id");
            var total_display = $('#sort_num').val();
            Getdata(page,total_display);
        });
        // keychang -----------------------------------
        $("#programid").on('change',function(){
            get_schlevel($(this).val());
        });
        $("#schlevelids ").on('change',function(){
            get_schyera($(this).val());
        });
        $("#rangelevelid").on('change',function(){
            get_schclass($(this).val());
        });
        $("#yearid").on('change',function(){
            schranglavel($(this).val());
        });
        $("#to_programid").on('change',function(){
            keychang();
            to_get_schlevel($(this).val());
        });
        $("#to_schlevelids").on('change',function(){
            keychang();
            to_get_schyera($(this).val());
        });
        $("#to_yearid").on('change',function(){
            keychang();
            get_schranglavel($(this).val());
        });
        $("#to_classid").on('change',function(){
            keychang();
        });
        $("#to_raglevelids").on('change',function(){
            to_get_schclass($(this).val());
        });
        // btn_search ---------------------------------
        $('body').delegate('#serch', 'click', function(){
            Getdata();
        });
        // refresh -----------------------------------
        $('body').delegate('#refresh', 'click', function(){
            location.reload();
        });
        // Addnew--------------------------------------
        $('body').delegate('#a_addnew', 'click', function(){
            $('#collapseExample').collapse('toggle');
            cleadata();
        });
        // main chech----------------------------------
        $('body').delegate('.checkAll', 'click', function(){
            var checkAll = $(this).val();           
            if($(this).is(':checked')){                
                $('.checksub').each(function(){
                   $('.checksub'+checkAll).prop('checked', true); 
                });
            }else{
               $('.checksub'+checkAll).prop('checked', false); 
            } 
        });
        // save --------------------------------------
        $("body").delegate("#save", "click", function (){

            var program = $('#to_programid').val();
            if(program ==""){
                alert("Select to Grades...!");
            }else if($('.checksub').is(':checked')){
                var i = 0;
                var arr_check = [];
                $(".checksub:checked").each(function(){
                    var studentId  = $(this).attr('data-studentId');
                    var schoolid   = $(this).attr('data-schoolid');
                    // console.log(schoolid);
                    // return false;
                    arr_check[i] = {studentId : studentId,schoolid : schoolid};
                    i++;
                });
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('student_fee/c_student_in_grades/savedata') ?>",
                    data : {
                        enrollid       : $("#enrollid").val(),
                        to_programid   : $("#to_programid").val(),
                        to_schlevelids : $("#to_schlevelids").val(),
                        to_yearid      : $("#to_yearid").val(),
                        to_raglevelids : $("#to_raglevelids").val(),
                        to_classid     : $("#to_classid").val(),
                        to_date        : $("#to_date").val(),
                        arr_check      : arr_check
                    },

                    success:function(data){

                        toastr["success"]("Students in Grades was created...!"); 
                        $('#collapseExample').collapse('toggle');
                        Getdata(1,$('#sort_num').val());
                        cleadata();
                        $("#save").html('Save'); 
                    }
                });
            }else{
                    alert(" Please check data first before process.!");
                }

        });
    });// end functiont 
    // copare funtioni-------------------------------------------------
    function keychang(){
        var form_program = $('#programid').val();
        var to_program = $('#to_programid').val();
        var from_schlevelid = $('#schlevelids').val();
        var to_schlevelid = $('#to_schlevelids').val();
        var from_yearid   = $('#yearid').val();
        var to_yearid    = $('#to_yearid').val();
        var from_classid = $('#classid').val();
        var to_classid   = $('#to_classid').val();
        var from_rangelevelid = $('#rangelevelid').val();
        var to_raglevelids = $('#to_raglevelids').val();
        if(form_program != "" && to_program !="" && from_schlevelid !="" && to_schlevelid !="" && from_yearid !="" && to_yearid !="" && from_classid !="" && to_classid !="" && from_rangelevelid !="" && to_raglevelids !=""){
            if((form_program == to_program) && (from_schlevelid == to_schlevelid) && (from_yearid == to_yearid) && (from_classid == to_classid) && (from_rangelevelid == to_raglevelids)){

               //toastr["success"]("Students in grades Duplicate...!");  
               alert("Students in grades Duplicate...!");
            }
        }
    }
    // Getdata -----------------------------------------------------------
        function Getdata(page,total_display,sortby,sorttype){
            var roleid = <?php echo $this->session->userdata('roleid');?>;
            var per = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
            var sort_num = $('#sort_num').val();
            var student_num = $('#student_num').val();
            var full_name = $('#full_name').val();
            var yearid = $('#yearid').val();
            var programid = $('#programid').val();
            var schlevelids = $('#schlevelids').val();
            var classid = $('#classid').val();
            var rangelevelid = $('#rangelevelid').val();
            var full_Khmer = $('#full_Khmer').val();

            $.ajax({
                url:"<?php echo base_url();?>index.php/student_fee/c_student_in_grades/Getdata",
                type: "POST",
                datatype: "Json",
                async: false,
                data:{
                        'page'  : page,
                        'p_page': per,
                        'total_display': total_display,
                        'sort_num':sort_num,
                        'sortby':sortby,
                        'sorttype':sorttype,
                        'student_num':student_num,
                        'full_name':full_name,
                        'yearid':yearid,
                        'programid':programid,
                        'schlevelids':schlevelids,
                        'classid':classid,
                        'rangelevelid': rangelevelid,
                        'full_Khmer': full_Khmer
                },
                success: function (data) {
                    $("#listrespon").html(data.data);
                    $('.pagination').html(data['pagination']['pagination']);
                }
            });
        }
        // sort -------------------------------------------------------------
        function sort(event){
           /*$(event.target)=$(this)*/ 
            this.sortby = $(event.target).attr("rel");
            if ($(event.target).attr("sorttype") == "ASC") {
                $('.sort').removeClass('cur_sort_up');
                $(event.target).addClass(' cur_sort_down');
                $(event.target).attr("sorttype", "DESC");
                this.sorttype = "ASC";
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_up');
            } else if ($(event.target).attr("sorttype") == "DESC") {
                $(event.target).attr("sorttype", "ASC");
                this.sorttype = "DESC";
                $('.sort').removeClass('cur_sort_up');
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_down');    
            }        
            Getdata(1,$('#sort_num').val(),this.sortby,this.sorttype);
        }
    // get_schlevel --------------------------------------------------
      function get_schlevel(programid){
           $.ajax({        
              url: "<?= site_url('student_fee/c_student_in_grades/get_schlevel') ?>",    
              data: {         
                'programid':programid
              },
              type: "post",
              dataType: "json",
              success: function(data){ 

                  var opt = '';
                      opt="<option  value=''>--select--</option>";
                  if(data.schlevel.length > 0){
                      $.each(data.schlevel, function(i, row){
                          opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                      });
                  }else{
                      opt += '';
                  }

                  $('#schlevelids').html(opt);
                  get_schyera($('#programid').val(), $('#schlevelids').val());
                  get_schclass($('#programid').val(), $('#classid').val());
             }
          });
      }
    //get yare---------------------------------------------------------
    function get_schyera(schlevelids){
        $.ajax({        
            url: "<?= site_url('student_fee/c_student_in_grades/get_schyera') ?>",    
            data: {         
              'schlevelids':schlevelids
            },
            type: "post",
            dataType: "json",
            success: function(data){ 
                var ya = '';
                    ya="<option  value=''>--select--</option>";
                if(data.schyear.length > 0){
                    $.each(data.schyear, function(i, row){
                        ya += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                    });
                }else{
                    ya += '';
                }

                $('#yearid').html(ya);
                 schranglavel($('#yearid').val(), $('#rangelevelid').val());
            }
        });
    }
    // from ranglavel--------------------------------------------------
    function schranglavel(yearid){
        $.ajax({        
            url: "<?= site_url('student_fee/c_student_in_grades/schranglavel') ?>",    
            data: {         
              'yearid':yearid
            },
            type: "post",
            dataType: "json",
            success: function(data){ 
                var rang = '';
                    rang="<option  value=''>--select--</option>";
                if(data.schranglavel.length > 0){
                    $.each(data.schranglavel, function(i, row){
                        rang += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
                    });
                }else{
                    rang += '';
                }
                
                $('#rangelevelid').html(rang);
            }
        });
    }
    // from class------------------------------------------------------
    function get_schclass(rangelevelid){
      $.ajax({        
          url: "<?= site_url('student_fee/c_student_in_grades/get_schclass') ?>",    
          data: {         
            'rangelevelid':rangelevelid
          },
          type: "post",
          dataType: "json",
          success: function(data){ 
              var cla = '';
                  cla="<option  value=''>--select--</option>";
              if(data.schclass.length > 0){
                  $.each(data.schclass, function(i, row){
                      cla += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                  });
              }else{
                  cla += '';
              }

              $('#classid').html(cla);
          }
      });
    }

// from 2 ----------
    // get_schlevel ---------------------------------------------
    function to_get_schlevel(to_programid){
       $.ajax({        
            url: "<?= site_url('student_fee/c_student_in_grades/to_get_schlevel') ?>",    
            data: {         
                'to_programid':to_programid
            },
            type: "post",
            dataType: "json",
            success: function(data){ 

                var opt = '';
                     opt="<option  value=''>--select--</option>";
                if(data.to_schlevel.length > 0){
                    $.each(data.to_schlevel, function(i, row){
                        opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                    });
                }else{
                    opt += '';
                }

              $('#to_schlevelids').html(opt);
                to_get_schyera($('#to_programid').val(), $('#to_schlevelids').val());
                to_get_schclass($('#to_programid').val(), $('#to_classid').val());
            }
        });
    }
    //get yare---------------------------------------------------------
    function to_get_schyera(to_schlevelids){
        $.ajax({        
            url: "<?= site_url('student_fee/c_student_in_grades/to_get_schyera') ?>",    
            data: {         
              'to_schlevelids':to_schlevelids
            },
            type: "post",
            dataType: "json",
            success: function(data){ 
                var ya = '';
                    ya="<option  value=''>--select--</option>";
                if(data.to_schyear.length > 0){
                    $.each(data.to_schyear, function(i, row){
                        ya += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                    });
                }else{
                    ya += '';
                }

                $('#to_yearid').html(ya);
                get_schranglavel($('#to_yearid').val(), $('#to_raglevelids').val());
            }
        });
    }
    // from ranglavel--------------------------------------------------
    function get_schranglavel(to_yearid){
        $.ajax({        
            url: "<?= site_url('student_fee/c_student_in_grades/get_schranglavel') ?>",    
            data: {         
              'to_yearid':to_yearid
            },
            type: "post",
            dataType: "json",
            success: function(data){ 
                var rang = '';
                    rang="<option  value=''>--select--</option>";
                if(data.schrang.length > 0){
                    $.each(data.schrang, function(i, row){
                        rang += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
                    });
                }else{
                    rang += '';
                }

                $('#to_raglevelids').html(rang);

            }
        });
    }

    // from class-----------------------------------------------
    function to_get_schclass(to_raglevelids){
        $.ajax({        
            url: "<?= site_url('student_fee/c_student_in_grades/to_get_schclass') ?>",    
            data: {         
                'to_raglevelids':to_raglevelids
            },
            type: "post",
            dataType: "json",
            success: function(data){ 
                var cla = '';
                    cla="<option  value=''>--select--</option>";
                if(data.to_schclass.length > 0){
                    $.each(data.to_schclass, function(i, row){
                        cla += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                    });
                }else{
                    cla += '';
                }

                $('#to_classid').html(cla);
            }
        });
    }
    // number key ---------------------------------------------
    function isNumberKey(evt) {
        var e = window.event || evt;  
        var charCode = e.which || e.keyCode;
        if ((charCode > 45 && charCode < 58) || charCode == 8) {
            return true;
        }
        return false;
    }
    // cleardata ----------------------------------------------
    function cleadata(){
        $("#to_programid").val("");
        $("#to_schlevelids").val("");
        $("#to_yearid").val("");
        $("#to_raglevelids").val("");
        $("#to_classid").val("");
        $("#to_date").val("");
        $("#enrollid").val("");
        $('.checksub').attr('checked', false);
        $('.checkAll ').attr('checked', false);
    }
</script>