<div class="container-fluid" style="width:100% !important">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
            	<span class="icon">
                    <i class="fa fa-th"></i>
                    </span>
                <strong>Assign Class</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                  <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
               </a>
               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
               </a>
               <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
               </a>           
               
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse in" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
         <input type="hidden" name="termid" id="termid">
         
         <div class="col-sm-4" style="">         
            <div class="form-group">
               <label for="student_num">ID<span style="color:red"></span></label>
               <input type="text" name="student_num" id="student_num" class="form-control" placeholder="ID">
            </div>

            <div class="form-group">
               <label for="full_name">Name (English)<span style="color:red"></span></label>
               <input type="text" name="english_name" id="english_name" class="form-control" placeholder="English Name">
            </div>

            <div class="form-group">
               <label for="full_name">Name (Khmer)<span style="color:red"></span></label>
               <input type="text" name="khmer_name" id="khmer_name" class="form-control" placeholder="Khmer Name">
            </div> 

            <div class="form-group" style="display:none">
               <label for="gender">Gender<span style="color:red"></span></label>
               <select name="gender" id="gender" class="form-control">
                  <option value="">All</option>
                  <option value="Female">Female</option>
                  <option value="Male">Male</option>
               </select>
            </div>  

         </div><!-- 1 -->

         <div class="col-sm-4" style="">           

            <div class="form-group">
               <label for="schoolid">School Info<span style="color:red"></span></label>
               <select name="schoolid" id="schoolid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->info->getschinfor() as $row) {
                        $opt .= '<option value="'.$row->schoolid.'">'.$row->name.'</option>';
                     }

                     echo $opt;        
                  ?>                  
               </select>
            </div>
            
            <div class="form-group">
               <label for="programid">Program<span style="color:red"></span></label>
               <select name="programid" id="programid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->p->getprograms() as $row) {
                        $opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group">
               <label for="schlevelids">School Level<span style="color:red"></span></label>
               <select name="schlevelids" id="schlevelids" class="form-control">
                  
               </select>
            </div>        
            
         </div><!-- 2 -->

         <div class="col-sm-4">

            <div class="form-group">
               <label for="yearid">Year<span style="color:red"></span></label>
               <select name="yearid" id="yearid" class="form-control">

               </select>
            </div>

            <div class="form-group">
               <label for="rangelevelid">Rangelevel Name<span style="color:red"></span></label>
               <select name="rangelevelid" id="rangelevelid" class="form-control">

               </select>
            </div>

         	<div class="form-group" style="">
               <label for="classid">Class<span style="color:red"></span></label>
               <select name="classid" id="classid" class="form-control">
                  <?php
                     // $opt = '';
                     // $opt .= '<option></option>';
                     // foreach ($this->c->allclass() as $row) {
                     //    $opt .= '<option value="'.$row->classid.'">'.$row->class_name.'</option>';
                     // }

                     // echo $opt;        
                  ?>
               </select>
            </div>

         </div><!-- 3 -->      

         <div class="col-sm-7 col-sm-offset-5">
            <div class="form-group">
               <button type="button" class="btn btn-success btn-sm btn_search" name="btn_search" id="btn_search" data-save='1'>Search</button>&nbsp; 
               <button type="button" class="btn btn-warning btn-sm btn_clear" name="btn_clear" id="btn_clear" data-save='1' style="display: none;">Clear</button>
               
            </div>            
         </div>
         <div class="row">
            <div class="col-sm-12">
               <div class="form-group" style="border-bottom: 1px solid #CCC;">&nbsp;</div>           
            </div>
         </div>
      </form>
   </div>   
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
	         <div id="tab_print">
	            <table border="0"​ cellspacing="0" cellpadding="0" align="center" id="se_list" class="table table-hover">               
						<thead>
						   <tr>
						      <th style="width: 5%;">No</th>
						      <th style="width: 10%;text-align: center;">Photo</th>
						      <th style="width: 18%;">Student Name</th>					      
						      <th style="width: 5%;">Program</th>
						      <th style="width: 5%;">School Level</th>
						      <th style="width: 5%;">Year</th>                        
						      <th style="width: 10%;" class="iscur">Current Range Level</th>
                        <th style="width: 15%;">New Range Level</th>
						      <th style="width: 10%;" class="iscur">Current Class</th>
                        <th style="width: 15%;">New Class</th>                        
						      <th style="width: 5%;text-align: center;">Status</th>
						   </tr>
						  
						</thead>                
	               <tbody>
	               	
	               </tbody>
	               
	               <tfoot>
	                 <tr class="remove_tag" style="border-top: 2px solid #ddd;">
                        <td style="padding-top: 32px;" colspan="5">
                        <span id="sp_total">
                        <label for="limit_record">
                        Limit
                        <span style="color:red"></span>
                        </label>
                        <select id="limit_record" class="" style="width:50px;" name="limit_record">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="300">300</option>
                        <option value="500">500</option>
                        <option value="">All</option>
                        </select>
                        </span>
                        </td>
                        <td style="text-align: right;" colspan="7">
                        <span id="sp_page" class="btn-group pagination" aria-label="..." role="group" style="display:none"></span>
                        </td>
                        </tr>
	               </tfoot>               
	            </table>
            </div>        
         </div>
      </div>
   </div>

</div>

<style type="text/css">
	#se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	.iscur {color:#65BA69; font-weight:bold;}
	
</style>

<script type="text/javascript">
   $(function(){     
$("#year,#schlevelid").hide();
      // init. =====
      grid(1, 10);

      // get ======= 
      $('body').delegate('#programid', 'change', function(){
         if($(this).val() - 0 > 0){
            get_schlevel($(this).val());
         }else{
            $('#schlevelids').html('');
            $('#yearid').html('');            
            $('#rangelevelid').html('');
            $('#classid').html('');
         }
                    
      });
      $('body').delegate('#schlevelids', 'change', function(){
         if($(this).val() - 0 > 0){
            get_year($('#programid').val(), $(this).val());
         }else{
            $('#yearid').html('');            
            $('#rangelevelid').html('');
            $('#classid').html('');
         }
      });
      $('body').delegate('#yearid', 'change', function(){
         get_rangelevel($(this).val());
      });

      // get schlevel ======
	  
	 function get_schlevel(programid = ''){      
         $.ajax({
            url: '<?= site_url('student_fee/c_student_fee_reminder_period/get_schlevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid                
            },
            success: function(data){

               // schoollevel ======
               var opt = '';
               opt += '<option value="">All</option>';
               if(data.schlevel.length > 0){
                  $.each(data.schlevel, function(i, row){
                     opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#schlevelids').html(opt);

               // get year =====
               get_year($('#programid').val(), $('#schlevelids').val());                  

            },
            error: function() {

            }
         });         
      }

      // get year =======
      function get_year(programid = '', schlevelids = ''){
         $.ajax({
            url: '<?= site_url('student_fee/c_student_fee_reminder_period/get_year') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelids                                  
            },
            success: function(data){
               var opt = '';
               opt += '<option value="">All</option>';
               if(data.year.length > 0){
                  $.each(data.year, function(i, row){
                     opt += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#yearid').html(opt);


               // class ========
               var opt1 = '';
               opt1 += '<option value="">All</option>';
               if(data.class.length > 0){
                  $.each(data.class, function(i, row){
                     opt1 += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                  });
               }else{
                  opt1 += '';
               }
               $('#classid').html(opt1);

               // range level ======
               get_rangelevel($('#yearid').val());

            },
            error: function() {

            }
         });
      }

      // get rangelevel =======
      function get_rangelevel(yearid = ''){
         // alert();

         $.ajax({
            url: '<?= site_url('student_fee/c_student_fee_reminder_period/get_rangelevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               yearid: yearid                                  
            },
            success: function(data){
               var opt = '';
               opt += '<option value="">All</option>';
               if(data.rangelevel.length > 0){
                  $.each(data.rangelevel, function(i, row){
                     opt += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#rangelevelid').html(opt);
            },
            error: function() {

            }
         });
      }   

      // tooltip ======
      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // print =======
      $('body').delegate('#a_print', 'click', function(){
         var title="<h4 align='center'>"+ "Assign Class" +"</h4>";
         var data = $("#tab_print").html(); //.replace(/<img[^>]*>/gi, "")
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint(title, export_data);
      });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Assign Class";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });      

      // chk =======
      $('body').delegate('#isclosed', 'click', function(){
         $(this).is(':checked') ? $(this).val(1) : $(this).val(0);
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#semester').select();
         $('#save').html('Save');
         $('#save_next').show();
         $('#f_save').parsley().destroy();
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#term').select();
         $('#save').html('Save');
         $('#save_next').show();
      });

      // search ========
      $('body').delegate('#btn_search', 'click', function(){
         grid(1, 10);
      });

      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, 10);
      });
	  
	$('body').delegate('.btn_update', 'click', function() {
		var tr=$(this).parent().parent();
		var studentid=tr.find('.studentid').val();
		var programid=tr.find('.programid').val();
		var schoolid=tr.find('.schoolid').val();
		var schlevelid=tr.find('.schlevelid').val();
		var yearid=tr.find('.yearid').val();
		var rangelevelid=tr.find('.rangelevelid').val();
		var classid=tr.find('.classid').val();
		var new_range=tr.find('.new_range option:selected').val();
		var new_class=tr.find('.new_class option:selected').val();
		// alert(studentid+'--'+programid+'--'+schoolid+'--'+schlevelid+'--'+yearid+'--'+rangelevelid+'--'+classid+'--'+new_range+'--'+new_class);
		if(confirm("Are yuo sure want to change recode...!")){
			$(this).parent().parent().find('#btn_update').addClass("disabled");
			$(this).parent().parent().find('#btn_update').removeClass("btn-success");
			$(this).parent().parent().find('#btn_update').html("Disabled");
			$.ajax({			
				url: '<?= site_url('student_fee/c_student_assignclass/FsaveRangClass') ?>',
				type: 'POST',
				datatype: 'JSON',
				// async: false,
				beforeSend: function(){
				},
				data: {
				   studentid: studentid,
				   programid: programid,
				   schoolid: schoolid,
				   schlevelid: schlevelid,
				   yearid: yearid,
				   rangelevelid: rangelevelid,
				   classid: classid,
				   new_range: new_range,	
				   new_class: new_class			                   
				},
				success: function(data){
					toastr["success"]("Data has bee saved...!");
					 grid(1, 10);
			   }
			 });
		}else{
			
		}
		 
	});
	 $('body').delegate('.new_range,.new_class', 'change', function() {
       // $('.oppaymentmethod option:selected').val()
		var new_class =$(this).parent().parent().find('.new_class option:selected').val();
		var new_range =$(this).parent().parent().find('.new_range option:selected').val();
		$(this).parent().parent().find('.new_range_class').val(new_range+'_'+new_class);
		var is_new=$(this).parent().parent().find('.new_range_class').val();
		var is_cur=$(this).parent().parent().find('.cur_range_class').val();
		if(is_new != is_cur){
			// alert("is save");
			$(this).parent().parent().find('#btn_update').addClass("btn-success");
			$(this).parent().parent().find('#btn_update').removeClass("disabled");
			$(this).parent().parent().find('#btn_update').html("Update");
		}else{
			$(this).parent().parent().find('#btn_update').addClass("disabled");
			$(this).parent().parent().find('#btn_update').removeClass("btn-success");
			$(this).parent().parent().find('#btn_update').html("Disabled");
			//alert("is not save");
		}
		
      });
   }); // ready ========

   // clear =======
   function clear(){
      $('#termid').val('');
      $('#term').val('');      
      $('#term_kh').val('');
      $('#programid').val('');
      $('#yearid').html('');
      $('#schlevelids').html('');
      $('#start_date').val('');
      $('#end_date').val('');
      $('#isclosed').val(0);
      $('#isclosed').prop('checked', false);
      $('#dv_rangelevelid').html('');      
   }

   // grid =========
   function grid(current_page = 0, total_display = 0){
      var offset = ((current_page - 1) * total_display) - 0;
      var limit = total_display - 0;

      $.ajax({
         url: '<?= site_url('student_fee/c_student_assignclass/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            offset: offset,
            limit: limit
            ,
            student_num: $('#student_num').val(),
            english_name: $('#english_name').val(),
            khmer_name: $('#khmer_name').val(),            
            gender: $('#gender').val(),
            schoolid: $('#schoolid').val(),
            programid: $('#programid').val(),
            schlevelid: $('#schlevelids').val(),
            yearid: $('#yearid').val(),
            rangelevelid: $('#rangelevelid').val(),
            limit_record: $('#limit_record').val(),
            classid: $('#classid').val()
         },
         success: function(data) {
         	// console.log(data.option);

            var tr = '';
            var total = '';
            var page = '';
			$('#se_list tbody').html(data.arr_tr);
			
            return false;
            if(data.result.length > 0){
               $.each(data.result, function(i, row){

               	var img = '<?= base_url()."assets/upload/students" ?>/'+ row.year + '/' + (row.student_num != null ? (row.student_num + '.jpg') : '') ;
				
						if(row.is_paid==0){
							var add_class='class="active"';
						}else{
							var add_class='class="inactive"';
						}

                  tr += '<tr '+add_class+'>'+
                           '<td>'+ (i + 1 + offset) +'</td>'+
                           '<td style="text-align: center;"><img src="'+ img +'" class="img-circle" alt="No Image" width="70" height="70"></td>'+
                           '<td>\
      								<div style="font-weight: bold;">'+ (row.student_num != null ? row.student_num : '') +'</div>\
      								<div>'+ (row.first_name_kh + ' ' + row.last_name_kh) +'</div>\
      								<div>'+ (row.first_name + ' ' + row.last_name) +'</div>\
      								<div>'+ (row.gender != null ? row.gender : '') +'</div>\
                           </td>'+  

                           '<td>\
                              <div class="programid" data-programid="'+ row.programid +'">'+ (row.program != null ? row.program : '') +'</div>\
                           </td>'+
                           '<td>\
                              <div class="schlevelid" data-schlevelid="'+ row.schlevelid +'">'+ (row.sch_level != null ? row.sch_level : '') +'</div>\
                           </td>'+
                           '<td>\
                              <div class="yearid" data-yearid="'+ row.year +'">'+ (row.sch_year != null ? row.sch_year : '') +'</div>\
                           </td>'+
                           '<td>\
                              <div class="rangelevelid" data-rangelevelid="'+ row.rangelevelid +'">'+ (row.rangelevelname != null ? row.rangelevelname : '') +'</div>\
                           </td>'+
                           '<td>\
                              <div class="classid" data-classid="'+ row.classid +'">'+ (row.class_name != null ? row.class_name : '') +'</div>\
                           </td>'+                        
                        '</tr>';
                  });

               // previous ====
               page += '<button type="button" class="btn btn-default btn-sm a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-sm a-pagination" data-current_page="' + (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next&nbsp;<i class="glyphicon glyphicon-chevron-right"></i></button>';

               total += '<span style="">'+ (data.totalRecord - 0 > 0 ? (data.totalRecord + ' Records') : '') +'</span>';
               
            }else{
               tr += '<tr><td colspan="12" style="text-align: center;font-weight: bold;">No Results</td></tr>';
            }

            $('#se_list tbody').html(tr);
            $('#se_list tfoot').find('#sp_total').html(total);
            $('#se_list tfoot').find('.pagination').html(page);
            
            show_select();

         },
         error: function() {

         }
      });
   }

   function get_new_range_lv(year = ''){
      // $.ajax({
      //    url: '<?= site_url('student_fee/c_student_fee_reminder_period/get_rangelevel') ?>',
      //    type: 'POST',
      //    datatype: 'JSON',
      //    // async: false,
      //    beforeSend: function(){
      //    },
      //    data: {
      //       yearid: yearid                                  
      //    },
      //    success: function(data){
      //       var opt = '';
      //       opt += '<option value="">All</option>';
      //       if(data.rangelevel.length > 0){
      //          $.each(data.rangelevel, function(i, row){
      //             opt += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
      //          });
      //       }else{
      //          opt += '';
      //       }
      //       $('#rangelevelid').html(opt);
      //    },
      //    error: function() {

      //    }
      // });
   }

   function show_select(){
      $('.new_rangelevelid').each(function(i){
        var value_select = $(this).parent().parent().find(".rangelevelid").data('rangelevelid');
        $(this).parent().parent().find(".new_rangelevelid").val(value_select);
      });
   }
</script>