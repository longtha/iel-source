<style type="text/css">
	#se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	#mdinvoice .modal-dialog  {width:99% !important;}			
	
	a {
		cursor: pointer;
	}
	 .gamt{ font-size:14px; color:#00F; font-weight:bold; text-align:right}
	 
div.relative {
    position: relative;
    width: 200px;
    height: 85px;
    border: 3px solid #73AD21;
}

div.absolute {
    position: absolute;
    top: 0px;
    right: 0;
    width: 200px;
    height: 80px;
    border: 3px solid #73AD21;
}

table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }

    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    /*.border-data{
    	border-radius: 50%;
    	border: 1px solid #ccc;
    }*/

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    .cl_inv{
      border-top: 1px solid #ccc;
    }

    .tr_row{
      color: red;
    }
    .padleft{
    	padding-left: 20px;
    }
</style>
<div class="container-fluid" style="width:100% !important">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6 btn-success" style="background: #4cae4c;color: white;">
            	<span class="icon">
                  <i class="glyphicon glyphicon-stats"></i>
              </span>
                <strong>History Fee Delete List </strong>  
            </div>
            <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
              <!-- <a href="javascript:;" class="btn btn-sm btn-success" id="a_addnew" data-toggle="tooltip" data-placement="top" title="New others expenses"><span class="glyphicon glyphicon-plus"></span></a> -->
               <?php if($this->green->gAction("R")){ ?> 
               <a href="javascript:;" class="btn btn-sm btn-success" id="a_search" data-toggle="tooltip" data-placement="top" title="Search"><span class="glyphicon glyphicon-search"></span></a>
                <?php }?>
               <a href="javascript:;" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh"><span class="glyphicon glyphicon-refresh"></span></a>
               <?php if($this->green->gAction("E")){ ?> 
               <a href="javascript:;" class="btn btn-success" id="Export" data-toggle="tooltip" data-placement="top" title="Export"><span class="glyphicon glyphicon-export"></span></a>
               <?php }?>
               <?php if($this->green->gAction("P")){ ?> 
               <a href="javascript:;" class="btn btn-success " id="Print" data-toggle="tooltip" data-placement="top" title="Print"><span class="glyphicon glyphicon-print"></span></a>            
                <?php }?> 
            </div>          
         </div>
      </div>
   </div>

   <div class="collapse" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
         <input type="hidden" name="termid" id="termid">
         <div class="col-sm-4" style=""> 
            <div class="form-group">
               <label for="typeno">Reference<span style="color:red"></span></label>
               <input type="text" name="typeno" id="typeno" class="form-control" placeholder="# Reference">
            </div>

            <div class="form-group">
               <label for="student_num">ID<span style="color:red"></span></label>
               <input type="text" name="student_num" id="student_num" class="form-control" placeholder="ID">
            </div>            

			     <div class="form-group">
               <label for="student_name">English/Khmer Name<span style="color:red"></span></label>
               <input type="text" name="student_name" id="student_name" class="form-control" placeholder="English/Khmer Name">
            </div>

            <div class="form-group">
               <label for="gender">Gender<span style="color:red"></span></label>
               <select name="gender" id="gender" class="form-control">
               	<option value="">--select--</option>
               	<option value="Female">Female</option>
               	<option value="Male">Male</option>
               </select>
            </div>

            <div class="form-group">
               <label for="schoolid">School Info<span style="color:red"></span></label>
               <select name="schoolid" id="schoolid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">--select--</option>';
                     foreach ($this->info->getschinfor() as $row) {
                        $opt .= '<option value="'.$row->schoolid.'">'.$row->name.'</option>';
                     }

                     echo $opt;        
                  ?>                 
               </select>
            </div>     

         </div><!-- 1 -->

         <div class="col-sm-4">
            
            <div class="form-group">
               <label for="programid">Program<span style="color:red"></span></label>
               <select name="programid" id="programid" class="form-control">
                   <?php
                     $opt = '';
                     $opt .= '<option value="">--select--</option>';
                     foreach ($this->p->getprograms() as $row) {
                        $opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group">
               <label for="schlevelids">School Level<span style="color:red"></span></label>
               <select name="schlevelids" id="schlevelids" class="form-control">
                  
               </select>
            </div>

            <div class="form-group">
               <label for="yearid">Year<span style="color:red"></span></label>
               <select name="yearid" id="yearid" class="form-control">

               </select>
            </div>

            <div class="form-group">
               <label for="rangelevelid">Rangelevel Name<span style="color:red"></span></label>
               <select name="rangelevelid" id="rangelevelid" class="form-control">

               </select>
            </div>
            
            <div class="form-group" style="">
               <label for="classid">Class<span style="color:red"></span></label>
               <select name="classid" id="classid" class="form-control">
                  
               </select>
            </div>        
            
         </div><!-- 2 -->

        <div class="col-sm-4">
            <div class="form-group"​ style="">
               <label for="from_date">From Date<span style="color:red"></span></label>
               <div class="input-group">
                 <input type="text" name="from_date" id="from_date" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
               </div>
            </div>

            <div class="form-group" style="">
               <label for="to_date">To Date<span style="color:red"></span></label>
                 <div class="input-group">
                 <input type="text" name="to_date" id="to_date" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
               </div>
            </div>

            <div class="form-group" style="">
               <label for="feetypeid">Payment Method<span style="color:red"></span></label>
               <select name="feetypeid" id="feetypeid" class="form-control"  >
                  <?php
                     $opt = '';
                     $opt .= '<option value="">--select--</option>';
                     foreach ($this->f->getfeetypes() as $row) {
                        $opt .= '<option value="'.$row->feetypeid.'">'.$row->schoolfeetype.'</option>';
                     }

                     echo $opt;        
                  ?> 
               </select>
            </div>

            <div class="form-group" style="">
               <label for="term_sem_year_id">Study Period<span style="color:red"></span></label>
               <select name="term_sem_year_id" id="term_sem_year_id" class="form-control">
                 
               </select>
            </div>
 			      <div class="form-group">
               <label for="report_type">Restor Type<span style="color:red"></span></label>
               <select name="report_type" id="report_type" class="form-control">
               		<?php
	                    $systype = '';
	                    $systype .= '<option value="">--select--</option>';
	                    foreach ($this->restor->get_systype() as $row) {
	                        $systype .= '<option value="'.$row->typeid.'">'.$row->type.'</option>';
	                    }

	                    echo $systype;        
                  	?> 
               </select>
            </div>
         </div><!-- 3 -->      

         <div class="col-sm-7 col-sm-offset-5">
            <div class="form-group">
                <?php if($this->green->gAction("R")){ ?> 
                  <a id="btn_search" class="btn btn-primary btn-success btn_search"><span class="glyphicon glyphicon-search"></span> Search</a>
                <?php }?>
               <button type="button" class="btn btn-primary btn-sm btn_search" name="btn_search" id="btn_search" data-save='1' style="display: none;">Search</button>&nbsp; 
               <button type="button" class="btn btn-warning btn-sm btn_clear" name="btn_clear" id="btn_clear" data-save='1' style="display: none;">Clear</button>               
            </div>            
         </div>

      </form>      
      <div class="row">
         <div class="col-sm-12">
            <div class="col-sm-12" style="border-bottom: 1px solid #CCC;">&nbsp;</div>
         </div>           
      </div>
   </div>   
   
   <div class="row">
      	<div class="col-sm-12">
            <div class="panel panel-default">
               <div class="table-responsive" id="div_export_print">
                    <div id="tab_print">
                        <table width="100%" border="0" class="table table-hover" id="setBorderTbl">
                            <thead>
	                             <tr>
		                              <?php
		                                 foreach ($thead as $th => $val) {
                                            if ($th == 'No'){
                                                echo "<th class='$val'width='5%'>".$th."</th>";
                                            }else{
		                                    	echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
											}
		                                 }
		                              ?>
	                           	</tr>
                            </thead>
                            <tbody id='listrespon'></tbody>
                        </table>
                     </div>
                 </div>
            </div>
            <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
                Display :   <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                                <?php
                                    $num=10;
                                    for($i=0;$i<10;$i++){?>
                                        <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                        <?php $num+=30;
                                    }
                                ?>
                            </select>
            </div>
            <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
                <div class="pagination" id="pagination" style="text-align:center"></div>
            </div>
        </div>
	</div>
<script type="text/javascript">
	 $(function(){    
      
	 	  schowdata(1,$('#sort_num').val());
      $(".trprint").hide();

      $('[data-toggle="tooltip"]').tooltip();
		  $('#from_date').datepicker({
	      format: 'dd/mm/yyyy',
	        //startDate: '-3d'
	    });
	     
	    $('#to_date').datepicker({
	      format: 'dd/mm/yyyy',
	        //startDate: '-3d'
	    });
	     // hide schow -----------------------------------
        $('body').delegate('#a_search', 'click', function(){
            $('#collapseExample').collapse('toggle');
            clardata();
        });
      	// refresh -------------------------------------
      	$('body').delegate('#refresh', 'click', function(){
         	location.reload();
      	});
	    // serch -----------------------------------------
	    $('body').delegate('#btn_search', 'click', function(){
         	schowdata(1, $('#sort_num').val() - 0);
          $(".trprint").hide();
      	});
	    // key up -----------------------------------------
	    $("#programid").on('change',function(){
            get_schlevel($(this).val());
        });
	     $("#schlevelids ").on('change',function(){
            get_schyera($(this).val());
        });
	    $("#rangelevelid").on('change',function(){
            get_schclass($(this).val());
        });
        $("#yearid").on('change',function(){
            schranglavel($(this).val());
        });
        // sort_num ------------------------------------
        $('#sort_num').change(function(){
            var  total_display= $(this).val();
            schowdata(1,total_display);
            $(".trprint").hide();
        });
        // pagenav --------------------------------------
        $("body").delegate(".pagenav","click",function(){
            var page = $(this).attr("id");
            var total_display = $('#sort_num').val();
            schowdata(page,total_display);
            $(".trprint").hide();
        });

        // Restor ---------------------------------------
        $('body').delegate('#Restor', 'click', function(){
           
            var type=$(this).attr('type');
            var typeno=$(this).attr('typeno');
            var attr_type=$(this).attr('attr_type');
            var attr_typeno=$(this).attr('attr_typeno');
            //alert(type+'--'+typeno+'--'+attr_type+'--'+attr_typeno);
            if(confirm("Are You Sure want to Restor.....")){
              $.ajax({
                  url:"<?php echo base_url(); ?>index.php/student_fee/c_student_restor/FrestorDatas",    
                  data: {   
                    'type':type,                  
                    'typeno':typeno,   
                    'attr_type':attr_type,   
                    'attr_typeno':attr_typeno   

                    },
                  type:"post",
                  dataType:"json",
                  async:false,
                  success: function(data){          
                    // toastr["warning"]('Record has been deleted!');
                   
                  }
             });

          }

        });
        // select term_sem_year_id ----------------------
        $('body').delegate('#feetypeid', 'change', function(){
	      	var feetypeid = $(this).val() - 0;
	      	$.ajax({
	            url: '<?= site_url('student_fee/c_student_restor/get_paymentType') ?>',
	            type: 'POST',
	            datatype: 'JSON',
	            // async: false,
	            beforeSend: function(){
	            },
	            data: {
	               feetypeid: feetypeid,
	               programid: $('#programid').val(),
	               schlevelid: $('#schlevelids').val(),
	               yearid: $('#yearid').val()
	                               
	            },
	            success: function(data){
	               // schoollevel ----------------
	               var opt = '';               
	               if(data.length > 0){     
	                  opt += '<option value="">--select--</option>';          
	                  $.each(data, function(i, row){
	                     opt += '<option value="'+ row.term_sem_year_id +'">'+ row.period +'</option>';
	                  });
	               }else{
	                  opt += '';
	               }
	               $('#term_sem_year_id').html(opt);             

	            },
	            error: function() {

	            }
	        });
      	});
      // tbn printer ------------------------------------------
      $("#Print").on("click", function () {
          $(".trprint").show();
          $(".trhide").hide();
          var htmlToPrint = '' +
              '<style type="text/css">' +
              'table th, table td {' +
              'border:1px solid #000 !important;' +
              'padding;0.5em;' +
              '}' +
              '</style>';
          var title = "History Fee Delete List";
          var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "");
          var export_data = $("<center>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
          export_data += htmlToPrint;
          gsPrint(title, export_data);
          $(".trprint").hide();
          $(".trhide").show();

      });
      // tbn exsport ---------------------------------------------
      $("#Export").on("click", function (e) {
          $(".trprint").show();
          $(".trhide").hide();
          var title = "History Fee Delete List";
          var data = $('.table').attr('border', 1);
          data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
          var export_data = $("<center><h3 align='center'>" + title + "</h3>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
          window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
          e.preventDefault();
          $('.table').attr('border', 0);
          $(".trprint").hide();
          $(".trhide").show();
      });
	}); // end function -------------------------------------------
      
       function gsPrint(emp_title, data) {
          var element = "<div>" + data + "</div>";
          $("<center><p style='padding-top:15px;text-align:center;'><b>" + emp_title + "</b></p><hr>" + element + "</center>").printArea({
              mode: "popup",  //printable window is either iframe or browser popup
              popHt: 600,  // popup window height
              popWd: 500,  // popup window width
              popX: 0,  // popup window screen X position
              popY: 0, //popup window screen Y position
              popTitle: "test", // popup window title element
              popClose: false,  // popup window close after printing
              strict: false

          });
        }

	 	// get_schlevel --------------------------------------------------
      function get_schlevel(programid){
           $.ajax({        
              url: "<?= site_url('student_fee/c_student_restor/get_schlevel') ?>",    
              data: {         
                'programid':programid
              },
              type: "post",
              dataType: "json",
              success: function(data){ 

                  var opt = '';
                      opt="<option  value=''>--select--</option>";
                  if(data.schlevel.length > 0){
                      $.each(data.schlevel, function(i, row){
                          opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                      });
                  }else{
                      opt += '';
                  }

                  $('#schlevelids').html(opt);
                  get_schyera($('#programid').val(), $('#schlevelids').val());
                  get_schclass($('#programid').val(), $('#classid').val());
             }
          });
     	}

     	//get yare---------------------------------------------------------
	    function get_schyera(schlevelids){
	        $.ajax({        
	            url: "<?= site_url('student_fee/c_student_restor/get_schyera') ?>",    
	            data: {         
	              'schlevelids':schlevelids
	            },
	            type: "post",
	            dataType: "json",
	            success: function(data){ 
	                var ya = '';
	                    ya="<option  value=''>--select--</option>";
	                if(data.schyear.length > 0){
	                    $.each(data.schyear, function(i, row){
	                        ya += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
	                    });
	                }else{
	                    ya += '';
	                }

	                $('#yearid').html(ya);
	                 schranglavel($('#yearid').val(), $('#rangelevelid').val());
	            }
	        });
	    }

      	// from ranglavel--------------------------------------------------
	    function schranglavel(yearid){
	        $.ajax({        
	            url: "<?= site_url('student_fee/c_student_restor/schranglavel') ?>",    
	            data: {         
	              'yearid':yearid
	            },
	            type: "post",
	            dataType: "json",
	            success: function(data){ 
	                var rang = '';
	                    rang="<option  value=''>--select--</option>";
	                if(data.schranglavel.length > 0){
	                    $.each(data.schranglavel, function(i, row){
	                        rang += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
	                    });
	                }else{
	                    rang += '';
	                }
	                
	                $('#rangelevelid').html(rang);
	            }
	        });
	    }

	    // from class------------------------------------------------------
	    function get_schclass(rangelevelid){
	      $.ajax({        
	          url: "<?= site_url('student_fee/c_student_restor/get_schclass') ?>",    
	          data: {         
	            'rangelevelid':rangelevelid
	          },
	          type: "post",
	          dataType: "json",
	          success: function(data){ 
	              var cla = '';
	                  cla="<option  value=''>--select--</option>";
	              if(data.schclass.length > 0){
	                  $.each(data.schclass, function(i, row){
	                      cla += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
	                  });
	              }else{
	                  cla += '';
	              }

	              $('#classid').html(cla);
	          }
	      });
	    }
	    // function showdata -------------------------------------------------
	    function schowdata(page,total_display,sortby,sorttype){
            var roleid = <?php echo $this->session->userdata('roleid');?>;
            var per = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
            var sort_num = $('#sort_num').val();

            var typeno = $('#typeno').val();
            var student_num = $('#student_num').val();
            var student_name = $('#student_name').val();
            var gender = $('#gender').val();
            var schoolid = $('#schoolid').val();
            var programid = $('#programid').val();
            var schlevelids = $('#schlevelids').val();
            var yearid = $('#yearid').val();
            var rangelevelid = $('#rangelevelid').val();
            var classid = $('#classid').val();
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var feetypeid = $('#feetypeid').val();
            var term_sem_year_id = $('#term_sem_year_id').val();
            var report_type = $('#report_type').val();

            $.ajax({
                url:"<?php echo base_url();?>index.php/student_fee/c_student_restor/schowdata",
                type: "POST",
                datatype: "Json",
                async: false,
                data:{
                        page  : page,
                        p_page: per,
                        total_display: total_display,
                        sort_num:sort_num,
                        sortby:sortby,
                        sorttype:sorttype,

                        typeno:typeno,
                        student_num:student_num,
                        student_name:student_name,
                        gender:gender,
                        schoolid:schoolid,
                        programid:programid,
                        schlevelids:schlevelids,
                        yearid:yearid,
                        rangelevelid:rangelevelid,
                        classid:classid,
                        from_date:from_date,
                        to_date:to_date,
                        feetypeid:feetypeid,
                        term_sem_year_id:term_sem_year_id,
                        report_type:report_type,
                        m: '<?php echo (isset($_GET['m']) ? $_GET['m'] : ''); ?>',
      					        p: '<?php echo (isset($_GET['p']) ? $_GET['p'] : ''); ?>'
                },
                success: function (data) {
                    $("#listrespon").html(data.data);
                    $('.pagination').html(data['pagination']['pagination']);
                }
            });
	    }
	    // sort ----------------------------------------------------
        function sort(event){
           /*$(event.target)=$(this)*/ 
            this.sortby = $(event.target).attr("rel");
            if ($(event.target).attr("sorttype") == "ASC") {
                $('.sort').removeClass('cur_sort_up');
                $(event.target).addClass(' cur_sort_down');
                $(event.target).attr("sorttype", "DESC");
                this.sorttype = "ASC";
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_up');
            } else if ($(event.target).attr("sorttype") == "DESC") {
                $(event.target).attr("sorttype", "ASC");
                this.sorttype = "DESC";
                $('.sort').removeClass('cur_sort_up');
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_down');    
            } 
            schowdata(1,$('#sort_num').val(),this.sortby,this.sorttype);
             $(".trprint").hide(); 
        }
        // clardata -------------------------------------------------
        function clardata(){
        	$('#typeno').val("");
            $('#student_num').val("");
            $('#student_name').val("");
            $('#gender').val();
            $('#schoolid').val("");
            $('#programid').val("");
            $('#schlevelids').val("");
            $('#yearid').val("");
            $('#rangelevelid').val("");
            $('#classid').val("");
            //$('#from_date').val("");
            //$('#to_date').val("");
            $('#feetypeid').val("");
            $('#term_sem_year_id').val("");
            $('#report_type').val("");
        }
</script>