<?php
	//$cars=array("Volvo","BMW","Toyota");
	$relation=array("Legal Student","father`s child","mother`s child","brother/sister","Cousin","niece/nephew");
	$social_status=array("Normal","alcohol","violent","Mental","Drug");
	$Health=array("Normal","Handicap 50%","Handicap 100%");
	$civil_status=array("Single","Married","Divorce/widow");
	$m='';
	$p='';
	if(isset($_GET['m'])){
    	$m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }
	
?>

<style type="text/css">
	body {
		font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
	}
	.vin_engin{ line-height:15px !important;}
	.loop_detial{ padding:2px !important;}
	#container-fluid{
		border: 0px solid #CCC;		
		width:793.700787402px;
		height:1022.52px;
		margin-right: auto;
  		margin-left: auto;
		padding:12px !important;
	}

	.container_1{
		height:166px;
		border:#0F0 solid 0px;
		padding-left:5px !important;
		
		}
	.container {
		overflow: auto;
		border: 0px solid #f00;
		margin-right: auto;
  		margin-left: auto;
	}
	.container-note-squair {
		/*width: 788px;
		border: 0px solid #06F;
		overflow: auto;
		padding-bottom:0px;
		*/
	}
	.container-note-squair-15px {
		width: 668px;
		border: 0px solid #f00;
		overflow: auto;
		padding-left: 0px;
	}
	
	.tbl-address td {
		font-size: 13.5px !important;
		line-height: 18.45px;
		vertical-align: top;
		font-family:Khmer OS battambang;
		padding-top:1px !important;
		
	}
	.tbl-address td > span {
		font-size: 10px !important;
	}
	.marging-toppx {
		margin-top: -4px;
	}
	.marging-top15px {
		margin-top: 0px;
	}
	.margin-under-text{
		margin-top: -10px;
		display:none;
	}
	#set-line {
		width: 737px;
		margin: 0.5px 0px;
		border-top: 2px solid #000;
		margin-right: auto;
  		margin-left: auto;
	}
	#block-logo {
		width: 100px;
		border: 0px solid #00f;
		float: left;
		margin-top: 5px !important;
		margin-left: 0px !important;
		text-align: center;
	}
	/*#block-cominfo{
		border: 1px dashed #666;
		width: 688px;
		float: left;
		overflow: auto;
	}*/
	.border-dashed {
		border: 0px dashed #666;
		padding: 2px;
	}
	#com-name-kh {
		font-size: 20px;
		text-align: center;
		font-weight: bold;
		font-family: 'Khmer OS Muol Light';
		
	}
	#inv_no {
		font-size:25px;
		text-align: center;
		
		font-family: 'Khmer OS Muol Light';
		padding-top:0px !important;
		
	}
	#com-name-en {
		font-size: 18px;
		text-align: center;
		font-weight: bold;
	}
	.com-name-en-top,.com-name-kh-top{		
		padding-right:70px;
	}
	.note-number1 {
		font-size: 11px;
		width: 375px;
		float: left;
		font-family:Khmer OS battambang;
		border:solid #00F 0px;
		font-weight: bold;
		text-align:right;
	}
	.note-number {
		font-size: 11px;
		width: 212px;
		float: left;
		font-family:Khmer OS battambang;
		
		border:solid #00F 0px;
		padding-left:0px;
		font-weight: bold;
	}.note-number > span {
		font-size: 10px;
	}
	.font-bold {
		font-weight: bold;
	}
	.contain-squair {
		width: 348px;
		border: 0px solid #00f;
		float: left;
		
	}
	.contain-squair1 {
		
		border: 0px solid #00f;
		float: left;
		
	}.squair-box {
		border: 1px solid #666;
	    float: left;
	    height: 15px;
	    margin: 1px;
	    padding: 0;
	    text-align: center;
	    width: 15px;
		font-size:12px;
	}
	.squair-box-tray {
		width: 10px;
		height: 10px;
		float:left;
		text-align: center;
	}
	.cust-info-left {
		width: 390px;
		float: left;
		border: 1px dashed #666;
		padding-left: 5px;		
	}
	.cust-info-right {
		width: 390px;
		float: left;
		border: 1px dashed #666;
		margin-left: 10px;
		padding-left: 15px;
	}
	.tbl-address td > .font-bold-cust {
		font-size: 14px !important;
		font-weight: bold;
		font-family: 'Khmer OS Muol Light';
		line-height: 25px;
	}
	.tbl-address td > .cust_info_kh {
		font-size: 14px !important;
		font-weight: bold;
		font-family: 'Khmer OS Muol Light';
		line-height: 25px;
		
		
	}
	.tbl-address td > .cust_info_eng {
		font-size: 12px !important;
		font-weight: bold;	
		line-height: 25px;
	}
	
	.tbl-items{
		border-collapse: collapse;
	}
	
	
	.tbl-items tbody td{
		border: 1px solid #000;
		line-height: 22px;
		font-size:13px;
		font-family:Khmer OS battambang;
		line-height: 21px !important;
		padding:5px;
	}
	.tbl-items th {
		font-size: 14px !important;
		text-align: center;
		font-weight: bold;
		font-family:Khmer OS battambang;
		border: 1px solid #000;
		line-height: 20px;
		padding-top:3px;
		
	}
	.tbl-items th > span{
		font-size:12px;
	}
	
	.add-line-height {
		line-height: 10px !important;
		padding-right: 3px !important;
		font-size: 12px !important;
		font-weight:bold;
		width:125px !important;
	}
	.add-line-height > span{
		font-size: 9px !important;
		font-weight:bold;
	}
	.note-footer {
		margin-top: 15px;
		font-size: 12px;
		line-height: 15px;
	}
	.note-footer > span {
		font-size: 11px;
	}
	.margin-top-5px {
		 margin-top:-5px;
	}
	.tfoot_amt{ font-weight:bold;width:125px !important;}
	
	.custinfo{ font-size:13px !important}
	.inv_info{ font-size:12px !important}
	
	.notepaid{
		font-size: 12px !important;
		vertical-align: top;
		font-family:Khmer OS battambang;
		padding-top:1px !important;
		
	}
</style>

<!DOCTYPE html>

<head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <link rel="shortcut icon" href="http://localhost:8181/laravel-app/ineedpost/public/assets/images/icons/returant-icon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
</head>
<!-- returant-icon.ico -->
<body>
	<div id="container-fluid">	    
	   <!--<div id="set-line"></div>-->	
	   	<div style="padding-top:130px"></div>
        <!--
        <table border="0" style="padding-top:130px;  width:100% !important">
	    	<tr>
	    		
	    		<td style="padding-left: 650px; font-family: Times New Roman; font-size: 20px; font-weight:bold;" width="20%"><strong>&nbsp;<?php //echo $typeno;?></strong></td>
	    	</tr>
	    </table>-->
	    
	    <div class="container" style="text-align: right; height: 120px; margin-top:0px; padding-top:24px;">
			<?php

	    		$have_img = base_url()."assets/upload/students/".$acandemic.'/'.$studentcode.'.jpg';
				$no_imgs = base_url()."assets/upload/students/NoImage.png";							
				$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:90px;height:100px;">';						
				if (file_exists(FCPATH . "assets/upload/students/".$acandemic.'/'.$studentcode.'.jpg')) {				
					$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:90px;height:100px;">';
				}

				echo $img;

    		?>	
	    		
	    </div>
	   	<div class="container">
	   		<div id="inv_no">វិក្កយបត្រ / INVOICE</div><br>
	    	<div id="com-name-en"></div>
	   	</div>
            <table border="0" style="width:100% !important">
            	<tr>
	                <td style="width:60% !important">
	                    <table  border="0" class="tbl-address" width="100%" cellpadding="0" cellspacing="0">
	                        <tr>
	                            <td><div style="padding-left: 3px;padding-top:0px;">វិក្កយបត្រ&nbsp;/&nbsp;<span>Invoice Nº</span></div></td>
	                            <td>:</td>
	                            <td>
	                                <div style="padding-left: 3px;padding-top:0px; font-size:18px !important" class="inv_info"><strong>&nbsp;<?php echo $typeno?></strong></div>                                    
	                            </td>
	                        </tr>
	                        
	                        <tr>
	                            <td><div style="padding-left: 3px;padding-top:0px;">អត្តលេខសិស្ស&nbsp;/&nbsp;<span>Student&nbsp;ID</span></div></td>
	                            <td>:</td>
	                            <td>
	                                <div style="padding-left: 3px;padding-top:0px;" class="inv_info"><strong>&nbsp;<?php echo $studentid;?></strong></div>                                    
	                            </td>
	                        </tr>
	                        <tr>
	                            <td><div style="padding-left: 3px;padding-top:0px;">ឈ្មោះ&nbsp;/&nbsp;<span>Name</span></div></td>
	                            <td>:</td>
	                            <td>
	                                <div style="padding-left: 3px;padding-top:0px;" class="inv_info"><strong>&nbsp;<?php  echo $fullname;?></strong></div>                                    
	                            </td>
	                        </tr>
	                         <tr>
	                            <td><div style="padding-left: 3px;padding-top:0px;">ភេទ&nbsp;/&nbsp;<span>Gender</span></div></td>
	                            <td>:</td>
	                            <td>
	                                <div style="padding-left: 3px;padding-top:0px;" class="inv_info"><strong>&nbsp;<?php echo ucfirst($gender);?></strong></div>                                    
	                            </td>
	                        </tr>
	                        <tr>
	                            <td><div style="padding-left: 3px;padding-top:0px;">ទូរស័ព្ទ&nbsp;/&nbsp;<span>Phone</span></div></td>
	                            <td>:</td>
	                            <td>
	                                <div style="padding-left: 3px;padding-top:0px;" class="inv_info"><strong>&nbsp;<?php  echo $phone1;?></strong></div>                                    
	                            </td>
	                        </tr>
	                         <tr>
	                            <td><div style="padding-left: 3px;padding-top:0px;">ឆ្នាំសិក្សា&nbsp;/&nbsp;<span>Acandemic&nbsp;Year</span></div></td>
	                            <td>:</td>
	                            <td>
	                                <div style="padding-left: 3px;padding-top:0px;" class="inv_info"><strong>&nbsp;<?php  echo $acandemicname;?></strong></div>                                    
	                            </td>
	                        </tr>
	                   </table>
	            	</td>
	                <td style="width:5% !important">&nbsp;</td>
	                <td style="width:45% !important">
		   				<table  border="0" class="tbl-address" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><div style="padding-left: 3px;padding-top:0px;">កាលបរិច្ឆេទ&nbsp;/&nbsp;<span>Date</span></div></td>
                                <td>:</td>
                                <td>
                                    <div style="padding-left: 3px;padding-top:0px;" class="inv_info"><strong>&nbsp;<?php echo $trandate;?></strong></div>                                    
                                </td>
                            </tr>
                            <tr>
                                <td><div style="padding-left: 3px;padding-top:0px;">សំរាប់&nbsp;/&nbsp;<span>For </span></div></td>
                                <td>:</td>
                                <td>
                                    <div style="padding-left: 3px;padding-top:0px;" class="inv_info"><strong>&nbsp;<?php echo $period;?></strong></div>                                    
                                </td>
                            </tr>
                             <tr>
                                <td style="text-align:left !important">  ចាប់ពីថ្ងៃទី/From</td>
                                <td>:</td>
                                <td>
                                	<table class="fro_to">
                                    	<tr>
                                             <td style="text-align:left !important"></td>
											 <td style="width:100% !important; text-align:left !important;font-family: Khmer OS battambang;font-size: 12px !important;" ><strong><?php echo $start_date;?></strong></td> 
                                             <td>ដល់/To</td>
                                             <td style="width:100% !important; text-align:left !important;font-family: Khmer OS battambang;font-size: 12px !important;"><strong>&nbsp;<?php echo $end_date;?></strong></td>
                               
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><div style="padding-left: 3px;padding-top:0px;">ថ្នាក់&nbsp;/&nbsp;<span>&nbsp;&nbsp;Class</span></div></td>
                                <td>:</td>
                                <td>
                                    <div style="padding-left: 3px;padding-top:0px;" class="inv_info"><strong>&nbsp;<?php echo $rangelevelname;?></strong></div>                                    
                                </td>
                            </tr>
                            <tr>
                                <td><div style="padding-left: 3px;padding-top:0px;">ពេល​&nbsp;/&nbsp;<span>Time</span></div></td>
                                <td>:</td>
                                <td><?php  echo $from_to_time;?></td>
                            </tr>
                            <tr>
                                <td><div style="padding-left: 3px;padding-top:0px;">ថ្ងៃកំណត់ទូទាត់&nbsp;/&nbsp;<span>Due&nbsp;Date</span></div></td>
                                <td>:</td>
                                <td>
                                    <div style="padding-left: 3px;padding-top:0px;" class="inv_info"><strong>&nbsp;<?php  echo $duedate;?></strong></div>                                    
                                </td>
                            </tr>
	                   </table>
	                </td>
                </tr>
            </table>
	   	<div class="container marging-toppx border-dashed">
	   		<table class="tbl-items" width="100%" cellspacing="0" cellpadding="0">
	   			<thead>
	   				<tr>
	   					<th width="5%">ល.រ<br /><span>Nº</span></th>
	   					<th>បរិយាយ<br /><span>Description</span></th>
	   					<th style="width:80px;">បរិមាណ<br /><span>Quantity</span></th>
	   					<th>ថ្លៃឯកតា<br /><span>Unit Price</span></th>
	   					<th style="display:none-">បញ្ចុះថ្លៃ<br /><span>Discount</span></th>
	   					<th>ថ្លៃទំនិញ<br /><span>Amount</span></th>
	   				</tr>
	   			</thead>
	   			<tbody>
		   			<?php 
		   				echo $trinv;
		   			?>
	   				<tr>
	   					<td colspan="4" rowspan="3" style="border-bottom:none; border-left:none" valign="top">
	                        <div class="note" style="font-size:13px !important"><strong>បញ្ជាក់:</strong><br> 
	                            -សាលាសូមកិច្ចសហការពីមាតាបិតា អាណាព្យាបាលក្នុងការទូទាត់ថ្លៃសិក្សាអោយបានមុន ឬទាន់ពេលវេលា។<br>
	                            (Thank you for your cooperation on the payment for fees above ontime)<br>
	                            -ប្រាក់ដែលបានបង់រួចហើយ មិនអាចដកវិញ ឬបង្វិលទៅអោយអ្នកផ្សេងបានទេ។<br>
								(The payment is non-returnable or non-transferable)

	                           <div class="notepaid"><?php  echo $notepaid?></div>
	                        </div>
                        </td>
                        <td colspan="1" align="right" class="add-line-height">ប្រាក់សរុប<br /><span>Sub Total</span></td>
	   					<td align="right" class="tfoot_amt"><?php echo number_format($amt_total,2).'&nbsp;$'?>&nbsp;</td>
	   				</tr>
	   				<tr style="display:none-">
	   					<td colspan="1" align="right" class="add-line-height" style="width:138px !important;"><span style="font-size:12px !important">ប្រាក់កក់​<?php // echo $typetaxrate;?></span><br /><span>Deposit<?php // echo $typetaxrate;?></span></td>
	   					<td align="right" class="tfoot_amt"><?php echo number_format(($amt_paid),2).'&nbsp;$'?>&nbsp;</td>
	   				</tr>
	   				<tr style="display:none-">
	   					<td colspan="1" align="right" class="add-line-height">ប្រាក់សរុបរួម<br /><span>Grand Total</span></td>
	   					<td align="right" class="tfoot_amt"><?php echo number_format(($amt_balance),2).'&nbsp;$'?>&nbsp;</td>
	   				</tr>
	   			</tbody>
	   		</table>
	   	</div>
	   	<div class="container" style="height: 65px"></div>
	   	<div class="container">
	   		<table class="tbl-address" width="100%" cellpadding="0" cellspacing="0">
	   			<tr>
	   				<td class="font-bold" align="left">
	   					<div style="border:1px solid #000;width: 250px;"></div>
	   					ប្រធានរដ្ឋបាល&nbsp;/&nbsp;Head of Administration
	   				</td>
                    <td width="40%"></td>
	   				<!--<td align="center" class="font-bold" style="display:none">
	   					<div style="border:1px solid #000;width: 190px;"></div>
	   					ហត្ថលេខា​ ឈ្មោះអ្នកដឹក<br /><span>Delivere's Signature & Name</span>
	   				</td>
	   				<td width="20%"></td>
	   				<td align="center" class="font-bold">
	   					<div style="border:1px solid #000;width: 190px;"></div>
	   					ហត្ថលេខា​ ឈ្មោះអ្នកលក់<br /><span>Seller's Signature & Name</span>
	   				</td>-->
	   			</tr>
	   		</table>
	   	</div>
	   	<!-- <div class="note-footer">
	   		<strong>សម្គាល់៖</strong> ច្បាប់ដើមសម្រាប់អ្នកទិញ ច្បាប់ចម្លងសម្រាប់អ្នកលក់<br />
	   		<span>Original invoice for customer, copied invoice for seller</span>
	   	</div> -->
  	</div>

</body>

</html>

 <!-- start new dialog  -->
     
<?php 
	$photodel="<img src='".site_url('../assets/images/icons/delete.png')."'/>";
	$addnew="<img src='".site_url('../assets/images/icons/add.png')."'/>";
?>  
     
<script type="text/javascript">

</script>
	
		
	
