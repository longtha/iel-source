<?php
   //$cars=array("Volvo","BMW","Toyota");
   $relation=array("Legal Student","father`s child","mother`s child","brother/sister","Cousin","niece/nephew");
   $social_status=array("Normal","alcohol","violent","Mental","Drug");
   $Health=array("Normal","Handicap 50%","Handicap 100%");
   $civil_status=array("Single","Married","Divorce/widow");
   $m='';
   $p='';
   if(isset($_GET['m'])){
      $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }
    
  // schoolinfor
    if(isset($schooinfor) && count($schooinfor)>0){
      $infor="";
      $infor="<option attr_price='0' value=''></option>";
      foreach($schooinfor as $for){
         $infor.="<option value='".$for->schoolid."'>".$for->name."</option>";
      }
    }
    
   // Program
    if(isset($getprogram) && count($getprogram)>0){
        $opprogram="";
        $opprogram="<option attr_price='0' value=''></option>";
        foreach($getprogram as $rowpro){
            $opprogram.="<option value='".$rowpro->programid."'>".$rowpro->program."</option>";
        }
        
    }
    
?>

<style type="text/css">
   ul,ol{
      margin-bottom: 0px !important;
   }
   a{
      cursor: pointer;
   }
   table tbody tr td img{width: 20px; margin-right: 10px}
   .datepicker {z-index: 9999;}
   .lihide{ line-height:35.3px !important;}
   .lihidephoto{ line-height:25px !important; color:#00F !important}
   .lihideinfo{ line-height:30px !important;}
   .fiel_head{ font-size:10px !important; line-height:25.3px !important;}
   #amt_balance,#atm_total,#amt_dis{ 
   color:#900 !important;
   font-size:17px !important;
   font-weight:bold; 
   
   }
  #amt_balance,#atm_total,#amt_dis,#amt_total,#amt_deposit,#amt_deposited{ 
    color:#900 !important;
    font-size:17px !important;
    font-weight:bold;   
    
    }
    
#op_payment{
background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 2px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #555;
    display: block;
    font-size: 12px;
    height: 30px;
    line-height: 1.42857;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;
}
    
.payment-amt{ 
   color:#906 !important;
   font-size:14px !important;
   font-weight:bold; 
   
   }
   #mdinvoice .modal-dialog  {width:99% !important;}
   .paymentlist{ cursor:pointer;}
   
   table tbody tr td img{width: 20px; margin-right: 10px}
    a,.sort {
        cursor: pointer;
    }
    .cur_sort_up{
        background: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') no-repeat !important;
        background-position: left center  !important;
        text-align: center;
    }
    #top-bar img{width: 20px; margin-top: 5px;}

    .cur_sort_down{
        background: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') no-repeat !important;
        background-position: left center  !important;
        text-align: center;
    }
	
.t_studentcode{
    background-color: #239d60;
    background-image: none;
    border: 1px solid #e5e5e5;
    box-shadow: none;
    color: #fff;
    left: 7px;
    position: relative;
    top: 7px;
	cursor:pointer;
	
	border-radius: 10px;
    font-size: 12px;
    font-weight: normal;
    line-height: 18px;
    padding: 3.6px 6px;
	font-weight:bold;
}
.t_studentcode:hover{
	background-color:#F00;
	color:#000;
	cursor:pointer;
	font-weight:bold;
	
}
.void_inv{
    background-color: #F00;
    background-image: none;
    border: 1px solid #e5e5e5;
    box-shadow: none;
    color: #fff;
    left: 7px;
    position: relative;
    top: 7px;
	cursor:pointer;
	
	border-radius: 10px;
    font-size: 12px;
    font-weight: normal;
    line-height: 18px;
    padding: 3.6px 6px;
	font-weight:bold;
}
.void_inv:hover{
	background-color:#239d60;
	color:#F00;
	cursor:pointer;
	font-weight:bold;
	
}
.toamt{ font-size:14px; color:#00F; font-weight:bold; text-align:right}
.tr_row{ color:#F00 !important}
</style>


<div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
              <span class="icon">
                  <i class="fa fa-th"></i>
              </span>
                <strong>Create Receipt</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add New">
                  <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
               </a>
               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
               </a>
               <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
               </a>           
               
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>
 <div class="collapse" id="collapseExample" style="display:none-"> 
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? //= site_url('school/semester/save') ?>" id="f_save">
         <input type="hidden" name="semesterid" id="semesterid">
         
         <div class="col-sm-4">         
            <div class="form-group">
               <span id="sp_semester" style="display: none;">&nbsp;</span>
               <label for="invoice">INV</label>
               <input type="text" name="invoice" id="invoice" class="form-control" placeholder="Search..."  tabindex="1">
            </div>

            <div class="form-group">
               <label for="studenid">Student Id</label>
               <input type="text" name="studen_id" id="studen_id" class="form-control" placeholder="Search..."  tabindex="1">
            </div>

            <div class="form-group">
               <label for="studentname">Student Name</label>
               <input type="text" name="studentname" id="studentname" class="form-control" placeholder="Search..." tabindex="1">
            </div>

            <div class="form-group">
               <label for="schoolinfor">School Infor</label>
               <select name="schoolinfor" id="schoolinfor" class="form-control"  tabindex="5">
                  <?php echo $infor?>
               </select>
            </div>
            
         </div><!-- 1 -->

         <div class="col-sm-4">

            <div class="form-group">
               <label for="program">Program</label>
               <select name="program" id="program" class="form-control" tabindex="5">
                    <?php echo $opprogram?>
               </select>
            </div>

             <div class="form-group">
               <label for="schoollavel">School Lavel</label>
               <select name="schoollavel" id="schoollavel" class="form-control"  tabindex="5">
                  
               </select>
            </div>

            <div class="form-group">
               <label for="yeae">Year</span></label>
               <select name="yeae" id="yeae" class="form-control"  tabindex="5">
                  
               </select>
            </div>

            <div class="form-group">
               <label for="classname">Class</span></label>
               <select name="classname" id="classname" class="form-control"  tabindex="5">
                  
               </select>
            </div>      
            
         </div><!-- 2 -->

         <div class="col-sm-4">
            <div class="form-group">
               <label for="ranglavel">Rang Lavel</span></label>
               <select name="ranglavel" id="ranglavel" class="form-control"  tabindex="5">
                  
               </select>
            </div>
            
            <div class="form-group">
                <label for="formdate">From Date</label>
                <div class="input-group">
                  <input type="text" name="formdate" id="formdate" class="form-control" placeholder="dd/mm/yyyy">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label for="todate">To Date</label>
                <div class="input-group">
                  <input type="text" name="todate" id="todate" class="form-control" placeholder="dd/mm/yyyy">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
            </div>

            <div class="form-group">
               <label for="status">Status</label>
               <select name="status" id="status" class="form-control" tabindex="6">
                     <option value="">--All--</option>
                     <option value="0">Pending</option>
                     <option value="2">Partial</option>
                              
               </select>
            </div>

         </div><!-- 3 -->      

         <div class="col-sm-7 col-sm-offset-4">
            <div class="form-group">
               <button type="button" class="btn btn-primary btn-sm " name="tbnserch" id="tbn_serch" value="serch" tabindex="11" data-save='2'>Search</button>
               
              
            </div>            
         </div>

      </form>
   </div>

<div class="col-xs-12">
   <div class="panel panel-default">
         <div class="table-responsive" id="div_export_print">
                <div id="tab_print">
                     <table border="0" class="table table-hover" id="setBorderTbl"  cellspacing="0" cellpadding="0">
                         <thead>
                          <tr>
                              <?php
                                 foreach ($thead as $th => $val) {
                                    if ($th == 'Status'){
                                       	echo "<th align='center' class='remove_tag' width='5%' colspan='2' style='text-align:center'>" . $th . "</th>";
                  					}else if ($th == 'No'){
                                        echo "<th class='$val'>".$th."</th>";
                  					}else if($th == 'Photo'){
										echo"<th class='$val' width='8%'>" . $th . "</th>";
									}else{
                                    	echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
									  }
                                 }
                              ?>
                           </tr>
                         
                         </thead>
                        
                         <tbody id='listrespon'>
                         </tbody>
                     </table>
                 </div>
             </div>


              <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
                Display : <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                                <?php
                                    $num=10;
                                    for($i=0;$i<10;$i++){?>
                                        <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                        <?php $num+=20;
                                    }
                                ?>
                          </select>
            </div>
            <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
                <div class="pagination" id="pagination" style="text-align:center"></div>
            </div>

      </div>
</div>

 <!-- start new dialog  -->
     
<?php 
   $photodel="<img src='".site_url('../assets/images/icons/delete.png')."'/>";
   $addnew="<img src='".site_url('../assets/images/icons/add.png')."'/>";
   
?>      
<div class="modal fade" id="mdinvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="wrapper">
				<div class="clearfix" id="main_content_outer">
				    <div id="main_content">
					    <div class="result_info">
					    	<div class="col-sm-6">
                                <span class="icon">
                                <i class="fa fa-th"></i>
                                </span>
					      		<strong>Create Receipt</strong>
					      	</div>
					      	<div class="col-sm-6" style="text-align: center">
					      		<strong>
					      			<center class='visit_error' style='color:red;'></center>
					      		</strong>	
					      	</div>
					    </div>
                        <div class="col-xs-12" style="border:#CCC solid 1px;margin:0px;height:100%;">
                              <div class="col-sm-2 img-circle img-responsive photostu" style="border:#e7e7e7 solid 0px; padding:3px; text-align:center !important">
								
                             </div>
                              
                             <div class="col-sm-3" style="height:100% ;border-right:#CCC solid 1px;align-items: stretch;">
                                <table style="width:100% !important"> 
                                    <tr class="lihideinfo">
                                        <td>ID
                                        	<input type="hidden" class="h_typeno" id="h_typeno" />
                                        </td><td>:</td><td><span class="stuid" style="text-align:center !important; display:none"></span><span class="student_num" style="text-align:center !important"></span></td>
                                    </tr>
                                    <tr class="lihideinfo">
                                        <td>Full Name(Kh)</td><td>:</td><td><span class="namekh"></span></td>
                                    </tr>
                                    <tr class="lihideinfo">
                                        <td>Full Name(Eng)</td><td>:</td><td> <span class="nameeng" style="text-align:center !important"></span></td>
                                    </tr>
                                    
                                    <tr class="lihideinfo">
                                        <td>Gender</td><td>:</td><td><span class="sex"></span></td>
                                    </tr>
                                    <tr class="lihideinfo">
                                        <td>Address</td><td>:</td><td><span class="address"></span></td>
                                    </tr>
                                    
                                    <tr class="lihideinfo">
                                        <td>Telephone </td><td>:</td><td><span class="phone"></span></td>
                                    </tr>
                                   
                                </table>    
                              </div>
                                    
                              <div class="col-sm-4" style="height:100% ;border-right:#CCC solid 1px;align-items: stretch;">
                                <table style="width:100% !important" class="gform"> 
                                    <tr class="lihide">
                                        <td>Program</td><td>:</td>
                                         <td class="program">

                                         <td>
                                    </tr>
                                    
                                    <tr class="lihide">
                                        <td>School&nbsp;Level</td><td>:</td>
                                        <td class="schoollavel">

                                       </td>
                                    </tr>
                                   <tr class="lihide">
                                        <td>Academic&nbsp;Year</td><td>:</td>
                                         <td class="year">

                                         </td>
                                    </tr>
                                    
                                    <tr class="lihide">
                                        <td>Rang&nbsp;Level</td><td>:</td>
                                        <td class="ranglavel">

                                       </td>
                                    </tr>
                                    <tr class="lihide">
                                        <td>Class</td><td>:</td>
                                        <td class="class">

                                        </td>
                                    </tr>
                                </table>
                             </div>
                                   
                              <div class="col-sm-3" style="height:100% ;align-items: stretch;">
                                  <table style="width:100% !important" class=""> 
                                           
                                    <tr class="lihide gform">
                                        <td>Payment&nbsp;Method</td><td>:</td>
                                        <td class="paymethod">

                                        </td>
                                    </tr>
                                    <tr class="lihide gform">
                                        <td>Payment&nbsp;Type</td><td>:</td>
                                        <td class="paymenttype"> 

                                        </td>
                                    </tr>
                                    
                                    <tr class="lihide gform">
                                        <td>Area</td><td>:</td>
                                        <td class="area">

                                        </td>
                                        
                                    </tr>
                                   <tr>
                                        <td>Bus No</td><td>:</td>
                                        <td class="bus">

                                        </td>
                                    </tr>
                                    
                                    <tr class="lihide gform" style="display:none">
                                        <td>Due Date</td><td>:</td>
                                       <td class="date">
                                          <div class="input-group" data-date-format="dd-mm-yyyy">
                                          <!-- 
                                          <input id="duedate" class="form-control duedate" type="text" placeholder="dd-mm-yyyy" name="duedate">
                                          <span class="input-group-addon"> -->
                                          <!-- <span class="glyphicon glyphicon-calendar"> </span> -->
                                          </span>
                                          </div>
                                      </td>
                                      </tr>
                                      <tr>
                                                        <td colspan="1" align="">Tran&nbsp;Date&nbsp;&nbsp;</td>
                                                        <td>:</td>
                                                        <td>
                                                       
                                                        <div class="input-group" data-date-format="dd-mm-yyyy">
                                                             <input  value="<?php echo date('d-m-Y'); ?>" id="trandate" class="form-control trandate" type="text" placeholder="dd-mm-yyyy" name="duedate" style="text-align:right"> 
                                                            <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"> </span>
                                                            </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                    
                                </table>
                             </div>
                     </div>
                    <div class="col-xs-12 table-responsive">
                        <table style="width:100%" align="center " border="0" class="table  table-striped table-hover">
                            <thead>
                                 <tr class="table">
                                    <th width="30%" style="text-align:left !important">Description</th>
                                    <th width="20%"  style="text-align:center !important">Note</th>
                                    <th width="10%"  style="text-align:center !important">Qty</th>
                                    <th width="10%"  style="text-align:center !important">Unit Price</th>
                                    <th width="10%"  style="text-align:center !important">Dis(%)</th>
                                    <th width="10%" style="text-align:center !important">Amount</th>
                                    
                                </tr>
                                
                              </thead>
                              <tbody class="tbody_paymenttype">
                              </tbody> 
                            <tfoot>
                                
                                <tr>
                                    <td  rowspan="4" colspan="4"align="right">
                                    	
                                        <textarea id="text_note" class="text_note" placeholder="Add Note"></textarea>
                                        
                                        
                                    </td>
                                    <td rowspan="1" colspan="1" align="right">Total &nbsp;:&nbsp;</td>
                                    <td>
                                        <input id="amt_total" readonly="readonly" class="form-control amt_total" type="text" placeholder=""  value="0"  style="text-align:right">
                                        
                                    </td>
                                    
                                </tr> 
                                 <tr>
                                    <td colspan="1" align="right">Deposited&nbsp;:&nbsp;</td>
                                    <td>
                                        <input id="amt_deposited" readonly="readonly" onkeypress="return isNumberKey(event);" class="form-control amt_deposited" type="text" placeholder=""  value="" style="text-align:right">
                                        
                                    </td>
                                    
                                </tr> 
                                 <tr>
                                    <td colspan="1" align="right">Deposit&nbsp;:&nbsp;</td>
                                    <td>
                                        <input id="amt_deposit" onkeypress="return isNumberKey(event);" class="form-control amt_deposit" type="text" placeholder=""  value="0" style="text-align:right">
                                        
                                    </td>
                                    
                                </tr> 
                                
                                <tr>
                                    <td colspan="1" align="right">Balance&nbsp;:&nbsp;</td>
                                    <td>
                                        <input  readonly="readonly" id="amt_balance" class="form-control amt_balance" type="text" placeholder=""  value="0"  style="text-align:right">
                                        
                                    </td>
                                </tr>
                                
                            </tfoot>
                     </table>
                  </div>
               </div> 
             </div>
         </div> 
            <div class="modal-footer">       
                <input data-dismiss="modal" id="btnsaves" class="btn btn-primary" type="button" value="Save" name="btnsaves"/>
                 <input data-dismiss="modal" id="btncancel" class="btn btn-warning" type="button" value="Cancel" name="btncancel"/>
            </div>
        </div>                       <!-- /.modal-content -->
    </div>                     <!-- /.modal-dialog -->
</div> 
<!--/dialog -->
 <div class="modal" id="dialog_print" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="wrapper">
                <div class="clearfix" id="main_content_outer">
                    <div id="main_content">
                        <div class="result_info">
                            <div class="col-sm-6">
                            	<span class="icon">
                                    <i class="fa fa-th"></i>
                                </span>
                                <strong>Print Report</strong>
                            </div>
                        </div>
                        <div class="col-xs-12" style="border:#CCC solid 1px;margin:0px;height:100%;">
                            <table style="text-align:center" align="center">
                                <tr style="line-height:40px;"  class="printreceipt">
                                    <td>
                                        <a  target="_blank" href="" class="print_p2">preview Receipt</a>
                                    </td>
                                </tr>
                            </table>                       
                        </div>
                           
                    </div> 
                </div>
            </div> 
           <div class="modal-footer">     
              <button  data-dismiss="modal" id="btn_cancel" class="btn btn-warning" type="button" name="btncancel">Cancel</button>
              <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close -->
            </div>
        </div>                       <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
</div> 
<span data-toggle="modal" data-target="#dialog_print" class="clprint" ></span>

<!-- dialog -->
<div class="modal fade bs-example-modal-lg" title="Export" id="exporttap" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="wrapper">
                <div class="clearfix" id="main_content_outer">
                    <div id="main_content">
                        <div class="result_info">
                            <div class="col-sm-6">
                                <strong>Choose Column To Export</strong>
                            </div>
                            <div class="col-sm-6" style="text-align: center">
                                <strong>
                                    <center class='visit_error' style='color:red;'></center>
                                </strong>
                            </div>
                        </div>
                        <form enctype="multipart/form-data" name="frmvisit" id="frmvisit" method="POST">
                                <div class="row">
                                    <div class="col-sm-12">
                                            <div class="panel-body">
                                                <div class='table-responsive'>
                                                       <table class='table'>
                                                            <thead >
                                                                <?php
                                                                foreach($thead as $th=>$val){
                                                                    if($th!='Status')
                                                                        echo "<th class='remove_tag'>".$th."</th>";    
                                                                }?>
                                                            </thead>
                                                            <tbody id="class_body">
                                                                <?php
                                                                foreach($thead as $th=>$val){
                                                                    if($th!='Status')
                                                                        echo "<td align='center'><input type='checkbox' checked class='colch' rel='".$val."'></td>";    
                                                                }?>
                                                            </tbody>
                                                       </table>
                                               </div>
                                            </div>
                                    </div> 
                                </div>
                          </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id='btnprint' class="btn btn-primary">Print</button>
                <button type="button" id='btnexport' class="btn btn-primary">Export</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 
<script type="text/javascript">
$("#year,#schlevelid").hide();
$('.year').val($('#year').val());
is_leave();
$('#year').change(function(){
  $('.year').val($('#year').val());
});

 function isNumberKey(evt){
        var e = window.event || evt; // for trans-browser compatibility 
        var charCode = e.which || e.keyCode; 
        if ((charCode > 45 && charCode < 58) || charCode == 8){ 
            return true; 
        } 
        return false;  
        }
  function fillusername(event){
    var f_name=$('#first_name').val();
    var l_name=$('#last_name').val();
    var username=f_name+'.'+l_name;
          $.ajax({
                            url:"<?php echo base_url(); ?>index.php/student/student/validateuser",    
                            data: {'username':username},
                            type:"post",
                            success: function(data){
                           if(data>0){
                              $('#login_username').val(username+'1');
                           }else{
                              $('#login_username').val(username);
                           }
                        }
                    });
  }

  function is_leave(event){
    if($("#leave_school").is(':checked')){
      $('.leave').removeClass('hide');

    }else{
      $('.leave').addClass('hide');
    }
  }

  function previewstudent(event){
        var year=$(event.target).attr('year');
        var yearid=$(event.target).attr('yearid');
        var student_id=jQuery(event.target).attr("rel");
        window.open("<?PHP echo site_url('student/student/preview');?>/"+student_id+"?yearid="+yearid,"_blank");
  }

  function isvtc(event){
    var classid=$(event.target).val();
    $.ajax({
                    url:"<?php echo base_url(); ?>index.php/student/student/getschlevel",    
                    data: {'classid':classid},
                    type:"post",
                    success: function(data){
                    if(data==1){
                      $('#promo_sep').removeClass('hide');
                      var familyid=$('#familyid').val();
                      if(familyid!='')
                        fillstudent(familyid);
                      
                    }else{
                      $('#promo_sep').addClass('hide');
                    }
                }
            });
  }
  
  function fillpwd(event){
    var pwd=$('#student_num').val();
    var student_num=$(event.target).val();
    //alert(student_num);
    $.ajax({
            url:"<?php echo base_url(); ?>index.php/student/student/getstdbyid",    
            data: {'std_num':student_num},
            type:"post",
            success: function(data){
            if(data==1){
              $('.error').html('Student ID :'+ student_num+' is already used please choose other ID');
              $('#student_num').val(''); 
            }else{
              $('#password').val(pwd);
              $('.error').html('');
            }
        }
    });
  }
  //===================================================
  $(function(){
    var sort_num = $("#sort_num").val();

    Fsearch(1,$('#sort_num').val());
  // sort_num ==============================
    $('#sort_num').change(function(){
        // alert($(this).val());
        var  total_display= $(this).val();
        Fsearch(1,total_display);
    });

 // pagenav ===============================
    $("body").delegate(".pagenav","click",function(){
        var page = $(this).attr("id");
        var total_display = $('#sort_num').val();
        Fsearch(page,total_display);
    });

    // $('#collapseExample').collapse('toggle')
    $("#formdate,#todate").datepicker({
        language: 'en',
        pick12HourFormat: true,
        format:'dd-mm-yyyy'
    });

    $("#sechdate").datepicker({ 
      format:'dd-mm-yyyy'
    }).on('changeDate',function(ev){ 
      //alert($(this).val());
        Fsearch();
    });
    $("#search_date").datepicker({ 
      format:'dd-mm-yyyy'
    }).on('changeDate',function(ev){ 
      //alert($(this).val());
        Fsearch();
    });
    $('#std_register').parsley();
    autofillfamily();
    $(".res_dob,.mem_dob,#leave_sch_date,#boarding_date,#trandate,.input_validate,#duedate").datepicker({
          language: 'en',
          pick12HourFormat: true,
         // format:'yyyy-mm-dd'
		 format:'dd-mm-yyyy'
      });
    
    $('body').delegate('.t_studentcode','click',function(){
		FClear();
		var typeno = $(this).attr('attr_typeno');
		var mes=$(this).attr('attr_id');
		$(".h_typeno").val(typeno);
		FgetRowStuinfo(mes,typeno);
		$(".tr_addrow").remove();
    });
	$('body').delegate('.void_inv','click',function(){
		if(confirm("Are you sure want to void this invoice")){
			var typeno = $(this).attr('attr_typeno');
			FVoid(typeno);
		}
    });
    $(".dl_edit_studentid").on('click', function() {
      var mes=$(this).attr('id');
      var typeno=$(this).attr('attr_typeno');
      
      FgetRowStuinfo(mes,typeno);
      
    });
    
   
    $("body").delegate(".payment-amt,.payment-unitprice,.payment-dis","keyup",function(){
      //alert(2);
      Fcalpayment();
    });
    $("body").delegate(".paymenttype_list","change",function(){
      // var me =$(this).find('.paymenttype_list option:selected').val();
      // var me =$(this).find('option:selected').val();
      var me =$(this).find('option:selected').text();
      var attr_price =$(this).find('option:selected').attr('attr_price')-0;
      $(this).parent().parent().find('.payment-unitprice').val(attr_price);
      $(this).parent().parent().find('.payment-des').val(me);
    });
    $('body').delegate("a#a_delete", "click", function() {
      // if ($(".table1 tbody.tbody_sale tr").size() == 1) return;
      
      if(confirm('Are you sure remove?','Remove Item fee!')){
        $(this).parents("tr:first").remove();
        Fcalpayment();
      }
    });
    
    $("#btnsaves").on('click', function() {
      var  amt_deposit=$(".amt_deposit").val()-0;
      if(amt_deposit==0){
        alert("Deposit Can note = 0");
        $(".amt_deposit").select();
        return false;
      }else{
         FSavePayment();
      }
     
    });
    $("#btn_cancel").on('click', function() {
     // alert();
        // $('#dialog_print').hide();
    });

    $("#opprpid").on('change', function() {
      var me=$(this).val();
      FvgetOp(me);
    });
    
    $("#shlevelid").on('change', function() {
      Fvshlev();
    });
    $(".oparea").on('change', function() {
      // alert($(this).val());
      Fvbusfee();
    });
    $("#opbusfeetype").on('change', function() {
      FVcalarea();
    });
    $("#oppaymentmethod").on('change', function() {
      var me=$(this).val();
      // alert(me);
      FVpaymentType();
    });
    $("#paymenttype,#ranglevid").on('change', function() {
      FVpaymentTypepice();
    });
    $("#descount").on('keyup',function(){
      totalbalan();
    });
    $("body").delegate(".payment-amt,.payment-unitprice,.payment-dis").on('keyup', function() {
            Fcalpayment();
            
    });
    $("#program").on('change',function(){
        // alert($(this).val());
        get_schlevel($(this).val());
    });

    $("#schoollavel").on('change',function(){
        // alert($(this).val());
        get_schyera($(this).val());
        get_schclass($(this).val());
    });
    $("#yeae").on('change',function(){
        //alert($(this).val());
       get_schranglavel($(this).val());
    });
    // tbnserch ======================================
    $('body').delegate('#tbn_serch', 'click', function(){
         Fsearch();
    });
    // addnew serch===================================
    $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
          FClear();
         
      });
      // refresh ======================================
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });
      //chos colum ============================================
       $('#a_export').click(function () {
            $('#exporttap').modal('show');
            $('#btnprint').hide();
            $('#btnexport').show();
        });
    
        $('#a_print').click(function () {
            $('#exporttap').modal('show');
            $('#btnprint').show();
            $('#btnexport').hide();
        });
        // printter =============================================
        $("#btnprint").on("click", function () {
            var htmlToPrint = '' +
                '<style type="text/css">' +
                'table th, table td {' +
                'border:1px solid #000 !important;' +
                'padding;0.5em;' +
                '}' +
                '</style>';
            var title = "Create Receipt";
            var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "");
            var export_data = $("<center>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
            export_data += htmlToPrint;
            gsPrint(title, export_data);
        });
        // exsport=================================================
        $("#btnexport").on("click", function (e) {
            var title = "Create Receipt";
            var data = $('.table').attr('border', 1);
            data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
            var export_data = $("<center><h3 align='center'>" + title + "</h3>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
            e.preventDefault();
            $('.table').attr('border', 0);
        });
        // colch ================================================
        $('.colch').click(function(){
            var col=$(this);
            if(col.is(':checked')){
                $('.'+col.attr('rel')).removeClass('remove_tag');
            }else{
                $('.'+col.attr('rel')).addClass('remove_tag');
            }
        });
 });// function ===================================================================================
  
  function gsPrint(emp_title, data) {
      var element = "<div>" + data + "</div>";
      $("<center><p style='padding-top:15px;text-align:center;'><b>" + emp_title + "</b></p><hr>" + element + "</center>").printArea({
          mode: "popup",  //printable window is either iframe or browser popup
          popHt: 600,  // popup window height
          popWd: 500,  // popup window width
          popX: 0,  // popup window screen X position
          popY: 0, //popup window screen Y position
          popTitle: "test", // popup window title element
          popClose: false,  // popup window close after printing
          strict: false
      });
  }
  // get school level ==================================================
  function get_schlevel(program){
     $.ajax({        
        url: "<?= site_url('student_fee/c_student_fee_receipt/get_schlevel') ?>",    
        data: {         
          'program':program
        },
        type: "post",
        dataType: "json",
        // async:false,
        success: function(data){ 

            var opt = '';
                opt="<option  value=''>All</option>";
            if(data.schlevel.length > 0){
                $.each(data.schlevel, function(i, row){
                    //
                    opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                });
            }else{
                opt += '';
            }

            $('#schoollavel').html(opt);
            get_schyera($('#program').val(), $('#schoollavel').val());
            get_schclass($('#program').val(), $('#classname').val());
       }
     });
  }
//get yare==============================================================
  function get_schyera(schoollavel){
    $.ajax({        
        url: "<?= site_url('student_fee/c_student_fee_receipt/get_schyera') ?>",    
        data: {         
          'schoollavel':schoollavel
        },
        type: "post",
        dataType: "json",
        // async:false,
        success: function(data){ 
            var ya = '';
                ya="<option  value=''>All</option>";
            if(data.schyear.length > 0){
                $.each(data.schyear, function(i, row){
                    ya += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                });
            }else{
                ya += '';
            }

            $('#yeae').html(ya);
            get_schranglavel($('#yeae').val(), $('#ranglavel').val());
        //    get_schranglavel($('#yeae').val(), $('#ranglavel').val());
        }
    });
  }

  // ranglavel======================================================
  function get_schranglavel(yeae){
    $.ajax({        
        url: "<?= site_url('student_fee/c_student_fee_receipt/get_schranglavel') ?>",    
        data: {         
          'yeae':yeae
        },
        type: "post",
        dataType: "json",
        // async:false,
        success: function(data){ 
            var rang = '';
                rang="<option  value=''>All</option>";
            if(data.schrang.length > 0){
                $.each(data.schrang, function(i, row){
                    rang += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
                });
            }else{
                rang += '';
            }

            $('#ranglavel').html(rang);

        }
    });
  }

  // class============================================================
  function get_schclass(schoollavel){
    $.ajax({        
        url: "<?= site_url('student_fee/c_student_fee_receipt/get_schclass') ?>",    
        data: {         
          'schoollavel':schoollavel
        },
        type: "post",
        dataType: "json",
        // async:false,
        success: function(data){ 
            var cla = '';
                cla="<option  value=''>All</option>";
            if(data.schclass.length > 0){
                $.each(data.schclass, function(i, row){
                    //cla="<option  value=''>All</option>";
                    cla += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                });
            }else{
                cla += '';
            }

            $('#classname').html(cla);

        }
    });
  }
//=========================================================================
  function fillrevenue(familyid){
    //var familyid=$(event.target).val();
    if(familyid>0){
          $.ajax({
                            url:"<?php echo base_url(); ?>index.php/student/student/getfamilyrow",    
                            data: {
                                'familyid':familyid},
                            type:"post",
              dataType:"json",
                            async:false,
                            success: function(data){
                              $('#province').val(data.province);
                              $('#commune').val(data.commune);
                              $('#district').val(data.district);
                              $('#village').val(data.village);
                              $('#phone1').val(data.tel);
                              $('#zoon').val(data.zoon);
                              $('#permanent_adr').val(data.permanent_adr);
                              $('#family_revenue').val(data.revenue);
                              $('#from_school').val(data.from_school);
                              $('#measure').val(data.measure);
                              $("#listparent").html("<tr>"+
                                            "<td>"+data.father_name+"</td>"+
                                            "<td>"+data.father_name_kh+"</td>"+
                                            "<td>Male</td>"+
                                            "<td>"+data.father_dob+"</td>"+
                                            "<td> Father's Child </td>"+
                                            "<td>"+data.father_ocupation+"</td>"+
                                            "<td>"+data.father_revenu+"</td>"+
                                            
                                          "</tr>"+
                                          "<tr>"+
                                            "<td>"+data.mother_name+"</td>"+
                                            "<td>"+data.mother_name_kh+"</td>"+
                                            "<td>FeMale</td>"+
                                            "<td></td>"+
                                            "<td> Mother's Child </td>"+
                                            "<td>"+data.mother_ocupation+"</td>"+
                                            "<td>"+data.mother_revenu+"</td>"+
                                            
                                          "</tr>");
                              fillstudent(familyid);
                              getlistres(familyid);
                              
                        }
                   });
    }else{
       $('#family_revenue').val('');
    }  
  }

  function getlistres(familyid){
        $.ajax({
                url:"<?php echo base_url(); ?>index.php/student/student/getresponstudent",    
                data: {
                    'familyid':familyid},
                type:"post",
                success: function(data){
                  $("#listrespon").html(data);
                }
           });
  }

  function fillstudentinf(event){
        var memberid=$(event.target).val();
        var familyid=$('#familyid').val();
        $.ajax({
                        url:"<?php echo base_url(); ?>index.php/student/student/getmemberrow",    
                        data: {
                        'r_id':memberid},
                        type:"post",
              dataType:"json",
                      async:false,
                      success: function(data){
                        $('#last_name').val(data.last_name);
                        $('#first_name').val(data.first_name);
                        $('#last_name_kh').val(data.last_name_kh);
                        $('#first_name_kh').val(data.first_name_kh);
                        $('#dob').val(convertSQLDate(data.dob));
                        fillusername();
                        getlistmember(familyid,memberid);
                  }
             });
  }

  function getlistmember(familyid,memberid){
        $.ajax({
                            url:"<?php echo base_url(); ?>index.php/student/student/getmemberstudent",    
                            data: {
                                'familyid':familyid,
                                'memberid':memberid},
                            type:"post",
                            success: function(data){
                              //alert(data);
                              $("#listmember").html(data);
                $('#liststdlink').html(getstdlink(familyid));
                  
                        }
                   });
        
  }
  function getstdlink(familyid){
    var count;
    $.ajax({
             url:"<?php echo base_url(); ?>index.php/student/student/getstdlink",    
             data: {'familyid':familyid,'studentid':0},
             async: false,
             type:"post",
             success: function(data){
              count=data;
           //alert(data);
         }
    });
    return count;
  }

  function fillstudent(familyid){
        var classid=$('#classid').val();
        $.ajax({
                            url:"<?php echo base_url(); ?>index.php/student/student/getmemberbyfamily",    
                            data: {
                                'familyid':familyid,'classid':classid},
                            type:"post",
                            success: function(data){
                              $('#student').html(data);
                        }
                   });
  }
  function autofillfamily(){    
    var fillrespon="<?php echo site_url("student/student/fillfamily")?>";
      $(".family_id").autocomplete({
        source: fillrespon,
        minLength:0,
        select: function( events, ui ) {          
          var f_id=ui.item.id;
          //alert(f_id);
          $("#familyid").val(f_id);
          fillrevenue(ui.item.id)//alert(f_id);
        }           
      });
  }
  
  //================================
  function Fvbusfee(){
    $.ajax({
        url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_receipt/FCbusfee",    
        data: {
          'oparea':$(".oparea").val()
          },
        type:"post",
        dataType:"json",
        async:false,
        success: function(data){                
        $('#opbusfeetype').html(data.busfe);
        FVcalarea();
      }
     });
  };
  
  
  function FvgetOp(opprpid){
    if(opprpid>0){
          $.ajax({
                    url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_receipt/Fcgetop",    
                    data: {
                        'opprpid':opprpid,
                'opprpid':$(".opprpid").val()
                },
                            type:"post",
              dataType:"json",
                            async:false,
                            success: function(data){
                $('#shlevelid').html(data.opshlev);
                $('#acandemicid').html(data.datayears);
                Fvshlev();
                // Fvyear();
                // Fvclass();
                        }
        });
    }else{
      //  $('#family_revenue').val('');
    } 
  };
  
  
  function Fvshlev(){ 
    $.ajax({
        url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_receipt/Fcgetopshlev",    
        data: {
          'opprpid':$(".opprpid").val(),
          'shlevid':$(".shlevelid").val()
          },
        type:"post",
        dataType:"json",
        async:false,
        success: function(data){                
          $('#acandemicid').html(data.datashlev);
          Fvyear();
          Fvclass();
          FVpaymentType();
      }
     });
  };
  function Fvyear(){  
    $.ajax({
        url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_receipt/Fcgetopyear",    
        data: {
          'opprpid':$(".opprpid").val(),
          'shlevelid':$(".shlevelid").val()
          },
        type:"post",
        dataType:"json",
        async:false,
        success: function(data){                
          $('#ranglevid').html(data.datayears);
      }
     });
  };
  
  function Fvclass(){ 
    $.ajax({
        url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_receipt/Fcgetclass",    
        data: {         
          'shlevelid':$(".shlevelid").val()
          },
        type:"post",
        dataType:"json",
        async:false,
        success: function(data){                
          $('#classid').html(data.dataclasss);
      }
     });
  };
  
  function FgetRowStuinfo(stuid,typeno){    
    if(stuid>0){
          $.ajax({
                  url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_receipt/Fcstudent",    
                  data: {
                      'stuid_pos':stuid,
                      'typeno':typeno
                },
              type:"post",
              dataType:"json",
              async:false,
              success: function(data){
                        $('.photostu').html(data.image_stu);
                              if(typeno!=""){
                               $('.tbody_paymenttype').html(data.getpaylist);
                              }
                              $('.stuid').html(data['getRow']['studentid']);
							  $('.student_num').html(data['getRow']['student_num']);
							  
                              $('.nameeng').html(data['getRow']['first_name']+' '+data['getRow']['last_name']);
                              $('.namekh').html(data['getRow']['first_name_kh']+' '+data['getRow']['last_name_kh']);
                              $('.email').html(data['getRow']['email']);
                              $('.phone').html(data['getRow']['phone1']);
                              $('.sex').html(data['getRow']['gender']);
                              $('.term').html(data['getRow']['classid']);  
                                                          
                              $('.amt_total').val(data['arayOrder']['amt_total']);
                              $('.amt_deposited').val(data['arayOrder']['amt_paid']);                               
                              $('.amt_deposit').val(data['arayOrder']['amt_balance']);  
							   // alert(data['arayOrder']['amt_balance']);                            
                               $('.program').html(data['arayOrder']['program']);
                               $('.schoollavel').html(data['arayOrder']['chlevel']);
                               $('.year').html(data['arayOrder']['sch_year']);
                               $('.ranglavel').html(data['arayOrder']['rangelevelname']);
                               $('.class').html(data['arayOrder']['class_name']);
                               
                               $('.paymethod').html(data['arayOrder']['schoolfeetype_kh']);
                               $('.paymenttype').html(data['arayOrder']['paymenttype']);
                               $('.area').html(data['arayOrder']['area']);
                               $('.bus').html(data['arayOrder']['busfeetypid']);
                               // $('.date').html(data['arayOrder']['trandate']);                               
                            // Fcalpayment();
                
                        }
          });
    }
  }
  //-------------------------------
  function totalamount(){
    var amt_line = 0;
    $(".amt_line").each(function(){
        amt_line = amt_line += $(this).html()-0;
    });

    $('.amt_balance').html(amt_line)-0;
  }

  function totalbalan(){
    var amt_balance = $(".amt_balance").html(-0);
    var descount    = $("#descount").val()-0;
    if(descount > amt_balance){
      descount =0;
      $(".descount").val(0);
      $(".balance").html(0);
    }
    var balance  = (amt_balance - descount) - 0;

    $("#balance").html(balance); 

    alert(balance);
  }
  function Fcalpayment(){
        var totals=0;
        var amt_balance=$("#amt_balance").val()-0;
        var atm_total=$("#amt_total").val()-0;
        var amt_deposited=$("#amt_deposited").val()-0;
        var amt_deposit=$("#amt_deposit").val()-0;
        
    $('input.payment-amt').each(function(i){                        
        var tr=$(this).parent().parent();                                                                                   
        var payment_des=tr.find('.payment-des').val()-0;
        var payment_note=tr.find('.payment-note').val()-0;                                                                                  
        var payment_unitprice=tr.find('.payment-unitprice').val()-0;                                                                                
        var payment_dis=tr.find('.payment-dis').val()-0;                                                                            
        var numqty=tr.find('.numqty').val()-0;
        
        if(payment_dis>100){
            tr.find('.payment-dis').val(0);
            tr.find('.payment-dis').select();
            payment_dis=0;
            // return false;
            
        }
        if(payment_unitprice==""){
            tr.find('.payment-unitprice').val(0);
            payment_unitprice=0;
            // return false;
            
        }
        
                        
        var payment_amt=tr.find('.payment-amt').val()-0;
        
        var amt_line=((payment_unitprice*numqty)-((payment_dis/100)*(payment_unitprice)*numqty))-0;
        tr.find('.payment-amt').val(amt_line.toFixed(3));   
        totals+=amt_line;
        })
       $("#amt_total").val(totals.toFixed(3));
       // alert(amt_deposited);
         var lastba=(totals-(amt_deposited+amt_deposit))-0;
         $(".amt_balance").val(lastba.toFixed(3));
         
         if((amt_deposited+amt_deposit)>atm_total){
            $(".amt_deposit").val(0); 
            $(".amt_deposit").select();   
            var todep=$("#amt_total").val()-amt_deposited;          
            $(".amt_balance").val(todep.toFixed(3));
        }
        
    }
  // save ===========================================================  
  function FSavePayment(){
    var totals=0;
    
    if(confirm("Are You Sure want to process.....")){
      $("#save,.update").hide();
        $.ajax({
        url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_receipt/FCsave",
            
        data: {
          text_note:$(".text_note").val(),          
          amt_deposit:$(".amt_deposit").val(),         
          trandate:$("#trandate").val(),
          h_typeno:$("#h_typeno").val()
          },
        type:"post",
        dataType:"json",
        async:false,
        success: function(data){
          var nextTran=(data.nextTran);
          toastr["success"]('Success!'+nextTran);
          // alert("Record has bee saved! "+nextTran);
          FClear();
            // $('#dialog_print').collapse('toggle')
			
			$('.clprint').click();
            $(".print_p2").attr("href","<?php echo site_url("student_fee/c_student_print_receipt")?>?param_reno="+nextTran);
            
            Fsearch(1,$('#sort_num').val());    
        }
        });
    }
  }

  function FVcalarea(){
    var attr_pric=$('.opbusfeetype option:selected').attr('attr_pric');
    var busfee_des=$('.opbusfeetype option:selected').html();
    // alert(attr_pric+'---'+busfee_des);
    $("#busfee").val(attr_pric);
    $(".busfee_des").val(busfee_des);
    Fcalpayment();
  }
  
  function FVpaymentType(){
    var shlevelid=$('.shlevelid option:selected').val();
    var acandemicid=$('.acandemicid option:selected').val();
    var ranglevid=$('.ranglevid option:selected').val();
    var oppaymentmethod=$('.oppaymentmethod option:selected').val();
    $.ajax({
        url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_receipt/FCgetPaymenttype",    
        data: {         
          'shlevelid':shlevelid,
          'acandemicid':acandemicid,
          'ranglevid':ranglevid,
          'paymentmethod':oppaymentmethod         
          },
        type:"post",
        dataType:"json",
        async:false,
        success: function(data){                
          $('#paymenttype').html(data.gpaymenttype);
          FVpaymentTypepice();
      }
     });    
  }
  
  function FVpaymentTypepice(){
    var shlevelid=$('.shlevelid option:selected').val();
    var acandemicid=$('.acandemicid option:selected').val();
    var ranglevid=$('.ranglevid option:selected').val();
    var oppaymentmethod=$('.oppaymentmethod option:selected').val();
    var paymenttype=$('.paymenttype option:selected').val();
    
    $.ajax({
        url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_receipt/FCpaymenttyp_price",    
        data: {         
          'shlevelid':shlevelid,
          'acandemicid':acandemicid,
          'ranglevid':ranglevid,
          'paymenttype':paymenttype,
          'paymentmethod':oppaymentmethod         
          },
        type:"post",
        dataType:"json",
        async:false,
        success: function(data){  
          var fee=(data.fee)-0;
          var book_price=(data.book_price)-0;
          var admin_fee=(data.admin_fee)-0;
          var amt=(fee+book_price+admin_fee); 
          if(isNaN(amt)) {
            var amt = 0;
          }   
          $('.schoolfee_prices').val(amt.toFixed(3));
          // alert(data.ranglevfeeid);
      }
     });    
  }
// ---------------------Fsearch-------------------------------------
    function Fsearch(page,total_display,sortby,sorttype){
        var roleid = <?php echo $this->session->userdata('roleid');?>;
        var per    = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
        var sort_num = $('#sort_num').val();
        var invoice = $('#invoice').val();
        var studen_id = $('#studen_id').val();
        var studentname = $("#studentname").val();
        var schoolinfor = $('#schoolinfor').val();
        var formdate =$("#formdate").val();
        var todate =$("#todate").val();
        var program = $("#program").val();
        var schoollavel = $("#schoollavel").val();
        var yeae = $("#yeae").val();
        var classname = $("#classname").val();
        var ranglavel = $("#ranglavel").val();
        var status = $("#status").val();

        $.ajax({
            url:"<?php echo base_url();?>index.php/student_fee/c_student_fee_receipt/search",
            type: "POST",
            datatype: "Json",
            async: false,
            data:{
                  'page'  : page,
                  'p_page': per,
                  'total_display': total_display,
                  'sort_num':sort_num,
                  'sortby':sortby,
                  'sorttype':sorttype,
                  'invoice':invoice,
                  'studen_id': studen_id, 
                  'studentname':studentname,
                  'schoolinfor':schoolinfor,
                  'formdate':formdate,
                  'todate':todate,
                  'program':program,
                  'schoollavel':schoollavel,
                  'yeae':yeae,
                  'classname':classname,
                  'ranglavel':ranglavel,
                  'status': status

            },
            success: function (data) {
                $("#listrespon").html(data.data);
                $('.pagination').html(data['pagination']['pagination']);
            }
        });
    }

   function sort(event){
     /*$(event.target)=$(this)*/ 
      this.sortby = $(event.target).attr("rel");
      if ($(event.target).attr("sorttype") == "ASC") {
          $('.sort').removeClass('cur_sort_up');
          $(event.target).addClass(' cur_sort_down');
          $(event.target).attr("sorttype", "DESC");
          this.sorttype = "ASC";
          $('.sort').removeClass('cur_sort_down');
          $(event.target).addClass(' cur_sort_up');
      } else if ($(event.target).attr("sorttype") == "DESC") {

          $(event.target).attr("sorttype", "ASC");
          this.sorttype = "DESC";
          $('.sort').removeClass('cur_sort_up');
          $('.sort').removeClass('cur_sort_down');
          $(event.target).addClass(' cur_sort_down');    
      }          
      Fsearch(1,$('#sort_num').val(),this.sortby,this.sorttype);
  }

  function FClear(){
    $("#h_typeno").val("");
    $("#stuid").val("");  
    $("#typeno").val("");
    $("#invoice").val("");
    $("#studentname").val("");
    // $(".payment-note").val(""); 
    $(".text_note").val("");    
   // $(".payment-unitprice").val(0);  
    $(".amt_deposit").val(0);    
    $(".payment-dis").val(0);     
    $(".amt_balance").val(0);   
    // $(".payment-amt").val(0); 
    // $("#atm_total").val(0);
    // $("#amt_dis").val(0);
    // $("#amt_balance").val(0);
  }
  
  function FVoid(deltypeno){		
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_invoice/FCVoid",   
				data: {					
					'deltypeno':deltypeno				
					},
				type:"post",
				dataType:"json",
				async:false,
				success: function(data){					
					toastr["success"]('Record has been Void!');
					Fsearch(1,$('#sort_num').val()); 
					// $(".cl_btncancel").click();
			}
	   });		
	}
	
</script>