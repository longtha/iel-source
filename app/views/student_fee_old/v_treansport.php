<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }
    
    //  area  =======================================
    if(isset($areaname) && count($areaname)>0){
      $area="";
      $area="<option attr_price='0' value=''></option>";
      foreach($areaname as $row){
         $area.="<option value='".$row->areaid."'>".$row->area."</option>";
      }
    }
    
 ?>
 <style type="text/css">
    table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }

    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

</style>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
    <div class="col-sm-12">
        <div class="row">
          <div class="col-xs-12">
             <div class="result_info">
                <div class="col-xs-6">
                  <span class="icon">
                      <i class="fa fa-th"></i>
                  </span>
                    <strong>Student Transport List</strong>  
                </div>
                <div class="col-xs-6" style="text-align: right">
                   <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add New">
                      <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
                   </a>
                   <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                      <img src="<?= base_url('assets/images/icons/print.png') ?>">
                   </a>
                   <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
                      <img src="<?= base_url('assets/images/icons/export.png') ?>">
                   </a>           
                   
                   <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                      <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
                   </a>
                </div>         
             </div>
          </div>
       </div>

    <!-- dialog -->
        <div class="collapse" id="collapseExample" style=""> 
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? //= site_url('school/semester/save') ?>" id="f_save">
         <input type="hidden" name="semesterid" id="semesterid">
         
         <div class="col-sm-4"> 
            <div class="form-group">
               <label for="studenid">Student Name & Student ID</label>
               <input type="text" name="studen_id" id="studen_id" class="form-control" placeholder="Search..."  tabindex="1">
            </div>

            <div class="form-group">
               <label for="bus_name">Bus Name</label>
               <select name="bus_name" id="bus_name" class="form-control"  tabindex="5">

               </select>
            </div>
            
         </div><!-- 1 -->

         <div class="col-sm-4">
            <div class="form-group">
               <label for="area_name">Area Name</label>
               <select name="area_name" id="area_name" class="form-control" tabindex="5">
                  <?php echo $area; ?>
               </select>
            </div>

            <div class="form-group">
               <label for="driver_name">Driver Name</span></label>
               <select name="driver_name" id="driver_name" class="form-control"  tabindex="5">

               </select>
            </div>
            
         </div><!-- 2 -->

         <div class="col-sm-4">

            <div class="form-group"​ style="">
               <label for="formdate">From Date<span style="color:red"></span></label>
               <div class="input-group">
                 <input type="text" name="formdate" id="formdate" class="form-control" placeholder="dd/mm/yyyy">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
               </div>
            </div>

            <div class="form-group" style="">
               <label for="todate">To Date<span style="color:red"></span></label>
                 <div class="input-group">
                 <input type="text" name="todate" id="todate" class="form-control" placeholder="dd/mm/yyyy">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
               </div>
            </div>
         </div><!-- 3 -->      

         <div class="col-sm-7 col-sm-offset-5">
            <div class="form-group">
               <button type="button" class="btn btn-primary btn-sm " name="tbnserch" id="tbn_serch" value="serch" tabindex="11" data-save='2'>Search</button>
               
              
            </div>            
         </div>

      </form>
   </div>
    <!-- show header -->
        <div class="col-sm-12">
            <div class="panel panel-default">
               <div class="table-responsive" id="div_export_print">
                    <div id="tab_print">
                         <table border="0" class="table table-hover" id="setBorderTbl">
                             <thead>
                                <tr>
                                    <?php
                                     foreach ($thead as $th => $val) {
                                        if ($th == 'No'){
                                          echo "<th class='$val'>".$th."</th>";
                                        }else if ($th == 'Photo'){
                                          echo "<th class='$val'>".$th."</th>";
                                        }else if ($th == 'Other'){
                                          echo "<th class='$val'>".$th."</th>";
                                        }else{
                                          echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
                                        }
                                      }
                                    ?>
                                </tr>
                              </thead>
                             <tbody id='listrespon'>
                             </tbody>
                         </table>
                     </div>
                 </div>
            </div>
            <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
                Display : <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                                <?php
                                    $num=10;
                                    for($i=0;$i<10;$i++){?>
                                        <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                        <?php $num+=20;
                                    }
                                ?>
                          </select>
            </div>
            <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
                <div class="pagination" id="pagination" style="text-align:center"></div>
            </div>
        </div>
    </div>
</div>

<!-- dialog -->
<div class="modal fade bs-example-modal-lg" title="Export" id="exporttap" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="wrapper">
                <div class="clearfix" id="main_content_outer">
                    <div id="main_content">
                        <div class="result_info">
                            <div class="col-sm-6">
                                <strong>Choose Column To Export</strong>
                            </div>
                            <div class="col-sm-6" style="text-align: center">
                                <strong>
                                    <center class='visit_error' style='color:red;'></center>
                                </strong>
                            </div>
                        </div>
                        <form enctype="multipart/form-data" name="frmvisit" id="frmvisit" method="POST">
                                <div class="row">
                                    <div class="col-sm-12">
                                            <div class="panel-body">
                                                <div class='table-responsive'>
                                                       <table class='table'>
                                                            <thead >
                                                                <?php
                                                                foreach($thead as $th=>$val){
                                                                    if($th!='Action')
                                                                        echo "<th>".$th."</th>";    
                                                                }?>
                                                            </thead>
                                                            <tbody id="class_body">
                                                                <?php
                                                                foreach($thead as $th=>$val){
                                                                    if($th!='Action')
                                                                        echo "<td align='center'><input type='checkbox' checked class='colch' rel='".$val."'></td>";    
                                                                }?>
                                                            </tbody>
                                                       </table>
                                               </div>
                                            </div>
                                    </div> 
                                </div>
                          </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id='btnprint' class="btn btn-primary">Print</button>
                <button type="button" id='btnexport' class="btn btn-primary">Export</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 
<script type="text/javascript">
    $(function(){
        var sort_num = $("#sort_num").val();
        Getdata(1,$('#sort_num').val());
        // sort_num ========================
        $('#sort_num').change(function(){
            // alert($(this).val());
            var  total_display= $(this).val();
            Getdata(1,total_display);
        });

        // pagenav ==========================
        $("body").delegate(".pagenav","click",function(){
            var page = $(this).attr("id");
            var total_display = $('#sort_num').val();
            Getdata(page,total_display);
        });
        // ADD ==============================
        $('body').delegate('#a_addnew', 'click', function(){
            $('#collapseExample').collapse('toggle')
             cleadata();
         
        });
        //Serch==============================
        $('body').delegate('#tbn_serch', 'click', function(){
            Getdata();
        
        });
        //formartdate==================================
        $("#formdate,#todate").datepicker({
            language: 'en',
            pick12HourFormat: true,
            format:'dd-mm-yyyy'
        });
        // refresh =======================================
        $('body').delegate('#refresh', 'click', function(){
            location.reload();
        });
        // tbnprinter =========================================
        $("#btnprint").on("click", function () {
            var htmlToPrint = '' +
                '<style type="text/css">' +
                'table th, table td {' +
                'border:1px solid #000 !important;' +
                'padding;0.5em;' +
                '}' +
                '</style>';
            var title = "Student Transport List";
            var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "");
            var export_data = $("<center>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
            export_data += htmlToPrint;
            gsPrint(title, export_data);
        });
        // tbn exsport =============================================
        $("#btnexport").on("click", function (e) {
            var title = "Student Transport List";
            var data = $('.table').attr('border', 1);
            data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
            var export_data = $("<center><h3 align='center'>" + title + "</h3>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
            e.preventDefault();
            $('.table').attr('border', 0);
        });

        $("#area_name").on('change',function(){
            // alert($(this).val());
            get_bus($(this).val());
        });
        $("#bus_name").on('change',function(){
            get_driver($(this).val());
        });
        // chos colume ==================================
        $('#a_export').click(function () {
            $('#exporttap').modal('show');
            $('#btnprint').hide();
            $('#btnexport').show();
        });
        $('#a_print').click(function () {
            $('#exporttap').modal('show');
            $('#btnprint').show();
            $('#btnexport').hide();
        });
});// function===================================================================================
        function gsPrint(emp_title, data) {
            var element = "<div>" + data + "</div>";
            $("<center><p style='padding-top:15px;text-align:center;'><b>" + emp_title + "</b></p><hr>" + element + "</center>").printArea({
                mode: "popup",  //printable window is either iframe or browser popup
                popHt: 600,  // popup window height
                popWd: 500,  // popup window width
                popX: 0,  // popup window screen X position
                popY: 0, //popup window screen Y position
                popTitle: "test", // popup window title element
                popClose: false,  // popup window close after printing
                strict: false
            });
        }
    // colch ================================================
        $('.colch').click(function(){
            var col=$(this);
            if(col.is(':checked')){
                $('.'+col.attr('rel')).removeClass('remove_tag');
            }else{
                $('.'+col.attr('rel')).addClass('remove_tag');
            }
        });
    // detbus ====================================================
        function get_bus(area_name){
            $.ajax({        
                url: "<?= site_url('student_fee/c_treansport/get_bus') ?>",    
                data: {         
                  'area_name':area_name
                },
                type: "post",
                dataType: "json",
                // async:false,
                success: function(data){ 

                    var opt = '';
                        opt="<option  value=''>All</option>";
                    if(data.bus.length > 0){
                        $.each(data.bus, function(i, row){
                            //
                            opt += '<option value="'+ row.busid +'">'+ row.busno +'</option>';
                        });
                    }else{
                        opt += '';
                    }

                    $('#bus_name').html(opt);
                    get_driver($('#bus_name').val(), $('#driver_name').val());
               }
            });
        }
    // get_driver=================================================
        function get_driver(bus_name){
            $.ajax({        
                url: "<?= site_url('student_fee/c_treansport/get_driver') ?>",    
                data: {         
                  'bus_name':bus_name
                },
                type: "post",
                dataType: "json",
                // async:false,
                success: function(data){ 

                    var opt = '';
                        opt="<option  value=''>All</option>";
                    if(data.dri.length > 0){
                        $.each(data.dri, function(i, row){
                            //
                            opt += '<option value="'+ row.driverid +'">'+ row.driver_name +'</option>';
                        });
                    }else{
                        opt += '';
                    }

                    $('#driver_name').html(opt);
                   // get_schyera($('#area_name').val(), $('#schoollavel').val());
                    //get_schclass($('#program').val(), $('#classname').val());
               }
            });
        }
        // Getdata =====================================================
        function Getdata(page,total_display,sortby,sorttype){
            var roleid = <?php echo $this->session->userdata('roleid');?>;
            var per = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
            var sort_num = $('#sort_num').val();
            var studen_id = $('#studen_id').val();
            var bus_name = $('#bus_name').val()
            var formdate = $('#formdate').val();
            var area_name = $('#area_name').val();
            var driver_name = $('#driver_name').val();
            var todate = $('#todate').val();
            $.ajax({
                url:"<?php echo base_url();?>index.php/student_fee/c_treansport/Getdata",
                type: "POST",
                datatype: "Json",
                async: false,
                data:{
                        'page'  : page,
                        'p_page': per,
                        'total_display': total_display,
                        'sort_num':sort_num,
                        'sortby':sortby,
                        'sorttype':sorttype,
                        'studen_id':studen_id,
                        'formdate':formdate,
                        'bus_name':bus_name,
                        'area_name':area_name,
                        'driver_name':driver_name,
                        'todate':todate
                },
                success: function (data) {
                    $("#listrespon").html(data.data);
                    $('.pagination').html(data['pagination']['pagination']);
                }
            });
        }
    // sort ===================================================
        function sort(event){
           /*$(event.target)=$(this)*/ 
            this.sortby = $(event.target).attr("rel");
            if ($(event.target).attr("sorttype") == "ASC") {
                $('.sort').removeClass('cur_sort_up');
                $(event.target).addClass(' cur_sort_down');
                $(event.target).attr("sorttype", "DESC");
                this.sorttype = "ASC";
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_up');
            } else if ($(event.target).attr("sorttype") == "DESC") {

                $(event.target).attr("sorttype", "ASC");
                this.sorttype = "DESC";
                $('.sort').removeClass('cur_sort_up');
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_down');    
            }               
            Getdata(1,$('#sort_num').val(),this.sortby,this.sorttype);
        }
    // cleadata ==============================================
        function cleadata(){
            $('#studen_id').val('');
            $('#bus_name').val('');
            $('#formdate').val('');
            $('#area_name').val('');
            $('#driver_name').val('');
            $('#todate').val('');
        }
</script>
