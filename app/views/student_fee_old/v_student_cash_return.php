<style type="text/css">
	#se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	#mdinvoice .modal-dialog  {width:99% !important;}			
	
	a {
		cursor: pointer;
	}
	 .gamt{ font-size:14px; color:#00F; font-weight:bold; text-align:right}
	 
div.relative {
    position: relative;
    width: 200px;
    height: 85px;
    border: 3px solid #73AD21;
}

div.absolute {
    position: absolute;
    top: 0px;
    right: 0;
    width: 200px;
    height: 80px;
    border: 3px solid #73AD21;
}

table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }

    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    .cl_inv{
      border-top: 1px solid #ccc;
    }

    .tr_row{
      color: red;
    }
</style>
<?php
   
		$oppaylist="";
		$oppaylist="<option attr_price='0' value=''></option><option attr_price='create_new' value='create_new'><span style='color:blue'>--- Create New ---</span></option>";
		
		foreach ($this->otherfee->FMotherfee() as $rowotherfee) {
			$oppaylist.="<option attr_typefee='".$rowotherfee->typefee."' attr_datetran='".$this->green->convertSQLDate($rowotherfee->fromdate)."  -  ". $this->green->convertSQLDate($rowotherfee->todate)."' attr_price='".$rowotherfee->prices."' value='".$rowotherfee->otherfeeid."'>".$rowotherfee->otherfee."</option>";
		}
      $photodel="<a id='a_delete' class='btn btn-xs btn-danger'><span class='glyphicon glyphicon-trash'></span></a>";
?>
<div class="container-fluid" style="width:100% !important">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6 btn-success" style="background: #4cae4c;color: white;">
            	<span class="icon">
                  <i class="fa fa-th"></i>
              </span>
                <strong>Cash Return</strong>  
            </div>
            <div class="col-xs-6 " style="text-align: right ;background: #4cae4c;color: white;">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                  <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
               </a>
               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
               <img src="<?= base_url('assets/images/icons/print.png') ?>"> </a>
               <!-- <a href="javascript:;" id="a_addnew" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" ><span class="glyphicon glyphicon-search" title="Search..."></span></a>
               <a id="a_print" data-toggle="tooltip" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-print" data-placement="top" title="Print..."></span></a> -->
                  
               
               <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
               </a>           
               
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
         <input type="hidden" name="termid" id="termid">
         <div class="col-sm-4" style=""> 
            <div class="form-group">
               <label for="typeno">Invoice<span style="color:red"></span></label>
               <input type="text" name="typeno" id="typeno" class="form-control" placeholder="# Invoice">
            </div>

            <div class="form-group">
               <label for="student_num">ID<span style="color:red"></span></label>
               <input type="text" name="student_num" id="student_num" class="form-control" placeholder="ID">
            </div>            

			     <div class="form-group">
               <label for="student_name">English/Khmer Name<span style="color:red"></span></label>
               <input type="text" name="student_name" id="student_name" class="form-control" placeholder="English/Khmer Name">
            </div>

            <div class="form-group">
               <label for="gender">Gender<span style="color:red"></span></label>
               <select name="gender" id="gender" class="form-control">
               	<option value="">All</option>
               	<option value="Female">Female</option>
               	<option value="Male">Male</option>
               </select>
            </div>

            <div class="form-group">
               <label for="schoolid">School Info<span style="color:red"></span></label>
               <select name="schoolid" id="schoolid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->info->getschinfor() as $row) {
                        $opt .= '<option value="'.$row->schoolid.'">'.$row->name.'</option>';
                     }

                     echo $opt;        
                  ?>                  
               </select>
            </div>     

         </div><!-- 1 -->

         <div class="col-sm-4">
            
            <div class="form-group">
               <label for="programid">Program<span style="color:red"></span></label>
               <select name="programid" id="programid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->p->getprograms() as $row) {
                        $opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group">
               <label for="schlevelids">School Level<span style="color:red"></span></label>
               <select name="schlevelids" id="schlevelids" class="form-control">
                  
               </select>
            </div>

            <div class="form-group">
               <label for="yearid">Year<span style="color:red"></span></label>
               <select name="yearid" id="yearid" class="form-control">

               </select>
            </div>

            <div class="form-group">
               <label for="rangelevelid">Rangelevel Name<span style="color:red"></span></label>
               <select name="rangelevelid" id="rangelevelid" class="form-control">

               </select>
            </div>
            
            <div class="form-group" style="">
               <label for="classid">Class<span style="color:red"></span></label>
               <select name="classid" id="classid" class="form-control">
                  <?php
                     // $opt = '';
                     // $opt .= '<option></option>';
                     // foreach ($this->c->allclass() as $row) {
                     //    $opt .= '<option value="'.$row->classid.'">'.$row->class_name.'</option>';
                     // }

                     // echo $opt;        
                  ?>
               </select>
            </div>        
            
         </div><!-- 2 -->

         <div class="col-sm-4">
            <div class="form-group"​ style="">
               <label for="from_date">From Date<span style="color:red"></span></label>
               <div class="input-group">
                 <input type="text" name="from_date" id="from_date" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
               </div>
            </div>

            <div class="form-group" style="">
               <label for="to_date">To Date<span style="color:red"></span></label>
                 <div class="input-group">
                 <input type="text" name="to_date" id="to_date" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
               </div>
            </div>

            <div class="form-group" style="">
               <label for="feetypeid">Payment Method<span style="color:red"></span></label>
               <select name="feetypeid" id="feetypeid" class="form-control"  >
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->f->getfeetypes() as $row) {
                        $opt .= '<option value="'.$row->feetypeid.'">'.$row->schoolfeetype.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group" style="">
               <label for="term_sem_year_id">Study Period<span style="color:red"></span></label>
               <select name="term_sem_year_id" id="term_sem_year_id" class="form-control">
                  <?php
                     // $opt = '';
                     // $opt .= '<option></option>';
                     // foreach ($this->f->getfeetypes() as $row) {
                     //    $opt .= '<option value="'.$row->feetypeid.'" '.($row->feetypeid - 0 == 1 ? 'selected="selected"' : '').'>'.$row->schoolfeetype.'</option>';
                     // }

                     // echo $opt;        
                  ?>
               </select>
            </div>
 			      <div class="form-group">
               <label for="report_type">Status<span style="color:red"></span></label>
               <select name="report_type" id="report_type" class="form-control">
               		<option value="">All</option>
                    <option value="1">Cash Return</option>  
                    <option value="0">Create Invoice</option>
               </select>
            </div>
            <div class="form-group"  style="display:none">
               <label for="is_closed">Is active?<span style="color:red"></span></label>
               <select name="is_closed" id="is_closed" class="form-control">
               	
               	<option value="0">Active</option>
               	<option value="1">Inactive</option>               	
               </select>
            </div>

         </div><!-- 3 -->      

         <div class="col-sm-7 col-sm-offset-5">
            <div class="form-group">
               <a id="btn_search" class="btn btn-primary btn-success btn_search"><span class="glyphicon glyphicon-search"></span> Search</a>
               <button type="button" class="btn btn-primary btn-sm btn_search" name="btn_search" id="btn_search" data-save='1' style="display: none;">Search</button>&nbsp; 
               <button type="button" class="btn btn-warning btn-sm btn_clear" name="btn_clear" id="btn_clear" data-save='1' style="display: none;">Clear</button>               
            </div>            
         </div>

      </form>      
      <div class="row">
         <div class="col-sm-12">
            <div class="col-sm-12" style="border-bottom: 1px solid #CCC;">&nbsp;</div>
         </div>           
      </div>
   </div>   
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
	         <div id="tab_print">
	            <table border="0"​ align="center" id="se_list" class="table table-hover">               
                    <thead>
                        <tr>
							               <?php
                                foreach ($thead as $th => $val) {
                                  if ($th == 'No'){
                                      echo "<th class='$val'width='3%'>".$th."</th>";
                                  }else if($th == 'Photo'){
                                      echo "<th class='$val'>".$th."</th>";
                                }else if ($th == 'Status') {
                                echo "<th class='$val remove_tag no_wrap'>".$th."</th>";
                                  }else{
                                    echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
                                  }
                               }
                            ?>
                        </tr>				  
                    </thead>                
	               <tbody></tbody>
	               
	               <tfoot>
	                  <tr style="border-top: 2px solid #ddd;" class="remove_tag">
	                     <td colspan="7" style="padding-top: 10px;">
	                        <span id="sp_total">
                            <label for="limit_record">Limit<span style="color:red"></span></label>
                              <select name="limit_record" id="limit_record" class="limit_record" style="width:50px;">
                                  <option value="10">10</option>                                 
                                 	<option value="50">50</option>
                                 	<option value="100">100</option>
                                 	<option value="300">300</option>
                                 	<option value="500">500</option>
                                 	<option value="50000">50000</option>
                                 	<option value="500000">500000</option>
                              </select>
                            </span>
	                     </td>
	                     <td colspan="6" style="text-align: right;">
	                        <span style="" id="sp_page" class="btn-group pagination" role="group" aria-label="..."></span>
	                     </td>
                        
                     </tr>
	                     
	                 </tfoot>               
	               </table>
              </div>        
           </div>
       </div>
   </div>
</div>

<!--modol -->
<div class="modal" id="mdinvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="wrapper">
				        <div class="clearfix" id="main_content_outer">
				            <div id="main_content">
        					    <div class="result_info">
        					    	  <div class="col-sm-12 col-xs-12 " style="background: #4cae4c;color: white;">
                              <span class="icon"> <i class="fa fa-th"></i></span>
        					      		     <strong class="c_inv_update">Create Cash Return</strong>
                                        <strong class="inv"></strong>
                                        <!-- <a class="btn btn-sm btn-success"><span style="text-align:right;"  class="glyphicon glyphicon-remove"></span></a> -->
                                        
        					      	  </div>
          					      	<div class="col-sm-6" style="text-align: center">
            					      		<strong>
            					      			  <center class='visit_error' style='color:red;'></center>
            					      		</strong> 
                                  <div align="right" style="display:none">
                                   	<strong>Is General English(GEP)</strong>
                                    	<strong>
                                          <select class="is_generalenglish" id="is_generalenglish">
                                              <option value="0">No</option>
                                              <option value="1">Yes</option>
                                          </select>
                                      </strong>
                                    	
                                  </div>	
          					      	</div>
        					     </div>
                        <div class="col-xs-12" style="border:#CCC solid 1px;margin:0px;height:100%;">
                             <div class="col-sm-2 img-circle img-responsive photostu" style="border:#e7e7e7 solid 0px; padding:3px; text-align:center !important">
								
                             </div>
                              
                             <div class="col-sm-3" style="height:100% ;border-right:#CCC solid 1px;align-items: stretch;">
                                        <table style="width:100% !important"> 
                                            <tr class="lihideinfo">
                                            	<td style="display:none">
                                                <input type="text" class="h-program" value="" />
                                                <input type="text" class="h-schoollavel" value="" />
                                                <input type="text" class="h-year" value="" />
                                                <input type="text" class="h-ranglavel" value="" />
                                                <input type="text" class="h-class" value="" />
                                                <input type="text" class="h-area" value="" />
                                                
                                                <input type="text" class="h-enrollid" value="" />
                                                <input type="text" class="h-studentid" value="" />
                                                <input type="text" class="h-iscond" value="" />
                                                <input type="text" class="h-iscond-save" value="" />
                                                <input type="text" class="h-isaddnewinv" id="h-isaddnewinv" value="" />
                                            
                                       		 </td>
                                              <input type="hidden" id="typeno_" class="typeno_" >
                                              <input type="hidden" id="editehiden" class="editehiden" >
                                                <td>ID</td>

                                                <td>:</td><td><span class="stuid" style="text-align:center !important"></span></td>
                                            </tr>
                                            <tr class="lihideinfo">
                                                <td>Full Name(Kh)</td><td>:</td><td><span class="namekh"></span></td>
                                            </tr>
                                            <tr class="lihideinfo">
                                                <td>Full Name(Eng)</td><td>:</td><td> <span class="nameeng" style="text-align:center !important"></span></td>
                                            </tr>
                                            
                                            <tr class="lihideinfo">
                                                <td>Gender</td><td>:</td><td><span class="sex"></span></td>
                                            </tr>
                                            <tr class="lihideinfo">
                                                <td>E-Mail</td><td>:</td><td><span class="email"></span></td>
                                            </tr>
                                            
                                            <tr class="lihideinfo">
                                                <td>Telephone </td><td>:</td><td><span class="phone"></span></td>
                                            </tr>
                                           
                                        </table>    
                              </div>
                                    
                              <div class="col-sm-4" style="height:100% ;border-right:#CCC solid 1px;align-items: stretch;">
                                        <table style="width:100% !important" class="gform"> 
                                    <tr class="lihide">
                                    	
                                        <td>Program</td><td>:</td>
                                        	
                                         <td class="program">
                                          
                                              <?php //echo $opprogram?>
                                         </td>
                                    </tr>
                                    
                                    <tr class="lihide">
                                        <td>School&nbsp;Level</td><td>:</td>
                                        <td class="schoollavel">

                                          <?php  //echo $opget_sle?>

                                       </td>
                                    </tr>
                                   <tr class="lihide">
                                        <td>Academic&nbsp;Year</td><td>:</td>
                                         <td class="year">
                                          <?php  //echo $opgetyears?>

                                         </td>
                                    </tr>
                                    
                                    <tr class="lihide">
                                        <td>Rang&nbsp;Level</td><td>:</td>
                                        <td class="ranglavel">
                                          <?php //echo $oprang?>

                                       </td>
                                    </tr>
                                    <tr class="lihide">
                                        <td>Class</td><td>:</td>
                                        <td class="class">
                                          <?php  //echo $opclass?>
                                        </td>
                                    </tr>
                                    <tr class="lihide">
                                        <td>Time</td><td>:</td>
                                        <td class="time_study" id="time_study">
                                            <select id="opstudytime" class="form-control opstudytime" value="" disabled="disabled">
                                            
                                            </select>
                                        </td>
                                    </tr>
                                    
                                </table>
                             </div>
                                   
                              <div class="col-sm-3" style="height:100% ;align-items: stretch;">
                                        <table style="width:100% !important" class=""> 
                                           
                                           <tr class="lihide gform">
                                                <td>Payment&nbsp;Method </td>
                                                <td>:</td>
                                                <td style="display:none">
                                                	<input type="text" class="mh_paymethod" id="mh_paymethod" />
                                                    <input type="text" class="mh_peroid" id="mh_peroid" />
                                                    <input type="text" class="mh_prices" id="mh_prices" />
                                                </td>
                                                <td >
                                                  <select disabled="disabled"  name="oppaymentmethod" id="oppaymentmethod" class="form-control oppaymentmethod"  >
                                                    <?php
                                                        $optpay = '';
                                                        $optpay .= '<option value="">All</option>';
                                                        foreach ($this->f->getfeetypes() as $row) {
                                                        $optpay .= '<option value="'.$row->feetypeid.'">'.$row->schoolfeetype.'</option>';
                                                        }
                                                
                                                        echo $optpay;        
                                                    ?>
                                                  </select>
                                                </td>
                                            </tr>
                                            <tr class="lihide gform">
                                                <td>Study Period</td><td>:
                                                	<input type="hidden" class="h-studyperiod" id="h-studyperiod" />
                                                </td>
                                                <td>
                                                    	<span class="">
                                                        <select  id="paymenttype" class="form-control paymenttype" data-parsley-required-message="Select any payment type" required="" min="1" name="paymenttype" data-parsley-id="7216" disabled="disabled">
                                                        <?php // echo $opget_sle?>
                                                        </select>
                                               			 </span>
                                                	</td>
                                            </tr>
                                            
                                            <tr class="lihide gform">
                                                <td>Area</td><td>:</td>
                                                <td>
                                                    	<span class="">
                                                        <select id="oparea" class="form-control oparea" data-parsley-required-message="Select any area" required="" min="1" name="oparea" data-parsley-id="7216" disabled="disabled">
                                                                                        	
                                														<?php
                                															 $opt = '';
                                															 $opt .= '<option value="">All</option>';
                                															 foreach ($this->marea->FMareas() as $row) {
                                																$opt .= '<option value="'.$row->areaid.'">'.$row->area.'</option>';
                                															 }
                                										
                                															 echo $opt;        
                                														  ?>
                                                        </select>
                                               			 </span>
                                                	</td>
                                                
                                            </tr>
                                           <tr class="lihide gform">
                                                <td>Bus No</td><td>:</td>
                                                <td>
                                                    	<span class="">
                                                        <select id="opbusfeetype" class="form-control opbusfeetype" data-parsley-required-message="Select any school" required="" min="1" name="opbusfeetype" data-parsley-id="7216" disabled="disabled">
                                                        <?php // echo $opget_sle?>
                                                        </select>
                                               			 </span>
                                                	</td>
                                            </tr>
                                            <tr class="lihide">
                                                <td>Due Date</td><td>:</td>
                                               	<td>
                                                    <div class="input-group" data-date-format="dd-mm-yyyy">
                                                      <input value="<?php echo date('d-m-Y'); ?>"  id="duedate" class="form-control duedate" type="text" placeholder="dd-mm-yyyy" name="duedate" style="text-align:right">
                                                      <span class="input-group-addon">
                                                      <span class="glyphicon glyphicon-calendar"> </span>
                                                      </span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" align="">Tran&nbsp;Date&nbsp;&nbsp;</td>
                                                <td>:</td>
                                                <td>
                                               
                                                    <div class="input-group" data-date-format="dd-mm-yyyy">
                                                      <input value="<?php echo date('d-m-Y'); ?>" id="trandate" class="form-control trandate" type="text" placeholder="dd-mm-yyyy" name="duedate" style="text-align:right"> 
                                                      <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"> </span>
                                                      </span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                             		</div>
                           	</div>
                            <div class="col-xs-12">
                                <table style="width:100%" align="center" class="btl_paymenttype" id="btl_paymenttype">
                                	<thead class="thead_paymenttype">
                                     
                                   </thead>
                                   <tbody class="tbody_paymenttype"></tbody>
                                       <tfoot>
                                           <tr>
                                                <td rowspan="3" colspan="4" align="right">
                                                	<textarea placeholder="Add Note" class="text_note" id="text_note"></textarea>
                                                </td>
                                                <td colspan="1" align="right">Amount Return &nbsp;:&nbsp;</td>
                                                <td>
                                                    <input id="amt_total" readonly="readonly" class="form-control amt_total" type="text" placeholder=""  value="0"  style="text-align:right">
                                                    
                                                </td>
                                                
                                            </tr> 
                                            <tr style="display:none">
                                                <td colspan="1" align="right">Deposit&nbsp;:&nbsp;</td>
                                                <td>
                                                    <input id="amt_deposit" class="form-control amt_deposit" type="text" placeholder=""  value="0"  style="text-align:right">
                                                    
                                                </td>
                                                
                                            </tr> 
                                            <tr style="display:none">
                                                <td colspan="1" align="right">Balance&nbsp;:&nbsp;</td>
                                                <td>
                                                    <input id="amt_balance" class="form-control amt_balance" type="text" placeholder=""  value="0"  style="text-align:right">
                                                    
                                                </td>
                                                
                                            </tr>
                                             
                                  		</tfoot>
                                </table>
                        </div>
					       </div> 
			    </div>
			 </div> 
        <div class="modal-footer">   
        	   <input data-dismiss="modal" id="btnsaves"  class="btn btn-primary btn-success" type="button"  name="btnsaves"/> 
             <input  data-dismiss="modal" class="btn btn-warning cl_btncancel btn-success" type="button" value="Cancel" name="btncancel"/> 
        </div>
        <div style="padding-top:20px; padding:20px !important;" class="t_bodyhis"></div> 
        <div style="padding-top:30px;"></div>
      </div> <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog -->
</div> 

<span data-toggle="modal" data-target="#dialog_print" class="clprint" ></span>
<div class="modal" id="dialog_print" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="wrapper">
                <div class="clearfix" id="main_content_outer">
                   <div id="main_content">
                      <div class="result_info">
                            <div class="col-sm-12 btn-success">
                                <span class="icon"><i class="fa fa-th"></i></span><strong> Cash Return </strong>
                            </div>
                        </div>
                        <div class="col-xs-12" style="border:#CCC solid 1px;margin:0px;height:100%;">
                            <table style="text-align:center" align="center">
                                <tr style="line-height:40px;">
                                    <td>
                                      <a  target="_blank" href="" class="print_p1">Preview cash return</a>
                                    </td>
                                </tr>
                            </table>                             
                        </div>
                      </div> 
                  </div>
              </div> 
              <div class="modal-footer"> 
                  <input data-dismiss="modal" id="btncancel" class="btn btn-warning" type="button" value="Close" name="btncancel"/>
              </div>
          </div> <!-- /.modal-content -->
      </div>
</div> <!-- /.modal-dialog -->
<script type="text/javascript">
  $(function(){

    $("#btnsaves").on('click', function() {
        var oppaymentmethod=$('#oppaymentmethod').val()==null?'':$('#oppaymentmethod').val();
        var paymenttype=$('#paymenttype').val()==null?'':$('#paymenttype').val();
        
        // alert(oppaymentmethod+'------'+paymenttype);
        if($(".editehiden").val()!="" && $(".text_note").val()==""){         
            alert("Please comment in add note !");
            $(".text_note").focus();
            return false;
        }

        if($(".h-isaddnewinv").val()==0){// 
          if(oppaymentmethod==""){
            alert("Payment Method Can not blank...!");
            return false
            }else if(paymenttype==""){
            alert("Study Period Can not blank...!");
            return false
            }
        } 
          Fcalpayment();
          FSavePayment();
      
    });
    

    $("#year,#schlevelid").hide();
   	// get ======= 
      $('body').delegate('#feetypeid', 'change', function(){
      	var feetypeid = $(this).val() - 0;

      	$.ajax({
            url: '<?= site_url('student_fee/c_student_cash_return/get_paymentType') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               feetypeid: feetypeid,
               programid: $('#programid').val(),
               schlevelid: $('#schlevelids').val(),
               yearid: $('#yearid').val()
                               
            },
            success: function(data){
               // schoollevel ======
               var opt = '';               
               if(data.length > 0){     
                  opt += '<option value="">All</option>';          
                  $.each(data, function(i, row){
                     opt += '<option value="'+ row.term_sem_year_id +'">'+ row.period +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#term_sem_year_id').html(opt);             

            },
            error: function() {

            }
         });
      });

      // get ======= 
      $('body').delegate('#programid', 'change', function(){
         get_schlevel($(this).val());
         get_year($(this).val(), $('#schlevelids').val());    
                    
      });
      $('body').delegate('#schlevelids', 'change', function(){
         get_rangelevel($('#programid').val(), $(this).val());
         get_year($('#programid').val(), $(this).val());
         get_class($('#programid').val(), $('#schlevelids').val(), $('#yearid').val());
      });

    $('#duedate').datepicker({
      format: 'dd/mm/yyyy',
        //startDate: '-3d'
    });
     
    $('#trandate').datepicker({
      format: 'dd/mm/yyyy',
        //startDate: '-3d'
    });
   	$('#from_date').datepicker({
   		format: 'dd/mm/yyyy',
    		//startDate: '-3d'
   	});
   	$('#to_date').datepicker({
   		format: 'dd/mm/yyyy',
   		//startDate: '-3d'
   	});   
    $(".oparea").on('change', function() {
      // alert($(this).val());
      Fvbusfee();
    });
    $("#paymenttype").on('change', function() {
      if($(".oppaymentmethod").val()==""){
        $(this).val("");
      }
      $(".h-studyperiod").val($(this).val()); 
      FVpaymentTypepice();  
      Fcalpayment();
    });
    
      // tooltip ======
      $('[data-toggle="tooltip"]').tooltip();

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // print =======
      $('body').delegate('#a_print', 'click', function(){
         var title="<h4 align='center'>"+ "Cash Return Lists" +"</h4>";
         var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "")
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint(title, export_data);
      });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Cash Return Lists";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });      

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#term').select();
         $('#save').html('Save');
         $('#save_next').show();
      });

      // init. =====
      grid(1, $('#limit_record').val() - 0);

      // search ========
      $('body').delegate('#limit_record', 'change', function(){
         grid(1, $(this).val() - 0);
      });
      $('body').delegate('#btn_search', 'click', function(){
         grid(1, $('#limit_record').val() - 0);
      });


      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#limit_record').val() - 0);
      });
	
	  $('body').delegate("#a_delete_inv", "click", function() {
		 	 var meid=$(this).attr('attr_id');
		 	 var attr_type=$(this).attr('attr_type');
			if(confirm('Are you sure want to delete ?','Data has deleted!')){
				FVdeinv(attr_type,meid);
			}
			
		});
    $('body').delegate("#a_edite", "click", function(){
      var meid=$(this).attr('attr_id');
      //alert(meid);
      $(".tr_head1").remove();
      FVupdate(meid);
      FaddRow();
      $('.editehiden').val(meid);
	 $("#btnsaves").val("Update");
	 $(".c_inv_update").html("Update Cash Return:"+meid);
	 
    });

    $("#oppaymentmethod").on('change', function() {
      FVpaymentType();
      Fcalpayment();
    });

		$('body').delegate('.tr_studentcode', 'click', function(){
    
			FClear();
			var attr_enroid=$(this).attr('attr_enroid');
			var attr_stuid=$(this).attr('attr_stuid');
			var attr_paymethod=$(this).attr('attr_paymethod');
			var attr_term_sem_year_id=$(this).attr('attr_term_sem_year_id');
			var attr_student_num=$(this).attr('attr_student_num');
			var isaddnewinv=$(this).attr('isaddnewinv');
			var attr_is_pt=$(this).attr('attr_is_pt');
      var typeno=$(this).parent().parent().find('.a_').attr('data-typeno');
      $('.typeno_').val(typeno);

			$(".stuid").html(attr_student_num);
			
		 FVpaymentType();
			FaddHead();
			
			$("#oppaymentmethod").val(attr_paymethod);
			$("#h-studyperiod").val(attr_term_sem_year_id);
				
			$(".tr_addrow").remove();
			FaddRow();			
			FgetRowStuinfo(attr_enroid,attr_stuid,isaddnewinv,attr_is_pt);
		});
		$("body").delegate(".paymenttype_list","change",function(){
			
			var idselect =$(this).find('option:selected').val();
			var me =$(this).find('option:selected').text();
			var attr_price =$(this).find('option:selected').attr('attr_price')-0;
			$(this).parent().parent().find('.payment-unitprice').val(attr_price);
			$(this).parent().parent().find('.payment_des').val(me);
			// alert(me+'--'+idselect);
			 $('.claddnew').click();
			var attr_typefee =$(this).find('option:selected').attr('attr_typefee');
			var attr_datetran =$(this).find('option:selected').attr('attr_datetran');
			// alert(attr_typefee+'--'+attr_datetran);
			if(attr_typefee==1){
				$(this).parent().parent().find('.payment-note').val(attr_datetran);
				$(this).parent().parent().find('.is_bus').val(1);
			}else{				
				$(this).parent().parent().find('.payment-note').val("");				
				$(this).parent().parent().find('.is_bus').val("");
			}
			
			var countqty = $(this).parent().parent().find('.payment_des').val();
			var desid=0;
			$('.payment_des').each(function(i){
				if($(this).val()==countqty){
					desid++;
				}
			});			
			if(desid>1){
				toastr["warning"]("Data already exist ! Please try again");
				$(this).parent().parent().remove();
				FaddRow();	
				return false;
			}
			FaddRow();
		});

		$("body").delegate(".payment-amt,.payment-unitprice,.payment-dis").on('keyup', function() {
			Fcalpayment();
			
		});
		
		$('body').delegate("#a_delete", "click", function() {
			// if ($(".table1 tbody.tbody_sale tr").size() == 1) return;
			if(confirm('Are you sure remove?','Remove Item fee!')){
				$(this).parents("tr:first").remove();
				FaddRow();
				Fcalpayment();
			}
		});

   }); // ready ========
 
 // get schlevel ======
 function FVupdate(attr_typeno){
  $.ajax({
    url:"<?php echo base_url(); ?>index.php/student_fee/c_student_cash_return/Fupdate_Return",    
    data: {
     'attr_typeno':attr_typeno
     },
    type:"post",
    dataType:"json",
    async:false,
    success: function(data){        
    
    $("tbody.tbody_paymenttype").html(data.tr_loop);
		// Fcalpayment();
		toastr["warning"]("Please Carefully for update record !");
		$(".text_note").focus();
   	}
    });
};

function get_schlevel(programid){      
         $.ajax({
            url: '<?= site_url('student_fee/c_student_cash_return/get_schlevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid                
            },
            success: function(data){
               // schoollevel ======
               var opt = '';               
               if(data.schlevel.length > 0){
                  opt += '<option value="">All</option>';
                  $.each(data.schlevel, function(i, row){
                     opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#schlevelids').html(opt);

               // get year =======
               get_year($('#programid').val(), $('#schlevelids').val());   

               // rangelevel ===== 
               get_rangelevel($('#programid').val(), $('#schlevelids').val());      

            },
            error: function() {

            }
         });         
      }

      // get year =======
      function get_year(programid, schlevelid = ''){
         $.ajax({
            url: '<?= site_url('student_fee/c_student_cash_return/get_year') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelid                                  
            },
            success: function(data){
               var opt = '';
               
               if(data.year.length > 0){
                  opt += '<option value="">All</option>';
                  $.each(data.year, function(i, row){
                     opt += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#yearid').html(opt);

            },
            error: function() {

            }
         });
      }

      // get rangelevel =======
      function get_rangelevel(programid = '', schlevelid){
         $.ajax({
            url: '<?= site_url('student_fee/c_student_cash_return/get_rangelevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelid                                 
            },
            success: function(data){
               var opt = '';               
               if(data.rangelevel.length > 0){
                  opt += '<option value="">All</option>';
                  $.each(data.rangelevel, function(i, row){
                     opt += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#rangelevelid').html(opt);
            },
            error: function() {

            }
         });
      }

      // get class =======
      function get_class(programid = '', schlevelid, yearid = ''){
         $.ajax({
            url: '<?= site_url('student_fee/c_student_cash_return/get_class') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelid,
               yearid: yearid                                 
            },
            success: function(data){
               var opt = '';              
               if(data.class.length > 0){
                   opt += '<option value="">All</option>';
                  $.each(data.class, function(i, row){
                     opt += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#classid').html(opt);
            },
            error: function() {

            }
         });
      }

function FVdeinv(attr_type,deltypeno){		
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_invoice/FCDelinv",    
				data: {					
					'attr_type':attr_type,				
					'deltypeno':deltypeno				
					},
				type:"post",
				dataType:"json",
				async:false,
				success: function(data){					
					toastr["success"]('Record has been deleted!');
					 grid(1, $('#limit_record').val() - 0);
					$(".cl_btncancel").click();
			}
	   });		
	}
	
    function clear(){
        $('#termid').val('');
        $('#term').val('');      
        $('#term_kh').val('');
        $('#programid').val('');
        $('#yearid').html('');
        $('#schlevelids').html('');
        $('#start_date').val('');
        $('#end_date').val('');
        $('#isclosed').val(0);
        $('#isclosed').prop('checked', false);
        $('#dv_rangelevelid').html('');      
   }
   // sort ----------------------------------------------
   function sort(event){
       /*$(event.target)=$(this)*/ 
        this.sortby = $(event.target).attr("rel");
        if ($(event.target).attr("sorttype") == "ASC") {
            $('.sort').removeClass('cur_sort_up');
            $(event.target).addClass(' cur_sort_down');
            $(event.target).attr("sorttype", "DESC");
            this.sorttype = "ASC";
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass(' cur_sort_up');
        } else if ($(event.target).attr("sorttype") == "DESC") {
            $(event.target).attr("sorttype", "ASC");
            this.sorttype = "DESC";
            $('.sort').removeClass('cur_sort_up');
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass(' cur_sort_down');    
        }        
        grid(1, $('#limit_record').val(),this.sortby,this.sorttype);
     //    GetdataReceipt(1,$('#sort_num').val(),this.sortby,this.sorttype);
   }
   // grid =========
   function grid(current_page = 0, total_display = 0, sortby = '',sorttype = ''){
      var roleid = <?php echo $this->session->userdata('roleid');?>;
      var offset = ((current_page - 1) * total_display) - 0;
      var limit = total_display - 0;

      $.ajax({
         url: '<?= site_url('student_fee/c_student_cash_return/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            sortby:sortby,
            sorttype:sorttype,
            offset: offset,
            limit: limit,
            typeno: $('#typeno').val(),
            student_num: $('#student_num').val(),
            student_name: $('#student_name').val(),
            gender: $('#gender').val(),
            from_date: $('#from_date').val(),            
            schoolid: $('#schoolid').val(),
            programid: $('#programid').val(),
            schlevelid: $('#schlevelids').val(),            
            yearid: $('#yearid').val(),
            rangelevelid: $('#rangelevelid').val(),
            to_date: $('#to_date').val(),
            classid: $('#classid').val(),
            feetypeid: $('#feetypeid').val(),
            term_sem_year_id: $('#term_sem_year_id').val(),            
            report_type: $('#report_type').val(),			
            limit_record: $('#limit_record').val()
         },
         success: function(data) {
  			   $('#se_list tbody').html(data.tr); 

              var page = '';
              var total = '';
              if(data.totalRecord - 0 > 0){
                 // previous ====
                 page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;Prev.</button>';

                 // next =======
                 page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next&nbsp;<i class="glyphicon glyphicon-chevron-right"></i></button>';

              }

              $('#se_list tfoot').find('.pagination').html(page);
         },
         error: function(){

         }
      });
   }


   function FaddRow(){		
		var trrow='<tr class="tr_addrow">'+
					'<td>'+
						"<select id='paymenttype_list' class='form-control paymenttype_list gform' required=''><?php echo $oppaylist;?></select>"+
						'<input id="paytypeid" class="form-control payment_des" type="text" value="" style="text-align:left; display:none">'+
						'<input id="is_bus" class="form-control is_bus" type="text" value="2" style="text-align:left; display:none">'+
					'</td>'+
					'<td>'+
						'<input id="paytypeid" class="form-control payment-note" type="text" placeholder="" style="text-align:right">'+
					'</td>'+
					'<td>'+
						'<input id="numqty" class="form-control numqty" type="text" placeholder="" value="1"  style="text-align:right">'+
					'</td>'+
					
					'<td>'+
						'<input id="paytypeid" class="form-control payment-unitprice" type="text" placeholder=""  value="0"  style="text-align:right">'+
					'</td>'+
					'<td>'+
						'<input id="paytypeid" class="form-control payment-dis" type="text" placeholder=""  value="0"  style="text-align:right">'+
					'</td>'+
					'<td>'+
						'<input id="paytypeid" class="form-control payment-amt" type="text" placeholder=""  value="0"  style="text-align:right">'+
					'</td>'+
					"<td >&nbsp;&nbsp;&nbsp;<a id='a_delete' class='a_delete a_delete1' ><?php echo $photodel;?></a></td>"+
				'</tr>';
				
				var rowCount=$('#btl_paymenttype tr').length;
				if(rowCount<=12){
					var idLast=$('tbody.tbody_paymenttype tr:has(select.paymenttype_list):last select.paymenttype_list').val();		
					if(idLast != ''){
						$("tbody.tbody_paymenttype").append(trrow);
					}
				}
				
				$(".payment-amt,.amt_balance").attr('disabled','disabled');
				Fcalpayment();
				
	}
function FaddHead(){
		var trhead='<tr class="gform_1">'+
						'<th style="text-align:left !important">Description</th>'+
						'<th style="text-align:right !important">Note</th>'+
						'<th style="text-align:right !important">Qty</th>'+
						'<th style="text-align:right !important">Unit Price</th>'+
						'<th style="text-align:right !important">Dis(%)</th>'+
						'<th style="text-align:right !important">Amount</th>'+
						'<th style="text-align:center !important" class="addnew">'+
						"<a id='a_addnew' title='' data-placement='top' data-toggle='tooltip' href='javascript:;' data-original-title='Add New'><?Php // echo $addnew?></a>"+
						'</th>'+
					'</tr>'+
					'<tr class="gform_1 tr_head1">'+
						   '<td>'+
								'<input id="paytypeid" class="form-control schoolfee_des payment_des" type="text" placeholder="" value="ថ្លៃសិក្សា \ Tuition Fee" style="text-align:left">'+
								'<input id="paytypeid" class="form-control paymenttype_list" type="text" value="1" style="text-align:left; display:none">'+
								'<input id="is_bus" class="form-control is_bus" type="text" value="2" style="text-align:left; display:none">'+
							'</td>'+
							'<td>'+
								'<input id="schoolfee" class="form-control payment-note schoolfee_des" type="text" placeholder="" value=""  style="text-align:right">'+
								
							'</td>'+
							'<td>'+
								'<input id="numqty" class="form-control numqty" type="text" placeholder="" value="1"  style="text-align:right">'+
							'</td>'+
							'<td>'+
								'<input id="" class="form-control payment-unitprice schoolfee_prices" type="text" placeholder=""  value="0"  style="text-align:right">'+
							'</td>'+
							'<td>'+
								'<input id="paytypeid" class="form-control payment-dis" type="text" placeholder=""  value="0"  style="text-align:right">'+
								
							'</td>'+
							'<td>'+
								'<input id="paytypeid" class="form-control payment-amt" type="text" placeholder=""  value="0"  style="text-align:right">'+
							'</td>'+
							'<td style="text-align:center">'+
							"<a><?php echo "<a class='btn btn-xs btn-danger a_delete '><span id='a_delete' class='glyphicon glyphicon-trash'></span></a>"?></a>"+
							'</td>'+
					'+</tr>';
				$(".gform_1").remove();
				$("thead.thead_paymenttype").append(trhead);
	}

function FgetRowStuinfo(enrollid,studentid,isaddnewinv,attr_is_pt){		
		if(studentid>0){
					$.ajax({
                  url:"<?php echo base_url(); ?>index.php/student_fee/c_student_cash_return/Fcstudentinfo",    
                  data: {
                      'enrollid':enrollid,
                      'studentid':studentid,
                     
                      'attr_is_pt':attr_is_pt
								},
                            type:"post",
							dataType:"json",
                            async:false,
                            success: function(data){
								$('.photostu').html(data.image_stu);
								var ispayexist=(data.ispayexist);
								var is_pt=(data.is_pt);
								
								if(ispayexist==0){// it new and never paid
									$('.h-iscond').val(5);
									$(".h-iscond-save").val(1);									
									// $(".oppaymentmethod").removeAttr('disabled');
								}else{
									$('.h-iscond').val(data['getRow']['feetypeid']);
									$(".h-iscond-save").val(3);								
									$('.oppaymentmethod').val(data['getRow']['feetypeid']);
									// $(".oppaymentmethod").attr('disabled','disabled');
								}
								
								if(isaddnewinv==1){
									$('.mh_paymethod').val("");
									$('.mh_peroid').val("");
								}else{
									$('.mh_paymethod').val(data['getRow']['feetypeid']);
									$('.mh_peroid').val(data['getRow']['term_sem_year_id']);	
								}
								// $('.stuid').html(data['getRow']['studentid']);
								
								$('.namekh').html(data['getRow']['last_name_kh']+' '+data['getRow']['first_name_kh']);								
								$('.nameeng').html(data['getRow']['last_name']+' '+data['getRow']['first_name']);
								$('.email').html(data['getRow']['email']);
								$('.phone').html(data['getRow']['phone1']);								
								$('.sex').html(data['getRow']['gender']);	
								
								$('.h-program').val(data['getRow']['programid']);
								$('.h-schoollavel').val(data['getRow']['schlevelid']);
								$('.h-year').val(data['getRow']['year']);	
								$('.h-ranglavel').val(data['getRow']['rangelevelid']);	
								$('.h-class').val(data['getRow']['classid']);	
								$('.h-area').val(data['getRow']['zoon']);	
								
								$('.h-enrollid').val(data['getRow']['enrollid']);
								$('.h-studentid').val(data['getRow']['studentid']);
								
                               $('.program').html(data['getRow']['program']);
                               $('.schoollavel').html(data['getRow']['sch_level']);
                               $('.year').html(data['getRow']['sch_year']);
                               $('.ranglavel').html(data['getRow']['rangelevelname']);
                               $('.class').html(data['getRow']['class_name']);
                               
                               $('.paymethod').html(data['getRow']['schoolfeetype_kh']);
                              //  ('.paymenttype').html(data['arayOrder']['schoolfeetype_kh']);
                               $('.area').html(data['getRow']['area']);
                               $('.bus').html(data['getRow']['busfeetypid']);
                               $('.date').html(data['getRow']['trandate']);
								// alert(data.tr_inv);
								$('.t_bodyhis').html("");
								$('.t_bodyhis').html(data.trinv);
								$('.opstudytime').html(data.studytime);
								// alert(is_pt);
								$('.is_generalenglish').val(is_pt);	
								$('.h-isaddnewinv').val(isaddnewinv);	

                  // $('.typeno').val(typeno);

								Floadperiod();
								Fcalpayment();
								
                        }
                   });
		}else{
			//  $('#family_revenue').val('');
		}
	}


  function FVpaymentType(){
    var shlevelid=$('.h-schoollavel').val();
    var acandemicid=$('.h-year').val();
    var ranglevid=$('.h-ranglavel').val();
    var oppaymentmethod=$('.oppaymentmethod option:selected').val();
    var h_program=$(".h-program").val();
    var h_class=$(".h-class").val();

    $.ajax({
        url:"<?php echo base_url(); ?>index.php/student_fee/c_student_cash_return/FCgetPaymenttype",    
        data: {         
          'shlevelid':shlevelid,
          'acandemicid':acandemicid,
          'ranglevid':ranglevid,
          'h_program':h_program,
          'h_class':h_class,
          'paymentmethod':oppaymentmethod     
          },
        type:"post",
        dataType:"json",
        async:false,
        success: function(data){                
          $('#paymenttype').html(data.gpaymenttype);
          FVpaymentTypepice();
      }
     });    
  }

	function Floadperiod(){
		var shlevelid=$('.h-schoollavel').val();
		var acandemicid=$('.h-year').val();
		var ranglevid=$('.h-ranglavel').val();
		
		var oppaymentmethod=$(".mh_paymethod").val();
		var h_program=$(".h-program").val();
		var h_class=$(".h-class").val();
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/student_fee/c_student_cash_return/FCgetPaymenttype",    
				data: {					
					'shlevelid':shlevelid,
					'acandemicid':acandemicid,
					'ranglevid':ranglevid,
					'h_program':h_program,
					'h_class':h_class,
					'paymentmethod':oppaymentmethod			
					},
				type:"post",
				dataType:"json",
				async:false,
				success: function(data){								
					$('#paymenttype').html(data.gpaymenttype);
					Floadprice();
			}
	   });		
	}
	
	function Fcalpayment(){
		var totals=0;
		var amt_balance=$("#amt_balance").val()-0;
		var atm_total=$("#amt_total").val()-0;
		var amt_deposit=$("#amt_deposit").val()-0;
		
		$('input.payment-amt').each(function(i){						
		var tr=$(this).parent().parent();																					
		var payment_des=tr.find('.payment_des').val();
		var payment_note=tr.find('.payment-note').val();																					
		var payment_unitprice=tr.find('.payment-unitprice').val()-0;																				
		var payment_dis=tr.find('.payment-dis').val()-0;																			
		var numqty=tr.find('.numqty').val()-0;
		
		if(payment_dis>100){
			tr.find('.payment-dis').val(0);
			tr.find('.payment-dis').select();
			payment_dis=0;
			// return false;
			
		}
		if(payment_unitprice==""){
			tr.find('.payment-unitprice').val(0);
			payment_unitprice=0;
			// return false;
			
		}			
		var payment_amt=tr.find('.payment-amt').val()-0;
		var amt_line=((payment_unitprice*numqty)-((payment_dis/100)*(payment_unitprice)*numqty))-0;
		tr.find('.payment-amt').val(amt_line.toFixed(3));	
		totals+=amt_line;
		})
		$(".amt_balance,#amt_total").val(totals.toFixed(3));
		 var lastba=(totals-amt_deposit)-0;
		 $(".amt_balance").val(lastba.toFixed(3));
		 if(amt_deposit>atm_total){
			$(".amt_deposit").val(0); 
			$(".amt_deposit").select(); 			
			$(".amt_balance").val(totals.toFixed(3));
		}
	// alert(totals);
	}


  function Floadprice(){
  
    var shlevelid=$('.h-schoollavel').val();
    var acandemicid=$('.h-year').val();
    var ranglevid=$('.h-ranglavel').val();  
    var h_program=$(".h-program").val();
    var h_class=$(".h-class").val();
    var h_studentid=$(".h-studentid").val();
    var h_enrollid=$(".h-enrollid").val();
    
    var oppaymentmethod=$(".mh_paymethod").val();
    var paymenttype=$(".mh_peroid").val();
    $.ajax({
        url:"<?php echo base_url(); ?>index.php/student_fee/c_student_cash_return/FCpaymenttyp_price",    
        data: {         
          'shlevelid':shlevelid,
          'acandemicid':acandemicid,
          'ranglevid':ranglevid,
          'paymenttype':paymenttype,
          'h_program':h_program,
          'h_class':h_class,
          'h_studentid':h_studentid,
          'enrollid':h_enrollid,
          'paymentmethod':oppaymentmethod     
          },
        type:"post",
        dataType:"json",
        async:false,
        success: function(data){
          $('.oppaymentmethod').val($(".mh_paymethod").val());
          $(".paymenttype").val($(".mh_peroid").val());
          
          var fee=(data['rows']['fee'])-0;
          var book_price=(data['rows']['book_price'])-0;
          var admin_fee=(data['rows']['admin_fee'])-0;
          var amt=(fee+book_price+admin_fee); 
          // alert(amt);
          if(isNaN(amt)) {
            var amt = 0;
          }   
          $('.schoolfee_prices').val(amt.toFixed(3));
          $('.mh_prices').val(amt.toFixed(3));
          
          $('#oppaymentmethod').attr("disabled",true);
          $('#paymenttype').attr("disabled",true);
          // alert(amt);
          // $(".paymenttype").attr('disabled','disabled');
        }
     });    
  
}
  
function FVpaymentTypepice(){
    var shlevelid=$('.h-schoollavel').val();
    var acandemicid=$('.h-year').val();
    var ranglevid=$('.h-ranglavel').val();
    var oppaymentmethod=$('.oppaymentmethod option:selected').val();
    var h_program=$(".h-program").val();
    var h_class=$(".h-class").val();
    var h_studentid=$(".h-studentid").val();
    var h_enrollid=$(".h-enrollid").val();
    var paymenttype=$(".h-studyperiod").val();
    var h_iscond=$(".h-iscond").val();
    $.ajax({
        url:"<?php echo base_url(); ?>index.php/student_fee/c_student_cash_return/FCpaymenttyp_price",    
        data: {         
          'shlevelid':shlevelid,
          'acandemicid':acandemicid,
          'ranglevid':ranglevid,
          'paymenttype':paymenttype,
          'h_program':h_program,
          'h_class':h_class,
          'h_studentid':h_studentid,
          'enrollid':h_enrollid,
          'paymentmethod':oppaymentmethod     
          },
        type:"post",
        dataType:"json",
        async:false,
        success: function(data){
          var studentid=(data.studentid);
          var fee=(data['rows']['fee'])-0;
          var book_price=(data['rows']['book_price'])-0;
          var admin_fee=(data['rows']['admin_fee'])-0;
          var amt=(fee+book_price+admin_fee); 
          // alert(amt);
          if(isNaN(amt)) {
            var amt = 0;
          }   
          $('.schoolfee_prices').val(amt.toFixed(3));
          //-------------------------
                    
          if(h_iscond==5 && oppaymentmethod!=""){
            // alert(h_iscond+'='+me+"Add new by ....!"+me);
            // 1-insert new record by select for paymentmethod
            $(".h-iscond-save").val(1);
            // $(".paymenttype").removeAttr('disabled');
            
          }else if(h_iscond!=5 && oppaymentmethod!="" && h_iscond != oppaymentmethod){
            // alert(h_iscond+'='+me+"Need To  Change and update all ....!");
            // 1-update old recorded
            // 2-insert new record by select for paymentmethod
            
            if(confirm('Are you sure! Do you want to change Paymentmethod ?',' Change Paymentmethod!')){
              
            $(".h-iscond-save").val(2);
            $('.h-studyperiod').val("");
            // $(".paymenttype").removeAttr('disabled');
            }else{              
              Floadperiod();
              $(".h-iscond-save").val(3);
            }
          }else if(h_iscond!=5 && oppaymentmethod!="" && h_iscond == oppaymentmethod){
              //alert(h_iscond+'='+me+"Not Change....!");
              if($(".h-isaddnewinv").val()!=1){
                $(".h-iscond-save").val(3);
                $(".h-studyperiod").val($('.mh_peroid').val());
                $(".paymenttype").val($('.mh_peroid').val());
                $(".schoolfee_prices").val($('.mh_prices').val());
              }
              //$(".paymenttype").attr('disabled','disabled');
              // alert(10);
          }else if(oppaymentmethod==""){
            $(".h-iscond-save").val(3);
          }
        }// success
          
     });  // end $.ajax({ 
  }//
	
  function Fvbusfee(){
    $.ajax({
        url:"<?php echo base_url(); ?>index.php/student_fee/c_student_cash_return/FCbusfee",    
        data: {
          'oparea':$(".oparea").val()
          },
        type:"post",
        dataType:"json",
        async:false,
        success: function(data){                
        $('#opbusfeetype').html(data.busfe);
      }
     });
  };

  function FSavePayment(){
    var editehiden =$('.editehiden').val();
    var typeno_ = $('.typeno_').val();
    var totals=0;
    var amt_balance=$("#amt_balance").val();
    var atm_total=$("#atm_total").val()-0;
    var amt_dis=$("#amt_dis").val()-0;
    var arr_inv = new Array();
    var ii = 0;
  $('input.payment-amt').each(function(i){
    var tr=$(this).parent().parent();         
    var payment_unitprice=tr.find('.payment-unitprice').val()-0;  
    if(payment_unitprice!=""){                     
      var payment_des=tr.find('.payment_des').val();                                          
      var paymenttype_list=tr.find('.paymenttype_list').val();                                        
      var is_bus=tr.find('.is_bus').val();
      var payment_note=tr.find('.payment-note').val();        
      var payment_dis=tr.find('.payment-dis').val()-0;          
      var payment_amt=tr.find('.payment-amt').val()-0;        
      var numqty=tr.find('.numqty').val()-0;
      arr_inv[ii] ={'payment_des':payment_des,
              'payment_note':payment_note,
              'prices':payment_unitprice,
              'amt_dis':payment_dis,
              'numqty':numqty,
              'paymenttype_list':paymenttype_list,
              'is_bus':is_bus,
              'payment_amt':payment_amt
            };
      ii++;
    }// if(payment_unitprice!=""){
  });

   // console.log(editehiden);
   // return false;

      if(confirm("Are You Sure want to process.....")){
        if($('#oppaymentmethod').val()==""){
          $('.paymenttype').val("");
          $('.h-iscond-save').val("");  
        }else if($('#paymenttype').val()==""){
          $('.oppaymentmethod').val("");
          $('.h-iscond-save').val("");
        }
        $("#btnsaves").hide();
        $.ajax({
          url:"<?php echo base_url(); ?>index.php/student_fee/c_student_cash_return/FCsave",
          type:"post",
          dataType:"JSON",
          async:false,
          data: {
            arr_inv:arr_inv,
            atm_total:atm_total,
            amt_dis:amt_dis,
            trandate:$(".trandate").val(),
            duedate:$(".duedate").val(),
            stuid:$(".h-studentid").val(),
            enrollid:$(".h-enrollid").val(),
            opprpid:$(".h-program").val(),
            shlevelid:$(".h-schoollavel").val(),
            acandemicid:$(".h-year").val(),
            ranglevid:$(".h-ranglavel").val(),
            classe:$(".h-class").val(),
            h_area:$(".h-area").val(),
            is_condic:$(".h-iscond").val(),         
            h_isaddnewinv:$(".h-isaddnewinv").val(),
            h_iscond_save:$(".h-iscond-save").val(),
            oppaymentmethod:$(".oppaymentmethod").val(),
            paymenttype:$(".paymenttype").val(),
            oparea:$(".oparea").val(),
            opbusfeetype:$(".opbusfeetype").val(),
            amt_total:$(".amt_total").val(),
            amt_paid:$(".amt_deposit").val(),
            text_note:$(".text_note").val(),  
            opstudytime:$(".opstudytime").val(),

            is_generalenglish:$(".is_generalenglish").val(),    
            typeno_ :typeno_, 
            editehiden : editehiden,  
            amt_balance:amt_balance
            },
          
          success: function(data){
            $("#btnsaves").show();
            var nextTran=(data.nextTran);
            toastr["success"]('Success!'+nextTran);
              $('.clprint').click();
              $(".print_p1").attr("href","<?php echo site_url("student_fee/c_print_cash_return")?>?FCprint_inv="+nextTran);
             grid(1, 10); 
            FClear();
          }
        });
      }// if(confirm("Are You Sure want to process.....")){
  }


  function FClear(){
        $(".payment-note").val("");
        $("#oppaymentmethod").val("");
        $("#paymenttype").val("");
        $("#oparea").val("");
        $("#opbusfeetype").val("");
        $("#text_note").val("");
        $("#opstudytime").val("");
        
        $(".h-program").val("");
        $(".h-schoollavel").val("");
        $(".h-year").val("");
        $(".h-ranglavel").val("");
        $(".h-class").val("");
        $(".h-area").val("");
        $(".h-iscond").val("");
        $(".h-enrollid").val("");
        $(".h-studentid").val("");
        
        $(".payment-unitprice").val(0);   
        $(".payment-dis").val(0);   
        $(".payment-amt").val(0); 
        
        $("#atm_total").val(0);
        $("#amt_dis").val(0);
        $("#amt_balance").val(0);
        $(".numqty").val(1);
        $('.stuid').html("");
        $('.namekh').html("");
        $('.nameeng').html("");
        $('.sex').html("");
        $('.email').html("");
        $('.phone').html("");
        $('.program').html("");
        $('.schoollavel').html("");
        $('.year').html("");
        $('.class').html("");
        $('.is_generalenglish').val("");    
        $(".t_bodyhis").html("");
        $(".editehiden").val("");
        // $(".typeno_").val("");
        $(".isupdate_save").val("");
        $("#btnsaves").val("Save");
		    $(".c_inv_update").html("Create Cash Return");		
        $("#btnsaves").addClass("btn-primary");
        $(".paymenttype,.oppaymentmethod").removeAttr('disabled');

        $("#typeno_").val("");
    
  }
</script>