<div class="container-fluid" style="width:100% !important">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
            	<span class="icon">
                  <i class="fa fa-th"></i>
              </span>
                <strong>Assign Payment Method</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                  <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
               </a>
               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
               </a>
               <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
               </a>           
               
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
         <input type="hidden" name="termid" id="termid">
         
         <div class="col-sm-4" style="">         
            <div class="form-group">
               <label for="student_num">ID<span style="color:red"></span></label>
               <input type="text" name="student_num" id="student_num" class="form-control" placeholder="ID">
            </div>

            <div class="form-group">
               <label for="full_name">Name (KH)<span style="color:red"></span></label>
               <input type="text" name="full_kh" id="full_kh" class="form-control" placeholder="Name">
            </div>
			<div class="form-group">
               <label for="full_name">Name (Eng)<span style="color:red"></span></label>
               <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Name">
            </div>

            <div class="form-group">
               <label for="gender">Gender<span style="color:red"></span></label>
               <select name="gender" id="gender" class="form-control">
               	<option value="">All</option>
               	<option value="Female">Female</option>
               	<option value="Male">Male</option>
               </select>
               
               
            </div>

            <div class="form-group"​ style="display:none">
               <label for="from_date">From Date<span style="color:red"></span></label>
				   <div class="input-group">
					  <input type="text" name="from_date" id="from_date" class="form-control" placeholder="dd/mm/yyyy">
					  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
					</div>
            </div>

            <div class="form-group" style="display:none">
               <label for="to_date">To Date<span style="color:red"></span></label>
         		<div class="input-group">
					  <input type="text" name="to_date" id="to_date" class="form-control" placeholder="dd/mm/yyyy">
					  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
					</div>
            </div>     

         </div><!-- 1 -->

         <div class="col-sm-4" style="">            

            <div class="form-group" style="display:none">
               <label for="schoolid">School Info<span style="color:red"></span></label>
               <select name="schoolid" id="schoolid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->info->getschinfor() as $row) {
                        $opt .= '<option value="'.$row->schoolid.'">'.$row->name.'</option>';
                     }

                     echo $opt;        
                  ?>                  
               </select>
            </div>
            
            <div class="form-group">
               <label for="programid">Program<span style="color:red"></span></label>
               <select name="programid" id="programid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->p->getprograms() as $row) {
                        $opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group">
               <label for="schlevelid">School Level<span style="color:red"></span></label>
               <select name="schlevelids" id="schlevelids" class="form-control">
                  
               </select>
            </div>

            <div class="form-group">
               <label for="yearid">Year<span style="color:red"></span></label>
               <select name="yearid" id="yearid" class="form-control">

               </select>
            </div>

            <div class="form-group">
               <label for="rangelevelid">Rangelevel Name<span style="color:red"></span></label>
               <select name="rangelevelid" id="rangelevelid" class="form-control">

               </select>
            </div>        
            
         </div><!-- 2 -->

         <div class="col-sm-4">

         	<div class="form-group" style="">
               <label for="classid">Class<span style="color:red"></span></label>
               <select name="classid" id="classid" class="form-control">
                  <?php
                     // $opt = '';
                     // $opt .= '<option></option>';
                     // foreach ($this->c->allclass() as $row) {
                     //    $opt .= '<option value="'.$row->classid.'">'.$row->class_name.'</option>';
                     // }

                     // echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group" style="">
               <label for="feetypeid">Payment Method<span style="color:red"></span></label>
               <select name="feetypeid" id="feetypeid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->f->getfeetypes() as $row) {
                        $opt .= '<option value="'.$row->feetypeid.'">'.$row->schoolfeetype.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group" style="">
               <label for="feetypeid">Study Period<span style="color:red"></span>
               		
               </label>
               <select name="term_sem_year_id" id="term_sem_year_id" class="form-control">
                  <?php
                     // $opt = '';
                     // $opt .= '<option></option>';
                     // foreach ($this->f->getfeetypes() as $row) {
                     //    $opt .= '<option value="'.$row->feetypeid.'" '.($row->feetypeid - 0 == 1 ? 'selected="selected"' : '').'>'.$row->schoolfeetype.'</option>';
                     // }

                     // echo $opt;        
                  ?>
               </select>
            </div>

         	<div class="form-group" style="display:none">
               <label for="is_paid">Is paid<span style="color:red"></span></label>
               <select name="is_paid" id="is_paid" class="form-control"> 
                  <option value="">All</option>    
               	<option value="0">Unpaid</option>
                  <option value="1">Paid</option>          	
               </select>
            </div>

            <div class="form-group"  style="display:none">
               <label for="is_closed">Is active?<span style="color:red"></span></label>
               <select name="is_closed" id="is_closed" class="form-control">
               	<option value="0">Active</option>
               	<option value="1">Inactive</option>               	
               </select>
            </div>

         </div><!-- 3 -->      

         <div class="col-sm-7 col-sm-offset-5" style="display:none">
            <div class="form-group">
               <button type="button" class="btn btn-primary btn-sm btn_search" name="btn_search" id="btn_search" data-save='1'>Search</button>&nbsp; 
               <button type="button" class="btn btn-warning btn-sm btn_clear" name="btn_clear" id="btn_clear" data-save='1' style="display: none;">Clear</button>
               
            </div>            
         </div>

      </form>      
      <div class="col-sm-12">
         <div class="form-group" style="border-bottom: 1px solid #CCC;">&nbsp;</div>           
      </div>
   </div>   
   
   <div class="col-sm-12">
      <div class="form-group">
         <div class="table-responsive">
	         <div id="tab_print">
	            <table border="0"​ align="center" id="se_list" class="table table-hover">               
						<thead>
                <tr>
                  <?php
                     foreach ($thead as $th => $val) {
                        if ($th == 'No'){
                            echo "<th class='$val'width='3%'>".$th."</th>";
                        }else if($th == 'Photo'){
                            echo "<th class='$val'>".$th."</th>";
                        }else if($th == 'Status' ){
                            echo "<th class='$val'width='5%'>".$th."</th>";
                        }else{
                          echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
                        }
                     }
                  ?>
                </tr> 
						    <!-- <tr>
						      <th style="width: 2%;">No</th>
						      <th style="width: 5%;">Photo</th>
						      <th style="width: 20%;">Student Name</th>					      
						      <th style="width: 10%;">Program</th>
						      <th style="width: 10%;">School Level</th>
						      <th style="width: 10%;">Year</th>                        
						      <th style="width: 10%;">Range Level</th>
						      <th style="width: 10%;">Class</th> 
						      <th style="width: 10%;">Payment Method</th>
						      <th style="width: 5%;text-align: center;">Status</th>
						   </tr>  --> 
						  
						</thead>                
	               <tbody>
	               	
	               </tbody>
	               
	               <tfoot>
	                  <tr style="border-top: 2px solid #ddd;" class="remove_tag">
	                     <td colspan="5" style="padding-top: 32px;">
	                        <span id="sp_total">
                            <label for="limit_record">Limit<span style="color:red"></span></label>
                                <select name="limit_record" id="limit_record" class="" style="width:50px;">
                                 	<option value="10">10</option>
                                 	<option value="50">50</option>
                                 	<option value="100">100</option>
                                 	<option value="300">300</option>
                                 	<option value="500">500</option>
                                 	<option value="">All</option>
                                </select>
                          </span>
	                     </td>
	                     <td colspan="7" style="text-align: right;">
	                        <span style="display:none" id="sp_page" class="btn-group pagination" role="group" aria-label="..."></span>
	                     </td>
	                     
	                  </tr>
	               </tfoot>               
	            </table>
            </div>        
         </div>
      </div>

   </div>

</div>

<div class="modal" id="mdinvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="wrapper">
				<div class="clearfix" id="main_content_outer">
				    <div id="main_content">
					    <div class="result_info">
					    	<div class="col-sm-6">
                            	<span class="icon">
                                <i class="fa fa-th"></i>
                                </span>
					      		<strong>Assign Payment Method</strong>
					      	</div>
					      	<div class="col-sm-6" style="text-align: center">
					      		<strong>
					      			<center class='visit_error' style='color:red;'></center>
					      		</strong>	
					      	</div>
					    </div>
                        <div class="col-xs-12" style="border:#CCC solid 1px;margin:0px;height:100%;">
                             <div class="col-sm-2 img-circle img-responsive photostu" style="border:#e7e7e7 solid 0px; padding:3px; text-align:center !important">
								
                             </div>
                              
                             <div class="col-sm-3" style="height:100% ;border-right:#CCC solid 1px;align-items: stretch;">
                                        <table style="width:100% !important"> 
                                            <tr class="lihideinfo">
                                            	<td style="display:none">
                                                <input type="text" class="h-program" value="" />
                                                <input type="text" class="h-schoollavel" value="" />
                                                <input type="text" class="h-year" value="" />
                                                <input type="text" class="h-ranglavel" value="" />
                                                <input type="text" class="h-class" value="" />
                                                <input type="text" class="h-area" value="" />
                                                
                                                <input type="text" class="h-enrollid" value="" />
                                                <input type="text" class="h-studentid" value="" />
                                                <input type="text" class="h-iscond" value="" />
                                                <input type="text" class="h-iscond-save" value="" />
                                               
                                       		 </td>
                                                <td>ID
                                                
                                                </td><td>:</td><td><span class="stuid" style="text-align:center !important"></span></td>
                                            </tr>
                                            <tr class="lihideinfo">
                                                <td>Full Name(Kh)</td><td>:</td><td><span class="namekh"></span></td>
                                            </tr>
                                            <tr class="lihideinfo">
                                                <td>Full Name(Eng)</td><td>:</td><td> <span class="nameeng" style="text-align:center !important"></span></td>
                                            </tr>
                                            
                                            <tr class="lihideinfo">
                                                <td>Gender</td><td>:</td><td><span class="sex"></span></td>
                                            </tr>
                                            <tr class="lihideinfo">
                                                <td>E-Mail</td><td>:</td><td><span class="email"></span></td>
                                            </tr>
                                            
                                            <tr class="lihideinfo">
                                                <td>Telephone </td><td>:</td><td><span class="phone"></span></td>
                                            </tr>
                                           
                                        </table>    
                              </div>
                                    
                              <div class="col-sm-4" style="height:100% ;border-right:#CCC solid 1px;align-items: stretch;">
                                        <table style="width:100% !important" class="gform"> 
                                    <tr class="lihide">
                                    	
                                        <td>Program</td><td>:</td>
                                        	
                                         <td class="program">
                                          
                                              <?php //echo $opprogram?>
                                         </td>
                                    </tr>
                                    
                                    <tr class="lihide">
                                        <td>School&nbsp;Level</td><td>:</td>
                                        <td class="schoollavel">

                                          <?php  //echo $opget_sle?>

                                       </td>
                                    </tr>
                                   <tr class="lihide">
                                        <td>Academic&nbsp;Year</td><td>:</td>
                                         <td class="year">
                                          <?php  //echo $opgetyears?>

                                         </td>
                                    </tr>
                                    
                                    <tr class="lihide">
                                        <td>Rang&nbsp;Level</td><td>:</td>
                                        <td class="ranglavel">
                                          <?php //echo $oprang?>

                                       </td>
                                    </tr>
                                    <tr class="lihide">
                                        <td>Class</td><td>:</td>
                                        <td class="class">
                                          <?php  //echo $opclass?>
                                        </td>
                                    </tr>
                                </table>
                             </div>
                                   
                              <div class="col-sm-3" style="height:100% ;align-items: stretch;">
                                        <table style="width:100% !important" class=""> 
                                           <tr class="lihide gform">
                                                <td><span style="color:#F00; font-weight:bold">From&nbsp;Payment&nbsp;Method</span></td>
                                                <td>:</td>
                                                <td>
                                                <select name="op_frompayment" id="op_frompayment" class="form-control op_frompayment">
                                                <?php
                                                $optpay = '';
                                                $optpay .= '<option value="">All</option>';
                                                foreach ($this->f->getfeetypes() as $row) {
                                                $optpay .= '<option value="'.$row->feetypeid.'">'.$row->schoolfeetype.'</option>';
                                                }
                                                
                                                echo $optpay;        
                                                ?>
                                                </select>
                                                </td>
                                            </tr>
                                             
                                           <tr class="lihide gform">
                                                <td><span style="color:#239d60; font-weight:bold">To&nbsp;Payment&nbsp;Method</span></td>
                                                <td>:</td>
                                                <td style="display:none">
                                                	<input type="text" class="mh_paymethod" id="mh_paymethod" />
                                                    <input type="text" class="mh_peroid" id="mh_peroid" />
                                                    <input type="text" class="mh_prices" id="mh_prices" />
                                                </td>
                                                <td>
                                                    	
                                                <select name="oppaymentmethod" id="oppaymentmethod" class="form-control oppaymentmethod">
                                                <?php
                                                $optpay = '';
                                                $optpay .= '<option value="">All</option>';
                                                foreach ($this->f->getfeetypes() as $row) {
                                                $optpay .= '<option value="'.$row->feetypeid.'">'.$row->schoolfeetype.'</option>';
                                                }
                                                
                                                echo $optpay;        
                                                ?>
                                                </select>
                                             </td>
                                            </tr>
                                            
                                            <tr class="lihide gform">
                                                <td>Note</td><td>:
                                                	<input type="hidden" class="h-studyperiod" id="h-studyperiod" />
                                                </td>
                                                <td>
                                                	<textarea id="text_note" class="text_note form-control"></textarea>
                                                </td>
                                             </tr>
                                             
                                            <tr class="lihide" style="display:nones">
                                                <td>Date</td><td>:</td>
                                               <td>
                                                            <div class="input-group" data-date-format="dd-mm-yyyy">
                                                            <input id="duedate" class="form-control duedate" type="text" placeholder="dd-mm-yyyy" name="duedate">
                                                            <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"> </span>
                                                            </span>
                                                            </div>
                                                        </td>
                                            </tr>
                                        </table>
                             </div>
                             
                           </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <table id="" class="table table-hover" cellspacing="0" cellpadding="0" border="0" align="center" ​="">
                                        <thead class="thead_paymenttype">
                                        
                                        </thead>
                                        <tbody class="">
                                        
                                        </tbody> 
                                        <tfoot class=" to_menthod">
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
					</div> 
			    </div>
			</div> 
            <div class="modal-footer">   
            	<input data-dismiss="modal" id="btnsaves" class="btn btn-primary" type="button" value="Update" name="btnsaves"/>
                 <input data-dismiss="modal" id="btncancel" class="btn btn-warning cl_btncancel" type="button" value="Cancel" name="btncancel"/>
            </div>
          
        </div> <!-- /.modal-content --> 
       
      
    </div>
      <!-- /.modal-dialog -->
</div> 

<div class="modal" id="dialog_print" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="wrapper">
				<div class="clearfix" id="main_content_outer">
				    <div id="main_content">
					    <div class="result_info">
					    	<div class="col-sm-6">
                                <span class="icon">
                                    <i class="fa fa-th"></i>
                                </span>
					      		<strong>Print Report (s)</strong>
					      	</div>
					      	<div class="col-sm-6" style="text-align: center">
					      		<strong>
					      			<center class='visit_error' style='color:red;'></center>
					      		</strong>	
					      	</div>
					    </div>
                        <div class="col-xs-12" style="border:#CCC solid 1px;margin:0px;height:100%;">                                   
                              
                                <table style="text-align:center" align="center">
                                	<tr style="line-height:40px;">
                                    	
                                        <td>
                                        	<a  target="_blank" href="" class="print_p1">preview Invoice</a>
                                        </td>
                                    </tr>
                                    <tr style="line-height:40px; display:none" class="printreceipt">
                                        <td>
                                        	<a  target="_blank" href="" class="print_p2">preview Receipt</a>
                                        </td>
                                    </tr>
                                    
                                </table>                             
                           </div>
                           
					</div> 
			    </div>
			</div> 
            <div class="modal-footer"> 
                 <input data-dismiss="modal" id="btncancel" class="btn btn-warning" type="button" value="Close" name="btncancel"/>
            </div>
        </div>                       <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
</div> 

	<?php
   
		$oppaylist="";
		$oppaylist="<option attr_price='0' value=''></option>";
		foreach ($this->otherfee->FMotherfee() as $rowotherfee) {
			$oppaylist.="<option attr_typefee='".$rowotherfee->typefee."' attr_datetran='".$this->green->convertSQLDate($rowotherfee->fromdate)."  -  ". $this->green->convertSQLDate($rowotherfee->todate)."' attr_price='".$rowotherfee->prices."' value='".$rowotherfee->otherfeeid."'>".$rowotherfee->otherfee."</option>";
		}
      $photodel="<img src='".site_url('../assets/images/icons/delete.png')."'/>";
    ?>
<style type="text/css">
  table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }

    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

	.td_history{ font-weight:bold; font-size:12px}
	.tbl_history{ padding:20px !important;}
	#se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	#mdinvoice .modal-dialog  {width:99% !important;}
			
	
	a{
		cursor: pointer;
	}
	
	.datepicker {z-index: 9999;}
	.lihide{ line-height:35.3px !important;}
	.lihidephoto{ line-height:25px !important; color:#00F !important}
	.lihideinfo{ line-height:30px !important;}
	.fiel_head{ font-size:10px !important; line-height:25.3px !important;}
	#amt_balance,#atm_total,#amt_dis,#amt_total,#amt_deposit{ 
	color:#900 !important;
	font-size:17px !important;
	font-weight:bold;	
	
	}
.invorder{ font-size:14px; color:#00F !important}
.panding{ font-size:14px; color:#00F !important}
.partial{ font-size:14px; color:#C00 !important}
.fclassact{ font-size:14px; color:#000 !important}
.completed{ font-size:14px; color:#999 !important}


#oppaymentmethod{


background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 2px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #555;
    display: block;
    font-size: 12px;
    height: 30px;
    line-height: 1.42857;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;
}
.cl_inv,.tr_inv{ border-top: solid 1px #CCC !important;}
	 
.payment-amt{ 
	color:#906 !important;
	font-size:14px !important;
	font-weight:bold;	
	
	}
	#mdinvoice .modal-dialog  {width:99% !important;}
	.paymentlist{ cursor:pointer;}
	
 .create_inv{
    background-color: #239d60;
    background-image: none;
    border: 1px solid #e5e5e5;
    box-shadow: none;
    color: #fff;
    left: 7px;
    position: relative;
    top: 7px;
	cursor:pointer;
	
	border-radius: 10px;
    font-size: 12px;
    font-weight: normal;
    line-height: 18px;
    padding: 3.6px 9px;
}
.create_inv:hover{
	background-color:#F00;
	color:#000;
	cursor:pointer;
	
}
.addnew_inv{
    background-color: #CCC;
    background-image: none;
    border: 1px solid #e5e5e5;
    box-shadow: none;
    color: #fff;
    left: 7px;
    position: relative;
    top: 7px;
	cursor:pointer;
	
	border-radius: 10px;
    font-size: 12px;
    font-weight: normal;
    line-height: 18px;
    padding: 3.6px 9px;
}
.addnew_inv:hover{
	background-color:#239d60;
	color:#FFF;
	cursor:pointer;
	
}
.tr_aid {
    background-color: #CCC;
    background-image: none;
    border: 1px solid #e5e5e5;
    box-shadow: none;
    color: #fff;
    left: 7px;
    position: relative;
    top: 7px;
	border-radius: 10px;
    font-size: 12px;
    font-weight: normal;
    line-height: 18px;
    padding: 3.6px 9px;
}

.valign_top{ vertical-align:top !important;}
.a_delete1{ padding-left:0px !important;}
</style>
<span data-toggle="modal" data-target="#dialog_print" class="clprint" ></span>
<script type="text/javascript">
   $(function(){
		$(".res_dob,.mem_dob,#leave_sch_date,#boarding_date,#trandate,.input_validate,#duedate").datepicker({
      		language: 'en',
      		pick12HourFormat: true,
      		format:'dd-mm-yyyy'
    	});
		$("#year,#schlevelid").hide();
   	// get ======= 
      $('body').delegate('#feetypeid', 'change', function(){
      	var feetypeid = $(this).val() - 0;

      	$.ajax({
            url: '<?= site_url('student_fee/c_assign_payment_method/get_paymentType') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               feetypeid: feetypeid,
               programid: $('#programid').val(),
               schlevelid: $('#schlevelids').val(),
               yearid: $('#yearid').val()
                               
            },
            success: function(data){
            	// console.log(data);

               // schoollevel ======
               var opt = '';
               opt += '<option value="">All</option>';
               if(data.length > 0){
                  $.each(data, function(i, row){
                     opt += '<option value="'+ row.term_sem_year_id +'">'+ row.period +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#term_sem_year_id').html(opt);             

            },
            error: function() {

            }
         });
      });

      // get ======= 
      $('body').delegate('#programid', 'change', function(){
         if($(this).val() - 0 > 0){
            get_schlevel($(this).val());
         }else{
            $('#schlevelids').html('');
            $('#yearid').html('');            
            $('#rangelevelid').html('');
            $('#classid').html('');
         }
                    
      });
      $('body').delegate('#schlevelids', 'change', function(){
         if($(this).val() - 0 > 0){
            get_year($('#programid').val(), $(this).val());
         }else{
            $('#yearid').html('');            
            $('#rangelevelid').html('');
            $('#classid').html('');
         }
      });
      $('body').delegate('#yearid', 'change', function(){
         if($(this).val() - 0 > 0){
            get_rangelevel($(this).val());
         }else{          
            $('#rangelevelid').html('');
            $('#classid').html('');
         }
         
      });

      // get schlevel ======
      function get_schlevel(programid = ''){      
         $.ajax({
            url: '<?= site_url('student_fee/c_assign_payment_method/get_schlevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid                
            },
            success: function(data){

               // schoollevel ======
               var opt = '';
               opt += '<option value="">All</option>';
               if(data.schlevel.length > 0){
                  $.each(data.schlevel, function(i, row){
                     opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#schlevelids').html(opt);

               // get year =====
               get_year($('#programid').val(), $('#schlevelids').val());                  

            },
            error: function() {

            }
         });         
      }

      // get year =======
      function get_year(programid = '', schlevelids = ''){
         $.ajax({
            url: '<?= site_url('student_fee/c_assign_payment_method/get_year') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelids                                  
            },
            success: function(data){
               var opt = '';
               opt += '<option value="">All</option>';
               if(data.year.length > 0){
                  $.each(data.year, function(i, row){
                     opt += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#yearid').html(opt);


               // class ========
               var opt1 = '';
               opt1 += '<option value="">All</option>';
               if(data.class.length > 0){
                  $.each(data.class, function(i, row){
                     opt1 += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                  });
               }else{
                  opt1 += '';
               }
               $('#classid').html(opt1);

               // range level ======
               get_rangelevel($('#yearid').val());

            },
            error: function() {

            }
         });
      }

      // get rangelevel =======
      function get_rangelevel(yearid = ''){
         $.ajax({
            url: '<?= site_url('student_fee/c_assign_payment_method/get_rangelevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               yearid: yearid                                  
            },
            success: function(data){
               var opt = '';
               opt += '<option value="">All</option>';
               if(data.rangelevel.length > 0){
                  $.each(data.rangelevel, function(i, row){
                     opt += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#rangelevelid').html(opt);
            },
            error: function() {

            }
         });
      }

   	$('#from_date').datepicker({
   		format: 'dd/mm/yyyy',
    		startDate: '-3d'
   	});
   	$('#to_date').datepicker({
		format: 'dd/mm/yyyy',
		startDate: '-3d'
	});      

      // tooltip ======
      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // print =======
      $("#a_print").on("click", function () {
            var htmlToPrint = '' +
                '<style type="text/css">' +
                'table th, table td {' +
                'border:1px solid #000 !important;' +
                'padding;0.5em;' +
                '}' +
                '</style>';
            var title = "Assign Payment Method";
            var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "");
            var export_data = $("<center>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
            export_data += htmlToPrint;
            gsPrint(title, export_data);
        });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Assign Payment Method";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });      

      // chk =======
      $('body').delegate('#isclosed', 'click', function(){
         $(this).is(':checked') ? $(this).val(1) : $(this).val(0);
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#semester').select();
         $('#save').html('Update');
         $('#save_next').show();
         $('#f_save').parsley().destroy();
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#term').select();
         $('#save').html('Update');
         $('#save_next').show();
      });

      // init. =====
      grid(1, 10);

      // search ========
      $('body').delegate('#btn_search', 'click', function(){
         grid(1, 10);
      });
	   $('body').delegate('#gender,#programid,#schlevelids,#yearid,#rangelevelid,#classid,#feetypeid,#term_sem_year_id,#is_paid,#limit_record', 'change', function(){
         grid(1, 10);
      });
      $('body').delegate('#student_num,#full_kh,#full_name', 'keyup', function(){
         grid(1, 10);
      });


      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, 10);
      });
	  
	
	$('body').delegate('.tr_studentcode', 'click', function(){
			FClear();
			var attr_schlevel=$(this).attr('attr_schlevel');
			var attr_class=$(this).attr('attr_class');
			
			var attr_stuid=$(this).attr('attr_stuid');
			var attr_paymethod=$(this).attr('attr_paymethod');
			var attr_term_sem_year_id=$(this).attr('attr_term_sem_year_id');
			// alert(attr_schlevel+'--'+attr_class);
			var attr_student_num=$(this).attr('attr_student_num');
			var attr_coninv=$(this).attr('attr_coninv');
			
			$(".stuid").html(attr_student_num);
			
			// FVpaymentType();
			
			FaddHead();
			if(attr_coninv==1){// add new inv
				$(".tr_head1").remove();
				$(".oppaymentmethod").val("");
				$(".paymenttype,.h-studyperiod,.mh_paymethod,.mh_peroid").val("");
				$(".oppaymentmethod,#op_frompayment").attr('disabled','disabled');
			}else{
				// alert(attr_paymethod);
				$("#op_frompayment").val(attr_paymethod);
				// $("#h-studyperiod").val(attr_term_sem_year_id);
				$(".oppaymentmethod,.paymenttype").removeAttr('disabled');
				$("#op_frompayment").attr('disabled','disabled');
			}
			$(".tr_addrow").remove();
			FaddRow();			
			FgetRowStuinfo(attr_schlevel,attr_class,attr_stuid,attr_coninv);
		});
		
		$("body").delegate(".payment-amt,.payment-unitprice,.payment-dis").on('keyup', function() {
			Fcalpayment();
			
		});
		
		$("body").delegate(".paymenttype_list","change",function(){
			
			// var idselect =$(this).find('option:selected').val();
			var me =$(this).find('option:selected').text();
			var attr_price =$(this).find('option:selected').attr('attr_price')-0;
			$(this).parent().parent().find('.payment-unitprice').val(attr_price);
			$(this).parent().parent().find('.payment-des').val(me);
			
			var attr_typefee =$(this).find('option:selected').attr('attr_typefee');
			var attr_datetran =$(this).find('option:selected').attr('attr_datetran');
			// alert(attr_typefee+'--'+attr_datetran);
			if(attr_typefee==1){
				$(this).parent().parent().find('.payment-note').val(attr_datetran);
				$(this).parent().parent().find('.is_bus').val(1);
			}else{				
				$(this).parent().parent().find('.payment-note').val("");				
				$(this).parent().parent().find('.is_bus').val("");
			}
			
			var countqty = $(this).parent().parent().find('.payment-des').val();
			var desid=0;
			$('.payment-des').each(function(i){
				if($(this).val()==countqty){
					desid++;
				}
			});			
			if(desid>1){
				toastr["warning"]("Data already exist ! Please try again");
				$(this).parent().parent().remove();
				FaddRow();	
				return false;
			}
			FaddRow();
		});
		
		$("body").delegate(".payment-des,.payment-unitprice","keyup",function(){
			FaddRow();
		});
		$('body').delegate("a#a_delete", "click", function() {
			// if ($(".table1 tbody.tbody_sale tr").size() == 1) return;
			
			if(confirm('Are you sure remove?','Remove Item fee!')){
				$(this).parents("tr:first").remove();
				FaddRow();
				Fcalpayment();
			}
		});
		
		$("#btnsaves").on('click', function() {
			Fcalpayment();
			FSavePayment();
		});
		$("#opprpid").on('change', function() {
			var me=$(this).val();
			FvgetOp(me);
		});
		
		$("#shlevelid").on('change', function() {
			Fvshlev();
		});
		$(".oparea").on('change', function() {
			// alert($(this).val());
			Fvbusfee();
		});
		$("#opbusfeetype").on('change', function() {
			// FVcalarea();
		});
		$("#oppaymentmethod").on('change', function() {
			var oppaymentmethod=$('.oppaymentmethod option:selected').val();
			var op_frompayment=$('.op_frompayment option:selected').val();
			// alert(oppaymentmethod+'----'+op_frompayment);
			if(oppaymentmethod==op_frompayment){
				alert("Please chose other payment Method");
				$('.oppaymentmethod').val("");
			}
			FVpaymentType();
			Fcalpayment();
			FcoundTo();
		});
		
		$("#paymenttype").on('change', function() {
			if($(".oppaymentmethod").val()==""){
				$(this).val("");
			}
			$(".h-studyperiod").val($(this).val());	
				
			Fcalpayment();
		});
		
		
		$('body').delegate("a#a_delete_inv", "click", function() {
			if(confirm('Are you sure want to delete ?','Data has deleted!')){
				var meid=$(this).attr('attr_id');
				FVdeinv(meid);
			}
			
		});
		$('body').delegate(".ch_to", "click", function() {
			Fearch();
			FcoundTo();
			
		});
		$('body').delegate(".schoolfee_prices", "keyup", function() {
			var pmenthod=$(".oppaymentmethod").val();
			// alert(pmenthod);
			if(pmenthod==""){
				alert("Please Select Paymen method first...!");
				$(this).val(0).select();
			}
		});
   }); // ready ========
//==================================Main==========================================================================================================================================================================================================================================================
function Fvbusfee(){
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/student_fee/c_assign_payment_method/FCbusfee",    
				data: {
					'oparea':$(".oparea").val()
					},
				type:"post",
				dataType:"json",
				async:false,
				success: function(data){								
				$('#opbusfeetype').html(data.busfe);
			}
	   });
	};
function FVdeinv(deltypeno){		
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/student_fee/c_assign_payment_method/FCDelinv",    
				data: {					
					'deltypeno':deltypeno				
					},
				type:"post",
				dataType:"json",
				async:false,
				success: function(data){
					
					toastr["success"]('Record has been deleted!');
					 grid(1, 10);
					$(".cl_btncancel").click();
			}
	   });		
	}

	
function FVpaymentType(){
		var shlevelid=$('.h-schoollavel').val();
		var acandemicid=$('.h-year').val();
		var ranglevid=$('.h-ranglavel').val();
		var oppaymentmethod=$('.oppaymentmethod option:selected').val();
		var h_program=$(".h-program").val();
		var h_class=$(".h-class").val();
		
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/student_fee/c_assign_payment_method/FCgetPaymenttype",    
				data: {					
					'shlevelid':shlevelid,
					'acandemicid':acandemicid,
					'ranglevid':ranglevid,
					'h_program':h_program,
					'h_class':h_class,
					'paymentmethod':oppaymentmethod			
					},
				type:"post",
				dataType:"json",
				async:false,
				success: function(data){								
					$('.to_menthod').html(data.gpaymenttype);
			}
	   });		
	}

	
function FcodeExist(meid){
				var result=0;
				var arrItemSelected=[];		
				$("input.payment-des").each(function(i){
					if($(this).val()!=""){
						arrItemSelected[i]=$(this).val();
					}
				});			
				if($.inArray(meid,arrItemSelected)!=-1){
					result=1;
				}
				return result;
		}
		
function Fcalpayment(){
		var totals=0;
		var amt_balance=$("#amt_balance").val()-0;
		var atm_total=$("#amt_total").val()-0;
		var amt_deposit=$("#amt_deposit").val()-0;
		
	$('input.payment-amt').each(function(i){						
		var tr=$(this).parent().parent();																					
		var payment_des=tr.find('.payment-des').val()-0;
		var payment_note=tr.find('.payment-note').val()-0;																					
		var payment_unitprice=tr.find('.payment-unitprice').val()-0;																				
		var payment_dis=tr.find('.payment-dis').val()-0;																			
		var numqty=tr.find('.numqty').val()-0;
		
		if(payment_dis>100){
			tr.find('.payment-dis').val(0);
			tr.find('.payment-dis').select();
			payment_dis=0;
			// return false;
			
		}
		if(payment_unitprice==""){
			tr.find('.payment-unitprice').val(0);
			payment_unitprice=0;
			// return false;
			
		}
		
						
		var payment_amt=tr.find('.payment-amt').val()-0;
		
		var amt_line=((payment_unitprice*numqty)-((payment_dis/100)*(payment_unitprice)*numqty))-0;
		tr.find('.payment-amt').val(amt_line.toFixed(3));	
		totals+=amt_line;
		})
		$(".amt_balance,#amt_total").val(totals.toFixed(3));
		 var lastba=(totals-amt_deposit)-0;
		 $(".amt_balance").val(lastba.toFixed(3));
		 if(amt_deposit>atm_total){
			$(".amt_deposit").val(0); 
			$(".amt_deposit").select(); 			
			$(".amt_balance").val(totals.toFixed(3));
		}
		
		
	// alert(totals);
	}
function FcoundTo(){
	var len=0;
	$('input.ch_to:checked').each(function(i){
		len=$(this).length;
	});
	
	if(len==0){// never need payment method		
		$("#btnsaves").attr('disabled','disabled'); 
		$("#btnsaves").val("Update");
	}else{		
		$("#btnsaves").removeAttr('disabled');
		$("#btnsaves").val("Update");
	}
	// alert(len);
}
function Fearch(){
	
	$('input.ch_to').each(function(i){
		var tr=$(this).parent().parent();		
		if($(this).is(":checked")){
			tr.find('.ispaid').val(0);// will pay
		}else{
			tr.find('.ispaid').val(1);// paid
		}
	});
	
}
function FSavePayment(){
		var totals=0;
		var amt_balance=$("#amt_balance").val()-0;
		var atm_total=$("#atm_total").val()-0;
		var amt_dis=$("#amt_dis").val()-0;
		
		var arr_inv = Array();			
		$('input.ch_to').each(function(i){
			var tr=$(this).parent().parent();															
			var term_sem_year_id=tr.find('.term_sem_year_id').val();																					
			var payment_type=tr.find('.payment_type').val();																					
			var ispaid=tr.find('.ispaid').val();			
				arr_inv[i]={
					'ispaid':ispaid,
					'payment_type':payment_type,
					'term_sem_year_id':term_sem_year_id
				};
		});				
		var attr_all =Array();
		$('input.from_to').each(function(i){		
			var tr=$(this).parent().parent();														
			var term_sem_year_id=tr.find('.term_sem_year_id').val();																					
			var payment_type=tr.find('.payment_type').val();																					
			var ispaid=tr.find('.ispaid').val();																								
			var description=tr.find('.description').val();			
				attr_all[i]={
						'ispaid':ispaid,
						'payment_type':payment_type,
						'term_sem_year_id':term_sem_year_id,
						'description':description,
						'from_to':$(this).val()
						};
		});
			if(confirm("Are You Sure want to process.....")){
				if($('#oppaymentmethod').val()==""){
					$('.paymenttype').val("");
					$('.h-iscond-save').val("");	
				}else if($('#paymenttype').val()==""){
					$('.oppaymentmethod').val("");
					$('.h-iscond-save').val("");
				}
				$("#btnsaves").hide();
					$.ajax({
					url:"<?php echo base_url(); ?>index.php/student_fee/c_assign_payment_method/FCsave",
					data: {
						arr_inv:arr_inv,
						attr_all:attr_all,
						atm_total:atm_total,
						amt_dis:amt_dis,
						trandate:$(".trandate").val(),
						duedate:$(".duedate").val(),
						stuid:$(".h-studentid").val(),
						enrollid:$(".h-enrollid").val(),
						
						opprpid:$(".h-program").val(),
						shlevelid:$(".h-schoollavel").val(),
						acandemicid:$(".h-year").val(),
						ranglevid:$(".h-ranglavel").val(),
						classe:$(".h-class").val(),
						h_area:$(".h-area").val(),
						is_condic:$(".h-iscond").val(),
						h_iscond_save:$(".h-iscond-save").val(),
						oppaymentmethod:$(".oppaymentmethod").val(),
						op_frompayment:$(".op_frompayment").val(),
						paymenttype:$(".paymenttype").val(),
						oparea:$(".oparea").val(),
						opbusfeetype:$(".opbusfeetype").val(),
						amt_total:$(".amt_total").val(),
						amt_paid:$(".amt_deposit").val(),
						text_note:$(".text_note").val(),		
								
						amt_balance:amt_balance
						},
					type:"post",
					dataType:"json",
					async:false,
					success: function(data){
						$("#btnsaves").show();
						var nextTran=(data.nextTran);
						toastr["success"]('Success!'+nextTran);
						// alert("Record has bee saved! "+nextTran);
						if((data.print_receipt)==1){
							$(".printreceipt").show();
						}else{
							$(".printreceipt").hide();
						}
							$('.clprint').click();
							$(".print_p1").attr("href","<?php echo site_url("student_fee/c_student_print_invoice")?>?FCprint_inv="+nextTran);
							$(".print_p2").attr("href","<?php echo site_url("student_fee/c_student_print_receipt")?>?param_reno="+nextTran);
							
						 grid(1, 10);	
						FClear();
					}
					});
			}// if(confirm("Are You Sure want to process.....")){
		
	}
function FaddRow(){
		
		var trrow='<tr class="tr_addrow">'+
					'<td>'+
						"<select id='paymenttype_list' class='form-control paymenttype_list gform' required=''><?php echo $oppaylist;?></select>"+
						'<input id="paytypeid" class="form-control payment-des" type="text" value="" style="text-align:left; display:none">'+
						'<input id="is_bus" class="form-control is_bus" type="text" value="" style="text-align:left; display:none">'+
						
					'</td>'+
					'<td>'+
						'<input id="paytypeid" class="form-control payment-note" type="text" placeholder="" style="text-align:right">'+
					'</td>'+
					'<td>'+
						'<input id="numqty" class="form-control numqty" type="text" placeholder="" value="1"  style="text-align:right">'+
					'</td>'+
					
					'<td>'+
						'<input id="paytypeid" class="form-control payment-unitprice" type="text" placeholder=""  value="0"  style="text-align:right">'+
					'</td>'+
					'<td>'+
						'<input id="paytypeid" class="form-control payment-dis" type="text" placeholder=""  value="0"  style="text-align:right">'+
					'</td>'+
					'<td>'+
						'<input id="paytypeid" class="form-control payment-amt" type="text" placeholder=""  value="0"  style="text-align:right">'+
					'</td>'+
					"<td style='text-align:center'>&nbsp;&nbsp;&nbsp;<a id='a_delete' class='a_delete a_delete1' style='text-align:center'><?php   echo $photodel;?></a></td>"+
				'</tr>';
				// $(".tbody_paymenttype").append(trrow);	
				// var rowCount= $('#btl_paymenttype').children('tr').length;
				var rowCount=$('#btl_paymenttype tr').length;
				
				// alert(rowCount);
				
				if(rowCount<=9){
					var idLast=$('tbody.tbody_paymenttype tr:has(select.paymenttype_list):last select.paymenttype_list').val();		
					if(idLast != ''){
						$("tbody.tbody_paymenttype").append(trrow);
					}
				}
				// alert(rowCount);
				$(".payment-amt,.amt_balance").attr('disabled','disabled');
				Fcalpayment();
				
	}
function FaddHead(){
	return false;
		var trhead='<tr class="gform_1">'+
						'<th style="text-align:left !important">Description</th>'+
						'<th style="text-align:right !important">Note</th>'+
						'<th style="text-align:right !important">Qty</th>'+
						'<th style="text-align:right !important">Unit Price</th>'+
						'<th style="text-align:right !important">Dis(%)</th>'+
						'<th style="text-align:right !important">Amount</th>'+
						'<th style="text-align:center !important" class="addnew">'+
						"<a id='a_addnew' title='' data-placement='top' data-toggle='tooltip' href='javascript:;' data-original-title='Add New'><?Php // echo $addnew?></a>"+
						'</th>'+
					'</tr>'+
					'<tr class="gform_1 tr_head1">'+
						   '<td>'+
								'<input readonly="readonly" id="paytypeid" class="form-control payment-des schoolfee_des paymenttype_list" type="text" placeholder="" value="ថ្លៃសិក្សា \ Tuition Fee" style="text-align:left">'+
								'<input id="is_bus" class="form-control is_bus" type="text" value="" style="text-align:left; display:none">'+
							'</td>'+
							'<td>'+
								'<input id="schoolfee" class="form-control payment-note schoolfee_des" type="text" placeholder="" value=""  style="text-align:right">'+
								
							'</td>'+
							'<td>'+
								'<input id="numqty" class="form-control numqty" type="text" placeholder="" value="1"  style="text-align:right">'+
								
							'</td>'+
							
							'<td>'+
								'<input id="" class="form-control payment-unitprice schoolfee_prices" type="text" placeholder=""  value="0"  style="text-align:right">'+
								
							'</td>'+
							'<td>'+
								'<input id="paytypeid" class="form-control payment-dis" type="text" placeholder=""  value="0"  style="text-align:right">'+
								
							'</td>'+
							'<td>'+
								'<input id="paytypeid" class="form-control payment-amt" type="text" placeholder=""  value="0"  style="text-align:right">'+
								
							'</td>'+
							'<td style="text-align:center">'+
							"<a id='a_delete' class='a_delete' style='text-align:center'><?php echo "<img style='' onclick='' src='".site_url('../assets/images/icons/delete.png')."'/>"?></a>"+
							'</td>'+
							'+</tr>';
				// $(".tbody_paymenttype").append(trrow);	
				$(".gform_1").remove();
				$("thead.thead_paymenttype").append(trhead);
	}

function FgetRowStuinfo(attr_schlevel,attr_class,studentid,attr_coninv){		
		if(studentid>0){
					$.ajax({
                            url:"<?php echo base_url(); ?>index.php/student_fee/c_assign_payment_method/Fcstudentinfo",    
                            data: {
                                'attr_schlevel':attr_schlevel,
								'attr_class':attr_class,								
                                'studentid':studentid
								},
                            type:"post",
							dataType:"json",
                            async:false,
                            success: function(data){
								$('.photostu').html(data.image_stu);
								var ispayexist=(data.ispayexist);
								
								if(ispayexist==0){// never need payment method
									$('.h-iscond').val(5);
									$(".h-iscond-save").val(1);
									$("#btnsaves,.oppaymentmethod").attr('disabled','disabled'); 
									$("#btnsaves").val("New Student");
								}else{
									$('.h-iscond').val(data['getRow']['feetypeid']);
									$(".h-iscond-save").val(3);
									$("#btnsaves,.oppaymentmethod").removeAttr('disabled');
									$("#btnsaves").val("Update");
								}
								
								if(attr_coninv==1){
									$('.mh_paymethod').val("");
									$('.mh_peroid').val("");
								}else{
									$('.mh_paymethod').val(data['getRow']['feetypeid']);
									$('.mh_peroid').val(data['getRow']['term_sem_year_id']);	
								}
								// $('.stuid').html(data['getRow']['studentid']);
								
								$('.namekh').html(data['getRow']['first_name_kh']+' '+data['getRow']['last_name_kh']);								
								$('.nameeng').html(data['getRow']['first_name']+' '+data['getRow']['last_name']);
								$('.email').html(data['getRow']['email']);
								$('.phone').html(data['getRow']['phone1']);								
								$('.sex').html(data['getRow']['gender']);	
								
								$('.h-program').val(data['getRow']['programid']);
								$('.h-schoollavel').val(data['getRow']['schlevelid']);
								$('.h-year').val(data['getRow']['year']);	
								$('.h-ranglavel').val(data['getRow']['rangelevelid']);	
								$('.h-class').val(data['getRow']['classid']);	
								$('.h-area').val(data['getRow']['zoon']);	
								
								$('.h-enrollid').val(data['getRow']['enrollid']);
								$('.h-studentid').val(data['getRow']['studentid']);
								
                               $('.program').html(data['getRow']['program']);
                               $('.schoollavel').html(data['getRow']['sch_level']);
                               $('.year').html(data['getRow']['sch_year']);
                               $('.ranglavel').html(data['getRow']['rangelevelname']);
                               $('.class').html(data['getRow']['class_name']);
                               
                               $('.paymethod').html(data['getRow']['schoolfeetype_kh']);
                              //  ('.paymenttype').html(data['arayOrder']['schoolfeetype_kh']);
                               $('.area').html(data['getRow']['area']);
                               $('.bus').html(data['getRow']['busfeetypid']);
                               $('.date').html(data['getRow']['trandate']);
								// alert(data.tr_inv);
								$('.to_menthod').html("");
								$('.thead_paymenttype').html("");
								$('.thead_paymenttype').html(data.trinv);
								Fcalpayment();
								FcoundTo();
                        }
                   });
		}else{
			//  $('#family_revenue').val('');
		}
	}

function FClear(){
		$(".payment-note").val("");
		$("#oppaymentmethod").val("");
		$("#paymenttype").val("");
		$("#oparea").val("");
		$("#opbusfeetype").val("");
		$("#text_note").val("");
		
		$(".h-program").val("");
		$(".h-schoollavel").val("");
		$(".h-year").val("");
		$(".h-ranglavel").val("");
		$(".h-class").val("");
		$(".h-area").val("");
		$(".h-iscond").val("");
		$(".h-enrollid").val("");
		$(".h-studentid").val("");
		
		$(".payment-unitprice").val(0);		
		$(".payment-dis").val(0);		
		$(".payment-amt").val(0);	
		
		$("#atm_total").val(0);
		$("#amt_dis").val(0);
		$("#amt_balance").val(0);
		$(".numqty").val(1);
		$('.stuid').html("");
		$('.namekh').html("");
		$('.nameeng').html("");
		$('.sex').html("");
		$('.email').html("");
		$('.phone').html("");
		$('.program').html("");
		$('.schoollavel').html("");
		$('.year').html("");
		$('.class').html("");
		
		$(".t_bodyhis").html("");
	}
	//--------------
   function clear(){
      $('#termid').val('');
      $('#term').val('');      
      $('#term_kh').val('');
      $('#programid').val('');
      $('#yearid').html('');
      $('#schlevelids').html('');
      $('#start_date').val('');
      $('#end_date').val('');
      $('#isclosed').val(0);
      $('#isclosed').prop('checked', false);
      $('#dv_rangelevelid').html('');      
   }
   // sort ----------------------------------------------
  function sort(event){
    /*$(event.target)=$(this)*/ 
     this.sortby = $(event.target).attr("rel");
     if ($(event.target).attr("sorttype") == "ASC") {
         $('.sort').removeClass('cur_sort_up');
         $(event.target).addClass(' cur_sort_down');
         $(event.target).attr("sorttype", "DESC");
         this.sorttype = "ASC";
         $('.sort').removeClass('cur_sort_down');
         $(event.target).addClass(' cur_sort_up');
     } else if ($(event.target).attr("sorttype") == "DESC") {
         $(event.target).attr("sorttype", "ASC");
         this.sorttype = "DESC";
         $('.sort').removeClass('cur_sort_up');
         $('.sort').removeClass('cur_sort_down');
         $(event.target).addClass(' cur_sort_down');    
     }        
     grid(1, 10,this.sortby,this.sorttype);
  }
  
   // grid =========
   function grid(current_page = 0, total_display = 0,sortby = '', sorttype = ''){
      var offset = ((current_page - 1) * total_display) - 0;
      var limit = total_display - 0;

      $.ajax({
         url: '<?= site_url('student_fee/c_assign_payment_method/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            sortby:sortby,
            sorttype:sorttype,
            offset: offset,
            limit: limit
            ,
            student_num: $('#student_num').val(),
            full_name: $('#full_name').val(),
            full_kh: $('#full_kh').val(),
            gender: $('#gender').val(),
            schoolid: $('#schoolid').val(),
            programid: $('#programid').val(),
            schlevelid: $('#schlevelids').val(),
            term_sem_year_id: $('#term_sem_year_id').val(),
            feetypeid: $('#feetypeid').val(),
            yearid: $('#yearid').val(),
            rangelevelid: $('#rangelevelid').val(),
            classid: $('#classid').val(),
            is_paid: $('#is_paid').val(),
            is_closed: $('#is_closed').val(),            
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            years: $('#year').val(),			
            limit_record: $('#limit_record').val(),
            schlevelids: $('#schlevelid').val()
         },
         success: function(data) {
			 $('#se_list tbody').html(data.tr); 
		}
      });
   }
</script>