<?php
    //  area -------------------------------------------------
    if(isset($areaname) && count($areaname)>0){
      $area="";
      $area="<option attr_price='0' value=''>--select--</option>";
      foreach($areaname as $row){
         $area.="<option value='".$row->areaid."'>".$row->area."</option>";
      }
    }
    
 ?>
<style type="text/css">
    table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }

    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }
</style>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
    <div class="col-sm-12">     
    	<div class="row">
          <div class="col-xs-12">
             <div class="result_info">
                <div class="col-xs-6" style="background: #4cae4c;color: white;">
                   <span class="glyphicon glyphicon-stats"></span>
                   <strong>Student in Bus</strong>  
                </div>
                <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
                    <a href="javascript:;" class="btn btn-sm btn-success" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add new... item"><span class="glyphicon glyphicon-minus"></span></a>

                    <a href="javascript:;" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                      <span class="glyphicon glyphicon-refresh"></span>
                    </a>
                    <?php if($this->green->gAction("E")){ ?> 
                      <a href="javascript:;" class="btn btn-sm btn-success" id="a_export" data-toggle="tooltip" data-placement="top" title="Export..."><span class="glyphicon glyphicon-export"></span></a>
                    <?php }?>
                    <?php if($this->green->gAction("P")){ ?> 
                      <a href="javascript:;" class="btn btn-sm btn-success" id="Print" data-toggle="tooltip" data-placement="top" title="Print..."><span class="glyphicon glyphicon-print"></span></a>
                    <?php }?>
                  
                </div>         
             </div>
          </div>
        </div>
		<div class="collapse" id="collapseExample">             
		    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
		        
		     	  <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                   <h1 class="panel-title"  align="center">Search Student</h1>
                            </div>
                            <div class="panel-body">
                                <div class="form_sep">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                           <label for="studenid">Student Name & Student ID</label>
                                           <input type="hidden" name="studentid" id="studentid" class="studentid">
                                           <input type="text" name="studen_id" id="studen_id" class="form-control" placeholder="Search..."  tabindex="1">
                                        </div>

                                        <div class="form-group"​ style="">
                                           <label for="formdate">From Date<span style="color:red"></span></label>
                                           <div class="input-group">
                                                <input type="text" name="formdate" id="formdate" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                           </div>
                                        </div>

                                        <div class="form-group" style="">
                                           <label for="todate">To Date<span style="color:red"></span></label>
                                             <div class="input-group">
                                             <input type="text" name="todate" id="todate" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                           </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-6">
                                        <div class="form-group">
                                           <label for="area_name">Area Name</label>
                                           <select name="area_name" id="area_name" class="form-control" tabindex="5">
                                              <?php echo $area; ?>
                                           </select>
                                        </div>

                                        <div class="form-group">
                                           <label for="bus_name">Bus Name</label>
                                           <select name="bus_name" id="bus_name" class="form-control"  tabindex="5">

                                           </select>
                                        </div>

                                        <div class="form-group">
                                           <label for="driver_name">Driver Name</span></label>
                                           <select name="driver_name" id="driver_name" class="form-control"  tabindex="5">

                                           </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
		     	<!-- form2 -->
                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title"  align="center">Apply To Bus</h1>
                            </div>
                            <div class="panel-body">
                                <div class="form_sep">
                                   <div class="col-xs-6">
                                        <div class="form-group">
                                           <label for="area_id"> Area Name </label>
                                           <select name="area_id" id="area_id" class="form-control" tabindex="5">
                                              <?php echo $area; ?>
                                           </select>
                                        </div>

                                        <div class="form-group">
                                           <label for="bus_name">Bus Name</label>
                                           <select name="bus_name" id="bus_id" class="form-control"  tabindex="5">

                                           </select>
                                        </div>

                                        <div class="form-group">
                                           <label for="driver_id">Driver Name</span></label>
                                           <select name="driver_id" id="driver_id" class="form-control"  tabindex="5">

                                           </select>
                                        </div>
                                    </div>
                                    <!-- form 2 -->
                                    <div class="col-xs-6">
                                        <div class="form-group" style="">
                                           <label for="to_date"> Date<span style="color:red"></span></label>
                                             <div class="input-group">
                                             <input type="text" name="to_date" id="to_date" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div style="padding-top:10px; text-align: center; " class="form-group">
                            <?php if($this->green->gAction("R")){ ?> 
                            <button id="serch" class="btn btn-success btn-sm" type="button" name="serch"><span class="glyphicon glyphicon-search"></span>&nbsp;Serch</button>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <?php }?>
                            <?php if($this->green->gAction("C")){ ?> 
                            <button id="save" class="btn btn-success btn-sm" type="button" name="save" value="save"><span class="glyphicon glyphicon-saved"></span>&nbsp;Save</button>
                            <?php }?>
                        </div>           
                    </div>
  		     	  <div class="row">
  			        <div class="col-sm-12">
  			            <div class="form-group" style="border-bottom: 1px solid #CCC;">&nbsp;</div>           
  			        </div>
  		     	</div>
		  	</form>
		</div>
    <!-- show header -->
        <div>
            <div class="panel panel-default">
               <div class="table-responsive">
                    <div id="tab_print">
                        <table width="100%" border="0" class="table table-hover" >
                            <thead>
	                            <tr style="background: #4cae4c;color: white;">
                                    <?php
                                         foreach ($thead as $th => $val) {
                                            if ($th == 'No'){
                                                echo "<th class='$val'width='5%'>".$th."</th>";
                                            }else if($th == "<input type='checkbox' name='checkAll[]'' class='checkAll ' value=''>"){
                                                echo "<th style='width:50px;' class='$val'>".$th."</th>";
                                            }else if($th == 'Photo'){
                                                echo"<th class='$val' width='8%'>".$th."</th>";
                                            }else{
                                                echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
                                            }
                                         }
                                      ?>
                                </tr>
                            </thead>
                            <tbody id='listrespon'></tbody>
                        </table>
                     </div>
                 </div>
            </div>
            <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
                Display :   <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                                <?php
                                    $num=50;
                                    for($i=0;$i<10;$i++){?>
                                        <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                        <?php $num+=100;
                                    }
                                ?>
                            </select>
            </div>
            <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
                <div class="pagination" id="pagination" style="text-align:center"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#collapseExample').addClass('in');
        var sort_num = $("#sort_num").val();
        Getdata(1,$('#sort_num').val());
        // sort_num -----------------------
        $('#sort_num').change(function(){
            // alert($(this).val());
            var  total_display= $(this).val();
            Getdata(1,total_display);
        });

        //pagenav--------------------------
        $("body").delegate(".pagenav","click",function(){
            var page = $(this).attr("id");
            var total_display = $('#sort_num').val();
            Getdata(page,total_display);
        });
        // date ---------------------------
        $('#formdate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
            //startDate: '-3d'
        });

        $('#todate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
            //startDate: '-3d'
        });

        $('#to_date').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
            //startDate: '-3d'
        });

        // btn_search ---------------------------------
        $('body').delegate('#serch', 'click', function(){
            Getdata(1,$('#sort_num').val());
        });

        // keychang ----------------------------
        $("#area_name").on('change',function(){
            get_bus($(this).val());
        });
        $("#bus_name").on('change',function(){
            get_driver($(this).val());
        });

        $("#area_id").on('change',function(){
            keychang();
            get_busid($(this).val());
        });
        $("#bus_id").on('change',function(){
            keychang();
            get_driverid($(this).val());
        });
        $("#driver_id").on('change',function(){
            keychang();
        });
        // refresh -----------------------------------
        $('body').delegate('#refresh', 'click', function(){
            location.reload();
        });
        // show hide----------------------------------
        $('body').delegate('#a_addnew', 'click', function(){
            $('#collapseExample').collapse('toggle');
            clardata();
            $('#f_save').parsley().destroy();
            if($(this).find('span').hasClass('glyphicon-minus')){
                $(this).find('span').removeClass('glyphicon-minus');
                $(this).find('span').addClass('glyphicon-plus');            
            }
             else{
                $(this).find('span').removeClass('glyphicon-plus');
                $(this).find('span').addClass('glyphicon-minus');
            }
        });
        // chackbox ----------------------------------
        $('body').delegate('.checksub','click',function(){
            var attr_studentid=$(this).attr('data-studentid');
            $(".studentid").val(attr_studentid);
        });
        // main chech----------------------------------
        $('body').delegate('.checkAll', 'click', function(){
            var checkAll = $(this).val();           
            if($(this).is(':checked')){                
                $('.checksub').each(function(){
                   $('.checksub'+checkAll).prop('checked', true); 
                });
            }else{
               $('.checksub'+checkAll).prop('checked', false); 
            } 
        });

        // save --------------------------------------
        $("body").delegate("#save", "click", function (){

            var applyto = $('#area_id').val();
            if(applyto ==""){
                alert("Select apply to bus...!");
            }else if($('.checksub').is(':checked')){
                var i = 0;
                var arr_check = [];
                $(".checksub:checked").each(function(){
                    var studenttype  = $(this).attr('data-studenttype');
                    var othefeeid   = $(this).attr('data-othefeeid');

                    // console.log(studenttype);
                    // return false;
                    arr_check[i] = {studenttype : studenttype,othefeeid : othefeeid};
                    i++;
                });
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('student_fee/c_student_in_bus/savedata') ?>",
                    data : {
                        studentid : $("#studentid").val(),
                        area_id   : $("#area_id").val(),
                        bus_id    : $("#bus_id").val(),
                        driver_id : $("#driver_id").val(),
                        to_date   : $("#to_date").val(),
                        arr_check : arr_check
                    },

                    success:function(data){

                        toastr["success"]("Students apply to bus was created...!"); 
                        //$('#collapseExample').collapse('toggle');
                        Getdata(1,$('#sort_num').val());
                        clardata();
                        $("#save").html('Save'); 
                    }
                });
            }else{
                    alert(" Please check data first before process.!");
                }

        });

        // print ------------------------------
        $("#Print").on("click", function () {
          var htmlToPrint = '' +
               '<style type="text/css">' +
               'table th, table td {' +
               'border:1px solid #000 !important;' +
               'padding;0.5em;' +
               '}' +
               '</style>';
           var title = "Student in Bus List";
           var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "");
           var export_data = $("<center>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
           export_data += htmlToPrint;
           gsPrint(title, export_data);
       });

        // export------------------------------
        $("#Export").on("click", function (e) {
            var title = "Student in Bus List";
            var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
            var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
            e.preventDefault();
        }); 
        $('body').delegate('#a_export', 'click', function(e){
           var title = "Cash Return Lists";
           var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
           var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
           window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
           e.preventDefault();
      });      
    }); // end funtion --------------------------

    // get_bus ----------------------------------
        function get_bus(area_name){
            $.ajax({        
                url: "<?= site_url('student_fee/c_student_in_bus/get_bus') ?>",    
                data: {         
                  'area_name':area_name
                },
                type: "post",
                dataType: "json",
                // async:false,
                success: function(data){ 

                    var opt = '';
                        opt="<option  value=''>--select--</option>";
                    if(data.bus.length > 0){
                        $.each(data.bus, function(i, row){
                            //
                            opt += '<option value="'+ row.busid +'">'+ row.busno +'</option>';
                        });
                    }else{
                        opt += '';
                    }

                    $('#bus_name').html(opt);
                    get_driver($('#bus_name').val());
               }
            });
        }
    // get_driver --------------------------------------
        function get_driver(bus_name){
            $.ajax({        
                url: "<?= site_url('student_fee/c_student_in_bus/get_driver') ?>",    
                data: {         
                  'bus_name':bus_name
                },
                type: "post",
                dataType: "json",
                // async:false,
                success: function(data){ 

                    var opt = '';
                        opt="<option  value=''>--select--</option>";
                    if(data.dri.length > 0){
                        $.each(data.dri, function(i, row){
                            //
                            opt += '<option value="'+ row.driverid +'">'+ row.driver_name +'</option>';
                        });
                    }else{
                        opt += '';
                    }

                    $('#driver_name').html(opt);
               }
            });
        }


        // get_busid ----------------------------------
        function get_busid(area_id){
            $.ajax({        
                url: "<?= site_url('student_fee/c_student_in_bus/get_busid') ?>",    
                data: {         
                  'area_id':area_id
                },
                type: "post",
                dataType: "json",
                // async:false,
                success: function(data){ 

                    var opt = '';
                        opt="<option  value=''>--select--</option>";
                    if(data.bus.length > 0){
                        $.each(data.bus, function(i, row){
                            //
                            opt += '<option value="'+ row.busid +'">'+ row.busno +'</option>';
                        });
                    }else{
                        opt += '';
                    }

                    $('#bus_id').html(opt);
                    get_driverid($('#bus_id').val());
               }
            });
        }
    // get_driverid --------------------------------------
        function get_driverid(bus_id){
            $.ajax({        
                url: "<?= site_url('student_fee/c_student_in_bus/get_driverid') ?>",    
                data: {         
                  'bus_id':bus_id
                },
                type: "post",
                dataType: "json",
                // async:false,
                success: function(data){ 

                    var opt = '';
                        opt="<option  value=''>--select--</option>";
                    if(data.dri.length > 0){
                        $.each(data.dri, function(i, row){
                            //
                            opt += '<option value="'+ row.driverid +'">'+ row.driver_name +'</option>';
                        });
                    }else{
                        opt += '';
                    }

                    $('#driver_id').html(opt);
               }
            });
        }

        // copare funtioni--------------------------------------
        function keychang(){
            var area_name = $('#area_name').val();
            var area_id  = $('#area_id').val();
            var bus_name = $('#bus_name').val();
            var bus_id  = $('#bus_id').val();
            var driver_name  = $('#driver_name').val();
            var driver_id   = $('#driver_id').val();
            if(area_name != "" && area_id !="" && bus_name !="" && bus_id !="" && driver_name !="" && driver_id !=""){
                if((area_name == area_id) && (bus_name == bus_id) &&(driver_name == driver_id)){

                   //toastr["success"]("Students in grades Duplicate...!");  
                   alert("Apply to Duplicate...!");
                }
            }
        }

        // Getdata ---------------------------------------------
        function Getdata(page,total_display,sortby,sorttype){
            var roleid = <?php echo $this->session->userdata('roleid');?>;
            var per = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
            var sort_num = $('#sort_num').val();

            var studen_id = $("#studen_id").val();
            var formdate = $("#formdate").val();
            var todate = $("#todate").val();
            var area_name = $("#area_name").val();
            var bus_name = $("#bus_name").val();
            var driver_name = $("#driver_name").val();

            $.ajax({
                url:"<?php echo base_url();?>index.php/student_fee/c_student_in_bus/Getdata",
                type: "POST",
                datatype: "Json",
                async: false,
                data:{
                        sortby:sortby,
                        sorttype:sorttype,
                        page  : page,
                        p_page: per,
                        total_display: total_display,
                        sort_num:sort_num,
                        studen_id:studen_id,
                        formdate:formdate,
                        todate:todate,
                        area_name:area_name,
                        bus_name:bus_name,
                        driver_name:driver_name
                },
                success: function (data) {
                    $("#listrespon").html(data.data);
                    $('.pagination').html(data['pagination']['pagination']);
                }
            });
        }

        // sort -------------------------------------------------
        function sort(event){
            this.sortby = $(event.target).attr("rel");
            if ($(event.target).attr("sorttype") == "ASC") {
                $('.sort').removeClass('cur_sort_up');
                $(event.target).addClass(' cur_sort_down');
                $(event.target).attr("sorttype", "DESC");
                this.sorttype = "ASC";
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_up');
            } else if ($(event.target).attr("sorttype") == "DESC") {

                $(event.target).attr("sorttype", "ASC");
                this.sorttype = "DESC";
                $('.sort').removeClass('cur_sort_up');
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_down');    
            }               
            Getdata(1,$('#sort_num').val(),this.sortby,this.sorttype);
        }

        // clardata -------------------------------------------
        function clardata(){
            $('#studentid').val('');
            $('#area_id').val('');
            $('#bus_id').val(''); 
            $('#driver_id').val('');
            $('#to_date').val('');
            $('.checksub').attr('checked', false);
            $('.checkAll ').attr('checked', false);
        }

        // gsPrint --------------------------------------------------------------
        function gsPrint(emp_title, data) {
            var element = "<div>" + data + "</div>";
            $("<center><p style='padding-top:15px;text-align:center;'><b>" + emp_title + "</b></p><hr>" + element + "</center>").printArea({
                mode: "popup",  //printable window is either iframe or browser popup
                popHt: 600,  // popup window height
                popWd: 500,  // popup window width
                popX: 0,  // popup window screen X position
                popY: 0, //popup window screen Y position
                popTitle: "test", // popup window title element
                popClose: false,  // popup window close after printing
                strict: false
            });
        }
</script>