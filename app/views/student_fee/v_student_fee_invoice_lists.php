<div class="container-fluid" style="width:100% !important">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
            	<span class="icon">
                  <i class="fa fa-th"></i>
              </span>
                <strong>Invoice Lists </strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
              <?php if($this->session->userdata('match_con_posid')!='stu'){?>
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                  <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
               </a>
               <?php }?>
               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
               </a>
               <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
               </a>           
               
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>

            </div>         
         </div>
      </div>
   </div>

   <div class="collapse" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
         <input type="hidden" name="termid" id="termid">
         
         <div class="col-sm-4" style=""> 

            <div class="form-group">
               <label for="typeno">Invoice#<span style="color:red"></span></label>
               <input type="text" name="typeno" id="typeno" class="form-control" placeholder="Invoice#">
            </div>

            <div class="form-group">
               <label for="student_num">ID<span style="color:red"></span></label>
               <input type="text" name="student_num" id="student_num" class="form-control" placeholder="ID">
            </div>            

			   <div class="form-group">
               <label for="student_name">English/Khmer Name<span style="color:red"></span></label>
               <input type="text" name="student_name" id="student_name" class="form-control" placeholder="English/Khmer Name">
            </div>

            <div class="form-group">
               <label for="gender">Gender<span style="color:red"></span></label>
               <select name="gender" id="gender" class="form-control">
               	<option value="">All</option>
               	<option value="Female">Female</option>
               	<option value="Male">Male</option>
               </select>
            </div>

            <div class="form-group">
               <label for="schoolid">School Info<span style="color:red"></span></label>
               <select name="schoolid" id="schoolid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->info->getschinfor() as $row) {
                        $opt .= '<option value="'.$row->schoolid.'">'.$row->name.'</option>';
                     }

                     echo $opt;        
                  ?>                  
               </select>
            </div>     

         </div><!-- 1 -->

         <div class="col-sm-4">
            
            <div class="form-group">
               <label for="programid">Program<span style="color:red"></span></label>
               <select name="programid" id="programid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->p->getprograms() as $row) {
                        $opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group">
               <label for="schlevelids">School Level<span style="color:red"></span></label>
               <select name="schlevelids" id="schlevelids" class="form-control">
                  
               </select>
            </div>

            <div class="form-group">
               <label for="yearid">Year<span style="color:red"></span></label>
               <select name="yearid" id="yearid" class="form-control">

               </select>
            </div>

            <div class="form-group">
               <label for="rangelevelid">Rangelevel Name<span style="color:red"></span></label>
               <select name="rangelevelid" id="rangelevelid" class="form-control">

               </select>
            </div>
            
            <div class="form-group" style="">
               <label for="classid">Class<span style="color:red"></span></label>
               <select name="classid" id="classid" class="form-control">
                  <?php
                     // $opt = '';
                     // $opt .= '<option></option>';
                     // foreach ($this->c->allclass() as $row) {
                     //    $opt .= '<option value="'.$row->classid.'">'.$row->class_name.'</option>';
                     // }

                     // echo $opt;        
                  ?>
               </select>
            </div>        
            
         </div><!-- 2 -->

         <div class="col-sm-4">

            <div class="form-group"​ style="">
               <label for="from_date">From Date<span style="color:red"></span></label>
               <div class="input-group">
                 <input type="text" name="from_date" id="from_date" class="form-control" placeholder="dd/mm/yyyy">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
               </div>
            </div>

            <div class="form-group" style="">
               <label for="to_date">To Date<span style="color:red"></span></label>
                 <div class="input-group">
                 <input type="text" name="to_date" id="to_date" class="form-control" placeholder="dd/mm/yyyy">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
               </div>
            </div>

            <div class="form-group" style="">
               <label for="feetypeid">Payment Method<span style="color:red"></span></label>
               <select name="feetypeid" id="feetypeid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->f->getfeetypes() as $row) {
                        $opt .= '<option value="'.$row->feetypeid.'">'.$row->schoolfeetype.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group" style="">
               <label for="term_sem_year_id">Study Period<span style="color:red"></span></label>
               <select name="term_sem_year_id" id="term_sem_year_id" class="form-control">
                  <?php
                     // $opt = '';
                     // $opt .= '<option></option>';
                     // foreach ($this->f->getfeetypes() as $row) {
                     //    $opt .= '<option value="'.$row->feetypeid.'" '.($row->feetypeid - 0 == 1 ? 'selected="selected"' : '').'>'.$row->schoolfeetype.'</option>';
                     // }

                     // echo $opt;        
                  ?>
               </select>
            </div>
 			<div class="form-group">
               <label for="report_type">Status<span style="color:red"></span></label>
               <select name="report_type" id="report_type" class="form-control">
               		<option value="">-- All--</option>
                    <option value="1" style="display:none-">Paid</option>  
                    <option value="0">Unpaid</option>
               </select>
            </div>
            <div class="form-group"  style="display:none">
               <label for="is_closed">Is active?<span style="color:red"></span></label>
               <select name="is_closed" id="is_closed" class="form-control">
               	
               	<option value="0">Active</option>
               	<option value="1">Inactive</option>               	
               </select>
            </div>

         </div><!-- 3 -->      

         <div class="col-sm-7 col-sm-offset-5">
            <div class="form-group">
               <button type="button" class="btn btn-primary btn-sm btn_search" name="btn_search" id="btn_search" data-save='1'>Search</button>&nbsp; 
               <button type="button" class="btn btn-warning btn-sm btn_clear" name="btn_clear" id="btn_clear" data-save='1' style="display: none;">Clear</button>               
            </div>            
         </div>

      </form>      
      <div class="row">
         <div class="col-sm-12">
            <div class="col-sm-12" style="border-bottom: 1px solid #CCC;">&nbsp;</div>
         </div>           
      </div>
   </div>   
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
	         <div id="tab_print">
	            <table border="0"​ align="center" id="se_list" class="table table-hover">               
						<thead>
                      <tr>
                      <?php
                         foreach ($thead as $th => $val) {
                            if ($th == 'No'){
                                echo "<th class='$val'width='3%'>".$th."</th>";
                            }else if($th == 'Photo'){
                                echo "<th class='$val'>".$th."</th>";
                            }else{
                              echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
                            }
                         }
                      ?>
                   </tr>
						   <!-- <tr>
						      <th style="width: 2%;">No</th>
						      <th style="width: 5%;text-align: center;">Photo</th>
						      <th style="width: 28%;">Description</th>					      
						      <th style="width: 10%;">Date</th>                             
						      <th style="width: 10%;">School Level</th>
						      <th style="width: 10%;">Year</th>                        
						      <th style="width: 10%;">Range Level</th>
						      <th style="width: 10%;">Class</th> 
                        <th style="width: 10%;">Study Period</th>
						      <th class="show_hide" style="width: 5%;">#Invoice</th>
                        <th style="width: 10%;">Total</th>
                        <th style="width: 10%;">Deposit</th>
                        <th style="width: 5%;" colspan="2">Balance</th>						      
						   </tr>	 -->					  
						</thead>                
	               <tbody>
  	               	
	               </tbody>
	               
	               <tfoot>
	                  <tr style="border-top: 2px solid #ddd;" class="remove_tag">
	                     <td colspan="7" style="padding-top: 10px;">
	                        <span id="sp_total">
                            <label for="limit_record">Limit<span style="color:red"></span></label>
                               <select name="limit_record" id="limit_record" class="limit_record" style="width:50px;">
                                 <option value="10">10</option>                                 
                               	<option value="50">50</option>
                               	<option value="100">100</option>
                               	<option value="300">300</option>
                               	<option value="500">500</option>
                               	<option value="50000">50000</option>
                               	<option value="500000">500000</option>
                               	<option value="5000000">5000000</option>
                               	<option value="50000000">50000000</option>
                               	<option value="500000000">500000000</option>
                               </select>
                            </span>
	                     </td>
	                     <td colspan="6" style="text-align: right;">
	                        <span style="" id="sp_page" class="btn-group pagination" role="group" aria-label="..."></span>
	                     </td>
                        
                     </tr>
	                     
	               </tfoot>               
	            </table>
            </div>        
         </div>
      </div>
   </div>

</div>

<div class="modal" id="dialog_print" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="wrapper">
				<div class="clearfix" id="main_content_outer">
				    <div id="main_content">
					    <div class="result_info">
					    	<div class="col-sm-6">
                                <span class="icon">
                                    <i class="fa fa-th"></i>
                                </span>
					      		<strong>Print Report (s)</strong>
					      	</div>
					      	<div class="col-sm-6" style="text-align: center">
					      		<strong>
					      			<center class='visit_error' style='color:red;'></center>
					      		</strong>	
					      	</div>
					    </div>
                        <div class="col-xs-12" style="border:#CCC solid 1px;margin:0px;height:100%;">                                   
                              
                                <table style="text-align:center" align="center">
                                	<tr style="line-height:40px;">
                                    	
                                        <td>
                                        	<a  target="_blank" href="" class="print_p1">preview Invoice</a>
                                        </td>
                                    </tr>
                                    <tr style="line-height:40px; display:none" class="printreceipt">
                                        <td>
                                        	<a  target="_blank" href="" class="print_p2">preview Receipt</a>
                                        </td>
                                    </tr>
                                    
                                </table>                             
                           </div>
                           
					</div> 
			    </div>
			</div> 
            <div class="modal-footer"> 
                
                 <input data-dismiss="modal" id="btncancel" class="btn btn-warning" type="button" value="Close" name="btncancel"/>
           
            </div>
        </div> <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
</div>

<span data-toggle="modal" data-target="#dialog_print" class="clprint" ></span>



<div class="modal" id="dl_viod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="wrapper">
                <div class="clearfix" id="main_content_outer">
                    <div id="main_content">
                        <div class="result_info">
                            <div class="col-sm-6">
                            	<span class="icon">
                                    <i class="fa fa-th"></i>
                                </span>
                                <strong>Comment The Reason</strong>
                            </div>
                        </div>
                        <div class="col-xs-12" style="border:#CCC solid 1px;margin:0px;height:100%;">
                         <textarea id="dl_text_note" class="dl_text_note" placeholder="Add Note" style="width: 575px; height: 190px;"></textarea>
                       	 <input type="hidden" class="hh_typeno" id="hh_typeno" value="" />
                        </div>
                           
                    </div> 
                </div>
            </div> 
           <div class="modal-footer">     
              <button  data-dismiss="modal" id="btn_cancel" class="btn btn-warning btn_cancel_viod" type="button" name="btncancel">Cancel</button>
              <button type="button" class="btn btn-default btnsave">Save</button>
            </div>
        </div>                       <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
</div>

<style type="text/css">
	#se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	#mdinvoice .modal-dialog  {width:99% !important;}			
	
	a {
		cursor: pointer;
	}
	 .gamt{ font-size:14px; color:#00F; font-weight:bold; text-align:right}
	 
div.relative {
    position: relative;
    width: 200px;
    height: 85px;
    border: 3px solid #73AD21;
}

div.absolute {
    position: absolute;
    top: 0px;
    right: 0;
    width: 200px;
    height: 80px;
    border: 3px solid #73AD21;
}

table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }

    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }
</style>

<span data-toggle="modal" data-target="#dialog_print" class="clprint" ></span>

<script type="text/javascript">
   $(function(){
$("#year,#schlevelid").hide();
   	// get ======= 
      $('body').delegate('#feetypeid', 'change', function(){
      	var feetypeid = $(this).val() - 0;

      	$.ajax({
            url: '<?= site_url('student_fee/c_student_fee_invoice_lists/get_paymentType') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               feetypeid: feetypeid,
               programid: $('#programid').val(),
               schlevelid: $('#schlevelids').val(),
               yearid: $('#yearid').val()
                               
            },
            success: function(data){
               // schoollevel ======
               var opt = '';               
               if(data.length > 0){     
                  opt += '<option value="">All</option>';          
                  $.each(data, function(i, row){
                     opt += '<option value="'+ row.term_sem_year_id +'">'+ row.period +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#term_sem_year_id').html(opt);             

            },
            error: function() {

            }
         });
      });

      // get ======= 
      $('body').delegate('#programid', 'change', function(){
         get_schlevel($(this).val());
         get_year($(this).val(), $('#schlevelids').val());    
                    
      });
      $('body').delegate('#schlevelids', 'change', function(){
         get_rangelevel($('#programid').val(), $(this).val());
         get_year($('#programid').val(), $(this).val());
         get_class($('#programid').val(), $('#schlevelids').val(), $('#yearid').val());
      });

      // get schlevel ======
      function get_schlevel(programid){      
         $.ajax({
            url: '<?= site_url('student_fee/c_student_fee_invoice_lists/get_schlevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid                
            },
            success: function(data){
               // schoollevel ======
               var opt = '';               
               if(data.schlevel.length > 0){
                  opt += '<option value="">All</option>';
                  $.each(data.schlevel, function(i, row){
                     opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#schlevelids').html(opt);

               // get year =======
               get_year($('#programid').val(), $('#schlevelids').val());   

               // rangelevel ===== 
               get_rangelevel($('#programid').val(), $('#schlevelids').val());      

            },
            error: function() {

            }
         });         
      }

      // get year =======
      function get_year(programid, schlevelid = ''){
         $.ajax({
            url: '<?= site_url('student_fee/c_student_fee_invoice_lists/get_year') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelid                                  
            },
            success: function(data){
               var opt = '';
               
               if(data.year.length > 0){
                  opt += '<option value="">All</option>';
                  $.each(data.year, function(i, row){
                     opt += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#yearid').html(opt);

            },
            error: function() {

            }
         });
      }

      // get rangelevel =======
      function get_rangelevel(programid = '', schlevelid){
         $.ajax({
            url: '<?= site_url('student_fee/c_student_fee_invoice_lists/get_rangelevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelid                                 
            },
            success: function(data){
               var opt = '';               
               if(data.rangelevel.length > 0){
                  opt += '<option value="">All</option>';
                  $.each(data.rangelevel, function(i, row){
                     opt += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#rangelevelid').html(opt);
            },
            error: function() {

            }
         });
      }

      // get class =======
      function get_class(programid = '', schlevelid, yearid = ''){
         $.ajax({
            url: '<?= site_url('student_fee/c_student_fee_invoice_lists/get_class') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelid,
               yearid: yearid                                 
            },
            success: function(data){
               var opt = '';              
               if(data.class.length > 0){
                   opt += '<option value="">All</option>';
                  $.each(data.class, function(i, row){
                     opt += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#classid').html(opt);
            },
            error: function() {

            }
         });
      }

   	$('#from_date').datepicker({
   		format: 'dd/mm/yyyy'
   	});
   	$('#to_date').datepicker({
   		format: 'dd/mm/yyyy'
   	});      

      // tooltip ======
      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // print =======
      $('body').delegate('#a_print', 'click', function(){
         var title="<h4 align='center'>"+ "Invoice Lists" +"</h4>";
         var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "")
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint(title, export_data);
      });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Invoice Lists";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });      

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#term').select();
         $('#save').html('Save');
         $('#save_next').show();
      });

      // init. =====
      grid(1, $('#limit_record').val() - 0);

      // search ========
      $('body').delegate('#limit_record', 'change', function(){
         grid(1, $(this).val() - 0);
      });
      $('body').delegate('#btn_search', 'click', function(){
         grid(1, $('#limit_record').val() - 0);
      });


      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#limit_record').val() - 0);
      });
	  

	
	 $('body').delegate("a#a_delete_inv", "click", function() {
		 	var meid=$(this).attr('attr_id');
			$(".hh_typeno").val(meid);
		});
	// $('body').delegate(".relative", "hover", function() {
		
	$('body').delegate(".btnsave", "click", function() 
	{
			
		 	var attr_type=17;
			var hh_typeno = $(".hh_typeno").val();
		 	var text_note=$(".dl_text_note").val();
			// alert(hh_typeno);
			if($(".dl_text_note").val()==""){
				alert("Please comment The reason!");
				$(".dl_text_note").focus();
			}else{
				if(confirm('Are you sure want to delete ?','Data has deleted!'))
				{
					FVdeinv(attr_type,hh_typeno,text_note);
				}
			}
	});

   }); // main ========
   
function FVdeinv(attr_type,typeno,text_note){		
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/student_fee/c_student_fee_invoice/FCDelinv",    
				data: {					
					'attr_type':attr_type,				
					'typeno':typeno,
					'text_note':text_note				
					},
				type:"post",
				dataType:"json",
				async:false,
				success: function(data){					
					toastr["success"]('Record has been deleted!');
					 grid(1, $('#limit_record').val() - 0);
					$(".cl_btncancel").click();
			}
	   });		
	}
	
    function clear(){
        $('#termid').val('');
        $('#term').val('');      
        $('#term_kh').val('');
        $('#programid').val('');
        $('#yearid').html('');
        $('#schlevelids').html('');
        $('#start_date').val('');
        $('#end_date').val('');
        $('#isclosed').val(0);
        $('#isclosed').prop('checked', false);
        $('#dv_rangelevelid').html('');      
   }
   // sort ----------------------------------------------
   function sort(event){
       /*$(event.target)=$(this)*/ 
        this.sortby = $(event.target).attr("rel");
        if ($(event.target).attr("sorttype") == "ASC") {
            $('.sort').removeClass('cur_sort_up');
            $(event.target).addClass(' cur_sort_down');
            $(event.target).attr("sorttype", "DESC");
            this.sorttype = "ASC";
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass(' cur_sort_up');
        } else if ($(event.target).attr("sorttype") == "DESC") {
            $(event.target).attr("sorttype", "ASC");
            this.sorttype = "DESC";
            $('.sort').removeClass('cur_sort_up');
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass(' cur_sort_down');    
        }        
        grid(1, $('#limit_record').val(),this.sortby,this.sorttype);
     //    GetdataReceipt(1,$('#sort_num').val(),this.sortby,this.sorttype);
   }
   // grid =========
   function grid(current_page = 0, total_display = 0, sortby = '',sorttype = ''){
      var roleid = <?php echo $this->session->userdata('roleid');?>;
      var offset = ((current_page - 1) * total_display) - 0;
      var limit = total_display - 0;

      $.ajax({
         url: '<?= site_url('student_fee/c_student_fee_invoice_lists/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            sortby:sortby,
            sorttype:sorttype,
            offset: offset,
            limit: limit,
            typeno: $('#typeno').val(),
            student_num: $('#student_num').val(),
            student_name: $('#student_name').val(),
            gender: $('#gender').val(),
            from_date: $('#from_date').val(),            
            schoolid: $('#schoolid').val(),
            programid: $('#programid').val(),
            schlevelid: $('#schlevelids').val(),            
            yearid: $('#yearid').val(),
            rangelevelid: $('#rangelevelid').val(),
            to_date: $('#to_date').val(),
            classid: $('#classid').val(),
            feetypeid: $('#feetypeid').val(),
            term_sem_year_id: $('#term_sem_year_id').val(),            
            report_type: $('#report_type').val(),			
            limit_record: $('#limit_record').val()
         },
         success: function(data) {
            if($('#report_type').val() - 0 == 0){
               $('.show_hide').css({display: ''})
            }else{            
               $('.show_hide').css({display: 'none'})
            }

			   $('#se_list tbody').html(data.tr); 

            var page = '';
            var total = '';
            if(data.totalRecord - 0 > 0){
               // previous ====
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next&nbsp;<i class="glyphicon glyphicon-chevron-right"></i></button>';

            }

            $('#se_list tfoot').find('.pagination').html(page);
         },
         error: function(){

         }
      });
   }
</script>