<head>
   <script src="<?= base_url('assets/js/plotly-latest.min.js') ?>"></script>
</head>

<body>
<div class="container-fluid">
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6" style="background: #4cae4c;color: white;">
               <span class="glyphicon glyphicon-stats"></span>
               <strong>Revenues graphics</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
               <?php if($this->green->gAction("R")){ ?> 
                  <!-- <a href="javascript:;" class="btn btn-sm btn-success" id="a_search" data-toggle="tooltip" data-placement="top" title="Search"><span class="glyphicon glyphicon-search"></span></a> -->
               <?php }?>
               <a href="javascript:;" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh"><span class="glyphicon glyphicon-refresh"></span></a>
            </div>         
         </div>
      </div>
   </div>

   <form class="form-horizontal">
      <div class="form-group">
         <label for="yyear" class="col-sm-1 control-label" style="padding-right: 0;">Year</label>
         <div class="col-sm-3">
            <select type="text" name="yyear" id="yyear" class="form-control input-sm">
               <?php
               $year = date('Y');
               $xyear = $year - 15;
               for($i = 1; $i <= 30; $i++){
                  $y = $xyear + $i;
                  $selected = $y == $year ? ' selected="selected" ' : '';
                  echo '<option value="'.$y.'" '.$selected.'>'.$y.'</option>';
               }
               ?>
            </select>
         </div>

         <label for="type_chart" class="col-sm-1 control-label" style="padding-right: 0;">Chart type</label>
         <div class="col-sm-3">
            <select type="text" name="type_chart" id="type_chart" class="col-sm-3 form-control input-sm">
               <option value="1">Bar chart</option>
               <option value="2">Line chart</option>            
            </select>
         </div>         
      </div>      
   </form>

   <div class="row">
      <div class="col-sm-12">
         <div class="col-sm-12" style="border-top: 1px solid #DDD;">&nbsp;</div>
      </div>             
   </div>

   <div class="col-sm-12 graph">
      <div id="revenue" style="border-left: 6px solid #337ab7;margin-bottom: 8px;padding-left: 5px;font-size: 11px;"></div>
      <div id="expense" style="border-left: 6px solid #f0ad4e;padding-left: 5px;font-size: 11px;"></div> 

      <div id="myDiv" style="width: 100%;height_: 80%;"></div>
      <!-- Plotly chart will be drawn inside this DIV -->
   </div>

   <div class="col-sm-12 display" style="background: #F2F2F2;text-align: center;"></div>

</div>
<script>

   $(function(){
      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      $('body').delegate('#yyear', 'change', function(){
         incomes();
      });

      $('body').delegate('#type_chart', 'change', function(){
         incomes();
      });

      incomes();
   });// ready ====

   function incomes(){
      $.ajax({
         url: '<?= site_url('student_fee/c_student_fee_invoice/get_data_graphic') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
            $('.xmodal').show(); 
         },
         complete: function(){
            $('.xmodal').hide();
         },
         data: {
            yyear: $('#yyear').val()
         },
         success: function(dd) {
            if(dd.c != ''){
               $('.graph').show();
               $('.display').hide();

               // re. =======
               var dv_revenue = (dd.to_revenue - 0 > 0  ? '<div style="font-weight: bold;">'+ number_format(dd.to_revenue) +'$</div> <div style="color: gray;">REVENUES</div>' : '');
               $('#revenue').html(dv_revenue);

               var xm = ['1-Jan', '2-Feb', '3-Mar', '4-Apr', '5-May', '6-Jun', '7-Jul', '8-Aug', '9-Sep', '10-Oct', '11-Nov', '12-Dec'];
               var type_ = $('#type_chart').val() - 0 == 1 ? 'bar' : 'line';
               
               // trace1 ========
               var trace1 = {
                 x: xm, 
                 y: dd.arr_r, 
                 name: 'Revenues', 
                 type: type_
               };

               var data = [trace1];
               var layout = {
                  barmode: 'group',
                  title: 'Revenues report ' + $('#yyear').val(),
                  xaxis: {
                     tickangle: -45,
                     title: 'Monthly',
                  },
                  yaxis: {
                      title: 'USD (Dollars)',
                      titlefont: {
                        size: 14,
                        color: 'rgb(107, 107, 107)'
                      },
                      tickfont: {
                        size: 14,
                        color: 'rgb(107, 107, 107)'
                      }
                  },
               };

               Plotly.newPlot('myDiv', data, layout);
            }
            else{
               $('.graph').hide();
               $('.display').show();
               $('.display').html('<span style="font-weight: bold;font-size: 12px;color: #337ab7;">No data!</span>');
            }
         },
         error: function() {

         }
      });
   }

   function number_format(num) {
      return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
   }
   
</script>
</body>