<style>
   .texts{
   		font-family:'Kh Muol';
   }
   .titles{
	   	font-family: 'Kh Muol';
	   	font-size:13pt;
	   	text-align: center;
	   	line-height: 25px; 
	   	padding: 5px 0px;  	
   }
   .text-comment{
   		border-top: hidden;
   		border-right: hidden;
   		border-left: hidden;
   		border-bottom: 1px #ccc dotted;
   		width: 900px;
   }
   .col-sm-6, .col-sm-4{
   		line-height: 22px;
   }
   th{
	   	text-align: center;
	   	vertical-align: middle !important;
   }
  table tr td {
  	line-height: 15px !important;
  	font-weight: normal !important;
  }
  .padding_from_text {
  	line-height: 50px !important;
  }
</style>
<div class="container-fluid"> 
	<div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
            	<span class="icon">
                    <i class="fa fa-th"></i>
                    </span>
                <strong><?php echo (isset($title)?$title:'')?></strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
               </a>
               <a href="javascript:void(0)" id="export" data-toggle="tooltip" data-placement="top" title="Export">
                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
               </a>           
            </div>         
         </div>
      </div>
	</div>
	<div class="row">
      	<div class="col-xs-12">
      			<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" class="tdrow" id="frm_subject_score_kgp">
	      		<!--form search of student information-->
	      		<div class="col-sm-12">
		        	<div class="row">
		        		<div class="panel panel-default">
		        			<div class="panel-body" style="padding: 5px;">
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_id">School</label>
					        			<select name="school_id" id="school_id" minlength='1' class="form-control">
					                    	<?php
												$row = $this->school->getschinfor_row(1);
												echo '<option value="">--- Select School ---</option>';										
												echo '<option value="'. $row->schoolid .'">'. $row->name .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="program_id">Program</label>
					        			<select name="program_id" id="program_id" minlength='1' class="form-control">
					                    	<?php
												$row = $this->program->getprogram(1);
												echo '<option value="">--- Select Program ---</option>';											
												echo '<option value="'. $row->programid .'">'. $row->program .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_level_id">School Level<span style="color:red">*</span></label>
					        			<select name="school_level_id" id="school_level_id" minlength='1' class="form-control" required data-parsley-required-message="This field require">
					                    	<?php
												$get_level_data = $this->school_level->getsch_level(1);	
												echo '<option value="">--- Select School Level ---</option>';											
												foreach($get_level_data as $r_level){
													echo '<option value="'. $r_level->schlevelid .'">'. $r_level->sch_level .'</option>';
												}
											?>	                      
		                                </select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="adcademic_year_id">Academic Year<span style="color:red">*</span></label>
	                                    <select name="adcademic_year_id" id="adcademic_year_id" minlength='1' class="form-control" required data-parsley-required-message="This field require">
	                                    	<option value="">--- Select Academic Year ---</option>
	                                    </select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="grade_level_id">Grade Level<span style="color:red">*</span></label>
	                                    <select name="grade_level_id" id="grade_level_id" minlength='1' class="form-control" required data-parsley-required-message="This field require">
	                                    	<option value="">--- Select Grade Level ---</option>
	                                    </select>
					        		</div>
					        	</div>	        	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="class_id">Class Name<span style="color:red">*</span></label>					        			
	                                    <select name="class_id" id="class_id" minlength='1' class="form-control" required data-parsley-required-message="This field require">
	                                    	<option value="">--- Select Class Name ---</option>
	                                    </select>
					        		</div>
					        	</div>		        					        	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="student_id">Student Name</label>
	                                    <select name="student_id" id="student_id" class="form-control"><option value="">--- Select Student Name ---</option></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="type_of_result">Type of Result</label>
	                                    <select name="type_of_result" id="type_of_result" class="form-control">
	                                    	<option value="">--- Select Type of Result ---</option>
	                                    	<option value="1">Semester Rank Report</option>
	                                    	<option value="2">Semester Mention Report</option>
	                                    	<option value="3">Yearly</option>
	                                    </select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="student_id">Semester</label>
	                                    <div id="display_semester"></div>
					        		</div>
					        	</div>					        	
		        			</div>
		        		</div>
		        	</form>
		        	</div>
		        </div><!--end form search of student information-->
		        
		        <!--button-->
		        <div class="col-sm-12">	
		        	<div class="row">
		        		<div class="form-group" style="text-align:center;">
		                <?php if($this->green->gAction("R")){ ?>
		                	<input type="button" name="btnsearch" id='btnsearch' value="Search" class="btn btn-primary btn-md" />
		                <?php } ?>
	               </div> 
		        	</div>	                                           
		        </div><!--end button-->

	      	</form>
    	</div>
	</div>
	<div id="div_export_print" style="margin-top:30px !important;">    
		<!-- ************************************* start body report ********************************** -->
			<div class="col-sm-12">    
			  <div class="panel panel-default">
			    <div class="panel-body">                 
			      <div class="table-responsive">
			      	<div class="col-sm-12" style="border:0px solid #f00;">
						<img width="200" src="<?php echo base_url('assets/images/logo/logo.png')?>" />
					</div>

					<div class="col-sm-2 titles" style="margin-bottom:5px;padding-top:20px; text-align: center;border:0px solid #000;">
						<span><b>ថ្នាក់ទី <span class="label_classname"></span></b></span><br>
						<span class="padding_from_text"><b>Grade <span class="label_classname"></span><b></span>
					</div>
					<div class="col-sm-10 titles" style="margin-bottom:5px;padding-top:20px; text-align: center;border:0px solid #f00;">
						<span><b><?php echo (isset($title)?$title:'')?></b></span><br>
						<span class="padding_from_text"><b><span class="typeofresult"></span><b></span>
					</div>
			          <table border="0"​ align="center" class="table listheaderreport" width="100%">
			            <thead>
			          
			            </thead>
			            <tbody>
			            </tbody>
			        </table>      
			      </div>
			    </div>
			  </div>    
			</div>
		<!-- *************************************** end body report ************************************* -->
	</div>
	<div style="page-break-after: always;"></div>
</div>
<script type="text/javascript">
    $(function(){
    	$("#school_id").val($("#school_id option:first").val());
		$("#program_id").val($("#program_id option:first").val());
		$("#school_level_id").val($("#school_level_id option:first").val());
		$("#adcademic_year_id").val($("#adcademic_year_id option:first").val());
		$("#grade_level_id").val($("#grade_level_id option:first").val());
		$("#class_id").val($("#class_id option:first").val());
		$("#student_id").val($("#student_id option:first").val());
		$("#type_of_result").val($("#type_of_result option:first").val());
		$("#export").on("click", function(){
			var data=$('.table').attr('border',1);
	        data = $("#div_export_print").html().replace(/<img[^>]*>/gi,"");
	        var export_data=$("<center><meta charset='utf-8'>"+data+"</center>").clone().find(".remove_tag").remove().end().html(); 
	        this.href = "data:application/vnd.ms-excel,"+encodeURIComponent(export_data);
	        this.download = "<?php echo (isset($title)?$title:'')?><?php echo (isset($sub_title)?$sub_title:'')?>.xls";
	        $('.table').attr('border',0);
        });
		$("#btnsearch").on("click", function(){
    		showStudentList();
        });
        $("#type_of_result").change(function(){
        	var $this = $(this).val();
        	if($this == 3){
				$('.check_semeste').prop('checked', false);
				$("#display_semester").hide();
			}else{
				$("#display_semester").show();
			}
        });
		$('#school_level_id').change(function(){
			var school_level_id = $(this).val();
			var school_id = $('#school_id').val();
			var program_id = $('#program_id').val();

			if(school_level_id != ""){
				$.ajax({
	            url: "<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_year_and_grade_data') ?>",	            
	            type: "post",
	            dataType: 'json',
	            data: {
	            	'school_id': school_id, 
	            	'program_id': program_id, 
	            	'school_level_id': school_level_id
	            },
	            success: function (data) {
	            	//get year--------------------------------
	            	var getYear = '<option value="">--- Select Academic Year ---</option>';	            	
	            	$.each(data["year"], function(k,v){
	            		getYear += '<option value="'+ v.yearid +'">'+ v.sch_year+'</option>';		            		
	            	}); 
	            	$('#adcademic_year_id').html(getYear);
	            	//get grade--------------------------------
	            	var getGrade = '<option value="">--- Select Grade Level ---</option>';
	            	$.each(data["grade"], function(ke,re){
	            		getGrade += '<option value="'+ re.grade_levelid +'">'+ re.grade_level+'</option>';
	            	});	
	            	$('#grade_level_id').html(getGrade);
					//remove message require-------------------
					var aId = $('#adcademic_year_id').data('parsley-id');
					var gId = $('#grade_level_id').data('parsley-id');
					$('#parsley-id-'+aId).remove();	   
					$('#adcademic_year_id').removeClass('parsley-error');
					$('#parsley-id-'+gId).remove();	   
					$('#grade_level_id').removeClass('parsley-error');
					getDataFromClass(); 
					fn_display_semester();      	
			   	}
	        });

			}else{
				$('#adcademic_year_id').html('');
				$('#grade_level_id').html('');
				$('#class_id').html('');	
				$('#student_id').html('');
			}
		});
		//get teacher data-----------------------------------------
		$('#grade_level_id').change(function(){
			var grade_level_id = $(this).val();
			var school_level_id = $('#school_level_id').val();			
			var adcademic_year_id = $('#adcademic_year_id').val();
			getDataFromClass();
			fn_display_semester();
		});
		//get student data------------------------------------------
		$('#class_id').change(function(){	
			var class_id = $(this).val();
			var school_level_id = $('#school_level_id').val();
			var grade_level_id = $('#grade_level_id').val();			
			var adcademic_year_id = $('#adcademic_year_id').val();
			getDataStudent();
			fn_display_semester();
		});
		function fn_display_semester(){
			$.ajax({
	            url: "<?php echo site_url('reports/generalkhmerlanguage/show_semester');?>",	            
	            type: "post",
	            dataType: 'html',
	            data: {
	            	para :1,
	            	schlevelid :$("#school_level_id").val(),
	            	yearid :$("#adcademic_year_id").val(),
	            	type_result:$("#result_type").val()
	            },
	            success: function (data) {	
	            	$("#display_semester").html(data);            	
	            }
	        });	
		}
		$("body").delegate(".check_semeste","click",function(){
			var result_type = $("#result_type").val();
			if(result_type == 3){
				$('input:checkbox').prop('checked', true);
			}else{
				$('input:checkbox').not(this).prop('checked', false);
			}
		});
		function getDataFromClass(){
			$.ajax({
	            url: "<?php echo site_url('score_rank_by_subject/c_score_rank_by_subject/getClassName') ?>",	            
	            type: "post",
	            dataType: 'json',
	            data: {
	            	'school_id':$('#school_id option:selected').val(),
	            	'school_level_id':$('#school_level_id option:selected').val(),
	            	'adcademic_year_id':$('#adcademic_year_id option:selected').val(),
	            	'grade_level_id':$('#grade_level_id option:selected').val()
	            },
	            success: function (list) {
	            	var opt_class='<option value="">--- Select Class Name ---</option>';
		            $.each(list['list'],function(i,item)
		            { 
		               opt_class += '<option value="'+ item['classid'] +'">'+ item['class_name']+'</option>';
		            });
					$("#class_id").html( opt_class );		            	
	            }
	        });	
		}
		function getDataStudent(){
			$.ajax({
	            url: "<?php echo site_url('score_rank_by_subject/c_score_rank_by_subject/getStudentData') ?>",	            
	            type: "post",
	            dataType: 'json',
	            data: {
	            	'school_id':$('#school_id option:selected').val(),
	            	'program_id':$('#program_id option:selected').val(),
	            	'school_level_id':$('#school_level_id option:selected').val(),
	            	'adcademic_year_id':$('#adcademic_year_id option:selected').val(),
	            	'grade_level_id':$('#grade_level_id option:selected').val(),
	            	'class_id':$('#class_id option:selected').val(),
	            	'student_id':$('#student_id option:selected').val()
	            },
	            success: function (list) {
	            	var opt_student='<option value="">--- Select Student Name ---</option>';
		            $.each(list['list'],function(i,item)
		            { 
		            	var i_d = (i+1);
		               	opt_student += '<option value="'+ item['studentid'] +'">'+i_d+'. '+item['fullname']+'</option>';
		            });
					$("#student_id").html( opt_student );		            	
	            }
	        });	
		}
		function getDataByAjax(selector, url, data){
			$.ajax({
	            url: url,	            
	            type: "post",
	            dataType: 'html',
	            data: data,
	            success: function (data) {
	            	selector.html(data);		            	
	            }
	        });	
		}
		function isset(variable) {
		    try {
		        return typeof eval(variable) !== 'undefined';
		    } catch (err) {
		        return false;
		    }
		}
		function showStudentList(){
			var school_id = $('#school_id option:selected').val();
			var program_id = $('#program_id option:selected').val();
			var school_level_id = $('#school_level_id option:selected').val();
			var adcademic_year_id = $('#adcademic_year_id option:selected').val();
			var grade_level_id = $('#grade_level_id option:selected').val();
			var class_id = $('#class_id option:selected').val();
			var student_id = $('#student_id option:selected').val();
			var check_semeste = $('.check_semeste:checked').val();
			var type_of_result = $('#type_of_result option:selected').val();
			if(type_of_result==''){
				alert('Please select Type of Result.');
				return false;
			}
			var str_semester='';
			if(check_semeste==40){
				str_semester='១';
			}else if(check_semeste==42){
				str_semester='២'
			}
			var set_url='';
			if(type_of_result==1 || type_of_result==2){
				set_url="<?php echo site_url('score_rank_by_subject/c_score_rank_by_subject/setReportSemesterRank') ?>";
			}else if(type_of_result==3){
				set_url="<?php echo site_url('score_rank_by_subject/c_score_rank_by_subject/setReportFinal') ?>";
			}
			$.ajax({
	            url: set_url,	            
	            type: "post",
	            dataType: 'json',
	            data: {
	            	'school_id':school_id,
	            	'program_id':program_id,
	            	'school_level_id':school_level_id,
	            	'adcademic_year_id':adcademic_year_id,
	            	'grade_level_id':grade_level_id,
	            	'class_id':class_id,
	            	'student_id':student_id,
	            	'check_semeste':check_semeste
	            },
	            success: function (list) {
	            	$('.label_classname').html($('#class_id option:selected').html());
	            	var str_rank_or_mention = '';
	            	if(type_of_result==1){
						$(".typeofresult").html('<?php echo (isset($sub_title)?$sub_title:'')?>'+str_semester);
						str_rank_or_mention='Rank';
					}
					else if(type_of_result==2){
						$(".typeofresult").html('<?php echo (isset($sub_title)?$sub_title:'')?>'+str_semester + ' ឆ្នាំសិក្សា '+ list['school_year']);
						str_rank_or_mention='Men';
					}
					else if(type_of_result==3){
						$(".typeofresult").html('ប្រចាំឆ្នាំសិក្សា '+ list['school_year']);
						str_rank_or_mention='Rank';
					}
	            	var tbl_header='<tr><th>No</th><th width="10%">Student_Name</th><th width="1%">Sex</th><th>Year</th><th>Grade</th>';
		           	var ii=0
		           	var arr_subjectid=[];
		            $.each(list['header'],function(i,item)
		            { 
		               	tbl_header += '<th>'+ item['short_sub']+'</th><th>'+str_rank_or_mention+'</th>';
		               	arr_subjectid[i]=item['subjectid'];
		               	ii++;
		            });
		            tbl_header+='</tr>';
	            	var tbl_body='';
            		$.each(list['student'],function(i,item)
		            { 
		            	var str_point='';
		            	$.each(list['header'],function(i,item_s){
		            		var str_p='';
		            		var str_rank='';
		            		var str_mention = '';
			           		$.each(list['point'],function(i,item_p){
							    if(item_p['student_id']==item['studentid'] && item_p['subject_id']==item_s['subjectid']){
			            			if(jQuery.inArray(arr_subjectid,item_p['subject_id'])  && item_p['total_score'] >0) {
				           				str_p+=numberFormat(item_p['total_score'], 2);
				           				str_rank+=item_p['rank'];
				           				if(list['mention'].length>0){
											$.each(list['mention'],function(i,item_mention){
						           				if(item_mention['Min_m'] <=(item_p['total_score']-0) && item_mention['Max_m']>=(item_p['total_score']-0)){
						           					str_mention= item_mention['Mention'];
						           				}	
							            	});
										}	
			           	 			}
	                             }
			            	 });
			           		
			           		var result_rank_mention='';
			           		if(type_of_result==1 || type_of_result==3){
			           			result_rank_mention=(str_rank!=""?str_rank:0);
							}
							else if(type_of_result==2){
								result_rank_mention=str_mention;
							}
			           		str_point+='<td>'+(str_p!=""?str_p:numberFormat(0, 2))+'</td><td align="center">'+result_rank_mention+'</td>';
		            	});
		            	var i_d = (i+1);
		               	tbl_body += '<tr>'+
		               					'<td width="1%">'+i_d+'</td>'+
		               					'<td>'+ item['fullname']+'</td>'+
		               					'<td>'+ item['gender']+'</td>'+
		               					'<td>'+ item['sch_year']+'</td>'+
		               					'<td>'+ item['class_name']+'</td>'+str_point+
		               				'<tr>';
		            });
		            if(tbl_body==''){
		            	tbl_body += '<tr><td colspan="'+(ii+ii+5)+'" align="center"><b>We did not find anything to show here...</b></td></tr>';
		            }
					$(".listheaderreport > thead").html( tbl_header );
					$(".listheaderreport > tbody").html( tbl_body );		            	
	            },
		          beforeSend: function() { $('.proccess_loading').show(); },
		          complete: function() { $('.proccess_loading').hide();}
	        });	
		}


	});
</script>
