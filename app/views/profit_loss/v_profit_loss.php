<div class="container-fluid">     
 	<div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6" style="background: #4cae4c;color: white;">
               <span class="glyphicon glyphicon-stats"></span>
               <strong>Profit/loss list</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
               <a href="javascript:;" class="btn btn-sm btn-success" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Show/hide search"><span class="glyphicon glyphicon-search"></span></a>
               <?php if($this->green->gAction("P")){ ?> 
                  <a href="javascript:;" class="btn btn-sm btn-success" id="a_print" data-toggle="tooltip" data-placement="top" title="Print..."><span class="glyphicon glyphicon-print"></span></a>
               <?php }?>
               <?php if($this->green->gAction("E")){ ?> 
                  <a href="javascript:;" class="btn btn-sm btn-success" id="a_export" data-toggle="tooltip" data-placement="top" title="Export..."><span class="glyphicon glyphicon-export"></span></a>
               <?php }?>
               <a href="javascript:;" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <span class="glyphicon glyphicon-refresh"></span>
               </a>
            </div>         
         </div>
      </div>
	</div>

	<div class="collapse in" id="collapseExample">             
		<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" id="f_save">

			<input type="hidden" name="id" id="id">

         <div class="col-sm-6">
            <div class="form-group">           
               <label for="from_date">From date<span style="color:red"></span></label>&nbsp;
               <input type="text" name="from_date" id="from_date" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
            </div>           
         </div>
         <div class="col-sm-6">
            <div class="form-group">           
               <label for="to_date">To date<span style="color:red"></span></label>&nbsp;
               <input type="text" name="to_date" id="to_date" class="form-control"  placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
            </div>           
         </div>

			<div class="col-sm-7 col-sm-offset-5">
            <div class="form-group"> 
               <?php if($this->green->gAction("R")){ ?>           
               <button type="button" class="btn btn-success btn-sm" name="search" id="search">Search</button>
               <?php }?>
               <button type="button" class="btn btn-default btn-sm" name="clear" id="clear" style="_display: none;">Clear</button>
            </div>
         </div>

			<div class="row">
				<div class="col-sm-12">
				   <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
				</div>             
			</div>
		</form>      
	</div>
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive" id="tab_print">
            <table border="0"​ align="center" cellspacing="0" cellpadding="0" id="pro_loss_list" class="table table-hover">
                <thead>
                     <tr style="background: #4cae4c;color: white;">
                        <th style="width: 8%;text-align: left;border: 0;">No</th>
                        <th style="width: 70%;text-align: left;border: 0;">Description</th>
                        <th style="width: 20%;text-align: right;border: 0;">Amount ($)</th>
                     </tr>
                </thead>
                
   				<tbody>

   				</tbody>
   				<tfoot class="remove_tag">
                  <tr>
                     <td colspan="1">
                        <div>&nbsp;</div>
                        <select name="to_display" id="to_display" title="Display items..." style="height: 22px;display: none;">
                           <option value="5">5</option>
                           <option value="10">10</option>
                           <option value="30">30</option>
                           <option value="50">50</option>
                           <option value="100">100</option>
                           <option value="200">200</option>
                           <option value="500">500</option>
                           <option value="1000">1000</option>                       
                        </select>
                     </td>
                     <td colspan="3">
                        <div>&nbsp;</div>                     
                        <div class="btn-group pagination" role="group" aria-label="..." style="display: none;"></div>
                     </td>                  
                  </tr>
   				</tfoot>
            </table>         
         </div>
      </div>

   </div>

</div>

<!-- style -->
<style type="text/css">
   #pro_loss_list th{vertical-align: middle;}
   #pro_loss_list td{vertical-align: middle;}

   .ui-autocomplete-loading {
      background: white url("<?= base_url('assets/images/ui-anim_basic_16x16.gif') ?>") right center no-repeat;
   }
   .ui-autocomplete{height: auto;overflow-y: hidden;}
   .ui-autocomplete > li:hover {
      background: #4cae4c;
      color: white;
   }
</style>

<script type="text/javascript">
   $(function(){

      // autocomplete ========
      $( "#item_name" ).autocomplete({
         source: "<?= site_url('inventory/c_opening_balance/get_std') ?>",
         minLength: 0,
         autoFocus: false,
         focus: function( event, ui ) {
            return false;
         },
         select: function( event, ui ) {          
            $(this).val(ui.item.descr_eng);           
            return false;                       
         }
         }).focus(function(){
            $( this ).autocomplete("search");
         })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              .append( "<span>" + item.stockcode + "</span> | " )
              .append( "<span>" + item.descr_eng + "</span>" )              
              .append( "</li>" )
              .appendTo( ul );
      }

      // print =======
      $('body').delegate('#a_print', 'click', function(){
         var title="<h4 align='center'>"+ 'Profits/Loss report' +"</h4>";
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint(title, export_data);
      });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Profits/Loss report";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         // clear();
         $('#f_save').parsley().destroy();

         if($(this).find('span').hasClass('glyphicon-minus')){
            $(this).find('span').removeClass('glyphicon-minus');
            $(this).find('span').addClass('glyphicon-plus');            
         }
         else{
            $(this).find('span').removeClass('glyphicon-plus');
            $(this).find('span').addClass('glyphicon-minus');
         }
      });

      $('#from_date').datepicker({
         format: 'dd/mm/yyyy',
         autoclose: true
      });
      $('#to_date').datepicker({
         format: 'dd/mm/yyyy',
         autoclose: true
      });

      // init. =======
      grid();
      // search =======
      $('body').delegate('#search', 'click', function(){
         grid(1, $('#to_display').val() - 0);
      });  
      $('body').delegate('#to_display', 'change', function(){
         grid(1, $('#to_display').val() - 0);
      });   
      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#to_display').val() - 0);
      });


      // clear =======      
      $('body').delegate('#clear', 'click', function(){
         clear();
         grid();
      });      

   }); // ready =======

	// clear =========
   function clear(){
      $('#from_date').val("<?= date('d/m/Y') ?>");
      $('#to_date').val("<?= date('d/m/Y') ?>");         
   }

   // grid =========
   function grid(){//current_page, total_display
      $.ajax({
         url: '<?= site_url('profit_loss/c_profit_loss/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
         	$('.xmodal').show();	
         },
         complete: function(){
         	$('.xmodal').hide();
         },
         data: {
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val()
         },
         success: function(data) {
            $('#pro_loss_list tbody').html(data.tr);
         },
         error: function() {

         }
      });
   }
</script>