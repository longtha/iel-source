<div class="container-fluid">     
 	<div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6" style="background: #4cae4c;color: white;">
               <span class="glyphicon glyphicon-stats"></span>
               <strong>Stock transactons list</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
               <a href="javascript:;" class="btn btn-sm btn-success" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Show/hide search"><span class="glyphicon glyphicon-search"></span></a>
               <a href="javascript:;" class="btn btn-sm btn-success" id="a_print" data-toggle="tooltip" data-placement="top" title="Print..."><span class="glyphicon glyphicon-print"></span></a>
               <a href="javascript:;" class="btn btn-sm btn-success" id="a_export" data-toggle="tooltip" data-placement="top" title="Export..."><span class="glyphicon glyphicon-export"></span></a>
               <a href="javascript:;" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <!-- <img src="<?= base_url('assets/images/icons/refresh.png') ?>"> -->
                  <span class="glyphicon glyphicon-refresh"></span>
               </a>
            </div>         
         </div>
      </div>
	</div>

	<div class="collapse in" id="collapseExample">             
		<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" id="f_save">

			<input type="hidden" name="id" id="id">

         <div class="col-sm-4">
            <div class="form-group">           
               <label for="from_date">From date<span style="color:red"></span></label>&nbsp;
               <input type="text" name="from_date" id="from_date" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
            </div>           
         </div>
         <div class="col-sm-4">
            <div class="form-group">           
               <label for="to_date">To date<span style="color:red"></span></label>&nbsp;
               <input type="text" name="to_date" id="to_date" class="form-control"  placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
            </div>           
         </div>
         <div class="col-sm-4">
            <div class="form-group">           
               <label for="categoryid">Category<span style="color:red"></span></label>&nbsp;
               <select name="categoryid" id="categoryid" class="form-control" placeholder="Category">
               <?php
               // getoption_($sql, $key, $display, $select = '', $istop = false)
               echo getoption_("SELECT DISTINCT
                                    mo.cate_id,
                                    c.cate_name
                                 FROM
                                    sch_stock_stockmove AS mo
                                 LEFT JOIN sch_stock_category AS c ON mo.cate_id = c.categoryid ", "cate_id", "cate_name", "", true);
               ?>
               </select>
            </div>           
         </div>
         <div class="col-sm-4">
            <div class="form-group">           
               <label for="item_name">Item name<span style="color:red"></span></label>&nbsp;
               <input type="text" name="item_name" id="item_name" class="form-control" placeholder="Item name">
            </div>           
         </div>
         <div class="col-sm-4">
            <div class="form-group">           
               <label for="whcode">Location<span style="color:red"></span></label>&nbsp;
               <select name="whcode" id="whcode" class="form-control" placeholder="Location">
               <?php
               // getoption_($sql, $key, $display, $select = '', $istop = false)
               echo getoption_("SELECT DISTINCT
                                    mo.whcode,
                                    w.wharehouse
                                 FROM
                                    sch_stock_stockmove AS mo
                                 LEFT JOIN sch_stock_wharehouse AS w ON mo.whcode = w.whcode
                                 ORDER BY
                                    w.wharehouse ASC ", "whcode", "wharehouse", "", true);
               ?>
               </select>
            </div>           
         </div>
         <div class="col-sm-4">
            <div class="form-group">           
               <label for="type">Transaction type<span style="color:red"></span></label>&nbsp;
               <select name="type" id="type" class="form-control" placeholder="Transacton type">
               <?php
               // getoption_($sql, $key, $display, $select = '', $istop = false)
               echo getoption_("SELECT DISTINCT
                                    mo.type AS typeid,
                                    sys.type
                                 FROM
                                    sch_stock_stockmove AS mo
                                 LEFT JOIN sch_z_systype AS sys ON mo.type = sys.typeid ", "typeid", "type", "", true);
               ?>
               </select>
            </div>           
         </div>

			<div class="col-sm-7 col-sm-offset-5">
            <div class="form-group">           
               <button type="button" class="btn btn-success btn-sm" name="search" id="search">Search</button>
               <button type="button" class="btn btn-default btn-sm" name="clear" id="clear">Clear</button>
            </div>
         </div>

			<div class="row">
				<div class="col-sm-12">
				   <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
				</div>             
			</div>
		</form>      
	</div>
   
   <div class="row">
      <div class="col-sm-12" id="tab_print">
         <div class="table-responsive">
            <table border="0"​ align="center" cellspacing="0" cellpadding="0" id="transactoin_list" class="table table-hover">
                <thead>
                     <tr style="background: #4cae4c;color: white;">
                        <th style="width: 8%;text-align: left;border: 0;">No</th>
                        <th style="width: 15%;text-align: left;border: 0;">Item name</th>
                        <th style="width: 15%;text-align: left;border: 0;">Date</th>
                        <th style="width: 15%;text-align: left;border: 0;">Type</th>
                        <th style="width: 15%;text-align: right;border: 0;">Quantity</th>
                        <th style="width: 15%;text-align: right;border: 0;">Price ($)</th>
                        <th style="width: 15%;text-align: right;border: 0;">Amount ($)</th>
                     </tr>
                </thead>
                
   				<tbody>

   				</tbody>
   				<tfoot class="remove_tag">
                  <tr>
                     <td colspan="1">
                        <div>&nbsp;</div>
                        <select name="to_display" id="to_display" title="Display items..." style="height: 22px;">
                           <option value="5">5</option>
                           <option value="10">10</option>
                           <option value="30">30</option>
                           <option value="50">50</option>
                           <option value="100">100</option>
                           <option value="200">200</option>
                           <option value="500">500</option>
                           <!-- <option value="1000">1000</option> -->
                        </select>
                     </td>
                     <td colspan="2">
                        <div>&nbsp;</div>  
                        <span id="show_display" style="margin-left: 10px;">&nbsp;</span>
                        <div class="btn-group pagination" role="group" aria-label="..." style="display: inline;"></div>
                     </td>                  
                     <td colspan="5">&nbsp;</td>
                  </tr>
   				</tfoot>
            </table>         
         </div>
      </div>

   </div>

</div>

<!-- style -->
<style type="text/css">
   #transactoin_list th{vertical-align: middle;}
   #transactoin_list td{vertical-align: middle;}

   .ui-autocomplete-loading {
      background: white url("<?= base_url('assets/images/ui-anim_basic_16x16.gif') ?>") right center no-repeat;
   }
   .ui-autocomplete{height: auto;overflow-y: hidden;}
   .ui-menu .ui-menu-item:hover, .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus {
      background: #4cae4c;
      color: white;
   }
</style>

<script type="text/javascript">
   $(function(){

      // autocomplete ========
      $( "#item_name" ).autocomplete({
         source: function(request, response) {
            $.getJSON("<?= site_url('inventory/c_stock_transaction/get_stock') ?>", {
               term : request.term,
               categoryid: $('#categoryid').val()
            }, response);
        },
         minLength: 0,
         autoFocus: true,
         delay: 0,
         focus: function( event, ui ) {
            return false;
         },
         select: function( event, ui ) {          
            $(this).val(ui.item.descr_eng);           
            return false;                       
         }
         }).focus(function(){
            $( this ).autocomplete("search");
         })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              .append( "<span>" + item.stockcode + "</span> | " )
              .append( "<span>" + item.descr_eng + "</span>" )              
              .append( "</li>" )
              .appendTo( ul );
      }

      // print =======
      $('body').delegate('#a_print', 'click', function(){
         var title="<h4 align='center'>"+ 'Stock transactions report' +"</h4>";
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint(title, export_data);
      });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Stock transactions report";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // save =======      
      $('body').delegate('.save', 'click', function(){
         var save = $(this).data('save');
         if($('#f_save').parsley().validate()){            
            $.ajax({
               url: '<?= site_url('inventory/c_stock_transaction/save') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){
                  $('.xmodal').show();
               },
               complete: function(){
                  $('.xmodal').hide();
               },
               data: {
                  whcode: $('#whcode').val(),
                  wherehouse: $('#wherehouse').val(),
                  address: $('#address').val(),
                  contact_person: $('#contact_person').val(),
                  contact_tel: $('#contact_tel').val(),
                  note: $('#note').val()                                    
               },
               success: function(data){
                  if(data.saved){
                     if(save == 1){
                        clear();
                        $('#wherehouse').select();
                     }
                     else{
                        $('#wherehouse').select();
                     }             
	                  grid(1, $('#to_display').val() - 0);
	                  toastr["success"](data.saved);	                  
	               }
	               else if(data.updated){
	                  grid(1, $('#to_display').val() - 0);
	                  clear();	
                     $('#wherehouse').select();                 
	                  toastr["success"](data.updated);
                     $('#save').text('Save');
                     $('#save_next').show();
	               }
	               else if(data.existed){
	                  toastr["warning"](data.existed);
	               }                  
               },
               error: function() {

               }
            });
         }
      });

      // edit ========
      $('body').delegate('.edit', 'click', function(){
         var whcode = $(this).data('whcode');

         $.ajax({
            url: '<?= site_url('inventory/c_stock_transaction/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
               $('.xmodal').show();
            },
            complete: function(){
               $('.xmodal').hide();
            },
            data: {
               whcode: whcode
            },
            success: function(data){
               $('#whcode').val(data.whcode);
               $('#wherehouse').val(data.wharehouse);
               $('#address').val(data.address);
               $('#contact_person').val(data.contact_person);
               $('#contact_tel').val(data.contact_tel);               
               $('#note').val(data.note);               

               $('#f_save').parsley().destroy();
               $('#save').text('Update');
               $('#save_next').hide();
               $('#collapseExample').addClass('in');

               $('#a_addnew').find('span').removeClass('glyphicon-plus');
               $('#a_addnew').find('span').addClass('glyphicon-minus');
            },
            error: function() {

            }
         });         
         
      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var whcode = $(this).data('whcode');
         var whcode_chk = $(this).data('whcode_chk') - 0;
        
         if(whcode_chk > 0){
            toastr["warning"]("Can't delete! data in process...");
         }else{
            if(window.confirm('Are you sure to delete?')){
               $.ajax({
                  url: '<?= site_url('inventory/c_stock_transaction/delete') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     whcode: whcode
                  },
                  success: function(data){
                     if(data == 1){
                        toastr["warning"]("Deleted!");
                        clear();
                        grid(1, $('#to_display').val() - 0);
                     }else{
                        toastr["warning"]("Can't delete!");
                     }               
                     
                  },
                  error: function() {

                  }
               });
            }
         }       
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         // clear();
         $('#f_save').parsley().destroy();

         if($(this).find('span').hasClass('glyphicon-minus')){
            $(this).find('span').removeClass('glyphicon-minus');
            $(this).find('span').addClass('glyphicon-plus');            
         }
         else{
            $(this).find('span').removeClass('glyphicon-plus');
            $(this).find('span').addClass('glyphicon-minus');
         }
      });

      $('#from_date').datepicker({
         format: 'dd/mm/yyyy',
         autoclose: true
      });
      $('#to_date').datepicker({
         format: 'dd/mm/yyyy',
         autoclose: true
      });

      // init. =======
      grid(1, $('#to_display').val() - 0);
      // search =======
      $('body').delegate('#search', 'click', function(){
         grid(1, $('#to_display').val() - 0);
      });  
      $('body').delegate('#to_display', 'change', function(){
         grid(1, $('#to_display').val() - 0);
      });   
      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#to_display').val() - 0);
      });


      // clear =======      
      $('body').delegate('#clear', 'click', function(){
         clear();
         grid(1, $('#to_display').val() - 0);
      });      

   }); // ready =======

	// clear =========
   function clear(){
      $('#from_date').val('<?= date('d/m/Y') ?>');      
      $('#to_date').val('<?= date('d/m/Y') ?>');
      $('#categoryid').val('');
      $('#item_name').val('');
      $('#whcode').val('');
      $('#type').val('');         
   }

   // grid =========
   function grid(current_page, total_display){
   	var offset = (current_page - 1)*total_display - 0;  
   	var limit = total_display - 0;
      $.ajax({
         url: '<?= site_url('inventory/c_stock_transaction/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
         	$('.xmodal').show();	
         },
         complete: function(){
         	$('.xmodal').hide();
         },
         data: {
         	offset: offset,
         	limit: limit
            ,
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            categoryid: $('#categoryid').val(),
            descr_eng: $('#item_name').val(), 
            whcode: $('#whcode').val(),
            type: $('#type').val()
         },
         success: function(data) {
            $('#transactoin_list tbody').html(data.tr);

            var page = '';
            var total = '';
            var show_display = '';
            if(data.totalRecord - 0 > 0){
               // previous ====
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next<i class="glyphicon glyphicon-chevron-right"></i></button>';

               show_display += ((offset + limit > data.totalRecord ? data.totalRecord : offset + limit) + ' / ' + data.totalRecord);
            }
            
            $('#show_display').html(show_display);
            $('.pagination').html(page);

         },
         error: function() {

         }
      });
   }
</script>