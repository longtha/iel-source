<div class="container-fluid">     
 	<div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6" style="background: #4cae4c;color: white;">
               <span class="glyphicon glyphicon-stats"></span>
               <strong>Stock balance list</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
               <a href="javascript:;" class="btn btn-sm btn-success" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add new... item"><span class="glyphicon glyphicon-minus"></span></a>

               <a href="javascript:;" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <!-- <img src="<?= base_url('assets/images/icons/refresh.png') ?>"> -->
                  <span class="glyphicon glyphicon-refresh"></span>
               </a>
               <?php if($this->green->gAction("P")){ ?> 
                  <a href="javascript:;" class="btn btn-sm btn-success" id="a_print" data-toggle="tooltip" data-placement="top" title="Print..."><span class="glyphicon glyphicon-print"></span></a>
               <?php }?>
               <?php if($this->green->gAction("E")){ ?> 
                  <a href="javascript:;" class="btn btn-sm btn-success" id="a_export" data-toggle="tooltip" data-placement="top" title="Export..."><span class="glyphicon glyphicon-export"></span></a>
               <?php }?>
            </div>         
         </div>
      </div>
	</div>

	<div class="collapse in" id="collapseExample">             
		<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" id="f_save">

         <div class="col-sm-4">
            <div class="form-group">
               <label for="categoryid">Category name<span style="color:red"></span></label>&nbsp;
               <select name="categoryid" id="categoryid" class="form-control input-sm" placeholder="Category name">
                  <?php
                  // getoption($sql, $key, $display, $select = '', $istop = false)
                  echo '<option value="">All</option>';
                  echo getoption("SELECT * FROM sch_stock_category AS c ORDER BY c.cate_name ASC", "categoryid", "cate_name", "", false);
                  ?>
               </select>
            </div>               
         </div>

			<div class="col-sm-4">
				<div class="form-group">           
				   <label for="stockcode">Item code<span style="color:red"></span></label>
				   <input type="text" name="stockid" id="stockcode" class="form-control input-sm" placeholder="Item code"> 
				</div>		
			</div>

			<div class="col-sm-4">
				<div class="form-group">           
				   <label for="descr_eng">Item name<span style="color:red"></span></label>
				   <input type="text" name="descr_eng" id="descr_eng" class="form-control input-sm" placeholder="Item name"> 
				</div>				
			</div>

         <div class="col-sm-4">
            <div class="form-group">           
               <label for="whcode">Location<span style="color:red"></span></label>
               <select name="whcode" id="whcode" class="form-control input-sm" placeholder="Location">
                  <?php
                  // getoption($sql, $key, $display, $select = '', $istop = false)
                  echo '<option value="">All</option>';
                  echo getoption("SELECT * FROM sch_stock_wharehouse AS w ORDER BY w.wharehouse ASC", "whcode", "wharehouse", "", false);
                  ?>
               </select> 
            </div>         
         </div>

         <div class="col-sm-1" style="padding-right: 0;">
            <div class="form-group">           
               <label for="quantity">Quantity<span style="color:red"></span></label>
               <input type="text" name="quantity" id="quantity" class="form-control input-sm" placeholder="Quantity" style="border-radius: 0;" value="N...value" disabled>
            </div>            
         </div>
         <div class="col-sm-1" style="padding: 0;">
            <div class="form-group">           
               <label for="others">Compare<span style="color:red"></span></label>
               <select name="others" id="others" class="form-control input-sm" placeholder="Others" style="border-radius: 0;">
                  <option value="all">All</option>
                  <option value="=">=</option>                  
                  <option value=">">></option>
                  <option value=">=">>=</option>
                  <option value="<"><</option>
                  <option value="<="><=</option>                  
               </select> 
            </div>         
         </div>
         <div class="col-sm-2" style="padding-left: 0;">
            <div class="form-group">           
               <label for="value_compare">Value<span style="color:red"></span></label>
               <input type="text" name="value_compare" id="value_compare" class="form-control input-sm" placeholder="Quantity" decimal style="border-radius: 0;" value="0" disabled>
            </div>            
         </div> 

         <div class="col-sm-4">
            <div class="form-group">  
               <label for="is_active">Status<span style="color:red"></span></label>
               <select name="is_active" id="is_active" class="form-control input-sm" placeholder="Status">
                  <option value="">All</option>
                  <option value="1">Active</option>
                  <option value="0">Inactive</option>
               </select>         
            </div>         
         </div>			

			<div class="col-sm-7 col-sm-offset-5">
            <div class="form-group"> 
               <?php if($this->green->gAction("R")){ ?>             
                 <button type="button" class="btn btn-success btn-sm" name="search" id="search">Search</button>
               <?php } ?>   
               <button type="button" class="btn btn-default btn-sm" name="clear" id="clear">Clear</button>
            </div>
         </div>

			<div class="row">
				<div class="col-sm-12">
				   <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
				</div>             
			</div>
		</form>      
	</div>
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive" id="tab_print">
            <table border="0"​ align="center" id="balance_list" class="table table-hover">
                <thead>
                     <tr style="background: #4cae4c;color: white;">
                        <th style="width: 5%;text-align: left;border: 0;">No</th>
                        <th style="width: 15%;text-align: left;border: 0;">Category</th>
                        <th style="width: 20%;text-align: left;border: 0;">Location</th>
                        <th style="width: 10%;text-align: left;border: 0;">Item code</th>
                        <th style="width: 20%;text-align: left;border: 0;">Item name</th>
                        <th style="width: 10%;text-align: right;border: 0;">Quantity</th>
                        <th style="width: 10%;text-align: right;border: 0;">Status</th>
                     </tr>
                </thead>
                
   				<tbody>

   				</tbody>
   				<tfoot class="remove_tag">
                  <tr>
                     <td colspan="1">
                        <div>&nbsp;</div>
                        <select name="to_display" id="to_display" title="Display items..." style="height: 22px;">
                           <!-- <option value="5">5</option> -->
                           <option value="10">10</option>
                           <option value="30">30</option>
                           <option value="50">50</option>
                           <option value="100">100</option>
                           <option value="200">200</option>
                           <option value="500">500</option>
                           <option value="1000">1000</option>                       
                        </select>
                     </td>
                     <td colspan="2">
                        <div>&nbsp;</div> 
                        <span id="show_display" style="margin-left: 10px;">&nbsp;</span>
                        <div class="btn-group pagination" role="group" aria-label="..." style="display: inline;"></div>
                     </td>                  
                     <td colspan="4">&nbsp;</td>
                  </tr>
   				</tfoot>
            </table>         
         </div>
      </div>
   </div>

</div>


<!-- style -->
<style type="text/css">
   #balance_list th{vertical-align: middle;}
   #balance_list td{vertical-align: middle;}

   .ui-autocomplete-loading {
      background: white url("<?= base_url('assets/images/ui-anim_basic_16x16.gif') ?>") right center no-repeat;
   }
   .ui-autocomplete{height: auto;overflow-y: hidden;}
   .ui-menu .ui-menu-item:hover, .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus {
      background: #4cae4c;
      color: white;
   }
</style>

<script type="text/javascript">
   $(function(){

      // print =======
      $('body').delegate('#a_print', 'click', function(){
         var title="<h4 align='center'>"+ 'Stock balance report' +"</h4>";
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint(title, export_data);
      });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Stock balance report";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // clear =======      
      $('body').delegate('#clear', 'click', function(){
         clear();
         grid(1, $('#to_display').val() - 0);
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#f_save').parsley().destroy();

         if($(this).find('span').hasClass('glyphicon-minus')){
            $(this).find('span').removeClass('glyphicon-minus');
            $(this).find('span').addClass('glyphicon-plus');            
         }
         else{
            $(this).find('span').removeClass('glyphicon-plus');
            $(this).find('span').addClass('glyphicon-minus');
         }
      });

      // init. =======
      grid(1, $('#to_display').val() - 0);
      // search ========
      $('body').delegate('#search', 'click', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#to_display', 'change', function(){
         grid(1, $('#to_display').val() - 0);
      });           
      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#to_display').val() - 0);
      });

      // compare =======
      $('body').delegate('#quantity, #value_compare', 'focusout', function() {
         if($(this).val() + "" == ""){
            $(this).val(0);
         }         
      });      
      $('body').delegate('#quantity, #value_compare', 'focus', function() {
         $(this).select();
      });
      $('body').delegate('#others', 'change', function() {
         if($(this).val() != 'all'){
            $('#quantity').val('N...value');
            $('#value_compare').removeAttr('disabled');
            $('#value_compare').select();
         }
         else{
            $('#value_compare').attr('disabled', 'disabled');
         }
      });

      //autocomplete ======
      $( "#stockcode" ).autocomplete({
         source: function(request, response) {
            $.getJSON("<?= site_url('inventory/c_stock_balance/get_stock') ?>", {
               term : request.term,
               categoryid: $('#categoryid').val()
            }, response);
         },
         minLength: 0,
         delay: 0,
         autoFocus: true,
         select: function( event, ui ) {          
            $(this).val(ui.item.stockcode);           
            return false;                       
         }
         })
         .focus(function(){
            $( this ).autocomplete("search");
         })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              .append( "<span>" + item.stockcode + "</span>" )              
              .append( "</li>" )
              .appendTo( ul );
      }

      $( "#descr_eng" ).autocomplete({
         source: function(request, response) {
            $.getJSON("<?= site_url('inventory/c_stock_balance/get_stock_') ?>", {
               term : request.term,
               categoryid: $('#categoryid').val()
            }, response);
         },
         minLength: 0,
         delay: 0,
         autoFocus: true,
         select: function( event, ui ) {          
            $(this).val(ui.item.descr_eng);           
            return false;                       
         }
         })
         .focus(function(){
            $( this ).autocomplete("search");
         })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              .append( "<span>" + item.descr_eng + "</span>" )              
              .append( "</li>" )
              .appendTo( ul );
      }

   }); // ready =======

	// clear =========
   function clear(){
      $('#categoryid').val('');
      $('#stockcode').val('');
      $('#descr_eng').val('');
      $('#whcode').val('');
      $('#quantity').val('0');
      $('#others').val($("#others option:first").val());
      $('#value_compare').val('0');
      $('#quantity, #value_compare').attr('disabled', 'disabled');
      $('#is_active').val($("#is_active option:first").val());    
   }

   // grid =========
   function grid(current_page, total_display){
   	var offset = (current_page - 1)*total_display - 0;  
   	var limit = total_display - 0;
      $.ajax({
         url: '<?= site_url('inventory/c_stock_balance/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
         	$('.xmodal').show();	
         },
         complete: function(){
         	$('.xmodal').hide();
         },
         data: {
         	offset: offset,
         	limit: limit,
            categoryid: $('#categoryid').val(),
            stockcode: $('#stockcode').val(),
            descr_eng: $('#descr_eng').val(),
            // quantity: $('#quantity').val(),
            whcode: $('#whcode').val(),
            others: $('#others').val(),
            value_compare: $('#value_compare').val(),            
            is_active: $('#is_active').val(),            
   			m: '<?php echo (isset($_GET['m']) ? $_GET['m'] : ''); ?>',
   			p: '<?php echo (isset($_GET['p']) ? $_GET['p'] : ''); ?>'
         },
         success: function(data) {
            $('#balance_list tbody').html(data.tr);
            var page = '';
            var total = '';
            var show_display = '';

            if(data.totalRecord - 0 > 0){
               // previous ====
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next<i class="glyphicon glyphicon-chevron-right"></i></button>';

               show_display += ((offset + limit > data.totalRecord ? data.totalRecord : offset + limit) + ' / ' + data.totalRecord);
            }

            $('#show_display').html(show_display);
            $('.pagination').html(page);

         },
         error: function() {

         }
      });
   }
</script>