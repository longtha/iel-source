<div class="container-fluid">     
 	<div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6" style="background: #4cae4c;color: white;">
               <span class="glyphicon glyphicon-stats"></span>
               <strong>Items list</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
               <a href="javascript:;" class="btn btn-sm btn-success" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add new... item"><span class="glyphicon glyphicon-minus"></span></a>

               <a href="javascript:;" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <!-- <img src="<?= base_url('assets/images/icons/refresh.png') ?>"> -->
                  <span class="glyphicon glyphicon-refresh"></span>
               </a>
            </div>         
         </div>
      </div>
	</div>

	<div class="collapse in" id="collapseExample">             
		<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" id="f_save">

			<input type="hidden" name="stockid" id="stockid">

         <div class="col-sm-3">
            <div class="form-group">  
            	        
               <label for="categoryid">Category name<span style="color:red">*</span></label>&nbsp;
               <a href="javascript:;" name="a_add_cate" id="a_add_cate" class_="btn btn-xs btn-success" title="Add new category"><span class="glyphicon glyphicon-plus"></span>New</a>
               <select name="categoryid" id="categoryid" class="form-control" placeholder="Category name" data-parsley-required="true" data-parsley-required-message="This field require" tabindex="1">
                  <?php
                  // getoption($sql, $key, $display, $select = '', $istop = false)
                  echo getoption("SELECT * FROM sch_stock_category AS c ORDER BY c.cate_name ASC", "categoryid", "cate_name", "", true);
                  ?>
               </select>
            </div>               
         </div>

			<div class="col-sm-3">
				<div class="form-group">           
				   <label for="stockcode">Item code<span style="color:red">*</span></label>
				   <input type="text" name="stockcode" id="stockcode" class="form-control" placeholder="Item code" data-parsley-required="true" data-parsley-required-message="This field require"> 
				</div>		
			</div>

			<div class="col-sm-3">
				<div class="form-group">           
				   <label for="descr_eng">Item name<span style="color:red">*</span></label>
				   <input type="text" name="descr_eng" id="descr_eng" class="form-control" placeholder="Item name" data-parsley-required="true" data-parsley-required-message="This field require"> 
				</div>				
			</div>

			<div class="col-sm-3">
				<div class="form-group">           
				   <label for="recorder_qty">Quantity<span style="color:red"></span></label>
				   <input type="text" name="recorder_qty" id="recorder_qty" class="form-control" placeholder="Quantity" decimal readonly="readonly">
				</div>				
			</div>

         <div class="col-sm-3">
            <div class="form-group">           
              <label for="sale_price">Sale price ($)<span style="color:red"></span></label>
              <textarea rows="2" name="sale_price" id="sale_price" class="form-control" placeholder="Sale price" decimal style="resize: none;"></textarea> 
            </div>
         </div>

         <div class="col-sm-3">            
            <div class="form-group">           
               <label for="specification">Specification<span style="color:red"></span></label>
               <textarea rows="2" name="specification" id="specification" class="form-control" placeholder="Specification" style="resize: none;"></textarea> 
            </div>
         </div>

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="descr_kh">Description<span style="color:red"></span></label>
               <textarea rows="2" name="descr_kh" id="descr_kh" class="form-control" placeholder="Description" style="resize: none;"></textarea> 
            </div>
         </div>		
				
			<div class="col-sm-3">
				<div class="form-group">           
				   <label for="note">Note<span style="color:red"></span></label>
				   <textarea rows="2" name="note" id="note" class="form-control" placeholder="Note" style="resize: none;"></textarea>
				</div>
			</div>			

			<div class="col-sm-7 col-sm-offset-5">
				<div class="form-group">   
               <?php if($this->green->gAction("C")){ ?>        
				     <button type="button" class="btn btn-success btn-sm save" name="save" id="save" data-save="1">Save</button>
               <?php }?>
               <?php if($this->green->gAction("C")){ ?>  
                  <button type="button" class="btn btn-success btn-sm save" name="save_next" id="save_next" data-save="2">Save next</button>
               <?php }?>
				   <button type="button" class="btn btn-default btn-sm" name="clear" id="clear">Clear</button>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
				   <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
				</div>             
			</div>
		</form>      
	</div>
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <table border="0"​ align="center" id="item_list" class="table table-hover">
                <thead>
                     <tr style="background: #4cae4c;color: white;">
                        <th style="width: 5%;text-align: left;">No</th>
                        <th style="width: 25%;text-align: left;">Category name</th>
                        <th style="width: 15%;text-align: left;">Item code</th>
                        <th style="width: 30%;text-align: left;">Item name</th>
                        <th style="width: 15%;text-align: right;">Quantity</th>
                        <th style="width: 15%;text-align: right;"> Sale price ($)</th>
                        <th style="width: 10%;text-align: center;" colspan="2">Action</th>
                     </tr>
                     <tr>
                        <td>&nbsp;</td>
                        <td>
                        	<input type="text" class="form-control input-sm" name="search_cate" id="search_cate" placeholder="Search category" title="Search category">
                        </td>
                        <td>
                        	<input type="text" class="form-control input-sm" name="search_stockcode" id="search_stockcode" placeholder="Search item code" title="Search item code">
                        </td>
                        <td>
                        	<input type="text" class="form-control input-sm" name="search_descr_eng" id="search_descr_eng" placeholder="Search item name" title="Search item name">
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                     </tr>
                </thead>
                
   				<tbody>

   				</tbody>
   				<tfoot>
                  <tr>
                     <td colspan="1">
                        <div>&nbsp;</div>
                        <select name="to_display" id="to_display" title="Display items..." style="height: 22px;">
                           <!-- <option value="5">5</option> -->
                           <option value="10">10</option>
                           <option value="30">30</option>
                           <option value="50">50</option>
                           <option value="100">100</option>
                           <option value="200">200</option>
                           <option value="500">500</option>
                           <option value="1000">1000</option>                       
                        </select>
                     </td>
                     <td colspan="2">
                        <div>&nbsp;</div> 
                        <span id="show_display" style="margin-left: 10px;">&nbsp;</span>
                        <div class="btn-group pagination" role="group" aria-label="..." style="display: inline;"></div>
                     </td>                  
                     <td colspan="5">&nbsp;</td>
                  </tr>
   				</tfoot>
            </table>         
         </div>
      </div>

   </div>

</div>


<div class="modal bs-example-modal-lg fade m_category" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #4cae4c;color: white;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="gridSystemModalLabel">Categories</h5>
      </div>
      <div class="modal-body" style="overflow-y: auto;max-height: 550px;">
      	<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" id="f_save_cate">
	      	<input type="hidden" name="categoryid_" id="categoryid_">
	        <div class="row">
				<div class="col-md-4">
					<label for="cate_name">Category name<span style="color:red">*</span></label>
				   <input type="text" name="cate_name" id="cate_name" class="form-control input-sm" placeholder="Category name" data-parsley-required="true" data-parsley-required-message="This field require">
				</div>
				<div class="col-md-8">
					<label for="description_cate">Description<span style="color:red"></span></label>
				   <input type="text" name="description_cate" id="description_cate" class="form-control input-sm" placeholder="Description">
				</div>
				<div class="col-md-12">
					<div class="col-md-12" style="border-bottom: 1px solid #CCC;">
						&nbsp;
					</div>
				</div>
	        </div>
        </form>
        
        <div class="row">
		      <div class="col-sm-12">
		         <div class="table-responsive">
		            <table border="0"​ align="center" id="cate_list" class="table table-hover">
		               <thead>
	                     <tr>
	                        <th style="width: 8%;text-align: left;">No</th>
	                        <th style="width: 30%;text-align: left;">Category name</th>
	                        <th style="width: 50%;text-align: left;">Description</th>
	                        <th style="width: 6%;text-align: center;" colspan="2">Action</th>
	                     </tr>
	                     <tr>
	                        <td>&nbsp;</td>
	                        <td>
	                        	<input type="text" class="form-control input-sm" name="search_cate_name" id="search_cate_name" placeholder="Search category..." title="Search by category">
	                        </td>
	                        <td>&nbsp;</td>
	                        <td colspan="2">&nbsp;</td>
	                     </tr>
		               </thead>		                
							<tbody>
								
							</tbody>
							<tfoot>
							  
							</tfoot>
		            </table>         
		         </div>
		      </div>
		   </div>

      </div>
      <div class="modal-footer"> 
         <?php if($this->green->gAction("C")){ ?>
            <button type="button" id="save_cate" class="btn btn-success btn-sm save_cate" data-cate="1">Save next</button>
         <?php }?>
         <?php if($this->green->gAction("C")){ ?>
            <button type="button" id="save_close_cate" class="btn btn-success btn-sm save_cate" data-cate="2">Save close</button>
         <?php }?>
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- style -->
<style type="text/css">
   #cate_list th{vertical-align: middle;}
   #cate_list td{vertical-align: middle;}

   #item_list th{vertical-align: middle;}
   #item_list td{vertical-align: middle;}
</style>

<script type="text/javascript">
   $(function(){

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // clear =======      
      $('body').delegate('#clear', 'click', function(){
      	clear();
         $('#categoryid').val('');
         $('#save').text('Save');
         $('#save_next').show();
         $('#f_save').parsley().destroy();
   	});

      // save =======      
      $('body').delegate('.save', 'click', function(){
         var save = $(this).data('save');
         if($('#f_save').parsley().validate()){            
            $.ajax({
               url: '<?= site_url('inventory/c_stock/save') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){
                  $('.xmodal').show();
               },
               complete: function(){
                  $('.xmodal').hide();
               },
               data: {
                  stockid: $('#stockid').val(),
                  categoryid: $('#categoryid').val(),
                  stockcode: $('#stockcode').val(),
                  descr_eng: $('#descr_eng').val(),
                  recorder_qty: $('#recorder_qty').val(),                  
                  sale_price: $('#sale_price').val(),
                  descr_kh: $('#descr_kh').val(),
                  specification: $('#specification').val(),
                  note: $('#note').val()                                    
               },
               success: function(data){
                  if(data.saved){
                     if(save == 1){
                        clear();
                        $('#categoryid').val('');
                     }
                     else{
                        clear();
                        $('#stockcode').select();
                     }	                  
	                  grid(1, $('#to_display').val() - 0);
	                  toastr["success"](data.saved);	                  
	               }
	               else if(data.updated){
	                  grid(1, $('#to_display').val() - 0);
	                  clear();	
                     $('#categoryid').val('');                 
	                  toastr["success"](data.updated);

                     $('#save').text('Save');
                     $('#save_next').show();
	               }
	               else if(data.existed){
	                  toastr["warning"](data.existed);
	               }                  
               },
               error: function() {

               }
            });
         }
      });

      // edit ========
      $('body').delegate('.edit', 'click', function(){
         var stockid = $(this).data('stockid');

         $.ajax({
            url: '<?= site_url('inventory/c_stock/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
               $('.xmodal').show();
            },
            complete: function(){
               $('.xmodal').hide();
            },
            data: {
               stockid: stockid
            },
            success: function(data){
               $('#stockid').val(data.stockid);
               $('#categoryid').val(data.categoryid);
               $('#stockcode').val(data.stockcode);
               $('#descr_eng').val(data.descr_eng);
               $('#recorder_qty').val(data.recorder_qty - 0 > 0 ? data.recorder_qty : '');
               $('#sale_price').val(data.sale_price - 0 > 0 ? data.sale_price : '');
               $('#descr_kh').val(data.descr_kh);
               $('#specification').val(data.specification);
               $('#note').val(data.note);               

               $('#f_save').parsley().destroy();
               $('#save').text('Update');
               $('#save_next').hide();
               $('#collapseExample').addClass('in');

               $('#a_addnew').find('span').removeClass('glyphicon-plus');
               $('#a_addnew').find('span').addClass('glyphicon-minus');
            },
            error: function() {

            }
         });         
         
      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var stockid = $(this).data('stockid');
         var stockid_chk = $(this).data('stockid_chk') - 0;
        
         if(stockid_chk > 0){
            toastr["warning"]("Can't delete! data in process...");
         }else{
            if(window.confirm('Are you sure to delete?')){
               $.ajax({
                  url: '<?= site_url('inventory/c_stock/delete') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     stockid: stockid
                  },
                  success: function(data){
                     if(data == 1){
                        toastr["warning"]("Deleted!");
                        clear();
                        $('#save').text('Save');
                        $('#save_next').show();
                        grid(1, $('#to_display').val() - 0);
                     }else{
                        toastr["warning"]("Can't delete!");
                     }               
                     
                  },
                  error: function() {

                  }
               });
            }
         }        
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#f_save').parsley().destroy();

         if($(this).find('span').hasClass('glyphicon-minus')){
            $(this).find('span').removeClass('glyphicon-minus');
            $(this).find('span').addClass('glyphicon-plus');            
         }
         else{
            $(this).find('span').removeClass('glyphicon-plus');
            $(this).find('span').addClass('glyphicon-minus');
         }
      });

      // init. =======
      grid(1, $('#to_display').val() - 0);
      // search ========
      $('body').delegate('#search_cate', 'keyup', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#search_stockcode', 'keyup', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#search_descr_eng', 'keyup', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#to_display', 'change', function(){
         grid(1, $('#to_display').val() - 0);
      });      
      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#to_display').val() - 0);
      });
      


      // category ======================
      // save cate. =======      
      $('body').delegate('.save_cate', 'click', function(){
      	var save = $(this).data('cate');
      	if($('#f_save_cate').parsley().validate()){
	         $.ajax({
	            url: '<?= site_url('inventory/c_stock/save_category') ?>',
	            type: 'POST',
	            datatype: 'JSON',
	            // async: false,
	            beforeSend: function(){
	            	$('.xmodal').show();
	            },
	            complete: function(){
						$('.xmodal').hide();
	            },
	            data: {
	            	categoryid: $('#categoryid_').val(),
	            	cate_name: $('#cate_name').val(),
	            	description: $('#description_cate').val()	            	
	            },
	            success: function(data){                  
	               if(data.saved){	                  
	                  if(save == 2){
	                  	$('.m_category').modal('hide')
	                  }
	                  grid_cate();
	                  clear_cate();	     
	                  $('#cate_name').select();             
	                  toastr["success"](data.saved);
                     $('#categoryid').attr('data-last_categoryid', data.last_categoryid);	                  
	               }
	               else if(data.updated){
	               	if(save == 2){
	                  	$('.m_category').modal('hide')
	                  }
                     clear_cate();
                     $('#save_cate').text('Save next');
                     $('#save_close_cate').show();
	                  grid_cate();	                  	     
	                  $('#cate_name').select();             
	                  toastr["success"](data.updated);
                     $('#categoryid').attr('data-last_categoryid', data.last_categoryid);
	               }
	               else{
	                  toastr["warning"](data.existed);
	               }
	            },
	            error: function() {

	            }
	         });
         }
      });

      // edit cate. ========
      $('body').delegate('.edit_cate', 'click', function(){
         var categoryid = $(this).data('categoryid');

         $.ajax({
            url: '<?= site_url('inventory/c_stock/edit_category') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
					$('.xmodal').show();
            },
            complete: function(){
					$('.xmodal').hide();
            },
            data: {
               categoryid: categoryid
            },
            success: function(data){
               $('#categoryid_').val(data.categoryid);
               $('#cate_name').val(data.cate_name);
               $('#description_cate').val(data.description);               
               $('#f_save_cate').parsley().destroy();
               $('#save_cate').text('Update');
               $('#save_close_cate').hide();
            },
            error: function() {

            }
         });
      });

      // delete cate. ========
      $('body').delegate('.delete_cate', 'click', function(){
         var categoryid = $(this).data('categoryid');
         var categoryid_c = $(this).data('categoryid_c') - 0;

         if(categoryid_c > 0){
            toastr["warning"]("Can't delete! data in process...");
         }else{
            if(window.confirm('Are you sure to delete?')){
               $.ajax({
                  url: '<?= site_url('inventory/c_stock/delete_category') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     categoryid: categoryid
                  },
                  success: function(data){
                     if(data == 1){
                        toastr["warning"]("Data deleted!");
                        clear_cate();
                        $('#save_cate').text('Save next');
                        $('#save_close_cate').show();
                        grid_cate();
                     }else{
                        toastr["warning"]("Can't delete!");
                     }               
                     
                  },
                  error: function() {

                  }
               });
            }
         }         
      });

		// ini. modal cate. ======      
      $('body').delegate('#a_add_cate', 'click', function(){
         $('#categoryid').removeAttr('data-last_categoryid');      	
      	grid_cate_();      	 
      });

      // search cate. ====
      $('body').delegate('#search_cate_name', 'keyup', function(){
      	grid_cate();
      });

   }); // ready =======

	// clear =========
   function clear(){
      $('#stockid').val('');      
      $('#stockcode').val('');
      $('#descr_eng').val('');
      $('#recorder_qty').val('');
      $('#sale_price').val('');
      $('#descr_kh').val('');      
      $('#specification').val('');      
      $('#note').val('');      
   }

   // clear cate. =======
   function clear_cate(){
      $('#categoryid_').val('');     
      $('#cate_name').val('');
      $('#description_cate').val('');      
   }

   // get cate. ====
   function get_category(){
   	$.ajax({
         url: '<?= site_url('inventory/c_stock/get_category') ?>',
         type: 'POST',
         datatype: 'Html',
         beforeSend: function(){
         	$('.xmodal').show();
         },
         complete: function(){
         	$('.xmodal').hide();
         },
         data: {
        	   last_categoryid: $('#categoryid').attr('data-last_categoryid')
         },
         success: function(data){
            $('#categoryid').html(data);         
         },
         error: function() {

         }
      });
   }

   // grid cate. ====
   function grid_cate(){
   	$.ajax({
         url: '<?= site_url('inventory/c_stock/grid_category') ?>',
         type: 'POST',
         datatype: 'Html',
         // async: false,
         beforeSend: function(){
         	$('.xmodal').show();
         },
         complete: function(){
         	$('.xmodal').hide();
         },
         data: {
         	search_cate_name: $('#search_cate_name').val()         	
         },
         success: function(data){
            $('#cate_list tbody').html(data.tr);              
         	$('#f_save_cate').parsley().destroy();    
         	get_category();
         },
         error: function() {

         }
      });
   }

   function grid_cate_(){
   	$.ajax({
         url: '<?= site_url('inventory/c_stock/grid_category') ?>',
         type: 'POST',
         datatype: 'Html',
         // async: false,
         beforeSend: function(){
         	$('.xmodal').show();
         },
         complete: function(){
         	$('.xmodal').hide();
         },
         data: {
         	search_cate_name: $('#search_cate_name').val()         	
         },
         success: function(data){
            $('#cate_list tbody').html(data.tr);
            $('.m_category').modal({
	      		backdrop: 'static'
	      	})
            clear_cate();
	      	$('#cate_name').select();             
         	$('#f_save_cate').parsley().destroy();  

            $('#save_cate').text('Save next');
            $('#save_close_cate').show();  
         },
         error: function() {

         }
      });
   }


   // grid =========
   function grid(current_page, total_display){
   	var offset = (current_page - 1)*total_display - 0;  
   	var limit = total_display - 0;
      $.ajax({
         url: '<?= site_url('inventory/c_stock/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
         	$('.xmodal').show();	
         },
         complete: function(){
         	$('.xmodal').hide();
         },
         data: {
         	offset: offset,
         	limit: limit,
            search_cate: $('#search_cate').val(),
            search_stockcode: $('#search_stockcode').val(),
            search_descr_eng: $('#search_descr_eng').val(),
   			m: '<?php echo (isset($_GET['m']) ? $_GET['m'] : ''); ?>',
   			p: '<?php echo (isset($_GET['p']) ? $_GET['p'] : ''); ?>'
         },
         success: function(data) {
            $('#item_list tbody').html(data.tr);

            var page = '';
            var total = '';
            var show_display = '';
            if(data.totalRecord - 0 > 0){
               // previous ====
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next<i class="glyphicon glyphicon-chevron-right"></i></button>';

               show_display += ((offset + limit > data.totalRecord ? data.totalRecord : offset + limit) + ' / ' + data.totalRecord);
            }

            $('#show_display').html(show_display);
            $('.pagination').html(page);

         },
         error: function() {

         }
      });
   }
</script>