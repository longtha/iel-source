<div class="container-fluid">     
 	<div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6" style="background: #4cae4c;color: white;">
               <span class="glyphicon glyphicon-stats"></span>
               <strong>Opening Balances</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
                  <a href="javascript:;" class="btn btn-sm btn-success" id="a_addnew" data-toggle="tooltip" data-placement="top" title="New opening balance"><span class="glyphicon glyphicon-plus"></span></a>
               <?php if($this->green->gAction("R")){ ?>   
                  <a href="javascript:;" class="btn btn-sm btn-success" id="a_search" data-toggle="tooltip" data-placement="top" title="Search"><span class="glyphicon glyphicon-search"></span></a>
               <?php }?>   
                  <a href="javascript:;" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh"><span class="glyphicon glyphicon-refresh"></span></a>
                        
            </div>         
         </div>
      </div>
	</div>

	<div class="in collapse" id="collapseExample">             
		<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" id="f_save">
			<div class="col-sm-3">
				<div class="form-group">           
					<label for="cate_id">Category name<span style="color:red"></span></label>&nbsp;
					<select name="cate_id" id="cate_id" class="form-control" placeholder="Category name" data-parsley-required="true" data-parsley-required-message="This field require">
						<?php
						// getoption($sql, $key, $display, $select = '', $istop = false)
						echo getoption("SELECT * FROM sch_stock_category AS c ORDER BY c.cate_name ASC", "categoryid", "cate_name", "", true);
						?>
					</select>
				</div>					
			</div>

			<div class="col-sm-3">
				<div class="form-group">           
				   <label for="item_code">Item code<span style="color:red"></span></label>
				   <input type="text" name="item_code" id="item_code" class="form-control" placeholder="Item code"> 
				</div>	
			</div>

			<div class="col-sm-3">
				<div class="form-group">           
				   <label for="item_name">Item name<span style="color:red"></span></label>
				   <input type="text" name="item_name" id="item_name" class="form-control" placeholder="Item name"> 
				</div>			
			</div>

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="qty">Quantity<span style="color:red"></span></label>
               <input type="text" name="qty" id="qty" class="form-control" placeholder="Quantity" decimal> 
            </div>         
         </div>

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="selling_price">Sale price<span style="color:red"></span></label>
               <input type="text" name="selling_price" id="selling_price" class="form-control" placeholder="Sale price" decimal> 
            </div>         
         </div>

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="whcode">Location<span style="color:red"></span></label>
               <select name="whcode" id="whcode" class="form-control" placeholder="Location">
                  <?php
                  // getoption($sql, $key, $display, $select = '', $istop = false)
                  echo getoption("SELECT * FROM sch_stock_wharehouse AS w ORDER BY w.wharehouse ASC", "whcode", "wharehouse", "", true);
                  ?>
               </select> 
            </div>         
         </div>

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="from_date">From date<span style="color:red"></span></label>
               <input type="text" name="from_date" id="from_date" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>"> 
            </div>         
         </div>

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="to_date">To date<span style="color:red"></span></label>
               <input type="text" name="to_date" id="to_date" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
            </div>         
         </div>

			<div class="col-sm-7 col-sm-offset-5">
				<div class="form-group"> 
               <?php if($this->green->gAction("R")){ ?>             
				     <button type="button" class="btn btn-success btn-sm" name="search" id="search">Search</button>
               <?php } ?>   
				   <button type="button" class="btn btn-default btn-sm" name="clear" id="clear">Clear</button>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
				   <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
				</div>             
			</div>
		</form>      
	</div>
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <table border="0"​ cellspacing="0" cellpadding="0" align="center" id="item_list" class="table table-hover">
                <thead>
                     <tr style="background: #4cae4c;color: white;">
                        <th style="width: 5%;text-align: left;">No</th>
                        <th style="width: 20%;text-align: left;">Category name</th>
                        <th style="width: 10%;text-align: left;">Item code</th>
                        <th style="width: 20%;text-align: left;">Item name</th>
                        <th style="width: 15%;text-align: right;">Quantity</th>
                        <th style="width: 15%;text-align: right;">Sale price ($)</th>
                        <th style="width: 15%;text-align: right;">Amount ($)</th>
                        <th style="width: 10%;text-align: center;" colspan="2">Action</th>
                     </tr>
                </thead>
                
				<tbody>
					
				</tbody>
				<tfoot>
               <tr>
                  <td colspan="1">
                     <div>&nbsp;</div>
                     <select name="to_display" id="to_display" title="Display items..." style="height: 22px;">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="30">30</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="500">500</option>
                        <!-- <option value="1000">1000</option> -->
                     </select>
                  </td>
                  <td colspan="2">
                     <div>&nbsp;</div> 
                     <span id="show_display" style="margin-left: 10px;">&nbsp;</span>
                     <div class="btn-group pagination" role="group" aria-label="..." style="display: inline;"></div>
                  </td>                  
                  <td colspan="6">&nbsp;</td>
               </tr>
				</tfoot>
            </table>         
         </div>
      </div>

   </div>

</div>


<div class="modal bs-example-modal-lg fade m_master_detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #4cae4c;color: white;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="gridSystemModalLabel">Opening Balances</h5>
      </div>
      <div class="modal-body" style="overflow-y: auto;max-height: 550px;">
      	<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" id="f_save_cate">
	      	<input type="hidden" name="typeno" id="typeno">
	        <div class="row">
				<div class="col-md-3">
					<label for="tran_date">Date<span style="color:red">*</span></label>
				   <input type="text" name="tran_date" id="tran_date" class="form-control input-sm" placeholder="dd/mm/yyyy" data-parsley-required="true" data-parsley-required-message="This field require" value="<?= date('d/m/Y') ?>">
				</div>
				<div class="col-md-3">
					<label for="whcode">Location<span style="color:red">*</span></label>
				   <select name="whcode_cate" id="whcode_cate" class="form-control input-sm" placeholder="Location" data-parsley-required="true" data-parsley-required-message="This field require">
                  <?php
                  // getoption($sql, $key, $display, $select = '', $istop = false)
                  echo getoption("SELECT * FROM sch_stock_wharehouse AS w ORDER BY w.wharehouse ASC", "whcode", "wharehouse", "", true);
                  ?>
               </select>
				</div>
            <div class="col-md-6">
               <label for="note">Note<span style="color:red"></span></label>
               <input type="text" name="note" id="note" class="form-control input-sm" placeholder="Note">
            </div>            
				<div class="col-md-12">
					<div class="col-md-12" style="border-bottom: 1px solid #CCC;">
						&nbsp;
					</div>
				</div>
	        </div>
        </form>
        
        <div class="row">
		      <div class="col-sm-12">
		         <div class="table-responsive">
		            <table border="0"​ cellspacing="0" cellpadding="0" align="center" id="stock_detail_list" class="table table-hover table-condensed">
		                <thead>
		                     <tr>
		                        <th style="width: 5%;text-align: left;">No</th>
		                        <th style="width: 15%;text-align: left;">Item code</th>
		                        <th style="width: 25%;text-align: left;">Item name</th>
                              <th style="width: 30%;text-align: left;">Remark</th>
                              <th style="width: 10%;text-align: right;">Quantity</th>
                              <th style="width: 14%;text-align: right;">Sale price ($)</th>
		                        <th style="width: 5%;text-align: center;" colspan="1">Action</th>
		                     </tr>
		                </thead>
		                
							<tbody>
								
							</tbody>
							<tfoot>
							  
							</tfoot>
		            </table>         
		         </div>
		      </div>
		   </div>

      </div>
      <div class="modal-footer"> 
         <?php if($this->green->gAction("C")){ ?>     
           <button type="button" id="save_cate" class="btn btn-success btn-sm save_cate" data-cate="1">Save next</button>
         <?php } ?> 
         <?php if($this->green->gAction("C")){ ?> 
           <button type="button" id="save_close_cate" class="btn btn-success btn-sm save_cate" data-cate="2">Save close</button>
         <?php } ?> 
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- style -->
<style type="text/css">
   .ui-autocomplete-loading {
      background: white url("<?= base_url('assets/images/ui-anim_basic_16x16.gif') ?>") right center no-repeat;
   }
   .ui-autocomplete{height: auto;overflow-y: hidden;}
   .ui-menu .ui-menu-item:hover, .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus {
      background: #4cae4c;
      color: white;
   }

   #stock_detail_list th{vertical-align: middle;}
   #stock_detail_list td{vertical-align: middle;} 

   .input-xs {
       height: 22px;
       padding: 5px 5px;
       font-size: 12px;
       line-height: 1.5;
       border-radius: 3px;
   }  
</style>

<script type="text/javascript">
   $(function(){

      // select ========
      $('body').delegate('.qty, .selling_price, .stockcode', 'focus', function(){
         $(this).select();
      });

      // chk ========
      $('body').delegate('.qty', 'focusout', function(){
         if($.trim($(this).val()) - 0 == 0){
            $(this).val('1');
         }
      });
      $('body').delegate('.selling_price', 'focusout', function(){
         if($.trim($(this).val()) - 0 == 0){
            $(this).val('0.00');
         }
      });

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // clear =======      
      $('body').delegate('#clear', 'click', function(){
      	clear();
         grid(1, $('#to_display').val() - 0);
   	});

      // search =======      
      $('body').delegate('#search', 'click', function(){
         grid(1, $('#to_display').val() - 0);
      });

      // edit ========
      $('body').delegate('.edit', 'click', function(){
         var typeno = $(this).data('typeno');
         $.ajax({
            url: '<?= site_url('inventory/c_opening_balance/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
               $('.xmodal').show();
            },
            complete: function(){
               $('.xmodal').hide();
            },
            data: {
               typeno: typeno
            },
            success: function(data){
               $('.m_master_detail').modal({
                  backdrop: 'static'
               })
               $('#typeno').val(data.row.typeno);
               $('#tran_date').val(data.row.tran_date);               
               $('#note').val(data.row.note);
               
               var tr = '';
               if(data.q.length > 0){
                  $.each(data.q, function(i, v){
                     $('#whcode_cate').val(v.whcode);
                     tr += '<tr>\
                              <td class="no">'+ (i + 1) +'</td>\
                              <td>\
                                 <input type="text" class="form-control input-sm stockcode" name="stockcode[]" placeholder="Item code" value="'+ (v.stockcode != null ? v.stockcode : '') +'" data-categoryid="'+ v.cate_id +'" data-stockid="'+ v.stockid +'"></td>\
                              <td class="descr_eng">'+ (v.descr_eng != null ? v.descr_eng : '') +'</td>\
                              <td>\
                                 <input type="text" class="form-control input-sm remark" name="remark[]" placeholder="Remark" style="_resize: none;" value="'+ (v.note != null ? v.note : '') +'"></td>\
                              <td style="text-align: right;">\
                                 <input type="text" class="form-control input-sm qty" name="qty[]" style="text-align: right;" value="'+ (v.qty - 0 > 0 ? v.qty : '1') +'" decimal></td>\
                              <td style="text-align: right;">\
                                 <input type="text" class="form-control input-sm selling_price" name="selling_price[]" style="text-align: right;" value="'+ (v.selling_price - 0 > 0 ? v.selling_price : '0.00') +'" decimal></td>\
                              <td style="text-align: center;"><?php if($this->green->gAction("D")){ ?><a href="javascript:;" class="btn btn-xs btn-danger del_d"><span class="glyphicon glyphicon-trash"></span></a><?php } ?> </td>\
                           </tr>';
                     if(data.q.length - (i + 1) == 0){
                        tr += '<tr>\
                                    <td class="no">'+ (i + 2) +'</td>\
                                    <td>\
                                       <input type="text" class="form-control input-sm stockcode" name="stockcode[]" placeholder="Item code"></td>\
                                    <td class="descr_eng"></td>\
                                    <td>\
                                       <input type="text" class="form-control input-sm remark" name="remark[]" placeholder="Remark" style="_resize: none;"></td>\
                                    <td style="text-align: right;">\
                                       <input type="text" class="form-control input-sm qty" name="qty[]" style="text-align: right;" value="1" decimal></td>\
                                    <td style="text-align: right;">\
                                       <input type="text" class="form-control input-sm selling_price" name="selling_price[]" value="0.00" style="text-align: right;"></td>\
                                    <td style="text-align: center;"><?php if($this->green->gAction("D")){ ?>   <a href="javascript:;" class="btn btn-xs btn-danger del_d"><span class="glyphicon glyphicon-trash"></span></a><?php } ?></td>\
                                 </tr>';
                     }
                  });
                  $('#stock_detail_list tbody').html(tr);
               }
               else{
                  add_row_();
               }               
               
               $('#f_save_cate').parsley().destroy();
               $('#save_cate').text('Update');
               $('#save_close_cate').hide();            
            },
            error: function() {

            }
         });         
         
      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var typeno = $(this).data('typeno');
         if(window.confirm('Are you sure to delete?')){
            $.ajax({
               url: '<?= site_url('inventory/c_opening_balance/delete') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  typeno: typeno
               },
               success: function(data){
                  if(data == 1){
                     toastr["warning"]("Deleted!");
                     // clear();
                     grid(1, $('#to_display').val() - 0);
                  }else{
                     toastr["warning"]("Can't delete!");
                  }               
                  
               },
               error: function() {

               }
            });
         }        
      });

      // search =======
      $('body').delegate('#a_search', 'click', function(){
         $('#collapseExample').collapse('toggle')
         // clear();
      });

      // add new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('.m_master_detail').modal({
            backdrop: 'static'
         })         
         $('#tran_date').val('<?= date("d/m/Y") ?>');
         $('#save_cate').text('Save next');
         $('#save_close_cate').show();
         $('#f_save_cate').parsley().destroy();
         clear_();
      });

      // init. =======
      grid(1, $('#to_display').val() - 0);
      // search ========
      $('body').delegate('#search_cate', 'keyup', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#search_item_code', 'keyup', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#search_item_name', 'keyup', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#to_display', 'change', function(){
         grid(1, $('#to_display').val() - 0);
      });      
      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#to_display').val() - 0);
      });      


      // diag. ======================
      // save master_detail. =======      
      $('body').delegate('.save_cate', 'click', function(){
      	var save = $(this).data('cate');
         var typeno = $('#typeno').val();
         var tran_date = $('#tran_date').val();
         var whcode_cate = $('#whcode_cate').val();
         var note = $('#note').val();
         
         var arr =[];
         var j = 0;
         $('.stockcode').each(function(i){
            var stockcode = $(this).val();            
            if($.trim(stockcode) != ""){
               j++;
            }

            var tr = $(this).parent().parent();
            var categoryid = $(this).data('categoryid');
            var stockid = $(this).data('stockid');                        
            var remark = tr.find('.remark').val();
            var qty = tr.find('.qty').val();
            var selling_price = tr.find('.selling_price').val();
            arr[i] = {categoryid: categoryid, stockid: stockid, stockcode: stockcode, remark: remark, qty: qty, selling_price: selling_price};
         });

         if(j == 0){
            toastr["warning"]("At least choose a item to save!");
         }
         else{
         	if($('#f_save_cate').parsley().validate()){
   	         $.ajax({
   	            url: '<?= site_url('inventory/c_opening_balance/save_op_md') ?>',
   	            type: 'POST',
   	            datatype: 'JSON',
   	            // async: false,
   	            beforeSend: function(){
   	            	$('.xmodal').show();
   	            },
   	            complete: function(){
   						$('.xmodal').hide();
   	            },
   	            data: {
   	            	tran_date: tran_date,
                     typeno: typeno,
   	            	note: note,
                     whcode_cate: whcode_cate,
   	            	arr: arr	            	
   	            },
   	            success: function(data){
   	               if(data == 1){	                  
   	                  if(save == 2){
   	                  	$('.m_master_detail').modal('hide')
   	                  }
   	                  grid(1, $('#to_display').val() - 0);
   	                  clear_();	     
   	                  toastr["success"]('Saved!');	                  
   	               }
   	               else if(data == 2){
   	               	if(save == 2){
                           $('.m_master_detail').modal('hide')
                        }
                        grid(1, $('#to_display').val() - 0);
                        clear_(); 
                        $('#whcode_cate').val('');            
   	                  toastr["success"]('Updated!');                     
                        $('#save_cate').text('Save next');
                        $('#save_close_cate').show();
   	               }
   	               else{
   	                  toastr["warning"]("Can't save!");
   	               }         
   	            
   	            },
   	            error: function() {

   	            }
   	         });
            }
         }
      });

      $('#tran_date').datepicker({
         format: 'dd/mm/yyyy',
         autoclose: true
      });
      $('#from_date').datepicker({
         format: 'dd/mm/yyyy',
         autoclose: true
      });
      $('#to_date').datepicker({
         format: 'dd/mm/yyyy',
         autoclose: true
      });

      // search cate. ====
      $('body').delegate('#search_cate_name', 'keyup', function(){
      	grid_cate();
      });

      // add row ======
      add_row();

      // remove =======
      $('body').delegate('.del_d', 'click', function(){
         if($('.no').length > 1){
            $(this).parent().parent().remove();
            $('.no').each(function(i) {
               $(this).html(i + 1);
            });
         }
         else{
            toastr["warning"]("Only one row can't deleted!");
         }
      });

      // autocomplete ========
      $('body').delegate(".stockcode", 'focus', function(e){
         $( this ).autocomplete({
         source: "<?= site_url('inventory/c_opening_balance/get_std') ?>",
         minLength: 0,
         delay: 0,
         autoFocus: true,
         focus: function( event, ui ) {
            // $(this).val(ui.item.stockcode);
            return false;
         },
         select: function(event, ui) { //if (event.keyCode == 13 || event.keyCode == 9) {          
            var tr = $(this).parent().parent();
            var stockcode = ui.item.stockcode;
            var b = false;
            var j = 1;            
            $('.stockcode').not(this).each(function(){
               j++;
               var stockcode_chk = $(this).val();
               if(j > 1){
                  if(stockcode_chk == stockcode){
                     b = true;             
                  } 
               }            
            });            
            if(b == true){
               toastr["warning"]('Item code "' + stockcode + '" existed!');
               $(this).val('');
               $(this).blur();              
            }
            else{
               $(this).attr("data-categoryid", ui.item.categoryid);
               $(this).attr("data-stockid", ui.item.stockid);            
               $(this).val(ui.item.stockcode);                                      
               tr.find('.descr_eng').text(ui.item.descr_eng);
               tr.find('.qty').val(1);
               if (event.keyCode == 9) {
                  tr.find('.remark').select();
               }
               else{
                  tr.find('.qty').select();
               }                           
               tr.find('.selling_price').val(ui.item.sale_price - 0 > 0 ? ui.item.sale_price : '0.00');
               if($('#stock_detail_list tr:last').find('.stockcode').val() != ""){              
                  add_row();
               }
            }
            return false;                       
         }//}
         })
         .focus(function(){
            $( this ).autocomplete("search");
         })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              .append( "<span>" + item.stockcode + "</span> | " )
              .append( "<span>" + item.descr_eng + "</span>" )              
              .append( "</li>" )
              .appendTo( ul );
         }
      });


      // focusout =====
      $('body').delegate('.stockcode', 'focusout', function(e){
         var stockcode = $(this).val();
         var stockcode_ = $(this);
         var tr = stockcode_.parent().parent();

         if($.trim(stockcode) != ""){
            $.ajax({
               url: '<?= site_url('inventory/c_opening_balance/chk_stockcode') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  stockcode: stockcode
               },
               success: function(data){
                  if(data.n){
                     toastr["warning"](data.n);
                     stockcode_.removeAttr("data-categoryid");
                     stockcode_.removeAttr("data-stockid");
                     stockcode_.val('');
                     stockcode_.select();                        
                     tr.find('.descr_eng').text('');
                     tr.find('.remark').val('');
                     tr.find('.qty').val('1');
                     tr.find('.selling_price').val('0.00');
                  }
                  else{
                     // ==========                        
                  }

               },
               error: function() {

               }
            });
         }
      });

      $( "#item_code" ).autocomplete({
         source: function(request, response) {
            $.getJSON("<?= site_url('inventory/c_opening_balance/get_stock') ?>", {
               term : request.term,
               categoryid: $('#cate_id').val()
            }, response);
         },
         minLength: 0,
         delay: 0,
         autoFocus: true,
         select: function( event, ui ) {          
            $(this).val(ui.item.stockcode);           
            return false;                       
         }
         })
         .focus(function(){
            $( this ).autocomplete("search");
         })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              .append( "<span>" + item.stockcode + "</span>" )              
              .append( "</li>" )
              .appendTo( ul );
      }

      $( "#item_name" ).autocomplete({
         source: function(request, response) {
            $.getJSON("<?= site_url('inventory/c_opening_balance/get_stock_') ?>", {
               term : request.term,
               categoryid: $('#cate_id').val()
            }, response);
         },
         minLength: 0,
         delay: 0,
         autoFocus: true,
         select: function( event, ui ) {          
            $(this).val(ui.item.descr_eng);           
            return false;                       
         }
         })
         .focus(function(){
            $( this ).autocomplete("search");
         })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              .append( "<span>" + item.descr_eng + "</span>" )              
              .append( "</li>" )
              .appendTo( ul );
      }

   }); // ready =======

   
   function add_row(){
      var tr = "";
      var i = $('.no').length + 1;
      tr += '<tr>\
                  <td class="no">'+ i +'</td>\
                  <td>\
                     <input type="text" class="form-control input-sm stockcode" name="stockcode[]" placeholder="Item code" data-parsley-required="true" data-parsley-required-message="This field require"></td>\
                  <td class="descr_eng"></td>\
                  <td>\
                     <input type="text" class="form-control input-sm remark" name="remark[]" placeholder="Remark" style="_resize: none;"></td>\
                  <td style="text-align: right;">\
                     <input type="text" class="form-control input-sm qty" name="qty[]" placeholder="0" style="text-align: right;" value="1" decimal></td>\
                  <td style="text-align: right;">\
                     <input type="text" class="form-control input-sm selling_price" name="selling_price[]" value="0.00" style="text-align: right;"></td>\
                  <td style="text-align: center;"><?php if($this->green->gAction("D")){ ?><a href="javascript:;" class="btn btn-xs btn-danger del_d"><span class="glyphicon glyphicon-trash"></span></a><?php } ?></td>\
               </tr>';
      $('#stock_detail_list tbody').append(tr);      
   }

   function add_row_(){
      var tr = '';
      tr += '<tr>\
                  <td class="no"></td>\
                  <td>\
                     <input type="text" class="form-control input-sm stockcode" name="stockcode[]" placeholder="Item code" data-parsley-required="true" data-parsley-required-message="This field require"></td>\
                     <input type="hidden" class="stockcode_chk" name="stockcode_chk[]"></td>\
                  <td class="descr_eng"></td>\
                  <td>\
                     <input type="text" class="form-control input-sm remark" name="remark[]" placeholder="Remark" style="_resize: none;"></td>\
                  <td style="text-align: right;">\
                     <input type="text" class="form-control input-sm qty" name="qty[]" placeholder="0" style="text-align: right;" value="1" decimal></td>\
                  <td style="text-align: right;">\
                     <input type="text" class="form-control input-sm selling_price" name="selling_price[]" value="0.00" style="text-align: right;"></td>\
                  <td style="text-align: center;"><?php if($this->green->gAction("D")){ ?> <a href="javascript:;" class="btn btn-xs btn-danger del_d"><span class="glyphicon glyphicon-trash"></span></a><?php } ?></td>\
               </tr>';
      $('#stock_detail_list tbody').html(tr);
      $('.no').each(function(i) {
         $(this).html(i + 1);
      });
   }

	// clear =========
   function clear(){
      $('#cate_id').val('');
      $('#item_code').val('');
      $('#item_name').val('');
      $('#qty').val('');
      $('#selling_price').val('');
      $('#from_date').val('<?= date('d/m/Y') ?>');      
      $('#to_date').val('<?= date('d/m/Y') ?>'); 
      $('#whcode').val(''); 
   }

   // clear_ =======
   function clear_(){
      $('#tran_date').val('<?= date("d/m/Y") ?>');
      $('#typeno').val('');
      $('#note').val('');
      add_row_();      
   }

   // get cate. ====
   function get_category(){
   	$.ajax({
         url: '<?= site_url('inventory/c_opening_balance/get_category') ?>',
         type: 'POST',
         datatype: 'Html',
         beforeSend: function(){
         	// $('.xmodal').show();
         },
         complete: function(){
         	// $('.xmodal').hide();
         },
         data: {
        	
         },
         success: function(data){
            $('#cate_id').html(data);         
         },
         error: function() {

         }
      });
   }

   // grid =========
   function grid(current_page, total_display){
   	var offset = (current_page - 1)*total_display - 0;  
   	var limit = total_display - 0;
      $.ajax({
         url: '<?= site_url('inventory/c_opening_balance/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
         	$('.xmodal').show();	
         },
         complete: function(){
         	$('.xmodal').hide();
            // $('.xmodal').css('display', 'block');
         },
         data: {
         	offset: offset,
         	limit: limit,
            categoryid: $('#cate_id').val(),
            stockcode: $('#item_code').val(),
            descr_eng: $('#item_name').val(),
            qty: $('#qty').val(),
            selling_price: $('#selling_price').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            whcode: $('#whcode').val()                                    
         },
         success: function(data) {
            $('#item_list tbody').html(data.tr);

            var page = '';
            var total = '';
            var show_display = '';
            if(data.totalRecord - 0 > 0){
               // previous ====
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next<i class="glyphicon glyphicon-chevron-right"></i></button>';

               show_display += ((offset + limit > data.totalRecord ? data.totalRecord : offset + limit) + ' / ' + data.totalRecord);
            }

            $('#show_display').html(show_display);
            $('.pagination').html(page);                        

         },
         error: function() {

         }
      });
   }
</script>