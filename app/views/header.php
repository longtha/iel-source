<?php
	$user_name=$this->session->userdata('user_name');
	if($user_name==""){
		$this->green->goToPage(site_url('login'));
	}
	#====== get menu ============
	//print_r($this->session->userdata('moduleids'));
	$menu="";
	$roleid=$this->session->userdata('roleid');
	$modules=$this->session->userdata('ModuleInfors');
	// print_r($modules);
	$pages=$this->session->userdata('PageInfors');
	// print_r($pages[3]);
    $year=$this->session->userdata('year');
    $schoolid=$this->session->userdata('schoolid');

    if(isset($_REQUEST['yearid']) && $_REQUEST['yearid']!=""){
        $year=$_REQUEST['yearid'];
    }elseif(isset($_REQUEST['y']) && $_REQUEST['y']!=""){
        $year=$_REQUEST['y'];
    }elseif(isset($_REQUEST['year']) && $_REQUEST['year']!=""){
        $year=$_REQUEST['year'];
    }
    $this->green->setActiveUser($this->session->userdata('userid'));
    $this->green->setActiveRole($roleid);
    if(isset($_GET['m'])){
        $this->green->setActiveModule($_GET['m']);
    }
    if(isset($_GET['p'])){
        $this->green->setActivePage($_GET['p']);
    }


    if(count($modules)>0){

		foreach ($modules as $row) {
            if(isset($row['mod_position'])){
                if($row['mod_position']=='2'){

                    $menu.='<li>
    		                <a href="#" class="a-menu"><i class="fa fa-wrench fa-fw"></i> <b>'.$row['module_name'].'</b><span class="fa arrow"></span></a>';
                    if(count($pages)>0){

                        if(isset($pages[$row['moduleid']])){
                            $page_mod=$pages[$row['moduleid']];
// print_r($page_mod);
                            if(count($page_mod)>0 && isset($pages[$row['moduleid']])){

                                $menu.='<ul class="nav nav-second-level">';
                                foreach($page_mod as $page){
                                    $page_link=isset($page['link'])?$page['link']:"";
                                  	if($page_link != ""){
                                       $menu.='<li>
        					                        <a href="'.site_url($page_link).'?y='.$year.'&m='.urlencode($row['moduleid']).'&p='.urlencode($page['pageid']).'" class="a-menu"><i class="fa '.$page['icon'].' fa-fw"></i> '.$page['page_name'].'</a>
        					                    </li>';
                                    }
                                }
                                $menu.='</ul>';
                            }
                        }

                    }


                    $menu.='</li>';
                }
            }
		}
	}

    // ------------------------------------ start long tha working---------------------------------------
    $setmonth = date('m');
    $setnum_mp = 3;
    $this->green->set_montly_permission($year,$setmonth,$setnum_mp); //Montly permission >=3

    $setnum_yp =5;
    $this->green->set_yearly_permission($year,$setnum_yp); //Yearly permission >=5

    $setnover2 = 2;
    $this->green->set_montly_permission_bigger_one($year,$setmonth,$setnover2); //Montly permission >=2
    $this->green->set_messages($this->session->userdata('user_name'));
    // ------------------------------------ end long tha working---------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GreenSIM</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url('assets/bower_components/metisMenu/dist/metisMenu.min.css')?>" rel="stylesheet">
    <!-- Timeline CSS -->
    <link href="<?php echo base_url('assets/dist/css/timeline.css')?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/dist/css/sb-admin-2.css')?>" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url('assets/bower_components/morrisjs/morris.css')?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <!-- Jquery ui -->
    <link href="<?php echo base_url('assets/css/jquery/jquery-ui.css') ?>" rel="stylesheet">
	<!-- datepicker -->
	<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">
	<!-- Ck editor-->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/editor/summernote.css') ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/editor/codemirror/codemirror.min.css') ?>" />

    <link rel="stylesheet" href="<?php echo base_url('assets/css/toastr.css') ?>" />
    <link href="https://fonts.googleapis.com/css?family=Moul|Siemreap" rel="stylesheet">

	<!-- Green CSS -->
	<link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
	<!-- table css -->
	<link href="<?php echo base_url('assets/css/table.css') ?>" rel="stylesheet">

	<!-- Favicon -->
    <link rel="apple-touch-icon" href="../../apple-touch-icon.html">
    <link rel="icon" href="../../favicon.html">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- jQuery -->
	<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/jquery/jquery-ui.js')?>"></script>

	<!-- Parsley Form Validation -->
	<script src="<?php echo base_url('assets/js/parsley.min.js')?>"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
	<!-- Metis Menu Plugin JavaScript -->
	<script src="<?php echo base_url('assets/bower_components/metisMenu/dist/metisMenu.min.js')?>"></script>
	<!-- Morris Charts JavaScript
	<script src="<?php echo base_url('assets/bower_components/raphael/raphael-min.js')?>"></script>
	<script src="<?php echo base_url('assets/bower_components/morrisjs/morris.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/morris-data.js')?>"></script>	-->

    <!-- Ck editor-->
    <script src="<?php echo base_url('assets/js/editor/summernote.js')?>"></script>
	<script src="<?php echo base_url('assets/js/editor/codemirror/codemirror.min.js')?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('assets/dist/js/sb-admin-2.js')?>" ></script>
	<!-- Date pictker -->
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js')?>"></script>
    <!-- jqprint -->
    <script src="<?php echo base_url('assets/js/jquery/jquery.PrintArea.js')?>"></script>
    <script src="<?php echo base_url('assets/js/gScript.js')?>"></script>

    <script src="<?php echo base_url('assets/js/toastr.js')?>"></script>

   <style type="text/css">
        .xmodal {display: none;
            position: fixed;
            z-index: 2000;
            top: 1%;
            left: 50%;
            height: 5%;
            width: 5%;
            /* background: rgba( 255, 255, 255, .8 ) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat; */
            background: rgba(255, 255, 255, 0) url(<?= base_url('assets/images/FhHRx.gif') ?>)  50% 50% no-repeat;
            cursor: progress !important;
        }

        /* When the body has the loading class, we turn
           the scrollbar off with overflow:hidden */
        body.loading {
            overflow: hidden;
        }

        /* Anytime the body has the loading class, our
           modal element will be visible */
        body.loading .xmodal {
            display: inline;
            cursor: progress !important;
        }

        /*.a-menu { color: #208000 !important; }*/
   </style>

   <script type="text/javascript">
       $(function(){
          // decimal =======
          $('body').delegate('[decimal]', 'keydown', function(e) {
          var key = e.charCode || e.keyCode || 0;
          // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
          // home, end, period, and numpad decimal
          if (key == 110 || key == 190) {
             var text = $(this).val();
             if (text.toString().indexOf(".") != -1) {
                 return false;
             }
          }
          return (
                 key == 8 ||
                 key == 9 ||
                 key == 46 ||
                 key == 110 ||
                 key == 190 ||
                 (key >= 35 && key <= 40) ||
                 (key >= 48 && key <= 57) ||
                 (key >= 96 && key <= 105));
          });

          // number =======
          $('body').delegate('[number]', 'keydown', function(e) {
              var key = e.charCode || e.keyCode || 0;
              // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
              // home, end, period, and numpad decimal
              return (
                     key == 8 ||
                     key == 9 ||
                     key == 46 ||
                     // key == 110 ||
                     // key == 190 ||
                     (key >= 35 && key <= 40) ||
                     (key >= 48 && key <= 57) ||
                     (key >= 96 && key <= 105));
          });

          // prevent paste error =====
          // $('body').bind('cut copy paste', '#selling_price, .selling_price', function (e) {
          //    e.preventDefault();
          //    // toastr["warning"]("Only number!");
          // });
      });
   </script>
</head>
<body>
    <div class="xmodal"></div>
    <div class="proccess_loading"></div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">
                	<img src="<?php echo base_url('assets/images/logo/logo.png')?>"  />
                </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <select class="" id="year" name="year" style="display: none;">
                  <?php echo $this->green->GetSchYear($schoolid,$year);?>
                </select>
                <select class="" id="schlevelid" name="schlevelid" style="display: none;">
                   <?php
                    echo $this->green->GetSchLevel()
                   ?>
                </select>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i><i class="fa fa-caret-down"></i>
						<span class="badge"><span style="color:#f00;"> <?php echo $this->green->get_messages();?></span></span>
                    </a>
				<style type="text/css">
                        .span.badge {
                            margin-bottom:4490px !important;
                        }
                          a.text-center:hover{
                            text-decoration: none !important;
                            background:none!important;
                            cursor: inherit;
                        }
                    </style>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="<?php echo site_url('message/message');?>">

                                <div>
                                    <strong>Message</strong>

                                    <span class="pull-right text-muted">
                                        <em>New</em>

                                    </span>
                                </div>
                                <div>
                                	Just coming new messages

                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center">
                                <strong>&nbsp;</strong>

                                <!-- <i class="fa fa-angle-right"></i> -->
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown hide">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="<?php echo site_url('message/message');?>">
                                <div>
                                    <p>
                                        <strong>Message</strong>
                                        <span class="pull-right text-muted">(<span style="color:#f00;"><?php echo  $this->green->get_messages();?></span>)</span>
                                    </p>
                                    <!-- <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div> -->
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>

                     <!--  ------------------------------------ start long tha working --------------------------------------- -->
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="<?php echo site_url('student/permission/mothlyperm').'?y='.$year.'&m=9&month='.$setmonth.'&n='.$setnum_mp.'&p=0';?>">
                                <div>
                                    <i class="fa fa-user fa-fw"></i> Montly permission >=3
                                    <span class="pull-right text-muted small"><?php echo $this->green->get_montly_permission();?></span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo site_url('student/permission/yearlyPerm').'?y='.$year.'&m=0&month=0&n='.$setnum_yp.'&p=0';?>">
                                <div>
                                    <i class="fa fa-user fa-fw"></i> Yearly permission >=5
                                    <span class="pull-right text-muted small"><?php echo $this->green->get_yearly_permission();?></span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo site_url('student/permission/mothlypermover2').'?y='.$year.'&m=0&month='.$setmonth.'&n='.$setnover2.'&p=0';?>">
                                <div>
                                    <i class="fa fa-user fa-fw"></i> Montly permission >=2
                                    <span class="pull-right text-muted small"><?php echo  $this->green->get_montly_permission_bigger_one();?></span>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!--  ------------------------------------ end long tha working --------------------------------------- -->
                    <!-- /.dropdown-alerts -->
                </li>

                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php if($roleid=='1'){?>
                        <li><a href="<?php echo site_url("setting/user")?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="<?php echo site_url("setting/setting")?>"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <?php }?>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url("login/logOut") ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation" id="navigation_left">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default a-menu" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="<?php echo site_url("system/dashboard") ?>" class="a-menu"><i class="fa fa-dashboard fa-fw"></i> <b>Dashboard</b></a>
                        </li>
                        <!--
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts Report<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Student by class</a>
                                </li>
                                <li>
                                    <a href="morris.html">Families visit by month</a>
                                </li>
                            </ul>
                        </li>
                        -->
                        <?php echo $menu?>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
       <!-- content --- -->
