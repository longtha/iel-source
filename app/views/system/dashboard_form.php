<?php
$row=$this->db->query("SELECT * FROM sch_dashboard_item WHERE dashid='1'")->row();
$m=$row->moduleid;
$p=$row->link_pageid;
$showage=0;
if($age['s_minage']!="" || $age['s_maxage']!=""){
	$showage=1;
}

 ?>
 
 <style type="text/css">
    #tblfix td.padding {
        padding-left: 130px !important;
    }
    tr.erow td {
        background: #eeede9 none repeat scroll 0 0;
        border-bottom: 1px solid #f7f7f7;
    }
 </style>

<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row-1 result_info">
		      	<div class="col-xs-3">
		      		<strong id='title'>DASHBOARD </strong>
		      	</div>
		      	<div class="col-xs-9" style="text-align: right">
		      		<span class="top_action_button">

			    	</span>
			    	<span class="top_action_button">
			    		<a href="#" id="export" title="Export">
			    			<img src="<?php echo base_url('assets/images/icons/export.png')?>" />
			    		</a>
			    	</span>
		      		<span class="top_action_button">
						<a href="#" id="print" title="Print">
			    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
			    		</a>
		      		</span>
		      	</div>
		  </div>
		  <?php
		  	@$year=$this->db->where('yearid',$date['year'])->get('sch_school_year')->row()->sch_year;
		  ?>
		   
		  <div class="row-2" id='tab_print'>
	      	<div class="col-sm-6">
	      		<div class="panel panel-default">

	      			<div class="panel-heading">
		                   <span class="panel-title">Student</span>
		                   <span class='panel-title pull-right danger'><a><img onclick="view_std();" src="<?php echo base_url('assets/images/icons/detail.png')?>" /></a></span>
		            </div>

	      			<div class="panel-body">

	      				<div class="row">
			      			<div class="form_sep">
    		           			  <div class="col-xs-4">
    									<select onchange='search(event);' class='form-control input-sm' id='s_year'>
    										<?php foreach ($this->db->order_by('yearid','DESC')->get('sch_school_year')->result() as $y) {?>
    											<option value='<?PHP echo $y->yearid ?>'<?php if($y->yearid==$date['year']) echo 'selected' ?>><?PHP echo $y->sch_year ?></option>
    										<?PHP } ?>
    									</select>
    
    							  </div>
    							  <div class="col-xs-4">
    							    	<select  class='form-control input-sm' id='s_term_type'>
    							    		<option value="">--- Select Type ---</option>
    							    		<?php foreach ($this->db->order_by('feetypeid','ASC')->get('sch_school_feetype')->result() as $feetype) {?>
    											<option value='<?PHP echo $feetype->feetypeid ?>'><?PHP echo $feetype->schoolfeetype ?></option>
    										<?PHP } ?>
    									</select>
    							  </div>
    							  <div class="col-xs-4">
    							    	<select onchange='searchByTerm(event);'  class='form-control input-sm' id='s_term'>
    							    		<option value="">--- Select Term ---</option>
    									</select>
    							  </div>
    							  
    							  <div class="col-xs-4 hide">
    							    	<div class='input-group new_ex' id='datetimepicker'>
    									    <input value="<?php echo $date['s_date'];?>" type='text' class='form-control input-sm' id='s_date'/>
    									    <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>
    									</div>
    							  </div>
    							  <div class="col-xs-4 hide">
    							    	<div class='input-group new_ex' id='datetimepicker'>
    									    <input value="<?php echo $date['e_date'] ?>" type='text' class='form-control input-sm' id='e_date'/>
    									    <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>
    									</div>
    							  </div>
    							  <div class="col-xs-3 hide" >
    							   		<button type="button" onclick='search(event);' class="btn btn-default btn-sm">
    									  	<span class="glyphicon glyphicon-search"  aria-hidden="true"></span> Search
    									</button>
    							  </div>
    		           		</div>
		      			</div>
		      			<br/>
	      				<div class="row-1">
	      					<div class="col-sm-12">
				           		<div class="form_sep">
				                   <table class='table table-bordered' id="tblfix">
				                   		<tbody>
				                   			<tr>
				                   				<td><label class="control-label">Academy Year </label></td>
				                   				<td><label class="control-label"><?php echo $year?></label></td>
				                   				<?php echo ($showage==1?"<td><label class='control-label'>Student by Age</label></td>":"") ?>
				                   			</tr>
				                   			
				                   			<?php
				                   			$yearid=$this->session->userdata('year');

				                   				foreach ($data['level'] as $row) {
				                   					$num_by_age="";
				                   					$age_str="";
				                   					$age_blank="";
				                   					if($showage==1){
				                   						$age_str=$age['s_minage']."__".$age['s_maxage'];
				                   						$num_by_age=$this->dash->getNumStdByAgeLev($row->gradeid,$date['year'],$age['s_minage'],$age['s_maxage']);
				                   					}

				                   					$level='Level ';
				                   					$tr='';
				                   					$st_gen="Student";
				                   					$st_gen1="Student";

				                   					if($row->total_stu>1) $st_gen.="s";
				                   					if($num_by_age>1) $st_gen1.="s";

				                   					echo "$tr<tr>
								                   				<td><label class='control-label'> * $level ".$row->grade."</label></td>
								                   				<td>
								                   					<a href='".site_url("/student/student/search?s_id=&fn=&fnk=&class=&s_num=150&year=$yearid&m=$m&p=$p&l=&b=&le=$row->gradeid&per_page=0&ag=$age_blank&pro=&fone=1")."' target='_blank'>".$row->total_stu." $st_gen</a>
								                   				</td>";
								                   	if($showage==1){
								                   		if($num_by_age>0){
															echo "<td><a href='".site_url("/student/student/search?s_id=&fn=&fnk=&class=&s_num=150&year=$yearid&m=$m&p=$p&l=&b=&le=$row->gradeid&per_page=0&ag=$age_str&pro&fone=1")."' target='_blank'>".$num_by_age." $st_gen1</a></td>";
								                  		}else{
								                  			echo "<td>".$num_by_age."</td>";
								                  		}

								                   	}
								                   	echo "</tr>";

				                   				}
				                   			?>
				                   			<tr>
				                   				<td><label class="control-label"> * Total Number Of Student</label></td>
				                   				<td><label class="control-label"><?php echo $data['total']->total?> Students</label></td>
				                   				<?php
				                   					$totalbyage=$this->dash->getNumByAge($date['year'],$age['s_minage'],$age['s_maxage']);

				                   					$st_stat="Student";
				                   					if($totalbyage>1) $st_stat.="s";
				                   					echo ($showage==1?"<td><label class=control-labe'>".$totalbyage." $st_stat</label></td>":"");
				                   				 ?>
				                   			</tr>

				                   		</tbody>
				                   </table>
				                </div>
				            </div>
			            </div>

					</div>
					<!-- Rothna -->
					 <div class="panel-heading">
					 	 <div class="row-1">
					 	 	<div class="col-xs-5">
		                   		<span class="panel-title">Student Absent / Permission / Late</span>
		                   	</div>
		                  
    		                   <div class="col-xs-4">
                                    <label class="req" for="sclevelid">School Level</label>
                                    <select class="form-control" id="sclevelid" name="sclevelid" onchange='search_permission(event);'>                                       
                                        <?php if (isset($schevels) && count($schevels) > 0) {
                                            foreach ($schevels as $sclev) {
                                                if (($sclev->schlevelid) == $sclevelid) $select = 'selected="selected"';
                                                else $select = '';
                                                echo '<option value="' . $sclev->schlevelid . '"  pro_id="'. $sclev->programid .'"'.$select.'>' . $sclev->sch_level . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </div>
    		                   <div class='col-xs-3'>
    		                   		<label class="req">&nbsp;</label>
    								<select name="attendance" class="form-control" id="present" onchange='search_permission(event);'>
    									<option value=""></option>
    									<?php 
    									   if ($present == 1) $select_absent = 'selected="selected"';
    									   else if ($present == 2) $select_psermission = 'selected="selected"';
    									   else if ($present == 3) $select_late = 'selected="selected"';
        								    else $select1 = '';
    									?>
    									<option value="1" <?php echo @$select_absent ?>>Absent</option>
    									<option value="2" <?php echo @$select_psermission ?>>Permission</option>
    									<option value="3" <?php echo @$select_late ?>>Late</option>
    								</select>
    							</div>
    						</div>
		            </div>
	      			 <div class="panel-body">
		           		<div class="form_sep">
		           			<table border="0" class="table table-bordered">
		           				<?php
		           				   $i = 1;
		           				   foreach($data_present as $present) {
		           				       $no_imgs = base_url()."assets/upload/students/NoImage.png";
		           				       $have_img = base_url()."assets/upload/students/".$present->yearid.'/thumbs/'.$present->studentid.'.jpg';
	           				           $img = '<img src="'.$no_imgs.'" class="img-circle" alt="No Image" style="width:70px !important; height:70px !important;">';
		           				       if (file_exists(FCPATH . "assets/upload/students/".$present->yearid.'/thumbs/'.$present->studentid.'.jpg')) {
		           				           $img = '<img src="'.$have_img.'" class="img-circle" alt="No Image" style="width:70px !important; height:70px !important;">';
		           				       }
           				         ?>	           				   
   									<tr>
   										<td><?php echo str_pad($i,2,"0",STR_PAD_LEFT); ?></th>
           								<td><?php echo $img; ?></th>
									<?php
									   $sql_page="SELECT DISTINCT
                        						s.last_name_kh,
                        						s.first_name_kh,                       						
                        						s.class_name,
                                                s.student_num,
                                                s.sch_year
                        					FROM
                        						v_student_profile s
                        				WHERE s.is_active=1 and studentid = $present->studentid and classid = $present->classid and yearid = $present->yearid ";
									   $student =  $this->db->query($sql_page)->result_array();
									?>
									<?php foreach($student as $row){ ?>
           								<td><?php echo $row['last_name_kh'].' '.$row['first_name_kh']; ?></th>
           								<td><?php echo $row['student_num'] ?></th>
           								<td><?php echo $row['class_name'] ?></th>
           								<td><?php echo $row['sch_year'] ?></th>
           							<?php } ?>
		           					</tr>
		           				
								<?php 
								
                                   $i++;
		           				   } 
                                ?>
	           					<?php
	                   			 	$yearid=$this->session->userdata('year');
	                   			 	$vtc_class_id=$data['vtc_class_id'];
	                   			 	$vtc_grade_levelid=$data['vtc_grade_levelid'];
	                   				$tr='';
	                   				foreach($data['vtc_promotion']->result() as $row) {
	                   					$tr .= "<tr>
						                   			<td>".$row->proname."</td>
						                   			<td><a href='".site_url("/student/student/search?s_id=&fn=&fnk=&class=$vtc_class_id&s_num=50&year=$yearid&m=$m&p=$p&l=&b=&le=$vtc_grade_levelid&per_page=0&ag=&pro=$row->promot_id")."' target='_blank'>".$row->num_stu." Student</a></td>
					                   			</tr>";
	                   				}
                   					echo $tr;
	                   				?>
		           			</table>
		           			<?php if($count > 0){ ?>
    		           			<div style="width:300px;float:right;border:1px solid #ccc;">
    		           				<div style="float:left;border-bottom:1px solid #ccc;">
        		           				<div style="width:150px;float:left;border-right:1px solid #ccc;padding: 5px;">
        		           					<span>អវត្តមានអត់ច្បាប់</span>
        		           				</div>
        		           				<div style="width:148px;float:left;padding: 5px;">
        		           					<span><?php echo intval(@$sqlsum[0]['sum_absent']); ?></span>
        		           				</div>
        		           			</div>
        		           			<div style="float:left;border-bottom:1px solid #ccc;">
        		           				<div style="width:150px;float:left;border-right:1px solid #ccc;padding: 5px;">
        		           					<span>អវត្តមាន មានច្បាប់</span>
        		           				</div>
        		           				<div style="width:148px;float:left;padding: 5px;">
        		           					<span><?php echo intval(@$sqlsum[0]['sum_permission']); ?></span>
        		           				</div>
        		           			</div>
        		           			<div style="float:left;">
        		           				<div style="width:150px;float:left;border-right:1px solid #ccc;padding: 5px;">
        		           					<span>សិស្សមកយឺត</span>
        		           				</div>
        		           				<div style="width:148px;float:left;padding: 5px;">
        		           					<span><?php echo intval(@$sqlsum[0]['sum_late']); ?></span>
        		           				</div>
        		           			</div>
    		           			</div>
    		           		<?php } ?>
		                </div>
					</div>
					<!-- end rothna-->
					<div class="panel-heading">
		                   <span class="panel-title">Leave School</span>		                 
		            </div>
	      			<div class="panel-body">
		           		<div class="form_sep">
		                   <table class='table table-bordered'>
		                   		<tbody>
		                   			 <tr>
		                   				<td><label class="control-label"> * Total </label></td>
		                   				<?php
    									   $sql_leave="SELECT count(student_num) as leavetotal FROM sch_student
                            				WHERE leave_school=1 ";
    									   $student_leave =  $this->db->query($sql_leave)->result_array();
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstdleave")?>'>
		                   						<?php echo $student_leave[0]['leavetotal'] ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   		</tbody>
		                   </table>
		                </div>
					</div>
	      		</div>
	      	</div>
         	<div class="col-sm-6">
	      		<div class="panel panel-default">
	      			<div class="panel-heading">
		                   <span class="panel-title">New Students / សិស្សថ្មី</span>		                  
		            </div>
	      			<div class="panel-body">
		           		<div class="form_sep">
		                   <table class='table table-bordered'>
		                   		<tbody>
		                   			<!-- <tr>
		                   				<td colspan="2"><label class="control-label">Academy Year:</label> <?php echo $year ?> </td>
		                   			</tr> -->
		                   			<tr>
		                   				<td><label class="control-label"> * Total </label></td>
		                   				<?php
		                   				   $current_date = date("Y-m-d");
    									   $sql_new="SELECT
                                        				CONCAT(s.last_name,' ',s.first_name) AS fullname,
                                        						s.studentid,
                                        						s.sch_year,
                                        						s.familyid,
                                        						s.yearid,
                                        						s.student_num,
                                        						s.register_date,
                                        						s.class_name,
                                        						s.classid
                                                        FROM v_student_profile AS s
                                                        WHERE  DATEDIFF('$current_date',s.register_date) <= 75 and leave_school = 0 and s.is_active = 1
                                                        GROUP BY s.studentid,
                                        						s.sch_year,
                                        						s.familyid,
                                        						s.yearid,
                                        						s.student_num,
                                        						s.register_date,
                                        						s.class_name,
                                        						s.classid";
    									   $student_new =  $this->db->query($sql_new);
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstdnew")?>'>
		                   						<?php echo $student_new->num_rows(); ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   		</tbody>
		                   </table>
		                </div>
					</div>
					<div class="panel-heading">
		                <span class="panel-title">Yet Payment / សិស្សអត់ទាន់បង់លុយ</span>		               
		            </div>
					<div class="panel-body">
		           		<div class="form_sep">
		                   <table class='table table-bordered'>
		                   		<tbody>
		                   			<!-- <tr>
		                   				<td colspan="2"><label class="control-label">Academy Year:</label> <?php echo $year ?> </td>
		                   			</tr> -->
		                   			<tr>
		                   				<td><label class="control-label"> * Total </label></td>
		                   				<?php
    									   $sql_yet_payment="SELECT count(student_num) as payment FROM v_student_fee_inv_list
                            				WHERE amt_balance > 0";
    									   $student_yet_payment =  $this->db->query($sql_yet_payment)->result_array();
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstdyetpayment")?>'>
		                   						<?php echo $student_yet_payment[0]['payment'] ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   		</tbody>
		                   </table>
		                </div>
					</div>
					
					<div class="panel-heading">
		                <span class="panel-title">Teachers / គ្រូបង្រៀន</span>		                
		            </div>
					<div class="panel-body">
		           		<div class="form_sep">
		                   <table class='table table-bordered'>
		                   		<tbody>
		                   			
		                   			<tr>
		                   				<td><label class="control-label"> * Total </label></td>
		                   				<?php
    									   $sql_gep_teacher="SELECT
                                        				CONCAT(s.last_name,' ',s.first_name) AS fullname,
                                        						s.sex,
                                        						s.phone
                                                        FROM sch_emp_profile AS s
                                                        LEFT JOIN sch_emp_position as p ON(p.posid = s.pos_id)
                                                        WHERE  p.position = 'GEP Teacher' and s.is_active = 1 and p.is_active = 1
                                        
                                                        ";
    									   $gep_teacher =  $this->db->query($sql_gep_teacher);
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewteacher?position=gep")?>'>
		                   						<?php echo $gep_teacher->num_rows(); ?> GEP Teachers
		                   					</a>
		                   				</td>
		                   			</tr>
		                   			<tr>
		                   				<td><label class="control-label"> * Total </label></td>
		                   				<?php
    									   $sql_kgp_teacher="SELECT
                                        				CONCAT(s.last_name,' ',s.first_name) AS fullname,
                                        						s.sex,
                                        						s.phone
                                                        FROM sch_emp_profile AS s
                                                        LEFT JOIN sch_emp_position as p ON(p.posid = s.pos_id)
                                                        WHERE  p.position = 'KGP Teacher' and s.is_active = 1 and p.is_active = 1
                                        
                                                        ";
    									   $kgp_teacher =  $this->db->query($sql_kgp_teacher);
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewteacher?position=kgp")?>'>
		                   						<?php echo $kgp_teacher->num_rows(); ?> KGP Teachers
		                   					</a>
		                   				</td>
		                   			</tr>
		                   			<tr>
		                   				<td><label class="control-label"> * Total </label></td>
		                   				<?php
    									   $sql_iep_teacher="SELECT
                                        				CONCAT(s.last_name,' ',s.first_name) AS fullname,
                                        						s.sex,
                                        						s.phone
                                                        FROM sch_emp_profile AS s
                                                        LEFT JOIN sch_emp_position as p ON(p.posid = s.pos_id)
                                                        WHERE  p.position = 'IEP Teacher' and s.is_active = 1 and p.is_active = 1
                                        
                                                        ";
    									   $iep_teacher =  $this->db->query($sql_iep_teacher);
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewteacher?position=iep")?>'>
		                   						<?php echo $iep_teacher->num_rows(); ?> IEP Teachers
		                   					</a>
		                   				</td>
		                   			</tr>
		                   			<tr>
		                   				<td><label class="control-label"> * Total </label></td>
		                   				<?php
    									   $sql_chinese_teacher="SELECT
                                        				CONCAT(s.last_name,' ',s.first_name) AS fullname,
                                        						s.sex,
                                        						s.phone
                                                        FROM sch_emp_profile AS s
                                                        LEFT JOIN sch_emp_position as p ON(p.posid = s.pos_id)
                                                        WHERE  p.position = 'Chinese Teacher' and s.is_active = 1 and p.is_active = 1
                                        
                                                        ";
    									   $chinese_teacher =  $this->db->query($sql_chinese_teacher);
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewteacher?position=chinese")?>'>
		                   						<?php echo $chinese_teacher->num_rows(); ?> Chinese Teachers
		                   					</a>
		                   				</td>
		                   			</tr>
		                   		</tbody>
		                   </table>
		                </div>
					</div>
					
					<div class="panel-heading">
		                <span class="panel-title">Students Study By Skills / សិស្សសិក្សា តាម កម្មវិធី</span>           
		            </div>
					<div class="panel-body">
		           		<div class="form_sep">
		                   <table class='table table-bordered'>
		                   		<tbody>
		                   			<tr>
		                   				<td><label class="control-label"> * KGP Program / ចំណេះទូទៅភាសាខ្មែរ </label></td>
		                   				<?php
    									   $sql_kgp="SELECT s.studentid,
                                            						s.sch_year,
                                            						s.familyid,
                                            						s.yearid,
                                            						s.student_num,
                                            						s.register_date,
                                            						s.class_name,
                                                                    sc.program
                                             FROM v_student_profile as s
                                            LEFT JOIN sch_school_program as sc ON(sc.programid = s.programid)
                            				WHERE s.programid = 1 and s.is_active = 1 and s.leave_school = 0
                                             GROUP BY s.studentid,
                    						s.sch_year,
                    						s.familyid,
                    						s.yearid,
                    						s.student_num,
                    						s.register_date,
                    						s.class_name
                                            ";
    									   $student_kgp =  $this->db->query($sql_kgp);
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=1")?>'>
		                   						<?php echo $student_kgp->num_rows(); ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   			<tr>
		                   				<td><label class="control-label"> * IEP Program / ចំណេះទូទៅភាសាអង់គ្លេស</label></td>
		                   				<?php
    									   $sql_iep ="SELECT s.studentid,
                                            						s.sch_year,
                                            						s.familyid,
                                            						s.yearid,
                                            						s.student_num,
                                            						s.register_date,
                                            						s.class_name,
                                                                    sc.program
                                             FROM v_student_profile as s
                                            LEFT JOIN sch_school_program as sc ON(sc.programid = s.programid)
                            				WHERE s.programid = 2 and s.is_active = 1 and s.leave_school = 0
                                             GROUP BY s.studentid,
                    						s.sch_year,
                    						s.familyid,
                    						s.yearid,
                    						s.student_num,
                    						s.register_date,
                    						s.class_name";
    									   $student_iep =  $this->db->query($sql_iep);
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=2")?>'>
		                   						<?php echo $student_iep->num_rows(); ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   			<tr>
		                   				<td><label class="control-label"> * GEP Program / ភាសាអង់គ្លេសទូទៅ </label></td>
		                   				<?php
    									   $sql_gep ="SELECT s.studentid,
                                            						s.sch_year,
                                            						s.familyid,
                                            						s.yearid,
                                            						s.student_num,
                                            						s.register_date,
                                            						s.class_name,
                                                                    sc.program
                                             FROM v_student_profile as s
                                            LEFT JOIN sch_school_program as sc ON(sc.programid = s.programid)
                            				WHERE s.programid = 3 and s.is_active = 1 and s.leave_school = 0
                                             GROUP BY s.studentid,
                    						s.sch_year,
                    						s.familyid,
                    						s.yearid,
                    						s.student_num,
                    						s.register_date,
                    						s.class_name";
    									   $student_gep =  $this->db->query($sql_gep);
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=3")?>'>
		                   						<?php echo $student_gep->num_rows(); ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   			<tr>
		                   				<td><label class="control-label"> * Chinese Program / ចំណេះទូទៅភាសាចិន </label></td>
		                   				<?php
    									   $sql_ch ="SELECT s.studentid,
                                            						s.sch_year,
                                            						s.familyid,
                                            						s.yearid,
                                            						s.student_num,
                                            						s.register_date,
                                            						s.class_name,
                                                                    sc.program
                                             FROM v_student_profile as s
                                            LEFT JOIN sch_school_program as sc ON(sc.programid = s.programid)
                            				WHERE s.programid = 5 and s.is_active = 1 and s.leave_school = 0
                                             GROUP BY s.studentid,
                    						s.sch_year,
                    						s.familyid,
                    						s.yearid,
                    						s.student_num,
                    						s.register_date,
                    						s.class_name";
    									   $student_ch =  $this->db->query($sql_ch);
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=5")?>'>
		                   						<?php echo $student_ch->num_rows(); ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   		</tbody>
		                   </table>
		                </div>
					</div>
					
					<div class="panel-heading">
		                <span class="panel-title">Grand Total / ចំនួនសិស្សសរុប</span>		                
		            </div>
					<div class="panel-body">
		           		<div class="form_sep">
		                   <table class='table table-bordered'>
		                   		<tbody>
		                   			<!-- <tr>
		                   				<td colspan="2"><label class="control-label">Academy Year:</label> <?php echo $year ?> </td>
		                   			</tr> -->
		                   			<tr>                   				
		                   				<td><label class="control-label"> * KGP Program / ចំណេះទូទៅភាសាខ្មែរ </label></td>		                   				
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=1")?>'>
		                   						<?php echo $student_kgp->num_rows(); ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   			<tr>
		                   				<td><label class="control-label"> * IEP Program / ចំណេះទូទៅភាសាអង់គ្លេស</label></td>	                   				
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=2")?>'>
		                   						<?php echo $student_iep->num_rows(); ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   			<tr>
		                   				<td style="padding: 0 !important;">
		                   					<div style="float: left;margin-top: 20px;">
		                   						<label class="control-label"> * GEP Program / ភាសាអង់គ្លេសទូទៅ </label>
		                   					</div>
		                   					<?php
        									   $sql_pt_0 ="SELECT s.studentid,
                                                						s.sch_year,
                                                						s.familyid,
                                                						s.yearid,
                                                						s.student_num,
                                                						s.register_date,
                                                						s.class_name,
                                                                        sc.program
                                                 FROM v_student_profile as s
                                                LEFT JOIN v_sch_class as sc ON(sc.classid = s.classid)
                                				WHERE s.programid = 3 and s.is_active = 1 and s.leave_school = 0 and sc.is_active = 1 and sc.is_pt = 0
                                                 GROUP BY s.studentid,
                        						s.sch_year,
                        						s.familyid,
                        						s.yearid,
                        						s.student_num,
                        						s.register_date,
                        						s.class_name";
        									   $student_pt_0 =  $this->db->query($sql_pt_0);
        									?>
        									<?php
        									   $sql_pt_1 ="SELECT s.studentid,
                                                						s.sch_year,
                                                						s.familyid,
                                                						s.yearid,
                                                						s.student_num,
                                                						s.register_date,
                                                						s.class_name,
                                                                        sc.program
                                                 FROM v_student_profile as s
                                                LEFT JOIN v_sch_class as sc ON(sc.classid = s.classid)
                                				WHERE s.programid = 3 and s.is_active = 1 and s.leave_school = 0 and sc.is_active = 1 and sc.is_pt = 1
                                                 GROUP BY s.studentid,
                        						s.sch_year,
                        						s.familyid,
                        						s.yearid,
                        						s.student_num,
                        						s.register_date,
                        						s.class_name";
        									   $student_pt_1 =  $this->db->query($sql_pt_1);
        									?>
		                   					<div style="float: right;width:100px;text-align: center;border-left:1px solid #ddd;">
    		                   					<label style="width:100%;border-bottom: 1px solid #ddd;margin-top: 5px;padding-bottom: 5px;">Full time</label>
    		                   					<label style="">Part time</label>
		                   					</div>
		                   				</td>
		                   				<td style="padding: 0 !important;">
		                   					<div style="border-left:1px solid #ddd;">
    		                   					<label style="width:100%;border-bottom: 1px solid #ddd;margin-top: 5px;padding-bottom: 5px;font-weight: normal;padding-left:4px;">
    		                   						<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=3&is_pt=0")?>'>
    		                   							<?php echo $student_pt_0->num_rows(); ?> Students
    		                   						</a>
    		                   					</label>
    		                   					<label style="font-weight: normal;padding-left:4px;">
    		                   						<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=3&is_pt=1")?>'>
    		                   							<?php echo $student_pt_1->num_rows(); ?> Students
    		                   						</a>
    		                   					</label>
		                   					</div>
		                   				
		                   				</td>
		                   			</tr>
                   					<?php
									   $sql_ch_pt_0 ="SELECT s.studentid,
                                        						s.sch_year,
                                        						s.familyid,
                                        						s.yearid,
                                        						s.student_num,
                                        						s.register_date,
                                        						s.class_name,
                                                                sc.program
                                         FROM v_student_profile as s
                                        LEFT JOIN v_sch_class as sc ON(sc.classid = s.classid)
                        				WHERE s.programid = 5 and s.is_active = 1 and s.leave_school = 0 and sc.is_active = 1 and sc.is_pt = 0
                                         GROUP BY s.studentid,
                						s.sch_year,
                						s.familyid,
                						s.yearid,
                						s.student_num,
                						s.register_date,
                						s.class_name";
									   $student_ch_pt_0 =  $this->db->query($sql_ch_pt_0);
									?>
									<?php
									$sql_ch_pt_1 ="SELECT s.studentid,
                                        						s.sch_year,
                                        						s.familyid,
                                        						s.yearid,
                                        						s.student_num,
                                        						s.register_date,
                                        						s.class_name,
                                                                sc.program
                                         FROM v_student_profile as s
                                        LEFT JOIN v_sch_class as sc ON(sc.classid = s.classid)
                        				WHERE s.programid = 5 and s.is_active = 1 and s.leave_school = 0 and sc.is_active = 1 and sc.is_pt = 1
                                         GROUP BY s.studentid,
                						s.sch_year,
                						s.familyid,
                						s.yearid,
                						s.student_num,
                						s.register_date,
                						s.class_name";
									   $student_ch_pt_1 =  $this->db->query($sql_ch_pt_1);
									?>
		                   			<tr>
		                   				<td><label class="control-label"> * Chinese Program / ចំណេះទូទៅភាសាចិន </label></td>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=5&is_pt=1")?>'>
		                   						<?php echo $student_ch_pt_1->num_rows(); ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   			<tr>
		                   				<td><label class="control-label"> * Chinese Program Part Time/ ថ្នាក់ភាសាចិនក្រៅម៉ោង</label></td>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=5&is_pt=0")?>'>
		                   						<?php echo $student_ch_pt_0->num_rows(); ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   			<tr>
		                   				<td><label class="control-label"> * Summer KGP/ វគ្គវិស្សមកាលភាសាខ្មែរ</label></td>
		                   				<?php
    									   $sql_kgp_summer ="SELECT s.studentid,
                                            						s.sch_year,
                                            						s.familyid,
                                            						s.yearid,
                                            						s.student_num,
                                            						s.register_date,
                                            						s.class_name,
                                                                    sc.program
                                             FROM v_student_profile as s
                                            LEFT JOIN sch_school_program as sc ON(sc.programid = s.programid)
                            				WHERE s.programid = 6 and s.is_active = 1 and s.leave_school = 0
                                             GROUP BY s.studentid,
                    						s.sch_year,
                    						s.familyid,
                    						s.yearid,
                    						s.student_num,
                    						s.register_date,
                    						s.class_name";
    									   $student_kgp_summer =  $this->db->query($sql_kgp_summer);
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=6")?>'>
		                   						<?php echo $student_kgp_summer->num_rows(); ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   			<tr>
		                   				<td><label class="control-label"> * Summer IEP/ វគ្គវិស្សមកាលភាសាអង់គ្លេស</label></td>
		                   				<?php
    									   $sql_gep_summer ="SELECT s.studentid,
                                            						s.sch_year,
                                            						s.familyid,
                                            						s.yearid,
                                            						s.student_num,
                                            						s.register_date,
                                            						s.class_name,
                                                                    sc.program
                                             FROM v_student_profile as s
                                            LEFT JOIN sch_school_program as sc ON(sc.programid = s.programid)
                            				WHERE s.programid = 7 and s.is_active = 1 and s.leave_school = 0
                                             GROUP BY s.studentid,
                    						s.sch_year,
                    						s.familyid,
                    						s.yearid,
                    						s.student_num,
                    						s.register_date,
                    						s.class_name";
    									   $student_gep_summer =  $this->db->query($sql_gep_summer);
    									?>
		                   				<td>
		                   					<a target='_blank' href='<?php echo site_url("system/dashboard/viewstudentbyprogram?programid=7")?>'>
		                   						<?php echo $student_gep_summer->num_rows(); ?> Students
		                   					</a>
		                   				</td>
		                   			</tr>
		                   		</tbody>
		                   </table>
		                </div>
					</div>
 					
	      		</div>
	      	</div>
	    </div>
   </div>
</div>
<style type="text/css">
	a{
		cursor: pointer;
	}
</style>
<script type="text/javascript">
	function view_std(){
		var yearid=$('#s_year').val();
		window.open("<?PHP echo site_url('system/dashboard/view_std');?>/"+yearid,'_blank');
	}
	// function view_staff(){
	// 	var yearid=$('#s_year').val();
	// 	window.open("<?PHP echo site_url('system/dashboard/view_staff');?>/"+yearid,'_blank');
	// }
	function view_staff(){
		var yearid=$('#s_year').val();
		window.open("<?PHP echo site_url('system/dashboard/view_staff');?>",'_blank');
	}
	function view_soc(){
		var yearid=$('#s_year').val();
		var sdate=$('#sdate').val();
		var edate=$('#edate').val();
		window.open("<?PHP echo site_url('system/dashboard/view_soc');?>/"+yearid+'/'+sdate+'/'+edate,'_blank');
	}
	function view_health(){
		//var yearid=$('#s_year').val();
		var s_date=$('#s_date').val();
		var e_date=$('#e_date').val();
		window.open("<?PHP echo site_url('system/dashboard/view_health');?>/"+s_date+'/'+e_date,'_blank');
	}
	function gsPrint(emp_title,data){
		 var element = "<div id='print_area'>"+data+"</div>";
		 $("<center><p style='padding-top:15px;text-align:center;'><b>"+emp_title+"</b></p><hr>"+element+"</center>").printArea({
		  mode:"popup",  //printable window is either iframe or browser popup
		  popHt: 1000 ,  // popup window height
		  popWd: 1024,  // popup window width
		  popX: 0 ,  // popup window screen X position
		  popY: 0 , //popup window screen Y position
		  popTitle:"test", // popup window title element
		  popClose: false,  // popup window close after printing
		  strict: false
		  });
	}
	$(function(){
		// $("#s_year").val($("#s_year option:first").val());
		searchvisited();
		searchdis();
		//connamevisited();
		getSemesterFromTerm();
		$("#sdate_visited,#edate_visited").datepicker({
	      		language: 'en',
	      		pick12HourFormat: true,
	      		format:'yyyy-mm-dd'
		});
		$("#sdate,#edate").datepicker({
	      		language: 'en',
	      		pick12HourFormat: true,
	      		format:'yyyy-mm-dd'
		});
		$("#s_date,#e_date").datepicker({
	      		language: 'en',
	      		pick12HourFormat: true,
	      		format:'yyyy-mm-dd'
		});
		$("#print").on("click",function(){
				var title="<h4 align='center'>"+ $("#title").text()+"</h4>";
			   	var data = $("#tab_print").html().replace(/<img[^>]*>/gi,"");
			   	var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
			   	var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
			   	gsPrint(title,export_data);
			});
		$("#export").on("click",function(e){
				var data=$("#tab_print").html().replace(/<img[^>]*>/gi,"");
	   			var export_data = $("<center><h3 align='center'>DASHBOARD</h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
				window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
    			e.preventDefault();
		});
		//----- Rothna ---------------
		$('#view_times').click(function(){
			var sdate =$('#sdate_visited').val();
			var edate =$('#edate_visited').val();
			window.open("<?PHP echo site_url('system/dashboard/viewsfamlyvisited');?>/"+sdate+"/"+edate,'_blank');
		});
		$("#s_term_type").change(function(){
			searchByTerm();
			getSemesterFromTerm();
		});
	})
	function search(){

		var yearid=$('#s_year').val();
		var s_date=$('#s_date').val();
		var e_date=$('#e_date').val();
		var s_minage=$('#s_minage').val();
		var s_maxage=$('#s_maxage').val();
		var sclevelid = $('#sclevelid').val(); 
		var present=$('#present').val()?$('#present').val():'0';	
		location.href="<?PHP echo site_url('system/dashboard/search');?>/"+yearid+'/'+s_date+'/'+e_date+'/'+sclevelid+'/'+present;

	}

	function search_permission(){
		var sclevelid = $('#sclevelid').val();
		var present=$('#present').val()?$('#present').val():'';	
		var yearid=$('#s_year').val();
		var s_date=$('#s_date').val();
		var e_date=$('#e_date').val();
		var s_minage=$('#s_minage').val();
		var s_maxage=$('#s_maxage').val();
		location.href="<?PHP echo site_url('system/dashboard/searchpresent');?>/"+sclevelid+'/'+present+'/'+yearid;
	}
	function searchdis(){
		var sdate="";//$('#sdate').val();
		var edate="";//$('#edate').val();
		$.ajax({
				type: "POST",
				url:"<?php echo base_url(); ?>index.php/system/dashboard/searchdis/"+sdate+"/"+edate,
				success: function(data){
					$('#fdate').html(sdate);
					$('#tdate').html(edate);
		            $('#total_rice').html(data.split('.')[0]+" Kg");
		            $('#oil_total').html(data.split('.')[1]+" Litre");
				}
		});
	}


	function searchvisited(){
		var sdate_visited="";//$('#sdate_visited').val();
		var edate_visited="";//$('#edate_visited').val();
		$.ajax({
			type : 'POST',
			url  : "<?php echo base_url(); ?>index.php/system/dashboard/searchvisited/"+sdate_visited+"/"+edate_visited,
			data :{
				sdate_visited : sdate_visited,
				edate_visited : edate_visited
			},
			success:function(data){
				$("#fdate_visited").html(sdate_visited);
				$("#tdate_visited").html(edate_visited);
				$("#times_visited").html(data);
			}
		});
		$.ajax({
			type : 'POST',
			url  : "<?php echo base_url(); ?>index.php/system/dashboard/connamevisited/"+sdate_visited+"/"+edate_visited,
			success:function(data){
				$('.listbodyVisited').html(data);
			}
		});
	}
	function searchByTerm(){
		var s_year = $("#s_year option:selected").val();
		var s_term_type = $("#s_term_type option:selected").val();
		var s_term = $("#s_term option:selected").val();
		var min_age = <?php echo ($age['s_minage']>0?$age['s_minage']:0);?>;
		var max_age = <?php echo ($age['s_maxage']>0?$age['s_maxage']:0);?>;
		$.ajax({
			type : 'POST',
			url  : "<?php echo base_url('index.php/system/dashboard/serachbyterm'); ?>",
			data :{
				s_year     : s_year,
				s_term_type: s_term_type,
				s_term     : s_term,
				min_age    : min_age,
				max_age    : max_age,
				m          : "<?php echo $m;?>",
				p          : "<?php echo $p;?>"
			},
			dataType:"JSON",
			async:false,
			success:function(data){
				$("#tblfix > tbody").html(data['list']);
			}
		});
	}

	function getSemesterFromTerm(){
		var s_year = $("#s_year option:selected").val();
	 	var id_term_type = $("#s_term_type option:selected").val();
		  $.ajax({
			   type: "post",
			   url: "<?php echo base_url('index.php/system/dashboard/getTermSemesterYear'); ?>/"+s_year+"/"+id_term_type,
			   cache: false,    
			   data:{
			   	s_year        :s_year,
			   	id_term_type  :id_term_type
			   },
		   	   success: function(response){
			    var obj = JSON.parse(response); 
			    if(obj.length>0){
				     try{
				      var items=[]; 
				      items.push('<option value="">--- Select ---</option>');
				      $.each(obj, function(i,val){   
						items.push('<option value="'+val.term_sem_year_id+'">'+val.period+'</option>'); 
				      }); 
				      $('#s_term').html(items);
				     }catch(e) {  
				      $('#s_term').html('<option value="">No Data Found</option>');
				     }  
			    }else{
			     $('#s_term').html('<option value="">No Data Found</option>');
			    }  
			    
		   },
		   error: function(){      
		    $('#s_term').html('<option value="">No Data Found</option>');
		   }
		  });
	}
	//--end rothna----------------
</script>
