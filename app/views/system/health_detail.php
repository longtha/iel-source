<?php
	$m='';
	$p='';
	if(isset($_GET['m'])){
	    $m=$_GET['m'];
	}
	if(isset($_GET['p'])){
	    $p=$_GET['p'];
	}
 ?>
<?php
	$tr_search ='<tr class="remove_tag" >
					<td></td>
					<td><input type="text" name="location" id="location" class="form-control input-sm" onkeyup="search();"></td>
					<td><input type="text" name="category" id="category" class="form-control input-sm" onkeyup="search();"></td>
					<td><input type="text" name="medicine" id="medicine" class="form-control input-sm" onkeyup="search();"></td>
					<td"colspan="3"></td>
				</tr>';

	$tr = ""; $num = 1;
	if(isset($medicine))
		foreach($medicine as $row){
			$tr .= '<tr class="no_data" rel="1">
						<td class="pos_1">'.str_pad($num++,2,"0",STR_PAD_LEFT).'</td>
						<td class="pos_3">'.$row->wharehouse.'</td>
						<td class="pos_3">'.$row->description.'</td>
						<td class="pos_3">'.$row->descr_eng.'</td>
						<td class="pos_4">'.$row->qty.'</td>
						<td class="pos_4">'.$row->uom.'</td>
					</tr>';
		}
?>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	    	<div class="row result_info">
	      	<div class="col-xs-6">
	      		<strong class="contr_title"><?php echo $title = "Medicine List Detail";?></strong>
	      	</div>
	      	<div class="col-xs-6" style="text-align: right">
				<?php if($this->green->gAction("E")){ ?>
		    	<span class="top_action_button">	
		    		<a href="JavaScript:void(0);" id="export" class="export" title="Export">
		    			<img src="<?php echo base_url('assets/images/icons/export.png')?>" />
		    		</a>
		    	</span>
		    	<?php } ?>
		    	<?php if($this->green->gAction("P")){ ?>
	      		<span class="top_action_button">
					<a href="JavaScript:void(0);" id="print" class="print" title="Print">
		    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
		    		</a>
	      		</span>
	      		<?php } ?>
	      	</div> 
	      </div>
			</div>
	      <!--End -->
	      <!-- <div class="row"> -->
	      <div class="panel panel-default">
	      			<div class="panel-heading">
		                   <h4 class="panel-title">Search</h4>
		            </div>
	      			<div class="panel-body">
		           		<div class="form_sep">
							  <div class="col-xs-3">
							    	<div class='input-group new_ex' id='datetimepicker'>
									    <input type='text' class='form-control input-sm' id='s_date'/>
									    <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>
									</div>	
							  </div>
							  <div class="col-xs-3">
							    	<div class='input-group new_ex' id='datetimepicker'>
									    <input type='text' class='form-control input-sm' id='e_date'/>
									    <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>
									</div>	
							  </div>
							  <div class="col-xs-2">
							   		<button type="button" onclick='search(event);' class="btn btn-default btn-sm" id="search">
									  	<span class="glyphicon glyphicon-search"  aria-hidden="true"></span> Search
									</button>	
							  </div>
		           		</div>
		           	</div>
	      		<div class="col-xs-12">
	      				<div class="panel-body">
		           	    </div>
	      					<div class="table-responsive" id="div_export_print">
				      			<table class="table" id="setBorderTbl"  cellspacing="0" cellpadding="0">
				      				<thead>
				      					<tr>
				      						<th class="pos_1">N&deg;</th>
				      						<th class="pos_2 sort" rel="position">Wharehouse</th>
				      						<th class="pos_3 sort" rel="position_kh">Category</th>
				      						<th class="pos_4 sort" rel="description">Medicine</th>
				      						<th class="pos_4 sort" rel="description">Qty</th>
				      						<th class="pos_4 sort" rel="description">UOM</th>
				      					</tr>
				      					<input type='hidden' value='ASC' name='sort' id='sort' style='width:80px;'/>
		       							<input type='hidden' value='' name='sortquery' id='sortquery' style='width:200px;'/>
				      					<?php echo $tr_search;?>
				      				</thead>
				      				<tbody class="listbody">
				      				<?php echo $tr;?>
				      				<tr class="remove_tag">
										<td colspan='12' id='pgt'>
											<div style='margin-top:20px; width:11%; float:left;'>
											Display : <select id="sort_num" style='padding:5px; margin-right:0px;'>
															<?php
															$num=50;
															for($i=0;$i<10;$i++){?>
																<option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
																<?php $num+=50;
															}
															?>
														</select>
											</div>
											<div style='text-align:center; verticle-align:center; width:89%; float:right;'>
												<ul class='pagination' style='text-align:center'>
													<?php echo $this->pagination->create_links(); ?>
												</ul>
											</div>

										</td>
									</tr> 
					      			</tbody>
				      			</table>
				      		</div>
	      				</div>
	      			</div>
	      		</div>
	      <!-- </div> -->
	    </div>
	</div>
</div>
<script type="text/javascript">
	$("#s_date,#e_date").datepicker({
		language: 'en',
		pick12HourFormat: true,
		format:'dd-mm-yyyy'
	});
		
	//----------Export to excel--------
	$("#export").on("click", function(){
		var no_data = $(".no_data").attr('rel');
		var data=$('.table').attr('border',1);
			data = $("#div_export_print").html().replace(/<img[^>]*>/gi,"");
		var export_data=$("<center><h3 align='center'>Medicine Used Detail List</h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html(); 
		
		if( no_data == 0){
			 $(".message-body").text("We didn't find anything to Export.");
			 $("#myModal_export_print").modal('show');
		}else{
			this.href = "data:application/vnd.ms-excel,"+encodeURIComponent(export_data);
			this.download = "Medicine-Used-Detail-list.xls";
			$('.table').attr('border',0);
		}
	});
	//----------print data--------
	$("#print").on("click", function(){
		var no_data = $("#no_data").attr('rel');
		var data = $("#div_export_print").html();
		if( no_data == 0){
			$(".message-body").text("We didn't find anything to Print.");
			$("#myModal_export_print").modal('show');
		}else{
			gsPrint('Medicine Detail List', data, 'remove_tag');
		}
	});
	$('#year').on('change', function() {
  		search();
	});
	$("#search").on("click",function(){
		search();
	});
	$("#sort_num").on("change", function(){
		search();
	});
	function search(sort){
		var yearid		= $("#year").val();
		var sort_num 	= $("#sort_num").val();
		var location 	= $("#location").val();
		var category 	= $("#category").val();
		var medicine    = $("#medicine").val();
		var s_date		= $("#s_date").val();
		var e_date		= $("#e_date").val();
		$.ajax({
			type: "POST",
			url:"<?php echo base_url(); ?>index.php/system/dashboard/search_health_detail",    
			data: {
				'yearid':yearid,
				'sort_num':sort_num,
				'location':location,
				'category':category,
				'medicine':medicine,
				's_date':s_date,
				'e_date':e_date
			},
			success: function(data){
				$('.listbody').html(data);
			}
		});
	}
</script>