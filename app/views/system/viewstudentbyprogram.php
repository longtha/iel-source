
<style type="text/css">
	#preview td{
		padding: 3px ;
	}
	td img{border:1px solid #CCCCCC; padding: 2px;}
	#preview_wr{
		margin: 10px auto !important;
	}
	#tblmention td,#tblmention th{padding: 3px 5px !important;}
	span.set,.tab_head{font-family:"Times New Roman", Times, serif !important; font-size: 14px !important;}
	/*td,th{font-family:"Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important; font-size: 14px;}*/

</style>
<?php
		$school=$this->db->where('schoolid',$this->session->userdata('schoolid'))->get('sch_school_infor')->row();
   			$school_name=$school->name;
   			$school_adr=$school->address;
   		// $Student=$this->db->query("SELECT DISTINCT fullname,class_name,classid,familyid,dob,student_num FROM v_student_profile")->row();

   		
  ?>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info col-xs-10">
		      	<div class="col-xs-6">
		      		<strong>Student By Program</strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
		      		<span class="top_action_button">	
			    		<a href="#" id="export" title="Export">
			    			<img src="<?php echo base_url('assets/images/icons/export.png')?>" />
			    		</a>
			    	</span>
		      		<span class="top_action_button">
						<a href="#"  title="Print">
			    			<img id="print" src="<?php echo base_url('assets/images/icons/print.png')?>" />
			    		</a>
		      		</span>		
		      	</div> 			      
		  </div>
		</div>
	</div>
</div>

<div class="row" id='export_tap'>
	<div class="col-sm-10" id='preview_wr'>
	    <div class="panel panel-default">
	      	<div class="panel-body">
		         <div class="table-responsive" id="tab_print">
		         		<style type="text/css">
							#preview td{
								padding: 3px ;
							}
							.small tbody tr td img{width: 15px; margin-right: 10px}
							table.small th{padding:2px !important;}
							td img{border:1px solid #CCCCCC; padding: 2px;}
							#preview_wr{
								margin: 10px auto !important;
							}
							#tblmention td,#tblmention th{padding: 3px 5px !important; text-align: center;}
							.tab_head th{font-family:"Times New Roman", Times, serif !important; font-size: 14px; font-weight: bold !important;}
							.tab_head th,label.control-label{font-family:"Times New Roman", Times, serif !important; font-size: 14px; font-weight: bold !important;}
							/*td,th,label.control-label{font-family:"Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important; font-size: 14px;}*/
						</style>
						
						<div style='width:300px; float:right;'>
							<img src="<?php echo base_url('assets/images/logo/logo.png')?>" style='width:250px;  height:60px;' />
							
						</div>
						<div style='float:left; '>
							<div style=' font-weight:bold; text-transform: uppercase;'><?php echo $school_name ?></div>
							<div style='text-align:center !important; font-weight:bold;'></div>
						</div>
						<p style="clear:both"></p>
		                <table align='center' style='width:100%'>
							<thead>
								<?php 
								    if($programid == 1) $pro = "KGP";
								    elseif($programid == 2) $pro = "IEP";
								    elseif($programid == 3) $pro = "GEP";
								    elseif($programid == 5) $pro = "Chinese";
								    elseif($programid == 6) $pro = "Summer KGP";
								    elseif($programid == 7) $pro = "Summer IEP";
								?>
								<th valign='top' align="center" style='width:80%'><h4 align="center">Student Study By <?php echo $pro ; ?> Program Report</h4></th> 
							</thead>
						</table>
						<table>
							<!-- <tr>
								<th>Promotion &nbsp</th>
								<td>: <?php echo $data->proname ?></td>
							</tr>
							<tr>
								<th>Partner </th>
								<td>: <?php echo $data->company_name ?></td>
							</tr>
							<tr>
								<th>From </th>
								<td>: <?php echo $this->green->convertSQLDate($data->from_date).' To '. $this->green->convertSQLDate($data->to_date) ?></td>
							</tr> -->
						</table><br/>
						<div class='table-responsive'>
		                  	<table class='table table-bordered table-striped' id='tblstdmention'>
		                		<thead>
		                			
			                		<tr>
			                			<th style='text-align:center;'>No</th>
			                			<th style='text-align:center;' >Student Name</th>
			                			<th style='text-align:center;' >Year</th>			                			
			                			<th style='text-align:center;'>Program</th>
			                			<th style='text-align:center;' >Class Name</th>
			                		</tr>
			                		
		                		</thead>
		                		<?php
		                		    $tr="";	
		                			$i=1;
		                			foreach ($data as $std) {
		                			    $tr.= "<tr>
		                					<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
		                					<td>".$std['fullname']."</td>
                                            <td>".$std['sch_year']."</td>
		                					<td>".$std['program']."</td>
		                					<td>".$std['class_name']."</td>
		                				</tr>";
		                				$i++;
		                			}
		                				
		                			
	                			?>
		                		<tbody class='listbody'>
		                			<?php echo $tr; ?>	
		                			<tr class='remove_tag'>
											<td colspan='12' id='pgt'>
												<div style='margin-top:20px; width:20%; float:left;'>
													<div style="float: left;margin-right: 10px;">
														Display : <select id="sort_num"  style='padding:5px; margin-right:0px;'>
																<?php
																$num=10;
																for($i=0;$i<10;$i++){?>
																	<option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
																	<?php $num+=10;
																}
																?>
																<option value='all' <?php if(isset($_GET['s_num'])){ if($_GET['s_num']=='all') echo 'selected'; }?>>All</option>
															</select>
														</div>
														<input type="button" style="float: left;" value="show" class="btn btn-primary" id="show" name="Show">
												
												</div>
												<div style='text-align:center; verticle-align:center; width:80%; float:right;'>
													<ul class='pagination' style='text-align:center'>
														
														<?php echo $this->pagination->create_links(); ?>
													</ul>
												</div>
											</td>
										</tr> 	                			
		                		</tbody>
		                	</table>
		                </div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function gsPrint(data){
			 var element = "<div>"+data+"</div>";
			 $("<center>"+element+"</center>").printArea({
			  mode:"popup",  //printable window is either iframe or browser popup              
			  popHt: 600 ,  // popup window height
			  popWd: 500,  // popup window width
			  popX: 0 ,  // popup window screen X position
			  popY: 0 , //popup window screen Y position
			  popTitle:"test", // popup window title element
			  popClose: false,  // popup window close after printing
			  strict: false 
			  });
		}
		// $(function(){		
		// 	$("#print").on("click",function(){
		// 		gPrint("tab_print");
		// 	});
		// })
		$("#print").on("click",function(){
					var htmlToPrint = '' +
					        '<style type="text/css">' +
					        'table.table-bordered th, table.table-bordered td {' +
					        'border:1px solid #000 !important;' +
					        'padding;0.5em;' +
					        '}' +
					        'table.small th,table.small td{border:0px solid #000 !important; border-bottom:1px solid #CCCCCC !important;}'+
					        '</style>';
				   	var data = $("#tab_print").html();
				   	var export_data = $("<center>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
				   		export_data+=htmlToPrint;
				   	gsPrint(export_data);
		});
		$("#export").on("click",function(e){
				var data=$('.table-bordered').attr('border',1);
					data = $("#export_tap").html().replace(/<img[^>]*>/gi,"");
	   			var export_data = $("<center><h3 align='center'></h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
				window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
    			e.preventDefault();
    			$('.table-bordered').attr('border',0);
		});

		$('#show').click(function(){			
			dispaybylimit();
		});

		function dispaybylimit(event){
			var sort_num 	 = $('#sort_num').val();
			var sortstd 	 = $('#sortquery').val();
			
			if(sortstd=='')
				sortstd='order by studentid desc';
			$.ajax({
						url:"<?php echo base_url(); ?>index.php/system/dashboard/dispaybylimit",    
						data: {
							'sort_num'		: sort_num,
							'sort'			: sortstd,
							'programid'		: <?php echo isset($_GET['programid'])?$_GET['programid']:1; ?>,
							'is_pt'		: <?php echo isset($_GET['is_pt'])?$_GET['is_pt']:''; ?>
						},
						type: "POST",
						dataType:'json',
						async:false,
						success: function(data){
	                       jQuery('.listbody').html(data.data);
					}
				});
			}
</script>