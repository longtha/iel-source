<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	     <div class="row result_info">
		      	<div class="col-xs-3">
		      		<strong id='title'>List of Family Visited </strong>
		      	</div>
		      	<div class="col-xs-9" style="text-align: right">
			    	<span class="top_action_button">	
			    		<a href="#" id="export" title="Export">
			    			<img src="<?php echo base_url('assets/images/icons/export.png')?>" />
			    		</a>
			    	</span>
		      		<span class="top_action_button">
						<a href="#" id="print" title="Print">
			    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
			    		</a>
		      		</span>		
		      	</div> 			      
		  	</div>
		</div>
	</div>
</div>
	<div class="row" id='export_tap'>
		<div class="col-sm-12" id='preview_wr'>
    		<div class="form_sep">
	         	<div id="tab_print">
		         	<div  class="panel-body">
			         	<div class="form_sep">
							<label style="padding-left:50px;" class="req" for="reg_input_name">List total number visited family</label>
							<table  class='table table-bordered '>
								<thead>
									<tr>
										<td style='text-align:center;'>No</td>
										<td style='text-align:center;'>Date of Visited</td>
										<td style='text-align:center;'>Family ID</td>
										<td style='text-align:center;'>Family Name</td>
										<td style='text-align:center;'>By Social Assistant</td>
									</tr>
								</thead>
								<tbody class="visitedfamily">
									<?php
			                			$i=1;
			                			foreach ($data as $row) {
			                				echo "<tr>
				                					<td style='text-align:center;'>".$i."</td>
				                					<td>".$row->date."</td>
				                					<td>".$row->family_code."</td>
				                					<td>".$row->family_name."</td>
				                					<td>".$row->Fullname."</td>
			                					 </tr>";
			                				$i++;
			                			}
			                		?>
								</tbody>
							</table>
						</div>
		            </div>
	            </div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function gsPrint(data){
			var element = "<div>"+data+"</div>";
			$("<center>"+element+"</center>").printArea({
			mode:"popup",  //printable window is either iframe or browser popup              
			popHt: 600 ,  // popup window height
			popWd: 500,  // popup window width
			popX: 0 ,  // popup window screen X position
			popY: 0 , //popup window screen Y position
			popTitle:"test", // popup window title element
			popClose: false,  // popup window close after printing
			strict: false 
			});
		}
		$("#print").on("click",function(){
			var htmlToPrint = '' +
			        '<style type="text/css">' +
			        'table.table-bordered th, table.table-bordered td {' +
			        'border:1px solid #000 !important;' +
			        'padding;0.5em;' +
			        '}' +
			        'table.small th,table.small td{border:0px solid #000 !important; border-bottom:1px solid #CCCCCC !important;}'+
			        '</style>';
		   	var data = $("#tab_print").html();
		   	var export_data = $("<center>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
		   		export_data+=htmlToPrint;
		   	gsPrint(export_data);
		});
		$("#export").on("click",function(e){
			var data=$('.table-bordered').attr('border',1);
				data = $("#export_tap").html().replace(/<img[^>]*>/gi,"");
   			var export_data = $("<center><h3 align='center'></h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
			window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
			e.preventDefault();
			$('.table-bordered').attr('border',0);
		});
	</script>

