<?php
	$m='';
	$p='';
	if(isset($_GET['m'])){
	    $m=$_GET['m'];
	}
	if(isset($_GET['p'])){
	    $p=$_GET['p'];
	}
 ?>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
		      	<div class="col-xs-6">
		      		<strong id='title'>Health Report Summary</strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
		      		
		      		<span class="top_action_button">
						<a href="#" id="print" title="Print">
			    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
			    		</a>
		      		</span>		    	
			    	<span class="top_action_button">
			    		<a href="<?php echo site_url('social/family/add')?>" >
			    			<img src="<?php echo base_url('assets/images/icons/add.png')?>" />
			    		</a>
			    	</span>	      		
		      	</div> 			      
		  </div>
		  <?php $school=$this->db->where('schoolid',$this->session->userdata('schoolid'))->get('sch_school_infor')->row();
   			$school_name=$school->name;
   			$school_adr=$school->address;?>
		  <!-- <div class="row" id='export_tab'> -->
	      	<div class="col-sm-6" id='tab_print'>
	      		<div class="panel panel-default">
	      			<div class="panel-body" >
		           		<div class="form_sep">
		           				<div style='float:left; '>
									<div style=' font-weight:bold; text-transform: uppercase;'><?php echo $school_name ?></div>
									<div style='text-align:center !important; font-weight:bold;'></div>
								</div>
								<p style="clear:both"></p>
				                <table align='center'>
									<thead>
										<th valign='top' align="center" style='width:80%'><h5 align="center">Health Report</h5></th> 
									</thead>
								</table>
			                  <label class="req" for="reg_input_name">Health : </label> Date :<?php echo date('d-m-Y',strtotime($date['s_date'])).' <b>To :</b> '.date('d-m-Y',strtotime($date['e_date'])) ?><br/>
			                  <label class='req'>Primary School : </label><br/>
			                  &emsp; &emsp; &emsp; Total Of medical visit: <?php echo $health['total'] ?>
			                 <div class="table-responsive" >
				                  <table class='table table-bordered'>
				                  		<thead>
				                  			<th></th>
				                  			<th># of visit</th>
				                  			<?php foreach ($this->dash->getdoctor() as $doc) {
				                  				echo "<th>$doc->first_name</th>";
				                  			} ?>
				                  		</thead>
					                  		<tr>
					                  			<td>Student</td>
					                  			<td><?php echo $health['stu'] ?></td>
					                  			<?php foreach ($this->dash->getdoctor() as $doc) {
					                  				$count=$this->db->query("SELECT count('treatmentid') as total 
					                  										FROM sch_medi_treatment 
					                  										WHERE patient_type='student' 
					                  										AND doctorid='".$doc->empid."'")->row()->total;
					                  				echo "<td>$count</td>";
					                  			} ?>
					                  		</tr>
					                  		<tr>
					                  			<td>Employee</td>
					                  			<td><?php echo $health['emp'] ?></td>
					                  			<?php foreach ($this->dash->getdoctor() as $doc) {
					                  				$counts=$this->db->query("SELECT count('treatmentid') as total 
					                  										FROM sch_medi_treatment 
					                  										WHERE patient_type='emp' 
					                  										AND doctorid='".$doc->empid."'")->row()->total;
					                  				echo "<td>$counts</td>";
					                  			} ?>
					                  		</tr>
				                  			
				                  		</tbody>
				                  </table>
				                </div>
		                </div>
		                <div class="form_sep">
		                	 &emsp; &emsp; &emsp; <label class='req'>Sickness Statistic : </label>
		                	<ul class='list-unstyled' style='margin-left:80px;'>
		                		<?php foreach ($this->dash->getsicknes($date['s_date'],$date['e_date']) as $sick) {
		                			echo "<li>$sick->disease : $sick->discount </li>";
		                		} ?>
		                	</ul>
		                </div>
		                <div class="form_sep">
		                	<?php $total=$this->dash->getrefer($date['s_date'],$date['e_date']);

		                	$patient=$this->dash->getrefer($date['s_date'],$date['e_date']); ?>
		                	 &emsp; &emsp; &emsp; <label class='req'>Refer : <?php echo $patient['patient'];  ?> patient, <?php echo $total['total']   ?> times of refer</label>
		                	<ul class='list-unstyled' style='margin-left:80px;'>

		                		<?php 
		                			$ex=$this->dash->getrefer($date['s_date'],$date['e_date']);
		                			foreach ($ex['ex'] as $refer) {
		                			echo "<li>$refer->external_hospital : $refer->count </li>";
		                		} ?>
		                	</ul>
		                </div> 
					</div>
					
	      		</div>
            	<div class="panel panel-default">
            		<div class="panel-heading">
		               <strong>Medicine Used</strong>
		               <span class='panel-title pull-right danger remove_tag'><a><img onclick="view_health_detail();" src="<?php echo base_url('assets/images/icons/detail.png')?>" /></a></span>
		            </div>
	          		<div class="panel-body">
	            		<div class="form_sep">
	            			 <?php 
	            			 	$count_wh = $this->db->query("SELECT count(*) count FROM sch_stock_wharehouse")->row()->count;
	            			 ?>
	            			<table border="0"​ id='listmedicine' class="table">
								    <?php
								    	$ival = 1;
									    foreach($medicine as $row){
									    	
									        for($i=$ival;$i<=$count_wh;$i++){

										        if($row->whcode==$i){
										        	$whname = $this->db->query("SELECT wharehouse FROM sch_stock_wharehouse WHERE whcode=$row->whcode")->row();
										        	?>
													    <thead>
														    <tr>
														        <th colspan="2">Wharehouse: <?php echo $whname->wharehouse; ?></th>
														    </tr>
														    <tr>
														        <th>&nbsp;&nbsp;&nbsp;Medicine</th>
														        <th>Qty</th>
														        <th>UOM</th>
														    </tr>
													    </thead>
										        	<?php
										        	$ival = $row->whcode+1;
										        }
									    	}

									    	echo "
										        <tr>
										          <td>&nbsp;&nbsp;&nbsp;$row->descr_eng</td>
										          <td>".$row->qty."</td>
										          <td>$row->uom</td>
										        </tr>";

									    }
								    ?>
							</table>
					        
					    </div>
		            </div>
		        </div>
	      	</div>	      	
	      <!-- </div> -->
	    </div>
   </div>
</div>
<style type="text/css">
	a{
		cursor: pointer;
	}
</style>
<script type="text/javascript">
		$("#print").on("click", function(){
			var no_data = $("#no_data").attr('rel');
			var data = $("#tab_print").html();
			if( no_data == 0){
				$(".message-body").text("We didn't find anything to Print.");
				$("#myModal_export_print").modal('show');
			}else{
				gsPrint('Medicine Summary List', data, 'remove_tag');
			}
		});
		$("#export").on("click",function(e){
				var data=$('.table').attr('border',1);
					data = $("#export_tap").html().replace(/<img[^>]*>/gi,"");
	   			var export_data = $("<center><h3 align='center'></h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
				window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
    			e.preventDefault();
    			$('.table').attr('border',0);
		});
		function view_health_detail(){
			//var yearid=$('#s_year').val();
			var s_date=$('#s_date').val();
			var e_date=$('#e_date').val();
			window.open("<?PHP echo site_url('system/dashboard/view_health_detail');?>/",'_blank');
	}
	</script>