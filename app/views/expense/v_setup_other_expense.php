<div class="container-fluid">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6" style="background: #4cae4c;color: white;">
               <span class="glyphicon glyphicon-stats"></span>
               <strong>Setup others expenses list</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
               <a href="javascript:;" class="btn btn-sm btn-success" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add new... item"><span class="glyphicon glyphicon-minus"></span></a>

               <a href="javascript:;" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <!-- <img src="<?= base_url('assets/images/icons/refresh.png') ?>"> -->
                  <span class="glyphicon glyphicon-refresh"></span>
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse in" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" id="f_save">

         <input type="hidden" name="otherfeeid" id="otherfeeid">

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="otherfee_cdoe">Other fee code<span style="color:red">*</span></label>&nbsp;
               <input type="text" name="otherfee_code" id="otherfee_code" class="form-control" placeholder="Other fee code" data-parsley-required="true" data-parsley-required-message="This field require">
            </div>            
         </div>

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="otherfee">Other fee<span style="color:red"></span></label>
               <input type="text" name="otherfee" id="otherfee" class="form-control" placeholder="Other fee" data-parsley-required="true" data-parsley-required-message="This field require"> 
            </div>     
         </div>
      
         <div class="col-sm-6">
            <div class="form-group">           
               <label for="note">Note<span style="color:red"></span></label>
               <input type="text" name="note" id="note" class="form-control" placeholder="Note" style="_resize: none;">
            </div>      
         </div>

         <div class="col-sm-7 col-sm-offset-5">
            <div class="form-group"> 
               <?php if($this->green->gAction("C")){ ?>          
                  <button type="button" class="btn btn-success btn-sm save" name="save" id="save" data-save="1">Save</button>
               <?php }?>
               <?php if($this->green->gAction("C")){ ?>  
                  <button type="button" class="btn btn-success btn-sm save" name="save_next" id="save_next" data-save="2">Save next</button>
               <?php }?>
               <button type="button" class="btn btn-default btn-sm" name="clear" id="clear">Clear</button>
            </div>
         </div>

         <div class="row">
            <div class="col-sm-12">
               <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
            </div>             
         </div>
      </form>      
   </div>
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <table border="0"​ align="center" cellspacing="0" cellpadding="0" id="other_expense_list" class="table table-hover">
                <thead>
                     <tr style="background: #4cae4c;color: white;">
                        <th style="width: 5%;text-align: left;">No</th>
                        <th style="width: 15%;text-align: left;">Other fee code</th>
                        <th style="width: 30%;text-align: left;">Other fee</th>
                        <th style="width: 40%;text-align: left;">Note</th>                        
                        <th style="width: 5%;text-align: center;" colspan="2">Action</th>
                     </tr>
                     <tr>
                        <td>&nbsp;</td>
                        <td><input type="text" class="form-control input-sm" name="search_otherfee_code" id="search_otherfee_code" placeholder="Other fee code" title="Search other fee code"></td>
                        <td><input type="text" class="form-control input-sm" name="search_otherfee" id="search_otherfee" placeholder="Other fee" title="Search other fee"></td>
                        <td>&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                     </tr>
                </thead>
                
               <tbody>

               </tbody>
               <tfoot>
                  <tr>
                     <td colspan="1">
                        <div>&nbsp;</div>
                        <select name="to_display" id="to_display" title="Display items..." style="height: 22px;">
                           <!-- <option value="5">5</option> -->
                           <option value="10">10</option>
                           <option value="30">30</option>
                           <option value="50">50</option>
                           <option value="100">100</option>
                           <option value="200">200</option>
                           <option value="500">500</option>
                           <option value="1000">1000</option>                       
                        </select>
                     </td>
                     <td colspan="2">
                        <div>&nbsp;</div>  
                        <span id="show_display" style="margin-left: 10px;">&nbsp;</span>
                        <div class="btn-group pagination" role="group" aria-label="..." style="display: inline;"></div>
                     </td>                  
                     <td colspan="5">&nbsp;</td>
                  </tr>
               </tfoot>
            </table>         
         </div>
      </div>

   </div>

</div>

<!-- style -->
<style type="text/css">
   #other_expense_list th{vertical-align: middle;}
   #other_expense_list td{vertical-align: middle;}
</style>

<script type="text/javascript">
   $(function(){

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // clear =======      
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#otherfee_code').select();
         $('#save').text('Save');
         $('#save_next').show();
      });

      // save =======      
      $('body').delegate('.save', 'click', function(){
         var save = $(this).data('save');
         if($('#f_save').parsley().validate()){            
            $.ajax({
               url: '<?= site_url('expense/c_setup_other_expense/save') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){
                  $('.xmodal').show();
               },
               complete: function(){
                  $('.xmodal').hide();
               },
               data: {
                  otherfeeid: $('#otherfeeid').val(),
                  otherfee_code: $('#otherfee_code').val(),
                  otherfee: $('#otherfee').val(),
                  note: $('#note').val()                                    
               },
               success: function(data){
                  if(data.saved){
                     if(save == 1){
                        clear();
                        $('#otherfee_code').select();
                     }
                     else{
                        $('#otherfee_code').select();
                     }             
                     grid(1, $('#to_display').val() - 0);
                     toastr["success"](data.saved);                     
                  }
                  else if(data.updated){
                     grid(1, $('#to_display').val() - 0);
                     clear(); 
                     $('#otherfee_code').select();                 
                     toastr["success"](data.updated);
                     $('#save').text('Save');
                     $('#save_next').show();
                  }
                  else if(data.existed){
                     toastr["warning"](data.existed);
                  }                  
               },
               error: function() {

               }
            });
         }
      });

      // edit ========
      $('body').delegate('.edit', 'click', function(){
         var otherfeeid = $(this).data('otherfeeid');

         $.ajax({
            url: '<?= site_url('expense/c_setup_other_expense/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
               $('.xmodal').show();
            },
            complete: function(){
               $('.xmodal').hide();
            },
            data: {
               otherfeeid: otherfeeid
            },
            success: function(data){
               $('#otherfeeid').val(data.otherfeeid);
               $('#otherfee_code').val(data.otherfee_code);
               $('#otherfee').val(data.otherfee);              
               $('#note').val(data.note);               

               $('#f_save').parsley().destroy();
               $('#save').text('Update');
               $('#save_next').hide();
               $('#collapseExample').addClass('in');

               $('#a_addnew').find('span').removeClass('glyphicon-plus');
               $('#a_addnew').find('span').addClass('glyphicon-minus');
            },
            error: function() {

            }
         });         
         
      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var otherfeeid = $(this).data('otherfeeid');
         var otherfeeid_chk = $(this).data('otherfeeid_chk') - 0;
        
         if(otherfeeid_chk > 0){
            toastr["warning"]("Can't delete! data in process...");
         }else{
            if(window.confirm('Are you sure to delete?')){
               $.ajax({
                  url: '<?= site_url('expense/c_setup_other_expense/delete') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     otherfeeid: otherfeeid
                  },
                  success: function(data){
                     if(data == 1){
                        toastr["warning"]("Deleted!");
                        clear();
                        grid(1, $('#to_display').val() - 0);
                        $('#save').text('Save');
                        $('#save_next').show();
                     }else{
                        toastr["warning"]("Can't delete!");
                     }               
                     
                  },
                  error: function() {

                  }
               });
            }
         }       
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#f_save').parsley().destroy();

         if($(this).find('span').hasClass('glyphicon-minus')){
            $(this).find('span').removeClass('glyphicon-minus');
            $(this).find('span').addClass('glyphicon-plus');            
         }
         else{
            $(this).find('span').removeClass('glyphicon-plus');
            $(this).find('span').addClass('glyphicon-minus');
         }
      });

      // init. =======
      grid(1, $('#to_display').val() - 0);
      // search ========
      $('body').delegate('#search_otherfee_code', 'keyup', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#search_otherfee', 'keyup', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#to_display', 'change', function(){
         grid(1, $('#to_display').val() - 0);
      });      
      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#to_display').val() - 0);
      });
      

   }); // ready =======

   // clear =========
   function clear(){
      $('#otherfeeid').val('');
      $('#otherfee_code').val('');
      $('#otherfee').val('');         
      $('#note').val('');      
   }

   // grid =========
   function grid(current_page, total_display){
      var offset = (current_page - 1)*total_display - 0;  
      var limit = total_display - 0
      $.ajax({
         url: '<?= site_url('expense/c_setup_other_expense/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
            $('.xmodal').show(); 
         },
         complete: function(){
            $('.xmodal').hide();
         },
         data: {
            offset: offset,
            limit: limit,
            search_otherfee_code: $('#search_otherfee_code').val(),
            search_otherfee: $('#search_otherfee').val()
         },
         success: function(data) {
            $('#other_expense_list tbody').html(data.tr);

            var page = '';
            var total = '';
            var show_display = '';
            if(data.totalRecord - 0 > 0){
               // previous ====
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next<i class="glyphicon glyphicon-chevron-right"></i></button>';

               show_display += ((offset + limit > data.totalRecord ? data.totalRecord : offset + limit) + ' / ' + data.totalRecord);
            }

            $('#show_display').html(show_display);
            $('.pagination').html(page);

         },
         error: function() {

         }
      });
   }
</script>