<div class="container-fluid">     
 	<div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6" style="background: #4cae4c;color: white;">
               <span class="glyphicon glyphicon-stats"></span>
               <strong>Others expenses</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
               <a href="javascript:;" class="btn btn-sm btn-success" id="a_addnew" data-toggle="tooltip" data-placement="top" title="New others expenses"><span class="glyphicon glyphicon-plus"></span></a>
               <?php if($this->green->gAction("R")){ ?> 
               <a href="javascript:;" class="btn btn-sm btn-success" id="a_search" data-toggle="tooltip" data-placement="top" title="Search"><span class="glyphicon glyphicon-search"></span></a>
                <?php }?>
               <a href="javascript:;" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh"><span class="glyphicon glyphicon-refresh"></span></a>
               <?php if($this->green->gAction("E")){ ?> 
               <a href="javascript:;" class="btn btn-success" id="Export" data-toggle="tooltip" data-placement="top" title="Export"><span class="glyphicon glyphicon-export"></span></a>
               <?php }?>
               <?php if($this->green->gAction("P")){ ?> 
               <a href="javascript:;" class="btn btn-success " id="Print" data-toggle="tooltip" data-placement="top" title="Print"><span class="glyphicon glyphicon-print"></span></a>            
                <?php }?>
            </div>         
         </div>
      </div>
	</div>

	<div class="in collapse" id="collapseExample">             
		<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" id="f_save">
			
         <div class="col-sm-3">
				<div class="form-group">           
					<label for="otherfee_code_">Other fee code<span style="color:red"></span></label>&nbsp;
					<input type="text" name="otherfee_code_" id="otherfee_code_" class="form-control" placeholder="Other fee code">
				</div>					
			</div>

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="otherfee_">Other fee<span style="color:red"></span></label>&nbsp;
               <input type="text" name="otherfee_" id="otherfee_" class="form-control" placeholder="Other fee">
            </div>               
         </div>
         

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="qty_">Quantity<span style="color:red"></span></label>
               <input type="text" name="qty_" id="qty_" class="form-control" placeholder="Quantity" decimal> 
            </div>         
         </div>

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="whcode">Location<span style="color:red"></span></label>
               <select name="whcode" id="whcode" class="form-control" placeholder="Location" disabled>
                  <?php
                  // getoption($sql, $key, $display, $select = '', $istop = false)
                  echo getoption("SELECT * FROM sch_stock_wharehouse AS w ORDER BY w.wharehouse ASC", "whcode", "wharehouse", "", true);
                  ?>
               </select> 
            </div>         
         </div>

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="from_date">From date<span style="color:red"></span></label>
               <input type="text" name="from_date" id="from_date" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>"> 
            </div>         
         </div>

         <div class="col-sm-3">
            <div class="form-group">           
               <label for="to_date">To date<span style="color:red"></span></label>
               <input type="text" name="to_date" id="to_date" class="form-control" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y') ?>">
            </div>         
         </div>

			<div class="col-sm-7 col-sm-offset-5">
				<div class="form-group"> 
            <?php if($this->green->gAction("R")){ ?> 
				   <button type="button" class="btn btn-success btn-sm" name="search" id="search">Search</button>
           <?php }?>
				   <button type="button" class="btn btn-default btn-sm" name="clear" id="clear">Clear</button>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
				   <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
				</div>             
			</div>
		</form>      
	</div>
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <div id="tab_print">
               <table border="0"​ cellspacing="0" cellpadding="0" align="center" id="other_expense_list" class="table table-hover">
                   <thead>
                        <tr style="background: #4cae4c;color: white;">
                           <th style="width: 5%;text-align: left;">No</th>
                           <th style="width: 15%;text-align: left;">Other fee code</th>
                           <th style="width: 20%;text-align: left;">Other fee</th>
                           <th style="width: 20%;text-align: left;">Remark</th>
                           <th style="width: 10%;text-align: right;">Quantity</th>
                           <th style="width: 10%;text-align: right;">Unit price ($)</th>
                           <th style="width: 10%;text-align: right;">Discount (%)</th>
                           <th style="width: 10%;text-align: right;">Amount ($)</th>
                           <th class="remove_tag no_wrap" style="width: 5%;text-align: center;" colspan="2">Action</th>
                        </tr>
                   </thead>
                   
      				<tbody>
      					
      				</tbody>
      				<tfoot>
                     <tr class="remove_tag no_wrap">
                        <td colspan="1">
                           <div>&nbsp;</div>
                           <select name="to_display" id="to_display" title="Display items..." style="height: 22px;">
                              <option value="5">5</option>
                              <option value="10">10</option>
                              <option value="30">30</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                              <option value="200">200</option>
                              <option value="500">500</option>
                              <option value="1000">1000</option>                       
                           </select>
                        </td>
                        <td colspan="2">
                           <div>&nbsp;</div>
                           <div class="btn-group pagination" role="group" aria-label="..." style="display: inline;"></div>
                           <span id="show_display" style="margin-left: 10px;">&nbsp;</span>
                        </td>                  
                        <td colspan="6">&nbsp;</td>
                     </tr>
      				</tfoot>
               </table> 
            </div>        
         </div>
      </div>

   </div>

</div>


<div class="modal bs-example-modal-lg fade m_master_detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #4cae4c;color: white;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="gridSystemModalLabel">Others expenses</h5>
      </div>
      <div class="modal-body" style="overflow-y: auto;max-height: 550px;">
      	<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" id="f_save_cate">
	      	<input type="hidden" name="typeno" id="typeno">
	        <div class="row">
				<div class="col-md-3">
					<label for="tran_date">Date<span style="color:red">*</span></label>
				   <input type="text" name="tran_date" id="tran_date" class="form-control input-sm" placeholder="dd/mm/yyyy" data-parsley-required="true" data-parsley-required-message="This field require" value="<?= date('d/m/Y') ?>">
				</div>
            <div class="col-md-9">
               <label for="note">Note<span style="color:red"></span></label>
               <input type="text" name="note" id="note" class="form-control input-sm" placeholder="Note">
            </div>            
				<div class="col-md-12">
					<div class="col-md-12" style="border-bottom: 1px solid #CCC;">
						&nbsp;
					</div>
				</div>
	        </div>
        </form>
        
        <div class="row">
		      <div class="col-sm-12">
		         <div class="table-responsive">
		            <table border="0"​ cellspacing="0" cellpadding="0" align="center" id="otherfee_expense_detail_list" class="table table-hover table-condensed">
		                <thead>
		                     <tr>
		                        <th style="width: 5%;text-align: left;">No</th>
		                        <th style="width: 15%;text-align: left;">Other fee code</th>
		                        <th style="width: 15%;text-align: left;">Other fee name</th>
                              <th style="width: 20%;text-align: left;">Remark</th>
                              <th style="width: 10%;text-align: right;">Quantity</th>
                              <th style="width: 14%;text-align: right;">Unit price ($)</th>
                              <th style="width: 10%;text-align: right;">Discount (%)</th>
                              <th style="width: 14%;text-align: right;">Amount ($)</th>
		                        <th style="width: 5%;text-align: center;" colspan="1">Action</th>
		                     </tr>
		                </thead>
		                
							<tbody>
								
							</tbody>
							<tfoot>
						      <tr>
                           <td colspan="7" style="text-align: right;">Total:</td>
                           <td class="total" style="text-align: right;font-weight: bold;">0.00</td>
                           <td colspan="2">&nbsp;</td>
                        </tr>
							</tfoot>
		            </table>         
		         </div>
		      </div>
		   </div>

      </div>
      <div class="modal-footer"> 
        <?php if($this->green->gAction("C")){ ?>        
        <button type="button" id="save_cate" class="btn btn-success btn-sm save_cate" data-cate="1">Save next</button>
        <?php }?>
        <?php if($this->green->gAction("C")){ ?>     
        <button type="button" id="save_close_cate" class="btn btn-success btn-sm save_cate" data-cate="2">Save close</button>
        <?php }?>
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- style -->
<style type="text/css">
   .ui-autocomplete-loading {
      background: white url("<?= base_url('assets/images/ui-anim_basic_16x16.gif') ?>") right center no-repeat;
   }
   .ui-autocomplete{height: auto;overflow-y: hidden;}
   .ui-menu .ui-menu-item:hover, .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus {
      background: #4cae4c;
      color: white;
   }

   #otherfee_expense_detail_list th{vertical-align: middle;}
   #otherfee_expense_detail_list td{vertical-align: middle;}

   #other_expense_list th{vertical-align: middle;}
   #other_expense_list td{vertical-align: middle;}    

   .input-xs {
       height: 22px;
       padding: 5px 5px;
       font-size: 12px;
       line-height: 1.5;
       border-radius: 3px;
   }  
</style>

<script type="text/javascript">
   $(function(){

      // select ========
      $('body').delegate('.qty, .price, .dis, .otherfee_code', 'focus', function(){
         $(this).select();
      });

      // chk ========
      $('body').delegate('.qty', 'focusout', function(){
         if($.trim($(this).val()) - 0 == 0){
            $(this).val('1');
         }
      });
      $('body').delegate('.price', 'focusout', function(){
         if($.trim($(this).val()) - 0 == 0){
            $(this).val('0.00');
         }
      });
      $('body').delegate('.dis', 'focusout', function(){
         if($.trim($(this).val()) - 0 == 0){
            $(this).val('0');
         }
      });

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // clear =======      
      $('body').delegate('#clear', 'click', function(){
      	clear();
         grid(1, $('#to_display').val() - 0);
   	});

      // search =======      
      $('body').delegate('#search', 'click', function(){
         grid(1, $('#to_display').val() - 0);
      });

      // print ----------------------------------------------------------
      $("#Print").on("click", function () {
         $(".trprint").show();
         $(".trhide").hide();
         var htmlToPrint = '' +
             '<style type="text/css">' +
             'table th, table td {' +
             'border:1px solid #000 !important;' +
             'padding;0.5em;' +
             '}' +
             '</style>';
         var title = "Others Expenses List";
         var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "");
         var export_data = $("<center>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         export_data += htmlToPrint;
         gsPrint(title, export_data);
         $(".trprint").hide();
         $(".trhide").show();
      });

      // export----------------------------------------------------------
      $('body').delegate('#Export', 'click', function(e){
         $(".trprint").show();
         $(".trhide").hide();
        var title = "Others Expenses List";
        // var data = $('.table').attr('border', 1);
        var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
        var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
        e.preventDefault();
         // $('.table').attr('border', 0);
         $(".trprint").hide();
         $(".trhide").show();
      }); 

      // edit ========
      $('body').delegate('.edit', 'click', function(){
         var typeno = $(this).data('typeno');
         $.ajax({
            url: '<?= site_url('expense/c_other_expense/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
               $('.xmodal').show();
            },
            complete: function(){
               $('.xmodal').hide();
            },
            data: {
               typeno: typeno
            },
            success: function(data){
               $('.m_master_detail').modal({
                  backdrop: 'static'
               })
               $('#typeno').val(data.row.typeno);
               $('#tran_date').val(data.row.tran_date);               
               $('#note').val(data.row.note);
               var total = data.row.total - 0 > 0 ? data.row.total - 0 : '0.00';
               $('.total').text(total.toFixed(2));

               var tr = '';
               if(data.q.length > 0){
                  $.each(data.q, function(i, v){
                     tr += '<tr>\
                              <td class="no">'+ (i + 1) +'</td>\
                              <td>\
                                 <input type="text" class="form-control input-sm otherfee_code" name="otherfee_code[]" placeholder="Other fee code" value="'+ (v.otherfee_code != null ? v.otherfee_code : '') +'" data-otherfeeid="'+ v.otherfeeid +'"></td>\
                              <td class="otherfee">'+ v.otherfee +'</td>\
                              <td>\
                                 <input type="text" class="form-control input-sm remark" name="remark[]" placeholder="Remark" style="_resize: none;" value="'+ (v.remark != null ? v.remark : '') +'"></td>\
                              <td style="text-align: right;">\
                                 <input type="text" class="form-control input-sm qty" name="qty[]" value="'+ (v.qty - 0 > 0 ? v.qty : '1') +'" style="text-align: right;" decimal></td>\
                              <td style="text-align: right;">\
                                 <input type="text" class="form-control input-sm price" name="price[]" value="'+ (v.price - 0 > 0 ? (v.price - 0).toFixed(2) : '0.00') +'" style="text-align: right;" decimal></td>\
                              <td style="text-align: right;">\
                                 <input type="text" class="form-control input-sm dis" name="dis[]" value="'+ (v.dis - 0 > 0 ? v.dis : '0') +'" style="text-align: right;" decimal></td>\
                              <td style="text-align: right;" name="amt[]" class="amt">'+ (v.amt - 0 > 0 ? (v.amt - 0).toFixed(2) : '0.00') +'</td>\
                              <td style="text-align: center;"><?php if($this->green->gAction("D")){ ?><a href="javascript:;" class="btn btn-xs btn-danger del_d"><span class="glyphicon glyphicon-trash"></span></a><?php }?></td>\
                           </tr>';
                     if(data.q.length - (i + 1) == 0){
                        tr += '<tr>\
                                    <td class="no">'+ (i + 2) +'</td>\
                                    <td>\
                                       <input type="text" class="form-control input-sm otherfee_code" name="otherfee_code[]" placeholder="Other fee code"></td>\
                                    <td class="otherfee"></td>\
                                    <td>\
                                       <input type="text" class="form-control input-sm remark" name="remark[]" placeholder="Remark" style="_resize: none;"></td>\
                                    <td style="text-align: right;">\
                                       <input type="text" class="form-control input-sm qty" name="qty[]" value="1" style="text-align: right;" decimal></td>\
                                    <td style="text-align: right;">\
                                       <input type="text" class="form-control input-sm price" name="price[]" value="0.00" style="text-align: right;" decimal></td>\
                                    <td style="text-align: right;">\
                                       <input type="text" class="form-control input-sm dis" name="dis[]" value="0" style="text-align: right;" decimal></td>\
                                    <td style="text-align: right;" name="amt[]" class="amt">0.00</td>\
                                    <td style="text-align: center;"><?php if($this->green->gAction("D")){ ?><a href="javascript:;" class="btn btn-xs btn-danger del_d"><span class="glyphicon glyphicon-trash"></span></a><?php }?></td>\
                                 </tr>';
                     }
                  });
                  $('#otherfee_expense_detail_list tbody').html(tr);
               }
               else{
                  add_row_();
               }               
               
               $('#f_save_cate').parsley().destroy();
               $('#save_cate').text('Update');
               $('#save_close_cate').hide();            
            },
            error: function() {

            }
         });         
         
      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var typeno = $(this).data('typeno');
         if(window.confirm('Are you sure to delete?')){
            $.ajax({
               url: '<?= site_url('expense/c_other_expense/delete') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  typeno: typeno
               },
               success: function(data){
                  if(data == 1){
                     toastr["warning"]("Deleted!");
                     // clear();
                     grid(1, $('#to_display').val() - 0);
                  }else{
                     toastr["warning"]("Can't delete!");
                  }               
                  
               },
               error: function() {

               }
            });
         }        
      });

      // search =======
      $('body').delegate('#a_search', 'click', function(){
         $('#collapseExample').collapse('toggle')
         // clear();
      });

      // add new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('.m_master_detail').modal({
            backdrop: 'static'
         })         
         $('#tran_date').val('<?= date("d/m/Y") ?>');
         $('#save_cate').text('Save next');
         $('#save_close_cate').show();
         $('#f_save_cate').parsley().destroy();
         clear_();
      });

      // init. =======
      grid(1, $('#to_display').val() - 0);
      // search ========
      $('body').delegate('#search_cate', 'keyup', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#search_item_code', 'keyup', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#search_item_name', 'keyup', function(){
         grid(1, $('#to_display').val() - 0);
      });
      $('body').delegate('#to_display', 'change', function(){
         grid(1, $('#to_display').val() - 0);
      });      
      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#to_display').val() - 0);
      });      


      // diag. ======================
      // save master_detail. =======      
      $('body').delegate('.save_cate', 'click', function(){
      	var save = $(this).data('cate');
         var typeno = $('#typeno').val();
         var tran_date = $('#tran_date').val();
         var note = $('#note').val();
         var total = $('.total').text();         

         var arr =[];
         var j = 0;
         $('.otherfee_code').each(function(i){
            var otherfee_code = $(this).val();            
            if($.trim(otherfee_code) != ""){
               j++;
            }

            var tr = $(this).parent().parent();
            var otherfeeid = $(this).data('otherfeeid');
            var otherfee_code = tr.find('.otherfee_code').val();
            var remark = tr.find('.remark').val();
            var qty = tr.find('.qty').val();
            var price = tr.find('.price').val();
            var dis = tr.find('.dis').val();
            var amt = tr.find('.amt').text();                        
            arr[i] = {otherfeeid: otherfeeid, otherfee_code: otherfee_code, remark: remark, qty: qty, price: price, dis: dis, amt: amt};
         });

         if(j == 0){
            toastr["warning"]("At least choose a item to save!");
         }
         else{
         	if($('#f_save_cate').parsley().validate()){
   	         $.ajax({
   	            url: '<?= site_url('expense/c_other_expense/save_op_md') ?>',
   	            type: 'POST',
   	            datatype: 'JSON',
   	            // async: false,
   	            beforeSend: function(){
   	            	$('.xmodal').show();
   	            },
   	            complete: function(){
   						$('.xmodal').hide();
   	            },
   	            data: {
   	            	tran_date: tran_date,
                     typeno: typeno,
   	            	note: note,
                     total: total,
   	            	arr: arr	            	
   	            },
   	            success: function(data){
   	               if(data == 1){	                  
   	                  if(save == 2){
   	                  	$('.m_master_detail').modal('hide')
   	                  }
   	                  grid(1, $('#to_display').val() - 0);
   	                  clear_();	     
   	                  toastr["success"]('Saved!');	                  
   	               }
   	               else if(data == 2){
   	               	if(save == 2){
                           $('.m_master_detail').modal('hide')
                        }
                        grid(1, $('#to_display').val() - 0);
                        clear_();           
   	                  toastr["success"]('Updated!');                     
                        $('#save_cate').text('Save next');
                        $('#save_close_cate').show();
   	               }
   	               else{
   	                  toastr["warning"]("Can't save!");
   	               }         
   	            
   	            },
   	            error: function() {

   	            }
   	         });
            }
         }
      });

      $('#tran_date').datepicker({
         format: 'dd/mm/yyyy',
         autoclose: true
      });
      $('#from_date').datepicker({
         format: 'dd/mm/yyyy',
         autoclose: true
      });
      $('#to_date').datepicker({
         format: 'dd/mm/yyyy',
         autoclose: true
      });

      // search cate. ====
      $('body').delegate('#search_cate_name', 'keyup', function(){
      	grid_cate();
      });

      // add row ======
      add_row();

      // remove =======
      $('body').delegate('.del_d', 'click', function(){
         if($('.no').length > 1){
            $(this).parent().parent().remove();
            $('.no').each(function(i) {
               $(this).html(i + 1);
            });
            cal_col();
         }
         else{
            toastr["warning"]("Only one row can't deleted!");
         }
      });

      // autocomplete ========
      $('body').delegate(".otherfee_code", 'focus', function(){
         $( this ).autocomplete({
         source: "<?= site_url('expense/c_other_expense/get_std') ?>",
         minLength: 0,
         autoFocus: true,
         delay: 0,
         focus: function( event, ui ) {
            return false;
         },
         select: function( event, ui ) {
            var tr = $(this).parent().parent();
            var b = false;
            var otherfee_code = ui.item.otherfee_code;
            var k = 1;
            $('.otherfee_code').not(this).each(function(){
               k++;
               var otherfee_code_chk = $(this).val();
               if(k > 1){
                  if(otherfee_code_chk == otherfee_code){                   
                     b = true;               
                  }   
               }            
            }); 

            if(b == true){
               toastr["warning"]('Other fee code "' + otherfee_code + '" existed!');
               $(this).val('');
               $(this).blur();              
            }
            else{
               // if(k > 1){
                  $(this).attr("data-otherfeeid", ui.item.otherfeeid);           
                  $(this).val(ui.item.otherfee_code);
                  tr.find('.otherfee').text(ui.item.otherfee);
                  tr.find('.qty').val(1);
                  if (event.keyCode == 9) {
                     tr.find('.remark').select();
                  }
                  else{
                     tr.find('.qty').select();
                  }                                          
                  tr.find('.price').val('0.00');

                  // if($('#otherfee_expense_detail_list tr:last').find('.otherfee_code').val() != ""){
                  if($('#otherfee_expense_detail_list tr:last').find('.otherfee_code').val() != ""){
                     add_row();
                  }
               // }
            }

            return false;                       
         }
         }).focus(function(){
            $( this ).autocomplete("search");
         })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              .append( "<span>" + (item.otherfee_code != null ? item.otherfee_code : '') + "</span> | " )
              .append( "<span>" + (item.otherfee != null ? item.otherfee : '') + "</span>" )
              .append( "</li>" )
              .appendTo( ul );
         }
      });

      $( "#otherfee_code_" ).autocomplete({
         source: "<?= site_url('expense/c_other_expense/get_std') ?>",
         minLength: 0,
         autoFocus: true,
         delay: 0,
         focus: function( event, ui ) {
            return false;
         },
         select: function( event, ui ) { 
            // $( "#item_code" ).val(ui.item.stockcode);         
            $(this).val(ui.item.otherfee_code);           
            return false;                       
         }
         }).focus(function(){
            $( this ).autocomplete("search");
         })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              .append( "<div>" + item.otherfee_code + "</div>" )             
              .append( "</li>" )
              .appendTo( ul );
      }

      $( "#otherfee_" ).autocomplete({
         source: "<?= site_url('expense/c_other_expense/get_std') ?>",
         minLength: 0,
         autoFocus: true,
         delay: 0,
         focus: function( event, ui ) {
            return false;
         },
         select: function( event, ui ) { 
            // $( "#item_code" ).val(ui.item.stockcode);         
            $(this).val(ui.item.otherfee);           
            return false;                       
         }
         }).focus(function(){
            $( this ).autocomplete("search");
         })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              .append( "<div>" + item.otherfee + "</div>" )              
              .append( "</li>" )
              .appendTo( ul );
      }

      // otherfee code =====
      $('body').delegate(".otherfee_code", 'focusout', function(){
         var otherfee_code = $(this).val();
         var otherfee_code_ = $(this);
         
         if(otherfee_code != ""){
            $.ajax({
               url: '<?= site_url('expense/c_other_expense/chk_stockcode') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  otherfee_code: otherfee_code
               },
               success: function(data){
                  if(data.n){
                     toastr["warning"](data.n);
                     otherfee_code_.removeAttr("data-otherfeeid");
                     otherfee_code_.val('');
                     otherfee_code_.select();
                     otherfee_code_.parent().parent().find('.otherfee').text('');
                     otherfee_code_.parent().parent().find('.remark').val('');
                     otherfee_code_.parent().parent().find('.qty').val(1);
                     otherfee_code_.parent().parent().find('.price').val('0.00');
                     otherfee_code_.parent().parent().find('.dis').val('0'); 
                     otherfee_code_.parent().parent().find('.amt').text('0.00'); 
                     $('.total').text('0.00');                    
                  }
                  else{
                     // otherfee_code_.attr("data-otherfeeid", data.r.otherfeeid);
                  }
               },
               error: function() {

               }
            });
         }
      });

      // cal. =========
      $('body').delegate('.dis', 'keyup focusout', function (e) {
         var v = $(this).val() - 0;
         if (v > 100 || v < 0) {
            toastr["warning"]("Can't input value more than 100!");
            $(this).val(0);
            $(this).select();
         }
         var tr = $(this).parent().parent();
         cal_line(tr);
      });
      $('body').delegate('.qty', 'keyup focusout', function (e) {
         var tr = $(this).parent().parent();
         cal_line(tr);
      });
      $('body').delegate('.price', 'keyup focusout', function (e) {
         var tr = $(this).parent().parent();
         cal_line(tr);
      });
      

   }); // ready =======  

   function cal_line(tr){
      var dis = tr.find('.dis').val() - 0;
      var qty = tr.find('.qty').val() - 0;
      var price = tr.find('.price').val() - 0;      
      var amt = qty * price * (1 - dis / 100);// var amt = qty*price - qty*price*dis/100;
      tr.find('.amt').text(amt.toFixed(2));
      cal_col();
   }
    
   function cal_col(){
      var total = 0;
      $('.amt').each(function(){
         var v = $(this).text() - 0;
         total += v;
      });
      $('.total').text(total.toFixed(2));
   } 
   
   function add_row(){
      var tr = "";
      var i = $('.no').length + 1;
      tr += '<tr>\
                  <td class="no"></td>\
                  <td>\
                     <input type="text" class="form-control input-sm otherfee_code" name="otherfee_code[]" placeholder="Other fee code"></td>\
                  <td class="otherfee"></td>\
                  <td>\
                     <input type="text" class="form-control input-sm remark" name="remark[]" placeholder="Remark" style="_resize: none;"></td>\
                  <td style="text-align: right;">\
                     <input type="text" class="form-control input-sm qty" name="qty[]" style="text-align: right;" value="1" decimal></td>\
                  <td style="text-align: right;">\
                     <input type="text" class="form-control input-sm price" name="price[]" value="0.00" style="text-align: right;" decimal></td>\
                  <td style="text-align: right;">\
                     <input type="text" class="form-control input-sm dis" name="dis[]" value="0" style="text-align: right;" decimal></td>\
                  <td style="text-align: right;" name="amt[]" class="amt">0.00</td>\
                  <td style="text-align: center;"><?php if($this->green->gAction("D")){ ?><a href="javascript:;" class="btn btn-xs btn-danger del_d"><span class="glyphicon glyphicon-trash"></span></a><?php }?></td>\
               </tr>';
      $('#otherfee_expense_detail_list tbody').append(tr);
      $('.no').each(function(i) {
         $(this).html(i + 1);
      });      
   }

   function add_row_(){
      var tr = '';
      tr += '<tr>\
                  <td class="no"></td>\
                  <td>\
                     <input type="text" class="form-control input-sm otherfee_code" name="otherfee_code[]" placeholder="Other fee code"></td>\
                     <input type="hidden" class="otherfee_code_chk" name="otherfee_code_chk[]"></td>\
                  <td class="otherfee"></td>\
                  <td>\
                     <input type="text" class="form-control input-sm remark" name="remark[]" placeholder="Remark" style="_resize: none;"></td>\
                  <td style="text-align: right;">\
                     <input type="text" class="form-control input-sm qty" name="qty[]" value="1" style="text-align: right;" value="1" decimal></td>\
                  <td style="text-align: right;">\
                     <input type="text" class="form-control input-sm price" name="price[]" value="0.00" style="text-align: right;" decimal></td>\
                  <td style="text-align: right;">\
                     <input type="text" class="form-control input-sm dis" name="dis[]" value="0" style="text-align: right;" decimal></td>\
                  <td style="text-align: right;" name="amt[]" class="amt">0.00</td>\
                  <td style="text-align: center;"><?php if($this->green->gAction("D")){ ?><a href="javascript:;" class="btn btn-xs btn-danger del_d"><span class="glyphicon glyphicon-trash"></span></a><?php }?></td>\
               </tr>';
      $('#otherfee_expense_detail_list tbody').html(tr);
      $('.no').each(function(i) {
         $(this).html(i + 1);
      });
   }

	// clear =========
   function clear(){
      $('#otherfee_code_').val('');
      $('#otherfee_').val('');
      $('#qty_').val('');
      $('#from_date').val('<?= date('d/m/Y') ?>');      
      $('#to_date').val('<?= date('d/m/Y') ?>'); 
      $('#whcode').val(''); 
   }

   // clear_ =======
   function clear_(){
      $('#tran_date').val('<?= date("d/m/Y") ?>');
      $('#typeno').val('');
      $('#note').val('');
      $('.total').text('0.00');
      add_row_();      
   }

   // get cate. ====
   function get_category(){
   	$.ajax({
         url: '<?= site_url('expense/c_other_expense/get_category') ?>',
         type: 'POST',
         datatype: 'Html',
         beforeSend: function(){
         	// $('.xmodal').show();
         },
         complete: function(){
         	// $('.xmodal').hide();
         },
         data: {
        	
         },
         success: function(data){
            $('#cate_id').html(data);         
         },
         error: function() {

         }
      });
   }

   // gsPrint --------------------
   function gsPrint(emp_title, data) {
      var element = "<div>" + data + "</div>";
      $("<center><p style='padding-top:15px;text-align:center;'><b>" + emp_title + "</b></p><hr>" + element + "</center>").printArea({
          mode: "popup",  //printable window is either iframe or browser popup
          popHt: 600,  // popup window height
          popWd: 500,  // popup window width
          popX: 0,  // popup window screen X position
          popY: 0, //popup window screen Y position
          popTitle: "test", // popup window title element
          popClose: false,  // popup window close after printing
          strict: false
      });
   }

   // grid =========
   function grid(current_page, total_display){
   	var offset = (current_page - 1)*total_display - 0;  
   	var limit = total_display - 0
      $.ajax({
         url: '<?= site_url('expense/c_other_expense/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
         	$('.xmodal').show();	
         },
         complete: function(){
         	$('.xmodal').hide();
         },
         data: {
         	offset: offset,
         	limit: limit
            ,
            otherfee_code: $('#otherfee_code_').val(),
            otherfee_: $('#otherfee_').val(),            
            qty: $('#qty_').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            whcode: $('#whcode').val()                                    
         },
         success: function(data) {
            $('#other_expense_list tbody').html(data.tr);

            var page = '';
            var total = '';
            var show_display = '';
            if(data.totalRecord - 0 > 0){
               // previous ====
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next<i class="glyphicon glyphicon-chevron-right"></i></button>';

               show_display += ((offset + limit > data.totalRecord ? data.totalRecord : offset + limit) + ' / ' + data.totalRecord);
            }

            $('#show_display').html(show_display);
            $('.pagination').html(page);

         },
         error: function() {

         }
      });
   }
</script>