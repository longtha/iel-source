<head>
   <script src="<?= base_url('assets/js/plotly-latest.min.js') ?>"></script>
</head>

<body>
<div class="container-fluid">
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6" style="background: #4cae4c;color: white;">
               <span class="glyphicon glyphicon-stats"></span>
               <strong>Profit/loss graphics search</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
               <?php if($this->green->gAction("R")){ ?> 
                  <!-- <a href="javascript:;" class="btn btn-sm btn-success" id="a_search" data-toggle="tooltip" data-placement="top" title="Search"><span class="glyphicon glyphicon-search"></span></a> -->
               <?php }?>
               <a href="javascript:;" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh"><span class="glyphicon glyphicon-refresh"></span></a>
            </div>         
         </div>
      </div>
   </div>

   <form class="form-horizontal">
      <div class="form-group">
         <label for="yyear" class="col-sm-1 control-label" style="padding-right: 0;">Year</label>
         <div class="col-sm-3">
            <select type="text" name="yyear" id="yyear" class="form-control input-sm">
               <?php
               $year = date('Y');
               $xyear = $year - 15;
               for($i = 1; $i <= 30; $i++){
                  $y = $xyear + $i;
                  $selected = $y == $year ? ' selected="selected" ' : '';
                  echo '<option value="'.$y.'" '.$selected.'>'.$y.'</option>';
               }
               ?>
            </select>
         </div>

         <label for="type_chart" class="col-sm-1 control-label" style="padding-right: 0;">Chart type</label>
         <div class="col-sm-3">
            <select type="text" name="type_chart" id="type_chart" class="col-sm-3 form-control input-sm">
               <option value="1">Bar chart</option>
               <option value="2">Line chart</option>            
            </select>
         </div>         
      </div>      
   </form>

   <div class="row">
      <div class="col-sm-12">
         <div class="col-sm-12" style="border-top: 1px solid #DDD;">&nbsp;</div>
      </div>             
   </div>

   <div class="col-sm-12 graph">
      <div id="revenue" style="border-left: 6px solid #337ab7;margin-bottom: 5px;padding-left: 5px;font-size: 11px;"></div>
      <div id="expense" style="border-left: 6px solid #f0ad4e;padding-left: 5px;font-size: 11px;"></div> 

      <div id="myDiv" style="width: 100%;height_: 80%;"></div>
      <!-- Plotly chart will be drawn inside this DIV -->
   </div>

   <div class="col-sm-12 display" style="background: #F2F2F2;text-align: center;"></div>

</div>
<script>

   $(function(){
      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      $('body').delegate('#yyear', 'change', function(){
         expenses();
      });

      $('body').delegate('#type_chart', 'change', function(){
         expenses();
      });

      expenses();
   });// ready ====

   function expenses(){
      $.ajax({
         url: '<?= site_url('expense/c_setup_other_expense/get_data_graph') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
            $('.xmodal').show(); 
         },
         complete: function(){
            $('.xmodal').hide();
         },
         data: {
            yyear: $('#yyear').val()
         },
         success: function(dd) {
            if(dd.c != '' && dd.c_ != ''){
               $('.graph').show();
               $('.display').hide();

               // re. & exp. ======
               var dv_revenue = (dd.to_revenue - 0 > 0  ? '<div style="font-weight: bold;">'+ dd.to_revenue.toFixed(2) +'$</div> <div style="color: #CCC;">REVENUES</div>' : '');
               var dv_expense = (dd.to_expense - 0 > 0  ? '<div style="font-weight: bold;">'+ dd.to_expense.toFixed(2) +'$</div> <div style="color: #CCC;">EXPENSES</div>' : '');
               $('#revenue').html(dv_revenue);
               $('#expense').html(dv_expense);
               
               // trace1 ========
               var trace1 = {
                 x: dd.arr_r, 
                 y: dd.arr_r1, 
                 name: 'Revenues', 
                 type: ($('#type_chart').val() - 0 == 1 ? 'bar' : 'line')
               };

               // trace2 ========
               var trace2 = {
                 x: dd.arr_ex,//['giraffes', 'orangutans', 'monkeys'], 
                 y: dd.arr_ex1,//[12, 18, 29],
                 name: 'Expenses', 
                 type: ($('#type_chart').val() - 0 == 1 ? 'bar' : 'line')
               };

               var data = [trace1, trace2];
               var layout = {barmode: 'group'};
               Plotly.newPlot('myDiv', data, layout);
            }
            else{
               $('.graph').hide();
               $('.display').show();
               $('.display').html('<span style="font-weight: bold;font-size: 12px;color: #337ab7;">No data!</span>');
            }
         },
         error: function() {

         }
      });
   }

   
</script>
</body>