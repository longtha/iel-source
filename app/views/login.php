<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>GreenSIM</title>
	    <!-- Bootstrap Core CSS -->
	    <link href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
	    <!-- Custom Fonts -->
	    <link href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
	    <!-- Jquery ui -->
	    <link href="<?php echo base_url('assets/css/jquery/jquery-ui.css') ?>" rel="stylesheet">

		<!-- Green CSS --->
		<link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
		<!-- Favicon -->
	    <link rel="apple-touch-icon" href="../../apple-touch-icon.html">
	    <link rel="icon" href="../../favicon.html">
	    <link href="<?php echo base_url('assets/css/toggle/bootstrap-toggle.css')?>" rel="stylesheet">
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

		<!-- jQuery -->
		<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js')?>"></script>
		<script src="<?php echo base_url('assets/js/jquery/jquery-ui.js')?>"></script>
		<script src="<?php echo base_url('assets/js/parsley.min.js')?>"></script>
		<!-- Bootstrap Core JavaScript -->
		<script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>

		<script src="<?php echo base_url('assets/js/parsley.min.js')?>"></script>
		<script src="<?php echo base_url('assets/js/toggle/bootstrap-toggle.js')?>"></script>

		<script type="text/javascript">
			$(function(){
				$('#loginform').parsley();
			});

		</script>

	</head>
	<body>

		<div class="container-fluid">
		    <div class="row-fluid">
		        <div class="loginform text-center">
		        	<div class="dv_login" style="height: 228px;">
		        		<div class="dv_login_icon">
		        			<img src="<?php echo base_url('assets/images/icons/login.png') ?>"/>
		        		</div>
		        		<div class="dv_login_form">
		        			<form action="<?php echo site_url('login/getLogin')?>" method="post" id="loginform">
		        				<div class="row" style="line-height: 30px;">
		        					<div class="col-md-12 login_title text-left">GreenSIM V02</div>
		        				</div>
		        				<div class="row" id="rowSch" style="display: none; line-height: 40px;">
		        					<div class="col-md-4 text-left">School</div>
		        					<div class="col-md-8">
		        						<select class="form-control" name="schoolid">
			        						<?php echo $this->green->GetSchool()?>
			        					</select>
		        					</div>
		        				</div>
		        				<div class="row" id="rowYear" style="display: none; line-height: 40px;">
		        					<div class="col-md-4 text-left">Year</div>
		        					<div class="col-md-8">
		        						<select class="form-control" name="year">
		        							<?php echo $this->green->GetSchYear();?>
		        						</select>
		        					</div>
		        				</div>
		        				<div class="row" style="line-height: 40px;">
		        					<div class="col-md-4 text-left">User name</div>
		        					<div class="col-md-8">
		        						<input type="text" name="user_name" id="user_name" class="form-control col-sm-4" required data-parsley-required-message="Enter User Name"/>
		        					</div>
		        				</div>
		        				<div class="row" style="line-height: 40px;">
		        					<div class="col-md-4 text-left">Password</div>
		        					<div class="col-md-8">
		        						<input type="password" name="password" id="password"  class="form-control col-sm-4" required data-parsley-required-message="Enter Password"/>
		        					</div>
		        				</div>
		        				<div class="row" style="line-height: 50px;">
		        					<div class="col-md-12 text-left">
		        						Login as student?
			        					<input type="checkbox" id="status" name="status" checked data-toggle="toggle" data-on="Yes" data-off="No" />
		        					</div>
		        				</div>
		        				<div class="row" style="line-height: 40px;">
		        					 <div class="col-md-6 col-md-offset-3 content">
		        					 	<input type="submit" name="login" id="login"  class="form-control btn-primary" value="Login"/>
		        					 </div>
		        				</div>

		        			</form>
		        		</div>
		        	</div>
		        </div>
		    </div>
		</div>

	</body>
	<style>
		html, body{height:100%; margin:0;padding:0}

		body {
		    background: url(<?php echo base_url("assets/images/background.png")?>);
		    background-repeat: no-repeat;
		    background-size: cover;
		    opacity: 0.5px;
		    background-attachment: fixed;
    		background-position: center;
		}

		.login_title{
			font-size: 18px;
			color: #276B08;
			font-weight: bold;
			padding-bottom: 8px;
			border-bottom: 1px solid #E9EDE8;
			font-family: "Khmer OS Battambang";
		}

		.container-fluid{
		  height:100%;
		  display:table;
		  width: 100%;
		  padding: 0;
		}
		.row-fluid {height: 100%; display:table-cell; vertical-align: middle;}
		
		.dv_login{
			position: relative;
			width:600px;
			/*height:340px;*/
			margin: 0 auto;
			background: none repeat scroll 0 0 #f8f8f8;
		    border: 1px solid #DBD4D4;
		    box-shadow: 1px 5px 5px 2px #666363;
		    border-radius: 4px;
		    padding-top: 15px;
		    opacity: 0.7;

		}

		.dv_login_icon{
			width:200px;
			float: left;
		}
		.dv_login_form{
			width:290px;
			float: left;
		}
		#login{
			width: 100px;
			display: block;
			margin: auto;
		}
	</style>
	<script type="text/javascript">
		
		$("#status").change(function() {
			var newHeight = $(".dv_login").height();
				if ($("#status").is(':checked')) {
					newHeight -= 80;
					$("#rowSch").css({
						"display": "none"
					});
					$("#rowYear").css({
						"display": "none"
					});
				} else {
					newHeight += 80;
					$("#rowSch").css({
						"display": "block"
					});
					$("#rowYear").css({
						"display": "block"
					});
				}
			$(".dv_login").height(newHeight);
		});
		var hasErrorUsername = false;
		var hasErrorPassword = false;
		$('#user_name').parsley().on('field:success', function() {
  			// In here, `this` is the parlsey instance of #some-input
  			if (hasErrorUsername) {
				decreaseHeight(45);
  				hasErrorUsername = false;
  			}
		});

		$('#user_name').parsley().on('field:error', function() {
  			// In here, `this` is the parlsey instance of #some-input
  			if (!hasErrorUsername) {
	  			increaseHeight(45);
	  			hasErrorUsername = true;
	  		}
		});

		$('#password').parsley().on('field:success', function() {
  			// In here, `this` is the parlsey instance of #some-input
  			if (hasErrorPassword) {
  				decreaseHeight(45);
  				hasErrorPassword = false;
  			}
		});

		$('#password').parsley().on('field:error', function() {
  			// In here, `this` is the parlsey instance of #some-input
  			if (!hasErrorPassword) {
	  			increaseHeight(45);
	  			hasErrorPassword = true;
  			}
		});

		function increaseHeight(height) {
			var newHeight = $(".dv_login").height() + height;
  			$(".dv_login").height(newHeight);
		}

		function decreaseHeight(height) {
			var newHeight = $(".dv_login").height() - height;
  			$(".dv_login").height(newHeight);
		}

	</script>
</html>
