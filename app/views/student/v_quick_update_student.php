<style type="text/css">
    table tbody tr td img{width: 20px; margin-right: 10px}
    a,.sort,.panel-heading span{cursor: pointer;}
    .panel-heading span{margin-left: 10px;}
    .cur_sort_up{
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
        background-position: left;
        background-repeat: no-repeat;
        padding-left: 15px !important;
    }
    .cur_sort_down{
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
        background-position: left;
        background-repeat: no-repeat;
        padding-left: 15px !important;
    }
    .ui-autocomplete{z-index: 9999;}

    .panel-body{
        /*height: 650px;*/
        max-height: calc(100vh - 200px);
        overflow-y: auto;
    }

    #list th {vertical-align: middle;}
    #list td {vertical-align: middle;}
    .hide_controls{display:none;}
</style>
<?php  
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
}
?>

<div class="container-fluid" style="width:100% !important;padding-bottom: 20px;">
    <div class="row">
	    <div class="col-xs-12">
	        <div class="result_info">
	            <div class="col-xs-6">
		            <span class="icon">
		                <i class="fa fa-th"></i>
		            </span>
	                <strong>Quick Update Student</strong>  
	            </div>
	            <div class="col-xs-6" style="text-align: right">
	                <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
	                    <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
	                </a>
	                <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
	                   <img src="<?= base_url('assets/images/icons/print.png') ?>">
	                </a>
	                <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
	                   <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
	                </a>
	            </div>         
	        </div>
	    </div>
    </div>
<div class="collapse in" id="colapseform">
    <div class="collapse in" id="colapseform">
        <div class="col-sm-12">
  		    <div class="panel panel-default" style="margin-left: -14px;margin-right: -14px;">
  			    <div class="panel-body">
  				    <div class="col-sm-4">
  				    	<div class="form-group">
  							<label>Student ID:</label>
  							<input type='text' value="" class='form-control input-sm' name='s_student_id' id='s_student_id'/>
  						</div>
  					    <div class="form-group">					
  					        <label>Program:</label>
  						    <select class="form-control" id="program">
  							    <option value=""></option>
  							   		<?php echo $opt_program;?>
  							</select>
  						</div>
  						<div class="form-group">					
  							<label>Grad Level:</label>
  							<select class="form-control" id="grad_level"></select>
  						</div>
  					</div>
  					<div class="col-sm-4">
  						<div class="form-group">
  							<label>Name(Khmer/English):</label>
  							<input type='text' value="" class='form-control input-sm' name='s_full_name' id='s_full_name'/>
  						</div>					
  						<div class="form-group">					
    						<label>School Level:</label>
    							<select class="form-control" id="sch_level"></select>
  							</div>
  						<div class="form-group">						
  							<label>Class:</label>
  							<select class="form-control" id="classid"></select>
  						</div>  						
  					</div>
	  				<div class="col-sm-4">
	  					<div class="form-group">
	  						<label>Gender:</label>
							<select class="form-control" id="gender">
    							<option value=""></option>
    							<option value="male">Male</option>
    							<option value="female">Female</option>
							</select>
	  					</div> 
	  					<div class="form-group">					
	    					<label>Year:</label>
	    						<select class="form-control" id="years"></select>
	  					</div>				
	  				</div>  				
				  	<div class="col-sm-12" style="padding-top:20px;">
				        <div class="form-group">
				            <input type="button" class="btn btn-primary" value="Search" id="search">
				        </div>            
				    </div>
  				</div>
  			</div>
  		</div>
  	</div>
</div>
<!-- List Data -->
	<div class="row">
      	<div class="col-sm-12">
      		<div class="panel panel-default">
           		<div class="table-responsive">
           			<table class="table" border='0'>
           				<thead >
           					<tr>
           						<?php
		                            foreach ($thead as $th => $val) {
		                                if ($th == 'Action'){
		                                    echo "<th class='remove_tag'>" . $th . "</th>";
		                                }
		                                else{
		                                    echo "<th class='sort $val no_wrap' onclick='sort(event);' rel='$val' sortype='ASC'>" . $th . "</th>";
		                                }
		                            }
		                        ?>
           					</tr>
           				</thead>
           				<tbody class='listbody'>
           					
           				</tbody>
           			</table>
           		</div>
           	</div>
   			<div class="fg-toolbar">
                <div style='margin-top:20px; width:11%; float:left;'>
				Display : <select id='perpage' onchange='get_student_list(1);' style='padding:5px; margin-right:0px;'>
								<?PHP
                                    for ($i = 10; $i < 500; $i += 10) {           
                                       echo "<option value='$i' selected>$i</option>";			                                            
                                    }
                                ?>												
							</select>
				</div>
                <div class='paginate' style="float:right;"></div>
            </div> 
		</div>
	</div>	      	
</div>
<!-- --End List Data-- -->

<script type="text/javascript">
	$(function(){

		//----------------Pagenation---------------------
		$(document).on('click', '.pagenav', function(){
		    var page = $(this).attr("id");
			get_student_list(page);			
		});
		//---------------End------------------------------
		$('#perpage').val(10);

		clear_data();
		$('#program').change(function(){
			var $program = $(this).val();
			if($program != ""){
				get_school_level($program);
				if($('#sch_level').val() != ""){
					get_gradlevel(program,$("#sch_level").val());
					get_year(program,$("#sch_level").val());
				}else{
					 $('#grad_level').html('');
				}
			}else{
				clear_data();
			}
		});

		$("#a_addnew").click(function(){
          $("#colapseform").collapse('toggle');
        });

		$('#sch_level').change(function(){      
	        var sch_level = $(this).val(); 
	        var program = $('#program').val();
	        if(sch_level !=""){
	            get_year(program,sch_level);               
	        }else{
	        	$('#years').html('');
	        	$('#grad_level').html('');
	        	$('#classid').html('');
	        }
	     });

		$('#grad_level').change(function(){
	        var sch_level = $('#sch_level').val();
	        var grad_level = $(this).val();  
	        if(grad_level !=""){
	             get_class(sch_level,grad_level);            
	        }else{
	        	$('#classid').html('');
	        }
	      });

		$( "#search" ).click(function() {    	
  		    get_student_list(1);
      });
	});

	function  get_school_level(program){
	    $.ajax({
		      url: '<?php echo site_url('student/c_quick_update_student/school_level') ?>',
		      datatype:'JSON',
		      type:'POST',
		      async:false,
		      data:{
		            'program':program
		      },
		      success:function(data){
		          $('#sch_level').html(data.schlevel);    
		      }
	    });
  	}

  	function get_gradlevel(program,sch_level){
    $.ajax({
      url:'<?php echo site_url('student/c_quick_update_student/get_gradlevel') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'program':program,
              'sch_level':sch_level
      },
      success:function (data){
          $('#grad_level').html(data.gradlevel);         
      },
    });
  }

  	function get_year(program,sch_level){
	    $.ajax({
		      url:'<?php echo site_url('student/c_quick_update_student/get_years') ?>',
		      type:'POST',
		      datatype:'JSON',     
		      async:false,
		      data:{
		              'program':program,
		              'sch_level':sch_level
		      },
		      success:function (data){
		          $('#years').html(data.schyear);
		          get_gradlevel(program,sch_level);
		      }
	    });
  	}

  	function get_class(sch_level,grad_level){
	    $.ajax({
		      url:'<?php echo site_url('student/c_quick_update_student/get_class') ?>',
		      type:'POST',
		      datatype:'JSON',     
		      async:false,
		      data:{
		              'sch_level':sch_level,
		              'grad_level':grad_level
		      },
		      success:function (data){
		          $('#classid').html(data.getclass);
		      },
	    });
	 }
	function clear_data(){
		document.getElementById("program").selectedIndex = "0";
		document.getElementById("gender").selectedIndex="0";
	    $('#sch_level').html('');
	    $('#years').html('');
	    $('#classid').html('');
	    $('#s_student_id').val('');
	    $('#grad_level').html('');
	    $('#s_full_name').val('');
  	}

  	function get_student_list(page, sortby, sorttype){
	  		var url = "<?php echo site_url('student/c_quick_update_student/get_student_list')?>";
	  		var m = "<?PHP echo $m?>";
	    	var p = "<?PHP echo $p?>";
		    var program = $('#program').val();
		    var sch_level = $('#sch_level').val();
		    var years = $('#years').val();
		    var classid = $('#classid').val();
		    var gradlevel = $('#grad_level').val();
		    var gender =$('#gender').val();
		    var perpage = $('#perpage').val();
		    var s_student_id = $('#s_student_id').val();
		    var s_full_name = $('#s_full_name').val();	
		    $.ajax({
	        url: url,
	        type: "POST",
	        datatype: "JSON",
	        async: false,
	        data: {
	            'm': m,
	            'p': p,
	            'page': page,
	            's_sortby': sortby,
	            's_sorttype': sorttype,
	            'program': program,
	            'sch_level':sch_level,
	            'years':years,
	            'classid':classid,
	            'gradlevel':gradlevel,
	            'gender':gender,
	            's_student_id':s_student_id,
	            's_full_name':s_full_name,
	            'perpage': perpage
	        },
	        success: function (data) {
	            if(data.tr_data ==""){
	                var table = '<table class="table table-bordered table-striped table-hover">'+
	                                '<tr>'+
	                                    '<td colspan="11" style="color:red;text-align:center;font-size:14px;"><i>We did not find anything to show here!</i></td>'+
	                                '</tr>'+
	                                '<tr><td colspan="11"></td></tr>'+
	                            '</table>';
	                $('.listbody').html(table);
	            }else{
	                $(".listbody").html(data.tr_data);
	            }                   
	            $('.paginate').html(data.pagina.pagination);
	        }
	    });
  	}

  	function sort(event) {
        this.sortby = $(event.target).attr("rel");
        if ($(event.target).attr("sortype") == "ASC") {
            $(event.target).attr("sortype", "DESC");
            this.sorttype = "ASC";
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass('cur_sort_up');
        } else if ($(event.target).attr("sortype") == "DESC") {
            $(event.target).attr("sortype", "ASC");
            this.sorttype = "DESC";
            $('.sort').removeClass('cur_sort_up');
            $(event.target).addClass('cur_sort_down');
        }

        get_student_list(this.fn, this.sortby, this.sorttype);
    }

	var con_edit = 0;
	function hide_select(event){
		$(event.target).closest('tr').find('.hide_controls').hide();
		$(event.target).closest('tr').find('.data_gender').show();
		con_edit =0;	
	}
	
    function up_gender(event){
    	
    	var studentid =$(event.target).attr('rel');
    	con_edit += $(event.target).closest('tr').find('.hide_edit').length;
    	if(con_edit<=1){
    	$.ajax({
		      url:'<?php echo site_url('student/c_quick_update_student/up_gender') ?>',
		      type:'POST',
		      datatype:'JSON',     
		      async:false,
		      data:{
		              'studentid':studentid
		      },
		      success:function (data){
		      	$(event.target).closest('tr').find('.hide_controls').show();
		      	$(event.target).closest('tr').find('.data_gender').hide();	        
		      },
	    });

	    }
    }

	function save_gender(event){
		var studentid =$(event.target).attr('rel');
		var gender = $(event.target).closest('tr').find('#gender_id').val();
		$.ajax({
                url: "<?= site_url('student/c_quick_update_student/save_gender') ?>",
                dataType: "JSON",
                type: "POST",
                async: false,    
                data: { 
                    'studentid': studentid,
                    'gender': gender
                },
                success: function(data){
                	if(data.insert=="success"){
                		toastr["success"]('Gender was Update!');                		
                		$(event.target).closest('tr').find('.hide_controls').show();
		      			$(event.target).closest('tr').find('.data_gender').hide();
		      			get_student_list(1);
		      			con_edit=0;
                	}
                }
        });
	}

</script>