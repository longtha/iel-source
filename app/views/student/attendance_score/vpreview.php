<style type="text/css">
    #preview td {
        padding: 3px;
    }

    th img {
        border: 1px solid #CCCCCC;
        padding: 2px;
    }

    #preview_wr {
        margin: 10px auto !important;
    }

    .tab_head th, label.control-label {
        font-family: "Times New Roman", Times, serif !important;
        font-size: 14px;
    }

    td {
        font-family: "Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important;
        font-size: 14px;
    }

</style>
<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
        <div id="main_content">
            <div class="row result_info col-xs-12" style="padding-top: 0px">
                <div class="col-xs-6">
                    <strong>Attendance Detail</strong>
                </div>
                <div class="col-xs-6" style="text-align: right">

		      		<span class="top_action_button">
						<a href="javascript:void(0)" id="print" title="Print">
                            <img src="<?php echo base_url('assets/images/icons/print.png') ?>"/>
                        </a>
		      		</span>
                    <span class="top_action_button">
						<a href="javascript:void(0)" id="close" title="Close" onclick="window.close()">
                            <i class="fa fa-times-circle fa-2x"></i>
                        </a>
		      		</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12" id='preview_wr'>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive" id="tab_print">
                    <style type="text/css">
                        #preview td {
                            padding: 3px;
                        }

                        th img {
                            border: 1px solid #CCCCCC;
                            padding: 2px;
                        }

                        #preview_wr {
                            margin: 10px auto !important;
                        }

                        .tab_head th, label.control-label {
                            font-family: "Times New Roman", Times, serif !important;
                            font-size: 14px;
                        }

                        td {
                            font-family: "Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important;
                            font-size: 14px;
                        }

                    </style>
                    <div class="col-sm-12">
                        <div class="col-sm-5">
                            <div style='width:250px; float:left;'>
                                <img src="<?php echo base_url('assets/images/logo/logo.png') ?>" style='width:240px;'/>
                            </div>
                        </div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-5">
                            <div style='float:left;'>
                                <h5 style='margin:15px 5px 0px 0px; font-weight:bold;'><?php echo $this->db->where("schoolid",$this->session->userdata("schoolid"))->get("sch_school_infor")->row()->name; ?></h5>
                                <span style='text-align:center;'><?php echo $this->db->where("schoolid",$this->session->userdata("schoolid"))->get("sch_school_infor")->row()->address; ?></span><br/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12" style="text-align: center">
                        <h4><?php echo isset($rep_title)?$rep_title:"" ?></h4>
                    </div>
                    <p style="clear:both;margin-bottom: 20px;" ></p>

                    <table align='center' id='preview' style='width:100%'>

                        <?php if(isset($attdailyrow)){
                            foreach ($attdailyrow as $attrow){
                                $subject="";
                                if(isset($attrow->subject) && $attrow->baseattid==1 ){
                                    $subject=$attrow->subject;
                                }
                                echo '<tr>
                                    <td>Program</td><td class="bold">: '.$attrow->program.' </td>
                                    <td>School Level </td><td class="bold">: '.$attrow->sch_level.',</td>	
                                    <td>Academic Year</td><td class="bold">: '.$attrow->sch_year.',</td>
                                    <td>Term 	</td><td class="bold">: '.$attrow->term.',</td>
                                </tr>
                                <tr>
                                    
                                    <td>Week 	</td><td class="bold">: '.$attrow->week.',</td>
                                    <td>Date 	</td><td class="bold">: '.$this->green->convertSQLDate($attrow->date).',</td>
                                    <td>Class 	</td><td class="bold">: '.$attrow->class_name.',</td>
                                    <td>Subject</td><td class="bold">: '.$subject.',</td>
                                </tr>';
                            }

                        }
                        ?>
                    </table>
                    <br/>
                    <table align='center' id='previewdet' style='width:100%' class="table table-striped table-bordered table-hover">
                        <?php
                            if(isset($attdailydetrow)){
                                echo '<thead>
                                            <th>#</th>
                                            <th>StudentID</th>
                                            <th>Student Name</th>
                                            <th>Att Type</th>
                                            <th>Present</th>
                                            <th>Permission</th>
                                            <th>(-)Absent</th>
                                            <th>(-)Late</th>
                                            <th>(-)Leave Early</th>
                                            <th>(-)Permission</th>
                                            <th>Total Score</th>
                                        </thead>';
                                $i=1;
                                foreach ($attdailydetrow as $attrowdet){
                                    echo '<tr>
                                            <td>'.$i.'</td>
                                            <td>'.$attrowdet->student_num.'</td>
                                            <td>'.$attrowdet->last_name.' '.$attrowdet->first_name.'</td>
                                            <td>'.$attrowdet->attnote.'</td>
                                            <td class="alright">'.$attrowdet->attend_score.'</td>
                                            <td class="alright">'.$attrowdet->permis_score.'</td>
                                            <td class="alright">'.$attrowdet->minus_absentscore.'</td>
                                            <td class="alright">'.$attrowdet->witdraw_late_point.'</td>
                                            <td class="alright">'.$attrowdet->widrleave_early_point.'</td>
                                            <td class="alright">'.$attrowdet->minus_permscore.'</td>
                                            <td class="alright">'.$attrowdet->full_score.'</td>
                                            </tr>';
                                    $i++;
                                }
                            }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $("#print").on("click", function () {
            gPrint("tab_print", "Evaluation");
        });

    })
</script>

