<?php
	//--------------------------------------------
	if(isset($className) && count($className)>0){
      $cla="";
      $cla="<option attr_price='0' value='0'>Select Class</option>";
      foreach($className as $row){
         $cla.="<option value='".$row->classid."'>".$row->class_name."</option>";
      }
    }

    //---------------------
    $en_teacher='';
	    	$kh_teacher='';
	    	$month=array(array('month'=>'Jan'),
							array('month'=>'Feb'),
							array('month'=>'Mar'),
							array('month'=>'Apr'),
							array('month'=>'May'),
							array('month'=>'Jun'),
							array('month'=>'Jul'),
							array('month'=>'Aug'),
							array('month'=>'Sep'),
							array('month'=>'Oct'),
							array('month'=>'Nov'),
							array('month'=>'Dec')
						);
	    	//$month[]=Array ( [0] => Array ( [month] => 'Feb' ), [1] => Array ( [month] => 'Jan' ) ) ;
	    	if(isset($data->kh_teacher)){
	    		$teacher_en='';
	    		$teacher_kh='';
	    		$teacher_fr='';
	    		$monthtran='';
	    		if($data->forign_teacher>0){
	    			$emp1=$this->db->where('empid',$data->forign_teacher)->get('sch_emp_profile')->row();
	    			$teacher_en=$emp1->last_name.' '.$emp1->first_name;
	    		}
	    		if($data->kh_teacher>0){
	    			$emp2=$this->db->where('empid',$data->kh_teacher)->get('sch_emp_profile')->row();
	    			$teacher_kh=$emp2->last_name.' '.$emp2->first_name;
	    		}
	    		if($data->eng_teach_name){
	    			$emp3=$this->db->where('empid',$data->eng_teach_name)->get('sch_emp_profile')->row();
	    			$teacher_fr=$emp3->last_name.' '.$emp3->first_name;
	    		}
	    		if($data->evaluate_type=='semester'){
	    			$month=$this->evaluate->getmonthclass($data->classid,$data->yearid);
	    		}
	    	}
?>

<div class="wrapper">

	<div class="clearfix" id="main_content_outer">
    
	    <div id="main_content">
        
	      	<div class="row result_info">
		      	<div class="col-xs-3">
		      		<strong id='title'>Evaluate In Formation </strong>
		      	</div>    
		  	</div>
		  	<div class="row" id='tab_print'>
                 <div class="collapse in" id="collapseExample">             
                  	<div class="col-sm-3" style="">
                            <div class="form-group">
                                <label for="schoolid">School Info<span style="color:red">*</span></label>
                                    <select name="schoolid" id="schoolid" class="form-control" data-parsley-required="true" data-parsley-required-message="This field require">
                                    <?php
                                     $opt = '';
                                     // $opt .= '<option value="">All</option>';
                                     foreach ($this->info->getschinfor() as $row) {
                                        $opt .= '<option value="'.$row->schoolid.'">'.$row->name.'</option>';
                                     }
                                     echo $opt;        
                                    ?>                  
                                </select>
                            </div>
                        </div><!-- 1 -->
                        <div class="col-sm-3">
                            <div class="form-group" style="display: none;">
                                <label for="programid">Program<span style="color:red">*</span></label>
                                <select name="programid" id="programid" class="form-control">
                                
                                </select>
                                </div>
                                
                                <div class="form-group">
                                <label for="schlevelid">School Level<span style="color:red">*</span></label>
                                <select name="schlevelid" id="schlevelid" class="form-control schlevelid schlevel_id" data-parsley-required="true" data-parsley-required-message="This field require">
                                <?php
                                 $opt = '';
                                 // $opt .= '<option value=""></option>';
                                 foreach ($this->level->getsch_level_kgp() as $row) {
                                    $opt .= '<option value="'.$row->schlevelid.'">'.$row->sch_level.'</option>';
                                 }
                                 echo $opt;        
                                ?>
                                </select>
                            </div>      
                        
                        </div><!-- 2 -->
                        
                        <div class="col-sm-3">
                        
                        <div class="form-group">
                            <label for="yearid">Year<span style="color:red">*</span></label>
                            <select name="yearid" id="yearid" class="form-control">
                            
                            </select>
                            </div>
                        
                        </div><!-- 3 -->
                        
                         <div class="col-sm-3">
                            
                         <div class="form-group" style="">
                            <label for="examtypeid">Grade Level<span style="color:red">*</span></label>
                            <select class="form-control input-sm grade_levelid" name="grade_levelid" id="grade_levelid" data-examid="0"></select>
                          </div>
                       </div><!-- 4 --> 
                        
                         <!--  ========================================================= ========================================================= ========================================================= ========================================================= --> 
	     
                        
                    <div class="form-group">
                    <div class="col-sm-12">
                    <div class="col-sm-3" style="">
                            <div class="form-group">
                                <label for="schoolid">Class Name<span style="color:red">*</span></label>
                                    <select name="classname" id="classname" class="form-control classname" data-parsley-required="true" data-parsley-required-message="This field require">
                                              
                                </select>
                            </div>
                        </div><!-- 1 -->
                        <div class="col-sm-3">
                           <div class="form-group">
                                <label for="schlevelid">Subject Group</label>
                                <select name="subjectgroup" id="subjectgroup" class="form-control subjectgroup" data-parsley-required="true" data-parsley-required-message="This field require">
                                <?php
                                /* $opt = '';
                                 // $opt .= '<option value=""></option>';
                                 foreach ($this->level->getsch_level() as $row) {
                                    $opt .= '<option value="'.$row->schlevelid.'">'.$row->sch_level.'</option>';
                                 }
                                 echo $opt;     
								 */   
                                ?>
                                </select>
                            </div>      
                        
                        </div><!-- 2 -->
                         <div class="col-sm-3">
                         <div class="form-group" style="">
                          <label for="examtypecode">Exam Type<span style="color:red">*</span></label>
                            <select name="examtypecode" id="examtypecode" class="form-control examtypecode" data-parsley-required="true" data-parsley-required-message="This field require">
                            <?php
                            $opt = '';
                            $opt .= '<option value=""></option>';
                            foreach ($this->e->examtype() as $row) {
                            $opt .= '<option value="'.$row->examtypeid.'">'.$row->exam_test.'</option>';
                            }
                            echo $opt;        
                            ?>
                            </select>
                          </div>
                       </div><!-- 3 --> 
                       
                        <div class="col-sm-3">
                        
                        <div class="form-group">
                            <label for="yearid">Averag Coefficient<span style="color:red">*</span></label>
                            	<input type="text" name="averag" id="averag" class="form-control" tabindex="1">
                            </div>
                        
                        </div><!-- 4 -->
                         <div class="form-group">
                            <div class="col-sm-12"> 
                            
                                    
                                <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="date">Entry Date</label>
                                            <div class="input-group">
                                                <input type="text" name="date" id="date" class="form-control" placeholder="dd/mm/yyyy">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div><!-- 1 -->
                                    
                            	<div class="col-sm-6">
                                    <div class="form-group">
                                    
                                            <div class="form_year" id='form_year' style="display:none">
                                                    <label class="req" for="student_act">Acandemic</label><br>
                                                    <div class="form_semester_dis" id="form_semester_dis"></div>
                                            </div> 
                                            <div class="form_semester" id='form_semester' style="display:none">
                                                    <label class="req" for="student_num">Semester</label><br>
                                                    <div class="form_semester_dis" id="form_semester_dis"></div>
                                            </div>
                                            
                                    		<div class="form_month" id='form_month' style="display:none">
                                            <label for="month">Months:<span class="ismonth" style="color:#00F; padding-left:50px !important; font-size:15px; font-weight:bold"></span></label>
                                           
                                                <div class="isexammonth" id="isexammonth">
                                                
                                                </div>
                                        </div>
                                        </div>
                                    </div><!-- 2 -->
                                    
                                    
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-12" align="center">
                           		<input type="button"  class="btn btn-primary btn-sm save" value="Save"/>
                                <input type="button" class="btn btn-info btn-sm" value="Search" />
                                <input type="button" class="btn btn-calulate btn-sm" value="Calulate" />
                                
                            </div>
                          </div>
                    </div>           
                    </div>
                  </div>
                  
  <!--  ========================================================= ========================================================= ========================================================= ========================================================= --> 
		      	
		    	
         <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Score Entry</h4>
                </div>
                    <div class="panel-body">
                        <div class="form_sep scrollable-table" style="width:100% !important">
                        <table id="tblstdmention" style="width:100% !important;" class="table table-bordered table-striped table-header-rotated table-responsive">
                            	<tbody class="tbody_avg" id="tbody_avg">
                                
                                </tbody>                                
                            </table>
                            
                            <table id="tblstdmention" style="width:100% !important;" class="table table-bordered table-striped table-header-rotated table-responsive">
                            	<tbody class="tbody">
                                
                                </tbody>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
		    </div>
   		</div>
        </div>  
   	</div>
</div>
<style>
.th_head1,.th_head3{ border: 1px solid #ccc;  !important; text-align:center !important}
 #navigation_left{
        display: none;
    }
    #page-wrapper{
        margin:0 0 0 0px !important; ;
    }
	
</style>
<script type="text/javascript">
	$(function(){
		$("#date").datepicker({
            language: 'en',
            pick12HourFormat: true,
            format:'dd-mm-yyyy'
        });
		$('body').delegate('.cl_month', 'click', function(){
			var attr_month=$(this).attr("attr_month");
			$('.ismonth').html(attr_month);
			// alert(attr_month);
		});
		$('body').delegate('.schlevelid_list', 'change', function(){
			 var schlevelid_list = $(this).val();
			 var grade_levelid_list = $(this).closest('tr').find('.grade_levelid_list');
			 get_grade_change_all(grade_levelid_list, schlevelid_list);
			 get_year($('.yearid_list'), '', schlevelid_list);
    	 });
	  
      get_program($('.schlevelid').val());
      get_grade($('.schlevelid').val());       
      get_year($('#yearid'), '', $('.schlevelid').val());
	  
	  
      $('#add_new_row').attr("data-schlevelid", $('.schlevelid').val());
	  
	  $('body').delegate('.grade_levelid', 'change', function(){
		 get_classid($('.schlevelid').val(),$('.grade_levelid').val());
		 
	  });
	  
	  $('body').delegate('#yearid', 'change', function(){
		 FgetSubjgroup($(".schlevel_id option:selected").val(),$("#yearid option:selected").val());
	  });
	  
      $('body').delegate('.schlevelid', 'change', function(){
		  // alert(10);
         if($(this).val() != ''){
            // show =======
            $('#add_new_row').css("visibility", "visible");

            get_program($(this).val());
            get_year($('#yearid'), '', $(this).val());
            //get_grade_change_all($('#grade_levelid'), $('.schlevelid').val());
			
            // set attr school level ======
            $('#add_new_row').attr("data-schlevelid", $(this).val());

            // grid(1, $('#limit_record').val() - 0);

         }else{        
            $('#add_new_row').css("visibility", "hidden");
         } 
		 get_grade($('.schlevelid').val());
		
		 
      });  
	   
	   $('body').delegate('.studentid_click', 'click', function(){ 
	  	 
	  	  var me=$(this).attr('attr_stuid_click');
	  	 FgetVale(me);
		 
	  });
	  
	  
	  $('body').delegate('.btn-info', 'click', function(){ 
	  	 FvSelect();
	  });
	   $('body').delegate('.btn-calulate', 'click', function(){ 
	  	 Fgetallv();
		 // Fcals();
	  });
	  
	  $('body').delegate('.save', 'click', function(){
		 Fgetallv(); 
	  	 FsaveData();
	  });
	  
	 /* $('body').delegate('.scoreentry', 'change', function(){
		  console.log(Fsot());
	  }) ;*/
		
	 $('body').delegate('.examtypecode', 'change', function(){
		 var me =$("#examtypecode option:selected").html();
		 var me_val =$("#examtypecode option:selected").val();
		// alert(me);
		 getm_sem_yea(me_val);
		 if(me=='Montly'){
			 $(".form_month").show(); 
			 $(".form_semester").hide();
			 $(".form_year").hide();	
		 }else  if(me=='Final Semester'){
			$(".form_semester").show();	
			$(".form_month").hide(); 
			$(".form_year").hide();	
		}else  if(me=='Final Grade'){
			$(".form_year").show();	
			$(".form_month").hide();				
			$(".form_semester").hide();  
		}else{
			$(".form_semester,.form_month,.form_year").hide();
		}
	 });
		
	});// end main
	
	function numberDes(a,b) {
		return b-a;
	}

	function Fsot(){
		var attrsc=Array();
		$(".scoreentry").each(function(i){					
			attrsc[i]=$(this).val();
		})
		// attrsc.sort();
		//attrsc.reverse();
		ln=(attrsc.sort(numberDes)).length;
		var me=Array();
		var h=1;
		for(ii=0;ii<ln;ii++){
			 me[ii]=attrsc[ii];
			 // me[ii]=h;
			h++;	
		}
		alert(me);
		// console.log(attrsc);
		return attrsc;
	}
	
	function Fcals(){
				
				var trs='';
				var defsc=10;
				for (ii=1; ii<=5; ii++){
						trs+='<tr><td style="text-align:right"><input type="text" value="'+(defsc-ii)+'" class="scoreentry" id="scoreentry"></td><td class="rankingline" id="rankingline">'+ii+'</td></tr>';
				}
				$(".tbody_avg").html(trs);
	}
	
	function Fgetallv(){
		$(".ch_pro").each(function(){
			var mes=$(this).val();
			FgetVale(mes);
		})	
	}
	
   function FgetVale(this_stuid){
	   var averag=$("#averag").val()-0;
	   var total=0;
	   $('.'+this_stuid).each(function(){
		   total+=$(this).val()-0;
		   var tottotal=total;
		   if(averag!=0 || averag!=""){
			   tottotal=(total/averag);
			}
		   $(this).parent().parent().find('.score_total_line').html(tottotal);
		   
	   });
	  //  alert(total);
	}
	function getm_sem_yea(is_mes_year){
		 $.ajax({
         url: '<?= site_url('student/c_evaluate_moyes_kgp/Fgetm_s_y') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            is_mes_year: is_mes_year,
			schoolid:$(".schoolid").val(),
			schlevelid:$(".schlevelid").val(),
			yearid:$("#yearid").val()                       
         },
         success: function(data){
			// alert(data.is_mes_year);
			if(data.is_mes_year==1){
				 $('.isexammonth').html(data.tr);
			}else{
				 $('.form_semester_dis').html(data.tr);
			}
          
         },
         error: function(){
				alert('Exam Type Can not blank');
         }
      });
	}
   function get_year(mySelector, programid = '', schlevelid = ''){
      $.ajax({
         url: '<?= site_url('school/c_setupexam/get_year') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            programid: programid,
            schlevelid: schlevelid                                  
         },
         success: function(data){
            mySelector.html(data.opt);
			// alert($(".schlevel_id option:selected").val());
			FgetSubjgroup($(".schlevel_id option:selected").val(),$("#yearid option:selected").val());
         },
         error: function() {

         }
      });
   }
	function get_grade(schlevelid){      
      $.ajax({
         url: '<?= site_url('school/c_setupexam/get_grade') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
            $('#add_new_row').css("visibility", "hidden");
         },
         data: {
            schlevelid: schlevelid                
         },
         success: function(data){
            $('#grade_levelid').html(data.opt);
            $('#add_new_row').css("visibility", "visible");
			get_classid($('.schlevelid').val(),$('.grade_levelid').val());
			
         },
         error: function() {

         }
      });         
   }
	  // get program ======
	 function get_program(schlevelid){ 
	 // alert(10);     
      $.ajax({
         url: '<?= site_url('school/c_setupexam/get_program') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            schlevelid: schlevelid                
         },
         success: function(data){
            $('#programid').html(data.opt); 

         },
         error: function() {

         }
      });         
   }
// get grade change all ======
   function get_grade_change_all(mySelector, schlevelid){      
      $.ajax({
         url: '<?= site_url('school/c_setupexam/get_grade') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            schlevelid: schlevelid                
         },
         success: function(data){
            mySelector.html(data.opt);

         },
         error: function() {

         }
      });         
   }

   // get grade with search ======
   function get_grade_with_search(mySelector, schlevelid){      
      $.ajax({
         url: '<?= site_url('school/c_setupexam/get_grade') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            schlevelid: schlevelid                
         },
         success: function(data){
            mySelector.html(data.opt);
            mySelector.prepend('<option value="" selected="selected"></option');

         },
         error: function() {

         }
      });         
   }
   
	function changemonth(event){
		var val=$('#evaluate_type').val();
		// alert(val);
		if(val=='month'){
			$('#semester').addClass('hide');
			$('#semesters').addClass('hide');
			$('#sep_months').removeClass('hide');
		}
		if(val=='semester'){
			$('#semesters').removeClass('hide');
			$('#semester').addClass('hide');
			$('#sep_months').removeClass('hide');
		}
		if(val=='Three Month'){
			$('#semester').removeClass('hide');
			$('#semesters').addClass('hide');
			$('#sep_months').addClass('hide');
		}
	}

	function getMonthbc(yearid, schlevelid, classname,examtypecode,cl_tmap){
		$.ajax({
			url:"<?php echo site_url('student/c_evaluate_moyes_kgp/getClassbygradeid_') ?>",
				data: {'yearid':yearid,'schlevelid':schlevelid,'classname':classname,'examtypecode':examtypecode,'cl_tmap':cl_tmap},
				type:"post",
				success: function(data){
				$('#isexammonth').html(data);
				}
			});
		}
   
	
	function get_classid(schlevelid,gradeid){
		$.ajax({
	                url:"<?php echo site_url('student/c_evaluate_moyes_kgp/getClassbygradeid') ?>",
	                data: {'schlevelid':schlevelid,'gradeid':gradeid},
	                type:"post",
	                success: function(data){
	                	$('#classname').html(data);
	                }
	    	});

	}

	
	function FgetSubjgroup(schlevelid,yearid){
		$.ajax({
	                url:"<?php echo site_url('student/c_evaluate_moyes_kgp/getSubid') ?>",
	                data: {'schlevelid':schlevelid,'yearid':yearid},
	                type:"post",
	                success: function(data){
	                	$('#subjectgroup').html(data);
	                }
	    	});

	}
		
function getmonthclass(event){
		var classid=$('#classname').val();
		var yearid=$('#year').val();
		var evaluate_type=$('#evaluate_type').val();
		getteacher(classid);
		if(evaluate_type=='semester'){
			$.ajax({
	                url:"<?php echo site_url('student/c_evaluate_moyes_kgp/getmonthclass') ?>",
	                data: {'classid':classid,'yearid':yearid},
	                type:"post",
	                success: function(data){
	                	$('#months').html(data);
	                }
	    	});
		}else{
			$.ajax({
	                url:"<?php echo site_url('student/c_evaluate_moyes_kgp/getmonth') ?>",
	                data: {'classid':classid,'yearid':yearid},
	                type:"post",
	                success: function(data){
	                	$('#months').html(data);
	                }
	    	});
		}

	}
	function getteacher(classid){
		$.ajax({
	                url:"<?php echo site_url('student/c_evaluate_moyes_kgp/getteacher') ?>",
	                data: {'classid':classid},
	                type:"post",
	                success: function(data){
	              		$('#listteacher').html(data);
	            	}
	       });
		// FvSelect();
		
		$('body').delegate("#tbn_serch", "click", function() {
			FvSelect();
		});
		
		$('body').delegate("#tbn_save", "click", function() {
			
			FsaveData();
		});
		
	}
	
//==========================================================================
function FsaveData(){	
			var schoolid=$(".schoolid").val();
			var attr_student = Array();				
			$(".ch_pro:checked").each(function(i){
				var arr = $(this).val();
				var this_val = arr.split("###");
				var tr = $(this).parent().parent();
				var this_ched_attr = $(this).attr('arr_subjid').split("###");
				var unitqty = tr.find("#unit_qty").val();
				var absenty_with_permison = tr.find(".absenty_with_permison").val();
				var score_total_line = tr.find(".score_total_line").html();
				var absenty_total = tr.find(".absenty_total").val();
				
				var studenid = $(this).val();
				var arr_subjid = Array();
			
				for(var ii=0;ii < this_ched_attr.length-1;ii++){					
					arr_subjid[ii]= {"stuidline":studenid,"enterscore":tr.find(".score"+studenid+"____"+this_ched_attr[ii]).val(),"subjectid":tr.find(".subj_id"+studenid+"____"+this_ched_attr[ii]).val(),"subjtypeid":tr.find(".score"+studenid+"____"+this_ched_attr[ii]).attr("subjtypeid")};
					
				}
				attr_student[i] ={"studen_code":studenid,"arr_subjid":arr_subjid,"absenty_total":absenty_total,"absenty_with_permison":absenty_with_permison,"score_total_line":score_total_line};
			});
			//==========================================================================
			// console.log(attr_student);
			$.ajax({
				type:"post",
				url:"<?php echo base_url(); ?>index.php/student/c_evaluate_moyes_kgp/Fsave_Data",
				data:{
					save_data  : 1,
					attr_student:attr_student,
					schoolid:$(".schoolid").val(),
					classname:$(".classname").val(),
					date:$(".date").val(),
					schlevelid:$(".schlevelid").val(),
					subjectgroup:$(".subjectgroup").val(),
					yearid:$(".yearid").val(),
					examtypecode:$(".examtypecode").val(),
					grade_levelid:$(".grade_levelid").val(),
					averag:$(".averag").val()
				},
			    success: function(result){
					toastr["success"]('Record has been save!');
				// FsearchList();
			  }
			  
		});
		// fn_reload_img();
	}
	
  function FvSelect(){
	  	var deltypeno=1;
		var subjectgroup=$(".subjectgroup").val();
		var classname=$(".classname").val();
		
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/student/c_evaluate_moyes_kgp/Fcstu_inclass",   
				data: {					
					'subjectgroup':subjectgroup,
					'classname':classname
					},
				type:"post",
				dataType:"json",
				async:false,
				success: function(data){					
					toastr["success"]('Record has been Search!');
					$(".tbody").html(data.tr_mesure);
					// alert(deltypeno);
					// $(".cl_btncancel").click();
					Fgetallv();
			}
	   });	
	}
</script>