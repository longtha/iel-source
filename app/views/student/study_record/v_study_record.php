<div class="container-fluid" style="width:100% !important;padding-bottom: 20px;">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
              <span class="icon">
                  <i class="fa fa-th"></i>
              </span>
                <strong>Student's Study Record</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                  <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
               </a>
               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
               </a>
               <!-- <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
               </a> -->
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse in" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" id="f_save">

         <div class="col-sm-3">
            <div class="form-group">
               <label for="student_num">Student ID<span style="color:red"></span></label>
               <input type="text" name="student_num" id="student_num" class="form-control input-sm" placeholder="Student ID">
            </div>

            <div class="form-group">
               <label for="programid">Program<span style="color:red"></span></label>
               <select name="programid" id="programid" class="form-control input-sm">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->p->getprograms() as $row) {
                        $opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
                     }
                     echo $opt;        
                  ?>
               </select>
            </div>            
            
         </div><!-- 1 -->

         <div class="col-sm-3">
            <div class="form-group">
               <label for="full_name">Full Name<span style="color:red"></span></label>
               <input type="text" name="full_name" id="full_name" class="form-control input-sm" placeholder="Full Name">
            </div>

            <div class="form-group">
               <label for="schlevelid">School Level<span style="color:red"></span></label>
               <select name="schlevelids" id="schlevelids" class="form-control input-sm">

               </select>
            </div>
         </div><!-- 2 -->   

         <div class="col-sm-3">
            <div class="form-group">
               <label for="full_name_kh">Khmer Name<span style="color:red"></span></label>
               <input type="text" name="full_name_kh" id="full_name_kh" class="form-control input-sm" placeholder="Khmer Name">
            </div>

            <div class="form-group">
               <label for="yearid">Year<span style="color:red"></span></label>
               <select name="yearid" id="yearid" class="form-control input-sm">

               </select>
            </div>
         </div><!-- 3 -->

         <div class="col-sm-3">
            <div class="form-group" style="">
               <label for="card_type">Book Type<span style="color:red"></span></label>
               <select name="card_type" id="card_type" class="form-control input-sm">
                  <option value="1">English</option>
                  <option value="2">Khmer</option>
               </select>
            </div>

            <div class="form-group">
               <label for="classid">Class<span style="color:red"></span></label>
               <select name="classid" id="classid" class="form-control input-sm">

               </select>
            </div>            
         </div><!-- 3 -->                 

         <div class="col-sm-7 col-sm-offset-5">
            <div class="form-group">
               <button type="button" class="btn btn-primary btn-sm btn_search" name="btn_search" id="btn_search">Search</button>
               <button type="button" class="btn btn-warning btn-sm" name="btn_clear" id="btn_clear" style="display: none;">Clear</button>
            </div>            
         </div>

      </form>      
      <div class="form-group">
         <div class="col-sm-12">
            <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
         </div>           
      </div>
   </div>
   
   <div class="form-group">
      <div class="col-sm-12">
         <div class="table-responsive">
            <table border="0"​ cellspacing="5" cellpadding="0" style="width: 100%;">
               <tr>
                  <td align="center">
                     <div id="tab_print" style="width: 20cm;border: 0;height: 29.8cm;display: inline-block;">
                        <table border="0"​ cellspacing="0" cellpadding="0" align="right" id="list_data" class="table-condensed" style="width: 88%;font-family: khmer mef1;font-size: 16px;margin-right: 25px;">
                           <tbody>
                              
                           </tbody>
                        </table>
                     </div>
                  </td>
               </tr>
            </table>
         </div><!-- responsive -->
      </div>
   </div><!-- row -->

   <div class="form-group">
      <!-- <div class="col-sm-4">&nbsp;</div> -->
      <div class="col-sm-1">
         <select class="" name="limit_record" id="limit_record" data-toggle="tooltip" data-placement="left" title="Display" style="margin-top: 10px;">
            <option value="1">1</option>
            <option value="5">5</option>            
            <option value="10">10</option>
            <option value="30">30</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="200">200</option>
            <option value="300">300</option>
            <option value="500">500</option>
            
         </select>
      </div>
      <div class="col-sm-2" style="margin-top: 8px !important;">
         <div id="sp_page" class="btn-group pagination btn-group-xs" role="group" aria-label="..." style="display: inline;"></div>
      </div>
   </div>

</div>

<style type="text/css">
  #list_data th {vertical-align: middle;text-align: center;}
  #list_data td {vertical-align: middle;}

  .xmodal {
      display: none;
      position: fixed;
      z-index: 2000;
      top: 5%;
      left: 50%;
      height: 5%;
      width: 5%;
      /* background: rgba( 255, 255, 255, .8 ) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat; */
      background: rgba(255, 255, 255, 0) url(<?= base_url('assets/images/FhHRx.gif') ?>)  50% 50% no-repeat;
      cursor: progress !important;
  }

  /* When the body has the loading class, we turn
          the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
          modal element will be visible */
  body.loading .xmodal {
      display: inline;
      cursor: progress !important;
  }

</style>

<div class="xmodal"></div>

<script type="text/javascript">
   $(function(){

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // show/hide =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
      });

      // tooltip ======
      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // print =======
      $('body').delegate('#a_print', 'click', function(){
         var title="<h4 align='center' style='display: none;'>"+ "Cards" +"</h4>";
         var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "")
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint_book(title, export_data);
      });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Cards";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "")
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });      

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#examtypeid').select();
         $('#save').html('Save');
         $('#save_next').show();
      });

      // init. =====
      //grid(1, $('#limit_record').val() - 0);

      // search ========
      $('body').delegate('#limit_record', 'change', function(){
         grid(1, $(this).val() - 0);
      });
      $('body').delegate('#btn_search', 'click', function(){
         grid(1, $('#limit_record').val() - 0);
      });

      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#limit_record').val() - 0);
      });

      // get ===================

      // program ===========
      // get_class($('#schlevelids').val());
      // get_schlevel($('#schlevelids').val());
      // get_year($('#programid').val(), $('#schlevelids').val());
      $('body').delegate('#programid', 'change', function() {
         get_schlevel($(this).val());
         get_class($('#schlevelids').val());

         if($(this).val() - 0 == 1){
            $('#card_type').val(2);
         }else{
            $('#card_type').val(1);
         }
      });
      // school level ======    
      $('body').delegate('#schlevelids', 'change', function() { 
         var programid = $('#programid').val();
         var schlevelid = $(this).val();         
         get_year(programid, schlevelid);

         get_class(schlevelid);
      });


      $('body').delegate('#clear', 'click', function() {
         clear();
         grid(1, $('#limit_record').val() - 0);
      });

   }); // ready =======

   // get school level ========
   function get_schlevel(programid = ''){
      $.ajax({
         url: '<?= site_url('student/c_study_record/get_schlevel') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            programid: programid
         },
         success: function(data){
            $('#schlevelids').html(data.opt);

            // get year =====
            get_year($('#programid').val(), $('#schlevelids').val());
         },
         error: function() {

         }
      });
   }

   // get year ========
   function get_year(programid = '', schlevelid = ''){
      $.ajax({
         url: '<?= site_url('student/c_study_record/get_year') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            programid: programid,
            schlevelid: schlevelid
         },
         success: function(data){
            $('#yearid').html(data.opt);
         },
         error: function() {

         }
      });
   }

   // get class ========
   function get_class(schlevelid = ''){
      $.ajax({
         url: '<?= site_url('student/c_study_record/get_class') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            schlevelid: schlevelid           
         },
         success: function(data){
            $('#classid').html(data.opt);
         },
         error: function() {

         }
      });
   }

   function clear(){
      $('#student_num').val('');
      $('#full_name').val('');
      $('#full_name_kh').val('');
   }

   // grid =========
   function grid(current_page = 0, total_display = 0){
      var offset = ((current_page - 1) * total_display) - 0;
      var limit = total_display - 0;

      $.ajax({
         url: '<?= site_url('student/c_study_record/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
            $('.xmodal').show();
         },
         data: {
            offset: offset,
            limit: limit
            ,
            student_num: $('#student_num').val(),
            full_name: $('#full_name').val(),
            full_name_kh: $('#full_name_kh').val(),
            card_type: $('#card_type').val(),
            programid: $('#programid').val(),
            schlevelid: $('#schlevelids').val(),            
            yearid: $('#yearid').val(),
            classid: $('#classid').val()

         },
         success: function(data) {            
            $('#list_data tbody').html(data.tr);

            var page = '';
            var total = '';

            if(data.totalRecord - 0 > 0){
               // previous ====
               page += '<button type="button" class="btn btn-default btn-sm a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-sm a-pagination" data-current_page="' + (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next<i class="glyphicon glyphicon-chevron-right"></i></button>';
               
            }
            
            $('.pagination').html(page);

            $('.xmodal').hide();
         },
         error: function(){

         }
      });
   }
</script>