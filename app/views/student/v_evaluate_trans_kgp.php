<div class="wrapper">
 	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
    		<div class="result_info">
                <div class="col-sm-6">
                    <strong>Evaluation for Semester Form</strong>                    
                </div>
            </div>
			<!--form element-->
	      	<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" class="tdrow" id="frm_evaluate_kgp_tran">
	      		<!--form search of student information-->
	      		<div class="col-sm-12">
		        	<div class="row">
		        		<div class="panel panel-default">
		        			<div class="panel-body">
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_id">School</label>
					        			<select name="school_id" id="school_id" minlength='1' class="form-control">
					                    	<?php
												$row = $this->school->getschinfor_row(1);											
												echo '<option value="'. $row->schoolid .'">'. $row->name .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="program_id">Study Program</label>
					        			<select name="program_id" id="program_id" minlength='1' class="form-control">
					                    	<?php
												$row = $this->program->getprogram(1);											
												echo '<option value="'. $row->programid .'">'. $row->program .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_level_id">Study Level<span style="color:red">*</span></label>
					        			<select name="school_level_id" id="school_level_id" minlength='1' class="form-control" data-parsley-required="true" data-parsley-required-message="This field require">
		                                    <option value=""></option>
					                    	<?php
												$get_level_data = $this->school_level->getsch_level(1);												
												foreach($get_level_data as $r_level){
													echo '<option value="'. $r_level->schlevelid .'">'. $r_level->sch_level .'</option>';
												}
											?>	                      
		                                </select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="academic_year_id">Academic Year<span style="color:red">*</span></label>
	                                    <select name="academic_year_id" id="academic_year_id" minlength='1' class="form-control" data-parsley-required="true" data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="grade_level_id">Grade Level<span style="color:red">*</span></label>
	                                    <select name="grade_level_id" id="grade_level_id" minlength='1' class="form-control" data-parsley-required="true" data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>					        	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="class_id">Class Name<span style="color:red">*</span></label>					        			
	                                    <select name="class_id" id="class_id" minlength='1' class="form-control" data-parsley-required="true" data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>					        					        	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="student_id">Student Name</label>
	                                    <select name="student_id" id="student_id" class="form-control"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label class="req" for="student_num">Semester<span style="color:red">*</span></label><br>
					        			<div class="form_semester" id='form_semester' style="text-align:center; border:1px #ccc solid; padding:5px 0 8px 0;">                                            
                                            <label class="radio-inline">
                                                <input type="radio" checked="checked" name="semester" value='1' class='semester'>1st
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="semester" value='2' class='semester'>2nd
                                            </label>
	                                    </div>	                                    
					        		</div>
					        	</div>
					        	
		        			</div>
		        		</div>
		        	</div>
		        </div><!--end form search of student information-->
		        
		        <!--button-->
		        <div class="col-sm-12" style="margin-top:-12px;">	
		        	<div class="row">
		        		<div class="form-group" style="text-align:center;">
			                <?php if($this->green->gAction("R")){ ?>
			                	<input type="button" name="btnsearch" id='btnsearch' value="Search" class="btn btn-primary" />
			                <?php } ?>
			                <input type="button" name="btncancel" id='btncancel' value="Cancel" class="btn btn-warning" />
		                </div> 
		        	</div>	                                           
		        </div><!--end button-->

		        <!--show student form detail-->
		        <div class="col-sm-12 show_student_detail" style="margin-top:-5px; display:none;">
		        	<div class="row">
		        		<div class="panel panel-default">
		        			<div class="panel-body">
		        				<div class="result_info">
					                <div class="col-sm-6">
					                    <strong>Student Information</strong>                    
					                </div>
					            </div>
					            <!--table detail-->
					            <div class="table-responsive">							
									<table border="0"​ align="center" id='listsubject' class="table table-bordered">
									    <thead class="theads">
									    	<tr>								        
										        <th>ទំលាប់ការសិក្សានិងជំនាញសង្គម <br> (Work Habits and Social Skills)</th>
										        <th width="90">កម្រ <br> (Seldom) <br> <input type="checkbox" id="all_seldom" class="all_seldom check_all"></th>
										        <th width="90">ម្ដងម្កាល <br> (Sometimes) <br> <input type="checkbox" id="all_sometimes" class="all_sometimes check_all"></th>
										        <th width="90">តែងតែ <br> (Usuall) <br> <input type="checkbox" id="all_usuall" class="all_usuall check_all"></th>
										        <th width="90">ជាប្រចាំ <br> (Consistently) <br> <input type="checkbox" id="all_consistenly" class="all_consistenly check_all"></th>										        
										    </tr>
									    </thead>
							    		<tbody class="tbodys">
							    			
							    		</tbody>
									</table>
								</div><!--end table detail-->

								<!--button save record-->
						        <div class="col-sm-12" style="margin:-8px 0 -15px 0;">	
						        	<div class="row">
						        		<div class="form-group" style="text-align:right;">
							                <?php if($this->green->gAction("C")){ ?>
							                	<input type="button" name="btnsave" id='btnsave' value="Save" class="btn btn-primary" />
							                <?php } ?>
						                </div> 
						        	</div>	                                           
						        </div><!--end button save record-->
		        			</div>
			        	</div>
		        	</div>	        	
		        </div><!--end show student form detail-->		        
	      	</form>
	  	</div>
	</div>
</div>

<style>
	th, td{
		text-align: center;
		vertical-align: middle !important;
	}
</style>

<script type="text/javascript">
	$(function(){
		$('#year, #schlevelid').hide();

		//get year and class data---------------------------------
		$('#school_level_id').change(function(){
			var school_level_id = $(this).val();
			var school_id = $('#school_id').val();
			var program_id = $('#program_id').val();
			
			if(school_level_id != ""){
				$.ajax({
		            url: "<?php echo site_url('student/c_evaluate_trans_kgp/get_year_and_class_data') ?>",	            
		            type: "post",
		            dataType: 'json',
		            data: {'school_id': school_id, 'program_id': program_id, 'school_level_id': school_level_id},
		            success: function (data) {
		            	//console.log(data);
		            	//get year--------------------------------
		            	var getYear = '';	            	
		            	$.each(data["year"], function(k,v){
		            		getYear += '<option value="'+ v.yearid +'">'+ v.sch_year+'</option>';		            		
		            	}); $('#academic_year_id').html(getYear);

		            	//get grade--------------------------------
		            	var getGrade = '';
		            	$.each(data["grade"], function(ke,re){
		            		getGrade += '<option value="'+ re.grade_levelid +'">'+ re.grade_level+'</option>';
		            	});	$('#grade_level_id').html(getGrade);

		            	//get class--------------------------------
		            	var grade_level_id = $('#grade_level_id').val();
		            	var url = "<?php echo site_url('student/c_evaluate_trans_kgp/get_class_data') ?>";
						var data = 'grade_level_id='+grade_level_id;
						getDataByAjax($('#class_id'), url, data);

						//remove message require-------------------
						var aId = $('#academic_year_id').data('parsley-id');
						var gId = $('#grade_level_id').data('parsley-id');

						$('#parsley-id-'+aId).remove();	   
						$('#academic_year_id').removeClass('parsley-error');

						$('#parsley-id-'+gId).remove();	   
						$('#grade_level_id').removeClass('parsley-error');        	
				    }
		        });	
			}else{
				$('#academic_year_id').html('');
				$('#grade_level_id').html('');
				$('#class_id').html('');
				$('#student_id').html('');	
			}
		});

		//get class data--------------------------------------------
		$('#grade_level_id').change(function(){
			var grade_level_id = $(this).val();
			if(grade_level_id == ""){
				$('#class_id').html('');
			}else{
				var url = "<?php echo site_url('student/c_evaluate_trans_kgp/get_class_data') ?>";
				var data = 'grade_level_id='+grade_level_id;
				getDataByAjax($('#class_id'), url, data);
			}
		});

		//get student data------------------------------------------
		$('#class_id').change(function(){
			var class_id = $(this).val();
			if(class_id == ""){
				$('#student_id').html('');				
			}else{
				var url = "<?php echo site_url('student/c_evaluate_trans_kgp/get_student_data') ?>";
				var data = 'class_id='+class_id;
				getDataByAjax($('#student_id'), url, data);	
			}
		});

		//search report---------------------------------------------
		$('#btnsearch').click(function(){
			var program_id = $('#program_id').val();		
			var school_level_id = $('#school_level_id').val();			
			var academic_year_id = $('#academic_year_id').val();
			var class_id = $('#class_id').val();
			var semester = $('.semester:checked').val();
			var student_id = $('#student_id').val();

			$('.tbodys').html('');

			if($('#frm_evaluate_kgp_tran').parsley().validate()){
				$.ajax({
		            url:"<?php echo site_url('student/c_evaluate_trans_kgp/get_student_information') ?>",	            
		            type: "post",
		            dataType: 'json',
		            data: {'program_id': program_id,'school_level_id': school_level_id, 'academic_year_id':academic_year_id, 'class_id':class_id, 'student_id':student_id, 'semester':semester},
		            success: function (data) {
		            	//console.log(data[0]);
		            	$('.show_student_detail').show();
		            	$('.check_all').prop('checked', false);
		            	
		            	var tr = '';
		            	var default_comment = 'ប្អូនជាសិស្ស ដែលគោរពវិន័យសាលាបានល្អ។ឯការសិក្សាវិញបានលទ្ធផលល្អបង្គួរ ។ប្អូនត្រូវប្រឹងបន្ថែមទៀតដូចជាត្រូវធ្វើស្វ័យសិក្សាបន្ថែមនៅផ្ទះ។';
		            	$.each(data[0], function(k,v){
		            		tr += '<tr>'+
		            				'<td colspan="5" style="text-align:left !important; color: #fff; font-weight:600; background:#31708f;">'+           					 
		            					'<span style="color:#eea236 !important;">Name: </span>'+
		            					'<span>'+ v.last_name +'</span>'+
		            					'<span style="margin-left:15px;">'+ v.first_name +'</span>'+
		            					'<span style="margin-left:5px;">('+ (v.gender == "female" ? "F" : "M") +'), </span>'+
		            					'<span style="margin-left:10px; color:#eea236 !important;">ID: </span>'+
		            					'<span>'+ v.student_num +'</span>'+
		            				'</td>'+
		            			  '</tr>';
		            		
		            		var get_comment = "";
		            		$.each(v.evaluate, function(ke,ve){
		            			var che_seldom = ''; 
		            			var che_sometime = '';
		            			var che_usuall = '';
		            			var che_consistenly = '';

		            			if(ve.comment != ""){
		            				get_comment = ve.comment
		            			}            			
		            			if(ve.seldom == 1){
		            				che_seldom = 'checked';
		            			}
		            			if(ve.sometimes == 1){
		            				che_sometime = 'checked';
		            			}
		            			if(ve.usuall == 1){
		            				che_usuall = 'checked';
		            			}
		            			if(ve.consistenly == 1){
		            				che_consistenly = 'checked';
		            			}		            					            					            			
		            			tr += '<tr>'+
		            					'<td style="padding-left:30px; text-align:left !important;">'+ ve.evaluate_name_kh +' ('+ ve.evaluate_name_eng +')'+
		            					'<input type="hidden" name="student_id[]" class="student_id" value="'+ v.studentid +'">'+
		            					'<input type="hidden" name="evaluate_id[]" class="evaluate_id" value="'+ ve.evaluate_id +'">'+		            					
		            					'<input type="hidden" name="evaluate_tran_id[]" class="evaluate_tran_id" value="'+ (ve.evaluate_tran_id>0?ve.evaluate_tran_id:"") +'">'+
		            					'<textarea name="comment[]" class="comment comment_'+ v.studentid +'" style="display:none">'+ (ve.comment==""||ve.comment==null||ve.comment==undefined?default_comment : get_comment) +'</textarea></td>'+
		            					'<td><input type="checkbox" name="seldom[]" class="check_list seldom" value="'+ (ve.seldom>0?ve.seldom:0) +'" '+che_seldom+'></td>'+
		            					'<td><input type="checkbox" name="sometimes[]" class="check_list sometimes" value="'+ (ve.sometimes>0?ve.sometimes:0) +'" '+che_sometime+'></td>'+
		            					'<td><input type="checkbox" name="usuall[]" class="check_list usuall" value="'+ (ve.usuall>0?ve.usuall:0) +'" '+che_usuall+'></td>'+
		            					'<td><input type="checkbox" name="consistenly[]" class="check_list consistenly" value="'+ (ve.consistenly>0?ve.consistenly:0) +'" '+che_consistenly+'></td>'+
		            				  '</tr>';
		            		});
							
							tr += '<tr>'+
										'<td colspan="5" style="text-align:left !important; color:#31708f;">'+           					 
			            					'<textarea class="main_comment" data-st-id="'+ v.studentid +'" style="margin:0; padding:0; width:950px; height:40px; border:hidden;">'+ (get_comment==""||get_comment==null||get_comment==undefined?default_comment : get_comment) +'</textarea>'+
			            				'</td>'+
								  '</tr>';
		            	});
		            	$('.tbodys').append(tr);

		            	//check seldom
		            	var count_sel_all = $('.seldom').size();
		            	$('.seldom').each(function(){
		            		var count_sel_one = $('.seldom:checked').size();
		            		if(count_sel_all == count_sel_one){
		            			$('#all_seldom').prop('checked', true);
		            		}
		            	});
		            	//check sometime 
		            	var count_some_all = $('.sometimes').size();
		            	$('.sometimes').each(function(){
		            		var count_some_one = $('.sometimes:checked').size();
		            		if(count_some_all == count_some_one){
		            			$('#all_sometimes').prop('checked', true);
		            		}
		            	});
		            	//check usuall 
		            	var count_usu_all = $('.usuall').size();
		            	$('.usuall').each(function(){
		            		var count_usu_one = $('.usuall:checked').size();
		            		if(count_usu_all == count_usu_one){
		            			$('#all_usuall').prop('checked', true);
		            		}
		            	});
		            	//check consistenly 
		            	var count_con_all = $('.consistenly').size();
		            	$('.consistenly').each(function(){
		            		var count_con_one = $('.consistenly:checked').size();
		            		if(count_con_all == count_con_one){
		            			$('#all_consistenly').prop('checked', true);
		            		}
		            	});          		            	
		            }
		        });
			}			
		});

		//can check only one for class check all-------------------
		$('.check_all').on('click', function(){
			$('input:checkbox').not(this).prop('checked', false);
			$('.seldom').removeClass('count_seldom').prop('value', 0);
			$('.sometimes').removeClass('count_sometimes').prop('value', 0);
			$('.usuall').removeClass('count_usuall').prop('value', 0);
			$('.consistenly').removeClass('count_consistenly').prop('value', 0);		
		});

		//can check only one for class check list------------------
		$('.tbodys').delegate('.check_list', 'click', function(){
			var tr = $(this).parent().parent();
			tr.find('input:checkbox').not(this).prop('checked', false).prop('value', 0);
			tr.find('input:checkbox').not(this).removeClass('count_seldom');
			tr.find('input:checkbox').not(this).removeClass('count_sometimes');
			tr.find('input:checkbox').not(this).removeClass('count_usuall');
			tr.find('input:checkbox').not(this).removeClass('count_consistenly');

			$('.check_all').removeAttr('checked');
		});

		//change comment-------------------------------------------
		$('.tbodys').delegate('.main_comment', 'change', function(){
			var getText = $(this).val();
			var getId = $(this).data('st-id');
			$('.comment_'+getId).text(getText);
		});

		//event check all------------------------------------------
		$('#all_seldom').on('click', function(){
			checkAll($(this),$('.seldom'), 'count_seldom');
		});
		$('#all_sometimes').on('click', function(){
			checkAll($(this),$('.sometimes'), 'count_sometimes');
		});
		$('#all_usuall').on('click', function(){
			checkAll($(this),$('.usuall'), 'count_usuall');
		});
		$('#all_consistenly').on('click', function(){
			checkAll($(this),$('.consistenly'), 'count_consistenly');
		});		

		//event check one by one-----------------------------------
		$('.tbodys').delegate('.seldom','click', function(){
			checkOne($('#all_seldom'), $(this), $('.seldom'), 'count_seldom');
		});
		$('.tbodys').delegate('.sometimes','click', function(){
			checkOne($('.all_sometimes'), $(this), $('.sometimes'), 'count_sometimes');
		});
		$('.tbodys').delegate('.usuall','click', function(){
			checkOne($('.all_usuall'), $(this), $('.usuall'), 'count_usuall');
		});
		$('.tbodys').delegate('.consistenly','click', function(){
			checkOne($('.all_consistenly'), $(this), $('.consistenly'), 'count_consistenly');
		});

		//save data------------------------------------------------
		$('#btnsave').on('click', function(){
			var class_id = $('#class_id').val();
			var academic_year_id = $('#academic_year_id').val();
			var grade_level_id = $('#grade_level_id').val();
			var school_level_id = $('#school_level_id').val();
			var program_id = $('#program_id').val();
			var semester = $('.semester:checked').val();						
			var student_array = [];

			$('.student_id').each(function(i){							
				var tr = $(this).parent().parent();
				var student_id = $(this).val();				
				var evaluate_id = tr.find('.evaluate_id').val();
				var evaluate_tran_id = tr.find('.evaluate_tran_id').val();
				var seldom = tr.find('.seldom').val();
				var sometimes = tr.find('.sometimes').val();
				var usuall = tr.find('.usuall').val();
				var consistenly = tr.find('.consistenly').val();
				var comment = tr.find('.comment').text();
				if(student_id != ""){				
					student_array[i] = {
											'student_id': student_id,
											'evaluate_id':evaluate_id,
											'evaluate_tran_id':evaluate_tran_id,
										 	'seldom':seldom,
										 	'sometimes':sometimes,
										 	'usuall':usuall,
										 	'consistenly':consistenly,
										 	'comment':comment
										};
				}	    		
			});
			//console.log(student_array); return false;
			$.ajax({
	            url: "<?php echo site_url('student/c_evaluate_trans_kgp/saveStudentEvaluate');?>",
	            dataType: "Json",
	            type: "POST",
	            async: false,
	            data: {
	                'class_id': class_id,
	                'academic_year_id': academic_year_id,
	                'grade_level_id': grade_level_id,
	                'school_level_id': school_level_id,
	                'program_id': program_id,
	                'semester': semester,
	                'student_array': student_array
	            },
	            success: function (data) {
	            	if (data.res == 1) {                            
                    	toastr["success"]('Data has been saved!'); 
                    }
                    if (data.res == 2) {                        
                        toastr["success"]('Data has been updated!');
                    }
	            }
            });
		});

	});

	/*----------------------------------------FUNCTION------------------------------------*/

	//function ajax to get data
	function getDataByAjax(selector, url, data){
		$.ajax({
            url: url,	            
            type: "post",
            dataType: 'html',
            data: data,
            success: function (data) {
            	selector.html(data);		            	
            }
        });	
	}

	//function check all
	function checkAll(m_selector, sub_selector, add_class){
		if(m_selector.is(':checked')){
			sub_selector.each(function(){
				var tr = $(this).parent().parent();
				tr.find('input:checkbox').not(this).prop('checked', false);
				$(this).addClass(add_class);
				$(this).prop('checked', true);
				$(this).val(1);
			});
		}else{
			sub_selector.removeClass(add_class);
			sub_selector.prop('checked', false);
			sub_selector.val(0);
		}
	}

	//function check one
	function checkOne(m_selector, sub_selector, count_check, add_class){
		var addClass = "."+add_class;
		var count_all = count_check.size();
		if(sub_selector.is(':checked')){
			sub_selector.addClass(add_class);				
			sub_selector.prop('checked', true);
			sub_selector.val(1);

			//var counts = $('.seldom:checked').size();
			var counts = $(addClass).size();
			//alert(count_all +"Vs"+ counts);
			if(count_all == counts){
				m_selector.prop('checked', true);
			}
		}else{
			m_selector.prop('checked', false);
			sub_selector.removeClass(add_class);
			sub_selector.prop('checked', false);
			sub_selector.val(0);
		}

	}

</script>