<style type="text/css">
    table tbody tr td img{width: 20px; margin-right: 10px}
    a,.sort,.panel-heading span{cursor: pointer;}
    .panel-heading span{margin-left: 10px;}
    .cur_sort_up{
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
        background-position: left;
        background-repeat: no-repeat;
        padding-left: 15px !important;
    }
    .cur_sort_down{
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
        background-position: left;
        background-repeat: no-repeat;
        padding-left: 15px !important;
    }
    .ui-autocomplete{z-index: 9999;}

    .panel-body{
        /*height: 650px;*/
        max-height: calc(100vh - 200px);
        overflow-y: auto;
    }

    .t_border{
        border:none !important;
    }
/*
    input[type=text]{
      border:none !important;
    }*/
    #guardian,#academic,#teacher{
      border: none; 
      border-bottom: 1px dashed black;
      text-align:center;
    }

    #list th {vertical-align: middle;}
    #list td {vertical-align: middle;}
    .hide_controls{display:none;}
</style>
<?php  
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }    
?>

<div class="container-fluid" style="width:100% !important;padding-bottom: 20px;">
    <div class="row">
      <div class="col-xs-12">
          <div class="result_info">
              <div class="col-xs-6">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                  <strong>List Student</strong>  
              </div>
              <div class="col-xs-6" style="text-align: right">
                  <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                      <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
                  </a>
                  <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                     <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
                  </a>
              </div>         
          </div>
      </div>
    </div>
  <?php if($this->session->userdata('match_con_posid')!='stu'){?>
    <div class="collapse in " id="colapseform">
        <div class="col-sm-12">
          <div class="panel panel-default" style="margin-left: -14px;margin-right: -14px;">
            <div class="panel-body">
              <div class="col-sm-12">    
                  <div class="col-sm-4">                
                      <div class="form-group">          
                          <label>School:</label>
                          <select class="form-control" id="school">
                            <option value=""></option>
                              <?php echo $opt_school;?> 
                        </select>
                        <input type='hidden' class='form-control input-sm' id='id'/>
                      </div>
                  </div>
                  <div class="col-sm-4"> 
                      <div class="form-group">          
                          <label>Program:</label>
                          <select class="form-control" id="program"></select>
                      </div>
                  </div>
                  <div class="col-sm-4"> 
                      <div class="form-group">          
                      <label>School Level:</label>
                        <select class="form-control" id="sch_level"></select>
                      </div>   
                  </div>
              </div>
              <div class="col-sm-12"> 
                  <div class="col-sm-4"> 
                    <div class="form-group">          
                      <label>Year:</label>
                      <select class="form-control" id="years"></select>
                    </div> 
                  </div>
                  <div class="col-sm-4"> 
                    <div class="form-group">          
                      <label>Grad Level:</label>
                      <select class="form-control" id="grad_level"></select>
                    </div>
                  </div>
                  <div class="col-sm-4">
                      <div class="form-group">            
                        <label>Class:</label>
                        <select class="form-control" id="classid"></select>
                      </div>
                  </div>
              </div>
              <div class="col-sm-12"> 
                <div class="col-sm-4"> 
                    <div class="form-group">          
                      <label>Term:</label>
                      <select class="form-control" id="termid"></select>
                    </div> 
                </div>
                <div class="col-sm-4"> 
                    <div class="form-group">          
                      <label>Exam Type:</label>
                      <select class="form-control" id="exam_type">
                        <?php echo $opt_examtyp;?>
                      </select>
                    </div> 
                </div>  
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Student ID:</label>
                        <input type='text' value="" class='form-control input-sm' name='s_student_id' id='s_student_id'/>
                    </div>   
                </div>                               
            </div>
            <div class="col-sm-12">
                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Name(Khmer/English):</label>
                      <input type='text' value="" class='form-control input-sm' name='s_full_name' id='s_full_name'/>
                    </div>
                </div>
                  
            </div>          
            <div class="col-sm-12" style="padding-top:20px;">
                <div class="form-group">
                    <input type="button" class="btn btn-primary" value="Search" id="search">
                </div>            
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php }?>
</div>
<!-- List Data -->
  <div class="row-1">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="table-responsive">
          <table class="table" border='0' style="">
            <thead >
              <tr>
                <?php
                    foreach ($thead as $th => $val) {
                        if ($th == 'Action'){
                            echo "<th class='remove_tag'>" . $th . "</th>";
                        }
                        else{
                            echo "<th class='sort $val no_wrap' onclick='sort(event);' rel='$val' sortype='ASC'>" . $th . "</th>";
                        }
                    }
                    echo "<th id='show_print'>Print All</th>";
                ?>
              </tr>
            </thead>
            <tbody class='listbody'>

            </tbody>
          </table>
        </div>
      </div>
      <div class="fg-toolbar">
        <div style='margin-top:20px; width:11%; float:left;'>
          Display : <select id='perpage' onchange='show_list(1);' style='padding:5px; margin-right:0px;'>
            <?PHP
                for ($i = 10; $i < 500; $i += 10) {           
                   echo "<option value='$i' selected>$i</option>";                                     
                }
            ?>                        
          </select>
        </div>
          <div class='paginate' style="float:right;"></div>
      </div> 
    </div>
  </div> 
  
<div class="modal fade" id="show_comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Add Comment</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <input type="hidden" name="hstudent_id" id="hstudent_id" class="hstudent_id">
            <table style="width:100%;">
              <tr><td colspan="6" style="text-align: center;"><span id="title_comment"></span></td></tr>
              <tr>
                  <td style="width:100px;">Student`s name</td>
                  <td>:</td>
                  <td><p id="get_student_name"></p></td>
                  <td style="width:100px;">Class</td>
                  <td>:</td>
                  <td><p id="get_class_name"></p></td>
              </tr>
              <tr>
                  <td style="width:100px;">Student ID</td>
                  <td>:</td>
                  <td><p id="get_student_id"></p></td>
                  <td style="width:100px;">Year</td>
                  <td>:</td>
                  <td><span id="ftdate_show"></span></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="row" style="margin-top: 20px;">
          <div class="col-sm-12">
            <label>Teacher`s Comment</label>
            <textarea rows="5" id="tearchcomment" class="form-control"></textarea>
          </div>
          <div class="col-sm-12" style="margin-top: 20px;">
            <div class="col-sm-6">
              <div class="col-sm-2"><label>Date:</label></div>
              <div class="col-sm-10"><input type="text" name="date_academic" id="date_academic" value="<?php echo date('d-m-Y');?>" style="border:none;border-bottom: 1px dotted black;text-align: center;"></div>
            </div>
            <div class="col-sm-6">
              <div class="col-sm-2"><label>Date:</label></div>
              <div class="col-sm-10"><input type="text" name="date_teachersign" id="date_teachersign" value="<?php echo date('d-m-Y');?>"  style="border:none;border-bottom: 1px dotted black;text-align: center;"></div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6" style="">
              <center>
              <label>Academic Manager</label>
              <SELECT id="academicid" class="form-control" style="width:150px;">
                <?php 
                  $sql_user_acad = $this->db->query("SELECT
                                                      sch_user.userid,
                                                      sch_user.user_name
                                                      FROM
                                                      sch_user
                                                      WHERE match_con_posid='acca'");
                  $opt_acad = "<option value=''></option>";
                  if($sql_user_acad->num_rows() > 0){
                    foreach($sql_user_acad->result() as $row_acad){
                      $opt_acad.= "<option value='".$row_acad->userid."'>".$row_acad->user_name."</option>";
                    }
                  }   
                  echo $opt_acad;  
                ?>
              </SELECT>
              </center>
            </div>
            <div class="col-sm-6" style="text-align: center;">
              <label>Teacher`s signature</label>
              <?php 
                  $userid = $this->session->userdata('userid');
                  $sql_user_acad = $this->db->query("SELECT
                                                        sch_user.user_name
                                                        FROM
                                                        sch_user
                                                        WHERE sch_user.userid='".$userid."'")->row();
                  $username = isset($sql_user_acad->user_name)?$sql_user_acad->user_name:"";
              ?>
              <p id="label_teacher"><b><?php echo ucwords($username);?></b></p>
              <input type="hidden" name="userid_teacher" id="userid_teacher" value="<?= $userid ?>">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12" style="margin-top: 20px;">
            <label>Guardian`s Comment</label>
            <textarea rows="5" id="guardiancomment" class="form-control"></textarea>
          </div>
          <div class="col-sm-12" style="margin-top: 20px;">
            <div class="col-sm-7" style="text-align: center;">
              <p>Please return the record book to school before</p>
              <div class="col-sm-10"><input type="text" name="datereturn" id="datereturn"  value="<?php echo date('d-m-Y');?>" style="border:none;border-bottom: 1px dotted black;text-align: center;"></div>
            </div>
            <div class="col-sm-5" style="text-align: center;">
              <div class="col-sm-2"><label>Date:</label></div>
              <div class="col-sm-10"><input type="text" name="dateguadingsign" id="dateguadingsign"  value="<?php echo date('d-m-Y');?>" style="border:none;border-bottom: 1px dotted black;text-align: center;"></div>
              <p>Guardian`s Signature</p>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn_save">Save</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $( document ).ready(function() {     
      clear_form();
      show_list(1);
        //----------------Pagenation---------------------
      $(document).on('click', '.pagenav', function(){
          var page = $(this).attr("id");        
          show_list(page);     
      });
    $("#date_academic,#date_teachersign,#datereturn,#dateguadingsign").datepicker();
    //---------------End------------------------------
    $('#perpage').val(10);

      $("#a_addnew").click(function(){
          $("#colapseform").collapse('toggle');
      });

      $("#refresh").click(function(){
          location.reload();
      });

      $( "#search" ).click(function() {         
            show_list(1);
      });     

      $('#school').change(function(){
        var schoolid = $(this).val();
         if(schoolid != ""){
            get_program(schoolid);  
         }else{
            $('#program').html('');
            $('#years').html('');
            $('#grad_level').html('');
            $('#sch_level').html('');
         }
      });

      $('#program').change(function(){
        var program = $(this).val();
         if(program != ""){
            get_school_level(program);  
         }else{
            $('#years').html('');
            $('#grad_level').html('');
            $('#sch_level').html('');
         }
      });

      $('#sch_level').change(function(){        
        var sch_level = $(this).val(); 
        var program = $('#program').val();
        var grad_level = $('#grad_level').val();
        if(sch_level !=""){
            get_year(program,sch_level); 
            get_gradlevel(program,sch_level);
            get_class(sch_level,grad_level);              
        }
      });

      $('#grad_level').change(function(){
        var sch_level = $('#sch_level').val();
        var grad_level = $(this).val();  
        if(grad_level !=""){
             get_class(sch_level,grad_level);    

        }
      });

      $( "#exam_type" ).change(function() {
        show_list(1);
      });

      $('#classid').change(function(){
      	var classid = $(this).val();
      	if(classid != ""){
      		get_term(classid);
      	}
      });

      $( "#s_student_id" ).keyup(function(){
        var this_v = $(this);
        stuentid_auto(this_v);
      });

      $( "#s_full_name" ).autocomplete({
          source: function(request, response) {
              $.getJSON("<?= site_url('student/c_list_student_term/st_auto') ?>", {
                name : request.term,
                pid: $("#program").val(),
                schlevel:$("#sch_level").val(),
                classid:$("#classid").val()
              }, response);
          },
          minLength: 0,
          delay: 0,
          autoFocus: true,
          select: function( event, ui ) {          
              $(this).val(ui.item.name);           
              return false;                       
          }
      })
      .focus(function(){
        $( this ).autocomplete("search");
      })
      .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
          .append( "<span>" + item.name + "</span>" )              
          .append( "</li>" )
          .appendTo( ul );
      }

      $("body").delegate("#add_comment","click",function(){
            var studentid = $(this).attr("student_id");
            $.ajax({
                url: '<?php echo site_url('student/c_list_student_term/show_comment') ?>',
                datatype:'JSON',
                type:'POST',
                async:false,
                data:{
                      'schoolid'   : $("#school").val(),
                      'sch_level'  : $("#sch_level").val(),
                      'years'      : $("#years").val(),
                      'grad_level' : $("#grad_level").val(),
                      'classid'    : $("#classid").val(),
                      'termid'     : $("#termid").val(),
                      'exam_type'  : $("#exam_type").val(),
                      'student_id' : studentid
                },
                success:function(data){
                    $("#hstudent_id").val(data.comment.student_id),
                    $("#tearchcomment").val(data.comment.command_teacher),
                    $("#guardiancomment").val(data.comment.command_guardian),
                    $("#date_academic").val(data.comment.date_academic),
                    $("#date_teachersign").val(data.comment.date_teacher),
                    $("#datereturn").val(data.comment.date_return),
                    $("#dateguadingsign").val(data.comment.date_guardian),
                    $("#academicid").val(data.comment.academicid),
                    $("#userid_teacher").val(data.comment.userid)
                }
            });
            var tr = $(this).parent().parent();
            var exam_type = $("#exam_type").val();
            if(exam_type == 1){
              $("#title_comment").html("<h4 style='font-size:20px !important;'>STUDENT`S IEP MID-TERM REPORT</h4>");
            }else{
              $("#title_comment").html("<h4 style='font-size:20px !important;'>STUDENT`S IEP FINAL TERM REPORT</h4>");
            }
            var student_id  = $(this).attr("student_id");
            var student_num = tr.find(".student_num").html();
            var fullname_st = tr.find("#fullname_st").html();
            var ftdate    = tr.find("#ftdate").val();
            var classname = $("#classid option:selected").text();
            $("#hstudent_id").val(student_id);
            $("#get_student_name").html("<p style='font-size:15px;'><b>"+fullname_st+"</b></p>");
            $("#get_class_name").html("<b>"+classname+"</b>");
            $("#get_student_id").html("<b>"+student_num+"</b>");
            $("#ftdate_show").html("<b>"+ftdate+"</b>");
            $("#show_comment").modal("show");
      });

      $("body").delegate("#btn_save","click",function(){
          var command_teacher  = $("#tearchcomment").val();
          var comment_guardian = $("#guardiancomment").val();
          var academicid = $("#academicid").val();
          if(command_teacher == ""){
            alert("Please input teacher comment.");
          }else if(comment_guardian == ""){
            alert("Please input guardian comment.");
          }else if(academicid == ""){
            alert("Please choose academic manager.");
          }else{
                $.ajax({
                    url: '<?php echo site_url('student/c_list_student_term/save_comment') ?>',
                    datatype:'JSON',
                    type:'POST',
                    async:false,
                    data:{
                          'schoolid'   : $("#school").val(),
                          'program'    : $("#program").val(),
                          'sch_level'  : $("#sch_level").val(),
                          'years'      : $("#years").val(),
                          'grad_level' : $("#grad_level").val(),
                          'classid'    : $("#classid").val(),
                          'termid'     : $("#termid").val(),
                          'exam_type'  : $("#exam_type").val(),
                          'student_id' : $("#hstudent_id").val(),

                          'comment_teacher' : $("#tearchcomment").val(),
                          'comment_guardian': $("#guardiancomment").val(),
                          'date_academic': $("#date_academic").val(),
                          'date_teacher' : $("#date_teachersign").val(),
                          'date_return'  : $("#datereturn").val(),
                          'date_guardian': $("#dateguadingsign").val(),
                          'academicid': $("#academicid").val(),
                          'userid_teacher': $("#userid_teacher").val()
                    },
                    success:function(data){
                        $("#hstudent_id").val("")
                        $("#tearchcomment").val(""),
                        $("#guardiancomment").val(""),
                        $("#date_academic").val(""),
                        $("#date_teachersign").val(""),
                        $("#datereturn").val(""),
                        $("#dateguadingsign").val(""),
                        $("#academicid").val(""),
                        $("#userid_teacher").val("")
                        $("#show_comment").modal("hide");
                    }
                });
            }
      })
  });
  function stuentid_auto(this_v){
      this_v.autocomplete({
              source: function(request, response) {
                  $.getJSON("<?= site_url('student/c_list_student_term/stid_auto') ?>", {
                     //st_num : request.term,
                     id:this_v.val(),
                     pid: $("#program").val(),
                     schlevel:$("#sch_level").val(),
                     classid:$("#classid").val()
                  }, response);
              },
              minLength: 0,
              delay: 0,
              autoFocus: true,
              select: function( event, ui ) {   
                  var id = ui.item.st_num;
                  var name = ui.item.st_name;
                  var full = id+'-'+name;      
                  this_v.val(id);
                  $("#id").val(ui.item.st_num);           
                  return false;                       
              }
        })
        .focus(function(){
          this_v.autocomplete("search");
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
          return $( "<li>" )
            .append( "<span>" + item.st_num + "-" + item.st_name + "</span>" )              
            .append( "</li>" )
            .appendTo( ul );
        }      
  }



  function get_program(schoolid){
    $.ajax({
      url: '<?php echo site_url('student/c_list_student_term/get_program') ?>',
      datatype:'JSON',
      type:'POST',
      async:false,
      data:{
            'schoolid':schoolid
      },
      success:function(data){
        $('#program').html(data.program);    
      }
    });
  }  

  function  get_school_level(program){
    $.ajax({
      url: '<?php echo site_url('student/c_list_student_term/school_level') ?>',
      datatype:'JSON',
      type:'POST',
      async:false,
      data:{
            'program':program
      },
      success:function(data){
          $('#sch_level').html(data.schlevel);    
          get_year(program,$("#sch_level").val());
      }
    });
  }

  function get_year(program,sch_level){
    $.ajax({
      url:'<?php echo site_url('student/c_list_student_term/get_years') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'program':program,
              'sch_level':sch_level
      },
      success:function (data){
          $('#years').html(data.schyear);
          get_gradlevel(program,sch_level);
      },
    });
  }

  function get_gradlevel(program,sch_level){
    $.ajax({
      url:'<?php echo site_url('student/c_list_student_term/get_gradlevel') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'program':program,
              'sch_level':sch_level
      },
      success:function (data){
          $('#grad_level').html(data.gradlevel);         
      },
    });
  }

  function get_class(sch_level,grad_level){
    $.ajax({
      url:'<?php echo site_url('student/c_list_student_term/get_class') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'sch_level':sch_level,
              'grad_level':grad_level
      },
      success:function (data){
          $('#classid').html(data.getclass);
      },
    });
  }

  function get_term(classid){
  	$.ajax({
      url:'<?php echo site_url('student/c_list_student_term/get_term') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'classid':classid
      },
      success:function (data){
          $('#termid').html(data.get_termid);
      },
    });
  }

  function clear_form(){
    $('school_level').val('');
    $('#grad_level').val('');
    $('#years').val('');
    $('#id').val('');
    $('#s_student_id').val('');
  } 

  function sort(event) {
        this.sortby = $(event.target).attr("rel");
        if ($(event.target).attr("sortype") == "ASC") {
            $(event.target).attr("sortype", "DESC");
            this.sorttype = "ASC";
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass('cur_sort_up');
        } else if ($(event.target).attr("sortype") == "DESC") {
            $(event.target).attr("sortype", "ASC");
            this.sorttype = "DESC";
            $('.sort').removeClass('cur_sort_up');
            $(event.target).addClass('cur_sort_down');
        }

        show_list(this.fn, this.sortby, this.sorttype);       
  }

  function show_list(page, sortby, sorttype){
        var url = "<?php echo site_url('student/c_list_student_term/show_list') ?>";
        var m = "<?PHP echo $m?>";
        var p = "<?PHP echo $p?>";
        var program = $('#program').val();
        var sch_level = $('#sch_level').val();
        var years = $('#years').val();
        var classid = $('#classid').val();
        var gradlevel = $('#grad_level').val();
        var termid = $('#termid').val();
        //var gender =$('#gender').val();
        var perpage = $('#perpage').val();
        var s_student_id = $('#s_student_id').val();
        var s_full_name = $('#s_full_name').val(); 
        var exam_type = $('#exam_type').val();
        var schoolid = $('#school').val();
        
        $.ajax({
          url: url,
          type: "POST",
          datatype: "JSON",
          async: false,
          data: {
              'm': m,
              'p': p,
              'page': page,
              's_sortby': sortby,
              's_sorttype': sorttype,
              'program': program,
              'sch_level':sch_level,
              'years':years,
              'classid':classid,
              'termid':termid,
              'gradlevel':gradlevel,
              //'gender':gender,
              'exam_type':exam_type,
              's_student_id':s_student_id,
              's_full_name':s_full_name,
              'schoolid':schoolid,
              'perpage': perpage
          },
          success: function (data) {
              if(data.tr_data ==""){
                  var table = '<table class="table table-bordered table-striped table-hover">'+
                                  '<tr>'+
                                      '<td colspan="11" style="color:red;text-align:center;font-size:14px;"><i>We did not find anything to show here!</i></td>'+
                                  '</tr>'+
                                  '<tr><td colspan="11"></td></tr>'+
                              '</table>';
                  $('.listbody').html(table);
              }else{
                  $(".listbody").html(data.tr_data);
                  //clear_form();
              }                   
              $('.paginate').html(data.pagina.pagination);
              
              if(exam_type == 1){
                $("#show_print").html("<a href='<?php echo site_url('student/c_list_student_term/pint_list_midterm');?>?page="+perpage+"&numrow="+data.pagina.start+"&p_id="+program+"&schoolid="+schoolid+"&schlid="+sch_level+"&grandid="+gradlevel+"&yearid="+years+"&classid="+classid+"&termid="+termid+"&examtype="+exam_type+"' target='_blank'>Print All</a>");
              }else{
                $("#show_print").html("<a href='<?php echo site_url('student/c_list_student_term/pint_list_final');?>?page="+perpage+"&numrow="+data.pagina.start+"&p_id="+program+"&schoolid="+schoolid+"&schlid="+sch_level+"&grandid="+gradlevel+"&yearid="+years+"&classid="+classid+"&termid="+termid+"&examtype="+exam_type+"' target='_blank'>Print All</a>");
              }
          }
      });
    }
  
</script>