<style type="text/css">
    table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }

    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    .ui-autocomplete-loading {
      background: white url("<?= base_url('assets/images/ui-anim_basic_16x16.gif') ?>") right center no-repeat;
     }
     .ui-autocomplete{height: auto;overflow-y: hidden;}
     .ui-menu .ui-menu-item:hover, .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus {
        background: #4cae4c;
        color: white;
     }
</style>
<div class="container-fluid">     
      <div class="row">
          <div class="col-xs-12">
             <div class="result_info">
                <div class="col-xs-6" style="background: #4cae4c;color: white;">
                   <span class="glyphicon glyphicon-stats"></span>
                   <strong>Student Attendent</strong>  
                </div>
                <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
                  <a href="javascript:void(0);" class="btn btn-sm btn-success" id="a_addnew" data-toggle="tooltip" data-placement="top" title="New Student Attendent"><span class="glyphicon glyphicon-plus"></span></a>
                  <a href="javascript:void(0);" class="btn btn-sm btn-success" id="a_search" data-toggle="tooltip" data-placement="top" title="Search"><span class="glyphicon glyphicon-search"></span></a>
                  <a href="javascript:void(0);" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh"><span class="glyphicon glyphicon-refresh"></span></a>
                  <?php if($this->green->gAction("E")){ ?>
                  <a href="javascript:void(0);" class="btn btn-sm btn-success" id="Export" data-toggle="tooltip" data-placement="top" title="Export"><span class="glyphicon glyphicon-export"></span></a>
                  <?php }?>
                  <?php if($this->green->gAction("P")){ ?>
                  <a href="javascript:void(0);" class="btn btn-sm btn-success " id="Print" data-toggle="tooltip" data-placement="top" title="Print"><span class="glyphicon glyphicon-print"></span></a>         
                  <?php }?>
                </div>         
             </div>
          </div>
      </div>
  <div class="in collapse" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post"  method="POST" action="" >
          
          <div class="col-sm-3">
              <div class="form-group">           
                 <label for="student">Student<span style="color:red"></span></label>
                     <input type="text" name="student" id="student" class="form-control" placeholder="Student..." >  
              </div>  
          </div>
          <input type="hidden" name="attid" id="attid" class="attid">
          <div class="col-sm-3">
            <div class="form-group">           
               <label for="program">Program<span style="color:red"></span></label>
                   <select name="program" id="program" class="form-control"  data-parsley-required="true" data-parsley-required-message="This field require">
                      <?php
                          $opt = '';
                          $opt .= '<option value="">--Select--</option>';
                          foreach ($this->attendent->getprograms() as $row) {
                             $opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
                          }
                         echo $opt;        
                      ?>
                   </select> 
            </div>      
          </div>

             <div class="col-sm-3">
                <div class="form-group">           
                   <label for="Yare_id">Yare<span style="color:red"></span></label>&nbsp;
                   <select name="Yare_id" id="Yare_id" class="form-control"  data-parsley-required="true" data-parsley-required-message="This field require">
                      
                   </select>
                </div>               
             </div>
             
             <div class="col-sm-3">
                <div class="form-group">           
                   <label for="from_date">From Date<span style="color:red"></span></label>
                    <div data-date-format="dd/mm/yyyy" class="input-group date from">
                      <input type="text"  value="<?php if(isset($row->from_date)) echo $this->green->convertSQLDate($row->from_date); else echo date('d/m/Y');?>" data-type="dateIso" class="form-control" id="from_date" name="from_date">
                      <span class="input-group-addon"><i class="icon-calendar"></i></span> 
                    </div>
                </div>         
             </div>

             <div class="col-sm-3">
                <div class="form-group">           
                   <label for="approveby">Approve by<span style="color:red"></span></label>
                   <input type="text" name="approveby" id="approveby" class="form-control" placeholder="Approve..." > 
                </div>         
             </div>

             <div class="col-sm-3">
                <div class="form-group">           
                   <label for="school">School Level<span style="color:red"></span></label>
                   <select name="school" id="school" class="form-control"  data-parsley-required="true" data-parsley-required-message="This field require">
                      
                   </select> 
                </div>         
             </div>

             <div class="col-sm-3">
                <div class="form-group">           
                   <label for="classname">Class<span style="color:red"></span></label>
                   <select name="classname" id="classname" class="form-control"  data-parsley-required="true" data-parsley-required-message="This field require">
                      
                   </select> 
                </div>         
             </div>

             <div class="col-sm-3">
                <div class="form-group">           
                   <label for="to_date">To Date<span style="color:red"></span></label>
                    <div data-date-format="dd/mm/yyyy" class="input-group date to">
                        <input type="text"   value="<?php if(isset($row->to_date)) echo $this->green->convertSQLDate($row->to_date); else echo date('d/m/Y');?>" data-type="dateIso" class="form-control" id="to_date" name="to_date">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span> 
                    </div>
                </div> 
             </div>

            
             

          <div class="col-sm-7 col-sm-offset-5">
            <div class="form-group"> 
              <?php if($this->green->gAction("R")){ ?>          
                <button type="button" class="btn btn-success btn-sm" name="search" id="search">Search</button>
              <?php }?>
               <button type="button" class="btn btn-default btn-sm" name="clear" id="clear">Clear</button>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
               <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
            </div>             
          </div>
      </form>      
  </div>
   <div>
      <div class="panel panel-default">
         <div class="table-responsive">
              <div id="tab_print">
                  <table width="100%" border="0" class="table table-hover" >
                      <thead>
                         <tr style="background: #4cae4c;color: white;">
                              <?php
                                   foreach ($thead as $th => $val) { 
                                      if ($th == 'No'){
                                          echo "<th class='$val' width='3%'>".$th."</th>";
                                      }else if($th == 'Action'){
                                          echo"<th class='$val remove_tag no_wrap' width='5%'>".$th."</th>";
                                      }else{
                                          echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
                                      }
                                   }
                                ?>
                          </tr>
                      </thead>
                      <tbody id='listrespon'></tbody>
                  </table>
               </div>
           </div>
      </div>
      <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
          Display : <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                      <?php
                          $num=50;
                          for($i=0;$i<10;$i++){?>
                              <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                              <?php $num+=50;
                          }
                      ?>
                    </select>
      </div>
      <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
          <div class="pagination" id="pagination" style="text-align:center"></div>
      </div>
   </div>
</div>
<div id="myModal_writer" class="modal bs-example-modal-lg fade m_master_detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #4cae4c;color: white;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="gridSystemModalLabel">Student Attendent</h5>
      </div>
      <div class="modal-body" style="max-height: 550px;">
        <form enctype="multipart/form-data" accept-charset="utf-8" method="post" name="basic_validate" id="basic_validate">
          <input type="hidden" name="typeno" id="typeno">
               <div class="row">
                  <div class="form_sep hide">
                      <input type="text" id="permisid" name="permisid" value="<?php if(isset($row->permisid)) echo $row->permisid ?>">
                  </div>
                  <!-- 1 -->
                  <div class="col-md-4">
                      <div class="form-group"> 
                        <label for="programid">Program <span style="color:red">*</span></label>
                        <select name="programid" id="programid" class="form-control" data-parsley-required="true" data-parsley-required-message="Select Program" >
                               <?php
                                  $opt = '';
                                  $opt .= '<option value="">--Select--</option>';
                                  foreach ($this->attendent->getprograms() as $row) {
                                     $opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
                                  }
                                 echo $opt;        
                               ?>
                            </select>
                      </div>

                      <div class="form-group"> 
                        <label for="schlevelids">School Level<span style="color:red"></span></label>
                        <select name="schlevelids" id="schlevelids" class="form-control " >

                        </select>
                      </div>

                      <div class="form-group"> 
                        <label for="yearid">Year<span style="color:red"></span></label>
                          <select name="yearid" id="yearid" class="form-control ">
                          
                        </select>
                      </div>

                      
                  </div>
                  <!-- 2 -->
                  <div class="col-md-4">
                      <div class="form-group"> 
                          <label for="classid">Class<span style="color:red"></span></label>
                          <select name="classid" id="classid" class="form-control ">
                            
                          </select>
                      </div>
                      <div class="form-group">           
                          <label for="studentid">Student <span style="color:red">*</span></label>
                          <input type="text" name="studentid" data-parsley-required="true" data-parsley-required-message="Student" placeholder="Autocomplete" value="<?php if(isset($row->fullname)) echo $row->fullname ?>"  id="studentid" class="form-control" > 
                          <input type="text" class="hide" name="student" value="<?php if(isset($row->studentid)) echo $row->studentid ?>" id="student">
                      </div>   

                      <div class="form-group">
                        <label>Approve by <span style="color:red">*</span></label>
                        <input type="text" name="hr_respond" placeholder="Autocomplete" value="<?php if(isset($row->last_name)) echo $row->last_name.' '.$row->first_name ?>"  id="hr_respond" class="form-control" data-parsley-required="true" data-parsley-required-message="Approve by">
                        <input type="text" value="<?php if(isset($row->last_name)) echo $row->approve_by ?>" name="approve_by" class="hide" id="approve_by" class="form-control">
                      </div>
                  </div>
                  <!-- 3 -->
                  <div class="col-md-4">
                      <div class="form-group">
                        <label>Permission Status</label>
                        <select name="permis_status" id="permis_status" class="form-control">
                          <option value="1" <?php if(isset($row->permis_status)) if($row->permis_status==1) echo "selected";?>>Permission</option>
                          <option value="0" <?php if(isset($row->permis_status)) if($row->permis_status==0) echo "selected";?>>No Permission</option>
                        </select>
                      </div>
                  
                      <div id="from_dateforM" class="form-group from_dateform">
                        <label>From Date</label>
                          <div class="input-group date to">
                            <input data-format="yyyy-MM-dd hh:mm:ss"  id="from_dateform" class="form-control from_dateform" type="text" value="<?= date('d/m/Y') ?>"></input>
                            <span id="checkdatetimepicker" class="input-group-addon add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                          </div>
                      </div>

                      <div id="to_dateforM" class="form-group to_dateform">
                        <label>To Date</label>
                          <div class="input-group date to">
                            <input data-format="yyyy-MM-dd hh:mm:ss" id="to_dateform" class="form-control to_dateform" type="text" value="<?= date('d/m/Y') ?>"></input>
                            <span id="todatetimepicker" class="input-group-addon add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                          </div>
                      </div>

                      <!-- <div class="form-group">
                        <label>From Date</label>
                            <div data-date-format="dd/mm/yyyy"" class="input-group date from">
                              <input type="text"   class="form-control fromdate" id="from_dateform" value="<?= date('d/m/Y') ?>">
                              <span class="input-group-addon"><i class="icon-calendar"></i></span> 
                            </div>
                      </div>
                      <div class="form-group">
                        <label>To Date</label>
                        <div data-date-format="dd/mm/yyyy" class="input-group date to" >
                              <input type="text"  class="form-control todate" id="to_dateform"value="<?= date('d/m/Y') ?>" >
                              <span class="input-group-addon"><i class="icon-calendar"></i></span> 
                            </div>
                      </div> -->
                  </div>



                    <div class=" col-lg-12">
                       <label for="reason">Reason <span style="color:red">*</span></label>
                       <div class="col-md-14">
                          <textarea class="form-control input-sm" name="reason" id="reason" tabindex="8" placeholder="Reason..." data-parsley-required="true" data-parsley-required-message="Reason" ></textarea> 
                       </div>
                    </div> 

                <div class="col-md-12"></div>
               </div>
            </form>
            </div>
            <div class="modal-footer">  
              <?php if($this->green->gAction("C")){ ?>        
              <button type="button" id="save" class="btn btn-success btn-sm save" data-cate="1">Save</button>
             <?php }?>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
         </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <script type="text/javascript">
    $(function(){
        Getdata(1,$('#sort_num').val());

        $('#from_dateforM').datetimepicker({
          language: 'en',
          pick12HourFormat: true
        });

        $('.from_dateform').on('click', function(){
          $("#checkdatetimepicker").click();
        });

         $('#to_dateforM').datetimepicker({
          language: 'en',
          pick12HourFormat: true
        });

        $('.to_dateform').on('click', function(){
          $("#todatetimepicker").click();
        });
        
        // add new ------------------------
        $('body').delegate('#a_addnew', 'click', function(){
           $('.m_master_detail').modal({
              backdrop: 'static'
           })         
           claredata();
          $('#save').html('Save');
        });
        // sort_num -----------------------
        $('#sort_num').change(function(){
           //alert($(this).val());
           var  total_display= $(this).val();
           Getdata(1,total_display);
        });
        //pagenav--------------------------
        $("body").delegate(".pagenav","click",function(){
           var page = $(this).attr("id");
           var total_display = $('#sort_num').val();
           Getdata(page,total_display);
        });
        // key chang ----------------------
        $("#programid").on('change',function(){
           get_schlevel($(this).val());
        });
        $("#schlevelids").on('change',function(){
           get_year($(this).val());
           get_class($(this).val());
        });

        $("#program").on('change',function(){
          get_school($(this).val());
        });
        $("#school").on('change',function(){
          get_yearid($(this).val());
          get_toclass($(this).val());
        });
        // btn_search ---------------------
        $('body').delegate('#search', 'click', function(){
            Getdata(1,$('#sort_num').val());
        });
        // key enter ----------------------
        $(document).keypress(function(e) {
          if(e.which == 13) {
              Getdata(1,$('#sort_num').val());
          }
        });
        // clare sech---------------------
         $('body').delegate('#clear', 'click', function(){
            cleasech();
        });
        // search -------------------------
        $('body').delegate('#a_search', 'click', function(){
          $('#collapseExample').collapse('toggle')
          cleasech();
        });
        // refresh ------------------------
        $('body').delegate('#refresh', 'click', function(){
          location.reload();
        });

        // date ---------------------------
        // $('#from_dateform').datepicker({
        //    format: 'dd/mm/yyyy',
        //    autoclose: true
        // });
        // $('#to_dateform').datepicker({
        //    format: 'dd/mm/yyyy',
        //    autoclose: true
        // });

        $('#from_date').datepicker({
           format: 'dd/mm/yyyy',
           autoclose: true
        });
        $('#to_date').datepicker({
           format: 'dd/mm/yyyy',
           autoclose: true
        });
        // autocomplete ----------------------
        $( "#studentid" ).autocomplete({
             source: function(request, response) {
                $.getJSON("<?= site_url('student/c_student_attendent/get_student') ?>", {
                   term : request.term,
                   //studentid: $('#studentid').val(),
                   classid: $('#classid').val()
                }, response);
             },
             minLength: 0,
             delay: 0,
             autoFocus: true,
              select: function( event, ui ) {          
                $(this).val(ui.item.stu_name);
                $(this).attr('data-studentid', ui.item.studentid);
                           
                return false;                       
              }
        })
          .focus(function(){
            $( this ).autocomplete("search");
          })
          .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              //.append( "<span>" + item.studentid + "</span> | " ) 
              .append( "<span>" + item.stu_name + "</span>" ) 
                           
              .append( "</li>" )
              .appendTo( ul );
          }


        // aoutocomplete hr ------------------------------
        $( "#hr_respond" ).autocomplete({
             source: function(request, response) {
                $.getJSON("<?= site_url('student/c_student_attendent/get_respond') ?>", {
                   term : request.term,
                   hr_respond: $('#hr_respond').val()
                }, response);
             },
             minLength: 0,
             delay: 0,
             autoFocus: true,
              select: function( event, ui ) {          
                $(this).val(ui.item.stu_name);
                $(this).attr('data-empid', ui.item.empid);
                           
                return false;                       
              }
        })
          .focus(function(){
            $( this ).autocomplete("search");
          })
          .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            //  .append( "<span>" + item.empid + "</span> | " ) 
              .append( "<span>" + item.stu_name + "</span>" ) 
                           
              .append( "</li>" )
              .appendTo( ul );
          }

          // delete ---------------------------------
          $('body').delegate('.delete', 'click', function(){
                var permisid = $(this).data('permisid');
                //alert(permisid);
            if(window.confirm('Are you sure to delete?')){
                $.ajax({
                   url: '<?= site_url('student/c_student_attendent/delete') ?>',
                   type: 'POST',
                   datatype: 'JSON',
                   // async: false,
                   beforeSend: function(){

                   },
                   data: {
                      permisid: permisid
                   },
                   success: function(data){
                      
                      toastr["warning"]("Deleted!");
                      Getdata(1,$('#sort_num').val());
                     
                   },
                   error: function() {

                   }
                });
             }        
          });

         //  edite -----------------------------
          $("body").delegate(".edit","click",function(){
              var permisid = $(this).data('permisid');
              // alert(permisid);
              $.ajax({
                url: '<?= site_url('student/c_student_attendent/editdata') ?>',
                type: 'POST',
                datatype: 'JSON',
                // async: false,
                beforeSend: function(){

                },
                data: {
                   permisid: permisid
                },
                success: function(data){
                    $('#permisid').val(data.permisid);
                    $('#reason').val(data.reason);
                    $('#programid').val(data.programid);
                    $('#schlevelids').val(data.schlevelid);
                    $('#yearid').val(data.yearid);
                    $('#classid').val(data.classid);
                    $('#permis_status').val(data.permis_status);
                    $('#from_dateform').val(data.from_date);
                    $('#to_dateform').val(data.to_date);
                    // aouto complete
                    $('#studentid').val(data.student);
                    $('#studentid').attr('data-studentid', data.studentid);
                    $('#hr_respond').val(data.Approveby);
                    $('#hr_respond').attr('data-empid', data.hr_respond);
                    //end aoutocomplete
                    $('#save').html('Update'); 
                    $('.m_master_detail').modal({
                        backdrop: 'static'
                    })
                    // program ----------------------------
                   $('#programid').val(data.programid);
                   var schlevelid_edit = data.schlevelid;
                   var yearid_edit = data.yearid;
                   var classid_edit = data.classid;

                    // schlevel edit ----------------------
                    $.ajax({
                        url: '<?= site_url('student/c_student_attendent/get_schlevel') ?>',
                        type: 'POST',
                        datatype: 'JSON',
                        // async: false,
                        beforeSend: function(){

                        },
                        data: {
                           programid: $('#programid').val() - 0                
                        },
                        success: function(data){                     
                           var opt = '';
                           if(data.schlevel.length > 0){
                              $.each(data.schlevel, function(i, row){
                                 opt += '<option value="'+ row.schlevelid +'" '+ (row.schlevelid - 0 == schlevelid_edit - 0 ? selected="selected" : '') +'>'+ row.sch_level +'</option>';
                              });
                           }else{
                              opt += '';
                           }
                           $('#schlevelids').html(opt);

                            // year --------------------------
                            $.ajax({
                                url: '<?= site_url('student/c_student_attendent/get_year') ?>',
                                type: 'POST',
                                datatype: 'JSON',
                                beforeSend: function(){

                                },
                                data: {
                                   programid: $('#programid').val(),
                                   schlevelids: $('#schlevelids').val()                                  
                                },
                                success: function(data){
                                   var opt = '';
                                   if(data.year.length > 0){
                                      $.each(data.year, function(i, row){
                                         opt += '<option value="'+ row.yearid +'" '+ (row.yearid - 0 == yearid_edit - 0 ? selected="selected" : '') +'>'+ row.sch_year +'</option>';
                                      });
                                   }else{
                                      opt += '';
                                   }
                                   $('#yearid').html(opt);
                                },
                                error: function() {

                                }
                           });//end  year -------

                            // class --------------------------
                            $.ajax({
                                url: '<?= site_url('student/c_student_attendent/get_class') ?>',
                                type: 'POST',
                                datatype: 'JSON',
                                beforeSend: function(){

                                },
                                data: {
                                   //programid: $('#programid').val(),
                                   schlevelids: $('#schlevelids').val()                                  
                                },
                                success: function(data){
                                   var opt = '';
                                   if(data.schclass.length > 0){
                                      $.each(data.schclass, function(i, row){
                                         opt += '<option value="'+ row.classid +'" '+ (row.classid - 0 == classid_edit - 0 ? selected="selected" : '') +'>'+ row.class_name +'</option>';
                                      });
                                   }else{
                                      opt += '';
                                   }
                                   $('#classid').html(opt);
                                },
                                error: function() {

                                }
                           });//end  class -----

                        }
                    });
                    

                },
                error: function() {

                }
             }); 
                      
          });


        // save -------------------------------
         $('#save').click(function(){ 

             var permisid = $("#permisid").val();
             var programid = $("#programid").val();
             var schlevelids = $('#schlevelids').val();    
             var yearid = $('#yearid').val();
             var classid = $('#classid').val();
             var student = $('#student').val();
             var permis_status = $('#permis_status').val();
             var fromdate = $('#from_dateform').val();
             var todate = $('#to_dateform').val();
             var reason = $('#reason').val();
             var approve_by = $('#hr_respond').attr('data-empid');
             var studentid = $('#studentid').attr('data-studentid');
             //alert(fromdate);
              if($('#basic_validate').parsley().validate()){
                  $.ajax({ 
                     type: 'POST',
                     url : "<?= site_url('student/c_student_attendent/save')?>",
                     dataType : 'JSON',
                     async : false,
                     data : { 
                              permisid :permisid,
                              programid : programid,
                              schlevelids : schlevelids,
                              yearid : yearid,
                              classid : classid,
                              studentid : studentid,
                              approve_by : approve_by,
                              permis_status : permis_status,
                              fromdate : fromdate,
                              todate : todate,
                              reason : reason
                            },
                      success:function(data){ 
                          $('#save').html('Save');
                          //toastr["success"]('Data Success!'); 
                          Getdata(1,$('#sort_num').val());
                          // $('.m_master_detail').modal('hide')
                          // claredata(); 
                        // if(data == 1){                    
                        //   if(save == 2){
                        //       $('.m_master_detail').modal('hide')
                        //   }
                        //    // clear();      
                        //     toastr["success"]('Saved!');                    
                        // }
                        //   else if(data == 2){
                        //     if(save == 2){
                        //       $('.m_master_detail').modal('hide')
                        //     }
                        //  // clear();          
                        //   toastr["success"]('Updated!'); 
                        //   $('.m_master_detail').modal('hide');
                        // }
                        //  else{
                        //     toastr["success"]("Saved");
                        //     $('.m_master_detail').modal('hide');
                        // }  

                      },
                      error:function(){

                      }
                    
                  });
            
              }

        });

      // print -------------------------------------
        $("#Print").on("click", function () {
          var htmlToPrint = '' +
               '<style type="text/css">' +
               'table th, table td {' +
               'border:1px solid #000 !important;' +
               'padding;0.5em;' +
               '}' +
               '</style>';
           var title = "Student Attendent List";
           var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "");
           var export_data = $("<center>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
           export_data += htmlToPrint;
           gsPrint(title, export_data);
       });

      // Export ------------------------------------
      $("#Export").on("click", function (e) {
            var title = "Student Attendent List";
            var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
            var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
            e.preventDefault();
      }); 

    }); // end funtion ---------------------------------

   
        //get_schlevel -------------------------------
        function get_schlevel(programid){
            $.ajax({        
                url: "<?= site_url('student/c_student_attendent/get_schlevel') ?>",    
                data: {         
                  'programid':programid
                },
                type: "post",
                dataType: "json",
                // async:false,
                success: function(data){ 
                  // console.log(data);
                    var opt = '';
                        opt="<option  value=''>--select--</option>";
                    if(data.schlevel.length > 0){
                        $.each(data.schlevel, function(i, row){
                            
                            opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                        });

                    }else{

                        opt += '';

                    }

                    get_year($('#schlevelids').val(), $('#year').val()); 
                    get_class($('#schlevelids').val(), $('#classid').val());   
                    $('#schlevelids').html(opt);
               }

            });
      }

      // yare --------------------------------------
      function get_year(schlevelids){
         $.ajax({
            url: '<?= site_url('student/c_student_attendent/get_year') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               schlevelids: schlevelids                                
            },
            success: function(data){
               var opt = '';
               
               if(data.year.length > 0){
                  opt += '<option value="">--select--</option>';
                  $.each(data.year, function(i, row){
                     opt += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#yearid').html(opt);

            }
         });
      }

      // classid ---------------------------------
      function get_class(schlevelids){
         $.ajax({
            url: '<?= site_url('student/c_student_attendent/get_class') ?>',
            type: 'POST',
            datatype: 'JSON',
            beforeSend: function(){
            },
            data: {
               schlevelids: schlevelids                                 
            },
            success: function(data){
               var opt = '';              
               if(data.schclass.length > 0){
                   opt += '<option value="">--select--</option>';
                  $.each(data.schclass, function(i, row){
                     opt += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#classid').html(opt);
            },
            error: function() {

            }
         });
      }

      // ----------------form sech ------------------------
      // to program -------------------------------
      function get_school(program){
            $.ajax({        
                url: "<?= site_url('student/c_student_attendent/get_school') ?>",    
                data: {         
                  'program':program
                },
                type: "post",
                dataType: "json",
                success: function(data){ 
                  // console.log(data);
                    var opt = '';
                        opt="<option  value=''>--select--</option>";
                    if(data.school.length > 0){
                        $.each(data.school, function(i, row){
                            //
                            opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                        });
                    }else{
                        opt += '';
                    }
                     get_yearid($('#school').val(), $('#year').val());
                     get_toclass($('#school').val(), $('#classname').val());   
                     $('#school').html(opt);
               }

            });
      }

      // yareto ---------------------------------
      function get_yearid(school){
         $.ajax({
            url: '<?= site_url('student/c_student_attendent/get_yearid') ?>',
            type: 'POST',
            datatype: 'JSON',
            beforeSend: function(){
            },
            data: {
               school: school                                
            },
            success: function(data){
               var opt = '';
               
               if(data.yearid.length > 0){
                  opt += '<option value="">--select--</option>';
                  $.each(data.yearid, function(i, row){
                     opt += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#Yare_id').html(opt);

            }
         });
      }

      // to classid ------------------------------
      function get_toclass(school){
         $.ajax({
            url: '<?= site_url('student/c_student_attendent/get_toclass') ?>',
            type: 'POST',
            datatype: 'JSON',
            beforeSend: function(){
            },
            data: {
               school: school                                 
            },
            success: function(data){
               var opt = '';              
               if(data.toschclass.length > 0){
                   opt += '<option value="">--select--</option>';
                  $.each(data.toschclass, function(i, row){
                     opt += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#classname').html(opt);
            },
            error: function() {

            }
         });
      }
      
      // Getdata ----------------------------------
        function Getdata(page,total_display,sortby,sorttype){
            var roleid = <?php echo $this->session->userdata('roleid');?>;
            var per = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
            var sort_num = $('#sort_num').val();
            var student = $("#student").val();
            var approveby = $("#approveby").val();
            var program = $("#program").val();
            var school = $("#school").val();
            var Yare_id = $("#Yare_id").val();
            var classname = $("#classname").val();
            var from_date = $("#from_date").val();
            var to_date = $("#to_date").val();

            $.ajax({
                url:"<?php echo base_url();?>index.php/student/c_student_attendent/Getdata",
                type: "POST",
                datatype: "Json",
                async: false,
                data:{
                        sortby:sortby,
                        sorttype:sorttype,
                        total_display: total_display,
                        page  : page,
                        student:student,
                        approveby:approveby,
                        program:program,
                        school:school,
                        Yare_id:Yare_id,
                        classname:classname,
                        from_date: from_date,
                        to_date:to_date,
                        m: '<?php echo (isset($_GET['m']) ? $_GET['m'] : ''); ?>',
                        p: '<?php echo (isset($_GET['p']) ? $_GET['p'] : ''); ?>' 
                },
                success: function (data) {
                    $("#listrespon").html(data.data);
                    $('.pagination').html(data['pagination']['pagination']);
                }
            });
        }

     // sort ------------------------------------
     function sort(event){
         this.sortby = $(event.target).attr("rel");
         if ($(event.target).attr("sorttype") == "ASC") {
             $('.sort').removeClass('cur_sort_up');
             $(event.target).addClass(' cur_sort_down');
             $(event.target).attr("sorttype", "DESC");
             this.sorttype = "ASC";
             $('.sort').removeClass('cur_sort_down');
             $(event.target).addClass(' cur_sort_up');
         } else if ($(event.target).attr("sorttype") == "DESC") {
             $(event.target).attr("sorttype", "ASC");
             this.sorttype = "DESC";
             $('.sort').removeClass('cur_sort_up');
             $('.sort').removeClass('cur_sort_down');
             $(event.target).addClass(' cur_sort_down');    
         } 

         Getdata(1,$('#sort_num').val(),this.sortby,this.sorttype);
     }
     // clare sech ------------------------------
      function cleasech(){
        $("#student").val("");
        $("#approveby").val("");
        $("#program").val("");
        $("#school").val("");
        $("#Yare_id").val("");
        $("#classname").val("");
        $('#from_date').val('<?= date('d/m/Y') ?>');      
        $('#to_date').val('<?= date('d/m/Y') ?>'); 
      }
      // clare data --------------------------
      function claredata(){
          $("#permisid").val("");
          $("#programid").val("");
          $('#schlevelids').val("");    
          $('#yearid').val("");
          $('#classid').val("");
          $('#student').val("");
          $('#reason').val("");
          $('#hr_respond').val("");
          $('#studentid').val("");
          $('#from_dateform').val('<?= date('d/m/Y') ?>');      
          $('#to_dateform').val('<?= date('d/m/Y') ?>'); 
      }

      // gsPrint -----------------------------------
      function gsPrint(emp_title, data) {
          var element = "<div>" + data + "</div>";
          $("<center><p style='padding-top:15px;text-align:center;'><b>" + emp_title + "</b></p><hr>" + element + "</center>").printArea({
              mode: "popup",  //printable window is either iframe or browser popup
              popHt: 600,  // popup window height
              popWd: 500,  // popup window width
              popX: 0,  // popup window screen X position
              popY: 0, //popup window screen Y position
              popTitle: "test", // popup window title element
              popClose: false,  // popup window close after printing
              strict: false
          });
      }
  </script>



