<?php
	//$cars=array("Volvo","BMW","Toyota");		
	$relation=array("Legal Student","father`s child","mother`s child","brother/sister","Cousin","niece/nephew");
	$social_status=array("Normal","alcohol","violent","Mental","Drug");
	$Health=array("Normal","Handicap 50%","Handicap 100%");
	$civil_status=array("Single","Married","Divorce/widow");

	$studentid = $student["studentid"];
	$familyid = $student['familyid']-0;
	$register_date = date('d-m-Y');
	$dob = "";

	if($student["register_date"] !="" && $student["register_date"] !="0000-00-00" ){
		$register_date = $this->green->convertSQLDate($student["register_date"]);
	}	
	if($student["dob"] !="" && $student["dob"]!="0000-00-00"){
		$dob = $this->green->convertSQLDate($student["dob"]);
	}
	if($student["leave_school"]=="1"){
		$leave_school = "1" ;
		$leave_school_check = 'checked' ;
	}else{		
		$leave_school = "0" ;
		$leave_school_check = '' ;
	}
	$objbtn='<button id="std_reg_submit" name="std_reg_submit" tabindex="200000" type="submit" class="btn btn-success">Save</button>';
	$family_code ='';
	$family_name ='';
	$family_code ='';
	$family_name ='';
	$family_book_no ='';
	$family_home_no = "";
    $family_street = "";
    $family_village = "";
    $family_commune = "";
    $family_district = "";
    $family_province = "";
    $family_tel = "";
	$father_name ='';
	$father_name_kh ='';
	$father_ocupation ='';
	$father_ocupation_kh ='';
	$father_dob ='';
	$father_home ='';
	$father_phone ='';
	$father_street ='';
	$father_village ='';
	$father_commune ='';
	$father_district ='';
	$father_province ='';
	$mother_name ='';
	$mother_name_kh ='';
	$mother_ocupation ='';
	$mother_ocupation_kh ='';
	$mother_dob ='';
	$mother_home ='';
	$mother_phone ='';
	$mother_street ='';
	$mother_village ='';
	$mother_commune ='';
	$mother_district ='';
	$mother_province ='';

	if( $familyid !="" && $familyid !=0){
		$family = $this->family->getfamilyrow($familyid);
		if(count($family)>0){
			$family_code = $family["family_code"];
			$family_name = $family["family_name"];
			$family_book_no = $family["family_book_record_no"];
			$family_home_no = $family["home_no"];
			$family_street = $family["street"];
			$family_village = $family["village"];
			$family_commune = $family["commune"];
			$family_district = $family["district"];
			$family_province = $family["province"];
			$family_tel = $family["tel"];

			$father_name = $family["father_name"];
			$father_name_kh = $family["father_name_kh"];
			$father_ocupation = $family["father_ocupation"];
			$father_ocupation_kh = $family["father_ocupation_kh"];
			$father_dob = $this->green->convertSQLDate($family["father_dob"]);
			$father_home = $family["father_home"];
			$father_phone = $family["father_phone"];
			$father_street = $family["father_street"];
			$father_village = $family["father_village"];
			$father_commune = $family["father_commune"];
			$father_district = $family["father_district"];
			$father_province = $family["father_province"];

			$mother_name = $family["mother_name"];
			$mother_name_kh = $family["mother_name_kh"];
			$mother_ocupation = $family["mother_ocupation"];
			$mother_ocupation_kh = $family["mother_ocupation_kh"];
			$mother_dob = $this->green->convertSQLDate($family["mother_dob"]);
			$mother_home = $family["mother_home"];
			$mother_phone = $family["mother_phone"];
			$mother_street = $family["mother_street"];
			$mother_village = $family["mother_village"];
			$mother_commune = $family["mother_commune"];
			$mother_district = $family["mother_district"];
			$mother_province = $family["mother_province"];
		}
		
	}
		$responsible = $this->std->getresponstudent($studentid);
		if(count($responsible)>0){
			$guard_name = $responsible->respon_name;
			$guard_name_eng = $responsible->respon_name_eng;
	        $guard_relationship = $responsible->relationship;
	        $guard_occupation = $responsible->occupation;
	        $guard_occupation_eng = $responsible->occupation_eng;
	        $guard_tel1 = $responsible->tel1;
	        $guard_tel2 = $responsible->tel2;
	        $guard_email = $responsible->email;
	        $guard_home = $responsible->home_no;
	        $guard_street = $responsible->street;
	        $guard_village = $responsible->village;
	        $guard_commune = $responsible->commune;
	        $guard_district = $responsible->district;
	        $guard_province = $responsible->province;
		}else{
			$guard_name_eng = "";
	        $guard_relationship = "";
	        $guard_occupation= "";
	        $guard_occupation_eng= "";
	        $guard_tel1= "";
	        $guard_tel2= "";
	        $guard_email = "";
	        $guard_home= "";
	        $guard_street= "";
	        $guard_village= "";
	        $guard_commune= "";
	        $guard_district= "";
	        $guard_province= "";
		}		

	//$sql="SELECT * FROM v_student_profile s WHERE s.studentid='".$student['studentid']."' AND s.yearid='".$_GET['yearid']."'";
	//$stdrow=$this->db->query($sql)->row();
	//$classid= $stdrow->classid;

	$m='';
	$p='';
	if(isset($_GET['m'])){
    	$m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }			
?>

<?php
	$dash= array('Full'=>'/system/dashboard',
				'Socail'=>'system/dashboard/view_soc',
				'Health'=>'/system/dashboard/view_health',
				'Employee'=>'/system/dashboard/view_staff',
				'Student'=>'/system/dashboard/view_std');
	$moduleid = '';
	if(isset($query['def_open_page'])){
		parse_str((isset($query['def_open_page']) ? $query['def_open_page'] : ''),$arr);
		$moduleid = $arr['m'];
	}
?>

<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	ul,ol{
		margin-bottom: 0px !important;
	}
	a{
		cursor: pointer;
	}
	.ui-autocomplete,.datepicker {z-index: 9999;}
</style>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	        <div class="result_info">
				      	<div class="col-sm-6">
				      		<strong>Update Student Information</strong>				      		
				      	</div>
				      	<div class="col-sm-6" style="text-align: center">
				      		<strong>
				      			<center class='error' style='color:red;'><?php if(isset($error)) echo "$error"; ?></center>
				      		</strong>	
				      	</div>
			</div> 
	      	<div id="stdregister_div" style="text-align:right;">
	      		<div class="col-sm-10">
	      			<input id="add_history" class="btn btn-primary" type="button"  data-toggle="modal" data-target="#myModalhistory" name="add_history" value="Card history">							 
				</div>
	      		<div class="col-sm-2">
	      			<input type="text" class="form-control"  name="enter_id_card" id="enter_id_card"  placeholder="Enter Card ID">
	      		</div>
	      	</div>
	      <!-- main content -->
	       <form enctype="multipart/form-data" class="gform" name="std_register" id="std_register" method="POST" action="<?php echo site_url("student/student/save?m=$m&p=$p")?>">	        
	          	<div class="col-sm-12">
                    <div class="col-sm-12">
                        <div class="form_sep" style="text-align:right">
                            <?php //echo $objbtn;?>
                        </div>
                    </div>
                </div>
	          	<div class="col-sm-12">
		            <div class="col-sm-12 panel panel-default" style="padding-left: 0;padding-right: 0px">
		              	<div class="col-sm-12 panel-heading">
		                	<h4 class="panel-title"><?php echo $this->lang->line("Personal Details");?></h4>
		              	</div>
		              	<div class="panel-body">
			              	<div class="col-sm-12 form_sep">
			              		<div class="col-sm-2">
									<label class="req" for="last_name"> <?php echo $this->lang->line("studentid");?><?php //echo $this->lang->line("studentid");?> <span style="color:red">*</span></label>
								</div>
								<div class="col-sm-2">
									<input type="hidden"  name="studentid" id="studentid"  value="<?php echo $studentid ; ?>">
									<input type="text"  name="id_number" id="id_number"  onblur=' checkstudentid(event);'  required placeholder="Enter ID" data-required="true" class="form-control" value="<?php echo $student["student_num"] ; ?>">
								
								</div>
								<div class="col-sm-2">
									<input type="text" placeholder="ID Card" data-required="true" readonly class="form-control"  name="id_card" value="<?php echo $student["id_card_no"] ; ?>" id="id_card">
								</div>
								<div class="col-sm-2">
									<div class="hide">
										<select class="form-control" id='id_year' name='id_year' min=1 required data-parsley-required-message="Select any school">
										<?php
								 		$currentYear = date('y') + 10;
								  		$LastYear = date('y') - 10;
								        foreach (range($LastYear, $currentYear) as $value) {
								            echo '<option value="'.$value.'"'.($value !=date('y') ? "":"selected").'>'. $value . '</option>';
								        }	
									       $maxid = $this->std->getmaxid(); 
										?>
										</select>
									</div>
								</div>
								
								<div class="col-sm-2">
									<label class="req"><?php echo $this->lang->line("register_date");?> <span style="color:red">*</span></label>
								</div>
								<div class="col-sm-2">
									<div data-date-format="dd-mm-yyyy" class="input-group date reqister">
							            <input type="text" value="<?php echo $register_date; ?>" data-type="dateIso"  data-required="true" required class="form-control input_validate parsley-validated" id="register_date" name="register_date">
							            <span class="input-group-addon"><i class="icon-calendar"></i></span>
							        </div>
								</div>								
			              	</div>
			              	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="last_name"><?php echo $this->lang->line("first_name");?> <span style="color:red">*</span></label>
								</div>
								<div class="col-sm-2">
									<input type="text" required data-required="true" onblur='fillusername(event);' class="form-control parsley-validated" name="last_name" value="<?php echo $student["last_name"] ; ?>" id="last_name">
								</div>
								<div class="col-sm-2">
									<label class="req" for="last_name_kh"><?php echo $this->lang->line("first_name_kh");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" data-required="true" class="form-control parsley-validated"  name="last_name_kh" value="<?php echo $student["last_name_kh"] ; ?>" id="last_name_kh">
								</div>
								<div class="col-sm-2">
									<label class="req" for="last_name_ch"><?php echo $this->lang->line("first_name_ch");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" data-required="true" class="form-control parsley-validated"  name="last_name_ch" value="<?php echo $student["last_name_ch"] ; ?>" id="last_name_ch">
								</div>
			          	  	</div>
			              	<div class="col-sm-12 form_sep">
			          	  		<div class="col-sm-2">
									<label class="req" for="first_name"><?php echo $this->lang->line("last_name");?><span style="color:red">*</span></label>
								</div>
								<div class="col-sm-2">
									<input type="text" required data-required="true" onblur='fillusername(event);' class="form-control parsley-validated" name="first_name" value="<?php echo $student["first_name"] ; ?>" id="first_name">
								</div>
								<div class="col-sm-2">
									<label class="req" for="first_name_kh"><?php echo $this->lang->line("last_name_kh");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" data-required="true" class="form-control parsley-validated"  name="first_name_kh" value="<?php echo $student["first_name_kh"] ; ?>" id="first_name_kh">
								</div>
								<div class="col-sm-2">
									<label class="req" for="first_name_ch"><?php echo $this->lang->line("last_name_ch");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" data-required="true" class="form-control parsley-validated"  name="first_name_ch" value="<?php echo $student["first_name_ch"] ; ?>" id="first_name_ch">
								</div>
			          	  	</div>
			              		          	  	
			          	  	<div class="col-sm-12 form_sep">
			          	  		<div class="col-sm-2">
									<label class="req"><?php echo $this->lang->line("dob");?><span style="color:red">*</span></label>
								</div>
								<div class="col-sm-2">
									<div data-date-format="dd-mm-yyyy" class="input-group date dob">
				              			<input type="text" id="dob" name="dob" required value="<?php echo $dob ; ?>" data-type="dateIso" data-parsley-required-message="Select Date Of Birth"  data-required="true" class="form-control input_validate parsley-validated">
				              			<span class="input-group-addon"><i class="icon-calendar"></i></span> 
				              		</div>
								</div>
								<div class="col-sm-2">
									<label for="gender"><?php echo $this->lang->line("gender"); ?></label>
								</div>
								<div class="col-sm-2">
									<select data-required="true" name="gender" id="gender" required data-parsley-required-message="Select Gender"  class="form-control parsley-validated">
					                  	<option value="male" <?php echo ($student["gender"]=="male"?"selected":""); ?> >Male</option>
					                  	<option value="female" <?php echo ($student["gender"]=="female"?"selected":""); ?> >Female</option>
				                  	</select>
								</div>
								<div class="col-sm-2">
									<label for="gender "><?php echo $this->lang->line("nationality");?><span style="color:red"><span style="color:red">*</span></label>
								</div>
								<div class="col-sm-2">
									<input type="text"  id="nationality" name="nationality" required value="<?php echo $student["nationality"] ; ?>" data-type="dateIso" data-parsley-required-message="Choose Your nationality" data-required="true" class="form-control input_validate parsley-validated">
								</div>
			          	  	</div>
			          	  	<div class="col-sm-12 panel-heading">
                                <h4 class="panel-title"><u><?php echo $this->lang->line("place_of_birth");?></u>                                
                                </h4>
                            </div>
                            <div class="col-sm-12 hide">
                                <div class="col-sm-2">
                                    <label class="req" for="dob_home_no"><?php echo $this->lang->line("home_no");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_home_no" id="dob_home_no" value="<?php echo $student["dob_home_no"] ; ?>" data-type="number"
                                           data-required="true" class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_street"><?php echo $this->lang->line("street");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="dob_street" name="dob_street" value="<?php echo $student["dob_street"] ; ?>" data-type="dateIso"
                                           data-required="true"
                                           class="form-control input_validate parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_village"><?php echo $this->lang->line("village");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="dob_village" value="<?php echo $student["dob_village"] ; ?>" name="dob_village" data-type="dateIso"
                                           data-required="true"
                                           class="form-control input_validate parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="dob_commune"><?php echo $this->lang->line("commune");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_commune" id="dob_commune" value="<?php echo $student["dob_commune"] ; ?>" data-type="number"
                                           data-required="true" class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_district"><?php echo $this->lang->line("district");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_district" id="dob_district" value="<?php echo $student["dob_district"] ; ?>" data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_province"><?php echo $this->lang->line("province");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_province" id="dob_province" value="<?php echo $student["dob_province"] ; ?>" data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="dob_commune_eng">Commune</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_commune_eng" id="dob_commune_eng" value="<?php echo $student["dob_commune_eng"] ; ?>" data-type="number"
                                           data-required="true" class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_district_eng">District</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_district_eng" id="dob_district_eng" value="<?php echo $student["dob_district_eng"] ; ?>"  data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_province_eng">Province</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_province_eng" id="dob_province_eng" value="<?php echo $student["dob_province_eng"] ; ?>"  data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 panel-heading">
                                <h4 class="panel-title"><u><?php echo $this->lang->line("current_address");?></u>                                   
                                </h4>
                            </div>
							<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="home_no"><?php echo $this->lang->line("home_no");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" name="home_no" id="home_no" value="<?php echo $student["home_no"] ; ?>" data-type="number" data-required="true" class="form-control parsley-validated">
								</div>
								<div class="col-sm-2">
									<label class="req"  for="street"><?php echo $this->lang->line("street");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" id="street" value="<?php echo $student["street"] ; ?>" name="street" data-type="dateIso"  data-required="true" class="form-control input_validate parsley-validated">
								</div>
								<div class="col-sm-2">
									<label class="req" for="village"><?php echo $this->lang->line("village");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" id="village" value="<?php echo $student["village"] ; ?>" name="village" data-type="dateIso"  data-required="true" class="form-control input_validate parsley-validated" >
								</div>						
					    	</div>
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="home_no_eng">Home No</label>
								</div>
								<div class="col-sm-2">
									<input type="text" name="home_no_eng" id="home_no_eng" value="<?php echo $student["home_no_eng"] ; ?>" data-type="number" data-required="true" class="form-control parsley-validated">
								</div>
								<div class="col-sm-2">
									<label class="req"  for="street_eng">Street</label>
								</div>
								<div class="col-sm-2">
									<input type="text" id="street_eng" value="<?php echo $student["street_eng"] ; ?>" name="street_eng" data-type="dateIso"  data-required="true" class="form-control input_validate parsley-validated">
								</div>
								<div class="col-sm-2">
									<label class="req" for="village_eng">Village</label>
								</div>
								<div class="col-sm-2">
									<input type="text" id="village_eng" value="<?php echo $student["village_eng"] ; ?>" name="village_eng" data-type="dateIso"  data-required="true" class="form-control input_validate parsley-validated" >
								</div>						
					    	</div>
							<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="commune"><?php echo $this->lang->line("commune");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" name="commune" id="commune" value="<?php echo $student["commune"] ; ?>" data-type="number" data-required="true" class="form-control parsley-validated">
								</div>
								<div class="col-sm-2">
									<label class="req" for="district"><?php echo $this->lang->line("district");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" name="district" id="district" value="<?php echo $student["district"] ; ?>" data-required="true" class="form-control parsley-validated">
								</div>
								<div class="col-sm-2">
									<label class="req" for="province"><?php echo $this->lang->line("province");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text"  name="province" id="province" value="<?php echo $student["province"] ; ?>" data-required="true" class="form-control parsley-validated">
								</div>
					    	</div>
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="commune_eng">Commune</label>
								</div>
								<div class="col-sm-2">
									<input type="text" name="commune_eng" id="commune_eng" value="<?php echo $student["commune_eng"] ; ?>" data-type="number" data-required="true" class="form-control parsley-validated">
								</div>
								<div class="col-sm-2">
									<label class="req" for="district_eng">District</label>
								</div>
								<div class="col-sm-2">
									<input type="text" name="district_eng" id="district_eng" value="<?php echo $student["district_eng"] ; ?>" data-required="true" class="form-control parsley-validated">
								</div>
								<div class="col-sm-2">
									<label class="req" for="province_eng">Province</label>
								</div>
								<div class="col-sm-2">
									<input type="text"  name="province_eng" id="province_eng" value="<?php echo $student["province_eng"] ; ?>" data-required="true" class="form-control parsley-validated">
								</div>
					    	</div>	
					    	<div class="col-sm-12 form_sep">
					    		<div class="col-sm-2">
									<label class="req" for="stu_relationship"><?php echo $this->lang->line("staywith");?> <span style="color:red">*</span></label>
								</div>
								<div class="col-sm-2">
									<select data-required="true" required  data-parsley-required-message="Select Relationship"  class="form-control parsley-validated" name="stu_relationship" id="stu_relationship">
				                		<option value="">Select stay with</option>
				                    <?php	
				                    	$staywith ="stay_with";	                   		
				                    	foreach ($this->std->getsocailinfo($staywith) as $relat_row) {
				                    		echo '<option value="'.$relat_row->famnoteid.'" '.($relat_row->famnoteid==$student['staywith']?'selected':'').'  >'.$relat_row->description.'</option>';
					                    }
					                ?>
					                </select>
								</div>
								<div class="col-sm-2">
									<label class="req" for="phone1"><?php echo $this->lang->line("phone1");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $student["phone1"] ; ?>" data-type="phone" onkeypress='return isNumberKey(event);' value="" data-required="true"  class="form-control parsley-validated" name="phone1" id="phone1">
								</div>
								<div class="col-sm-2">
									<label class="req" for="reg_input_currency"><?php echo $this->lang->line("email");?></label>
								</div>
								<div class="col-sm-2">
									<input type="email" value="<?php echo $student["email"] ; ?>" data-parsley-type="email" data-type="email" value="" data-required="true"  class="form-control parsley-validated" name="email" id="email">
								</div>
					    	</div>
					    	<div class="col-sm-12 form_sep">
					    		<div class="col-sm-2">
                                    <label class="req" for="phone1"><?php echo $this->lang->line("registered_number");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" data-type="number" onkeypress='return isNumberKey(event);'
                                           value="<?php echo $student["registered_number"] ; ?>" data-required="true" class="form-control parsley-validated"
                                           name="registered_number" id="registered_number">
                                </div>
                                <div class="col-sm-2">
                                	<label for="reg_input_logo"><?php echo $this->lang->line("registered_discount");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" data-type="number" onkeypress='return isNumberKey(event);'
                                          value="<?php echo $student["registered_discount"] ; ?>" data-required="true" class="form-control parsley-validated"
                                           name="register_discount" id="register_discount">
                                </div>
					    		<div class="col-sm-2">
									<label for="reg_input_logo"></label>
								</div>
								<div class="col-sm-2">									
								</div>
					    	</div>	
					    	<div class="col-sm-12 form_sep">
					    		<div class="col-sm-2"></div>					    			    		
								<div class="col-sm-2">
									<input type="checkbox"  name="leave_school" id='leave_school' <?php echo $leave_school_check?> value="<?php echo $leave_school;?>" />		
			                  		<label class="req" for="leave_school"><?php echo $this->lang->line("is_vtc");?></label>
								</div>
								<div class="col-sm-2">
									<label for="leave_reason"><?php echo $this->lang->line("leave_reason");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" placeholder="Reason Leave" data-required="true" class="form-control"  name="leave_reason" value="" id="leave_reason">
								</div>
								<div class="col-sm-2">
									<label class="req"><?php echo $this->lang->line("leave_sch_date");?></label>
								</div>
								<div class="col-sm-2">
									<div data-date-format="dd-mm-yyyy" class="input-group date">
							            <input type="text" value="" data-type="dateIso"  data-required="true" class="form-control input_validate parsley-validated" id="leave_date" name="leave_date">
							            <span class="input-group-addon"><i class="icon-calendar"></i></span>
							        </div>
								</div>	
							</div>	
						</div>

						<!-- 
						<div class="panel-heading hide">
		                	<h4 class="panel-title">Login Information</h4>
		              	</div>
		              	<div class="panel-body">
				            <div class="col-sm-12 form_sep hide">
				            	<div class="col-sm-2">
				                  <label class="req" for="login_username">User Name</label> 
				                </div>
				                <div class="col-sm-2">                
				                  <input type="text" data-minlength-message="Username should have at least 5 characters." value="" data-required="true" onclick="get_stdregno();" class="form-control parsley-validated" name="login_username" id="login_username">
				                </div>
				                <div class="col-sm-2">
				                  <label class="req" for="password">Password</label>
				                </div>
				                <div class="col-sm-2">    
				                  <input type="password" value="<?php //echo $this->std->getmaxid(); ?>"  data-required-message="Please enter a valid Password"  data-minlength-message="Password should have at least 6 characters." data-minlength="6" data-required="true" onclick="get_dob();" class="form-control parsley-validated" name="password" id="password">
				                </div>
				                <div class="col-sm-4">
				                </div>
				            </div>
				        </div>	
				        -->	    

		              	<div class="panel-heading">
		                	<h4 class="panel-title"><?php echo $this->lang->line("Father Information");?></h4>
		              	</div>
		              	<div class="panel-body">
			              	<div class="col-sm-12 form_sep">
			              		<div class="col-sm-2">
									<label class="req" for="family_id"><?php echo $this->lang->line("familyid");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" placeholder="Famaily Name Or Code" value="<?php echo $family_code;?>" name="family_id" id="family_id" data-required="true" data-parsley-required-message="Enter Family" class="form-control parsley-validated family_id" >
		                  			<input type="hidden" value="<?php echo $familyid;?>" class="hide" name="familyid" id="familyid">
								</div>
								<div class="col-sm-1">
									<input id="add_family" class="btn btn-primary" type="button"  data-toggle="modal" data-target="#myModal" name="add_family" value="Add">							 
								</div>
								<div class="col-sm-1"><label class="req" for="family_name"><?php echo $this->lang->line("family_name");?></label></div>
								<div class="col-sm-2">							
									<input type="text" readonly placeholder="Famaily Name" value="<?php echo $family_name;?>" class="form-control parsley-validated" name="family_name" id="family_name">
								</div>
								<div class="col-sm-2">
									<label class="req" for="family_book_no"><?php echo $this->lang->line("family_book_record_no");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly onblur="" name="family_book_no" id="family_book_no" value="<?php echo $family_book_no;?>" class="form-control parsley-validated">
								</div>
			              	</div>			              	
			            	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="father_name"><?php echo $this->lang->line("father_name");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly onblur="" value="<?php echo $father_name_kh;?>" class="form-control parsley-validated" name="father_name" id="father_name">
								</div>
								<div class="col-sm-2">
									<label class="req" for="father_occupation"><?php echo $this->lang->line("father_ocupation");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $father_ocupation_kh;?>" class="form-control parsley-validated" name="father_occupation" id="father_occupation">
								</div>
								<div class="col-sm-2">
									<label class="req" for="father_dob"><?php echo $this->lang->line("father_dob");?></label>
								</div>
								<div class="col-sm-2">
									<div data-date-format="dd-mm-yyyy" class="input-group date dob">
										<input type='text' readonly id="father_dob" class="form-control" value="<?php echo $father_dob;?>" name="father_dob" placeholder="dd-mm-yyyy"/>
								    	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
								    </div>
								</div>
					    	</div>
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="father_name_eng">Father's Name</label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly onblur="" value="<?php echo $father_name;?>" class="form-control parsley-validated" name="father_name_eng" id="father_name_eng">
								</div>
								<div class="col-sm-2">
									<label class="req" for="father_occupation_eng">Occupation</label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $father_ocupation;?>" class="form-control parsley-validated" name="father_occupation_eng" id="father_occupation_eng">
								</div>
								<div class="col-sm-2">
									<label class="req" for="father_phone"><?php echo $this->lang->line("father_phone");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $father_phone;?>" name="father_phone" id="father_phone"  class="form-control"  />
								</div>	
					    	</div>
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="father_home"><?php echo $this->lang->line("father_home");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $father_home;?>" readonly  name="father_home" id="father_home"  class="form-control"/>
								</div>
								<div class="col-sm-2">
									<label class="req" for="father_street"><?php echo $this->lang->line("father_street");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $father_street;?>"  name="father_street" id="father_street"  class="form-control"/>
								</div>
								<div class="col-sm-2">
									<label class="req" for="father_village"><?php echo $this->lang->line("father_village");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $father_village;?>" name="father_village" id="father_village"  class="form-control"/>	
								</div>
					    	</div>
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="father_commune"><?php echo $this->lang->line("father_commune");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $father_commune;?>" name="father_commune" id="father_commune"  class="form-control"  />
								</div>
								<div class="col-sm-2">
									<label class="req" for="father_district"><?php echo $this->lang->line("father_district");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $father_district;?>" name="father_district" id="father_district"  class="form-control" />
								</div>
								<div class="col-sm-2">
									<label class="req" for="father_province"><?php echo $this->lang->line("father_province");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $father_province;?>" name="father_province" id="father_province"  class="form-control"  />
								</div>
					    	</div>					    	
					    </div>
				    	<div class="panel-heading">
		                	<h4 class="panel-title"><?php echo $this->lang->line("Mother Information");?></h4>
		              	</div>
		              	<div class="panel-body">
			              	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="mother_name"><?php echo $this->lang->line("mother_name");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly onblur="" value="<?php echo $mother_name_kh;?>" class="form-control parsley-validated" name="mother_name" id="mother_name">
								</div>
								<div class="col-sm-2">
									<label class="req" for="mother_occupation"><?php echo $this->lang->line("mother_ocupation");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $mother_ocupation_kh;?>"  class="form-control parsley-validated" name="mother_occupation" id="mother_occupation">
								</div>
								<div class="col-sm-2">
									<label class="req" for="mother_dob"><?php echo $this->lang->line("mother_dob");?></label>
								</div>
								<div class="col-sm-2">
									<div data-date-format="dd-mm-yyyy" class="input-group date dob">
										<input type='text' value="<?php echo $mother_dob;?>" readonly id="mother_dob" class="form-control" name="mother_dob" placeholder="dd-mm-yyyy"/>
									    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
									</div>
								</div>
					    	</div>
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="mother_name_eng">Mother' Name</label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly onblur="" value="<?php echo $mother_name;?>" class="form-control parsley-validated" name="mother_name_eng" id="mother_name_eng">
								</div>
								<div class="col-sm-2">
									<label class="req" for="mother_occupation_eng">Occupation</label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $mother_ocupation;?>"  class="form-control parsley-validated" name="mother_occupation_eng" id="mother_occupation_eng">
								</div>
								<div class="col-sm-2">
									<label class="req" for="mother_commune"><?php echo $this->lang->line("mother_phone");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $mother_phone;?>" name="mother_phone" id="mother_phone"  class="form-control"  />
								</div>
					    	</div>
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="mother_home"><?php echo $this->lang->line("mother_home");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly name="mother_home" id="mother_home" value="<?php echo $mother_home;?>" class="form-control"/>
								</div>
								<div class="col-sm-2">
									<label class="req" for="mother_street"><?php echo $this->lang->line("mother_street");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $mother_street;?>" name="mother_street" id="mother_street"  class="form-control"/>
								</div>
								<div class="col-sm-2">
									<label class="req" for="mother_village"><?php echo $this->lang->line("mother_village");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $mother_village;?>" name="mother_village" id="mother_village"  class="form-control"/>	
								</div>
					    	</div>
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="mother_commune"><?php echo $this->lang->line("mother_commune");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $mother_commune;?>" name="mother_commune" id="mother_commune"  class="form-control"  />
								</div>
								<div class="col-sm-2">
									<label class="req" for="mother_district"><?php echo $this->lang->line("mother_district");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $mother_district;?>" name="mother_district" id="mother_district"  class="form-control" />
								</div>
								<div class="col-sm-2">
									<label class="req" for="mother_province"><?php echo $this->lang->line("mother_province");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" readonly value="<?php echo $mother_province;?>" name="mother_province" id="mother_province"  class="form-control"  />
								</div>
					    	</div>					    	
					    </div>
				    	<div class="panel-heading">
		                	<h4 class="panel-title"><?php echo $this->lang->line("Guardain Information");?></h4>
		              	</div>
		              	<div class="panel-body">
			            	<div class="col-sm-12 form_sep">
			            		<div class="col-sm-2">
									<label class="req" for="guard_name"><?php echo $this->lang->line("respon_name");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" name="guard_name" id="guard_name" value="<?php echo $guard_name;?>" class="form-control" data-parsley-required-message="Enter First Name" />
								</div>						
								<div class="col-sm-2">
									<label class="req" for="guard_occupation"><?php echo $this->lang->line("occupation");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $guard_occupation;?>" name="guard_occupation" id="guard_occupation"  class="form-control"/>
								</div>
								<div class="col-sm-2">
									<label class="req" for="consultant"><?php echo $this->lang->line("consultant_by");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $student["consultant_by"];?>" readonly="readonly" name="consultant" id="consultant"  class="form-control"/>
								</div>
					    	</div>	
					    	<div class="col-sm-12 form_sep">
			            		<div class="col-sm-2">
									<label class="req" for="guard_name_eng">Guardain's Name</label>
								</div>
								<div class="col-sm-2">
									<input type="text" name="guard_name_eng" id="guard_name_eng" value="<?php echo $guard_name_eng;?>" class="form-control" data-parsley-required-message="Enter First Name" />
								</div>						
								<div class="col-sm-2">
									<label class="req" for="guard_occupation_eng">Occupation</label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $guard_occupation_eng;?>" name="guard_occupation_eng" id="guard_occupation_eng"  class="form-control"/>
								</div>
								<div class="col-sm-2">
									<label class="req" for="getinfor"><?php echo $this->lang->line("get_info_from");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $student["get_info_from"] ;?>" name="getinfor" id="getinfor"  class="form-control"/>
								</div>
					    	</div>	      
							<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="guard_tel1"><?php echo $this->lang->line("tel1");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $guard_tel1;?>" name="guard_tel1" onkeypress='return isNumberKey(event);' id="guard_tel1"  class="form-control"  data-parsley-required-message="Enter Phone Number" />
								</div>
								<div class="col-sm-2">
									<label class="req" for="guard_tel2"><?php echo $this->lang->line("tel2");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $guard_tel2;?>" name="guard_tel2" onkeypress='return isNumberKey(event);' id="guard_tel2"  class="form-control"  data-parsley-required-message="Enter Phone Number" />
								</div>
								<div class="col-sm-2">
									<label class="req" for="email"><?php echo $this->lang->line("email");?></label>
								</div>
								<div class="col-sm-2">
									<input type="email" value="<?php echo $guard_email;?>" name="guard_email" id="guard_email"  data-parsley-type="email" data-type="email" value="" data-required="true"  class="form-control parsley-validated">
								</div>
					    	</div>
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="guard_home"><?php echo $this->lang->line("home_no");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $guard_home;?>" name="guard_home" id="guard_home"  class="form-control"/>
								</div>
								<div class="col-sm-2">
									<label class="req" for="guard_street"><?php echo $this->lang->line("street");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $guard_street;?>" name="guard_street" id="guard_street"  class="form-control"/>
								</div>
								<div class="col-sm-2">
									<label class="req" for="guard_village"><?php echo $this->lang->line("village");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $guard_village;?>" name="guard_village" id="guard_village"  class="form-control"/>	
								</div>
					    	</div>
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="guard_commune"><?php echo $this->lang->line("commune");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $guard_commune;?>" name="guard_commune" id="guard_commune"  class="form-control"  />
								</div>
								<div class="col-sm-2">
									<label class="req" for="guard_district"><?php echo $this->lang->line("district");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $guard_district;?>" name="guard_district" id="guard_district"  class="form-control" />
								</div>
								<div class="col-sm-2">
									<label class="req" for="guard_province"><?php echo $this->lang->line("province");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $guard_province;?>" name="guard_province" id="guard_province"  class="form-control"  />
								</div>
					    	</div>
					    	</div>
			              	<div class="panel-heading">
			                	<h4 class="panel-title"><?php echo $this->lang->line("Academic Year");?></h4>
			              	</div>

			            	<div class="col-sm-12 form_sep">
			            		<div class="col-sm-2">
									<label class="req"><?php echo $this->lang->line("program");?></label>
								
									<select class="form-control" name="sch_program" id="sch_program" >
						               <?php 

										$program ='<option value="">Select Program</option>';
										$programid= "";					
					    				foreach ($this->pro->getprograms($programid) as $row_program){			    					
					                    		$program .='<option value="'.$row_program->programid.'">'.$row_program->program.'</option>';
									    }
						               echo $program; ?>	
					                </select>
								</div>
								<div class="col-sm-2">
									<label class="req"><?php echo $this->lang->line("sch_level");?></label>

                                    <select class="form-control" name="sch_level" id="sch_level">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req"><?php echo $this->lang->line("sch_year");?></label>

                                    <select class="form-control" id='sch_year' name='sch_year' min=1
                                            data-parsley-required-message="Select Academic year">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req"><?php echo $this->lang->line("rangelevelname");?></label>

                                    <select class="form-control" id='sch_rangelevel' name='sch_rangelevel' min=1
                                            data-parsley-required-message="Select Range Level">
                                        <option value=""></option>
                                    </select>

                                </div>
                                <div class="col-sm-2">
                                    <label class="req">Grade Level</label>
                                    <select class="form-control" id='gradlevel' name='gradlevel' min=1 data-parsley-required-message="Select grade level">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req"><?php echo $this->lang->line("class_name");?></label>

                                    <select class="form-control" id='sch_class' name='sch_class' min=1
                                            data-parsley-required-message="Select Class">
                                        <option value=""></option>
                                    </select>
                                </div>

                                <div class="col-sm-12" style="text-align: center;margin-top: 5px;">
                                    <input id="add_academic" class="btn btn-primary" type="button" name="add_academic"
                                           value="Add">
                                </div>
					    	</div> 

			            	<div class="col-sm-12 form_sep">
								<div class="col-sm-12">
									<div class="table-responsive">
									<table class="table table-bordered table-inverse" id="academic_year">
				                		<thead>
				                		<tr><th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("reg_input_logo");?></label></th>
                                            <th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("program");?></label></th>
                                            <th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("sch_level");?></label></th>
                                            <th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("sch_year");?></label></th>
											<th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("rangelevelname");?></label></th>
											<th style="text-align: center !important; width: 17%"><label class="req">Grade Level</label></th>
											<th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("class_name");?></label></th>
											<th style="text-align: center !important; width: 17%"><label class="req">Is active</label></th>
                                        </tr>
				                		</thead>
				                		<tbody id="body_school_year">
				                		<?php 
				                				foreach($this->std->getenrollments($studentid) as $rowenroll){
				                					$enrollid="";
				                					$sch_programid=$rowenroll["programid"];
				                					$sch_program=$rowenroll["program"];
				                					$sch_levelid=$rowenroll["schlevelid"];
				                					$sch_level=$rowenroll["sch_level"];
				                					$sch_yearid=$rowenroll["year"];
				                					$sch_year=$rowenroll["sch_year"];
				                					$sch_rangelevelid=$rowenroll["rangelevelid"];
				                					$sch_rangelevel=$rowenroll["rangelevelname"];
				                					$sch_classid= $rowenroll["classid"];
				                					$sch_class  = $rowenroll["class_name"];
				                					$grade_level= $rowenroll["grade_level"];
				                					$is_active  = $rowenroll["is_active"];
				                					$check_isactive = "";
				                					if($is_active == 1){
				                						$check_isactive = "checked='checked'";
				                					}
				                					($sch_rangelevelid!=""?$sch_rangelevelid:$sch_rangelevelid=0);
				                					($sch_classid!=""?$sch_classid:$sch_classid=0);

				                					$allstucture= $sch_programid."_".$sch_levelid."_".$sch_yearid."_".$sch_rangelevelid."_".$sch_classid;

				                					$getValide_edit = $this->std->get_checkpayment($studentid,$sch_programid,$sch_levelid,$sch_yearid,$sch_rangelevelid,$sch_classid);	

				                					echo '<tr>
				                							<td>
												                <div id="file" class="form-group show_hide_file"><div class="input-group col-xs-12">
												                <label class="input-group-btn"><span class="btn btn-primary">
												                Browse&hellip; <input type="file" style="display: none;" id="files" value="" class="file stufiles" name="stufiles[]"></span></label>
												                <input type="text" class="form-control getfilename" id="getfilename" name="getfilename[]" value=""  readonly>
												                </div></div>
												            </td>
												            <td>
												            	<input type="hidden" value="0" class="is_new" id="is_new" name="is_new[]" >
													            <input type="hidden" value="'.$allstucture .'" class="allstucture" name="allstucture[]" >
													            <input type="hidden" value="'.$enrollid .'" class="enrollid" name="enrollid[]" >
													            <input type="hidden" class="sch_programid" name="sch_programid[]" value="'.$sch_programid.'"/>'.$sch_program.'
													        </td>
												            <td>
												            	<input type="hidden" class="sch_levelid" name="sch_levelid[]" value="'.$sch_levelid.'"/>'.$sch_level.'
												            	</td>
												            <td>
												            	<input type="hidden" class="sch_yearid" name="sch_yearid[]" value="'.$sch_yearid.'"/>'.$sch_year.'
												            </td>
												            <td>
												            	<input type="hidden" class="sch_rangelevelid" name="sch_rangelevelid[]" value="'.$sch_rangelevelid.'"/>'.$sch_rangelevel.'
												            </td>
												            <td>
												            	<input type="hidden" class="sch_gradelevelid" name="sch_gradelevelid[]" value="'.$sch_rangelevelid.'"/>'.$grade_level.'
												            </td>
												            <td>
												            	<input type="hidden" class="sch_class" name="sch_classid[]" value="'.$sch_classid.'"/>'.$sch_class.'
												            </td>
												            <td style="text-align:center;"><input type="checkbox" id="is_active" value="'.$is_active.'" name="is_active" '.$check_isactive.'></td>
												            <td width="40" align="center">';
												            if($getValide_edit>0){
												            	echo '<img src="'.base_url("assets/images/icons/deletedis.png").'" />';
												            }else{
												            	echo '<a href="javascript:void(0)" class="link_del_time"><img src="'.base_url("assets/images/icons/delete.png").'" /></a>';
												            }												            
												        echo '</td>
											            </tr>';
				                					}
				                		?>
				                		</tbody>
				                	</table>
				                	</div>	
								</div>												
					    	</div>
					    </div>
				    	<div class="panel-heading">
		                	<h4 class="panel-title"><?php echo $this->lang->line("Office Use Only");?></h4>
		              	</div>
		              	<div class="panel-body">		              	
			            	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<label class="req" for="interviewby"><?php echo $this->lang->line("interview_by");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" name="interviewby" value="<?php echo $student["interview_by"]; ?>" id="interviewby"  class="form-control"/>
								</div>
								<div class="col-sm-2">
									<label class="req" for="introduced"><?php echo $this->lang->line("introduced_by");?></label>
								</div>
								<div class="col-sm-4">
									<?php
									$intro_std = " ";
									$intro_teach = " ";
									$intro_other = " ";
									$introduced_by = " ";
									if($student["introduced_by"]=="intro_std"){
										$intro_std = ' checked="checked "';
									}elseif($student["introduced_by"]=="intro_teach"){
										$intro_teach = ' checked="checked "';
									}elseif($student["introduced_by"]=="intro_other"){
										$intro_other = ' checked="checked "';
									}else{
										$intro_other = ' checked="checked "';
										$introduced_by = $student["introduced_by"];
									}
									
									?>
									<input type="radio" <?php echo $intro_std;?> id="introducedby_student" class="introducedby_student" name="introducedby" VALUE="intro_std"> <label class="req" for="introducedby_student"><?php echo $this->lang->line("student");?></label>&nbsp;
									<input type="radio" id="introducedby_teach" <?php echo $intro_teach;?> class="introducedby_teach" name="introducedby" VALUE="intro_teach"> <label class="req" for="introducedby_teach"><?php echo $this->lang->line("teacher");?></label>&nbsp; 
									<input type="radio" id="introducedby_other" class="introducedby_other" name="introducedby" VALUE="intro_other" <?php echo $intro_other;?> > <label class="req" for="introducedby_other"><?php echo $this->lang->line("Other");?></label>
								</div>
								<div class="col-sm-2"><input type="text" value="<?php echo $introduced_by;?>" name="introduced" id="introduced" class="form-control"/></div>
					    	</div>
					    </div>					    
				    	<div class="panel-heading">
		                	<h4 class="panel-title"><?php echo $this->lang->line("Transportation");?></h4>
		              	</div>
		              	<div class="panel-body">
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-2">
									<?php
									$owner = " ";
									$school = " ";
									if($student["transporttype"]=="owner"){
										$owner = ' checked="checked "';
									}
									if($student["transporttype"]=="school"){
										$school = ' checked="checked "';
									}									
									?>
									<input type="radio" id="seft_transport" class="seft_transport" name="transportation" VALUE="owner" <?php echo $owner;?>> <label class="req" for="seft_transport"><?php echo $this->lang->line("is_owner_by");?></label>
								</div>
								<div class="col-sm-2">
									<select data-required="true" data-parsley-required-message="Select transportation"  class="form-control parsley-validated" name="owner_tran_by" id="owner_tran_by">
				                		<option value="">Select Transportation</option>
				                    <?php	
				                    	//is_owner_by
				                    	$transportation ='transport';	                   		
				                    	foreach ($this->std->getsocailinfo($transportation) as $relat_row) {?>
				                    		<option value="<?php echo $relat_row->famnoteid ; ?>" <?php echo ($relat_row->famnoteid==$student["is_owner_by"]?"selected":"").'>'.$relat_row->description; ?></option>
					                    <?php }
					                ?>
					                </select>
								</div>
								<div class="col-sm-2">
									<input type="radio" id="school_car" class="school_car" name="transportation" VALUE="school" <?php echo $school;?> >
									<label class="req" for="introducedby_student"><?php echo $this->lang->line("is_sch_car_no");?></label>
								</div>
								<div class="col-sm-2">
									<input type="text" value="<?php echo $student["is_sch_car_no"];?>" name="school_carno" id="school_carno" placeholder="Enter Car No" class="form-control"/>
								</div>
								<div class="col-sm-2"></div>
								<div class="col-sm-2"></div>
					    	</div>
					    </div>
				    	<div class="panel-heading">	                	
		               		<h4 class="panel-title"><?php echo $this->lang->line("Academic Background");?></h4>	            
		              	</div>
		              	<div class="panel-body">
			            	<div class="col-sm-12 form_sep">
								<div class="col-sm-12">
								<?php 
									$primary = $this->std->getacademic_back($studentid,1,1);
									$low_secondary = $this->std->getacademic_back($studentid,1,2);
									$upp_secondary = $this->std->getacademic_back($studentid,1,3);
									$gen_english = $this->std->getacademic_back($studentid,2,4);									
									if(count($primary)>0){
										$primary_school = $primary->school;
										$primary_year = $primary->year;
									}else{
										$primary_school ="";
										$primary_year ="";
									}

									if(count($low_secondary)>0){
										$low_secondary_school = $low_secondary->school;
										$low_secondary_year = $low_secondary->year;
									}else{
										$low_secondary_school ="";
										$low_secondary_year ="";
									}
									if(count($upp_secondary)>0){
										$upp_secondary_school = $upp_secondary->school;
										$upp_secondary_year = $upp_secondary->year;
									}else{
										$upp_secondary_school ="";
										$upp_secondary_year ="";
									}
									if(count($gen_english)>0){
										$education = $gen_english->education;
										$gen_english_school = $gen_english->school;
										$gen_english_year = $gen_english->year;
									}else{
										$education ="";
										$gen_english_school =""; 
										$gen_english_year ="";
									}
									$Beginner ="";
									$Elementary =""; 
									$Pre_intermediate ="";  
									$Intermediate ="";
									$Upper_intermediate ="";  
									$Advanced =""; 


									if($education =="Beginner"){
										$Beginner = ' checked="checked"';
									}
									if($education =="Elementary"){
										$Elementary = ' checked="checked"';
									}
									if($education =="Pre-intermediate"){
										$Pre_intermediate = ' checked="checked"';
									}
									if($education =="Intermediate"){
										$Intermediate = ' checked="checked"';
									}
									if($education =="Upper-intermediate"){
										$Upper_intermediate = ' checked="checked"';
									}
									if($education =="Advanced"){
										$Advanced = ' checked="checked"';
									}
								?>
								<table class="table table-bordered table-inverse" id="academic_background">
				                   		<thead>
				                   		<tr>
				                   			<th><?php echo $this->lang->line("acadtype");?></th>
				                   			<th><?php echo $this->lang->line("`name`");?></th>
				                   			<th><?php echo $this->lang->line("sch_year");?></th>
				                   		</tr>
				                   		</thead>
				                   		<tbody>
				                   		<tr>
				                   			<td>
				                   				<input type="hidden" name="edu_primary" id="edu_primary" value="Primary School(Grade1-6)" />
				                   				<label class="req"><?php echo $this->lang->line("Primary School(Grade1-6)");?></label>
				                   			</td>
				                   			<td><input type="text"  name="primary_sch_name" value="<?php echo $primary_school;?>" id="primary_sch_name"  class="form-control"/></td>
				                   			<td><input type="text" name="primary_sch_ayear" value="<?php echo $primary_year;?>" id="primary_sch_ayear"  class="form-control"/></td>
				                   		</tr>
				                   		<tr>
				                   			<td>
				                   				<label class="req"><?php echo $this->lang->line("Lower Secondary School(Grade7-9)");?></label>
				                   				<input type="hidden" name="edu_Lower" id="edu_Lower" value="Lower Secondary School(Grade7-9)" />
				                   			</td>
				                   			<td><input type="text" name="lower_sch_name" value="<?php echo $low_secondary_school;?>" id="lower_sch_name"  class="form-control"/></td>
				                   			<td><input type="text" name="lower_sch_ayear"  value="<?php echo $low_secondary_year;?>"  id="lower_sch_ayear"  class="form-control"/></td>
				                   		</tr>
				                   		<tr>
				                   			<td>
				                   				<input type="hidden" name="edu_upper" id="edu_upper" value="Upper Secondary School(Grade10-12)" />
				                   				<label class="req"><?php echo $this->lang->line("Upper Secondary School(Grade10-12)");?></label>
				                   			</td>
				                   			<td><input type="text" name="upper_sch_name" value="<?php echo $upp_secondary_school;?>"  id="upper_sch_name"  class="form-control"/></td>
				                   			<td><input type="text" name="upper_sch_ayear"  value="<?php echo $upp_secondary_year;?>" id="upper_sch_ayear"  class="form-control"/></td>
				                   		</tr>
				                   		</tbody>
				                   </table>
								</div>						
					    	</div>
					    </div>
				    	<div class="panel-heading">
		                	<h4 class="panel-title"><?php echo $this->lang->line("General English");?></h4>
		              	</div>
		              	<div class="panel-body">
					    	<div class="col-sm-12 form_sep">
								<div class="col-sm-12">
									<table class="table table-bordered table-inverse" id="general_background">
				                   		<tr>
				                   			<th><?php echo $this->lang->line("Current Level Of English");?></th>
				                   			<th><?php echo $this->lang->line("`name`");?></th>
				                   			<th><?php echo $this->lang->line("sch_year");?></th>
				                   		</tr>
				                   		<tr>
				                   			<td rowspan="2">
				                   				<div class="radio">
				                   					<label class="req" for="beginner"><input type="radio" checked class="beginner" name="general_currentlevel" <?php echo $Beginner; ?> value="Beginner" id="beginner">Beginner</label>
				                   					<label class="req" for="elementary"><input type="radio" class="elementary" name="general_currentlevel" <?php echo $Elementary; ?>  value="Elementary" id="elementary">Elementary</label>
				                   					<label class="req" for="pre_intermediate"><input type="radio" class="pre_intermediate" name="general_currentlevel" <?php echo $Pre_intermediate; ?>  value="Pre-intermediate" id="pre_intermediate">Pre-Intermediate</label>
				                   					<label class="req" for="intermediate"><input type="radio" class="intermediate" name="general_currentlevel" <?php echo $Intermediate; ?>  value="Intermediate" id="intermediate">Intermediate</label>
				                   					<label class="req" for="upper_intermediate"><input type="radio" class="upper_intermediate" name="general_currentlevel" <?php echo $Upper_intermediate; ?> value="Upper-intermediate" id="upper_intermediate">Upper-Intermediate</label>
				                   					<label class="req" for="advanced"><input type="radio" class="advanced" name="general_currentlevel" <?php echo $Advanced; ?> value="Advanced" id="advanced">Advanced</label>
				                   				</div>
				                   			</td>
				                   			<td>
				                   				<input type="text" name="general_sch_name"  value="<?php echo $gen_english_school; ?>"  id="general_sch_name"  class="form-control"/>
				                   			</td>
				                   			<td>
				                   				<input type="text" name="general_sch_ayear"  value="<?php echo $gen_english_year; ?>"  id="general_sch_ayear"  class="form-control"/>
				                   			</td>
				                   		</tr>
				                   		<tr class="hide">	                   			
				                   			<td>
				                   				<input type="text" name="general_sch_name1" id="general_sch_name1"  class="form-control"/>
				                   			</td>
				                   			<td>
				                   				<input type="text" name="general_sch_ayear1" id="general_sch_ayear1"  class="form-control"/>
				                   			</td>
				                   		</tr>	                   		
				                    </table>
								</div>
					    	</div>		  					

					    	<div class="panel-heading hide">
			                	<h4 class="panel-title">More info...</h4>
			              	</div>
			            	<div class="col-sm-12 form_sep hide">
								<div class="col-sm-2"></div>
								<div class="col-sm-2"></div>
								<div class="col-sm-2"></div>
								<div class="col-sm-2"></div>
								<div class="col-sm-2"></div>
								<div class="col-sm-2"></div>
					    	</div>
					    </div>


				    	<!-- login -->
						<div class="panel-heading">
	  						<h4 class="panel-title">Login Options</h4>
	  					</div>
	  					<div class="panel-body" style="padding: 5px;">
	  						<input type="hidden" id="userid" name="userid" value="<?= (isset($query['userid']) ? $query['userid'] : '') ?>">
	  						<div class="col-sm-6">
	      						<div class="form_sep">
	      							<label>User Login</label>	      							
	      							<input type="text" class="form-control" name="txtu_name" id="txtu_name" value="<?= (isset($query['user_name']) ? $query['user_name'] : ''); ?>" placeholder="Enter Username" /> 
	      						</div>
	      						
	      						<div class="panel panel-default">
									<div class="panel-heading" style="">
										<label class="checkbox-inline">
	                                    	<input type="checkbox" id="chkReset" name="chkReset">Reset Password
	                                	</label>
									</div>
									<div id="authentication_panel" class="panel-body hidden">
										<div class="form_sep">
			      							<label>New Password</label>
			      							<div class="input-group">
			                                    <input type="password" class="form-control" name="txtpwd" id="txtpwd" value="<?= (isset($query['password']) ? $query['password'] : '') ?>" placeholder="Enter New Password" /> 
			                                    <span class="input-group-btn">
			                                        <button type="button" id="btnShowPassword" class="btn btn-default">
			                                            <span class="glyphicon glyphicon-eye-close"></span>
			                                        </button>
			                                    </span>
			                                </div>

			      						</div>
									</div>
								</div>
										      						
	  						</div>
	  						<div class="col-sm-6">
	  							<div class="form_sep">
	      							<label>Role</label>
	      							<select name='cborole' id='cborole' class="form-control">
										<?php
										foreach ($this->role->getallrole() as $role_row) {?>
											<option value='<?php echo $role_row->roleid; ?>' <?php if((isset($query['roleid']) ? $query['roleid'] : '')==$role_row->roleid) echo 'selected';?>> <?php echo $role_row->role ; ?></option>
										<?php }
										?>
									</select>
	      						</div>
	      						<div class="form_sep">
	      							<label>School Level</label>
	      							<select name="cboschlevel[]" id="cboschlevel" class="form-control" multiple="multiple">
	      								<?php foreach ($this->db->get('sch_school_level')->result() as $schl) {

											$selec = $this->db->query("SELECT
																				COUNT(u.userid) AS c
																			FROM
																				sch_user AS u
																			LEFT JOIN sch_user_schlevel AS l ON u.userid = l.userid
																			WHERE
																				u.emp_id = '{$query['emp_id']}'
																			AND l.schlevelid = '{$schl->schlevelid}' ")->row()->c - 0;
										 ?>
											<option value="<?php echo $schl->schlevelid ?>" <?php if($selec>0) echo 'selected' ?>><?php echo $schl->sch_level ?></option>";
										<?php } ?>
	      							</select>
	      						</div>
	  						</div>
						</div>
						
						<!-- start up -->
						<div class="col-sm-12" style="padding: 23px;">
							<div class="panel panel-default">
								<div class="panel-heading" style="">
									<h5 class="panel-title">Startup Page</h5>
								</div>
								<div class="panel-body">
									<div class="col-sm-4" style="">
										<div class="form_sep">
											<label for="dashboard">Dashboard</label>
			      							<select  class="form-control months" name="dashboard" id="dashboard" >
												<?php foreach ($dash as $key => $value) { ?>
														<option value='<?PHP echo $value ?>' <?php if((isset($query['def_dashboard']) ? $query['def_dashboard'] : '')==$value) echo 'selected'; ?>><?php echo $key ?> </option>";
												<?php 	} ?>
											</select>
			      						</div>
		      						</div>
		      						<div class="col-sm-4" style="">
										<div class="form_sep">
											<label>Module</label>
			      							<select  class="form-control months" name="moduleallow[]" id="moduleallow" >
												<option></option>
											</select>
			      						</div>
		      						</div>
		      						<div class="col-sm-4" style="">
										<div class="form_sep">
											<label for="emailField">Def. Page</label>
			      							<select  class="form-control months"  name="defpage" id="defpage" >
											</select>
			      						</div>
		      						</div>
								</div>
		  					</div>
	  					</div>


		            </div>
		        </div>


	        <div class="row">
	          <div class="col-sm-5">
	            <div class="form_sep">
	              <?php echo $objbtn;?>
	            </div>
	          </div>
	        </div>        
	      </form>
	    </div>                      <!-- /.modal-content -->
    </div>                     <!-- /.modal-dialog -->
</div>

<!-- start new dialog  -->

<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $this->lang->line("New Family Information");?></h4>
            </div>
            <div class="modal-body ">
                <div class="stdscrollbar">
                    <form method="post" class="gform frmaddrespond" accept-charset="utf-8" id="frmaddrespond">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel-body">
                                    <div class="form-group" id="exist" style="display:none">Data is already exist,
                                        Please try again...!
                                    </div>
                                    <!-- Start Form  -->
                                    <div class="panel-body">
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_id"><?php echo $this->lang->line("familyid");?><span
	                                                    style="color:red">*</span></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" required class="form-control parsley-validated"
	                                                   name="d_family_id" value="" id="d_family_id">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_name"><?php echo $this->lang->line("family_name");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" class="form-control parsley-validated"
	                                                   name="d_family_name" value="" id="d_family_name">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_book_no"><?php echo $this->lang->line("family_book_record_no");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" class="form-control parsley-validated"
	                                                   name="d_family_book_no" value="" id="d_family_book_no">
	                                        </div>
	                                    </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_home"><?php echo $this->lang->line("home_no");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_home" id="d_family_home"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_street"><?php echo $this->lang->line("street");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_street" id="d_family_street"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_village"><?php echo $this->lang->line("village");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_village" id="d_family_village"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_commune"><?php echo $this->lang->line("commune");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_commune" id="d_family_commune"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_district"><?php echo $this->lang->line("district");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_district" id="d_family_district"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_province"><?php echo $this->lang->line("province");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_province" id="d_family_province"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>
	                                </div>
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><?php echo $this->lang->line("Father Information");?></h4>
                                    </div>
                                    <div class="panel-body">
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_name"><?php echo $this->lang->line("father_name");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" value="" class="form-control parsley-validated"
	                                                   name="d_father_name" id="d_father_name">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_occupation"><?php echo $this->lang->line("father_ocupation");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" class="form-control parsley-validated"
	                                                   name="d_father_occupation" value="" id="d_father_occupation">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="father_dob"><?php echo $this->lang->line("father_dob");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <div data-date-format="dd-mm-yyyy" class="input-group date dob">
	                                                <input type='text' id="d_father_dob" class="form-control"
	                                                       name="d_father_dob" placeholder="dd-mm-yyyy"/>
													    	<span class="input-group-addon"><span
	                                                                class="glyphicon glyphicon-calendar">
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="col-sm-12 form_sep">
                                            <div class="col-sm-2">
                                                <label class="req" for="d_father_name_eng">Father's Name</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" value="" class="form-control parsley-validated"
                                                       name="d_father_name_eng" id="d_father_name_eng">
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="req" for="d_father_occupation_eng">Occupation</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" value="" class="form-control parsley-validated"
                                                       name="d_father_occupation_eng" id="d_father_occupation_eng">
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="req" for="d_father_phone"><?php echo $this->lang->line("father_phone");?></label>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" name="d_father_phone" id="d_father_phone"
                                                       class="form-control"/>
                                            </div>
                                        </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_home"><?php echo $this->lang->line("father_home");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_home" id="d_father_home"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_street"><?php echo $this->lang->line("father_street");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_street" id="d_father_street"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_village"><?php echo $this->lang->line("father_village");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_village" id="d_father_village"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_commune"><?php echo $this->lang->line("father_commune");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_commune" id="d_father_commune"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_district"><?php echo $this->lang->line("father_district");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_district" id="d_father_district"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_province"><?php echo $this->lang->line("father_province");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_province" id="d_father_province"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>	                                    
	                                </div>
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><?php echo $this->lang->line("Mother Information");?></h4>
                                    </div>
                                    <div class="panel-body">
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_name"><?php echo $this->lang->line("mother_name");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" onblur="" value="" class="form-control parsley-validated"
	                                                   name="d_mother_name" id="d_mother_name">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_occupation"><?php echo $this->lang->line("mother_ocupation");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" value="" class="form-control parsley-validated"
	                                                   name="d_mother_occupation" id="d_mother_occupation">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_dob"><?php echo $this->lang->line("mother_dob");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <div data-date-format="dd-mm-yyyy" class="input-group date dob">
	                                                <input type='text' id="d_mother_dob" class="form-control"
	                                                       name="d_mother_dob" placeholder="dd-mm-yyyy"/>
														    <span class="input-group-addon"><span
	                                                                class="glyphicon glyphicon-calendar">
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_name_eng">Mother' Name</label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" onblur="" value="" class="form-control parsley-validated"
	                                                   name="d_mother_name_eng" id="d_mother_name_eng">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_occupation_eng">Occupation</label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" value="" class="form-control parsley-validated"
	                                                   name="d_mother_occupation_eng" id="d_mother_occupation_eng">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_phone"><?php echo $this->lang->line("mother_phone");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_phone" id="d_mother_phone"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_home"><?php echo $this->lang->line("mother_home");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_home" id="d_mother_home"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_street"><?php echo $this->lang->line("mother_street");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_street" id="d_mother_street"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_village"><?php echo $this->lang->line("mother_village");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_village" id="d_mother_village"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_commune"><?php echo $this->lang->line("mother_commune");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_commune" id="d_mother_commune"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_district"><?php echo $this->lang->line("mother_district");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_district" id="d_mother_district"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_province"><?php echo $this->lang->line("mother_province");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_province" id="d_mother_province"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>	                                    
	                                </div>
                                    <!-- end form  -->

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="dbtnclose">Close</button>
                <button type="button" class="btn btn-primary" id="dsavefamily">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- end new dialog  -->
<div class="modal" id="myModalhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Print Card History</h4>
            </div>
            <div class="modal-body ">
                <div class="stdscrollbar">
                    <form method="post" class="gform frmaddrespond" accept-charset="utf-8" id="frmaddrespond">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel-body">
                                    <div class="form-group" id="exist" style="display:none">Data is already exist,
                                        Please try again...!
                                    </div>
                                    <!-- Start Form  -->
                                    <div class="panel-body">
	                                    <div class="col-sm-12">
	                                        <table class="table table-bordered table-inverse" id="card_history">
						                		<thead>
						                		<tr>
						                			<th style="text-align: center !important; width: 17%"><label class="req">No</label></th>
		                                            <th style="text-align: center !important; width: 17%"><label class="req">Card ID</label></th>
		                                            <th style="text-align: center !important; width: 17%"><label class="req">Academic Year</label></th>
		                                            <th style="text-align: center !important; width: 17%"><label class="req">Print Date</label></th>
													<th style="text-align: center !important; width: 17%"><label class="req">Print By</label></th>
		                                        </tr>
						                		</thead>
						                		<tbody id="body_card_history">
						                		<?php 
						                			$i = 1;
						                				$sql_card = $this->std->getcardhistory($studentid);
						                				if(count($sql_card)>0){
						                					foreach($sql_card as $row_card){				                					
						                					echo '<tr>				                							
														            <td align="center">
														            	<label>'.$i.'</label>
														            </td>
														            <td>
														            	<label>'.$row_card["card_id"].'</label>
														            </td>
														            <td>
														            	<label>'.$row_card["academic_year"].'</label>
														            </td>
														            <td>
														            	<label>'.$row_card["print_date"].'</label>
														            </td>
														            <td>
														            	<label>'.$row_card["print_by"].'</label>
														            </td>												            
													            </tr>';
													            $i++;
						                					}
						                				}else{
						                					echo '<tr>				                							
														            <td colspan="5" align="center">
														            	<label>No Results</label>
														            </td>												            												            
													            </tr>';
						                				}
						                		?>
						                		</tbody>
						                	</table>
	                                    </div>
	                                </div>
                                    
                                    <!-- end form  -->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="dhisbtnclose">Close</button>                
            </div>
        </div>
    </div>
</div>
<!-- end new dialog  -->
<script type="text/javascript">
	function loadPage(defpage=''){
		$.ajax({
			type : "POST",
			url  : "<?php echo site_url('setting/user/getPage')?>",
			data: {
				'moduleid' : $("#moduleallow").val(),
				'defpage'  : defpage
			},
			success:function(data){
				$('#defpage').html(data);
			}
		});
	}
	function LoadModule(moduleid=''){
		$.ajax({
			type : "POST",
			url  : "<?php echo site_url('setting/user/getModule')?>",
			dataType: 'json',
			async: false,
			data: {
				'roleid' : $("#cborole").val(),
				'moduleid': moduleid
			},
			success:function(data){
				$('#moduleallow').html(data.op);
				$('#defpage').html(data.oppage);
			}
		});
	}//End Load Module

	$(document).on('change', '.stufiles', function() {        
        var tr = $(this).parent().parent().parent();
	    var input = $(this),
	        numFiles = input.get(0).files ? input.get(0).files.length : 1,
	        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	        input.trigger('fileselect', [numFiles, label]);
            log = numFiles > 1 ? numFiles + ' files selected' : label;                
            
            if( input.length ) {
                tr.find('.getfilename').val(log);
            } else {
                console.log(log);
            }
	});

	  // We can watch for our custom `fileselect` event like this
	$(document).ready( function() {
        $('.stufiles').on('fileselect', function(event, numFiles, label) {
              var tr = $(this).parent().parent().parent();
              var input = tr.find('.getfilename'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;
              if( input.length ) {
                  input.val(log);
              } else {
                  console.log(log);
              }

          });
	});

	$(function () {
		
		LoadModule("<?php echo $moduleid?>");
		loadPage("<?php echo (isset($query['def_open_page']) ? $query['def_open_page'] : '') ?>");
		$(document).on('change','#cborole',function(){
			LoadModule();
		});
		$(document).on('change','#moduleallow',function(){
			loadPage();
		});

		var familyid=$("#familyid").val();
		var mem_stdid=$("#student").val();
		//if(familyid!='' && mem_stdid!=''){
			//getlistmember(familyid,mem_stdid);
			//fillrevenue(familyid);
		//}
		$("#leave_school").click(function(){
			if($(this).is(":checked")){
				$(this).val(1);
			}else{
				$(this).val(0);
			}
           
        });

        //is_leave();
		//isvtc();

	 	$("#enter_id_card").keyup(function (e) {
	 		var enter_id = $(this).val();
	 		var card_id = enter_id.replace(/;/gi, "");
	 		var card_ids = card_id.replace(/\?/gi, "");
	        $("#id_card").val(card_ids);		
    	});

	    $('#year').change(function(){
			$('.year').val($('#year').val());
		});

	    $('#id_year').change(function(e){
            checkstudentid(e);
        });

		$('#std_reg_submit').on('click',function(event){			 
			var j= 0;
			var n= 0;
			$(".allstucture").each(function(j){
				n = j+1;
			});			
			if(n>0){
			 	if(confirm("Are you sure? You want to upate !")){
					$('#std_register').submit();
			 	}else{				
			 		return false;
			 	}
			}else{
			 	toastr["warning"]("Please add program!");
			 	return false;
			}			 
		});
		$('#sch_program').change(function () {
            var programid = $(this).val();
            if (programid != "") {
                getsch_level(programid);                
            } else {
                $('#sch_level').html("");
                $('#sch_year').html("");
                $('#sch_rangelevel').html("");
                $('#sch_class').html("");
            }
        });
        $('#sch_rangelevel').change(function () {
           $.ajax({
	            url: "<?php echo base_url(); ?>index.php/student/student/getgradeLevel",
	            dataType:"HTML",
	            data: {
	            		'schlevel':$("#sch_level").val(),
	            		'sch_program': $("#sch_program").val()
	            },
	            async: false,
	            type: "post",
	            success: function (data) {
	               $("#gradlevel").html(data);
	            }
	        });
        });
        // $('#gradlevel').change(function () {
        // 	var sch_levelid = $("#sch_level").val();
        // 	var gradlevel   = $("#gradlevel").val();
        //   	getsch_class(sch_levelid,gradlevel);
        // });
        $('#sch_level').change(function () {
            var programid = $('#sch_program').val();
            var sch_levelid = $(this).val();
            if (sch_levelid != "") {
                getsch_year(programid, sch_levelid);
                getsch_rangelevel(programid, sch_levelid);
                getsch_class(sch_levelid);
            } else {
                $('#sch_year').html("");
            }
        });
        $("body").delegate("#add_academic", "click", function () {
            var program = $("#sch_program").val();
            var sch_level = $("#sch_level").val();
            var sch_year = $("#sch_year").val();
            var sch_rangelevel = $("#sch_rangelevel").val();
            var sch_class = $("#sch_class").val();

            if (program == "") {
                toastr["warning"]("Please select program!");
            }else if(sch_level=="" || !sch_level){
				toastr["warning"]("Please select school level!");
            }else if(sch_year=="" || !sch_year ){
            	toastr["warning"]("Please select school year!");
            }else{
                addacademic();
            }
        });
        $("body").delegate(".link_del_time", "click", function (){
        	var tr = $(this).parent().parent();
        	var CheckVal = tr.children("td").find("#is_new").val();
        	if(CheckVal=="0"){
	        	if(confirm("Are you sure ! You want to delete?")){	   

	        		var studentid= $("#studentid").val();
	        		var sch_programid= tr.children("td").find(".sch_programid").val();
	        		var sch_levelid= tr.children("td").find(".sch_levelid").val();
	        		var sch_yearid= tr.children("td").find(".sch_yearid").val();
	        		var sch_rangelevelid= tr.children("td").find(".sch_rangelevelid").val();
	        		var sch_classid= tr.children("td").find(".sch_class").val();

		        	$.ajax({
			            url: "<?php echo base_url(); ?>index.php/student/student/cdelete_acdimice",
			            data: {"studentid":studentid, "sch_programid":sch_programid, "sch_levelid":sch_levelid, "sch_yearid":sch_yearid, "sch_rangelevelid":sch_rangelevelid, "sch_classid":sch_classid},
			            async: false,
			            type: "post",
			            success: function (data) {
			            	toastr["success"]("Data has been deleleted!"); 
			            }
			        });
			        $(this).closest("tr").remove();		
	       		 }else{
	       		 	return false;
	       		 }
            }else{
            	$(this).closest("tr").remove();
            }/////////if(CheckVal=="0")

        });
        $("body").delegate("#add_family", "click", function () {
            clearDialog();
        });
        $("body").delegate("#dsavefamily", "click", function () {
            SaveNewFamily();
            clearDialog();
        });
        $("body").delegate("#btnclose", "click", function () {
            clearDialog();
        });
		$('#std_register').parsley();		
		autofillfamily();
		//autofillsponsor();

		$("#chkReset").change(function () {
			if($(this).is(":checked")){
                $("#authentication_panel").removeClass('hidden');
            }else{
                $("#authentication_panel").addClass('hidden');
            }
		});

		var showPassword = false;

        $("#btnShowPassword").click( function() {
            if (showPassword) {
                $("#btnShowPassword span:first-child").removeClass("glyphicon-eye-open");
                $("#btnShowPassword span:first-child").addClass("glyphicon-eye-close");
                $("#txtpwd").attr("type", "password");
                showPassword = false;
            } else {
                $("#btnShowPassword span:first-child").removeClass("glyphicon-eye-close");
                $("#btnShowPassword span:first-child").addClass("glyphicon-eye-open");
                $("#txtpwd").attr("type", "text");
                showPassword = true;
            }
            $("#txtpwd").focus();
        });


		$(".res_dob,.mem_dob,#leave_date,#register_date, #reqister, #dob, #d_mother_dob, #d_father_dob").datepicker({
      		language: 'en',
      		pick12HourFormat: true,
      		format:'dd-mm-yyyy'
    	});		
		$('#d_family_home').change(function () {
            var myVal = $(this).val();
            $('#d_father_home').val(myVal);
            $('#d_mother_home').val(myVal);
        });
        $('#d_family_street').change(function () {
            var myVal = $(this).val();
            $('#d_father_street').val(myVal);
            $('#d_mother_street').val(myVal);
        });
        $('#d_family_village').change(function () {
            var myVal = $(this).val();
            $('#d_father_village').val(myVal);
            $('#d_mother_village').val(myVal);
        });
        $('#d_family_commune').change(function () {
            var myVal = $(this).val();
            $('#d_father_commune').val(myVal);
            $('#d_mother_commune').val(myVal);
        });
        $('#d_family_district').change(function () {
            var myVal = $(this).val();
            $('#d_father_district').val(myVal);
            $('#d_mother_district').val(myVal);
        });
        $('#d_family_province').change(function () {
            var myVal = $(this).val();
            $('#d_father_province').val(myVal);
            $('#d_mother_province').val(myVal);
        });

        $("body").delegate("#is_active","click",function(){
        	if($(this).is(":checked")){
        		$(this).val(1);
        	}else{
        		$(this).val(0);
        	}
        })

	});
 	function isNumberKey(evt){
        var e = window.event || evt; // for trans-browser compatibility 
        var charCode = e.which || e.keyCode; 
        if ((charCode > 45 && charCode < 58) || charCode == 8){ 
            return true; 
        } 
        return false;  
    }
	
	function SaveNewFamily() {

        var family_id = $("#d_family_id").val();
        var family_name = $("#d_family_name").val();
        var family_book_no = $("#d_family_book_no").val();
        var family_province = $("#d_family_province").val();
        var family_district = $("#d_family_district").val();
        var family_commune = $("#d_family_commune").val();
        var family_village = $("#d_family_village").val();
        var family_street = $("#d_family_street").val();
        var family_home = $("#d_family_home").val();

        var father_name = $("#d_father_name").val();
        var father_name_eng = $("#d_father_name_eng").val();
        var father_occupation = $("#d_father_occupation").val();
        var father_occupation_eng = $("#d_father_occupation_eng").val();
        var father_dob = $("#d_father_dob").val();
        var father_province = $("#d_father_province").val();
        var father_district = $("#d_father_district").val();
        var father_commune = $("#d_father_commune").val();
        var father_village = $("#d_father_village").val();
        var father_street = $("#d_father_street").val();
        var father_phone = $("#d_father_phone").val();
        var father_home = $("#d_father_home").val();

        var mother_name = $("#d_mother_name").val();
        var mother_name_eng = $("#d_mother_name_eng").val();
        var mother_occupation = $("#d_mother_occupation").val();
        var mother_occupation_eng = $("#d_mother_occupation_eng").val();
        var mother_dob = $("#d_mother_dob").val();
        var mother_province = $("#d_mother_province").val();
        var mother_district = $("#d_mother_district").val();
        var mother_commune = $("#d_mother_commune").val();
        var mother_village = $("#d_mother_village").val();
        var mother_street = $("#d_mother_street").val();
        var mother_phone = $("#d_mother_phone").val();
        var mother_home = $("#d_mother_home").val();

        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/c_savenewfamily",
            data: {
                'family_id': family_id,
                'family_name': family_name,
                'family_book_no': family_book_no,
                'family_province': family_province,
                'family_district': family_district,
                'family_commune': family_commune,
                'family_village': family_village,
                'family_street': family_street,
                'family_home': family_home,

                'father_name': father_name,
                'father_name_eng': father_name_eng,
                'father_occupation': father_occupation,
                'father_occupation_eng': father_occupation_eng,
                'father_dob': father_dob,
                'father_province': father_province,
                'father_district': father_district,
                'father_commune': father_commune,
                'father_village': father_village,
                'father_street': father_street,
                'father_phone': father_phone,
                'father_home': father_home,

                'mother_name': mother_name,
                'mother_name_eng': mother_name_eng,
                'mother_occupation': mother_occupation,
                'mother_occupation_eng': mother_occupation_eng,
                'mother_dob': mother_dob,
                'mother_province': mother_province,
                'mother_district': mother_district,
                'mother_commune': mother_commune,
                'mother_village': mother_village,
                'mother_street': mother_street,
                'mother_phone': mother_phone,
                'mother_home': mother_home
            },
            type: "post",
            success: function (data) {
                var familyid = data;
                if (familyid != "") {
                    $('#familyid').val(familyid);
                    fillfamily(familyid);
                    $('#dbtnclose').click();
                } else {
                    toastr["warning"]("Data save not success ! Please try again");
                }
            }
        });
    }
    function getsch_level(sch_program) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/cgetschlevel",
            data: {'sch_program': sch_program},
            async: false,
            type: "post",
            success: function (data) {
                $('#sch_level').html(data.schlevel);
                var sch_level = $('#sch_level').val();
                getsch_year(sch_program, sch_level);
                getsch_rangelevel(sch_program, sch_level);
                getsch_class(sch_level);
            }
        });

    }
    function checkstudentid(event) {
        var student_num = $('#id_number').val();
        var studentid = $('#studentid').val();
        //var student_num = id_year + '-' + id_number;
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/validatstudid",
            data: {'student_num': student_num,'studentid': studentid},
            type: "post",
            success: function (data) {
                if (data > 0) {
                    toastr["warning"]("Student had already exist! Please try again");
                    $('#id_number').val("");
                    $('#id_number').focus();
                }
            }
        });
    }
    function getsch_year(sch_program, sch_level) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/cgetschoolyear",
            data: {'sch_program': sch_program, 'sch_level': sch_level},
            async: false,
            type: "post",
            success: function (data) {
                $('#sch_year').html(data.schyear);
            }
        });
    }
    function getsch_rangelevel(sch_program, sch_level) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/cgetschoolrangelevel",
            data: {'sch_program': sch_program, 'sch_level': sch_level},
            async: false,
            type: "post",
            success: function (data) {
                $('#sch_rangelevel').html(data.schrangelevel);
            }
        });
    }
    function getsch_class(sch_level,gradlevel) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/cgetschoolclass",
            data: {'sch_level': sch_level,'gradlevel':gradlevel},
            async: false,
            type: "post",
            success: function (data) {
                $('#sch_class').html(data.schclass);
            }
        });
    }
    function addacademic() {
        this.sch_programid = $("#sch_program").val();
        this.sch_yearid    = $("#sch_year").val();
        this.sch_levelid   = $("#sch_level").val();
		this.sch_rangelevelid = $("#sch_rangelevel").val();
		this.sch_classid   = $("#sch_class").val();

		if(sch_rangelevelid==""){
			sch_rangelevelid = 0;
		}
		if(sch_classid ==""){
			sch_classid = 0;
		}
        this.sch_program   = $("#sch_program option:selected").text();
        this.sch_level     = $("#sch_level option:selected").text();
        this.sch_year      = $("#sch_year option:selected").text();
        this.sch_rangelevel= $("#sch_rangelevel option:selected").text();
        this.sch_class     = $("#sch_class option:selected").text();

        var newprogram = sch_programid + "_" + sch_levelid + "_" + sch_yearid+ "_" + sch_rangelevelid + "_" + sch_classid;

        this.tr = '<tr>' +
            '<td>' +
                '<div id="file" class="form-group show_hide_file"><div class="input-group col-xs-12">' + 
                '<label class="input-group-btn"><span class="btn btn-primary"> ' + 
                'Browse&hellip; <input type="file" style="display: none;" id="files" value="" class="file stufiles" name="stufiles[]"></span></label>' +
                '<input type="text" class="form-control getfilename" id="getfilename" name="getfilename[]" value=""  readonly>' +
                '</div></div>' +
            '</td>' +        
            '<td>' +
            '<input type="hidden" value="1" class="is_new"  id="is_new" name="is_new[]" >'+
            '<input type="hidden" value="" class="enrollid" name="enrollid[]" >'+
            '<input type="hidden" value="' + newprogram + '" class="allstucture" name="allstucture[]" >' +
            '<input type="hidden" class="sch_programid" name="sch_programid[]" value="' + this.sch_programid + '"/>' + this.sch_program +
            '</td>' +
            '<td><input type="hidden" class="sch_levelid" name="sch_levelid[]" value="' + this.sch_levelid + '"/>' + this.sch_level + '</td>' +
            '<td><input type="hidden" class="sch_yearid" name="sch_yearid[]" value="' + this.sch_yearid + '"/>' + this.sch_year + '</td>' +
            '<td><input type="hidden" class="sch_rangelevelid" name="sch_rangelevelid[]" value="' + this.sch_rangelevelid + '"/>' + this.sch_rangelevel + '</td>' +
            '<td><input type="hidden" class="sch_gradelevel" name="sch_gradelevel[]" value=""/></td>' +
            '<td><input type="hidden" class="sch_class" name="sch_classid[]" value="' + this.sch_classid + '"/>' + this.sch_class + '</td>' +
            '<td style="text-align:center;"><input type="checkbox" id="is_active" name="is_active" value="0"></td>'+
            '<td width="40" align="center"><a href="javascript:void(0)" class="link_del_time"><img src="<?php echo base_url("assets/images/icons/delete.png")?>" /> </a></td>' +
            '</tr>';

        if ($("#body_school_year tr").size() > 0) {
            //var newprogram = sch_programid + "_" + sch_yearid + "_" + sch_levelid;            
            var arraprogram = [];
            $(".sch_program").each(function (i) {
                var curtr = $(this).closest("tr");
                arraprogram[i] = curtr.find(".sch_program").val() + "_" + curtr.find(".sch_level").val() + "_" + curtr.find(".sch_year").val() + "_" + curtr.find(".sch_rangelevel").val() + "_" + curtr.find(".sch_class").val();
            })
            if ($.inArray(newprogram, arraprogram) == '-1') {
                var db = 1;
                var i = 1;
                $(".allstucture").each(function (j) {
                    var val_each = $(this).val();
                    if (val_each == newprogram) {
                        db = i + 1;
                        i++;
                    }
                });
                if (db > 1) {
                    toastr["warning"]("Data already exist ! Please try again");
                } else {
                    $("#body_school_year").append(this.tr);
                }
            }
        } else {
            $("#body_school_year").append(this.tr);
        }
    }
	function isvtc(event){
		var classid=$("#classid").val();
		$.ajax({
                    url:"<?php echo base_url(); ?>index.php/student/student/getschlevel",    
                    data: {'classid':classid},
                    type:"post",
                    success: function(data){
                   	if(data==1){
                   		$('#promo_sep').removeClass('hide');
                   		var familyid=$('#familyid').val();
                   		fillstudent(familyid);
                   	}else{
                   		$('#promo_sep').addClass('hide');
                   	}
                }
            });
	}
	function fillusername(event){
		var f_name=$('#first_name').val();
		var l_name=$('#last_name').val();
		var username=f_name+'.'+l_name;
			$.ajax({
                    url:"<?php echo base_url(); ?>index.php/student/student/validateuser",    
                    data: {'username':username},
                    type:"post",
                    success: function(data){
                   if(data>0){
                   		$('#login_username').val(username+'1');
                   }else{
                   		$('#login_username').val(username);
                   }
                }
            });
	}
	function fillpwd(event){
		var pwd=$('#student_num').val();
		
		var student_num=$(event.target).val();
		//alert(student_num);
		$.ajax({
                            url:"<?php echo base_url(); ?>index.php/student/student/getstdbyid",    
                            data: {'std_num':student_num},
                            type:"post",
                            success: function(data){
                           	if(data==1){
                           		$('.error').html('Student ID :'+ student_num+' is already used please choose other ID');
                           		$('#student_num').val(''); 
                           	}else{
                           		$('#password').val(pwd);
                           		$('.error').html('');
                           	}
                        }
                    });
	}
	
	function is_leave(event) {
        if ($("#leave_school").is(':checked')) {
            $(this).val("1");
        } else {
            $(this).val("0");           
        }
    }
	
	function autofillfamily(){		
		var fillrespon = "<?php echo site_url("student/student/c_fillfamily")?>";
        $(".family_id").autocomplete({
            source: fillrespon,
            minLength: 0,
            select: function (events, ui) {
                var f_id = ui.item.id;
                var family_code = ui.item.family_code;
                $("#familyid").val(f_id);
                $("#family_id").val(family_code);
                fillfamily(f_id);
            }
        });
	}
	function fillfamily(familyid) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/cgetfamilyrow",
            data: {
                'familyid': familyid
            },
            type: "post",
            success: function (data) {

                $("#familyid").val(data.familyid);
                $("#family_id").val(data.family_code);
                $("#family_name").val(data.family_name);                
                $("#family_book_no").val(data.family_book_record_no);

                $("#father_name").val(data.father_name_kh);
                $("#father_name_eng").val(data.father_name);
                $("#father_occupation_eng").val(data.father_ocupation);
                $("#father_occupation").val(data.father_ocupation_kh);
                $("#father_dob").val(data.father_dob);
                $("#father_province").val(data.father_province);
                $("#father_district").val(data.father_district);
                $("#father_commune").val(data.father_commune);
                $("#father_village").val(data.father_village);
                $("#father_street").val(data.father_street);
                $("#father_phone").val(data.father_phone);
                $("#father_home").val(data.father_home);

                $("#mother_name").val(data.mother_name_kh);
                $("#mother_name_eng").val(data.mother_name);
                $("#mother_occupation").val(data.mother_ocupation_kh);
                $("#mother_occupation_eng").val(data.mother_ocupation);
                $("#mother_dob").val(data.mother_dob);
                $("#mother_province").val(data.mother_province);
                $("#mother_district").val(data.mother_district);
                $("#mother_commune").val(data.mother_commune);
                $("#mother_village").val(data.mother_village);
                $("#mother_street").val(data.mother_street);
                $("#mother_phone").val(data.mother_phone);
                $("#mother_home").val(data.mother_home);

                $("#guard_name").val(data.father_name_kh);
                $("#guard_name_eng").val(data.father_name);
                $("#guard_occupation").val(data.father_ocupation_kh);
                $("#guard_occupation_eng").val(data.father_ocupation);
                $("#guard_province").val(data.father_province);
                $("#guard_district").val(data.father_district);
                $("#guard_commune").val(data.father_commune);
                $("#guard_village").val(data.father_village);
                $("#guard_street").val(data.father_street);
                $("#guard_phone").val(data.father_phone);
                $("#guard_home").val(data.father_home);
            }
        });
    }
    function clearDialog() {
        $("#d_family_id").val("");
        $("#d_family_name").val("");
        $("#d_family_book_no").val("");
        $("#d_family_province").val("");
        $("#d_family_district").val("");
        $("#d_family_commune").val("");
        $("#d_family_village").val("");
        $("#d_family_street").val("");
        $("#d_family_home").val("");

        $("#d_father_name").val("");
        $("#d_father_occupation").val("");
        $("#d_father_dob").val("");
        $("#d_father_province").val("");
        $("#d_father_district").val("");
        $("#d_father_commune").val("");
        $("#d_father_village").val("");
        $("#d_father_street").val("");
        $("#d_father_phone").val("");
        $("#d_father_home").val("");

        $("#d_mother_name").val("");
        $("#d_mother_occupation").val("");
        $("#d_mother_dob").val("");
        $("#d_mother_province").val("");
        $("#d_mother_district").val("");
        $("#d_mother_commune").val("");
        $("#d_mother_village").val("");
        $("#d_mother_street").val("");
        $("#d_mother_phone").val("");
        $("#d_mother_home").val("");
    }
	function updatemem(event){
		$('#update_mem').val('yes');
	}
    function upresult(){
        toastr["success"]("Student was updated.");
    }
</script>
	
		
	
