<div class="wrapper">
 	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
    		<div class="result_info">
                <div class="col-sm-6">
                    <strong>Multiple Subject Score Entry</strong>                    
                </div>
            </div>
			<!--form element-->
	      	<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" class="tdrow" id="frm_subject_score_kgp">
	      		<!--form search of student information-->
	      		<div class="col-sm-12">
		        	<div class="row">
		        		<div class="panel panel-default">
		        			<div class="panel-body" style="padding: 5px;">
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_id">School</label>
					        			<select name="school_id" id="school_id" minlength='1' class="form-control">
					                    	<?php
												$row = $this->school->getschinfor_row(1);										
												echo '<option value="'. $row->schoolid .'">'. $row->name .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="program_id">Program</label>
					        			<select name="program_id" id="program_id" minlength='1' class="form-control">
					                    	<?php
												$row = $this->program->getprogram(1);											
												echo '<option value="'. $row->programid .'">'. $row->program .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_level_id">School Level<span style="color:red">*</span></label>
					        			<select name="school_level_id" id="school_level_id" minlength='1' class="form-control" required data-parsley-required-message="This field require">
		                                    <option value=""></option>
					                    	<?php
												$get_level_data = $this->school_level->getsch_level(1);												
												foreach($get_level_data as $r_level){
													echo '<option value="'. $r_level->schlevelid .'">'. $r_level->sch_level .'</option>';
												}
											?>	                      
		                                </select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="adcademic_year_id">Academic Year<span style="color:red">*</span></label>
	                                    <select name="adcademic_year_id" id="adcademic_year_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="grade_level_id">Grade Level<span style="color:red">*</span></label>
	                                    <select name="grade_level_id" id="grade_level_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4" style="display: none;">
					        		<div class="form-group">
					        			<label for="teacher_id">Teacher Name<span style="color:red"></span></label>					        			
	                                    <select name="teacher_id" id="teacher_id" minlength='1' class="form-control" _required data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>					        	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="class_id">Class Name<span style="color:red">*</span></label>					        			
	                                    <select name="class_id" id="class_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4" style="display: none;">
					        		<div class="form-group">
					        			<label for="subject_id">Subject Name<span style="color:red">*</span></label>
					        			<input type="hidden" name="subject_group_id" id="subject_group_id">
					        			<input type="hidden" name="subject_main_id" id="subject_main_id">					        			
	                                    <select name="subject_id" id="subject_id" minlength='1' class="form-control" _required data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>					        					        	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="student_id">Student Name</label>
	                                    <select name="student_id" id="student_id" class="form-control"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-12">
					        		<div class="form-group">
					        			<label class="req" for="student_num">Examp Type<span style="color:red">*</span></label><br>
					        			<div class="form_exam_type" id='form_semester' style="text-align:center; border:1px #337ab7 solid; padding:5px 5px 0 5px;"> 						
											<table border="0"​ align="center" id='tbl-exam-type' class="tbl-exam-type table table-bordered">
												<tr style="color:#337ab7 !important;">
					        						<td>
					        							<label class="radio-inline">
			                                            	<input type="radio" name="select_exam_type" value='1' class='select_exam_type'>Monthly
			                                            </label> 
					        						</td>
					        						<td>
					        							<label class="radio-inline">
			                                            	<input type="radio" name="select_exam_type" value='2' class='select_exam_type'>Semester
			                                            </label> 
					        						</td>
					        						<td>
					        							<label class="radio-inline">
			                                            	<input type="radio" name="select_exam_type" value='3' class='select_exam_type'>Final
			                                            </label> 
					        						</td>
					        					</tr>
					        					<tr class="show_extam_type" style="display:none;"></tr>
					        					<tr class="show_mess_alert" style="display:none;"></tr>		
											</table>
										</div>                                    
					        		</div>
					        	</div>					        	
		        			</div>
		        		</div>
		        	</div>
		        </div><!--end form search of student information-->
		        
		        <!--button-->
		        <div class="col-sm-12">	
		        	<div class="row">
		        		<div class="form-group" style="text-align:center;">
		                <?php if($this->green->gAction("R")){ ?>
		                	<input type="button" name="btnsearch" id='btnsearch' value="Search" class="btn btn-primary btn-sm" disabled />
		                <?php } ?>
		                <?php if($this->green->gAction("C")){ ?>
		                	<input type="button" name="btnsave" id='btnsave' value="Save" class="btn btn-primary btn-sm" disabled />
		                <?php } ?>
		                <button type="button" name="btncancel" id="btncancel" value="Refresh" class="btn btn-warning btn-sm" title="Refresh" /><span class="glyphicon glyphicon-refresh"></span></button>
	               </div> 
		        	</div>	                                           
		        </div><!--end button-->


		        <!--show student form detail-->
		        <div class="col-sm-12 student_list" style="_margin-top: -5px;display: none;">
		        	<div class="row">
		        		<div class="panel panel-success">
		        			<div class="panel-body">

		        				<div class="panel panel-default">
								  <div class="panel-body">
								  		<form class="form-horizontal">
										  <div class="form-group">
										    <label for="co-effecient" class="col-sm-2 control-label">Co-effecient</label>
										    <div class="col-sm-2">
										      <input type="text" class="form-control input-sm" name="co_effecient" id="co_effecient" placeholder="Co effect" value="1" placeholder="Co-effecient">
										    </div>
										  </div>
										</form>
								  </div>
								</div>

		        				<!-- list -->
								<div class="col-sm-12" style="padding: 0;">
						        	<div class="table-responsive">
						            <div id="tab_print">
						               <table border="0"​ align="center" id="score_list" class="table table-hover table-condensed table-bordered" style="width: 100%;" cellpadding="0" cellspacing="0">
						                  <thead _style="background: #337ab7;color: white;">

						                  </thead>
						                   
						      				<tbody>
						      				</tbody>

						      				<tfoot>
						      				</tfoot>
						               </table>
						            </div>         
						        	</div>
					    		</div>
						        
		        			</div>
			        	</div>
		        	</div>	        	
		        </div>

	      	</form>
	  	</div>
	</div>
</div>

<style>
	#score_list th {vertical-align: middle;}
   #score_list td {vertical-align: middle;padding: 0 !important;}

   .input-xs {
       height: 24px;
       padding: 2px;
       font-size: 11px;
       line-height: 1.5;
       border-radius: 0;
       text-align: center;
       min-width: 44px;
       border: 0;
   }


	.tbl-exam-type td{
		text-align: left;
		vertical-align: middle !important;
		border: hidden !important;		
	}

	.table-student-detail th{
		text-align: center;
		vertical-align: middle !important;
		background:#337ab7;
		color: #FFF;
	}
	.table-student-detail td{
		text-align: right;
		vertical-align: middle !important;
	}
	.title-info{
		 border:2px #FFD700 solid;
		 padding:5px 5px 5px 5px;
		 margin-bottom: 15px;
	}
	.input_score, .output_score, .total_score, .averag_coefficient, .coefficients{
		width: 60px;
		height: 25px;		
		text-align: right;
		/*border: hidden;*/
	}
	.final_score{
		width: 70px;
		height: 25px;		
		text-align: right;
		border: hidden;
	}
	/* text for header */
	.kh{
		font-size: 8.5pt;
	}
	.en{
		font-size: 7.2pt;
		color: #FFD700;
	}
	.en-title{
		font-size: 7.2pt;
		color: #337ab7;
	}
	.kh-title{
		color: #337ab7;
		font-weight: 600;
	}
</style>

<script type="text/javascript">
	$(function(){
		$('#year, #schlevelid').hide();
		$("#school_level_id").val($("#school_level_id first").val());

		//get year and class data---------------------------------
		$('#school_level_id').change(function(){
			var school_level_id = $(this).val();
			var school_id = $('#school_id').val();
			var program_id = $('#program_id').val();

			if(school_level_id != ""){
				$.ajax({
	            url: "<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_year_and_grade_data') ?>",	            
	            type: "post",
	            dataType: 'json',
	            data: {'school_id': school_id, 'program_id': program_id, 'school_level_id': school_level_id},
	            success: function (data) {
	            	// console.log(data);
	            	
	            	//get year--------------------------------
	            	var getYear = '';	            	
	            	$.each(data["year"], function(k,v){
	            		getYear += '<option value="'+ v.yearid +'">'+ v.sch_year+'</option>';		            		
	            	}); $('#adcademic_year_id').html(getYear);

	            	//get grade--------------------------------
	            	var getGrade = '';
	            	$.each(data["grade"], function(ke,re){
	            		getGrade += '<option value="'+ re.grade_levelid +'">'+ re.grade_level+'</option>';
	            	});	$('#grade_level_id').html(getGrade);

	            	//get teacher------------------------------
	            	var grade_level_id = $('#grade_level_id').val();
	            	var adcademic_year_id = $('#adcademic_year_id').val();
	            	var url = "<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_teacher_data') ?>";
						var data = 'school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
						getDataByAjax($('#teacher_id'), url, data);
						$('#class_id').html('');
						$('#subject_id').html('');	
						$('#student_id').html('');

						// get class ========
						var teacher_id = '';//$('#teacher_id').val();
						if(school_level_id != "" && adcademic_year_id != "" && grade_level_id != ""){
							var url_ = "<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_class_data') ?>";
							var data_ = 'school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id+'&teacher_id='+teacher_id;
							getDataByAjax($('#class_id'), url_, data_);
						}
						else{
							$('#class_id').html('');
							$('#subject_id').html('');	
							$('#student_id').html('');			
						}

						//remove message require-------------------
						var aId = $('#adcademic_year_id').data('parsley-id');
						var gId = $('#grade_level_id').data('parsley-id');

						$('#parsley-id-'+aId).remove();	   
						$('#adcademic_year_id').removeClass('parsley-error');

						$('#parsley-id-'+gId).remove();	   
						$('#grade_level_id').removeClass('parsley-error');        	
			   	}
	        });

			}else{
				$('#adcademic_year_id').html('');
				$('#grade_level_id').html('');
				$('#teacher_id').html('');
				$('#class_id').html('');
				$('#subject_id').html('');	
				$('#student_id').html('');
			}
		});

		//get teacher data-----------------------------------------
		$('#grade_level_id').change(function(){
			var grade_level_id = $(this).val();
			var school_level_id = $('#school_level_id').val();			
			var adcademic_year_id = $('#adcademic_year_id').val();
			if(school_level_id == "" && adcademic_year_id == "" && grade_level_id == ""){
				$('#teacher_id').html('');
			}else{
				var url = "<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_teacher_data') ?>";
				var data = 'school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
				getDataByAjax($('#teacher_id'), url, data);
				$('#class_id').html('');
				$('#subject_id').html('');	
				$('#student_id').html('');

				// get class ========
				var teacher_id = '';//$('#teacher_id').val();
				if(school_level_id != "" && adcademic_year_id != "" && grade_level_id != ""){
					var url_ = "<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_class_data') ?>";
					var data_ = 'school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id+'&teacher_id='+teacher_id;
					getDataByAjax($('#class_id'), url_, data_);
				}
				else{
					$('#class_id').html('');
					$('#subject_id').html('');	
					$('#student_id').html('');			
				}
				
			}
		});

		//get class and subject-------------------------------------
		$('#teacher_id').change(function(){
			var teacher_id = $(this).val();
			var grade_level_id = $('#grade_level_id').val();
			var school_level_id = $('#school_level_id').val();			
			var adcademic_year_id = $('#adcademic_year_id').val();

			if(school_level_id != "" && adcademic_year_id != "" && grade_level_id != ""){
				var url = "<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_class_data') ?>";
				var data = 'school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id+'&teacher_id='+teacher_id;
				getDataByAjax($('#class_id'), url, data);
			}
			else{
				$('#class_id').html('');
				$('#subject_id').html('');	
				$('#student_id').html('');			
			}
		});

		//get student data------------------------------------------
		$('#class_id').change(function(){	
			var class_id = $(this).val();
			var teacher_id = $('#teacher_id').val();
			var school_level_id = $('#school_level_id').val();
			var grade_level_id = $('#grade_level_id').val();			
			var adcademic_year_id = $('#adcademic_year_id').val();
			if(class_id == ""){
				$('#student_id').html(''); $('#subject_id').html('');
				//$('#subject_group_id').val(''); $('#subject_main_id').val('');			
			}else{
				var url = "<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_student_data') ?>";
				var data = 'class_id='+class_id;
				getDataByAjax($('#student_id'), url, data);

				var url = "<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_subject_data') ?>";
				var data = 'class_id='+class_id+'&school_level_id='+school_level_id+'&grade_level_id='+grade_level_id+'&adcademic_year_id='+adcademic_year_id+'&teacher_id='+teacher_id;
				getDataByAjax($('#subject_id'), url, data);
			}
		});
	
		//get group subject id and main subject id------------------
		$('#subject_id').change(function(){
			var subject_id = $(this).val();
			var class_id = $('#class_id').val();
			var teacher_id = $('#teacher_id').val();
			var school_level_id = $('#school_level_id').val();
			var grade_level_id = $('#grade_level_id').val();			
			var adcademic_year_id = $('#adcademic_year_id').val();
			if(subject_id == ""){
				$('#subject_group_id').val(''); $('#subject_main_id').val('');
			}else{
				$.ajax({
					url:"<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_group_main_id_subject') ?>",
					type:'POST',
					dataType: 'json',
		            data:'subject_id='+subject_id+'&class_id='+class_id+'&school_level_id='+school_level_id+'&grade_level_id='+grade_level_id+'&adcademic_year_id='+adcademic_year_id+'&teacher_id='+teacher_id,
		            success: function (data) {
		            	//console.log(data[0].subj_type_id);
		            	$('#subject_group_id').val(data[0].subj_type_id); $('#subject_main_id').val(data[0].subj_main_id);		            		            	
		            }
				});
			}
		});

		//choose exame type-----------------------------------------
		$('.select_exam_type').click(function(){
			if ($(this).is(':checked') == false) {
				toastr["warning"]("Please choose a exam type");				
			}
			else {
				$('#btnsearch').removeAttr('disabled');
				$('#btnsave').attr('disabled', true);
				$('.student_list').css('display', 'none');

				// if($('#frm_subject_score_kgp').parsley().validate()){
					$('.show_extam_type').html('');	
					$('.show_mess_alert').hide().html('');		
					var month_data = ['January', 'February', 'March', 'April-May', 'June', 'July', 'August', 'September', 'Octomber', 'November', 'December'];
					var getVal = $(this).val();
					var opt = ''; 
					var elements = '';

					var school_id = $('#school_id').val();
					var program_id = $('#program_id').val(); 
					var school_level_id = $('#school_level_id').val();
					var adcademic_year_id = $('#adcademic_year_id').val();

					//monthly=========
					if(getVal == 1){
						for(i = 1; i <= month_data.length; i++) {
							opt += '<option value="'+ i +'">'+ month_data[i-1] +'</option>';
						}

						elements += '<td colspan="3">'+
										'<select name="exam_monthly" class="form-control exam_monthly" style="width:910px; border-color:#337ab7;">'+ opt +'</select>'+
							        '</td>';
					}
					//semester========
					else if(getVal == 2){								
						for(j = 1; j <= month_data.length; j++) {
							opt += '<option value="'+ j +'">'+ month_data[j-1] +'</option>';
						}

						elements += '<td colspan="3" class="get_semester_data"></td>';
						$.ajax({
							url:"<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_semester') ?>",
							type:'POST',
							dataType: 'json',
				            data:{	'school_id':school_id,
				            		'program_id':program_id,
				            		'school_level_id':school_level_id,
				            		'adcademic_year_id':adcademic_year_id
				            	 },
				            success: function (data){
				            	var tbl ='<table>'+
												'<tr>'+
													'<td width="300" style="text-align:center !important;">'+
														'<input type="checkbox" name="exam_semester[]" id="exam_semester" class="exam_semester" value="'+ data[0].semesterid +'" data-parsley-required="true" data-parsley-multiple="mymultiplelink">&nbsp;<label for="exam_semester" style="font-weight:bold;">'+ data[0].semester +'</label><br>'+
														'<input type="checkbox" name="exam_semester[]" id="exam_semester_" class="exam_semester" value="'+ data[1].semesterid +'" data-parsley-required="true" data-parsley-multiple="mymultiplelink">&nbsp;<label for="exam_semester_" style="font-weight:bold;">'+ data[1].semester +'</label><br>'+
													'</td>'+
													'<td><select name="get_monthly[]" class="form-control get_monthly" style="width:620px; height:120px; border-color:#337ab7;" multiple  data-parsley-required="true" data-parsley-inputs>'+ opt +'</select></td>'+
												'</tr>'+
						            	  '</table>';            	
						        $('.get_semester_data').html(tbl);
				            }
						});
					}
					//final===========
					else{
						elements += '<td colspan="3" class="show_semester_data"></td>';
						$.ajax({
							url:"<?php echo site_url('student/c_subject_score_entry_kgp_multi/get_semester') ?>",
							type:'POST',
							dataType: 'json',
				            data:{	'school_id':school_id,
				            		'program_id':program_id,
				            		'school_level_id':school_level_id,
				            		'adcademic_year_id':adcademic_year_id
				            	 },
				            success: function (data){
				            	var tbl ='<table>'+
											'<tr>'+
												'<td style="text-align:right !important;">'+
													'<input type="checkbox" name="get_semester_id[]" checked class="get_semester" value="'+ data[0].semesterid +'"><span style="margin-left:10px; margin-right:70px; font-weight:bold;">'+ data[0].semester +'</span>'+
													'<input type="checkbox" name="get_semester_id[]" checked class="get_semester" value="'+ data[1].semesterid +'"><span style="margin-left:10px; font-weight:bold;">'+ data[1].semester +'</span>'+
												'</td>'+
											'</tr>'+
										'</table>';    	
						        $('.show_semester_data').html(tbl);
				            }
						});
					}			

					$('.show_extam_type').show().append(elements);
				// }
			}
		});

		//can check only one with checkbox--------------------------
		$('.show_extam_type').delegate('.exam_semester', 'click', function(){
			$('input:checkbox').not(this).prop('checked', false);
		});

		//check to choose both semester
		$('.show_extam_type').delegate('.get_semester', 'change', function(){
			if($(this).not(':checked')){
				$('<div class="check_semester">'+
                	'<div style="border: none;padding: 9px 0 9px 0; height:auto; margin:0 0 8px 8px; width:95%;">'+
                  		'<span style="color:#2d80c3; margin-left:5px; font-weight:bold; font-size:9pt;">You should check both semester1 and semester2</span>'+ 
                	'</div>'+
               '</div>')
               .dialog({
	                height:'auto',
	                width: 430,
	                modal: true,
	                resizable:false,
	                title: 'Check Semester',
	                dialogClass: 'noTitleStuff',                           
	                buttons: {
	                  "OK": function (event) {
	                    $(this).dialog('destroy').remove();
	                    $('.get_semester').prop('checked', true);                      
	                  }
	                }
	            });
	            $('.ui-dialog-titlebar-close').remove();               	
			}
		});


		// refresh =========
		$('body').delegate('#btncancel', 'click', function() {
			location.reload();
		});


		//check subject which student learn or not
		$('body').delegate('.none_subject', 'click', function(){
			if($(this).is(':checked')){
				$(this).val(0);
			}else{
				$(this).val(1);
			}
		});

		//calculate total score------------------------------------
		$('body').delegate('.input_score', 'change', function(){
			var tr = $(this).parent().parent();
			var total_score = 0; 
			tr.find('.input_score').each(function(){
				var td = $(this).parent();
				var scores = $(this).val()-0;
				var percentage = td.find('.percentage').val();
				var max_score = td.find('.check_max_score').val();

				if(scores > 0){
    				td.find('.input_score').css('color','#000');
    			}else{
    				td.find('.input_score').val(0);
    				td.find('.input_score').css('color','red');
    			} 

				if(scores > max_score){
					$('<div class="check_score">'+
		                	'<div style="border: none;padding: 9px 0 9px 0; height:auto; margin:0 0 8px 8px; width:95%;">'+
		                  		'<span style="color:#2d80c3; margin-left:5px; font-weight:bold; font-size:9pt;">Input score should be equal or less than '+ max_score +' score !</span>'+ 
		                	'</div>'+
		               '</div>')
		               .dialog({
			                height:'auto',
			                width: 430,
			                modal: true,
			                resizable:false,
			                title: 'Check Score',
			                dialogClass: 'noTitleStuff',                           
			                buttons: {
			                  "OK": function (event) {
			                    $(this).dialog('destroy').remove();
			                    td.find('.input_score').val('');                        
			                  }
			                }
			            });
		               	$('.ui-dialog-titlebar-close').remove();
				}else{
					total_score += (scores * percentage)-0;					
				}						
			});

			if(total_score > 0){
				tr.find('.total_score').val(number_format(total_score, 2));
			}else{
				tr.find('.total_score').val('');
			}			
		});

		//input coefficient (មេគុណ)-------------------------------
		$('body').delegate('.coefficients', 'change', function(){
			var coefic_val = $(this).val();
			if(coefic_val != ""){
				$('.averag_coefficient').each(function(){
					$(this).val(coefic_val);
					var tr = $(this).parent().parent();
					var co_val = $(this).val();
					if(co_val <= 0){						
						tr.find('.averag_coefficient').css('color', 'red');		
					}else{
						tr.find('.averag_coefficient').css('color', '#000');
					}
				});
			}else{ $('.averag_coefficient').val(''); }			
		});

		$('body').delegate('.averag_coefficient', 'change', function(){
			$('.averag_coefficient').each(function(){
				var tr = $(this).parent().parent();
				var co_val = $(this).val();
				if(co_val <= 0 || co_val == ""){
					tr.find('.averag_coefficient').val(0);
					tr.find('.averag_coefficient').css('color', 'red');					
				}else{
					tr.find('.averag_coefficient').css('color', '#000');
				}
			});
		});


		// select text ========      
		$('body').delegate('.input-xs', 'focus', function() {
			$(this).select();
		});
		$('body').delegate('.input-xs', 'change', function() {
			if ($(this).val() - 0 == 0) {
				$(this).val(0);
			}         
		});


		// search report ------------------
		$('#btnsearch').click(function() {			
			$('.div-student-detail').html('');
	         var school_id = $('#school_id').val();
	         var program_id = $('#program_id').val();
	         var school_level_id = $('#school_level_id').val();
	         var adcademic_year_id = $('#adcademic_year_id').val();
	         var grade_level_id = $('#grade_level_id').val();
	         var teacher_id = $('#teacher_id').val();
	         var class_id = $('#class_id').val();         
	         var subject_id = $('#subject_id').val();        
	         var student_id = $('#student_id').val();
	         var exam_type = [];
			var select_exam_type = $('.select_exam_type:checked').val();

			//get monthly value=============
			if(select_exam_type == 1) {
				$exam_monthly = $('.exam_monthly').val();
				exam_type = {'get_exam_type': 'monthly','exam_monthly': $exam_monthly};	
			}
			//get semester value============
			else if(select_exam_type == 2) {
				var exam_semester = $('.exam_semester:checked').val();
				$get_monthly_in = [];				
				$('.get_monthly :selected').each(function(i){
					$get_monthly_in[i] = $(this).val();
				});
				exam_type = {'get_exam_type': 'semester', 'exam_semester': exam_semester, 'get_monthly_in': $get_monthly_in};
			}
			//get final value===============
			else if(select_exam_type == 3) {
				$get_semester_in = [];
				$('.get_semester:checked').each(function(i) {
					$get_semester_in[i] = $(this).val();
				});
				exam_type = {'get_exam_type': 'final', 'get_semester_in': $get_semester_in};
			}


         if ($('#frm_subject_score_kgp').parsley().validate()) {
            $.ajax({
               url: '<?= site_url('student/c_subject_score_entry_kgp_multi/search') ?>',
               type: 'POST',
               datatype: 'JSON',
               beforeSend: function() {
                  $('.xmodal').show();
               },
               complete: function() {
                  $('.xmodal').hide();
               },
               data: {
                     'school_id' : school_id,
                     'program_id' : program_id,
                     'school_level_id' : school_level_id,
                     'adcademic_year_id' : adcademic_year_id, 
                     'grade_level_id' : grade_level_id,
                     'teacher_id' : teacher_id,
                     'class_id' : class_id,     
                     'subject_id' : subject_id,                   
                     'student_id': student_id,
                     'exam_type' : exam_type
               },
               success: function(data) {
	                  $('#score_list').html(data.tbl);
	                  $('#btnsave').prop('disabled', false);
	                  $('.student_list').show();
               },
               error: function() {

               }
            });
        }
				
		});


		// save data -----------------------
		$('#btnsave').on('click', function(){
			var school_id = $('#school_id').val();
			var program_id = $('#program_id').val();
			var school_level_id = $('#school_level_id').val();
			var adcademic_year_id = $('#adcademic_year_id').val();
			var grade_level_id = $('#grade_level_id').val();
			var teacher_id = $('#teacher_id').val();
			var class_id = $('#class_id').val();			
			var subject_id = $('#subject_id').val();
			var subject_group_id = $('#subject_group_id').val();
			var subject_maisn_id = $('#subject_main_id').val();
			var student_array = [];
			var exam_type = [];
			var select_exam_type = $('.select_exam_type:checked').val();

			//get monthly value=========
			if(select_exam_type == 1) {
				$exam_monthly = $('.exam_monthly').val();
				exam_type = {'get_exam_type': 'monthly','exam_monthly': $exam_monthly, select_exam_type: select_exam_type};	
			}

			//get semester value =====
			else if(select_exam_type == 2) {
				var exam_semester = $('.exam_semester:checked').val();
				$get_monthly_in = [];				
				$('.get_monthly :selected').each(function(i){
					$get_monthly_in[i] = $(this).val();
				});
				exam_type = {'get_exam_type': 'semester', 'exam_semester': exam_semester, 'get_monthly_in': $get_monthly_in, select_exam_type: select_exam_type};
			}

			//get final value ========
			else if(select_exam_type == 3) {
				$get_semester_in = [];
				$('.get_semester:checked').each(function(i) {
					$get_semester_in[i] = $(this).val();
				});
				exam_type = {'get_exam_type': 'final', 'get_semester_in': $get_semester_in, select_exam_type: select_exam_type};
			}

			var btnsave = $(this);
         var arr = [];
         var arr__ = [];
                  
         // main ===========         
         $('.no_').each(function(i) {         	
         	var tr = $(this).parent();
            var student_id = tr.find('.student_id').attr('attr-student_id');
            var exam_final = tr.find('.student_id').attr('attr-exam_final');
            var get_semester_id = tr.find('.student_id').attr('attr-get_semester_id');            
            var present = tr.find('.present').val();
            var absent = tr.find('.absent').val();            
            var comments = tr.find('.comments').val();
         	
         	// final =========
         	if (select_exam_type == 3) {
         		var semester1 = tr.find('.semester1').text();
         		var semester2 = tr.find('.semester2').text();
         		var total_avg = tr.find('.total_avg').text();       		
         		var total_exam = tr.find('.total_exam').text();  
         		
         		arr__[i] = {student_id: student_id, exam_final: exam_final, get_semester_id: get_semester_id, present: present, absent: absent, comments: comments, total_avg_score_s1: semester1, total_avg_score_s2: semester2, total_score: total_avg, average_score: total_exam};
         	}
         	else {
         		var g_total_score = tr.find('.comments').attr('attr-g_total_score');
            	var g_average_score = tr.find('.comments').attr('attr-g_average_score');  
	            var arr_ = [];        
	            var obj_subject_id = tr.find('.subject_id');
	            
	            obj_subject_id.each(function(j) {
	               var subject_mainid = $(this).attr('attr-subject_mainid');	               
	               var subject_type_id = $(this).attr('attr-subject_type_id');
	               var subject_id = $(this).attr('attr-subject_id');
	               var total_score = $(this).val();                                  
	               
	               arr_[j] = {
	                  student_id: student_id,
	                  g_total_score: g_total_score,
	                  g_average_score: g_average_score,                     
	                  subject_mainid: subject_mainid,
							subject_type_id: subject_type_id,
	                  subject_id: subject_id,
	                  total_score: total_score
	               };
	            });

	            arr[i] = arr_;
	            arr__[i] = {student_id: student_id, present: present, absent: absent, g_total_score: g_total_score, g_average_score: g_average_score, comments: comments};
	         }
         });

         // console.log(arr__);
         // return false;
		
			if ($('#frm_subject_score_kgp').parsley().validate()) {
				$.ajax({
	               url: '<?= site_url('student/c_subject_score_entry_kgp_multi/save') ?>',
	               type: 'POST',
	               datatype: 'JSON',
	               beforeSend: function() {
	                  $('.xmodal').show();
	               },
	               complete: function() {
	                  $('.xmodal').hide();
	               },
	               data: {
	                  teacher_id: $('#teacher_id').val(),
	                  school_id: $('#school_id').val(),
	                  program_id: $('#program_id').val(),    
	                  school_level_id: $('#school_level_id').val(),
	                  academic_year_id: $('#adcademic_year_id').val(),
	                  grade_level_id: $('#grade_level_id').val(),        
	                  class_id: $('#class_id').val(),   
	                  student_id: $('#student_id').val(),     
	                  co_effecient: $('#co_effecient').val(),

	                  exam_type: exam_type,
	                  arr: arr,
	                  arr__: arr__
	               },
	               success: function(data) {
	                  if (data == 1) {
	                     toastr["success"]('Saved!');
	                     btnsave.attr('disabled', true);
	                     $('.student_list').css('display', 'none');                  
	                  }
	                  else {
                  		if (confirm('Saved ready! Do you want to save again?')) {
                  			delete_data();
                  		}
	                  }
	               },
	               error: function() {
	                  toastr["warning"]('Cannot save! please try again');
	               }

	            });// ajax ====
	      	}

		});


	});// ready =======

	function delete_data() {
		var exam_type = [];
		var select_exam_type = $('.select_exam_type:checked').val();

		//get monthly value=========
		if(select_exam_type == 1) {
			$exam_monthly = $('.exam_monthly').val();
			exam_type = {'get_exam_type': 'monthly','exam_monthly': $exam_monthly, select_exam_type: select_exam_type};	
		}

		//get semester value =====
		else if(select_exam_type == 2) {
			var exam_semester = $('.exam_semester:checked').val();
			$get_monthly_in = [];				
			$('.get_monthly :selected').each(function(i){
				$get_monthly_in[i] = $(this).val();
			});
			exam_type = {'get_exam_type': 'semester', 'exam_semester': exam_semester, 'get_monthly_in': $get_monthly_in, select_exam_type: select_exam_type};
		}

		//get final value ========
		else if(select_exam_type == 3) {
			$get_semester_in = [];
			$('.get_semester:checked').each(function(i) {
				$get_semester_in[i] = $(this).val();
			});
			exam_type = {'get_exam_type': 'final', 'get_semester_in': $get_semester_in, select_exam_type: select_exam_type};
		}

		$.ajax({
			url: '<?= site_url('student/c_subject_score_entry_kgp_multi/delete_data') ?>',
			type: 'POST',
			datatype: 'JSON',
			beforeSend: function() {
			  $('.xmodal').show();
			},
			complete: function() {
			  $('.xmodal').hide();
			},
			data: {
				school_id: $('#school_id').val(),
				program_id: $('#program_id').val(),    
				school_level_id: $('#school_level_id').val(),
				academic_year_id: $('#adcademic_year_id').val(),
				grade_level_id: $('#grade_level_id').val(),        
				class_id: $('#class_id').val(),   
				student_id: $('#student_id').val(),

				exam_type: exam_type
			},
			success: function(data) {
				if (data == 1) {
					// toastr["warning"]('Delete!');
					$('#btnsave').click();
				}
			  
			},
			error: function() {
				toastr["warning"]('Cannot save! please try again');
			}
      });
	}


	/*----------------------------------------FUNCTION------------------------------------*/
	//function ajax to get data
	function getDataByAjax(selector, url, data){
		$.ajax({
            url: url,	            
            type: "post",
            dataType: 'html',
            data: data,
            success: function (data) {
            	selector.html(data);		            	
            }
        });	
	}

</script>