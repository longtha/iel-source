<div class="container-fluid" style="width:100% !important">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
                <strong>Student Fee Reminder Period</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                  <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
               </a>
               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
               </a>
               <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
               </a>           
               
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
         <input type="hidden" name="termid" id="termid">
         
         <div class="col-sm-4" style="">         
            <div class="form-group">
               <label for="student_num">ID<span style="color:red"></span></label>
               <input type="text" name="student_num" id="student_num" class="form-control" placeholder="ID">
            </div>

            <div class="form-group">
               <label for="full_name">Name<span style="color:red"></span></label>
               <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Name">
            </div>

            <div class="form-group">
               <label for="gender">Gender<span style="color:red"></span></label>
               <select name="gender" id="gender" class="form-control">
               	<option value="">All</option>
               	<option value="Female">Female</option>
               	<option value="Male">Male</option>
               </select>
            </div>

            <div class="form-group">
               <label for="from_date">From Date<span style="color:red"></span></label>
				   <div class="input-group">
					  <input type="text" name="from_date" id="from_date" class="form-control" placeholder="dd/mm/yyyy">
					  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
					</div>
            </div>

            <div class="form-group">
               <label for="to_date">To Date<span style="color:red"></span></label>
         		<div class="input-group">
					  <input type="text" name="to_date" id="to_date" class="form-control" placeholder="dd/mm/yyyy">
					  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
					</div>
            </div>     

         </div><!-- 1 -->

         <div class="col-sm-4" style="">            

            <div class="form-group">
               <label for="schoolid">School Info<span style="color:red"></span></label>
               <select name="schoolid" id="schoolid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->info->getschinfor() as $row) {
                        $opt .= '<option value="'.$row->schoolid.'">'.$row->name.'</option>';
                     }

                     echo $opt;        
                  ?>                  
               </select>
            </div>
            
            <div class="form-group">
               <label for="programid">Program<span style="color:red"></span></label>
               <select name="programid" id="programid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->p->getprograms() as $row) {
                        $opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group">
               <label for="schlevelid">School Level<span style="color:red"></span></label>
               <select name="schlevelids" id="schlevelids" class="form-control">
                  
               </select>
            </div>

            <div class="form-group">
               <label for="yearid">Year<span style="color:red"></span></label>
               <select name="yearid" id="yearid" class="form-control">

               </select>
            </div>

            <div class="form-group">
               <label for="rangelevelid">Rangelevel Name<span style="color:red"></span></label>
               <select name="rangelevelid" id="rangelevelid" class="form-control">

               </select>
            </div>        
            
         </div><!-- 2 -->

         <div class="col-sm-4">

         	<div class="form-group" style="">
               <label for="classid">Class<span style="color:red"></span></label>
               <select name="classid" id="classid" class="form-control">
                  <?php
                     // $opt = '';
                     // $opt .= '<option></option>';
                     // foreach ($this->c->allclass() as $row) {
                     //    $opt .= '<option value="'.$row->classid.'">'.$row->class_name.'</option>';
                     // }

                     // echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group" style="">
               <label for="feetypeid">Payment Method<span style="color:red"></span></label>
               <select name="feetypeid" id="feetypeid" class="form-control">
                  <?php
                     $opt = '';
                     $opt .= '<option value="">All</option>';
                     foreach ($this->f->getfeetypes() as $row) {
                        $opt .= '<option value="'.$row->feetypeid.'">'.$row->schoolfeetype.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group" style="">
               <label for="feetypeid">Payment Type<span style="color:red"></span></label>
               <select name="term_sem_year_id" id="term_sem_year_id" class="form-control">
                  <?php
                     // $opt = '';
                     // $opt .= '<option></option>';
                     // foreach ($this->f->getfeetypes() as $row) {
                     //    $opt .= '<option value="'.$row->feetypeid.'" '.($row->feetypeid - 0 == 1 ? 'selected="selected"' : '').'>'.$row->schoolfeetype.'</option>';
                     // }

                     // echo $opt;        
                  ?>
               </select>
            </div>

         	<div class="form-group" style="">
               <label for="is_paid">Is paid?<span style="color:red"></span></label>
               <select name="is_paid" id="is_paid" class="form-control">
                  <option value="">All</option>
                  <option value="1">Paid</option>
               	<option value="0">Unpaid</option>               	
               </select>
            </div>

            <div class="form-group" style="">
               <label for="is_closed">Is active?<span style="color:red"></span></label>
               <select name="is_closed" id="is_closed" class="form-control">
               	<option value="">All</option>
               	<option value="0">Active</option>
               	<option value="1">Inactive</option>               	
               </select>
            </div>

         </div><!-- 3 -->      

         <div class="col-sm-7 col-sm-offset-5">
            <div class="form-group">
               <button type="button" class="btn btn-primary btn-sm btn_search" name="btn_search" id="btn_search" data-save='1'>Search</button>&nbsp; 
               <button type="button" class="btn btn-warning btn-sm btn_clear" name="btn_clear" id="btn_clear" data-save='1' style="display: none;">Clear</button>
               
            </div>            
         </div>

      </form>      
      <div class="col-sm-12">
         <div class="form-group" style="border-bottom: 1px solid #CCC;">&nbsp;</div>           
      </div>
   </div>   
   
   <div class="col-sm-12">
      <div class="form-group">
         <div class="table-responsive">
	         <div id="tab_print">
	            <table border="0"​ align="center" id="se_list" class="table table-hover">               
						<thead>
						   <tr>
						      <th style="width: 5%;">No</th>
						      <th style="width: 10%;">Photo</th>
						      <th style="width: 10%;">Student Name</th>
						      <th style="width: 10%;">Enroll Date</th>						      
						      <th style="width: 10%;">Program</th>
						      <th style="width: 10%;">School Level</th>
						      <th style="width: 10%;">Year</th>                        
						      <th style="width: 10%;">Range Level</th>
						      <th style="width: 10%;">Class</th> 
						      <th style="width: 10%;">Payment Type</th>
						      <th style="width: 5%;text-align: center;">Status</th>
						   </tr>
						  
						</thead>                
	               <tbody>
	               	
	               </tbody>
	               
	               <tfoot>
	                  <tr style="border-top: 2px solid #ddd;" class="remove_tag">
	                     <td colspan="5" style="padding-top: 32px;">
	                        <span id="sp_total"></span>
	                     </td>
	                     <td colspan="7" style="text-align: right;">
	                        <span id="sp_page" class="btn-group pagination" role="group" aria-label="..."></span>
	                     </td>
	                     
	                  </tr>
	               </tfoot>               
	            </table>
            </div>        
         </div>
      </div>
   </div>

</div>

<style type="text/css">
	#se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	
</style>

<script type="text/javascript">
   $(function(){

   	// get ======= 
      $('body').delegate('#feetypeid', 'change', function(){
      	var feetypeid = $(this).val() - 0;

      	$.ajax({
            url: '<?= site_url('student/c_student_change_class/get_paymentType') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               feetypeid: feetypeid,
               programid: $('#programid').val(),
               schlevelid: $('#schlevelids').val(),
               yearid: $('#yearid').val()
                               
            },
            success: function(data){
            	// console.log(data);

               // schoollevel ======
               var opt = '';
               opt += '<option value="">All</option>';
               if(data.length > 0){
                  $.each(data, function(i, row){
                     opt += '<option value="'+ row.term_sem_year_id +'">'+ row.period +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#term_sem_year_id').html(opt);             

            },
            error: function() {

            }
         });
      });

      // get ======= 
      $('body').delegate('#programid', 'change', function(){
         if($(this).val() - 0 > 0){
            get_schlevel($(this).val());
         }else{
            $('#schlevelids').html('');
            $('#yearid').html('');            
            $('#rangelevelid').html('');
            $('#classid').html('');
         }
                    
      });
      $('body').delegate('#schlevelids', 'change', function(){
         if($(this).val() - 0 > 0){
            get_year($('#programid').val(), $(this).val());
         }else{
            $('#yearid').html('');            
            $('#rangelevelid').html('');
            $('#classid').html('');
         }
      });
      $('body').delegate('#yearid', 'change', function(){
         if($(this).val() - 0 > 0){
            get_rangelevel($(this).val());
         }else{          
            $('#rangelevelid').html('');
            $('#classid').html('');
         }
         
      });

      // get schlevel ======
      function get_schlevel(programid = ''){      
         $.ajax({
            url: '<?= site_url('student/c_student_change_class/get_schlevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid                
            },
            success: function(data){

               // schoollevel ======
               var opt = '';
               opt += '<option value="">All</option>';
               if(data.schlevel.length > 0){
                  $.each(data.schlevel, function(i, row){
                     opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#schlevelids').html(opt);

               // get year =====
               get_year($('#programid').val(), $('#schlevelids').val());                  

            },
            error: function() {

            }
         });         
      }

      // get year =======
      function get_year(programid = '', schlevelids = ''){
         $.ajax({
            url: '<?= site_url('student/c_student_change_class/get_year') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelids                                  
            },
            success: function(data){
               var opt = '';
               opt += '<option value="">All</option>';
               if(data.year.length > 0){
                  $.each(data.year, function(i, row){
                     opt += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#yearid').html(opt);


               // class ========
               var opt1 = '';
               opt1 += '<option value="">All</option>';
               if(data.class.length > 0){
                  $.each(data.class, function(i, row){
                     opt1 += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                  });
               }else{
                  opt1 += '';
               }
               $('#classid').html(opt1);

               // range level ======
               get_rangelevel($('#yearid').val());

            },
            error: function() {

            }
         });
      }

      // get rangelevel =======
      function get_rangelevel(yearid = ''){
         $.ajax({
            url: '<?= site_url('student/c_student_change_class/get_rangelevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               yearid: yearid                                  
            },
            success: function(data){
               var opt = '';
               opt += '<option value="">All</option>';
               if(data.rangelevel.length > 0){
                  $.each(data.rangelevel, function(i, row){
                     opt += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#rangelevelid').html(opt);
            },
            error: function() {

            }
         });
      }

   	$('#from_date').datepicker({
   		format: 'dd/mm/yyyy',
    		startDate: '-3d'
   	});
   	$('#to_date').datepicker({
		format: 'dd/mm/yyyy',
		startDate: '-3d'
	});      

      // tooltip ======
      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // print =======
      $('body').delegate('#a_print', 'click', function(){
         var title="<h4 align='center'>"+ "Student Fee Reminder Period List" +"</h4>";
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint(title, export_data);
      });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Student Fee Reminder Period List";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });      

      // chk =======
      $('body').delegate('#isclosed', 'click', function(){
         $(this).is(':checked') ? $(this).val(1) : $(this).val(0);
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#semester').select();
         $('#save').html('Save');
         $('#save_next').show();
         $('#f_save').parsley().destroy();
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#term').select();
         $('#save').html('Save');
         $('#save_next').show();
      });

      // init. =====
      grid(1, 10);

      // search ========
      $('body').delegate('#btn_search', 'click', function(){
         grid(1, 10);
      });

      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, 10);
      });

   }); // ready ========

   // clear =======
   function clear(){
      $('#termid').val('');
      $('#term').val('');      
      $('#term_kh').val('');
      $('#programid').val('');
      $('#yearid').html('');
      $('#schlevelids').html('');
      $('#start_date').val('');
      $('#end_date').val('');
      $('#isclosed').val(0);
      $('#isclosed').prop('checked', false);
      $('#dv_rangelevelid').html('');      
   }

   // grid =========
   function grid(current_page = 0, total_display = 0){
      var offset = ((current_page - 1) * total_display) - 0;
      var limit = total_display - 0;

      $.ajax({
         url: '<?= site_url('student/c_student_change_class/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            offset: offset,
            limit: limit
            ,
            student_num: $('#student_num').val(),
            full_name: $('#full_name').val(),
            gender: $('#gender').val(),
            schoolid: $('#schoolid').val(),
            programid: $('#programid').val(),
            schlevelid: $('#schlevelids').val(),
            term_sem_year_id: $('#term_sem_year_id').val(),
            feetypeid: $('#feetypeid').val(),
            yearid: $('#yearid').val(),
            rangelevelid: $('#rangelevelid').val(),
            classid: $('#classid').val(),
            is_paid: $('#is_paid').val(),
            is_closed: $('#is_closed').val(),            
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            years: $('#year').val(),
            schlevelids: $('#schlevelid').val()
         },
         success: function(data) {
         	// console.log(data);

            var tr = '';
            var total = '';
            var page = '';

/*            if(data.result.length > 0){
               $.each(data.result, function(i, row){
               	var img = '<?= base_url()."assets/upload/students" ?>/'+row.year+'/' + (row.student_num != null ? (row.student_num + '.jpg') : '') ;
				
				if(row.is_paid==0){
					var add_class='class="active"';
				}else{
					var add_class='class="inactive"';
				}
                  tr += '<tr '+add_class+'>'+
                           '<td>'+ (i + 1 + offset) +'</td>'+
                           '<td> <img src="'+ img +'" class="img-circle" alt="No image" width="70" height="70"></td>'+
                           '<td>\
								<div style="font-weight: bold;">'+ (row.student_num != null ? row.student_num : '') +'</div>\
								<div>'+ (row.first_name_kh + ' ' + row.last_name_kh) +'</div>\
								<div>'+ (row.first_name + ' ' + row.last_name) +'</div>\
								<div>'+ (row.gender != null ? row.gender : '') +'</div>\
                           </td>'+                           
                           '<td>'+ (row.enroll_date != null ? row.enroll_date : '') +'</td>'+
                           '<td>'+ (row.program != null ? row.program : '') +'</td>'+
                           '<td>'+ (row.sch_level != null ? row.sch_level : '') +'</td>'+
                           '<td>'+ (row.sch_year != null ? row.sch_year : '') +'</td>'+
                           '<td>'+ (row.rangelevelname != null ? row.rangelevelname : '') +'</td>'+
                           '<td>'+ (row.class_name != null ? row.class_name : '') +'</td>'+
                           '<td>'+ (row.period != null ? row.period : '') +'</td>'+
                           '<td>'+ (row.is_closed - 0 == 1 ? 'Inactive' : 'Active') +'</td>'+                         
                        '</tr>';
               });

               // previous ====
               page += '<button type="button" class="btn btn-default btn-sm a-pagination" data-current_page="' + (current_page > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left   "></i>&nbsp;Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-sm a-pagination" data-current_page="' + (current_page < data.totalPage ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next&nbsp;<i class="glyphicon glyphicon-chevron-right"></i></button>';

               total += '<span style="">'+ (data.totalRecord - 0 > 0 ? (data.totalRecord + ' Records') : '') +'</span>';
            }else{
               tr += '<tr><td colspan="12" style="text-align: center;font-weight: bold;">No Results</td></tr>';
            }*/
			

            $('#se_list tbody').html(data.tr);
            // $('#se_list tfoot').find('#sp_total').html(total);
            // $('#se_list tfoot').find('.pagination').html(page);
         },
         error: function() {

         }
      });
   }
</script>