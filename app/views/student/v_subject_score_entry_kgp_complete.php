<div class="container-fluid">     
 	<div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6" style="background: #337ab7;color: white;">
               <span class="glyphicon glyphicon-stats"></span>
               <strong>Entry complete</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right;background: #337ab7;color: white;">&nbsp;
               <a href="javascript:;" class="btn btn-sm btn-primary" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add new... item" style="display: none;"><span class="glyphicon glyphicon-minus"></span></a>

					<a href="javascript:;" class="btn btn-sm btn-primary" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh"> <span class="glyphicon glyphicon-refresh"></span></a>

               <!-- <?php if($this->green->gAction("E")){ ?>
                  <a href="javascript:void(0);" class="btn btn-sm btn-success" id="Export" data-toggle="tooltip" data-placement="top" title="Export"><span class="glyphicon glyphicon-export"></span></a>
               <?php }?>
               <?php if($this->green->gAction("P")){ ?>
                  <a href="javascript:void(0);" class="btn btn-sm btn-success " id="Print" data-toggle="tooltip" data-placement="top" title="Print"><span class="glyphicon glyphicon-print"></span></a>         
               <?php }?> -->  
               
            </div>         
         </div>
      </div>
	</div>

	<div class="collapse in" id="collapseExample">             
		<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" class="tdrow" id="frm_subject_score_kgp">
         <div class="col-sm-12">
            <div class="row">
               <div class="panel panel-default">
                  <div class="panel-body" style="padding: 0;">
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="school_id">School</label>
                           <select name="school_id" id="school_id" minlength='1' class="form-control">
                                 <?php
                                 $row = $this->school->getschinfor_row(1);                               
                                 echo '<option value="'. $row->schoolid .'">'. $row->name .'</option>';
                              ?>                       
                                   </select>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="program_id">Program</label>
                           <select name="program_id" id="program_id" minlength='1' class="form-control">
                                 <?php
                                 $row = $this->program->getprogram(1);                                
                                 echo '<option value="'. $row->programid .'">'. $row->program .'</option>';
                              ?>                       
                          </select>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="school_level_id">School Level<span style="color:red">*</span></label>
                           <select name="school_level_id" id="school_level_id" minlength='1' class="form-control" required data-parsley-required-message="This field require">
                                       <option value=""></option>
                                 <?php
                                 $get_level_data = $this->school_level->getsch_level(1);                                   
                                 foreach($get_level_data as $r_level){
                                    echo '<option value="'. $r_level->schlevelid .'">'. $r_level->sch_level .'</option>';
                                 }
                              ?>                       
                           </select>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="adcademic_year_id">Academic Year<span style="color:red">*</span></label>
                           <select name="adcademic_year_id" id="adcademic_year_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="grade_level_id">Grade Level<span style="color:red">*</span></label>
                           <select name="grade_level_id" id="grade_level_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="teacher_id">Teacher Name<span style="color:red">*</span></label>                             
                           <select name="teacher_id" id="teacher_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
                        </div>
                     </div>                        
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="class_id">Class Name<span style="color:red">*</span></label>                              
                           <select name="class_id" id="class_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
                        </div>
                     </div>
                     <div class="col-md-4" style="display: none;">
                        <div class="form-group">
                           <label for="subject_id">Subject Name<span style="color:red"></span></label>
                           <input type="hidden" name="subject_group_id" id="subject_group_id">
                           <input type="hidden" name="subject_main_id" id="subject_main_id">
                           <select name="subject_id" id="subject_id" minlength='1' class="form-control"></select>
                        </div>
                     </div>                                             
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="student_id">Student Name</label>
                           <select name="student_id" id="student_id" class="form-control"></select>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="req" for="student_num">Examp Type<span style="color:red">*</span></label><br>
                           <div class="form_exam_type" id='form_semester' style="text-align:center; border:1px #337ab7 solid; padding:5px 5px 0 5px;">                  
                              <table border="0"​ align="center" id='tbl-exam-type' class="tbl-exam-type table table-bordered">
                                 <tr style="color:#337ab7 !important;">
                                    <td>
                                       <label class="radio-inline">
                                       <input type="radio" name="select_exam_type" value='1' class='select_exam_type'>Monthly</label> 
                                    </td>
                                    <td>
                                       <label class="radio-inline">
                                       <input type="radio" name="select_exam_type" value='2' class='select_exam_type'>Semester</label> 
                                    </td>
                                    <td _style="display: none;">
                                       <label class="radio-inline">
                                       <input type="radio" name="select_exam_type" value='3' class='select_exam_type'>Final</label> 
                                    </td>
                                 </tr>
                                 <tr class="show_extam_type" style="display:none;"></tr>
                                 <tr class="show_mess_alert" style="display:none;"></tr>     
                              </table>
                           </div>                                    
                        </div>
                     </div> 

                     <!-- display -->
                     <div class="col-sm-12">
                        <div class="table-responsive">

                        </div>
                     </div>

                     <div class="col-sm-7 col-sm-offset-5">
                        <div class="form-group">   
                           <?php if($this->green->gAction("R")){ ?>        
                             <button type="button" class="btn btn-primary btn-sm search" name="search" id="search" data-save="1"><span class="glyphicon glyphicon-search"></span> Search</button>
                           <?php }?>
                           <?php if($this->green->gAction("C")){ ?>                                
                              <button type="button" class="btn btn-primary btn-sm save" name="save" id="save" disabled><span class="glyphicon glyphicon-save"></span> Save</button>
                           <?php }?>
                        </div>
                     </div>  

                  </div>
               </div>
            </div>
         </div><!--end form search of student information-->
      </form>     
	</div>
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <div id="tab_print">
               <table border="0"​ align="center" id="score_list" class="table table-hover table-condensed table-bordered" style="width: 100%;" cellpadding="0" cellspacing="0">
                  <thead _style="background: #337ab7;color: white;">

                  </thead>
                   
      				<tbody>
      				</tbody>

      				<tfoot>
      				</tfoot>
               </table>
            </div>         
         </div>
      </div>
   </div>

</div><!-- container -->


<!-- style -->
<style type="text/css">
   #score_list th {vertical-align: middle;}
   #score_list td {vertical-align: middle;padding: 0 !important;}

   .input-xs {
       height: 24px;
       padding: 2px;
       font-size: 11px;
       line-height: 1.5;
       border-radius: 0;
       text-align: center;
       min-width: 44px;
       border: 0;
   }
</style>

<script type="text/javascript">
   $(function() {

      // choose exame type ==============
      $('body').delegate('.select_exam_type', 'change', function() {
         $('.show_extam_type').html('');  
         $('.show_mess_alert').hide().html('');    
         var month_data = ['January', 'February', 'March', 'April-May', 'June', 'July', 'August', 'September', 'Octomber', 'November', 'December'];
         var getVal = $(this).val();
         var opt = ''; 
         var elements = '';

         var school_id = $('#school_id').val();
         var program_id = $('#program_id').val(); 
         var school_level_id = $('#school_level_id').val();
         var adcademic_year_id = $('#adcademic_year_id').val();

         //monthly=========
         if(getVal == 1){
            for(i = 1; i <= month_data.length; i++) {
               opt += '<option value="'+ i +'">'+ month_data[i-1] +'</option>';
            }

            elements += '<td colspan="3">'+
                        '<select name="exam_monthly" class="form-control exam_monthly" style="width:910px; border-color:#337ab7;">'+ opt +'</select>'+
                       '</td>';
         }

         //semester========
         else if(getVal == 2){                        
            for(j = 1; j <= month_data.length; j++) {
               opt += '<option value="'+ j +'">'+ month_data[j-1] +'</option>';
            }

            elements += '<td colspan="3" class="get_semester_data"></td>';
            $.ajax({
               url:"<?php echo site_url('student/c_subject_score_entry_kgp/get_semester') ?>",
               type:'POST',
               dataType: 'json',
                  data:{   'school_id':school_id,
                        'program_id':program_id,
                        'school_level_id':school_level_id,
                        'adcademic_year_id':adcademic_year_id
                      },
                  success: function (data){
                     // console.log(data[0]['semesterid']);
                     // return false;
                     var tbl ='<table>'+
                              '<tr>'+
                                 '<td width="300" style="text-align:center !important;">'+
                                    '<input type="checkbox" name="exam_semester[]" class="exam_semester" value="'+ data[0].semesterid +'"><span style="margin-left:10px; font-weight:bold;">'+ data[0].semester +'</span><br>'+
                                    '<input type="checkbox" name="exam_semester[]" class="exam_semester" value="'+ data[1].semesterid +'"><span style="margin-left:10px; font-weight:bold;">'+ data[1].semester +'</span>'+
                                 '</td>'+
                                 '<td><select name="get_monthly[]" class="form-control get_monthly" style="width:620px; height:80px; border-color:#337ab7;" multiple>'+ opt +'</select></td>'+
                              '</tr>'+
                             '</table>';              
                    $('.get_semester_data').html(tbl);
                  }
            });
         }       

         $('.show_extam_type').show().append(elements);
      });


      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // select text ========      
      $('tbody').delegate('.input-xs', 'focus', function() {
         $(this).select();
      });
      $('tbody').delegate('.input-xs', 'change', function() {
         if ($(this).val() - 0 == 0) {
            $(this).val(0);
         }         
      });

      $('#year, #schlevelid').hide();
      $("#school_level_id").val($("#school_level_id first").val());

      //get year and class data---------------------------------
      $('#school_level_id').change(function(){
         var school_level_id = $(this).val();
         var school_id = $('#school_id').val();
         var program_id = $('#program_id').val();

         if(school_level_id != ""){
            $.ajax({
                  url: "<?php echo site_url('student/c_subject_score_entry_kgp_complete/get_year_and_grade_data') ?>",              
                  type: "post",
                  dataType: 'json',
                  data: {'school_id': school_id, 'program_id': program_id, 'school_level_id': school_level_id},
                  success: function (data) {
                     // console.log(data);
                     
                     //get year--------------------------------
                     var getYear = '';                
                     $.each(data["year"], function(k,v){
                        getYear += '<option value="'+ v.yearid +'">'+ v.sch_year+'</option>';                        
                     }); $('#adcademic_year_id').html(getYear);

                     //get grade--------------------------------
                     var getGrade = '';
                     $.each(data["grade"], function(ke,re){
                        getGrade += '<option value="'+ re.grade_levelid +'">'+ re.grade_level+'</option>';
                     });   $('#grade_level_id').html(getGrade);

                     //get teacher------------------------------
                     var grade_level_id = $('#grade_level_id').val();
                     var adcademic_year_id = $('#adcademic_year_id').val();
                     var url = "<?php echo site_url('student/c_subject_score_entry_kgp_complete/get_teacher_data') ?>";
                  var data = 'school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
                  getDataByAjax($('#teacher_id'), url, data);
                  $('#class_id').html('');
                  $('#subject_id').html(''); 
                  $('#student_id').html('');

                  //remove message require-------------------
                  var aId = $('#adcademic_year_id').data('parsley-id');
                  var gId = $('#grade_level_id').data('parsley-id');

                  $('#parsley-id-'+aId).remove();     
                  $('#adcademic_year_id').removeClass('parsley-error');

                  $('#parsley-id-'+gId).remove();     
                  $('#grade_level_id').removeClass('parsley-error');          
                }
              }); 
         }else{
            $('#adcademic_year_id').html('');
            $('#grade_level_id').html('');
            $('#teacher_id').html('');
            $('#class_id').html('');
            $('#subject_id').html(''); 
            $('#student_id').html('');
         }
      });

      //get teacher data-----------------------------------------
      $('#grade_level_id').change(function(){
         var grade_level_id = $(this).val();
         var school_level_id = $('#school_level_id').val();       
         var adcademic_year_id = $('#adcademic_year_id').val();
         if(school_level_id == "" && adcademic_year_id == "" && grade_level_id == ""){
            $('#teacher_id').html('');
         }else{
            var url = "<?php echo site_url('student/c_subject_score_entry_kgp_complete/get_teacher_data') ?>";
            var data = 'school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
            getDataByAjax($('#teacher_id'), url, data);
            $('#class_id').html('');
            $('#subject_id').html(''); 
            $('#student_id').html('');
         }
      });

      //get class and subject-------------------------------------
      $('#teacher_id').change(function(){
         var teacher_id = $(this).val();
         var grade_level_id = $('#grade_level_id').val();
         var school_level_id = $('#school_level_id').val();       
         var adcademic_year_id = $('#adcademic_year_id').val();

         if(school_level_id != "" && adcademic_year_id != "" && grade_level_id != ""){
            var url = "<?php echo site_url('student/c_subject_score_entry_kgp_complete/get_class_data') ?>";
            var data = 'school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id+'&teacher_id='+teacher_id;
            getDataByAjax($('#class_id'), url, data);
         }else{
            $('#class_id').html('');
            $('#subject_id').html(''); 
            $('#student_id').html('');       
         }
      });

      //get student data------------------------------------------
      $('#class_id').change(function(){   
         var class_id = $(this).val();
         var teacher_id = $('#teacher_id').val();
         var school_level_id = $('#school_level_id').val();
         var grade_level_id = $('#grade_level_id').val();         
         var adcademic_year_id = $('#adcademic_year_id').val();
         if(class_id == ""){
            $('#student_id').html(''); $('#subject_id').html('');
            //$('#subject_group_id').val(''); $('#subject_main_id').val('');        
         }else{
            var url = "<?php echo site_url('student/c_subject_score_entry_kgp_complete/get_student_data') ?>";
            var data = 'class_id='+class_id;
            getDataByAjax($('#student_id'), url, data);

            var url = "<?php echo site_url('student/c_subject_score_entry_kgp_complete/get_subject_data') ?>";
            var data = 'class_id='+class_id+'&school_level_id='+school_level_id+'&grade_level_id='+grade_level_id+'&adcademic_year_id='+adcademic_year_id+'&teacher_id='+teacher_id;
            getDataByAjax($('#subject_id'), url, data);
         }
      });
   
      //get group subject id and main subject id------------------
      $('#subject_id').change(function(){
         var subject_id = $(this).val();
         var class_id = $('#class_id').val();
         var teacher_id = $('#teacher_id').val();
         var school_level_id = $('#school_level_id').val();
         var grade_level_id = $('#grade_level_id').val();         
         var adcademic_year_id = $('#adcademic_year_id').val();
         if(subject_id == ""){
            $('#subject_group_id').val(''); $('#subject_main_id').val('');
         }else{
            $.ajax({
               url:"<?php echo site_url('student/c_subject_score_entry_kgp_complete/get_group_main_id_subject') ?>",
               type:'POST',
               dataType: 'json',
                  data:'subject_id='+subject_id+'&class_id='+class_id+'&school_level_id='+school_level_id+'&grade_level_id='+grade_level_id+'&adcademic_year_id='+adcademic_year_id+'&teacher_id='+teacher_id,
                  success: function (data) {
                     //console.log(data[0].subj_type_id);
                     $('#subject_group_id').val(data[0].subj_type_id); $('#subject_main_id').val(data[0].subj_main_id);                                     
                  }
            });
         }
      });


      // search =======      
      $('body').delegate('#search', 'click', function() {
         $('.div-student-detail').html('');
         var school_id = $('#school_id').val();
         var program_id = $('#program_id').val();
         var school_level_id = $('#school_level_id').val();
         var adcademic_year_id = $('#adcademic_year_id').val();
         var grade_level_id = $('#grade_level_id').val();
         var teacher_id = $('#teacher_id').val();
         var class_id = $('#class_id').val();         
         var subject_id = $('#subject_id').val();        
         var student_id = $('#student_id').val();

         var exam_type = [];
         var select_exam_type = $('.select_exam_type:checked').val();

         if ($('#frm_subject_score_kgp').parsley().validate()) {
            alert();
            // return false;

            $.ajax({
               url: '<?= site_url('student/c_subject_score_entry_kgp_complete/search') ?>',
               type: 'POST',
               datatype: 'JSON',
               beforeSend: function() {
                  $('.xmodal').show();
               },
               complete: function() {
                  $('.xmodal').hide();
               },
               data: {
                     'school_id' : school_id,
                     'program_id' : program_id,
                     'school_level_id' : school_level_id,
                     'adcademic_year_id' : adcademic_year_id, 
                     'grade_level_id' : grade_level_id,
                     'teacher_id' : teacher_id,
                     'class_id' : class_id,     
                     'subject_id' : subject_id,                   
                     'student_id': student_id,
                     'exam_type' : exam_type
               },
               success: function(data) {
                  $('#score_list thead').html(data.thead);
                  $('#score_list tbody').html(data.tr);
                  $('#save').prop('disabled', false);
               },
               error: function() {

               }
            });
         }

         return false;


         //for semester result =====================
         if(select_exam_type == 2){
            $('.show_mess_alert').hide().html('');
            var exam_semester = $('.exam_semester:checked').val();            
            if(exam_semester != undefined){  
               $get_monthly_in = [];            
               $selected = 0;             
               $('.get_monthly :selected').each(function(i){
                  $selected = $selected + 1;
                  $get_monthly_in[i] = $(this).val();
               });

               if($selected == 0){              
                     var mess_select_month = '<td colspan="3">'+
                                       '<div class="alert alert-danger" style="margin:5px 7px 5px 0; padding:5px 0 7px 12px;">Please select at least one month before you search</div>'+
                                      '</td>';
                     $('.show_mess_alert').show().append(mess_select_month);
               }else{
                  $('.show_mess_alert').hide().html('');
                  exam_type = {'get_exam_type':'semester','exam_semester':exam_semester, 'get_monthly_in':$get_monthly_in};
               }
            }else{
               var mess_select_semester = '<td colspan="3">'+
                                       '<div class="alert alert-danger" style="margin:5px 7px 5px 0; padding:5px 0 7px 12px;">Please select at least one semester before you search</div>'+
                                     '</td>';
                  $('.show_mess_alert').show().append(mess_select_semester);
            }  
         }
         //for final result ============
         else if(select_exam_type == 3){
            $('.show_mess_alert').hide().html('');
            $get_semester_in = [];
            $('.get_semester:checked').each(function(i){
               $get_semester_in[i] = $(this).val();
            });
            exam_type = {'get_exam_type':'final', 'get_semester_in':$get_semester_in};          
         }
         //for monthly result ==========
         else
         {
            $exam_monthly = $('.exam_monthly').val();
            if($exam_monthly == ""){
               var mess_select_month = '<td colspan="3">'+
                                    '<div class="alert alert-danger" style="margin:5px 7px 5px 0; padding:5px 0 7px 12px;">Please select at least one month before you search</div>'+
                                   '</td>';
                  $('.show_mess_alert').show().append(mess_select_month);
            }else{
               $('.show_mess_alert').hide().html('');
               exam_type = {'get_exam_type':'monthly','exam_monthly':$exam_monthly};
            }
         }

         var data = {
                  'school_id' : school_id,
                  'program_id' : program_id,
                  'school_level_id' : school_level_id,
                  'adcademic_year_id' : adcademic_year_id, 
                  'grade_level_id' : grade_level_id,
                  'teacher_id' : teacher_id,
                  'class_id' : class_id,     
                  'subject_id' : subject_id,                   
                  'student_id': student_id
               };

         
      });


      // save =======      
      $('body').delegate('#save', 'click', function() {         
         // if ($('.select_exam_type').prop('checked') == false) {
         //    toastr["warning"]('Cannot save! please choose exam type');
         // }
         // else {
            var btnCheck = $(this);
            var arr = [];

            // main =======
            $('.no_').each(function(i) {
               var tr = $(this).parent();
               var student_id = tr.find('.student_id').attr('attr-student_id');
               var comments = tr.find('.comments').val();
               // sub ========   
               var arr_ = [];        
               var obj_subject_id = tr.find('.subject_id');

               obj_subject_id.each(function(j) {
                  var subject_id = $(this).attr('attr-subject_id');
                  var subject_type_id = $(this).attr('attr-subject_type_id');
                  var score = $(this).val();
                  
                  arr_[j] = {
                     student_id: student_id,
                     comments: comments,
                     subject_type_id: subject_type_id,
                     subject_id: subject_id,
                     score: score
                  };
               });

               arr[i] = arr_;
            });

            // exam type =======================
            var exam_type = [];
            var select_exam_type = $('.select_exam_type:checked').val();

            // get monthly value =======
            if (select_exam_type == 1) {
               var exam_monthly = $('.exam_monthly').val();
               exam_type = {'get_exam_type': 'monthly', 'exam_monthly': $exam_monthly};   
            }
            // get semester value ======
            else if (select_exam_type == 2) {
               var exam_semester = $('.exam_semester:checked').val();
               var get_monthly_in = [];           
               $('.get_monthly option:selected').each(function(i) {
                  get_monthly_in[i] = $(this).val();
               });               

               exam_type = {'get_exam_type': 'semester', 'exam_semester': exam_semester, 'get_monthly_in': get_monthly_in};
            }

            // console.log(exam_type);
            // return false;

            if($('#frm_subject_score_kgp').parsley().validate()) {
               $.ajax({
                  url: '<?= site_url('student/c_subject_score_entry_kgp_complete/save') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  beforeSend: function() {
                     $('.xmodal').show();
                  },
                  complete: function() {
                     $('.xmodal').hide();
                  },
                  data: {
                     teacher_id: $('#teacher_id').val(),
                     school_id: $('#school_id').val(),
                     program_id: $('#program_id').val(),    
                     school_level_id: $('#school_level_id').val(),
                     academic_year_id: $('#adcademic_year_id').val(),
                     grade_level_id: $('#grade_level_id').val(),        
                     class_id: $('#class_id').val(),        
                     
                     exam_type: exam_type,
                     arr: arr
                  },
                  success: function(data) {
                     if (data == 1) {
                        toastr["success"]('Saved!');
                        btnCheck.attr('disabled', true);
                     }
                     else {
                        toastr["warning"]('Cannot save! please try again');
                     }               
                  },
                  error: function() {
                     toastr["warning"]('Cannot save! please try again');
                  }
               });// ajax ====
            }

      });  


   });// ready =======


   //function ajax to get data
   function getDataByAjax(selector, url, data){
      $.ajax({
            url: url,               
            type: "post",
            dataType: 'html',
            data: data,
            success: function (data) {
               selector.html(data);                   
            }
        }); 
   }

</script>