
<style type="text/css">
	.ulForcing{
        margin-bottom: 0px !important;
	}
	a{
		cursor: pointer;
	}
	.ui-autocomplete{z-index: 9999;}
	.datepicker {z-index: 9999;}
	.com_sms{
		color:#11ADAF;
		font-size:18px;
		font-style:italic;
		font-family:"Comic Sans MS","Times New Roman",Arial !important;
		background-color:#F0F2D7;
	}
	#selected_name{
		color:#11ADAF;
		font-size:16px;
		font-style:italic;
		font-family:"Comic Sans MS","Times New Roman",Arial !important;
	}
	.modal-dialog{
		width:40%;
		top:15%;
		left:7%;
		outline: none;
	}
	
	.btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 80%;
        min-height: 80%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background-color: #337ab7 !important;
		border-color: #2e6da4 !important;
        cursor: inherit;
        display: block;
    }
    #navigation_left{
        display: none;
    }
    #page-wrapper{
        margin:0 0 0 0px !important; ;
    }
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{
    	padding: 3px;
    }
</style>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      	<div class="result_info">
				      	<div class="col-sm-6">
				      		<strong>Evaluate Information</strong>

				      	</div>
				      	<div class="col-sm-6" style="text-align: center">
				      		<strong>
				      			<center class='error' style='color:red;'><?php if(isset($error)) echo "$error"; ?></center>
				      		</strong>
				      	</div>
			</div>
	    <?php
	    	
	    	$en_teacher='';
	    	$kh_teacher='';
	    	$month=array(array('month'=>'Jan'),
	    				array('month'=>'Feb'),
	    				array('month'=>'Mar'),
	    				array('month'=>'Apr'),
	    				array('month'=>'May'),
	    				array('month'=>'Jun'),
	    				array('month'=>'Jul'),
	    				array('month'=>'Aug'),
	    				array('month'=>'Sep'),
	    				array('month'=>'Oct'),
	    				array('month'=>'Nov'),
	    				array('month'=>'Dec'));
	    	//$month[]=Array ( [0] => Array ( [month] => 'Feb' ), [1] => Array ( [month] => 'Jan' ) ) ;
	    	if(isset($data->kh_teacher)){
	    		$teacher_en='';
	    		$teacher_kh='';
	    		$teacher_fr='';
	    		$monthtran='';
	    		if($data->forign_teacher>0){
	    			$emp1=$this->db->where('empid',$data->forign_teacher)->get('sch_emp_profile')->row();
	    			$teacher_en=$emp1->last_name.' '.$emp1->first_name;
	    		}
	    		if($data->kh_teacher>0){
	    			$emp2=$this->db->where('empid',$data->kh_teacher)->get('sch_emp_profile')->row();
	    			$teacher_kh=$emp2->last_name.' '.$emp2->first_name;
	    		}
	    		if($data->eng_teach_name){
	    			$emp3=$this->db->where('empid',$data->eng_teach_name)->get('sch_emp_profile')->row();
	    			$teacher_fr=$emp3->last_name.' '.$emp3->first_name;
	    		}
	    		if($data->evaluate_type=='semester'){
	    			$month=$this->evaluate->getmonthclass($data->classid,$data->yearid);
	    		}
	    	}
	    	$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
	    	// print_r($month);
	     ?>
	       <form enctype="multipart/form-data" id='frmtreatment' action="<?php echo base_url()."index.php/student/evaluate_moyes/save?m=$m&p=$p"; ?>" method="POST">
			        <div class="row">
						<div class="col-sm-6">
				            	<div class="panel panel-default">
				            		<div class="panel-heading">
						               <h4 class="panel-title">Evaluate Details
								    		<label style="float:right !important; font-size:11px !important; color:red;"><?php if(isset($data->modified_by)) if($data->modified_by!='') echo "Last Modified Date: ".date_format(date_create($data->modified_date),'d-m-Y H:i:s')." By : $data->modified_by"; ?></label>

						               </h4>
						        	</div>
					          		<div class="panel-body">
					          			<div class="form_sep">
								                <input type='text' value="<?php if(isset($data->transno)) echo $data->transno; ?>" class='hide' name='transno' id='transno'/>
								                <input type='text' value="<?php if(isset($data->transno)) echo 'update'; ?>" class='hide' name='upd' id='upd'/>
								        </div>
					          			<div class="form_sep">
									        <label class="req" for="student_num">Date</label>
									        <div class='input-group date' id='datetimepicker'>
											    <input type='text' <?php if(isset($data->date)){ echo "value='".$this->green->convertSQLDate($data->date)."' disabled"; } ?> required  data-parsley-required-message="Enter Date"  id="date" class="form-control" name="date"/>
											    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
											</div>
											<input type='text' class='hide' id='eva_date' name='eva_date' <?php if(isset($data->date)){echo "value='".$this->green->convertSQLDate($data->date)."'"; } ?>/>
									    </div>
									    <div class="form_sep">
						                  	<label class="req" for="student_num">Evaluate Type</label>
						                 	<select data-required="true" <?php if(isset($data->date)) echo "disabled"; ?> class="form-control parsley-validated" onchange="changemonth(event);" onclick="getmonthclass(event);" name="evaluate_type" value="" id="evaluate_type" >
							                   <option value="month" <?php if(isset($data->evaluate_type)) if($data->evaluate_type=='month') echo 'selected'; ?>>Monthly</option>
							                   <option value="semester" <?php if(isset($data->evaluate_type)) if($data->evaluate_type=='semester') echo 'selected'; ?>>Semester</option>

				                  			</select>
				                  			<input type='text' class='hide' <?php if(isset($data->evaluate_type)){echo "value='$data->evaluate_type'"; } ?> id='eva_type' name='eva_type'/>
							            </div>
							            <div class="form_sep" id='semester'>
						                  	<label class="req" for="student_num">Trimester</label><br>
						                 	 	<label class="radio-inline">
											      <input type="radio" value='1st' class='semester' name="semester" <?php if(isset($data->evaluate_type)) if($data->eval_semester=='1st') echo 'checked disabled'; else echo 'disabled' ?>>1st
											    </label>
											    <label class="radio-inline">
											      <input type="radio" value='2nd' class='semester' name="semester" <?php if(isset($data->evaluate_type)) if($data->eval_semester=='2nd') echo 'checked disabled'; else echo 'disabled' ?>>2nd
											    </label>
											    <label class="radio-inline">
											      <input type="radio" value='3th' class='semester' name="semester" <?php if(isset($data->evaluate_type)) if($data->eval_semester=='3st') echo 'checked disabled';else echo 'disabled'  ?>>3th
											    </label>
											    <input type='text' class='hide' id='eva_semester' <?php if(isset($data->classid)){echo "value='$data->eval_semester'"; } ?> name='eva_semester'/>
							            </div>
							            <div class="form_sep" id='semesters'>
						                  	<label class="req" for="student_num">Semester</label><br>
						                 	 	<label class="radio-inline">
											      <input type="radio" value='1st' class='semester' name="semester" <?php if(isset($data->month)){ if($data->evaluate_type=='semester') if($data->eval_semester=='1st') echo 'checked disabled'; else echo 'disabled';} ?>>1st
											    </label>
											    <label class="radio-inline">
											      <input type="radio" value='2nd' class='semester' name="semester" <?php if(isset($data->month)){ if($data->evaluate_type=='semester') if($data->eval_semester=='2nd') echo 'checked disabled'; else echo 'disabled'; }?>>2nd
											    </label>
							            </div>
							            <div class="form_sep">
						                  	<label class="req" for="student_num">Class</label>
						                 	<select data-required="true" <?php if(isset($data->date)) echo "disabled"; ?> class="form-control parsley-validated" name="class" value="" id="class" onchange="getmonthclass(event);" >
							                  	<option value=''>Select Class</option>
							                  <?php
							                  	foreach ($this->db->get('sch_class')->result() as $class) { ?>
							                  		<option value='<?php echo $class->classid ?>' <?php if(isset($data->classid)) if($class->classid==$data->classid) echo 'selected'; ?>> <?php echo $class->class_name ?></option>
							                  	<?php
							                    }
							                  ?>
				                  			</select>
				                  			<input type='text' class='hide' id='classid' <?php if(isset($data->classid)){echo "value='$data->classid'"; } ?> name='classid'/>
							            </div>
                                        <div class="form_sep">
                                            <label class="req" for="avg_coefficient">Average Coefficient</label>
                                            <input type="text" name='avg_coefficient' id='avg_coefficient' class='form-control' value='<?php if(isset($data->avg_coefficient)){echo $data->avg_coefficient; } ?>'></textarea>
                                        </div>

						            </div>

						            <div class="panel-body">
								            <div class="form_sep">
								              <button id="btnnewdis" name="btnnewdis" <?php if(isset($data->date)) echo "disabled"; ?> onclick="gethearder(event);" type="button" class="btn btn-primary <?php if(isset($_GET['upd'])) echo "hide"?>">Select</button>
                                                <button  type="button" class="btn btn-primary btnexport" onclick="Javascript:void(0);">
                                                    <i class="fa fa-download"></i> Download
                                                </button>
                                                <button  type="button" class="btn btn-primary iconupload" href="#myModal3" data-toggle="modal" id="iconupload" title="Click to import">
                                                    <span class="glyphicon glyphicon-upload"></span> Upload
                                                </button>
                                                <button id="add_disease" name="btncancel"  onclick="rollback();"type="button" class="btn btn-danger <?php if(isset($data->date)) echo "hide"?>">Rollback Data</button>
								            </div>
								    </div>
						        </div>
						</div>
						<div class="col-sm-6">
				            <div class="panel panel-default">
				            		<div class="panel-heading">
						               <h4 class="panel-title">Evaluate Details</h4>
						            </div>
					          		<div class="panel-body">
						                <div class='form_sep'>
						                	<table class='table'>
						                		<thead>
						                			<th>Teacher Response</th>
						                		</thead>
						                		<tbody id='listteacher'>
						                			<?php
						                				$yearid=$this->session->userdata('year');
						                				if(isset($data->classid))
						                					if(count($this->evaluate->getteacherbyclass($data->classid,$yearid))>0)
																foreach ($this->evaluate->getteacherbyclass($data->classid,$yearid) as $t) {
																	echo "<tr>
																			<td>".$t->last_name.' '.$t->first_name."</td>
																		</tr>";
																}
															else
																echo "<label style='color:red'>No Teacher in this Class</label>";
						                			 ?>
						                		</tbody>
						                	</table>
						                </div>
						                <div class="form_sep hide">
						                  	<label class="req" for="reg_input_name">Khmer Teacher</label>
						                   	<input type="text" <?php if(isset($data->forign_teacher)) echo "value='".$teacher_kh."' disabled" ?> class="form-control parsley-validated" name="teacher_kh" id="teacher_kh">
						                	<input type='text' value="<?php if(isset($data->kh_teacher)) echo $data->kh_teacher ?>" class='hide' id='teacher_khid' name='teacher_khid'/>
						                </div>
						                <div class="form_sep hide">
						                  	<label class="req" for="reg_input_name">English Teacher</label>
						                   	<input type="text" <?php if(isset($data->forign_teacher)) echo "value='".$teacher_en."' disabled" ?> class="form-control parsley-validated" name="teacher_en" id="teacher_en">
						                	<input type='text' class='hide' <?php if(isset($data->forign_teacher)) echo "value='".$data->forign_teacher."'" ?> id='teacher_enid' name='teacher_enid'/>
						                </div>
						                <div class="form_sep hide">
						                  	<label class="req" for="reg_input_name">French Teacher</label>
						                   	<input type="text" <?php if(isset($data->eng_teach_name)) echo "value='".$teacher_fr."' disabled" ?> class="form-control parsley-validated" name="teacher_fr" id="teacher_fr">
						                	<input type='text' class='hide' value="<?php if(isset($data->eng_teach_name)) echo $data->eng_teach_name ?>" id='teacher_frid' name='teacher_frid'/>
						                </div>
						                <div class="form_sep hide" id='sep_month'>
						                  	<label class="req" for="student_num">Month</label>
						                 	<select data-required="true" <?php if(isset($data->date)) echo "disabled"; ?> class="form-control parsley-validated" name="month" value="" id="month" >
							                  <?php
							                  	foreach ($month as $montht) { ?>
							                  		<option value='<?php echo $montht->month ?>'<?php if(isset($data->month)) if($montht->month==$data->month) echo 'selected'; ?>><?php echo $montht->month; ?></option>
							                  	<?php }
							                  ?>

				                  			</select>
				                  			<input type='text' class='hide'  <?php if(isset($data->month)){echo "value='$data->month'"; } ?> id='eva_month' name='eva_month'/>
							            </div>
							            <div class="form_sep" id='sep_months'>
						                  	<label class="req" for="student_num">Months</label>
						                 	<select <?php if(isset($data->date)) echo "disabled"; ?> class="form-control months" multiple data-max-options="3" name="months[]" id="months" >
							                  <?php
							                  if(isset($data->date)){
							                  	$srtM=$data->month;
							                  	$arrMs=explode(",", $srtM);
							                  }
							                  //print_r($arrMs);
							                  	foreach ($month as $months) {
							                  		$sel='';
							                  		$monthtran.=$months['transno'].',';
							                  		if(isset($data->date))
								                  		 if(in_array($months['month'], $arrMs)){
								                  		 	$sel="selected";
								                  		 }
							                  		?>
							                  		<option value="<?php echo $months['month'] ?>"<?php  echo $sel; ?>><?php echo $months['month']; ?></option>
							                  	<?php }
							                  ?>
				                  			</select>

				                  			<input type='text' class='hide' <?php if(isset($data->month)){echo "value='$data->month'"; } ?> id='eva_months' name='eva_months'/>
				                  			<input type='text' class='hide' <?php if(isset($data->month)){echo "value='$monthtran'"; } ?> id='eva_transno' name='eva_transno'/>
							            </div>


						            </div>

					        </div>
					    </div>
					    <div class="col-sm-12">
				            	<div class="panel panel-default">
				            		<div class="panel-heading">
						               <h4 class="panel-title">Student Mention</h4>
						        	</div>
					          		<div class="panel-body">

						                <div class="form_sep scrollable-table">
						                  	<table class='table table-bordered table-striped table-header-rotated table-responsive' id='tblstdmention'>
						                		<?php
						                			if(isset($data->classid)){
						                				$schlevelid=$this->db->where('classid',$data->classid)->get('sch_class')->row()->schlevelid;
						                				$isvtc=$this->db->where('schlevelid',$schlevelid)->get('sch_school_level')->row()->is_vtc;
						                				$action='';
						                				$trimester=0;
														if($data->evaluate_type!='Three Month')
															$action='return isNumberKey(event);';
														else
															$trimester=1;
														echo "<thead>
																	<tr>
													        			<th rowspan='2' style='vertical-align: middle !important;'><input type='text' class='hide' value='$isvtc' id='schlevelids'/>No</th>
													        			<th rowspan='2' style='vertical-align: middle !important;'>Student</th>";
													        			// echo "<th rowspan='2' width='70' style='vertical-align: middle !important;'>Comments</th>";
												        //==================subject type========================
														foreach ($this->evaluate->gets_type($data->classid,$data->yearid,$trimester) as $sub) {
															echo "<th colspan='$sub->s_total' style='text-align:center !important;'>$sub->subject_type</th>";
														}
															echo "<!--<th  rowspan='2' style='vertical-align: middle !important;'>Save</th>-->
																</tr>
																<tr>";
														////=========================subject========================
														foreach ($this->evaluate->gets_type($data->classid,$data->yearid,$trimester) as $sub) {
															foreach ($this->evaluate->getsubject($data->classid,$sub->subj_type_id,$data->yearid,$trimester) as $subject) {
																echo "<th width='50'>$subject->subject_kh</th>";
															}
														}
														echo "</tr></thead>
																<tbody>";
														$i=1;
														foreach ($this->evaluate->getstudent($data->classid,$data->yearid) as $std) {

															$eva_row=$this->db->where('transno',$data->transno)->where('studentid',$std->studentid)->get('sch_student_evaluated')->row();

															echo "<tr class='tr_".$i."'>
																	<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
																	<td>
																		<input class='form-control input-sm hide studentid' value='$std->studentid' name='studentid[]'
																		id='studentid' stud_name='".$std->last_name.' '.$std->first_name."' />".$std->last_name_kh.' '.$std->first_name_kh."
																		<input type='hidden' class='stud_name$std->studentid' value='".$std->last_name.' '.$std->first_name."'/>
																		</td>";

																	foreach ($this->evaluate->gets_type($data->classid,$data->yearid,$trimester) as $sub) {

																		foreach ($this->evaluate->getsubject($data->classid,$sub->subj_type_id,$data->yearid,$trimester) as $subject) {
																			$mention='';
                                                                            if(isset($eva_row->evaluateid)){
                                                                                $mention=$this->green->getValue("SELECT score FROM sch_student_evaluated_mention
                                                                                                                        WHERE evaluateid='".$eva_row->evaluateid."'
                                                                                                                        AND studentid='".$std->studentid."'
                                                                                                                        AND subjectid='".$subject->subjectid."'
                                                                                                    ");
                                                                            }
                                                                            echo "<td>
                                                                                    <input type='text' onkeypress='" . $action . "' class='form-control input-sm txtmention' value='$mention' rel='$subject->subjectid' name='$subject->subjectid[]' id='txtmention' style='width:40px;' onfocus='this.select();'/>
                                                                                </td>";
																		}
																	}
															echo"<!-- <td style='text-align:center !important;' width='50'>
																		<a>
																 			<img onclick='saveinline(event,$std->studentid);' src='".site_url('../assets/images/icons/save.png')."' class='save$std->studentid'/>
																 		</a>
																 	</td> -->
																</tr>";
																$i++;
														}
														echo "</tbody>";
						                			}

						                		 ?>
						                	</table>
						                </div>

						            </div>
						        </div>
                                <!-- Set Block to export to excel app -->
                                <div class="table-responsive" id="ExportToExcel" style="display:none">

                                </div>
                                <!-- End Set Block to export to excel app -->
						</div>

				</div>
				<div class="row">
		          <div class="col-sm-5">
		            <div class="form_sep">
		              <button id="std_reg_submit" name="std_reg_submit" type="submit" class="btn btn-success">Save</button>
		              <button id="btncancel" type="button" class="btn btn-danger">Cancel</button>
		            </div>
		          </div>
		        </div>
		 </form>
	    </div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" id="tapcomment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="wrapper">
				<div class="clearfix" id="main_content_outer">
				    <div id="main_content">
					    <div class="result_info">
					    	<div class="col-sm-6">
					      		<strong>Class Comment</strong>
					      	</div>
					      	<div class="col-sm-6" style="text-align: center">
					      		<strong>
					      			<center class='visit_error' style='color:red;'></center>
					      		</strong>
					      	</div>
					    </div>
					      	<form enctype="multipart/form-data" name="frmvisit" id="frmvisit" method="POST">
						        <div class="row">
									<div class="col-sm-12">
							            	<div class="panel-body">
							            		<div class='table-responsive'>
										               <div class="form_sep">
										                  <label class="req" for="reg_input_name">Class Comment</label>
										                  <textarea data-required="true"  value="" class="form-control parsley-validated summernote" name="class_comment"  id="class_comment"></textarea>
										                	<input type='text' id='field' class='hide'/>
										                </div>
											   </div>
								            </div>
								    </div>
								</div>
					      </form>
					</div>
			    </div>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id='savecmd' class="btn btn-primary">Save</button>
            </div>
        </div>                       <!-- /.modal-content -->
    </div>
</div>

<!-- Stard dialog export data -->
<div class="modal fade bs-example-modal-sm" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title"><?php echo $upload_download?></h3>
            </div>
            <!-- start from  -->
            <form class="form-horizontal" role="form" method="post" action="<?php echo base_url()."index.php/student/evaluate_moyes/importStudentScore?m=$m&p=$p"; ?>" enctype="multipart/form-data" id='myForm'>
                <div class="modal-body">
                    <div class="form_sep">
                        <label class="req" for="avg_coefficient">Choose File</label>
                        <div class="input-group">
							<span class="input-group-btn">
								<span class="btn btn-primary btn-file">Browse…<input type="file" name="file_import[]" id="file_import" class="file_import"></span>
							</span>
                            <input class="form-control" type="text" readonly="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnImport" name="btnImport"  onclick="JavaScript:void(0);" type="submit" class="btn btn-success">Import</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
            <!-- end form -->
        </div>
    </div>
</div>

<!-- End dialog export data -->

<script type="text/javascript">
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    $(document).ready( function() {
        $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
    });
 	changemonth();
	$(function(){
        $('.btnexport').hide();
		autofillteacher();
		autofillteacheren();
		autofillteacherfr();
		$("#frmmedicine").parsley();
		$("#date").datepicker({
      		language: 'en',
      		pick12HourFormat: true,
      		format:'dd-mm-yyyy'
    	});

        $('.btnexport').on('click',function(e){
            e.preventDefault();
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(PrintingHtml()));
            return( false );
        });
        $("#btnImport").on('click',function(){
            getStudentScore("<?php echo site_url('student/evaluate_moyes/importStudentScore') ?>");
        });		
		       

	});
    var PrintingHtml = function(){
        return $("<center>"+$("#ExportToExcel").html()+"</center>").clone().find(".remove_tag").remove().end().html();
    }
	function getmonthclass(event){
		var classid=$('#class').val();
		var yearid=$('#year').val();
		var evaluate_type=$('#evaluate_type').val();
		getteacher(classid);
		if(evaluate_type=='semester'){
			$.ajax({
	                url:"<?php echo site_url('student/evaluate_moyes/getmonthclass') ?>",
	                data: {'classid':classid,'yearid':yearid},
	                type:"post",
	                success: function(data){
	                	$('#months').html(data);
	                }
	    	});
		}else{
			$.ajax({
	                url:"<?php echo site_url('student/evaluate_moyes/getmonth') ?>",
	                data: {'classid':classid,'yearid':yearid},
	                type:"post",
	                success: function(data){
	                	$('#months').html(data);
	                }
	    	});
		}

	}
	function changemonth(event){
		var val=$('#evaluate_type').val();
		// alert(val);
		if(val=='month'){
			$('#semester').addClass('hide');
			$('#semesters').addClass('hide');
			$('#sep_months').removeClass('hide');
		}
		if(val=='semester'){
			$('#semesters').removeClass('hide');
			$('#semester').addClass('hide');
			$('#sep_months').removeClass('hide');
		}
		if(val=='Three Month'){
			$('#semester').removeClass('hide');
			$('#semesters').addClass('hide');
			$('#sep_months').addClass('hide');
		}
	}
	function getteacher(classid){
		$.ajax({
	                url:"<?php echo site_url('student/evaluate_moyes/getteacher') ?>",
	                data: {'classid':classid},
	                type:"post",
	                success: function(data){
	              		$('#listteacher').html(data);
	            	}
	       });
	}
    function gethearder(event){
        getStudentScore("<?php echo site_url('student/evaluate_moyes/getsub_type') ?>",'#tblstdmention');
        getStudentScore("<?php echo site_url('student/evaluate_moyes/getsub_type_export') ?>",'#ExportToExcel');
    }

    function getStudentScore(urlLinke,setId=''){
        var semester=$('#eva_semester').val();
        var month=$('#months').val();//alert($('#months').val());
        //alert(month);
        var classid=$('#class').val();
        var year=$('#year').val();
        var evaluate_type=$("#evaluate_type").val();
        var avg_coefficient=$("#avg_coefficient").val();
		if($('#date').val()!='' && classid!='' && ($('#months').val()!=null || evaluate_type=='month')) {

			$.ajax({
	                url:urlLinke,
	                data: {
                        'classid':classid,
                        'year':year,
                        'evaluate_type':evaluate_type,
                        'month':month,
                        'eva_date':$('#date').val(),
                        'eva_semester': semester,
                        'avg_coefficient':avg_coefficient
                    },
	                type:"post",
	                success: function(data){
	                	if(data!='false'){
                            $(setId).html(data);
		              		$('#eva_date').val($('#date').val());
		              		$('#eva_type').val($('#evaluate_type').val());
		              		$('#classid').val($('#class').val());
		              		$('#eva_month').val($('#month').val());
		              		$('.semester').each(function(){
		              			if($(this).is(':checked'))
		              				$('#eva_semester').val($(this).val());
		              		})
		              		var months='';
		              		var eva_transno='';
		              		$('.months>option').each(function(){
		              			if($(this).is(':selected')){
		              				months+=$(this).text()+',';// $('#eva_semester').val($(this).val());
		              				eva_transno+=$(this).val()+',';
		              			}
		              		})
		              		$('#eva_months').val(months);
		              		$('#eva_transno').val(eva_transno);
		              		$('#date').attr('disabled',true);
		              		$('#evaluate_type').attr('disabled',true);
		              		$('#class').attr('disabled',true);
		              		$('#month').attr('disabled',true);
		              		$('#months').attr('disabled',true);
		              		$('#teacher_kh').attr('disabled',true);
		              		$('#teacher_en').attr('disabled',true);
		              		$('#teacher_fr').attr('disabled',true);
		              		$('#btnnewdis').attr('disabled',true);
		              		$('.semester').attr('disabled',true);
		              		//gettransno();
                            $('.btnexport').show();
	                	}else{
	                		$('.error').html("This month, Trimester Or Semester is already exist....!");
	                	}

	            	}
	       });
			$('.error').html('');
		}else{
			$('.error').html('Please Fill out all field...!');
		}
	}

	$('#btncancel').click(function(){
		var con=confirm('Are you Sure to Cancel...!');
		if(con==true)
			location.href="<?PHP echo site_url('student/evaluate_moyes');?>"+"?<?php echo "m=$m&p=$p" ?>";
	})
	function rollback(){
		var transno=$('#transno').val();
		var con=confirm('This action will be clear all data in this form...!');
		if(con==true)
			location.href="<?PHP echo site_url('student/evaluate_moyes/rollback');?>/"+transno+"?<?php echo "m=$m&p=$p" ?>";
	}
	function gettransno(){
		$.ajax({
	                url:"<?php echo site_url('student/evaluate_moyes/gettransno') ?>",
	                data: {'test':1},
	                type:"post",
	                success: function(data){
	              		$('#transno').val(data);
	            	}
	       });
	}

	function saveinline(event,studentid){
		var date=$('#date').val();

		if(studentid==""){
			var studentid=$(event.target).closest('tr').find('#studentid').val();
            alert(studentid);
		}

  		var eva_type=$('#evaluate_type').val();
  		var classid=$('#class').val();
  		var month=$('#eva_months').val();
  		var semester='';
  		var teacher_kh=$('#teacher_khid').val();
  		var teacher_en=$('#teacher_enid').val();
  		var teacher_fr=$('#teacher_frid').val();

  		var transno=$('#transno').val();
  		var upd=$('#upd').val();
        var avg_coefficient=$('#avg_coefficient').val();

  		var tranmonth='';
  		if(eva_type!='month'){
  			month=$('#eva_months').val();
  			semester=$('#eva_semester').val();
  			tranmonth=$('#eva_transno').val();
  		}
  		$.ajax({
	                url:"<?php echo site_url('student/evaluate_moyes/saveinline') ?>",
	                data: {'date':date,
	            			'eva_type':eva_type,
	            			'classid':classid,
	            			'studentid':studentid,
	            			'transno':transno,
	            			'month':month,
	            			'semester':semester,
	            			'upd':upd,
	            			'teacher_kh':teacher_kh,
	            			'teacher_fr':teacher_fr,
	            			'teacher_en':teacher_en,
                            'avg_coefficient':avg_coefficient
                    },
	                type:"post",
	                dataType:"json",
                    async:false,
	                success: function(data){
	                	var total_score=0;
	                	var countsub=0;
	                	$(event.target).closest('tr').find('.txtmention').each(function(){
	                		countsub++;
	                		var subjectid=$(this).attr('rel');
	                		var mention=$(this).val();
	                			total_score+=Number(mention);
	                		savemention(transno,subjectid,mention,data.evaluateid,studentid,data.level,eva_type);
	                	});
	                	if(eva_type=='semester'){
	                		savesemester(transno,total_score,studentid,classid,data.yearid,semester,month,tranmonth,countsub);
	                	}
	                		//
	            	}
	       });
	}
	function savesemester(transno,total_score,studentid,classid,yearid,semester,month,tranmonth,countsub){
			$.ajax({
	                url:"<?php echo site_url('student/evaluate_moyes/savesemester') ?>",
	                data: {'total_score':total_score,
	            			'transno':transno,
	            			'classid':classid,
	            			'countsub':countsub,
	            			'yearid':yearid,
	            			'month':month,
	            			'tranmonth':tranmonth,
	            			'semester':semester,
		            		'studentid':studentid},
	                type:"post",
	                success: function(data){
	              		//alert(data);
	            	}
	       });
	}
	function savemention(transno,subject,mention,evaluateid,studentid,level,eva_type){
			$.ajax({
	                url:"<?php echo site_url('student/evaluate_moyes/savemention') ?>",
	                data: {'subjectid':subject,
	            			'mention':mention,
	            			'level':level,
	            			'eva_type':eva_type,
	            			'transno':transno,
		            		'evaluateid':evaluateid,
		            		'studentid':studentid},
	                type:"post",
	                success: function(data){
	              		//alert(data);
	            	}
	       });
	}

	function removerow(event){
		var row_class=$(event.target).closest('tr').remove();
	}
	function autofillteacher(){
		var teacher="<?php echo site_url("student/evaluate_moyes/fillteacher")?>";
    	$("#teacher_kh").autocomplete({
				source: teacher,
				minLength:0,
				select: function( events, ui ) {
					$('#teacher_khid').val(ui.item.id);
				}
		});
	}
	function autofillteacheren(){
		var teacher="<?php echo site_url("student/evaluate_moyes/fillteacher")?>";
    	$("#teacher_en").autocomplete({
				source: teacher,
				minLength:0,
				select: function( events, ui ) {
					$('#teacher_enid').val(ui.item.id);
				}
		});
	}
	function autofillteacherfr(){
		var teacher="<?php echo site_url("student/evaluate_moyes/fillteacher")?>";
    	$("#teacher_fr").autocomplete({
				source: teacher,
				minLength:0,
				select: function( events, ui ) {
					$('#teacher_frid').val(ui.item.id);
				}
		});
	}

	function isNumberKey(evt){
        var e = window.event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;
        if ((charCode > 45 && charCode < 58) || charCode == 8){
            return true;
        }
        return false;
     }

</script>



