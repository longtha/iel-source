<style type="text/css">



	.table > tbody > tr > td, 
	.table > tfoot > tr > td, 
	.table > thead > tr > td
	{
    	line-height: 1.42857;
   		padding: 4px;
   		font-size: 16px;
	}
	.table > thead > tr > th.addWidth
	{
    	width: 30px !important;
	}
	.set_font_kh {
		padding: 5px 0px;
		font-size: 18px;
		font-weight:bold;
	}
	.setAlignCenter {
		text-align: center !important;
		vertical-align: middle !important;
		font-size: 12px;
	}

	
    .table > tbody > tr > td, .table > tfoot > tr > td, .table > thead > tr > td{
        font-size: 12px;
    }
</style>
<div class="wrapper">

	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
		      	<div class="col-xs-6">
		      		<strong id='title'><?php echo $page_header;?></strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
		      		
			    	<span class="top_action_button">	
			    		<?php if($this->green->gAction("E")){ ?>
				    		<a href="javaScript:void(0)" id="exports" title="Export to excel">
				    			<img id='export' src="<?php echo base_url('assets/images/icons/export.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>
		      		<span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ ?>
							<a href="javaScript:void(0)" id="print" title="Print">
				    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
				    		</a>
				    	<?php } ?>
                        <!--    hidden selector    -->
                        <input type="hidden" id="fyearid" value="<?php echo $fyearid?>"/>
                        <input type="hidden" id="fschlevelid" value="<?php echo $fschlevelid?>"/>
                        <input type="hidden" id="transno" value="<?php echo $transno?>"/>
                        <input type="hidden" id="classid" value="<?php echo $classid?>"/>

		      		</span>	
		      	</div> 			      
		  </div>
	      	<div class="col-sm-12">
	      		<div class="panel panel-default">
	      			<div class="panel-body">
	      				<!-- start class table responsive -->
		           		<div class="table-responsive">
		           		
							<div id="export_tap">
								<div class="col-sm-12">
						            <div class="col-sm-12 set_font_kh" align="center" style="font-size: 22px;font-weight:bold;"><b>ព្រះរាជាណាចក្រកម្ពុជា</b></div>
						            <div class="col-sm-12 set_font_kh" align="center" style="font-size: 22px;font-weight:bold;"><b>ជាតិ សាសនា ព្រះមហាក្សត្រ</b></div>
						            <div class="col-sm-12" style="height:20px;" style="font-size: 22px;font-weight:bold;">&nbsp;</div>
						           	<div class="col-sm-12 set_font_kh" align="center" style="font-size: 22px;font-weight:bold;"><b>សាលាហេបភីច័ន្ទតារានារីព្រែកថ្មី</b></div>
						            <div class="col-sm-12 set_font_kh underlinebouble" align="center" style="font-size: 22px;font-weight:bold;"><b>ចំណាត់ថ្នាក់ប្រចាំ ខែ<?php echo $this->evaluate->khmonth($month);?> ថ្នាក់ទី "​ <?php echo $this->evaluate->setClassName($classid);?> "​ ឆ្នាំសិក្សា <?php echo $this->green->GetYear($fyearid);?></b></div>
						        	<div class="col-sm-12" style="height:20px;" style="font-size: 22px;font-weight:bold;">&nbsp;</div>
						        </div>
						        <div class="clearfix"></div>
								<table class="table table-bordered table-striped" width="100%" border="1">
									<style type="text/css" media="all">
					           			.dsetRotateText{
											 -webkit-transform: rotate(-90deg);  /* Chrome, Safari 3.1+ */
										     -moz-transform: rotate(-90deg);  /* Firefox 3.5-15 */
										      -ms-transform: rotate(-90deg);  /* IE 9 */
										       -o-transform: rotate(-90deg);  /* Opera 10.50-12.00 */
										          transform: rotate(-90deg);							    
											-webkit-transform-origin: 50%  50%;
										}
										.table-bordered {
										    border: 1px solid black !important;
										}
										.table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
										    border: 1px solid black !important;
										}
										.table > thead > tr > th {
										    border-bottom: 2px solid black !important;
										    vertical-align: bottom;
										}

		           					</style>
						        	<thead>
						        		<tr>
						        			<th class="setAlignCenter" style="text-align: center !important;vertical-align: middle !important; font-size:1px;">
						        				លេខ
						        			</th>
						        			<th class="setAlignCenter addWidth" style="text-align: center !important;vertical-align: middle !important; font-size:14px;">នាម​ និងគោត្តនាម</th>
						        			<?php 
						        			$tr='';
						        			foreach($subtype as $stype)
						        			{
						        				//$totalCol = $this->evaluate->getTotalRowType($stype->subj_type_id);
                                                $totalCol = $stype->s_total;
						        				$tr.='<th colspan="'.$totalCol.'" class="setAlignCenter" style="text-align: center !important;vertical-align: middle !important; font-size:14px;">'.$stype->subject_type.'</th>';
						        			}
						        			echo $tr;
						        			?>
						        			
						        			<th colspan="4" class="setAlignCenter" style="text-align: center !important;vertical-align: middle !important; font-size:14px;">លទ្ធផលប្រចាំខែ</th>
						        		</tr>
						        		<tr>
						        			<th>&nbsp;</th>
						        			<th>&nbsp;</th>
						        			<?php 
						        			$tr='';
						        			foreach($subtype as $srow)
						        			{
						        				foreach($this->evaluate->getsubject($classid,$srow->subj_type_id,$fyearid,0) as $subjec_row)
						        				{
													$tr.='<th class="setRotateText"  style="text-align: center !important;vertical-align: middle !important; font-size:14px;">'.$subjec_row->subject_kh.'</th>';
						        				}
						        						      
						        			}
						        			echo $tr;
						        			?>
						        			<th class="setAlignCenter" style="text-align: center !important;vertical-align: middle !important; font-size:14px;">ពិន្ទុសរុប</th>
						        			<th class="setAlignCenter" style="text-align: center !important;vertical-align: middle !important; font-size:14px;">មធ្យមភាគ</th>
						        			<th class="setAlignCenter" style="text-align: center !important;vertical-align: middle !important; font-size:14px;">ចំនាត់ថ្នាក់</th>
						        			<th class="setAlignCenter" style="text-align: center !important;vertical-align: middle !important; font-size:14px;">និទ្ទេស</th>
						        		</tr>
						        	</thead>
						        	<tbody id="show_data"></tbody>
						        </table>
							</div>
							<!--  -->							
						</div> 
						  <!-- end class table responsive -->
					</div>
	      		</div>
	      	</div>	      	 
	    </div>
   </div>
</div>
<!--  end block -->
<script type="text/javascript">
	$(function(){
        $(document).ready(function(){
            views();
        });

		$('#print').on('click',function(){
			gsPrint(PrintingHtml());
		});
		$("#export").on("click",function(e){
                var data=$('.table').attr('border',1);
                    data = $("#export_tap").html().replace(/<img[^>]*>/gi,"");
                var export_data = $("<center><h3 align='center'></h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
                e.preventDefault();
                $('.table').attr('border',0);
        });

	});
	var views = function(){
		//$("#wait").css("display", "block");
		$.ajax({
			type:"POST",
			url:"<?php echo site_url('student/evaluate_moyes/getstudentinfo');?>",
			async:false,
			dataType:"Json",
			data:{
                'yearid'		: $('#fyearid').val(),
                'classid'		: $('#classid').val(),
                'schlevelid'	: $('#fschlevelid').val(),
                'transno'	    : $('#transno').val()
			},
			success:function(data){
				if(data.tdata!=""){
					$('#show_data').html(data.tdata);
				 }
				else{
					$('.listbody').html('<tr><td colspan="20" align="center">Data is empty display, Please try again..!</td></tr>');
				}
			}
		});
	}
	var PrintingHtml = function(){
		return $("<center>"+$("#export_tap").html()+"</center>").clone().find(".remove_tag").remove().end().html();
	}
	var gsPrint = function(data){
			var modes = { iframe : "iframe", popup : "popup" };
		 	var options = {
		 				mode 	:modes.iframe,
		 				popHt 	: 600,
		 				popWd 	: 700,
		 				popX 	: 0,
		 				popY 	: 0,
		 				popTitle:"<?php echo $page_header;?>",
		 				popClose: false,
		 				strict 	: false
		 			};
		 $("<div>"+data+"</div>").printArea(options);
	}
</script>

