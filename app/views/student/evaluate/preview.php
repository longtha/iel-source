
<?php
		$school=$this->db->where('schoolid',$this->session->userdata('schoolid'))->get('sch_school_infor')->row();
   			$school_name=$school->name;
   			$school_adr=$school->address;
   		$level=$this->db->where('classid',$eva->classid)->get('sch_class')->row()->grade_levelid;
   		$schlevel=$this->db->where('classid',$eva->classid)->get('sch_class')->row()->schlevelid;
   		$direcrow=$this->db->query("SELECT *
   									FROM sch_school_level sch
   									INNER JOIN sch_emp_profile emp
   									ON(sch.schlv_director=emp.empid)")->row();
   		$isvtc=$this->db->where('schlevelid',$schlevel)->get('sch_school_level')->row()->is_vtc;
   		$year=$this->db->where('yearid',$eva->yearid)->get('sch_school_year')->row()->sch_year;
   		$Student=$this->db->where('studentid',$eva->studentid)->where('yearid',$eva->yearid)->get('v_student_profile')->row();
   		if($schlevel==4)
   			$level=$this->db->select('p.proname')
   							->from('sch_student s')
   							->join('sch_school_promotion p','s.promot_id=p.promot_id','innter')
   							->where('s.studentid',$eva->studentid)->get()->row()->proname;
   		$mention=array('A','B','C','D','O');
   		$trimester=0;
   		if($eva->evaluate_type=='Three Month')
			$trimester=1;

		$semes_supfix="";
		if($eva->eval_semester=='3th'){
			$semes_supfix="3<sup>ème</sup>";
		}elseif($eva->eval_semester=='2nd'){
			$semes_supfix="2<sup>ème</sup>";
		}elseif($eva->eval_semester=='1st'){
			$semes_supfix="1<sup>er</sup>";
		}

		$pic_path=site_url('../assets/upload/No_person.jpg');
		if(file_exists(FCPATH.'assets/upload/students/'.$eva->yearid.'/'.$Student->student_num.'.jpg')){
			$pic_path=site_url('../assets/upload/students/'.$eva->yearid.'/'.$Student->student_num.'.jpg');
		}

?>

<div class="wrapper col-xs-10">
	<div class="wrapper">
		<div class="clearfix" id="main_content_outer">
		    <div id="main_content">
		      <div class="row result_info col-xs-12">
		      	<div class="col-xs-6">
		      		<strong>Evaluation Preview</strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">

		      		<span class="top_action_button">
			    		<a href="#" id="export" title="Export">
			    			<img src="<?php echo base_url('assets/images/icons/export.png')?>" />
			    		</a>
			    	</span>
		      		<span class="top_action_button">
						<a href="#"  title="Print">
			    			<img id="print" src="<?php echo base_url('assets/images/icons/print.png')?>" />
			    		</a>
		      		</span>
                    <a href="javascript:void(0)" id="close" title="Close" onclick="window.close()">
                        <img src="<?php echo base_url('assets/images/icons/close.png')?>" />
                    </a>
		      	</div>
		  </div>
		</div>
	</div>
</div>

<div class="row" id='export_tap'>
	<div class="col-sm-12" id='preview_wr'>
	    <div class="panel panel-default">
	      	<div class="panel-body">
		         <div class="table-responsive" id="tab_print">

						<style type="text/css">

							.sch_name{
								font-family:"Comic Sans MS","Times New Roman", Times, serif ;
								font-size:14px;
								font-weight:bold;
							}
							.Comic,#tblmention td,#tblmention th,.control-label,span.set,.tab_head th, label.control-label {
								font-family:"Comic Sans MS","Times New Roman",Arial !important;
								font-size:14px;
								font-weight:bold;
							}
							.time12{
								font-family:"Times New Roman",Arial,"Comic Sans MS";
								font-size:13px;
								padding-left:4px !important;
							}

							#preview td{
								padding: 3px ;
							}
							td img{border:1px solid #CCCCCC; padding: 1px;}
							#preview_wr{
								margin: 10px auto !important;
							}
							.left{
								text-align:left;
							}
							#tblmention th{padding: 3px;}
							ul.signatures li{list-style: none; display: inline-block; vertical-align:top;}
							ul.signatures li label{font-size: 10px;}
							img.signature{width: 70px; margin: 0px; border:none !important;}
							img.dir_signature{width: 155px; margin: 0px; border:none !important;}
							p{
								margin-bottom:2px !important;
							}
							.panel{
									border:none !important;
								}
						</style>

						<div style='width:200px; float:right;position:absolute;right:20px;top:2px;'>
							<img src="<?php echo base_url('assets/images/logo/logo.png')?>" style='width:200px;' />
						</div>
						<div style='margin-top: 8px; float:left;'>
							<table align='left' id='preview' >
									<tr class="Comic">
										<div class="sch_name"><?php echo $school_name ?></div>
									    <div class="sch_name"><?php if($isvtc!=1) echo "Niveau "; echo $level ?> - Evaluation <?php if($eva->evaluate_type!='month') echo $semes_supfix." trimestre"; else echo "pour le mois : ".$eva->month; echo " - $year"?></div>
										<div style='margin-top:12px'>
											<td><label class="control-label Comic">Elève : </label> </td>
											<td style='padding-top:0px; padding-bottom: 5px;'> <?php echo $Student->fullname; ?></td>
											<td width='20'></td>
											<td><label class="control-label Comic">Date de naissance : </label></td>
											<td class="left" style='padding-top:0px; padding-bottom: 5px;'> <?php echo $Student->dob; ?></td>
											<td width='20'></td>
											<td><label class="control-label Comic"><?php if($isvtc!=1) echo "Classe"; else echo "Training";?> : </label></td>
											<td style='padding-top:0px; padding-bottom: 5px;'> <?php echo $Student->class_name; ?></td>
										</div>
									</tr>
							</table>
						</div>
						<table align='center' id='comment' style='width:100%'>
								<tr>
									<td style='border:1px solid black; vertical-align:top;padding-left:2px'><label class="control-label"><u> Commentaire général sur la classe </u> : </label>
										<p></p><span class="time12"><?php echo $eva->class_comment;?></span>
									</td>
									<td width='200' style='vertical-align:top;'>
										<img src="<?php echo $pic_path ?>" style='width:140px; height:180px; float:right; margin-top:10px; margin-right:15px;'/>
									</td>
								</tr>
						</table>

						<br>
						<div class="table-responsive">
							<span class='mention_type control-label'>
								A= acquis, B= à renforcer, C= en cours d’acquisition, D= émergent, O= non évalué
							</span>
							<table id='tblmention' border='1' style='width:100%;' cellspacing='0' cellpadding='0'>

								<tr>
									<th colspan='2' style='text-align:center !important; vertical-align:middle;border-top:none' ></th>
									<?php
										foreach ($mention as $m) {
											echo "<th style='text-align:center !important; vertical-align:middle;'>".$m."</th>";
										}
									 ?>
								</tr>
								<?php
									foreach ($this->evaluate->gets_type($eva->classid,$eva->yearid,$trimester) as $sub) {
											$rowspan=$sub->s_total+1;
											echo "<tr>
													<th width='150' rowspan='".$rowspan."' style='text-align:center !important; vertical-align:middle; background-color:#EEEEEE; font-weight:bold !important;'>$sub->subject_type</th>
												</tr>";
											foreach ($this->evaluate->getsubject($eva->classid,$sub->subj_type_id,$eva->yearid,$trimester) as $subject) {
												echo "<tr>
														<th width='360'>$subject->subject</th>";
														foreach ($mention as $m) {
															if($this->evaluate->getmention($eva->evaluateid,$subject->subjectid)->mention==$m)
																echo "<th style='text-align:center !important; vertical-align:middle;'>X</th>";
															else
																echo "<td></td>";
														}
													echo "</tr>";
											}
									}
								 ?>

							</table>
							<br/>
							<table align='center' id='preview' style='width:100%'>
								<tr>
									<td colspan='2' style='border:1px solid black; vertical-align:top;'><label class="control-label"><u>Commentaire personnel sur l’élève</u> : </label>
										<br/>
										<table style='width:100%'>
											<?php
												if($eva->kh_teacher_comment!=''){
													echo "<tr>
															<td width='100' valign='top'><label class='control-label'>Khmer :</label></td>
															<td class='time12'>".$eva->kh_teacher_comment."</td>
														</tr>";
												}



												if($eva->forign_teacher_comment!=''){
													echo "<tr>
															<td width='100' valign='top'><label class='control-label'>Anglais :</label></td>
															<td class='time12'>".$eva->forign_teacher_comment."</td>
														</tr>";
												}
												if($eva->technic_com!=''){
													echo "<tr>
															<td width='100'><label class='control-label'>Course techniques:</label></td>
															<td class='time12'>".$eva->technic_com."</td>
														</tr>";
												}
												if($eva->supple_com!=''){
													echo "<tr>
															<td width='100' class='no_wrap'><label class='control-label'>Cours supplémentaires :</label></td>
															<td class='time12'>".$eva->supple_com."</td>
														</tr>";
												}
												if($eva->eng_teach_com!='' && $level>4){
													echo "<tr>
															<td width='100' valign='top'><label class='control-label'>Français :</label></td>
															<td class='time12'>".$eva->eng_teach_com."</td>
														</tr>";
												}

											?>
										</table>
									</td>
								</tr>

								<tr>
									<td align='left' style="padding-left:150px" class="control-label">Les enseignants:</td>
									<td align='center' valigh='top' style="min-width:200px;" class="control-label">La directrice:</td>
								</tr>
								<tr>
									<td width="80%">
										<table>
											<tr>
												<td >
													<ul class='signatures'>
													<?php
													$i=0;
														foreach ($this->evaluate->getteacherbyclass($eva->classid,$eva->yearid) as $t) {

															if($i==6) echo "</ul><span style='clear:both'></span><ul class='signatures'>";
															if(file_exists(FCPATH.'assets/upload/teacher/signature/'.$t->empid.'.jpg')){
																echo "<li>
																		<img class='signature' src='".base_url('/assets/upload/teacher/signature/'.$t->empid.'.jpg')."'/>
																	 </li>";
															}
															$i++;
														}

													 ?>
													</ul>
												</td>
											</tr>
										</table>
									</td>
									<td align='center' valign="top">
										<?PHP if(file_exists(FCPATH.'assets/upload/school/director/'.$schlevel.'.jpg')){
													echo "<span>
															<img class='dir_signature' src='".base_url('/assets/upload/school/director/'.$schlevel.'.jpg')."' width='140'/>
														 </span>";
												}
									?>
									</td>


								</tr>
							</table>
							<br/>

						</div>

					</div>

				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			$("#print").on("click",function(){
				gPrint("tab_print");
			});
		})
		$("#export").on("click",function(e){
				var data=$('.table').attr('border',1);
					data = $("#export_tap").html().replace(/<img[^>]*>/gi,"");
	   			var export_data = $("<center><h3 align='center'></h3>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
				window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
    			e.preventDefault();
    			$('.table').attr('border',0);
		});
	</script>

