<style type="text/css">
    table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }
    .cssclass {
        width: 650px;
    }
    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }
    
    #se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	.to {color:#cc66ff; font-weight:bold;}
	.from {color:#FF0000; font-weight:bold;}
	.class_name_{color:#FF0000;}
</style>
<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }

    // Program --------------------------------------------
    if(isset($getprogram) && count($getprogram)>0){
        $opprogram="";
        $opprogram="<option attr_price='0' value=''>--select--</option>";
        foreach($getprogram as $rowpro){
            $opprogram.="<option value='".$rowpro->programid."'>".$rowpro->program."</option>";
        }
        
    }
 ?>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
    <div class="col-sm-12">     
	    <div class="row">
	      	<div class="col-xs-12">
		        <div class="result_info">
		            <div class="col-xs-6" style="background: #4cae4c;color: white;">
		                <strong>Convert Student</strong>  
		            </div>
		            <div class="col-xs-6" style="text-align: right;background: #4cae4c;color: white;">
                        <a href="javascript:void(0);" class="btn btn-sm btn-success" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Convert Student"><span class="glyphicon glyphicon-plus"></span></a>
                        <a href="javascript:void(0);" class="btn btn-sm btn-success" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh"><span class="glyphicon glyphicon-refresh"></span></a>
		            </div>         
		        </div>
	      	</div>
	   	</div>
		<div class="in collapse" id="collapseExample">             
		    <form enctype="multipart/form-data" accept-charset="utf-8" method="post"  id="f_save">
		     	<div class="col-sm-12">
                    <div style="padding-top:10px;">
                        <div class="form_sep">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="student_num">ID<span style="color:red"></span></label>
                                    <input  type="text" name="student_num" id="student_num" class="form-control" placeholder="ID">
                                     
                                </div>
                                <div class="form-group">
                                    <label for="full_name">Name English <span style="color:red"></span></label>
                                    <input type="text" name="full_name" id="full_name" class="form-control" placeholder="English">
                                </div>
                                 <div class="form-group">
                                    <label for="full_Khmer">Name Khmer <span style="color:red"></span></label>
                                    <input type="text" name="full_Khmer" id="full_Khmer" class="form-control" placeholder="Khmer">
                                </div>
                                <div class="form-group">
                                    <label for="rangelevelid">Rang Level<span style="color:red"></span></label>
                                    <select name="rangelevelid" id="rangelevelid" class="form-control">
                                      
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group" style="display:none">
                                    <label for="schoolid">School Info<span style="color:red"></span></label>
                                    <select name="schoolid" id="schoolid" class="form-control">
                                               
                                   </select>
                                </div>
                                <div class="form-group">
                                    <label for="programid">Program<span style="color:red"> *</span></label>
                                    <select name="programid" id="programid" class="form-control" data-parsley-required="true" data-parsley-required-message="Select Program" >
                                          <?php echo $opprogram;?>     
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="schlevelid">School Level<span style="color:red"></span></label>
                                    <select name="schlevelids" id="schlevelids" class="form-control">
                                      
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="yearid">Year<span style="color:red"></span></label>
                                    <select name="yearid" id="yearid" class="form-control">

                                   </select>
                                </div>
                            </div>
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <label for="classid">From Class<span style="color:red"> *</span></label>
                                        <select name="classid" id="classid" class="form-control" data-parsley-required="true" data-parsley-required-message="Select From Class">
                                          
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <label for="to_classid">To Class<span style="color:red"> *</span></label>
                                        <select name="to_classid" id="to_classid" class="toclassid form-control" data-parsley-required="true" data-parsley-required-message="Select To Class" >
                                          
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
	
                    <div class="col-sm-12">
                        <div style="padding-top:10px; text-align: center; " class="form-group">
                            <button id="serch" class="btn btn-warning btn-sm" type="button" name="serch">Serch</button>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <button id="save" class="btn btn-success btn-sm save" type="button" name="save" value="save">Save</button>
                        </div>            
                    </div>
    		     	<div class="row">
    			        <div class="col-sm-12">
    			            <div class="form-group" style="border-bottom: 1px solid #CCC;">&nbsp;</div>           
    			        </div>
    		     	</div>
		  	</form>
		</div>
    <!-- show header -->
        <div >
            <div class="panel panel-default">
               <div class="table-responsive" id="div_export_print">
                    <div id="tab_print">
                        <table width="100%" border="0" class="table table-hover" id="setBorderTbl">
                            <thead>
	                             <tr style="background: #4cae4c;color: white;">
		                              <?php
		                                 foreach ($thead as $th => $val) {
                                            if ($th == 'No'){
                                                echo "<th class='$val'width='5%'>".$th."</th>";                                            
		                  					}else if($th == 'Photo'){
                                                echo"<th class='$val' width='8%'>".$th."</th>";
                                            }else{
		                                    	echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
											}
		                                 }
		                              ?>
                                      <td><input style="display:none-;" type='checkbox' name='checkAll[]'' class='checkAll' value=''></td>
	                           	</tr>
                            </thead>
                            <tbody id='listrespon'></tbody>
                        </table>
                     </div>
                 </div>
            </div>
            <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
                Display:<select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                            <?php
                                $num=50;
                                for($i=0;$i<10;$i++){?>
                                    <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                    <?php $num+=100;
                                }
                            ?>
                        </select>
            </div>
            <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
                <div class="pagination" id="pagination" style="text-align:center"></div>
            </div>
        </div>
    </div>
</div>
 <script type="text/javascript">
    $(function(){
        $("#year,#schlevelid").hide();
        Getdata(1,$('#sort_num').val());
        // sort_num ------------------------------------
        $('#sort_num').change(function(){
            var  total_display= $(this).val();
            Getdata(1,total_display);
        });
        // pagenav --------------------------------------
        $("body").delegate(".pagenav","click",function(){
            var page = $(this).attr("id");
            var total_display = $('#sort_num').val();
            Getdata(page,total_display);
        });
        // toclass ------------------------------------
        $(".toclassid,#classid").change(function(){
            $('#serch').click();
            $('.checkAll ').attr('checked', false);
        });
        // keychang -----------------------------------
        $("#programid").on('change',function(){
            get_schlevel($(this).val());
        });
        $("#schlevelids ").on('change',function(){
            get_schyera($(this).val());
        });
        $("#rangelevelid").on('change',function(){
            get_schclass($(this).val());
            get_toschclass($(this).val());
        });
        $("#yearid").on('change',function(){
            schranglavel($(this).val());
        });
        $("#to_classid").on('change',function(){           
            keychang();
        });
        // btn_search ---------------------------------
        $('body').delegate('#serch', 'click', function(){
            Getdata();
        });
        // key enter ---------------------------------
        $(document).keypress(function(e) {
          if(e.which == 13) {
              Getdata(1,$('#sort_num').val());
          }
        });
        // refresh -----------------------------------
        $('body').delegate('#refresh', 'click', function(){
            location.reload();
        });

        // Addnew--------------------------------------
        $('body').delegate('#a_addnew', 'click', function(){
            $('#collapseExample').collapse('toggle')
            cleadata();
        });

        // main checkAll-------------------------------
        $('body').delegate('.checkAll', 'change', function(){
            var cc = this.checked;               
            $('.checksub').each(function(){
                 $(this).prop('checked', cc);
                
            });

        });
        // checksub ----------------------------------
        $('body').delegate('.checksub', 'change',function(){
            checked_all();
        });

        // save --------------------------------------
        $("body").delegate("#save", "click", function (){
            if($('#f_save').parsley().validate()){
                var i = 0;
                var arr_check = [];
                $(".checksub:checked").each(function(i){
                    var studentId  = $(this).attr('data-studentId');
                    var schoolid   = $(this).attr('data-schoolid');
                    var enrollID  = $(this).parent().parent().find('.enrollid').attr('data-enrolid');
                    
                    var yearid  = $(this).parent().parent().find('.yearid').val();
                    var programid  = $(this).parent().parent().find('.programid').val();
                    var schlevelid  = $(this).parent().parent().find('.schlevelid').val();
                    arr_check[i] = {studentId : studentId, schoolid : schoolid, enrollID : enrollID,yearid:yearid,programid:programid,schlevelid:schlevelid};
                    i++;
                });
                var langdata=(arr_check.length);
                //--------------
                if(langdata>0){
                    $.ajax({ 
                            type : "POST",
                            url  : "<?= site_url('student/c_convert_student/savedata') ?>",
                            beforeSend: function() {
                                $('.xmodal').show();
                            },
                            complete: function() {
                                $('.xmodal').hide();
                            },
                            data : {
                                enrollid    : $(".enrollid").val(),
                                to_classid  : $("#to_classid").val(),
                                classid  : $("#classid").val(),
                                rangelevelid  : $("#rangelevelid").val(),                                       
                                arr_check      : arr_check
                            }, 
                            success:function(data){
                                toastr["success"]("Convert students was created...!"); 
                                //$('#collapseExample').collapse('toggle');
                                Getdata(1,$('#sort_num').val());
                                cleadata();
                                $("#save").html('Save'); 
                            },
                            error:function(){
                                toastr["warning"]("Cant not convert students created exist...?");
                            }

                    });
                }else{
                    toastr["warning"](" Please check data first before process.!");
                }
                //--------------
            }  
            
        });

    });// end functiont 
    
    // Getdata -----------------------------------------------------------
        function Getdata(page,total_display,sortby,sorttype){
            var roleid = <?php echo $this->session->userdata('roleid');?>;
            var per = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
            var sort_num = $('#sort_num').val();
            var student_num = $('#student_num').val();
            var full_name = $('#full_name').val();
            var yearid = $('#yearid').val();
            var programid = $('#programid').val();
            var schlevelids = $('#schlevelids').val();
            var classid = $('#classid').val();
            var toclassid = $('.toclassid option:selected').text();
            var rangelevelid = $('#rangelevelid').val();
            var full_Khmer = $('#full_Khmer').val();

            $.ajax({
                url:"<?php echo base_url();?>index.php/student/c_convert_student/Getdata",
                type: "POST",
                datatype: "Json",
                async: false,
                beforeSend: function() {
                    $('.xmodal').show();
                },
                complete: function() {
                    $('.xmodal').hide();
                },
                data:{
                        'page'  : page,
                        'p_page': per,
                        'total_display': total_display,
                        'sort_num':sort_num,
                        'sortby':sortby,
                        'sorttype':sorttype,
                        'student_num':student_num,
                        'full_name':full_name,
                        'yearid':yearid,
                        'programid':programid,
                        'schlevelids':schlevelids,
                        'classid':classid,
                        'toclassid':toclassid,                        
                        'rangelevelid': rangelevelid,
                        'full_Khmer': full_Khmer
                },
                success: function (data) {
                    $("#listrespon").html(data.data);
                    $('.pagination').html(data['pagination']['pagination']);
                }
            });
        }
        // sort -------------------------------------------------------------
        function sort(event){
            this.sortby = $(event.target).attr("rel");
            if ($(event.target).attr("sorttype") == "ASC") {
                $('.sort').removeClass('cur_sort_up');
                $(event.target).addClass(' cur_sort_down');
                $(event.target).attr("sorttype", "DESC");
                this.sorttype = "ASC";
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_up');
            } else if ($(event.target).attr("sorttype") == "DESC") {
                $(event.target).attr("sorttype", "ASC");
                this.sorttype = "DESC";
                $('.sort').removeClass('cur_sort_up');
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_down');    
            }        
            Getdata(1,$('#sort_num').val(),this.sortby,this.sorttype);
        }

        // copare funtioni-------------------------------------------------
        function keychang(){
            var from_classid = $('#classid').val();
            var to_classid   = $('#to_classid').val();
            if(from_classid !="" && to_classid !=""){
                if((from_classid == to_classid)){
                   toastr["warning"]("Convert students class duplicate...!");
                   $("#to_classid").val("");
                   //$(this).focus();
                }
            }
        }
        // get_schlevel --------------------------------------------------
        function get_schlevel(programid){
           $.ajax({        
              url: "<?= site_url('student/c_convert_student/get_schlevel') ?>",    
              data: {         
                'programid':programid
              },
              type: "post",
              dataType: "json",
                success: function(data){ 

                    var opt = '';
                        opt="<option  value=''>--select--</option>";
                    if(data.schlevel.length > 0){
                        $.each(data.schlevel, function(i, row){
                             opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                        });
                    }else{
                        opt += '';
                      }

                    $('#schlevelids').html(opt);
                    get_schyera($('#programid').val(), $('#schlevelids').val());
                    get_schclass($('#programid').val(), $('#classid').val());
                    get_toschclass($('#programid').val(), $('#to_classid').val());
                }
            });
        }
    //get yare---------------------------------------------------------
    function get_schyera(schlevelids){
        $.ajax({        
            url: "<?= site_url('student/c_convert_student/get_schyera') ?>",    
            data: {         
              'schlevelids':schlevelids
            },
            type: "post",
            dataType: "json",
            success: function(data){ 
                var ya = '';
                    ya="<option  value=''>--select--</option>";
                if(data.schyear.length > 0){
                    $.each(data.schyear, function(i, row){
                        ya += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                    });
                }else{
                    ya += '';
                }

                $('#yearid').html(ya);
                 schranglavel($('#yearid').val(), $('#rangelevelid').val());
            }
        });
    }
    // from ranglavel--------------------------------------------------
    function schranglavel(yearid){
        $.ajax({        
            url: "<?= site_url('student/c_convert_student/schranglavel') ?>",    
            data: {         
              'yearid':yearid
            },
            type: "post",
            dataType: "json",
            success: function(data){ 
                var rang = '';
                    rang="<option  value=''>--select--</option>";
                if(data.schranglavel.length > 0){
                    $.each(data.schranglavel, function(i, row){
                        rang += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
                    });
                }else{
                    rang += '';
                }
                
                $('#rangelevelid').html(rang);
            }
        });
    }
    // from class------------------------------------------------------
    function get_schclass(rangelevelid){
      $.ajax({        
          url: "<?= site_url('student/c_convert_student/get_schclass') ?>",    
          data: {         
            'rangelevelid':rangelevelid
          },
          type: "post",
          dataType: "json",
          success: function(data){ 
            var cla = '';
                  cla="<option  value=''>--select--</option>";
            if(data.schclass.length > 0){
                $.each(data.schclass, function(i, row){
                    cla += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                });
            }else{
                cla += '';
            }

            $('#classid').html(cla);
            //$('#to_classid').html(cla);
          }
      });
    }

    // to  class------------------------------------------------------
    function get_toschclass(rangelevelid){
      $.ajax({        
          url: "<?= site_url('student/c_convert_student/get_toschclass') ?>",    
          data: {         
            'rangelevelid':rangelevelid
          },
          type: "post",
          dataType: "json",
          success: function(data){ 
            var cla = '';
                  cla="<option  value=''></option>";
            if(data.schclass.length > 0){
                $.each(data.schclass, function(i, row){
                    cla += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                });
            }else{
                cla += '';
            }
            $('#to_classid').html(cla);
          }
      });
    }

    // number key ---------------------------------------------
    function isNumberKey(evt) {

        var e = window.event || evt;  
        var charCode = e.which || e.keyCode;
        if ((charCode > 45 && charCode < 58) || charCode == 8) {
            return true;
        }
        return false;
    }
    // cleardata ----------------------------------------------
    function cleadata(){
        $("#to_classid").val("");
        $("#enrollid").val("");
        $('.checksub').attr('checked', false);
        $('.checkAll ').attr('checked', false);
    }

    // checked_all --------------------------------------
    function checked_all(){
        var ca = true;
        $('.checksub').each(function() {
            var cc = this.checked;
            if (!cc) {
                ca = false;
            }

        });
        $('.checkAll').prop('checked', ca);
    }
    
</script>