<?php
$yearid = $_GET['yearid'];
$year = $this->syear->getschoolyearrow($yearid);
$studentid = $student['studentid'];
?>
<style type="text/css">
    #preview td {
        padding: 3px;
    }

    th img {
        border: 1px solid #CCCCCC;
        padding: 2px;
    }

    #preview_wr {
        margin: 10px auto !important;
    }

    .tab_head th, label.control-label {
        font-family: "Times New Roman", Times, serif !important;
        font-size: 14px;
    }

    td {
        font-family: "Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important;
        font-size: 14px;
    }

</style>
<?php
//$studentinfo = $this->std->getstudent($studentid);


    $familyid = $student['familyid'];
    $family_code ='';
    $family_name ='';
    $family_book_no ='';
    $family_home_no = "";
    $family_street = "";
    $family_village = "";
    $family_commune = "";
    $family_district = "";
    $family_province = "";
    $family_tel = "";
    $father_name ='';
    $father_ocupation ='';
    $father_dob ='';
    $father_home ='';
    $father_phone ='';
    $father_street ='';
    $father_village ='';
    $father_commune ='';
    $father_district ='';
    $father_province ='';
    $mother_name ='';
    $mother_ocupation ='';
    $mother_dob ='';
    $mother_home ='';
    $mother_phone ='';
    $mother_street ='';
    $mother_village ='';
    $mother_commune ='';
    $mother_district ='';
    $mother_province ='';

    if( $familyid !="" && $familyid !=0){
        $family = $this->family->getfamilyrow($familyid);
        if(count($family)>0){
            $family_code = $family["family_code"];
            $family_name = $family["family_name"];
            $family_book_no = $family["family_book_record_no"];
            $family_home_no = $family["home_no"];
            $family_street = $family["street"];
            $family_village = $family["village"];
            $family_commune = $family["commune"];
            $family_district = $family["district"];
            $family_province = $family["province"];
            $family_tel = $family["tel"];
            $father_name = $family["father_name"];
            $father_ocupation = $family["father_ocupation"];
            $father_dob = $this->green->convertSQLDate($family["father_dob"]);
            $father_home = $family["father_home"];
            $father_phone = $family["father_phone"];
            $father_street = $family["father_street"];
            $father_village = $family["father_village"];
            $father_commune = $family["father_commune"];
            $father_district = $family["father_district"];
            $father_province = $family["father_province"];
            $mother_name = $family["mother_name"];
            $mother_ocupation = $family["mother_ocupation"];
            $mother_dob = $this->green->convertSQLDate($family["mother_dob"]);
            $mother_home = $family["mother_home"];
            $mother_phone = $family["mother_phone"];
            $mother_street = $family["mother_street"];
            $mother_village = $family["mother_village"];
            $mother_commune = $family["mother_commune"];
            $mother_district = $family["mother_district"];
            $mother_province = $family["mother_province"];
        }
        
    }

$family = $this->family->getfamily($familyid );

$student_photo = base_url('assets/upload/NoImage.png');
if (file_exists(FCPATH . "assets/upload/students/" . $_GET['yearid'] . '/' . $student["studentid"] . ".jpg")) {
    $student_photo = base_url("assets/upload/students/" . $_GET['yearid'] . '/' . $student["studentid"] . ".jpg");
}

?>
<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
        <div id="main_content">
            <div class="row result_info col-xs-10">
                <div class="col-xs-6">
                    <strong>Student Detail</strong>
                </div>
                <div class="col-xs-6" style="text-align: right">

		      		<span class="top_action_button">
						<a href="javascript:void(0)" id="print" title="Print">
                            <img src="<?php echo base_url('assets/images/icons/print.png') ?>"/>
                        </a>
		      		</span>
                    <span class="top_action_button">
						<a href="javascript:void(0)" id="close" title="Close" onclick="window.close()">
                            <i class="fa fa-times-circle fa-2x"></i>
                        </a>
		      		</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-10" id='preview_wr'>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive" id="tab_print">
                    <style type="text/css">
                        #preview td {
                            padding: 3px;
                        }

                        th img {
                            border: 1px solid #CCCCCC;
                            padding: 2px;
                        }

                        #preview_wr {
                            margin: 10px auto !important;
                        }

                        .tab_head th, label.control-label {
                            font-family: "Times New Roman", Times, serif !important;
                            font-size: 14px;
                        }

                        td {
                            font-family: "Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important;
                            font-size: 14px;
                        }

                    </style>
                    <div style='width:250px; float:left;'>
                        <img src="<?php echo base_url('assets/images/logo/logo.png') ?>" style='width:240px;'/>

                    </div>
                    <div style='float:left;'>
                        <h5 style='margin:15px 5px 0px 0px; font-weight:bold;'><?php echo $this->db->where("schoolid",$this->session->userdata("schoolid"))->get("sch_school_infor")->row()->name; ?></h5>
                        <span style='text-align:center;'><?php echo $this->db->where("schoolid",$this->session->userdata("schoolid"))->get("sch_school_infor")->row()->address; ?></span><br/>
                    </div>
                    <p style="clear:both"></p>
                    <table align='center'>
                        <thead>
                        <th valign='top' align="center" style='width:80%'>
                            <h5 align="center"><u>FICHE DE RENSEIGNEMENTS</u></h5>

                            <div style='text-align:center;'>School Year: <?php echo $year->sch_year; ?> </div>
                            <?php if(isset($student['proname']) && $student['proname']){ ?>
                                <div style='text-align:center;'><?php echo $student['proname']; ?> </div>
                            <?php } ?>
                        </th>
                        <th align='right'>
                            <img src='<?php echo $student_photo ?>'
                                 style='width:140px; height:180px; float:right; margin-top:10px; margin-right:15px;'/>
                        </th>
                        </thead>
                    </table>
                    <table align='center' id='preview' style='width:100%'>
                        <tr>
                            <td><label class="control-label">Student ID :</label></td>
                            <td><?php echo $student['student_num']; ?></td>
                            <td><label class="control-label">Family ID :</label></td>
                            <td><?php echo $family_code; ?></td>
                            <td><label class="control-label">Class : </label><?php echo $student['class_name']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Student Name:</label></td>
                            <td><?php echo $student['last_name'] . " " . $student['first_name']; ?></td>

                            <td><label class="control-label">Name Khmer:</label></td>
                            <td colspan='2'><?php echo $student['last_name_kh'] . " " . $student['first_name_kh']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Date Of Birth : </label></td>
                            <td colspan='4'><?php echo $student['dob']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Nationality : </label></td>
                            <td colspan='4'><?php echo $student['nationality']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Address : </label></td>
                            <td colspan='4'><?php echo $student['home_no'].",".$student['street'].",".$student['village'].",".$student['commune'].",".$student['district'].", ".$student['province']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Mother Name:</label></td>
                            <td><?php echo $mother_name; ?></td>
                            <td><?php //echo $mother_name_kh; ?></td>
                            <td><label class="control-label">Date Of Birth : </label></td>
                            <td><?php echo $mother_dob; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Mother Occupation : </label></td>
                            <td><?php echo $mother_ocupation; ?></td>
                            <td></td>
                            <td colspan='2'></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Father Name:</label></td>
                            <td><?php echo $father_name; ?></td>
                            <td><?php //echo $family->father_name_kh; ?></td>
                            <td><label class="control-label">Date Of Birth : </label></td>
                            <td><?php echo $father_dob; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Father Occupation : </label></td>
                            <td><?php echo $father_ocupation; ?></td>
                            <td></td>
                            <td colspan='2'></td>
                        </tr>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $("#print").on("click", function () {
            gPrint("tab_print", "Evaluation");
        });

    })
</script>

