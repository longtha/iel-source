<?php
$yearid = $_GET['yearid'];
$year = $this->syear->getschoolyearrow($yearid);
$studentid = $student['studentid'];
?>
<style type="text/css">
    #preview td {
        padding: 3px;
    }

    th img {
        border: 1px solid #CCCCCC;
        padding: 2px;
    }

    #preview_wr {
        margin: 10px auto !important;
    }

    .tab_head th, label.control-label {
        font-family: "Times New Roman", Times, serif !important;
        font-size: 14px;
    }

    td {
        font-family: "Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important;
        font-size: 14px;
    }
    
     .listbody td {
        font-family: "Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important;
        font-size: 14px;
        text-align: center;
    }
    
    .menutab .tab ul li.active {
        background: #ffffff none repeat scroll 0 0;
        line-height: 22px;
        margin-bottom: -3px;
        position: relative;
        top: -2px;
        font-size: 14px;
    }
    .menutab .tab ul li a.active {
        color: #008000;
    }
    .menutab .tab ul li a {
        color: #3b5998;
        font-size: 13px;
        text-decoration: none;
    }
    .menutab .tab {
        border-bottom: 1px solid #ccc;
        float: left;
        padding-left: 20px;
        width: 100%;
    }
    .menutab .tab ul li {
        border-left: 1px solid #ccc;
        border-right: 1px solid #ccc;
        border-top: 1px solid #ccc;
        float: left;
        list-style: outside none none;
        padding: 3px 15px;
    }
    .menutab{margin-top: 20px;}

</style>
<?php
//$studentinfo = $this->std->getstudent($studentid);


    $familyid = $student['familyid'];
    $family_code ='';
    $family_name ='';
    $family_book_no ='';
    $family_home_no = "";
    $family_street = "";
    $family_village = "";
    $family_commune = "";
    $family_district = "";
    $family_province = "";
    $family_tel = "";
    $father_name ='';
    $father_ocupation ='';
    $father_dob ='';
    $father_home ='';
    $father_phone ='';
    $father_street ='';
    $father_village ='';
    $father_commune ='';
    $father_district ='';
    $father_province ='';
    $mother_name ='';
    $mother_ocupation ='';
    $mother_dob ='';
    $mother_home ='';
    $mother_phone ='';
    $mother_street ='';
    $mother_village ='';
    $mother_commune ='';
    $mother_district ='';
    $mother_province ='';

    if( $familyid !="" && $familyid !=0){
        $family = $this->family->getfamilyrow($familyid);
        if(count($family)>0){
            $family_code = $family["family_code"];
            $family_name = $family["family_name"];
            $family_book_no = $family["family_book_record_no"];
            $family_home_no = $family["home_no"];
            $family_street = $family["street"];
            $family_village = $family["village"];
            $family_commune = $family["commune"];
            $family_district = $family["district"];
            $family_province = $family["province"];
            $family_tel = $family["tel"];
            $father_name = $family["father_name"];
            $father_ocupation = $family["father_ocupation"];
            $father_dob = $this->green->convertSQLDate($family["father_dob"]);
            $father_home = $family["father_home"];
            $father_phone = $family["father_phone"];
            $father_street = $family["father_street"];
            $father_village = $family["father_village"];
            $father_commune = $family["father_commune"];
            $father_district = $family["father_district"];
            $father_province = $family["father_province"];
            $mother_name = $family["mother_name"];
            $mother_ocupation = $family["mother_ocupation"];
            $mother_dob = $this->green->convertSQLDate($family["mother_dob"]);
            $mother_home = $family["mother_home"];
            $mother_phone = $family["mother_phone"];
            $mother_street = $family["mother_street"];
            $mother_village = $family["mother_village"];
            $mother_commune = $family["mother_commune"];
            $mother_district = $family["mother_district"];
            $mother_province = $family["mother_province"];
        }
        
    }

$family = $this->family->getfamily($familyid );

$student_photo = base_url('assets/upload/No_person.jpg');
if (file_exists(FCPATH . "assets/upload/students/" . $_GET['yearid'] . '/' . $student["studentid"] . ".jpg")) {
    $student_photo = base_url("assets/upload/students/" . $_GET['yearid'] . '/' . $student["studentid"] . ".jpg");
}

?>
<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
        <div id="main_content">
            <div class="row result_info col-xs-12">
                <div class="col-xs-6">
                	<?php 
                	   $link=site_url("student/student/detail/".$studentid."?yearid=".$yearid);
                	?>
                     <a href="<?php echo $link ?>" ><strong>Student Detail</strong></a> >> <span>Modify Student</span>
                </div>
                <div class="col-xs-6" style="text-align: right">

		      		<span class="top_action_button">
						<a href="javascript:void(0)" id="print" title="Print">
                            <img src="<?php echo base_url('assets/images/icons/print.png') ?>"/>
                        </a>
		      		</span>                   
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12" id='preview_wr'>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive" id="tab_print">
                    <style type="text/css">
                        #preview td {
                            padding: 3px;
                        }

                        th img {
                            border: 1px solid #CCCCCC;
                            padding: 2px;
                        }

                        #preview_wr {
                            margin: 10px auto !important;
                        }

                        .tab_head th, label.control-label {
                            font-family: "Times New Roman", Times, serif !important;
                            font-size: 14px;
                        }

                        td {
                            font-family: "Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important;
                            font-size: 14px;
                        }

                    </style>
                   
                   
                    <p style="clear:both"></p>
                    <table align='center'>
                        <thead>
                        <th valign='top' align="center" style='width:80%'>
                            <h5 align="center"><u>FICHE DE RENSEIGNEMENTS</u></h5>

                            <div style='text-align:center;'>School Year: <?php echo $year->sch_year; ?> </div>
                            <?php if(isset($student['proname']) && $student['proname']){ ?>
                                <div style='text-align:center;'><?php echo $student['proname']; ?> </div>
                            <?php } ?>
                        </th>
                        <th align='right'>
                            <img src='<?php echo $student_photo ?>'
                                 style='width:140px; height:180px; float:right; margin-top:10px; margin-right:15px;'/>
                        </th>
                        </thead>
                    </table>
                    <table align='center' id='preview' style='width:100%'>
                        <tr>
                            <td><label class="control-label">Student ID :</label></td>
                            <td><?php echo $student['student_num']; ?></td>
                            <td><label class="control-label">Family ID :</label></td>
                            <td><?php echo $family_code; ?></td>
                            <td><label class="control-label">Class : </label><?php echo $student['class_name']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Student Name:</label></td>
                            <td><?php echo $student['last_name'] . " " . $student['first_name']; ?></td>

                            <td><label class="control-label">Name Khmer:</label></td>
                            <td colspan='2'><?php echo $student['last_name_kh'] . " " . $student['first_name_kh']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Date Of Birth : </label></td>
                            <td colspan='4'><?php echo $student['dob']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Nationality : </label></td>
                            <td colspan='4'><?php echo $student['nationality']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Address : </label></td>
                            <td colspan='4'><?php echo $student['home_no'].",".$student['street'].",".$student['village'].",".$student['commune'].",".$student['district'].", ".$student['province']; ?></td>
                        </tr>
                       
                        
                    </table>
                    
                    <div class="menutab">
                    	<div class="tab">
                    		
                    		<ul>	               				    				
                    			<li class="active " data-toggle="tab">សៀវភៅទំនាក់ទំនង</li>		
                    		</ul>
                    	</div>
                    	<div class="tab_content">	
                    		<form enctype="multipart/form-data" class="gform" name="std_register" id="std_register" method="POST" action="<?php echo site_url("student/student/bookupdate")?>">
                    			<div class="panel-body" style="margin-top: 10px;float:left;">
        			              	<div class="col-sm-12 form_sep">       			              		
        								<div class="col-sm-2">
        									<label class="req" for="month"> ខែ </label>
        								</div>
        								<div class="col-sm-4">       							
        									<select name="month" class="form-control">
									 			<?php                   		
									               foreach($month as $r):
			                    	            ?>
    				                    				<option value="<?php echo $r->id ?>" <?php if(isset($obj['monthid'])){ if($r->id==$obj['monthid']) echo 'selected'; }?>><?php echo $r->month_kh ?></option>
            					                <?php 
            					                   endforeach;
            					                ?>
        									</select>      								
        								</div>
        																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep">       			              		
        								
        								<div class="col-sm-2">
        									<label class="req" for="month">  សប្ដាហ៍</label>
        								</div>
        								<div class="col-sm-4">        							
        									<select name="week" class="form-control">
									 			<?php                   		
									               foreach($week as $r):
			                    	            ?>
    				                    				<option value="<?php echo $r->id ?>" <?php if(isset($obj['weekid'])){ if($r->id==$obj['weekid']) echo 'selected'; }?>><?php echo $r->week_kh ?></option>
            					                <?php 
            					                   endforeach;
            					                ?>
        									</select> 								
        								</div>      																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep">        								
        								<div class="col-sm-2">
        									<label class="req" for="mentality">ចិត្តចលភាព</label>
        								</div>
        								<div class="col-sm-4">       							
        									<textarea name="mentality" class="form-control"><?php echo $obj['mentality'] ?></textarea>        								
        								</div>        								  																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep">
        								<div class="col-sm-2">
        									<label class="req" for="mentality">បញ្ញា</label>
        								</div>
        								<div class="col-sm-4">       							
        									<textarea name="intellectual" class="form-control" ><?php echo $obj['intellectual'] ?></textarea>        								
        								</div>        								  																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep">
        								<div class="col-sm-2">
        									<label class="req" for="mentality">អារម្មណ៍</label>
        								</div>
        								<div class="col-sm-4">       							
        									<textarea name="feeling" class="form-control" ><?php echo $obj['feeling'] ?></textarea>        								
        								</div>        								  																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep">
        								<div class="col-sm-2">
        									<label class="req" for="mentality">សង្គម</label>
        								</div>
        								<div class="col-sm-4">       							
        									<textarea name="society" class="form-control" ><?php echo $obj['society'] ?></textarea>        								
        								</div>        								  																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep" style="margin-top: 20px;">
            			              	<div class="col-sm-2">
                                            <input id="add_academic" class="btn btn-primary" type="submit" name="add_academic" value="Save">
                                        	<input type="hidden" name="yearid" id="yearid" value="<?php echo $yearid; ?>">
                                        	<input type="hidden" name="studentid" id="studentid" value="<?php echo $studentid; ?>">
                                        	<input type="hidden" name="bookid" id="bookid" value="<?php echo $bookid; ?>">
                                        </div>
                                    </div>
    			              	</div>
                    		</form>
                    	</div>
                    	
                    	<!-- <div class="rows">
            		      	<div class="col-sm-12">
            		      		<div class="panel panel-default">
            		      			<div class="panel-body">
            			           		<div class="table-responsive">
            			           			<?php 
            			           			$thr="";	
            			           			$tr="";	
            			           			$school=$this->db->where('schoolid',$this->session->userdata('schoolid'))->get('sch_school_infor')->row();
            			           			$school_name=$school->name;
            			           			$school_adr=$school->address;
            			           			foreach($thead as $th=>$val){           			          				
                                                $thr.="<th class='sort $val' onclick='sort(event);' rel='$val'>".$th."</th>";								
            			           			}
            			           			
            			           			if(count($tdata) > 0){
            			           			    $i=1;
            			           			    foreach($tdata as $row){            			           			                  			           			       
                                                    $tr.="<tr>
							                         <td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
							                         <td class='student_num'>".$row->month_kh."</td>
    											     <td class='student_num'>".$row->week_kh."</td>
                                                    <td class='student_num'>".$row->mentality."</td>
                                                    <td class='student_num'>".$row->intellectual."</td>
                                                    <td class='student_num'>".$row->feeling."</td>
                                                    <td class='student_num'>".$row->society."</td>
											 
        											 <td class='remove_tag'>";
                    			           			        
                    			           			        if($this->green->gAction("D")){
                    			           			            $tr.="<a class='del_row' rel=".$row->id." ><img src='".site_url('../assets/images/icons/delete.png')."'/></a>";
                    			           			        }
                    			           			        if($this->green->gAction("U")){
                    			           			            
                    			           			            $edit_link=site_url("student/student/edit/".$row->id);
                    			           			            $tr.="<a href='".$edit_link."' target='_blank'>
        	                                                    <img src='".site_url('../assets/images/icons/edit.png')."'/>
        	                                                  </a>";
                    			           			        }
                    			           			       
                    			           			        $tr.="</td>
											     </tr>";
            			           			        $i++;
            			           			    }
            			           			}
            														
            			           			?>
            
            			           			<table class="table" border="0">
            			           				<thead ><?php echo $thr ?></thead>
            			           				<thead class='remove_tag'>
            			           					<td class='col-xs-1' >
            			           						<input type='text' id='page' class='hide' value="<?PHP if(isset($_GET['per_page'])) echo $_GET['per_page'] ?>" />
            			           						<input type='text' value='asc' name='sort' id='sort' style='width:30px; display:none'/>			           						
            										</td>
            								 		<td class='col-xs-1'>
            								 			<select class='form-control input-sm parsley-validated' name='s_classid' id='s_classid'>
    	                   						 			<option value="">Month</option>
                						                    <?php
                						                    	foreach ($this->std->getmonth() as $class) {?>
                						                    		<option value="<?php echo $class->id ;?>" <?php if(isset($_GET['class'])){ if($class->id==$_GET['class']) echo 'selected'; }?> ><?php echo $class->month_kh;?></option>
                						                    	<?php 
                						                    	}
                						                    ?>
            					                 		</select>
            					              		</td>
            					              		
            					              		<td>
            					              			<input type="button" name="search" id="search" class="btn btn-primary" value="Search" style="height: 30px !important;">
            					              		</td>
            			           											 		
            			           				</thead>
            			           				<tbody class='listbody'>
            			           					<?php echo $tr ?>
            			           					
            			           				</tbody>
            			           			</table>
            							</div>  
            						</div>
            		      		</div>
            		      	</div>	      	
            		    </div> -->
                    	
                    	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $("#print").on("click", function () {
            gPrint("tab_print", "Evaluation");
        });

        $('#search').click(function(){   		
			search();
    	});

        $('body').delegate('.del_row','click',function(){
            var obj = $(this);
            var subjectid=$(this).attr("rel");
            deletesubject(obj,subjectid);
        });

    });
    function search(event){
		var studentid 	 = jQuery('#studentid').val();
		var yearid 	 = jQuery('#yearid').val();
		var s_classid 	 = jQuery('#s_classid').val();
				
		$.ajax({
					url:"<?php echo base_url(); ?>index.php/student/student/searchbook",    
					data: {
						's_classid'	: s_classid,
						'studentid'			: studentid,
						'yearid'		: yearid
					},
					type: "POST",
					dataType:'json',
					async:false,
					success: function(data){
                       jQuery('.listbody').html(data.data);
				}
			});
		}

    function deletesubject(obj,subjectid){
        var r = confirm("Are you sure to delete this item?");  
        var tr= obj.closest("tr");                
        if (r == true) {              
             $.ajax({
                  url: "<?php echo site_url('student/student/deletebook');?>",
                  dataType: "Json",
                  type: "POST",
                  async: false,
                  data: {
                     'bookid': subjectid
                  },
                  success: function (data) { 
                      if(data==1){
                          toastr["success"]("Book has been deleted !");
                          tr.remove();
                      }else{
                          toastr["warmning"]("Book can't delete !");
                      } 
                  }
              });   
         }else{
            return false;
         }            
    }
</script>

