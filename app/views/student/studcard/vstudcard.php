<?php
	
	if(!empty($_GET['yearid']) && !empty($_GET['classid']))
	{
		$this->db->where('yearid',$_GET['yearid']);
		$year=$this->db->get('sch_school_year')->row()->sch_year;
		$class_id = $_GET['classid'];
		$yearid = $_GET['yearid'];
	}
	else
	{
		$yearid=$this->session->userdata('year');
		$this->db->where('yearid',$yearid);
		$year=$this->db->get('sch_school_year')->row()->sch_year;
		$class_id = 1;	
	}
 ?>

<div class='col-xs-13'>
	<div class="wrapper">
		<div class="clearfix" id="main_content_outer">
		    <div id="main_content">
		      <div class="row result_info col-xs-12">
			      	<div class="col-xs-7">
			      		<strong><?php echo $page_header;?></strong>
			      	</div>
			      	<div class="col-xs-5" align="right">
			      	<table>
			      		<tr>
			      			<td>
			      				<a href="#" id="print" title="Print">
					    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
					    		</a>
					    		&nbsp;
			      			</td>
			      			<td style="padding-left:3px;">
			      				<button class="btn btn-primary"  data-toggle="modal" data-target="#myModal" style="height:31px; margin-top:-3px;">
			      					<i class="glyphicon glyphicon-shopping-cart"></i> Name 
			      				</button>
			      				<!-- 
			      					<a class="btn btn-success" href="#" data-toggle="modal" data-target="#myModal">
										<i class="glyphicon glyphicon-shopping-cart"></i> Name
									</a> 
								-->
			      			</td>
			      			<td style="padding-left:2px;">&nbsp;&nbsp;Class :&nbsp;&nbsp;</td>
			      			<td>
			      				<select onchange='previewstudcard(event);' class='form-control input-sm' id='s_classid' name='s_classid'>
									<option value=''>----Select----</option>
									<?php
										foreach ($this->db->get('sch_class')->result() as $c) {?>
											<option value='<?php echo $c->classid ?>' <?php if(isset($class_id)) if($class_id==$c->classid) echo 'Selected' ?>><?php echo $c->class_name ?></option>	
									<?php	}
									 ?>
								</select>
			      			</td>
			      			
			      		</tr>
			      	</table>
			      	</div> 		      
			  </div>
			</div>
		</div>
	</div>
	<div class="wrapper col-sm-12">

	 <div class="row ">
		<div class="col-sm-12">
		    <div class="panel panel-default">
		      	<div class="panel-body">
			         <div class="table-responsive">
			    			<div id="tab_print" style="background-color:#fff;border:0px solid #f00;">
							<style type="text/css">
								.larg_font_class { 
									font-size:22px !important;
									color:#666;
									text-transform: uppercase;
									color:#4BBABC;
								}
								.larg_font_year { 
									font-size:13px !important;
									color:#666;
									text-transform: uppercase;
									color:#4BBABC;
								}
								.larg_font_name { 
									font-size:22px !important;
									color:#666;
									text-transform: uppercase;
									color:#4BBABC;
								}
								

									@media (min-height: 40em) {
									  .modal {
									    position: fixed;
									  }
									  .modal-body {
									    max-height: 35em; /* This should be less than the min-height in the media query. */
									    overflow-y: auto;
									  }
									}
									
							</style>
							<!-- Start Student Badge List Script-->
							<table border="0"  id="tblstd"></table>
							<!-- End Student Badge List Script-->

							<!-- Start Student Badge List PHP-->
							<table border="0"  id="tdatastudcard" class="tblstdhide"></table>
							<!-- End Student Badge List PHP-->
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- <p style="page-break-after:always;"></p> -->
		<?php //} ?>

       <!-- end block dialog -->
       <!-- start new dialog  -->
      
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo $page_header;?></h4>
                    </div>
                    <div class="modal-body ">
                    	<div class="stdscrollbar">
	                        <form method="post" accept-charset="utf-8" class="frmaddrespond" id="frmaddrespond">
					      		<div class="row">          
						       		<div class="col-sm-12">                       	
						              	<div class="panel-body">
						              	<div class="form-group" id="exist" style="display:none">Data is already exist, Please try again...!</div>
											  <div class="form-group">
											    <input type="text" class="form-control searchdatamulti" id="searchdatamulti" name="searchdatamulti"  placeholder="Search here..!">
											 	
											  </div>
							                <table   class="table table-bordered" border="0" width="100%">
							                	<thead>
								                	<tr>
								                		<th>Student ID</th>
								                		<th>Family ID</th>
								                		<th>Nom et Prénom</th>
								                		<th>En Khmer</th>
								                		<th>Class</th>
								                	</tr>
							                	</thead>
							                	<tbody id="tdata"></tbody>
							                </table> 
							            </div>
						            </div>   
						        </div>
							</form>
						</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="btnclose">Close</button>
                        <button type="button" class="btn btn-primary" id="sdstdcard">Add</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end new dialog  -->
	</div>
</div>

	<script type="text/javascript">
		$(function(){
			loadstudcard();
			autostudentcard();
			$("#searchdatamulti").on("change",function(){
				if($(this).val()=="")
		        {
		        	$('#exist').hide();
		        }else{
					autostudentcard();
					$(this).select();
		        }

			});			
			$("#print").on("click",function(){
				gPrint("tab_print");
			});
			$("#sdstdcard").on("click",function(event){ 
				// alert(500);
				var arrstd = Array();
				var yearid=$('#year').val();
				$(".studcardid:checked").each(function(i){
					var stdid = $(this).val();
					var tr = $(this).parent().parent();
					var classid = tr.find("#classid").val();
					arrstd[i]={"stdid":stdid,"yearid":yearid,"classid":classid};
				});
				$.ajax({
						url:"<?php echo site_url('student/studcard/addstudcardtolist')?>",
						type:"POST",
						datatype:"Json",
						async:false,
						data:{
							arrstd:arrstd
						},
						success:function(rstd){
						if(rstd.student!=""){
							$('table.tblstdhide').hide();
							$("#tblstd").html(rstd.student);
							$("#searchdatamulti").val('');
							$("#btnclose").click();
						}		
					
					}
				});
			 });
			$('#myModal').on('click',function(){
				//alert(5);
				$("#searchdatamulti").val('');
				$('#exist').hide();
			});
		});

		function previewstudcard(event){
			var classid=$('#s_classid').val();
			var yearid=$('#year').val();
			var setY = "<?php echo $_GET['y'];?>";
			var setM = "<?php echo $_GET['m'];?>";
			var setP = "<?php echo $_GET['p'];?>";
			location.href="<?PHP echo site_url('student/studcard/');?>?y="+setY+"&m="+setM+"&p="+setP+"&classid="+classid+"&yearid="+yearid;
		}

		$("#year").change(function(){
    		loadstudcard();
		});

		function search(searchdatamulti='',yearid='',classid=''){
			$.ajax({
				url:"<?php echo site_url('student/studcard/getAutoResult')?>",
				type:"POST",
				datatype:"Json",
				async:false,
				data:{
					yearid:yearid,
					classid:classid,
					searchdatamulti:searchdatamulti
				},
				success:function(res){
					if(res.tdata!=""){
						$("#tdata").append(res.tdata);
					}		
					else
					{
						$("#tdata").html("<tr><td colspan='5' align='center'>Data is empty...!</td></tr>");
					}
				}
			})			
		}
		function loadstudcard()
		{
			var yearid=$("#year").val();
			var classid=$("#s_classid").val();
			$.ajax({
				url:"<?php echo site_url('student/studcard/loadstudcard')?>",
				type:"POST",
				datatype:"Json",
				async:false,
				data:{
					yearid:yearid,
					classid:classid
				},
				success:function(restd){
					if(restd.tdatastudcard!=""){
						$("#tdatastudcard").html(restd.tdatastudcard);
					}	
				}
			});
		}

	function autostudentcard(){    
	  var fillclass_url="<?php echo site_url('student/studcard/fillstudentcard')?>";
	    $("#searchdatamulti").autocomplete({
	      source: fillclass_url,
	      minLength:0,
	      select: function(events,ui) { 
	      	var yearid=$("#year").val();
			var classid=$("#s_classid").val();
	        var student_num=ui.item.student_num;
	        var exist=false;
	        $("#searchdatamulti").val(student_num);
	        $('.studcardid:checked').each(function(){
	        	var setID = $(this).val();
	        	if(setID == student_num)
	        	{
	        		exist=true;
	        	}
	        });

	        if(exist==true)
	        {
	        	$('#exist').show();	        	
	        }else
	        {
	        	search(student_num,yearid,classid);
	        }       	
	      }           
	    });
	}
	</script>

