

<div class="wrapper">
    <div class="clearfix" id="main_content_outer">

        <div id="main_content">

            <div class="result_info" style="padding-top: 0px">
                <div class="col-sm-6">
                    <strong>Homework</strong>
                </div>
                <div class="col-sm-6" style="text-align: right">

                </div>
            </div>
            <?php
            $m = '';
            $p = '';
            if (isset($_GET['m'])) {
                $m = $_GET['m'];
            }
            if (isset($_GET['p'])) {
                $p = $_GET['p'];
            }
            ?>
            <?php if($this->session->userdata('match_con_posid')!='stu'){?>
            <form method="post" accept-charset="utf-8" id="fragelev"
                  action="<?php echo site_url('student/c_home_work/reload?m=' . $m . '&p=' . $p . ''); ?>" class="gform">

                <div class="col-sm-3 col-md-3 ">
                    <div class="">
                        <label class="req" for="school">School</label>
                        <select class="form-control" id='school' name='school' min='1' required
                                data-parsley-required-message="Select any school">

                            <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                                <option
                                    value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="school-level">School Level</label>
                        <select class="form-control" id="school-level" name="school-level">
                            <option value=""></option>
                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="academic-year">Academic Year</label>
                        <select class="form-control" id="academic-year" name="academic-year">
                            <option value=""></option>
                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">
                    <div class="">
                        <label class="req" for="grade-level">Grade Level</label>
                        <select class="form-control" id="grade-level" name="grade-level">
                          <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 ">
                    <div class="">
                        <label class="req" for="class">Select Class</label>
                        <select class="form-control" id="class" name="class">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 hiable_sub">
                    <label for="subject">Subject</label>
                    <select id="subject" name="subject" class="form-control">
                        <option value=""></option>

                    </select>
                </div>

                <div class="col-sm-3 col-md-3 ">

                    <label class="req" for="from_date">From Date</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="from_date" name="from_date" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <label class="req" for="to_date">To Date</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="to_date" name="to_date" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>

                </div>
                <div class="col-sm-12">
                    <div class="dv-std" id="dv-std"></div>
                </div>
                <div class="col-sm-12" style="padding-bottom: 5px;padding-top: 5px">
                    <div align="center">
                        <?php if ($this->green->gAction("R")) { ?>
                            <input type="button" name="btn-search" id='btn-search' value="Search" class="btn btn-primary"/>
                        <?php } ?>
                    </div>
                </div>

            </form>
            <?php }?>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="panel panel-default">

        <div class="panel-body table-responsive">
            <table border="0" ? align="center" id='tbl-homework' class="data-table table-striped table-bordered table-hover">
                <thead>
                <th>School</th>
                <th>School Level</th>
                <th>Academic Year</th>
                <th>Grade Level</th>
                <th>Class</th>
                <th>Subject</th>
                <th>Title</th>
                <th>Note</th>
                <th>File</th>
                <th>Date</th>
                <th>Create By</th>
                <th align="center" colspan="2">
                  <span id="span-addnew" data-toggle="modal" data-target="#homework-modal">
                  <?php if ($this->green->gAction("C")) { ?>
                    <img src="<?php echo base_url('assets/images/icons/add.png') ?>"/>
                  <?php } ?>
                  </span>
                </th>
              </thead>

                <tbody>
                  <tr>
                    <td colspan="11">No data found..</td>
                  </tr>
                </tbody>
                <!---- Start pagination ---->
                <tfoot>
                <tr>
                    <td colspan='13' id='pgt'>
                        <ul class='pagination'>

                        </ul>
                    </td>
                </tr>
                </tfoot>
                <!---- End Pagination ---->
            </table>

        </div>
    </div>
</div>

</div>

<!-- Modal -->
<div id="homework-modal" class="modal fade" role="dialog"  tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" style="width:80%">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create Homework</h4>
      </div>
      <div class="modal-body">
        <form method="post" accept-charset="utf-8" id="dlg-frm-homework" target="dlg-if-homework" enctype="multipart/form-data"
              action="<?php echo site_url('student/c_home_work/save?m=' . $m . '&p=' . $p . ''); ?>" class="gform">

            <div class="col-sm-3 col-md-3 ">
                <div class="">
                    <label class="req" for="dlg-school">School</label>
                    <select class="form-control" id='dlg-school' name='school' min='1' required
                            data-parsley-required-message="Select any school">

                        <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                            <option
                                value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-3 col-md-3 ">

                <div class="">
                    <label class="req" for="dlg-school-level">School Level</label>
                    <select class="form-control" id="dlg-school-level" name="school-level">
                        <option value=""></option>
                    </select>
                </div>

            </div>
            <div class="col-sm-3 col-md-3 ">

                <div class="">
                    <label class="req" for="dlg-academic-year">Academic Year</label>
                    <select class="form-control" id="dlg-academic-year" name="academic-year">
                        <option value=""></option>
                    </select>
                </div>

            </div>
            <div class="col-sm-3 col-md-3 ">
                <div class="">
                    <label class="req" for="dlg-grade-level">Grade Level</label>
                    <select class="form-control" id="dlg-grade-level" name="grade-level">
                      <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 ">
                <div class="">
                    <label class="req" for="dlg-class">Select Class</label>
                    <select class="form-control" id="dlg-class" name="class">
                        <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3 col-md-3">
                <label for="dlg-subject">Subject</label>
                <select id="dlg-subject" name="subject" class="form-control">
                    <option value=""></option>

                </select>
            </div>
            <div class="col-sm-3 col-md-3">
                <label for="dlg-title">Title</label>
                <input type="text" id="dlg-title" class="form-control" name="title" value="" />
            </div>
           
             <div class="col-sm-3 col-md-3">

                    <label class="req" for="deadline">Deadline</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="deadline" name="deadline" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>
            </div>
            <div class="col-sm-3 col-md-3 hiable_sub">
                <label for="dlg-note">Note</label>
                <textarea id="dlg-note"  class="form-control" name="note"></textarea>
            </div>
            <div class="col-sm-3 col-md-3">
                <label for="dlg-file">Attach File</label>
                <input type="file" id="dlg-file" name="file" value="" />
            </div>
 			
			 

            <div class="col-sm-12">
                <div class="dv-std" id="dv-std"></div>
            </div>

        </form>
        <iframe id="dlg-if-homework" name="dlg-if-homework" style="border: 0 none; width: 100px; height: 100px;"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" id="dlg-btn-save" class="btn btn-default" data-dismiss="modal">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="edit-homework-modal" class="modal fade" role="dialog"  tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" style="width:80%">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Homework</h4>
      </div>
      <div class="modal-body">
        <form style="display: hidden;" method="post" accept-charset="utf-8" id="dlg-edit-frm-homework" target="dlg-edit-if-homework" enctype="multipart/form-data"
              action="<?php echo site_url('student/c_home_work/update?m=' . $m . '&p=' . $p . ''); ?>" class="gform">

            <div class="col-sm-3 col-md-3 ">
                <div class="">
                    <label class="req" for="dlg-edit-school">School</label>
                    <select class="form-control" id='dlg-edit-school' name='school_edit' min='1' required
                            data-parsley-required-message="Select any school">

                        <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                            <option
                                value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" id="id_edit" name="id_edit"/>
                </div>
            </div>

            <div class="col-sm-3 col-md-3 ">

                <div class="">
                    <label class="req" for="dlg-edit-school-level">School Level</label>
                    <select class="form-control" id="dlg-edit-school-level" name="school-level_edit">
                        <option value=""></option>
                    </select>
                </div>

            </div>
            <div class="col-sm-3 col-md-3 ">

                <div class="">
                    <label class="req" for="dlg-edit-academic-year">Academic Year</label>
                    <select class="form-control" id="dlg-edit-academic-year" name="academic-year_edit">
                        <option value=""></option>
                    </select>
                </div>

            </div>
            <div class="col-sm-3 col-md-3 ">
                <div class="">
                    <label class="req" for="dlg-edit-grade-level">Grade Level</label>
                    <select class="form-control" id="dlg-edit-grade-level" name="grade-level_edit">
                      <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 ">
                <div class="">
                    <label class="req" for="dlg-edit-class">Select Class</label>
                    <select class="form-control" id="dlg-edit-class" name="class_edit">
                        <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3 col-md-3">
                <label for="dlg-edit-subject">Subject</label>
                <select id="dlg-edit-subject" name="subject_edit" class="form-control">
                    <option value=""></option>

                </select>
            </div>
            <div class="col-sm-3 col-md-3">
                <label for="dlg-edit-title">Title</label>
                <input type="text" id="dlg-edit-title" class="form-control" name="title_edit" value="" />
            </div>
            
            
            <div class="col-sm-3 col-md-3">

                    <label class="req" for="deadlineedit">Deadline</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="deadlineedit" name="deadline_edit" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>
            </div>
            
            <div class="col-sm-3 col-md-3 hiable_sub">
                <label for="dlg-edit-note">Note</label>
                <textarea id="dlg-edit-note"  class="form-control" name="note_edit"></textarea>
            </div>
            <div class="col-sm-3 col-md-3">
                <label for="dlg-edit-file">Attach File</label>
                <input type="file" id="dlg-edit-file" name="file_edit" value="" />
            </div>

            <div class="col-sm-12">
                <div class="dv-std" id="dv-std"></div>
            </div>

        </form>
        <iframe id="dlg-edit-if-homework" name="dlg-edit-if-homework" style=";border: 0 none; width: 100px; height: 1px;"></iframe>
        <iframe style="border: 0 none; width: 100px; height: 100px;"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" id="dlg-btn-edit" class="btn btn-default" data-dismiss="modal">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">

  function error() {
    alert("save error!!!");
  }

  function reload(isFirstLoad) {
    //var parameters = isFirstLoad || isFirstLoad == undifined ? 0 : 1;
    var parameters = isFirstLoad === undefined || isFirstLoad === null ? 1 : 0;

      $.ajax({
          url: "<?php echo site_url('student/c_home_work/reload'); ?>",
          type: "POST",
          dataType: 'json',
          async: false,
          data: {
              school: $("#school").val(),
              school_level: $("#school-level").val(),
              academic_year: $("#academic-year").val(),
              grade_level: $("#grade-level").val(),
              school_class: $("#class").val(),
              subject: $("#subject").val(),
              from_date: $("#from-date").val(),
              to_date: $("#to-date").val(),
              parameter: parameters
          },
          success: function (homeworks) {
            var tbody = "";

            if (homeworks.length > 0) {

              for (var i = 0; i < homeworks.length; i++) {
                  var publish_date="";
            	  try {
            		  var date_string=homeworks[i].transaction_date;
                	  var publishdates = date_string.split(' ');
                	  publish_date= publishdates[0];
            		}
            		catch(err) {
            		   
            		}
            	 
                  
                tbody += '<tr>' +
                            '<td>' + homeworks[i].school + '</td>' +
                            '<td>' + homeworks[i].school_level + '</td>' +
                            '<td>' + homeworks[i].academic_year + '</td>' +
                            '<td>' + homeworks[i].grade_level + '</td>' +
                            '<td>' + homeworks[i].class + '</td>' +
                            '<td>' + homeworks[i].subject + '</td>' +
                            '<td><div class="truncate">' + homeworks[i].title + '</div></td>' +
                            '<td><div class="truncate">' + homeworks[i].description + '</div></td>' +
                            '<td>' +
                              '<a download class="a-homework-file" href="<?php echo base_url('assets/upload/homeworks') ?>/' + homeworks[i].attach_file + '">' +
                                '<img title="' + homeworks[i].attach_file +'" src="' + callAttachedFileIcon(homeworks[i].file_type) + '" />' +
                              '</a>' +
                            '</td>' +
                            '<td align="center">' +  publish_date + '</td>' +
                            '<td>' + homeworks[i].created_by + '</td>' +
                            '<td align="center">' +
                              '<span id="' + homeworks[i].id + '" class="span-delete">' +
                              <?php if ($this->green->gAction("C")) { ?>
                                '<img src="<?php echo base_url('assets/images/icons/delete.png') ?>"/>' +
                              <?php } ?>
                              '</span>' +
                            '</td>' +
                            '<td align="center">' +
                              '<span id="' + homeworks[i].id + '" class="span-edit"  data-toggle="modal">' +
                              <?php if ($this->green->gAction("U")) { ?>
                                '<img src="<?php echo base_url('assets/images/icons/edit.png') ?>"/>' +
                              <?php } ?>
                              '</span>' +
                            '</td>' +
                         '</tr>';
              }
            }
            else {
              tbody += '<tr><td colspan="11" align="center">No data found..</td></tr>';
            }

            $("#tbl-homework tbody").html(tbody);
          }
      });
  }
  function filter_search() {
	  
	    var search_params = {};
	    
		if($("#school").val()!=null && $("#school").val()!='undefined' && $("#school").val()!=""){
			search_params["school_id"]=$("#school").val();
		}
		
		if($("#school-level").val()!=null && $("#school-level").val()!='undefined' && $("#school-level").val()!=''){
			search_params["school_level_id"]=$("#school-level").val();
		}

		if($("#academic-year").val()!=null && $("#academic-year").val()!='undefined' && $("#academic-year").val()!=''){
			search_params["academic_year_id"]=$("#academic-year").val();
		}

		if($("#grade-level").val()!=null && $("#grade-level").val()!='undefined' && $("#grade-level").val()!=''){
			search_params["grade_level_id"]=$("#grade-level").find("option:selected").val();
		}

		if($("#class").val()!=null && $("#class").val()!='undefined' && $("#class").val()!=''){
			search_params["class_id"]=$("#class").find("option:selected").val();
		}

		if($("#subject").val()!=null && $("#subject").val()!='undefined' && $("#subject").val()!=''){
			search_params["subject_id"]=$("#subject").val();
		} 
		 if($("#from_date").val()!=null 
				&& $("#from_date").val()!='undefined' 
    			&& $("#to_date").val()!=null 
    			&& $("#to_date").val()!='undefined' 
    			&& $("#from_date").val()!="" 
				&& $("#to_date").val()!=""){
			
			search_params["from_date"]=$("#from_date").val();
			search_params["to_date"]=$("#to_date").val();
		}

    	 if(($("#from_date").val()=="" && $("#to_date").val()!="")){
    			
    		 alert("Please select From Date");
    		 return;
    	 }else if($("#to_date").val()=="" && $("#from_date").val()!=""){
        	 
    		 alert("Please select To Date");
    		 return;
         }



		

		      $.ajax({
		          url: "<?php echo site_url('student/c_home_work/filter'); ?>",
		          type: "POST",
		          dataType: 'json',
		          async: false,
		          data: 'data='+JSON.stringify(search_params),
		          success: function (homeworks) {

						
		            var tbody = "";

		            if (homeworks.length > 0) {
			            
	            	  
		        	  
		              for (var i = 0; i < homeworks.length; i++) {

		            	  var publish_date="";
			        	  try {
				        	  
			        		    var date_string=homeworks[i].transaction_date;
								var date_strings=date_string.split(' ');
								publish_date=date_strings[0];
			        	  }catch(err) {
			        		  
			        		publish_date=homeworks[i].transaction_date;
				        
			        	  }
			              
		                tbody += '<tr>' +
		                            '<td>' + homeworks[i].school + '</td>' +
		                            '<td>' + homeworks[i].school_level + '</td>' +
		                            '<td>' + homeworks[i].academic_year + '</td>' +
		                            '<td>' + homeworks[i].grade_level + '</td>' +
		                            '<td>' + homeworks[i].class + '</td>' +
		                            '<td>' + homeworks[i].subject + '</td>' +
		                            '<td><div class="truncate">' + homeworks[i].title + '</div></td>' +
		                            '<td><div class="truncate">' + homeworks[i].description + '</div></td>' +
		                            '<td>' +
		                              '<a download class="a-homework-file" href="<?php echo base_url('assets/upload/homeworks') ?>/' + homeworks[i].attach_file + '">' +
		                                '<img title="' + homeworks[i].attach_file +'" src="' + callAttachedFileIcon(homeworks[i].file_type) + '" />' +
		                              '</a>' +
		                            '</td>' +
		                            '<td align="center">' + publish_date + '</td>' +
		                            '<td>' + homeworks[i].created_by + '</td>' +
		                            '<td align="center">' +
		                              '<span id="' + homeworks[i].id + '" class="span-delete">' +
		                              <?php if ($this->green->gAction("C")) { ?>
		                                '<img src="<?php echo base_url('assets/images/icons/delete.png') ?>"/>' +
		                              <?php } ?>
		                              '</span>' +
		                            '</td>' +
		                            '<td align="center">' +
		                              '<span id="' + homeworks[i].id + '" class="span-edit"  data-toggle="modal">' +
		                              <?php if ($this->green->gAction("U")) { ?>
		                                '<img src="<?php echo base_url('assets/images/icons/edit.png') ?>"/>' +
		                              <?php } ?>
		                              '</span>' +
		                            '</td>' +
		                         '</tr>';
		              }
		            }
		            else {
		              tbody += '<tr><td colspan="11" align="center">No data found..</td></tr>';
		            }

		            $("#tbl-homework tbody").html(tbody);
		          },
                    beforeSend: function() { $('.proccess_loading').show(); },
                    complete: function() { $('.proccess_loading').hide();}
		      });
		  

	  }
  function callSchoolLevel(school, schoolLevelContainer, selectedId) {
    selectedId = (typeof selectedId == 'undefined' ? 0 : selectedId);

    $.ajax({
        url: "<?php echo site_url('student/c_home_work/getSchoolLevel'); ?>",
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
            school: school
        },
        success: function (schoolLevel) {
            var options = "<option value=''></option>";
            if (schoolLevel.length > 0) {
                for (var i = 0; i < schoolLevel.length; i++) {
                  var selected = schoolLevel[i].schlevelid == selectedId ? "selected" : "";
                  options += '<option value="' + schoolLevel[i].schlevelid + '" data-program="' + schoolLevel[i].programid + '" ' + selected + '>' + schoolLevel[i].sch_level + '</option>';
                }
            }

            $(schoolLevelContainer).html(options);
        }
    });
  }

  function callAcademicYear(schoolLevel, academicYearContainer, selectedId) {
    selectedId = (typeof selectedId == 'undefined' ? 0 : selectedId);

    $.ajax({
        url: "<?php echo site_url('school/academic_year/get_year'); ?>",
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
            schlevelid: schoolLevel
        },
        success: function (academic) {
            
            var options = "<option value=''></option>";
            if (academic.year.length > 0) {
                for (var i = 0; i < academic.year.length; i++) {
                  var selected = academic.year[i].yearid == selectedId ? "selected" : "";
               
                  options += '<option value="' + academic.year[i].yearid + '" ' + selected + '>' + academic.year[i].sch_year + '</option>';
                 
                }
            }

            $(academicYearContainer).html(options);
        }
    });
  }

  function callGradeLevel(schoolLevel, gradeLevelContainer, selectedId) {
	  selectedId = (typeof selectedId == 'undefined' ? 0 : selectedId);
	  try {
		  selectedId= parseInt(selectedId);
		}
		catch(err) {
			selectedId="";
		}
    $.ajax({
        url: "<?php echo site_url('school/gradelevel/getGradeLevel'); ?>",
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
            school_level: schoolLevel
        },
        success: function (gradeLevel) {
            var options = "<option value=''></option>";

            if (gradeLevel.length > 0) {
                for (var i = 0; i < gradeLevel.length; i++) {
                  var selected = gradeLevel[i].grade_levelid == selectedId ? "selected" : "";
                 
			
                  
                  options += '<option value="' + gradeLevel[i].grade_levelid + '" data-school-level="' + schoolLevel + '" ' + selected + '>' + gradeLevel[i].grade_level + '</option>';
                }
            }

            $(gradeLevelContainer).html(options);
        }
    });
  }

  function callClass(schoolLevel, gradeLevel, classContainer, selectedId) {
    selectedId = (typeof selectedId == 'undefined' ? 0 : selectedId);

    $.ajax({
        url: "<?php echo site_url('student/c_home_work/get_class'); ?>",
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
            school_level: schoolLevel,
            grade_level: gradeLevel
        },
        success: function (schoolClass) {
            var options = "<option value=''></option>";

            if (schoolClass.length > 0) {
                for (var i = 0; i < schoolClass.length; i++) {
                  var selected = schoolClass[i].classid == selectedId ? "selected" : "";
                  options += '<option value="' + schoolClass[i].classid + '" ' + selected + '>' + schoolClass[i].class_name + '</option>';
                }
            }

            $(classContainer).html(options);
        }
    });
  }

  function callSubject(schoolLevel, programId, subjectContainer, selectedId) {
    selectedId = (typeof selectedId == 'undefined' ? 0 : selectedId);
    $.ajax({
        url: "<?php echo site_url('student/c_home_work/getSubject'); ?>",
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
            school_level: schoolLevel,
            program_id: programId
        },
        success: function (subject) {

	
            
            var options = "<option value=''></option>";

            if (subject.length > 0) {
                for (var i = 0; i < subject.length; i++) {
                  var selected = subject[i].subjectid == selectedId ? "selected" : "";
                  options += '<option value="' + subject[i].subjectid + '" data-school-level="' + schoolLevel + '" ' + selected + '>' + (programId==1?subject[i].subject_kh:subject[i].subject) + '</option>';
                }
            }

            $(subjectContainer).html(options);
        }
    });
  }

  function callAttachedFileIcon(fileType) {
    var icon = fileType == 'jpeg' ? 'jpg' : fileType.toString().toLowerCase();
    return "<?php echo base_url('assets/images/icons/'); ?>/" + icon + ".ico";
  }

  function clear(){
	  var options = "<option value=''></option>";
	  $("#dlg-school-level").html(options);
 	  $("#dlg-academic-year").html(options);
 	  $("#dlg-grade-level").html(options);
 	  $("#dlg-class").html(options);
 	  $("#dlg-subject").html(options);
 	  $("#dlg-title").val("");
 	  $("#dlg-note").val("")
 	  $('#dlg-file').val("");
  }

  $(function () {

    reload(1);

    callSchoolLevel($("#school").val(), $("#school-level"));

    $("body").delegate("#school-level", "change", function() {
        
      callAcademicYear($(this).val(), $("#academic-year"));
      callGradeLevel($(this).val(), $("#grade-level"));
      callSubject($(this).val(), $(this).find("option:selected").data("program"), $("#subject"))
    });

    $("body").delegate("#grade-level", "change", function() {
      callClass($(this).find("option:selected").data("school-level"), $(this).val(), $("#class"));
    });

    $("#btn-search").click(function() {

    	filter_search();
    });

    $("body").delegate("#dlg-school-level", "change", function() {
      callAcademicYear($(this).val(), $("#dlg-academic-year"));
      callGradeLevel($(this).val(), $("#dlg-grade-level"));
      callSubject($(this).val(), $(this).find("option:selected").data("program"), $("#dlg-subject"))
    });

    $("body").delegate("#dlg-edit-school-level", "change", function() {
      callAcademicYear($(this).val(), $("#dlg-academic-year"));
      callGradeLevel($(this).val(), $("#dlg-grade-level"));
      callSubject($(this).val(), $(this).find("option:selected").data("program"), $("#dlg-edit-subject"))
    });

    $("body").delegate("#span-addnew", "click", function() {
      clear();
      callSchoolLevel($("#school").val(), $("#dlg-school-level"));
    });

    $("body").delegate("#dlg-grade-level", "change", function() {
      callClass($(this).find("option:selected").data("school-level"), $(this).val(), $("#dlg-class"));
    });

    $("body").delegate("#dlg-edit-grade-level", "change", function() {
    
      callClass($(this).find("option:selected").data("school-level"), $(this).val(), $("#dlg-class"));
    });

    $("#from_date,#to_date,#deadline,#deadlineedit").datepicker({
        language: 'en',
        pick12HourFormat: true,
        format: 'dd-mm-yyyy'
    });

   
    
    $("body").delegate("#dlg-btn-save", "click", function() {
      $("#dlg-frm-homework").submit();
      reload(1);
    });

    $("body").delegate("#dlg-btn-edit", "click", function() {
 
        $("#dlg-edit-frm-homework").submit();
        reload(1);
      });

    $("body").delegate(".span-delete", "click", function() {

      $.ajax({
          url: "<?php echo site_url('student/c_home_work/delete'); ?>",
          type: "POST",
          dataType: 'json',
          async: false,
          data: {
            id: $(this).attr("id")
          },
          success: function (result) {
            if (result.delete == "Success") {
              reload(1);
            }
            else {
              alert(result.delete);
            }
          }
      });
    });

  

    $("body").delegate(".span-edit","click", function() {
      //$('#edit-homework-modal').modal('show');
     $("#dlg-edit-if-homework").html("");
      var cl=0;
      var id = $(this).attr('id');
      $('#edit-homework-modal').modal('show').on('shown.bs.modal', function () {
          if(cl==0){

        	  $('#id_edit').val(id);
              $.ajax({
                  url: "<?php echo site_url('student/c_home_work/getEditData'); ?>",
                  type: "POST",
                  dataType: 'json',
                  async: false,
                  data: {
                    id: id
                  },
                  success: function (homework) {
                    
                  	var str=homework.deadline;
                      var deadlines=str.split(" ");
                      var str=deadlines[0];
                      var strs=str.split('-');
             
                      $('#deadlineedit').val(strs[2]+"-"+strs[1]+"-"+strs[0]);
                
                                             	
                      callSchoolLevel(homework.school_id, $("#dlg-edit-school-level"),homework.school_level_id);
                      callAcademicYear(homework.school_level_id, $("#dlg-edit-academic-year"),homework.academic_year_id);
                      callGradeLevel(homework.school_level_id, $("#dlg-edit-grade-level"),homework.grade_level_id);
                      callClass(homework.school_level_id, homework.grade_level_id, $("#dlg-edit-class"),homework.class_id);
                      callSubject(homework.school_level_id, $("#dlg-edit-school-level").find("option:selected").data("program"), $("#dlg-edit-subject"),homework.subject_id)
                      $("#dlg-edit-title").val(homework.title);
                      $("#dlg-edit-note").val(homework.description);
                      $("#dlg-edit-file").val("");

                      
                  }
              });

              $("#dlg-edit-if-homework").css("height","50px");
          }
    	  cl=cl+1;
      });
     $("#dlg-edit-if-homework").css("height","50px");
    });

  });
  function isEmpty(str) {
	    return (!str || 0 === str.length);
	}
</script>
