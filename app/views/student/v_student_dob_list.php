<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }
    
    // schoolinfor ---------------------------------------
    if(isset($schooinfor) && count($schooinfor)>0){
      $infor="";
      $infor="<option attr_price='0' value=''>All</option>";
      foreach($schooinfor as $for){
         $infor.="<option value='".$for->schoolid."'>".$for->name."</option>";
      }
    }
    // Program --------------------------------------------
    if(isset($getprogram) && count($getprogram)>0){
        $opprogram="";
        $opprogram="<option attr_price='0' value=''>All</option>";
        foreach($getprogram as $rowpro){
            $opprogram.="<option value='".$rowpro->programid."'>".$rowpro->program."</option>";
        }
        
    }

 ?>
 <style type="text/css">
    table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }

    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }
    
    #se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	.to {color:#cc66ff; font-weight:bold;}
	.from {color:#FF0000; font-weight:bold;}
</style>

<div class="wrapper" style="border:0px solid #f00;overflow:auto">
   <div class="col-sm-12">     
	   <div class="row">
	      	<div class="col-xs-12">
		         <div class="result_info">
		            <div class="col-xs-6">
		                <strong>Student Date Of Birth List</strong>  
		            </div>
		            <div class="col-xs-6" style="text-align: right">
		               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
		                  <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
		               </a>
		               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
		                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
		               </a>
		               <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
		                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
		               </a>           
		               
		               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
		                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
		               </a>
		            </div>         
		         </div>
	      	</div>
	   	</div>

		<div class="collapse" id="collapseExample">             
		  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
		     <input type="hidden" name="termid" id="termid">
		     
		     <div class="col-sm-4" style=""> 

            <div class="form-group">
               <label for="student_num">Student ID<span style="color:red"></span></label>
               <input type="text" name="student_num" id="student_num" class="form-control" placeholder="Student ID">
            </div>            

			<div class="form-group">
               <label for="english_name">Student Name English <span style="color:red"></span></label>
               <input type="text" name="english_name" id="english_name" class="form-control" placeholder="English">
            </div>
            <div class="form-group">
               <label for="khmer_name">Student Name Khmer <span style="color:red"></span></label>
               <input type="text" name="khmer_name" id="khmer_name" class="form-control" placeholder="Khmer">
            </div>

            <div class="form-group">
               <label for="gender">Gender<span style="color:red"></span></label>
               <select name="gender" id="gender" class="form-control">
               	<option value="">All</option>
               	<option value="Female">Female</option>
               	<option value="Male">Male</option>
               </select>
            </div>

         </div><!-- 1 -->

         <div class="col-sm-4">
            <div class="form-group">
               <label for="schoolid">School Info<span style="color:red"></span></label>
               <select name="schoolid" id="schoolid" class="form-control">
                    <?php echo $infor;?>            
               </select>
            </div>     

            <div class="form-group">
               <label for="programid">Program<span style="color:red"></span></label>
               <select name="programid" id="programid" class="form-control">
                  <?php echo $opprogram;?> 
               </select>
            </div>

            <div class="form-group">
               <label for="schlevelids">School Level<span style="color:red"></span></label>
               <select name="schlevelids" id="schlevelids" class="form-control">
                  
               </select>
            </div>

            <div class="form-group">
               <label for="yearid">Year<span style="color:red"></span></label>
               <select name="yearid" id="yearid" class="form-control">

               </select>
            </div>
         </div><!-- 2 -->

         <div class="col-sm-4">
            <div class="form-group">
               <label for="rangelevelid">Rangelevel Name<span style="color:red"></span></label>
               <select name="rangelevelid" id="rangelevelid" class="form-control">

               </select>
            </div>

            <div class="form-group" style="">
               <label for="classid">Class<span style="color:red"></span></label>
               <select name="classid" id="classid" class="form-control">
                  
               </select>
            </div>

            <div class="form-group"​ style="">
               <label for="from_dob">From Date Of Birth<span style="color:red"></span></label>
               <div class="input-group">
                 <input type="text" name="from_dob" id="from_dob" class="form-control" placeholder="dd/mm/yyyy">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
               </div>
            </div>

            <div class="form-group" style="">
               <label for="to_dob">To Date Of Birth<span style="color:red"></span></label>
                 <div class="input-group">
                 <input type="text" name="to_dob" id="to_dob" class="form-control" placeholder="dd/mm/yyyy">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
               </div>
            </div>
         </div><!-- 3 -->       

		     <div class="col-sm-7 col-sm-offset-5">
		        <div class="form-group">
		           <button type="button" class="btn btn-success btn-sm btn_search" name="btn_search" id="btn_search" data-save='1'>Search</button>&nbsp; 
		           <button type="button" class="btn btn-warning btn-sm btn_clear" name="btn_clear" id="btn_clear" data-save='1' style="display: none;">Clear</button>
		           
		        </div>            
		     </div>
		     <div class="row">
		        <div class="col-sm-12">
		           <div class="form-group" style="border-bottom: 1px solid #CCC;">&nbsp;</div>           
		        </div>
		     </div>
		  </form>
		</div>   
   
   <!-- show header -->
        <div class="col-sm-12">
            <div class="panel panel-default">
               <div class="table-responsive" id="div_export_print">
                    <div id="tab_print">
                         <table width="100%" border="0" class="table table-hover" id="setBorderTbl">
                             <thead>
	                             <tr>
		                              <?php
		                                 foreach ($thead as $th => $val) {
		                                    if ($th == 'No'){
		                                      echo "<th class='$val'>".$th."</th>";
		                  					        }
                                        else if ($th == 'Photo'){
                                          echo "<th class='$val'>".$th."</th>";
                                        }else{
		                                    	echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
											                  }
		                                 }
		                              ?>
	                           </tr>
                             </thead>
                             <tbody id='listrespon'>
                             </tbody>
                         </table>
                     </div>
                 </div>
            </div>
            <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
                Display : <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                                <?php
                                    $num=10;
                                    for($i=0;$i<10;$i++){?>
                                        <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                        <?php $num+=20;
                                    }
                                ?>
                          </select>
            </div>
            <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
                <div class="pagination" id="pagination" style="text-align:center"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(function(){
      $("#year,#schlevelid").hide();
      Getdatastudent(1,$('#sort_num').val());
    // sort_num --------------------------------------------
      $('#sort_num').change(function(){
          var  total_display= $(this).val();
          Getdatastudent(1,total_display);
      });
      // pagenav --------------------------------------------
      $("body").delegate(".pagenav","click",function(){
            var page = $(this).attr("id");
            var total_display = $('#sort_num').val();
            Getdatastudent(page,total_display);
      });
      // formatedate -----------------------------------------
      $("#from_dob,#to_dob").datepicker({
          language: 'en',
          pick12HourFormat: true,
          format:'dd-mm-yyyy'
      });
      // program -------------------------------------------
      $("#programid").on('change',function(){
          get_schlevel($(this).val());
      });
      $("#schlevelids").on('change',function(){
          get_schyera($(this).val());
          get_schclass($(this).val());
      });
       $("#yearid").on('change',function(){
         get_schranglavel($(this).val());
      });
       //addnew serch --------------------------------------
      $('body').delegate('#a_addnew', 'click', function(){
          $('#collapseExample').collapse('toggle')
              clear();
      });
      // refresh --------------------------------------------
      $('body').delegate('#refresh', 'click', function(){
        $("#year").hide();
        $('#schlevelid').hide();
        location.reload();
      });
      // btn_search -----------------------------------------
      $('body').delegate('#btn_search', 'click', function(){
          Getdatastudent();
      });
      // tbn printer ------------------------------------------
      $("#a_print").on("click", function () {
          var htmlToPrint = '' +
              '<style type="text/css">' +
              'table th, table td {' +
              'border:1px solid #000 !important;' +
              'padding;0.5em;' +
              '}' +
              '</style>';
          var title = "Student Date Of Birth List";
          var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "");
          var export_data = $("<center>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
          export_data += htmlToPrint;
          gsPrint(title, export_data);
      });
      // tbn exsport ---------------------------------------------
      $("#a_export").on("click", function (e) {
          var title = "Student Date Of Birth List";
          var data = $('.table').attr('border', 1);
          data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
          var export_data = $("<center><h3 align='center'>" + title + "</h3>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
          window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
          e.preventDefault();
          $('.table').attr('border', 0);
      });
  });//------------------------------end funtiont-------------------------------
      function gsPrint(emp_title, data) {
          var element = "<div>" + data + "</div>";
          $("<center><p style='padding-top:15px;text-align:center;'><b>" + emp_title + "</b></p><hr>" + element + "</center>").printArea({
              mode: "popup",  //printable window is either iframe or browser popup
              popHt: 600,  // popup window height
              popWd: 500,  // popup window width
              popX: 0,  // popup window screen X position
              popY: 0, //popup window screen Y position
              popTitle: "test", // popup window title element
              popClose: false,  // popup window close after printing
              strict: false
          });
      }
    // get_schlevel ---------------------------------------------
    function get_schlevel(programid){
           $.ajax({        
              url: "<?= site_url('student/c_student_dob_list/get_schlevel') ?>",    
              data: {         
                'programid':programid
              },
              type: "post",
              dataType: "json",
              success: function(data){ 

                  var opt = '';
                      opt="<option  value=''>All</option>";
                  if(data.schlevel.length > 0){
                      $.each(data.schlevel, function(i, row){
                          opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                      });
                  }else{
                      opt += '';
                  }

                  $('#schlevelids').html(opt);
                  get_schyera($('#programid').val(), $('#schlevelids').val());
                  get_schclass($('#programid').val(), $('#classname').val());
             }
          });
      }
    //get yare---------------------------------------------------------
      function get_schyera(schlevelids){
        $.ajax({        
            url: "<?= site_url('student/c_student_dob_list/get_schyera') ?>",    
            data: {         
              'schlevelids':schlevelids
            },
            type: "post",
            dataType: "json",
            success: function(data){ 
                var ya = '';
                    ya="<option  value=''>All</option>";
                if(data.schyear.length > 0){
                    $.each(data.schyear, function(i, row){
                        ya += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                    });
                }else{
                    ya += '';
                }

                $('#yearid').html(ya);
                get_schranglavel($('#yearid').val(), $('#ranglavel').val());
            }
        });
      }
      // from ranglavel--------------------------------------------------
    function get_schranglavel(yearid){
        $.ajax({        
            url: "<?= site_url('student/c_student_dob_list/get_schranglavel') ?>",    
            data: {         
              'yearid':yearid
            },
            type: "post",
            dataType: "json",
            success: function(data){ 
                var rang = '';
                    rang="<option  value=''>All</option>";
                if(data.schrang.length > 0){
                    $.each(data.schrang, function(i, row){
                        rang += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
                    });
                }else{
                    rang += '';
                }

                $('#rangelevelid').html(rang);

            }
        });
    }
  // from class------------------------------------------------------
    function get_schclass(schlevelids){
      $.ajax({        
          url: "<?= site_url('student/c_student_dob_list/get_schclass') ?>",    
          data: {         
            'schlevelids':schlevelids
          },
          type: "post",
          dataType: "json",
          success: function(data){ 
              var cla = '';
                  cla="<option  value=''>All</option>";
              if(data.schclass.length > 0){
                  $.each(data.schclass, function(i, row){
                      cla += '<option value="'+ row.classid +'">'+ row.class_name +'</option>';
                  });
              }else{
                  cla += '';
              }

              $('#classid').html(cla);
          }
      });
    }
    // Getdataother -----------------------------------------------------------
    function Getdatastudent(page,total_display,sortby,sorttype){
            var roleid = <?php echo $this->session->userdata('roleid');?>;
            var per = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
            var sort_num = $('#sort_num').val();
            var student_num = $('#student_num').val();
            var english_name = $('#english_name').val();
            var khmer_name = $('#khmer_name').val();
            var gender = $('#gender').val();
            var schoolid = $('#schoolid').val();
            var programid = $('#programid').val();
            var schlevelids = $('#schlevelids').val();
            var yearid = $('#yearid').val();
            var rangelevelid = $('#rangelevelid').val();
            var classid = $('#classid').val();
            var from_dob = $('#from_dob').val();
            var to_dob = $('#to_dob').val();
            $.ajax({
                url:"<?php echo base_url();?>index.php/student/c_student_dob_list/Getdatastudent",
                type: "POST",
                datatype: "Json",
                async: false,
                data:{
                        'page'  : page,
                        'p_page': per,
                        'total_display': total_display,
                        'sort_num':sort_num,
                        'sortby':sortby,
                        'sorttype':sorttype,
                        'student_num':student_num,
                        'english_name':english_name,
                        'khmer_name':khmer_name,
                        'gender':gender,
                        'schoolid':schoolid,
                        'programid':programid,
                        'schlevelids':schlevelids,
                        'yearid':yearid,
                        'rangelevelid':rangelevelid,
                        'classid':classid,
                        'from_dob':from_dob,
                        'to_dob':to_dob
                },
                success: function (data) {
                    $("#listrespon").html(data.data);
                    $('.pagination').html(data['pagination']['pagination']);
                }
            });
        }
        // sort -------------------------------------------------------------
        function sort(event){
           /*$(event.target)=$(this)*/ 
            this.sortby = $(event.target).attr("rel");
            if ($(event.target).attr("sorttype") == "ASC") {
                $('.sort').removeClass('cur_sort_up');
                $(event.target).addClass(' cur_sort_down');
                $(event.target).attr("sorttype", "DESC");
                this.sorttype = "ASC";
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_up');
            } else if ($(event.target).attr("sorttype") == "DESC") {
                $(event.target).attr("sorttype", "ASC");
                this.sorttype = "DESC";
                $('.sort').removeClass('cur_sort_up');
                $('.sort').removeClass('cur_sort_down');
                $(event.target).addClass(' cur_sort_down');    
            }        
            Getdatastudent(1,$('#sort_num').val(),this.sortby,this.sorttype);
        } 
        function clear(){
            $('#student_num').val('');
            $('#english_name').val('');
            $('#khmer_name').val('');
            $('#gender').val('');
            $('#schoolid').val('');
            $('#programid').val('');
            $('#schlevelids').val('');
            $('#yearid').val('');
            $('#rangelevelid').val('');
            $('#classid').val('');
            $('#from_dob').val('');
            $('#to_dob').val('');
        }  
</script>