<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }
 ?>
 <style type="text/css">
    table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }

    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }
    
    #se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	.to {color:#cc66ff; font-weight:bold;}
	.from {color:#FF0000; font-weight:bold;}
	
</style>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
    <div class="col-sm-12">     
	    <div class="row">
	      	<div class="col-xs-12">
		         <div class="result_info">
		            <div class="col-xs-6">
		                <strong>Evaluate khmer General Programming</strong>  
		            </div>
		            <div class="col-xs-6" style="text-align: right">
		            	<a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
		                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>" width="22px">
		               	</a>
		               	<a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add">
		                  <img src="<?= base_url('assets/images/icons/add.png') ?>" width="22px">
		               	</a>
		            </div>         
		         </div>
	      	</div>
	   	</div>
		<div class="collapse" id="collapseExample">             
		    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
		        <input type="hidden" name="evaluate_id" id="evaluate_id">
		     	<div class="col-sm-4">
			        <div class="form-group">
                       <label for="evaluate_kh">Evaluate Name Khmer<span style="color:red"></span></label>
                            <input type="text" name="evaluate_kh" id="evaluate_kh" class="form-control" placeholder="Evaluate Name Khme">
                    </div>
		     	</div><!-- 1 -->
		     	<div class="col-sm-4">
                    <div class="form-group">
                       <label for="evaluate_eng">Evaluate Name English<span style="color:red"></span></label>
                            <input type="text" name="evaluate_eng" id="evaluate_eng" class="form-control required" placeholder="Evaluate Name English">
                    </div>
		     	</div><!-- 2 -->
			    <div class="col-sm-4">
                    <div class="form-group">
                        <label for="status">Status<span style="color:red"></span></label>
                        <select name="status" id="status" class="form-control">
                            <option value="1">Active</option>
                            <option value="0">In_Active</option>
                       </select>
                    </div>
			    	<!-- <div class="form-group" style="padding-top:0px;">
		                <label class="control-label"><span>Is_Active</span></label>
		                	<input  type="checkbox" id="checkbox_isfillout" class="radio CKcheckbox" value="1" />
            		</div> -->
			    </div><!-- 3 --> 
			    <div class="col-sm-7 col-sm-offset-5">
			        <div style="padding-top:10px;" class="form-group">
			          <button id="save" class="btn btn-primary btn-sm" type="button" name="save" value="save">Save</button>&nbsp; 
			          <button id="clear" class="btn btn-warning btn-sm" type="button" name="clear">Clear</button> 
			        </div>            
			    </div>
		     	<div class="row">
			        <div class="col-sm-12">
			           <div class="form-group" style="border-bottom: 1px solid #CCC;">&nbsp;</div>           
			        </div>
		     	</div>
		  	</form>
		</div>
    <!-- show header -->
        <div class="col-sm-12">
            <div class="panel panel-default">
               <div class="table-responsive" id="div_export_print">
                    <div id="tab_print">
                        <table width="100%" border="0" class="table table-hover" id="setBorderTbl">
                            <thead>
	                             <tr>
		                              <?php
		                                 foreach ($thead as $th => $val) {
		                                 	if ($th == 'No'){
		                                      	echo "<th class='$val'>".$th."</th>";
		                  					}
		                                	else if ($th == 'Action'){
		                                        echo "<th style='text-align:center' class='$val'>".$th."</th>";
		                  					}else{
		                                    	echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
											}
		                                 }
		                              ?>
	                           	</tr>
	                           	<!-- <tr>
	                           		<td></td>
                                    <td><input style="width: 160px;" type="text" id="serch_name_kh" name="serch_name_kh" class="form-control" placeholder="Serch..."></td>
                                    <td><input style="width: 160px;" type="text" id="serch_name_eng" name="serch_name_eng" class="form-control" placeholder="Serch..."></td>
                                    <td><input style="text-align: center;" type="checkbox" id="sech_checkboxis_vtc" class="radio CKcheckboxis_itv" value="0"/></td>
                                    <td style="width:0px;"></td>
                                </tr> -->
                            </thead>
                            <tbody id='listrespon'></tbody>
                        </table>
                     </div>
                 </div>
            </div>
            <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
                Display :   <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                                <?php
                                    $num=10;
                                    for($i=0;$i<10;$i++){?>
                                        <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                        <?php $num+=20;
                                    }
                                ?>
                            </select>
            </div>
            <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
                <div class="pagination" id="pagination" style="text-align:center"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){
		 showdata(1,$('#sort_num').val());
		// pagenav ----------------------------------------
        $("body").delegate(".pagenav","click",function(){
            var page = $(this).attr("id");
            var total_display = $('#sort_num').val();
            showdata(page,total_display);
        });
        // sort_num -----------------------------------------
        $('#sort_num').change(function(){
            var  total_display= $(this).val();
            showdata(1,total_display);
        });
        // serch_name_kh ---------------------------------
        // $("#serch_name_kh").on('keyup',function(){
        //     var total_display = $("#sort_num").val();
        //     showdata(1,total_display);
        // });
        // serch_name_eng ---------------------------------
        // $("#serch_name_eng").on('keyup',function(){
        //     var total_display = $("#sort_num").val();
        //     showdata(1,total_display);
        // });
        //serch_checkbox -----------------------------------
        // $(".CKcheckboxis_itv").on('click', function() {
        //     $(this).val() - 0 == 0 ? $(this).val(1) : $(this).val(0);
        //     showdata(1,$('#sort_num').val());
        // });
		// check box --------------------------------------
        // $(".CKcheckbox").on('click', function() {
        //     var box = $(this);
        //     if (box.is(":checked")) {
        //         $('.CKcheckbox:checkbox[name="' + box.attr("name") + '"]').not(box).prop('checked',false);
        //         box.prop("checked",true);
        //         cmdId = box.val();
        //     }else{
        //         box.prop("checked",false);
        //     }
        // });
        // cleardata ---------------------------------------
        $('body').delegate('#clear', 'click', function(){
            cleadata();
            $('#save').html('Save');
        });
        // refresh ----------------------------------------
        $('body').delegate('#refresh', 'click', function(){
            location.reload();
        });
        // Addnew-----------------------------------------
        $('body').delegate('#a_addnew', 'click', function(){
            $('#collapseExample').collapse('toggle')
            cleadata();
        });
		// save ------------------------------------------
        $("#save").on("click",function(){
            var evaluate_id = $('#evaluate_id').val();
            var evaluate_kh = $('#evaluate_kh');
            if(evaluate_kh.val()==""){
                evaluate_kh.css('border-color','red');
            }else{
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('student/c_evaluate_setup_kgp/save')?>",
                    data : {
                        evaluate_id : $("#evaluate_id").val(),
                        evaluate_eng   : $("#evaluate_eng").val(),
                        evaluate_kh    : $("#evaluate_kh").val(),
                        status         : $("#status").val()
                        //CKcheckbox : $(".CKcheckbox:checked").val()
                    },
                    success:function(data){
                        if(data.evaluate_kh!=""){ 
                        	toastr["success"](data.msg);  
                            $("#save").html('Save');          
                            cleadata();
                            showdata(1,$('#sort_num').val());
                        }
                    }
                });

                evaluate_kh.css('border-color','#ccc');
            } 
        });
	});// end function---------------------------------------------
    // sort -------------------------------------------------------
    function sort(event){
       /*$(event.target)=$(this)*/ 
        this.sortby = $(event.target).attr("rel");
        if ($(event.target).attr("sorttype") == "ASC") {
            $('.sort').removeClass('cur_sort_up');
            $(event.target).addClass(' cur_sort_down');
            $(event.target).attr("sorttype", "DESC");
            this.sorttype = "ASC";
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass(' cur_sort_up');
        } else if ($(event.target).attr("sorttype") == "DESC") {
            $(event.target).attr("sorttype", "ASC");
            this.sorttype = "DESC";
            $('.sort').removeClass('cur_sort_up');
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass(' cur_sort_down');    
        }        
        showdata(1,$('#sort_num').val(),this.sortby,this.sorttype);
    }
	// getdata ------------------------------------------------
	function showdata(page,total_display,sortby,sorttype){
        var roleid = <?php echo $this->session->userdata('roleid');?>;
        var per    = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
        //var serch_name_eng = $('#serch_name_eng').val().toString();
        //var serch_name_kh = $('#serch_name_kh').val().toString();
        //var sech_checkboxis_vtc = $('#sech_checkboxis_vtc').val();
        var sort_num=$('#sort_num').val();           
        
        $.ajax({ 
            type : "POST",
            url  : "<?= site_url('student/c_evaluate_setup_kgp/showdata');?>",
            datatype:"Json",
            async: false,
            data : {
                'roleid': roleid,
                'sort_num':sort_num,
                'sortby':sortby,
                'sorttype':sorttype,
                'p_page': per,
                'page'  : page,
                'total_display': total_display
                //'serch_name_eng': serch_name_eng,
                //'serch_name_kh': serch_name_kh,
                //'sech_checkboxis_vtc': sech_checkboxis_vtc
            },
            success:function(data){
                $("#listrespon ").html(data['body']);
                $('.pagination').html(data['pagination']['pagination']);
            }
        });
    }
    // deletedata -------------------------------------------------
    function deletedata(event){ 
        if(window.confirm('Are your sure to delete?')){
           $.ajax({
              url: '<?= site_url('student/c_evaluate_setup_kgp/deletedata') ?>',
              type: 'POST',
              datatype: 'JSON',
              async: false,
              data: {
                 'evaluate_id':jQuery(event.target).attr("rel")
              },
                success: function(data){
                    if(data.msg = 'deleted'){
                        toastr["success"]("Data deleted!");
                        showdata(1,$('#sort_num').val());
                    }else{
                        toastr["warning"]("Can't delete!");
                    }  
                },
                error: function() {

                }
            });
        }
    }
    // update ----------------------------------------------------
    function update(event){ 
        $.ajax({ 
            type : "POST",
            url  : "<?= site_url('student/c_evaluate_setup_kgp/editdata')?>",
            data : { 
                'evaluate_id':jQuery(event.target).attr("rel")
            },
            success:function(datas){ 
                var data= datas.res;              
                $("#evaluate_id").val(data.evaluate_id);
                $("#evaluate_eng").val(data.evaluate_name_eng);
                $("#evaluate_kh").val(data.evaluate_name_kh);
                $("#status").val(data.is_active);
                $('#collapseExample').attr('aria-expanded', 'true');
                $('#collapseExample').addClass('in');
                // if (data.is_active==1){
                //     $('.CKcheckbox').prop('checked', true);
                // }else{
                //     $('.CKcheckbox').prop('checked', false);
                // }
                $("#save").html('Update');
            }
        }); 
    }
    // cleradata --------------------------------------------
    function cleadata(){
        $("#evaluate_id").val("");
        $("#evaluate_eng").val("");
        $("#evaluate_kh").val(""); 
        $('#serch_name_eng').val("");
        $('#serch_name_kh').val("");
        $('#status').val(1);
        //$('.CKcheckbox').attr('checked', false);
        //$('.CKcheckboxis_itv').attr('checked', false); 
        
    }
    
</script>