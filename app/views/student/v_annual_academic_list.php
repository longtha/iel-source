<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="result_info">
				<div class="col-sm-6"><strong>List Student</strong></div>
			</div>
		</div>
	</div>
<?php if($this->session->userdata('match_con_posid')!='stu'){?>
	<div class="row">		
		<div class="col-sm-12">
			<div class="panel panel-default">
			  	<div class="panel-body">
			    	<div class="col-sm-4">
			    		<label>School</label>
			    		<select class="form-control" id="schoolid">
			    			<option value=""></option>
			    			<?php echo $schoolid;?>
			    		</select>
			    	</div>
			    	<div class="col-sm-4">
			    		<label>School lavel</label>
			    		<select class="form-control" id="schoollavel">
			    		</select>
			    	</div>
			    	<div class="col-sm-4">
			    		<label>Year</label>
			    		<select class="form-control" id="yearid">
			    		</select>
			    	</div>
			    	<div class="col-sm-4">
			    		<label>Grade</label>
			    		<select class="form-control" id="gradeid">
			    		</select>
			    	</div>
			    	<div class="col-sm-4">
			    		<label>Class</label>
			    		<select class="form-control" id="classid">
			    		</select>
			    	</div>
			    	<div class="col-sm-12" style="margin-top: 15px;">
			  			<button id="search" name="search" class="form-control btn-primary" style="width:100px;">SEARCH</button>
			  		</div>
			  	</div>
			  	
			</div>
		</div>
	</div>
<?php } ?>
	<div class="modal fade bs-example-modal-lg" id="show_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      	<div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Annual Academic</h4>
			</div>
          <div class="modal-body">
              <div class="row">
                <div class="col-sm-4">
                  <img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="width:200px !important">
                </div>
                <div class="col-sm-8">
                  <h4 class="kh_font">របាយការណ៍សិក្សាប្រចាំឆ្នាំ</h4>
                  <h4 style="font-size:23px;">Annual Academic Report</h4>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                    <center>
                    <table class="table head_name" style="width:80%;">
                      <tr>
                        <td style="width:20%;">Student's Name:</td>
                        <td style="width:40%;">
							<span id="stud_name_eng"></span>
							<input type="hidden" name="h_studentid" id="h_studentid" class="h_studentid">
							<input type='hidden' class='form-control input-sm' id='commentid' name="commentid" value="" />
                        </td>
                        <td style="width:100px;">Level:</td><td><span id="level_name"></span></td>
                      </tr>
                      <tr>
                        <td style="width:20%;">សិស្សឈ្មោះ:</td>
                        <td style="width:40%;"><h5 id="stud_name_kh" class="kh_font"></h5></td>
                        <td>កម្រិត:</td><td><span id="level_name_kh"></span></td>
                      </tr>
                    </table>
                    </center>
                </div>
              </div>
             
              <div class="row">
                <div class="col-sm-12" style="">
                  <table class="table table-bordered" id="tbl_eval">
                    
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <textarea class="form-control" rows="5" id="t_comment" placeholder="Teacher's Comments / មតិយោបល់របស់គ្រូ"></textarea>
                </div>
                <div class="col-sm-12" style="margin-top:20px;">
                	<div class="col-sm-4" style="text-align: center;">
                        <div style="display: inline;"><b>Date:</b>&nbsp;<input type="text" name="date_director" id="date_director" class="date_director datepicker" value="<?php echo date('d-m-Y');?>" style="border:none;text-align: center;border-bottom: 1px dotted black;"></div><br>
                        <p><b>Director</b></p>
                        <center>
                        <SELECT id="user_director" class="form-control" style="width:150px !important;">
                            <?php 
                              $sql_user_acad = $this->db->query("SELECT
                                                                  sch_user.userid,
                                                                  sch_user.user_name
                                                                  FROM
                                                                  sch_user
                                                                  WHERE 1=1
                                                                  AND match_con_posid='admin'");
                              $opt_acad = "<option value=''></option>";
                              if($sql_user_acad->num_rows() > 0){
                                foreach($sql_user_acad->result() as $row_acad){
                                  $opt_acad.= "<option value='".$row_acad->userid."'>".$row_acad->user_name."</option>";
                                }
                              }   
                              echo $opt_acad;  
                            ?>
                        </SELECT>  
                        </center>
                    </div>
                    <div class="col-sm-4" style="text-align: center;">
                        <div style="display: inline;"><b>Date:</b>&nbsp;<input type="text" name="date_acade" id="date_acade" class="date_acade datepicker" value="<?php echo date('d-m-Y');?>" style="border:none;text-align: center;border-bottom: 1px dotted black;"></div><br>
                        <p><b>Academic Manager</b></p>
                        <center>
                        <SELECT id="user_acad" class="form-control" style="width:150px !important;">
                            <?php 
                              $sql_user_acad = $this->db->query("SELECT
                                                                  sch_user.userid,
                                                                  sch_user.user_name
                                                                  FROM
                                                                  sch_user
                                                                  WHERE match_con_posid='acca'");
                              $opt_acad = "<option value=''></option>";
                              if($sql_user_acad->num_rows() > 0){
                                foreach($sql_user_acad->result() as $row_acad){
                                  $opt_acad.= "<option value='".$row_acad->userid."'>".$row_acad->user_name."</option>";
                                }
                              }   
                              echo $opt_acad;  
                            ?>
                        </SELECT>  
                        </center>
                    </div>
                    <div class="col-sm-4" style="text-align: center;">
                        <div style="display: inline;"><b>Date:</b>&nbsp;<input type="text" name="date_teacher" id="date_teacher" class="date_teacher" value="<?php echo date('d-m-Y');?>" style="border:none;text-align: center;border-bottom: 1px dotted black;"></div>
                        <p><b>Teacher's Signature</b></p>
                        <?php 
                            $userid = $this->session->userdata('userid');
                            $sql_user_acad = $this->db->query("SELECT
                                                                  sch_user.user_name
                                                                  FROM
                                                                  sch_user
                                                                  WHERE sch_user.userid='".$userid."'")->row();
                            $username = isset($sql_user_acad->user_name)?$sql_user_acad->user_name:"";
                            echo "<p id='user_label'><b>".$username."</b></p>";
                        ?>
                        <input type="hidden" name="userid_teacher" id="userid_teacher" value="<?= $userid ?>">
                    </div>
                </div>
              </div>
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id='btn_save'>Save</button>
          </div>
        </div>
      </div>
  	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
						<th>No</th>
						<th>Student ID</th>
						<th>Student Name</th>
						<th>Sex</th>
						<th>School Lavel</th>
						<th>Grade lavel</th>
						<th>Year</th>
						<th>Class</th>
						<th>Total score</th>
						<th>
							<a href="javascript:void(0)" id="print_all">Print All</a>
						</th>
						</tr>
					</thead>
					<tbody id="show_body">
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		search_data();
		$("#date_acade,#date_teacher,#date_director").datepicker();
		$("body").delegate("#add_comment","click",function(){
			var tr = $(this).parent().parent();
			var fname_eng = tr.find("#f_name_eg").html();
			var fname_kh  = tr.find("#f_name_kh").html();
			var studentid = tr.find("#hstudentid").val();
			var gradeid   = $("#gradeid").find("option:selected").text();
			$("#h_studentid").val(studentid);
			$("#stud_name_eng").html(fname_eng);
			$("#stud_name_kh").html(fname_kh);
			$("#level_name").html(gradeid);
			$("#level_name_kh").html(gradeid);
			$("#show_modal").modal("show");
			$.ajax({
				type:"POST",
				url:"<?php echo site_url("reports/c_annual_academinc_report/show_command");?>",
				dataType:"JSON",
				async:false,
				data:{
					"studentid":$("#h_studentid").val(),
					"classid":$("#classid").val(),
					"schoolid":$("#schoolid").val(),
					"schlavelid":$("#schoollavel").val(),
					"yearid":$("#yearid").val(),
					"gradeid":$("#gradeid").val()
				},
				success:function(data){
					clear_comment();
					if(data.show_data != 10){
						$("#t_comment").val(data.show_data.command_teacher);
						$("#date_director").val(data.show_data.date_director);
						$("#date_acade").val(data.show_data.date_academic);
						$("#date_teacher").val(data.show_data.date_teacher);
						$("#user_director").val(data.show_data.directorid);
						$("#user_acad").val(data.show_data.academicid);
						$("#userid_teacher").val(data.show_data.teacherid);
						$("#commentid").val(data.show_data.commendid);
					}
				}
			})
		});
		$("body").delegate("#btn_save","click",function(){
			$.ajax({
				type:"POST",
				url:"<?php echo site_url("reports/c_annual_academinc_report/save_data");?>",
				dataType:"HTML",
				data:{
					"studentid":$("#h_studentid").val(),
					"classid":$("#classid").val(),
					"schoolid":$("#schoolid").val(),
					"schlavelid":$("#schoollavel").val(),
					"yearid":$("#yearid").val(),
					"gradeid":$("#gradeid").val(),
					"command":$("#t_comment").val(),
					"commentid":$("#commentid").val(),
					"date_director":$("#date_director").val(),
					"date_academic":$("#date_acade").val(),
					"date_teacher":$("#date_teacher").val(),
					"directorid":$("#user_director").val(),
					"academicid":$("#user_acad").val(),
					"teacherid":$("#userid_teacher").val()
				},
				success:function(data){
					$("#show_modal").modal("hide");
				}
			})
		})
		$("body").delegate("#schoolid","change",function(){
			$.ajax({
				type:"POST",
				url:"<?php echo site_url("reports/c_annual_academinc_report/show_school_lavel");?>",
				dataType:"JSON",
				async:false,
				data:{
					"schoolid":$(this).val()
				},
				success:function(data){
					$("#schoollavel").html(data.schlevel);
				}
			})
		});
		$("body").delegate("#schoollavel","change",function(){
			$.ajax({
				type:"POST",
				url:"<?php echo site_url("reports/c_annual_academinc_report/show_year");?>",
				dataType:"JSON",
				async:false,
				data:{
					"schoolid":$("#schoolid").val(),
					"schoollavel":$(this).val()
				},
				success:function(data){
					$("#yearid").html(data.yearid);
					show_grade_lavel();
				}
			})
		});

		$("body").delegate("#gradeid","change",function(){
			$.ajax({
				type:"POST",
				url:"<?php echo site_url("reports/c_annual_academinc_report/show_class");?>",
				dataType:"JSON",
				async:false,
				data:{
					"schoolid":$("#schoolid").val(),
					"schoollavel":$("#schoollavel").val(),
					"yearid":$("#yearid").val(),
					"gradeid":$(this).val()
				},
				success:function(data){
					$("#classid").html(data.classid);
				}
			})
		})
		$("body").delegate("#search","click",function(){
			search_data();
		})		
	})
	function search_data(){
		$.ajax({
				type:"POST",
				url:"<?php echo site_url("reports/c_annual_academinc_report/show_student_list");?>",
				dataType:"JSON",
				async:false,
				data:{
					"schoolid"    : $("#schoolid").val(),
					"schoollavel" : $("#schoollavel").val(),
					"yearid"      : $("#yearid").val(),
					"gradeid"     : $("#gradeid").val(),
					"classid"     : $("#classid").val()
				},
				success:function(data){
					$("#show_body").html(data.tr_tbl);
					if(data.link_p != ""){
						$("#print_all").html(data.link_p);
					}
				}
			})
	}
	function clear_comment(){
		$("#commentid").val("");
		$("#t_comment").val("");
		$("#user_director").val("");
		$("#user_acad").val("");
	}
	function show_grade_lavel(){
		$.ajax({
			type:"POST",
			url:"<?php echo site_url("reports/c_annual_academinc_report/show_grade_lavel");?>",
			dataType:"JSON",
			async:false,
			data:{
				"schoolid":$("#schoolid").val(),
				"schoollavel":$("#schoollavel").val()
			},
			success:function(data){
				$("#gradeid").html(data.grade);
			}
		})
	}
</script>