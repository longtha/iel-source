<?php
$yearid = $_GET['yearid'];
$year = $this->syear->getschoolyearrow($yearid);
$studentid = $student['studentid'];
?>
<style type="text/css">
    #preview td {
        padding: 3px;
    }

    th img {
        border: 1px solid #CCCCCC;
        padding: 2px;
    }

    #preview_wr {
        margin: 10px auto !important;
    }

    .tab_head th, label.control-label {
        font-family: "Times New Roman", Times, serif !important;
        font-size: 14px;
    }

    td {
        font-family: "Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important;
        font-size: 14px;
    }
    
     .listbody td {
        font-family: "Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important;
        font-size: 14px;
        text-align: center;
    }
    
    .menutab .tab ul li.active {
        background: #ffffff none repeat scroll 0 0;
        line-height: 22px;
        margin-bottom: -3px;
        position: relative;
        top: -2px;
        font-size: 14px;
    }
    .menutab .tab ul li a.active {
        color: #008000;
    }
    .menutab .tab ul li a {
        color: #3b5998;
        font-size: 13px;
        text-decoration: none;
    }
    .menutab .tab {
        border-bottom: 1px solid #ccc;
        float: left;
        padding-left: 20px;
        width: 100%;
    }
    .menutab .tab ul li {
        border-left: 1px solid #ccc;
        border-right: 1px solid #ccc;
        border-top: 1px solid #ccc;
        float: left;
        list-style: outside none none;
        padding: 3px 15px;
    }
    .menutab{margin-top: 20px;}
    

</style>

<?php
//$studentinfo = $this->std->getstudent($studentid);


    $familyid = $student['familyid'];
    $family_code ='';
    $family_name ='';
    $family_book_no ='';
    $family_home_no = "";
    $family_street = "";
    $family_village = "";
    $family_commune = "";
    $family_district = "";
    $family_province = "";
    $family_tel = "";
    $father_name ='';
    $father_ocupation ='';
    $father_dob ='';
    $father_home ='';
    $father_phone ='';
    $father_street ='';
    $father_village ='';
    $father_commune ='';
    $father_district ='';
    $father_province ='';
    $mother_name ='';
    $mother_ocupation ='';
    $mother_dob ='';
    $mother_home ='';
    $mother_phone ='';
    $mother_street ='';
    $mother_village ='';
    $mother_commune ='';
    $mother_district ='';
    $mother_province ='';

    if( $familyid !="" && $familyid !=0){
        $family = $this->family->getfamilyrow($familyid);
        if(count($family)>0){
            $family_code = $family["family_code"];
            $family_name = $family["family_name"];
            $family_book_no = $family["family_book_record_no"];
            $family_home_no = $family["home_no"];
            $family_street = $family["street"];
            $family_village = $family["village"];
            $family_commune = $family["commune"];
            $family_district = $family["district"];
            $family_province = $family["province"];
            $family_tel = $family["tel"];
            $father_name = $family["father_name_kh"];
            $father_ocupation = $family["father_ocupation_kh"];
            $father_dob = $this->green->convertSQLDate($family["father_dob"]);
            $father_home = $family["father_home"];
            $father_phone = $family["father_phone"];
            $father_street = $family["father_street"];
            $father_village = $family["father_village"];
            $father_commune = $family["father_commune"];
            $father_district = $family["father_district"];
            $father_province = $family["father_province"];
            $mother_name = $family["mother_name_kh"];
            $mother_ocupation = $family["mother_ocupation_kh"];
            $mother_dob = $this->green->convertSQLDate($family["mother_dob"]);
            $mother_home = $family["mother_home"];
            $mother_phone = $family["mother_phone"];
            $mother_street = $family["mother_street"];
            $mother_village = $family["mother_village"];
            $mother_commune = $family["mother_commune"];
            $mother_district = $family["mother_district"];
            $mother_province = $family["mother_province"];
        }
        
    }
    
    $responsible = $this->std->getresponstudent($studentid);
    if(count($responsible)>0){
        $guard_name = $responsible->respon_name;
        $guard_name_eng = $responsible->respon_name_eng;
        $guard_relationship = $responsible->relationship;
        $guard_ocupation = $responsible->occupation;
        $guard_occupation_eng = $responsible->occupation_eng;
        $guard_tel1 = $responsible->tel1;
        $guard_tel2 = $responsible->tel2;
        $guard_email = $responsible->email;
        $guard_home = $responsible->home_no;
        $guard_street = $responsible->street;
        $guard_village = $responsible->village;
        $guard_commune = $responsible->commune;
        $guard_district = $responsible->district;
        $guard_province = $responsible->province;
    }else{
        $guard_name_eng = "";
        $guard_relationship = "";
        $guard_ocupation= "";
        $guard_occupation_eng= "";
        $guard_tel1= "";
        $guard_tel2= "";
        $guard_email = "";
        $guard_home= "";
        $guard_street= "";
        $guard_village= "";
        $guard_commune= "";
        $guard_district= "";
        $guard_province= "";
    }		

$family = $this->family->getfamily($familyid );

$student_photo = base_url('assets/upload/No_person.jpg');
if (file_exists(FCPATH . "assets/upload/students/" . $_GET['yearid'] . '/' . $student["studentid"] . ".jpg")) {
    $student_photo = base_url("assets/upload/students/" . $_GET['yearid'] . '/' . $student["studentid"] . ".jpg");
}

?>
<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
        <div id="main_content">
            <div class="row result_info col-xs-12" style="padding-left: 0;padding-right: 0;">
                <div class="col-xs-6">                  
                    <?php 
                	   $link=site_url("student/student/student");
                	?>
                     <a href="<?php echo $link ?>" ><strong>Student List</strong></a> >> <span>Student Detail</span>
                </div>
                <div class="col-xs-6" style="text-align: right">
					<?php 
					   //$pdf=site_url("student/student/pdf/".$studentid."?yearid=".$yearid);
					?>
					<span class="top_action_button">						
                       <button type="button" id='btnprint' onclick='printbook();' class="btn btn-primary" style="border-style: none; background: rgba(0, 0, 0, 0) none repeat scroll 0 0;padding-left:0;padding-right: 0;">
                            <img src="<?php echo base_url('assets/images/icons/print.png') ?>"/>
                       </button>                        
		      		</span> 
		      		                  
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12" id='preview_wr'>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive" id="tab_print">
                    <style type="text/css">
                        #preview td {
                            padding: 3px;
                        }

                        th img {
                            border: 1px solid #CCCCCC;
                            padding: 2px;
                        }

                        #preview_wr {
                            margin: 10px auto !important;
                        }

                        .tab_head th, label.control-label {
                            font-family: "Times New Roman", Times, serif !important;
                            font-size: 14px;
                        }

                        td {
                            font-family: "Khmer OS", "Khmer OS Battambang", "Khmer OS Bokor" !important;
                            font-size: 14px;
                        }

                    </style>
                   
                   
                    <p style="clear:both"></p>
                    <table align='center'>
                        <thead>
                        <th valign='top' align="center" style='width:80%'>
                            <h5 align="center"><u>IEL International School</u></h5>

                            <div style='text-align:center;'>School Year: <?php echo $year->sch_year; ?> </div>
                            <?php if(isset($student['proname']) && $student['proname']){ ?>
                                <div style='text-align:center;'><?php echo $student['proname']; ?> </div>
                            <?php } ?>
                        </th>
                        <th align='right'>
                            <img src='<?php echo $student_photo ?>'
                                 style='width:140px; height:180px; float:right; margin-top:10px; margin-right:15px;'/>
                        </th>
                        </thead>
                    </table>
                    <table align='center' id='preview' style='width:100%'>
                        <tr>
                            <td><label class="control-label">Student ID :</label></td>
                            <td><?php echo $student['student_num']; ?></td>
                            <td><label class="control-label">Family ID :</label></td>
                            <td><?php echo $family_code; ?></td>
                            <td><label class="control-label">Class : </label><?php echo $student['class_name']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Student Name:</label></td>
                            <td><?php echo $student['last_name'] . " " . $student['first_name']; ?></td>

                            <td><label class="control-label">Name Khmer:</label></td>
                            <td colspan='2'><?php echo $student['last_name_kh'] . " " . $student['first_name_kh']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Date Of Birth : </label></td>
                            <td colspan='4'><?php echo $student['dob']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Nationality : </label></td>
                            <td colspan='4'><?php echo $student['nationality']; ?></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Address : </label></td>
                            <td colspan='4'><?php echo $student['home_no'].",".$student['street'].",".$student['village'].",".$student['commune'].",".$student['district'].", ".$student['province']; ?></td>
                        </tr>
                       
                        
                    </table>
                    
                    <div class="menutab">
                    	<div class="tab">
                    		
                    		<ul>	               				    				
                    			<li class="active " data-toggle="tab">សៀវភៅទំនាក់ទំនង</li>		
                    		</ul>
                    	</div>
                    	<div class="tab_content">	
                    		<form enctype="multipart/form-data" class="gform" name="std_register" id="std_register" method="POST" action="<?php echo site_url("student/student/booksave")?>">
                    			<div class="panel-body" style="margin-top: 10px;float:left;">
        			              	<div class="col-sm-12 form_sep">       			              		
        								<div class="col-sm-2">
        									<label class="req" for="month"> ខែ </label>
        								</div>
        								<div class="col-sm-4">       							
        									<select name="month" class="form-control">
									 			<?php                   		
									               foreach($month as $r):
			                    	            ?>
    				                    				<option value="<?php echo $r->id ?>" ><?php echo $r->month_kh ?></option>
            					                <?php 
            					                   endforeach;
            					                ?>
        									</select>      								
        								</div>
        																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep">       			              		
        								
        								<div class="col-sm-2">
        									<label class="req" for="month">សប្ដាហ៍</label>
        								</div>
        								<div class="col-sm-4">        							
        									<select name="week" class="form-control">
									 			<?php                   		
									               foreach($week as $r):
			                    	            ?>
    				                    				<option value="<?php echo $r->id ?>" ><?php echo $r->week_kh ?></option>
            					                <?php 
            					                   endforeach;
            					                ?>
        									</select> 								
        								</div>      																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep">        								
        								<div class="col-sm-2">
        									<label class="req" for="mentality">ចិត្តចលភាព</label>
        								</div>
        								<div class="col-sm-4">       							
        									<textarea name="mentality" class="form-control"></textarea>        								
        								</div>        								  																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep">
        								<div class="col-sm-2">
        									<label class="req" for="mentality">បញ្ញា</label>
        								</div>
        								<div class="col-sm-4">       							
        									<textarea name="intellectual" class="form-control"></textarea>        								
        								</div>        								  																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep">
        								<div class="col-sm-2">
        									<label class="req" for="mentality">អារម្មណ៍</label>
        								</div>
        								<div class="col-sm-4">       							
        									<textarea name="feeling" class="form-control"></textarea>        								
        								</div>        								  																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep">
        								<div class="col-sm-2">
        									<label class="req" for="mentality">សង្គម</label>
        								</div>
        								<div class="col-sm-4">       							
        									<textarea name="society" class="form-control"></textarea>        								
        								</div>        								  																
        			              	</div>
        			              	
        			              	<div class="col-sm-12 form_sep" style="margin-top: 20px;">
            			              	<div class="col-sm-2">
                                            <label for="times">More</label>
                                            <input id="add_academic" class="btn btn-primary" type="submit" name="add_academic" value="Add">
                                        	<input type="hidden" name="yearid" id="yearid" value="<?php echo $yearid; ?>">
                                        	<input type="hidden" name="studentid" id="studentid" value="<?php echo $studentid; ?>">
                                        	<input type="hidden" name="classid" id="classid" value="<?php echo $classid; ?>">
                                        </div>
                                    </div>
    			              	</div>
                    		</form>
                    	</div>
                    	
                    	<div class="rows">
            		      	<div class="col-sm-12">
            		      		<div class="panel panel-default">
            		      			<div class="panel-body">
            			           		<div class="table-responsive">
            			           			<?php 
            			           			$thr="";	
            			           			$tr="";	
            			           			$school=$this->db->where('schoolid',$this->session->userdata('schoolid'))->get('sch_school_infor')->row();
            			           			$school_name=$school->name;
            			           			$school_adr=$school->address;
            			           			foreach($thead as $th=>$val){           			          				
                                                $thr.="<th class='sort $val' onclick='sort(event);' rel='$val'>".$th."</th>";								
            			           			}
            			           			
            			           			if(count($tdata) > 0){
            			           			    $i=1;
            			           			    foreach($tdata as $row){            			           			                  			           			       
                                                    $tr.="<tr>
							                         <td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
							                         <td class='student_num'>".$row->month_kh."</td>
    											     <td class='student_num'>".$row->week_kh."</td>
                                                    <td class='student_num'>".$row->mentality."</td>
                                                    <td class='student_num'>".$row->intellectual."</td>
                                                    <td class='student_num'>".$row->feeling."</td>
                                                    <td class='student_num'>".$row->society."</td>
											 
        											 <td class='remove_tag'>";
                    			           			        
                    			           			        if($this->green->gAction("D")){
                    			           			            $tr.="<a class='del_row' rel=".$row->id." ><img src='".site_url('../assets/images/icons/delete.png')."'/></a>";
                    			           			        }
                    			           			        if($this->green->gAction("U")){
                    			           			            
                    			           			            $edit_link=site_url("student/student/bookedit/".$studentid."?yearid=".$yearid."&bookid=".$row->id);
                    			           			            $tr.="<a href='".$edit_link."'>
        	                                                    <img src='".site_url('../assets/images/icons/edit.png')."'/>
        	                                                  </a>";
                    			           			        }
                    			           			       
                    			           			        $tr.="</td>
											     </tr>";
            			           			        $i++;
            			           			    }
            			           			}
            														
            			           			?>
            
            			           			<table class="table" border="0">
            			           				<thead ><?php echo $thr ?></thead>
            			           				<thead class='remove_tag'>
            			           					<td class='col-xs-1' >
            			           						<input type='text' id='page' class='hide' value="<?PHP if(isset($_GET['per_page'])) echo $_GET['per_page'] ?>" />
            			           						<input type='text' value='asc' name='sort' id='sort' style='width:30px; display:none'/>			           						
            										</td>
            								 		<td class='col-xs-1'>
            								 			<select class='form-control input-sm parsley-validated' name='s_classid' id='s_classid'>
    	                   						 			<option value="">Month</option>
                						                    <?php
                						                    	foreach ($this->std->getmonth() as $class) {?>
                						                    		<option value="<?php echo $class->id ;?>" <?php if(isset($_GET['class'])){ if($class->id==$_GET['class']) echo 'selected'; }?> ><?php echo $class->month_kh;?></option>
                						                    	<?php 
                						                    	}
                						                    ?>
            					                 		</select>
            					              		</td>
            					              		
            					              		<td>
            					              			<input type="button" name="search" id="search" class="btn btn-primary" value="Search" style="height: 30px !important;">
            					              		</td>
            			           											 		
            			           				</thead>
            			           				<tbody class='listbody'>
            			           					<?php echo $tr ?>
            			           					
            			           				</tbody>
            			           			</table>
            							</div>  
            						</div>
            		      		</div>
            		      	</div>	      	
            		    </div>
                    	
                    	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="print-div" style="display:none;">
	<div style="padding:40px;">
    	<div class="print-book" style="font-size: 18px;font-family: Moul;">
    		<ul style="list-style: none;margin: 0;padding: 0;text-align: right;">
    			<li style="margin-right: 15px;">ព្រះរាជាណាចក្រកម្ពុជា</li>
    			<li>ជាតិ សាសនា ព្រះមហាក្សត្រ</li>
    		</ul>
    		<div style="float: left;margin-top: 50px;width:100%;">ក្រសួងអប់រំយុវជននិងកីឡារាជធានីភ្នំពេញ</div>
    		<div style="font-family: Siemreap;font-size:16px;float: left;margin-top: 20px;">
    			ការិយាល័យ អប់រំយុវជននិងកីឡាខណ្ឌៈ...........................<br />
    			សាលាមត្តេយ្យៈ............................................................
    		</div>
    		<div style="font-family: Moul;font-size:18px;text-align: center;margin-top: 100px;float:left;width:100%;">សៀវភៅទំនាក់ទំនង</div>
    		<div style="width:100%;float:left;text-align: center;margin-top: 100px;">
    			<div style="border: 1px solid #333;height: 130px;text-align: center;width: 100px;margin: 0 auto;padding-top: 59px;">4x6</div>
    		</div>
    		<div style="width:100%;float:left;margin-top: 150px;font-family: Siemreap;font-size:16px;">
    			<div style="width:55%;float:left;margin-right: 10%;">
    				ឈ្មោះសិស្សៈ <span style="font-weight: 700;"><?php echo $student['last_name_kh'] . " " . $student['first_name_kh']; ?></span><br /><br />
        			ថ្នាក់កំរិតៈ <span style="font-weight: 700;"><?php echo $student['class_name'] ?></span><br /><br />
        			ឆ្នាំសិក្សាៈ <span style="font-weight: 700;"><?php echo $year->sch_year; ?></span>	
    			</div>
    			<div style="width:35%;float:left;">
    				ភេទៈ <span style="font-weight: 700;"><?php echo $student['gender']; ?></span><br /><br />
        			អត្ថលេខៈ <span style="font-weight: 700;"><?php echo $student['student_num']; ?></span><br />
    			</div>
    			<div style="width:100%;float:left;margin-top: 50px;">
    				ទំនាក់ទំនងលេខខទូរស័ព្ទរបស់អាណាព្បាបាលៈ.........................................................................
    			</div>
    		</div>
    	</div>
    	
    	<div class="print-book" style="margin-top: 50px;float: left;width: 100%;">		
    		<div style="font-size: 18px;font-family: Moul;text-align: center;">ខ្លឹមសារទំនាក់ទំនង</div>
    		<div style="font-family: Siemreap;font-size:16px;float: left;margin-top: 50px;margin-bottom: 250px;">
        		<ul style="list-style: none;margin: 0;padding: 0;line-height: 50px;">
        			<li>- សៀវភៅទំានាក់ទំនងគឺជាសៀវភៅមួយដែលមានសារៈសំខាន់បំផុតក្នុងកិច្ទទំនាក់ទំនងពីការសិក្សារបស់កុមារនៅសាលា ទៅជំរាបជូនដល់អាណាព្យាបាល ដើម្បីស្វែងរកគុណភាពក្នុងការអប់រំកុមារ។</li>
        			<li>- សៀវភៅនេះត្រូវមានក្រប មានស្លាកឈ្មោះ ​និង រក្សាអោយបានស្អាតជានិច្ច។</li>
        			<li>- ជារៀងរាល់ដំណាច ខែនៃឆ្នាំសិក្សា សាលាត្រូវផ្ញើរសៀវភៅនេះជូនអាណាព្យាបាលតាមរយៈកុមារ ។</li>
        			<li>- ពេលបានទទួលសៀវភៅទំនាក់ទំនងនេះ អាណាព្យាបាលត្រូវត្រួតពិនិត្យមើលយោបល់របស់គ្រូប្រចាំថ្នាក់ និង សំណូមពររបស់សាលា ហើយដាច់ខាតត្រូវតែសរសេរឆ្លើយតបជាយោបល់នូវរាល់បំណងប្រាថ្នាមកសាលាវិញ (ដូចជាៈ ចង់បញ្ជាក់អត្តចរិកកុមារផ្តល់សំណូមពរផ្សេងៗលើចំណុចខ្វះខាតរបស់សាលា...។ល។ )
        			</li>
        			<li>- បីថ្ងៃ ក្រោយពីបានទទួលសៀវភៅនេះ អាណាព្យាបាលត្រូវឆ្លើយតបជាយោបល់រួចហើយបញ្ជូនមកសាលាវិញដាច់ខាត ។</li>
        			<li>- ករណីមិនឆ្លើយតបយោបល់មកសាលា ឬ ក៏ធ្វើអោយរហែក ឬ បាត់សៀវភៅទំនាក់ទំនងនេះ នោះមានន័យថា អាណាព្យាបាលពុំយកចិត្តទុកដាក់ និង ការអប់រំរបស់អ្នក គ្រូ និងសាលាផ្ទាល់។</li>
        		</ul>
        	</div>
        	
        	<div style="font-size: 20px;font-family: Moul;text-align: center;width:100%;float: left;margin-top: 50px;margin-bottom: 50px;">របៀបលើកសរសើរ</div>
        	<ul style="list-style: none;margin: 0;padding: 0;line-height: 50px;float: left;margin-bottom: 10px;">
        		<li style="font-size: 16px;font-family: Moul;">ទង់ប្រកួតប្រជែង (ប្រចាំថ្ងៃ)</li>
        		<li style="font-family: Siemreap;font-size:16px;">- ក្នុងមួយថ្ងៃកុមារមករៀនទៀងទាត់ ទាំងព្រឹក និង ល្ងាចគោរពវិន័យ សាលាបានល្អអនុវត្តប្រការទាំង ៧ ដែលកុមារត្រូវគោរពបាននោះ កុមារនឹងបានទទួលទង់ប្រគួតប្រជែងមួយ។</li>
        	</ul>
        	<ul style="list-style: none;margin: 0;padding: 0;line-height: 50px;float: left;margin-bottom: 10px;">
        		<li style="font-size: 16px;font-family: Moul;">ប័ណ្ណកុមារសុភាព (ប្រចាំសប្តាហ៍)</li>
        		<li style="font-family: Siemreap;font-size:16px;">- ក្នុងមួយសប្តាហ៍កុមារណាទទួលទង់ប្រកួតប្រជែង បានបួន ឬ ប្រាំ នោះជាប់ថាកុមារសុភាពល្អ ត្រូវទទួលប័ណ្ណកុមារសុភាពល្អមួយនៅចុងសប្តាហ៍។</li>
        		<li style="font-family: Siemreap;font-size:16px;">- ត្រូវសរសេរបន្ថែម នូវសកម្មភាព ដែលកុមារបានអភិវឌ្ឍ ដោយផ្តោតលើការសិក្សាអោយមានការអភិវឌ្ឍពេញលេញ និង គ្រប់ជ្រុងជ្រោយ លើចំណុច ចិត្តចលភាព បញ្ញា (ភាសា គណិត) សញ្ចេតនា (សង្គម/អារម្មណ៍)</li>
        	</ul>
        	<ul style="list-style: none;margin: 0;padding: 0;line-height: 50px;float: left;margin-bottom: 10px;">
        		<li style="font-size: 16px;font-family: Moul;">ប័ណ្ណកុមារឈ្លាសវៃ(ប្រចាំខែ)</li>
        		<li style="font-family: Siemreap;font-size:16px;">- ក្នុងមួយខែ កុមារណាមករៀនទៀងទាត់ បានទទួលប័ណ្ណកុមារសុភាពបួន នោះមានឈ្មោះថាជាកុមារឈ្លាសវៃល្អ គោរពវិន័យសាលាល្អនោះ នឹងទទួលប័ណ្ណកុមារឈ្លាសវៃមួយនៅដំណាច់ខែ។</li>  		
        	</ul>
        	<ul style="list-style: none;margin: 0;padding: 0;line-height: 50px;float: left;margin-bottom: 50px;">
        		<li style="font-size: 16px;font-family: Moul;">ប័ណ្ណបុប្ផាក្រហម</li>
        		<li style="font-family: Siemreap;font-size:16px;">- ក្នុងមួយត្រីមាស (៣ខែ) កុមារណាទទួលបានប័ណ្ណកុមារឈ្លាសវៃរៀងរាល់ដំណាច់ខែ គ្រប់បីខែ នោះថាជាកុមារល្អប្រសើរ នឹងត្រូវទទួលរង្វាន់ពិសេសគឺ    <span style="font-weight: 700;">”ប័ណ្ណបុប្ផាក្រហម”</span> ។</li>  		
        	</ul>
        	
        	<div style="font-size: 20px;font-family: Moul;text-align: center;width:100%;float: left;margin-top: 50px;margin-bottom: 50px;">ប្រការទាំងឡាយដែលកុមារត្រូវយល់ដឹង</div>
        	<ul style="list-style: none;margin: 0;padding: 0;line-height: 35px;float: left;margin-bottom: 10px;">
        		<li style="font-family: Siemreap;font-size:16px;">
        			១- ត្រូវសំអាតខ្លួនប្រាណ សក់ក្បាល និង សំលៀកបំពាក់ ស្អាតជានិច្ច ។
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			២- ត្រូវហូបទឹកឆ្អិន ផ្លែឈើ ចំណីអាហារ ដែលមានជីវជាតិ និង មានអនាម័យ
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			៣- ត្រូវខាកស្តោះ និង ចោលសំរាមក្នុងធុងសំរាម ឬ កន្លែងដាក់សំរាមជានិច្ច
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			៤- ត្រូវបត់ជើងតូច ធំ តាមកន្លែងដែលបានកំណត់ (ក្នុង បង្គន់)
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			៥- ចេះស្តាប់បង្គាប់គ្រូ ចេះជំរាបសួរ ចេះឆ្លើយ បាទ-ចាស សុំទោស ឬ អរគុណ និង  មិននិយាយកុហក់ និងជេរគ្នាឡើយ ។
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			៦- ចេះជួយគ្រូក្នុងការងារស្រាលៗ
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;width:100%;float: left;">
        			៧- ចេះស្រលាញ់មិត្តភក្តិ ចេះថែរក្សាប្រដាប់ប្រដាលេង មករៀនបានទៀងទាត់ និង មានការអភិវឌ្ឍពេញលេញគ្រប់ជ្រុងជ្រោយៈ
        			<ul style="list-style: none;margin: 0;padding: 0;line-height: 35px;float: left;margin-bottom: 10px;width:100%;margin-left: 20px;">
        				<li>
        					ក) <span style="font-weight: 700;">ចិត្តចលភាពៈ </span>
        					<ul>
        						<li>បំណិនចលករតូចៈ (គូរ ផាត់ពណ៌ សង់ សូន ដោត.....)</li>
        						<li>បំណិនចលករធំៈ (រត់ លោត លូន ដើរ តោង ឡើង.....)</li>
        					</ul>   					
        				</li>
        			</ul>
        			<ul style="list-style: none;margin: 0;padding: 0;line-height: 35px;float: left;margin-bottom: 10px;width:100%;margin-left: 20px;">
        				<li>
        					ខ) <span style="font-weight: 700;">បញ្ហាៈ (ភាសា  គណិត)</span>
        					<ul>
        						<li>ភាសាៈ បំណិនស្តាប់ សង្កេត និយាយបានជាឃ្លាសមញ្ញ រាយណ៍ការបានច្បាស់ បុរេសំណេរ (ចេះកាន់ខ្មៅដៃ ចេះគូសវាស...)</li>
        						<li>ការគិតៈ មានសមត្ថភាពប្រើប្រាស់វិញ្ញាណទាំង ៥ ដឹងបញ្ញត្តិគណិតវិទ្យា ចំនួនលំហ (លើ ក្រោម ឆ្វេង ស្តាំ កណ្តាល...)ពេលវេលា(មុនក្រោយ)លំដាប់លំដោយ សមត្ថភាពសមហេតុផល ទំហំ រង្វាស់</li>
        					</ul>   					
        				</li>
        			</ul>
        			<ul style="list-style: none;margin: 0;padding: 0;line-height: 35px;float: left;margin-bottom: 10px;width:100%;margin-left: 20px;">
        				<li>
        					គ) <span style="font-weight: 700;">សញេតនាៈ (អារម្មណ៍ ទំនាក់ទំនងសង្គម)</span>
        					<ul>
        						<li>អារម្មណ៍ៈ សប្បាយរីករាយ ពេញចិត្ត គ្រូ មិត្តភក្តិ សំភារៈក្នុង-ក្រៅថ្នាក់</li>
        						<li>ទំនាក់ទំនងសង្គមៈ លេងជាក្រុម សហការគ្នា ជួយគ្នា គោរពគ្នា ទទួល ខុសត្រូវ ម្ចាស់ការ អំណត់ព្យាយាម ។</li>
        					</ul>   					
        				</li>
        			</ul>
    			</li>
        	</ul>
        	
        	<div style="font-size: 20px;font-family: Moul;text-align: center;width:100%;float: left;margin-top: 50px;margin-bottom: 100px;">ប្រវត្តិរូបសិស្ស</div>
        	<ul style="list-style: none;margin: 0;padding: 0;line-height: 50px;float: left;margin-bottom: 50px;width:100%;">
        		<li style="font-family: Siemreap;font-size:16px;">
        			ឈ្មោះសិស្សៈ &nbsp;<span style="font-weight: 700;"><?php echo $student['last_name_kh'] . " " . $student['first_name_kh']; ?></span>
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			ថ្ងៃ ខែ ឆ្នាំ កំណើតៈ&nbsp;<?php if($student['dateofbirth'] != null )echo '<span style="font-weight: 700;">'.$student['dateofbirth'].'</span>'; else "................................."; ?>
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			ទីកន្លែងកំណើតៈ&nbsp;<?php if($student['dob_commune'] != null || $student['dob_district'] != null || $student['dob_province'] != null) echo 'ឃុំ/សង្កាត់ '.$student['dob_commune']." ស្រុក/ខណ្ឌ ".$student['dob_district']." ខេត្ត/ក្រុង  ".$student['dob_province']; else "............................"; ?>
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			ឈ្មោះឪពុកៈ &nbsp;<?php if($father_name != null || $father_name != '') echo '<span style="font-weight: 700;">'.$father_name.'</span>'; else "................................." ;?> 
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			មុខរបរៈ&nbsp;<?php if($father_ocupation != null || $father_ocupation != '') echo '<span style="font-weight: 700;">'.$father_ocupation.'</span>'; else "............................."; ?> 
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			ឈ្មោះម្តាយៈ&nbsp;<?php if($mother_name != null || $mother_name != '') echo '<span style="font-weight: 700;">'.$mother_name.'</span>'; else ".............................."; ?>
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			មុខរបរៈ&nbsp;<?php if($mother_ocupation != null || $mother_ocupation != '') echo '<span style="font-weight: 700;">'.$mother_ocupation.'</span>';else "............................."; ?>
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			ឈ្មោះអាណាព្យាបាលៈ&nbsp;<?php if($guard_name != null || $guard_name != '') echo '<span style="font-weight: 700;">'.$guard_name.'</span>'; else "..................................."; ?> 
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			មុខរបរៈ&nbsp;<?php if($guard_ocupation != null || $guard_ocupation != '') echo '<span style="font-weight: 700;">'.$guard_ocupation.'</span>'; else "..................................."; ?> 
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;">
        			ទីលំនៅបច្ចុប្បន្នៈ&nbsp;<?php echo 'ផ្ទះលេខ '.$student['home_no'].' ផ្លូវលេខ  '.$student['street'].' ​ឃុំ/សង្កាត់  '.$student['commune'].' ស្រុក/ខណ្ឌ '.$student['district'].' ខេត្ត/ក្រុង '.$student['province'] ?>
    			</li>
    			<li style="font-family: Siemreap;font-size:16px;margin-top: 100px;text-align: right;width:100%;">
        			ភ្នំពេញ, ថ្ងៃទី...............ខែ.............ឆ្នាំ  ២០....
        			<div style=" margin-right: 65px;">គ្រូទទួលបន្ទុកថ្នាក់</div>
    			</li>
    		</ul>
    		<?php
    		     $y = 0;
    		     $mm = $this->std->getmonthfilter($studentid,$yearid);
    		     foreach ($mm as $m){ 
                    $boo = $this->std->getbookbymonth($studentid,$yearid,$m->id);
            ?>
            		<div style="font-size: 16px;font-family: Moul;text-align: center;width:100%;float: left;margin-top: 50px;margin-bottom: 50px;">ខែ <?php echo $m->month_kh ?></div>
            		<div style="width:100%;float:left;font-family: Siemreap;font-size:16px;">
            <?php 
    
                    if(count($boo) > 0){
                        foreach ($boo as $b){
                            
            ?>
                    			
                            		<div style="width:50%;float:left;">
                            			<div style=" border: 1px solid #000;float: left;padding: 20px;text-align: center;width: 100%;font-size: 15px;font-family: Moul;"><?php echo $b->week_kh ?></div>
                            			<div style=" border: 1px solid #000;float: left;padding: 20px;text-align: center; width: 100%;">
                            				<span style="font-size: 15px;font-family: Moul;float:left;width: 100%;margin-bottom: 10px;">កុមារបានអភិវឌ្ឍ :</span>
                            				<ul style="list-style: none;margin: 0;padding: 0;line-height: 50px;float: left;margin-bottom: 10px;width:100%;text-align: left;">
                            					<li>ក) ចិត្តចលភាពៈ&nbsp;<span style="font-weight: 700;"><?php echo $b->mentality ?></span></li>
                            					<li>ខ) បញ្ញាៈ&nbsp;<span style="font-weight: 700;"><?php echo $b->intellectual ?></span></li>
                            					<li>គ) អារម្មណ៍ៈ&nbsp;<span style="font-weight: 700;"><?php echo $b->feeling ?></span></li>
                            					<li style="margin-left: 15px;">- សង្គមៈ&nbsp;<span style="font-weight: 700;"><?php echo $b->society ?></span></li>
                            				</ul> 
                            				<span style="font-size: 15px;font-family: Moul;float:left;width: 100%;margin-top: 50px;">ប័ណ្ណកុមារសុភាព</span>   				
                            			</div>
                            		</div>
                            	       			
            <?php 
    
                        }                
                    }
                  ?>             	
                  	</div>
                  	<div style="width:100%;float:left;margin-top: 30px;font-family: Siemreap;font-size:16px;">
                  		<div style="width:50%;float:left;">
                  			<?php if($y==0){ ?>
                  				<span style="font-size: 15px;font-family: Moul;float:left;width: 100%;margin-top: 10px;">ប័ណ្ណកុមារឈ្លាសវៃ</span> 	
                  			<?php }elseif ($y==2){?>            		
                  				<span style="font-size: 15px;font-family: Moul;float:left;width: 100%;margin-top: 10px;">ប័ណ្ណបុប្ផាក្រហម</span> 	
                  			<?php }else{ ?>
                  				<span style="font-size: 15px;font-family: Moul;float:left;width: 100%;margin-top: 10px;">ប័ណ្ណកុមារឈ្លាសវៃ</span>
                  			<?php } ?>
                  		</div>
                  		<div style="width:50%;float:left;">
                  			<span style="font-size: 15px;font-family: Moul;float:left;width: 100%;margin-top: 10px;">យោបល់របស់គ្រូ</span>
                  		</div>
                  	</div>
                  	<div style="margin-top: 200px;text-align: right;width:100%;float:left;font-family: Siemreap;font-size:16px;">
            			ភ្នំពេញ, ថ្ងៃទី...............ខែ.............ឆ្នាំ  ២០....
            			<div style=" margin-right: 65px;">គ្រូទទួលបន្ទុកថ្នាក់</div>
        			</div>
        			<div style="width:100%;float:left;margin-top: 150px;font-family: Siemreap;font-size:16px;margin-bottom: 20px;">
        				<div style="width:50%;float:left;">
                			<div style=" border: 1px solid #000;float: left;padding: 20px;text-align: center;width: 100%;font-size: 15px;font-family: Moul;">យោបល់របស់នាយិកា</div>
                			<div style=" border: 1px solid #000;float: left;padding: 20px;text-align: center; width: 100%;">
    				 			<div style="margin-top: 250px;text-align: center;width:100%;float:left;font-family: Siemreap;font-size:16px;margin-bottom: 50px;">
                        			ភ្នំពេញ, ថ្ងៃទី...............ខែ.............ឆ្នាំ  ២០....
                        			<div style=" margin-right: 0px;">ហត្ថលេខា</div>
                    			</div>		
                			</div>
                		</div>
                		<div style="width:50%;float:left;">
                			<div style=" border: 1px solid #000;float: left;padding: 20px;text-align: center;width: 100%;font-size: 15px;font-family: Moul;">យោបល់របស់មាតាបិតា</div>
                			<div style=" border: 1px solid #000;float: left;padding: 20px;text-align: center; width: 100%;">
                				<div style="margin-top: 250px;text-align: center;width:100%;float:left;font-family: Siemreap;font-size:16px;margin-bottom: 50px;">
                        			ភ្នំពេញ, ថ្ងៃទី...............ខែ.............ឆ្នាំ  ២០....
                        			<div style=" margin-right: 0px;">ឈ្មោះនិងហត្ថលេខា</div>
                    			</div>		  				
                			</div>
                		</div>
        			</div>
                 <?php 
                 $y++;
                 }
    		?>    			       	
    	</div>
	</div>	
</div>

<script type="text/javascript">
    $(function () {
        $("#print").on("click", function () {
            gPrint("tab_print", "Evaluation");
        });

        $('#search').click(function(){   		
			search();
    	});

        $('body').delegate('.del_row','click',function(){
            var obj = $(this);
            var subjectid=$(this).attr("rel");
            deletesubject(obj,subjectid);
        });     
       
    });    
    
    function search(event){
		var studentid 	 = jQuery('#studentid').val();
		var yearid 	 = jQuery('#yearid').val();
		var s_classid 	 = jQuery('#s_classid').val();
				
		$.ajax({
					url:"<?php echo base_url(); ?>index.php/student/student/searchbook",    
					data: {
						's_classid'	: s_classid,
						'studentid'			: studentid,
						'yearid'		: yearid
					},
					type: "POST",
					dataType:'json',
					async:false,
					success: function(data){
                       jQuery('.listbody').html(data.data);
				}
			});
		}
    function deletesubject(obj,subjectid){
        var r = confirm("Are you sure to delete this item?");  
        var tr= obj.closest("tr");                
        if (r == true) {              
             $.ajax({
                  url: "<?php echo site_url('student/student/deletebook');?>",
                  dataType: "Json",
                  type: "POST",
                  async: false,
                  data: {
                     'bookid': subjectid
                  },
                  success: function (data) { 
                      if(data==1){
                          toastr["success"]("Book has been deleted !");
                          tr.remove();
                      }else{
                          toastr["warmning"]("Book can't delete !");
                      } 
                  }
              });   
         }else{
            return false;
         }            
    }
</script>
<script type="text/javascript">
    function printbook(){   	
       	var data = $("#print-div").html();
       	gsPrint(data);
    }
    function gsPrint(data){
		 var element = "<div>"+data+"</div>";
		 $("<center>"+element+"</center>").printArea({
		  mode:"popup",  //printable window is either iframe or browser popup              
		  popHt: 600 ,  // popup window height
		  popWd: 500,  // popup window width
		  popX: 0 ,  // popup window screen X position
		  popY: 0 , //popup window screen Y position
		  popTitle:"", // popup window title element
		  popClose: false,  // popup window close after printing
		  strict: false 
		  });
	}

</script>