<?php
//$cars=array("Volvo","BMW","Toyota");
$relation = array("Legal Student", "father`s child", "mother`s child", "brother/sister", "Cousin", "niece/nephew");
$social_status = array("Normal", "alcohol", "violent", "Mental", "Drug");
$Health = array("Normal", "Handicap 50%", "Handicap 100%");
$civil_status = array("Single", "Married", "Divorce/widow");
$m = '';
$p = '';
if (isset($_GET['m'])) {
    $m = $_GET['m'];
}
if (isset($_GET['p'])) {
    $p = $_GET['p'];
}

?>

<style type="text/css">
    ul, ol {
        margin-bottom: 0px !important;
    }

    a {
        cursor: pointer;
    }

    table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    .datepicker {
        z-index: 9999;
    }

</style>
<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
        <div id="main_content">
            <div class="result_info">
                <div class="col-sm-6">
                    <strong>Student Information</strong>
                </div>
                <div class="col-sm-6" style="text-align: center">
                    <strong>
                        <center class='error' style='color:red;'><?php if (isset($error)) echo "$error"; ?></center>
                    </strong>
                </div>
            </div>
            <div id="stdregister_div" style="text-align:right;">
                <div class="col-sm-10"></div>
                <div class="col-sm-2">
                    <input type="text" class="form-control"  name="enter_id_card" id="enter_id_card"  placeholder="Enter Card ID">
                </div>
            </div>
            <!-- main content -->
            <form enctype="multipart/form-data" class="gform" name="std_register" id="std_register" method="POST"
                  action="<?php echo site_url("student/student/save?m=$m&p=$p") ?>">
                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <div class="form_sep" style="text-align:right">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-12 panel panel-default" style="padding-left: 0;padding-right: 0px">
                        <div class="col-sm-12 panel-heading">
                            <h4 class="panel-title"><?php echo $this->lang->line("Personal Details");?></h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="last_name"><?php echo $this->lang->line("studentid");?><span
                                            style="color:red">*</span></label>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" id='id_year' name='id_year' min=1 required
                                            data-parsley-required-message="Select any school">
                                        <?php
                                        $currentYear = date('y') + 10;
                                        $LastYear = date('y') - 10;
                                        foreach (range($LastYear, $currentYear) as $value) {
                                            echo '<option value="' . $value . '"' . ($value != date('y') ? "" : "selected") . '>' . $value . '</option>';
                                        }
                                        $maxid = $this->std->displayAutoid();
                                        //$maxid   = $this->green->nextTran(30,'Student AutoID');
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <div style="float:left; width:90%; padding-right:5px;">
                                        <input type="text" name="id_number" id="id_number" readonly="readonly" placeholder="Enter ID" required data-required="true" class="form-control parsley-validated" onblur='checkstudentid(event);'
                                           value="<?php echo $maxid; ?>"></div>
                                    <div style="float:right; padding-right:0;" >
                                        <input type="checkbox" name="is_change" value="0" onclick="is_changeid(event);" id="is_change" /></div>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" placeholder="Enter Card" data-required="true"
                                           class="form-control" readonly name="id_card" value="" id="id_card">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req"><?php echo $this->lang->line("register_date");?> <span style="color:red">*</span></label>
                                </div>
                                <div class="col-sm-2">
                                    <div data-date-format="dd-mm-yyyy" class="input-group date reqister">
                                        <input type="text" value="<?php echo date('d-m-Y'); ?>" data-type="dateIso"
                                               data-required="true" required
                                               class="form-control input_validate parsley-validated"
                                               id="register_date" name="register_date">
                                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="last_name"><?php echo $this->lang->line("first_name");?><span
                                            style="color:red">*</span></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" required data-required="true" onblur='fillusername(event);'
                                           class="form-control parsley-validated" name="last_name" value=""
                                           id="last_name">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="last_name"><?php echo $this->lang->line("first_name_kh");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" data-required="true" class="form-control parsley-validated"
                                           name="last_name_kh" value="" id="last_name_kh">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="last_name_ch"><?php echo $this->lang->line("first_name_ch");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" data-required="true" class="form-control parsley-validated"
                                           name="last_name_ch" value="" id="last_name_ch">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="first_name"><?php echo $this->lang->line("last_name");?><span
                                            style="color:red">*</span></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" required data-required="true" onblur='fillusername(event);'
                                           class="form-control parsley-validated" name="first_name" value=""
                                           id="first_name">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="first_name_kh"><?php echo $this->lang->line("last_name_kh");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" data-required="true" class="form-control parsley-validated"
                                           name="first_name_kh" value="" id="first_name_kh">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="first_name_ch"><?php echo $this->lang->line("last_name_ch");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" data-required="true" class="form-control parsley-validated"
                                           name="first_name_ch" value="" id="first_name_ch">
                                </div>
                            </div>

                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req"><?php echo $this->lang->line("dob");?><span style="color:red">*</span></label>
                                </div>
                                <div class="col-sm-2">
                                    <div data-date-format="dd-mm-yyyy" class="input-group date dob">
                                        <input type="text" id="dob" name="dob" required value="" data-type="dateIso"
                                               data-parsley-required-message="Select Date Of Birth"
                                               data-required="true"
                                               class="form-control input_validate parsley-validated">
                                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label for="gender"><?php echo $this->lang->line("gender");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <select data-required="true" name="gender" id="gender" required
                                            data-parsley-required-message="Select Gender" minlength='1'
                                            class="form-control parsley-validated">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label for="nationality"><?php echo $this->lang->line("nationality");?><span style="color:red">*</span></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="nationality" name="nationality" required
                                           data-type="dateIso"
                                           data-parsley-required-message="Choose Your nationality"
                                           data-required="true"
                                           class="form-control input_validate parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 panel-heading">
                                <h4 class="panel-title"><u><?php echo $this->lang->line("place_of_birth");?></u>
                                </h4>
                            </div>
                            <div class="col-sm-12 hide">
                                <div class="col-sm-2">
                                    <label class="req" for="dob_home_no"><?php echo $this->lang->line("home_no");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_home_no" id="dob_home_no" data-type="number"
                                           data-required="true" class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_street"><?php echo $this->lang->line("street");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="dob_street" name="dob_street" data-type="dateIso"
                                           data-required="true"
                                           class="form-control input_validate parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_village"><?php echo $this->lang->line("village");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="dob_village" name="dob_village" data-type="dateIso"
                                           data-required="true"
                                           class="form-control input_validate parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="dob_commune"><?php echo $this->lang->line("commune");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_commune" id="dob_commune" value="" data-type="number"
                                           data-required="true" class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_district"><?php echo $this->lang->line("district");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_district" id="dob_district" value="" data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_province"><?php echo $this->lang->line("province");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_province" id="dob_province" value="" data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="dob_commune_eng">Commune</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_commune_eng" id="dob_commune_eng" value="" data-type="number"
                                           data-required="true" class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_district_eng">District</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_district_eng" id="dob_district_eng" value="" data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="dob_province_eng">Province</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="dob_province_eng" id="dob_province_eng" value="" data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 panel-heading">
                                <h4 class="panel-title"><u><?php echo $this->lang->line("current_address");?></u>
                                </h4>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="home_no"><?php echo $this->lang->line("home_no");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="home_no" id="home_no" data-type="number"
                                           data-required="true" class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="street"><?php echo $this->lang->line("street");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="street" name="street" data-type="dateIso"
                                           data-required="true"
                                           class="form-control input_validate parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="village"><?php echo $this->lang->line("village");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="village" name="village" data-type="dateIso"
                                           data-required="true"
                                           class="form-control input_validate parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="home_no_eng">Home No</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="home_no_eng" id="home_no_eng" data-type="number"
                                           data-required="true" class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="street_eng">Street</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="street_eng" name="street_eng" data-type="dateIso"
                                           data-required="true"
                                           class="form-control input_validate parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="village_eng">Village</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="village_eng" name="village_eng" data-type="dateIso"
                                           data-required="true"
                                           class="form-control input_validate parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="commune"><?php echo $this->lang->line("commune");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="commune" id="commune" value="" data-type="number"
                                           data-required="true" class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="district"><?php echo $this->lang->line("district");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="district" id="district" value="" data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="province"><?php echo $this->lang->line("province");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="province" id="province" value="" data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="commune_eng">Commune</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="commune_eng" id="commune_eng" value="" data-type="number"
                                           data-required="true" class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="district_eng">District</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="district_eng" id="district_eng" value="" data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="province_eng">Province</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="province_eng" id="province_eng" value="" data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                            	<div class="col-sm-2">
                                    <label class="req" for="stu_relationship"><?php echo $this->lang->line("staywith");?><span​
                                            style="color:red">*</span></label>
                                </div>
                            	<div class="col-sm-2">
                                    <select data-required="true" required
                                            data-parsley-required-message="Select Relationship"
                                            class="form-control parsley-validated" name="stu_relationship"
                                            id="stu_relationship">
                                        <option value="">Select stay with</option>
                                        <?php
                                        $staywith = "stay_with";
                                        foreach ($this->std->getsocailinfo($staywith) as $relat_row) { ?>
                                            <option
                                                value="<?php echo $relat_row->famnoteid; ?>"><?php echo $relat_row->description; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="phone1"><?php echo $this->lang->line("phone1");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" data-type="phone" onkeypress='return isNumberKey(event);'
                                           value="" data-required="true" class="form-control parsley-validated"
                                           name="phone1" id="phone1">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="reg_input_currency"><?php echo $this->lang->line("email");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="email" data-parsley-type="email" data-type="email" value=""
                                           data-required="true" class="form-control parsley-validated" name="email"
                                           id="email">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                            	<div class="col-sm-2">
                                    <label class="req" for="phone1"><?php echo $this->lang->line("registered_number");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" data-type="number" onkeypress='return isNumberKey(event);'
                                           value="" data-required="true" class="form-control parsley-validated"
                                           name="registered_number" id="registered_number">
                                </div>
                                <div class="col-sm-2">
                                	<label for="reg_input_logo"><?php echo $this->lang->line("registered_discount");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" data-type="number" onkeypress='return isNumberKey(event);'
                                           value="" data-required="true" class="form-control parsley-validated"
                                           name="register_discount" id="register_discount">
                                </div>
                                <div class="col-sm-2">
                                    <!--<label for="reg_input_logo"><?php echo $this->lang->line("reg_input_logo");?></label> -->
                                </div>
                                <div class="col-sm-2">
                                </div>

                            </div>
                            <div class="col-sm-12 form_sep">
                            	<div class="col-sm-2"></div>
                                <div class="col-sm-2">
                                    <input type="checkbox" onclick="is_leave(event);" name="leave_school"
                                           id='leave_school' value="0"/>
                                    <label class="req" for="leave_school"><?php echo $this->lang->line("is_vtc");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <label for="leave_reason"><?php echo $this->lang->line("leave_reason");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" placeholder="Reason Leave" data-required="true"
                                           class="form-control" name="leave_reason" value="" id="leave_reason">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req"><?php echo $this->lang->line("leave_sch_date");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <div data-date-format="dd-mm-yyyy" class="input-group date">
                                        <input type="text" value="" data-type="dateIso" data-required="true"
                                               class="form-control input_validate parsley-validated" id="leave_date"
                                               name="leave_date">
                                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <!--
                            <div class="panel-heading hide">
                                <h4 class="panel-title">Login Information</h4>
                            </div>
                            <div class="col-sm-12 form_sep hide">
                                <div class="col-sm-2">
                                    <label class="req" for="login_username">User Name</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text"
                                           data-minlength-message="Username should have at least 5 characters."
                                           value="" data-required="true" onclick="get_stdregno();"
                                           class="form-control parsley-validated" name="login_username"
                                           id="login_username">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="password">Password</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="password" value="<?php //echo $this->std->getmaxid(); ?>"
                                           data-required-message="Please enter a valid Password"
                                           data-minlength-message="Password should have at least 6 characters."
                                           data-minlength="6" data-required="true" onclick="get_dob();"
                                           class="form-control parsley-validated" name="password" id="password">
                                </div>
                                <div class="col-sm-4">
                                </div>
                            </div>
                            -->
                        </div>
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $this->lang->line("Father Information");?></h4>
                        </div>
                        <div class="panel-body">

                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="family_id"><?php echo $this->lang->line("familyid");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" placeholder="Famaily Code" name="family_id"
                                           id="family_id" data-required="true" value=""
                                           data-parsley-required-message="Enter Family"
                                           class="form-control parsley-validated family_id">
                                    <input type="text" value="" class="hide" name="familyid" id="familyid">
                                </div>
                                <div class="col-sm-1">
                                    <input id="add_family" class="btn btn-primary" type="button" data-toggle="modal"
                                           data-target="#myModal" name="add_family" value="Add">
                                </div>
                                <div class="col-sm-1"><label class="req" for="family_name"><?php echo $this->lang->line("family_name");?></label></div>
                                <div class="col-sm-2">
                                    <input type="text" readonly placeholder="Famaily Name" value=""
                                           class="form-control parsley-validated" name="family_name" value=""
                                           id="family_name">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="family_book_no"><?php echo $this->lang->line("family_book_record_no");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly onblur="" name="family_book_no" id="family_book_no"
                                           value="" class="form-control parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="father_name"><?php echo $this->lang->line("father_name");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly onblur="" value=""
                                           class="form-control parsley-validated" name="father_name" value=""
                                           id="father_name">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="father_occupation"><?php echo $this->lang->line("father_ocupation");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly value="" class="form-control parsley-validated"
                                           name="father_occupation" value="" id="father_occupation">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="father_dob"><?php echo $this->lang->line("father_dob");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <div data-date-format="dd-mm-yyyy" class="input-group date dob">
                                        <input type='text' readonly id="father_dob" class="form-control"
                                               name="father_dob" placeholder="dd-mm-yyyy"/>
						    	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="father_name_eng">Father's Name</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly onblur="" value=""
                                           class="form-control parsley-validated" name="father_name_eng" value=""
                                           id="father_name_eng">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="father_occupation_eng">Occupation</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly value="" class="form-control parsley-validated"
                                           name="father_occupation_eng" value="" id="father_occupation_eng">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="father_phone"><?php echo $this->lang->line("father_phone");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="father_phone" id="father_phone"
                                           class="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="father_home"><?php echo $this->lang->line("father_home");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="father_home" id="father_home"
                                           class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="father_street"><?php echo $this->lang->line("father_street");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="father_street" id="father_street"
                                           class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="father_village"><?php echo $this->lang->line("father_village");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="father_village" id="father_village"
                                           class="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="father_commune"><?php echo $this->lang->line("father_commune");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="father_commune" id="father_commune"
                                           class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="father_district"><?php echo $this->lang->line("father_district");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="father_district" id="father_district"
                                           class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="father_province"><?php echo $this->lang->line("father_province");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="father_province" id="father_province"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $this->lang->line("Mother Information");?></h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="mother_name"><?php echo $this->lang->line("mother_name");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly onblur="" value=""
                                           class="form-control parsley-validated" name="mother_name" value=""
                                           id="mother_name">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="mother_occupation"><?php echo $this->lang->line("mother_ocupation");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly value="" class="form-control parsley-validated"
                                           name="mother_occupation" value="" id="mother_occupation">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="mother_dob"><?php echo $this->lang->line("mother_dob");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <div data-date-format="dd-mm-yyyy" class="input-group date dob">
                                        <input type='text' readonly id="mother_dob" class="form-control"
                                               name="mother_dob" placeholder="dd-mm-yyyy"/>
							    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="mother_name_eng">Mother's Name</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly onblur="" value=""
                                           class="form-control parsley-validated" name="mother_name_eng" value=""
                                           id="mother_name_eng">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="mother_occupation_eng">Occupation</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly value="" class="form-control parsley-validated"
                                           name="mother_occupation_eng" value="" id="mother_occupation_eng">
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="mother_phone"><?php echo $this->lang->line("mother_phone");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="mother_phone" id="mother_phone"
                                           class="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="mother_home"><?php echo $this->lang->line("mother_home");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="mother_home" id="mother_home"
                                           class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="mother_street"><?php echo $this->lang->line("mother_street");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="mother_street" id="mother_street"
                                           class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="mother_village"><?php echo $this->lang->line("mother_village");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="mother_village" id="mother_village"
                                           class="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="mother_commune"><?php echo $this->lang->line("mother_commune");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="mother_commune" id="mother_commune"
                                           class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="mother_district"><?php echo $this->lang->line("mother_district");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="mother_district" id="mother_district"
                                           class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="mother_province"><?php echo $this->lang->line("mother_province");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" readonly name="mother_province" id="mother_province"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $this->lang->line("Guardain Information");?></h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="guard_name"><?php echo $this->lang->line("respon_name");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_name" id="guard_name" class="form-control"
                                           data-parsley-required-message="Enter First Name"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="guard_occupation"><?php echo $this->lang->line("occupation");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_occupation" id="guard_occupation"
                                           class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="consultant"><?php echo $this->lang->line("consultant_by");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" value="<?php echo $this->session->userdata('user_name'); ?>"
                                           readonly="readonly" name="consultant" id="consultant" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="guard_name_eng">Guardian's Name</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_name_eng" id="guard_name_eng" class="form-control"
                                           data-parsley-required-message="Enter First Name"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="guard_occupation_eng">Occupation</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_occupation_eng" id="guard_occupation_eng"
                                           class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="getinfor"><?php echo $this->lang->line("get_info_from");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="getinfor" id="getinfor" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="guard_tel1"><?php echo $this->lang->line("tel1");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_tel1" onkeypress='return isNumberKey(event);'
                                           id="guard_tel1" class="form-control"
                                           data-parsley-required-message="Enter Phone Number"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="guard_tel2"><?php echo $this->lang->line("tel2");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_tel2" onkeypress='return isNumberKey(event);'
                                           id="guard_tel2" class="form-control"
                                           data-parsley-required-message="Enter Phone Number"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="email"><?php echo $this->lang->line("email");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="email" name="guard_email" id="guard_email" data-parsley-type="email"
                                           data-type="email" value="" data-required="true"
                                           class="form-control parsley-validated">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="guard_home"><?php echo $this->lang->line("home_no");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_home" id="guard_home" class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="guard_street"><?php echo $this->lang->line("street");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_street" id="guard_street" class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="guard_village"><?php echo $this->lang->line("village");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_village" id="guard_village" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="guard_commune"><?php echo $this->lang->line("commune");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_commune" id="guard_commune" class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="guard_district"><?php echo $this->lang->line("district");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_district" id="guard_district" class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="guard_province"><?php echo $this->lang->line("province");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="guard_province" id="guard_province" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $this->lang->line("Academic Year");?></h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req"><?php echo $this->lang->line("program");?></label>

                                    <select class="form-control" name="sch_program" id="sch_program">
                                        <?php

                                        $program = '<option value="">Select Program</option>';
                                        $programid = "";
                                        foreach ($this->pro->getprograms($programid) as $row_program) {
                                            $program .= '<option value="' . $row_program->programid . '">' . $row_program->program . '</option>';
                                        }
                                        echo $program; ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req"><?php echo $this->lang->line("sch_level");?></label>

                                    <select class="form-control" name="sch_level" id="sch_level">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req"><?php echo $this->lang->line("sch_year");?></label>

                                    <select class="form-control" id='sch_year' name='sch_year' min=1
                                            data-parsley-required-message="Select Academic year">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req"><?php echo $this->lang->line("rangelevelname");?></label>

                                    <select class="form-control" id='sch_rangelevel' name='sch_rangelevel' min=1
                                            data-parsley-required-message="Select Range Level">
                                        <option value=""></option>
                                    </select>

                                </div>
                                <div class="col-sm-2">
                                    <label class="req"><?php echo $this->lang->line("class_name");?></label>

                                    <select class="form-control" id='sch_class' name='sch_class' min=1
                                            data-parsley-required-message="Select Class">
                                        <option value=""></option>
                                    </select>

                                </div>

                                <div class="col-sm-2">
                                    <label for="times">More</label>
                                    <input id="add_academic" class="btn btn-primary" type="button" name="add_academic"
                                           value="Add">
                                </div>
                            </div>
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-inverse" id="academic_year">
                                        <thead>
                                        <tr><th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("reg_input_logo");?></label></th>
                                            <th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("program");?></label></th>
                                            <th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("sch_level");?></label></th>
                                            <th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("sch_year");?></label></th>
											<th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("rangelevelname");?></label></th>
											<th style="text-align: center !important; width: 17%"><label class="req"><?php echo $this->lang->line("class_name");?></label></th>
                                        </tr>
                                        </thead>
                                        <tbody id="body_school_year">

                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $this->lang->line("Office Use Only");?></h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 form_sep">
                                <div class="col-sm-2">
                                    <label class="req" for="interviewby"><?php echo $this->lang->line("interview_by");?></label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="interviewby" id="interviewby" class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="req" for="introduced"><?php echo $this->lang->line("introduced_by");?></label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="radio" id="introducedby_student" class="introducedby_student"
                                           name="introducedby" VALUE="intro_std"> <label class="req"
                                                                                         for="introducedby_student"><?php echo $this->lang->line("student");?></label>&nbsp;<input
                                        type="radio" id="introducedby_teach" class="introducedby_teach"
                                        name="introducedby" VALUE="intro_teach"> <label class="req"
                                                                                        for="introducedby_teach"><?php echo $this->lang->line("teacher");?></label>&nbsp;
                                    <input type="radio" id="introducedby_other" class="introducedby_other"
                                           name="introducedby" VALUE="intro_other" checked="checked"> <label class="req"
                                                                                                             for="introducedby_other"><?php echo $this->lang->line("Other");?></label>
                                </div>
                                <div class="col-sm-2"><input type="text" name="introduced" id="introduced"
                                                             class="form-control"/></div>
                            </div>
                        </div>

                    <div class="panel-heading">
                        <h4 class="panel-title"><?php echo $this->lang->line("Transportation");?></h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form_sep">
                            <div class="col-sm-2">
                                <input type="radio" id="seft_transport" class="seft_transport" name="transportation"
                                       VALUE="owner" checked="checked"> <label class="req" for="seft_transport">
                                    <?php echo $this->lang->line("is_owner_by");?></label>
                            </div>
                            <div class="col-sm-2">
                                <select data-required="true" data-parsley-required-message="Select transportation"
                                        class="form-control parsley-validated" name="owner_tran_by"
                                        id="owner_tran_by">
                                    <option value="">Select Transportation</option>
                                    <?php
                                    $transportation = 'transport';
                                    foreach ($this->std->getsocailinfo($transportation) as $relat_row) { ?>
                                        <option
                                            value="<?php echo $relat_row->famnoteid; ?>"><?php echo $relat_row->description; ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <input type="radio" id="school_car" class="school_car" name="transportation"
                                       VALUE="school">
                                <label class="req" for="introducedby_student"><?php echo $this->lang->line("is_sch_car_no");?></label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="school_carno" id="school_carno" placeholder="Enter Car No"
                                       class="form-control"/>
                            </div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-2"></div>
                        </div>
                    </div>
                    <div class="panel-heading">
                        <h4 class="panel-title"><?php echo $this->lang->line("Academic Background");?></h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form_sep">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-inverse" id="academic_background">
                                    <thead>
                                    <tr>
                                        <th><?php echo $this->lang->line("acadtype");?></th>
                                        <th><?php echo $this->lang->line("`name`");?></th>
                                        <th><?php echo $this->lang->line("sch_year");?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                        	<input type="hidden" name="edu_primary" id="edu_primary"
                                                   value="Primary School(Grade1-6)"/>
                                            <input type="hidden" name="edu_primary" id="edu_primary"
                                                   value="Primary School(Grade1-6)"/>
                                            <label class="req"><?php echo $this->lang->line("Primary School(Grade1-6)");?></label>
                                        </td>
                                        <td><input type="text" name="primary_sch_name" id="primary_sch_name"
                                                   class="form-control"/></td>
                                        <td><input type="text" name="primary_sch_ayear" id="primary_sch_ayear"
                                                   class="form-control"/></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="req"><?php echo $this->lang->line("Lower Secondary School(Grade7-9)");?></label>
                                            <input type="hidden" name="edu_Lower" id="edu_Lower"
                                                   value="Lower Secondary School(Grade7-9)"/>
                                        </td>
                                        <td><input type="text" name="lower_sch_name" id="lower_sch_name"
                                                   class="form-control"/></td>
                                        <td><input type="text" name="lower_sch_ayear" id="lower_sch_ayear"
                                                   class="form-control"/></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="edu_upper" id="edu_upper"
                                                   value="Upper Secondary School(Grade10-12)"/>
                                            <label class="req"><?php echo $this->lang->line("Upper Secondary School(Grade10-12)");?></label>
                                        </td>
                                        <td><input type="text" name="upper_sch_name" id="upper_sch_name"
                                                   class="form-control"/></td>
                                        <td><input type="text" name="upper_sch_ayear" id="upper_sch_ayear"
                                                   class="form-control"/></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-heading">
                        <h4 class="panel-title"><?php echo $this->lang->line("General English");?></h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form_sep">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-inverse" id="general_background">
                                    <tr>
                                        <th><?php echo $this->lang->line("Current Level Of English");?></th>
                                        <th><?php echo $this->lang->line("`name`");?></th>
                                        <th><?php echo $this->lang->line("sch_year");?></th>
                                    </tr>
                                    <tr>
                                        <td rowspan="">
                                            <div class="radio">
                                                <label class="req" for="beginner"><input type="radio" checked name="general_currentlevel" value="Beginner" id="beginner">Beginner</label>
                                                <label class="req" for="elementary"><input type="radio" class="elementary" name="general_currentlevel" value="Elementary"
                                                                                           id="elementary">Elementary</label>
                                                <label class="req" for="pre_intermediate"><input type="radio"
                                                                                                 class="pre_intermediate"
                                                                                                 name="general_currentlevel"
                                                                                                 value="Pre-intermediate"
                                                                                                 id="pre_intermediate">Pre-Intermediate</label>
                                                <label class="req" for="intermediate"><input type="radio"
                                                                                             class="intermediate"
                                                                                             name="general_currentlevel"
                                                                                             value="Intermediate"
                                                                                             id="intermediate">Intermediate</label>
                                                <label class="req" for="upper_intermediate"><input type="radio"
                                                                                                   class="upper_intermediate"
                                                                                                   name="general_currentlevel"
                                                                                                   value="Upper-intermediate"
                                                                                                   id="upper_intermediate">Upper-Intermediate</label>
                                                <label class="req" for="advanced"><input type="radio"
                                                                                         class="advanced"
                                                                                         name="general_currentlevel"
                                                                                         value="Advanced"
                                                                                         id="advanced">Advanced</label>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" name="general_sch_name" id="general_sch_name"
                                                   class="form-control"/>
                                        </td>
                                        <td>
                                            <input type="text" name="general_sch_ayear" id="general_sch_ayear"
                                                   class="form-control"/>
                                        </td>
                                    </tr>
                                    <tr class="hide">
                                        <td>
                                            <input type="text" name="general_sch_name1" id="general_sch_name1"
                                                   class="form-control"/>
                                        </td>
                                        <td>
                                            <input type="text" name="general_sch_ayear1" id="general_sch_ayear1"
                                                   class="form-control"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- login -->
                    <div class="panel-heading">
                        <h4 class="panel-title">Authentication</h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-6" style="padding: 2px;">
                            <div class="form_sep">
                                <label>User Login</label>
                                <input type="text" class="form-control" readonly name="txtu_name" id="txtu_name" placeholder="User name" required data-parsley-required-message="Enter User Name" />
                            </div>
                            <div class="form_sep">
                                <label>Password</label>

                                <div class="input-group">
                                    <input type='password' class="form-control" name='txtpwd' id='txtpwd' value="<?php echo $spass; ?>" required data-parsley-required-message="Enter Password" placeholder="Password" />
                                    <span class="input-group-btn">
                                        <button type="button" id="btnShowPassword" class="btn btn-default">
                                            <span class="glyphicon glyphicon-eye-close"></span>
                                        </button>
                                    </span>
                                </div>

                                <!-- <input type="password" class="form-control" name="txtpwd" id="txtpwd" value="<?php echo $spass; ?>" /> -->
                            </div>
                        </div>
                        <div class="col-sm-6" style="padding: 2px;">
                            <div class="form_sep">
                                <label>Role</label>
                                <select name='cborole' id='cborole' class="form-control">
                                    <?php
                                    foreach ($this->role->getallrole() as $role_row) {
                                        echo "<option value='$role_row->roleid'>$role_row->role</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form_sep">
                                <label>School Level</label>
                                <select name="cboschlevel[]" id="cboschlevel" class="form-control" multiple="multiple">
                                    <?php
                                    foreach ($this->db->get('sch_school_level')->result() as $schl) {
                                        echo "<option value='$schl->schlevelid'>$schl->sch_level</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <!-- start up -->
                        <div class="col-sm-12" style="padding: 5px;">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="padding: 5px;">
                                    <h5 class="panel-title">Startup Page</h5>
                                </div>
                                <div class="panel-body">
                                    <div class="col-sm-4" style="padding: 2px;">
                                        <div class="form_sep">
                                            <label for="emailField">Dashboard</label>
                                            <select  class="form-control months"  name="dashboard" id="dashboard" >
                                                <?php

                                                $dash= array('Full'=>'/system/dashboard',
                                                            'Socail'=>'system/dashboard/view_soc',
                                                            'Health'=>'/system/dashboard/view_health',
                                                            'Employee'=>'/system/dashboard/view_staff',
                                                            'Student'=>'/system/dashboard/view_std');

                                                foreach ($dash as $key => $value) {
                                                    echo "<option value='$value'>$key</option>";
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" style="padding: 2px;">
                                        <div class="form_sep">
                                            <label>Module</label>
                                            <select  class="form-control months" name="moduleallow[]" id="moduleallow" >
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" style="padding: 2px;">
                                        <div class="form_sep">
                                            <label for="emailField">Def. Page</label>
                                            <select  class="form-control months"  name="defpage" id="defpage" >
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="panel-heading hide">
                        <h4 class="panel-title">More info...</h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form_sep hide">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-2"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form_sep">
                        <button id="std_reg_submit" name="std_reg_submit" type="submit" class="btn btn-success">
                            Save
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- start new dialog  -->

<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $this->lang->line("New Family Information");?></h4>
            </div>
            <div class="modal-body ">
                <div class="stdscrollbar">
                    <form method="post" class="gform" accept-charset="utf-8" class="frmaddrespond" id="frmaddrespond">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel-body">
                                    <div class="form-group" id="exist" style="display:none">Data is already exist,
                                        Please try again...!
                                    </div>
                                    <!-- Start Form  -->
                                    <div class="panel-body">
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_id"><?php echo $this->lang->line("familyid");?><span
	                                                    style="color:red">*</span></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" required class="form-control parsley-validated"
	                                                   name="d_family_id" value="" id="d_family_id">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_name"><?php echo $this->lang->line("family_name");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" class="form-control parsley-validated"
	                                                   name="d_family_name" value="" id="d_family_name">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_book_no"><?php echo $this->lang->line("family_book_record_no");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" class="form-control parsley-validated"
	                                                   name="d_family_book_no" value="" id="d_family_book_no">
	                                        </div>
	                                    </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_home"><?php echo $this->lang->line("home_no");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_home" id="d_family_home"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_street"><?php echo $this->lang->line("street");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_street" id="d_family_street"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_village"><?php echo $this->lang->line("village");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_village" id="d_family_village"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_commune"><?php echo $this->lang->line("commune");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_commune" id="d_family_commune"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_district"><?php echo $this->lang->line("district");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_district" id="d_family_district"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_family_province"><?php echo $this->lang->line("province");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_family_province" id="d_family_province"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>
	                                </div>
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><?php echo $this->lang->line("Father Information");?></h4>
                                    </div>
                                    <div class="panel-body">
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_name"><?php echo $this->lang->line("father_name");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" value="" class="form-control parsley-validated"
	                                                   name="d_father_name" id="d_father_name">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_occupation"><?php echo $this->lang->line("father_ocupation");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" value="" class="form-control parsley-validated"
	                                                   name="d_father_occupation" id="d_father_occupation">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="father_dob"><?php echo $this->lang->line("father_dob");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <div data-date-format="dd-mm-yyyy" class="input-group date dob">
	                                                <input type='text' id="d_father_dob" class="form-control"
	                                                       name="d_father_dob" placeholder="dd-mm-yyyy"/>
													    	<span class="input-group-addon"><span
	                                                                class="glyphicon glyphicon-calendar">
	                                            </div>
	                                        </div>
	                                    </div>
                                        <div class="col-sm-12 form_sep">
                                            <div class="col-sm-2">
                                                <label class="req" for="d_father_name_eng">Father's Name</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" value="" class="form-control parsley-validated"
                                                       name="d_father_name_eng" id="d_father_name_eng">
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="req" for="d_father_occupation_eng">Occupation</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" value="" class="form-control parsley-validated"
                                                       name="d_father_occupation_eng" id="d_father_occupation_eng">
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="req" for="d_father_phone"><?php echo $this->lang->line("father_phone");?></label>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" name="d_father_phone" id="d_father_phone"
                                                       class="form-control"/>
                                            </div>
                                        </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_home"><?php echo $this->lang->line("father_home");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_home" id="d_father_home"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_street"><?php echo $this->lang->line("father_street");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_street" id="d_father_street"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_village"><?php echo $this->lang->line("father_village");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_village" id="d_father_village"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_commune"><?php echo $this->lang->line("father_commune");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_commune" id="d_father_commune"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_district"><?php echo $this->lang->line("father_district");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_district" id="d_father_district"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_father_province"><?php echo $this->lang->line("father_province");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_father_province" id="d_father_province"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>
	                                </div>
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><?php echo $this->lang->line("Mother Information");?></h4>
                                    </div>
                                    <div class="panel-body">
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_name"><?php echo $this->lang->line("mother_name");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" onblur="" value="" class="form-control parsley-validated"
	                                                   name="d_mother_name" id="d_mother_name">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_occupation"><?php echo $this->lang->line("mother_ocupation");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" value="" class="form-control parsley-validated"
	                                                   name="d_mother_occupation" id="d_mother_occupation">
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_dob"><?php echo $this->lang->line("mother_dob");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <div data-date-format="dd-mm-yyyy" class="input-group date dob">
	                                                <input type='text' id="d_mother_dob" class="form-control"
	                                                       name="d_mother_dob" placeholder="dd-mm-yyyy"/>
														    <span class="input-group-addon"><span
	                                                                class="glyphicon glyphicon-calendar">
	                                            </div>
	                                        </div>
	                                    </div>
                                        <div class="col-sm-12 form_sep">
                                            <div class="col-sm-2">
                                                <label class="req" for="d_mother_name_eng">Mother' Name</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" onblur="" value="" class="form-control parsley-validated"
                                                       name="d_mother_name_eng" id="d_mother_name_eng">
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="req" for="d_mother_occupation_eng">Occupation</label>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" value="" class="form-control parsley-validated"
                                                       name="d_mother_occupation_eng" id="d_mother_occupation_eng">
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="req" for="d_mother_phone"><?php echo $this->lang->line("mother_phone");?></label>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" name="d_mother_phone" id="d_mother_phone"
                                                       class="form-control"/>
                                            </div>
                                        </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_home"><?php echo $this->lang->line("mother_home");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_home" id="d_mother_home"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_street"><?php echo $this->lang->line("mother_street");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_street" id="d_mother_street"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_village"><?php echo $this->lang->line("mother_village");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_village" id="d_mother_village"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>
	                                    <div class="col-sm-12 form_sep">
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_commune"><?php echo $this->lang->line("mother_commune");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_commune" id="d_mother_commune"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_district"><?php echo $this->lang->line("mother_district");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_district" id="d_mother_district"
	                                                   class="form-control"/>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <label class="req" for="d_mother_province"><?php echo $this->lang->line("mother_province");?></label>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            <input type="text" name="d_mother_province" id="d_mother_province"
	                                                   class="form-control"/>
	                                        </div>
	                                    </div>
	                                </div>
                                    <!-- end form  -->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="dbtnclose">Close</button>
                <button type="button" class="btn btn-primary" id="dsavefamily">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- end new dialog  -->
<script type="text/javascript">

  function generateStudentUser(status) {
    var studentUser;

    if (status == "enter-id-card") {
      studentUser = $("#enter_id_card").val();
    }
    else {
      studentUser = $('#id_year').val() + '-' + $('#id_number').val();
    }

    $("#txtu_name").val(studentUser);
  }

    function LoadModule(){
        $.ajax({
            type : "POST",
            url  : "<?php echo site_url('setting/user/getModule')?>",
            dataType: 'json',
            async: false,
            data: {
                'roleid' : $("#cborole").val()
            },
            success:function(data){
                $('#moduleallow').html(data.op);
                $('#defpage').html(data.oppage);
            }
        });
    }//End Load Module

	$(document).on('change', '.stufiles', function() {
        var tr = $(this).parent().parent().parent();
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
            log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
                tr.find('.getfilename').val(log);
            } else {
                console.log(log);
            }
    });

      // We can watch for our custom `fileselect` event like this
    $(document).ready( function() {
        $('.stufiles').on('fileselect', function(event, numFiles, label) {
              var tr = $(this).parent().parent().parent();
              var input = tr.find('.getfilename'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;
              if( input.length ) {
                  input.val(log);
              } else {
                  console.log(log);
              }
          });
    });

    $(function () {
        // get...=====
        LoadModule();
        //clearPass();
        generateStudentUser("auto");

        $(document).on('change','#cborole',function(){
            LoadModule();
        });
        $(document).on('change','#moduleallow',function(){
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('setting/user/getPage')?>",
                data: {
                    'moduleid' : $("#moduleallow").val()
                },
                success:function(data){
                    $('#defpage').html(data);
                }
            });
        });

        var showPassword = false;

        $("#btnShowPassword").click( function() {
            if (showPassword) {
                $("#btnShowPassword span:first-child").removeClass("glyphicon-eye-open");
                $("#btnShowPassword span:first-child").addClass("glyphicon-eye-close");
                $("#txtpwd").attr("type", "password");
                showPassword = false;
            } else {
                $("#btnShowPassword span:first-child").removeClass("glyphicon-eye-close");
                $("#btnShowPassword span:first-child").addClass("glyphicon-eye-open");
                $("#txtpwd").attr("type", "text");
                showPassword = true;
            }
            $("#txtpwd").focus();
        });

        $('.year').val($('#year').val());

        $("#leave_school").click(function(){
            if($(this).is(":checked")){
                $(this).val(1);
            }else{
                $(this).val(0);
            }
        });
        $("#enter_id_card").keyup(function (e) {
            var enter_id = $(this).val();
            var card_id = enter_id.replace(/;/gi, "");
            var card_ids = card_id.replace(/\?/gi, "");
            $("#id_card").val(card_ids);

            generateStudentUser("enter-id-card");
        });

        $('#year').change(function () {
            $('.year').val($('#year').val());
        });

        $('#sch_program').change(function () {
            var programid = $(this).val();
            if (programid != "") {
                getsch_level(programid);
            } else {
                $('#sch_level').html("");
                $('#sch_year').html("");
                $('#sch_rangelevel').html("");
                $('#sch_class').html("");
            }
        });
        $('#sch_level').change(function () {
            var programid = $('#sch_program').val();
            var sch_levelid = $(this).val();
            if (sch_levelid != "") {
                getsch_year(programid, sch_levelid);
                getsch_rangelevel(programid, sch_levelid);
                getsch_class(sch_levelid);
            } else {
                $('#sch_year').html("");
            }
        });
        //function checkstudentid(event) {
        $('#id_year').change(function(e){
            checkstudentid(e);
        });

        $('#std_reg_submit').click(function(){
			var j= 0;
			var n= 0;
			$(".allstucture").each(function(j){
				n = j+1;
			});
			if(n>0){
				if(confirm("Do you want to save !")){
					$('#std_register').submit();
				}else{
					return false;
				}
			}else{
				toastr["warning"]("Please add program!");
				return false;
			}
		});
        $('#dob_home_no').change(function () {
            var myVal = $(this).val();
            $('#guard_home').val(myVal);
            $('#guard_home').val(myVal);
        });
        $('#dob_street').change(function () {
            var myVal = $(this).val();
            $('#street').val(myVal);
            $('#guard_street').val(myVal);
        });
        $('#dob_village').change(function () {
            var myVal = $(this).val();
            $('#village').val(myVal);
            $('#guard_village').val(myVal);
        });
        $('#dob_commune').change(function () {
            var myVal = $(this).val();
            $('#commune').val(myVal);
            $('#guard_commune').val(myVal);
        });
        $('#dob_district').change(function () {
            var myVal = $(this).val();
            $('#district').val(myVal);
            $('#guard_district').val(myVal);
        });
        $('#dob_province').change(function () {
            var myVal = $(this).val();
            $('#province').val(myVal);
            $('#guard_province').val(myVal);
        });
        $('#home_no').change(function () {
            var myVal = $(this).val();
            $('#guard_home').val(myVal);
        });
        $('#street').change(function () {
            var myVal = $(this).val();
            $('#guard_street').val(myVal);
        });
        $('#village').change(function () {
            var myVal = $(this).val();
            $('#guard_village').val(myVal);
        });
        $('#commune').change(function () {
            var myVal = $(this).val();
            $('#guard_commune').val(myVal);
        });
        $('#district').change(function () {
            var myVal = $(this).val();
            $('#guard_district').val(myVal);
        });
        $('#province').change(function () {
            var myVal = $(this).val();
            $('#guard_province').val(myVal);
        });
        $('#d_family_home').change(function () {
            var myVal = $(this).val();
            $('#d_father_home').val(myVal);
            $('#d_mother_home').val(myVal);
        });
        $('#d_family_street').change(function () {
            var myVal = $(this).val();
            $('#d_father_street').val(myVal);
            $('#d_mother_street').val(myVal);
        });
        $('#d_family_village').change(function () {
            var myVal = $(this).val();
            $('#d_father_village').val(myVal);
            $('#d_mother_village').val(myVal);
        });
        $('#d_family_commune').change(function () {
            var myVal = $(this).val();
            $('#d_father_commune').val(myVal);
            $('#d_mother_commune').val(myVal);
        });
        $('#d_family_district').change(function () {
            var myVal = $(this).val();
            $('#d_father_district').val(myVal);
            $('#d_mother_district').val(myVal);
        });
        $('#d_family_province').change(function () {
            var myVal = $(this).val();
            $('#d_father_province').val(myVal);
            $('#d_mother_province').val(myVal);
        });
        $("body").delegate("#add_family", "click", function () {
            clearDialog();
        });
        $("body").delegate("#dsavefamily", "click", function () {
            SaveNewFamily();
            clearDialog();
        });
        $("body").delegate("#btnclose", "click", function () {
            clearDialog();
        });
        $('#std_register').parsley();
        autofillfamily();
        $(".res_dob,.mem_dob,#leave_date,#register_date, #reqister, #dob, #d_mother_dob, #d_father_dob").datepicker({
            language: 'en',
            pick12HourFormat: true,
            format: 'dd-mm-yyyy'
        });
        $("body").delegate("#add_academic", "click", function () {
            var program = $("#sch_program").val();
            var sch_level = $("#sch_level").val();
            var sch_year = $("#sch_year").val();
            var sch_rangelevel = $("#sch_rangelevel").val();
            var sch_class = $("#sch_class").val();

            if (program == "") {
                toastr["warning"]("Please select program!");
            }else if(sch_level=="" || !sch_level){
				toastr["warning"]("Please select school level!");
            }else if(sch_year=="" || !sch_year ){
            	toastr["warning"]("Please select school year!");
            }else{
                addacademic();
            }
        });
        $("body").delegate(".link_del_time", "click", function () {
            $(this).closest("tr").remove();
        });

    });

    function isNumberKey(evt) {
        var e = window.event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;
        if ((charCode > 45 && charCode < 58) || charCode == 8) {
            return true;
        }
        return false;
    }
    function fillusername(event) {
        var f_name = $('#first_name').val();
        var l_name = $('#last_name').val();
        var username = f_name + '.' + l_name;
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/validateuser",
            data: {'username': username},
            type: "post",
            success: function (data) {
                if (data > 0) {
                    $('#login_username').val(username + '1');
                } else {
                    $('#login_username').val(username);
                }
            }
        });
    }

    function checkstudentid(event) {
        var id_year = $('#id_year').val();
        var id_number = $('#id_number').val();
        var student_num = id_year + '-' + id_number;
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/validatstudid",
            data: {'student_num': student_num},
            type: "post",
            success: function (data) {
                if (data > 0) {
                    toastr["warning"]("Student had already exist! Please try again");
                    $('#id_number').val("");
                    $('#id_number').focus();
                }
                else {
                  generateStudentUser("auto");
                }
            }
        });
    }

    function is_changeid(event) {
        if ($("#is_change").is(':checked')==true) {
            $("#is_change").val("1");
            $("#id_number").removeAttr("readonly");
        } else {
            $("#is_change").val("0");
            $("#id_number").attr("readonly","readonly");
        }
    }
    function is_leave(event) {
        if ($("#leave_school").is(':checked')==true) {
            $(this).val("1");
        } else {
            $(this).val("0");
        }
    }
    function previewstudent(event) {
        var year = $(event.target).attr('year');
        var yearid = $(event.target).attr('yearid');
        var student_id = jQuery(event.target).attr("rel");
        window.open("<?PHP echo site_url('student/student/preview');?>/" + student_id + "?yearid=" + yearid, "_blank");
    }
    function isvtc(event) {
        var classid = $(event.target).val();
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/getschlevel",
            data: {'classid': classid},
            type: "post",
            success: function (data) {
                if (data == 1) {
                    $('#promo_sep').removeClass('hide');
                    var familyid = $('#familyid').val();
                    if (familyid != '')
                        fillstudent(familyid);

                } else {
                    $('#promo_sep').addClass('hide');
                }
            }
        });
    }
    function fillpwd(event) {
        var pwd = $('#student_num').val();
        var student_num = $(event.target).val();
        //alert(student_num);
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/getstdbyid",
            data: {'std_num': student_num},
            type: "post",
            success: function (data) {
                if (data == 1) {
                    $('.error').html('Student ID :' + student_num + ' is already used please choose other ID');
                    $('#student_num').val('');
                } else {
                    $('#password').val(pwd);
                    $('.error').html('');
                }
            }
        });
    }


    function SaveNewFamily() {

        var family_id = $("#d_family_id").val();
        var family_name = $("#d_family_name").val();
        var family_book_no = $("#d_family_book_no").val();
        var family_province = $("#d_family_province").val();
        var family_district = $("#d_family_district").val();
        var family_commune = $("#d_family_commune").val();
        var family_village = $("#d_family_village").val();
        var family_street = $("#d_family_street").val();
        var family_home = $("#d_family_home").val();

        var father_name = $("#d_father_name").val();
        var father_name_eng = $("#d_father_name_eng").val();
        var father_occupation = $("#d_father_occupation").val();
        var father_occupation_eng = $("#d_father_occupation_eng").val();
        var father_dob = $("#d_father_dob").val();
        var father_province = $("#d_father_province").val();
        var father_district = $("#d_father_district").val();
        var father_commune = $("#d_father_commune").val();
        var father_village = $("#d_father_village").val();
        var father_street = $("#d_father_street").val();
        var father_phone = $("#d_father_phone").val();
        var father_home = $("#d_father_home").val();

        var mother_name = $("#d_mother_name").val();
        var mother_name_eng = $("#d_mother_name_eng").val();
        var mother_occupation = $("#d_mother_occupation").val();
        var mother_occupation_eng = $("#d_mother_occupation_eng").val();
        var mother_dob = $("#d_mother_dob").val();
        var mother_province = $("#d_mother_province").val();
        var mother_district = $("#d_mother_district").val();
        var mother_commune = $("#d_mother_commune").val();
        var mother_village = $("#d_mother_village").val();
        var mother_street = $("#d_mother_street").val();
        var mother_phone = $("#d_mother_phone").val();
        var mother_home = $("#d_mother_home").val();

        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/c_savenewfamily",
            data: {
                'family_id': family_id,
                'family_name': family_name,
                'family_book_no': family_book_no,
                'family_province': family_province,
                'family_district': family_district,
                'family_commune': family_commune,
                'family_village': family_village,
                'family_street': family_street,
                'family_home': family_home,

                'father_name': father_name,
                'father_name_eng': father_name_eng,
                'father_occupation': father_occupation,
                'father_occupation_eng': father_occupation_eng,
                'father_dob': father_dob,
                'father_province': father_province,
                'father_district': father_district,
                'father_commune': father_commune,
                'father_village': father_village,
                'father_street': father_street,
                'father_phone': father_phone,
                'father_home': father_home,

                'mother_name': mother_name,
                'mother_name_eng': mother_name_eng,
                'mother_occupation': mother_occupation,
                'mother_occupation_eng': mother_occupation_eng,
                'mother_dob': mother_dob,
                'mother_province': mother_province,
                'mother_district': mother_district,
                'mother_commune': mother_commune,
                'mother_village': mother_village,
                'mother_street': mother_street,
                'mother_phone': mother_phone,
                'mother_home': mother_home
            },
            type: "post",
            success: function (data) {
                var familyid = data;
                if (familyid != "") {
                    $('#familyid').val(familyid);
                    fillfamily(familyid);
                    $('#dbtnclose').click();
                } else {
                    toastr["warning"]("Data save not success ! Please try again");
                }
            }
        });
    }

    function getlistres(familyid) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/getresponstudent",
            data: {
                'familyid': familyid
            },
            type: "post",
            success: function (data) {
                $("#listrespon").html(data);
            }
        });
    }
    function fillstudentinf(event) {
        var memberid = $(event.target).val();
        var familyid = $('#familyid').val();
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/getmemberrow",
            data: {
                'r_id': memberid
            },
            type: "post",
            dataType: "json",
            async: false,
            success: function (data) {
                $('#last_name').val(data.last_name);
                $('#first_name').val(data.first_name);
                $('#last_name_kh').val(data.last_name_kh);
                $('#first_name_kh').val(data.first_name_kh);
                $('#dob').val(convertSQLDate(data.dob));
                fillusername();
                getlistmember(familyid, memberid);
            }
        });
    }
    function getlistmember(familyid, memberid) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/getmemberstudent",
            data: {
                'familyid': familyid,
                'memberid': memberid
            },
            type: "post",
            success: function (data) {
                //alert(data);
                $("#listmember").html(data);
                $('#liststdlink').html(getstdlink(familyid));
            }
        });

    }
    function getstdlink(familyid) {
        var count;
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/getstdlink",
            data: {'familyid': familyid, 'studentid': 0},
            async: false,
            type: "post",
            success: function (data) {
                count = data;
                //alert(data);
            }
        });
        return count;
    }

    function getsch_level(sch_program) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/cgetschlevel",
            data: {'sch_program': sch_program},
            async: false,
            type: "post",
            success: function (data) {
                $('#sch_level').html(data.schlevel);
                var sch_level = $('#sch_level').val();
                getsch_year(sch_program, sch_level);
                getsch_rangelevel(sch_program, sch_level);
                getsch_class(sch_level);
            }
        });

    }

    function getsch_year(sch_program, sch_level) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/cgetschoolyear",
            data: {'sch_program': sch_program, 'sch_level': sch_level},
            async: false,
            type: "post",
            success: function (data) {
                $('#sch_year').html(data.schyear);
            }
        });
    }
    function getsch_rangelevel(sch_program, sch_level) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/cgetschoolrangelevel",
            data: {'sch_program': sch_program, 'sch_level': sch_level},
            async: false,
            type: "post",
            success: function (data) {
                $('#sch_rangelevel').html(data.schrangelevel);
            }
        });
    }
    function getsch_class(sch_level) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/cgetschoolclass",
            data: {'sch_level': sch_level},
            async: false,
            type: "post",
            success: function (data) {
                $('#sch_class').html(data.schclass);
            }
        });
    }
    function addacademic() {
        this.sch_programid = $("#sch_program").val();
        this.sch_yearid    = $("#sch_year").val();
        this.sch_levelid   = $("#sch_level").val();
		this.sch_rangelevelid = $("#sch_rangelevel").val();
		this.sch_classid   = $("#sch_class").val();

        this.sch_program   = $("#sch_program option:selected").text();
        this.sch_level     = $("#sch_level option:selected").text();
        this.sch_year      = $("#sch_year option:selected").text();
        this.sch_rangelevel= $("#sch_rangelevel option:selected").text();
        this.sch_class     = $("#sch_class option:selected").text();

        var newprogram = sch_programid + "_" + sch_levelid + "_" + sch_yearid+ "_" + sch_rangelevelid + "_" + sch_classid;

        this.tr = '<tr>' +
            '<td>' +
                '<div id="file" class="form-group show_hide_file"><div class="input-group col-xs-12">' +
                '<label class="input-group-btn"><span class="btn btn-primary"> ' +
                'Browse&hellip; <input type="file" style="display: none;" id="files" value="" class="file stufiles" name="stufiles[]"></span></label>' +
                '<input type="text" class="form-control getfilename" id="getfilename" name="getfilename[]" value=""  readonly>' +
                '</div></div>' +
            '</td>' +
            '<td>' +
            '<input type="hidden" value="1" class="is_new"  id="is_new" name="is_new[]" >'+
            '<input type="hidden" value="" class="enrollid" name="enrollid[]" >'+
            '<input type="hidden" value="' + newprogram + '" class="allstucture" name="allstucture[]" >' +
            '<input type="hidden" class="sch_programid" name="sch_programid[]" value="' + this.sch_programid + '"/>' + this.sch_program +
            '</td>' +
            '<td><input type="hidden" class="sch_levelid" name="sch_levelid[]" value="' + this.sch_levelid + '"/>' + this.sch_level + '</td>' +
            '<td><input type="hidden" class="sch_yearid" name="sch_yearid[]" value="' + this.sch_yearid + '"/>' + this.sch_year + '</td>' +
            '<td><input type="hidden" class="sch_rangelevelid" name="sch_rangelevelid[]" value="' + this.sch_rangelevelid + '"/>' + this.sch_rangelevel + '</td>' +
            '<td><input type="hidden" class="sch_class" name="sch_classid[]" value="' + this.sch_classid + '"/>' + this.sch_class + '</td>' +
            '<td width="40" align="center"><a href="javascript:void(0)" class="link_del_time"><img src="<?php echo base_url("assets/images/icons/delete.png")?>" /> </a></td>' +
            '</tr>';

        if ($("#body_school_year tr").size() > 0) {
            //var newprogram = sch_programid + "_" + sch_yearid + "_" + sch_levelid;
            var arraprogram = [];
            $(".sch_program").each(function (i) {
                var curtr = $(this).closest("tr");
                arraprogram[i] = curtr.find(".sch_program").val() + "_" + curtr.find(".sch_level").val() + "_" + curtr.find(".sch_year").val() + "_" + curtr.find(".sch_rangelevel").val() + "_" + curtr.find(".sch_class").val();
            })
            if ($.inArray(newprogram, arraprogram) == '-1') {
                var db = 1;
                var i = 1;
                $(".allstucture").each(function (j) {
                    var val_each = $(this).val();
                    if (val_each == newprogram) {
                        db = i + 1;
                        i++;
                    }
                });
                if (db > 1) {
                    toastr["warning"]("Data already exist ! Please try again");
                } else {
                    $("#body_school_year").append(this.tr);
                }
            }
        } else {
            $("#body_school_year").append(this.tr);
        }
    }

    function fillstudent(familyid) {
        var classid = $('#classid').val();
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/getmemberbyfamily",
            data: {
                'familyid': familyid, 'classid': classid
            },
            type: "post",
            success: function (data) {
                $('#student').html(data);
            }
        });
    }

    function autofillfamily() {
        var fillrespon = "<?php echo site_url("student/student/c_fillfamily")?>";
        $(".family_id").autocomplete({
            source: fillrespon,
            minLength: 0,
            select: function (events, ui) {
                var f_id = ui.item.id;
                var family_code = ui.item.family_code;
                $("#familyid").val(f_id);
                $("#family_id").val(family_code);
                fillfamily(f_id);
            }
        });
    }

    function fillfamily(familyid) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/student/student/cgetfamilyrow",
            data: {
                'familyid': familyid
            },
            type: "post",
            success: function (data) {

                $("#familyid").val(data.familyid);
                $("#family_id").val(data.family_code);
                $("#family_name").val(data.family_name);
                $("#family_book_no").val(data.family_book_record_no);

                $("#father_name").val(data.father_name_kh);
                $("#father_name_eng").val(data.father_name);
                $("#father_occupation_eng").val(data.father_ocupation);
                $("#father_occupation").val(data.father_ocupation_kh);
                $("#father_dob").val(data.father_dob);
                $("#father_province").val(data.father_province);
                $("#father_district").val(data.father_district);
                $("#father_commune").val(data.father_commune);
                $("#father_village").val(data.father_village);
                $("#father_street").val(data.father_street);
                $("#father_phone").val(data.father_phone);
                $("#father_home").val(data.father_home);

                $("#mother_name").val(data.mother_name_kh);
                $("#mother_name_eng").val(data.mother_name);
                $("#mother_occupation").val(data.mother_ocupation_kh);
                $("#mother_occupation_eng").val(data.mother_ocupation);
                $("#mother_dob").val(data.mother_dob);
                $("#mother_province").val(data.mother_province);
                $("#mother_district").val(data.mother_district);
                $("#mother_commune").val(data.mother_commune);
                $("#mother_village").val(data.mother_village);
                $("#mother_street").val(data.mother_street);
                $("#mother_phone").val(data.mother_phone);
                $("#mother_home").val(data.mother_home);

                $("#guard_name").val(data.father_name_kh);
                $("#guard_name_eng").val(data.father_name);
                $("#guard_occupation").val(data.father_ocupation_kh);
                $("#guard_occupation_eng").val(data.father_ocupation);
                $("#guard_province").val(data.father_province);
                $("#guard_district").val(data.father_district);
                $("#guard_commune").val(data.father_commune);
                $("#guard_village").val(data.father_village);
                $("#guard_street").val(data.father_street);
                $("#guard_phone").val(data.father_phone);
                $("#guard_home").val(data.father_home);

            }
        });
    }

    function clearDialog() {
        $("#d_family_id").val("");
        $("#d_family_name").val("");
        $("#d_family_book_no").val("");
        $("#d_family_province").val("");
        $("#d_family_district").val("");
        $("#d_family_commune").val("");
        $("#d_family_village").val("");
        $("#d_family_street").val("");
        $("#d_family_home").val("");

        $("#d_father_name").val("");
        $("#d_father_occupation").val("");
        $("#d_father_dob").val("");
        $("#d_father_province").val("");
        $("#d_father_district").val("");
        $("#d_father_commune").val("");
        $("#d_father_village").val("");
        $("#d_father_street").val("");
        $("#d_father_phone").val("");
        $("#d_father_home").val("");

        $("#d_mother_name").val("");
        $("#d_mother_occupation").val("");
        $("#d_mother_dob").val("");
        $("#d_mother_province").val("");
        $("#d_mother_district").val("");
        $("#d_mother_commune").val("");
        $("#d_mother_village").val("");
        $("#d_mother_street").val("");
        $("#d_mother_phone").val("");
        $("#d_mother_home").val("");
    }
    function clearPass() {
        $("#txtu_name").val("");
        $("#txtpwd").val("");
    }
</script>
