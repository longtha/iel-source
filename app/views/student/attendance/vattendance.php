<div class="wrapper">
    <div class="clearfix" id="main_content_outer">

        <div id="main_content">

            <div class="result_info" style="padding-top: 0px">
                <div class="col-sm-6" style="padding-left: 5px !important;">
                    <span><i class="fa fa-caret-left" style="font-size: 18px;"></i></span>
                    <strong style="padding-left: 5px !important;">Student Attendance</strong>
                </div>
                <div class="col-sm-6" style="text-align: right">
                    <strong style="color: #00CC00"><?php echo (isset($_GET['res'])? ($_GET['res']==1?"Save success ! ":""):"") ?></strong>
                </div>
            </div>
            <?php
            $m = '';
            $p = '';
            if (isset($_GET['m'])) {
                $m = $_GET['m'];
            }
            if (isset($_GET['p'])) {
                $p = $_GET['p'];
            }
            ?>
            <form method="post" accept-charset="utf-8" id="fragelev"
                  action="<?php echo site_url('student/attendance/save?m=' . $m . '&p=' . $p . ''); ?>" class="gform">

                <div class="col-sm-3 col-md-3 ">
                    <div class="">
                       <input type="hidden" id="transno" name="transno" value="<?php echo ((isset($transno) && $transno!="")?$transno :"" ) ?>">
                        <input type="hidden" id="dailyattid" name="dailyattid" value="">

                        <label class="req" for="schoolid">School<span style="color:red">*</span></label>
                        <select class="form-control" id='schoolid' name='schoolid' min='1' required
                                data-parsley-required-message="Select any school">
                            <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                                <option
                                    value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="sclevelid">School Level<span style="color:red">*</span></label>
                      
                        <select class="form-control" id="sclevelid" required name="sclevelid">
                            <option value=""></option>
                            <?php if (isset($schevels) && count($schevels) > 0) {
                                foreach ($schevels as $sclev) {
                                    echo '<option value="' . $sclev->schlevelid . '" pro_id="'. $sclev->programid .'">' . $sclev->sch_level . '</option>';
                                }
                            } ?>
                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="yearid">Academic Year<span style="color:red">*</span></label>
                        <select class="form-control" id="yearid" required name="yearid">
                            <option value=""></option>
                            <?php if (isset($schyears) && count($schyears) > 0) {
                                foreach ($schyears as $schyear) {
                                    echo '<option value="' . $schyear->yearid . '" ' . ($this->session->userdata('year') == $schyear->yearid ? "selected=selected" : "") . ' >' . $schyear->sch_year . '</option>';
                                }
                            } ?>
                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="yearid">Select Class<span style="color:red">*</span></label>
                        <select class="form-control" id="classid" required name="classid">
                            <option value=""></option>
                            <?php if (isset($class) && count($class) > 0) {
                                foreach ($class as $cls) {
                                    echo '<option value="' . $cls->classid . '" >' . $cls->class_name . '</option>';
                                }
                            } ?>
                        </select>
                    </div>

                </div>


                <div class="col-sm-12" style="padding-bottom: 5px;padding-top: 5px">

                    <div class="">
                        <?php if ($this->green->gAction("R")) { ?>
                            <input type="button" name="btnshow" id='btnshow' value="Select"
                                   class="btn btn-primary" onclick="getStdtByCls()"/>
                        <?php } ?>
                    </div>


                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">                        
                        <label class="req" for="yearid">Base on</label>

                        <select class="form-control" id="baseattid" name="baseattid">
                            <?php if (isset($attbases) && count($attbases) > 0) {
                                foreach ($attbases as $attb) {
                                    echo '<option value="' . $attb->baseattid . '" >' . $attb->base_att . '</option>';
                                }
                            } ?>
                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 hiable_sub">
                    <div class="">
                        <label class="req" for="subject_grouop">Subject Group</label>
                        <select class="form-control subject_grouop" id="subject_grouop" name="subject_grouop">
                            <option value=""></option>

                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 hiable_sub">
                    <label for="full_att_score">Subject</label>
                    <select id="subjectid" name="subjectid" class="form-control subjectid">
                        <option value=""></option>

                    </select>
                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="termid">Term<span style="color:red">*</span></label>
                        <select class="form-control checksub" id="termid" required name="termid">
                            <option value=""></option>

                        </select>
                    </div>

                </div>

                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="weekid">Select Week<span style="color:red">*</span></label>
                        <select class="form-control checksub" required  id="weekid" name="weekid">
                            <option value=""></option>

                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">
                    <label class="req" for="att_date">Date<span style="color:red">*</span></label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="att_date" name="att_date" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                               data-parsley-required-message="Select Date" data-required="true" class="form-control"
                               type="text">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>

                </div>

                <div class="col-sm-3 col-md-3 hide">

                    <label class="req" for="full_att_score">Full Attendance Score<span
                            style="color:red">*</span></label>
                    <input id="full_att_score" name="full_att_score" class="form-control checksub" type="text"
                           value="1">

                </div>
                <div class="col-sm-12 col-md-12 " style="padding-bottom: 5px;">
                    <label class="req" for="weekid">Note:</label>

                    <div class="col-md-12">
                        <?php if (count($attnote) > 0) {
                            foreach ($attnote as $atnote) {
                                echo $atnote->symbol . ' : ' . $atnote->attnote . ", ";
                            }
                        } ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="dv-std" id="dv-std"></div>
                </div>
                <div class="col-sm-12">
                    <div class="">
                        <?php if ($this->green->gAction("C")) { ?>
                            <input type="submit" name="btnsave" id='btnsave' value="Save" class="btn btn-primary"  />
                        <?php } ?>
                    </div>
                </div>

            </form>
        </div>

    </div>
</div>

</div>

<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip()
    var transno="<?php echo $transno ?>";
    var programid="<?php echo $programid ?>";
    var getclassid="<?php echo $classid ?>"; 
    $( document ).ready(function() {
        if(transno!=""){
            $("#btnshow").hide();
            $("#btnsave").show();
            getAttEdit(transno,programid,getclassid);
        }else{
            $("#btnsave").hide();
        }
    });

    $(function () {
        
        $("#att_date").datepicker({
            language: 'en',
            pick12HourFormat: true,
            format: 'dd-mm-yyyy'
        });
        $("body").delegate("#btnshow", "click", function () {
            $("#btnsave").show();
        });
        $("body").delegate("#sclevelid", "change", function () {
            var programid = $("#sclevelid option:selected").attr("pro_id");
            getSchYear($(this).val());
            attbase($(this).val());
            getsubjectGroup(programid,$(this).val());
        });
        $("body").delegate("#subject_grouop", "change", function () {
            var programid = $("#sclevelid option:selected").attr("pro_id");
            getsubject(programid,$(this).val());
        });
        $("body").delegate("#yearid", "change", function () {
            var schlevelid = $("#sclevelid").val();
            getClass(schlevelid);
        });
        $("body").delegate("#classid", "change", function () {
            getStudyPeriod(1);
        });

        $("body").delegate("#termid", "change", function () {
            getweeks();
        });

        $("body").delegate(".score", "change", function () {

            var tr0 = $(this).closest("tr");
            var fullattscore = $("#full_att_score").val();

            if ($(this).val() - 0 > fullattscore) {
                $(this).val(0);
            } else {
                var total_score = calAttSco($(this));
                tr0.find(".total_att_score").val(number_format(total_score, 2));
            }
        });

        $("body").delegate(".checksub", "change", function () {
            checksub($(this));
        });

        $( "#subject" ).keyup(function(){
            var programid = $("#sclevelid option:selected").attr("pro_id");
            var grade_levelid = $("#classid option:selected").attr("grade_levelid");
            var sch_levelid = $("#sclevelid option:selected").val();
            var baseattid=0;
            baseattid=$("#baseattid").val()-0;
            if(baseattid==1) {
                //alert(programid);
                if (programid == 1){
                    autoSubject_kgp(grade_levelid);
                }else{
                    autoSubject_iep(sch_levelid);
                }
            }
        });

        $("body").delegate("#subject", "change", function () {
            if ($(this).val() == "") {
                $("#subjectid").val("");
            }
        });

        $('body').delegate(".radio_check","click",function(){
            $(this).closest('tr').find('.radio_check').not(this).prop('checked', false);
            $(this).closest('tr').find('.radio_check').not(this).val(0);
            $(this).closest('tr').find('.num').not(this).val(0);
            var classCheck = $(this).attr('class').split(" ").filter(getStyleClass);
            if(classCheck !="present_day"){
                $(this).closest('tr').find('.reason_note').removeAttr("readonly");
            }else{
                $(this).closest('tr').find('.reason_note').attr("readonly","readonly");
            }
            if($(this).prop("checked") == false) {
                $(this).prop("checked",true);
                $(this).val(1);
                $(this).siblings('.num').val(1);
            }else{
                $(this).val(1);
                $(this).siblings('.num').val(1);
            }
            
        });
    });
    function getStyleClass(value) {
        return value.indexOf("present_day") > -1;
    }
    function getSchYear(sclevelid) {

        $("#yearid").html("");
        if (sclevelid != "") {
            $.ajax({
                url: "<?php echo site_url('school/academic_year/get_year'); ?>",
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    schlevelid: sclevelid
                },
                success: function (res) {
                    var data = res.year;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {

                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.yearid + '">' + row.sch_year + '</option>';
                        }
                    }
                    $("#yearid").html(tr);
                }
            })
        }
    }
    function getClass(sclevelid) {

        $("#classid").html("");
        if (sclevelid != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getClass'); ?>",
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    schlevelid: sclevelid
                },
                success: function (res) {
                    var data = res.class;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.classid + '" grade_levelid="' + row.grade_levelid + '">' + row.class_name + '</option>';
                        }
                    }
                    $("#classid").html(tr);
                }
            })
        }
    }
    function getStudyPeriod(payment_type) {

        $("#termid").html("");
        if (payment_type != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getStudyPeriod'); ?>/" + payment_type,
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    schoolid: $("#schoolid").val(),
                    sclevelid: $("#sclevelid").val(),
                    payment_type: payment_type,
                    yearid: $("#yearid").val(),
                    classid: $("#classid").val()
                },
                success: function (res) {
                    var data = res.datas;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.term_sem_year_id + '">' + row.period + '</option>';
                        }
                    }
                    $("#termid").html(tr);
                }
            })
        }
    }
    function getweeks() {
        var termid = $("#termid").val();
        $("#weekid").html("");
        if (termid != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getweeks'); ?>",
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    termid: termid
                },
                success: function (res) {
                    var data = res.weeks;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.weekid + '">' + row.week + '</option>';
                        }
                    }
                    $("#weekid").html(tr);
                }
            })
        }
    }

    function calAttSco(evt) {
        var tr = evt.closest("tr");
        var atten = tr.find(".present_day").val() - 0;
        var permis = tr.find(".permission_day").val() - 0;
        var absen = tr.find(".absent_day").val() - 0;
        var late = tr.find(".lateday").val() - 0;
        return (atten + permis) - (absen + late + earkly + minusperm);
    }
    function attbase(schlevelid) {
        $("#baseattid").html("");
        var opattbs = '';
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getattbase'); ?>/" + schlevelid,
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (res) {
                    var data = res.attbases;
                    if (data.length > 0) {
                        for (var j = 0; j < data.length; j++) {
                            var attrow = data[j];
                            opattbs += '<option value="' + attrow.baseattid + '">' + attrow.base_att + '</option>';
                        }
                    }
                    opattbs += '</select>';
                }
            })
        }
        $("#baseattid").html(opattbs);
    }
    function checksub(evt) {
        var baseattid = $("#baseattid").val();
        if (baseattid == 1) {
            var subject = $("#subject").val();
            if (subject == "") {
                toastr["warning"]("Select Subject Please !");
                $("#subject").focus();
                evt.val("");
            }
        }
    }
    function getsubjectGroup(programid, sch_levelid){
        if(sch_levelid !=""){
            if(programid =="1"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/group_subjectkgp'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        sch_levelid: sch_levelid
                    },
                    success: function (res) {
                        var data = res.gs_kgp;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subj_type_id + '">' + row.subject_type + '</option>';
                            }
                        }
                        $("#subject_grouop").html(tr);
                    }
                })
            }
            if(programid =="2"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/group_subjectiep'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        sch_levelid: sch_levelid
                    },
                    success: function (res) {
                        var data = res.gs_iep;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subj_type_id + '">' + row.subject_type + '</option>';
                            }
                        }
                        $("#subject_grouop").html(tr);
                    }
                })
            }
            if(programid =="3"){
                $("#subject_grouop").html("");
                $("#subjectid").html("");

            }

        }else{
            toastr["warning"]("Select school level Please !");
        }
    }
    function getsubject(programid, subject_group){
        if(subject_group !=""){
            if(programid =="1"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/autosubjectkgp'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        subject_group: subject_group
                    },
                    success: function (res) {
                        var data = res.sub_kgp;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subjectid + '">' + row.subject + '</option>';
                            }
                        }
                        $("#subjectid").html(tr);
                    }
                })
            }
            if(programid =="2"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/autosubjectiep'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        subject_group: subject_group
                    },
                    success: function (res) {
                        var data = res.sub_iep;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subjectid + '">' + row.subject + '</option>';
                            }
                        }
                        $("#subjectid").html(tr);
                    }
                })
            }
        }else{
            toastr["warning"]("Select school level Please !");
        }
    }
    function getStdtByCls() {

        this.sclevelid = $("#sclevelid").val();
        this.yearid = $("#yearid").val();
        this.classid = $("#classid").val();
        this.termid = $("#termid").val();

        var tabstd = '<table class="table table-striped table-bordered table-hover">';

        var th = '<thead>';
            th += '<th>StudentID</th>';
            th += '<th>Student Name</th>';
            th += '<th>Phone</th>';
            th += '<th>Present</th>';
            th += '<th>Permission</th>';
            th += '<th>Absent</th>';
            th += '<th>Late</th>';
            th += '<th>Note</th>';
            th += '</thead><tbody id="tab-std-body"></tbody>';
        tabstd += th;
        tabstd += '</table>';
        $("#dv-std").html(tabstd);

        if (this.sclevelid != "" && this.yearid != "" && this.classid != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getstdbycls'); ?>",
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    sclevelid: this.sclevelid,
                    yearid: this.yearid,
                    classid: this.classid,
                    /*termid: this.termid*/
                },
                success: function (res) {
                    var data = res.stdbycls;
                    var attnote = res.attnote;
                    var op_attnote = '<select name="attnoteids[]" class="form-control attnoteid" style="min-width: 50px;padding: 0px;">';
                    if (attnote.length > 0) {
                        for (var j = 0; j < attnote.length; j++) {
                            var attnoterow = attnote[j];
                            op_attnote += '<option value="' + attnoterow.attnoteid + '">' + attnoterow.symbol + '</option>';
                        }
                    }
                    op_attnote += '</select>';

                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var trstd = '';
                            var strow = data[i];
                            trstd += '<tr><td>' + strow.student_num + '<input type="hidden" class="studentid" value="' + strow.studentid + '" name="studentids[]"/> </td>';
                            trstd += '<td attr_des_id="1">' + (strow.programid==1?strow.fullname_kh:strow.fullname) +'</td>';
                            trstd += '<td>' + strow.phone1 + '</td>';                            
                            trstd += '<td style="text-align: center !important;"><div class="form-group col-lg-4" style="padding: 0px;"><input type="checkbox" class="present_day radio_check" checked value="1" name="present_day[]"/><input type="hidden" class="present_days num" value="1" name="present_days[]"/></div></td>';
                            trstd += '<td style="text-align: center !important;"><div class="form-group col-lg-4" style="padding: 0px;"><input type="checkbox" class="permission_day radio_check" value="0"  name="permission_day[]"/><input type="hidden" class="permission_days num" value="0"  name="permission_days[]"/></div></td>';
                            trstd += '<td style="text-align: center !important;"><div class="form-group col-lg-4" style="padding: 0px;"><input type="checkbox" class="absent_day radio_check" value="0" name="absent_day[]"/><input type="hidden" class="absent_days num" value="0"  name="absent_days[]"/></div></td>';
                            trstd += '<td style="text-align: center !important;"><div class="form-group col-lg-4" style="padding: 0px;"><input type="checkbox" class="lateday radio_check"value="0" name="lateday[]"/><input type="hidden" class="latedays num" value="0"  name="latedays[]"/></div></td>';
                            trstd += '<td style="text-align: center !important;"><input type="text" readonly="readonly" class="reason_note form-control" value="" name="reason_note[]"/></td>';
                            $("#tab-std-body").append(trstd);
                        }
                    }
                }
            })
        }
    }
    function getAttEdit(transno,programid,classid){
        $.ajax({
            url: "<?php echo site_url('student/attendance/edit_attendance'); ?>/" + transno+'/'+programid+'/'+classid,
            type: "POST",
            dataType: 'json',
            async: false,
            success: function (result) {
                var attdaily=result.attdailyrow;
                var attdailydet=result.attdailydetrow;
               if(attdaily.length>0){

                   var tabstd = '<table class="table table-striped table-bordered table-hover">';

                   var th = '<thead>';
                       th += '<th>StudentID</th>';
                       th += '<th>Student Name</th>';
                       th += '<th class="no_wrap">Phone</th>';
                       th += '<th>Present</th>';
                       th += '<th>Permission</th>';
                       th += '<th>Absent</th>';
                       th += '<th>Late</th>';
                       th += '<th>Note</th>';
                       th += '</thead><tbody id="tab-std-body"></tbody>';
                   tabstd += th;
                   tabstd += '</table>';
                   $("#dv-std").html(tabstd);
                   $("#dailyattid").val(attdaily[0].dailyattid);

                   $("#schoolid").val(attdaily[0].schoolid);
                   $("#sclevelid").val(attdaily[0].schlevelid);
                   getSchYear(attdaily[0].schlevelid);
                   attbase(attdaily[0].schlevelid);
                   $("#yearid").val(attdaily[0].yearid);
                   $("#baseattid").val(attdaily[0].baseattid);

                   getClass(attdaily[0].schlevelid);
                   $("#classid").val(attdaily[0].classid);
                   getStudyPeriod(1);
                   $("#termid").val(attdaily[0].termid);
                   getweeks();
                   $("#weekid").val(attdaily[0].weekid);
                   $("#subjectid").val(attdaily[0].subjectid);
                   $("#subject").val(attdaily[0].subject);
                   $("#att_date").val(convertSQLDate(attdaily[0].date));
                   if(attdailydet.length>0){
                       var attnote = result.attnote;
                        var trstd = '';
                       for (var i = 0; i < attdailydet.length; i++) {
                           var strow = attdailydet[i];
                           var at_checked ="";
                           var p_checked ="";
                           var a_checked ="";
                           var l_checked ="";
                           var present_day = strow.present_day -0;
                           var permission_day = strow.permission_day -0;
                           var absent_day = strow.absent_day -0;
                           var lateday = strow.lateday -0;
                           var note = strow.note;
                           var programid = strow.programid;

                           if(present_day =="1"){
                               at_checked ="checked";
                           }
                           if(permission_day =="1"){
                               p_checked ="checked";
                           }
                           if(absent_day =="1"){
                               a_checked ="checked";
                           }
                           if(lateday =="1"){
                               l_checked ="checked";
                           }
                           trstd += '<tr><td>' + strow.student_num + '<input type="hidden" class="studentid" value="' + strow.studentid + '" name="studentids[]"/> </td>';
                           trstd += '<td>' + (programid==1?strow.fullname_kh:strow.fullname) + '</td>';
                           trstd += '<td>' + strow.phone1 + ' </td>';
                           trstd += '<td><div class="form-group col-lg-4" style="text-align: center !important;"><input type="checkbox" class="present_day radio_check" '+ at_checked +' value="' + present_day+ '" name="present_day[]"/><input type="hidden" class="present_days num" value="' + present_day+ '" name="present_days[]"/></div></td>';
                           trstd += '<td><div class="form-group col-lg-4" style="text-align: center !important;"><input type="checkbox" style="padding: 2px;" class="permission_day radio_check" '+ p_checked +'  value="' + permission_day+ '" name="permission_day[]"/><input type="hidden" class="permission_days num" value="' + permission_day+ '" name="permission_days[]"/></div></td>';
                           trstd += '<td><div class="form-group col-lg-4" style="text-align: center !important;"><input type="checkbox" style="padding: 2px;" class="absent_day radio_check" '+ a_checked +'  value="' + absent_day+ '" name="absent_day[]"/><input type="hidden" class="absent_days num" value="' + absent_day+ '" name="absent_days[]"/></div></td>';
                           trstd += '<td><div class="form-group col-lg-4" style="text-align: center !important;"><input type="checkbox" style="padding: 2px;" class="lateday radio_check" '+ l_checked +'  value="' + lateday + '" name="lateday[]"/><input type="hidden" class="latedays num" value="' + lateday+ '" name="latedays[]"/></div></td>';
                           trstd += '<td><input type="text" style="padding: 2px;" class="reason_note form-control" value="' + note+ '" name="reason_note[]"/></td></tr>';
                       }
                       $("#tab-std-body").append(trstd);
                   }
               }

            }
        })
    }

</script>