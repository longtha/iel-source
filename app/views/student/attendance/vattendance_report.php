<div class="wrapper">
    <div class="clearfix" id="main_content_outer">

        <div id="main_content">
            <div class="result_info" style="padding-top: 0px">
                <div class="col-sm-6">
                    <strong>Student Attendance</strong>
                </div>
                <div class="col-sm-6" style="text-align: right">
                    <a id="print" title="Print" href="javascript:void(0)">
                        <img src="<?php echo base_url('assets/images/icons/print.png')?>"/>
                    </a>
                </div>
            </div>
            <?php
            $m = '';
            $p = '';
            if (isset($_GET['m'])) {
                $m = $_GET['m'];
            }
            if (isset($_GET['p'])) {
                $p = $_GET['p'];
            }
            ?>

            <form method="post" accept-charset="utf-8" id="fragelev"
                  action="<?php echo site_url('student/attendance/save?m=' . $m . '&p=' . $p . ''); ?>" class="gform">

                <div class="col-sm-3 col-md-3 ">
                    <div class="">
                        <label class="req" for="schoolid">School</label>
                        <select class="form-control" id='schoolid' name='schoolid' min='1' required
                                data-parsley-required-message="Select any school">
                            <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                                <option
                                    value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="sclevelid">School Level</label>
                        <select class="form-control" id="sclevelid" name="sclevelid">
                            <option value=""></option>
                            <?php if (isset($schevels) && count($schevels) > 0) {
                                foreach ($schevels as $sclev) {
                                    echo '<option value="' . $sclev->schlevelid . '"  pro_id="'. $sclev->programid .'">' . $sclev->sch_level . '</option>';
                                }
                            } ?>
                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="yearid">Academic Year</label>
                        <select class="form-control" id="yearid" name="yearid">
                            <option value=""></option>
                            <?php if (isset($schyears) && count($schyears) > 0) {
                                foreach ($schyears as $schyear) {
                                    echo '<option value="' . $schyear->yearid . '" ' . ($this->session->userdata('year') == $schyear->yearid ? "selected=selected" : "") . ' >' . $schyear->sch_year . '</option>';
                                }
                            } ?>
                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="yearid">Select Class</label>
                        <select class="form-control" id="classid" name="classid">
                            <option value=""></option>
                            <?php if (isset($class) && count($class) > 0) {
                                foreach ($class as $cls) {
                                    echo '<option value="' . $cls->classid . '" >' . $cls->class_name . '</option>';
                                }
                            } ?>
                        </select>
                    </div>

                </div>

                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="yearid">Base on</label>
                        <select class="form-control" id="baseattid" name="baseattid">
                            <?php if (isset($attbases) && count($attbases) > 0) {
                                foreach ($attbases as $attb) {
                                    echo '<option value="' . $attb->baseattid . '" >' . $attb->base_att . '</option>';
                                }
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 hiable_sub">
                    <div class="">
                        <label class="req" for="subject_grouop">Subject Group</label>
                        <select class="form-control subject_grouop" id="subject_grouop" name="subject_grouop">
                            <option value=""></option>

                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 hiable_sub">
                    <label for="full_att_score">Subject</label>
                    <select id="subjectid" name="subjectid" class="form-control subjectid">
                        <option value=""></option>
                    </select>
                </div>
                <div class="col-sm-3 col-md-3 ">
                    <div class="">
                        <label class="req" for="termid">Term</label>
                        <select class="form-control checksub" id="termid" required name="termid">
                            <option value=""></option>

                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="weekid">Select Week</label>
                        <select class="form-control checksub" id="weekid" name="weekid">
                            <option value=""></option>

                        </select>
                    </div>

                </div>

                <div class="col-sm-3 col-md-3 ">

                    <label class="req" for="from_date">From Date</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="from_date" name="from_date" value="<?php echo date("01-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <label class="req" for="to_date">To Date</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="to_date" name="to_date" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>

                </div>
                <div class="col-sm-12 col-md-12 " style="padding-bottom: 5px;">
                    <label class="req" for="weekid">Note:</label>

                    <div class="col-md-12">
                        <?php if (isset($attnote) && count($attnote) > 0) {
                            foreach ($attnote as $atnote) {
                                echo $atnote->symbol . ' : ' . $atnote->attnote . ", ";
                            }
                        } ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="dv-std" id="dv-std"></div>
                </div>
                <div class="col-sm-12" style="padding-bottom: 5px;padding-top: 5px">
                    <div class="hide">
                        <?php if ($this->green->gAction("C")) { ?>
                            <input type="submit" name="btnsave" id='btnsave' value="Save"
                                   class="btn btn-primary"/>
                        <?php } ?>
                    </div>
                    <div class="">
                        <?php if ($this->green->gAction("R")) { ?>
                            <input type="button" name="btnshow" id='btnshow' value="Show" class="btn btn-primary"/>
                        <?php } ?>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
<style type="text/css">
    .tab_head th{
        background-color: #098ddf;
        color: #ffffff;
    }
</style>
<div class="col-sm-12">
    <div class="panel panel-default">

        <div class="panel-body table-responsive" id="tab_print">
            <table class="table table-striped " >
                <tr>
                    <td>Program</td><td>: <label class="p_program"></label></td>
                    <td>School Level</td><td>: <label class="p_schlevel"></label></td>
                    <td>Academic Year</td><td>: <label class="p_year"></label></td>
                    <td>Class</td><td>: <label class="p_class"></label></td>
                    <td>Term</td><td>: <label class="p_term"></label></td>
                </tr>
                <tr>
                    <td>Subject</td><td>: <label class="p_subject"></label></td>
                    <td>Week</td><td>: <label class="p_week"></label></td>
                    <td>From</td><td>: <label class="p_from"></label></td>
                    <td>To</td><td>: <label class="p_to"></label></td>

                </tr>
            </table>
            <table border="0" ? align="center" id='listsubject' class="table table-striped table-bordered table-hover">
                <thead id="tab_head" class="tab_head">
                </thead>

                <tbody id='bodylist'>

                </tbody>
                <!---- Start pagination ---->
                <tfoot>
                <tr>
                    <td colspan='13' id='pgt'>
                        <ul class='pagination'>

                        </ul>
                    </td>
                </tr>
                </tfoot>
                <!---- End Pagination ---->
            </table>
            <table class="table table-striped " >
                <tr>
                    <td></td><td></td>
                    <td></td><td></td>
                    <td></td><td></td>
                    <td></td><td></td>
                    <td align="right">Total Permission</td><td>:<span class="show_p"></span></td>
                </tr>
                <tr>
                    <td></td><td></td>
                    <td></td><td></td>
                    <td></td><td></td>
                    <td></td><td></td>
                    <td align="right">Total Absen</td><td>:<span class="show_a"></span></td>
                </tr>
                <tr>
                    <td></td><td></td>
                    <td></td><td></td>
                    <td></td><td></td>
                    <td></td><td></td>
                    <td align="right">Total Late</td><td>:<span class="show_l"></span></td>
                </tr>
            </table>
        </div>
    </div>
</div>

</div>

<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip()
   // search();

    function search(startpage) {

        var schoolid = $('#schoolid').val();
        var sclevelid= $('#sclevelid').val();
        var yearid= $('#yearid').val();
        var classid= $('#classid').val();
        var baseattid= $('#baseattid').val();
        var termid= $('#termid').val();
        var weekid= $('#weekid').val();
        var from_date= $('#from_date').val();
        var to_date= $('#to_date').val();
        var subjectid= $('#subjectid').val();

        var roleid =<?php echo $this->session->userdata('roleid');?>;
        var m =<?php echo $m;?>;
        var p =<?php echo $p;?>;
        
        $.ajax({
            url: "<?php echo site_url('student/attendance_report/search_detail'); ?>",
            data: {
                'schoolid': schoolid,
                'roleid': roleid,
                'm': m,
                'p': p,
                'startpage': startpage,
                sclevelid: sclevelid,
                yearid: yearid,
                classid: classid,
                baseattid: baseattid,
                termid: termid,
                weekid: weekid,
                from_date: from_date,
                to_date: to_date,
                subjectid:subjectid
            },
            type: "POST",
            success: function (res) {
                $('#tab_head').html(res.data_h);
                $('#bodylist').html(res.data_d);
                $('.show_p').html(res.total_p);
                $('.show_a').html(res.total_a);
                $('.show_l').html(res.total_l);
            },
            beforeSend: function() { $('.proccess_loading').show(); },
            complete: function() { $('.proccess_loading').hide();}
        });
    }
    function getSchYear(sclevelid) {

        $("#yearid").html("");
        if (sclevelid != "") {
            $.ajax({
                url: "<?php echo site_url('school/academic_year/get_year'); ?>",
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    schlevelid: sclevelid
                },
                success: function (res) {
                    var data = res.year;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {

                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.yearid + '">' + row.sch_year + '</option>';
                        }
                    }
                    $("#yearid").html(tr);
                }
            })
        }
    }
    function getClass(sclevelid) {

        $("#classid").html("");
        if (sclevelid != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getClass'); ?>",
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    schlevelid: sclevelid
                },
                success: function (res) {
                    var data = res.class;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.classid + '" grade_levelid="' + row.grade_levelid + '">' + row.class_name + '</option>';
                        }
                    }
                    $("#classid").html(tr);
                }
            })
        }
    }
    function getStudyPeriod(payment_type) {

        $("#termid").html("");
        if (payment_type != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance_report/getStudyPeriod'); ?>/" + payment_type,
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    schoolid: $("#schoolid").val(),
                    sclevelid: $("#sclevelid").val(),
                    payment_type: payment_type,
                    yearid: $("#yearid").val(),
                    classid: $("#classid").val()
                },
                success: function (res) {
                    var data = res.datas;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.term_sem_year_id + '">' + row.period + '</option>';
                        }
                    }
                    $("#termid").html(tr);
                }
            })
        }
    }
    function getweeks() {
        var termid = $("#termid").val();
        $("#weekid").html("");
        if (termid != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getweeks'); ?>",
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    termid: termid
                },
                success: function (res) {
                    var data = res.weeks;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.weekid + '">' + row.week + '</option>';
                        }
                    }
                    $("#weekid").html(tr);
                }
            })
        }
    }
    function attbase(schlevelid) {
        $("#baseattid").html("");
        var opattbs = '';
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getattbase'); ?>/" + schlevelid,
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (res) {
                    var data = res.attbases;
                    if (data.length > 0) {
                        for (var j = 0; j < data.length; j++) {
                            var attrow = data[j];
                            opattbs += '<option value="' + attrow.baseattid + '">' + attrow.base_att + '</option>';
                        }
                    }
                    opattbs += '</select>';

                }
            })
        }
        $("#baseattid").html(opattbs);
    }
    function getsubjectGroup(programid, sch_levelid){
        if(sch_levelid !=""){
            if(programid =="1"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/group_subjectkgp'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        sch_levelid: sch_levelid
                    },
                    success: function (res) {
                        var data = res.gs_kgp;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subj_type_id + '">' + row.subject_type + '</option>';
                            }
                        }
                        $("#subject_grouop").html(tr);
                    }
                })
            }
            if(programid =="2"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/group_subjectiep'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        sch_levelid: sch_levelid
                    },
                    success: function (res) {
                        var data = res.gs_iep;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subj_type_id + '">' + row.subject_type + '</option>';
                            }
                        }
                        $("#subject_grouop").html(tr);
                    }
                })
            }
            if(programid =="3"){
                $("#subject_grouop").html("");
                $("#subjectid").html("");
    
            }
    
        }else{
            toastr["warning"]("Select school level Please !");
        }
    }
    function getsubject(programid, subject_group){
        if(subject_group !=""){
            if(programid =="1"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/autosubjectkgp'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        subject_group: subject_group
                    },
                    success: function (res) {
                        var data = res.sub_kgp;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subjectid + '">' + row.subject + '</option>';
                            }
                        }
                        $("#subjectid").html(tr);
                    }
                })
            }
            if(programid =="2"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/autosubjectiep'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        subject_group: subject_group
                    },
                    success: function (res) {
                        var data = res.sub_iep;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subjectid + '">' + row.subject + '</option>';
                            }
                        }
                        $("#subjectid").html(tr);
                    }
                })
            }
        }
    }


    $(function () {
        $("#from_date,#to_date").datepicker({
            language: 'en',
            pick12HourFormat: true,
            format: 'dd-mm-yyyy'
        });
        $("body").delegate(".pagenav", "click", function () {
            var page = $(this).attr("id");
            search(page);
        })

        $("body").delegate("#btnshow", "click", function () {

            var schlevelid = $("#sclevelid option:selected").val();
            var yearid = $("#yearid option:selected").val();
            var classid = $("#classid option:selected").val();

            var school = $("#schoolid option:selected").html();
            var p_schlevel = $("#sclevelid option:selected").html();
            var p_year = $("#yearid option:selected").html();
            var p_term = $("#termid option:selected").html();
            var p_week = $("#weekid option:selected").html();
            var p_class = $("#classid option:selected").html();
            var subject_grouop = $("#subject_grouop option:selected").html();
            var p_subject = $("#subjectid option:selected").html();
            var p_from = $("#from_date").val();
            var p_to = $("#to_date").val();

            if(schlevelid !="" && yearid !="" && classid !=""){
                search();

                $(".p_program").html();
                $(".p_schlevel").html(p_schlevel);
                $(".p_year").html(p_year);
                $(".p_class").html(p_class);
                $(".p_term").html(p_term);
                $(".p_subject").html(p_subject);
                $(".p_week").html(p_week);
                $(".p_from").html(p_from);
                $(".p_to").html(p_to);

            }else{
                toastr["warning"]("Select class Please !");
            }

        });
        $("body").delegate("#sclevelid", "change", function () {
            var programid = $("#sclevelid option:selected").attr("pro_id");
            if($(this).val() !=""){
                getSchYear($(this).val());
                attbase($(this).val());
                getsubjectGroup(programid,$(this).val());
            }else{
                $("#yearid").val("");
                $("#subject_grouop").val("");
                $("#subjectid").val("");
            }
        });
        $("body").delegate("#yearid", "change", function () {
            var schlevelid = $("#sclevelid").val();
            getClass(schlevelid);
        });
        $("body").delegate("#classid", "change", function () {
            getStudyPeriod(1);
        });
        $("body").delegate("#termid", "change", function () {
            getweeks();
        });
        
        $("body").delegate("#subject_grouop", "change", function () {
            var programid = $("#sclevelid option:selected").attr("pro_id");
            if($(this).val() !=""){
                getsubject(programid,$(this).val());
            }else{
                $("#subjectid").val("");
            }
        });

        $("body").delegate(".link_delete", "click", function () {
            if (window.confirm("Sure! Do you want to delete? ")) {
                var transno = $(this).attr("rel");
                $.ajax({
                    url: "<?php echo site_url('student/attendance/delete'); ?>/" + transno,
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    success: function (res) {
                        toastr["success"](res.del);
                        search(0);
                    }
                })
            }
        });
        $("#print").on("click", function () {
            gPrint("tab_print", "Evaluation");
        });

    })
</script>