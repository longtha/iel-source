<div class="wrapper">
    <div class="clearfix" id="main_content_outer">

        <div id="main_content">

            <div class="result_info" style="padding-top: 0px">
                <div class="col-sm-6">
                    <strong>Student Attendance</strong>
                </div>
                <div class="col-sm-6" style="text-align: right">

                </div>
            </div>
            <?php
            $m = '';
            $p = '';
            if (isset($_GET['m'])) {
                $m = $_GET['m'];
            }
            if (isset($_GET['p'])) {
                $p = $_GET['p'];
            }
            ?>

            <form method="post" accept-charset="utf-8" id="fragelev"
                  action="<?php echo site_url('student/attendance/save?m=' . $m . '&p=' . $p . ''); ?>" class="gform">

                <div class="col-sm-3 col-md-3 ">
                    <div class="">
                        <label class="req" for="schoolid">School</label>
                        <select class="form-control" id='schoolid' name='schoolid' min='1' required
                                data-parsley-required-message="Select any school">
                            <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                                <option
                                    value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="sclevelid">School Level</label>
                        <select class="form-control" id="sclevelid" name="sclevelid">
                            <option value=""></option>
                            <?php if (isset($schevels) && count($schevels) > 0) {
                                foreach ($schevels as $sclev) {
                                    echo '<option value="' . $sclev->schlevelid . '"  pro_id="'. $sclev->programid .'">' . $sclev->sch_level . '</option>';
                                }
                            } ?>
                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="yearid">Academic Year</label>
                        <select class="form-control" id="yearid" name="yearid">
                            <option value=""></option>
                            <?php if (isset($schyears) && count($schyears) > 0) {
                                foreach ($schyears as $schyear) {
                                    echo '<option value="' . $schyear->yearid . '" ' . ($this->session->userdata('year') == $schyear->yearid ? "selected=selected" : "") . ' >' . $schyear->sch_year . '</option>';
                                }
                            } ?>
                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="yearid">Select Class</label>
                        <select class="form-control" id="classid" name="classid">
                            <option value=""></option>
                            <?php if (isset($class) && count($class) > 0) {
                                foreach ($class as $cls) {
                                    echo '<option value="' . $cls->classid . '" >' . $cls->class_name . '</option>';
                                }
                            } ?>
                        </select>
                    </div>

                </div>

                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="yearid">Base on</label>
                        <select class="form-control" id="baseattid" name="baseattid">
                            <?php if (isset($attbases) && count($attbases) > 0) {
                                foreach ($attbases as $attb) {
                                    echo '<option value="' . $attb->baseattid . '" >' . $attb->base_att . '</option>';
                                }
                            } ?>
                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 hiable_sub">
                    <div class="">
                        <label class="req" for="subject_grouop">Subject Group</label>
                        <select class="form-control subject_grouop" id="subject_grouop" name="subject_grouop">
                            <option value=""></option>

                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 hiable_sub">
                    <label for="full_att_score">Subject</label>
                    <select id="subjectid" name="subjectid" class="form-control subjectid">
                        <option value=""></option>

                    </select>
                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="termid">Term</label>
                        <select class="form-control checksub" id="termid" required name="termid">
                            <option value=""></option>

                        </select>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <div class="">
                        <label class="req" for="weekid">Select Week</label>
                        <select class="form-control checksub" id="weekid" name="weekid">
                            <option value=""></option>

                        </select>
                    </div>

                </div>

                <div class="col-sm-3 col-md-3 ">

                    <label class="req" for="from_date">From Date</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="from_date" name="from_date" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <label class="req" for="to_date">To Date</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="to_date" name="to_date" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>

                </div>
                <div class="col-sm-12 col-md-12 " style="padding-bottom: 5px;">
                    <label class="req" for="weekid">Note:</label>

                    <div class="col-md-12">
                        <?php if (isset($attnote) && count($attnote) > 0) {
                            foreach ($attnote as $atnote) {
                                echo $atnote->symbol . ' : ' . $atnote->attnote . ", ";
                            }
                        } ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="dv-std" id="dv-std"></div>
                </div>
                <div class="col-sm-12" style="padding-bottom: 5px;padding-top: 5px">
                    <div class="hide">
                        <?php if ($this->green->gAction("C")) { ?>
                            <input type="submit" name="btnsave" id='btnsave' value="Save"
                                   class="btn btn-primary"/>
                        <?php } ?>
                    </div>
                    <div class="">
                        <?php if ($this->green->gAction("R")) { ?>
                            <input type="button" name="btnshow" id='btnshow' value="Show" class="btn btn-primary"/>
                        <?php } ?>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
<style type="text/css">
    .tab_head th{
        background-color: #098ddf;
        color: #ffffff;
    }
</style>
<div class="col-sm-12">
    <div class="panel panel-default">

        <div class="panel-body table-responsive">
            <table border="0" ? align="center" id='listsubject' class="table table-striped table-bordered table-hover">
                <thead class="tab_head">
                <th>Program</th>
                <th>School Level</th>
                <th>Academic Year</th>
                <th>Term</th>
                <th>Week</th>
                <th>Date</th>
                <th>Class</th>
                <th>Subject</th>
                <th>Checked By</th>
                <th>Checked Date</th>
                <th colspan="3"><?php if ($this->green->gAction("C")) { ?>
                        <a href="<?php echo site_url("student/attendance/add?m=$m&p=$p") ?>" target="_blank">
                            <img src="<?php echo base_url('assets/images/icons/add.png') ?>"/>
                        </a>
                    <?php } ?> </th>
                </thead>

                <tbody id='bodylist'>


                </tbody>
                <!---- Start pagination ---->
                <tfoot>
                <tr>
                    <td colspan='13' id='pgt'>
                        <ul class='pagination'>

                        </ul>
                    </td>
                </tr>
                </tfoot>
                <!---- End Pagination ---->
            </table>

        </div>
    </div>
</div>

</div>

<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip()
    search();

    function search(startpage) {

        var schoolid = $('#schoolid').val();
        var sclevelid= $('#sclevelid').val();
        var yearid= $('#yearid').val();
        var classid= $('#classid').val();
        var baseattid= $('#baseattid').val();
        var termid= $('#termid').val();
        var weekid= $('#weekid').val();
        var from_date= $('#from_date').val();
        var to_date= $('#to_date').val();
        var subjectid= $('#subjectid').val();
        var subject_grouop= $('#subject_grouop').val();

        var roleid =<?php echo $this->session->userdata('roleid');?>;
        var m =<?php echo $m;?>;
        var p =<?php echo $p;?>;

        $.ajax({
            url: "<?php echo site_url('student/attendance/search'); ?>",
            data: {
                'schoolid': schoolid,
                'roleid': roleid,
                'm': m,
                'p': p,
                'startpage': startpage,
                sclevelid: sclevelid,
                yearid: yearid,
                classid: classid,
                baseattid: baseattid,
                termid: termid,
                weekid: weekid,
                from_date: from_date,
                to_date: to_date,
                subjectid:subjectid,
                subject_grouop:subject_grouop
            },
            type: "POST",
            success: function (res) {

                var data = res.datas;
                var paging = res.paging;
                var tr = "";
                if (data.length > 0) {

                    var arrprogram = [];
                    var arrlevel = [];
                    var arryearid = [];

                    for (var i = 0; i < data.length; i++) {
                        var row = data[i];

                        var program = "";
                        var sch_level = "";
                        var schyear = "";
                        var subject="";

                        if ($.inArray(row.programid, arrprogram) == '-1') {
                            program = row.program;
                        }
                        if ($.inArray(row.schlevelid, arrlevel) == '-1') {
                            sch_level = row.sch_level;
                        }
                        if ($.inArray(row.yearid, arryearid) == '-1') {
                            schyear = row.sch_year;
                        }

                        if(row.baseattid==1){
                            subject =row.subjectid;
                        }

                        tr += '<tr>' +
                            '<td>' + program + '</td>' +
                            '<td>' + sch_level + '</td>' +
                            '<td>' + schyear + '</td>' +
                            '<td>' + row.term + '</td>' +
                            '<td>' + row.week + '</td>' +
                            '<td>' + convertSQLDate(row.date) + '</td>' +
                            '<td>' + row.class_name + '</td>' +
                            '<td>' + subject + '</td>' +
                            '<td>' + row.created_by + '</td>' +
                            '<td>' + row.created_date + '</td>' +
                            '<td width="40">' +
                            '<a href="<?php echo site_url("student/attendance/add") ?>/' + row.transno + '/'+row.programid+'/'+row.classid+'?m=<?php echo $m?>&p=<?php echo $p?>" class="link_edit" rel="' + row.transno + '">' +
                            '<img src="<?php echo base_url('assets/images/icons/edit.png') ?>"></a></td>' +
                            '<td width="40"><a href="javascript:void(0)" class="link_delete" rel="' + row.transno + '">' +
                            '<img src="<?php echo base_url('assets/images/icons/delete.png') ?>"></a></td>' +
                            '<td width="40"><a href="<?php echo site_url("student/attendance/preview") ?>/' + row.transno + '/'+row.programid+'/'+row.yearid+'/'+row.classid+'?m=<?php echo $m?>&p=<?php echo $p?>" class="link_preview"  target="_blank">' +
                            '<img width="18" height="18" src="<?php echo base_url('assets/images/icons/preview.png') ?>"></a></td>' +
                            '</tr>';
                        arrprogram[i] = row.programid;
                        arrlevel[i] = row.schlevelid;
                        arryearid[i] = row.yearid;
                    }
                }else{
                    tr+='<tr><td colspan="13" align="center">Nothing record to display here..!</td></tr>';
                }
                $('#bodylist').html(tr);
                $('.pagination').html(paging.pagination);
            }
        });
    }
    function getSchYear(sclevelid) {

        $("#yearid").html("");
        if (sclevelid != "") {
            $.ajax({
                url: "<?php echo site_url('school/academic_year/get_year'); ?>",
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    schlevelid: sclevelid
                },
                success: function (res) {
                    var data = res.year;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {

                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.yearid + '">' + row.sch_year + '</option>';
                        }
                    }
                    $("#yearid").html(tr);
                }
            })
        }
    }
    function getClass(sclevelid) {

        $("#classid").html("");
        if (sclevelid != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getClass'); ?>",
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    schlevelid: sclevelid
                },
                success: function (res) {
                    var data = res.class;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.classid + '" grade_levelid="' + row.grade_levelid + '">' + row.class_name + '</option>';
                        }
                    }
                    $("#classid").html(tr);
                }
            })
        }
    }
    function getStudyPeriod(payment_type) {

        $("#termid").html("");
        if (payment_type != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getStudyPeriod'); ?>/" + payment_type,
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    schoolid: $("#schoolid").val(),
                    sclevelid: $("#sclevelid").val(),
                    payment_type: payment_type,
                    yearid: $("#yearid").val(),
                    classid: $("#classid").val()
                },
                success: function (res) {
                    var data = res.datas;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.term_sem_year_id + '">' + row.period + '</option>';
                        }
                    }
                    $("#termid").html(tr);
                }
            })
        }
    }
    function getweeks() {
        var termid = $("#termid").val();
        $("#weekid").html("");
        if (termid != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getweeks'); ?>",
                type: "POST",
                dataType: 'json',
                async: false,
                data: {
                    termid: termid
                },
                success: function (res) {
                    var data = res.weeks;
                    var tr = "<option value=''></option>";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="' + row.weekid + '">' + row.week + '</option>';
                        }
                    }
                    $("#weekid").html(tr);
                }
            })
        }
    }
    function attbase(schlevelid) {
        $("#baseattid").html("");
        var opattbs = '';
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo site_url('student/attendance/getattbase'); ?>/" + schlevelid,
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (res) {
                    var data = res.attbases;
                    if (data.length > 0) {
                        for (var j = 0; j < data.length; j++) {
                            var attrow = data[j];
                            opattbs += '<option value="' + attrow.baseattid + '">' + attrow.base_att + '</option>';
                        }
                    }
                    opattbs += '</select>';

                }
            })
        }
        $("#baseattid").html(opattbs);
    }
    function getsubjectGroup(programid, sch_levelid){
        if(sch_levelid !=""){
            if(programid =="1"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/group_subjectkgp'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        sch_levelid: sch_levelid
                    },
                    success: function (res) {
                        var data = res.gs_kgp;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subj_type_id + '">' + row.subject_type + '</option>';
                            }
                        }
                        $("#subject_grouop").html(tr);
                    }
                })
            }
            if(programid =="2"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/group_subjectiep'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        sch_levelid: sch_levelid
                    },
                    success: function (res) {
                        var data = res.gs_iep;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subj_type_id + '">' + row.subject_type + '</option>';
                            }
                        }
                        $("#subject_grouop").html(tr);
                    }
                })
            }
            if(programid =="3"){
                $("#subject_grouop").html("");
                $("#subjectid").html("");

            }

        }else{
            toastr["warning"]("Select school level Please !");
        }
    }
    function getsubject(programid, subject_group){
        if(subject_group !=""){
            if(programid =="1"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/autosubjectkgp'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        subject_group: subject_group
                    },
                    success: function (res) {
                        var data = res.sub_kgp;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subjectid + '">' + row.subject + '</option>';
                            }
                        }
                        $("#subjectid").html(tr);
                    }
                })
            }
            if(programid =="2"){
                $.ajax({
                    url: "<?php echo site_url('student/attendance/autosubjectiep'); ?>",
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    data: {
                        subject_group: subject_group
                    },
                    success: function (res) {
                        var data = res.sub_iep;
                        var tr = "<option value=''></option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var row = data[i];
                                tr += '<option value="' + row.subjectid + '">' + row.subject + '</option>';
                            }
                        }
                        $("#subjectid").html(tr);
                    }
                })
            }
        }else{
            toastr["warning"]("Select school level Please !");
        }
    }

    $(function () {
        $("#from_date,#to_date").datepicker({
            language: 'en',
            pick12HourFormat: true,
            format: 'dd-mm-yyyy'
        });
        $("body").delegate(".pagenav", "click", function () {
            var page = $(this).attr("id");
            search(page);
        })

        $("body").delegate("#btnshow", "click", function () {
            search();
        });
        $("body").delegate("#sclevelid", "change", function () {
            var programid = $("#sclevelid option:selected").attr("pro_id");
            getSchYear($(this).val());
            attbase($(this).val());
            getsubjectGroup(programid,$(this).val());
        });
        $("body").delegate("#subject_grouop", "change", function () {
            var programid = $("#sclevelid option:selected").attr("pro_id");
            getsubject(programid,$(this).val());
        });
        $("body").delegate("#yearid", "change", function () {
            var schlevelid = $("#sclevelid").val();
            getClass(schlevelid);
        });
        $("body").delegate("#classid", "change", function () {
            getStudyPeriod(1);
        });
        $("body").delegate("#termid", "change", function () {
            getweeks();
        });
        $("body").delegate("#subject", "focus", function () {
            var programid = $("#sclevelid option:selected").attr("pro_id");
            var grade_levelid = $("#classid option:selected").attr("grade_levelid");
            var baseattid=0;
            baseattid=$("#baseattid").val()-0;
            if(baseattid==1){
                //autoSubject(grade_levelid);
                if (programid == 1){
                    autoSubject_kgp(grade_levelid);
                }else{
                    autoSubject_iep(grade_levelid);
                }
            }
        });

        $("body").delegate("#subject", "change", function () {
            if ($(this).val() == "") {
                $("#subjectid").val("");
            }
        });
        $("body").delegate(".link_delete", "click", function () {
            if (window.confirm("Sure! Do you want to delete? ")) {
                var transno = $(this).attr("rel");
                $.ajax({
                    url: "<?php echo site_url('student/attendance/delete'); ?>/" + transno,
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    success: function (res) {
                        toastr["success"](res.del);
                        search(0);
                    }
                })
            }
        });


    })
</script>