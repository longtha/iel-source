<style type="text/css">
    table tbody tr td img{width: 20px; margin-right: 10px}
    a,.sort,.panel-heading span{cursor: pointer;}
    .panel-heading span{margin-left: 10px;}
    .cur_sort_up{
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
        background-position: left;
        background-repeat: no-repeat;
        padding-left: 15px !important;
    }
    .cur_sort_down{
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
        background-position: left;
        background-repeat: no-repeat;
        padding-left: 15px !important;
    }
    .ui-autocomplete{z-index: 9999;}

    .panel-body{
        /*height: 650px;*/
        max-height: calc(100vh - 200px);
        overflow-y: auto;
    }

    .t_border{
        border:none !important;
    }
/*
    input[type=text]{
      border:none !important;
    }*/
    .datepicker{
      z-index: 2000 !important;
    }
    #guardian,#date_acade,#date_teacher{
      border: none; 
      border-bottom: 1px dashed black;
      text-align:center;
    }

    #list th {vertical-align: middle;}
    #list td {vertical-align: middle;}
    .hide_controls{display:none;}
</style>
<?php  
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }    
?>

<div class="container-fluid" style="width:100% !important;padding-bottom: 20px;">
    <div class="row">
      <div class="col-xs-12">
          <div class="result_info">
              <div class="col-xs-6">
                  <strong>List Student</strong>  
              </div>
              <div class="col-xs-6" style="text-align: right">
                  <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                      <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
                  </a>
                  <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                     <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
                  </a>
              </div>         
          </div>
      </div>
    </div>
    <?php if($this->session->userdata('match_con_posid')!='stu'){?>
    <div class="collapse in" id="colapseform">
        <div class="col-sm-12">
          <div class="panel panel-default" style="margin-left: -14px;margin-right: -14px;">
            <div class="panel-body">
              <div class="col-sm-12">    
              <!-- <div class="col-sm-4">   -->              
                <div class="col-sm-4">          
                    <label>School:</label>
                    <select class="form-control" id="school">
                      <option value=""></option>
                        <?php echo $opt_school;?> 
                    </select>
                  <input type='hidden' class='form-control input-sm' id='id'/>
                </div>
                <div class="col-sm-4">          
                    <label>Program:</label>
                    <select class="form-control" id="program"></select>
                </div>
                <div class="col-sm-4">          
                    <label>School Level:</label>
                    <select class="form-control" id="sch_level"></select>
                </div>
                <div class="col-sm-4">          
                    <label>Year:</label>
                    <select class="form-control" id="years"></select>
                </div>
                <div class="col-sm-4">          
                  <label>Grad Level:</label>
                  <select class="form-control" id="grad_level"></select>
                </div>
                <div class="col-sm-4">            
                    <label>Class:</label>
                    <select class="form-control" id="classid"></select>
                </div> 
                <div class="col-sm-4">          
                    <label>Semester</label>
                    <select class="form-control" id="semester"></select>
                </div> 
                <div class="col-sm-4">
                    <label>Student ID:</label>
                    <input type='text' value="" class='form-control input-sm' name='s_student_id' id='s_student_id'/>
                </div>
                <div class="col-sm-4">
                    <label>Name(Khmer/English):</label>
                    <input type='text' value="" class='form-control input-sm' name='s_full_name' id='s_full_name'/>
                </div>
                               
            <!-- </div> -->
         
            <div class="col-sm-12" style="padding-top:20px;">
                <div class="form-group">
                    <input type="button" class="btn btn-primary" value="Search" id="search">
                </div>            
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php } ?>
<!-- List Data -->
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="table-responsive">
          <table class="table" border='0'>
            <thead >
              <tr>
                <?php
                    foreach ($thead as $th => $val) {
                        if ($th == 'Action'){
                            echo "<th class='remove_tag'>" . $th . "</th>";
                        }
                        else{
                            echo "<th>" . $th . "</th>";
                        }
                    }
                ?>
                <th id='show_print'><a href="javascript:void(0)" id="view_all">Print All</a></th>
              </tr>
            </thead>
            <tbody class='listbody'>

            </tbody>
          </table>
        </div>
      </div>
      <div class="fg-toolbar">
        <div style='margin-top:20px; width:11%; float:left;'>
          Display : <select id='perpage' onchange='show_list(1);' style='padding:5px; margin-right:0px;'>
            <?PHP
                for ($i = 10; $i < 500; $i += 10) {           
                   echo "<option value='$i' selected>$i</option>";                                     
                }
            ?>                        
          </select>
        </div>
          <div class='paginate' style="float:right;"></div>
      </div> 
    </div>
  </div> 
  <div class="modal fade bs-example-modal-lg" id="show_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Evaluation for Semester</h4>
          </div>
          <div class="modal-body">
              <div class="row">
                <div class="col-sm-4">
                  <img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="width:200px !important">
                </div>
                <div class="col-sm-8">
                  <h4 class="kh_font">របាយការណ៍សិក្សាប្រចាំឆមាស</h4>
                  <h4 style="font-size:23px;">Semester Acadimic Report</h4>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                    <center>
                    <table class="table head_name">
                      <tr>
                        <td>Student's Name:</td>
                        <td>
                          <span id="stud_name_eng"></span>
                          <input type="hidden" name="h_studentid" id="h_studentid" class="h_studentid">
                           <input type='hidden' class='form-control input-sm' id='evaluationid' name="evaluationid" value="" />
                        </td>
                        <td>Level:</td><td><span id="level_name"></span></td>
                        <td>Yearly:</td><td><span id="yearly"></span></td>
                      </tr>
                      <tr>
                        <td>សិស្សឈ្មោះ:</td><td><h5 id="stud_name_kh" class="kh_font"></h5></td>
                        <td>កម្រិត:</td><td><span id="level_name_kh"></span></td>
                        <td>ឆ្នាំសិក្សា:</td><td><span id="year_kh"></span></td>
                      </tr>
                    </table>
                    </center>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12" style="text-align: center;">
                  <p style="font-size:13px;"><b>Score and Evaluation of Each Subject / ពិន្ទុ និងការវាយតម្លៃមុខវិជ្ជានីមួយៗ</b></p>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12" style="">
                  <table class="table table-bordered" id="tbl_eval">
                    
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <textarea class="form-control" rows="5" id="t_comment" placeholder="Teacher's Comments / មតិយោបល់របស់គ្រូ"></textarea>
                </div>
                <div class="col-sm-12" style="margin-top:20px;">
                    <div class="col-sm-6" style="text-align: center;">
                        <div style="display: inline;"><b>Date:</b>&nbsp;<input type="text" name="date_acade" id="date_acade" class="date_acade datepicker" value=""></div><br>
                        <p><b>Academic Manager</b></p>
                        <center>
                        <SELECT id="user_acad" class="form-control" style="width:150px !important;">
                            <?php 
                              $sql_user_acad = $this->db->query("SELECT
                                                                  sch_user.userid,
                                                                  sch_user.user_name
                                                                  FROM
                                                                  sch_user
                                                                  WHERE match_con_posid='acca'");
                              $opt_acad = "<option value=''></option>";
                              if($sql_user_acad->num_rows() > 0){
                                foreach($sql_user_acad->result() as $row_acad){
                                  $opt_acad.= "<option value='".$row_acad->userid."'>".$row_acad->user_name."</option>";
                                }
                              }   
                              echo $opt_acad;  
                            ?>
                        </SELECT>  
                        </center>
                    </div>
                    <div class="col-sm-6" style="text-align: center;">
                        <div style="display: inline;"><b>Date:</b>&nbsp;<input type="text" name="date_teacher" id="date_teacher" class="date_teacher" value=""></div>
                        <p><b>Teacher's Signature</b></p>
                        <?php 
                            $userid = $this->session->userdata('userid');
                            $sql_user_acad = $this->db->query("SELECT
                                                                  sch_user.user_name
                                                                  FROM
                                                                  sch_user
                                                                  WHERE sch_user.userid='".$userid."'")->row();
                            $username = isset($sql_user_acad->user_name)?$sql_user_acad->user_name:"";
                            echo "<p id='user_label'><b>".$username."</b></p>";
                        ?>
                        <input type="hidden" name="userid_teacher" id="userid_teacher" value="<?= $userid ?>">
                    </div>
                </div>
              </div>
              <br>
              <div class="row">
                  <div class="col-sm-12">
                      <textarea class="form-control" rows="4" id="g_comment" placeholder="Guardian's Comments / មតិមាតាបិតាសិស្ស"></textarea>
                  </div>
                  <div class="col-sm-6">
                    <p>Please return the book to school before</p>
                    <p>សូមប្រគល់សៀវភៅតាមដាននេះមកសាលាមុនថ្ងៃទី៖</p>
                    <div class="col-sm-6" style="padding-left:0px;">
                      <input id="return_date" class="form-control return_date"  style="text-align:center;" type="text">                           
                    </div>
                  </div>
                  <div class="col-sm-6" style="text-align: center;margin-top:20px;">
                      <div style="display: inline;"><b>Date:</b><input id="guardian" class="guardian" name="guardian" type="text"></div>
                      <p>Guardian's Signature / ហត្ថលេខាមាតាមិតាសិស្ស</p>
                  </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id='btn_save'>Save</button>
          </div>
        </div>
      </div>
  </div>
<script type="text/javascript">
  $( document ).ready(function() {     
    clear_form();
    show_list(1);
      //----------------Pagenation---------------------
    $(document).on('click', '.pagenav', function(){
        var page = $(this).attr("id");        
        show_list(page);     
    });
    //---------------End------------------------------
    $('#perpage').val(10);
     

      $("#a_addnew").click(function(){
          $("#colapseform").collapse('toggle');
      });

      $("#refresh").click(function(){
          location.reload();
      });

      $("#search").click(function() {         
            show_list(1);
      });    
      
      $("body").delegate("#seldom","click",function(){
          var tr = $(this).parent().parent();
          if($(this).is(":checked")){
            $(this).val(1);
            tr.find("#sometim").removeAttr("checked",false);
            tr.find("#usually").removeAttr("checked",false);
            tr.find("#consistently").removeAttr("checked",false);
            tr.find("#sometim").val(0);
            tr.find("#usually").val(0);
            tr.find("#consistently").val(0);
          }else{
            $(this).val(0);
          }
      })
      $("body").delegate("#sometim","click",function(){
          var tr = $(this).parent().parent();
          if($(this).is(":checked")){
            $(this).val(1);
            tr.find("#seldom").removeAttr("checked",false);
            tr.find("#usually").removeAttr("checked",false);
            tr.find("#consistently").removeAttr("checked",false);
            tr.find("#seldom").val(0);
            tr.find("#usually").val(0);
            tr.find("#consistently").val(0);
          }else{
            $(this).val(0);
          }
      })
      $("body").delegate("#usually","click",function(){
          var tr = $(this).parent().parent();
          if($(this).is(":checked")){
            $(this).val(1);
            tr.find("#sometim").removeAttr("checked",false);
            tr.find("#seldom").removeAttr("checked",false);
            tr.find("#consistently").removeAttr("checked",false);
            tr.find("#sometim").val(0);
            tr.find("#seldom").val(0);
            tr.find("#consistently").val(0);
          }else{
            $(this).val(0);
          }
      })
      $("body").delegate("#consistently","click",function(){
          var tr = $(this).parent().parent();
          if($(this).is(":checked")){
            $(this).val(1);
            tr.find("#sometim").removeAttr("checked",false);
            tr.find("#usually").removeAttr("checked",false);
            tr.find("#seldom").removeAttr("checked",false);
            tr.find("#sometim").val(0);
            tr.find("#usually").val(0);
            tr.find("#seldom").val(0);
          }else{
            $(this).val(0);
          }
      })
      $('#school').change(function(){
        var schoolid = $(this).val();
         if(schoolid != ""){
            get_program(schoolid);  
            fn_semester();
         }else{
            $('#program').html('');
            $('#years').html('');
            $('#grad_level').html('');
            $('#sch_level').html('');
         }
      });

      $("body").delegate("#add_eval","click",function(){
          var tr = $(this).parent().parent();
          var studentid = $(this).attr("studentid");
          var student_eng = tr.find("#student_name").attr("name_eng");
          var student_kh  = tr.find("#student_name").attr("name_kh");
          $("#h_studentid").val(studentid);
          student_evaluation(studentid,student_eng,student_kh);
          $("#show_modal").modal();
      })
      //$(".date_acade, .date_teacher,.return_date,.guardian").datepicker();
      $("body").delegate("#btn_save","click",function(){
          var arr_valuation_des = [];
          var ii = 0;
          $(".id_valuation_des").each(function(){
              var id_valuation_des = $(this).val();
              var tr = $(this).parent().parent();
              var seldom = tr.find("#seldom").val();
              var sometim = tr.find("#sometim").val();
              var usually = tr.find("#usually").val();
              var consistently = tr.find("#consistently").val();
              arr_valuation_des[ii] = {'seldom':seldom,
                                      'sometim':sometim,
                                      'usually':usually,
                                      'consistently':consistently,
                                      'studentid' : $('#h_studentid').val(),
                                      'id_valuation_des':id_valuation_des
                                    };
              ii++;
          })
          $.ajax({
              url: '<?php echo site_url('student/c_list_student_semester/save_desc_modal') ?>',
              datatype:'JSON',
              type:'POST',
              async:false,
              data:{
                  schoolid  : $("#school").val(),
                  programid : $("#program").val(),
                  schlevelid: $("#sch_level").val(),
                  yearid    : $("#years").val(),
                  gradlevel : $('#grad_level').val(),
                  classid   : $("#classid").val(),
                  userid_teacher : $("#userid_teacher").val(),
                  user_acad : $("#user_acad").val(),
                  semesterid : $('#semester').val(),
                  studentid : $('#h_studentid').val(),
                  semesterid : $('#semester').val(),

                  command_teacher  : $('#t_comment').val(),
                  commend_guardian : $('#g_comment').val(),
                  date_acade    : $('#date_acade').val(),
                  date_teacher  : $('#date_teacher').val(),
                  guardian_date : $('#guardian').val(),
                  return_date   : $('#return_date').val(),
                  arr_valuation_des : arr_valuation_des,
                  evaluationid  : $("#evaluationid").val()
              },
              success:function(data){
                  if(data.success == 'OK'){  
                    toastr["success"]("Save successful !");
                    $("#show_modal").modal("hide");
                  }else{
                    toastr["error"]("Save fail !");
                  }
              }
          }); 
      })

      $('#program').change(function(){
        var program = $(this).val();
         if(program != ""){
            get_school_level(program);  
            fn_semester();
         }else{
            $('#years').html('');
            $('#grad_level').html('');
            $('#sch_level').html('');
         }
      });

      $('#sch_level').change(function(){        
        var sch_level = $(this).val(); 
        var program = $('#program').val();
        var grad_level = $('#grad_level').val();
        if(sch_level !=""){
            get_year(program,sch_level); 
            fn_semester();
            get_gradlevel(program,sch_level);
            get_class(sch_level,grad_level);              
        }
      });

      $('#grad_level').change(function(){
        var sch_level = $('#sch_level').val();
        var grad_level = $(this).val();  
        if(grad_level !=""){
             get_class(sch_level,grad_level);
        }
      });

      $('#years').change(function(){
          fn_semester();
      });
      

      $( "#s_student_id" ).keyup(function(){
        var this_v = $(this);
        stuentid_auto(this_v);
      });

      $( "#s_full_name" ).autocomplete({
          source: function(request, response) {
              $.getJSON("<?= site_url('student/c_list_student_semester/st_auto') ?>", {
                name : request.term,
                pid: $("#program").val(),
                schlevel:$("#sch_level").val(),
                classid:$("#classid").val()
              }, response);
          },
          minLength: 0,
          delay: 0,
          autoFocus: true,
          select: function( event, ui ) {          
              $(this).val(ui.item.name);           
              return false;                       
          }
      })
      .focus(function(){
        $( this ).autocomplete("search");
      })
      .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
          .append( "<span>" + item.name + "</span>" )              
          .append( "</li>" )
          .appendTo( ul );
      }
  });
  function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  function student_evaluation(studentid,name_eng,name_kh){
      $.ajax({
          url: '<?php echo site_url('student/c_list_student_semester/show_infor_evaluation') ?>',
          datatype:'JSON',
          type:'POST',
          async:false,
          data:{
              schoolid  : $("#school").val(),
              programid : $("#program").val(),
              schlevelid: $("#sch_level").val(),
              yearid    : $("#years").val(),
              gradlevel : $('#grad_level').val(),
              classid   : $("#classid").val(),
              semesterid : $('#semester').val(),
              studentid : studentid
          },
          success:function(data){
            var grad_level = $("#grad_level").find("option:selected").text();
            var yearly     = $("#years").find("option:selected").attr("att_y");

            $("#stud_name_eng").html(name_eng);
            $("#stud_name_kh").html(name_kh);
            $("#level_name").html(grad_level);
            $("#level_name_kh").html(grad_level);
            $("#yearly").html(yearly);
            $("#year_kh").html(yearly);

            $("#evaluationid").val(data.evaluationid);
            $('#tbl_eval').html(data.tbl_des);  
            $("#t_comment").val(data.comment.teach);  
            $("#g_comment").val(data.comment.guard);  
            $("#date_acade").val(data.date_sign.acad_date); 
            $("#date_teacher").val(data.date_sign.teach_date); 
            $("#guardian").val(data.date_sign.guard_data); 
            $("#return_date").val(data.date_sign.ret_date);
            $("#user_acad").val(data.date_sign.academicid);
            
            if(data.date_sign.userid != ""){
              $("#userid_teacher").val(data.date_sign.userid);
              $("#user_label").html("<b>"+data.date_sign.userlabel+"</b>");
            }
            
            $(".date_acade, .date_teacher,.return_date,#guardian").datepicker(); 
          }
      });
  }
  function stuentid_auto(this_v){
      this_v.autocomplete({
              source: function(request, response) {
                  $.getJSON("<?= site_url('student/c_list_student_semester/stid_auto') ?>", {
                     //st_num : request.term,
                     id:this_v.val(),
                     pid: $("#program").val(),
                     schlevel:$("#sch_level").val(),
                     classid:$("#classid").val()
                  }, response);
              },
              minLength: 0,
              delay: 0,
              autoFocus: true,
              select: function( event, ui ) {   
                  var id = ui.item.st_num;
                  var name = ui.item.st_name;
                  var full = id+'-'+name;      
                  this_v.val(id);
                  $("#id").val(ui.item.st_num);           
                  return false;                       
              }
        })
        .focus(function(){
          this_v.autocomplete("search");
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
          return $( "<li>" )
            .append( "<span>" + item.st_num + "-" + item.st_name + "</span>" )              
            .append( "</li>" )
            .appendTo( ul );
        }      
  }



  function get_program(schoolid){
    $.ajax({
      url: '<?php echo site_url('student/c_list_student_semester/get_program') ?>',
      datatype:'JSON',
      type:'POST',
      async:false,
      data:{
            'schoolid':schoolid
      },
      success:function(data){
        $('#program').html(data.program);    
      }
    });
  }  

  function  get_school_level(program){
    $.ajax({
      url: '<?php echo site_url('student/c_list_student_semester/school_level') ?>',
      datatype:'JSON',
      type:'POST',
      async:false,
      data:{
            'program':program
      },
      success:function(data){
          $('#sch_level').html(data.schlevel);    
          get_year(program,$("#sch_level").val());
      }
    });
  }

  function get_year(program,sch_level){
    $.ajax({
      url:'<?php echo site_url('student/c_list_student_semester/get_years') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'program':program,
              'sch_level':sch_level
      },
      success:function (data){
          $('#years').html(data.schyear);
          get_gradlevel(program,sch_level);
      },
    });
  }

  function get_gradlevel(program,sch_level){
    $.ajax({
      url:'<?php echo site_url('student/c_list_student_semester/get_gradlevel') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'program':program,
              'sch_level':sch_level
      },
      success:function (data){
          $('#grad_level').html(data.gradlevel);         
      },
    });
  }

  function get_class(sch_level,grad_level){
    $.ajax({
      url:'<?php echo site_url('student/c_list_student_semester/get_class') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'sch_level':sch_level,
              'grad_level':grad_level
      },
      success:function (data){
          $('#classid').html(data.getclass);
      },
    });
  }

  function fn_semester(classid){
    	$.ajax({
          url:'<?php echo site_url('student/c_list_student_semester/get_semester') ?>',
          type:'POST',
          datatype:'JSON',     
          async:false,
          data:{
              schoolid:$("#school").val(),
              programid:$("#program").val(),
              schlevelid:$("#sch_level").val(),
              yearid:$("#years").val()
          },
          success:function (data){
              $('#semester').html(data.get_semester);
          }
      });
  }

  function clear_form(){
    $('school_level').val('');
    $('#grad_level').val('');
    $('#years').val('');
    $('#id').val('');
    $('#s_student_id').val('');
  } 

  // function sort(event) {
  //       this.sortby = $(event.target).attr("rel");
  //       if ($(event.target).attr("sortype") == "ASC") {
  //           $(event.target).attr("sortype", "DESC");
  //           this.sorttype = "ASC";
  //           $('.sort').removeClass('cur_sort_down');
  //           $(event.target).addClass('cur_sort_up');
  //       } else if ($(event.target).attr("sortype") == "DESC") {
  //           $(event.target).attr("sortype", "ASC");
  //           this.sorttype = "DESC";
  //           $('.sort').removeClass('cur_sort_up');
  //           $(event.target).addClass('cur_sort_down');
  //       }

  //       show_list(this.fn, this.sortby, this.sorttype);       
  // }

  function show_list(page, sortby, sorttype){
        var url = "<?php echo site_url('student/c_list_student_semester/show_list') ?>";
        var m = "<?PHP echo $m?>";
        var p = "<?PHP echo $p?>";
        var program   = $('#program').val();
        var sch_level = $('#sch_level').val();
        var years     = $('#years').val();
        var classid   = $('#classid').val();
        var gradlevel = $('#grad_level').val();
        var perpage   = $('#perpage').val();
        var s_student_id = $('#s_student_id').val();
        var s_full_name = $('#s_full_name').val(); 
        var schoolid    = $('#school').val(); 
        var semesterid  = $('#semester').val(); 
        $.ajax({
          url: url,
          type: "POST",
          datatype: "JSON",
          async: false,
          data: {
              'm': m,
              'p': p,
              'page': page,
              's_sortby': sortby,
              's_sorttype': sorttype,
              'program': program,
              'sch_level':sch_level,
              'years':years,
              'classid':classid,
              'gradlevel':gradlevel,
              's_student_id':s_student_id,
              's_full_name':s_full_name,
              'schoolid':schoolid,
              'semesterid':semesterid,
              'perpage': perpage
          },
          success: function (data) {
              $("#show_print").html(data.printall);
              if(data.tr_data ==""){
                  var table = '<table class="table table-bordered table-striped table-hover">'+
                                  '<tr>'+
                                      '<td colspan="11" style="color:red;text-align:center;font-size:14px;"><i>We did not find anything to show here!</i></td>'+
                                  '</tr>'+
                                  '<tr><td colspan="11"></td></tr>'+
                              '</table>';
                  $('.listbody').html(table);
              }else{
                  $(".listbody").html(data.tr_data);
                  //clear_form();
              }                   
              $('.paginate').html(data.pagina.pagination);
          }
      });
    }
  
</script>