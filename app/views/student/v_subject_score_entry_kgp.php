<div class="wrapper">
 	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
    		<div class="result_info">
                <div class="col-sm-6">
                    <strong>Subject Score Entry Form</strong>                    
                </div>
            </div>
			<!--form element-->
	      	<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" class="tdrow" id="frm_subject_score_kgp">
	      		<!--form search of student information-->
	      		<div class="col-sm-12">
		        	<div class="row">
		        		<div class="panel panel-default">
		        			<div class="panel-body">
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_id">School</label>
					        			<select name="school_id" id="school_id" minlength='1' class="form-control">
					                    	<?php
												$row = $this->school->getschinfor_row(1);											
												echo '<option value="'. $row->schoolid .'">'. $row->name .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="program_id">Program</label>
					        			<select name="program_id" id="program_id" minlength='1' class="form-control">
					                    	<?php
												$row = $this->program->getprogram(1);											
												echo '<option value="'. $row->programid .'">'. $row->program .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_level_id">School Level<span style="color:red">*</span></label>
					        			<select name="school_level_id" id="school_level_id" minlength='1' class="form-control" required data-parsley-required-message="This field require">
		                                    <option value=""></option>
					                    	<?php
												$get_level_data = $this->school_level->getsch_level(1);												
												foreach($get_level_data as $r_level){
													echo '<option value="'. $r_level->schlevelid .'">'. $r_level->sch_level .'</option>';
												}
											?>	                      
		                                </select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="adcademic_year_id">Academic Year<span style="color:red">*</span></label>
	                                    <select name="adcademic_year_id" id="adcademic_year_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="grade_level_id">Grade Level<span style="color:red">*</span></label>
	                                    <select name="grade_level_id" id="grade_level_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="teacher_id">Teacher Name<span style="color:red">*</span></label>					        			
	                                    <select name="teacher_id" id="teacher_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>					        	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="class_id">Class Name<span style="color:red">*</span></label>					        			
	                                    <select name="class_id" id="class_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="subject_id">Subject Name<span style="color:red">*</span></label>
					        			<input type="hidden" name="subject_group_id" id="subject_group_id">
					        			<input type="hidden" name="subject_main_id" id="subject_main_id">					        			
	                                    <select name="subject_id" id="subject_id" minlength='1' class="form-control" required data-parsley-required-message="This field require"></select>
					        		</div>
					        	</div>					        					        	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="student_id">Student Name</label>
	                                    <select name="student_id" id="student_id" class="form-control"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-12">
					        		<div class="form-group">
					        			<label class="req" for="student_num">Examp Type<span style="color:red">*</span></label><br>
					        			<div class="form_exam_type" id='form_semester' style="text-align:center; border:1px #337ab7 solid; padding:5px 5px 0 5px;"> 						
											<table border="0"​ align="center" id='tbl-exam-type' class="tbl-exam-type table table-bordered">
												<tr style="color:#337ab7 !important;">
					        						<td>
					        							<label class="radio-inline">
			                                            	<input type="radio" name="select_exam_type" value='1' class='select_exam_type'>Monthly
			                                            </label> 
					        						</td>
					        						<td>
					        							<label class="radio-inline">
			                                            	<input type="radio" name="select_exam_type" value='2' class='select_exam_type'>Semester
			                                            </label> 
					        						</td>
					        						<td>
					        							<label class="radio-inline">
			                                            	<input type="radio" name="select_exam_type" value='3' class='select_exam_type'>Final
			                                            </label> 
					        						</td>
					        					</tr>
					        					<tr class="show_extam_type" style="display:none;"></tr>
					        					<tr class="show_mess_alert" style="display:none;"></tr>		
											</table>
										</div>                                    
					        		</div>
					        	</div>					        	
		        			</div>
		        		</div>
		        	</div>
		        </div><!--end form search of student information-->
		        
		        <!--button-->
		        <div class="col-sm-12" style="margin-top:-12px;">	
		        	<div class="row">
		        		<div class="form-group" style="text-align:center;">
			                <?php if($this->green->gAction("R")){ ?>
			                	<input type="button" name="btnsearch" id='btnsearch' value="Search" class="btn btn-primary" />
			                <?php } ?>
			                <input type="button" name="btncancel" id='btncancel' value="Cancel" class="btn btn-warning" />
		                </div> 
		        	</div>	                                           
		        </div><!--end button-->

		        <!--show student form detail-->
		        <div class="col-sm-12 show_student_detail" style="margin-top:-5px; display:none;">
		        	<div class="row">
		        		<div class="result_info">
			                <div class="col-sm-6">
			                    <strong>Student Information</strong>                    
			                </div>
			            </div>
		        		<div class="panel panel-default">
		        			<div class="panel-body">
					            <!--table detail-->
					            <div class="table-responsive div-student-detail">	</div>		
								<!--end table detail-->

								<!--button save record-->
						        <div class="col-sm-12" style="margin:-8px 0 -15px 0;">	
						        	<div class="row">
						        		<div class="form-group" style="text-align:right;">
							                <?php if($this->green->gAction("C")){ ?>
							                	<input type="button" name="btnsave" id='btnsave' value="Save" class="btn btn-primary" />
							                <?php } ?>
						                </div> 
						        	</div>	                                           
						        </div><!--end button save record-->
		        			</div>
			        	</div>
		        	</div>	        	
		        </div><!--end show student form detail-->		        
	      	</form>
	  	</div>
	</div>
</div>

<style>
	.tbl-exam-type td{
		text-align: left;
		vertical-align: middle !important;
		border: hidden !important;		
	}

	.table-student-detail th{
		text-align: center;
		vertical-align: middle !important;
		background:#337ab7;
		color: #FFF;
	}
	.table-student-detail td{
		text-align: right;
		vertical-align: middle !important;
	}
	.title-info{
		 border:2px #FFD700 solid;
		 padding:5px 5px 5px 5px;
		 margin-bottom: 15px;
	}
	.input_score, .output_score, .total_score, .averag_coefficient, .coefficients{
		width: 60px;
		height: 25px;		
		text-align: right;
		/*border: hidden;*/
	}
	.final_score{
		width: 70px;
		height: 25px;		
		text-align: right;
		border: hidden;
	}
	/* text for header */
	.kh{
		font-size: 8.5pt;
	}
	.en{
		font-size: 7.2pt;
		color: #FFD700;
	}
	.en-title{
		font-size: 7.2pt;
		color: #337ab7;
	}
	.kh-title{
		color: #337ab7;
		font-weight: 600;
	}
</style>

<script type="text/javascript">
	$(function(){
		$('#year, #schlevelid').hide();
		$("#school_level_id").val($("#school_level_id first").val());

		//get year and class data---------------------------------
		$('#school_level_id').change(function(){
			var school_level_id = $(this).val();
			var school_id = $('#school_id').val();
			var program_id = $('#program_id').val();

			if(school_level_id != ""){
				$.ajax({
		            url: "<?php echo site_url('student/c_subject_score_entry_kgp/get_year_and_grade_data') ?>",	            
		            type: "post",
		            dataType: 'json',
		            data: {'school_id': school_id, 'program_id': program_id, 'school_level_id': school_level_id},
		            success: function (data) {
		            	// console.log(data);
		            	
		            	//get year--------------------------------
		            	var getYear = '';	            	
		            	$.each(data["year"], function(k,v){
		            		getYear += '<option value="'+ v.yearid +'">'+ v.sch_year+'</option>';		            		
		            	}); $('#adcademic_year_id').html(getYear);

		            	//get grade--------------------------------
		            	var getGrade = '';
		            	$.each(data["grade"], function(ke,re){
		            		getGrade += '<option value="'+ re.grade_levelid +'">'+ re.grade_level+'</option>';
		            	});	$('#grade_level_id').html(getGrade);

		            	//get teacher------------------------------
		            	var grade_level_id = $('#grade_level_id').val();
		            	var adcademic_year_id = $('#adcademic_year_id').val();
		            	var url = "<?php echo site_url('student/c_subject_score_entry_kgp/get_teacher_data') ?>";
						var data = 'school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
						getDataByAjax($('#teacher_id'), url, data);
						$('#class_id').html('');
						$('#subject_id').html('');	
						$('#student_id').html('');

						//remove message require-------------------
						var aId = $('#adcademic_year_id').data('parsley-id');
						var gId = $('#grade_level_id').data('parsley-id');

						$('#parsley-id-'+aId).remove();	   
						$('#adcademic_year_id').removeClass('parsley-error');

						$('#parsley-id-'+gId).remove();	   
						$('#grade_level_id').removeClass('parsley-error');        	
				    }
		        });	
			}else{
				$('#adcademic_year_id').html('');
				$('#grade_level_id').html('');
				$('#teacher_id').html('');
				$('#class_id').html('');
				$('#subject_id').html('');	
				$('#student_id').html('');
			}
		});

		//get teacher data-----------------------------------------
		$('#grade_level_id').change(function(){
			var grade_level_id = $(this).val();
			var school_level_id = $('#school_level_id').val();			
			var adcademic_year_id = $('#adcademic_year_id').val();
			if(school_level_id == "" && adcademic_year_id == "" && grade_level_id == ""){
				$('#teacher_id').html('');
			}else{
				var url = "<?php echo site_url('student/c_subject_score_entry_kgp/get_teacher_data') ?>";
				var data = 'school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
				getDataByAjax($('#teacher_id'), url, data);
				$('#class_id').html('');
				$('#subject_id').html('');	
				$('#student_id').html('');
			}
		});

		//get class and subject-------------------------------------
		$('#teacher_id').change(function(){
			var teacher_id = $(this).val();
			var grade_level_id = $('#grade_level_id').val();
			var school_level_id = $('#school_level_id').val();			
			var adcademic_year_id = $('#adcademic_year_id').val();

			if(school_level_id != "" && adcademic_year_id != "" && grade_level_id != ""){
				var url = "<?php echo site_url('student/c_subject_score_entry_kgp/get_class_data') ?>";
				var data = 'school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id+'&teacher_id='+teacher_id;
				getDataByAjax($('#class_id'), url, data);
			}else{
				$('#class_id').html('');
				$('#subject_id').html('');	
				$('#student_id').html('');			
			}
		});

		//get student data------------------------------------------
		$('#class_id').change(function(){	
			var class_id = $(this).val();
			var teacher_id = $('#teacher_id').val();
			var school_level_id = $('#school_level_id').val();
			var grade_level_id = $('#grade_level_id').val();			
			var adcademic_year_id = $('#adcademic_year_id').val();
			if(class_id == ""){
				$('#student_id').html(''); $('#subject_id').html('');
				//$('#subject_group_id').val(''); $('#subject_main_id').val('');			
			}else{
				var url = "<?php echo site_url('student/c_subject_score_entry_kgp/get_student_data') ?>";
				var data = 'class_id='+class_id;
				getDataByAjax($('#student_id'), url, data);

				var url = "<?php echo site_url('student/c_subject_score_entry_kgp/get_subject_data') ?>";
				var data = 'class_id='+class_id+'&school_level_id='+school_level_id+'&grade_level_id='+grade_level_id+'&adcademic_year_id='+adcademic_year_id+'&teacher_id='+teacher_id;
				getDataByAjax($('#subject_id'), url, data);
			}
		});
	
		//get group subject id and main subject id------------------
		$('#subject_id').change(function(){
			var subject_id = $(this).val();
			var class_id = $('#class_id').val();
			var teacher_id = $('#teacher_id').val();
			var school_level_id = $('#school_level_id').val();
			var grade_level_id = $('#grade_level_id').val();			
			var adcademic_year_id = $('#adcademic_year_id').val();
			if(subject_id == ""){
				$('#subject_group_id').val(''); $('#subject_main_id').val('');
			}else{
				$.ajax({
					url:"<?php echo site_url('student/c_subject_score_entry_kgp/get_group_main_id_subject') ?>",
					type:'POST',
					dataType: 'json',
		            data:'subject_id='+subject_id+'&class_id='+class_id+'&school_level_id='+school_level_id+'&grade_level_id='+grade_level_id+'&adcademic_year_id='+adcademic_year_id+'&teacher_id='+teacher_id,
		            success: function (data) {
		            	//console.log(data[0].subj_type_id);
		            	$('#subject_group_id').val(data[0].subj_type_id); $('#subject_main_id').val(data[0].subj_main_id);		            		            	
		            }
				});
			}
		});

		//choose exame type-----------------------------------------
		$('.select_exam_type').click(function(){
			$('.show_extam_type').html('');	
			$('.show_mess_alert').hide().html('');		
			var month_data = ['January', 'February', 'March', 'April-May', 'June', 'July', 'August', 'September', 'Octomber', 'November', 'December'];
			var getVal = $(this).val();
			var opt = ''; 
			var elements = '';

			var school_id = $('#school_id').val();
			var program_id = $('#program_id').val(); 
			var school_level_id = $('#school_level_id').val();
			var adcademic_year_id = $('#adcademic_year_id').val();

			//monthly=========
			if(getVal == 1){
				for(i = 1; i <= month_data.length; i++) {
					opt += '<option value="'+ i +'">'+ month_data[i-1] +'</option>';
				}

				elements += '<td colspan="3">'+
								'<select name="exam_monthly" class="form-control exam_monthly" style="width:910px; border-color:#337ab7;">'+ opt +'</select>'+
					        '</td>';
			}
			//semester========
			else if(getVal == 2){								
				for(j = 1; j <= month_data.length; j++) {
					opt += '<option value="'+ j +'">'+ month_data[j-1] +'</option>';
				}

				elements += '<td colspan="3" class="get_semester_data"></td>';
				$.ajax({
					url:"<?php echo site_url('student/c_subject_score_entry_kgp/get_semester') ?>",
					type:'POST',
					dataType: 'json',
		            data:{	'school_id':school_id,
		            		'program_id':program_id,
		            		'school_level_id':school_level_id,
		            		'adcademic_year_id':adcademic_year_id
		            	 },
		            success: function (data){
		            	var tbl ='<table>'+
										'<tr>'+
											'<td width="300" style="text-align:center !important;">'+
												'<input type="checkbox" name="exam_semester[]" class="exam_semester" value="'+ data[0].semesterid +'"><span style="margin-left:10px; font-weight:bold;">'+ data[0].semester +'</span><br>'+
												'<input type="checkbox" name="exam_semester[]" class="exam_semester" value="'+ data[1].semesterid +'"><span style="margin-left:10px; font-weight:bold;">'+ data[1].semester +'</span>'+
											'</td>'+
											'<td><select name="get_monthly[]" class="form-control get_monthly" style="width:620px; height:80px; border-color:#337ab7;" multiple>'+ opt +'</select></td>'+
										'</tr>'+
				            	  '</table>';            	
				        $('.get_semester_data').html(tbl);
		            }
				});
			}
			//final===========
			else{
				elements += '<td colspan="3" class="show_semester_data"></td>';
				$.ajax({
					url:"<?php echo site_url('student/c_subject_score_entry_kgp/get_semester') ?>",
					type:'POST',
					dataType: 'json',
		            data:{	'school_id':school_id,
		            		'program_id':program_id,
		            		'school_level_id':school_level_id,
		            		'adcademic_year_id':adcademic_year_id
		            	 },
		            success: function (data){
		            	var tbl ='<table>'+
									'<tr>'+
										'<td style="text-align:right !important;">'+
											'<input type="checkbox" name="get_semester_id[]" checked class="get_semester" value="'+ data[0].semesterid +'"><span style="margin-left:10px; margin-right:70px; font-weight:bold;">'+ data[0].semester +'</span>'+
											'<input type="checkbox" name="get_semester_id[]" checked class="get_semester" value="'+ data[1].semesterid +'"><span style="margin-left:10px; font-weight:bold;">'+ data[1].semester +'</span>'+
										'</td>'+
									'</tr>'+
								'</table>';    	
				        $('.show_semester_data').html(tbl);
		            }
				});
			}			

			$('.show_extam_type').show().append(elements);
		});

		//can check only one with checkbox--------------------------
		$('.show_extam_type').delegate('.exam_semester', 'click', function(){
			$('input:checkbox').not(this).prop('checked', false);
		});

		//check to choose both semester
		$('.show_extam_type').delegate('.get_semester', 'change', function(){
			if($(this).not(':checked')){
				$('<div class="check_semester">'+
                	'<div style="border: none;padding: 9px 0 9px 0; height:auto; margin:0 0 8px 8px; width:95%;">'+
                  		'<span style="color:#2d80c3; margin-left:5px; font-weight:bold; font-size:9pt;">You should check both semester1 and semester2</span>'+ 
                	'</div>'+
               '</div>')
               .dialog({
	                height:'auto',
	                width: 430,
	                modal: true,
	                resizable:false,
	                title: 'Check Semester',
	                dialogClass: 'noTitleStuff',                           
	                buttons: {
	                  "OK": function (event) {
	                    $(this).dialog('destroy').remove();
	                    $('.get_semester').prop('checked', true);                      
	                  }
	                }
	            });
	            $('.ui-dialog-titlebar-close').remove();               	
			}
		});

		//search report---------------------------------------------
		$('#btnsearch').click(function(){	
			$('.div-student-detail').html('');
			var school_id = $('#school_id').val();
			var program_id = $('#program_id').val();
			var school_level_id = $('#school_level_id').val();
			var adcademic_year_id = $('#adcademic_year_id').val();
			var grade_level_id = $('#grade_level_id').val();
			var teacher_id = $('#teacher_id').val();
			var class_id = $('#class_id').val();			
			var subject_id = $('#subject_id').val();			
			var student_id = $('#student_id').val();
			var exam_type = [];

			var select_exam_type = $('.select_exam_type:checked').val();
			//for semester result=====================
			if(select_exam_type ==2){
				$('.show_mess_alert').hide().html('');
				var exam_semester = $('.exam_semester:checked').val();				
				if(exam_semester != undefined){	
					$get_monthly_in = [];				
					$selected = 0;					
					$('.get_monthly :selected').each(function(i){
						$selected = $selected + 1;
						$get_monthly_in[i] = $(this).val();
					});

					if($selected == 0){					
			           	var mess_select_month = '<td colspan="3">'+
													'<div class="alert alert-danger" style="margin:5px 7px 5px 0; padding:5px 0 7px 12px;">Please select at least one month before you search</div>'+
										        '</td>';
			           	$('.show_mess_alert').show().append(mess_select_month);
					}else{
						$('.show_mess_alert').hide().html('');
						exam_type = {'get_exam_type':'semester','exam_semester':exam_semester, 'get_monthly_in':$get_monthly_in};
					}
				}else{
					var mess_select_semester = '<td colspan="3">'+
													'<div class="alert alert-danger" style="margin:5px 7px 5px 0; padding:5px 0 7px 12px;">Please select at least one semester before you search</div>'+
										       '</td>';
		           	$('.show_mess_alert').show().append(mess_select_semester);
				}	
			}
			//for final result=========================
			else if(select_exam_type == 3){
				$('.show_mess_alert').hide().html('');
				$get_semester_in = [];
				$('.get_semester:checked').each(function(i){
					$get_semester_in[i] = $(this).val();
				});
				exam_type = {'get_exam_type':'final', 'get_semester_in':$get_semester_in};				
			}
			//for monthly result=======================
			else
			{
				$exam_monthly = $('.exam_monthly').val();
				if($exam_monthly == ""){
					var mess_select_month = '<td colspan="3">'+
												'<div class="alert alert-danger" style="margin:5px 7px 5px 0; padding:5px 0 7px 12px;">Please select at least one month before you search</div>'+
									        '</td>';
		           	$('.show_mess_alert').show().append(mess_select_month);
				}else{
					$('.show_mess_alert').hide().html('');
					exam_type = {'get_exam_type':'monthly','exam_monthly':$exam_monthly};
				}

				/*****************use with multiple select option****************/
				// $exam_monthly = [];
				// $selected = 0;
				// $('.show_mess_alert').hide().html('');
				// $('.exam_monthly :selected').each(function(i){
				// 	$selected = $selected + 1;
				// 	$exam_monthly[i] = $(this).val();
				// });

				// if($selected == 0){					
		  		//      var mess_select_month = '<td colspan="3">'+
				// 								'<div class="alert alert-danger" style="margin:5px 7px 5px 0; padding:5px 0 7px 12px;">Please select at least one month before you search</div>'+
				// 					        '</td>';
		  		//      $('.show_mess_alert').show().append(mess_select_month);
				// }else{
				// 	$('.show_mess_alert').hide().html('');
				// 	exam_type = {'get_exam_type':'monthly','exam_monthly':$exam_monthly};
				// }
			}
			
			var data = {
			                'school_id' : school_id,
							'program_id' : program_id,
							'school_level_id' : school_level_id,
							'adcademic_year_id' : adcademic_year_id, 
							'grade_level_id' : grade_level_id,
							'teacher_id' : teacher_id,
							'class_id' : class_id,		
							'subject_id' : subject_id,							
			                'student_id': student_id,
			                'exam_type' : exam_type
			            };
			//console.log(data); return false;

			if($('#frm_subject_score_kgp').parsley().validate()){
				$.ajax({
		            url:"<?php echo site_url('student/c_subject_score_entry_kgp/get_student_information') ?>",	            
		            type: "post",
		            dataType: 'json',
		            data: data,
		            success: function (data) {
		            	//console.log(data); return false;
		            	$('.show_student_detail').show();
		            	if(data.student.length > 0){
		            		var div = '' ; var table = ''; var tr = '';  var th = ''; var thh = ''; var tdd = '';
		            		var countCols = data.header.length;		            	

			            	//get table theader
			            	if(select_exam_type == 3){ //show final header
			            		th += '<th><span class="kh">ពិន្ទុមធ្យមភាគឆមាសទី១</span><br><span class="en">Average Score of​ <br> Semester1 </span></th>'+ 
			            			  '<th><span class="kh">ពិន្ទុមធ្យមភាគឆមាសទី២</span><br><span class="en">Average Score of <br> Semester2</span></th>';
				            		
			            		thh += '<th style="text-align:left !important;"><span class="kh">'+ data.title_data.subject_kh + '</span><span class="en-title">&nbsp;(' + data.title_data.subject +')</span></th>';				            	
			            	}
			            	else //show monthly and semester header
			            	{ 
			            		if(data.header.length > 0){
				            		$.each(data.header, function(kh, vh){
					            		th += '<th width="70"><span class="kh">'+ vh.assess_name_kh + '</span><br><span class="en">' + vh.assess_name_en +'</span></th>';	            		
					            		
					            		thh += '<th style="text-align:left !important;"><span class="kh">'+ vh.assess_name_kh + '</span><br><span class="en-title">' + vh.assess_name_en +'</span></th>';
					            		tdd += '<td>'+ (vh.percentage * 100) +'&nbsp;&nbsp; %</td>';
					            	});	
				            	}
				            	else
				            	{
				            		th += '<th width="70"><span class="kh">ពិន្ទុ'+ data.title_data.subject_kh + '</span><br><span class="en">' + data.title_data.subject +' Score</span></th>';
				            		
				            		thh += '<th style="text-align:left !important;"><span class="kh">'+ data.title_data.subject_kh + '</span><span class="en-title">&nbsp;(' + data.title_data.subject +')</span></th>';
				            		tdd += '<td>'+ (data.title_data.score_percence * 100) +'&nbsp;&nbsp; %</td>';
				            	}
			            	}			            				            	

			            	//get table body
			            	$.each(data.student, function(k,v){
			            		var td = ''; var is_subject = 0; $css = ''; var get_total_score = '';
			            		$avg_coeffict_val = ''; //(មេគុណ)
								
			            		if(select_exam_type == 3){ //show td of final result
			            			var avg_semester1 = ''; var avg_semester2 = ''; 

			            			if(v.total_avg_score_s1 == "" || v.total_avg_score_s1 == 0){			            				
			            				avg_semester1 = v.get_semester_score[0].total_average_score-0;
			            			}else{
			            				avg_semester1 = v.total_avg_score_s1-0;
			            			}

			            			if(v.total_avg_score_s2 == "" || v.total_avg_score_s2 == 0){
			            				avg_semester2 = v.get_semester_score[1].total_average_score-0;	
			            			}else{
				            			avg_semester2 = v.total_avg_score_s2-0;
			            			}
		            				
		            				var semester1 = 0; $s1_css = '';
		            				if(avg_semester1 == 0){
		            					$s1_css = 'color:red';			            					
		            				}else{ 
		            					semester1 = avg_semester1-0;
		            				}			            				
		            				
		            				var semester2 = 0; $s2_css = '';
		            				if(avg_semester2 == 0){
		            					$s2_css = 'color:red';			            					
		            				}else{
		            					semester2 = avg_semester2-0;
		            				}	
		            				
		            				get_total_score = (semester1 + semester2)-0;
		            				td += '<td style="padding:0;">'+
				            					'<input type="text" name="total_avg_score_s1[]" class="total_avg_score_s1 final_score" style="'+ $s1_css +'" readonly value="'+  Math.round(semester1 * 100)/100 +'">'+			            					
				            			  '</td>'+
				            			  '<td style="padding:0;">'+
				            					'<input type="text" name="total_avg_score_s2[]" class="total_avg_score_s2 final_score" style="'+ $s2_css +'" readonly value="'+ Math.round(semester2 * 100)/100 +'">'+			            					
				            			  '</td>';
			            		}
			            		else //show td of monthly and semester result
			            		{
			            			if(data.header.length > 0){
				            			$.each(data.header, function(ke, ve){
					            			var get_input_score = '';
					            			if(v.check_assessment.length > 0){
					            				$.each(v.check_assessment, function(i, vals){
						            				if(vals.assessment_id == ve.assess_id){
						            					get_input_score = vals.input_score;
						            				}
						            			});
					            			}
					            			
					            			if(get_input_score > 0 || get_input_score == ""){
					            				$css = 'color:#000';
					            			}else{
					            				$css = 'color:red';
					            			}            			
					            			td += '<td style="padding:0;">'+
						            					'<input type="text" name="input_score[]" class="input_score" value="'+ get_input_score +'" style="'+ $css +'">'+
						            					'<input type="hidden" name="assessment_id[]" class="assessment_id" value="'+ ve.assess_id +'">'+
						            					'<input type="hidden" name="is_subject[]" class="is_subject" value="'+ is_subject +'">'+
						            					'<input type="hidden" name="percentage[]" class="percentage" value="'+ ve.percentage +'">'+
						            			  		'<input type="hidden" class="ass_max_score check_max_score" value="'+ ve.ass_max_score +'">'+
						            			  '</td>';
					            		});
				            		}
				            		else
				            		{
				            			is_subject = 1;
				            			var input_score = '';
				            			$.each(v.check_assessment, function(i, vals){		            				
			            					input_score = vals.input_score;	            				
				            			});

				            			if(input_score > 0){
				            				$css = 'color:#000';
				            			}else{
				            				$css = 'color:red';
				            			}     
				            			td += '<td style="padding:0;">'+
					            					'<input type="text" name="input_score[]" class="input_score" value="'+ input_score +'" style="'+ $css +'">'+
					            					'<input type="hidden" name="assessment_id[]" class="assessment_id" value="'+ data.title_data.subjectid +'">'+
					            					'<input type="hidden" name="is_subject[]" class="is_subject" value="'+ is_subject +'">'+
					            					'<input type="hidden" name="percentage[]" class="percentage" value="'+ data.title_data.score_percence +'">'+
					            					'<input type="hidden" class="max_score check_max_score" value="'+ data.title_data.max_score +'">'+				            					
					            			  '</td>';				            		
				            		}
			            		}          		
			            		
			            		//check with averag_coefficient
			            		$set_color = '';
			            		if(v.averag_coefficient != null || v.averag_coefficient != undefined || v.averag_coefficient > 0){
			            			$avg_coeffict_val = v.averag_coefficient;
			            			if($avg_coeffict_val > 0){
			            				$set_color = 'color:#000';
			            			}else{
			            				$set_color = 'color:red';
			            			}
			            		}            		
			            		var totalscore=(v.total_score>0?v.total_score:Math.round(get_total_score * 100)/100);
			            		tr += '<tr>'+
			            					'<td style="text-align: center !important;">'+ (k+1) +'</td>'+
			            					'<td style="text-align: left !important;">'+ v.last_name_kh + '&nbsp;&nbsp; ' + v.first_name_kh +				
			            					'<input type="hidden" name="student_id[]" class="student_id" value="'+ (v.student_id>0?v.student_id:v.studentid) +'">'+
			            					'<input type="hidden" name="subject_score_id[]" class="subject_score_id" value="'+ (v.subject_score_id>0?v.subject_score_id:"") +'"></td>'+
			            					'<td style="text-align: center !important;">'+ (v.gender == "female" ? "F" : "M") +'</td>'+ td +
			            					'<td><input type="text" name="total_score[]" class="total_score" value="'+ totalscore +'" readonly style="font-weight:600; color:#337ab7;"></td>';
			            					if(select_exam_type != 3){
				            					tr += '<td style="text-align: center !important;"><input type="text" name="averag_coefficient[]" class="averag_coefficient" style="'+ $set_color +'" value="'+ $avg_coeffict_val +'"></td>';
				            				}else{
				            					tr += '<td><input type="text" name="average_score[]" class="average_score final_score" style="font-weight:600; color:#337ab7;" value="'+ Math.round((get_total_score/2)*100)/100 +'"></td>';
				            				}			            					
					    				tr += '<td style="text-align: center !important;"></td>'+
						    				  '<td style="text-align: left !important;"><input type="text" name="comments[]" class="comments" value="'+ (v.comments!=null?v.comments:"") +'" style="width:160px; height:25px;"></td>'+
						    				  '<td style="text-align: center !important;"><input type="checkbox" name="none_subject[]" class="none_subject" value="'+(v.none_subject==""||v.none_subject==null||v.none_subject==undefined?1:v.none_subject)+'"></td>'+
				            				'</tr>';
			            	});   		

			            	div += '<div class="title-info">'+
			            				'<table border="0" width="100%" style="line-height:20px;">'+
			            					'<tr>'+
			            						'<td width="160">- Name of the course:</td>'+
			            						'<td class="kh-title" colspan="'+ countCols +'">'+ data.title_data.subject +'</td>'+
			            					'</tr>'+
			            					'<tr>'+
			            						'<td>- ID of the course:</td>'+
			            						'<td class="kh-title" colspan="'+ countCols +'">'+ data.title_data.subject +'</td>'+
			            					'</tr>'+
			            					'<tr>'+
			            						'<td>- Teacher(s) in charge:</td>'+
			            						'<td class="kh-title" colspan="'+ countCols +'">'+ data.title_data.first_name + "&nbsp;&nbsp;" + data.title_data.last_name +'</td>'+
			            					'</tr>'+
			            					'<tr>'+
			            						'<td>- Coefficients:</td>'+ thh +
			            					'</tr>';
							            	if(tdd != ""){
							            		div += '<tr>'+
						            						'<td>- percentage (100 %):</td>'+ tdd +
						            					'</tr>';
							            	}			            					
			            	div += '</table></div>';

			            	table += '<table border="0"​ align="center" id="listsubject" class="table table-bordered table-student-detail">'+
			            				'<thead class="theads">'+
				            				'<tr>'+
				            					'<th width="50"><span class="kh">ល.រ</span><br><span class="en">No</span></th>'+
				            					'<th><span class="kh">ឈ្មោះសិស្ស</span><br><span class="en">Student Name</span></th>'+
				            					'<th width="30"><span class="kh">ភេទ</span><br><span class="en">Sex</span></th>'+ th;
				            					if(select_exam_type != 3){
				            						table += '<th width="70"><span class="kh">លទ្ធផល<br>ប្រលង</span><br><span class="en">Final Result</span></th>'+
				            								 '<th width="70"><span class="kh">មេគុណ</span><br><span class="en">Coefficients</span><br><input type="text" class="coefficients" style="color:#000 !important; height:17px;"></th>';				            						
				            					}else{
				            						table += '<th><span class="kh">ពិន្ទុមធ្យមភាគសរុប</span><br><span class="en">Total Average Score</span></th>'+
				            								 '<th><span class="kh">លទ្ធផលប្រលង</span><br><span class="en">Final Result</span></th>';
				            					}				            					
				            			table += '<th width="50"><span class="kh">ចំណាត់<br>ថ្នាក់</span><br><span class="en">Ranking</span></th>'+
				            					 '<th width="160"><span class="kh">មតិយោបល់</span><br><span class="en">Comments</span></th>'+
				            					 '<th width="50"><span class="kh">មុខវិជ្ជា<br>អត់រៀន</span><br><span class="en">None<br>subject</span></th>'+
				            				'</tr>'+
				            			'</thead>'+
				            			'<tbody class="tbodys">'+ tr +'</tbody>';
			            	table += '</table>';

			            	$('.div-student-detail').append(div);
			            	$('.div-student-detail').append(table);
			            	if(select_exam_type == 3){
			            		$('.total_score').addClass('final_score');
			            	}else{
			            		$('.total_score').removeClass('final_score');
			            	}
			            	//check subject which student learn or not
			            	$('.none_subject').each(function(){
			            		var par = $(this).parent().parent();
			            		var none_sub_val = $(this).val();
			            		if(none_sub_val != 1){
			            			$(this).prop('checked', true);
			            		}else{
			            			$(this).prop('checked', false);
			            		}
			            	});
			            	//end-----------------------------------

			            	$('#btnsave').show();
		            	}
		            	else
		            	{
		            		var mess = '<div class="alert alert-danger" style="margin:5px 0 5px 0; padding:5px 0 7px 12px;">No Data to Display !!!</div>'
		            		$('.div-student-detail').append(mess);
		            		$('#btnsave').hide();
		            	}
		            }
		        });	
			}
		});

		//check subject which student learn or not
		$('body').delegate('.none_subject', 'click', function(){
			if($(this).is(':checked')){
				$(this).val(0);
			}else{
				$(this).val(1);
			}
		});

		//calculate total score------------------------------------
		$('body').delegate('.input_score', 'change', function(){
			var tr = $(this).parent().parent();
			var total_score = 0; 
			tr.find('.input_score').each(function(){
				var td = $(this).parent();
				var scores = $(this).val()-0;
				var percentage = td.find('.percentage').val();
				var max_score = td.find('.check_max_score').val();

				if(scores > 0){
    				td.find('.input_score').css('color','#000');
    			}else{
    				td.find('.input_score').val(0);
    				td.find('.input_score').css('color','red');
    			} 

				if(scores > max_score){
					$('<div class="check_score">'+
		                	'<div style="border: none;padding: 9px 0 9px 0; height:auto; margin:0 0 8px 8px; width:95%;">'+
		                  		'<span style="color:#2d80c3; margin-left:5px; font-weight:bold; font-size:9pt;">Input score should be equal or less than '+ max_score +' score !</span>'+ 
		                	'</div>'+
		               '</div>')
		               .dialog({
			                height:'auto',
			                width: 430,
			                modal: true,
			                resizable:false,
			                title: 'Check Score',
			                dialogClass: 'noTitleStuff',                           
			                buttons: {
			                  "OK": function (event) {
			                    $(this).dialog('destroy').remove();
			                    td.find('.input_score').val('');                        
			                  }
			                }
			            });
		               	$('.ui-dialog-titlebar-close').remove();
				}else{
					total_score += (scores * percentage)-0;					
				}						
			});

			if(total_score > 0){
				tr.find('.total_score').val(number_format(total_score, 2));
			}else{
				tr.find('.total_score').val('');
			}			
		});

		//input coefficient (មេគុណ)-------------------------------
		$('body').delegate('.coefficients', 'change', function(){
			var coefic_val = $(this).val();
			if(coefic_val != ""){
				$('.averag_coefficient').each(function(){
					$(this).val(coefic_val);
					var tr = $(this).parent().parent();
					var co_val = $(this).val();
					if(co_val <= 0){						
						tr.find('.averag_coefficient').css('color', 'red');		
					}else{
						tr.find('.averag_coefficient').css('color', '#000');
					}
				});
			}else{ $('.averag_coefficient').val(''); }			
		});

		$('body').delegate('.averag_coefficient', 'change', function(){
			$('.averag_coefficient').each(function(){
				var tr = $(this).parent().parent();
				var co_val = $(this).val();
				if(co_val <= 0 || co_val == ""){
					tr.find('.averag_coefficient').val(0);
					tr.find('.averag_coefficient').css('color', 'red');					
				}else{
					tr.find('.averag_coefficient').css('color', '#000');
				}
			});
		});

		//save data------------------------------------------------
		$('#btnsave').on('click', function(){
			// alert(5000);
			var school_id = $('#school_id').val();
			var program_id = $('#program_id').val();
			var school_level_id = $('#school_level_id').val();
			var adcademic_year_id = $('#adcademic_year_id').val();
			var grade_level_id = $('#grade_level_id').val();
			var teacher_id = $('#teacher_id').val();
			var class_id = $('#class_id').val();			
			var subject_id = $('#subject_id').val();
			var subject_group_id = $('#subject_group_id').val();
			var subject_main_id = $('#subject_main_id').val();
			var student_array = [];		
			var exam_type = [];

			var select_exam_type = $('.select_exam_type:checked').val();
			//get monthly value=====================
			if(select_exam_type == 1){
				$exam_monthly = $('.exam_monthly').val();
				exam_type = {'get_exam_type':'monthly','exam_monthly':$exam_monthly};	
			}
			//get semester value====================
			else if(select_exam_type == 2){
				var exam_semester = $('.exam_semester:checked').val();
				$get_monthly_in = [];				
				$('.get_monthly :selected').each(function(i){
					$get_monthly_in[i] = $(this).val();
				});
				exam_type = {'get_exam_type':'semester','exam_semester':exam_semester, 'get_monthly_in':$get_monthly_in};
			}
			//get final value=======================
			else if(select_exam_type == 3){
				$get_semester_in = [];
				$('.get_semester:checked').each(function(i){
					$get_semester_in[i] = $(this).val();
				});
				exam_type = {'get_exam_type':'final', 'get_semester_in':$get_semester_in};
			}				
			
			$('.student_id').each(function(i){
				var tr = $(this).parent().parent();
				var getScore_array = [];

				tr.find('.input_score').each(function(j){
					var td = $(this).parent();
					var input_score = $(this).val()-0;
					var assessment_id = td.find('.assessment_id').val();
					var percentage = td.find('.percentage').val();
					var is_subject = td.find('.is_subject').val();
					var output_score = number_format((input_score * percentage)-0, 2);					
					//if(input_score > 0){
						getScore_array[j] = {
							'assessment_id':assessment_id,
							'is_subject':is_subject,
							'percentage':percentage,
							'input_score':input_score,
							'output_score':output_score
						};
					//}	
				});

				var total_avg_score_s1 = ""; 
				var total_avg_score_s2 = ""; 
				var none_subject = "";

				if(select_exam_type == 3){
					total_avg_score_s1 = tr.find('.total_avg_score_s1').val();
					total_avg_score_s2 = tr.find('.total_avg_score_s2').val();
				}
				else{
					none_subject = tr.find('.none_subject').val();
				}

				var student_id = $(this).val();				
				var subject_score_id = tr.find('.subject_score_id').val();
				var total_score = tr.find('.total_score').val()-0;
				var averag_coefficient = tr.find('.averag_coefficient').val();
				var comments = tr.find('.comments').val();				
				//if(total_score > 0){
					student_array[i] = {
						'student_id': student_id,
						'subject_score_id':subject_score_id,
						'total_score':total_score,
						'comments':comments,
						'none_subject':none_subject,
						'averag_coefficient':averag_coefficient,
						'total_avg_score_s1':total_avg_score_s1,
						'total_avg_score_s2':total_avg_score_s2,
						'getScoreDetail':getScore_array
						//'getScoreDetail':getScore_array.filter(Boolean)  // getScore_array.filter(v=>v!='')
					};
				//}	    		
			});
			// console.log(student_array.filter(Boolean));
			// return false;
			
			//if($('#frm_subject_score_kgp').parsley().validate()){
				$.ajax({
		            url: "<?php echo site_url('student/c_subject_score_entry_kgp/saveSubjectScoreEntry');?>",
		            dataType: "Json",
		            type: "POST",
		            async: false,
		            data: {
		                'school_id' : school_id,
						'program_id' : program_id,
						'school_level_id' : school_level_id,
						'adcademic_year_id' : adcademic_year_id, 
						'grade_level_id' : grade_level_id,
						'teacher_id' : teacher_id,
						'class_id' : class_id,		
						'subject_id' : subject_id,
						'subject_group_id' : subject_group_id,
						'subject_main_id' : subject_main_id,
						'exam_type' : exam_type,
		                'student_array': student_array
		            },
		            success: function (data) {
		            	if (data.res == 1) {                            
	                    	toastr["success"]('Data has been saved!');
	                    	 
	                    }
	                    if (data.res == 2) {                        
	                        toastr["success"]('Data has been updated!');
	                    }
	                   //  window.open("<?php echo site_url('student/c_subject_score_entry_kgp');?>", "_self");
		            }
	            });
			//}	
		});

	});

	/*----------------------------------------FUNCTION------------------------------------*/

	//function ajax to get data
	function getDataByAjax(selector, url, data){
		$.ajax({
            url: url,	            
            type: "post",
            dataType: 'html',
            data: data,
            success: function (data) {
            	selector.html(data);		            	
            }
        });	
	}

</script>