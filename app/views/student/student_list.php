<style type="text/css">
	table tbody tr td img{width: 15px; margin-right: 10px}
	a,.sort{cursor: pointer;}
	.cur_sort_up{
		background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	.cur_sort_down{
		background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	.mes_suc {
		color: green !important;
	}
	.mes_err {
		color: red !important;
	}
</style>
<?php
$m="";
$p="";
if(isset($_GET['m'])){
    $m=$_GET['m'];
}
if(isset($_GET['p'])){
    $p=$_GET['p'];
}
?>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      	<div class="result_info">
		      	<div class="col-xs-3">
		      		<strong id='title'>Student List <?php echo $this->session->userdata('match_con_posid');?></strong>
		      	</div>
		      	<div class="col-xs-4 hide" align="right">
		      		<strong id='title'><span class="messages_show"></span></strong>
		      	</div>
		      	<div class="col-xs-9" style="text-align: right">
                    <span class="top_action_button">
		      			<?php if($this->green->gAction("P")){?>
                            <a href="#" id="refresh" title="Refresh">
                                <img src="<?php echo base_url('assets/images/icons/refresh.png')?>" />
                            </a>
                        <?php } ?>
		      		</span>
		      		<span class="top_action_button">
		      			<?php if($this->green->gAction("I")){ ?>
				    		<a href="<?php echo site_url('student/student/import')?>" >
				    			<img src="<?php echo base_url('assets/images/icons/import.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>
			    	<span class="top_action_button">	
			    		<?php if($this->green->gAction("E")){ ?>
				    		<a href="JavaScript:void(0);" id="exports" title="Export">
				    			<img id='export' src="<?php echo base_url('assets/images/icons/export.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>
		      		<span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ ?>
							<a href="JavaScript:void(0);" id="print" title="Print">
				    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
				    		</a>
				    	<?php } ?>
		      		</span>	

                    <span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ $yearid=$this->session->userdata('year'); if(isset($_GET['y'])) $yearid=$_GET['y']?>
                            <a href="<?php echo site_url("student/student/previewmulti/?yearid=$yearid")?>" id="print_multi" title="print_multi" target="_blank">
                                <img src="<?php echo base_url('assets/images/icons/preview.png')?>" />
                            </a>
                        <?php } ?>
		      		</span>
			    	<span class="top_action_button">
			    		<?php if($this->green->gAction("C")){ ?>
				    		<a href="<?php echo site_url("student/student/add?m=$m&p=$p")?>" target="_blank">
				    			<img src="<?php echo base_url('assets/images/icons/add.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>	      		
		      	</div> 			      
		  	</div>
	    	<div class="row-1">
		      	<div class="col-sm-12" >
		      		<div class="panel panel-default">
		      			<div class="panel-body">
			           		<div class="table-responsive" id="export_tap">
			           			<?php 
			           			$thr="";	
			           			$tr="";	
			           			$school=$this->db->where('schoolid',$this->session->userdata('schoolid'))->get('sch_school_infor')->row();
			           			$school_name=$school->name;
			           			$school_adr=$school->address;
			           			foreach($thead as $th=>$val){
			           				if($th=='No' || $th=='Nationality' || $th=='Class')
			           					$thr.="<th class='$val'>".$th."</th>";
			           				else if($th=='Action')
			           					$thr.="<th class='remove_tag' style='min-width:150px; text-align:center;'> ".$th."</th>";
			           				else
			           					$thr.="<th class='sort $val' onclick='sort(event);' rel='$val'>".$th."</th>";								
			           			}
			           			if(count($tdata)>0){
			           				$i=1;
			           				if(isset($_GET['per_page']))
										$i=$_GET['per_page']+1;
									
									foreach($tdata as $row){
										if($row['familyid'] !='' && $row['familyid'] !=0){											
											$family_code=$this->db->query("SELECT family_code FROM sch_family WHERE familyid='".$row['familyid']."'")->row()->family_code;
										}
										else{
											$family_code='';
										}
										$no_imgs = base_url()."assets/upload/No_person.jpg";	
										$have_img = base_url()."assets/upload/students/".$row["yearid"].'/thumbs/'.$row["studentid"].'.jpg';
										$img = '<img src="'.$no_imgs.'" class="img-circle" alt="No Image" style="width:70px !important; height:70px !important;">';
										if (file_exists(FCPATH . "assets/upload/students/".$row["yearid"].'/thumbs/'.$row["studentid"].'.jpg')) {				
										$img = '<img src="'.$have_img.'" class="img-circle" alt="No Image" style="width:70px !important; height:70px !important;">';
										}
										$tr.="<tr>
											 <td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
											 <td>".$img."</td>											 
											 <td class='student_num'>".$row['student_num']."</td>
											 <td class='last_name'>
											 	<span>".$row['last_name_kh']." ".$row['first_name_kh']."</span><br>
											 	<span>".$row['last_name']." ".$row['first_name']."</span>											 	
											 </td>
											 <td class='phone1'>".$row['phone1']."</td>
											 <td class='dateofbirth'>".$row['dob']."</td>
											 <td class='register_date'>".$row['register_date']."</td>
											 <td class='class_name'>".$row['class_name']."</td>
											 <td class='yearid'>".$row['sch_year']."</td>
											 <td class='FamilyID'><a href='".site_url("social/family/preview/$row[familyid]")."' target='_blank'>$family_code</a></td>
											 <td class='remove_tag'>";
											if($this->green->gAction("P")){	
												 $tr.="<a>
												 		<img rel=".$row['studentid']." onclick='previewhistory(event);' src='".site_url('../assets/images/icons/a_preview.png')."'/>
												 	</a>
												 	<a>
												 		<img yearid='".$row['yearid']."' rel=".$row['studentid']." onclick='previewstudent(event);' src='".site_url('../assets/images/icons/preview.png')."'/>
												 	</a> "; 
											}
											if($this->green->gAction("D")){	
												$tr.="<a href='JavaScript:void(0);'><img rel=".$row['studentid']." class_id='".$row['classid']."' year_id='".$row['yearid']."' onclick='deletestudent(event);' src='".site_url('../assets/images/icons/delete.png')."'/></a>";
											}
											if($this->green->gAction("U")){

	                                            $edit_link=site_url("student/student/edit/".$row['studentid'])."?yearid=".$row['yearid']."&m=$m&p=$p" ;
												$tr.="<a href='".$edit_link."' target='_blank'>
	                                                    <img yearid='".$row['yearid']."' rel=".$row['studentid']." src='".site_url('../assets/images/icons/edit.png')."'/>
	                                                  </a>";
											}
											//if($this->green->gAction("R")){
											    $edit_link=site_url("student/student/detail/".$row['studentid'])."?yearid=".$row['yearid']."&classid=".$row['classid'] ;
											    $tr.="<a href='".$edit_link."' target='_blank'>
	                                                    <img yearid='".$row['yearid']."' rel=".$row['studentid']." src='".site_url('../assets/images/icons/detail.png')."'/>
	                                                  </a>";
											//}
											$tr.="</td>
											 </tr>
											 ";										 
										$i++;	 
									}
								}
														
			           			?>
			           			<div>
				           			<table class="table" border='0'>
				           				<thead ><?php echo $thr ?></thead>
				           				
				           				<thead class='remove_tag'>
				           					<?php if($this->session->userdata('match_con_posid')!='stu'){?>
				           					<tr>
				           					<td class='col-xs-1' >
				           						<input type='text' id='page' class='hide' value="<?PHP if(isset($_GET['per_page'])) echo $_GET['per_page'] ?>" />
				           						<input type='text' value='asc' name='sort' id='sort' style='width:30px; display:none'/>			           						
											</td>
											<td class='col-xs-1'>&nbsp;</td>
				           					<td class='col-xs-1'><input type='text' value="<?php if(isset($_GET['s_id'])) echo $_GET['s_id']; ?>"  class='form-control input-sm' name='s_student_id' id='s_student_id'/></td>
									 		<td class='col-xs-2'><input type='text' value="<?php if(isset($_GET['fn'])) echo $_GET['fn']; ?>"  class='form-control input-sm' name='s_full_name' id='s_full_name'/></td>
									 		<td class='col-xs-2'><input type='text' value="<?php if(isset($_GET['fnk'])) echo $_GET['fnk']; ?>" class='form-control input-sm' name='s_phone1' id='s_phone1'/></td>
									 		<td class='col-xs-1'>
									 			<input type='text' value='' name='sortquery' id='sortquery' style='width:80px; display:none'/>
									 			<select class='form-control input-sm parsley-validated' name='s_level' id='s_level' title='Search Level'>
		                   						 <option value="">Level</option>
							                    <?php
							                    	foreach ($this->db->get('sch_grade_level')->result() as $level) {?>
							                    		<option value="<?php echo $level->grade_levelid ;?>" <?php if(isset($_GET['le'])){ if($level->grade_levelid==$_GET['le']) echo 'selected'; }?> ><?php echo $level->grade_level;?></option>
							                    	<?php }
							                    ?>
						                 		</select>
									 		</td>
									 		<td class='col-xs-1'>
									 			<select class='form-control input-sm parsley-validated' name='s_classid' id='s_classid'>
		                   						 <option value="">Class</option>
							                    <?php
							                    	foreach ($this->std->getclass()->result() as $class) {?>
							                    		<option value="<?php echo $class->classid ;?>" <?php if(isset($_GET['class'])){ if($class->classid==$_GET['class']) echo 'selected'; }?> ><?php echo $class->class_name;?></option>
							                    	<?php }
							                    ?>
						                 		</select>
						              		</td>
						              		<td class='col-xs-4'>
									 			<select class='form-control input-sm parsley-validated' name='s_year' id='s_year'>
		                   						 <option value="">Year</option>
							                    <?php
							                    	foreach ($this->std->getyear()->result() as $year) {?>
							                    		<option value="<?php echo $year->yearid ;?>"><?php echo $year->sch_year;?></option>
							                    	<?php }
							                    ?>
						                 		</select>
						              		</td>
											
						              		<td ><input type="button" name="search" id="search" class="btn btn-primary" value="Search" style="height: 30px !important;"></td>
				           					<td >
									 			<div class="form_sep hide" id='promo_sep' style="width:150px;">
								                  <select data-required="true" class="form-control parsley-validated input-sm" name="promot_id" id="promot_id">
								                    <option value="">---Select Promotion---</option>
								                    <?php
								                    	$school=$this->session->userdata('schoolid');
								                    	foreach ($this->db->where('schoolid',$school)->get('sch_school_promotion')->result() as $pro) {?>
								                    		<option value="<?php echo $pro->promot_id ;?>" ><?php echo $pro->proname;?></option>
								                    	<?php }
								                    ?>
								                  </select>
								                </div> 
								            </td>
								        </tr>
								            <?php }?>								 		
				           				</thead>
				           				
				           				<tbody class='listbody'>
				           					<?php echo $tr ?>
				           					<tr class='remove_tag'>
												<td colspan='12' id='pgt'>
													<div style='margin-top:20px; width:11%; float:left;'>
													Display : <select id="sort_num"  style='padding:5px; margin-right:0px;' onchange='search(event);'>
																	<?php
																	$num=10;
																	for($i=0;$i<10;$i++){?>
																		<option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
																		<?php $num+=10;
																	}
																	?>
																	<option value='all' <?php if(isset($_GET['s_num'])){ if($_GET['s_num']=='all') echo 'selected'; }?>>All</option>
																</select>
													</div>
													<div style='text-align:center; verticle-align:center; width:89%; float:right;'>
														<ul class='pagination' style='text-align:center'>
															
															<?php echo $this->pagination->create_links(); ?>
														</ul>
													</div>
												</td>
											</tr> 
				           				</tbody>
				           			</table>
			           			</div>
							</div>  
						</div>
		      		</div>
		      	</div>	      	
		    </div>
	    </div>
   </div>
</div>
<div class="modal fade bs-example-modal-sm" id='loading' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
         <div class="modal-content">
			<img src="<?php echo base_url().'assets/images/icons/loading.gif'?>">
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('body').delegate('#exports', 'click', function(event){
            $('.table').attr('border',1);
            var data = $("#export_tap").html().replace(/<img[^>]*>/gi,"");
            var export_data=$("<div>"+data+"</div>").clone().find(".remove_tag").remove().end().html(); 
            this.href = ('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
            this.download = "Student List.xls";
            $('.table').attr('border',0);
        });
        $('body').delegate('#print', 'click', function(event){
            prints();
        });
		selectschlevel();
		
		$('#promot_id').on('change', function() {
	        search();
		});
	    

		$('#search').click(function(){
			var classid=$("#s_classid").val();
			if(classid !="" || classid != 0)
			{
				search_class();
			}else{
				$('#schlevelid').val('');
				search();
			}		
		});
	});
	
	function gsPrint(emp_title,data){
		 var element = "<div>"+data+"</div>";
		 $("<center><p style='padding-top:15px;text-align:center;'><b>"+emp_title+"</b></p><hr>"+element+"</center>").printArea({
		  mode:"popup",            
		  popHt: 600 ,
		  popWd: 500,
		  popX: 0 ,
		  popY: 0 ,
		  popTitle:"test",
		  popClose: false,
		  strict: false 
		  });
	}

	function selectschlevel(){
		var schlevelid="<?php if(isset($_GET['l'])) echo $_GET['l'] ?>";
		$("#schlevelid option[value='"+schlevelid+"']").attr('selected',true);
	}
    $('#refresh').click(function(){
        search();
    });
	function search_class(){
		var classid=$("#s_classid").val();
		$.ajax({
                url:"<?php echo base_url(); ?>index.php/student/Student/getschlevel",    
                data: {'classid':classid},
                type:"post",
                success: function(data){

               	if(data==1){
               		$('#promo_sep').removeClass('hide');
               	}else{
               		$('#promo_sep').addClass('hide');
               	}
            }
        });
        $('#schlevelid').val('');
        search();
	}

	
	function prints(){
		var htmlToPrint = '' +
		        '<style type="text/css">' +
		        'table th, table td {' +
		        'border:1px solid #000 !important;' +
		        'padding;0.5em;' +
		        '}' +
		        '</style>';
		var title="<div style='width:300px; text-align:left;'><span style='font-weight:bold; font-size:16px;'><?php echo $school_name; ?></span><br>";
		var s_adr="";

			title+=s_adr;
			title +="<h4 align='center'>"+ $("#title").text()+"</h4>";
		var year =$("#year :selected").text();
			title+="Year : "+year;
		if($('#s_classid').val()!='')
			title+="<br>Class : " + $('#s_classid :selected').text();
	   	var data = $("#export_tap").html().replace(/<img[^>]*>/gi,"");
	   	var export_data = $("<center>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
	   		export_data+=htmlToPrint;
	   		gsPrint(title,export_data);
	}
	

	function search(event){
		var roleid 	   	 = <?php echo $this->session->userdata('roleid');?>;
		var m="<?php echo isset($_GET['m'])?$_GET['m']:'' ?>";
		var p="<?php echo isset($_GET['p'])?$_GET['p']:'' ?>";
		var year 		 = $('#s_year option:selected').val();
		var studentid 	 = jQuery('#s_student_id').val();
		var full_name 	 = jQuery('#s_full_name').val();
		var phone1       = jQuery('#s_phone1').val();
		var classid 	 = jQuery('#s_classid').val();
		var level 		 = jQuery('#s_level').val();
		var sort_num 	 = $('#sort_num').val();
		var sortstd 	 = $('#sortquery').val();
		var schlevelid 	 = $('#schlevelid').val();
		var promot_id 	 = $('#promot_id').val();
		// if(year==''){
		// 	alert('Please select year first before search!');
		// 	return false;
		// }
		// if(classid==''){
		// 	alert('Please select class first before search!');
		// 	return false;
		// }
		// if(level==''){
		// 	alert('Please select year grade level before search!');
		// 	return false;
		// }
		if(sortstd==''){
			sortstd='order by s.fullname_kh ASC';
		}
		$.ajax({
					url:"<?php echo base_url(); ?>index.php/student/student/search",    
					data: {
						'schlevelid'	: schlevelid,
						'level'			: level,
						'roleid'		: roleid,
						'm'				: m,
						'p'				: p,
						'year'			: year,
						'sort_num'		: sort_num,
						'sort'			: sortstd,
						'studentid'		: studentid,
						'full_name'	   	: full_name,
						'phone1' 	    : phone1,
						'classid'		: classid,
						'promot_id'		: promot_id
					},
					type: "POST",
					dataType:'json',
					async:false,
					success: function(data){
                       jQuery('.listbody').html(data.data);
				},
				 beforeSend: function() { $('.proccess_loading').show(); },
		          complete: function() { $('.proccess_loading').hide();}
			});
		}
	function deletestudent(event){
		var stu_id=jQuery(event.target).attr("rel");
		var class_id = jQuery(event.target).attr('class_id');
		var year_id  = jQuery(event.target).attr('year_id');
		$.ajax({//Check student is evaluated
			type : "POST",
			url  : "<?= site_url('student/student/check_stu_evaluate')?>",
			data : {
				stu_id  : stu_id,
				year_id : year_id,
				class_id: class_id
			},
			success:function(data){

				if(data.isdel == 1){//if data==0 mean student is not evaluated
					var r = confirm("Are you sure! You want to delete this student ?");
					if (r == true) {
						$.ajax({//Delete student
							type : "POST",
							url  : "<?= site_url('student/student/delete')?>",
							data : {
								stu_id  : stu_id,
								year_id : year_id,
								class_id: class_id
							},
							success:function(){
								search();
								toastr["success"]('Student was deleted!');//message when delete success
							}
						});
					}
				}else{//student was evaluate
					toastr["warning"]('This student can not delete!');
				}
			}
		});
	}
	function loading(){
		 $('#loading').modal('show');
          setTimeout(function() { $('#loading').modal('hide'); }, 2000);
	}
	function updateuser(event){
			var student_id=jQuery(event.target).attr("rel");
			var classid=jQuery(event.target).attr("yearid");
			location.href="<?PHP echo site_url('student/student/edit');?>/"+student_id+'?yearid='+classid+"&<?php echo "m=$m&p=$p" ?>";
	}
	function previewstudent(event){
			var year=$(event.target).attr('year');
			var yearid=$(event.target).attr('yearid');
			var student_id=jQuery(event.target).attr("rel");
			window.open("<?PHP echo site_url('student/student/preview');?>/"+student_id+"?yearid="+yearid+"","_blank");
	}
	function previewhistory(event){
			var student_id=jQuery(event.target).attr("rel");
			window.open("<?PHP echo site_url('student/student/previewhistory');?>/"+student_id,"_blank");
	}
	function sort(event)
	{
		var sort=$(event.target).attr('rel');
		var ex_sort="";
		var sorttype=$('#sort').val();
		var classid=$("#s_classid").val();
		if(sorttype=='asc'){
			$('#sort').val('desc');
			$('.sort').removeClass('cur_sort_down');
			$(event.target).addClass('cur_sort_up');
		}
		else{
			$('#sort').val('asc');
			$('.sort').removeClass('cur_sort_up');
			$(event.target).addClass('cur_sort_down');
		}
		
		if(sort!=""){
			ex_sort=" classid ASC , ";
		}	
		//alert('sort : '+ sort);
		$('#sortquery').val('ORDER BY'+ex_sort+sort+' '+sorttype);

		search();
		//loading();
	}
	var showsms = function(sms='',messages_show=''){
	  if(sms!=""){
	   $('.messages_show').html(sms).slideDown(1000).delay(3000).slideUp(500).addClass(messages_show);
	  }
	}
</script>