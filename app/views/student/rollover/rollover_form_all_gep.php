
<style type="text/css">
	ul,ol{
		margin-bottom: 0px !important;
	}
	a{
		cursor: pointer;
	}
	.ui-autocomplete{z-index: 9999;}
	.datepicker {z-index: 9999;}
</style>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      	<div class="result_info">
				      	<div class="col-sm-6">
				      		<strong>Rollover</strong>				      		
				      	</div>
				      	<div class="col-sm-6" style="text-align: center">
				      		<strong>
				      			<center class='error' style='color:red;'><?php if(isset($error)) echo "$error"; ?></center>
				      		</strong>	
				      	</div>
			</div> 
	       <!-- <form enctype="multipart/form-data" id='frmtreatment' action="" method="POST"> -->
		        <div class="row">
		        	<div class="col-sm-12">
						<div class="panel panel-default">
		            		<div class="panel-heading">
				               <h4 class="panel-title">RollOver Details</h4>
				        	</div>
				          	<div class="panel-body">
				          			<div class="col-sm-12">
				          				<div class="col-sm-2"><label>From Year</label></div>
					          			<div class="col-sm-10">
					          				<div class="col-sm-12">
												<div class="col-sm-4">
								                  	<label class="req" for="schoolid">School</label>
									                <select data-required="true" class="form-control input-sm parsley-validated" att_f="100" name="schoolid" id="schoolid" >
									                  	<option value=''></option>
									                   	<?php 
									                   		$schid = $this->roall->schoolid();
									                   		if($schid->num_rows() > 0){
									                   			foreach($schid->result() as $rsch){
									                   				echo "<option value='".$rsch->schoolid."'>".$rsch->name."</option>";
									                   			}
									                   		}
									                   	?>
						                  			</select>
									            </div>
									            <div class="col-sm-4">	
									                <label class="req" for="program">Program</label>
										            <select data-required="true"  minlength='1' class="form-control" name="program" id="program"></select>
										        </div>	
									            <div class="col-sm-4">	
									                <label class="req" for="schoollevel">School Level</label>
										            <select data-required="true"  minlength='1' class="form-control" name="schoollevel" id="schoollevel"></select>
										        </div>
							            	</div>
							            	<div class="col-sm-12">
							            		<div class="col-sm-4">
								                  	<label class="req" for="yearid">Year</label>
									                <select data-required="true" class="form-control input-sm parsley-validated" name="yearid" value="" id="yearid" ></select>
									            </div>
									            <div class="col-sm-4">	
								                  	<label class="req" for="ranglevel">Rang lavel</label>
									                <select data-required="true" minlength='1' class="form-control parsley-validated" name="ranglevel" id="ranglevel"> </select>
									            </div>	
								        	 	<div class="col-sm-4">	
								                  	<label class="req" for="gradelavel">Grade Level</label>
									                <select data-required="true" minlength='1' class="form-control parsley-validated" name="gradelavel" id="gradelavel"> </select>
									            </div>
								        	</div>
								        	<div class="col-sm-12">
								        		<div class="col-sm-4" id='class_select'>
								                  	<label class="req" for="classid">Class</label>
									                <select data-required="true" minlength='1' class="form-control parsley-validated" name="classid" id="classid"></select>
									            </div>
								        	</div>
								        	<div class="col-sm-12" style="margin-top: 10px;">
								        		<div class="col-sm-12">
								        			<button id="search" name="search" type="button" class="btn btn-success">Search</button>
								        		</div>
								        	</div>
					          			</div>
							        </div>

							        <div class="col-sm-12"><hr style="border-width: 1px dotted black;"></div>
							        <div class="col-sm-12">
				          				<div class="col-sm-2"><label>To Year</label></div>
					          			<div class="col-sm-10">
					          				<div class="col-sm-12">
												<div class="col-sm-4">
								                  	<label class="req" for="toschoolidlabel">School</label>
									                <select class="form-control input-sm parsley-validated" name="toschoolid" id="toschoolid" >
									                   <option value=''></option>
									                   	<?php 
									                   		$schid = $this->roall->schoolid();
									                   		if($schid->num_rows() > 0){
									                   			foreach($schid->result() as $rsch){
									                   				echo "<option value='".$rsch->schoolid."'>".$rsch->name."</option>";
									                   			}
									                   		}
									                   	?>
						                  			</select>
									            </div>	
									             <div class="col-sm-4">	
									                <label class="req" for="toprogram">Program</label>
										            <select data-required="true"  minlength='1' class="form-control" name="toprogram" id="toprogram"></select>
										        </div>
									            <div class="col-sm-4">	
								                  	<label class="req" for="toschoollevellabel">School Level</label>
									                <select class="form-control" name="toschoollevel" id="toschoollevel"></select>
							            		</div>
							            	</div>
							            	<div class="col-sm-12">
							            		<div class="col-sm-4">
								                  	<label class="req" for="toyearidlabel">Year</label>
									                <select class="form-control input-sm parsley-validated" name="toyearid" value="10" id="toyearid" >
									                   <option value=''></option>
						                  			</select>
									            </div>	
									            <div class="col-sm-4">	
								                  	<label class="req" for="toranglevel">Rang lavel</label>
									                <select data-required="true" minlength='1' class="form-control parsley-validated" name="toranglevel" id="toranglevel"> </select>
									            </div>
								        	 	<div class="col-sm-4">	
								                  	<label class="req" for="togradelavellabel">Grade Level</label>
									                <select class="form-control parsley-validated" name="togradelavel" id="togradelavel"></select>
										        </div>
									        </div>
									        <div class="col-sm-12"> 
									            <div class="col-sm-4" id='class_select'>
								                  	<label class="req" for="toclassidlabel">Class</label>
									                <select class="form-control parsley-validated" name="toclassid" id="toclassid"></select>
									            </div>
								        	</div>
					          			</div>
							        </div>      
							    </div>
						</div>
					</div>
				</div>
				
		 	<!-- </form> -->
	    </div>
	    <div class="row">
			<div class="col-sm-12">
			    <div class="form_sep" style="text-align: center;">
			      <button id="save_st_rollover" name="save_st_rollover" type="button" class="btn btn-success">Save</button>
			      <button id="btncancel" type="button" class="btn btn-warning">Cancel</button>
			      <button id="btndelete" type="button" class="btn btn-danger">Delete</button>
			    </div>
			</div>
	    </div>
	    <div class="row">
	    	<div class="col-sm-12">
	    		<table class="table">
	    			<thead>
	    				<tr>
	    					<th>No</th>
	    					<th>Student Name</th>
	    					<th>School Level</th>
	    					<th>Year</th>
	    					<th>Grade Level</th>
	    					<th>Class</th>
	    					<th><label><input type="checkbox" name="checkall" id="checkall" title="check rollover all"></label></th>
	    				</tr>
	    			</thead>
	    			<tbody id="tbl_data"></tbody>
	    		</table>
	    	</div>
	    </div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		var arr_data = new Object();
		$("body").delegate("#schoolid,#toschoolid","change",function(){
			var ch_id = $(this).attr('id');
			var url = "<?php echo site_url('student/c_rollover_all_gep/show_program');?>";
			if(ch_id == 'schoolid'){
				var opt_data = show_option(url,1);
				$("#program").html(opt_data);
			}else{
				var opt_data = show_option(url,2);
				$("#toprogram").html(opt_data);
			}
		});
		$("body").delegate("#program,#toprogram","change",function(){
			var ch_id = $(this).attr('id');
			var url = "<?php echo site_url('student/c_rollover_all_gep/show_school_level');?>";
			if(ch_id == 'program'){
				var opt_data = show_option(url,1);
				$("#schoollevel").html(opt_data);
			}else{
				var opt_data = show_option(url,2);
				$("#toschoollevel").html(opt_data);
			}
		});
		$("body").delegate("#schoollevel,#toschoollevel","change",function(){
			var ch_id = $(this).attr('id');
			var url = "<?php echo site_url('student/c_rollover_all_gep/show_school_year');?>";
			var url_ranglevel = "<?php echo site_url('student/c_rollover_all_gep/show_sch_ranglevel');?>";
			if(ch_id == 'schoollevel'){
				var opt_data = show_option(url,1);
				$("#yearid").html(opt_data);
				var opt_ranglevel = show_option(url_ranglevel,1);
				$("#ranglevel").html(opt_ranglevel);
			}else{
				var opt_data = show_option(url,2);
				$("#toyearid").html(opt_data);
				var opt_ranglevel1 = show_option(url_ranglevel,2);
				$("#toranglevel").html(opt_ranglevel1);
			}
		});
		$("body").delegate("#ranglevel,#toranglevel","change",function(){
			var url = "<?php echo site_url('student/c_rollover_all_gep/show_school_grade');?>";
			var ch_id = $(this).attr('id');
			if(ch_id == 'ranglevel'){
				var opt_gradlevel = show_option(url,1);
				$("#gradelavel").html(opt_gradlevel);
			}else{
				var opt_gradlevel1 = show_option(url,2);
				$("#togradelavel").html(opt_gradlevel1);
			}
			
		});
		$("body").delegate("#btndelete","click",function(){
			var conf = confirm("Do you want to delete these student from this class ?");
			if(conf == true){
				var url = "<?php echo site_url('student/c_rollover_all_gep/delete_student_from_class');?>";
				var obj_var = new Object();
				obj_var = {
							'schoolid'    : $("#schoolid").val(),
							'program'     : $("#program").val(),
							'schoollevel' : $("#schoollevel").val(),
							'yearid'      : $("#yearid").val(),
							'gradelavel'  : $("#gradelavel").val(),
							'ranglevel'  : $("#ranglevel").val(),
							'classid'  : $("#classid").val()
						}
				var i = 0;
				var obj_student = new Array();
				$(".chstudent:checked").each(function(){
					var student_id = $(this).parent().parent().find("#studentid").val();
					obj_student[i] = student_id;
					i++;
				})
				var obj_show = {'inf':obj_var,'studentid':obj_student};
				if(i == 0){
					alert("Please check box before this process !");
				}else{
					Ajax_transection(obj_show,url);
				}
				
			}
		});
		$("body").delegate("#gradelavel,#togradelavel","change",function(){
			var url = "<?php echo site_url('student/c_rollover_all_gep/show_school_class');?>";
			var ch_id = $(this).attr('id');
			if(ch_id == 'gradelavel'){
				var opt_data = show_option(url,1);
				$("#classid").html(opt_data);
			}else{
				var opt_data = show_option(url,2);
				$("#toclassid").html(opt_data);
			}
		});
		$("body").delegate("#search","click",function(){
			Search_data();
		});
		$("body").delegate("#btncancel","click",function(){
			window.location.reload(true);
		});
		$("body").delegate("#checkall","click",function(){
			if($(this).is(':checked')){
				$(".chstudent").prop("checked",true);
			}else{
				$(".chstudent").prop("checked",false);
			}
		});
		$("body").delegate("#chstudent","click",function(){
			var ch_count = $(".chstudent:checked").length;
			var un_count = $(".chstudent").length;
			if(ch_count == un_count){
				$("#checkall").prop("checked",true);
			}else{
				$("#checkall").prop("checked",false);
			}
		})
		$("body").delegate("#save_st_rollover","click",function(){
			var schoolid     = $("#schoolid").val();
			var program      = $("#program").val();
			var schoollevel  = $("#schoollevel").val();
			var yearid       = $("#yearid").val();
			var ranglevel    = $("#ranglevel").val();
			var gradelavel   = $("#gradelavel").val();
			var classid      = $("#classid").val();

			var toschoolid     = $("#toschoolid").val();
			var toprogram      = $("#toprogram").val();
			var toschoollevel  = $("#toschoollevel").val();
			var toyearid       = $("#toyearid").val();
			var toranglevel    = $("#toranglevel").val();
			var togradelavel   = $("#togradelavel").val();
			var toclassid      = $("#toclassid").val();
			var leng_ch = $(".chstudent:checked").length;
			var fclass  = $("#classid").val();
			var fyear   = $("#yearid").val();
			if(toschoolid == ""){
				alert("Please choose shool before save.");
				return false;
			}else if(toschoollevel == ""){
				alert("Please choose shool level before save.");
				return false;
			}else if(toyearid == ""){
				alert("Please choose year before save.");
				return false;
			}else if(togradelavel == ""){
				alert("Please choose grade level before save.");
				return false;
			}else if(toclassid == ""){
				alert("Please choose class before save.");
				return false;
			}else if(leng_ch == 0){
				alert("Please check student before save.")
				return false;
			}else if(fclass == toclassid && fyear == toyearid){
				alert("Your class is exist. Please choose other class.");
				return false;
			}
			else{
					var obj_inf = new Object();
					var obj_inf_old = new Object();
					var obj_inf_new = new Object();
					obj_inf_old = {'schoolid':schoolid,'program':program,'schoollevel':schoollevel,'yearid':yearid,'ranglevel':ranglevel,'gradelavel':gradelavel,'classid':classid};
					obj_inf_new = {'toschoolid':toschoolid,'toprogram':toprogram,'toschoollevel':toschoollevel,'toyearid':toyearid,'toranglevel':toranglevel,'togradelavel':togradelavel,'toclassid':toclassid};
					var i = 0;
					$(".chstudent:checked").each(function(){
						var student_id = $(this).parent().parent().find("#studentid").val();
						obj_inf[i] = student_id;
						i++;
					})
					Save_data(obj_inf,obj_inf_old,obj_inf_new);
			}
		})
	});
	function Search_data(){
		var url_search = "<?php echo site_url('student/c_rollover_all_gep/show_student');?>";
		var obj_var = new Object();
		obj_var = {
					'schoolid'    : $("#schoolid").val(),
					'program'     : $("#program").val(),
					'schoollevel' : $("#schoollevel").val(),
					'yearid'      : $("#yearid").val(),
					'rangelevelid':$("#ranglevel").val(),
					'gradelavel'  : $("#gradelavel").val(),
					'classid'  : $("#classid").val(),
				}
		$.ajax({
			type  : "POST",
			url   : url_search,
			dataType:"JSON",
			async : false,
			data  :{
				show_data : 1,
				arr_data  : obj_var
			},
			success:function(data){
				var tr = "";
				if(data != ""){
					var ii=1;
					$.each(data,function(kk,vv){
						tr+='<tr><td>'+ii+'</td>'+
							'<td>'+vv.last_name+'&nbsp;'+vv.first_name+'</td>'+
							'<td>'+vv.sch_level+'</td>'+
							'<td>'+vv.sch_year+'</td>'+
							'<td>'+vv.grade_level+'</td>'+
							'<td>'+vv.class_name+'</td>'+
							'<td style="text-align:center;">'+
							'<input type="checkbox" name="chstudent" class="chstudent" id="chstudent" checked="checked">'+
							'<input type="hidden" name="studentid" class="studentid" id="studentid" value="'+vv.studentid+'">'+
							'</td>'+
							'</tr>';
						ii++;
					});
					$("#checkall").prop("checked",true);
				}else{
					tr+='<tr><td colspan="5" style="text-align:center;"><i>Data not found.</i></td></tr>';
				}
				$("#tbl_data").html(tr);
			}
		});
	}
	function Ajax_transection(data,url){
		$.ajax({
			type  : "POST",
			url   : url,
			dataType:"HTML",
			async : false,
			data  :{
				data_pro : 1,
				arr_data : data
			},
			success:function(data){
				if(data != ""){
					alert(data);
					Search_data();
					//window.location.reload(true)
				}
			}
		});
	}
	function Save_data(obj_student,old_obj_inf,new_obj_inf){
		$.ajax({
			type  : "POST",
			url   : "<?php echo site_url('student/c_rollover_all_gep/save_data');?>",
			dataType:"JSON",
			async : false,
			data  :{
				par : 0,
				arr_student : obj_student,
				arr_inf_old : old_obj_inf,
				arr_inf_new : new_obj_inf
			},
			success:function(data){
				if(data[0] == 1){
					var conf_exit = confirm("Data exist! Do you want to merge this student ?");
					if(conf_exit == true){
						save_data_enrollment(obj_student,old_obj_inf,new_obj_inf);
					}else{
						return false;
					}
				}else{
					var conf = confirm("Do you want to save ?");
					if(conf == true){
						save_data_enrollment(obj_student,old_obj_inf,new_obj_inf);
					}
				}
				//window.location.reload(true)
			}
		});
	}
	function save_data_enrollment(obj_student,old_obj_inf,new_obj_inf){
		$.ajax({
			type  : "POST",
			url   : "<?php echo site_url('student/c_rollover_all_gep/save_data');?>",
			dataType:"JSON",
			async : false,
			data  :{
				par : 1,
				arr_student : obj_student,
				arr_inf_old : old_obj_inf,
				arr_inf_new : new_obj_inf
			},
			success:function(data){
				alert("save success..");
			}
		});
	}
	function show_option(get_url,type_select){
		var opt = "";
		var obj_var = new Object();
		if(type_select == 1){
			obj_var = {
					'schoolid'    : $("#schoolid").val(),
					'program'    : $("#program").val(),
					'schoollevel' : $("#schoollevel").val(),
					'yearid'      : $("#yearid").val(),
					'ranglevel'   : $("#ranglevel").val(),
					'gradelavel'  : $("#gradelavel").val()
				}
		}else{
			obj_var = {
					'schoolid'    : $("#toschoolid").val(),
					'program'    : $("#toprogram").val(),
					'schoollevel' : $("#toschoollevel").val(),
					'yearid'      : $("#toyearid").val(),
					'toranglevel' : $("#toranglevel").val(),
					'gradelavel'  : $("#togradelavel").val()
				}
		}
		
		$.ajax({
			type  : "POST",
			url   : get_url,
			dataType:"HTML",
			async : false,
			data  :{
				show_option : 1,
				arr_data    : obj_var
			},
			success:function(data){
				opt = data;
			}
		})
		return opt;
	}
</script>
