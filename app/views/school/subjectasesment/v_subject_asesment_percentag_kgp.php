<style type="text/css">
    table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }
    .cssclass {
        width: 650px;
    }
    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }
    
    #se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	.to {color:#cc66ff; font-weight:bold;}
	.from {color:#FF0000; font-weight:bold;}
	
</style>
<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }

    // subject --------------------------------------------
    if(isset($getsubjecttype) && count($getsubjecttype)>0){
        $subjecttype="";
        $subjecttype="<option attr_price='0' value=''>--select--</option>";
        foreach($getsubjecttype as $row){
            $subjecttype.="<option value='".$row->subj_type_id."'>".$row->subject_type."</option>";
        }
        
    }
    // subject --------------------------------------------
    if(isset($getschoollavel) && count($getschoollavel)>0){
        $school="";
        $school="<option attr_price='0' value=''>--select--</option>";
        foreach($getschoollavel as $row){
            $school.="<option value='".$row->schlevelid."'>".$row->sch_level."</option>";
        }
        
    }
 ?>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
    <div class="col-sm-12">     
	    <div class="row">
	      	<div class="col-xs-12">
		         <div class="result_info">
		            <div class="col-xs-6">
		                <strong>Asesment Percentag khmer General Programming </strong>  
		            </div>
		            <div class="col-xs-6" style="text-align: right">
		            	<a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
		                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>" width="22px">
		               	</a>
		               	<a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add">
		                  <img src="<?= base_url('assets/images/icons/add.png') ?>" width="22px">
		               	</a>
		            </div>         
		         </div>
	      	</div>
	   	</div>
		<div class="collapse" id="collapseExample">             
		    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
		        
		     	<div class="col-sm-6">
                    <div class="form-group">
                        <label for="schlevel_id">School Level<span style="color:red"></span></label>
                        <select name="schlevel_id" id="schlevel_id" class="form-control input-sm required">
                             <?php echo $school;?> 
                       </select>
                    </div>
                    <div class="form-group">
                        <label for="yearid">Year<span style="color:red"></span></label>
                        <select name="yearid" id="yearid" class="form-control  subjectid">

                       </select>
                    </div>
		     	</div><!-- 1 -->
		     	<div class="col-sm-6">
                    
                     <div class="form-group">
                        <label for="subj_type_id">Subject Type<span style="color:red"></span></label>
                        <select name="subj_type_id" id="subj_type_id" class="form-control">
                            <?php echo $subjecttype;?> 
                       </select>
                    </div>
                    <div class="form-group">
                        <label for="subjectid">Subject<span style="color:red"></span></label>
                        <select name="subjectid" id="subjectid" class="form-control  subjectid">

                       </select>
                    </div>
		     	</div><!-- 2 -->
                <div class="col-sm-12">
                    <div><label for="grade_levelid"></label></div>
                    <div class="panel panel-default">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-hover" id="tbl_gradlevel">
                                <thead>
                                        <tr>
                                            <th style="text-align:center;width: 100px;">Grade Level </th>
                                            <th class="cssclass_" style="text-align:center; width: 397px;">Assesment Name</th>
                                            <th class="cssclass_" style="text-align:center; width:158px;">Max Score</th>
                                            <th style="text-align:center;width:161px;">Percentag(%)</th>
                                        </tr>
                                    </thead>
                            <tbody id="tbody_gradlevel">
                            </tbody>
                        </table>
                    </div>  
                </div><!-- 2 -->
                 <div class="col-sm-7 col-sm-offset-5">
                    <div style="padding-top:10px;" class="form-group">
                      <button id="save" class="btn btn-primary btn-sm" type="button" name="save" value="save">Save</button>&nbsp; 
                      <button id="clear" class="btn btn-warning btn-sm" type="button" name="clear">Clear</button> 
                    </div>            
                </div>
		     	<div class="row">
			        <div class="col-sm-12">
			           <div class="form-group" style="border-bottom: 1px solid #CCC;">&nbsp;</div>           
			        </div>
		     	</div>
		  	</form>
		</div>
    <!-- show header -->
        <div class="col-sm-12">
            <div class="panel panel-default">
               <div class="table-responsive" id="div_export_print">
                    <div id="tab_print">
                        <table width="100%" border="0" class="table table-hover" id="setBorderTbl">
                            <thead>
	                             <tr>
		                              <?php
		                                 foreach ($thead as $th => $val) {
                                            if ($th == 'No'){
                                                echo "<th class='$val'width='5%'>".$th."</th>";
                                            }else if($th == 'Action'){
		                                      	echo "<th class='$val'width='3%'>".$th."</th>";
		                  					}else{
		                                    	echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
											}
		                                 }
		                              ?>
	                           	</tr>
                            </thead>
                            <tbody id='listrespon'></tbody>
                        </table>
                     </div>
                 </div>
            </div>
            <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
                Display :   <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                                <?php
                                    $num=10;
                                    for($i=0;$i<10;$i++){?>
                                        <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                        <?php $num+=20;
                                    }
                                ?>
                            </select>
            </div>
            <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
                <div class="pagination" id="pagination" style="text-align:center"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
         showdata(1,$('#sort_num').val());
        // pagenav ------------------------------------------------------------
        $("body").delegate(".pagenav","click",function(){
            var page = $(this).attr("id");
            var total_display = $('#sort_num').val();
            showdata(page,total_display);
        });
        // sort_num ------------------------------------------------------------
        $('#sort_num').change(function(){
            var  total_display= $(this).val();
            showdata(1,total_display);
        });
        // refresh ------------------------------------------------------------
        $('body').delegate('#refresh', 'click', function(){
            location.reload();
        });
        // Addnew---------------------------------------------------------------
        $('body').delegate('#a_addnew', 'click', function(){
            $('#collapseExample').collapse('toggle')
            //cleadata();
        });
        // cleardata ----------------------------------------------------------
        $('body').delegate('#clear', 'click', function(){
            cleadata();
            $('#save').html('Save');
        });

        // main chech---------------------------------------------------------
        $('body').delegate('.grade_levelid', 'click', function(){
            var grade_id = $(this).val();           
            if($(this).is(':checked')){                
                $('.assess_id').each(function(){
                   $('.assess_id_'+grade_id).prop('checked', true); 
                });
            }else{
               $('.assess_id_'+grade_id).prop('checked', false); 
            } 
        });
        // sub chech---------------------------------------------------------
        $('body').delegate('.assess_id', 'click', function(){
            var cc = $(this).closest('.tr').find('.assess_id:checked').length;
            if(cc==0){
                $(this).closest('.tr').find('.grade_levelid').prop('checked',false);
            }
            $('.assess_id:checked').each(function(){
                $(this).parent().parent().find('.percentag').focus();
                $(this).closest('.tr').find('.grade_levelid').prop('checked', true);
            });
        });
        // key up ------------------------------------------------------------
        $('body').delegate('.percentag', 'keyup', function(){
            var obj_val = $(this).val();
            if(obj_val>0 && obj_val!=""){
               $(this).closest('.tr').find('.grade_levelid').prop('checked', true);
               $(this).parent().parent().find('.assess_id').prop('checked', true);
            }else{
                //$(this).closest('.tr').find('.grade_levelid').prop('checked', false);
                $(this).parent().parent().find('.assess_id').prop('checked', false);
            } 
        });
        // key change ------------------------------------------------------
        $("body").delegate("#schlevel_id, #subj_type_id, #subjectid, #yearid","change", function () { 
            var myval = $('#schlevel_id').val(); 
            var subj_type_id = $('#subj_type_id').val();
            var subjectid = $('#subjectid').val();
            var yearid = $('#yearid').val();
            getGradeLevels(myval, subj_type_id, subjectid, yearid);            
        });
        // change  ----------------------------------------------------------
        $("#schlevel_id").on('change',function(){
          get_schyera($(this).val());
        });

       // key subj_type_id change --------------------------------------------

        $("body").delegate("#subj_type_id", "change", function () {   
                var my = $(this).val(); 
                getsubject(my);
        });

        // save ------------------------------------------------------------
        $("body").delegate("#save", "click", function (){
            var b = false;
            $('.grade_levelid').each(function(){
                var percentag_ = $(this).closest('.tr').find('.percentag');
                var to_percent = 0;
                percentag_.each(function(){
                    to_percent += $(this).val() - 0;
                });
                if(to_percent > 100){
                        b = true;
                } 

            });
            // alert(to_percent);
            // return false;

            if(b == true){
                alert('Percentages greater than one hundred !');
            }else{
                var schlevel_id = $('#schlevel_id');
                var j=0;
                var i = 0;
                // main --------------------------
                var arr_mian = [];
                if($(".grade_levelid").is(":checked")){
                    $(".grade_levelid:checked").each(function(i){
                        var grade_levelid = $(this).val();
                        var assess_id = $(this).closest('.tr').find('.assess_id_'+grade_levelid+':checked');

                        // sub --------------------
                        var arr_ass = [];
                        assess_id.each(function(j){
                            var assessid = $(this).val();
                            var maxscore = $(this).parent().parent().find(".maxscore_"+grade_levelid).val();
                            var percentag = $(this).parent().parent().find(".percentag_"+grade_levelid).val();
                            var ass_percent_id = $(this).parent().parent().find(".ass_percent_id_"+grade_levelid).val();
                            arr_ass[j] = {grade_levelid, assessid, percentag, maxscore,ass_percent_id}
                        });
                        arr_mian[i] = {arr: arr_ass}
                    });
                }
                //console.log(arr_mian); 
                if(schlevel_id.val()==""){
                    schlevel_id.css('border-color','red');
                }else{
                    $.ajax({ 
                        type : "POST",
                        url  : "<?= site_url('school/c_subject_asesment_percentag_kgp/save')?>",
                        data : {
                            schlevel_id    : $("#schlevel_id").val(),
                            yearid         : $("#yearid").val(),
                            subj_type_id   : $("#subj_type_id").val(),
                            subjectid      : $("#subjectid").val(),
                            arr_mian       : arr_mian
                        },
                        success:function(data){
                            if(data.schlevel_id!=""){ 
                                //toastr["success"](data.msg); 
                                toastr["success"]("Assesment Percentag kgp was created...!");  
                                $("#save").html('Save'); 
                                $('#collapseExample').collapse('toggle');
                                showdata(1,$('#sort_num').val());
                                //cleadata();
                            }
                        }
                    });
                    schlevel_id.css('border-color','#ccc');
                } 
            }
        });
    });
    // number key ----------------------------------------------------------
    function isNumberKey(evt) {
        var e = window.event || evt; // for trans-browser compatibility 
        var charCode = e.which || e.keyCode;
        if ((charCode > 45 && charCode < 58) || charCode == 8) {
            return true;
        }
        return false;
    }
    // getGradeLevels -------------------------------------------------------
    function getGradeLevels(schlevelid, subj_type_id, subjectid) {
        $("#tbody_gradlevel").html("");        
        if (schlevelid != null && subj_type_id != null && subjectid != null) {
            $.ajax({
                url  : "<?= site_url('school/c_subject_asesment_percentag_kgp/getgradlevels');?>",
                data: {
                    'schlevelid':schlevelid,
                    'subj_type_id':subj_type_id,
                    'subjectid':subjectid
                    },
                type: "POST",
                dataType: 'json',
                // async: false,
                success: function (data) {
                    $("#tbody_gradlevel").html(data);
                    $('.grade_levelid').each(function(){
                        var grade_id = $(this).val();
                        $('.assess_id_'+grade_id).each(function(){
                            var counts = $('.assess_id_'+grade_id+':checked').size();
                            if(counts>0){
                                $('.grade_'+grade_id).prop('checked', true);
                            }else{
                                $('.grade_'+grade_id).prop('checked', false);
                            }
                        });
                    });
                }
            })
        }
    }
    //get yare---------------------------------------------------------
    function getsubject(subj_type_id, subject_id = ''){
        $.ajax({        
            url: "<?= site_url('school/c_subject_asesment_percentag_kgp/getsubject') ?>",    
            data:{         
                    'subj_type_id':subj_type_id
                },
            type: "post",
            dataType: "json",
            success: function(data){ 
                var sub = '';
                    sub="<option  value=''>--select--</option>";
                if(data.subject.length > 0){
                    $.each(data.subject, function(i, row){
                        sub += '<option value="'+ row.subjectid +'" '+ (row.subjectid == subject_id ? 'selected="selected" ' : '') +'>'+ row.subject_kh +'</option>';
                    });
                }else{
                    sub += '';
                }
                $('#subjectid').html(sub);
            }
        });
    }
    //get yare---------------------------------------------------------
      function get_schyera(schlevel_id){
        $.ajax({        
            url: "<?= site_url('school/c_subject_asesment_percentag_kgp/get_schyera') ?>",    
            data: {         
              'schlevel_id':schlevel_id
            },
            type: "post",
            dataType: "json",
            success: function(data){ 
                var ya = '';
                    ya="<option  value=''>--select--</option>";
                if(data.schyear.length > 0){
                    $.each(data.schyear, function(i, row){
                        ya += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                    });
                }else{
                    ya += '';
                }

                $('#yearid').html(ya);
            }
        });
      }
    // deletedata -------------------------------------------------
    function deletedata(event){ 
        if(window.confirm('Are your sure to delete?')){
           $.ajax({
              url: '<?= site_url('school/c_subject_asesment_percentag_kgp/delete') ?>',
              type: 'POST',
              datatype: 'JSON',
              async: false,
              data: {
                 'ass_percent_id':jQuery(event.target).attr("rel")
              },
                success: function(data){
                    if(data.msg = 'deleted'){
                        toastr["success"]("Data deleted!");
                        showdata(1,$('#sort_num').val());
                    }else{
                        toastr["warning"]("Can't delete!");
                    }  
                },
                error: function() {

                }
            });
        }
    }
    // sort -------------------------------------------------------
    function sort(event){
       /*$(event.target)=$(this)*/ 
        this.sortby = $(event.target).attr("rel");
        if ($(event.target).attr("sorttype") == "ASC") {
            $('.sort').removeClass('cur_sort_up');
            $(event.target).addClass(' cur_sort_down');
            $(event.target).attr("sorttype", "DESC");
            this.sorttype = "ASC";
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass(' cur_sort_up');
        } else if ($(event.target).attr("sorttype") == "DESC") {
            $(event.target).attr("sorttype", "ASC");
            this.sorttype = "DESC";
            $('.sort').removeClass('cur_sort_up');
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass(' cur_sort_down');    
        }        
        showdata(1,$('#sort_num').val(),this.sortby,this.sorttype);
    }
    // getdata --------------------------------------------------
    function showdata(page,total_display,sortby,sorttype){
        var roleid = <?php echo $this->session->userdata('roleid');?>;
        var per    = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
        var sort_num=$('#sort_num').val();
        $.ajax({ 
            type : "POST",
            url  : "<?= site_url('school/c_subject_asesment_percentag_kgp/showdata');?>",
            datatype:"Json",
            async: false,
            data : {
                'roleid': roleid,
                'sort_num':sort_num,
                'sortby':sortby,
                'sorttype':sorttype,
                'p_page': per,
                'page'  : page,
                'total_display': total_display
            },
            success:function(data){
                $("#listrespon").html(data['body']);
                $('.pagination').html(data['pagination']['pagination']);
            }
        });
    }
    // cleradata --------------------------------------------
    function cleadata(){
        $("#schlevel_id").val("");
        $("#subj_type_id").val(""); 
        $('#subjectid').val("");
        $('#yearid').val("");
        $('#percentag').val("");
        $('#maxscore').val("");
    }
</script>