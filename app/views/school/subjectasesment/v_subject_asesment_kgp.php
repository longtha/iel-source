<style type="text/css">
    table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }
    .cssclass {
        width: 650px;
    }
    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }
    
    #se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	.to {color:#cc66ff; font-weight:bold;}
	.from {color:#FF0000; font-weight:bold;}
	
</style>
<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }
    $opt_school = "";
    if(isset($school)){
        foreach($school as $rschool){
            $opt_school.= "<option value='".$rschool->schoolid."'>".$rschool->name."</option>";
        }
    }
    $opt_program = "";
    if(isset($program)){
        foreach($program as $rpro){
            $opt_program.= "<option value='".$rpro->programid."'>".$rpro->program."</option>";
        }
    }
    // subject --------------------------------------------
    if(isset($getsubjecttype) && count($getsubjecttype)>0){
        $subjecttype="";
        $subjecttype="<option attr_price='0' value=''>--select--</option>";
        foreach($getsubjecttype as $row){
            $subjecttype.="<option value='".$row->subj_type_id."'>".$row->subject_type."</option>";
        }
        
    }
    // subject --------------------------------------------
     $opt_level="";
    if(isset($getschoollavel) && count($getschoollavel)>0){
        $opt_level="<option value=''></option>";
        foreach($getschoollavel as $row){
            $opt_level.="<option value='".$row->schlevelid."'>".$row->sch_level."</option>";
        }
        
    }
 ?>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
    <div class="col-sm-12">     
	    <div class="row">
	      	<div class="col-xs-12">
		         <div class="result_info">
		            <div class="col-xs-6">
		                <strong>Asesment Percentag khmer General Programming </strong>  
		            </div>
		            <div class="col-xs-6" style="text-align: right">
		            	<a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
		                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>" width="22px">
		               	</a>
		               	<a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add">
		                  <img src="<?= base_url('assets/images/icons/add.png') ?>" width="22px">
		               	</a>
		            </div>         
		         </div>
	      	</div>
	   	</div>
		<div class="collapse" id="collapseExample">             
		    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
		        <div class="col-sm-12">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <input type="hidden" name="hid_assessment" id="hid_assessment" value="">
                            <label for="schoolid">School<span style="color:red"></span></label>
                            <select name="schoolid" id="schoolid" class="form-control"><?php echo $opt_school;?></select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="programid">Program<span style="color:red"></span></label>
                            <select name="programid" id="programid" class="form-control"><?php echo $opt_program;?></select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="">School level<span style="color:red"></span></label>
                            <select name="schoolevelid" id="schoolevelid" class="form-control"><?php echo $opt_level;?></select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="gradlevel">Grade Level<span style="color:red"></span></label>
                            <select name="gradlevel" id="gradlevel" class="form-control"></select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="groupsubject">Group subject<span style="color:red"></span></label>
                            <select name="groupsubject" id="groupsubject" class="form-control"></select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="subjectexamp">Subject exam<span style="color:red"></span></label>
                            <select name="subjectexamp" id="subjectexamp" class="form-control"></select>
                        </div>
                    </div>
                    
                </div>
		     	 <div class="col-sm-12">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="subject_assesment">Subject assessment<span style="color:red"></span></label>
                            <input type="text" name="subject_assesment" id="subject_assesment" class="form-control"></select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="max_score">Max score<span style="color:red"></span></label>
                            <input type="text" name="max_score" id="max_score" class="form-control"></select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="percentage">Percentage<span style="color:red"></span></label>
                            <input type="text" name="percentage" id="percentage" class="form-control"></select>
                        </div>
                    </div>
                    
                </div>
                
                 <div class="col-sm-7 col-sm-offset-5">
                    <div style="padding-top:10px;" class="form-group">
                      <button id="save" class="btn btn-primary btn-sm" type="button" name="save" value="save">Save</button>&nbsp; 
                      <button id="clear" class="btn btn-warning btn-sm" type="button" name="clear">Clear</button> 
                    </div>            
                </div>
		     	<div class="row">
			        <div class="col-sm-12">
			           <div class="form-group" style="border-bottom: 1px solid #CCC;">&nbsp;</div>           
			        </div>
		     	</div>
		  	</form>
		</div>
    <!-- show header -->
        <div class="col-sm-12">
            <div class="panel panel-default">
               <div class="table-responsive" id="div_export_print">
                    <div id="tab_print">
                        <table width="100%" border="0" class="table" id="setBorderTbl">
                            <thead>
	                             <tr>
		                              <?php
		                                 foreach ($thead as $th => $val) {
                                            if ($th == 'No'){
                                                echo "<th width='5%'>".$th."</th>";
                                            }else if($th == 'Action'){
		                                      	echo "<th class='$val'width='8%'>".$th."</th>";
		                  					}else{
		                                    	echo "<th>" . $th . "</th>";
											}
		                                 }
		                              ?>
	                           	</tr>
                            </thead>
                            <tbody id='listrespon'></tbody>
                        </table>
                     </div>
                 </div>
            </div>
            <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
                Display :   <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                                <?php
                                    $num=10;
                                    for($i=0;$i<10;$i++){?>
                                        <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                        <?php $num+=20;
                                    }
                                ?>
                            </select>
            </div>
            <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
                <div class="pagination" id="pagination" style="text-align:center"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        showdata();
        $("body").delegate("#edit_ass","click",function(){
            var id_assessment = $(this).attr("att_edit");
            $.ajax({ 
                type : "POST",
                url  : "<?= site_url('school/c_subject_asesment_kgp/show_edit_assessment')?>",
                dataType:"json",
                data : {
                    id_assessment : id_assessment
                },
                success:function(data){
                    //if(data['order'][0].lenght > 0){
                        $("#groupsubject").html(data['groupsubject']);
                        $("#subjectexamp").html(data['subjectexamp']);
                        $("#gradlevel").html(data['gradlevel']);
                        $("#schoolid").val(data['order'][0]);
                        $("#programid").val(data['order'][1]);
                        $("#schoolevelid").val(data['order'][2]);
                        $("#hid_assessment").val(data['order'][3]);
                        $("#subject_assesment").val(data['order'][4]);
                        $("#percentage").val(data['order'][5]);
                        $("#max_score").val(data['order'][6]);
                        //$("#groupsubject").val(data['order'][0]['group_subj_id']);
                        // $("#hid_assessment").val(data['order'][0]['userid']);
                    //}
                }
            });
        })
        $("body").delegate("#clear","click",function(){
            cleadata();
        });
        $("body").delegate("#delete_ass","click",function(){
            var id_assement = $(this).attr("att_del");
            var conf = confirm("Do you want to delete this data ?");
            if(conf == true){
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('school/c_subject_asesment_kgp/delete_ass')?>",
                    dataType:"json",
                    data : {
                        id_assement : id_assement
                    },
                    success:function(data){
                        if(data['data'] == 100){
                            alert("You can't delete this record !.It have transection already.");
                        }else{
                            alert("You delect success.");
                            showdata();
                        }
                    }
                });   
            }else{
                return false;
            }
           
        });
        $("body").delegate("#schoolevelid","change",function(){
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('school/c_subject_asesment_kgp/show_main_subject')?>",
                    data : {
                        schoolid    : $("#schoolid").val(),
                        programid   : $("#programid").val(),
                        schlevelid  : $(this).val()
                    },
                    success:function(data){
                        $("#groupsubject").html(data);
                        fn_show_grandlevel();
                    }
                });
         });
        $("body").delegate("#groupsubject","change",function(){
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('school/c_subject_asesment_kgp/show_subject')?>",
                    data : {
                        schoolid    : $("#schoolid").val(),
                        programid   : $("#programid").val(),
                        schlevelid   : $("#schoolevelid").val(),
                        gradelevelid  : $("#gradlevel").val(),
                        groupsubject : $(this).val()
                    },
                    success:function(data){
                        $("#subjectexamp").html(data);
                    }
                });
         });
        $("body").delegate("#save", "click", function (){
            $.ajax({ 
                type : "POST",
                url  : "<?= site_url('school/c_subject_asesment_kgp/save')?>",
                data : {
                    hid_assessment : $("#hid_assessment").val(),
                    schoolid    : $("#schoolid").val(),
                    programid   : $("#programid").val(),
                    schlevelid   : $("#schoolevelid").val(),
                    groupsubject : $("#groupsubject").val(),
                    subjectexamp : $("#subjectexamp").val(),
                    subject_assesment : $("#subject_assesment").val(),
                    max_score    : $("#max_score").val(),
                    percentage   : $("#percentage").val(),
                    gradlevel : $("#gradlevel").val()
                },
                success:function(data){
                    $("#subjectexamp").html(data);
                    showdata();
                }
            });            
        });
       
        // Addnew---------------------------------------------------------------
        $('body').delegate('#a_addnew', 'click', function(){
            $('#collapseExample').collapse('toggle')
            //cleadata();
        });
        
    });
    function fn_show_grandlevel(){
        $.ajax({ 
            type : "POST",
            url  : "<?= site_url('school/c_subject_asesment_kgp/getgradlevels')?>",
            dataType:"HTMl",
            data : {
                schlevel_id : $("#schoolevelid").val(),
                schoolid : $("#schoolid").val(),
                programid : $("#programid").val()
            },
            success:function(data){
               //$("#tbody_gradlevel").html(data);
               $("#gradlevel").html(data);
            }
        });
    }
    // number key ----------------------------------------------------------
    function isNumberKey(evt) {
        var e = window.event || evt; // for trans-browser compatibility 
        var charCode = e.which || e.keyCode;
        if ((charCode > 45 && charCode < 58) || charCode == 8) {
            return true;
        }
        return false;
    }

    // getdata --------------------------------------------------
    function showdata(){
        $.ajax({ 
            type : "POST",
            url  : "<?= site_url('school/c_subject_asesment_kgp/showdata');?>",
            datatype:"HTML",
            async: false,
            data : {
                para_m : 1
            },
            success:function(data){
                $("#listrespon").html(data);
                cleadata();
            }
        });
    }
    // cleradata --------------------------------------------
    function cleadata(){
        $("#hid_assessment").val("");
        $("#gradlevel").html("");
        $("#schoolevelid").val("");
        $("#groupsubject").val("");
        $("#subjectexamp").val("");
        $("#subject_assesment").val("");
        $("#max_score").val("");
        $("#percentage").val("");
    }
</script>