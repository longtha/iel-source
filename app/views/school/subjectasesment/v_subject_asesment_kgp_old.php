<?php
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }
 ?>
 <style type="text/css">
    table tbody tr td img {
        width: 20px;
        margin-right: 10px
    }

    a, .sort {
        cursor: pointer;
    }

    .cur_sort_up {
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }

    #top-bar img {
        width: 20px;
        margin-top: 5px;
    }

    .cur_sort_down {
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>') !important;
        background-position: left !important;
        background-repeat: no-repeat !important;
        padding-left: 15px !important;
    }
    
    #se_list th {vertical-align: middle;}
	#se_list td {vertical-align: middle;}
	.to {color:#cc66ff; font-weight:bold;}
	.from {color:#FF0000; font-weight:bold;}
	
</style>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
    <div class="col-sm-12">     
	    <div class="row">
	      	<div class="col-xs-12">
		         <div class="result_info">
		            <div class="col-xs-6">
		                <strong>Assesment khmer General Programming</strong>  
		            </div>
		            <div class="col-xs-6" style="text-align: right">
		            	<a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
		                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>" width="22px">
		               	</a>
		               	<a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add">
		                  <img src="<?= base_url('assets/images/icons/add.png') ?>" width="22px">
		               	</a>
		            </div>         
		         </div>
	      	</div>
	   	</div>
		<div class="collapse" id="collapseExample">             
		    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
		        <input type="hidden" name="assess_id" id="assess_id">
		     	<div class="col-sm-4">
			        <div class="form-group">
                       <label for="asesment_kh">Assesment Name Khmer<span style="color:red"></span></label>
                            <input type="text" name="asesment_kh" id="asesment_kh" class="form-control" placeholder="Asesment Name Khme">
                    </div>
                   <!--  <div class="form-group">
                       <label for="percentag">Percentag(%)<span style="color:red"></span></label>
                            <input type="text" name="percentag" id="percentag" class="form-control" onkeypress='return isNumberKey(event);' placeholder="Percentag(%)">
                    </div> -->
		     	</div><!-- 1 -->
		     	<div class="col-sm-4">
                    <div class="form-group">
                       <label for="asesment_eng">Assesment Name English<span style="color:red"></span></label>
                            <input type="text" name="asesment_eng" id="asesment_eng" class="form-control required" placeholder="Asesment Name English">
                    </div>
		     	</div><!-- 2 -->
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="status">Status<span style="color:red"></span></label>
                        <select name="status" id="status" class="form-control">
                            <option value="1">Active</option>
                            <option value="0">In_Active</option>
                       </select>
                    </div>
                </div><!-- 3 -->
			    <div class="col-sm-7 col-sm-offset-5">
			        <div style="padding-top:10px;" class="form-group">
			          <button id="save" class="btn btn-primary btn-sm" type="button" name="save" value="save">Save</button>&nbsp; 
			          <button id="clear" class="btn btn-warning btn-sm" type="button" name="clear">Clear</button> 
			        </div>            
			    </div>
		     	<div class="row">
			        <div class="col-sm-12">
			           <div class="form-group" style="border-bottom: 1px solid #CCC;">&nbsp;</div>           
			        </div>
		     	</div>
		  	</form>
		</div>
    <!-- show header -->
        <div class="col-sm-12">
            <div class="panel panel-default">
               <div class="table-responsive" id="div_export_print">
                    <div id="tab_print">
                        <table width="100%" border="0" class="table table-hover" id="setBorderTbl">
                            <thead>
	                             <tr>
		                              <?php
		                                 foreach ($thead as $th => $val) {
		                                 	if ($th == 'No'){
		                                      	echo "<th class='$val'>".$th."</th>";
		                  					}
		                                	else if ($th == 'Action'){
		                                        echo "<th style='text-align:center' class='$val'>".$th."</th>";
		                  					}else{
		                                    	echo "<th class='sort $val' onclick='sort(event);' rel='$val' sorttype='ASC'>" . $th . "</th>";
											}
		                                 }
		                              ?>
	                           	</tr>
                            </thead>
                            <tbody id='listrespon'></tbody>
                        </table>
                     </div>
                 </div>
            </div>
            <div class="form-group" style="padding-right:10px;border:0px solid #f00; width:20%;float:left">
                Display :   <select id="sort_num" class="form-control" style='padding:5px; margin-right:0px;width:100px;'>
                                <?php
                                    $num=10;
                                    for($i=0;$i<10;$i++){?>
                                        <option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
                                        <?php $num+=20;
                                    }
                                ?>
                            </select>
            </div>
            <div style="text-align:center; verticle-align:center;border:0px solid #f00; width:50%;float:left">
                <div class="pagination" id="pagination" style="text-align:center"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        showdata(1,$('#sort_num').val());
        // pagenav ----------------------------------------
        $("body").delegate(".pagenav","click",function(){
            var page = $(this).attr("id");
            var total_display = $('#sort_num').val();
            showdata(page,total_display);
        });
        // sort_num ---------------------------------------
        $('#sort_num').change(function(){
            var  total_display= $(this).val();
            showdata(1,total_display);
        });
        // cleardata ---------------------------------------
        $('body').delegate('#clear', 'click', function(){
            cleadata();
            $('#save').html('Save');
        });
        // refresh ----------------------------------------
        $('body').delegate('#refresh', 'click', function(){
            location.reload();
        });
        // Addnew---------------------------------------
        $('body').delegate('#a_addnew', 'click', function(){
            $('#collapseExample').collapse('toggle')
            cleadata();
        });
        // serch_name_eng ---------------------------------
        // $("#percentag").on('keyup',function(){
        //     var percentag = $('#percentag').val();
        //     if(percentag <= 100 ){
        //        // showdata(1,$('#sort_num').val());
        //     }else{
        //         alert('Scores in excess of one hundred percent percent ?');
        //         cler();
        //     }
        // });
        // save ------------------------------------------
        $("#save").on("click",function(){
            var assess_id = $('#assess_id').val();
            var asesment_kh = $('#asesment_kh');
            if(asesment_kh.val()==""){
                asesment_kh.css('border-color','red');
            }else{
                $.ajax({ 
                    type : "POST",
                    url  : "<?= site_url('school/c_subject_asesment_kgp/save')?>",
                    data : {
                        assess_id    : $("#assess_id").val(),
                        asesment_kh  : $("#asesment_kh").val(),
                        asesment_eng : $("#asesment_eng").val(),
                        //percentag    : $("#percentag").val(),
                        status       : $("#status").val()
                    },
                    success:function(data){
                        if(data.asesment_kh!=""){ 
                            toastr["success"](data.msg);  
                            $("#save").html('Save');          
                            cleadata();
                            showdata(1,$('#sort_num').val());
                        }
                    }
                });

                asesment_kh.css('border-color','#ccc');
            } 
        });
    });// end function---------------------------------------------
    function isNumberKey(evt) {
        var e = window.event || evt; // for trans-browser compatibility 
        var charCode = e.which || e.keyCode;
        if ((charCode > 45 && charCode < 58) || charCode == 8) {
            return true;
        }
        return false;
    }
    // sort -------------------------------------------------------
    function sort(event){
       /*$(event.target)=$(this)*/ 
        this.sortby = $(event.target).attr("rel");
        if ($(event.target).attr("sorttype") == "ASC") {
            $('.sort').removeClass('cur_sort_up');
            $(event.target).addClass(' cur_sort_down');
            $(event.target).attr("sorttype", "DESC");
            this.sorttype = "ASC";
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass(' cur_sort_up');
        } else if ($(event.target).attr("sorttype") == "DESC") {
            $(event.target).attr("sorttype", "ASC");
            this.sorttype = "DESC";
            $('.sort').removeClass('cur_sort_up');
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass(' cur_sort_down');    
        }        
        showdata(1,$('#sort_num').val(),this.sortby,this.sorttype);
    }
    // getdata ------------------------------------------------
    function showdata(page,total_display,sortby,sorttype){
        var roleid = <?php echo $this->session->userdata('roleid');?>;
        var per    = <?php echo isset($_GET['per_page'])?$_GET['per_page']:0 ?>;
        var sort_num=$('#sort_num').val();
        $.ajax({ 
            type : "POST",
            url  : "<?= site_url('school/c_subject_asesment_kgp/showdata');?>",
            datatype:"Json",
            async: false,
            data : {
                'roleid': roleid,
                'sort_num':sort_num,
                'sortby':sortby,
                'sorttype':sorttype,
                'p_page': per,
                'page'  : page,
                'total_display': total_display
            },
            success:function(data){
                $("#listrespon ").html(data['body']);
                $('.pagination').html(data['pagination']['pagination']);
            }
        });
    }
    // deletedata -------------------------------------------------
    function deletedata(event){ 
        if(window.confirm('Are your sure to delete?')){
           $.ajax({
              url: '<?= site_url('school/c_subject_asesment_kgp/deletedata') ?>',
              type: 'POST',
              datatype: 'JSON',
              async: false,
              data: {
                 'assess_id':jQuery(event.target).attr("rel")
              },
                success: function(data){
                    if(data.msg = 'deleted'){
                        toastr["success"]("Data deleted!");
                        showdata(1,$('#sort_num').val());
                    }else{
                        toastr["warning"]("Can't delete!");
                    }  
                },
                error: function() {

                }
            });
        }
    }
    // update ----------------------------------------------------
    function update(event){ 
        $.ajax({ 
            type : "POST",
            url  : "<?= site_url('school/c_subject_asesment_kgp/editdata')?>",
            data : { 
                'assess_id':jQuery(event.target).attr("rel")
            },
            success:function(datas){ 
                var data= datas.res;              
                $("#assess_id").val(data.assess_id);
                $("#asesment_kh").val(data.assess_name_kh);
                $("#asesment_eng").val(data.assess_name_en);
                //$("#percentag").val((data.def_percentag*100));
                $("#status").val(data.is_active);
                $('#collapseExample').attr('aria-expanded', 'true');
                $('#collapseExample').addClass('in');
                $("#save").html('Update');
            }
        }); 
    }
    // cleradata --------------------------------------------
    function cleadata(){
        $("#assess_id").val("");
        $("#asesment_kh").val("");
        $('#asesment_eng').val("");
        //$('#percentag').val("");
        $('#status').val(1);
    }
    // function cler(){
    //      $('#percentag').val("");
    // }
    
</script>