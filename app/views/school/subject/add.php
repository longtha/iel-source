<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
        <div id="main_content">
            <div class="result_info">
                <div class="col-sm-6">
                    <strong>Setup Subject</strong>
                </div>
                <div class="col-sm-6" style="text-align: right">
                    <!-- Block message -->
                    <?php if (isset($exist)) echo $exist ?>
                    <?PHP if (isset($_GET['save'])) {
                        echo "<p>Your data has been saved successfully...!</p>";
                    } else if (isset($_GET['edit'])) {
                        echo "<p>Your data has been updated successfully...!</p>";
                    } else if (isset($_GET['delete'])) {
                        echo "<p style='color:red'>Your data has been deleted successfully...!</p>";
                    }
                    $m = '';
                    $p = '';
                    if (isset($_GET['m'])) {
                        $m = $_GET['m'];
                    }
                    if (isset($_GET['p'])) {
                        $p = $_GET['p'];
                    }
                    ?>
                    <!-- End block message -->
                    <span class="res_sms"></span>
                </div>
            </div>

            <form method="post" accept-charset="utf-8" class="tdrow" id="fsubject">
                    <div class="col-sm-6">
                        <div class="panel-body">
                            <div class="form_sep">
                                <label class="req" for="classid">School<span style="color:red">*</span></label>
                                <select class="form-control" id='cboschool' name='cboschool' min=1 required
                                        data-parsley-required-message="Select any school">
                                    <option value=''>Select School</option>
                                    <?php foreach ($this->subjects->getschool() as $schoolrow) { ?>
                                        <option
                                            value='<?php echo $schoolrow->schoolid; ?>' <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel-body">
                            <div class="form_sep">
                                <label class="req" for="schlevel_id">School Level<span style="color:red">*</span></label>
                                <select class="form-control"  id="schlevel_id" required name="schlevel_id">
                                    <option value=""></option>
                                    <?php if (isset($schevels) && count($schevels) > 0) {
                                        foreach ($schevels as $sclev) {
                                            echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                                        }
                                    } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="panel-body">
                            <div class="form_sep">
                                <label class="req" for="cboschool">Subject Group<span style="color:red">*</span></label>
                                <select class="form-control" id='cbosubjecttypeid' name='cbosubjecttypeid' min=1
                                        required data-parsley-required-message="Select any subject Group">
                                    <option value=''></option>
                                    <?php
                                    //foreach ($this->subjects->getsubjecttype() as $subjecttyperow) {
                                       // echo "<option value='$subjecttyperow->subj_type_id' maintype='$subjecttyperow->main_type' >$subjecttyperow->subject_type</option>";
                                   // }
                                    ?>
                                </select>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="cboschool">Subject<span style="color:red">*</span></label>
                                <input type="text" name="txtsubject" id="txtsubject" class="form-control" required
                                       data-parsley-required-message="Please fill the subject"/>
                                <input type="hidden" id="subjectid"/>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="cboschool">Subject (KH)<span style="color:red">*</span></label>
                                <input type="text" name="txtsubjectkh" id="txtsubjectkh" class="form-control" required
                                       data-parsley-required-message="Please fill the subject"/>
                            </div>                            
                           
                            <div class="form_sep hide">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name='is_trimester' id='is_trimester'>For TAE
                                    </label>
                                    <label>
                                        <input type="checkbox" name='is_eval' id='is_eval'>For Evaluate
                                    </label>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel-body">
                            <div class="form_sep hide">
                                <label class="req" for="yearid">Academic Year<span style="color:red">*</span></label>
                                <select class="form-control" id="yearid" name="yearid">
                                    <option value=""></option>
                                    <?php if (isset($schyears) && count($schyears) > 0) {
                                        foreach ($schyears as $schyear) {
                                            echo '<option value="' . $schyear->yearid . '" ' . ($this->session->userdata('year') == $schyear->yearid ? "selected=selected" : "") . ' >' . $schyear->sch_year . '</option>';
                                        }
                                    } ?>
                                </select>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="student_num">Shortcut</label>
                                <input type="text" name="txtshort_sub" id="txtshort_sub" class="form-control" />
                            </div>
                            <div class="form_sep">
                                <label class="req" for="orders">Percence(%)<span style="color:red">*</span></label>
                                <input type="text" class="form-control required
                                       data-parsley-required-message="Please fill the short cut" id="orders" name="orders"/>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="max_score">Max Score<span style="color:red">*</span></label>
                                <input type="text" class="form-control required
                                       data-parsley-required-message="Please fill the short cut" id="max_score" name="max_score"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">                        
                        <div class="panel-body" style="">
                            <div class="form_sep">
                            <div class="table-responsive">
                                <table  class="table table-bordered" id="tbl_gradlevel" style="">
                                    <thead>
                                        <tr>
                                            <th style="text-align:center">Grade Level</th>                                            
                                            
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_gradlevel">         
                                    </tbody>                                    
                                </table>
                            </div>
                            </div>
                        </div>                        
                    </div> 
					<div class="col-sm-6">    
						<div class="panel-body">
							<label class="req" for="checkgroupsubject"><input type="checkbox" id="checkgroupsubject">&nbsp;Score subject group</label>
						</div>
					</div>
                <div class="col-sm-12">
                    <div class="panel-body">
                        <div class="form_sep">
                            <?php if ($this->green->gAction("C")) { ?>
                                <input type="button" name="btnsavesubject" id='btnsavesubject' value="Save"
                                       class="btn btn-primary"/>
                            <?php } ?>
                            <input type="button" name="btncancel" id='btncancel' value="Cancel"
                                   class="btn btn-warning"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<style>
    .tdrow tr, .tdrow th, .tdrow td {
        border: none !important;
    }

    .res_sms {
        font-size: 16px;
        font-family: "Comic Sans MS", "Times New Roman", Arial;
        color: #c12e2a;
    }

</style>

<script type="text/javascript">

    $(function () {
        //--------- parseley validation -----
        $('#fsubject').parsley();

        //---------- end of validation ----

        $("#btncancel").click(function () {
            var r = confirm("Do you want to cancel?");
            if (r == true) {
                location.href = ("<?php echo site_url('school/subject/?m='.$m.'&p='.$p.'');?>");
            }
        });
		$("#checkgroupsubject").click(function(){
			if($(this).is(":checked")){
				$(this).val(1);
			}else{
				$(this).val(0);
			}
		})
        $('body').delegate('.grade_levelid', 'click', function(){ 
            var tr = $(this).parent().parent().parent();                   
            if($(this).prop("checked") == false) {               
                tr.find('.examtype').each(function(){
                    this.checked = false;
                });
            }else{                
                tr.find('.examtype').each(function(){
                    this.checked = true;
                });
            }
        });
       
        //========== ajax save with submit form ===========

        //$("#btnsavesubject").click(function () {
        //    $('#fsubject').submit();
        //});
        $('body').delegate('#btnsavesubject', 'click', function(e){
            e.preventDefault();
            if($('#fsubject').parsley().validate()){
                var j= 0;
                var n= 0;
                $(".grade_levelid:checked").each(function(j){
                    n = j+1;
                });
                if(n>0){
                    if(confirm("Do you want to save !")){
                        var subjectid = $('#subjectid').val();
                        var subject = $('#txtsubject').val();
                        var subject_kh = $('#txtsubjectkh').val();
                        var subjecttypeid = $('#cbosubjecttypeid').val();
                        var maintype = $('option:selected', '#cbosubjecttypeid').attr("maintype");
                        var short_sub = $('#txtshort_sub').val();
                        var schlevelid = $('#schlevel_id').val();
                        var schoolid = $('#cboschool').val();
                        var max_score = $('#max_score').val();
						var checkgroupsubject = $("#checkgroupsubject").val();
                        var is_trimester = 0;
                        var is_eval = 0;
                        var orders = $('#orders').val();
                        if ($('#is_trimester').prop("checked") == true) {
                            is_trimester = 1;
                        }
                        if ($('#is_eval').prop("checked") == true) {
                            is_eval = 1;
                        }
                        var arrlevel = [];
                        var arrexamtype = [];
                        $('.grade_levelid:checked').each(function(i){
                            var tr = $(this).parent().parent().parent();                  
                            var grade_levelid = $(this).data('grade_levelid');
                                // tr.find(".examtype:checked").each(function(j){
                                //    arrexamtype[j] = $(this).data('examtype');
                                // });
                                //arrlevel[i] = {'grade_levelid': grade_levelid, 'arrexamtype':arrexamtype};
                            arrlevel[i] = grade_levelid;
                        });
                        $.ajax({
                            url: "<?php echo site_url('school/subject/save');?>",
                            dataType: "Json",
                            type: "POST",
                            async: false,
                            data: {
                                'subjectid': subjectid,
                                'subject': subject,
                                'subject_kh': subject_kh,
                                'subjecttypeid': subjecttypeid,
                                'maintype':maintype,
                                'short_sub': short_sub,
                                'schlevelid': schlevelid,
                                'schoolid': schoolid,
                                'is_trimester': is_trimester,
                                'is_eval': is_eval,
                                'orders': orders,
                                'max_score':max_score,
								'checkgroupsubject':checkgroupsubject,
                                'arrgradelevel': arrlevel
                            },
                            success: function (data) {
                                if (data.res == 3) {
                                    toastr["success"]('Subject was updated!');
                                    location.href = ("<?php echo site_url('school/subject/?m='.$m.'&p='.$p.'');?>");
                                }
                                if (data.res == 1) {
                                    toastr["success"]('Subject was save!');
                                }
                                if (data.res == 2) {
                                    toastr["warning"]("Subject is already existed !");
                                }
                                if (subjectid == "") {
                                    $('#txtsearchsubject').val(subject);
                                }
                                //alert(data.res);
                                $("#fsubject").clearForm();
                                search(e);
                            }
                        });
                }else{          
                    return false;
                    }
                }else{
                    toastr["warning"]("Please add grade Level!");
                    return false;
                }
            }
        });
        //========= end of ajax save ======================
        
        $("#cbosubjecttypeid").on("change", function () {
            if ($(this).val() != "") {
               getOrders($(this).val());
            }
        });
        
        $("body").delegate("#schlevel_id", "change", function () {   
            var myval = $(this).val();     
            getGradeLevels(myval);
            getSchYear(myval);
            getsubjecttype(myval);
            $(this).val(myval);
        });
		
		$("body").delegate("#cbosubjecttypeid", "change", function () {   
           var cbosubjecttypeid=$("#cbosubjecttypeid option:selected").text();
		   $("#txtsubject,#txtsubjectkh,#txtshort_sub").val(cbosubjecttypeid);
        });
		
    });
    
    function getOrders(subtypeid) {
        $.ajax({
            url: "<?php echo site_url('school/subject/orders');?>",
            dataType: "Json",
            type: "POST",
            async: false,
            data: {
                'subtypeid': subtypeid
            },
            success: function (data) {
                $("#orders").val(data);
            }
        })
    }
    
    function getsubjecttype(schlevelid) {
        $("#cbosubjecttypeid").html("");
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo base_url(); ?>index.php/school/subject/getsubjecttype",
                data: {'schlevelid':schlevelid},
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (data) {
                    $("#cbosubjecttypeid").html(data);
                }
            })
        }
    }
    
    function getGradeLevels(schlevelid) {
        $("#tbody_gradlevel").html("");
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo base_url(); ?>index.php/school/subject/getgradlevels",
                data: {'schlevelid':schlevelid},
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (data) {                    
                    $("#tbody_gradlevel").html(data);
                }
            })
        }
    }

    function getSchYear(schlevelid) {

        $("#yearid").html("");
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo site_url('school/academic_year/get_year'); ?>",
                type: "POST",
                dataType: 'Json',
                async: false,
                data:{
                    schlevelid:schlevelid
                },
                success: function (res) {
                    var data = res.year;
                    var tr = "";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="'+row.yearid+'">'+row.sch_year+'</option>';
                        }
                    }
                    $("#yearid").html(tr);
                }
            })
        }
    }
</script>