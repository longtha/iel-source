<style>
   .tdrow tr,.tdrow th,.tdrow td{
    border: none !important; 
  }
</style>
<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
    <div id="main_content">
      
       <div class="result_info">
        <div class="col-sm-6">
            <strong>Setup Subject</strong>  
        </div>
        <div class="col-sm-6" style="text-align: right">
            <strong>                
            </strong>   
          <!-- Block message -->
            <?PHP if(isset($_GET['save'])){
               echo "<p>Your data has been saved successfully...!</p>";
              }else if(isset($_GET['edit'])){
                  echo "<p>Your data has been updated successfully...!</p>";
              }else if(isset($_GET['delete'])){
                  echo "<p style='color:red'>Your data has been deleted successfully...!</p>";
              }
              $subjectid = $query->subjectid;
              $m='';
              $p='';
              if(isset($_GET['m'])){
                $m=$_GET['m'];
              }
              if(isset($_GET['p'])){
                  $p=$_GET['p'];
              }
              // if ($query->modified_by !='')
              //   $user=$this->db->where('userid',$query->modified_by)->get('sch_user')->row()->user_name;
            ?>
          <!-- End block message -->
        </div> 
      </div> 
      
      <form method="post" accept-charset="utf-8" class="tdrow" id="fsubject">
          <input type="hidden" name="txtsubjectid" id='txtsubjectid' value="<?php echo $query->subjectid;?>"/>
          <label style="float:right !important; font-size:11px !important; color:red;">&nbsp;</label> 
        <div class="row">    
           <div class="col-sm-6">           
                <div class="panel-body">
                  <div class="form_sep">
                    <label class="req" for="classid">School<span style="color:red">*</span></label>
                    <select class="form-control" id='cboschool' name='cboschool'  min=1 required data-parsley-required-message="Select any school">
                      <option value=''>Select School</option>
                          <?php
                          foreach ($this->subjects->getschool() as $schoolrow) {?>
                              <option value="<?php echo $schoolrow->schoolid; ?>" <?php echo ($schoolrow->schoolid==$query->schoolid?"selected":"")?>><?php echo $schoolrow->name;?></option>
                         <?php }
                         ?>
                    </select>
                  </div>
                  <div class="form_sep">
                      <label class="req" for="cboschool">Subject Group<span style="color:red">*</span></label>
                      <select class="form-control" id='cbosubjecttypeid' name='cbosubjecttypeid' min=1 required data-parsley-required-message="Select any subject type">
                          <option value=''></option>
                              <?php

                              foreach ($this->subjects->getsubjecttype($query->schlevelid) as $subjecttyperow) {?>
                                  <option value="<?php echo $subjecttyperow->subj_type_id; ?>" maintype="<?php echo $subjecttyperow->main_type ?>"  <?php echo ($subjecttyperow->subj_type_id==$query->subj_type_id?"selected":"");?>><?php echo $subjecttyperow->subject_type;?></option>
                             <?php }
                             ?>
                      </select>
                  </div>     
                  <div class="form_sep">
                    <label class="req" for="cboschool">Subject (En)<span style="color:red">*</span></label>
                  <input type="text" name="txtsubject" id="txtsubject" class="form-control" value="<?php echo $query->subject?>" required data-parsley-required-message="Please fill the subject" />
                  </div>
                  <div class="form_sep">
                    <label class="req" for="cboschool">Subject (KH)<span style="color:red">*</span></label>
                  <input type="text" name="txtsubjectkh" id="txtsubjectkh" class="form-control" value="<?php echo $query->subject_kh?>" required data-parsley-required-message="Please fill the subject" />
                  </div>
                                
                </div>
            </div>
            
            <div class="col-sm-6">                          
              <div class="panel-body">
                <div class="form_sep"  style="margin-top:-20px;">
                    <label class="req" for="schlevel_id">School Level<span style="color:red">*</span></label>
                    <select class="form-control"  id="schlevel_id" required name="schlevel_id">
                        <option value=""></option>
                        <?php 
                        $schevels = $this->schlevels->getsch_level("1");
                        if (isset($schevels) && count($schevels) > 0) {
                            foreach ($schevels as $sclev) {
                                echo '<option value="' . $sclev->schlevelid . '"'.($sclev->schlevelid==$query->schlevelid?"selected":"").'>' . $sclev->sch_level . '</option>';
                            }
                        } ?>
                    </select>
                </div>
                <div class="form_sep">
                  <label class="req" for="student_num">Short Cut<span style="color:red">*</span></label>
                  <input type="text" name="txtshort_sub" id="txtshort_sub" class="form-control" value="<?php echo $query->short_sub?>"  required data-parsley-required-message="Please fill the short cut"/>   
                </div>
                <div class="form_sep">
                    <label class="req" for="orders">Percence(%)</label>
                    <input type="text" class="form-control" id="orders" name="orders" value="<?php echo ($query->score_percence*100);?>"/>
                </div>
                <div class="form_sep">
                                <label class="req" for="max_score">Max Score</label>
                                <input type="text" class="form-control" id="max_score" name="max_score" value="<?php echo $query->max_score;?>"/>
                            </div>
                <div class="form_sep hide">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name='is_trimester' id='is_trimester' <?PHP if($query->is_trimester_sub==1) echo 'checked'; ?>> For TAE
                    </label>  
                  </div>                  
                </div> 
                <div class="form_sep hide">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name='is_eval' id='is_eval' <?PHP if($query->is_eval==1) echo 'checked'; ?>>For Evaluate
                    </label>  
                  </div>                  
                </div>              
              </div> 
            </div>  
            <div class="col-sm-6">                        
              <div class="panel-body" style="">
                  <div class="form_sep">
                  <div class="table-responsive">
                      <table  class="table table-bordered" id="tbl_gradlevel">
                          <thead>
                              <tr>
                                  <th style="text-align:center">Grade Level</th>           
                              </tr>
                          </thead>
                          <tbody id="tbody_gradlevel">
                            <?php
                                $gradelevel=$this->gdlevel->getgradelevels($query->schlevelid);
                                $html = "";
                                if(count($gradelevel)>0){
                                  foreach ($gradelevel as $row_grade) {

                                    $grade_check = $this->subjects->getsubjectdetail($subjectid, $row_grade->grade_levelid);

                                    $examtype = $this->extype->examtype();
                                    $html .='<tr>
                                                  <td>
                                                    <label><input type="checkbox" name="grade_levelid[]" id="grade_levelid" class="grade_levelid" value="'.$row_grade->grade_levelid.'" '.(count($grade_check)>0?"checked":"").' data-grade_levelid="'.$row_grade->grade_levelid.'">&nbsp;'.$row_grade->grade_level.'</label>
                                                  </td>
                                              </tr>';
                                                    // if(count($examtype)>0){
                                                    //     foreach ($examtype as $row_examtype) {
                                                    //           $exam_check = $this->subjects->getsubjectdetail($subjectid, $row_grade->grade_levelid, $row_examtype->examtypeid);
                                                    //           $html .='<label><input type="checkbox" name="examtype[]" id="examtype" class="examtype"  value="'.$row_examtype->examtypeid.'" '.(count($exam_check)>0?"checked":"").' data-examtype="'.$row_examtype->examtypeid.'">&nbsp;'.$row_examtype->exam_test.'&nbsp;&nbsp;&nbsp;</label>';
                                                    //     }
                                                    //   } 
                                    }
                                }
                                echo $html;
                            ?>        
                          </tbody>                                    
                      </table>
                  </div>
                  </div>
              </div>                        
            </div>   
			<div class="col-sm-6">    
				<div class="panel-body">
					<label class="req" for="checkgroupsubject"><input type="checkbox" <?php echo ($query->score_group==1?'checked':''); ?> id="checkgroupsubject" value="<?php echo $query->score_group;?>">&nbsp;Score subject group</label>
				</div>
			</div>
        </div>
        
        <div class="row">
          <div class="col-sm-12">
            
            <div class="panel-body">
                <div class="form_sep">
                <?php if($this->green->gAction("C")){ ?>
                 <input type="button" name="btnsavesubject" id='btnsavesubject' value="UPDATE" class="btn btn-primary" />
                <?php } ?>
                <!-- <input type="reset" name="btnreset" id='btnreset' value="Reset" class="btn btn-warning" /> -->
                <input type="button" name="btncancel" id='btncancel' value="Cancel" class="btn btn-warning" />
                </div>                               
            </div>
            
          </div>
        </div>
        
       </form>         
    </div>
    
 </div>  
</div>

<script type="text/javascript">
    $(function() {
       //--------- parseley validation -----
        $('#fsubject').parsley();
      
    //---------- end of validation ----

    $("#btncancel").click(function () {
        var r = confirm("Do you want to cancel?");
        if (r == true) {
            location.href = ("<?php echo site_url('school/subject/?m='.$m.'&p='.$p.'');?>");
        }
    });
	$("body").delegate("#cbosubjecttypeid", "change", function () {   
           var cbosubjecttypeid=$("#cbosubjecttypeid option:selected").text();
		   $("#txtsubject,#txtsubjectkh,#txtshort_sub").val(cbosubjecttypeid);
     });
	$("#checkgroupsubject").click(function(){
			if($(this).is(":checked")){
				$(this).val(1);
			}else{
				$(this).val(0);
			}
		})
    $('body').delegate('#btnsavesubject', 'click', function(e){
       e.preventDefault();
            if($('#fsubject').parsley().validate()){
                var j= 0;
                var n= 0;
                $(".grade_levelid:checked").each(function(j){
                    n = j+1;
                });
                if(n>0){
                    if(confirm("Do you want to update !")){
                        var subjectid = $('#txtsubjectid').val();
                        var subject = $('#txtsubject').val();
                        var subject_kh = $('#txtsubjectkh').val();
                        var subjecttypeid = $('#cbosubjecttypeid').val();
                        var maintype = $('option:selected', '#cbosubjecttypeid').attr("maintype");
                        var short_sub = $('#txtshort_sub').val();
                        var schlevelid = $('#schlevel_id').val();
                        var schoolid = $('#cboschool').val();
                        var max_score = $('#max_score').val();
						var checkgroupsubject = $("#checkgroupsubject").val();
                        var is_trimester = 0;
                        var is_eval = 0;
                        var orders = $('#orders').val();
                        if ($('#is_trimester').prop("checked") == true) {
                            is_trimester = 1;
                        }
                        if ($('#is_eval').prop("checked") == true) {
                            is_eval = 1;
                        }
                        var arrlevel = [];
                        var arrexamtype = [];
                        $('.grade_levelid:checked').each(function(i){
                            var tr = $(this).parent().parent().parent();
                            var grade_levelid = $(this).data('grade_levelid');
                            // tr.find(".examtype:checked").each(function(j){
                            //     arrexamtype[j] = $(this).data('examtype');
                            // });
                            arrlevel[i] = grade_levelid;
                        });

                        $.ajax({
                            url: "<?php echo site_url('school/subject/save');?>",
                            dataType: "Json",
                            type: "POST",
                            async: false,
                            data: {
                                'subjectid': subjectid,
                                'subject': subject,
                                'subject_kh': subject_kh,
                                'subjecttypeid': subjecttypeid,
                                'maintype': maintype,
                                'short_sub': short_sub,
                                'schlevelid': schlevelid,
                                'schoolid': schoolid,
                                'is_trimester': is_trimester,
                                'is_eval': is_eval,
                                'orders': orders,
                                'max_score':max_score,
                                'arrgradelevel': arrlevel,
								'checkgroupsubject': checkgroupsubject
                            },
                            success: function (data) {
                                if (data.res == 3) {
                                    toastr["success"]('Subject was updated!');
                                    location.href = ("<?php echo site_url('school/subject/?m='.$m.'&p='.$p.'');?>");
                                }
                                if (data.res == 1) {
                                    toastr["success"]('Subject was save!');
                                }
                                if (data.res == 2) {
                                    toastr["warning"]("Subject is already existed !");
                                }
                                if (subjectid == "") {
                                    $('#txtsearchsubject').val(subject);
                                }
                                //alert(data.res);
                                $("#fsubject").clearForm();
                                search(e);
                            }
                        });
                    }else{
                        return false;
                    }
                }else{
                    toastr["warning"]("Please add grade Level!");
                    return false;
                }
            }
        });
    });

</script>