<div class="container-fluid" style="width:100% !important;padding-bottom: 20px;">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
              <span class="icon">
                  <i class="fa fa-th"></i>
              </span>
                <strong>Setup Exam Schedule</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                  <img src="<?= base_url('assets/images/icons/add.png') ?>" width="24px">
               </a>
               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
               </a>
               <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
               </a>           
               
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse in" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" id="f_save">
         <div class="col-sm-3" style="">
            <div class="form-group">
               <label for="schoolid">School Info<span style="color:red">*</span></label>
               <select name="schoolid" id="schoolid" class="form-control" data-parsley-required="true" data-parsley-required-message="This field require">
                  <?php
                     $opt = '';
                     // $opt .= '<option value="">All</option>';
                     foreach ($this->info->getschinfor() as $row) {
                        $opt .= '<option value="'.$row->schoolid.'">'.$row->name.'</option>';
                     }
                     echo $opt;        
                  ?>                  
               </select>
            </div>
         </div><!-- 1 -->

         <div class="col-sm-3">
            
            <div class="form-group" style="display: none;">
               <label for="programid">Program<span style="color:red">*</span></label>
               <select name="programid" id="programid" class="form-control">

               </select>
            </div>

            <div class="form-group">
               <label for="schlevelid">School Level<span style="color:red">*</span></label>
               <select name="schlevelid" id="schlevelid" class="form-control schlevelid" data-parsley-required="true" data-parsley-required-message="This field require">
                  <?php
                     $opt = '';
                     // $opt .= '<option value=""></option>';
                     foreach ($this->level->getsch_level() as $row) {
                        $opt .= '<option value="'.$row->schlevelid.'">'.$row->sch_level.'</option>';
                     }
                     echo $opt;        
                  ?>
               </select>
            </div>      
            
         </div><!-- 2 -->

         <div class="col-sm-3">

            <div class="form-group">
               <label for="yearid">Year<span style="color:red">*</span></label>
               <select name="yearid" id="yearid" class="form-control">

               </select>
            </div>

         </div><!-- 3 -->

         <div class="col-sm-3">

            <div class="form-group" style="">
               <label for="examtypeid">Exam Type<span style="color:red">*</span></label>
               <select name="examtypeid" id="examtypeid" class="form-control" data-parsley-required="true" data-parsley-required-message="This field require">
                <?php
                 $opt = '';
                 $opt .= '<option value=""></option>';
                 foreach ($this->e->examtype() as $row) {
                    $opt .= '<option value="'.$row->examtypeid.'">'.$row->exam_test.'</option>';
                 }
                 echo $opt;        
                ?>
               </select>
            </div>

         </div><!-- 4 -->

         <div class="col-sm-12">
            <div class="table-responsive">
               <table border="0"​ align="center" id="list" class="table table-hover table-condensed table-bordered" style="width: 100%;">
                  <thead>
                     <tr>
                        <th rowspan="2" style="width: 3%;">No</th>
                        <th rowspan="2" style="width: 10%;">Grade<span style="color:red">*</span></th>
                        <th colspan="2" style="width: 30%;">Exam</th>
                        <th colspan="2" style="width: 30%;">Entry Score</th>
                        <th rowspan="2" style="width: 3%;padding: 0;">
                           <a href="javascript:;" id="add_new_row" style="display: block;"><i class="glyphicon glyphicon-plus"></i></a>
                        </th>                        
                     </tr>
                     <tr>
                        <th>Start</th>
                        <th>End</th>
                        <th>Start</th>
                        <th>End</th>
                     </tr>                                   
                  </thead>
                  <tbody>
                     <tr>
                        <td class="no" style="">1</td>
                        <td><select class="form-control input-sm grade_levelid" name="grade_levelid[]" data-examid="0" data-parsley-required="true" data-parsley-required-message="This field require"></select> <!-- -->
                        </td>
                        <td>
                           <div class="input-group">
                              <input type="text" class="form-control input-sm start_exam" name="start_exam[]" placeholder="dd/mm/yyyy">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                           </div>
                        </td>
                        <td>
                           <div class="input-group">
                              <input type="text" class="form-control input-sm end_exam" name="end_exam[]" placeholder="dd/mm/yyyy">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                           </div>
                        </td>
                        <td>
                           <div class="input-group">
                              <input type="text" class="form-control input-sm start_entry_score" name="start_entry_score[]" placeholder="dd/mm/yyyy">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                           </div>
                        </td>
                        <td>
                           <div class="input-group">
                              <input type="text" class="form-control input-sm end_entry_score" name="end_entry_score[]" placeholder="dd/mm/yyyy">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                           </div>
                        </td>
                        <td>
                           <a href="javascript:;" class="a_remove"><img src="<?= base_url('assets/images/icons/delete.png') ?>" width="14"></a>
                        </td>
                     </tr>                     
                  </tbody>

                  <tfoot>
                       
                  </tfoot>               
               </table>
            </div>        

         </div><!-- 5 -->               

         <div class="col-sm-7 col-sm-offset-4">
            <div class="form-group">
               <button type="button" class="btn btn-primary btn-sm save" name="save" id="save" value="save" tabindex="11" data-save='1'>Save</button>
               <button type="button" class="btn btn-primary btn-sm save" name="save_next" id="save_next" value="save" tabindex="11" data-save='2'>Save next</button>
               <button type="button" class="btn btn-warning btn-sm" name="clear" id="clear" tabindex="12">Clear</button>
            </div>            
         </div>

      </form>      
      <div class="form-group">
         <div class="col-sm-12">
            <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
         </div>           
      </div>
   </div>   
   
   <div class="form-group">
      <div class="col-sm-12">
         <div class="table-responsive">
            <div id="tab_print">
               <table border="0"​ cellspacing="0" cellpadding="0" align="center" id="list_data" class="table table-hover table-condensed table-bordered">               
                  <thead style="text-align: center;">
                     <tr>
                        <th rowspan="2" style="width: 3%;">No</th>
                        <th rowspan="2" style="width: 8%;">Grade</th>
                        <th rowspan="2" style="width: 10%;">School</th>
                        <th rowspan="2" style="width: 10%;">School Level</th>
                        <th rowspan="2" style="width: 10%;">Year</th>
                        <th rowspan="2" style="width: 10%;">Exam Type</th>
                        <th colspan="2" style="width: 20%;">Exam</th>
                        <th colspan="2" style="width: 20%;">Entry Score</th>
                        <th rowspan="2" colspan="2" style="width: 10%;" class="remove_tag">Action</th>                        
                     </tr>
                     <tr>
                        <th>Start</th>
                        <th>End</th>
                        <th>Start</th>
                        <th>End</th>
                     </tr>
                     <tr class="remove_tag">
                        <td>&nbsp;</td>
                        <td style="border-right: hidden;border-left: hidden;">
                           <select class="form-control input-sm" name="grade_list_search" id="grade_list_search" disabled>
                              
                           </select></td>
                        <td style="border-right: hidden;">
                           <select class="form-control input-sm" name="schoolid_list_search" id="schoolid_list_search">
                              <?php
                                 $opt = '';
                                 $opt .= '<option value=""></option>';
                                 foreach ($this->info->getschinfor() as $row) {
                                    $opt .= '<option value="'.$row->schoolid.'">'.$row->name.'</option>';
                                 }
                                 echo $opt;        
                              ?>
                           </select></td>
                        <td style="border-right: hidden;">
                           <select class="form-control input-sm" name="schlevelid_list_search" id="schlevelid_list_search">
                              <?php
                                 $opt = '';
                                 $opt .= '<option value=""></option>';
                                 foreach ($this->level->getsch_level() as $row) {
                                    $opt .= '<option value="'.$row->schlevelid.'">'.$row->sch_level.'</option>';
                                 }
                                 echo $opt;        
                              ?>
                           </select></td>
                        <td style="border-right: hidden;">
                           <select class="form-control input-sm" name="yearid_list_search" id="yearid_list_search" disabled>
                              
                           </select></td>
                        <td style="border-right: hidden;">
                           <select class="form-control input-sm" name="examtypeid_list_search" id="examtypeid_list_search">
                              <?php
                                 $opt = '';
                                 $opt .= '<option value=""></option>';
                                 foreach ($this->e->examtype() as $row) {
                                    $opt .= '<option value="'.$row->examtypeid.'">'.$row->exam_test.'</option>';
                                 }
                                 echo $opt;        
                              ?>
                           </select></td>   
                        <td style="border-right: hidden;text-align: left;">
                           <button  type="button" class="btn btn-info btn-sm" name="btn_search" id="btn_search">Search</button></td>
                        <td colspan="5">&nbsp;</td>                     
                     </tr>                                   
                  </thead>                
                  <tbody>
                       
                  </tbody>

                  <tfoot>

                  </tfoot>
               </table>
            </div>
         </div><!-- responsive -->
      </div>
      
      <div class="col-sm-2">
         <select class="input-sm" name="limit_record" id="limit_record" data-toggle="tooltip" data-placement="top" title="Display">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="30">30</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="200">200</option>
            <option value="300">300</option>
            <option value="500">500</option>
            <option value="1000">10000</option>            
         </select>
      </div>
      <div class="col-sm-2">
         <div id="sp_page" class="btn-group pagination" role="group" aria-label="..." style="display: inline;"></div>
      </div>
   </div><!-- row -->

</div>

<style type="text/css">
  #list_data th {vertical-align: middle;text-align: center;}
  #list_data td {vertical-align: middle;}

  #list th {vertical-align: middle;text-align: center;}
  #list td {vertical-align: middle;text-align: center;}

   table #fix_header {
      width: 100%;
   }

   .scroll {
       max-height: 200px;
       overflow: auto;
   }

</style>

<span data-toggle="modal" data-target="#dialog_print" class="clprint" ></span>

<script type="text/javascript">
   $(function(){ 

      // schlevelid list search =======      
      $('body').delegate('#schlevelid_list_search', 'change', function(){
         if($(this).val() != ''){
            var schlevelid_list_search = $(this).val();
            var grade_levelid_list_search = $(this).closest('tr').find('#grade_list_search');
            var yearid_list_search = $(this).closest('tr').find('#yearid_list_search');
            get_grade_with_search(grade_levelid_list_search, schlevelid_list_search);
            get_year_with_search(yearid_list_search, '', schlevelid_list_search);

            $(this).closest('tr').find('#grade_list_search').prop('disabled', false);
            $(this).closest('tr').find('#yearid_list_search').prop('disabled', false);
         }else{
            $(this).closest('tr').find('#grade_list_search').html('');
            $(this).closest('tr').find('#yearid_list_search').html('');

            $(this).closest('tr').find('#grade_list_search').prop('disabled', true);
            $(this).closest('tr').find('#yearid_list_search').prop('disabled', true);

         }         
      });

      // schlevelid list =======
      $('body').delegate('.schlevelid_list', 'change', function(){
         var schlevelid_list = $(this).val();
         var grade_levelid_list = $(this).closest('tr').find('.grade_levelid_list');
         get_grade_change_all(grade_levelid_list, schlevelid_list);
         get_year($('.yearid_list'), '', schlevelid_list);
      });


      // update ========
      $('body').delegate('.btn_edit', 'click', function(){
         var id = $(this).parent().data('id');
         var btn_edit = $(this);

         $.ajax({
            url: '<?= site_url('school/c_setupexam/update') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){

            },
            data: {
               examid_list: id,

               grade_levelid_list: btn_edit.closest('tr').find('.grade_levelid_list').val(),
               schoolid_list: btn_edit.closest('tr').find('.schoolid_list').val(),
               schlevelid_list: btn_edit.closest('tr').find('.schlevelid_list').val(),
               yearid_list: btn_edit.closest('tr').find('.yearid_list').val(),
               examtypeid_list: btn_edit.closest('tr').find('.examtypeid_list').val(),               
               start_exam_list: btn_edit.closest('tr').find('.start_exam_list').val(),
               end_exam_list: btn_edit.closest('tr').find('.end_exam_list').val(),
               start_entry_score_list: btn_edit.closest('tr').find('.start_entry_score_list').val(),
               end_entry_score_list: btn_edit.closest('tr').find('.end_entry_score_list').val()
               
            },
            success: function(data){
               if(data == 1){
                  toastr["success"]('Success!');
                  grid(1, $('#limit_record').val() - 0);
               }else if(data == 2){
                  toastr["warning"]('Duplicate data!');
               }else{
                  toastr["warning"]("Can't update data!");
               }
            },
            error: function() {

            }
         });
      });

      // edit ========
      $('body').delegate('.img_eidt', 'click', function(){
         var examid = $(this).parent().data('id');
         var img_eidt = $(this);

         $.ajax({
            url: '<?= site_url('school/c_setupexam/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){

            },
            data: {
               examid: examid
            },
            success: function(data){
               var grade_levelid_list = data.grade_levelid;
               img_eidt.closest('tr').find('.schoolid_list').val(data.schoolid);
               img_eidt.closest('tr').find('.schlevelid_list').val(data.schlevelid);
               var yearid_list = data.yearid;
               img_eidt.closest('tr').find('.examtypeid_list').val(data.examtypeid);

               img_eidt.closest('tr').find('.start_exam_list').val(data.start_exam != null && data.start_exam != '00/00/0000' ? data.start_exam : '');
               img_eidt.closest('tr').find('.end_exam_list').val(data.end_exam != null && data.end_exam != '00/00/0000' ? data.end_exam : '');
               img_eidt.closest('tr').find('.start_entry_score_list').val(data.start_entry_score != null && data.start_entry_score != '00/00/0000' ? data.start_entry_score : '');
               img_eidt.closest('tr').find('.end_entry_score_list').val(data.end_entry_score != null && data.end_entry_score != '00/00/0000' ? data.end_entry_score : '');

               // grade ========
               $.ajax({
                  url: '<?= site_url('school/c_setupexam/edit_grade') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     schlevelid: img_eidt.closest('tr').find('.schlevelid_list').val(),
                     grade_levelid_list: grade_levelid_list
                  },
                  success: function(data){
                     img_eidt.closest('tr').find('.grade_levelid_list').html(data.opt);
                  },
                  error: function() {

                  }
               });

               // year ========
               $.ajax({
                  url: '<?= site_url('school/c_setupexam/edit_year') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     schlevelid: img_eidt.closest('tr').find('.schlevelid_list').val(),
                     yearid_list: yearid_list
                  },
                  success: function(data){
                     img_eidt.closest('tr').find('.yearid_list').html(data.opt);
                  },
                  error: function() {

                  }
               });              
               
            },
            error: function() {

            }
         });

      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var examid = $(this).data('id') - 0;

         // if(areaid == $(this).data('ba_id') - 0){
         //    toastr["warning"]("Can't delete! data in process...");
         // }else{
            if(window.confirm('Are your sure to delete?')){
               $.ajax({
                  url: '<?= site_url('school/c_setupexam/delete') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     examid: examid
                  },
                  success: function(data){
                     if(data == 1){
                        toastr["success"]("Data deleted!");
                        grid(1, $('#limit_record').val() - 0);
                     }else{
                        toastr["warning"]("Can't delete!");
                     }               
                     
                  },
                  error: function() {

                  }
               });
            }
         // }
         
      });

      // date ========
      $('body').on('focus',".start_exam", function(){
         $(this).datepicker({format: 'dd/mm/yyyy'})
      });     
      $('body').on('focus',".end_exam", function(){
         $(this).datepicker({format: 'dd/mm/yyyy'})
      });
      $('body').on('focus',".start_entry_score", function(){
         $(this).datepicker({format: 'dd/mm/yyyy'})
      });
      $('body').on('focus',".end_entry_score", function(){
         $(this).datepicker({format: 'dd/mm/yyyy'})
      }); 

      $('body').on('focus',".start_exam_list", function(){
         $(this).datepicker({format: 'dd/mm/yyyy'})
      });     
      $('body').on('focus',".end_exam_list", function(){
         $(this).datepicker({format: 'dd/mm/yyyy'})
      });
      $('body').on('focus',".start_entry_score_list", function(){
         $(this).datepicker({format: 'dd/mm/yyyy'})
      });
      $('body').on('focus',".end_entry_score_list", function(){
         $(this).datepicker({format: 'dd/mm/yyyy'})
      });             

      // save ======= 
      $('body').delegate('.save', 'click', function(){
         var save_next = $(this).data('save');

         if($('#f_save').parsley().validate()){
            var arr = [];
            $('.grade_levelid').each(function(i){
               var examid = $(this).data('examid');
               var grade_levelid = $(this).val();
               var start_exam = $(this).parent().parent().find('.start_exam').val();
               var end_exam = $(this).parent().parent().find('.end_exam').val();
               var start_entry_score = $(this).parent().parent().find('.start_entry_score').val();
               var end_entry_score = $(this).parent().parent().find('.end_entry_score').val();
                               
               arr[i] = {'examid': examid,
                           'grade_levelid': grade_levelid,
                           'start_exam': start_exam,
                           'end_exam': end_exam,
                           'start_entry_score': start_entry_score,
                           'end_entry_score': end_entry_score                           
                        };
            });

            $.ajax({
               url: '<?= site_url("school/c_setupexam/save") ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  schoolid: $('#schoolid').val(),
                  programid: $('#programid').val(), 
                  schlevelid: $('.schlevelid').val(),
                  yearid: $('#yearid').val(),
                  examtypeid: $('#examtypeid').val(),                  
                  // sub data ======
                  arr: arr
               },
               success: function(data){
                  if(data == 1){
                     if(save_next - 0 == 2){
                        clear();
                     }
                     grid(1, $('#limit_record').val() - 0);
                     toastr["success"]('Success!');
                  }else if(data == 2){
                     toastr["warning"]('Duplicate data!');
                  }else{
                     toastr["warning"]("Please, enter start date!");
                  }         
               
               },
               error: function() {

               }
            });
         }
      });

      // get... =======
      if($('.schlevelid').val() != ''){
         $('#add_new_row').css("visibility", "visible");
      }else{            
         $('#add_new_row').css("visibility", "hidden");
      } 

      get_program($('.schlevelid').val());
      get_grade($('.schlevelid').val());       
      get_year($('#yearid'), '', $('.schlevelid').val());
      $('#add_new_row').attr("data-schlevelid", $('.schlevelid').val());

      $('body').delegate('.schlevelid', 'change', function(){
         if($(this).val() != ''){
            // show =======
            $('#add_new_row').css("visibility", "visible");

            get_program($(this).val());
            get_year($('#yearid'), '', $(this).val());
            get_grade_change_all($('.grade_levelid'), $('.schlevelid').val());

            // set attr school level ======
            $('#add_new_row').attr("data-schlevelid", $(this).val());

            grid(1, $('#limit_record').val() - 0);

         }else{        
            $('#add_new_row').css("visibility", "hidden");
         }         

      });    

      // tooltip ======
      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // print =======
      $('body').delegate('#a_print', 'click', function(){
         var title="<h4 align='center'>"+ "Setup Exam Schedule" +"</h4>";
         var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "")
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint(title, export_data);
      });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Setup Exam Schedule";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "")
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });      

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#examtypeid').select();
         $('#save').html('Save');
         $('#save_next').show();
      });

      // add new row ========
         // get grade in list =======
         get_grade($('.schlevelid').val());      
      $('body').delegate('#add_new_row', 'click', function(){
         add_new_row();

         $('.no').each(function(i){
            $(this).html(i + 1);
         });

         // get grade in list =======
         get_grade($(this).attr('data-schlevelid'));
         
      });

      // remove row ========
      $('body').delegate('.a_remove', 'click', function(){
         if($('#list > tbody > tr').length > 1){
            $(this).parent().parent().remove();
         }else{
            toastr["warning"]("Only one can't delete!");
         }
                  
         $('.no').each(function(i){
            $(this).html(i + 1);
         });

      });

      // init. =====
      grid(1, $('#limit_record').val() - 0);

      // search ========
      $('body').delegate('#limit_record', 'click', function(){
         grid(1, $(this).val() - 0);
      });
      $('body').delegate('#btn_search', 'click', function(){
         grid(1, $('#limit_record').val() - 0);
      });

      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#limit_record').val() - 0);
      });

      // change with edit ========
      $('body').delegate('.edit', 'click', function() {
         $(this).attr('data-show_hide') - 0 == 0 ? $(this).attr('data-show_hide', 1) : $(this).attr('data-show_hide', 0);

         if($(this).attr('data-show_hide') - 0 == 0){
            $(this).parent().parent().find('.grade_levelid_list').css('display', 'none');
            $(this).parent().parent().find('.sp_grade_levelid_list').css('display', 'block');

            $(this).parent().parent().find('.schoolid_list').css('display', 'none');
            $(this).parent().parent().find('.sp_schoolid_list').css('display', 'block');

            $(this).parent().parent().find('.schlevelid_list').css('display', 'none');
            $(this).parent().parent().find('.sp_schlevelid_list').css('display', 'block');

            $(this).parent().parent().find('.yearid_list').css('display', 'none');
            $(this).parent().parent().find('.sp_yearid_list').css('display', 'block');

            $(this).parent().parent().find('.examtypeid_list').css('display', 'none');
            $(this).parent().parent().find('.sp_examtypeid_list').css('display', 'block');            

            $(this).parent().parent().find('.start_exam_list').css('display', 'none');
            $(this).parent().parent().find('.sp_start_exam_list').css('display', 'block');

            $(this).parent().parent().find('.end_exam_list').css('display', 'none');
            $(this).parent().parent().find('.sp_end_exam_list').css('display', 'block');

            $(this).parent().parent().find('.start_entry_score_list').css('display', 'none');
            $(this).parent().parent().find('.sp_start_entry_score_list').css('display', 'block');

            $(this).parent().parent().find('.end_entry_score_list').css('display', 'none');
            $(this).parent().parent().find('.sp_end_entry_score_list').css('display', 'block');
            
            // action ======
            $(this).find('.img_eidt').css('display', 'inline');
            $(this).find('.btn_edit').css('display', 'none');
         }else{
            $(this).parent().parent().find('.grade_levelid_list').css('display', 'block');
            $(this).parent().parent().find('.sp_grade_levelid_list').css('display', 'none'); 

            $(this).parent().parent().find('.schoolid_list').css('display', 'block');
            $(this).parent().parent().find('.sp_schoolid_list').css('display', 'none');  

            $(this).parent().parent().find('.schlevelid_list').css('display', 'block');
            $(this).parent().parent().find('.sp_schlevelid_list').css('display', 'none');  

            $(this).parent().parent().find('.yearid_list').css('display', 'block');
            $(this).parent().parent().find('.sp_yearid_list').css('display', 'none');

            $(this).parent().parent().find('.examtypeid_list').css('display', 'block');
            $(this).parent().parent().find('.sp_examtypeid_list').css('display', 'none');

            $(this).parent().parent().find('.start_exam_list').css('display', 'block');
            $(this).parent().parent().find('.sp_start_exam_list').css('display', 'none');

            $(this).parent().parent().find('.end_exam_list').css('display', 'block');
            $(this).parent().parent().find('.sp_end_exam_list').css('display', 'none');

            $(this).parent().parent().find('.start_entry_score_list').css('display', 'block');
            $(this).parent().parent().find('.sp_start_entry_score_list').css('display', 'none');

            $(this).parent().parent().find('.end_entry_score_list').css('display', 'block');
            $(this).parent().parent().find('.sp_end_entry_score_list').css('display', 'none');

            // action ======
            $(this).find('.img_eidt').css('display', 'none');
            $(this).find('.btn_edit').css('display', 'inline');
         }

      });

   }); // ready ========

   // get year =======
   function get_year(mySelector, programid = '', schlevelid = ''){
      $.ajax({
         url: '<?= site_url('school/c_setupexam/get_year') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            programid: programid,
            schlevelid: schlevelid                                  
         },
         success: function(data){
            mySelector.html(data.opt);

         },
         error: function() {

         }
      });
   }

   // get year with search =======
   function get_year_with_search(mySelector, programid = '', schlevelid = ''){
      $.ajax({
         url: '<?= site_url('school/c_setupexam/get_year') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            programid: programid,
            schlevelid: schlevelid                                  
         },
         success: function(data){
            mySelector.html(data.opt);
            mySelector.prepend('<option value="" selected="selected"></option');
         },
         error: function() {

         }
      });
   }

   // get program ======
   function get_program(schlevelid){      
      $.ajax({
         url: '<?= site_url('school/c_setupexam/get_program') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            schlevelid: schlevelid                
         },
         success: function(data){
            $('#programid').html(data.opt); 

         },
         error: function() {

         }
      });         
   }

   function add_new_row(){
      var tr = '';
      tr += '<tr>'+
                  '<td class="no">No</td>'+
                  '<td><select class="form-control input-sm grade_levelid" name="grade_levelid[]" data-examid="0"></select>'+
                  '</td>'+
                  '<td>\
                     <div class="input-group">\
                        <input type="text" class="form-control input-sm start_exam" name="start_exam[]" placeholder="dd/mm/yyyy">\
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>\
                     </div>\
                  </td>'+
                  '<td>\
                     <div class="input-group">\
                        <input type="text" class="form-control input-sm end_exam" name="end_exam[]" placeholder="dd/mm/yyyy">\
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>\
                     </div>\
                  </td>'+
                  '<td>\
                     <div class="input-group">\
                        <input type="text" class="form-control input-sm start_entry_score" name="start_entry_score[]" placeholder="dd/mm/yyyy">\
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>\
                     </div>\
                  </td>'+
                  '<td>\
                     <div class="input-group">\
                        <input type="text" class="form-control input-sm end_entry_score" name="end_entry_score[]" placeholder="dd/mm/yyyy">\
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>\
                     </div>\
                  </td>'+
                  '<td><a href="javascript:;" class="a_remove"><img src="<?= base_url('assets/images/icons/delete.png') ?>" width="14"></a>'+
                  '</td>'+
               '</tr>';

      $('#list tbody').append(tr);

      get_grade($('.schlevelid').val());
   }

   // get grade ======
   function get_grade(schlevelid){      
      $.ajax({
         url: '<?= site_url('school/c_setupexam/get_grade') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){
            $('#add_new_row').css("visibility", "hidden");
         },
         data: {
            schlevelid: schlevelid                
         },
         success: function(data){
            $('#list tr:last-child').find('.grade_levelid').html(data.opt);
            $('#add_new_row').css("visibility", "visible");
         },
         error: function() {

         }
      });         
   }

   // get grade change all ======
   function get_grade_change_all(mySelector, schlevelid){      
      $.ajax({
         url: '<?= site_url('school/c_setupexam/get_grade') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            schlevelid: schlevelid                
         },
         success: function(data){
            mySelector.html(data.opt);

         },
         error: function() {

         }
      });         
   }

   // get grade with search ======
   function get_grade_with_search(mySelector, schlevelid){      
      $.ajax({
         url: '<?= site_url('school/c_setupexam/get_grade') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            schlevelid: schlevelid                
         },
         success: function(data){
            mySelector.html(data.opt);
            mySelector.prepend('<option value="" selected="selected"></option');

         },
         error: function() {

         }
      });         
   }

   function clear(){
      $('#schoolid option:first').prop('selected', true);
      $('.schlevelid option:first').prop('selected', true);
      get_year($('#yearid'), '', $('.schlevelid').val());
      $('#examtypeid').val(''); 

      // set attr school level ======
      $('#add_new_row').attr("data-schlevelid", $('.schlevelid').val());

      var tr = '';
      tr += '<tr>'+
                  '<td class="no">1</td>'+
                  '<td><select class="form-control input-sm grade_levelid" name="grade_levelid[]" data-examid="0"></select>'+
                  '</td>'+
                  '<td>\
                     <div class="input-group">\
                        <input type="text" class="form-control input-sm start_exanm" name="start_exam[]" placeholder="dd/mm/yyyy">\
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>\
                     </div>\
                  </td>'+
                  '<td>\
                     <div class="input-group">\
                        <input type="text" class="form-control input-sm end_exam" name="end_exam[]" placeholder="dd/mm/yyyy">\
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>\
                     </div>\
                  </td>'+
                  '<td>\
                     <div class="input-group">\
                        <input type="text" class="form-control input-sm start_entry_score" name="start_entry_score[]" placeholder="dd/mm/yyyy">\
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>\
                     </div>\
                  </td>'+
                  '<td>\
                     <div class="input-group">\
                        <input type="text" class="form-control input-sm end_entry_score" name="end_entry_score[]" placeholder="dd/mm/yyyy">\
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>\
                     </div>\
                  </td>'+
                  '<td><a href="javascript:;" class="a_remove"><img src="<?= base_url('assets/images/icons/delete.png') ?>" width="14"></a>'+
                  '</td>'+
               '</tr>';
      $('#list tbody').html(tr); 

      get_grade($('.schlevelid').val());

   }

   // grid =========
   function grid(current_page = 0, total_display = 0){
      var offset = ((current_page - 1) * total_display) - 0;
      var limit = total_display - 0;

      $.ajax({
         url: '<?= site_url('school/c_setupexam/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            offset: offset,
            limit: limit
            ,
            grade_list_search: $('#grade_list_search').val(),
            schoolid_list_search: $('#schoolid_list_search').val(),
            schlevelid_list_search: $('#schlevelid_list_search').val(),
            yearid_list_search: $('#yearid_list_search').val(),
            examtypeid_list_search: $('#examtypeid_list_search').val()
         },
         success: function(data) {
            $('#list_data tbody').html(data.tr); 

            var page = '';
            var total = '';
            if(data.totalRecord - 0 > 0){
               // previous ====
               page += '<button type="button" class="btn btn-default btn-sm a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-sm a-pagination" data-current_page="' + (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next<i class="glyphicon glyphicon-chevron-right"></i></button>';

               // var opt = '';
               // var to = data.totalRecord - 0;
               // for (var i = 10; i <= to; i += 10) {
               //    opt += '<option value="'+ i +'">'+ (to - i < i ? to : i) +'</option>';
               //    // if(to - i < i){
               //    //    opt += '<option value="'+ i +'">'+ to +'</option>';
               //    // }
               //    if(i == 1000){                     
               //       return false;
               //    }                  
               // }
               
            }
            
            // $('#limit_record').html(opt);
            $('.pagination').html(page);
         },
         error: function(){

         }
      });
   }
</script>