<style>
   .tdrow tr,.tdrow th,.tdrow td{
    border: none !important; 
  }
</style>
<?php
//print_r($query);
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
  }
?>
<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
    <div id="main_content">
      
       <div class="result_info">
        <div class="col-sm-6">
            <strong>Setup Group</strong>
        </div>
        <div class="col-sm-6" style="text-align: right">
            <strong>                
            </strong>   
          <!-- Block message -->
            <?php if(isset($_GET['save'])){
               echo "<p>Your data has been saved successfully...!</p>";
              }else if(isset($_GET['edit'])){
                  echo "<p>Your data has been updated successfully...!</p>";
              }else if(isset($_GET['delete'])){
                  echo "<p style='color:red'>Your data has been deleted successfully...!</p>";
              }
            ?>
          <!-- End block message -->
        </div> 
      </div> 
      
      <form method="post" accept-charset="utf-8" class="tdrow" action="<?php echo site_url("school/subjecttype/updatesubjecttype?m=$m&p=$p"); ?>" id="fsubjecttype">
            <input type="text" style="display:none;" name="txtsubjecttype_id" id='txtsubjecttype_id' value="<?php echo $query->subj_type_id;?>"/>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel-body">
                    <div class="form_sep">
                        <label class="req" for="school_id">School<span style="color:red">*</span></label>
                        <select class="form-control" id='school_id' name='school_id' min='1' required
                                data-parsley-required-message="Select any school">
                            <option value=''>Select School</option>
                            <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                                <option
                                    value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $query->schoolid) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel-body">
                    <div class="form_sep">
                        <label class="req" for="schlevel_id">School Level</label>
                        <select class="form-control" id="schlevel_id" required name="schlevel_id">
                            <?php if (isset($schevels) && count($schevels) > 0) {
                                foreach ($schevels as $sclev) {
                                    echo '<option value="' . $sclev->schlevelid . '" '.($sclev->schlevelid==$query->schlevelid? "selected":"").'>'.$sclev->sch_level . '</option>';
                                }
                            } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 hide">
                <div class="panel-body">
                    <div class="form_sep">
                        <label class="req" for="yearid">Academic Year<span style="color:red">*</span></label>
                        <select class="form-control" id="yearid" required name="yearid">
                            <option value=""></option>
                            <?php if (isset($schyears) && count($schyears) > 0) {
                                foreach ($schyears as $schyear) {
                                    echo '<option value="' . $schyear->yearid . '" '.($query->yearid == $schyear->yearid?"selected":"").' >' . $schyear->sch_year . '</option>';
                                }
                            } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 hide">
                <div class="panel-body" style="">
                    <div class="form_sep">
                        <label for="gradelevelid">Grade Level</label>
                        <select class="form-control" id="gradelevelid" required name="gradelevelid">
                            <option value=""></option>
                            <?php if(isset($rangelevs)) {
                                foreach($rangelevs as $row){
                                    echo '<option value="'.$row->gradelevelid.'" '.($row->gradelevelid == $query->gradelevelid?"selected":"").' >'.$row->rangelevelname.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                </div>
            </div>

           <div class="col-sm-6">           
                <div class="panel-body">
                    <div class="form_sep">
                      <label class="req" for="cboschool">Subject Group<span style="color:red">*</span></label>
                    <input type="text" name="txtsubjecttype" id="txtsubjecttype" class="form-control" required data-parsley-required-message="Enter subject type" value="<?php echo $query->subject_type?>" />
                    </div>
                    <div class="form_group hide">
                        <label for="is_moeys"> &nbsp; for Program </label>
                        <select name="is_moeys" id="is_moeys" class="form-control">
                            <option value="1" <?php echo ($query->is_moeys==1?"selected":"") ?>>MoEYS</option>
                            <option value="0" <?php echo ($query->is_moeys==0?"selected":"") ?>>TAE</option>
                        </select>
                    </div>
                    <div class="form_sep">
                      <label class="req" for="student_num">Description</label>
                      <textarea name="txtdescription" id="txtdescription" style="width:100%;height:40px; resize:none;" class="form-control"><?php echo $query->note?></textarea>
                    </div>  
                    <div class="form_sep"​ style="display: none;">
                      <label class="req hidable" for="orders">Is Group Calculate</label>
                      <select name="is_group_calc" id="is_group_calc" class="form-control">
                            <option value="1" <?php echo ($query->is_group_calc=="1"?"selected":"");?> >Yes</option>
                            <option value="0"  <?php echo ($query->is_group_calc=="0"?"selected":"");?> >No</option> 
                      </select>
                    </div>
                       <div class="form_sep"​ style="display: none;">
                          <label class="req hidable" for="orders">Is Group Calculate Percent(%)</label>
                          <input type="number" name="is_group_calc_percent" id="is_group_calc_percent" class="form-control" data-parsley-required-message="Enter Percent" data-parsley-type="number" value="<?php echo $query->is_group_calc_percent; ?>">
                    </div>
                     <div class="form_sep"​ style="display: none;">
                          <label class="req hidable" for="orders">Is Attendance Qty</label>
                          <input type="number" name="is_at_qty" id="is_at_qty" class="form-control" data-parsley-required-message="Enter Quantity" data-parsley-type="number" value="<?php echo $query->is_attendance_qty; ?>">
                    </div>                  
                </div>
            </div>
            
            <div class="col-sm-6">                          
              <div class="panel-body">
                  <div class="form_sep">
                      <label class="req" for="cboschool">Main Subject</label>
                      <select name="txtmaintype" id="txtmaintype" class="form-control">
                            <option value="1" <?php echo ($query->main_type=="1"?"selected":"");?> >ចំណេះដឹងទូទៅភាសាខ្នែរ</option>
                            <option value="0"  <?php echo ($query->main_type=="0"?"selected":"");?> >ភាសាបរទេស​ និងជំនាញ</option>                           
                      </select>
                  </div>                
                  <div class="form_sep">
                      <label class="req hidable" for="orders">Order</label>
                      <input type="text" name="orders" id="orders" class="form-control hidable" value="<?php echo $query->orders ?>" />
                  </div>   
                   <div class="form_sep"​ style="display: none;">
                      <label class="req hidable" for="orders">Is Class Participation</label>
                      <select name="is_cp" id="is_cp" class="form-control">
                            <option value="1" <?php echo ($query->is_class_participation=="1"?"selected":"");?> >Yes</option>
                            <option value="0"  <?php echo ($query->is_class_participation=="0"?"selected":"");?> >No</option>                           
                      </select>
                  </div>              
                   <div class="form_sep"​ style="display: none;">
                      <label class="req hidable" for="orders">Display In report</label>
                      <select name="report_display" id="report_display" class="form-control">
                            <option value="1" <?php echo ($query->is_report_display=="1"?"selected":"");?> >Yes</option>
                            <option value="0"  <?php echo ($query->is_report_display=="0"?"selected":"");?> >No</option>
                      </select>
                  </div>
              </div> 
            </div>  
        </div>
        
        <div class="row">
          <div class="col-sm-12">
            
            <div class="panel-body">
                <div class="form_sep">
                <?php if($this->green->gAction("C")){ ?>
                 <input type="submit" name="btnsavesubjecttype" id='btnsavesubjecttype' value="Save" class="btn btn-primary" />
                <?php } ?>
                <input type="button" name="btncancel" id='btncancel' value="Cancel" class="btn btn-warning" />
                </div>                               
            </div>
            
          </div>
        </div>
        
       </form>         
    </div>
    
 </div>  
</div>

<script type="text/javascript">
    $(function() {
         $('#fsubjecttype').parsley();
        // $('#fsubjecttype').parsley().validate();
         $("#btncancel").click(function(){
             var r = confirm("Do you want to cancel?");
                 if (r == true) {
                     location.href=("<?php echo site_url('school/subjecttype/?m='.$m.'&p='.$p.'');?>");  
               } else {
                    
            }
        });
        
        //$("#btnsavesubjecttype").click(function(e) {
         $('body').delegate('#btnsavesubjecttype', 'click', function(e){                       
            updatesubjecttype(e);           
        });
        $("body").delegate("#schlevel_id", "change", function () {
            var sch_levelid = $(this).val();
            var schoolid = $("#school_id").val();
            //if (sch_levelid != "") {
               // getsch_year(schoolid,sch_levelid);
               // getRangeLevels(sch_levelid);
           // } else {
               //$('#schlevelid').html("");
            //}
        });
    });
        //------ this function is not used any more ------
    function updatesubjecttype(e){        
            var subj_type_id=$('#txtsubjecttype_id').val();
            var subjecttype= $('#txtsubjecttype').val();
            var maintype= $('#txtmaintype').val(); 
            var description= $('#txtdescription').val();
            var schoolid= $('#school_id option:selected').val();
            var schlevelid= $('#schlevel_id option:selected').val();
            var orders= $('#orders').val();
            var is_cp= $('#is_cp').val();
            var is_group_calc= $('#is_group_calc').val();
            var is_group_calc_percent= $('#is_group_calc_percent').val();
            var is_at_qty= $('#is_at_qty').val();
            var report_display= $('#report_display').val();
            if(schoolid==''){
                toastr["warning"]("School info was required !");
            }else if(schlevelid==''){
                toastr["warning"]("School level required !");
            }else if(subjecttype==''){
                toastr["warning"]("subject group was required !");
            }else if(maintype==''){
                toastr["warning"]("Main subject was required !");
            }else{                    
                $.ajax({
                    //school/subjecttype is controller
                    url:"<?php echo base_url(); ?>index.php/school/subjecttype/savesubjecttype",    
                    data: {
                            'subj_type_id':subj_type_id,
                            'subjecttype':subjecttype,
                            'maintype':maintype, 
                            'description':description,
                            'schoolid':schoolid,
                            'schlevelid':schlevelid,
                            'orders':orders,
                            'is_cp':is_cp,
                            'is_group_calc':is_group_calc,
                            'is_group_calc_percent':is_group_calc_percent,
                            'is_at_qty':is_at_qty,
                            'report_display':report_display
                          },
                    type: "POST",
                    success: function(data){
                      if(data.res==2){
                          toastr["success"]("Subject group was updated!");
                      }else{
                           toastr["warning"]("Subject group is already existed !");
                      }
                      f_clear(e);
                      search(e);
                    }
                });
            }
        
    }  
    function f_clear(event) {
        $('#txtsubjecttype').val(""); 
        $('#txtmaintype').val(""); 
        $('#txtdescription').val("");
        $('#orders').val("");
        $('#is_group_calc_percent').val("");
        $('#is_at_qty').val("");
    }
    function getRangeLevels(schlevelid) {

        $("#gradelevelid").html("");
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo site_url('school/rangeslevelfee/getRangeLevels'); ?>/" + schlevelid,
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (res) {
                    var data = res.datas;
                    var tr = "";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="'+row.gradelevelid+'">'+row.rangelevelname+'</option>';
                        }
                    }
                    $("#gradelevelid").html(tr);
                }
            })
        }
    }

    function getsch_year(schoolid,sch_level) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/school/subjecttype/cgetschoolyear",
            data: {'schoolid':schoolid, 'sch_level': sch_level},
            async: false,
            type: "post",
            success: function (data) {
                $('#yearid').html(data.schyear);
            }
        });
    }
</script>