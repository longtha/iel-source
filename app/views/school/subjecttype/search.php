<style type="text/css">
  table{
   border-collapse: collapse;
   /*border:1px solid #CCCCCC;*/
  }
  td,th{
   
   padding: 5px ;
  }
  #listsubjecttype td,#listsubjecttype th{
   padding: 5px ;
   border:1px solid  #CCCCCC;
  }
  #pgt{
   border:solid 0px !important;
  }
  th{
    background-color: #383547;
    text-align: center;
    color: white;
  }
  a{
    cursor: pointer;
  }
 </style>

<h3 align="center">Subject Type List</h3>
<table border="0" cellpadding="5" cellspacing="0"​ width="970" align="center" id='listsubjecttype'>
    <thead>
        <th align=center width=40>No</th>
        <th width=170>Subject Group</th>        
        <th>Description</th>
        <th width=170>Main Subject</th>
        <th width=170>School Level</th>
        <th width=170>Group Calc</th>
        <th width=100>Percent(%)</th>
        <th width=100>Is CP</th>
        <th width=100>Is AT Qty</th>
        <th width=130>Action</th>
    </thead>
    <tr>
        <td></td>
        <td><input type="text" name="txtsearchsubjecttype" id="txtsearchsubjecttype" onkeyup='search(event);' class="form-control" /></td>
        <td><input type="text" name="txtsearchmaintype" id="txtsearchmaintype" onkeyup='search(event);' class="form-control" /></td>
        <td></td>
      <td></td>
    </tr>
    <tbody id='bodylist'>
    <?php
        $i=1;
    
    foreach($query as $subjecttyperow){
          
    echo "
          <tr>
            <td align=center width=40>$i</td>
            <td width=170>$subjecttyperow->subject_type</td>
            <td>$subjecttyperow->note</td>
            <td>$subjecttyperow->main_type</td>
            <td width=170>$subjecttyperow->sch_level</td>
            <td>".($subjecttyperow->is_group_calc==1?"Yes":"No")."</td>
            <td>".$subjecttyperow->is_group_calc_percent."</td>
            <td>".($subjecttyperow->is_class_participation==1?"Yes":"No")."</td>
            <td>".$subjecttyperow->is_attendance_qty."</td>
            <td width=170 class='hide'>".$arrMo[$subjecttyperow->is_moeys]."</td>
            <td align=center width=130>";
                if($this->green->gAction("U")){
                  echo "<a href='".site_url('school/subjecttype/editsubjecttype/'.$subjecttyperow->subj_type_id)."?m=$m&p=$p"."'>
                        <img src='".site_url('../assets/images/icons/edit.png')."' />
                        </a> |";
                }
                if($this->green->gAction("D")){
                  echo "<a><img onclick='deletesubjecttype(event);' rel='$subjecttyperow->subj_type_id' src='".site_url('../assets/images/icons/delete.png')."' />
                        </a>"; 
                }
            echo "</td>
          </tr>" ;
        $i++;
    }
    ?>
    <tr>
      <td colspan='10' id='pgt'>
        <ul class='pagination'>
          <?php echo $this->pagination->create_links();?>
        </ul>
      </td>
    </tr>
   </tbody> 
</table>

<script type="text/javascript">
    $(document).ready(function(e) {
              
    })
     function deletesubjecttype(event){
           var r = confirm("Are you sure to delete this item?");
           if (r == true) {
               var id=jQuery(event.target).attr("rel");
               //school/subjecttype is controller
              location.href="<?PHP echo site_url('school/subjecttype/deletesubjecttype');?>/"+id;
           } else {
               txt = "You pressed Cancel!";
           }      
    }
    function search(event){
        var subjecttype=jQuery('#txtsearchsubjecttype').val();
        var maintype=jQuery('#txtsearchmaintype').val();
        $.ajax({
           url:"<?php echo base_url(); ?>index.php/school/subjecttype/search",    
           data: {'subjecttype':subjecttype,'maintype':maintype},
           type: "POST",
           success: function(data){
              //alert(data);
              jQuery('#bodylist').html(data);                   
           }
         });
     } 
</script>