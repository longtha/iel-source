<!-- <div id='msg' style='color:white; background-color:#286090;height:30px;  text-align:center'></div> -->

<div class="wrapper">
  <div class="clearfix" id="main_content_outer">
    <div id="main_content">
      
       <div class="result_info">
        <div class="col-sm-4">
          <strong>Setup Teacher Assignation</strong>  
        </div>
        <div class="col-sm-6" style="text-align: right">
           
          <!-- Block message -->
            <?php if(isset($exist)) echo $exist ?>
            <?PHP if(isset($_GET['save'])){
                  echo "<p id='exist'>Your data has been saved successfully...!</p>";
              }else if(isset($_GET['edit'])){
                  echo "<p id='exist'>Your data has been updated successfully...!</p>";
              }else if(isset($_GET['delete'])){
                  echo "<p style='color:red;'>Your data has been deleted successfully...!</p>";
              }
              echo "<p style='color:red;' id='exist'></p>";
              //echo "";
            ?>
          <!-- End block message -->
        </div>

        <div class="col-sm-2" align="right">
            <strong>
               <a title="export" id="exportreport" href="javascript:void(0)">
                   <img src="<?php echo base_url('assets/images/icons/export.png') ?>" height="24">
               </a>
            </strong>
            <strong>
               <a title="Print" id="printreport" href="javascript:void(0)">
                   <img src="<?php echo base_url('assets/images/icons/print.png') ?>" height="24">
               </a>
            </strong>
            <strong>
               <a title="Copy" id="copy" href="JavaScript:void(0)">
                   <img src="<?php echo base_url('assets/images/icons/copy.png') ?>" height="24" data-target="#dlg_copy" data-toggle='modal'>
               </a>
            </strong>
        </div>
      </div> 
      <?php
          $m='';
          $p='';
          if(isset($_GET['m'])){
              $m=$_GET['m'];
          }
          if(isset($_GET['p'])){
              $p=$_GET['p'];
         }

      ?>
      
        <div class="row">          
       
          <div class="col-sm-6">         
                  <div class="form_sep">
                        <label class="req" for="cboschool">Year<span style="color:red">*</span></label>
                        <!--<input type="text" name="txtyear" id="txtyear" class="form-control" maxlength="4" onkeypress='return isNumberKey(event);' required data-parsley-required-message="Enter year" />-->
                        <select class="form-control" id='cboyear' name='cboyear' min=1 required data-parsley-required-message="Select any school">
                            <option value='0'>Select year</option>
                           <?php foreach ($this->tclass->getschoolyear() as $row) { ?>
                               <option value='<?php echo $row->yearid; ?>' <?php if($row->yearid==$this->session->userdata('year')) echo "selected";?> > <?php echo $row->sch_year;?></option>
                           <?php } ?>
                        </select>
                  </div> 
                  <div class="form_sep">
                       <label class="req" for="cboschlevel">School level<span style="color:red">*</span></label>
                       <select class="form-control" id='cboschoolevel' name='cboschoolevel' min=1 required data-parsley-required-message="Select any school level">
                            <option value='0'>Select School level</option>
                            <?php
                            foreach ($this->tclass->getschlevel() as $row) {?>
                            <option value='<?php echo $row->schlevelid ?>'<?php if(isset($_GET['sl'])) if($_GET['sl']==$row->schlevelid) echo 'selected' ?>><?php echo $row->sch_level?></option>
                           <?php  }
                           ?>
                        </select>
                  </div> 
                  <div class="form_sep">
                       <label class="req" for="grandlavel">Grand level<span style="color:red">*</span></label>
                       <input type="hidden" name="htypeno" id="htypeno">
                       <select class="form-control" id='grandlavel' name='grandlavel' min=1 required data-parsley-required-message="Select any grand level">
                            
                        </select>
                  </div> 
          </div>
          <div class="col-sm-6">
              <div class="form_sep">
                    <label class="req" for="cboschool">Teacher<span style="color:red">*</span></label>
                    <SELECT name="txtteacher" id="txtteacher" class="form-control" required data-parsley-required-message="Fill teacher">
                    <?php
                      echo "<option value='0'></option>";
                      foreach($this->tclass->getTeacherOpt() as $row_teach){
                          echo "<option value='".$row_teach->empcode."'>".$row_teach->first_name.'&nbsp;'.$row_teach->last_name."</option>";
                      }
                    ?>
                    </SELECT>
                    <input type="text" style="display:none;" name="txtgetteacherid" id="txtgetteacherid" />
                    <input type="text" style="display:none;" name="txttransno" id='txttransno' value=""/>
              </div>           
              <div class="form_sep">
                    <table class='table'>
                          <!--<td>
                               start upload image signature -->
                              <!-- <div align='center'>
                                <form enctype="multipart/form-data" method="post" accept-charset="utf-8" action="<?php echo site_url("school/tearcherclass/uploate_save")?>" class="tdrow" id="fschyear" target="ifram">
                                    <img  src="<?php //echo site_url('../assets/upload/No_person.jpg') ?>" id="uploadPreview" style='width:88px; height:77px; margin-bottom:15px; border:1px solid #CCCCCC;'>
                                    <input type="hidden" name="htypeno_img" id="htypeno_img">
                                    <input id="uploadImage" accept="image/gif, image/jpeg, image/jpg, image/png" type="file" class="sign_img" name="userfile[]"  onchange="PreviewImage();" style="visibility:hidden; display:none" />
                                    
                                </form> 
                                <iframe name="ifram" style="display:none"></iframe> 
                              </div>
                              <div align='center'>
                                    <input type='button' class="btn btn-success" onclick="$('#uploadImage').click();" value='Signature'/>
                              </div> -->
                              <!-- end upload image signature 
                          </td>-->
                          <td style="margin:0;padding:0;"><label class="req" for="cboschool">Description</label>
                              <textarea name="txtaddress" id="txtaddress" style="height:55px;border-radius:4px; resize:none;" class="form-control"></textarea>
                          </td>
                    </table>                 
              </div>
          </div>
          
          <div class="col-sm-12">
              <div class="panel-body">
                  <div class="form_sep">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <th class="col-sm-4">Handle Class</th>
                            <th class="col-sm-4">Assign Class</th>
                            <th class="col-sm-4">Subject</th>
                          </thead>
                          <tbody id="body_showClass">
                            <!-- <tr>
                              <td><ul id="classhandle" style="list-style: none;padding:0;overflow-y:auto;height: 150px;"></ul></td>
                              <td><ul id="assinclass" style="list-style: none;padding:0;"></ul></td>
                              <td><ul id="ul_subj" style="list-style: none;padding:0;overflow-y:auto;height: 150px;"></ul></td>
                            </tr> -->
                          </tbody>
                        </table>

                      </div>
                  </div> 
              </div>
          </div>
          <div  class="col-sm-12">
              <div class="form_sep">
                <?php if($this->green->gAction("C")){ ?>
                  <input type="button" name="btn_list" id='btn_list' value="ADD" class="btn btn-primary" />
                <?php } ?>
              </div>
          </div>
          <div  class="col-sm-12">
            <div class="form_sep">
                <div class='table-responsive' id="div_print">
                    <table class='table' style="width:100%;">
                      <thead class='thead'>
                        <th class="col-sm-2">School lavel</th>
                        <th class="col-sm-3">Handle class</th>
                        <th class="col-sm-3">Assign class</th>
                        <th class="col-sm-3">Subject</th>
                        <th class="col-sm-1" style="display:none;">Signature</th>
                        <th style='text-align: right;'>Delete</th>
                      </thead>
                      <tbody id='listclass'>
                        
                      </tbody>
                    </table>
                </div>  
            </div>
          </div>
        </div>     
        <div class="row">
          <div class="col-sm-12">
            
            <div class="panel-body">
                <div class="form_sep">
                  <?php if($this->green->gAction("C")){ ?>
                  <input type="button" name="btnsaves" id='btnsaves' value="Save" class="btn btn-primary" />
                  <?php } ?>
                  <input type="button" name="btncancel" id='btncancel' value="Cancel" class="btn btn-warning" />
                </div>                               
            </div>
            
          </div>
        </div>
        
       
              
    </div>
    
 </div>  
</div>

<!------------------- Modal copy ----------------------- -->
<div class="modal fade" id="dlg_copy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #C6EEEF">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Copy Class and Subject to Next Year</h4>
            </div>
            <div class="panel-body">
                <div class="form_sep">
                    <label class="req" for="">From Year</label>
                    <select class="form-control" id="from_year">
                        <?php echo $this->green->GetSchYear($this->session->userdata('schoolid'),$this->session->userdata('year'));?>
                    </select>
                </div>
                <div class="form_sep">
                    <label class="req" for="">To Year</label>
                    <select class="form-control" id="to_year">
                        <?php echo $this->green->GetSchYear($this->session->userdata('schoolid'),$this->session->userdata('year'));?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <span class="copy_warning" style="color: red"></span>
                <button type="button" id='save_copy' class="btn btn-primary save_copy">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

</div>
<!------------------- End of copy modal ---------------- -->
<style type="text/css">
   .tdrow tr,.tdrow th,.tdrow td{
    border: none !important;
   }
   table tbody tr td img{width: 20px; margin-right: 10px}
   .thead{
        border-style: solid;
        border-width: 2px;
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
        border-color: #DDDDDD;
  }
</style>

<script type="text/javascript"> 

$(function() {
  clear_data();
   //--------- parseley validation -----
    $('#fschyear').parsley(); 
        //autofillteacher();
        //autofillclasshandle(); 
        //autofillclass();
        //autofillsubject();
        //filterclass();
   

    $("body").delegate("#save_copy","click",function(){
      copySubClass();
    })

    $("body").delegate("#delete_tr","click",function(){

      var conf = confirm("Do you want to delete this record ?");
      if(conf == true){
        $(this).parent().parent().remove();
        $(".sign_img:last").remove();
      }else{
        return false;
      }
    })
    $("body").delegate("#btnsaves","click",function(){
       
        var yearid    = $("#cboyear").val();
        var teacherid = $("#txtteacher").val();
        var grandlavel = $("#grandlavel").val();
        var htypeno    = $("#htypeno").val();
        var description = $("#txtaddress").val();
        var arr_schoollavel = [];
        var arr_handle      = [];
        var arr_subject     = [];
        var arr_class       = [];
        $(".schlavelid").each(function(e){
            var tr = $(this).parent().parent();
            var val_schlavel = $(this).val();
            var grandid      = tr.find("#grandlavelid").val();
            if(val_schlavel != ""){
              arr_schoollavel[e] = {"schlavelid":val_schlavel,"grandid":grandid};
              
              var arr_h =[];
              var arr_s =[];
              var arr_c =[];
              tr.find(".handleid_save").each(function(hd){
                arr_h[hd] = $(this).val();
              })
              tr.find(".subject_save").each(function(sj){
                  arr_s[sj] = $(this).val();

              })
              tr.find(".class_save").each(function(cs){
                var classid = $(this).val();
                var grandid = $(this).attr("att_grandid");
                arr_c[cs] = {'grandid':grandid,'classid':$(this).val()};
              })
              arr_handle[e]  = arr_h;
              arr_subject[e] = arr_s;
              arr_class[e]   = arr_c;
            }
        })
        if(teacherid == 0){
          alert("Please choose teacher before proccess !");
          $("#txtteacher").focus();
        }else{
            var cons = confirm("Do you want to save ?");
            if(cons == true){
              $.ajax({
                  url:"<?php echo site_url('school/c_teacher_subject/save_handle_assign_subject'); ?>",
                  type:"POST",
                  dataType:"JSON",
                  async:false,
                  data: {
                    add_save : 1,
                    yearid   : yearid,
                    teacherid: teacherid,
                    typeno   : htypeno,
                    description : description,
                    arr_schoollavel : arr_schoollavel,
                    grandlavel : grandlavel,
                    arr_handle : arr_handle,
                    arr_subject : arr_subject,
                    arr_class   : arr_class
                  },
                  success: function(res){
                    $("#htypeno_img").val(res['typeno']);
                    $("#bodylist").html(res);
                    $("#fschyear").submit();
                  }
              });
              clear_data();
              search();
            }else{ return false;}
        }
    })
    $("body").delegate("#delete_class,#delete_subject,#delete_handle","click",function(){
      var conf = confirm("Do you want to delete this class ?");
      if(conf == true){
        $(this).parent().parent().remove();
      }
    })
    
    $("body").delegate("#cboschoolevel","change",function(){
      var teacherid = $("#txtteacher").val();
      if(teacherid == "" || teacherid == 0){
        alert("Please choose teacher first.");
        $(this).val(0);
      }else{
        grandoption();
        $("#body_showClass").html("");
      }
      
    })
    $("body").delegate("#grandlavel","change",function(){
      var grand = $(this).val();
      if( grand == 0){
        $("#body_showClass").html("");
      }else{
        show_grandlavel("");
      }
      
    })
    $("#sign_img").on("change",function(){
        PreviewImage($(this));
    })
    $("#btn_img").on("click",function(){
        $("#uploadImage").click();
    })
    $("#btn_list").on("click",function(){
        var tr_c = "";
        var tr_h = "";
        var tr_s = "";
        var grandid = $("#grandlavel").val();
        var schlavelid     = $("#cboschoolevel").val();
        var grandlavelname = $("#grandlavel").val();
        var val_ch = 0;
        $(".chclass:checked").each(function(i){
          var label_class = $(this).attr("att_label_class");
          var classid     = $(this).attr("att_assignclass");
          var grandid     = $(this).attr("grandid");
          if($(this).is(':enabled')){
            tr_c+= '<tr><td class="col-sm-1" style="margin:0;padding:0; vertical-align: text-top;"><input type="hidden" name="class_save" class="class_save" att_grandid="'+grandid+'" value="'+classid+'">'+(i+1)+'.</td><td class="col-sm-9" style="margin:0;padding:0;">'+label_class+'</td><td class="col-sm-2" style="margin:0;padding:0;text-align:center;"><a href="javascript:void(0)" id="delete_class" class="glyphicon glyphicon-remove" style="font-size:15px;text-decoration:none;"></a></td></tr>';
            val_ch++;
          }
        })
        $(".ishandle:checked").each(function(e){
          var label_ishandle = $(this).attr("att_label_handle");
          var handleid       = $(this).attr("att_ishandle");
          if($(this).is(':enabled')){
            tr_h+= '<tr><td class="col-sm-1" style="margin:0;padding:0; vertical-align: text-top;"><input type="hidden" class="handleid_save" hand_grandid="'+grandid+'" value="'+handleid+'">'+(e+1)+'.</td><td class="col-sm-9" style="margin:0;padding:0;">'+label_ishandle+'</td><td class="col-sm-2" style="margin:0;padding:0;text-align:center;"><a href="javascript:void(0)" class="glyphicon glyphicon-remove" id="delete_handle" style="font-size:15px;text-decoration:none;"></a></td></tr>';
            val_ch++;
          }
        })
        $(".subject:checked").each(function(j){
          var label_subj = $(this).attr("att_label_subj");
          var subjectid  = $(this).attr("subjid");
          if($(this).is(':enabled')){
            tr_s+= '<tr><td class="col-sm-1" style="margin:0;padding:0; vertical-align: text-top;"><input class="subject_save" sub_grandid="'+grandid+'" value="'+subjectid+'" type="hidden">'+(j+1)+'.</td><td class="col-sm-9" style="margin:0;padding:0;">'+label_subj+'</td><td class="col-sm-2" style="margin:0;padding:0;text-align:center;"><a href="javascript:void(0)" class="glyphicon glyphicon-remove" id="delete_subject" style="font-size:15px;text-decoration:none;"></a></td></tr>';
            val_ch++;
          }
        })

        var tbl_class   = '<table style="width:100%;" class="tblc_'+schlavelid+'_'+grandid+'"><tr><td colspan="3"><b>Grand Lavel :&nbsp;'+grandlavelname+'</b></td></tr>'+tr_c+'</table>';
        var tbl_handle  = '<table style="width:100%;" class="tblh_'+schlavelid+'_'+grandid+'"><tr><td colspan="3"><b>Grand Lavel :&nbsp;'+grandlavelname+'</b></td></tr>'+tr_h+'</table>';
        var tbl_subject = '<table style="width:100%;" class="tbls_'+schlavelid+'_'+grandid+'"><tr><td colspan="3"><b>Grand Lavel :&nbsp;'+grandlavelname+'</b></td></tr>'+tr_s+'</table>';

        var schoolevelName = $("#cboschoolevel option:selected").html();
        if(val_ch > 0)
        {
            var length_tr = $(".tr_"+schlavelid).length;
            if(length_tr == 0)
            {
              var tr ='<tr class="tr_'+schlavelid+' tr_'+schlavelid+'_'+grandid+'"><td style="margin:0;padding:0;">'+schoolevelName+'<input type="hidden" value="'+schlavelid+'" id="schlavelid" class="schlavelid"><input type="hidden" id="grandlavelid" class="grandlavelid" value="'+grandid+'"></td>';
                  tr+='<td style="margin:0;padding:0;" class="tdh_'+schlavelid+'">'+tbl_handle+'</td><td class="tdc_'+schlavelid+'" style="margin:0;padding:0;">'+tbl_class+'</td><td style="margin:0;padding:0;" class="tds_'+schlavelid+'">'+tbl_subject+'</td>';
                  tr+='<td style="display:none;"><div align="center">';
                  tr+='<form enctype="multipart/form-data" method="post" accept-charset="utf-8" action="<?php echo site_url("school/tearcherclass/uploate_save")?>" class="tdrow" id="fschyear" target="ifram">';
                  tr+='<img  src="<?php echo site_url("../assets/upload/No_person.jpg") ?>" id="uploadPreview" style="width:92px; height:77px; margin-bottom:5px; border:1px solid #CCCCCC;">';
                  tr+='<input type="hidden" name="htypeno_img" id="htypeno_img"><input id="uploadImage" accept="image/gif, image/jpeg, image/jpg, image/png" type="file" class="sign_img" name="userfile[]"  style="" />';
                  tr+='</form><iframe name="ifram" style="display:none"></iframe></div>';
                  tr+='<div align="center" style="display:none;"><input type="button" id="btn_img" class="btn btn-success" value="Signature"/>';
                  tr+='</div></td>';
                  tr+='<td style="margin:0;padding:0;text-align:center;">';
                  tr+='<a href="javascript:void(0)" id="delete_tr" class="glyphicon glyphicon-remove" style="font-size:15px;text-decoration:none;"></a>&nbsp;&nbsp;';
                  tr+='<a href="javascript:void(0)" id="edit_tr" att_g="'+grandid+'" class="glyphicon glyphicon-pencil" style="font-size:12px;text-decoration:none;"></a>';
                  tr+='</td></tr>';
            }
            else
            {
              
                if($(".tr_"+schlavelid+"_"+grandid).length == 0){
                  var tr = '<tr class="tr_'+schlavelid+' tr_'+schlavelid+'_'+grandid+'"><td style="margin:0;padding:0;">'+schoolevelName+'&nbsp;<input type="hidden" value="'+schlavelid+'" id="schlavelid" class="schlavelid"><input type="hidden" id="grandlavelid" class="grandlavelid" value="'+grandid+'"></td>';
                      tr+='<td style="margin:0;padding:0;" class="tdh_'+schlavelid+'">'+tbl_handle+'</td><td class="tdc_'+schlavelid+'" style="margin:0;padding:0;">'+tbl_class+'</td><td style="margin:0;padding:0;" class="tds_'+schlavelid+'">'+tbl_subject+'</td>';
                      tr+='<td style="display:none;"><div align="center">';
                      tr+='<form enctype="multipart/form-data" method="post" accept-charset="utf-8" action="<?php echo site_url("school/tearcherclass/uploate_save")?>" class="tdrow" id="fschyear" target="ifram">';
                      tr+='<img  src="<?php echo site_url("../assets/upload/No_person.jpg") ?>" id="uploadPreview" style="width:92px; height:77px; margin-bottom:15px; border:1px solid #CCCCCC;">';
                      tr+='<input type="hidden" name="htypeno_img" id="htypeno_img"><input id="uploadImage" accept="image/gif, image/jpeg, image/jpg, image/png" type="file" class="sign_img" name="userfile[]"  style="" />';
                      tr+='</form><iframe name="ifram" style="display:none"></iframe></div>';
                      tr+='<div align="center" style="display:none;"><input type="button" class="btn btn-success" id="btn_img" value="Signature"/>';
                      tr+='</div></td>';
                      tr+='<td style="margin:0;padding:0;text-align:center;">';
                      tr+='<a href="javascript:void(0)" id="delete_tr" class="glyphicon glyphicon-remove" style="font-size:15px;text-decoration:none;"></a>&nbsp;&nbsp;';
                      tr+='<a href="javascript:void(0)" id="edit_tr" att_g="'+grandid+'" class="glyphicon glyphicon-pencil" style="font-size:12px;text-decoration:none;"></a>';
                      tr+='</tr>';
                }else{
                  var confr = confirm("Do you want to update ?");
                  if(confr == true)
                  {
                      var tr1 = '<td style="margin:0;padding:0;">'+schoolevelName+'&nbsp;<input type="hidden" value="'+schlavelid+'" id="schlavelid" class="schlavelid"><input type="hidden" id="grandlavelid" class="grandlavelid" value="'+grandid+'"></td>';
                          tr1+='<td style="margin:0;padding:0;" class="tdh_'+schlavelid+'">'+tbl_handle+'</td><td class="tdc_'+schlavelid+'" style="margin:0;padding:0;">'+tbl_class+'</td><td style="margin:0;padding:0;" class="tds_'+schlavelid+'">'+tbl_subject+'</td>';
                          tr1+='<td style="margin:0;padding:0;text-align:center;">';
                          tr1+='<a href="javascript:void(0)" id="delete_tr" class="glyphicon glyphicon-remove" style="font-size:15px;text-decoration:none;"></a>&nbsp;&nbsp;';
                          tr1+='<a href="javascript:void(0)" id="edit_tr" att_g="'+grandid+'" class="glyphicon glyphicon-pencil" style="font-size:12px;text-decoration:none;"></a>';
                          tr1+='</td>';
                      $(".tr_"+schlavelid+"_"+grandid).html(tr1);
                  }
              }  
            }
            $("#listclass").append(tr);
            // if($(".schlavelid").length > 1){
            //   $(".sign_img:first").clone().appendTo(".tdrow");
            //   //$(".tdrow").append('<input id="uploadImage" accept="image/gif, image/jpeg, image/jpg, image/png" name="userfile[]" class="sign_img" onchange="PreviewImage();" style="visibility:hidden; display:none" type="file">');
            // }
        }else{
          alert("Please check class and subject !");
        }
    })
    
    $("body").delegate("#edit_tr","click",function(){
        var tr = $(this).parent().parent();
        var att_grandid  = $(this).attr("att_g");
        var schoolavelid = tr.find("#schlavelid").val();
        $("#cboschoolevel").val(schoolavelid);
        grandoption();
        $("#grandlavel").val(att_grandid);
        var arr_ah_up  = [];
        var arr_ac_up = [];
        var arr_as_up = [];
        $(".tblh_"+schoolavelid+"_"+att_grandid).find(".handleid_save").each(function(){
          var this_h = $(this).val();
          if(this_h != ""){
            arr_ah_up["h_"+this_h] = this_h;
          }
          
        })
        $(".tblc_"+schoolavelid+"_"+att_grandid).find(".class_save").each(function(){
          var this_c = $(this).val();
          if(this_c != ""){
            arr_ac_up["c_"+this_c] = this_c;
          }
          
        })
        $(".tbls_"+schoolavelid+"_"+att_grandid).find(".subject_save").each(function(){
          var this_s = $(this).val();
          if(this_s != ""){
            arr_as_up["s_"+this_s] = this_s;
          }
         
        })
        var att_all = [arr_ah_up,arr_ac_up,arr_as_up];
        show_grandlavel(att_all);
        
    });
    $("body").delegate("#edite_assign","click",function(){

      var tr = $(this).parent().parent();
      var typeno          = $(this).attr("typeno_edit");
      var schlavelid_edit = tr.find("#schlavelid_edit").val();
      var teacherid_edit  = tr.find("#teacherid_edit").val();
      var description = tr.find("#description").val();
      $("#htypeno").val(typeno);
      $("#cboschoolevel").val(schlavelid_edit);
      $("#txtteacher").val(teacherid_edit);
      $("#htypeno_img").val(typeno);
      $("#txtaddress").val(description);
      grandoption();
      var grandlavel_edit = tr.find("#grandlavel_edit").val();
      $("#grandlavel").val(grandlavel_edit);
      var arr_ah_up  = [];
      var arr_ac_up  = [];
      var arr_as_up  = [];
      $(".classh_"+typeno).each(function(){
        var att_classid_h = $(this).attr("att_h_classid");
        if(att_classid_h != ""){
            arr_ah_up["h_"+att_classid_h] = att_classid_h;
        }
      });

      $(".classc_"+typeno).each(function(){
        var att_classid_c = $(this).attr("att_c_classid");
        if(att_classid_c != ""){
            arr_ac_up["c_"+att_classid_c] = att_classid_c;
        }
      });

      $(".classs_"+typeno).each(function(){
        var att_classid_s = $(this).attr("att_s_classid");
        if(att_classid_s != ""){
            arr_as_up["s_"+att_classid_s] = att_classid_s;
        }
      });
      var att_all = [arr_ah_up,arr_ac_up,arr_as_up];
      show_grandlavel(att_all);
      //$(".ishandle,.chclass,.subject").attr("disabled",false);
      $(".ishandle").each(function(){
        var ishandleid = $(this).attr("att_ishandle");
        if(arr_ah_up['h_'+ishandleid] == ishandleid){
          $(this).attr("disabled",false);
        }
      })
      $(".chclass").each(function(){
        var classid = $(this).attr("att_assignclass");
        if(arr_ac_up['c_'+classid] == classid){
          $(this).attr("disabled",false);
        }
      })
      $(".subject").each(function(){
        var subjid = $(this).attr("subjid");
        if(arr_as_up['s_'+subjid] == subjid){
          $(this).attr("disabled",false);
        }
      })
      $("#uploadPreview").attr("src","<?php echo site_url('../assets/upload/teacher/signature');?>/"+typeno+'.jpg');
      $("#btnsaves").val("Update")
      
    })
    $("body").delegate("#delete_assign","click",function(){
      var typeno = $(this).attr("typeno_del");
      var conf = confirm("Do you want to delete this record ?");
      if(conf == true)
      {
          $.ajax({
                url:"<?php echo site_url("school/c_teacher_subject/delete_record"); ?>",
                type:"POST",
                dataType:"HTML",
                async:false,
                data: {
                    'delete_record' : 1,
                    'typeno' : typeno
                },
                success: function(data){
                  $("#bodylist").html(data);
                }
          });
      }
    })
    $("#btncancel").on("click",function(){
        var conf = confirm("Do you want to cancel ?");
        if(conf == true){
          clear_data();
        }else { return false;}
        
    })
    $("body").delegate("#txtteacher","change",function(){
      //$("#hteacherid").val($(this).val());
      //$("#grandlavel").html("");
      // $("#classhandle").html("");
      // $("#assinclass").html("");
      // $("#ul_subj").html("");
      // $("#body_showClass").html("");
      // $("#listclass").html("");
      $("#uploadPreview").attr("src","<?php echo site_url('../assets/upload/No_person.jpg');?>");
    })
    $("body").delegate("#txtsearchteacher","focus",function(){
      autofillteacher();

    })
    $("body").delegate("#txtsearchteacher","change",function(){
      var tearchname_s = $(this).attr("tearchname_s");
      var this_name    = $(this).val();
        search();
    })
    // $("body").delegate("#printreport","click",function(){
    //   // var teacher=$('#txtsearchteacher').val();
    //   // var roleid=<?php //echo $this->session->userdata('roleid');?>;
    //   // var m=<?php //echo $m;?>;
    //   // var p=<?php //echo $p;?>;
    //   // window.open("<?php //echo site_url('school/tearcherclass/preview');?>?m="+m+"&p="+p+"teacher="+teacher);
    //   //var thl_html = $("#listsubject").html();
    //   var title="<h4 align='center'>"+ 'List Asign Teacher' +"</h4>";
    //   var data = $("#listsubject").html().replace(/<img[^>]*>/gi, "");
    //   var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
    //   var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
    //   gsPrint(title, export_data);
    // })
});   
function search(){
    var teacher=$('#txtsearchteacher').val();
    var year=$('#year').val();
    var l=$('#schlevelid').val();
    var roleid=<?php echo $this->session->userdata('roleid');?>;
    var m=<?php echo $m;?>;
    var p=<?php echo $p;?>;

    $.ajax({
        url:"<?php echo site_url('school/c_teacher_subject/seach'); ?>", 
        type: "POST",
        dataType:"HTML",
        data: {
              'teacherid':teacher,
              'year':year,
              'roleid':roleid,
              'm':m,
              'p':p,
              'l':l
        },
      
       success: function(data){
          $('#bodylist').html(data);                   
       }
    });
} 
function clear_data(){
      $("#txtteacher").val(0);
      $("#htypeno_img").val(0);
      $("#cboschoolevel").val(0);
      $("#txtaddress").val('');
      $("#grandlavel").html("");
      $("#body_showClass").html("");
      $("#htypeno").val("");
      $("#btnsaves").val("Save");
      // $("#ul_subj").html("");
      $("#listclass").html("");
      $("#uploadPreview").attr("src","<?php echo site_url('../assets/upload/No_person.jpg');?>");
      $(".sign_img:first").nextAll().remove();
}
//-------- Upload image signature -------------- 
function PreviewImage(get_this) {
      var oFReader = new FileReader();
      oFReader.readAsDataURL(get_this.files[0]);

      oFReader.onload = function (oFREvent) {
          document.getElementById("uploadPreview").src = oFREvent.target.result;
          document.getElementById("uploadPreview").style.backgroundImage = "none";
      };
};
// function PreviewImage() {
//       var oFReader = new FileReader();
//       oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

//       oFReader.onload = function (oFREvent) {
//           document.getElementById("uploadPreview").src = oFREvent.target.result;
//           document.getElementById("uploadPreview").style.backgroundImage = "none";
//       };
// };
  function fn_reload_img(){
      $.ajax({
          type:"post",
          url:"<?php echo site_url("school/c_teacher_subject/uploate_save"); ?>",
          data:{
            upload_img : 1,
            teacherid  : $("#txtteacher").val()
          },
          success:function(data){
            $("#fschyear").submit();
          }
      });
  }
    
//----- end upload image signature ------------
// start sophorn =========================================
function grandoption(){
    var grandlavel = $("#grandlavel").val();
    $.ajax({
        url:"<?php echo site_url("school/c_teacher_subject/grandOption"); ?>",
        type:"POST",
        dataType:"JSON",
        async:false,
        data: {
            'schlavelid' : $("#cboschoolevel").val(),
            'grandlavelid' : grandlavel
        },
        success: function(data){
          if(data['arr_gran'].length > 0)
          {
            var opt_grand = "<option value='0'></option>";
            $.each(data['arr_gran'],function(k,v){
                opt_grand+="<option value='"+v['grade_levelid']+"'>"+v['grade_level']+"</option>";
            })
            $("#grandlavel").html(opt_grand);
          }
        }
    });
}
function show_grandlavel(get_arr){
    var grandlavel = $("#grandlavel").val();
    $.ajax({
        url:"<?php echo site_url("school/c_teacher_subject/get_grandLavel"); ?>",
        type:"POST",
        dataType:"JSON",
        async:false,
        data: {
            'schlavelid' : $("#cboschoolevel").val(),
            'teacherid'  : $("#txtteacher").val(),
            'grandlavelid' : grandlavel
        },
        success: function(data){
            
                //$("#ul_subj").html("");
                var li_subj = "";
                if(data['arr_subj'].length > 0){                  
                  $.each(data['arr_subj'],function(ks,vs){
                    if(get_arr.length > 0){
                      var ch_s = "";
                      if(get_arr[2]["s_"+vs['subjectid']] == vs['subjectid']){
                        ch_s = "checked='checked'";
                      }  
                    } 
                    var disable_su = "";
                    if(data['ch_classhandle'][2]['ch_classs_'+vs['subjectid']] == vs['subjectid']){
                      ch_s = "checked='checked'";
                      disable_su = "disabled='disabled'";
                    } 
                    // li_subj+="<li><input type='checkbox' "+disable_su+" "+ch_s+" class='subject' subjid='"+vs['subjectid']+"' id='id_subj"+vs['subjectid']+"' att_label_subj='"+vs['subject']+"("+vs['subject_kh']+")'>&nbsp;&nbsp;<label for='id_subj"+vs['subjectid']+"'>"+vs['subject']+"("+vs['subject_kh']+")</label></li>";
                    li_subj+="<li><input type='checkbox' "+ch_s+" class='subject' subjid='"+vs['subjectid']+"' id='id_subj"+vs['subjectid']+"' att_label_subj='"+vs['subject']+"("+vs['subject_kh']+")'>&nbsp;&nbsp;<label for='id_subj"+vs['subjectid']+"'>"+vs['subject']+"("+vs['subject_kh']+")</label></li>";  
                    
                  })
                  //$("#ul_subj").html(li_subj);
                }
                //alert(data['ch_classhandle']['ch_classh_207']);
                         
                var opt_class = "";
                var opt_handle = "";
                if(data['arr_class'].length > 0)
                {
                    var a = 0;
                    var b = 0;
                    
                    $.each(data['arr_class'],function(k,v){
                        if(v['grade_levelid'] == grandlavel){
                          var ch_h = "";
                          var ch_c = "";
                          var ch_s = "";
                          if(get_arr.length > 0)
                          {
                            if(get_arr[0]["h_"+v['classid']] == v['classid']){
                              ch_h = "checked='checked'";
                            }
                            
                            if(get_arr[1]["c_"+v['classid']] == v['classid']){
                              ch_c = "checked='checked'";
                            }
                            if(get_arr[2]["s_"+v['classid']] == v['classid']){
                              ch_s = "checked='checked'";
                            }
                          }
                          var disable_ch = "";
                          var disable_c = "";
                          if(data['ch_classhandle'][0]['ch_classh_'+v['classid']] == v['classid']){
                            ch_h = "checked='checked'";
                            disable_ch = "disabled='disabled'";
                          }
                          if(data['ch_classhandle'][1]['ch_classc_'+v['classid']] == v['classid']){
                            ch_c = "checked='checked'";
                            disable_c = "disabled='disabled'";
                          }
                          opt_class+="<li style='margin-left:10px;padding:0;'><input type='checkbox' "+disable_c+" "+ch_c+" class='chclass' id='chclassassign"+a+"' grandid='"+v['grade_levelid']+"' att_assignclass='"+v['classid']+"'  att_label_class='"+v['class_name']+"'>&nbsp;&nbsp;<label for='chclassassign"+a+"'>"+v['class_name']+"</label></li>";
                          opt_handle+="<li style='margin-left:10px;padding:0;'><input type='checkbox' "+disable_ch+" "+ch_h+" id='chhandle"+b+"' class='ishandle' att_ishandle='"+v['classid']+"' att_label_handle='"+v['class_name']+"'>&nbsp;&nbsp;<label for='chhandle"+b+"'>"+v['class_name']+"</label></li>";
                          a++;
                          b++;
                        }
                    })
                    $tr_asign = '<tr>'+
                                '<td><ul id="classhandle" style="list-style: none;padding:0;overflow-y:auto;height: 150px;">'+opt_handle+'</ul></td>'+
                                '<td><ul id="assinclass" style="list-style: none;padding:0;overflow-y:auto;height: 150px;">'+opt_class+'</ul></td>'+
                                '<td><ul id="ul_subj" style="list-style: none;padding:0;overflow-y:auto;height: 150px;">'+li_subj+'</ul></td>'+
                                '</tr>';
                    $("#body_showClass").html($tr_asign);
                    //$("#assinclass").html(opt_class);
                    //$("#classhandle").html(opt_handle);  
                }
        }
    });
}
// end sophorn ===============================

//---------- auto complete ------------
function autofillteacher(){    
  var fillteacher="<?php echo site_url("school/c_teacher_subject/fillteacher");?>";
    $("#txtsearchteacher").autocomplete({
      source: fillteacher,
      minLength:0,
      select: function(events,ui) {          
        var f_id=ui.item.id;
        var fulname=ui.item.value;
        $(this).val(fulname);
        //$(this).val(f_id);
        // $("#txtsearchteacher").attr("tearchid_s",f_id)
        // $("#txtsearchteacher").attr("tearchname_s",fulname)
         search();
      }           
    });
}
// function fn_autoAuthorize() {
//       var url = "<?php echo site_url('interface/cinterface/cautoCustomer');?>";
//     $("#authorizeno").autocomplete({
//           source: url,
//           minLength: 0,
//           select: function (events, ui) {
//               $("#authorizeno").val(ui.item.id);
//               $("#tourcode").val(ui.item.agency_typeno);
//               $("#numbervisitor").val(ui.item.visitor_number);
//               $("#description").val(ui.item.description);
//           }
//     });
      
// }

// $('#btnsaves').click(function(){
//   //----- validate teacher class -----------
//   var countclass= $('.class_id').length;
//   //var countclasshandle=$('.class_handle_id').length;
//   var countsubject=$('.subjectid').length;
//   if(countclass>0 && countsubject>0){
//     $('#fschyear').submit();
//     $('#exist').html('');
//   }else{
//     $('#exist').html('Please select any class or subject for teacher...!');
//   }
// })
// function autofillclasshandle(){
//   var schlevelids=jQuery('#cboschoolevel').val();
//   var fillclasshandle="<?php echo site_url("school/tearcherclass/fillclasshandle")?>/"+schlevelids;
//     $("#txtclasshandle").autocomplete({
//       source: fillclasshandle,
//       minLength:0,
//       select: function(events,ui) {          
//         var class_handle_id=ui.item.id;
//         var exist=false;
//         $("#txtclasshandleid").val(class_handle_id); 
//         $('.class_handle_id').each(function(){
//             var old_class=$(this).val();
//            if(old_class==class_handle_id)
//               exist=true;
//         })
//         if(exist==false){
//            getclasshandletolist(class_handle_id);
//            $('#exist').html('');
//         }else{
//           $('#exist').html('Data is already exist...!');
//         }
//       }           
//     });
// }
// function autofillclass(){
//   var schlevelid=jQuery('#cboschoolevel').val();
//   var fillclass="<?php echo site_url("school/tearcherclass/fillclass")?>/"+schlevelid;
//     $("#txtclass").autocomplete({
//       source: fillclass,
//       minLength:0,
//       select: function(events,ui) {          
//         var class_id=ui.item.id;
//         var exist=false;
//         $("#txtclassid").val(class_id); 
//         $('.class_id').each(function(){
//             var old_class=$(this).val();
//            if(old_class==class_id)
//               exist=true;
//         })
//         if(exist==false){
//            getclasstolist(class_id);
//            $('#exist').html('');
//         }else{
//           $('#exist').html('Data is already exist...!');
//         }
//       }           
//     });
// }
// function autofillsubject(){    
//   var fillclass="<?php echo site_url("school/tearcherclass/fillsubject")?>";
//     $("#txtsubject").autocomplete({
//       source: fillclass,
//       minLength:0,
//       select: function(events,ui) {          
//         var subject_id=ui.item.id;
//         var exist=false;
//         $("#txtsubjectid").val(subject_id); 
//         $('.subjectid').each(function(){
//             var old_subject=$(this).val();
//            if(old_subject==subject_id)
//               exist=true;
//         })
//         if(exist==false){
//            getsubjecttolist(subject_id);
//            $('#exist').html('');
//         }else{
//           $('#exist').html('Data is already exist...!');
//         }
//       }           
//     });
// }
//---------- end autocomplete ----------------------

// function removerow(event){
//     var transno=$(event.target).attr('tran');
//     var r = confirm("Are you sure to delete this item !");
//       if (r == true) {
//         var row_class=$(event.target).closest('tr').remove();
//       }
// }

// function filterclass(event){
//     var schlevelid=jQuery('#cboschoolevel').val();
//     $.ajax({
//        url:"<?php echo site_url('school/tearcherclass/getschlevelid'); ?>",
//        data: {
//               'schlevelid':schlevelid,
//              },
//        type: "POST",
//        success: function(data){
//           jQuery('#cboclass').html(data);                  
//        }
//      });
//     //----- fill via school level -----
//     autofillclass();
//     autofillclasshandle();
// }

// $("#btncancel").click(function(){
//     var r = confirm("Do you want to cancel?");
//     if (r == true) {
//       location.href=("<?php echo site_url("school/tearcherclass/?m=$m&p=$p");?>");  
//     } else {
      
//     }       
// });

// function getclasstolist(class_id){
//     $.ajax({
//               url:"<?php echo site_url(); ?>school/tearcherclass/getclasstolist",    
//               data: {'classid':class_id},
//               type:"post",
//               success: function(data){
//                 $('#listclass').append(data);
//             }
//         });
//   }
//   function getclasshandletolist(class_handle_id){
//     $.ajax({
//               url:"<?php echo site_url(); ?>school/tearcherclass/getclasshandletolist",    
//               data: {'classhandleid':class_handle_id},
//               type:"post",
//               success: function(data){
//                 $('#listclasshandle').append(data);
//             }
//         });
//   }
  // function getsubjecttolist(subject_id){

  //   $.ajax({
  //             url:"<?php echo site_url(); ?>school/tearcherclass/getsubjecttolist",    
  //             data: {'subjectid':subject_id},
  //             type:"post",
  //             success: function(data){
  //               $('#listsubject').append(data);
  //           }
  //       });
  // }
    // function copySubClass(){
    //     var from_year=$("#from_year").val()-0;
    //     var to_year=$("#to_year").val()-0;
    //     var is_valid=1;
    //     if(from_year>=to_year){
    //         $(".copy_warning").text("From year can't equal to or bigger then to year");
    //         is_valid=0;
    //     }
    //     if(is_valid==1){
    //         $(".copy_warning").text("");
    //         $.ajax({
    //             url:"<?php echo site_url(); ?>school/c_teacher_subject/copyTeachSub",
    //             type:"POST",
    //             datatype:"Json",
    //             async:false,
    //             data: {'from_year':from_year,'to_year':to_year},
    //             success: function(res){
    //                 if(res.save==1){
    //                     $(".copy_warning").text("Class and Subjects by teacher already copied to next year");
    //                     search();
    //                 }
    //             }
    //         });
    //     }

    // }
 
</script>