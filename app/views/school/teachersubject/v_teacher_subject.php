<div class="container-fluid" style="width:100% !important;padding-bottom: 20px;">
<div class="row">
  <div class="col-xs-12">
     <div class="result_info">
        <div class="col-xs-6">
          <span class="icon">
              <i class="fa fa-th"></i>
          </span>
            <strong>Teacher Subject</strong>  
        </div>
        <div class="col-xs-6" style="text-align: right">
           <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
              <img src="<?= base_url('assets/images/icons/export.png') ?>" width="24px">
           </a>
           <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
              <img src="<?= base_url('assets/images/icons/print.png') ?>">
           </a>
           <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
              <img src="<?= base_url('assets/images/icons/copy.png') ?>"  height="24">
           </a>
        </div>         
     </div>
  </div>
</div>
<div class="row">          
    <div class="col-sm-6">         
        <div class="form_sep">
            <label class="req" for="cboschool">Year<span style="color:red">*</span></label>
            <select class="form-control" id='cboyear' name='cboyear' min=1 required data-parsley-required-message="Select any school">
                <option value='0'>Select year</option>
                <?php foreach ($this->tclass->getschoolyear() as $row) { ?>
                   <option value='<?php echo $row->yearid; ?>' <?php if($row->yearid==$this->session->userdata('year')) echo "selected";?> > <?php echo $row->sch_year;?></option>
               ​​​​ <?php } ?>
            </select>
        </div> 
        <div class="form_sep">
            <label class="req" for="sch_level">School level<span style="color:red">*</span></label>
            <select class="form-control" id='sch_level' name='sch_level' min=1 required data-parsley-required-message="Select any school level">
                <option value='0'>Select School level</option>
                <?php
	                foreach ($this->tclass->getschlevel() as $row) {?>
	                <option value='<?php echo $row->schlevelid ?>'<?php if(isset($_GET['sl'])) if($_GET['sl']==$row->schlevelid) echo 'selected' ?>><?php echo $row->sch_level?></option>
               <?php  }
               ?>
            </select>
        </div> 
        <div class="form_sep">
            <label class="req" for="grandlavel">Grade level<span style="color:red">*</span></label>
            <input type="hidden" name="htypeno" id="htypeno">
            <select class="form-control" id='grandlavel' name='grandlavel' min=1 required data-parsley-required-message="Select any grand level">
        	</select>
        </div> 
    </div>
  	<div class="col-sm-6">
	    <div class="form_sep">
	        <label class="req" for="teacher">Teacher<span style="color:red">*</span></label>
	        <SELECT name="teacher" id="teacher" class="form-control" required data-parsley-required-message="Fill teacher">
	        	<?php
                  echo "<option value='0'></option>";
                  foreach($this->tclass->getTeacherOpt() as $row_teach){
                      echo "<option value='".$row_teach->empcode."'>".$row_teach->first_name.'&nbsp;'.$row_teach->last_name."</option>";
                  }
                ?>
	        </SELECT>
	        <input type="text" style="display:none;" name="txtgetteacherid" id="txtgetteacherid" />
	        <input type="text" style="display:none;" name="txttransno" id='txttransno' value=""/>
	    </div>
	    <div>
	    	<label class="req" for="cbodescription">Description<span style="color:red">*</span></label>
	    	<textarea cols="5" rows="3" class="form-control"></textarea>
	    </div>
	</div>
</div>
</div>
<script type="text/javascript">
$(function(){
	$("body").delegate("#sch_level","change",function(){
      var teacherid = $("#teacher").val();
      if(teacherid == "" || teacherid == 0){
        alert("Please choose teacher first.");
        $(this).val(0);
      }else{
        grandoption();
        $("#body_showClass").html("");
      }
      
    })
});
	
function grandoption(){
    var grandlavel = $("#grandlavel").val();
    $.ajax({
        url:"<?php echo site_url("school/tearcherclass/grandOption"); ?>",
        type:"POST",
        dataType:"JSON",
        async:false,
        data: {
            'schlavelid' : $("#sch_level").val(),
            'grandlavelid' : grandlavel
        },
        success: function(data){
          if(data['arr_gran'].length > 0)
          {
            var opt_grand = "<option value='0'></option>";
            $.each(data['arr_gran'],function(k,v){
                opt_grand+="<option value='"+v['grade_levelid']+"'>"+v['grade_level']+"</option>";
            })
            $("#grandlavel").html(opt_grand);
          }
        }
    });
}



</script>
     