<style type="text/css"> 
 
  a{
    cursor: pointer;
  }
 table tbody tr td img{
 	width: 20px;	
 }
  #navigation_left{
	  display: none;
  }
  #page-wrapper{
	  margin:0 0 0 0px !important; ;
  }
 </style>
<?php
	$m='';
	$p='';
	if(isset($_GET['m'])){
	    $m=$_GET['m'];
	}
	if(isset($_GET['p'])){
	    $p=$_GET['p'];
}
?>
<div class="row">          
 <div class="col-sm-12">    
 	<div class="panel panel-default">
 		  	
	  <div class="panel-body">	          		 
		<div class="table-responsive">	
			
			<table border="0"​ align="center" id='listsubject' class="table">
			    <thead>
			        <th align=center width=40>No</th>
			        <th width=150>Teacher</th>
			        <th width=150>Class</th>
					<th width=150>Type of Evaluate</th>
			        <th width=100 style="text-align: center">
			        <!------- Button Preview ------>
					<span class="">
			    		<?php if($this->green->gAction("C")){ ?>
							<a href="<?php echo site_url("student/evaluate_moyes/add?m=$m&p=$p")?>" target="_blank">
								<img src="<?php echo base_url('assets/images/icons/add.png')?>" />
							</a>
						<?php } ?>
			    	</span>
					</th>
		    	 	<!------ End button ------>
			    </thead>
			    <tr>
			        <td></td>
			        <td><input type="text" name="txtsearchteacher" id="txtsearchteacher" onkeyup='search(event);' 
			        	value='<?php if(isset($_GET['s'])) echo $_GET['s']; ?>' class="form-control input-sm" />
			        </td>
			    </tr>
			    <tbody id='bodylist'>
				    <?php
				        $i=1;
				    foreach($teachclass as $row){
                        $schlevelid = $row->schlevelid;
                        $yearid= $row->yearid;
						$tchinf=$this->tclass->getTchProfile($row->teacher_id);

						$evalinfo=$this->green->getTable("SELECT DISTINCT eval.transno,eval.evaluate_type
																FROM sch_student_evaluated eval
																INNER JOIN sch_student_evaluated_mention mention ON mention.transno=eval.transno
															WHERE eval.yearid={$yearid}
															AND eval.schlevelid={$schlevelid}
															AND eval.classid={$row->classid}");
						if(count($evalinfo)>0){

							foreach($evalinfo as $evalrow){

								$transno=$evalrow['transno'];
								$i==1?$name=$tchinf['last_name']." ".$tchinf['first_name']:$name="";
								echo "<tr>
										  <td align=center>$i</td>
												<td>".$name."</td>
												<td>$row->class_name</td>
												<td>".($evalrow['evaluate_type']=='month'?"MoEYS":"TAE")."</td>
												";
											echo "<td align=center class='no_wrap'>";
											if($this->green->gAction("P")){
												if($evalrow['evaluate_type']=='Three Month'){
													$link_edit=site_url("student/evaluate/edit/$transno?m=$m&p=$p");
												}else{
													$link_edit=site_url("student/evaluate_moyes/edit/$transno?m=$m&p=$p");
												}

												echo "<a href='".$link_edit."' target='_blank'>
													<img src='".base_url('assets/images/icons/edit.png')."' />
												</a>";
											}

											"</td>
										</tr>
										";
								$i++;
							}
						}

				    }
				    ?>
				<!---- Start pagination ---->
			     <tr>
			       <td colspan='5' id='pgt'>
			        <ul class='pagination'>
			          <?php echo $this->pagination->create_links();?>
			        </ul>
			      </td>
			    </tr>
			    <!---- End Pagination ---->
			   	</tbody>
			</table>
			
		</div>
  	 </div>
  	</div>
  	
  </div>
</div>
<script type="text/javascript">

    $('#year,#schlevelid').change(function(){
    	search();
    })
    function viewsall(event){
    	var year=jQuery('#year').val();
    	//location.href="<?PHP echo site_url('school/tearcherclass/preview/');?>/0/"+year;
    	window.open("<?php echo site_url('school/tearcherclass/preview');?>/0/"+year,"_blank");
    }
    function search(event){
        var teacher=jQuery('#txtsearchteacher').val();
        var year=jQuery('#year').val();
		var l=jQuery('#schlevelid').val();
        var roleid=<?php echo $this->session->userdata('roleid');?>;


        $.ajax({
           url:"<?php echo site_url('school/tearcherclass/searchs'); ?>", 
           data: {
                  'teacher':teacher,
                  'year':year,
                  'roleid':roleid,
			      'l':l
                 },
           type: "POST",
           success: function(data){
              jQuery('#bodylist').html(data);                   
           }
         });
     } 
</script>