<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	a,.sort,.panel-heading span{cursor: pointer;}
	.panel-heading span{margin-left: 10px;}
	.cur_sort_up{
		background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	.cur_sort_down{
		background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
		background-position: left;
		background-repeat: no-repeat;
		padding-left: 15px !important;
	}
	.ui-autocomplete{z-index: 9999;}

	.panel-body{
	    /*height: 650px;*/
	    max-height: calc(100vh - 200px);
	    overflow-y: auto;
	}

	#list th {vertical-align: middle;}
	#list td {vertical-align: middle;}
</style>
<?php
$school=$this->db->where('schoolid',$this->session->userdata('schoolid'))->get('sch_school_infor')->row();
		           			$school_name=$school->name;
		           			$school_adr=$school->address;	
$m='';
$p='';
if(isset($_GET['m'])){
    $m=$_GET['m'];
}
if(isset($_GET['p'])){
    $p=$_GET['p'];
}

 ?>
<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
		      	<div class="col-xs-6">
		      		<strong id='title'>Mention Score List</strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
		      		<span class="top_action_button">
			      			<span class='error' style='color:red;'><?php if(isset($error)) echo "$error"; ?></span>
			      	</span>
			    	<!-- <span class="top_action_button">	
			    		<?php if($this->green->gAction("E")){ ?>
				    		<a href="#" id="exports" title="Export">
				    			<img id='export' src="<?php echo base_url('assets/images/icons/export.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>
		      		<span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ ?>
							<a href="#" id="print" title="Print">
				    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
				    		</a>
				    	<?php } ?>
		      		</span>		 -->
			    	<span class="top_action_button">
			    		<?php if($this->green->gAction("C")){ ?>
				    		<a>
				    			<img onclick='addnew();' src="<?php echo base_url('assets/images/icons/add.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>	         		
		      	</div> 			      
		  </div>
	      <!-- <div class="row"> -->
	      	<div class="col-sm-12">
	      		<div class="panel panel-default">
	      			<div class="panel-body">
		           		<div class="table-responsive" id="tab_print">
		           			<?php 
		           			$thr="";
		           			$tr="";		
		           			foreach($thead as $th=>$val){
		           				if($th=='No')
		           					$thr.="<th class='$val' align='center' rel='$val'>".$th."</th>";
		           				else if($th=='Action')
		           					$thr.="<th class='remove_tag' align='center'>".$th."</th>";
		           				else
		           					$thr.="<th class='$val' align='center' class='sort' rel='$val'>".$th."</th>";								
		           			}
		           			
		           			if(count($tdata)>0){
		           				$i=1;
		           				if(isset($_GET['per_page']))
									$i=$_GET['per_page']+1;
								
								foreach($tdata as $row){
									
									$tr.="<tr>
											<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
											<td>".$row->mention."</td>											
											<td>".$row->mention_kh."</td>
											<td>".$row->sch_level."</td>
											<td class='remove_tag'>";
											if($this->green->gAction("D")){
												 $tr.="<a>
												 		<img rel=".$row->menid." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
												 	</a>";
											}
											if($this->green->gAction("U")){
												 $tr.="<a>
												 		<img  rel=".$row->menid." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
												 	</a>";
											}
										$tr.="</td>
									</tr>
									 ";
									 $i++;
								}
							}?>
		           			<table class="table">
		           				<thead><?php echo $thr ?></thead>
		           				<thead class='remove_tag'>		           					
									<td></td>
							 		<td></td>		           					
		           					<td></td>
							 		<td>
							 			<select class='form-control input-sm' name='s_schlevelid' onchange="search(event);" id='s_schlevelid'>
					                   		<option value=''>-----Select School Level-----</option>
											<?php if (isset($schevels) && count($schevels) > 0) {
												foreach ($schevels as $sclev) {
													echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
												}
											} ?>
					                    </select>
							 		</td>
							 		<td></td>
		           				</thead>
		           				<tbody class='listbody'>
		           					<?php echo $tr ?>
		           					<tr class='remove_tag'>
										<td colspan='12' id='pgt'>
											<div style='margin-top:20px; width:11%; float:left;'>
											Display : <select id="sort_num"  onchange='search(event);' style='padding:5px; margin-right:0px;'>
															<?php
															$num=10;
															for($i=0;$i<10;$i++){?>
																<option value="<?php echo $num ;?>" <?php if(isset($_GET['s_num'])){ if($num==$_GET['s_num']) echo 'selected'; }?> ><?php echo $num;?></option>
																<?php $num+=10;
															}
															?>
														</select>
											</div>
											<div style='text-align:center; verticle-align:center; width:89%; float:right;'>
												<ul class='pagination' style='text-align:center'>
													
													<?php echo $this->pagination->create_links(); ?>
												</ul>
											</div>

										</td>
									</tr> 
		           				</tbody>
		           			</table>
						</div>  
					</div>
	      		</div>
	      	</div>	      	
	      </div> 
	    </div>
   <!-- </div> -->
</div>

<!-- ++++++++++++++++++++++++++++++++++++++++++++add form+++++++++++++++++++++++++++++++++++++++ -->
<div class="modal bs-example-modal-lg" id="adddisease" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="wrapper">
				<div class="clearfix" id="main_content_outer">
				    <div id="main_content">
					    <div class="result_info">
					    	<div class="col-sm-6">
					      		<strong>Add Score Mention</strong>
					      	</div>
					      	<div class="col-sm-6" style="text-align: center">
					      		<strong>
					      			<center class='exist' style='color:red;'></center>
					      		</strong>	
					      	</div>
					    </div>
					      	<form enctype="multipart/form-data" action="<?php echo base_url()."index.php/school/score_mention/save?m=$m&p=$p"; ?>" id="frmdisease" method="POST">
						        <div class="row">
									<div class="col-sm-12">
						            	<div class="panel-body" style="padding-top: 5px;">
							                <div class="row">
							                	<div class="col-xs-5">
							                		<input type="hidden" name="men_id" id="men_id">

													<label class="req" for="hod_name">School Level<span style="color:red">*</span></label>
								                	<select class='form-control input-sm' onchange="search(event);" name='schlevel' id='schlevel' min=1 required data-parsley-required-message="Select any school year.">
						                   			<option value=''>-----Select School Level-----</option>
													<?php if (isset($schevels) && count($schevels) > 0) {
														foreach ($schevels as $sclev) {
															echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
														}
													} ?>
						                    		</select>
							                	</div>
							                </div><br>

							                <div class="row">
												<div class="col-sm-12">
												    <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
												</div>           
											</div>

											<div class="row">
												<div class="col-sm-12">
													<div class="table-responsive">
										                <table id="list" class="table table-hover table-condensed" border="0" cellspacing="0" cellpadding="0" align="left" style="width: 100%;">
										                	<thead>
										                		<tr>
										                			<th style="width: 5%;">No</th>
										                			<th>Mention English</th>
										                			<th>Mention Khmer</th>
										                			<th style="width: 5%;"><a href="javascript:;" name="a_new" id="a_new">Add</a></th>
										                		</tr>

										                	</thead>
										                	<tbody>
										                		<tr>
										                			<td name="xno[]" class="xno" data-menid="0">1</td>
										                			<td><input type="text" name="mentionen[]" class="form-control mentionen input-sm"></td>
										                			<td><input type="text" name="mentionkh[]" class="form-control mentionkh input-sm"></td>
																	<td style="text-align: center;"><a href="javascript:;" name="remove" class="remove"><img src="<?= base_url('assets/images/icons/delete.png') ?>" style="width: 16px;"></a></td>
										                		</tr>
										                	</tbody>
										                	<tfoot>
										                		
										                	</tfoot>
										                </table>
								                	</div>
								                </div>
							                </div>

							            </div><!-- panel -->
								    </div> 
								</div>
					      </form>
					</div> 
			    </div>
			</div> 
            <div class="modal-footer">
            	<div class="col-sm-12">
	            	<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
	                <button type="button" id="save" class="btn btn-primary btn-sm save" data-save="1">Save</button>
	                <button type="button" id="save_close" class="btn btn-primary btn-sm save" data-save="2">Save close</button>	
            	</div>
            </div>
        </div>                       <!-- /.modal-content -->
    </div>
</div>

<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<script type="text/javascript">
	$(function(){
		// new =========
		$('body').delegate('#a_new', 'click', function(){
			new_row();
			$('.xno').each(function(i){
	        	$(this).html(i + 1);
	        });
		});

		// remove =========
		$('body').delegate('.remove', 'click', function(){
			if($('#list > tbody > tr').length > 1){
			    $(this).parent().parent().remove();
			 }else{
			    toastr["warning"]("Only one can't delete!");
			 }
			          
			 $('.xno').each(function(i){
			    $(this).html(i + 1);
			 });

		});


		// save ======
		$('.save').click(function(event){
			if($('#frmdisease').parsley().validate()){
				var save_close = $(this).data('save');

				var arr = [];
				$('.xno').each(function(i){
					var menid = $(this).attr('data-menid');
					var mentionen = $(this).closest('tr').find('.mentionen').val();					
					var mentionkh = $(this).closest('tr').find('.mentionkh').val();			
					arr[i] = {menid: menid, mentionen: mentionen, mentionkh: mentionkh}
				});

				$.ajax({
					url: "<?= site_url('school/score_mention/save') ?>",
					dataType: "JSON",
					type: "POST",
					// async: false,
					beforeSend: function(){

					},    
					data: {	
						schlevel: $('#schlevel').val(),
						arr: arr
					},
					success: function(data){
						if(data == 1){
							if(save_close - 0 == 2){								
								$('#adddisease').modal('hide')
							}

							clear();
							search(event);
							toastr["success"]('Success!');

							if($('.xno').attr('data-menid') - 0 > 0){
								$('#a_new').hide();
								$('.remove').hide();					
							}else{
								$('#a_new').show();
								$('.remove').show();
							}

						}else if(data == 2){
							toastr["warning"]('Duplicate data!');
							
						}else{
							toastr["warning"]("Can't save, Please enter value in list!");
						}

					},
					error: function(err){

					}
				});
			}
		});

	}); // ready f. =====

	function clear(){
		$('#frmdisease').parsley().destroy();
		$('#schlevel').val('');
		var tr = '<tr>\
		    			<td name="xno[]" class="xno" data-menid="0">1</td>\
		    			<td><input type="text" name="mentionen[]" class="form-control input-sm mentionen"></td>\
		    			<td><input type="text" name="mentionkh[]" class="form-control input-sm mentionkh"></td>\
						<td style="text-align: center;"><a href="javascript:;" name="remove" class="remove"><img src="<?= base_url('assets/images/icons/delete.png') ?>" style="width: 16px;"></a></td>\
		    		</tr>';
		$('#list tbody').html(tr);
	}

	function new_row(){
		var tr = '';
		tr += '<tr>\
	    			<td name="xno[]" class="xno" data-menid="0"></td>\
	    			<td><input type="text" name="mentionen[]" class="form-control input-sm mentionen"></td>\
	    			<td><input type="text" name="mentionkh[]" class="form-control input-sm mentionkh"></td>\
					<td style="text-align: center;"><a href="javascript:;" name="remove" class="remove"><img src="<?= base_url('assets/images/icons/delete.png') ?>" style="width: 16px;"></a></td>\
	    		</tr>';
		$('#list tbody').append(tr);
	}

	function edit(event){	
		$('#adddisease').modal({backdrop: 'static'})
		$('#frmdisease')[0].reset();
		$('.exist').html('');
		var men_id=$(event.target).attr('rel');
		
		$.ajax({
            url:"<?php echo base_url(); ?>index.php/school/score_mention/edit",    
            data: {'menid':men_id},
            type:"post",
            dataType:"json",
            async:false,
            success: function(data){            	
            	clear();
            	$('.xno').attr('data-menid', data.menid);			
            	if($('.xno').attr('data-menid') - 0 > 0){
					$('#a_new').hide();
					$('.remove').hide();					
				}else{
					$('#a_new').show();
					$('.remove').show();
				}

            	$('.mentionen').val(data.mention);
            	$('.mentionkh').val(data.mention_kh);
            	$('#schlevel').val(data.schlevelid);
            	$("#men_id").val(data.menid);

            }
        });
	}

	function deletes(event){
		var r = confirm("Are you sure to delete this iteme !");
		if (r == true) {
		    var menid=$(event.target).attr('rel');
			location.href="<?PHP echo site_url('school/score_mention/delete');?>/"+menid+"?<?php echo "m=$m&p=$p" ?>";
		} 
	}

	function addnew(){
		$('#frmdisease')[0].reset();
		$('.exist').html('');
		$('#adddisease').modal({backdrop: 'static'})
		clear();
		if($('.xno').attr('data-menid') - 0 > 0){
			$('#a_new').hide();
			$('.remove').hide();					
		}else{
			$('#a_new').show();
			$('.remove').show();
		}
	}
	
	$(function(){	
		$('#frmdisease').parsley();		
	})
	
	function search(event){
		var roleid=<?php echo $this->session->userdata('roleid');?>;
		var m=<?php echo $m;?>;
		var p=<?php echo $p;?>;
		var s_schlevelid=$("#s_schlevelid").val();
		var s_num=$('#sort_num').val();
		$.ajax({
				url:"<?php echo base_url(); ?>index.php/school/score_mention/search",    
				data: {'schlevelid':s_schlevelid,
						'roleid':roleid,
						'm':m,
						'p':p,
						's_num':s_num
					},
				type: "POST",
				success: function(data){
                   $('.listbody').html(data);
			}
		});
	}

	function isNumberKey(evt){
        var e = window.event || evt; // for trans-browser compatibility 
        var charCode = e.which || e.keyCode; 
        if ((charCode > 45 && charCode < 58) || charCode == 8){ 
            return true; 
        } 
        return false;  
    }	
</script>
