<style type="text/css">
    table tbody tr td img{width: 20px; margin-right: 10px}
    a,.sort,.panel-heading span{cursor: pointer;}
    .panel-heading span{margin-left: 10px;}
    .cur_sort_up{
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
        background-position: left;
        background-repeat: no-repeat;
        padding-left: 15px !important;
    }
    .cur_sort_down{
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
        background-position: left;
        background-repeat: no-repeat;
        padding-left: 15px !important;
    }
    .ui-autocomplete{z-index: 9999;}

    .panel-body{
        /*height: 650px;*/
        max-height: calc(100vh - 200px);
        overflow-y: auto;
    }

    #list th {vertical-align: middle;}
    #list td {vertical-align: middle;}
</style>
<?php  
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
}

 ?>
<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
        <div id="main_content">
          <div class="row result_info">
                <div class="col-xs-6">
                    <strong id='title'>Mention Score List (GEP)</strong>
                </div>
                <div class="col-xs-6" style="text-align: right">
                    <span class="top_action_button">
                            <span class='error' style='color:red;'><?php if(isset($error)) echo "$error"; ?></span>
                    </span>
                    <span class="top_action_button">
                        <?php if($this->green->gAction("C")){ ?>
                            <a>
                                <img onclick='addnew();' src="<?php echo base_url('assets/images/icons/add.png')?>" />
                            </a>
                        <?php } ?>
                    </span>                 
                </div>                
          </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-body">
             <table class="table table-hover">
                <thead>
                    <tr>
                        <?php
                            foreach ($thead as $th => $val) {
                                if ($th == 'Action'){
                                    echo "<th class='remove_tag'>" . $th . "</th>";
                                }
                                else{
                                    echo "<th class='sort $val no_wrap' onclick='sort(event);' rel='$val' sortype='ASC'>" . $th . "</th>";
                                }
                            }
                        ?>
                    </tr>
                    <tr class='remove_tag'>
                        <th></th>                                        
                        <th style="width:150px">
                            <input type='text' onkeyup="getdata(1);" class='form-control input-sm' id='s_mention'/>
                        </th> 
                        <th style="width:150px">
                            <input type='text' onkeyup="getdata(1);" class='form-control input-sm' id='s_mentionkh'/>
                        </th>                                        
                        <th style="width:190px;">
                            <select class='form-control input-sm' name='s_schlevelid' onchange="getdata(1);" id='s_schlevelid'>
                                <option value=''>School Level</option>
                                <?php
                                if (isset($schevels) && count($schevels) > 0) {
                                    foreach ($schevels as $sclev) {
                                        echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                                    }
                                }?>
                            </select>
                        </th>   
                        <th></th>                                      
                        <th style="width:120px;"></th>   
                        <th></th>                                        
                        <th></th> 
                        <th></th>                                         
                    </tr>
                </thead>
                <tbody class='list'>

                </tbody>
            </table>  
            <div class="fg-toolbar">
                <div class='col-sm-3'>
                    <label>Show
                        <select id='perpage' onchange='getdata(1);' name="DataTables_Table_0_length" size="1" aria-controls="DataTables_Table_0" tabindex="-1" class="form-control select2-offscreen" style="height: 33px; width: 100px;">
                            <?PHP
                            for ($i = 10; $i <= 100; $i += 10) {
                                    echo '<option value="'.$i.'" '.($i=="10"?'selected':'').'>'.$i.'</option>';
                                }
                            ?>
                        </select>
                    </label>
                </div>
                <div class='dataTables_paginate' style="float:right;">

                </div>
            </div>          
        </div>
    </div>    
</div>

<div class="modal bs-example-modal-lg" id="adddisease" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="wrapper">
                <div class="clearfix" id="main_content_outer">
                    <div id="main_content">
                        <div class="result_info">
                            <div class="col-sm-6">
                                <strong>Add Score Mention(GEP)</strong>
                            </div>
                            <div class="col-sm-6" style="text-align: center">
                                <strong>
                                    <center class='exist' style='color:red;'></center>
                                </strong>   
                            </div>
                        </div>
                            <form enctype="multipart/form-data" action="<?php echo base_url()."index.php/school/c_score_final_mention/save?m=$m&p=$p"; ?>" id="frmdisease" method="POST">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="panel-body" style="padding-top: 5px;">
                                            <div class="row">
                                                <div class="col-xs-5">
                                                    <input type="hidden" name="men_id" id="men_id">

                                                    <label class="req" for="hod_name">School Level<span style="color:red">*</span></label>
                                                    <select class='form-control input-sm' onchange="getdata(1);" name='schlevel' id='schlevel' min=1 required data-parsley-required-message="Select any school year.">
                                                    <option value=''>-----Select School Level-----</option>
                                                    <?php
                                                    if (isset($schevels) && count($schevels) > 0) {
                                                        foreach ($schevels as $sclev) {
                                                            echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                    </select>
                                                </div>  
                                            </div><br>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
                                                </div>           
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="table-responsive">
                                                        <table id="list" class="table table-hover table-condensed" border="0" cellspacing="0" cellpadding="0" align="left" style="width: 100%;">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 5%;">No</th>
                                                                    <th>Mention English</th>
                                                                    <th>Mention Khmer</th>
                                                                    <th>Grade</th>                                                                    
                                                                    <th>Meaning</th>
                                                                    <th>Max Score</th>
                                                                    <th>Min Score</th>
                                                                    <th>Is_Past</th>
                                                                    <th style="width: 5%;"><a href="javascript:;" name="a_new" id="a_new">Add</a></th>
                                                                </tr>

                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td name="xno[]" class="xno" data-menid="0">1</td>
                                                                    <td><input type="text" name="mentionen[]" class="form-control mentionen input-sm" placeholder=" " data-parsley-required="true" data-parsley-error-message="Input Mention!"></td>
                                                                    <td><input type="text" name="mentionkh[]" class="form-control mentionkh input-sm"></td>
                                                                    <td><input type="text" name="grade[]" class="form-control grade input-sm"></td>
                                                                    <td><input type="text" name="meaning[]" class="form-control meaning input-sm"></td>
                                                                    <td><input type="text" name="max[]" class="form-control max input-sm max"></td>
                                                                    <td><input type="text" name="min[]" class="form-control min input-sm min"></td>                                                                    
                                                                    <td><input type="checkbox" name="is_past[]" class="form-control is_past input-sm" id="is_past" value="0"></td>
                                                                    <td style="text-align: center;"><a href="javascript:;" name="remove" class="remove"><img src="<?= base_url('assets/images/icons/delete.png') ?>" style="width: 16px;"></a></td>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot>
                                                                
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div><!-- panel -->
                                    </div> 
                                </div>
                          </form>
                    </div> 
                </div>
            </div> 
            <div class="modal-footer">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <!-- <button type="button" id="save" class="btn btn-primary btn-sm save" data-save="1">Save</button> -->
                    <!-- <button type="button" id="save_close" class="btn btn-primary btn-sm save" data-save="2" value="Save Close"></button>  -->
                    <input type="button" class="btn btn-primary save" value="Save" id="save" data-save="1">
                    <input type="button" class="btn btn-primary save" value="Save Close" id="save_close" data-save="2">
                </div>
            </div>
        </div>                       <!-- /.modal-content -->
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        getdata(1); 

    });

    $('body').delegate('#a_new', 'click', function(){
        new_row();
        $('.xno').each(function(i){
            $(this).html(i + 1);
        });
    });

    $('body').delegate('.max, .min','keydown',function(e){ 
        if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57) || e.keyCode == 8 || e.keyCode == 190 || e.keyCode == 110) {                
            $(this).removeAttr("readonly");
        } else {
            $(this).attr("readonly", "readonly");
        }
    });

    $('body').delegate('.remove', 'click', function(){
        if($('#list > tbody > tr').length > 1){
            $(this).parent().parent().remove();
         }else{
            toastr["warning"]("Only one can't delete!");
         }
                  
         $('.xno').each(function(i){
            $(this).html(i + 1);
         });
    });

    $('.save').click(function(event){
         
        if($('#frmdisease').parsley().validate()){
            var save_close = $(this).data('save');
            var arr = [];
            $('.xno').each(function(i){                
                var menid = $(this).attr('data-menid');
                var mentionen = $(this).closest('tr').find('.mentionen').val();                 
                var mentionkh = $(this).closest('tr').find('.mentionkh').val();
                var grade = $(this).closest('tr').find('.grade').val();                
                var min_score = $(this).closest('tr').find('.min').val();
                var max_score = $(this).closest('tr').find('.max').val();
                var meaning = $(this).closest('tr').find('.meaning').val();
                var is_past = $(this).closest('tr').find('.is_past').val();
                arr[i] = {
                            menid: menid, 
                            mentionen: mentionen, 
                            mentionkh: mentionkh,
                            grade:grade,
                            min_score:min_score,
                            max_score:max_score,
                            meaning:meaning,                            
                            is_past:is_past
                        }
            });

            $.ajax({
                url: "<?= site_url('school/c_score_final_mention/save_gep') ?>",
                dataType: "JSON",
                type: "POST",
                // async: false,
                beforeSend: function(){

                },    
                data: { 
                    schlevel: $('#schlevel').val(),
                    arr: arr
                },
                success: function(data){
                    if(data == 1){
                        if(save_close - 0 == 2){                                
                            $('#adddisease').modal('hide')
                        }
                        clear();
                        getdata(1);
                        toastr["success"]('Success!');

                        if($('.xno').attr('data-menid') - 0 > 0){
                            $('#a_new').hide();
                            $('.remove').hide();                    
                        }else{
                            $('#a_new').show();
                            $('.remove').show();
                        }

                    }else if(data == 2){
                        toastr["warning"]('Duplicate data!');
                        
                    }else{
                        toastr["warning"]("Can't save, Please enter value in list!");
                    }
                },
                error: function(err){

                }
            });
        }
    });

    $('body').delegate('.is_past','change',function(){
        if($(this).is(':checked') == true){ 
            $(this).val(1);
        }else{ 
            $(this).val(0);
        }
    });

    function sort(event) {
        this.sortby = $(event.target).attr("rel");
        if ($(event.target).attr("sortype") == "ASC") {
            $(event.target).attr("sortype", "DESC");
            this.sorttype = "ASC";
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass('cur_sort_up');
        } else if ($(event.target).attr("sortype") == "DESC") {
            $(event.target).attr("sortype", "ASC");
            this.sorttype = "DESC";
            $('.sort').removeClass('cur_sort_up');
            $(event.target).addClass('cur_sort_down');
        }

        getdata(this.fn, this.sortby, this.sorttype);
    }

    function deletes(event){
        var con = confirm("Are you sure to delete this iteme !");
        if (con == true) {
                        $.ajax({
                                url: "<?php echo site_url('school/c_score_final_mention/delete_gep')?>",
                                type: "POST",
                                dataType:'JSON',
                                async:false,
                                data: {
                                    'id': $(event.target).attr("rel")
                                },
                                success: function (data) {
                                    if (data == 1) {
                                        toastr["success"]('data was deleted!'); 
                                        getdata(1); 
                                    } else if (data.success == 'errorses') {
                                        toastr["error"]('Can not delete this score!'); 
                                    }
                                }
                        });
        }
    }

    function update(event){
        $('#adddisease').modal({backdrop: 'static'});
        $('#frmdisease')[0].reset();
        $('.exist').html('');
        var men_id=$(event.target).attr('rel');
        
        $.ajax({
            url:"<?php echo base_url(); ?>index.php/school/c_score_final_mention/edit",
            data: {'menid':men_id},
            type:"post",
            dataType:"json",
            async:false,
            success: function(data){                
                clear();
                $('.xno').attr('data-menid', data.menid);           
                if($('.xno').attr('data-menid') - 0 > 0){
                    $('#a_new').hide();
                    $('.remove').hide();                    
                }else{
                    $('#a_new').show();
                    $('.remove').show();
                }
                $('#save').val('Update');
                $('#save_close').val('Update Close');
                $('.mentionen').val(data.mention);
                $('.mentionkh').val(data.mention_kh);
                $('.grade').val(data.grade);
                $('.max').val(data.max_score);
                $('.min').val(data.min_score);
                $('.meaning').val(data.meaning);
                $('#schlevel').val(data.schlevelid);
                $("#men_id").val(data.menid);
                //$('#is_past').val(data.is_past);
                if(data.is_past == 1){
                    $('.is_past').prop('checked',true);
                    $('.is_past').val(1);
                }else{ 
                    $('.is_past').prop('checked',false);
                    $('.is_past').val(0);
                }
            }
        });
    }

    $(document).on('click', '.pagenav', function () {
        var page = $(this).attr("id");
        getdata(page);
    });

    function addnew(){
        $('#frmdisease')[0].reset();
        $('.exist').html('');
        $('#adddisease').modal({backdrop: 'static'})
        clear();
        if($('.xno').attr('data-menid') - 0 > 0){
            $('#a_new').hide();
            $('.remove').hide();                    
        }else{
            $('#a_new').show();
            $('.remove').show();
        }
    }
function clear(){
        $('#frmdisease').parsley().destroy();
        $('#save').val('Save');
        $('#save_close').val('Save Close');
        $("#men_id").val('');
        $('#schlevel').val('');
        var tr = '<tr>\
                        <td name="xno[]" class="xno" data-menid="0"></td>\
                        <td><input type="text" name="mentionen[]" class="form-control mentionen input-sm" placeholder=" " data-parsley-required="true" data-parsley-error-message="Input Mention!"></td>\
                        <td><input type="text" name="mentionkh[]" class="form-control mentionkh input-sm"></td>\
                        <td><input type="text" name="grade[]" class="form-control grade input-sm"></td>\
                        <td><input type="text" name="meaning[]" class="form-control meaning input-sm"></td>\
                        <td><input type="text" name="max[]" class="form-control max input-sm max"></td>\
                        <td><input type="text" name="min[]" class="form-control min input-sm min"></td>\
                        <td><input type="checkbox" name="is_past[]" class="form-control is_past input-sm" value="0"></td>\
                        <td style="text-align: center;"><a href="javascript:;" name="remove" class="remove"><img src="<?= base_url('assets/images/icons/delete.png') ?>" style="width: 16px;"></a></td>\
                    </tr>';
        $('#list tbody').html(tr);
    }
function new_row(){
        var tr = '';
        tr += '<tr>\
                    <td name="xno[]" class="xno" data-menid="0"></td>\
                    <td><input type="text" name="mentionen[]" class="form-control mentionen input-sm" placeholder=" " data-parsley-required="true" data-parsley-error-message="Input Mention!"></td>\
                    <td><input type="text" name="mentionkh[]" class="form-control mentionkh input-sm"></td>\
                    <td><input type="text" name="grade[]" class="form-control grade input-sm"></td>\
                    <td><input type="text" name="meaning[]" class="form-control meaning input-sm"></td>\
                    <td><input type="text" name="max[]" class="form-control max input-sm max"></td>\
                    <td><input type="text" name="min[]" class="form-control min input-sm min"></td>\
                    <td><input type="checkbox" name="is_past[]" class="form-control is_past input-sm" value="0"></td>\
                    <td style="text-align: center;"><a href="javascript:;" name="remove" class="remove"><img src="<?= base_url('assets/images/icons/delete.png') ?>" style="width: 16px;"></a></td>\
                </tr>';
        $('#list tbody').append(tr);
}

function getdata(page, sortby, sorttype) {
    var url = "<?php echo site_url('school/c_score_final_mention/getdata_mention')?>";
    var m = "<?PHP echo $m?>";
    var p = "<?PHP echo $p?>";
    var s_schlevelid = $('#s_schlevelid').val();
    var s_mention = $('#s_mention').val();
    var s_mentionkh = $('#s_mentionkh').val();
    var perpage = $('#perpage').val();

    $.ajax({
        url: url,
        type: "POST",
        datatype: "JSON",
        async: false,
        data: {
            'm': m,
            'p': p,
            'page': page,
            's_sortby': sortby,
            's_sorttype': sorttype,
            's_schlevelid': s_schlevelid,
            's_mention':s_mention,
            's_mentionkh':s_mentionkh,
            'perpage': perpage
        },
        success: function (data) {
            if(data.tr_data ==""){
                var table = '<table class="table table-bordered table-striped table-hover">'+
                                '<tr>'+
                                    '<td colspan="11" style="color:red;text-align:center;font-size:14px;"><i>Result Not Data</i></td>'+
                                '</tr>'+
                                '<tr><td colspan="11"></td></tr>'+
                            '</table>';
                $('.list').html(table);
            }else{
                $(".list").html(data.tr_data);
            }                    
            $('.dataTables_paginate').html(data.pagina.pagination);
        }
    });
}
</script>