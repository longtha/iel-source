<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_iep_entryed_order_report extends CI_Controller
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model("reports/mod_iep_entryed_order_report","iep");
	}
	function index(){
		$data['opt_program'] = $this->get_program(); 
		$data['opt_examtyp']=$this->get_examtype();		
		$this->load->view('header');
		$this->load->view('reports/iep_reports/v__iep_entryed_order_report',$data);
		$this->load->view('footer');
	}
	function get_program(){
		$optionprogram="";
		$query = $this->db->get('sch_school_program')->result();
		foreach($query as $pro_id ){
			$optionprogram .='<option value="'.$pro_id->programid.'">'.$pro_id->program.'</option>';
		}
		return $optionprogram;		
	}

	function school_level(){
		$school_level="";
		$option_level="";
		$program = $this->input->post('program');
		foreach($this->iep->getsch_level($program) as $id ){
			$option_level .='<option value="'.$id->schlevelid.'">'.$id->sch_level.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['schlevel'] = $option_level;
		echo json_encode($arr_success);
	}

	function get_years(){
		$schoolid = $this->session->userdata('schoolid');	
		$sch_program = $this->input->post("program");
		$schlevelid = $this->input->post("sch_level");
		$option_year ='';
		$option_year .='<option value=""></option>';
		foreach ($this->iep->getschoolyear($sch_program,$schoolid,$schlevelid) as $row){

			$option_year .='<option value="'.$row->yearid.'">'.$row->sch_year.'</option>';
		}		
		header("Content-type:text/x-json");
		$arr_success['schyear'] = $option_year;
		echo json_encode($arr_success);
		
	}

	function get_gradlevel(){
		$grad_level="";
		$schoolid = $this->session->userdata('schoolid');	
		$programid = $this->input->post("program");
		$schlevelid = $this->input->post("sch_level");
		$grad_level .='<option value=""></option>';
		foreach($this->iep->getgradelevel($schoolid,$programid,$schlevelid) as $id ){
			$grad_level .='<option value="'.$id->grade_levelid.'">'.$id->grade_level.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['gradlevel'] = $grad_level;
		echo json_encode($arr_success);
	}

	function get_class(){
		$schoolid = $this->session->userdata('schoolid');	
		$grad_level = $this->input->post("grad_level");
		$schlevelid = $this->input->post("sch_level");
		$class="";
		$class .='<option value=""></option>';
		foreach($this->iep->get_class($schoolid,$grad_level,$schlevelid) as $id ){			
			$class .='<option value="'.$id->classid.'">'.$id->class_name.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['getclass'] = $class;
		echo json_encode($arr_success);  
	}

	function get_examtype(){
		$examtype="";
		$query=$this->db->get('sch_student_examtype')->result();
		foreach($query as $id ){

			$examtype .='<option value="'.$id->examtypeid.'">'.$id->exam_test.'</option>';
		}
		return $examtype;
	}
	
	function show_report(){
		$perpage=$this->input->post('perpage');
		$program=$this->input->post('program');
        $classid=$this->input->post('classid');
        $from_date=$this->green->convertSQLDate($this->input->post('from_date'));
        $to_date=$this->green->convertSQLDate($this->input->post('to_date'));
        $sch_level=$this->input->post('sch_level');
        $exam_type=$this->input->post('exam_type');
        $years=$this->input->post('years');
        $grad_level=$this->input->post('grad_level');        
        $where='';        
        if($exam_type != ""){
        	$where.=" AND exam_typeid='".$exam_type."'";
        }
		if($from_date != ''){ 
				$where .= 'AND create_date >= "'.$from_date.'"';
			}
		if($to_date != ''){ 
			$where .= 'AND create_date <= "'.$to_date.'"';
		}	
		if($sch_level !=""){
			$where.= " AND schlevelid = '".$sch_level."'";
		}

		if($years !=""){
			$where.= " AND yearid = '".$years."'";
		}

		if($grad_level !=""){
			$where.= " AND gradelevelid = '".$grad_level."'";
		}

		if($classid !=""){
			$where.= " AND classid = '".$classid."'";
		}

           $sql = "SELECT *,
           					CONCAT(v_score_iep_enties.last_name, ' ', v_score_iep_enties.first_name) AS fullname,
           					CONCAT(v_score_iep_enties.last_name_kh, ' ',v_score_iep_enties.first_name_kh) AS fullname_kh
           			FROM v_score_iep_enties 
     				WHERE  1=1 {$where}";


     	$pagina='';
		$getperpage=0;
		if($perpage==''){
			$getperpage=10;
		}else{
			$getperpage=$perpage;
		}
		$paging=$this->green->ajax_pagination(count($this->db->query($sql)->result()),site_url("reports/c_iep_entryed_order_report/show_report"),$getperpage,"icon");
		
		$i=1;
		$getlimit=10;
		if($paging['limit']!=''){
			$getlimit=$paging['limit'];
		}
		$limit=" LIMIT {$paging['start']}, {$getlimit}";   


		$sql.=" {$limit}";

		$sqlquery= $this->db->query("SELECT * FROM v_score_iep_enties uni 		
					 				   WHERE  1=1 {$where}");
		$data=array();
		if($sqlquery->num_rows() > 0)
		{	
			if($exam_type != ""){
			foreach($this->db->query($sql)->result() as $row)						
			{
				$data['student_name'] 	=	$row->fullname;
				$data['student_namekh'] =	$row->fullname_kh;
				$data['gradlevel'] 		=	$row->grade_level;
				$data['year'] 			=	$row->sch_year;
				if($exam_type == 2){
					$this->load->view('reports/iep_reports/v_mid_term_report',$data);
				}elseif($exam_type == 3){
					$this->load->view('reports/iep_reports/v_final_term_report',$data);
				}elseif($exam_type == 4){	
					$this->load->view('reports/iep_reports/v__semester_acadimic_report',$data);
				}elseif($exam_type == 5){
					$this->load->view('reports/iep_reports/v_annual_academic_report',$data);
				}			
			}
			}
			else{
					echo '<tr><td colspan="2" style="font-weight: bold;text-align: center;background: #F2F2F2; color: red;">'."Please Select Exam Type!".'</tr>';
				}						
		}
		else
		{
			echo '<tr><td colspan="2" style="font-weight: bold;text-align: center;background: #F2F2F2; color: red;">'."We din't find data".'</tr>';
		}


			
	}	
}