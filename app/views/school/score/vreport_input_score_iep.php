<div class="wrapper">
 	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
    		<div class="result_info">
    			<div class="col-sm-6"><strong>&nbsp;Report score entry</strong></div>
    			<div class="col-sm-6" style="text-align: right;">
    				<span class="top_action_button">
                    <a id="export" href="javascript:void(0)" title="Export"><img src="<?php echo base_url('assets/images/icons/export.png');?>"></a>
					<a id="print" href="javascript:void(0)" title="Print"><img src="<?php echo base_url('assets/images/icons/print.png');?>"></a>
					</span>
    			</div>
    			
    			
    		</div>
    		<div class="row">
    			<div class="col-sm-6">
    				<label for="school">School<span style="color:red">*</span></label>
    				<SELECT name="school" id="school" class="form-control"><?php echo $schoolid;?></SELECT>
    			</div>
    			<div class="col-sm-6">
    				<label for="schoollavel">School Lavel<span style="color:red">*</span></label>
    				<SELECT name="schoollavel" id="schoollavel" class="form-control"><?php echo $schoolid_lav;?></SELECT>
    			</div>
    			
    		</div>
    		<div class="row">
    			<div class="col-sm-6">
    				<label for="year">Year<span style="color:red">*</span></label>
    				<SELECT name="years" id="years" class="form-control"><?php echo $yearid; ?></SELECT>
    			</div>
    			<div class="col-sm-6">
    				<label for="gradelavel">Grade Lavel<span style="color:red">*</span></label>
    				<SELECT name="gradelavel" id="gradelavel" class="form-control"></SELECT>
    			</div>
    			
    		</div>
    		<div class="row">
    			<div class="col-sm-6">
    				<label for="classid">Class<span style="color:red">*</span></label>
    				<SELECT name="classid" id="classid" class="form-control"></SELECT>
    			</div>
                <div class="col-sm-6">
                    <label for="termname">Term<span style="color:red">*</span></label>
                    <SELECT name="termname" id="termname" class="form-control"></SELECT>
                </div>
    			
    		</div>
    		<div class="row">
    			<div class="col-sm-6">
                    <label for="examtype">Exam Type<span style="color:red">*</span></label>
                    <SELECT name="examtype" id="examtype" class="form-control"><?php echo $exam_type;?></SELECT>
                </div>
                <div class="col-sm-6">
                    <label for="date_add">Date Exam<span style="color:red">*</span></label>
                    <input type="text" name="date_add" id="date_add" class="form-control" value="<?php echo date('d-m-Y'); ?>">
                </div>
                <div class="col-sm-6">
                    <label for="weekname">Week<span style="color:red">*</span></label>
                    <SELECT name="weekname" id="weekname" multiple="multiple" class="form-control"></SELECT>
                </div>
    			<div class="col-sm-6">
    				<label for="subject_group">Subject Group<span style="color:red">*</span></label>
    				<SELECT name="subject_group" id="subject_group" class="form-control"></SELECT>
    				
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-sm-12">
    				<input type="button" name="search" value="SEARCH" id="search" class="btn btn-info" style="margin: 10px 5px;">
    			</div>
    		</div>
            <div class="row">
                <div class="col-sm-12 print">
                    <input type="hidden" name="hteachername" id="hteachername" class="form-control">
                    <input type="hidden" name="hdate_examp" id="hdate_examp" class="form-control">
                    <div class="table-responsive list_tbl">
                        <table class="table table-bordered">
                            <tbody id="body_list">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    	</div>
    	
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#date_add").datepicker();
        var obj_show  = new fn_show();
        
        $("body").delegate("#schoollavel","change",function(){
            var url = "<?php echo site_url('school/input_score_class_iep/select_y'); ?>";
            obj_show.fn_gradelavel(url,4);
            obj_show.fn_opt_subjectgroup();
        })

        $('body').delegate('#export', 'click', function(e){
            var teachername     = $("#hteachername").val();
            var date_examp      = $("#hdate_examp").val();
            var att_grandlavel  = $("#gradelavel").find("option:selected").attr("att_grandlavel");
            var examtype        = $("#examtype").find("option:selected").attr("att_label");
            var sub_xam_label   = $("#subexamtype").find("option:selected").attr("label_term");
            var classname       = $("#classid").find("option:selected").attr("att_class");
            var is_time         = $("#classid").find("option:selected").attr("is_pt");
            var label_time      = "";
            if(is_time == 0){
                label_time = "Full-time";
            }else{
                label_time = "Part-time";
            }
            var title= "<table><tr><td colspan='11'><p align='center' style='font-family:Khmer MEF2;font-size:14px;'>សាលាអន្តរជាតិ អាយ អ៊ី អ៊ែល</h4></p></td></tr>";
                title+="<tr><td colspan='11'><p align='center' style='font-family:Khmer MEF2;font-size:14px;'>IEL INTERNATIONAL SCHOOL</h4></p></td></tr></table>";
            var data = $('.table').attr('border', 1);
            var data = $(".list_tbl").html().replace(/<img[^>]*>/gi, "");
            var infor= "<table style='width:80%;'><tr><td colspan='11'><p style='text-align:center;'><b>"+examtype+" Result ("+sub_xam_label+")</b></p></td></tr>";
                infor+="<tr><td colspan='11'><p style='text-align:center;'><b>Date :&nbsp;&nbsp;&nbsp;&nbsp;"+date_examp+"</b></p></td></tr>";
                infor+="<tr><td colspan='3'>Level:&nbsp;"+att_grandlavel+"</td><td>Time:&nbsp;"+label_time+"</td></tr>";
                infor+="<tr><td colspan='3'>Teacher's Name:&nbsp;"+teachername+"</td><td>Room:&nbsp;"+classname+"</td></tr></table>";
            var data_export  =$("<div>"+title+""+infor+"<br>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
            var export_data = $("<center>"+data_export+"</center>").clone().find(".remove_tag").remove().end().html();         
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
            e.preventDefault();
            $('.table').attr('border', 0);
          });      

        $("body").delegate("#years","change",function(){
            var url = "<?php echo site_url('school/input_score_class/grade_lavel'); ?>";
            obj_show.fn_gradelavel(url,1);
            var url_t = "<?php echo site_url('school/input_score_class/sub_exam_type'); ?>";
            obj_show.fn_gradelavel(url_t,3);
            
        })
        $("body").delegate("#gradelavel","change",function(){
            var url = "<?php echo site_url('school/input_score_class_iep/grade_class'); ?>";
            obj_show.fn_gradelavel(url,2);
        })
        $("body").delegate("#examtype","change",function(){
            obj_show.fn_weekname();
            //obj_show.fn_opt_subjectgroup();
        })
        $("body").delegate("#search","click",function(){
            var url = "<?php echo site_url('school/input_score_class_iep/show_report_list'); ?>";
            obj_show.fn_show_list(url);
        })
         $("body").delegate("#termname","click",function(){
            //var url = "<?php echo site_url('school/input_score_class_iep/show_report_list'); ?>";
            //obj_show.fn_weekname();
        })
        // $("body").delegate("#subject","change",function(){
        //     var label_subject = $(this).find("option:selected").attr("att_label");
        //     $("#show_label").html(label_subject);
        // })
        $("body").delegate("#print","click",function(){
            var teachername     = $("#hteachername").val();
            var date_examp      = $("#hdate_examp").val();
            var att_grandlavel  = $("#gradelavel").find("option:selected").attr("att_grandlavel");
            var examtype        = $("#examtype").find("option:selected").attr("att_label");
            var termname        = $("#termname").find("option:selected").attr("label_term");
            var classname       = $("#classid").find("option:selected").attr("att_class");
            var is_time         = $("#classid").find("option:selected").attr("is_pt");
            var label_time      = "";
            if(is_time == 0){
                label_time = "Full-time";
            }else{
                label_time = "Part-time";
            }
            var title="<p align='center' style='font-family:Khmer MEF2;font-size:20px;'>សាលាអន្តរជាតិ អាយ អ៊ី អ៊ែល</h4><h5 align='center'>IEL INTERNATIONAL SCHOOL</p>";
            var data = $(".list_tbl").html().replace(/<img[^>]*>/gi, "");
            var infor= "<table style='width:80%;'><tr><td>Teacher's Name:&nbsp;"+teachername+"</td><td>Room:&nbsp;"+classname+"</td></tr>";
                infor+="<tr><td>Level:&nbsp;"+att_grandlavel+"</td><td>Time:&nbsp;"+label_time+"</td></tr></table>";
            var data_print  =$("<div><p style='text-align:center;'><b>"+examtype+" Result ("+sub_xam_label+")</b></p><p style='text-align:center;'><b>Date :&nbsp;&nbsp;&nbsp;&nbsp;"+date_examp+"</b></p>"+infor+"<br>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
            var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
            gsPrint1(title, export_data);
        })
    })
    function gsPrint1(emp_title,data,hide_print){
        var export_data = $("<center>"+data+"</center>").clone().find("."+hide_print).remove().end().html();
        var element = "<div>"+export_data+"</div>";
        
        $("<center><p style='padding-top:15px;text-align:center;'><b>"+emp_title+"</b></p><hr>"+element+"</center>").printArea({
            mode:"popup",  //printable window is either iframe or browser popup                     
            popHt: 600 ,  // popup window height
            popWd: 500,  // popup window width
            popX: 0 ,  // popup window screen X position
            popY: 0 , //popup window screen Y position
            popTitle:emp_title, // popup window title element
            popClose: false,  // popup window close after printing
            strict: false 
            });
    }
    function fn_show()
    {
        this.fn_parameter = function()
        {
            var schoollavel = $("#schoollavel").val();
            var schoolid    = $("#school").val();
            var gradelavel  = $("#gradelavel").val();
            var classid     = $("#classid").val();
            var yearsid     = $("#years").val();
            var examtype    = $("#examtype").val();
            var termname    = $("#termname").val();
            var subject_group = $("#subject_group").val();
            var date_add     = $("#date_add").val();
            // var weekid      = $("#weekname").val();
            var obj_para = {
                            "schoolid":schoolid,
                            "schoollavel":schoollavel,
                            "gradelavel":gradelavel,
                            "classid":classid,
                            "yearsid":yearsid,
                            "examtype":examtype,
                            "termname":termname,
                            "subject_group":subject_group,
                            "date_add":date_add
                            //"weekid":weekid
                            }
            return obj_para;
        },
        this.fn_gradelavel=function(get_url,num)
        {
            var para_obj = this.fn_parameter();
            $.ajax({
                type:"POST",
                url : get_url,
                dataType:"HTML",
                async:false,
                data:{
                    para_gr  : 1,
                    para_obj:para_obj
                },
                success:function(data){
                    if(num == 1){
                        $("#gradelavel").html(data);
                    }
                    else if(num == 2){
                        $("#classid").html(data);
                    }else if(num == 4){
                        $("#years").html(data);
                    }else{
                        $("#termname").html(data);
                    }
                }
            })
        },
        this.fn_opt_subjectgroup = function()
        {
            var para_obj = this.fn_parameter();
            $.ajax({
                type:"POST",
                url : "<?php echo site_url('school/input_score_class_iep/subject_group'); ?>",
                dataType:"HTML",
                async:false,
                data:{
                    para_sub_g  : 1,
                    para_obj    : para_obj,
                    display_r   :2
                },
                success:function(data){
                    $("#subject_group").html(data);
                }
            })
        },
        this.fn_weekname = function()
        {
            //var obj_para = this.fn_parameter();
            $.ajax({
                type:"POST",
                url : "<?php echo site_url('school/input_score_class_iep/option_week'); ?>",
                dataType:"HTML",
                async:false,
                data:{
                    para_sub_subj : 1,
                    termid    : $("#termname").val(),
                    examid  : $("#examtype").val()
                },
                success:function(data){
                    $("#weekname").html(data);
                }
            })
        },
        this.fn_show_list = function(get_url)
        {
            //var weekid      = $("#weekname option:selected").text();
            var arr_w = [];
            $('#weekname :selected').each(function(i, sel){ 
                arr_w[i]={"weekid":$(sel).val(),"weekname":$(sel).text()};

            });
            //console.log(arr_w);
            var para_obj = this.fn_parameter();
            //var course = $("#course option:selected").text();
            $.ajax({
                type:"POST",
                url : get_url,
                dataType:"Json",
                async:false,
                data:{
                    para_gr  : 1,
                    arr_w    : arr_w,
                    para_obj : para_obj
                },
                success:function(data)
                {
                    //console.log(data);
                    $("#body_list").html(data.tr);
                    // $.each(data.rank_n,function(k,v){
                    //     $(".rank_"+k).html(v);
                    // });
                }
            })
        }

    }
    function sortNumber(a,b) {
        return b-a;
    }
</script>