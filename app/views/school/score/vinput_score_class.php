<?php 
$user_active = isset($user_act)?$user_act:"";
// $user_teacher = $this->db->query("SELECT
									// sch_emp_profile.empid,
									// sch_emp_profile.empcode,
									// sch_emp_profile.first_name,
									// sch_emp_profile.last_name,
									// sch_user.userid,
									// sch_user.match_con_posid
									// FROM
									// sch_emp_profile
									// INNER JOIN sch_user ON sch_emp_profile.empid = sch_user.emp_id
									// WHERE 1=1 
									// AND sch_user.match_con_posid='tch'
									// AND sch_user.userid='".$user_active."'");
// $user_tch= "";
// $tch = "";
// if($user_teacher->num_rows() > 0){
	// foreach($user_teacher->result() as $row_t){
		// $user_tch = $row_t->empid;
		// $tch = $row_t->match_con_posid;
	// }
// }else{
	// $user_tch = $user_active;
// }

//$user_active = $this->green->getActiveUser();
//echo $user_active;
?>
<div class="wrapper">
 	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
    		<div class="result_info">
    			<div class="col-sm-6"><strong>&nbsp;Input score entry</strong></div>
    			<div class="col-sm-6" style="text-align: right;">
    				<span class="top_action_button">
					<a id="view_all" href="javascript:void(0)" title="view_all">Report score entry</a>&nbsp;
					<a id="view_mid_term" href="javascript:void(0)" title="Report mid term">Report mid-term</a>&nbsp;
					<a id="view_term" href="javascript:void(0)" title="Report mid term">Report term</a>
					</span>
    			</div>
    			
    		</div>
    		<div class="row">
    			<div class="col-sm-6">
    				<label for="school">School<span style="color:red">*</span></label>
    				<SELECT name="school" id="school" class="form-control"><?php echo $schoolid;?></SELECT>
    			</div>
    			<div class="col-sm-6">
    				<label for="schoollavel">School Lavel<span style="color:red">*</span></label>
    				<SELECT name="schoollavel" id="schoollavel" class="form-control"><?php echo $schoolid_lav;?></SELECT>
    			</div>
    			
    		</div>
    		<div class="row">
    			<div class="col-sm-6">
    				<label for="year">Year<span style="color:red">*</span></label>
    				<SELECT name="years" id="years" class="form-control"><?php echo $yearid; ?></SELECT>
    			</div>
    			<div class="col-sm-6">
    				<label for="gradelavel">Grade Lavel<span style="color:red">*</span></label>
    				<SELECT name="gradelavel" id="gradelavel" class="form-control"></SELECT>
    			</div>
    			
    		</div>
    		<div class="row">
    			<div class="col-sm-6">
    				<label for="classid">Class<span style="color:red">*</span></label>
    				<SELECT name="classid" id="classid" class="form-control"></SELECT>
    			</div>
    			<div class="col-sm-3">
    				<label for="subexamtype">Term<span style="color:red">*</span></label>
    				<SELECT name="subexamtype" id="subexamtype" class="form-control"></SELECT>
    			</div>
    			<div class="col-sm-3" style="padding: 0px;"><label for="date_label"></label><p style="padding-top: 5px;" class="help-block" id="show_date"></p></div>
    		</div>
    		<div class="row">
    			<div class="col-sm-6">
    				<label for="examtype">For Exam<span style="color:red">*</span></label>
    				<SELECT name="examtype" id="examtype" class="form-control"><?php echo $exam_type;?></SELECT>
    			</div>
    			
    			<div class="col-sm-6">
    				<label for="course">Course<span style="color:red">*</span></label>
    				<SELECT name="course" id="course" class="form-control">
    					<option value="0">Core</option>
    					<option value="1">Skill</option>
    				</SELECT>
    			</div>
    			<div class="col-sm-6">
    				<label for="subject_group">Subject Group<span style="color:red">*</span></label>
    				<SELECT name="subject_group" id="subject_group" class="form-control"></SELECT>
    				
    			</div>
    			<div class="col-sm-6">
    				<label for="date_add">Date Exam<span style="color:red">*</span></label>
    				<input type="text" name="date_add" id="date_add" class="form-control">
    			</div>
    			<div class="col-sm-6" id="show_subject" style="display:none;">
    				<label for="subject">Subject<span style="color:red">*</span></label>
    				<SELECT name="subject" id="subject" class="form-control"></SELECT>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-sm-12" style="padding-top: 10px;">
    				<input type="button" name="search" value="SEARCH" id="search" class="btn btn-info" style="margin-bottom:10px;">
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-sm-12">
	    			<div class="table-responsive">
						<table class="table table-bordered">
						   	<thead id="show_label">
						   		
						   	</thead>
						   	<tbody id="body_list">
						   		
						   	</tbody>
						</table>
					</div>
				</div>
    		</div>
    		<div class="row">
    			<div class="col-sm-12" style="padding-top: 5px;">
    				<input type="button" name="save" value="SAVE" id="save" class="btn btn-info">
    			</div>
    		</div>
    	</div>
    	
    </div>
</div>
<script>
	$(function(){
		$("#date_add").datepicker();
		var obj_show  = new fn_show();
		$("#save").hide();
		var url_search = "<?php echo site_url('school/input_score_class/search_studen'); ?>";
		$("body").delegate("#schoollavel","change",function(){
			var url = "<?php echo site_url('school/input_score_class/select_y'); ?>";
			obj_show.fn_gradelavel(url,4);
			obj_show.fn_opt_subjectgroup();
			var url1 = "<?php echo site_url('school/input_score_class/sub_exam_type'); ?>";
			obj_show.fn_gradelavel(url1,3);
			$("#show_date").html("");
		})

		$("body").delegate("#years","change",function(){
			var url = "<?php echo site_url('school/input_score_class/grade_lavel'); ?>";
		 	obj_show.fn_gradelavel(url,1);
		 	var url1 = "<?php echo site_url('school/input_score_class/sub_exam_type'); ?>";
			obj_show.fn_gradelavel(url1,3);
			$("#show_date").html("");
		 	
		})
		$("body").delegate("#subject_group,#course","change",function(){
			obj_show.fn_sub_subjectgroup();
			obj_show.fn_show_list(url_search);
		})
		$("body").delegate("#gradelavel","change",function(){
			var url = "<?php echo site_url('school/input_score_class/grade_class'); ?>";
			obj_show.fn_gradelavel(url,2);
			obj_show.fn_show_list(url_search);
		})
		$("body").delegate("#examtype","change",function(){
			
			obj_show.fn_opt_subjectgroup();
			obj_show.fn_show_list(url_search);
			// var val_this = $(this).find("option:selected").attr("is_assigment");
			// if(val_this==0){
			// 	$("#show_subject").hide();
			// }else{
			// 	$("#show_subject").show();
			// }
		});
		$("body").delegate("#classid","change",function(){
			obj_show.fn_show_list(url_search);
		});
		$('#date_add').datepicker().on('changeDate', function(e) {
			obj_show.fn_show_list(url_search);
		});
		
		$("body").delegate("#search","click",function(){
			obj_show.fn_show_list(url_search);
		})
		$("body").delegate("#score","click",function(){
			$(this).select();
		})
		$("body").delegate("#save","click",function(){
			obj_show.fn_save();
		})
		$("body").delegate("#score","keydown", function (e) {
           	if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57) || e.keyCode == 8 || e.keyCode == 190 || e.keyCode == 110) {
                $(this).removeAttr("readonly");
            } else {
                $(this).attr("readonly", "readonly");
            }
        });
        $("body").delegate("#view_all","click",function(){
        	window.open("<?php echo site_url('school/input_score_class/show_report') ?>",target='_blank');
		})
		$("body").delegate("#view_mid_term","click",function(){
        	window.open("<?php echo site_url('school/input_score_class/show_report_midterm') ?>",target='_blank');
		})
		$("body").delegate("#view_term","click",function(){
        	window.open("<?php echo site_url('school/input_score_class/show_report_term') ?>",target='_blank');
		})
		$("body").delegate("#subexamtype","change",function(){
        	var fdate = $(this).find("option:selected").attr("fdate");
        	var tdate = $(this).find("option:selected").attr("tdate");
        	$("#show_date").html("<b>From:&nbsp;"+fdate+" to "+tdate+"</b>");
		})
		$("body").delegate(".score","keyup",function(){
			var max_score = $(this).attr("max_score")-0;
			var this_val  = $(this).val()-0;
			if(this_val > max_score){
				alert("Sorry you can't input score bigger then max score !");
				$(this).val(0);
				$(this).select();
			}
		})

	})
	function fn_show(){
		this.fn_parameter = function(){
			var schoollavel = $("#schoollavel").val();
			var schoolid    = $("#school").val();
			var gradelavel  = $("#gradelavel").val();
			var classid     = $("#classid").val();
			var is_pt       = $("#classid").find("option:selected").attr("is_pt");
			var yearsid     = $("#years").val();
			var examtype    = $("#examtype").val();
			
			var subexamtype = $("#subexamtype").val();
			var course      = $("#course").val();
			var subject_group = $("#subject_group").val();
			var is_class_participation = $("#subject_group").find("option:selected").attr("is_class_participation");
			var subject     = $("#subject").val();
			var date_add     = $("#date_add").val();
			var is_assigment = $("#examtype").find("option:selected").attr("is_assigment");
			var obj_para = {
							"schoolid":schoolid,
							"schoollavel":schoollavel,
							"gradelavel":gradelavel,
							"classid":classid,
							"yearsid":yearsid,
							"examtype":examtype,
							"subexamtype":subexamtype,
							"course":course,
							"subject_group":subject_group,
							"subject":subject,
							"date_add":date_add,
							"is_partime":is_pt,
							"is_assigment":is_assigment,
							"is_class_participation":is_class_participation
							}
			return obj_para;
		},
		this.fn_gradelavel=function(get_url,num){
			var para_obj = this.fn_parameter();
			$.ajax({
				type:"POST",
				url : get_url,
				dataType:"HTML",
				async:false,
				data:{
					para_gr  : 1,
					para_obj:para_obj
				},
				success:function(data){
					if(num == 1){
						$("#gradelavel").html(data);
					}
					else if(num == 2){
						$("#classid").html(data);
					}else if(num == 4){
						$("#years").html(data);
					}else{
						$("#subexamtype").html(data);
					}
				}
			})
		},
		this.fn_opt_subjectgroup = function(){
			var para_obj = this.fn_parameter();
			$.ajax({
				type:"POST",
				url : "<?php echo site_url('school/input_score_class/subject_group'); ?>",
				dataType:"HTML",
				async:false,
				data:{
					para_sub_g  : 1,
					para_obj:para_obj,
					is_assigment  : $("#examtype").find("option:selected").attr("is_assigment")
					
				},
				success:function(data){
					$("#subject_group").html(data);
				}
			})
		},
		this.fn_sub_subjectgroup = function(){
			var obj_para = this.fn_parameter();
			$.ajax({
				type:"POST",
				url : "<?php echo site_url('school/input_score_class/sub_subject_group'); ?>",
				dataType:"HTML",
				async:false,
				data:{
					para_sub_subj : 1,
					obj_para      : obj_para,
					subject_group : $("#subject_group").val()
				},
				success:function(data){
					$("#subject").html(data);
				}
			})
		},
		this.fn_show_list = function(get_url){
			var para_obj = this.fn_parameter();
			$.ajax({
				type:"POST",
				url : get_url,
				dataType:"Json",
				async:false,
				data:{
					para_gr  : 1,
					para_obj : para_obj,
					user_active:"<?=$user_active; ?>"
				},
				success:function(data){
					var th='<tr><th>No</th><th>Name</th><th>Sex</th>'+data.arr_hsubj+'</tr>';
					$("#show_label").html(th);
					if(data.arr_tr == ""){
						$("#body_list").html("<tr><td colspan='3' style='text-align:center;'><b>Data not found.</b></td></tr>");
						$("#save").hide();
					}else{
						$("#body_list").html(data.arr_tr);
						$("#save").show();
					}
					
					
				}
				
			})
		},
		this.fn_save = function(){
			var fn_parameter = this.fn_parameter();
			var arr_student = [];
			var ii = 0;
			var pp = 0;
			var typeno_tr = 0;
			$(".studentid").each(function(i){
				var tr     = $(this).parent().parent();
				var score  = tr.find("#score").val();
				var typeno = tr.find("#htypno").val();
				if(typeno > 0){
					typeno_tr = typeno;
				}
				var studentid = $(this).val();
				var k=0;
				var arr_scor = [];
				tr.find(".score").each(function(){
					var value_score = $(this).val()-0;
					var subjectid   = $(this).attr("subj_id");
					if(value_score > 0){
						arr_scor[k]={'val_score':value_score,'subjectid':subjectid};
						k++;
						pp++;
					}
				});
				arr_student[ii] = {'studentid':$(this).val(),'score':arr_scor};
				ii++;
			})

			if(fn_parameter.schoolid == ""){
				alert("Please choose school first");
			}else if(fn_parameter.schoollavel == ""){
				alert("Please choose school lavel before proccess");
			}else if (fn_parameter.gradelavel == "") {
				alert("Please choose school grade lavel before proccess");
			}else if(fn_parameter.classid == ""){
				alert("Please choose class before proccess");
			}else if(fn_parameter.yearsid == ""){
				alert("Please choose year before proccess");
			}else if(fn_parameter.examtype == ""){
				alert("Please choose exam type before proccess");
			}else if(fn_parameter.subexamtype == ""){
				alert("Please choose sub exam type before proccess");
			}else if(fn_parameter.course == ""){
				alert("Please choose course before proccess");
			}else if(fn_parameter.subject_group == ""){
				alert("Please choose subject group before proccess");
			}else if(fn_parameter.date_add == ""){
				alert("Please choose input date before proccess ");
			}else{
				if(pp == 0){
					alert("Please input score first.");
				}else{
					var conf = confirm("Do you want to save ?");
					if(conf == true)
					{
						$.ajax({
							type:"POST",
							url : "<?php echo site_url('school/input_score_class/save_data'); ?>",
							dataType:"HTML",
							async:false,
							data:{
								para_gr  : 1,
								para_obj : fn_parameter,
								arr_student : arr_student,
								typeno_tr   : typeno_tr
							},
							success:function(data){
								if(data == 0){
									alert("Please choose date exam between term.");
								}else if(data == 2){
									alert("Sorry your finale has finished.");
								}else{
									//parent.window.location.reload(true);
								}
								
							}
							
						})
						// this.fn_show_list();
					}
				}
			}
		}
	}
</script>