<div class="wrapper">
	<div class="clearfix" id="main_content_outer">
	    <div id="main_content">
	      <div class="row result_info">
		      	<div class="col-xs-6">
		      		<strong id='title'><?php echo $page_header;?></strong>
		      	</div>
		      	<div class="col-xs-6" style="text-align: right">
		      		
			    	<span class="top_action_button">	
			    		<?php if($this->green->gAction("E")){ ?>
				    		<a href="javaScript:void(0)" id="exports" title="Export">
				    			<img id='export' src="<?php echo base_url('assets/images/icons/export.png')?>" />
				    		</a>
				    	<?php } ?>
			    	</span>
		      		<span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ ?>
							<a href="javaScript:void(0)" id="print" title="Print">
				    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
				    		</a>
				    	<?php } ?>
		      		</span>	
		      	</div> 			      
		  </div>
	      <!-- <div class="row"> -->
	      	<div class="col-sm-12">
	      		<div class="panel panel-default">
	      			<div class="panel-body">
	      				<!-- start class table responsive -->
		           		<div class="table-responsive">
							<div id="tab_print"></div>
							<div id="wait" align="center"><img src='<?php echo base_url('assets/images/icons/ajax-loader.gif')?>'/></div>
						</div> 
						  <!-- end class table responsive -->
					</div>
	      		</div>
	      	</div>	      	
	      <!-- </div> --> 
	    </div>
   </div>
</div>

<script type="text/javascript">
	$(function(){
		
		views();
		$('#exports').click(function(e){
    		e.preventDefault();
			window.open('data:application/vnd.ms-excel,' + encodeURIComponent(PrintingHtml()));
		});

		$('#year').on("change",function(){
			views();
		});
		$('#schlevelid').on("change",function(){
			views();
		});
		$('#print').click(function(){
			gsPrint(PrintingHtml());
		});
		
	});

	var PrintingHtml = function(){
		var htmlToPrint ='<style type="text/css">' +
					        '#setBord th, #setBord td {border:1px solid #000 !important;padding;0.5em;}' +
					    '</style>';
		   	data = $("#tab_print").html();
		   	var export_data = $("<center>"+data+"</center>").clone().find(".remove_tag").remove().end().html();
		   	export_data+=htmlToPrint;
		return export_data;
	}
	
	var views = function(){
		$("#wait").css("display", "block");
		var variables = {'yid':$('#year').val(),'slid':$('#schlevelid').val()};	
		$('#tab_print').hide();
		$.ajax({
			type:"POST",
			url:"<?php echo site_url('school/schedule/scheduleprocess')?>",
			async:false,
			dataType:"Json",
			data:{
				variables:variables
			},
			success:function(data){
				if(data.row!=""){
					$('#tab_print').html(data.row).show();
				}else{
					$('#tab_print').html('Data is empty display, Please try again..!').css('text-align','center').show();
				}
			}
		}).done(function( msg ) {
         	$("#wait").css("display", "none");
		}).fail(function() {
		  $('#tab_print').html('Failed to fetch data, Please contact administrator to solve this problem..!').css('text-align','center').show();
		});
	}
	var gsPrint = function(data){
		 var options = {
		 				mode:"popup",
		 				popHt: 600,
		 				popWd: 700,
		 				popX: 0,
		 				popY: 0,
		 				popTitle:"<?php echo $page_header;?>",
		 				popClose: false,
		 				strict: false 
		 			};
		 $("<div>"+data+"</div>").printArea(options);
	}	
</script>
