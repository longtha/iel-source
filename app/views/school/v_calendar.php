<div class="wrapper">
    <div class="clearfix" id="main_content_outer">

        <div id="main_content">

            <div class="result_info" style="padding-top: 0px">
                <div class="col-sm-6">
                    <strong>School Calendar</strong>
                </div>
                <div class="col-sm-6" style="text-align: right">

                </div>
            </div>
            <?php
            $m = '';
            $p = '';
            if (isset($_GET['m'])) {
                $m = $_GET['m'];
            }
            if (isset($_GET['p'])) {
                $p = $_GET['p'];
            }
            ?>
            <?php if($this->session->userdata('match_con_posid')!='stu'){?>
            <form method="post" accept-charset="utf-8" id="fragelev"
                  action="<?php echo site_url('school/c_calendar/reload?m=' . $m . '&p=' . $p . ''); ?>" class="gform">

                <div class="col-sm-4 col-md-4 ">
                    <div class="">
                        <label class="req" for="school">School</label>
                        <select class="form-control" id='school' name='school' min='1' required
                                data-parsley-required-message="Select any school">

                            <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                                <option
                                    value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4 col-md-4 ">

                    <div class="">
                        <label class="req" for="school-level">School Level</label>
                        <select class="form-control" id="school-level" name="school-level">
                            <option value=""></option>
                        </select>
                    </div>

                </div>
                <div class="col-sm-4 col-md-4 ">

                    <div class="">
                        <label class="req" for="academic-year">Academic Year</label>
                        <select class="form-control" id="academic-year" name="academic-year">
                            <option value=""></option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3 ">

                    <label class="req" for="from_date">From Date</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="from_date" name="from_date" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <label class="req" for="to_date">To Date</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="to_date" name="to_date" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>

                </div>
                <div class="col-sm-12">
                    <div class="dv-std" id="dv-std"></div>
                </div>
                <div class="col-sm-12" style="padding-bottom: 5px;padding-top: 5px">
                    <div align="center">
                        <?php if ($this->green->gAction("R")) { ?>
                            <input type="button" name="btn-search" id='btn-search' value="Search" class="btn btn-primary"/>
                        <?php } ?>
                    </div>
                </div>

            </form>
            <?php }?>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="panel panel-default">

        <div class="panel-body table-responsive">
            <table border="0" ? align="center" id='tbl-calendar' class="data-table table-striped table-bordered table-hover">
                <thead class="tab_head">
                <th>School</th>
                <th>School Level</th>
                <th>Academic Year</th>
                <th>From</th>
                <th>To</th>
                <th>Title</th>
                <th>Note</th>
                <th>File</th>
                <th>Create By</th>
                <th colspan="3">
                  <span id="span-addnew" data-toggle="modal" data-target="#calendar-modal">
                  <?php if ($this->green->gAction("C")) { ?>
                    <img src="<?php echo base_url('assets/images/icons/add.png') ?>"/>
                  <?php } ?>
                  </span>
                </th>
              </thead>

                <tbody>
                  <tr>
                    <td colspan="11">No data found..</td>
                  </tr>
                </tbody>
                <!---- Start pagination ---->
                <tfoot>
                <tr>
                    <td colspan='13' id='pgt'>
                        <ul class='pagination'>

                        </ul>
                    </td>
                </tr>
                </tfoot>
                <!---- End Pagination ---->
            </table>

        </div>
    </div>
</div>

</div>

<!-- Modal -->
<div id="calendar-modal" class="modal fade" role="dialog"  tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" style="width:80%">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create Calendar</h4>
      </div>
      <div class="modal-body">
        <form method="post" accept-charset="utf-8" id="dlg-frm-calendar" target="dlg-if-calendar" enctype="multipart/form-data"
              action="<?php echo site_url('school/c_calendar/save?m=' . $m . '&p=' . $p . ''); ?>" class="gform">

            <div class="col-sm-4 col-md-4 ">
                <div class="">
                    <label class="req" for="dlg-school">School</label>
                    <select class="form-control" id='dlg-school' name='school' min='1' required
                            data-parsley-required-message="Select any school">

                        <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                            <option
                                value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-4 col-md-4 ">

                <div class="">
                    <label class="req" for="dlg-school-level">School Level</label>
                    <select class="form-control" id="dlg-school-level" name="school-level">
                        <option value=""></option>
                    </select>
                </div>

            </div>
            <div class="col-sm-4 col-md-4 ">

                <div class="">
                    <label class="req" for="dlg-academic-year">Academic Year</label>
                    <select class="form-control" id="dlg-academic-year" name="academic-year">
                        <option value=""></option>
                    </select>
                </div>

            </div>
            
            <div class="col-sm-4 col-md-4">
                <label for="dlg-title">Title</label>
                <input type="text" id="dlg-title" name="title" class="form-control" value="" />
            </div>
            <div class="col-sm-4 col-md-4 hiable_sub">
                <label for="dlg-note">Note</label>
                <textarea id="dlg-note" name="note" class="form-control"></textarea>
            </div>
            <div class="col-sm-4 col-md-4">
                <label for="dlg-file">Attach File</label>
                <input type="file" id="dlg-file" name="file" value="" />
            </div>

            <div class="col-sm-12 col-md-12">
                <div class="dv-std" id="dv-std"></div>
            </div>

        </form>
        <iframe id="dlg-if-calendar" name="dlg-if-calendar" style="border: 0 none; width: 0px; height: 0px;"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" id="dlg-btn-save" class="btn btn-default" data-dismiss="modal">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">

  function error() {
    alert("save error!!!");
  }

  function reload(isFirstLoad) {
    //var parameters = isFirstLoad || isFirstLoad == undifined ? 0 : 1;
    var parameters = isFirstLoad === undefined || isFirstLoad === null ? 1 : 0;

      $.ajax({
          url: "<?php echo site_url('school/c_calendar/reload'); ?>",
          type: "POST",
          dataType: 'json',
          async: false,
          data: {
              school: $("#school").val(),
              school_level: $("#school-level").val(),
              academic_year: $("#academic-year").val(),
              from_date: $("#from-date").val(),
              to_date: $("#to-date").val(),
              parameter: parameters
          },
          success: function (calendars) {
            var tbody = "";
		
            if (calendars.length > 0) {
              for (var i = 0; i < calendars.length; i++) {
                tbody += '<tr>' +
                            '<td>' + calendars[i].school + '</td>' +
                            '<td>' + calendars[i].school_level + '</td>' +
                            '<td>' + calendars[i].academic_year + '</td>' +
                            '<td>' + calendars[i].from_date + '</td>' +
                            '<td>' + calendars[i].to_date + '</td>' +
                            '<td>' + calendars[i].title + '</td>' +
                            '<td>' + calendars[i].description + '</td>' +
                            '<td>' +
                              '<a download class="a-calendar-file" href="<?php echo base_url('assets/upload/school_calendars') ?>/' + calendars[i].attach_file + '">' +
                                '<img title="' + calendars[i].attach_file +'" src="' + callAttachedFileIcon(calendars[i].file_type) + '" />' +
                              '</a>' +
                            '</td>' +
                            '<td>' + calendars[i].created_by + '</td>' +
                            '<td align="center">' +
                              '<span id="' + calendars[i].id + '" class="span-delete">' +
                              <?php if ($this->green->gAction("C")) { ?>
                                '<img src="<?php echo base_url('assets/images/icons/delete.png') ?>"/>' +
                              <?php } ?>
                              '</span>' +
                            '</td>' +
                         '</tr>';
              }
            }
            else {
              tbody += '<tr><td colspan="11" align="center">No data found..</td></tr>';
            }

            $("#tbl-calendar tbody").html(tbody);
          }
      });
  }

  function callSchoolLevel(school, schoolLevelContainer) {

    $.ajax({
        url: "<?php echo site_url('school/c_calendar/getSchoolLevel'); ?>",
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
            school: school
        },
        success: function (schoolLevel) {
            var options = "<option value=''></option>";
            if (schoolLevel.length > 0) {
                for (var i = 0; i < schoolLevel.length; i++) {
                    options += '<option value="' + schoolLevel[i].schlevelid + '" data-program="' + schoolLevel[i].programid + '">' + schoolLevel[i].sch_level + '</option>';
                }
            }

            $(schoolLevelContainer).html(options);
        }
    });
  }

  function callAcademicYear(schoolLevel, academicYearContainer) {

    $.ajax({
        url: "<?php echo site_url('school/academic_year/get_year'); ?>",
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
            schlevelid: schoolLevel
        },
        success: function (academic) {
            var options = "<option value=''></option>";
            if (academic.year.length > 0) {
                for (var i = 0; i < academic.year.length; i++) {
                    options += '<option value="' + academic.year[i].yearid + '">' + academic.year[i].sch_year + '</option>';
                }
            }

            $(academicYearContainer).html(options);
        }
    });
  }

  function callAttachedFileIcon(fileType) {
    var icon = fileType == 'jpeg' ? 'jpg' : fileType.toString().toLowerCase();
    return "<?php echo base_url('assets/images/icons/'); ?>/" + icon + ".ico";
  }

  $(function () {

    reload(1);

    callSchoolLevel($("#school").val(), $("#school-level"));

    $("body").delegate("#school-level", "change", function() {
      callAcademicYear($(this).val(), $("#academic-year"));
    });

    $("#btn-search").click(function() {
    	filter_search();
    });

    $("body").delegate("#dlg-school-level", "change", function() {
      callAcademicYear($(this).val(), $("#dlg-academic-year"));
    });

    $("body").delegate("#span-addnew", "click", function() {
    	clear();
      	callSchoolLevel($("#school").val(), $("#dlg-school-level"));
      	
    });

    $("#from_date,#to_date").datepicker({
        language: 'en',
        pick12HourFormat: true,
        format: 'dd-mm-yyyy'
    });

    $("body").delegate("#dlg-btn-save", "click", function() {
      $("#dlg-frm-calendar").submit();
      reload(1);
    });

    $("body").delegate(".span-delete", "click", function() {

      $.ajax({
          url: "<?php echo site_url('school/c_calendar/delete'); ?>",
          type: "POST",
          dataType: 'json',
          async: false,
          data: {
            id: $(this).attr("id")
          },
          success: function (result) {
            if (result.delete == "Success") {
              reload(1);
            }
            else {
              alert(result.delete);
            }
          }
      });
    });
  });
  function filter_search() {
	  
	    var search_params = {};
	    
		if($("#school").val()!=null && $("#school").val()!='undefined' && $("#school").val()!=""){
			search_params["school_id"]=$("#school").val();
		}
		
		if($("#school-level").val()!=null && $("#school-level").val()!='undefined' && $("#school-level").val()!=''){
			search_params["school_level_id"]=$("#school-level").val();
		}

		if($("#academic-year").val()!=null && $("#academic-year").val()!='undefined' && $("#academic-year").val()!=''){
			search_params["academic_year_id"]=$("#academic-year").val();
		}

		if($("#grade-level").val()!=null && $("#grade-level").val()!='undefined' && $("#grade-level").val()!=''){
			search_params["grade_level_id"]=$("#grade-level").find("option:selected").val();
		}

		 if($("#from_date").val()!=null 
				&& $("#from_date").val()!='undefined' 
			&& $("#to_date").val()!=null 
			&& $("#to_date").val()!='undefined' 
			&& $("#from_date").val()!="" 
				&& $("#to_date").val()!=""){
			
			search_params["from_date"]=$("#from_date").val();
			search_params["to_date"]=$("#to_date").val();
		}

	 if(($("#from_date").val()=="" && $("#to_date").val()!="")){
			
		 alert("Please select From Date");
		 return;
	 }else if($("#to_date").val()=="" && $("#from_date").val()!=""){
    	 
		 alert("Please select To Date");
		 return;
     }


	 $.ajax({
   	  url: "<?php echo site_url('school/c_calendar/filter'); ?>",
         type: "POST",
         dataType: 'json',
         async: false,
         data: 'data='+JSON.stringify(search_params),
         success: function (calendars) {



       	  var tbody = "";

             if (calendars.length > 0) {
               for (var i = 0; i < calendars.length; i++) {

   	                	 var publish_date="";
   			        	  try {
   				        	  
   			        		    var date_string=calendars[i].transaction_date;
   								var date_strings=date_string.split(' ');
   								publish_date=date_strings[0];
   			        	  }catch(err) {
   			        		  
   			        		publish_date=calendars[i].transaction_date;
   			        	  }
   			                
   		                  tbody += '<tr>' +
   		                              '<td>' + calendars[i].school + '</td>' +
   		                              '<td>' + calendars[i].school_level + '</td>' +
   		                              '<td>' + calendars[i].academic_year + '</td>' +
   		                              '<td>' + calendars[i].from_date + '</td>' +
   		                              '<td>' + calendars[i].to_date + '</td>' +
   		                              '<td><div class="truncate">' + calendars[i].title + '</div></td>' +
   		                              '<td><div class="truncate">' + calendars[i].description + '</div></td>' +
   		                              '<td>' +
   		                                '<a download class="a-calendar-file" href="<?php echo base_url('assets/upload/school_calendars') ?>/' + calendars[i].attach_file + '">' +
   		                                  '<img title="' + calendars[i].attach_file +'" src="' + callAttachedFileIcon(calendars[i].file_type) + '" />' +
   		                                '</a>' +
   		                              '</td>' +
   		                              '<td>' + calendars[i].created_by + '</td>' +
   		                              '<td align="center">' +
   		                                '<span id="' + calendars[i].id + '" class="span-delete">' +
   		                                <?php if ($this->green->gAction("C")) { ?>
   		                                  '<img src="<?php echo base_url('assets/images/icons/delete.png') ?>"/>' +
   		                                <?php } ?>
   		                                '</span>' +
   		                              '</td>' +
   		                           '</tr>';
               }

               
             }else {
               tbody += '<tr><td colspan="11" align="center">No data found..</td></tr>';
             }
        
             $("#tbl-calendar tbody").html(tbody);
         }
     });

    }
  function clear(){
	  var options = "<option value=''></option>";
	  $("#dlg-school-level").html(options);
 	  $("#dlg-academic-year").html(options);
 	  $("#dlg-grade-level").html(options);
 	  $("#dlg-title").val("");
 	  $("#dlg-note").val("")
 	  $('#dlg-file').val("");
  }
</script>
