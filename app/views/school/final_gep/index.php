
<div class="wrapper" style="width: 100% !important;">
 	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
    		<div class="result_info">
    			<div class="col-sm-6"><strong>&nbsp;Final GEP</strong></div>
    			<div class="col-sm-6" style="text-align: right;">
    				<!-- <span class="top_action_button">
						<a id="view_all" href="javascript:void(0)" title="view_all">Report score entry</a>&nbsp;
						<a id="view_mid_term" href="javascript:void(0)" title="Report mid term">Report mid-term</a>&nbsp;
						<a id="view_term" href="javascript:void(0)" title="Report mid term">Report term</a>
					</span> -->
    			</div>
    		</div>
    		<?php if($this->session->userdata('match_con_posid')!='stu'){?>
			<div class="col-sm-12">
    			<div class="col-sm-6">
    				<label for="school">School<span style="color:red">*</span></label>
    				<SELECT name="school" id="school" class="form-control"><?php echo $schoolid;?></SELECT>
    			</div>
    			<div class="col-sm-6">
    				<label for="schoollavel">School Level<span style="color:red">*</span></label>
    				<SELECT name="schoollavel" id="schoollavel" class="form-control"><?php echo $schoolid_lav;?></SELECT>
    			</div>    			
    		</div>
    		<div class="col-sm-12">
    			<div class="col-sm-6">
    				<label for="year">Year<span style="color:red">*</span></label>
    				<SELECT name="years" id="years" class="form-control"><?php echo $yearid; ?></SELECT>
    			</div>
    			<div class="col-sm-6">
    				<label for="gradelavel">Grade Level<span style="color:red">*</span></label>
    				<SELECT name="gradelavel" id="gradelavel" class="form-control"></SELECT>
    			</div> 
    		</div>
    		<div class="col-sm-12">
				<div class="col-sm-6">
					<label for="classid">Class<span style="color:red">*</span></label>
					<SELECT name="classid" id="classid" class="form-control"></SELECT>
				</div> 
				<div class="col-sm-6">
    				<label for="termid">Term<span style="color:red"></span></label>
    				<SELECT name="termid" id="termid" class="form-control"></SELECT>
    			</div>
			</div>  	
    		<div class="col-sm-12">
    			<div class="col-sm-6">
    				<label for="student_id">Student's Id<span style="color:red"></span></label>
    				<input type="text" name="student_num" id="student_num" class="form-control" placeholder="Student ID">
    			</div>
    			<div class="col-sm-6">
    				<label for="full_name">Full name<span style="color:red"></span></label>
    				<input type="text" name="full_name" id="full_name" class="form-control" placeholder="Full name">
    			</div>   			
    		</div>  		
    		
    		<div class="col-sm-12">
    			<div class="col-sm-5">&nbsp;</div>
    			<div class="col-sm-2" style="padding-top: 10px;">
    				<button type="button" name="search" id="search" class="btn btn-success btn-sm" style="margin-bottom:10px;">Search</button>
    			</div>
    		</div>
    		<?php } ?>
    		<div class="col-sm-12">
    			<div class="table-responsive">
					<table cellspacing="0" cellpadding="0" class="table table-hover">
					   	<thead id="show_label">
					   		<tr>
					   			<th>No</th>
					   			<th>Student Id.</th>
					   			<th>Name</th>
					   			<th>Gender</th>
					   			<th>Class</th>
					   			<th>Term</th>
					   			<th>Total Core</th>
					   			<th>Total Skill</th>
					   			<th>Total Score</th>
					   			<th id="show_print">Print All</th>						   			
					   		</tr>
					   	</thead>
					   	<tbody id="body_list">
					   		
					   	</tbody>
					</table>
				</div>
    		</div>
    		<div class="col-sm-12" style="padding-left: 0;">
				<div class="col-sm-12">
					<SELECT class="form-control" id="perpage" style="width:70px; float: left;">
						<option>10</option>
						<option>20</option>
						<option>30</option>
						<option>40</option>
						<option>50</option>
						<option>60</option>
						<option>70</option>
						<option>80</option>
						<option>90</option>
						<option>100</option>
					</SELECT>
					<div id="pagenation" style="float: right;"></div>
				</div>
		        <!-- <div id="sp_page" class="btn-group pagination" role="group" aria-label="..." style="display: inline;"></div> -->
		    </div>
    	</div>
    	
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      	<div class="modal-header bg-info">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="exampleModalLabel">Add Command Student</h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-sm-12">
					<table>
						<tr>
							<td colspan="4" style="text-align: center;"><p style="font-size: 14px;"><b>STUDENT'S GEP FULL-TIME FINAL REPORT</b></p></td>
						</tr>
			     		<tr>
			     			<td width="20%">Student`s name:</td>
			     			<td><span id="studentname"></span>
			     				<input type="hidden" name="hstudent_id" id="hstudent_id">
			     			</td>
			     			<td>Class</td>
			     			<td><span id="class_name"></span></td>
			     		</tr>
			     		<tr>
			     			<td width="20%">student ID</td>
			     			<td><span id="studentid"></span></td>
			     			<td>Year</td>
			     			<td><span id="yearid"></span></td>
			     		</tr>
			     	</table>	
				</div>
		     	
		    </div>
		    <div class="row" style="margin-top: 20px;">
		     	<div class="col-sm-12">
		     		<label>Command teacher</label>
		     		<textarea rows="5" class="form-control" id="txt_command"></textarea>
		     	</div>
		     	<div class="col-sm-6" style="margin-top: 10px;">
		     		<p>Date&nbsp;:<input type="text" name="date_academic" id="date_academic" value="" style="border:0;border-bottom: 1px dotted black;text-align: center;"></p>
		     		<p style="margin-left: 40px;">Academic Manager</p>
		     		<select id="academic" class="form-control" style="margin: 10px 0 0 10px;width:200px;">
		     		<?php 
	                  	$sql_user_acad = $this->db->query("SELECT
	                                                      sch_user.userid,
	                                                      sch_user.user_name
	                                                      FROM
	                                                      sch_user
	                                                      WHERE match_con_posid='acca'");
						$opt_acad = "<option value=''></option>";
						if($sql_user_acad->num_rows() > 0){
						foreach($sql_user_acad->result() as $row_acad){
						  $opt_acad.= "<option value='".$row_acad->userid."'>".$row_acad->user_name."</option>";
						}
						}   
						echo $opt_acad;  
	                ?>
		     		</select>
		     	</div>
		     	<div class="col-sm-6" style="text-align: center;margin-top: 10px;text-align: center;">
		     		<p style="margin-left: 50px;">Date&nbsp;:<input type="text" name="date_teacher" id="date_teacher" value="" style="border:0;border-bottom: 1px dotted black;text-align: center;"></p>
		     		<p style="margin-left: 40px;">Teacher's Signature</p>
		     		<?php
		   //   		$userid = $this->session->userdata('userid');
					// $sql_user_acad = $this->db->query("SELECT
					//                                     sch_user.user_name
					//                                     FROM
					//                                     sch_user
					//                                     WHERE sch_user.userid='".$userid."'")->row();
					// $username = isset($sql_user_acad->user_name)?$sql_user_acad->user_name:"";
					?>
					<p style="margin-left: 40px;" id="show_teacher"></p>
					<input type="hidden" name="teacherid" id="teacherid" value="">
		     	</div>
		  	</div>
		</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save_commend">Save</button>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
	.xmodal {
	display: none;
	position: fixed;
	z-index: 2000;
	top: 5%;
	left: 50%;
	height: 5%;
	width: 5%;
	/* background: rgba( 255, 255, 255, .8 ) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat; */
	background: rgba(255, 255, 255, 0) url(<?= base_url('assets/images/FhHRx.gif') ?>)  50% 50% no-repeat;
	cursor: progress !important;
	}

	/* When the body has the loading class, we turn
	      the scrollbar off with overflow:hidden */
	body.loading {
	  overflow: hidden;
	}

	/* Anytime the body has the loading class, our
	      modal element will be visible */
	body.loading .xmodal {
	  display: inline;
	  cursor: progress !important;
	}
</style>

<script>
	$(function(){

		$("#from_date").datepicker();
		$("#to_date").datepicker();
		$("#date_academic,#date_teacher").datepicker();
		var obj_show  = new fn_show();
		$("body").delegate("#add_comment","click",function(){
			$("#studentname").html("");
			$("#class_name").html("");
			$("#studentid").html("");
			$("#hstudent_id").val("");
			$("#yearid").html("");
			$("#txt_command").val("");
			var studentid_h = $(this).attr("studentid");
			var tr = $(this).parent().parent();
			var student_num = tr.find("#student_num").html();
			var fullname = tr.find("#fullname").html();
			var classname = tr.find("#classname").html();
			var showyear  = tr.find("#showyear").val();
			show_command(studentid_h);
			$("#studentname").html(":&nbsp;<b>"+fullname+"<b>");
			$("#class_name").html(":&nbsp;<b>"+classname+"<b>");
			$("#studentid").html(":&nbsp;<b>"+student_num+"<b>");
			$("#hstudent_id").val(studentid_h);
			$("#yearid").html(":&nbsp;<b>"+showyear+"<b>");
			$(".modal").modal("show");
		});
		$("body").delegate("#schoollavel","change",function(){
			var url = "<?php echo site_url('school/c_final_gep/select_y'); ?>";
			obj_show.fn_gradelavel(url,4);
			show_term();
		});

		$("body").delegate("#years","change",function(){
			var url = "<?php echo site_url('school/c_final_gep/grade_lavel'); ?>";
		 	obj_show.fn_gradelavel(url,1);
		 	show_term();
		 	
		})
		$("body").delegate("#gradelavel","change",function(){
			var url = "<?php echo site_url('school/c_final_gep/grade_class'); ?>";
			obj_show.fn_gradelavel(url,2);
			show_term();
		})

		$("body").delegate("#search","click",function(){
			var url = "<?php echo site_url('school/c_final_gep/search_studen'); ?>";
			obj_show.fn_show_list(url, 1);
		})
		<?php if($this->session->userdata('match_con_posid')=='stu'){?>
			obj_show.fn_show_list("<?php echo site_url('school/c_final_gep/search_studen'); ?>", 1);
		<?php }?>
		// page ==========
		// $('body').delegate('.a-pagination', 'click', function() {
		// 	var url = "<?php echo site_url('school/c_final_gep/search_studen'); ?>";
		// 	var current_page = $(this).data('current_page') - 0;
		// 	obj_show.fn_show_list(url, current_page, 10 - 0);
		// });
		$(document).on('click', '.pagenav', function(){
			var url = "<?php echo site_url('school/c_final_gep/search_studen'); ?>";
			//var current_page = $(this).data('current_page') - 0;
			var page = $(this).attr("id");   
			obj_show.fn_show_list(url, page);
	          
	    });
	    $("body").delegate("#save_commend","click",function(){
			save_commend();
			$(".modal").modal("hide");
	    });
	});// ready ======

	function save_commend(){
		$.ajax({
			type:"POST",
			url : "<?php echo site_url('school/c_final_gep/save_commend_student'); ?>",
			dataType:"JSON",
			async:false,
			data:{
				"command" : $("#txt_command").val(),
				"schoolid": $("#school").val(),
				"schoollavel": $("#schoollavel").val(),
				"years": $("#years").val(),	
				"gradelavel": $("#gradelavel").val(),
				"classid": $("#classid").val(),	
				"termid": $("#termid").val(),
				"hstudent_id": $("#hstudent_id").val(),
				"is_partime": $("#classid").find("option:selected").attr("is_pt"),
				"date_acade":$("#date_academic").val(),
				"academic":$("#academic").val(),
				"date_teacher":$("#date_teacher").val(),
				"userid":$("#teacherid").val()
			},
			success:function(data){
			}
		})
	}
	function show_command(studentidh){
		$.ajax({
			type:"POST",
			url : "<?php echo site_url('school/c_final_gep/show_command'); ?>",
			dataType:"JSON",
			async:false,
			data:{
				"schoolid": $("#school").val(),
				"schoollavel": $("#schoollavel").val(),
				"years": $("#years").val(),	
				"gradelavel": $("#gradelavel").val(),
				"classid": $("#classid").val(),	
				"termid": $("#termid").val(),
				"hstudent_id": studentidh,
				"is_partime": $("#classid").find("option:selected").attr("is_pt")
			},
			success:function(data){
				$("#txt_command").val(data.command[0]);
				$("#hstudent_id").val(data.command[1]);
				$("#academic").val(data.command[2]);
				$("#date_academic").val(data.command[3]);
				$("#date_teacher").val(data.command[4]);
				$("#teacherid").val(data.command[5]);
				$("#show_teacher").html(data.command[6]);
			}
		})
	}
	function show_term(){
		$.ajax({
				type:"POST",
				url : "<?php echo site_url('school/c_final_gep/show_term'); ?>",
				dataType:"HTML",
				async:false,
				data:{
					"get_term" : 1,
					"schoolid": $("#school").val(),
					"schoollavel": $("#schoollavel").val(),
					"years": $("#years").val(),			
				},
				success:function(data){
					$("#termid").html(data);
				}
		})
	}
	function fn_show(){
		this.fn_parameter = function(){
			var obj_para = {
								"schoolid": $("#school").val(),
								"schoollavel": $("#schoollavel").val(),
								"gradelavel": $("#gradelavel").val(),
								"classid": $("#classid").val(),
								"years": $("#years").val(),							
								"termid": $("#termid").val(),
								"is_partime": $("#classid").find("option:selected").attr("is_pt"),
								"student_num": $("#student_num").val(),
								"full_name": $("#full_name").val()							
							}
			return obj_para;		
		}
		,
		
		this.fn_gradelavel=function(get_url,num){
			var para_obj = this.fn_parameter();
			$.ajax({
				type:"POST",
				url : get_url,
				dataType:"HTML",
				async:false,
				data:{
					para_gr  : 1,
					para_obj:para_obj
				},
				success:function(data){
					if(num == 1){
						$("#gradelavel").html(data);
					}
					else if(num == 2){
						$("#classid").html(data);
					}else if(num == 4){
						$("#years").html(data);
					}else{
						$("#subexamtype").html(data);
					}
				}
			})
		}
		,

		this.fn_sub_subjectgroup = function(){
			var obj_para = this.fn_parameter();
			$.ajax({
				type:"POST",
				url : "<?php echo site_url('school/c_final_gep/sub_subject_group'); ?>",
				dataType:"HTML",
				async:false,
				data:{
					para_sub_subj : 1,
					obj_para      : obj_para,
					subject_group : $("#subject_group").val()
				},
				success:function(data){
					$("#subject").html(data);
				}
			})
		}
		,

		this.fn_show_list = function(get_url, page = 0){
			<?php if($this->session->userdata('match_con_posid')=='stu'){?>
				var perpage=1000;
			<?php }else{?>
				var perpage = $("#perpage").val();
			<?php }?>
    		
			var para_obj = this.fn_parameter();
			$.ajax({
				type:"POST",
				url : get_url,
				dataType:"Json",
				// async:false,
				beforeSend: function(){
					$('.xmodal').show();
				},
				complete: function(){
					$('.xmodal').hide();
				},
				data:{
					para_gr  : 1,
					para_obj : para_obj,
					perpage  : perpage,
					page : page
				},
				success:function(data){
					
					var schoolid = para_obj.schoolid;
					var schoollavel = para_obj.schoollavel;
					var gradelavel = para_obj.gradelavel;
					var classid = para_obj.classid;
					var years = para_obj.years;
					var is_partime = $("#classid").find("option:selected").attr("is_pt");
					var termid     = $("#termid").val();
					var perpage    = $("#perpage").val();
					var numrow     = data.pagenation.start;
					if(is_partime == 0){ // full time
						$("#show_print").html("<a href='<?php echo site_url('reports/c_full_time_final_report/print_all_fulltime');?>?perpage="+perpage+"&numrow="+numrow+"&schoolid="+schoolid+"&schoollavel="+schoollavel+"&gradelavel="+gradelavel+"&classid="+classid+"&years="+years+"&is_partime="+is_partime+"&termid="+termid+"' target='_blank'>Print All</a>");
					}else{ // partime
						$("#show_print").html("<a href='<?php echo site_url('reports/c_part_time_final_report/print_all_parttime');?>?perpage="+perpage+"&numrow="+numrow+"&schoolid="+schoolid+"&schoollavel="+schoollavel+"&gradelavel="+gradelavel+"&classid="+classid+"&years="+years+"&is_partime="+is_partime+"&termid="+termid+"' target='_blank'>Print All</a>");
					}
					$("#body_list").html(data.tr);	

					// var page = '';
		   //          var total = '';
		   //          if(data.totalRecord - 0 > 0){
		   //             // previous ====
		   //             page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="' + (current_page - 0 > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left"></i>Prev.</button>';
		   //             // next =======
		   //             page += '<button type="button" class="btn btn-default btn-xs a-pagination" data-current_page="'+ (current_page - 0 < data.totalPage - 0 ? current_page + 1 : data.totalPage) +'"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next<i class="glyphicon glyphicon-chevron-right"></i></button>';
		   //          }
		            //$('.pagination').html(page);
		            $('#pagenation').html(data.pagenation.pagination);
				}
				
			})
		}

	}
</script>