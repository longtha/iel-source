<div class="wrapper">
    <div class="clearfix" id="main_content_outer">

        <div id="main_content">

            <div class="result_info" style="padding-top: 0px">
                <div class="col-sm-6">
                    <strong>Timetable</strong>
                </div>
                <div class="col-sm-6" style="text-align: right">

                </div>
            </div>
            <?php
            $m = '';
            $p = '';
            if (isset($_GET['m'])) {
                $m = $_GET['m'];
            }
            if (isset($_GET['p'])) {
                $p = $_GET['p'];
            }
            ?>
            <?php if($this->session->userdata('match_con_posid')!='stu'){?>
            <form method="post" accept-charset="utf-8" id="fragelev"
                  action="<?php echo site_url('school/c_time_table/reload?m=' . $m . '&p=' . $p . ''); ?>" class="gform">

                <div class="col-sm-4 col-md-4 ">
                    <div class="">
                        <label class="req" for="school">School</label>
                        <select class="form-control" id='school' name='school' min='1' required
                                data-parsley-required-message="Select any school">

                            <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                                <option
                                    value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4 col-md-4 ">

                    <div class="">
                        <label class="req" for="school-level">School Level</label>
                        <select class="form-control" id="school-level" name="school-level">
                            <option value=""></option>
                        </select>
                    </div>

                </div>
                <div class="col-sm-4 col-md-4 ">

                    <div class="">
                        <label class="req" for="academic-year">Academic Year</label>
                        <select class="form-control" id="academic-year" name="academic-year">
                            <option value=""></option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3 ">
                    <div class="">
                        <label class="req" for="grade-level">Grade Level</label>
                        <select class="form-control" id="grade-level" name="grade-level">
                          <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 ">
                    <div class="">
                        <label class="req" for="class">Select Class</label>
                        <select class="form-control" id="class" name="class">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 ">

                    <label class="req" for="from_date">From Date</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="from_date" name="from_date" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-time_table"></i></span>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3 ">

                    <label class="req" for="to_date">To Date</label>

                    <div data-date-format="dd-mm-yyyy" class="input-group date dob checksub">
                        <input id="to_date" name="to_date" value="<?php echo date("d-m-Y") ?>" data-type="dateIso"
                                class="form-control" type="text">
                        <span class="input-group-addon"><i class="icon-time_table"></i></span>
                    </div>

                </div>
                <div class="col-sm-12">
                    <div class="dv-std" id="dv-std"></div>
                </div>
                <div class="col-sm-12" style="padding-bottom: 5px;padding-top: 5px">
                    <div align="center">
                        <?php if ($this->green->gAction("R")) { ?>
                            <input type="button" name="btn-search" id='btn-search' value="Search" class="btn btn-primary"/>
                        <?php } ?>
                    </div>
                </div>

            </form>
            <?php }?>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="panel panel-default">

        <div class="panel-body table-responsive">
            <table border="0" align="center" id='tbl-time-table' class="data-table table-striped table-bordered table-hover">
                <thead class="tab_head">
                <th>School</th>
                <th>School Level</th>
                <th>Academic Year</th>
                <th>Grade Level</th>
                <th>Class</th>
                <th>Title</th>
                <th>Note</th>
                <th>File</th>
                <th>Date</th>
                <th>Create By</th>
                <th colspan="3">
                  <span id="span-addnew" data-toggle="modal" data-target="#time_table-modal">
                  <?php if ($this->green->gAction("C")) { ?>
                    <img src="<?php echo base_url('assets/images/icons/add.png') ?>"/>
                  <?php } ?>
                  </span>
                </th>
              </thead>

                <tbody>
                  <tr>
                    <td colspan="11">No data found..</td>
                  </tr>
                </tbody>
                <!---- Start pagination ---->
                <tfoot>
                <tr>
                    <td colspan='13' id='pgt'>
                        <ul class='pagination'>

                        </ul>
                    </td>
                </tr>
                </tfoot>
                <!---- End Pagination ---->
            </table>

        </div>
    </div>
</div>

</div>

<!-- Modal -->
<div id="time_table-modal" class="modal fade" role="dialog"  tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" style="width:80%">

    <!-- Modal content-->
    <div id="model_content" class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create Timetable</h4>
      </div>
      <div class="modal-body">
        <form method="post" accept-charset="utf-8" id="dlg-frm-time_table" target="dlg-if-time_table" enctype="multipart/form-data"
              action="<?php echo site_url('school/c_time_table/save?m=' . $m . '&p=' . $p . ''); ?>" class="gform">

            <div class="col-sm-3 col-md-3 ">
                <div class="">
                    <label class="req" for="dlg-school">School</label>
                    <select class="form-control" id='dlg-school' name='school' min='1' required
                            data-parsley-required-message="Select any school">

                        <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                            <option
                                value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-3 col-md-3 ">

                <div class="">
                    <label class="req" for="dlg-school-level">School Level</label>
                    <select class="form-control" id="dlg-school-level" name="school-level">
                        <option value=""></option>
                    </select>
                </div>

            </div>
            <div class="col-sm-3 col-md-3 ">

                <div class="">
                    <label class="req" for="dlg-academic-year">Academic Year</label>
                    <select class="form-control" id="dlg-academic-year" name="academic-year">
                        <option value=""></option>
                    </select>
                </div>

            </div>
            <div class="col-sm-3 col-md-3 ">
                <div class="">
                    <label class="req" for="dlg-grade-level">Grade Level</label>
                    <select class="form-control" id="dlg-grade-level" name="grade-level">
                      <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 ">
                <div class="">
                    <label class="req" for="dlg-class">Select Class</label>
                    <select class="form-control" id="dlg-class" name="class">
                        <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3 col-md-3">
              <div>
                <label for="dlg-title">Title</label>
                <input type="text" id="dlg-title" name="title" class="form-control" value="" />
              </div>
            </div>
            <div class="col-sm-3 col-md-3">
                <label for="dlg-note">Note</label>
                <textarea id="dlg-note" name="note" class="form-control"></textarea>
            </div>
            <div class="col-sm-3 col-md-3">
                <label for="dlg-file">Attach File</label>
                <input type="file" id="dlg-file" name="file" value="" />
            </div>

            <div class="col-sm-12">
                <div class="dv-std" id=""></div>
            </div>
        </form>
        <iframe id="dlg-if-time_table" name="dlg-if-time_table" style="border: 0 none; width: 0px; height: 0px;"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" id="dlg-btn-save" class="btn btn-default" data-dismiss="modal">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">

  function error() {
    alert("save error!!!");
  }

  function reload(isFirstLoad) {
    //var parameters = isFirstLoad || isFirstLoad == undifined ? 0 : 1;
    var parameters = isFirstLoad === undefined || isFirstLoad === null ? 1 : 0;

      $.ajax({
          url: "<?php echo site_url('school/c_time_table/reload'); ?>",
          type: "POST",
          dataType: 'json',
          async: false,
          data: {
              school: $("#school").val(),
              school_level: $("#school-level").val(),
              academic_year: $("#academic-year").val(),
              grade_level: $("#grade-level").val(),
              school_class: $("#class").val(),
              from_date: $("#from-date").val(),
              to_date: $("#to-date").val(),
              parameter: parameters
          },
          success: function (time_tables) {
            var tbody = "";

            if (time_tables.length > 0) {

              for (var i = 0; i < time_tables.length; i++) {
                tbody += '<tr>' +
                            '<td>' + time_tables[i].school + '</td>' +
                            '<td>' + time_tables[i].school_level + '</td>' +
                            '<td>' + time_tables[i].academic_year + '</td>' +
                            '<td>' + time_tables[i].grade_level + '</td>' +
                            '<td>' + time_tables[i].class + '</td>' +
                            '<td>' + time_tables[i].title + '</td>' +
                            '<td>' + time_tables[i].description + '</td>' +
                            '<td>' +
                              '<a download class="a-timetable-file" href="<?php echo base_url('assets/upload/time_tables') ?>/' + time_tables[i].attach_file + '">' +
                                '<img title="' + time_tables[i].attach_file +'" src="' + callAttachedFileIcon(time_tables[i].file_type) + '" />' +
                              '</a>' +
                            '</td>' +
                            '<td>' + time_tables[i].transaction_date + '</td>' +
                            '<td>' + time_tables[i].created_by + '</td>' +
                            '<td align="center">' +
                              '<span id="' + time_tables[i].id + '" class="span-delete">' +
                              <?php if ($this->green->gAction("C")) { ?>
                                '<img src="<?php echo base_url('assets/images/icons/delete.png') ?>"/>' +
                              <?php } ?>
                              '</span>' +
                            '</td>' +
                         '</tr>';
              }
            }
            else {
              tbody += '<tr><td colspan="11" align="center">No data found..</td></tr>';
            }

            $("#tbl-time-table tbody").html(tbody);
          }
      });
  }

  function callSchoolLevel(school, schoolLevelContainer) {

    $.ajax({
        url: "<?php echo site_url('school/c_time_table/getSchoolLevel'); ?>",
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
            school: school
        },
        success: function (schoolLevel) {
            var options = "<option value=''></option>";
            if (schoolLevel.length > 0) {
                for (var i = 0; i < schoolLevel.length; i++) {
                    options += '<option value="' + schoolLevel[i].schlevelid + '" data-program="' + schoolLevel[i].programid + '">' + schoolLevel[i].sch_level + '</option>';
                }
            }

            $(schoolLevelContainer).html(options);
        }
    });
  }

  function callAcademicYear(schoolLevel, academicYearContainer) {

    $.ajax({
        url: "<?php echo site_url('school/academic_year/get_year'); ?>",
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
            schlevelid: schoolLevel
        },
        success: function (academic) {
            var options = "<option value=''></option>";
            if (academic.year.length > 0) {
                for (var i = 0; i < academic.year.length; i++) {
                    options += '<option value="' + academic.year[i].yearid + '">' + academic.year[i].sch_year + '</option>';
                }
            }

            $(academicYearContainer).html(options);
        }
    });
  }

  function callGradeLevel(schoolLevel, gradeLevelContainer) {

    $.ajax({
        url: "<?php echo site_url('school/gradelevel/getGradeLevel'); ?>",
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
            school_level: schoolLevel
        },
        success: function (gradeLevel) {
            var options = "<option value=''></option>";

            if (gradeLevel.length > 0) {
                for (var i = 0; i < gradeLevel.length; i++) {
                    options += '<option value="' + gradeLevel[i].grade_levelid + '" data-school-level="' + schoolLevel + '">' + gradeLevel[i].grade_level + '</option>';
                }
            }

            $(gradeLevelContainer).html(options);
        }
    });
  }

  function callClass(schoolLevel, gradeLevel, classContainer) {

      $.ajax({
          url: "<?php echo site_url('school/c_time_table/get_class'); ?>",
          type: "POST",
          dataType: 'json',
          async: false,
          data: {
              school_level: schoolLevel,
              grade_level: gradeLevel
          },
          success: function (schoolClass) {
              var options = "<option value=''></option>";

              if (schoolClass.length > 0) {
                  for (var i = 0; i < schoolClass.length; i++) {
                      options += '<option value="' + schoolClass[i].classid + '">' + schoolClass[i].class_name + '</option>';
                  }
              }

              $(classContainer).html(options);
          }
      });
    }

  function callAttachedFileIcon(fileType) {
    var icon = fileType == 'jpeg' ? 'jpg' : fileType.toString().toLowerCase();
    return "<?php echo base_url('assets/images/icons/'); ?>/" + icon + ".ico";
  }

  $(function () {

    reload(1);

    callSchoolLevel($("#school").val(), $("#school-level"));

    $("body").delegate("#school-level", "change", function() {
      callAcademicYear($(this).val(), $("#academic-year"));
      callGradeLevel($(this).val(), $("#grade-level"));
 
    });

    $("body").delegate("#grade-level", "change", function() {
      callClass($(this).find("option:selected").data("school-level"), $(this).val(), $("#class"));
    });

    $("#btn-search").click(function() {
    	filter_search();
    });

    $("body").delegate("#dlg-school-level", "change", function() {
      callAcademicYear($(this).val(), $("#dlg-academic-year"));
      callGradeLevel($(this).val(), $("#dlg-grade-level"));
   
    });

    $("body").delegate("#span-addnew", "click", function() {
        
      callSchoolLevel($("#school").val(), $("#dlg-school-level"));

      $('.modal-body').css('height','180px');
    });

    $("body").delegate("#dlg-grade-level", "change", function() {
      callClass($(this).find("option:selected").data("school-level"), $(this).val(), $("#dlg-class"));
    });

    $("#from_date,#to_date").datepicker({
        language: 'en',
        pick12HourFormat: true,
        format: 'dd-mm-yyyy'
    });

    $("body").delegate("#dlg-btn-save", "click", function() {
      $("#dlg-frm-time_table").submit();
      reload(1);
    });

    $("body").delegate(".span-delete", "click", function() {

      $.ajax({
          url: "<?php echo site_url('school/c_time_table/delete'); ?>",
          type: "POST",
          dataType: 'json',
          async: false,
          data: {
            id: $(this).attr("id")
          },
          success: function (result) {
            if (result.delete == "Success") {
              reload(1);
            }
            else {
              alert(result.delete);
            }
          }
      });
    });
  });
  
  function filter_search() {
	  
	    var search_params = {};
	    
		if($("#school").val()!=null && $("#school").val()!='undefined' && $("#school").val()!=""){
			search_params["school_id"]=$("#school").val();
		}
		
		if($("#school-level").val()!=null && $("#school-level").val()!='undefined' && $("#school-level").val()!=''){
			search_params["school_level_id"]=$("#school-level").val();
		}

		if($("#academic-year").val()!=null && $("#academic-year").val()!='undefined' && $("#academic-year").val()!=''){
			search_params["academic_year_id"]=$("#academic-year").val();
		}

		if($("#grade-level").val()!=null && $("#grade-level").val()!='undefined' && $("#grade-level").val()!=''){
			search_params["grade_level_id"]=$("#grade-level").find("option:selected").val();
		}

		if($("#class").val()!=null && $("#class").val()!='undefined' && $("#class").val()!=''){
			search_params["class_id"]=$("#class").find("option:selected").val();
		}
		 if($("#from_date").val()!=null 
				&& $("#from_date").val()!='undefined' 
			&& $("#to_date").val()!=null 
			&& $("#to_date").val()!='undefined' 
			&& $("#from_date").val()!="" 
				&& $("#to_date").val()!=""){
			
			search_params["from_date"]=$("#from_date").val();
			search_params["to_date"]=$("#to_date").val();
		}

	 if(($("#from_date").val()=="" && $("#to_date").val()!="")){
			
		 alert("Please select From Date");
		 return;
	 }else if($("#to_date").val()=="" && $("#from_date").val()!=""){
  	 
		 alert("Please select To Date");
		 return;
   }


	 $.ajax({
		 url: "<?php echo site_url('school/c_time_table/filter'); ?>",
       type: "POST",
       dataType: 'json',
       async: false,
       data: 'data='+JSON.stringify(search_params),
       success: function (time_tables) {
    	   var tbody = "";

           if (time_tables.length > 0) {

             for (var i = 0; i < time_tables.length; i++) {

        	 var publish_date="";
	        	  try {
		        	  
	        		    var date_string=time_tables[i].transaction_date;
						var date_strings=date_string.split(' ');
						publish_date=date_strings[0];
	        	  }catch(err) {
	        		  
	        		publish_date=time_tables[i].transaction_date;
	        	  }
                 
               tbody += '<tr>' +
                           '<td>' + time_tables[i].school + '</td>' +
                           '<td>' + time_tables[i].school_level + '</td>' +
                           '<td>' + time_tables[i].academic_year + '</td>' +
                           '<td>' + time_tables[i].grade_level + '</td>' +
                           '<td>' + time_tables[i].class + '</td>' +
                           '<td><div class="truncate">' + time_tables[i].title + '</div></td>' +
                           '<td><div class="truncate">' + time_tables[i].description + '</div></td>' +
                           '<td>' +
                             '<a download class="a-timetable-file" href="<?php echo base_url('assets/upload/time_tables') ?>/' + time_tables[i].attach_file + '">' +
                               '<img title="' + time_tables[i].attach_file +'" src="' + callAttachedFileIcon(time_tables[i].file_type) + '" />' +
                             '</a>' +
                           '</td>' +
                           '<td>' + publish_date + '</td>' +
                           '<td>' + time_tables[i].created_by + '</td>' +
                           '<td align="center">' +
                             '<span id="' + time_tables[i].id + '" class="span-delete">' +
                             <?php if ($this->green->gAction("C")) { ?>
                               '<img src="<?php echo base_url('assets/images/icons/delete.png') ?>"/>' +
                             <?php } ?>
                             '</span>' +
                           '</td>' +
                        '</tr>';
             }
           }
           else {
             tbody += '<tr><td colspan="11" align="center">No data found..</td></tr>';
           }

           $("#tbl-time-table tbody").html(tbody);
       }
   });
	
  }
 
</script>
