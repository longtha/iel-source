<div class="container-fluid" style="padding: 0 10px 10px 10px;">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
              <span class="icon">
                  <i class="fa fa-th"></i>
              </span>
                <strong>Subject Mention</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                  <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
               </a>
               <!-- <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
               </a> -->
               <!-- <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
               </a> -->
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse in" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" id="f_save">

         <div class="col-sm-4">
            <div class="form-group">
               <label for="schoolid">School Info<span style="color:red"></span></label>
               <select name="schoolid" id="schoolid" class="form-control">
                  <?php
                     $opt = '';
                     // $opt .= '<option></option>';
                     foreach ($this->info->getschinfor() as $row) {
                        $opt .= '<option value="'.$row->schoolid.'">'.$row->name.'</option>';
                     }
                     echo $opt;        
                  ?>                  
               </select>
            </div>
         </div><!-- 11111 --> 

         <div class="col-sm-4">
            <div class="form-group">
               <label for="schlevelid">School Level<span style="color:red"></span></label>
               <select name="schlevelid" id="schlevelid" class="form-control schlevelid">
                  <?php
                     $opt= '<option value=""></option>';
                     foreach ($this->level->getsch_level() as $row) {
                        $opt.= '<option value="'.$row->schlevelid.'" programid="'.$row->programid.'">'.$row->sch_level.'</option>';
                     }
                     echo $opt;        
                  ?>
               </select>
            </div>
         </div><!-- 2 -->     
         <div class="col-sm-4">
            <div class="form-group">
               <label for="grade_levelid">Grade Level<span style="color:red"></span></label>
               <select name="grade_levelid" id="grade_levelid" class="form-control grade_levelid">

               </select>
            </div>
         </div><!-- 4 -->
         <div class="col-sm-4" id="diplay_course" style="display: none;">
            <div class="form-group">
               <label for="course">Course</label>
               <SELECT name="course" id="course" class="form-control">
                  <option value="0">Core</option>
                  <option value="1">Skill</option>
               </SELECT>
            </div>
         </div>
         <div class="col-sm-4">
            <div class="form-group">
               <label for="subj_group">Subject Group<span style="color:red"></span></label>
               <select name="subj_group" id="subj_group" class="form-control subj_group">

               </select>
            </div>
         </div><!-- 4 -->  
         <div class="col-sm-4" id="show_subject">
            <div class="form-group">
               <label for="subjectid">Subject<span style="color:red"></span></label>
               <select name="subjectid" id="subjectid" class="form-control subjectid">

               </select>
            </div>
         </div><!-- 
        

         <div class="col-sm-7 col-sm-offset-5">
            <div class="form-group">
               <button type="button" class="btn btn-primary btn-sm save" id="save"><i class="glyphicon glyphicon-floppy-save"></i>&nbsp;Save</button>
               <button type="button" class="btn btn-primary btn-sm save" name="search" id="search">Search</button>
               <button type="button" class="btn btn-warning btn-sm" name="clear" id="clear" tabindex="12">Clear</button>
            </div>           
         </div> --> 
      </form>  

      <div class="row">
         <div class="col-sm-12">
            <div class="form-group col-sm-offset-5">
               <button type="button" class="btn btn-primary btn-sm search" id="search"><i class="glyphicon glyphicon-floppy-save"></i>&nbsp;SEARCH</button>
            </div> 
         </div>
         <div class="col-sm-12">
            <div class="col-sm-12" style="border-top: 1px solid #CCC;">&nbsp;</div>
         </div>           
      </div>
   </div>   
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <div id="tab_print">
               <table border="0"​ cellspacing="0" cellpadding="0" align="center" id="list_data" class="table table-hover table-condensed">               
                  <thead style="text-align: center;">
                     <tr>
                        <th style="width: 5%;">No</th>
                        <th style="width: 25%;">Subject</th>                        
                        <th style="width: 15%;text-align: right;">Grade Level</th>
                        <th style="width: 50%;text-align: center;">Mention</th>                        
                        <th style="width: 5%;">&nbsp;</th>
                     </tr>                                 
                  </thead>                
                  <tbody>
 
                  </tbody>

                  <tfoot>

                  </tfoot>
               </table>
            </div>
         </div><!-- responsive -->
      </div>
   </div>
      <!-- <div class="col-sm-2" style="display: none;">
         <select class="input-sm" name="limit_record" id="limit_record" data-toggle="tooltip" data-placement="top" title="Display">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="30">30</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="200">200</option>
            <option value="300">300</option>
            <option value="500">500</option>
            <option value="1000">10000</option>            
         </select>
      </div>
      <div class="col-sm-2" style="display: none;">
         <div id="sp_page" class="btn-group pagination" role="group" aria-label="..." style="display: inline;"></div>
      </div> -->
   
   <!-- row -->

   <div class="col-sm-7 col-sm-offset-5">
      <div class="form-group">
         <button type="button" class="btn btn-primary btn-sm save" id="save"><i class="glyphicon glyphicon-floppy-save"></i>&nbsp;Save</button>
      </div>            
   </div>
</div>

<style type="text/css">
  #list_data th {vertical-align: middle;}
  #list_data td {vertical-align: middle;}
</style>

<script type="text/javascript">
   $(function(){      

      // grade ======= 
      $('body').delegate('.grade_levelid_list', 'change', function(){
         if($(this).is(":checked")){
            $(this).closest('.tr_').find('.menid_list').prop("disabled", false);
            $(this).closest('.tr_').find('.menid_list').prop("disabled", false);

            $(this).closest('.tr_').find('.menid_list').prop("checked", true);

            $(this).closest('.tr_').find('.min_score').prop("disabled", false);
            $(this).closest('.tr_').find('.max_score').prop("disabled", false);
        
         }else{
            $(this).closest('.tr_').find('.menid_list').prop("disabled", true);
            $(this).closest('.tr_').find('.menid_list').prop("disabled", true);

            $(this).closest('.tr_').find('.menid_list').prop("checked", false);

            $(this).closest('.tr_').find('.min_score').prop("disabled", true);
            $(this).closest('.tr_').find('.max_score').prop("disabled", true);
         }         
      });

      // mention ======= 
      $('body').delegate('.menid_list', 'change', function(){
         if($(this).is(':checked')){
            $(this).closest('.tr__').find('.min_score').prop("disabled", false);
            $(this).closest('.tr__').find('.max_score').prop("disabled", false);            
         }else{
            $(this).closest('.tr__').find('.min_score').prop("disabled", true);
            $(this).closest('.tr__').find('.max_score').prop("disabled", true);            
         }         
      });

      // min score ======= 
      $('body').delegate('.min_score', 'focus', function(){
         $(this).select();
      });
      $('body').delegate('.min_score', 'focusout', function(){
         var min_object = $(this);
         var min = $(this).val() - 0;
         var max = $(this).closest('.tr__').find('.max_score').val() - 0;
         if(min > 0 && max > 0){
            if(min > max){
               toastr["warning"]("Min > Max!");
               min_object.val('');
               min_object.select();               
            }
         }
      });
      $('body').delegate('.min_score', 'keyup', function(){
         var mention_id = $(this).closest('.tr__').find('.menid_list').attr('data-menid') - 0;
         var min_score_val = $(this).val();         

         if($(this).closest('.tr__').closest('td').attr('data-top') - 0 == 1){
            var mention = $(this).closest('.tr_').closest('td').find('.menid_list');
            mention.each(function(){
               var mention_id_ = $(this).attr('data-menid') - 0;
               if(mention_id_ == mention_id){
                  $(this).closest('.tr__').find('.min_score').val(min_score_val);
               }
            });
         }
      });

      // max score ======= 
      $('body').delegate('.max_score', 'focus', function(){
         $(this).select();
      });
      $('body').delegate('.max_score', 'focusout', function(){
         var max_object = $(this);
         var min_object = $(this).closest('.tr__').find('.min_score');
         var min = $(this).closest('.tr__').find('.min_score').val() - 0;         
         var max = $(this).val() - 0;
         if(min > 0 && max >= 0){
            if(min > max){
               toastr["warning"]("Min > Max!");
               min_object.val('');
               min_object.select();               
            }
         }
      });      
      $('body').delegate('.max_score', 'keyup', function(){
         var mention_id = $(this).closest('.tr__').find('.menid_list').attr('data-menid') - 0;
         var max_score_val = $(this).val();

         if($(this).closest('.tr__').closest('td').attr('data-top') - 0 == 1){
            var mention = $(this).closest('.tr_').closest('td').find('.menid_list');
            mention.each(function(){
               var mention_id_ = $(this).attr('data-menid') - 0;
               if(mention_id_ == mention_id){
                  $(this).closest('.tr__').find('.max_score').val(max_score_val);
               }
            });
         }
      });

      $("body").delegate("#search","click",function(){
            grid();
      })
      // save ======= 
      $('body').delegate('.save', 'click', function(){
         if($('#list_data tbody').find('.grade_levelid_list').is(':checked')){
            var schoolid = $('#schoolid').val();
            var schlevelid = $('.schlevelid').val();
               
            var arr = []; 
            var arr_subj_edit = [];    
            var i = 0;       
            $('.no').each(function()
            {                             
               var subjectid = $(this).closest('tr').find('.subjectid_list').attr('data-subjectid');
               
               // grade... ==========
               var arr_grade      = [];
               var arr_grade_edit = [];
               var grade_levelid_list = $(this).closest('tr').find('.grade_levelid_list:checked');
               var j = 0;
               grade_levelid_list.each(function()
               {
                  var grandid = $(this).attr('data-grade');   
                  // mention =========   
                  var arr_men = [];
                  var menid_list = $(this).closest('.tr_').find('.menid_list:checked');
                  var k = 0;
                  menid_list.each(function(){
                     var mentionid = $(this).attr('data-menid');
                     var min_score = $(this).closest('.tr__').find('.min_score').val();
                     var max_score = $(this).closest('.tr__').find('.max_score').val();
                     if(max_score !=""  && min_score !="" ){
                        //arr_men[k] = {'subjectid': subjectid_list_id, 'grade_levelid_list_id': grade_levelid_list_id, 'menid_list_id': menid_list_id, 'min_score': min_score, 'max_score': max_score};
                        arr_men[k] = {'subjectid':subjectid,'grandid':grandid,'mentionid':mentionid,'min_score': min_score, 'max_score': max_score};
                        k++;
                     }
                     
                  });
                  arr_grade[j] =arr_men;
                  arr_grade_edit[j] = {'subjectid':subjectid,'gradid':grandid};
                  j++;
               });
               arr_subj_edit[i] = arr_grade_edit;
               arr[i] = arr_grade;
               i++;
            });
            //console.log(arr);
            $.ajax({
               url: '<?= site_url("school/subject_mention/save") ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  schoolid: schoolid,
                  schlevelid: schlevelid,
                  arr_subj_edit : arr_subj_edit,
                  arr: arr
               },
               success: function(data){
                  if(data == 1){
                     toastr["success"]('Success!');
                     grid();
                  }else{
                     toastr["warning"]("Can't save data!");
                  }
               },
               error: function() {

               }
            });// ajax =====
         }else{
            toastr["warning"]("Please, choose a 'Grade Level'...!");
         }
      });

      // tooltip ======
      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
      });

      // print =======
      $('body').delegate('#a_print', 'click', function(){
         var title="<h4 align='center'>"+ "Subject Mention" +"</h4>";
         var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "")
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint(title, export_data);
      });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Subject Mention";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html();//.replace(/<img[^>]*>/gi, "")
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });

      // remove row ========
      $('body').delegate('.del', 'click', function(){
         if($('#list_data > tbody > tr').length > 1){
            $(this).parent().parent().remove();
         }else{
            toastr["warning"]("Only one can't delete!");
         }
         
         $('.no').each(function(i){
            $(this).html(i + 1);
         });

      }); 

      // init. =====
      // grid();

      // search ========
      var programids = $('option:selected', '.schlevelid').attr("programid");
      get_subject($('.schlevelid').val(),programids);
      get_grade($('.schlevelid').val(),programids);
      $('body').delegate('.schlevelid', 'change', function(){
         var programid = $('option:selected', this).attr("programid");
         if(programid == 3){
            $("#diplay_course").show();
         }else{
            $("#diplay_course").hide();
         }
         //get_subject($(this).val(),programid);
         group_subject($(this).val(),programid);
         get_grade($(this).val(),programid);
         //grid();                 
      });
      // $('body').delegate('#subjectid', 'change', function(){         
      //    grid();  
      // });
      // $('body').delegate('#grade_levelid', 'change', function(){         
      //    grid();         
      // });
      $('.save').hide();
      /*// page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, $('#limit_record').val() - 0);
      });*/     

   }); // ready =======

   // decimal =======
   $('body').delegate('[decimal]', 'keydown', function(e) {
        var key = e.charCode || e.keyCode || 0;
        // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
        // home, end, period, and numpad decimal
        if (key == 110 || key == 190) {
            var text = $(this).val();
            if (text.toString().indexOf(".") != -1) {
                return false;
            }
        }
        return (
                key == 8 ||
                key == 9 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
   });
   $('body').delegate('#subj_group', 'change', function(e) {
      var schlavelid  = $(".schlevelid").val();
      var programid   = $(".schlevelid").find("option:selected").attr("programid");
      if(programid == 2){
         var cp = $(this).find("option:selected").attr("cp_participat");
         if(cp == 0){
            $("#show_subject").show();
            var g_subjectid = $(this).val();
            get_subject(schlavelid,programid,g_subjectid);
         }else{
            $("#show_subject").hide();
         }
      }else{
         $("#show_subject").show();
         var g_subjectid = $(this).val();
         get_subject(schlavelid,programid,g_subjectid);
      //grid();
      }
     
   });
   // get subject =======
   function get_subject(schlevelid="",programid="",g_subjectid=""){
      var course = programid==3?$("#course").val():"";
      $.ajax({
         url: '<?= site_url('school/subject_mention/get_subject') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            schlevelid: schlevelid,
            programid: programid,
            groupsubjectid: g_subjectid,
            course : course
         },
         success: function(data){
            $op = "";
            if(data.subject.length > 0){
               $op += '<option value=""></option>';
               $.each(data.subject, function(i, row){
                  $op += '<option value="'+ row.subjectid +'">'+ row.subject +'</option>';
               });
            }else{
               toastr["warning"]("Dont' have subjects!");
            }
            $("#subjectid").html($op);
         },
         error: function() {

         }
      });
   } 

   // get grade =======
   function get_grade(schlevelid = "", programid=""){
      $.ajax({
         url: '<?= site_url('school/subject_mention/get_grade') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            schlevelid: schlevelid,
            programid: programid                                  
         },
         success: function(data){
            $op = "";
            if(data.length > 0){
               $op += '<option value=""></option>';
               $.each(data, function(i, row){
                  $op += '<option value="'+ row.grade_levelid +'">'+ row.grade_level +'</option>';
               });
            }
            $("#grade_levelid").html($op);

         },
         error: function() {

         }
      });
   }
   function group_subject(get_schlavelid,programid){
      $.ajax({
         type:"POST",
         url:"<?php echo site_url('school/subject_mention/get_subject_group'); ?>",
         dataType:"HTML",
         async:false,
         data:{
            para_group:1,
            schlevelid:get_schlavelid,
            programid:programid
         },
         success:function(data){
            $("#subj_group").html(data);
         }
      })
   }
   // grid =========
   function grid(){
      //current_page = 0, total_display = 0
      // var offset = ((current_page - 1) * total_display) - 0;
      // var limit = total_display - 0;
      $.ajax({
         url: '<?= site_url('school/subject_mention/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            schlevelid: $('.schlevelid').val(),
            programid: $('option:selected', '.schlevelid').attr("programid"),
            subjectid: $('#subjectid').val(),
            subj_group: $('#subj_group').val(),
            is_cp: $('#subj_group').find("option:selected").attr("cp_participat"),
            grade_levelid: $('#grade_levelid').val(),
            course  : $('#course').val()
         },
         success: function(data) {
            $('#list_data tbody').html(data.tr); 
            if(data.value_check > 0){
               $('.grade_levelid_list').prop("checked", true);
                if($('.grade_levelid_list').is(":checked")){
                   $('.menid_list').prop("checked", true);
                }
               $('.save').show();
            }else{
               $('.save').hide();
            }
            // if(data.totalRecord - 0 > 0){
            //    $('.save').show();

            //    $('.grade_levelid_list').prop("checked", true);
            //    if($('.grade_levelid_list').is(":checked")){
            //       $('.menid_list').prop("checked", true);
            //    }
            // }else{
            //    $('.save').hide();
            // }
         },
         error: function(){

         }
      });
   }
</script>