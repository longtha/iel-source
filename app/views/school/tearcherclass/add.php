<!-- <div id='msg' style='color:white; background-color:#286090;height:30px;  text-align:center'></div> -->

<div class="wrapper">
  <div class="clearfix" id="main_content_outer">
    <div id="main_content">
      
       <div class="result_info" style="padding-top: 0px;">
        <div class="col-sm-4">
          <strong>Setup Teacher Assignation</strong>  
        </div>
        <div class="col-sm-6" style="text-align: right">
           
          <!-- Block message -->
            <?php if(isset($exist)) echo $exist ?>
            <?PHP if(isset($_GET['save'])){
                  echo "<p id='exist'>Your data has been saved successfully...!</p>";
              }else if(isset($_GET['edit'])){
                  echo "<p id='exist'>Your data has been updated successfully...!</p>";
              }else if(isset($_GET['delete'])){
                  echo "<p style='color:red;'>Your data has been deleted successfully...!</p>";
              }
              echo "<p style='color:red;' id='exist'></p>";
              //echo "";
            ?>
          <!-- End block message -->
        </div>

        <div class="col-sm-2" align="right">
            <strong>
               <a title="export" id="exportreport" href="javascript:void(0)">
                   <img src="<?php echo base_url('assets/images/icons/export.png') ?>" height="24">
               </a>
            </strong>
            <strong>
               <a title="Print" id="printreport" href="javascript:void(0)">
                   <img src="<?php echo base_url('assets/images/icons/print.png') ?>" height="24">
               </a>
            </strong>
            <strong>
               <a title="Copy" id="copy" href="#">
                   <img src="<?php echo base_url('assets/images/icons/copy.png') ?>" height="24" data-target="#dlg_copy" data-toggle='modal'>
               </a>
            </strong>
        </div>
      </div> 
      <?php
          $m='';
          $p='';
          if(isset($_GET['m'])){
              $m=$_GET['m'];
          }
          if(isset($_GET['p'])){
              $p=$_GET['p'];
         }

      ?>
      
        <div class="row">         
          <div class="col-sm-6">         
                  <div class="form_sep">
                        <label class="req" for="cboschlevel">School level<span style="color:red">*</span></label>
                        <select class="form-control" id='cboschoolevel' name='cboschoolevel' min=1 required data-parsley-required-message="Select any school level">
                            <option value='0'>Select School level</option>
                            <?php
                            foreach ($this->tclass->getschlevel() as $row) {?>
                            <option programid='<?php echo $row->programid;?>' value='<?php echo $row->schlevelid ?>'<?php if(isset($_GET['sl'])) if($_GET['sl']==$row->schlevelid) echo 'selected' ?>><?php echo $row->sch_level?></option>
                           <?php  }
                           ?>
                        </select>

                        
                  </div> 
                  <div class="form_sep">
                      <div class="form_sep">
                            <label class="req" for="cboschool">Teacher<span style="color:red">*</span></label>
                            <select name="txtteacher" id="txtteacher" class="form-control" required data-parsley-required-message="Fill teacher">
                            <?php
                              echo "<option value='0'></option>";
                              foreach($this->tclass->getTeacherOpt() as $row_teach){
                                 echo "<option value='".$row_teach->empid."'>".$row_teach->first_name.'&nbsp;'.$row_teach->last_name."</option>";
								 //echo "<option value='".$row_teach->userid."'>".$row_teach->first_name.'&nbsp;'.$row_teach->last_name."</option>";
                              }
                            ?>
                            </select>
                            <input type="text" style="display:none;" name="txtgetteacherid" id="txtgetteacherid" />
                            <input type="text" style="display:none;" name="txttransno" id='txttransno' value=""/>
                      </div>    

                     
                  </div> 
                  <div class="form_sep">
                       <label class="req" for="grandlavel">Grade level<span style="color:red">*</span></label>
                       <input type="hidden" name="htypeno" id="htypeno">
                       <select class="form-control" id='grandlavel' name='grandlavel' min=1 required data-parsley-required-message="Select any grade level">
                            
                      </select>
                  </div> 
          </div>
          <div class="col-sm-6">
              <label class="req" for="cboschool">Year<span style="color:red">*</span></label>
              <select class='form-control cboyear_year' id='cboyear' name='cboyear'>
                 
              </select>

              <div class="form_sep">
                    <!-- /<table class="table table-bordered">/ -->
                    <table >
                          <!--<td>
                               start upload image signature -->
                              <!-- <div align='center'>
                                <form enctype="multipart/form-data" method="post" accept-charset="utf-8" action="<?php // echo site_url("school/tearcherclass/uploate_save")?>" class="tdrow" id="fschyear" target="ifram">
                                    <img  src="<?php //echo site_url('../assets/upload/No_person.jpg') ?>" id="uploadPreview" style='width:88px; height:77px; margin-bottom:15px; border:1px solid #CCCCCC;'>
                                    <input type="hidden" name="htypeno_img" id="htypeno_img">
                                    <input id="uploadImage" accept="image/gif, image/jpeg, image/jpg, image/png" type="file" class="sign_img" name="userfile[]"  onchange="PreviewImage();" style="visibility:hidden; display:none" />
                                    
                                </form> 
                                <iframe name="ifram" style="display:none"></iframe> 
                              </div>
                              <div align='center'>
                                    <input type='button' class="btn btn-success" onclick="$('#uploadImage').click();" value='Signature'/>
                              </div> -->
                              <!-- end upload image signature 
                          </td>-->
                          <td style="margin:0;padding:0; border:none !important;"><label class="req" for="cboschool">Description</label>
                              <textarea name="txtaddress" id="txtaddress" style="height:55px;border-radius:4px; resize:none;" class="form-control"></textarea>
                          </td>
                    </table>                 
              </div>
          </div>
          
          <div class="col-sm-12">
              <div class="panel-body">
                  <div class="form_sep">
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <th class="col-sm-3">Handle Class</th>
                            <th class="col-sm-3">Assign Class</th>
                            <th class="col-sm-6">Subject</th>
                          </thead>
                          <tbody id="body_showClass">
                            <!-- <tr>
                              <td><ul id="classhandle" style="list-style: none;padding:0;overflow-y:auto;height: 150px;"></ul></td>
                              <td><ul id="assinclass" style="list-style: none;padding:0;"></ul></td>
                              <td><ul id="ul_subj" style="list-style: none;padding:0;overflow-y:auto;height: 150px;"></ul></td>
                            </tr> -->
                          </tbody>
                        </table>

                      </div>
                  </div> 
              </div>
          </div>
         
          
        </div>     
        <div class="row">
          <div class="col-sm-12">
            
            <div class="panel-body">
                <div class="form_sep">
                  <?php if($this->green->gAction("C")){ ?>
                  <input type="button" name="btnsaves" id='btnsaves' value="Save" class="btn btn-primary" />
                  <?php } ?>
                  <input type="button" name="btncancel" id='btncancel' value="Cancel" class="btn btn-warning" />
                </div>                               
            </div>
            
          </div>
        </div>
        
       
              
    </div>
    
 </div>  
</div>

<!------------------- Modal copy ----------------------- -->
<!-- <div class="modal fade" id="dlg_copy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #C6EEEF">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Copy Class and Subject to Next Year</h4>
            </div>
            <div class="panel-body">
                <div class="form_sep">
                    <label class="req" for="">From Year</label>
                    <select class="form-control" id="from_year">
                        <?php //echo $this->green->GetSchYear($this->session->userdata('schoolid'),$this->session->userdata('year'));?>
                    </select>
                </div>
                <div class="form_sep">
                    <label class="req" for="">To Year</label>
                    <select class="form-control" id="to_year">
                        <?php //echo $this->green->GetSchYear($this->session->userdata('schoolid'),$this->session->userdata('year'));?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <span class="copy_warning" style="color: red"></span>
                <button type="button" id='save_copy' class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

</div> -->
<!------------------- End of copy modal ---------------- -->
<style type="text/css">
   .tdrow tr,.tdrow th,.tdrow td{
    border: none !important;
   }
   table tbody tr td img{width: 20px; margin-right: 10px}
   .thead{
        border-style: solid;
        border-width: 2px;
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
        border-color: #DDDDDD;
  }
</style>

<script type="text/javascript"> 

$(function() {
   //--------- parseley validation -----
    
    // $("body").delegate("#delete_tr","click",function(){
    //   var conf = confirm("Do you want to delete this record ?");
    //   if(conf == true){
    //     $(this).parent().parent().remove();
    //     $(".sign_img:last").remove();
    //   }else{
    //     return false;
    //   }
    // })

    $("body").delegate("#printreport","click",function(){
        var htmlToPrint = '' +
                '<style type="text/css">' +
                'table th, table td {' +
                'border:1px solid #000 !important;' +
                'padding-left:50px;' +
                '}' +
                '</style>';
        var title='<div style="align:lift; padding-left: 50px; padding-bottom:20px;"><img src="<?php echo base_url('assets/images/logo/logoiel.png') ?>" height="45"></div>';
        var titles="<h4 align='center'>"+ 'Assign class and subject to teacher report' +"</h4>";
        var data = $(".print_list").html().replace(/<img[^>]*>/gi, "");
        var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
        var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
        export_data += htmlToPrint;
        title+=titles;
        gsPrint(title, export_data);

    })

    $("body").delegate("#exportreport","click",function(){  
            var title= "<table><tr><td colspan='5'><p align='center' style='font-family:Khmer MEF2;font-size:14px;'>សាលាអន្តរជាតិ អាយ អ៊ី អ៊ែល</h4></p></td></tr>";
                title+="<tr><td colspan='5'><p align='center' style='font-family:Khmer MEF2;font-size:14px;'>IEL INTERNATIONAL SCHOOL</h4></p></td></tr></table>";
            var data = $('.table').attr('border', 1,'text-align:top');
            var data = $(".print_list").html().replace(/<img[^>]*>/gi, "");
            var infor= "<table style='width:80%;'><tr><td colspan='5'><p align='center' style='font-family:Khmer MEF2;font-size:14px;'>Assign class and subject to teacher report</h4></p></td></tr></td></tr></table>";
            var data_export  =$("<div>"+title+""+infor+"<br>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
            var export_data = $("<center>"+data_export+"</center>").clone().find(".remove_tag").remove().end().html();         
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
            e.preventDefault();
            $('.table').attr('border', 0);
    }) 


    $("body").delegate("#btnsaves","click",function(){
       
        var yearid    = $("#cboyear").val();
        var teacherid = $("#txtteacher").val();
        var grandlavel = $("#grandlavel").val();
        var htypeno    = $("#htypeno").val();
        var description = $("#txtaddress").val();
        var schoollavel = $("#cboschoolevel").val();
        var programid   = $("#cboschoolevel").find("option:selected").attr("programid");
        var arr_handle      = [];
        var arr_subject     = [];
        var arr_class       = [];

        var bb = 0;
        $(".ishandle:checked").each(function(hd){
          arr_handle[hd] = $(this).attr("att_ishandle");
          bb++;
        })
        if(bb == 0){
          arr_handle[0]="";
        }
        var nn = 0;
        $(".subj_classid:checked").each(function(cs)
        {
          var classid = $(this).val();
          //var grandid = $(this).attr("grandid");
          
          var tr = $(this).parent().parent().parent().find("#tbl_"+classid);
          var arr_sub = [];
          var pp =0;
          tr.find(".subject:checked").each(function(sj){
              var classid_sub = $(this).attr("classinsubj");
              var subjid  = $(this).attr("subjid");
              //arr_subject["arr_"+classid][sj]   = {"subjid":subjid,"classid":classid_sub};
              arr_sub[pp]   = {"subjid":subjid,"classid":classid_sub};
              pp++;
              bb++;
          });
          if(pp > 0){ // check class when no have subject
            arr_class[cs] = classid;
            arr_subject[nn]   = arr_sub;
          }
          
          nn++;
          
        })
        //console.log(arr_subject);
        if(teacherid == 0){
          alert("Please choose teacher before proccess !");
          $("#txtteacher").focus();
        }else if(bb == 0){
          alert("Please choose class and subject.")
        }
        else{
            var cons = confirm("Do you want to save ?");
            if(cons == true){
              $.ajax({
                  url:"<?php echo site_url('school/tearcherclass/save_handle_assign_subject'); ?>",
                  type:"POST",
                  dataType:"HTML",
                  async:false,
                  data: {
                    add_save : 1,
                    yearid   : yearid,
                    teacherid: teacherid,
                    typeno   : htypeno,
                    description : description,
                    programid   : programid,
                    schoollavel : schoollavel,
                    grandlavel : grandlavel,
                    arr_handle : arr_handle,
                    arr_subject : arr_subject,
                    arr_class   : arr_class
                  },
                  success: function(res){
                    //$("#htypeno_img").val(res['typeno']);
                    //$("#bodylist").html(res);
                    //$("#fschyear").submit();
                  }
              });
              clear_data();
              window.location.reload();
              search();
            }else{ return false;}
        }
    })
    $("body").delegate("#delete_class,#delete_subject,#delete_handle","click",function(){
      var conf = confirm("Do you want to delete this class ?");
      if(conf == true){
        $(this).parent().parent().remove();
      }
    })
    
    $("body").delegate("#cboschoolevel","change",function(){
        get_year("");
        grandoption();
        show_grandlavel("");
      
    })
    $("body").delegate("#cboyear","change",function(){
        show_grandlavel("");
    })

    $("body").delegate("#grandlavel","change",function(){
      var grand = $(this).val();
      if( grand == 0){
        $("#body_showClass").html("");
      }else{
        show_grandlavel("");
      }
      
    })
    // $("#sign_img").on("change",function(){
    //     PreviewImage($(this));
    // })
    // $("#btn_img").on("click",function(){
    //     $("#uploadImage").click();
    // })
    
    // $("body").delegate("#edit_tr","click",function(){
    //     var tr = $(this).parent().parent();
    //     var att_grandid  = $(this).attr("att_g");
    //     var schoolavelid = tr.find("#schlavelid").val();
    //     $("#cboschoolevel").val(schoolavelid);
    //     grandoption();
    //     $("#grandlavel").val(att_grandid);
    //     var arr_ah_up  = [];
    //     var arr_ac_up = [];
    //     var arr_as_up = [];
    //     $(".tblh_"+schoolavelid+"_"+att_grandid).find(".handleid_save").each(function(){
    //       var this_h = $(this).val();
    //       if(this_h != ""){
    //         arr_ah_up["h_"+this_h] = this_h;
    //       }
          
    //     })
    //     $(".tblc_"+schoolavelid+"_"+att_grandid).find(".class_save").each(function(){
    //       var this_c = $(this).val();
    //       if(this_c != ""){
    //         arr_ac_up["c_"+this_c] = this_c;
    //       }
          
    //     })
    //     $(".tbls_"+schoolavelid+"_"+att_grandid).find(".subject_save").each(function(){
    //       var this_s = $(this).val();
    //       if(this_s != ""){
    //         arr_as_up["s_"+this_s] = this_s;
    //       }
         
    //     })
    //     var att_all = [arr_ah_up,arr_ac_up,arr_as_up];
    //     show_grandlavel(att_all);
        
    // });

    $("body").delegate("#edite_assign","click",function(){
      //clear_data();
      var tr = $(this).parent().parent();
      var typeno          = $(this).attr("typeno_edit");
      var schlavelid_edit = tr.find("#schlavelid_edit").val();
      var teacherid_edit  = tr.find("#teacherid_edit").val();
      $("#cboschoolevel").val(schlavelid_edit);
      
      $("#htypeno").val(typeno);
      $("#txtteacher").val(teacherid_edit);
      //$("#htypeno_img").val(typeno);
      var yedit     = tr.find("#yearid_edit").val();
      get_year(yedit);
      
      grandoption();
      
      var grandlavel_edit = tr.find("#grandlavel_edit").val();
      $("#grandlavel").val(grandlavel_edit);
      var arr_ah_up  = [];
      var arr_ac_up  = [];
      var arr_as_up  = [];
      $(".classh_"+typeno).each(function(){
        var att_classid_h = $(this).attr("att_h_classid");
        if(att_classid_h != ""){
            arr_ah_up["h_"+att_classid_h] = att_classid_h;
        }
      });

      $(".classc_"+typeno).each(function(){
        var att_classid_c = $(this).attr("att_c_classid");
        if(att_classid_c != ""){
            arr_ac_up["c_"+att_classid_c] = att_classid_c;
        }
      });

      $(".classs_"+typeno).each(function(){
        var classid_s     = $(this).attr("att_classid");
        var att_classid_s = $(this).attr("att_s_classid");
        if(att_classid_s != ""){
            //arr_as_up["s_"+att_classid_s] = {"subid":att_classid_s,"classid":classid_s};
            arr_as_up["s_"+classid_s+"_"+att_classid_s] = att_classid_s;
        }
      });
      var att_all = [arr_ah_up,arr_ac_up,arr_as_up];
      show_grandlavel(att_all);
      //$(".ishandle,.chclass,.subject").attr("disabled",false);
      // $(".ishandle").each(function(){
      //   var ishandleid = $(this).attr("att_ishandle");
      //   if(arr_ah_up['h_'+ishandleid] == ishandleid){
      //     $(this).attr("disabled",false);
      //   }
      // })
      // $(".chclass").each(function(){
      //   var classid = $(this).attr("att_assignclass");
      //   if(arr_ac_up['c_'+classid] == classid){
      //     $(this).attr("disabled",false);
      //   }
      // })
      // $(".subject").each(function(){
      //   var subjid = $(this).attr("subjid");
      //   var classinsubj = $(this).attr("classinsubj");
      //   if(arr_as_up['s_'+classinsubj+'_'+subjid] == subjid){
      //     $(this).attr("disabled",false);
      //   }
      // })
      //$("#uploadPreview").attr("src","<?php //echo site_url('../assets/upload/teacher/signature');?>/"+typeno+'.jpg');
      $("#btnsaves").val("Update");
      
    })
    $("body").delegate("#delete_assign","click",function(){
      var typeno = $(this).attr("typeno_del");
      var conf = confirm("Do you want to delete this record ?");
      if(conf == true)
      {
          $.ajax({
                url:"<?php echo site_url("school/tearcherclass/delete_record"); ?>",
                type:"POST",
                dataType:"HTML",
                async:false,
                data: {
                    'delete_record' : 1,
                    'typeno' : typeno
                },
                success: function(data){
                  $("#bodylist").html(data);
                }
          });
      }
    })
    $("#btncancel").on("click",function(){
        var conf = confirm("Do you want to cancel ?");
        if(conf == true){
          clear_data();
          window.location.reload();
        }else { return false;}
        
    })
    $("body").delegate("#txtteacher","change",function(){
      show_grandlavel("");
    })
    $("body").delegate("#txtsearchteacher","focus",function(){
      autofillteacher();

    })
    $("body").delegate("#txtsearchteacher","change",function(){
      var tearchname_s = $(this).attr("tearchname_s");
      var this_name    = $(this).val();
        search();
    })
    
}); 
function get_year(get_y){
    $.ajax({
        url:"<?php echo site_url('school/tearcherclass/getschoolyear'); ?>", 
        type: "POST",
        dataType:"HTML",
        async:false,
        data: {
              'schlavelid':$("#cboschoolevel").val()
        },
        success: function(data){
          $('#cboyear').html(data);
          $(".cboyear_year").val(get_y); 
        }
    });
}  
function search(){
    var teacher=$('#txtsearchteacher').val();
    var year=$('#year').val();
    var l=$('#schlevelid').val();
    var roleid=<?php echo $this->session->userdata('roleid');?>;
    var m=<?php echo $m;?>;
    var p=<?php echo $p;?>;

    $.ajax({
        url:"<?php echo site_url('school/tearcherclass/seach'); ?>", 
        type: "POST",
        dataType:"HTML",
        async:false,
        data: {
              'teacherid':teacher,
              'year':year,
              'roleid':roleid,
              'm':m,
              'p':p,
              'l':l
        },
      
       success: function(data){
          $('#bodylist').html(data);                   
       }
    });
} 
function clear_data(){
      $("#txtteacher").val(0);
      //$("#htypeno_img").val(0);
      $("#cboschoolevel").val(0);
      $("#grandlavel").html("");
      $("#body_showClass").html("");
      $("#htypeno").val("");
      $("#cboyear").val("");
      $("#btnsaves").val("Save");
      // $("#ul_subj").html("");
      $("#listclass").html("");
      //$("#uploadPreview").attr("src","<?php echo site_url('../assets/upload/No_person.jpg');?>");
      //$(".sign_img:first").nextAll().remove();
}
//-------- Upload image signature -------------- 
function PreviewImage(get_this) {
      var oFReader = new FileReader();
      oFReader.readAsDataURL(get_this.files[0]);

      oFReader.onload = function (oFREvent) {
          document.getElementById("uploadPreview").src = oFREvent.target.result;
          document.getElementById("uploadPreview").style.backgroundImage = "none";
      };
};
// function PreviewImage() {
//       var oFReader = new FileReader();
//       oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

//       oFReader.onload = function (oFREvent) {
//           document.getElementById("uploadPreview").src = oFREvent.target.result;
//           document.getElementById("uploadPreview").style.backgroundImage = "none";
//       };
// };
  // function fn_reload_img(){
  //     $.ajax({
  //         type:"post",
  //         url:"<?php echo site_url("school/tearcherclass/uploate_save"); ?>",
  //         data:{
  //           upload_img : 1,
  //           teacherid  : $("#txtteacher").val()
  //         },
  //         success:function(data){
  //           $("#fschyear").submit();
  //         }
  //     });
  // }
    
//----- end upload image signature ------------
// start sophorn =========================================
function grandoption(){
    var grandlavel = $("#grandlavel").val();
    $.ajax({
        url:"<?php echo site_url("school/tearcherclass/grandOption"); ?>",
        type:"POST",
        dataType:"JSON",
        async:false,
        data: {
            'schlavelid' : $("#cboschoolevel").val(),
            'grandlavelid' : grandlavel
        },
        success: function(data){
          if(data['arr_gran'].length > 0)
          {
            var opt_grand = "<option value='0'></option>";
            $.each(data['arr_gran'],function(k,v){
                opt_grand+="<option value='"+v['grade_levelid']+"'>"+v['grade_level']+"</option>";
            })
            $("#grandlavel").html(opt_grand);
          }
        }
    });
}
function show_grandlavel(get_arr){
    var grandlavel = $("#grandlavel").val();
	var programid  = $("#cboschoolevel").find("option:selected").attr("programid");
    $.ajax({
        url:"<?php echo site_url("school/tearcherclass/get_grandLavel"); ?>",
        type:"POST",
        dataType:"JSON",
        async:false,
        data: {
            'schlavelid' : $("#cboschoolevel").val(),
            'teacherid'  : $("#txtteacher").val(),
            'grandlavelid' : grandlavel,
            'programid' : programid,
            'yearid'    : $("#cboyear").val()
        },
        success: function(data){
                
                var opt_class    = "";
                var opt_handle   = "";
                var subj_byclass = "";
                var tr_first     = "";
                if(data['arr_class'].length > 0) 
                {// start if 11
                    var a = 0;
                    var b = 0;
                    
                    $.each(data['arr_class'],function(k,v)
                    { // start each 11
                        if(v['grade_levelid'] == grandlavel)
                        {
                          var ch_h = "";
                          var ch_c = "";
                          var ch_s = "";
                          if(get_arr.length > 0)
                          {
                            if(get_arr[0]["h_"+v['classid']] == v['classid']){
                              ch_h = "checked='checked'";
                            }
                            
                            // if(get_arr[1]["c_"+v['classid']] == v['classid']){
                            //   ch_c = "checked='checked'";
                            // }
                            //alert(get_arr[2]["s_"+v['classid']]);
                            // if(get_arr[2]["s_"+v['classid']] == v['classid']){
                            //   ch_s = "checked='checked'";
                            // }
                          }
                          var disable_ch = "";
                          var disable_c = "";
                          if(data['ch_classhandle'][0]['ch_classh_'+v['classid']] == v['classid']){
                            ch_h = "checked='checked'";
                            //disable_ch = "disabled='disabled'";
                          }
                          if(data['ch_classhandle'][1]['ch_classc_'+v['classid']] == v['classid']){
                            ch_c       = "checked='checked'";
                            //disable_c  = "disabled='disabled'";
                          }
                          //opt_class+="<li style='margin-left:10px;padding:0;'><input type='checkbox' "+disable_c+" "+ch_c+" class='chclass' id='chclassassign"+a+"' grandid='"+v['grade_levelid']+"' att_assignclass='"+v['classid']+"'  att_label_class='"+v['class_name']+"'>&nbsp;&nbsp;<label for='chclassassign"+a+"'>"+v['class_name']+"</label></li>";
                          opt_handle+="<li style='margin-left:10px;padding:0;'><input type='checkbox' "+disable_ch+" "+ch_h+" id='chhandle"+b+"' class='ishandle' att_ishandle='"+v['classid']+"' att_label_handle='"+v['class_name']+"'>&nbsp;&nbsp;<label for='chhandle"+b+"'>"+v['class_name']+"</label></li>";
                          
                          var ch_classinsubj = "";
                          var li_subj = "";
                          if(data['arr_subj'].length > 0)
                          { // start if 22
              							  var groupt_name = [];
              							  var kk = 1;
              							  var cor = 0,ski = 0;
                              $.each(data['arr_subj'],function(ks,vs)
              							  { // start each 22
													var label_c_k = "";
                    								if(programid == 3)
                    								{
                        									if(groupt_name[vs['subj_type_id']] != vs['subject_type']){
                          										groupt_name[vs['subj_type_id']] = vs['subject_type'];
                          										li_subj+="<li style=''><b>"+kk+".&nbsp;<i>"+vs['subject_type']+"</i></b></li>";
                          										kk++;
                        									}
                        									// if(vs['is_assessment'] == 0){
                            										// if(vs['is_core'] == 1){
                              											// if(cor == 0){
                              												// li_subj+="<li style=''><b>&nbsp;&nbsp;&nbsp;&nbsp;- Core</i></b></li>";
                              											// }
                              											// cor++;
                            										// }
																	// if(vs['is_skill'] == 1){
                              											// if(ski == 0){
                              												// li_subj+="<li style=''><b>&nbsp;&nbsp;&nbsp;&nbsp;- Skill</b></li>";
                              											// }
                              											// ski++;
                            										// }
                        									// }
															// else{
																
															// }
															if(vs['is_core'] == 1 && vs['is_skill'] == 0){
																label_c_k = "<i><strong>[Core]</strong></i>&nbsp;&nbsp;";
															}else if(vs['is_core'] == 0 && vs['is_skill'] == 1){
																label_c_k = "<i><strong>[Skill]</strong></i>&nbsp;&nbsp;";
															}
                    								}else if(programid == 2)
													{
                      									if(groupt_name[vs['subj_type_id']] != vs['subject_type']){
                      										groupt_name[vs['subj_type_id']] = vs['subject_type'];
                      										li_subj+="<li style=''><b>"+kk+".&nbsp;<i>"+vs['subject_type']+"</i></b></li>";
                      										kk++;
                      									}
                    								}else if(programid == 1)
													{
														/// li_subj+="<li style=''><b>"+kk+".&nbsp;<i>"+groupt_name[vs['subj_type_id']]+"</i></b></li>";
														if(groupt_name[vs['subj_type_id']] != vs['subject_type']){
															groupt_name[vs['subj_type_id']] = vs['subject_type'];
															li_subj+="<li style=''><b>"+kk+".&nbsp;<i>"+vs['subject_type']+"</i></b></li>";
															kk++;
														}
													}
    								  
    								 
                                    // check when edit
                                    if(get_arr.length > 0){
                                      if(get_arr[2]["s_"+v['classid']+"_"+vs['subjectid']]== vs['subjectid'] ){
                                        ch_s = "checked='checked'";
                                      }else{
                                        ch_s="";
                                      }  
                                    } 
                                    // check when change event
                                    var disable_su = "";
                                    if(data['ch_classhandle'][2][v['classid']] != undefined){
                                        if(data['ch_classhandle'][2][v['classid']]['ch_classs_'+vs['subjectid']] == vs['subjectid']){
                                          ch_s = "checked='checked'";
                                          //disable_su = "disabled='disabled'";
                                         
                                        }else{
                                          ch_s="";disable_su = "";
                                        } 
                                    }
									
									li_subj+="<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' "+disable_su+" "+ch_s+" class='subject' subjid='"+vs['subjectid']+"' classinsubj='"+v['classid']+"' id='id_subj_"+v['classid']+"_"+vs['subjectid']+"' att_label_subj='"+vs['subject_en']+"("+vs['subject_kh']+")'>&nbsp;&nbsp;"+label_c_k+"<label for='id_subj_"+v['classid']+"_"+vs['subjectid']+"'>"+vs['subject_en']+"&nbsp;("+vs['subject_kh']+")</label></li>";
									
							  }) // end each 22
                             
                            if(data['ch_classhandle'][3][v['classid']] == v['classid']){
                                  ch_classinsubj = "checked='checked'";
                            }else{
                                  ch_classinsubj = "";
                            }
                        }// end if 22
                        if(a==0){
                          tr_first +="<td><label for='subj_classid_"+v['classid']+"'><input type='checkbox' "+ch_classinsubj+" id='subj_classid_"+v['classid']+"' class='subj_classid' value='"+v['classid']+"' grandid='"+v['grade_levelid']+"' class_label='"+v['class_name']+"'>&nbsp;<b>"+v['class_name']+"</b></label></td>";
                          tr_first +="<td><table id='tbl_"+v['classid']+"'><tr style='border-bottom:0px solid black;'><td></td><td><ul id='ul_subj' style='list-style: none;padding:0;overflow-y::auto;height:: 150px;'>"+li_subj+"</ul></td><tr></table></td>";
                        
                        }else{
                          opt_class +="<tr><td><label for='subj_classid_"+v['classid']+"'><input type='checkbox' "+ch_classinsubj+" id='subj_classid_"+v['classid']+"' class='subj_classid' value='"+v['classid']+"' grandid='"+v['grade_levelid']+"' class_label='"+v['class_name']+"'>&nbsp;<b>"+v['class_name']+"</b></label></td>";
                          opt_class +="<td><table id='tbl_"+v['classid']+"'><tr style='border-bottom:0px solid black;'><td></td><td><ul id='ul_subj' style='list-style: none;padding:0;overflow-y::auto;height:: 150px;'>"+li_subj+"</ul></td><tr></table></td></tr>";
                        }
                          

                          // opt_class+="<td><input type='checkbox' "+ch_classinsubj+" id='subj_classid' class='subj_classid' value='"+v['classid']+"' class_label='"+v['class_name']+"'>&nbsp;"+v['class_name']+"</p></td>";
                          // subj_byclass+="<td><table><tr style='border-bottom:1px solid black;'><td></td><td><ul id='ul_subj' style='list-style: none;padding:0;overflow-y:auto;height: 150px;'>"+li_subj+"</ul></td><tr></table>";
                          a++;
                          b++;
                    }
                  }) // end each 11
                    $tr_asign = '<tr>'+
                                '<td rowspan="'+a+'"><ul id="classhandle" style="list-style: none;padding:0;overflow-y:auto;height::150px;">'+opt_handle+'</ul></td>'+tr_first+'</tr>'+opt_class;
                                //'<td><ul id="assinclass" style="list-style: none;padding:0;overflow-y:auto;height: 150px;">'+opt_class+'</ul></td>'+
                                //'<td><ul id="ul_subj" style="list-style: none;padding:0;overflow-y:auto;height: 150px;">'+li_subj+'</ul></td>'+
                                //'<td><table>'+subj_byclass+'</table></td>'
                                //'</tr>'+opt_class;
                    $("#body_showClass").html($tr_asign);
                    //$("#assinclass").html(opt_class);
                    //$("#classhandle").html(opt_handle);  
                }// end if 11
        }
    });
}
// end sophorn ===============================

//---------- auto complete ------------
function autofillteacher(){    
  var fillteacher="<?php echo site_url("school/tearcherclass/fillteacher");?>";
    $("#txtsearchteacher").autocomplete({
      source: fillteacher,
      minLength:0,
      select: function(events,ui) {          
        var f_id=ui.item.id;
        var fulname=ui.item.value;
        $(this).val(fulname);
        //$(this).val(f_id);
        // $("#txtsearchteacher").attr("tearchid_s",f_id)
        // $("#txtsearchteacher").attr("tearchname_s",fulname)
         search();
      }           
    });
}

 
</script>