<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
    <div id="main_content">
      
       <div class="result_info">
        <div class="col-sm-6">
            <strong>Setup Subject Group</strong>
        </div>
        <div class="col-sm-6" style="text-align: right"> 
          <!-- Block message -->
          <?php if(isset($exist)) echo $exist ?>
            <?PHP if(isset($_GET['save'])){
               echo "<p>Your data has been saved successfully...!</p>";
              }else if(isset($_GET['edit'])){
                  echo "<p>Your data has been updated successfully...!</p>";
              }else if(isset($_GET['delete'])){
                  echo "<p style='color:red'>Your data has been deleted successfully...!</p>";
              }
            ?>
          <!-- End block message -->
        </div> 
      </div> 
      <?php
          $m='';
          $p='';
          if(isset($_GET['m'])){
              $m=$_GET['m'];
          }
          if(isset($_GET['p'])){
              $p=$_GET['p'];
        }
      ?>
      <form method="post" accept-charset="utf-8" class="tdrow" action="<?php echo site_url("school/c_subjecttype_gep/savesubjecttype?m=$m&p=$p"); ?>" id="fsubjecttype" >      
        <div class="row">
            <div class="col-sm-6">
                <div class="panel-body">
                    <div class="form_sep">
                        <label class="req" for="schoolid">School<span style="color:red">*</span></label>
                        <select class="form-control" id='schoolid' name='schoolid' min='1' required
                                data-parsley-required-message="Select any school">
                            <option value=''>Select School</option>
                            <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                                <option value="<?php echo $schoolrow->schoolid; ?>" <?php echo ($schoolrow->schoolid == $this->session->userdata('schoolid')?"selected":""); ?> > <?php echo $schoolrow->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel-body">
                    <div class="form_sep">
                        <label class="req" for="schlevel_id">School Level<span style="color:red">*</span></label>
                        <select class="form-control" id="schlevel_id" required name="schlevel_id">
                            <option value=""></option>
                            <?php if (isset($schevels) && count($schevels) > 0) {
                                foreach ($schevels as $sclev) {
                                    echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                                }
                            } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 hide">
                <div class="panel-body">
                    <div class="form_sep">
                        <label class="req" for="yearid">Academic Year<span style="color:red">*</span></label>
                        <select class="form-control" id="yearid" required name="yearid">
                            <option value=""></option>                            
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 hide">
                <div class="panel-body" style="">
                    <div class="form_sep">
                        <label for="gradelevelid">Grade Level</label>
                        <select class="form-control" id="gradelevelid" required name="gradelevelid">
                            <option value=""></option>
                            <?php if(isset($rangelevs)) {
                                foreach($rangelevs as $row){
                                    echo '<option value="'.$row->gradelevelid.'">'.$row->rangelevelname.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                </div>
            </div>
           <div class="col-sm-6">           
                <div class="panel-body">
                    <div class="form_sep">
                        <label class="req" for="cboschool">Subject Group<span style="color:red">*</span></label>
                        <input type="text" name="txtsubjecttype" id="txtsubjecttype" class="form-control" required data-parsley-required-message="Enter subject group"/>
                    </div>
                    <div class="form_group hide">
                        <label for="is_moeys"> &nbsp; for Program </label>
                        <select name="is_moeys" id="is_moeys" class="form-control">
                            <option value="1" selected>MoEYS</option>
                            <option value="0" >TAE</option>
                        </select>
                    </div>
                    <div class="form_sep">
                        <label class="req" for="student_num">Description</label>
                        <textarea name="txtdescription" id="txtdescription" style="width:100%;height:40px; resize:none;" class="form-control"></textarea>
                    </div>
                    <div class="form_sep">
                      <label class="req hidable" for="orders">Is Group Calculate</label>
                      <select name="is_group_calc" id="is_group_calc" class="form-control">
                            <option value="1" selected>Yes</option>
                            <option value="0" >No</option>
                      </select>
                  </div>
                  <div class="form_sep">
                      <label class="req hidable" for="orders">Percent(%)</label>
                      <input type="number" name="is_group_calc_percent" value="0" id="is_group_calc_percent" class="form-control" data-parsley-required-message="Enter Percent" data-parsley-type="number">
                  </div>
                  <div class="form_sep hide">
                      <label class="req hidable" for="orders">Is Attendance Qty</label>
                      <input type="number" name="is_at_qty" value="0" id="is_at_qty" class="form-control" data-parsley-required-message="Enter Quantity" data-parsley-type="number">
                  </div>
                </div>
            </div>            
            <div class="col-sm-6">                          
              <div class="panel-body">
                  <div class="form_sep">
                      <label class="req" for="txtmaintype">Main Subject<span style="color:red">*</span></label>
                      <select name="txtmaintype" id="txtmaintype" class="form-control">
                          <option value="Subject" selected="selected">Subject</option>
                          <option value="Assessemnt" >Assessment</option>
                      </select>
                  </div>
                  <div class="form_sep">
                      <label class="req hidable" for="orders">Order</label>
                      <input type="text" name="orders" id="orders" class="form-control hidable"/>
                  </div>
                  <div class="form_sep">
                      <label class="req hidable" for="orders">Group</label>
                      <select name="is_cp" id="is_cp" class="form-control">
                          <option value="0" selected>Subject</option>
                      </select>
                  </div>
                  <div class="form_sep">
                      <label class="req hidable" for="orders">Display In report</label>
                      <select name="report_display" id="report_display" class="form-control">
                            <option value="1" selected>Yes</option>
                            <option value="0" >No</option>
                      </select>
                  </div>
              </div> 
            </div> 
        </div>        
        <div class="row">
          <div class="col-sm-12">            
            <div class="panel-body">
                <div class="form_sep">
                 <?php if($this->green->gAction("C")){ ?>
                 <input type="submit" name="btnsavesubjecttype" id='btnsavesubjecttype' value="Save" class="btn btn-primary" />
                 <?php } ?>
                 <input type="button" name="btncancel" id='btncancel' value="Cancel" class="btn btn-warning" />
                </div>                               
            </div>
          </div>
        </div>
       </form>         
    </div>    
 </div>  
</div>
<style>
   .tdrow tr,.tdrow th,.tdrow td{
    border: none !important; 
  }
</style>
<script type="text/javascript">

    $(function() {
        $('#fsubjecttype').parsley();

        $("#btncancel").click(function(){
            var r = confirm("Do you want to cancel?");
                if (r == true) {
                    location.href=("<?php echo site_url('school/c_subjecttype_gep/?m='.$m.'&p='.$p.'');?>"); 
                } else {
                    
                }
        });
        $("#btnsavesubjecttype").click(function(e) {
            addsubjecttype(e);  
        });
        $("body").delegate("#txtmaintype", "change", function () {
            var m = $(this).val();
            var g ="";
            if (m == "Subject") {
                g ='<option value="0" selected >Subject</option>';
            } else {
                g ='<option value="1" selected >Class Participation</option>'+
                    '<option value="2" >Home Work</option>'+
                    '<option value="3" >Attendance</option>';
            }
            $("#is_cp").html(g);
        });
        //----- this function is no used ------
        $("body").delegate("#schlevel_id", "change", function () {
            var sch_levelid = $(this).val();
            var schoolid = $("#schoolid").val();
            if (sch_levelid != "") {
               // getsch_year(schoolid,sch_levelid);
               // getGradeLevels(sch_levelid);
            } else {
               // $('#schlevel_id').html("");
            }
        });        
    });
    function addsubjecttype(e){
        e.preventDefault();
        var subjecttype= $('#txtsubjecttype').val();
        var maintype= $('#txtmaintype').val(); 
        var description= $('#txtdescription').val();
        var schoolid= $('#schoolid').val();
        var schlevelid= $('#schlevel_id').val();
        var orders= $('#orders').val();
        var is_cp= $('#is_cp').val();
        var is_group_calc= $('#is_group_calc').val();
        var is_group_calc_percent= $('#is_group_calc_percent').val();
        var is_at_qty= $('#is_at_qty').val();
        var report_display= $('#report_display').val();
        
        if(schoolid==''){
            toastr["warning"]("School info was required !");
        }else if(schlevelid==''){
            toastr["warning"]("School level required !");
        }else if(subjecttype==''){
            toastr["warning"]("subject group was required !");
        }else if(maintype==''){
            toastr["warning"]("Main subject was required !");            
        }else{                       
            $.ajax({
                    //school/subjecttype is controller
                    url:"<?php echo base_url(); ?>index.php/school/c_subjecttype_gep/savesubjecttype",    
                    data: {
                            'subjecttype':subjecttype,
                            'maintype':maintype, 
                            'description':description,
                            'schoolid':schoolid,
                            'schlevelid':schlevelid,
                            'orders':orders,
                            'is_cp':is_cp,
                            'is_group_calc':is_group_calc,
                            'is_group_calc_percent':is_group_calc_percent,
                            'is_at_qty':is_at_qty,
                            'report_display':report_display
                          },
                    type: "POST",
                    success: function(data){
                      if(data.res==1){
                          toastr["success"]("Subject group was save successfully!");
                      }else{
                           toastr["warning"]("Subject group is already existed !");
                      }
                      f_clear(e);
                      search(e);
                    }
            });
        }
    }
    function f_clear(event) {
        $('#txtsubjecttype').val(""); 
        $('#txtmaintype').val(""); 
        $('#txtdescription').val("");
        $('#orders').val("");
        $('#is_group_calc_percent').val("");
        $('#is_at_qty').val("");
    }
    function getGradeLevels(schlevelid) {
        $("#gradelevelid").html("");        
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo base_url(); ?>index.php/school/c_subjecttype_gep/cgetgradelevels",
                data: {'schlevelid':schlevelid},
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (data) {                    
                    $("#gradelevelid").html(data.gradid);
                }
            });
        }
    }
    function getsch_year(schoolid,sch_level) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/school/c_subjecttype_gep/cgetschoolyear",
            data: {'schoolid':schoolid, 'sch_level': sch_level},
            async: false,
            type: "post",
            success: function (data) {
                $('#yearid').html(data.schyear);
            }
        });
    }
</script>