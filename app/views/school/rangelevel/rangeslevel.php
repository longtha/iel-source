<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
        <div id="main_content">

            <div class="result_info">
                <div class="col-sm-6">
                    <strong>Grade Range</strong>
                </div>
                <div class="col-sm-6" style="text-align: right">

                </div>
            </div>
            <?php
            $m = '';
            $p = '';
            if (isset($_GET['m'])) {
                $m = $_GET['m'];
            }
            if (isset($_GET['p'])) {
                $p = $_GET['p'];
            }
            ?>
            <form method="post" accept-charset="utf-8" id="fragelev"
                  action="<?php echo site_url('school/rangeslevel/save?m=' . $m . '&p=' . $p . ''); ?>">

                <div class="col-sm-6">
                    <div class="panel-body">
                        <div class="form_sep">
                            <label class="req" for="schoolid">School<span style="color:red">*</span></label>
                            <select class="form-control" id='schoolid' name='schoolid' min='1' required
                                    data-parsley-required-message="Select any school">
                                <option value=''>Select School</option>
                                <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                                    <option
                                        value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>


                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel-body">
                        <div class="form_sep">
                            <label class="req" for="sclevelid">School Level<span style="color:red">*</span></label>
                            <select class="form-control" id="sclevelid" required name="sclevelid">
                                <option value=""></option>
                                <?php if (isset($schevels) && count($schevels) > 0) {
                                    foreach ($schevels as $sclev) {
                                        echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                                    }
                                } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="panel-body" style="">
                        <div class="form_sep">
                            <label for="rangelevelname">Range Level</label>
                            <input type="text" class="form-control" id="rangelevelname" name="rangelevelname"/>
                            <input type="hidden" class="form-control" id="rangelevelid" name="rangelevelid"/>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel-body" style="">
                        <div class="form_sep" id="dv_class_range">

                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><span data-toggle="tooltip" title="Set study day and time">Study Time</span>
                            </h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-4">
                                <div class="form_sep">
                                    <label for="sdaysrangeid">Day Range</label>
                                    <select class="sdaysrangeid form-control" name="sdaysrangeid" id="sdaysrangeid">
                                        <?php if (isset($dayrange) && count($dayrange) > 0) {
                                            foreach ($dayrange as $ds) {
                                                echo '<option value="' . $ds->daysrangeid . '">' . $ds->dayrange . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form_sep">
                                    <label for="sshiftid">Shift</label>
                                    <select class="sshiftid form-control" name="sshiftid" id="sshiftid">
                                        <?php if (isset($shifts) && count($shifts) > 0) {
                                            foreach ($shifts as $st) {
                                                echo '<option value="' . $st->shiftid . '">' . $st->shift . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form_sep">
                                    <label for="stimes">Time</label>
                                    <select class="stimes form-control" name="stimes" id="stimes">
                                        <?php if (isset($times) && count($times) > 0) {
                                            foreach ($times as $ts) {
                                                echo '<option value="' . $ts->timeid . '">' . $ts->from_time . '-' . $ts->to_time . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-1">
                                <div class="form_sep">
                                    <label for="times">More</label>
                                    <input type="button" class="btn btn-primary" value="Add" id="add_stutime"
                                           name="add_stutime">
                                </div>
                            </div>
                            <div class="col-sm-12" style="height: 10px;"></div>
                            <div class="col-sm-12">
                                <div class="form_sep">
                                    <table class="table table-bordered table-inverse" id="rangelev_days">
                                        <thead>
                                        <tr>
                                            <th>Day Range</th>
                                            <th>Shift</th>
                                            <th>Time</th>
                                            <th width="40"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbrange_days">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">

                    <div class="panel-body">
                        <div class="form_sep">
                            <?php if ($this->green->gAction("C")) { ?>
                                <input type="submit" name="btnsave" id='btnsave' value="Save"
                                       class="btn btn-primary"/>
                            <?php } ?>
                        </div>
                    </div>

                </div>


            </form>
        </div>

    </div>
</div>
<div class="col-sm-12">
    <div class="panel panel-default">

        <div class="panel-body">
            <table border="0" ? align="center" id='listsubject' class="table">
                <thead>
                <th>Study Program</th>
                <th>School Level</th>
                <th>Range</th>
                <th colspan="2">
                </th>
                </thead>
                <thead>
                <th colspan=""></th>
                <th>
                    <select class="form-control " id="s_sclevelid"
                            name="s_sclevelid" style="width: 130px">
                        <option value=""></option>
                        <?php if (isset($schevels) && count($schevels) > 0) {
                            foreach ($schevels as $sclev) {
                                echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                            }
                        } ?>
                    </select>
                </th>
                </thead>

                <tbody id='bodylist'>


                </tbody>
                <!---- Start pagination ---->
                <tfoot>
                <tr>
                    <td colspan='5' id='pgt'>
                        <ul class='pagination'>

                        </ul>
                    </td>
                </tr>
                </tfoot>
                <!---- End Pagination ---->
            </table>

        </div>
    </div>
</div>

</div>

<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip()
    search();
    function deleteclass(event) {
        var r = confirm("Are you sure to delete this item?");
        var schoolid = "<?php echo $this->session->userdata('schoolid') ?>";
        var glabelid = $(event.target).attr('rel');

        if (r == true) {
            var id = jQuery(event.target).attr("rel");
            location.href = "<?PHP echo site_url('school/classes/deleteclass/');?>/" + glabelid + "/" + schoolid + "?<?php echo "m=$m&p=$p" ?>";
        } else {
            txt = "You pressed Cancel!";
        }
    }
    function preview(event) {
        var year = jQuery('#year').val();
        //location.href="<?PHP echo site_url('school/tearcherclass/preview/');?>/0/"+year;
        window.open("<?php echo site_url('school/classes/preview');?>/0/" + year, "_blank");
    }

    function search(startpage) {
        var s_sclevelid = $('#s_sclevelid').val();

        var roleid =<?php echo $this->session->userdata('roleid');?>;
        var m =<?php echo $m;?>;
        var p =<?php echo $p;?>;

        $.ajax({
            url: "<?php echo site_url('school/rangeslevel/search'); ?>",
            data: {
                's_sclevelid': s_sclevelid,
                'roleid': roleid,
                'm': m,
                'p': p,
                'startpage': startpage
            },
            type: "POST",
            success: function (res) {

                var data = res.datas;
                var paging = res.paging;
                var tr = "";
                if (data.length > 0) {

                    var arrprogram = [];
                    var arrlevel = [];

                    for (var i = 0; i < data.length; i++) {
                        var row = data[i];

                        var program = "";
                        var sch_level = "";
                        var rangelev = row.rangelevelname;


                        if ($.inArray(row.programid, arrprogram) == '-1') {
                            program = row.program;
                        }
                        if ($.inArray(row.schlevelid, arrlevel) == '-1') {
                            sch_level = row.sch_level;
                        }
                        if ($.inArray(row.schoolid, arrlevel) == '-1') {
                            sch_level = row.sch_level;
                        }

                        tr += '<tr>' +
                            '<td>' + program + '</td>' +
                            '<td>' + sch_level + '</td>' +
                            '<td>' + rangelev + '</td>' +
                            '</td>' +
                            '<td width="40">' +
                            '<a href="javascript:void(0)" class="link_edit" rel="' + row.rangelevelid + '">' +
                            '<img src="<?php echo base_url('assets/images/icons/edit.png') ?>"></a></td>' +
                            '<td width="40"><a href="javascript:void(0)" class="link_delete" rel="' + row.rangelevelid + '">' +
                            '<img src="<?php echo base_url('assets/images/icons/delete.png') ?>"></a></td>' +
                            '</tr>';
                        arrprogram[i] = row.programid;
                        arrlevel[i] = row.schlevelid;
                    }

                }

                $('#bodylist').html(tr);
                $('.pagination').html(paging.pagination);
            }
        });
    }
    function getClass(schlevelid) {
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo site_url('school/rangeslevel/getClass'); ?>/" + schlevelid,
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (res) {
                    var data = res.datas;
                    var tr = "";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<div class="col-md-3"><input type="checkbox" value="' + row.classid + '" checked id="clsid_' + row.classid + '" class="classid" name="classids[]"/>' +
                                '  <label class="form-label" style="font-size: 14px" for="clsid_' + row.classid + '">' + row.class_name + '</label> </div>';
                        }
                    }
                    $("#dv_class_range").html(tr);
                }
            })
        }
    }
    function addStuTime() {
        this.daysrangeid = $("#sdaysrangeid").val();
        this.shiftid = $("#sshiftid").val();
        this.timeid = $("#stimes").val();
        this.daysrange = $("#sdaysrangeid option:selected").text();
        this.shift = $("#sshiftid option:selected").text();
        this.time = $("#stimes option:selected").text();
        this.tr = '<tr>' +
            '<td><input type="hidden" class="daysrangeid" name="daysrangeids[]" value="' + this.daysrangeid + '"/>' + this.daysrange + '</td>' +
            '<td><input type="hidden" class="shiftid" name="shiftids[]" value="' + this.shiftid + '"/>' + this.shift + '</td>' +
            '<td><input type="hidden" class="timeid" name="timeids[]" value="' + this.timeid + '"/>' + this.time + '</td>' +
            '<td width="40" align="center"><a href="javascript:void(0)" class="link_del_time"><img src="<?php echo base_url("assets/images/icons/delete.png")?>" /> </a></td>' +
            '</tr>';

        if ($("#tbrange_days tr").size() > 0) {
            var newtime = daysrangeid + "_" + shiftid + "_" + timeid;
            var arraddedtime = [];
            $(".daysrangeid").each(function (i) {
                var curtr = $(this).closest("tr");
                arraddedtime[i] = curtr.find(".daysrangeid").val() + "_" + curtr.find(".shiftid").val() + "_" + curtr.find(".timeid").val();
            })
            if ($.inArray(newtime, arraddedtime) == '-1') {
                $("#tbrange_days").append(this.tr);
            }
        } else {
            $("#tbrange_days").append(this.tr);
        }

    }
    $(function () {
        $("body").delegate(".pagenav", "click", function () {
            var page = $(this).attr("id");
            search(page);
        })
        $("body").delegate("#s_sclevelid", "change", function () {
            search();
        })
        $("body").delegate(".link_delete", "click", function () {
            if (window.confirm("Sure! Do you want to delete? ")) {
                var rangelevelid = $(this).attr("rel");

                $.ajax({
                    url: "<?php echo site_url('school/rangeslevel/delete'); ?>/" + rangelevelid,
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    success: function (res) {
                        toastr["success"](res.del);
                        search(0);
                    }
                })
            }
        })
        $("body").delegate(".link_edit", "click", function () {
            var tr = $(this).closest("tr");
            var rangelevelid = $(this).attr("rel");

            $.ajax({
                url: "<?php echo site_url('school/rangeslevel/edit'); ?>/" + rangelevelid,
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (result) {
                    var res = result.rnglevelrow;
                    var rngclass = [];
                    var rngtimerows = result.rngtimerows;
                    rngclass = result.rngclsrows;

                    if (res.rangelevelid != "") {
                        $("#rangelevelid").val(res.rangelevelid);
                        $("#rangelevelname").val(res.rangelevelname);
                        $("#schoolid").val(res.schoolid);
                        $("#sclevelid").val(res.schlevelid);
                        getClass(res.schlevelid);

                        $(".classid").each(function (i) {
                            if (rngclass.length > 0) {
                                if ($.inArray($(this).val(), rngclass) == '-1') {
                                    $(this).attr("checked", false);
                                } else {
                                    $(this).attr("checked", true);
                                }
                            }
                        });

                        $("#tbrange_days").html("");
                        if (rngtimerows.length > 0) {
                            this.tr = "";
                            for (var j = 0; j < rngtimerows.length; j++) {
                                var rangetime = rngtimerows[j];
                                this.tr += '<tr>' +
                                    '<td><input type="hidden" class="daysrangeid" name="daysrangeids[]" value="' + rangetime.daysrangeid + '"/>' + rangetime.dayrange + '</td>' +
                                    '<td><input type="hidden" class="shiftid" name="shiftids[]" value="' + rangetime.shiftid + '"/>' + rangetime.shift + '</td>' +
                                    '<td><input type="hidden" class="timeid" name="timeids[]" value="' + rangetime.timeid + '"/>' + rangetime.from_time + "-" + rangetime.to_time + '</td>' +
                                    '<td width="40" align="center"><a href="javascript:void(0)" class="link_del_time"><img src="<?php echo base_url("assets/images/icons/delete.png")?>" /> </a></td>' +
                                    '</tr>';
                                $("#tbrange_days").html("");
                                $("#tbrange_days").html(this.tr);
                            }
                        }

                    }

                }
            })
        });
        $("body").delegate("#sclevelid", "change", function () {
            getClass($(this).val());
        });
        $("body").delegate("#add_stutime", "click", function () {
            addStuTime();
        })
        $("body").delegate(".link_del_time", "click", function () {
            $(this).closest("tr").remove();
        });
    })
</script>