<div class="wrapper">
    <div class="clearfix" id="main_content_outer">

        <div id="main_content">

            <div class="result_info">
                <div class="col-sm-6">
                    <strong>Range Level Fee</strong>
                </div>
                <div class="col-sm-6" style="text-align: right">

                </div>
            </div>
            <?php
            $m = '';
            $p = '';
            if (isset($_GET['m'])) {
                $m = $_GET['m'];
            }
            if (isset($_GET['p'])) {
                $p = $_GET['p'];
            }
            ?>
            <form method="post" accept-charset="utf-8" id="fragelev"
                  action="<?php echo site_url('school/rangeslevelfee/save?m=' . $m . '&p=' . $p . ''); ?>">

                <div class="col-sm-6">
                    <div class="panel-body">
                        <div class="form_sep">
                            <label class="req" for="schoolid">School<span style="color:red">*</span></label>
                            <select class="form-control" id='schoolid' name='schoolid' min='1' required
                                    data-parsley-required-message="Select any school">
                                <option value=''>Select School</option>
                                <?php foreach ($this->sch->getschinfor() as $schoolrow) { ?>
                                    <option
                                        value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>


                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="panel-body">
                        <div class="form_sep">
                            <label class="req" for="sclevelid">School Level<span style="color:red">*</span></label>
                            <select class="form-control" id="sclevelid" required name="sclevelid">
                                <option value=""></option>
                                <?php if (isset($schevels) && count($schevels) > 0) {
                                    foreach ($schevels as $sclev) {
                                        echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                                    }
                                } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel-body">
                        <div class="form_sep">
                            <label class="req" for="yearid">Academic Year<span style="color:red">*</span></label>
                            <select class="form-control" id="yearid" required name="yearid">
                                <option value=""></option>
                                <?php if (isset($schyears) && count($schyears) > 0) {
                                    foreach ($schyears as $schyear) {
                                        echo '<option value="' . $schyear->yearid . '" '.($this->session->userdata('year')==$schyear->yearid?"selected=selected":"").' >' . $schyear->sch_year . '</option>';
                                    }
                                } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="panel-body" style="">
                        <div class="form_sep">
                            <label for="rangelevelid">Range Level</label>
                            <select class="form-control" id="rangelevelid" required name="rangelevelid">
                                <option value=""></option>
                                <?php if(isset($rangelevs)) {
                                    foreach($rangelevs as $row){
                                        echo '<option value="'.$row->rangelevelid.'">'.$row->rangelevelname.'</option>';
                                    }
                                }?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel-body" style="">
                        <div class="form_sep">
                            <label for="feetypeid">Payment Type</label>
                            <select class="form-control" id="feetypeid" required name="feetypeid">
                                <option value=""></option>
                                <?php if(isset($feetypes)) {
                                    foreach($feetypes as $row){
                                        echo '<option value="'.$row->feetypeid.'">'.$row->schoolfeetype.'</option>';
                                    }
                                }?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="panel-body" style="">
                        <div class="form_sep">
                            <div id="term_sem_year_id">
                                <table class="table">
                                    <thead>
                                        <th>Term/Semester/Year</th>
                                        <th>School Fee</th>
                                        <th>Book Price</th>
                                        <th>Admin Fee</th>
                                    </thead>
                                    <tbody id="tb_feeby_peroid">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel-body" style="">
                        <div class="form_sep" id="dv_class_range">

                        </div>
                    </div>
                </div>
                <div class="col-sm-12">

                </div>
                <div class="col-sm-12">

                    <div class="panel-body">
                        <div class="form_sep">
                            <?php if ($this->green->gAction("C")) { ?>
                                <input type="submit" name="btnsave" id='btnsave' value="Save"
                                       class="btn btn-primary"/>
                            <?php } ?>
                        </div>
                    </div>

                </div>


            </form>
        </div>

    </div>
</div>
<div class="col-sm-12">
    <div class="panel panel-default">

        <div class="panel-body table-responsive" >
            <table border="0" ? align="center" id='listsubject' class="table table-striped table-bordered table-hover">
                <thead>
                <th>Study Program</th>
                <th>School Level</th>
                <th>Range</th>
                <th>Payment Type</th>
                <th>Term/Semester/Year</th>
                <th>School Fee</th>
                <th>Book Price</th>
                <th>Admin Fee</th>
                <th>Total Fee</th>
                <th colspan="2">
                </th>
                </thead>
                <thead>
                <th colspan=""></th>
                <th>
                    <select class="form-control " id="s_sclevelid"
                            name="s_sclevelid" style="width: 130px">
                        <option value=""></option>
                        <?php if (isset($schevels) && count($schevels) > 0) {
                            foreach ($schevels as $sclev) {
                                echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                            }
                        } ?>
                    </select>
                </th>
                <th colspan="10"></th>
                </thead>

                <tbody id='bodylist'>


                </tbody>
                <!---- Start pagination ---->
                <tfoot>
                <tr>
                    <td colspan='12' id='pgt'>
                        <ul class='pagination'>

                        </ul>
                    </td>
                </tr>
                </tfoot>
                <!---- End Pagination ---->
            </table>

        </div>
    </div>
</div>

</div>

<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip()
    search();

    function preview(event) {
        var year = jQuery('#year').val();
        //location.href="<?PHP echo site_url('school/tearcherclass/preview/');?>/0/"+year;
        window.open("<?php echo site_url('school/classes/preview');?>/0/" + year, "_blank");
    }

    function search(startpage) {
        var s_sclevelid = $('#s_sclevelid').val();

        var roleid =<?php echo $this->session->userdata('roleid');?>;
        var m =<?php echo $m;?>;
        var p =<?php echo $p;?>;

        $.ajax({
            url: "<?php echo site_url('school/rangeslevelfee/search'); ?>",
            data: {
                's_sclevelid': s_sclevelid,
                'roleid': roleid,
                'm': m,
                'p': p,
                'startpage': startpage
            },
            type: "POST",
            success: function (res) {

                var data = res.datas;
                var paging = res.paging;
                var tr = "";
                if (data.length > 0) {

                    var arrprogram = [];
                    var arrlevel = [];
                    var arrranglv = [];
                    var arrrfeetype = [];

                    for (var i = 0; i < data.length; i++) {
                        var row = data[i];

                        var program = "";
                        var sch_level = "";
                        var rangelevelname = "";
                        var schoolfeetype = "";

                        if ($.inArray(row.programid, arrprogram) == '-1') {
                            program = row.program;
                        }
                        if ($.inArray(row.schlevelid, arrlevel) == '-1') {
                            sch_level = row.sch_level;
                        }

                        if ($.inArray(row.rangelevelname, arrranglv) == '-1') {
                            rangelevelname = row.rangelevelname;
                        }
                        if ($.inArray(row.schoolfeetype+"_"+row.rangelevelid, arrrfeetype) == '-1') {
                            schoolfeetype = row.schoolfeetype;
                        }
                        var total_fee= parseFloat(row.fee)+parseFloat(row.book_price)+parseFloat(row.admin_fee);

                        tr += '<tr>' +
                            '<td>' + program + '</td>' +
                            '<td>' + sch_level + '</td>' +
                            '<td>' + rangelevelname + '</td>' +
                            '<td>' +schoolfeetype+ '</td>' +
                            '<td>' +row.period+ '</td>' +
                            '<td>' +row.fee+ '</td>' +
                            '<td>' +row.book_price+ '</td>' +
                            '<td>' +row.admin_fee+ '</td>' +
                            '<td>' +number_format(total_fee,2)+ '</td>' +
                            '<td width="40">' +
                            '<a href="javascript:void(0)" class="link_edit" rel="' + row.ranglevfeeid + '">' +
                            '<img src="<?php echo base_url('assets/images/icons/edit.png') ?>"></a></td>' +
                            '<td width="40"><a href="javascript:void(0)" class="link_delete" rel="' + row.ranglevfeeid + '">' +
                            '<img src="<?php echo base_url('assets/images/icons/delete.png') ?>"></a></td>' +
                            '</tr>';
                        arrprogram[i] = row.programid;
                        arrlevel[i] = row.schlevelid;
                        arrranglv[i] = row.rangelevelname;
                        arrrfeetype[i] = row.schoolfeetype+"_"+row.rangelevelid;
                    }

                }

                $('#bodylist').html(tr);
                $('.pagination').html(paging.pagination);
            }
        });
    }
    function getStudyPeriod(payment_type) {

        $("#tb_feeby_peroid").html("");
        if (payment_type != "") {
            $.ajax({
                url: "<?php echo site_url('school/rangeslevelfee/getStudyPeriod'); ?>/" + payment_type,
                type: "POST",
                dataType: 'json',
                async: false,
                data:{
                    schoolid:$("#schoolid").val(),
                    sclevelid:$("#sclevelid").val(),
                    payment_type:$("#feetypeid").val(),
                    yearid:$("#yearid").val(),
                    rangelevelid:$("#rangelevelid").val()
                },
                success: function (res) {
                    var data = res.datas;
                    var tr = "";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            var from=row.fdateshow+"-"+row.edateshow;
                            tr += '<tr><td><input type="hidden" value="'+row.term_sem_year_id+'" class="term_sem_year_id" name="term_sem_year_ids[]"/>'+row.period+"("+from+")";
                            tr += '</td><td><input type="text" value="" class="form-control fee"  name="fees[]"/>';
                            tr += '</td><td><input type="text" value="" class="form-control book_price" name="book_prices[]"/>';
                            tr += '</td><td><input type="text" value="" class="form-control admin_fee" name="admin_fees[]"/>';
                            tr += '</td></tr>';
                        }
                    }
                    $("#tb_feeby_peroid").html(tr);
                }
            })
        }
    }
    function getRangeLevels(sclevelid) {

        $("#rangelevelid").html("");
        if (sclevelid != "") {
            $.ajax({
                url: "<?php echo site_url('school/rangeslevelfee/getRangeLevels'); ?>/" + sclevelid,
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (res) {
                    var data = res.datas;
                    var tr = "";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="'+row.rangelevelid+'">'+row.rangelevelname+'</option>';
                        }
                    }
                    $("#rangelevelid").html(tr);
                }
            })
        }
    }

    function getSchYear(sclevelid) {

        $("#yearid").html("");
        if (sclevelid != "") {
            $.ajax({
                url: "<?php echo site_url('school/academic_year/get_year'); ?>",
                type: "POST",
                dataType: 'json',
                async: false,
                data:{
                    schlevelid:sclevelid
                },
                success: function (res) {
                    var data = res.year;
                    var tr = "";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="'+row.yearid+'">'+row.sch_year+'</option>';
                        }
                    }
                    $("#yearid").html(tr);
                }
            })
        }
    }

    $(function () {
        $("body").delegate(".pagenav", "click", function () {
            var page = $(this).attr("id");
            search(page);
        })
        $("body").delegate("#s_sclevelid", "change", function () {
            search();
        })

        $("body").delegate(".link_delete", "click", function () {
            if (window.confirm("Sure! Do you want to delete? ")) {
                var ranglevfeeid = $(this).attr("rel");

                $.ajax({
                    url: "<?php echo site_url('school/rangeslevelfee/delete'); ?>/" + ranglevfeeid,
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    success: function (res) {
                        toastr["success"](res.del);
                        search(0);
                    }
                })
            }
        });

        $("body").delegate(".link_edit", "click", function () {
            var tr = $(this).closest("tr");
            var ranglevfeeid = $(this).attr("rel");

            $.ajax({
                url: "<?php echo site_url('school/rangeslevelfee/edit'); ?>/" + ranglevfeeid,
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (result) {
                    var res = result.rnglevelfeerow;
                    var tr="";
                    if (res.rangelevelid != "") {
                        $("#schoolid").val(res.schoolid);
                        $("#yearid").val(res.yearid);
                        $("#rangelevelid").val(res.rangelevelid);
                        $("#sclevelid").val(res.schlevelid);
                        $("#feetypeid").val(res.feetypeid);

                        tr += '<tr><td><input type="hidden" value="'+res.term_sem_year_id+'" class="term_sem_year_id" name="term_sem_year_ids[]"/>'+res.period;
                        tr += '</td><td><input type="text" value="'+res.fee+'" class="form-control fee"  name="fees[]" />';
                        tr += '</td><td><input type="text" value="'+res.book_price+'" class="form-control book_price" name="book_prices[]"/>';
                        tr += '</td><td><input type="text" value="'+res.admin_fee+'" class="form-control admin_fee" name="admin_fees[]"/>';
                        tr += '</td></tr>';
                        $("#tb_feeby_peroid").html(tr);
                    }

                }
            })
        });

        $("body").delegate("#feetypeid", "change", function () {
            getStudyPeriod($(this).val());
        });
        $("body").delegate("#sclevelid", "change", function () {
            getRangeLevels($(this).val());
            getSchYear($(this).val());
        });
        $("body").delegate(".link_del_time", "click", function () {
            $(this).closest("tr").remove();
        });
    })
</script>