<div class="container-fluid">    
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
                <strong>Term</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Add New">
                  <img src="<?= base_url('assets/images/icons/add.png') ?>">
               </a>
               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
               </a>
               <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
                  <img src="<?= base_url('assets/images/icons/export.png') ?>">
               </a>
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>

   <div class="collapse in" id="collapseExample">             
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?= site_url('school/term/save') ?>" id="f_save">
         <input type="hidden" name="termid" id="termid">
         
         <div class="col-sm-4">         
            <div class="form-group">
               <span id="sp_term" style="display: none;">&nbsp;</span>
               <label for="semester">Term<span style="color:red">*</span></label>
               <input type="text" name="term" id="term" class="form-control" placeholder="Term" data-parsley-required="true" data-parsley-required-message="This field require" tabindex="1">
            </div>

            <div class="form-group">
               <label for="schoolid">School Info<span style="color:red">*</span></label>
               <select name="schoolid" id="schoolid" class="form-control" data-parsley-required="true" data-parsley-required-message="This field require" tabindex="4">
                  <?php
                     $opt = '';
                     // $opt .= '<option></option>';
                     foreach ($this->info->getschinfor() as $row) {
                        $opt .= '<option selected="selected" value="'.$row->schoolid.'">'.$row->name.'</option>';
                     }
                     echo $opt;        
                  ?>                  
               </select>
            </div>

            <div class="form-group">
               <label for="yearid">Year<span style="color:red">*</span></label>
               <select name="yearid" id="yearid" class="form-control" tabindex="7">
                  <?php
                     // $opt = '';
                     // $opt .= '<option></option>';
                     // foreach ($this->y->getschoolyear() as $row) {
                     //    $opt .= '<option value="'.$row->yearid.'">'.$row->sch_year.'</option>';
                     // }
                     // echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group">
               <label for="semesterid">Semester<span style="color:red"></span></label>
               <select name="semesterid" id="semesterid" class="form-control" tabindex="7">
                  
               </select>
            </div>        

            <div class="form-group" style="display: none;"> <!-- hidden -->
               <label for="semester_kh">Term Khmer<span style="color:red"></span></label>
               <input type="text" name="term_kh" id="term_kh" class="form-control" placeholder="Term Khmer">
            </div>

            <div class="form-group" style="display: none;">
               <label for="feetypeid">Fee Type<span style="color:red">*</span></label>
               <select name="feetypeid" id="feetypeid" class="form-control" tabindex="9">
                  <?php
                     $opt = '';
                     $opt .= '<option></option>';
                     foreach ($this->f->getfeetypes() as $row) {
                        $opt .= '<option value="'.$row->feetypeid.'" '.($row->feetypeid - 0 == 1 ? 'selected="selected"' : '').'>'.$row->schoolfeetype.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>         

         </div><!-- 1 -->

         <div class="col-sm-4">

            <div class="form-group">
               <label for="from_date">Start Date<span style="color:red">*</span></label>
               <input type="text" name="start_date" id="start_date" class="form-control" placeholder="dd/mm/yyyy" data-parsley-required="true" data-parsley-required-message="This field require" tabindex="2">
            </div>
            
            <div class="form-group">
               <label for="programid">Program<span style="color:red">*</span></label>
               <select name="programid" id="programid" class="form-control" data-parsley-required="true" data-parsley-required-message="This field require" tabindex="5">
                  <?php
                     $opt = '';
                     $opt .= '<option></option>';
                     foreach ($this->p->getprograms() as $row) {
                        $opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
                     }

                     echo $opt;        
                  ?>
               </select>
            </div>

            <div class="form-group" style="border: 1px solid #CCC;padding: 5px;height: 128px;"  data-toggle="tooltip" data-placement="right" title="Select a Program to get Rang level here!">
               <label for="rangelevelid">Range Level<span style="color:red">*</span></label>
               <div id="dv_rangelevelid" class="dv_rangelevelid" tabindex="8" style="height: 95px;overflow-y: auto;">
                  
               </div>
            </div>         
            
         </div><!-- 2 -->

         <div class="col-sm-4">

            <div class="form-group">
               <label for="to_date">End Date<span style="color:red">*</span></label>
               <input type="text" name="end_date" id="end_date" class="form-control" placeholder="dd/mm/yyyy" data-parsley-required="true" data-parsley-required-message="This field require" tabindex="3">
            </div>

            <div class="form-group">
               <label for="schlevelids">School Level<span style="color:red">*</span></label>
               <select name="schlevelids" id="schlevelids" class="form-control" tabindex="6">
                  <?php
                     // $opt = '';
                     // $opt .= '<option></option>';
                     // foreach ($this->level->getsch_level() as $row) {
                     //    $opt .= '<option value="'.$row->schlevelid.'">'.$row->sch_level.'</option>';
                     // }
                     // echo $opt;        
                  ?>                  
               </select>
            </div>

            <div class="form-group">
               <!-- <div>&nbsp;</div>                -->
               <input type="checkbox" name="isclosed" id="isclosed" style="width: 15px;height: 15px;" value="0" tabindex="10">&nbsp;
               <label for="isclosed">Is Inactive?<span style="color:red"></span></label>
            </div>
             <div class="form-group" style="margin-bottom:5px !important;">
                 <label for="" class="">Select Week</label>
             </div>
             <div class="form-group col-sm-12" id="dv-weeks" style="padding:0px;" >
               <?php
                     $weeks=$this->term->getallweeks();
                     $show_w = "";
                     for($i=1;$i<=2;$i++){
                        if(count($weeks)>0){
                           $show_w.="<td>";
                           foreach($weeks as $week){
                              $show_w.='<label for="ckw_'.$week->weekid.'_'.$i.'" style="padding:5px;"><input type="checkbox" class="ckweek" att_exmpid="'.$i.'" id="ckw_'.$week->weekid.'_'.$i.'" value="'.$week->weekid.'" />'.$week->week.'</label>';
                           }
                           $show_w.="</td>";
                        }
                     }
                ?>
                  <table class="table" style="width:100% !important;">
                     <tr>
                        <th style="text-align:left;">Mid Term</th>
                        <th style="text-align:left;">Final</th>
                     </tr>
                     <tr>
                        <?php echo $show_w;?>
                     </tr>
                  </table>
             </div>


         </div><!-- 3 -->      

         <div class="col-sm-7 col-sm-offset-4">
            <div class="form-group">
               <button type="button" class="btn btn-primary btn-sm save" name="save" id="save" value="save" tabindex="11" data-save='1'>Save</button>
               <button type="button" class="btn btn-primary btn-sm save" name="save_next" id="save_next" value="save" tabindex="11" data-save='2'>Save next</button>
               <button type="button" class="btn btn-warning btn-sm" name="clear" id="clear" tabindex="12">Clear</button>
            </div>            
         </div>

      </form>
      
      <div class="row">
         <div class="col-sm-12">
            <div class="col-sm-12" style="border-top: 1px solid #CCC;">
               &nbsp;
            </div>
         </div>          
      </div>

   </div>   
   
   <div class="row">
      <div class="col-sm-12">
         <div class="table-responsive">
            <div id="tab_print">
               <table border="0"​ align="center" id="se_list" class="table table-hover table-condensed">               
                   <thead>
                        <tr>
                           <th style="width: 4%;">No</th>
                           <th style="width: 8%;">Term</th>
                           <th style="width: 8%;">School</th>
                           <th style="width: 10%;">Program</th>
                           <th style="width: 10%;">Shool Level</th>
                           <th style="width: 10%;">Year</th>
                           <th style="width: 10%;">Semester</th>
                           <th style="width: 10%;">Range Level</th>
                           <th style="width: 10%;">From Date</th>
                           <th style="width: 10%;">To Date</th> 
                           <th style="width: 3%;text-align: center;">Status</th>
                           <th style="width: 10%;text-align: center;" class="remove_tag"><span>Action</span></th>
                        </tr>

                        <tr style="border-bottom: 2px solid #ddd;" class="remove_tag">
                           <td>&nbsp;</td>
                           <td>                           
                           	<input type="text" name="term_search" id="term_search" class="form-control" placeholder="Term..." data-toggle="tooltip" data-placement="top" title=""> 
                           </td>
                           <td>
                              <select name="schoolid_search" id="schoolid_search" class="form-control" placeholder="School..." data-toggle="tooltip" data-placement="top" title="">
                                 <?php
                                    $opt = '';
                                    $opt .= '<option></option>';
                                    foreach ($this->info->getschinfor() as $row) {
                                       $opt .= '<option value="'.$row->schoolid.'">'.$row->name.'</option>';
                                    }

                                    echo $opt;        
                                 ?>  
                              </select>
                           </td>                        
                           <td>
                           	<select type="text" name="programid_search" id="programid_search" class="form-control" data-toggle="tooltip" data-placement="top" title="">
                           	   <?php
                                    $opt = '';
                                    $opt .= '<option></option>';
                                    foreach ($this->p->getprograms() as $row) {
                                       $opt .= '<option value="'.$row->programid.'">'.$row->program.'</option>';
                                    }

                                    echo $opt;                    
                                 ?>
                              </select>
                           </td>
                           <td>
                              <select name="schlevelid_search" id="schlevelid_search" class="form-control" data-toggle="tooltip" data-placement="top" title="">
                                 <?php
                                    // $opt = '';
                                    // $opt .= '<option></option>';
                                    // foreach ($this->level->getsch_level() as $row) {
                                    //    $opt .= '<option value="'.$row->schlevelid.'">'.$row->sch_level.'</option>';
                                    // }

                                    // echo $opt;        
                                 ?>  
                              </select>
                           </td>
                           <td>
                           	<select name="yearid_search" id="yearid_search" class="form-control" data-toggle="tooltip" data-placement="top" title="">
 
                              </select>
                           </td> 
                           <td>
                              <select name="semesterid_search" id="semesterid_search" class="form-control" data-toggle="tooltip" data-placement="top" title="">
 
                              </select>
                           </td>
                           <td>
                              <select name="rangelevelid_search" id="rangelevelid_search" class="form-control" data-toggle="tooltip" data-placement="top" title="">
                                 <?php
                                    $opt = '';
                                    $opt .= '<option></option>';
                                    foreach ($this->r->rangelevels() as $row) {
                                       $opt .= '<option value="'.$row->rangelevelid.'">'.$row->rangelevelname.'</option>';
                                    }

                                    echo $opt;                    
                                 ?>
                              </select>
                           </td>
                            
                           <td>
                           	<input type="text" name="start_date_search" id="start_date_search" class="form-control" placeholder="dd/mm/yyyy">
                           </td>
                           <td style="width: 10%;">
                           	<input type="text" name="end_date_search" id="end_date_search" class="form-control" placeholder="dd/mm/yyyy">
                           </td>                        
                           <td style="text-align: center;">
                           	<input type="checkbox" name="isclosed_search" id="isclosed_search" style="width: 18px;height: 18px;" value="0">
                           </td>
                           <td>&nbsp;</td>                        
                        </tr>
                   </thead>
                   
                  <tbody>
                  	
                  </tbody>
                  
                  <tfoot>
                     <tr style="border-top: 2px solid #ddd;" class="remove_tag">
                        <td colspan="5" style="padding-top: 10px;">
                           <span id="sp_total"></span>
                        </td>
                        <td colspan="7" style="text-align: right;">
                           <span id="sp_page" class="btn-group pagination" role="group" aria-label="..."></span>
                        </td>                        
                     </tr>
                  </tfoot>               
               </table>
            </div>        
         </div>
      </div>
   </div>

</div>

<style type="text/css">
   #se_list th {vertical-align: middle;}
   #se_list td {vertical-align: middle;}
</style>

<script type="text/javascript">
   $(function(){
      clear();
      // fee type ======= 
      $('body').delegate('#feetypeid', 'change', function(){
         if(confirm('Are your sure to select "'+ $('#feetypeid option:selected').text() +'"?')){

         }else{
            $(this).val('');
         }
      });

      // program search ======= 
      $('body').delegate('#programid_search', 'change', function(){
         get_program_search($('#programid_search').val());
         get_rangelevel_search($('#schlevelid_search').val());
      });
      $('body').delegate('#schlevelid_search', 'change', function(){       
         get_year_search($('#programid_search').val(), $('#schlevelid_search').val());
         get_rangelevel_search($('#schlevelid_search').val());
      });
      $('body').delegate('#yearid_search', 'change', function(){
         get_semester_search($('#programid_search').val(), $('#schlevelid_search').val(),$('#yearid_search').val());
      });


      $('body').delegate(".ckweek","click",function(){
            var valthis = $(this).val();
            var examid   = $(this).attr("att_exmpid");
            var tr = $(this).parent().parent().parent();
             if(examid == 1){
               if(tr.find("#ckw_"+valthis+"_2").is(":checked")){
                  $(this).attr('checked',false);
               }
            }else{
               if(tr.find("#ckw_"+valthis+"_1").is(":checked")){
                  $(this).attr('checked',false);
               }
            }
      });

      // get program search ======
      function get_program_search(programid_search = ''){
         if(programid_search - 0 > 0){            
            $.ajax({
               url: '<?= site_url('school/term/get_program_search') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){
               },
               data: {
                  programid_search: programid_search                
               },
               success: function(data){
                  // schoollevel search =====
                  var opt = '';
                  if(data.schlevel.length > 0){
                     $.each(data.schlevel, function(i, row){
                        opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                     });
                  }else{
                     opt += '';
                  }
                  $('#schlevelid_search').html(opt);

                  $('#schlevelid_search').prepend('<option selected="selected"></option>');
               },
               error: function() {

               }
            });
         }else{
            $('#schlevelid_search').html('');
            $('#yearid_search').html('');
         }
      }
      // get year search =======
      function get_year_search(programid_search = '', schlevelid_search=''){
         if(schlevelid_search - 0 > 0){
            $.ajax({
               url: '<?= site_url('school/term/get_year_search') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){
               },
               data: {
                  programid_search: programid_search,
                  schlevelid_search: schlevelid_search                                  
               },
               success: function(data){
                  var opt = '';
                  if(data.year.length > 0){
                     $.each(data.year, function(i, row){
                        opt += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                     });
                  }else{
                     opt += '';
                  }
                  $('#yearid_search').html(opt);

                  $('#yearid_search').prepend('<option selected="selected"></option>');
               },
               error: function() {

               }
            });
         }else{
            $('#yearid_search').html('');
         }
      }
      // get semester search =======
      function get_semester_search(programid = '', schlevelids, yearid = ''){
         $.ajax({
            url: '<?= site_url('school/term/get_semester') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelids,
               yearid: yearid                                  
            },
            success: function(data){
               var opt = '';
               opt += '<option></option>';
               if(data.semester.length > 0){
                  $.each(data.semester, function(i, row){
                     opt += '<option value="'+ row.semesterid +'">'+ row.semester +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#semesterid_search').html(opt);

            },
            error: function() {

            }
         });
      }
      // get range level search =======
      function get_rangelevel_search(schlevelid){
         $.ajax({
            url: '<?= site_url('school/term/get_rangelevel_search') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               schlevelid: schlevelid                                  
            },
            success: function(data){
               var opt = '';
               opt += '<option></option>';
               if(data.rangelevel.length > 0){
                  $.each(data.rangelevel, function(i, row){
                     opt += '<option value="'+ row.rangelevelid +'">'+ row.rangelevelname +'</option>';
                  });
               }else{
                  opt += '';
               }
               $('#rangelevelid_search').html(opt);

            },
            error: function() {

            }
         });
      }

      // get ======= 
      $('body').delegate('#programid', 'change', function(){
         if($(this).val() - 0 > 0){
            get_schlevel($(this).val());
         }else{ 
            $('#schlevelids').html('');
            $('#yearid').html('');
            $('.dv_rangelevelid').html('');
         }                    
      });
      $('body').delegate('#schlevelids', 'change', function(){
            get_year($('#programid').val(), $(this).val());
            get_rangelevel($('#termid').val(), $('#schlevelids').val());
      });
      $('body').delegate('#yearid', 'change', function(){
         get_semester($('#programid').val(), $('#schlevelids').val(),$('#yearid').val());         
      });   

      // get schlevel ======
      function get_schlevel(programid = ''){      
         $.ajax({
            url: '<?= site_url('school/term/get_schlevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
               
            },
            data: {
               programid: programid                
            },
            success: function(data){
               var opt = '';
               if(data.schlevel.length > 0){
                  $.each(data.schlevel, function(i, row){
                     opt += '<option value="'+ row.schlevelid +'">'+ row.sch_level +'</option>';
                  });
               }
               $('#schlevelids').html(opt);

               // get year =====
               get_year($('#programid').val(), $('#schlevelids').val());   

               // get rangelevel =====
               get_rangelevel($('#termid').val(), $('#schlevelids').val());               

            },
            error: function() {

            }
         });         
      }
      // get year =======
      function get_year(programid = '', schlevelids = ''){
         $.ajax({
            url: '<?= site_url('school/term/get_year') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelids                                  
            },
            success: function(data){
               var opt = '';
               // opt += '<option></option>';
               if(data.year.length > 0){
                  $.each(data.year, function(i, row){
                     opt += '<option value="'+ row.yearid +'">'+ row.sch_year +'</option>';
                  });
               }
               $('#yearid').html(opt);

               // semester ======
               get_semester($('#programid').val(), $('#schlevelids').val(), $('#yearid').val());

            },
            error: function() {

            }
         });
      }
      // get semester =======
      function get_semester(programid = '', schlevelid, yearid = ''){
         $.ajax({
            url: '<?= site_url('school/term/get_semester') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               programid: programid,
               schlevelid: schlevelid,
               yearid: yearid                                  
            },
            success: function(data){
               var opt = '';
               // opt += '<option></option>';
               if(data.semester.length > 0){
                  $.each(data.semester, function(i, row){
                     opt += '<option value="'+ row.semesterid +'">'+ row.semester +'</option>';
                  });
               }
               $('#semesterid').html(opt);
            },
            error: function() {

            }
         });
      }
      // get rangelevel =======
      function get_rangelevel(termid = "", schlevelid = ""){
         $.ajax({
            url: '<?= site_url('school/term/get_rangelevel') ?>',
            type: 'POST',
            datatype: 'JSON',
            // async: false,
            beforeSend: function(){
            },
            data: {
               termid:  termid,
               schlevelid:  schlevelid
                                               
            },
            success: function(data){
               var dv = '';
               if(data.rangelevel.length > 0){
                  $.each(data.rangelevel, function(i, row){
                     dv += '<div class="checkbox" style="padding-left: 10px;">\
                                 <label>\
                                    <input type="checkbox" class="rangelevelid" data-rangelevelid="'+ row.rangelevelid +'" data-parsley-required="true" data-parsley-multiple="mymultiplelink">&nbsp;'+ row.rangelevelname +'\
                                 </label>\
                              </div>';
                  });
               }
               
               $('.dv_rangelevelid').html(dv);
            },
            error: function() {

            }
         });
      }

   	$('#start_date').datepicker({
   		format: 'dd/mm/yyyy',
    		autoClose: true
   	});
   	$('#end_date').datepicker({
         format: 'dd/mm/yyyy',
         autoClose: true
      });      

      // tooltip ======
      $('body').delegate('#semester_search', 'keyup', function(){
         $(this).removeAttr('title');
         $(this).attr('title', $('#semester_search').val());
      });
      $('body').delegate('#programid_search', 'change', function(){
         $(this).removeAttr('title');
         $(this).attr('title', $('#programid_search option:selected').text());
      });
      $('body').delegate('#yearid_search', 'change', function(){
         $(this).removeAttr('title');
         $(this).attr('title', $('#yearid_search option:selected').text());
      });
      $('body').delegate('#schoolid_search', 'change', function(){
         $(this).removeAttr('title');
         $(this).attr('title', $('#schoolid_search option:selected').text());
      });
      $('body').delegate('#schlevelid_search', 'change', function(){
         $(this).removeAttr('title');
         $(this).attr('title', $('#schlevelid_search option:selected').text());
      });
      $('body').delegate('#from_date_search', 'keyup', function(){
         $(this).removeAttr('title');
         $(this).attr('title', $('#semester_search').val());
      });

      $('[data-toggle="tooltip"]').tooltip()

      // refresh =======
      $('body').delegate('#refresh', 'click', function(){
         location.reload();
      });

      // print =======
      $('body').delegate('#a_print', 'click', function(){
         var title="<h4 align='center'>"+ 'Term List' +"</h4>";
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint(title, export_data);
      });

      // export =======
      $('body').delegate('#a_export', 'click', function(e){
         var title = "Term List";
         // var data = $('.table').attr('border', 1);
         var data = $("#tab_print").html().replace(/<img[^>]*>/gi, "");
         var export_data = $("<center><h4 align='center'>" + title + "</h4>" + data + "</center>").clone().find(".remove_tag").remove().end().html();
         window.open('data:application/vnd.ms-excel,' + encodeURIComponent(export_data));
         e.preventDefault();
         // $('.table').attr('border', 0);
      });      

      // chk =======
      $('body').delegate('#isclosed', 'click', function(){
         $(this).is(':checked') ? $(this).val(1) : $(this).val(0);
      });

      // new =======
      $('body').delegate('#a_addnew', 'click', function(){
         $('#collapseExample').collapse('toggle')
         clear();
         $('#semester').select();
         $('#save').html('Save');
         $('#save_next').show();
         $('#f_save').parsley().destroy();
      });

      // save ======= 
      $('body').delegate('.save', 'click', function(){
         var save_next = $(this).data('save');

         if($('#f_save').parsley().validate()){
            var arr = [];
            var weekids=[];
            $('.rangelevelid:checked').each(function(i){
               var rangelevelid = $(this).data('rangelevelid');
               arr[i] = {'rangelevelid': rangelevelid};
            });

            $('.ckweek:checked').each(function(i){
               var weekid = $(this).val();
               var examid = $(this).attr('att_exmpid');
               weekids[i] =  {'weekid':weekid,'examid':examid};
            });
            //console.log(weekids);
            //return false;
            $.ajax({
               url: '<?= site_url('school/term/save') ?>',
               type: 'POST',
               datatype: 'JSON',
               // async: false,
               beforeSend: function(){

               },
               data: {
                  termid: $('#termid').val(),
                  term: $('#term').val(),
                  term_kh: $('#term_kh').val(),
                  programid: $('#programid').val(),
                  yearid: $('#yearid').val(),
                  schoolid: $('#schoolid').val(),
                  schlevelid: $('#schlevelids').val(),                  
                  start_date: $('#start_date').val(),
                  end_date: $('#end_date').val(),
                  isclosed: $('#isclosed').val(),
                  semesterid: $('#semesterid').val(),

                  rangelevelid: arr,
                  feetypeid: $('#feetypeid').val(),
                  weekids:weekids
               },
               success: function(data){
                  if(data == 1){
                     if(save_next - 0 == 1){                        
                        clear();
                        $('#term').select();
                     }else{
                        $('#term').val('');
                        $('#term').select();
                     }                     
                     toastr["success"]('Success!');
                     $('#save').html('Save');
                     $('#save_next').show();                     
                     grid(1, 10);

                  }else if(data == 2){
                     toastr["warning"]('Duplicate data!');
                  }else{
                     toastr["warning"]("Data can't save!");
                  }       
               
               },
               error: function() {

               }
            });
         }
      });

      // clear =======
      $('body').delegate('#clear', 'click', function(){
         clear();
         $('#term').select();
         $('#save').html('Save');
         $('#save_next').show();
      });

      // edit all ========
      $('body').delegate('.edit', 'click', function(){
         var termid = $(this).data('id');
         clear();
         show_weeks(termid);
         $.ajax({
            url: '<?= site_url('school/term/edit') ?>',
            type: 'POST',
            datatype: 'JSON',
            //async: false,
            beforeSend: function(){

            },
            data: {
               termid: termid
            },
            success: function(data){
               $('#termid').val(data.termid);
               $('#sp_term').html(data.term);
               $('#term').val($('#sp_term').html());
               $('#term_kh').val(data.term_kh);
               $('#schoolid').val(data.schoolid);
               $('#semesterid').val(data.semesterid);

               // program =======
               $('#programid').val(data.programid);
               var schlevelid_edit = data.schlevelid;
               var yearid_edit = data.yearid;
               var semesterid_edit = data.semesterid;

               // schlevel ========                              
               $.ajax({
                  url: '<?= site_url('school/term/get_schlevel') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     programid: $('#programid').val() - 0                
                  },
                  success: function(data){                     
                     var opt = '';
                     if(data.schlevel.length > 0){
                        $.each(data.schlevel, function(i, row){
                           opt += '<option value="'+ row.schlevelid +'" '+ (row.schlevelid - 0 == schlevelid_edit - 0 ? selected="selected" : '') +'>'+ row.sch_level +'</option>';
                        });
                     }else{
                        opt += '';
                     }
                     $('#schlevelids').html(opt);

                     // year ====================
                     $.ajax({
                        url: '<?= site_url('school/term/get_year') ?>',
                        type: 'POST',
                        datatype: 'JSON',
                        beforeSend: function(){

                        },
                        data: {
                           programid: $('#programid').val(),
                           schlevelid: $('#schlevelids').val()                                  
                        },
                        success: function(data){
                           var opt = '';
                           if(data.year.length > 0){
                              $.each(data.year, function(i, row){
                                 opt += '<option value="'+ row.yearid +'" '+ (row.yearid - 0 == yearid_edit - 0 ? selected="selected" : '') +'>'+ row.sch_year +'</option>';
                              });
                           }else{
                              opt += '';
                           }
                           $('#yearid').html(opt);
                        },
                        error: function() {

                        }
                     });// year =====================

                     // semester ====================
                     $.ajax({
                        url: '<?= site_url('school/term/get_semester') ?>',
                        type: 'POST',
                        datatype: 'JSON',
                        beforeSend: function(){
                        },
                        data: {
                           programid: $('#programid').val(),
                           schlevelid: schlevelid_edit,
                           yearid: yearid_edit                                  
                        },
                        success: function(data){
                           var opt = '';
                           // opt += '<option></option>';
                           if(data.semester.length > 0){
                              $.each(data.semester, function(i, row){
                                 opt += '<option value="'+ row.semesterid +'" '+ (row.semesterid - 0 == semesterid_edit - 0 ? selected="selected" : '') +'>'+ row.semester +'</option>';
                              });
                           }else{
                              opt += '';
                           }
                           $('#semesterid').html(opt);
                        },
                        error: function() {

                        }
                     });// semester ==============

                     // rangelevel =====================
                     $.ajax({
                        url: '<?= site_url('school/term/get_rangelevel') ?>',
                        type: 'POST',
                        datatype: 'JSON',
                        // async: false,
                        beforeSend: function(){
                        },
                        data: {
                           termid: termid,
                           schlevelid: $('#schlevelids').val(),
                           // semesterid: semesterid_edit,
                        },
                        success: function(data){
                           var dv = '';                                       
                           if(data.rangelevel.length > 0){                  
                              $.each(data.rangelevel, function(i, row){
                                 var chk = '';
                                 if(data.rangelevel_chk.length > 0){
                                    $.each(data.rangelevel_chk, function(i_chk, row_chk){
                                       if(row.rangelevelid - 0 == row_chk.rangelevelid - 0){
                                          chk += 'checked="checked"';
                                       }else{
                                          chk += '';
                                       }
                                    });
                                 };

                                 dv += '<div class="checkbox" style="padding-left: 10px;">\
                                             <label>\
                                                <input type="checkbox" class="rangelevelid" data-rangelevelid="'+ row.rangelevelid +'" '+ chk +' data-parsley-required="true" data-parsley-multiple="mymultiplelink">&nbsp;'+ row.rangelevelname +'\
                                             </label>\
                                          </div>';
                              });
                           }else{
                              dv += '';
                           }                                 
                           $('.dv_rangelevelid').html(dv);
                        },
                        error: function() {

                        }
                     });// rangelevelname ============== 

                     // Check Week ====================


                     // End Check Week==================                

                  },
                  error: function() {

                  }
               });// schlevel =====================================


               $('#start_date').val((data.start_date != null && data.strat_date != '0000-00-00 00:00:00') ? data.start_date : '');
               $('#end_date').val((data.end_date != null && data.end_date != '0000-00-00 00:00:00') ? data.end_date : '');
               $('#isclosed').val(data.isclosed);
               if($('#isclosed').val() - 0 == 1){
               	$('#isclosed').prop('checked', true);
               }else{
               	$('#isclosed').prop('checked', false);
               }

               $('#feetypeid').val(data.feetypeid);                                                   

               $('#save').html('Update');
               $('#save_next').hide();                               
               $('#collapseExample').removeAttr('style');               
               $('#collapseExample').attr('aria-expanded', 'true');
               $('#collapseExample').addClass('in');

               $('#f_save').parsley().destroy();
            },
            error: function() {

            }
         });

          // $('.ckweek').prop("checked",false);
          // $.ajax({
          //     url: '<?= site_url('school/term/jsontermweek') ?>/'+termid,
          //     type: 'POST',
          //     datatype: 'JSON',
          //     //async: false,
          //     beforeSend: function(){

          //     },
          //     success: function(data){
          //         var weeks=data.savedweek;
          //         if(weeks.length-0>0){
          //             for(var j=0;j<weeks.length;j++){
          //                 var week=weeks[j];
          //                 $("#ckw_"+week.weekid).prop("checked","checked");
          //             }
          //         }else{
          //             $('.ckweek').prop("checked",false);
          //         }

          //     },
          //     error: function() {

          //     }
          // });
      });

      // delete ========
      $('body').delegate('.delete', 'click', function(){
         var termid = $(this).data('id') - 0;
         // var year_of_enroll = $(this).data('year_of_enroll') - 0;

         // if(termid == year_of_enroll){
         //    toastr["warning"]("Can't delete! data in process...");
         // }else{
            if(window.confirm('Are your sure to delete?')){
               $.ajax({
                  url: '<?= site_url('school/term/delete') ?>',
                  type: 'POST',
                  datatype: 'JSON',
                  // async: false,
                  beforeSend: function(){

                  },
                  data: {
                     termid: termid
                  },
                  success: function(data){
                     if(data == true){
                        toastr["success"]("Data deleted!");
                        grid(1, 10);
                     }else{
                        toastr["warning"]("Can't delete!");
                     }               
                     
                  },
                  error: function() {

                  }
               });
            }
         // }         
      });

      // init. =====
      grid(1, 10);

      // search ========
      $('body').delegate('#term_search', 'keyup focusout', function(){
         grid(1, 10);
      });
      $('body').delegate('#programid_search', 'change', function(){
         grid(1, 10);
      });
      $('body').delegate('#yearid_search', 'change', function(){
         grid(1, 10);
      });
      $('body').delegate('#schoolid_search', 'change', function(){
         grid(1, 10);
      });
      $('body').delegate('#schlevelid_search', 'change', function(){
         grid(1, 10);
      });
      $('body').delegate('#semesterid_search', 'change', function(){
         grid(1, 10);
      });
      $('body').delegate('#isclosed_search', 'change', function(){
         grid(1, 10);
      });
      $('body').delegate('#rangelevelid_search', 'change', function(){
         grid(1, 10);
      });
      $('input#start_date_search').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
      }).on('changeDate focusout', function(e) {
         grid(1, 10);
         $(this).removeAttr('title');
         $(this).attr('title', $('#start_date_search').val());
      });
      $('input#end_date_search').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
      }).on('changeDate focusout', function(e) {
         grid(1, 10);
         $(this).removeAttr('title');
         $(this).attr('title', $('#end_date_search').val());         
      });
      $('body').delegate('#isclosed_search', 'click', function(){
         $(this).is(':checked') ? $(this).val(1) : $(this).val(0);
      });
      // page ==========
      $('body').delegate('.a-pagination', 'click', function() {
         var current_page = $(this).data('current_page') - 0;
         grid(current_page, 10);
      });

   }); // ready ========

   // clear =======
   function clear(){
      $('#termid').val('');
      $('#term').val('');      
      $('#term_kh').val('');
      $('#programid').val('');
      $('#yearid').html('');
      $('#schlevelids').html('');
      $('#semesterid').html('');      
      $('#start_date').val('');
      $('#end_date').val('');
      $('#isclosed').val(0);
      $('#isclosed').prop('checked', false);
      $('#dv_rangelevelid').html('');
       $('.ckweek').prop("checked",false);

   }

   // grid =========
   function grid(current_page = 0, total_display = 0){
      var offset = ((current_page - 1) * total_display) - 0;
      var limit = total_display - 0;

      $.ajax({
         url: '<?= site_url('school/term/grid') ?>',
         type: 'POST',
         datatype: 'JSON',
         // async: false,
         beforeSend: function(){

         },
         data: {
            offset: offset,
            limit: limit,

            term_search: $('#term_search').val(),
            programid_search: $('#programid_search').val(),
            yearid_search: $('#yearid_search').val(),
            semesterid_search: $('#semesterid_search').val(),
            schoolid_search: $('#schoolid_search').val(),
            schlevelid_search: $('#schlevelid_search').val(),
            start_date_search: $('#start_date_search').val(),
            end_date_search: $('#end_date_search').val(),
            isclosed_search: $('#isclosed_search').val(),
            rangelevelid_search: $('#rangelevelid_search').val()
         },
         success: function(data) {
            var tr = '';
            var total = '';
            var page = '';

            if(data.result.length > 0){
               $.each(data.result, function(i, row){
                  tr += '<tr>'+
                           '<td>'+ (i + 1 + offset) +'</td>'+
                           '<td>'+ (row.term != null ? row.term : '') +'</td>'+
                           '<td>'+ (row.name != null ? row.name : '') +'</td>'+
                           '<td>'+ (row.program != null ? row.program : '') +'</td>'+
                           '<td>'+ (row.sch_level != null ? row.sch_level : '') +'</td>'+
                           '<td>'+ (row.sch_year != null ? row.sch_year : '') +'</td>'+
                           '<td>'+ (row.semester != null ? row.semester : '') +'</td>'+
                           '<td>'+ (row.rangelevelname != null ? row.rangelevelname : '') +'</td>'+
                           '<td>'+ ((row.start_date != null && row.start_date != '0000-00-00 00:00:00') ? row.start_date : '') +'</td>'+
                           '<td>'+ ((row.end_date != null && row.end_date != '0000-00-00 00:00:00') ? row.end_date : '') +'</td>'+
                           '<td style="text-align: center;">'+ (row.isclosed - 0 == 0 ? 'Active' : 'Inactive') +'</td>'+
                           '<td style="text-align: center;" class="remove_tag">\
                              <a href="javascript:;" class="edit" data-id="'+ row.termid +'" data-toggle="tooltip" data-placement="left" title="Edit">\
                                 <img src="<?= base_url('assets/images/icons/edit.png') ?>" style="width: 16px;height: 16px;">\
                              </a>|\
                              <a href="javascript:;" class="delete" data-id="'+ row.termid +'" data-toggle="tooltip" data-placement="right" title="Delete">\
                                 <img src="<?= base_url('assets/images/icons/delete.png') ?>">\
                              </a>\
                           </td>'+                           
                        '</tr>';
               });

               // previous ====
               page += '<button type="button" class="btn btn-default btn-sm a-pagination" data-current_page="' + (current_page > 1 ? current_page - 1 : 1) + '"'+ (current_page == 1 ? 'disabled ' : '') +'><i class="glyphicon glyphicon-chevron-left   "></i>&nbsp;Prev.</button>';

               // next =======
               page += '<button type="button" class="btn btn-default btn-sm a-pagination" data-current_page="' + (current_page < data.totalPage ? current_page + 1 : data.totalPage) + '"'+ (current_page == data.totalPage ? 'disabled ' : '') +'>Next&nbsp;<i class="glyphicon glyphicon-chevron-right"></i></button>';

               total += '<span style="font-weight: bold;">'+ data.totalRecord +'</span>' + ' Items';
            }else{
               tr += '<tr><td colspan="11" style="text-align: center;font-weight: bold;">No Results</td></tr>';
            }

            $('#se_list tbody').html(tr);
            $('#se_list tfoot').find('#sp_total').html(total);
            $('#se_list tfoot').find('.pagination').html(page);
         },
         error: function() {

         }
      });
      // function fn_exam_type(){
      //    $.ajax({
      //       url:"<?php echo site_url('school/term/examtype_iep') ?>",
      //       type:"POST",
      //       dataType:"JSON",
      //       data:{

      //       }
      //    })
      // }
   }

   function show_weeks(termid) {
      var url = "<?php echo site_url('school/term/show_weeks')?>";  
      $.ajax({
          url: url,
          type: "POST",
          datatype: "JSON",
          async: false,
          data: {
              'termid':termid
          },
          success: function (data) {
               $.each(data['tr_data'],function(k,v){
                  $("#ckw_"+v.weekid+"_"+v.examtypeid).prop('checked', true);
               });                    
          }
      });
  }
</script>

