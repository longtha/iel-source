<style type="text/css"> 
 
  a{
    cursor: pointer;
  }
 table tbody tr td img{
  width: 20px;  
 }
 </style>
<?php
  $m='';
  $p='';
  if(isset($_GET['m'])){
    $m=$_GET['m'];
  }
  if(isset($_GET['p'])){
    $p=$_GET['p'];
  }
  
?>

 <div class="col-sm-12">    
  <div class="panel panel-default">
        
    <div class="panel-body">                 
    <div class="table-responsive">  
      
      <table border="0"​ align="center" id='listsubject' class="table">
        <thead>
          <th align=center width=40>No</th>
		  <th width=170>School Level</th>
		  <th width=170>Group Subject</th>
          <th width=170>Subject</th>
          <th width=170>Short cut</th>
          <th width=170>Assessment</th>
          <th width=130>Max Score</th>
          <th width=130>Action</th>
        </thead>
        <tr>
            <td></td>
			<td>
			  <select class="form-control" id='s_schlevelid' name='s_schlevelid'>
					  <option value="">--Select--</option>
					  <?php if (isset($schevels) && count($schevels) > 0) {
						  foreach ($schevels as $sclev) {
							  echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
						  }
						} 
					  ?>
				</select>
			</td>       
			<td><select class="form-control" id='cbosearchsubjecttype' name='cbosearchsubjecttype'>
                <option value=''>Select Subject type</option>
                      <?php
                        // foreach ($this->subjects->getsubjecttype() as $subjecttyperow) {
							// echo "<option value='$subjecttyperow->subj_type_id'>$subjecttyperow->subject_type</option>";
                        // }
                      ?>
				</select>
			</td>
			<td><SELECT type="text" name="txtsearchsubject" id="txtsearchsubject"  class="form-control">
				
				</SELECT>
			</td>
			<td><input type="text" name="txtsearchshortcut" id="txtsearchshortcut" onkeyup='search(event);' class="form-control" /></td>
			     
        <td>
          <select class="form-control" id='s_is_assessment' name='s_is_assessment' onchange='search(event);'>
                  <option value=''>--Select--</option>
                  <option value='1'>Yes</option>
                  <option value='0'>No</option>
            </select> 
        </td>
        
        <td colspan="2"></td>
          </tr>
          <tbody id='bodylist'>
            <?php
    $i=1;
    $getGride = $this->subjects->getpagination();
    if(count($getGride)>0){
      $arrSubjid = array();
      $arrGrade = array();
      foreach($getGride as $sub_row){
        $trim='';
        $is_eval='';
        $edit ="<a class='update_row'  href='".site_url('school/c_subject_iep/editsubject?subjid='.$sub_row->subjectid)."&schlevelid=".$sub_row->schlevelid."&groupsubj=".$sub_row->subj_type_id."&m=$m&p=$p'><img src='".site_url('../assets/images/icons/edit.png')."' /></a>";
        $del =" | <a class='del_row' href='JavaScript:void(0);' rel='$sub_row->subjectid'><img src='".site_url('../assets/images/icons/delete.png')."' /></a>";

        echo "<tr>
		
                <td align=center width=40>". (!in_array($sub_row->subjectid,$arrSubjid)?$i++:'') ."</td>
				<td>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->sch_level:'') ."</td>
				<td width=170>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->subject_type:'') ."</td>
                <td width=170>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->subject:'') ."</td>
                <td>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->short_sub:'') ."</td>
                <td style='text-align:center;'>".($sub_row->is_assessment==1?"Yes":"No")."</td>
                <td style='text-align:center;'>".($sub_row->max_score!=""?$sub_row->max_score:"0")."</td>
                <td align=center width=130>".$edit.$del."</td>
              </tr>" ;
         
        $arrSubjid[] = $sub_row->subjectid;
      }
    }else{
      echo '<tr>
              <td colspan="8" align="center">
                  <h4><i>No result</i></h4>
              </td>
            </tr>';
    }
    ?>
              <!-- Start Pagination -->
               <tr>
                <td colspan='11' id='pgt'>
                  <ul class='pagination'>
                    <?php echo $this->pagination->create_links();?>
                  </ul>
                </td>
              </tr>
                <!-- End Pagination-->
            </tbody>
          </table>      
        </div>
      </div>
    </div>    
  </div>

<script type="text/javascript">

    $(function(){

        $('body').delegate('.link_edit','click',function(){
            var subjectid=$(this).attr("rel");
            if(subjectid!=""){
                getSubInf(subjectid);
            }

        });
        $("body").delegate("#s_schlevelid", "change", function () {
           sgetGradeLevels($(this).val());
        });

        $('body').delegate('.del_row','click',function(){
            var obj = $(this);
            var subjectid=$(this).attr("rel");
            deletesubject(obj,subjectid);
        });
		$("body").delegate("#s_schlevelid","change",function(){
			search_group_subject();
		});
		$("body").delegate("#cbosearchsubjecttype","change",function(){
			search_subject();
		});
		$("body").delegate("#txtsearchsubject","change",function(){
			search();
		});
    });

    function getSubInf(subjectid){
         $.ajax({
             url:"<?php echo base_url(); ?>index.php/school/c_subject_gep/select",
             dataType:"Json",
             type:"POST",
             async:false,
             data: {
                 'subjectid':subjectid
             },
             success: function(data){
                if(data.subject!=""){
                    $('#subjectid').val(subjectid);
                    $('#txtsubject').val(data.subject);
                    $('#txtsubjectkh').val(data.subject_kh);
                    $('#txtshort_sub').val(data.short_sub);
                    $('#cbosubjecttypeid').val(data.subj_type_id);
                    $('#cboschool').val(data.schoolid);
                    $('#orders').val(data.orders);
                    if(data.is_trimester_sub==1){
                        $("#is_trimester").prop("checked","checked");
                        $("#is_trimester").val(1);
                    }else{
                        $("#is_trimester").removeAttr("checked");
                        $("#is_trimester").val(0);
                    }
                    if(data.is_eval==1){
                        $("#is_eval").prop("checked","checked");
                        $("#is_eval").val(1);
                    }else{
                        $("#is_eval").removeAttr("checked");
                        $("#is_eval").val(0);
                    }
                }
             }
         });
    }
    function deletesubject(obj,subjectid){
          var r = confirm("Are you sure to delete this subject?");  
          var tr= obj.closest("tr");                
          if (r == true) {              
               $.ajax({
                    url: "<?php echo site_url('school/c_subject_iep/deletesubject');?>",
                    dataType: "Json",
                    type: "POST",
                    async: false,
                    data: {
                       'subjectid': subjectid
                    },
                    success: function (data) { 
                        if(data.del==1){
                            toastr["success"]("Subject has been deleted !");
                            tr.remove();
                        }else{
                            alert("Subject can't delete !");
                        } 
                    }
                });   
           }else{
              return false;
           }
            
    }
	function search_subject(){
		$.ajax({
			url:"<?php echo base_url(); ?>index.php/school/c_subject_iep/searchsubject",    
			type: "POST",
			data: {
                'groupsubjid':$("#cbosearchsubjecttype").val()
            },
			success: function(data){
				$('#txtsearchsubject').html(data);   
				search();			  
			}
        });
	}
	function search_group_subject(){
		
		$.ajax({
			url:"<?php echo base_url(); ?>index.php/school/c_subject_iep/getsubjecttype",    
			type: "POST",
			data: {
                  'schlevelid':$("#s_schlevelid").val()
            },
			success: function(data){
				$('#cbosearchsubjecttype').html(data);  
				search();
			}
        });
	}
    function search(event){

        var subject=$('#txtsearchsubject').val();
        var subjecttype=$('#cbosearchsubjecttype').val();
        var shortcut=$('#txtsearchshortcut').val();
        var schlevelid=$('#s_schlevelid').val();
        //var gradelevelid=$('#s_gradelevelid').val();
        var is_assessment=$('#s_is_assessment').val();
        var examtype=$('#s_examtype').val();
        $.ajax({
			url:"<?php echo base_url(); ?>index.php/school/c_subject_iep/search",    
			data: {
                  'subject':subject,
                  'subjecttype':subjecttype,
                  'shortcut':shortcut,
                  'schlevelid':schlevelid,
                  //'gradelevelid':gradelevelid,
                  'is_assessment':is_assessment,
                  'examtype':examtype,
				  'm':"<?=$m?>",
				  'p':"<?=$p?>"
                },
			type: "POST",
			success: function(data){
				//alert(data);
				jQuery('#bodylist').html(data);
			}
         });
     } 
     function sgetGradeLevels(schlevelid) {
        $("#s_gradelevelid").html("");
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo base_url(); ?>index.php/school/c_subject_iep/sgetgradlevels",
                data: {'schlevelid':schlevelid},
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (data) {                    
                    $("#s_gradelevelid").html(data);
                }
            })
        }
    }    
</script>