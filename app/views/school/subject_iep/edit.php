<style>
   .tdrow tr,.tdrow th,.tdrow td{
    border: none !important; 
  }
</style>
<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
    <div id="main_content">
      
       <div class="result_info">
        <div class="col-sm-6">
            <strong>Setup Subject</strong>  
        </div>
        <div class="col-sm-6" style="text-align: right">
            <strong>                
            </strong>   
          <!-- Block message -->
            <?PHP if(isset($_GET['save'])){
               echo "<p>Your data has been saved successfully...!</p>";
              }else if(isset($_GET['edit'])){
                  echo "<p>Your data has been updated successfully...!</p>";
              }else if(isset($_GET['delete'])){
                  echo "<p style='color:red'>Your data has been deleted successfully...!</p>";
              }
              $m='';
              $p='';
              if(isset($_GET['m'])){
                $m=$_GET['m'];
              }
              if(isset($_GET['p'])){
                  $p=$_GET['p'];
              }
              if ($query->modified_by !='')
                $user=$this->db->where('userid',$query->modified_by)->get('sch_user')->row()->user_name;
            ?>
          <!-- End block message -->
        </div> 
      </div> 
      
      <form method="post" accept-charset="utf-8" action="#" class="tdrow" id="fschyear">
          <input type="hidden" name="txtsubjectid" id='txtsubjectid' value="<?php echo $query->subjectid;?>"/>
          <label style="float:right !important; font-size:11px !important; color:red;">&nbsp;</label> 
        <div class="row">    
           <div class="col-sm-6">           
                <div class="panel-body">
                  <div class="form_sep">
                    <label class="req" for="classid">School<span style="color:red">*</span></label>
                    <select class="form-control" id='cboschool' name='cboschool'  min=1 required data-parsley-required-message="Select any school">
                      <option value=''>Select School</option>
                          <?php
                          foreach ($this->subjects->getschool() as $schoolrow) {?>
                              <option value="<?php echo $schoolrow->schoolid; ?>" <?php echo ($schoolrow->schoolid==$query->schoolid?"selected":"")?>><?php echo $schoolrow->name;?></option>
                         <?php }
                         ?>
                    </select>
                  </div>
                  <div class="form_sep">
                      <label class="req" for="cboschool">Subject Group<span style="color:red">*</span></label>
                      <select class="form-control" id='cbosubjecttype' name='cbosubjecttype' min=1 required data-parsley-required-message="Select any subject type">
                          <option value=''>Select Subject type</option>
                              <?php
                              foreach ($this->subjects->getsubjecttype() as $subjecttyperow) {?>
                                  <option value="<?php echo $subjecttyperow->subj_type_id; ?>" <?php echo ($subjecttyperow->subj_type_id==$query->subj_type_id?"selected":"");?>><?php echo $subjecttyperow->subject_type;?></option>
                             <?php }
                             ?>
                      </select>
                  </div>     
                  <div class="form_sep">
                    <label class="req" for="cboschool">Subject<span style="color:red">*</span></label>
                  <input type="text" name="txtsubject" id="txtsubject" class="form-control" value="<?php echo $query->subject?>" required data-parsley-required-message="Please fill the subject" />
                  </div>
                  <div class="form_sep">
                    <label class="req" for="cboschool">Subject (KH)</label>
                  <input type="text" name="txtsubjectkh" id="txtsubjectkh" class="form-control" value="<?php echo $query->subject_kh?>" data-parsley-required-message="Please fill the subject" />
                  </div>
                  <div class="form_sep">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_core" class="is_core"  id="is_core" value="<?php echo $query->is_core;?>" <?php echo ($query->is_core=="1"?"checked":"")?> >Is Core
                            </label><label>&nbsp;</label>
                            <label>
                                <input type="checkbox" name="is_skill" class="is_skill"  id="is_skill" value="<?php echo $query->is_skill;?>" <?php echo ($query->is_skill=="1"?"checked":"")?> >Is Skill
                            </label><label>&nbsp;</label>
                            <label>
                                <input type="checkbox" name="is_assessment" class="is_assessment"  value="<?php echo $query->is_assessment?>" <?php echo ($query->is_assessment=="1"?"checked":"")?> id="is_assessment">Is Assessment
                            </label>
                        </div>
                    </div>             
                </div>
            </div>
            
            <div class="col-sm-6">                          
              <div class="panel-body">
                <div class="form_sep"  style="margin-top:-20px;">
                    <label class="req" for="schlevel_id">School Level<span style="color:red">*</span></label>
                    <select class="form-control"  id="schlevel_id" required name="schlevel_id">
                        <option value=""></option>
                        <?php 
                        $schevels = $this->schlevels->getsch_level();
                        if (isset($schevels) && count($schevels) > 0) {
                            foreach ($schevels as $sclev) {
                                echo '<option value="' . $sclev->schlevelid . '"'.($sclev->schlevelid==$query->schlevelid?"selected":"").'>' . $sclev->sch_level . '</option>';
                            }
                        } ?>
                    </select>
                </div>
                <div class="form_sep">
                  <label class="req" for="student_num">Short Cut<span style="color:red">*</span></label>
                  <input type="text" name="txtshort_sub" id="txtshort_sub" class="form-control" value="<?php echo $query->short_sub?>"  required data-parsley-required-message="Please fill the short cut"/>   
                </div>
                <div class="form_sep">
                    <label class="req" for="orders">Order</label>
                    <input type="text" class="form-control" id="orders" name="orders" value="<?php echo $query->orders?>"/>
                </div>
                <div class="form_sep">
                    <label class="req" for="cboschool">Max Score</label>
                    <input type="text" name="txtmaxscore" id="txtmaxscore" required class="form-control" value="<?php echo $query->max_score?>" data-parsley-required-message="Max score"/>
                </div>
                <div class="form_sep">
                      <label class="req" for="txtcalscore">Calculate Score</label>
                      <input type="text" name="txtcalscore" id="txtcalscore" class="form-control" value="<?php echo ($query->calc_score !=""?$query->calc_score:"0");?>" data-parsley-required-message="Max score"/>
                </div>

                <div class="form_sep hide">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name='is_trimester' id='is_trimester' <?PHP if($query->is_trimester_sub==1) echo 'checked'; ?>> For TAE
                    </label>  
                  </div>                  
                </div> 
                <div class="form_sep hide">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name='is_eval' id='is_eval' <?PHP if($query->is_eval==1) echo 'checked'; ?>>For Evaluate
                    </label>  
                  </div>                  
                </div>              
              </div> 
            </div>  
             
        </div>
        
        <div class="row">
          <div class="col-sm-12">
            
            <div class="panel-body">
                <div class="form_sep">
                <?php if($this->green->gAction("C")){ ?>
                 <input type="button" name="btnsavesubject" id='btnsavesubject' value="Update" class="btn btn-primary" />
                <?php } ?>
                <!-- <input type="reset" name="btnreset" id='btnreset' value="Reset" class="btn btn-warning" /> -->
                <input type="button" name="btncancel" id='btncancel' value="Cancel" class="btn btn-warning" />
                </div>                               
            </div>
            
          </div>
        </div>
        
       </form>         
    </div>
    
 </div>  
</div>

<script type="text/javascript">
     $(function() {
       //--------- parseley validation -----
        $('#fschyear').parsley();       
      
     //---------- end of validation ----

     $("#btncancel").click(function(){
        var r = confirm("Do you want to cancel?");
        if (r == true) {
          location.href=("<?php echo site_url('school/c_subject_gep/?m='.$m.'&p='.$p.'');?>");  
        } else {
          
        }
     }); 
     $('body').delegate('#is_assessment', 'click', function(){            
            if($(this).prop("checked") == false) {
                $(this).val(0);                
            }else{
                $(this).val(1);                
            }
        });
        $('body').delegate('#is_core', 'click', function(){            
            if($(this).prop("checked") == true) {
                $(this).val(1);
            }else{
                $(this).val(0);                
            }
        });
        $('body').delegate('#is_skill', 'click', function(){            
            if($(this).prop("checked") == false) {
                $(this).val(0);
            }else{
                $(this).val(1);                
            }
        });
        $('body').delegate('#btnsavesubject', 'click', function(e){                    
              if(confirm("Do you want to update!")){
                updatesubject();
              }
          });
             
      });

      function updatesubject(){
            var subjectid=$('#txtsubjectid').val();
            var subject=$('#txtsubject').val()
            var subjecttype=$('#cbosubjecttype').val();
            var short_sub=$('#txtshort_sub').val(); 
            var school=$('#cboschool').val();
            var schlevel_id=$('#schlevel_id').val() ;
            var programid = 3;
            var subjectkh=$('#txtsubjectkh').val();
            var max_score=$('#txtmaxscore').val() ;
            var calc_score=$('#txtcalscore').val() ;
            var is_core=$('#is_core').val();
            var is_skill=$('#is_skill').val();
            var is_assessment=$('#is_assessment').val();
            var orders=$('#orders').val();

            if(subject==''){
                toastr["warmning"]("Subject can't bank!");
            }else if (subjecttype==0){ 
                toastr["warmning"]("Please select any subject group!");
            } else if (school==0){
                toastr["warmning"]("Please select any school!");
            }else{
                  $.ajax({
                        url:"<?php echo site_url('school/c_subject_gep/updatesubject');?>",
                        dataType: "Json",
                        type: "POST",
                        async: false,    
                        data: {'subjectid':subjectid, 
                               'subject':subject,
                               'subjecttype':subjecttype,
                               'short_sub':short_sub, 
                               'schoolid':school,
                               'schlevelid':schlevel_id,
                               'programid':programid,
                               'subjectkh':subjectkh,
                               'is_core':is_core, 
                               'is_skill':is_skill, 
                               'is_assessment':is_assessment, 
                               'orders':orders,
                               'max_score':max_score,
                               'calc_score':calc_score
                             },
                        success: function(data){
                        //alert(data);
                        if(data.res =='1'){
                            toastr["success"]('Subject was updated!');
                            location.href=("<?php echo site_url('school/c_subject_gep/?m='.$m.'&p='.$p.''); ?>");
                        }else{
                             toastr["warning"]("This subject type is already exist ! Please try again");
                        }
                    }
                });
            }
        }
</script>