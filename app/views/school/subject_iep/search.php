<style type="text/css">
  table{
   border-collapse: collapse;
   /*border:1px solid #CCCCCC;*/
  }
  td,th{
   
   padding: 5px ;
  }
  #listsubjecttype td,#listsubjecttype th{
   padding: 5px ;
   border:1px solid  #CCCCCC;
  }
  #pgt{
   border:solid 0px !important;
  }
  th{
    background-color: #383547;
    text-align: center;
    color: white;
  }
  a{
    cursor: pointer;
  }
 </style>

<h3 align="center">Subject Type List</h3>
<table border="0" cellpadding="5" cellspacing="0"​ width="970" align="center" id='listsubject'>
    <thead>
        <th align=center width=40>No</th>
        <th width=170>Subject</th>
        <th width=170>Subject type</th>
        <th width=170>Short cut</th>
        <th width=170>School Level</th>
        <th width=170>Grade Level</th>
        <th width=170>Assessment</th>
        <th width=170>Max Score</th>
        <th width=130>Action</th>
    </thead>
    <tr>
        <td></td>
        <td><input type="text" name="txtsearchsubject" id="txtsearchsubject" onkeyup='search(event);' class="form-control" /></td>
        <td><select class="form-control" id='cbosearchsubjecttype' name='cbosearchsubjecttype' onchange='search(event);'>
                <option value=''>Select Subject type</option>
                      <?php
                        foreach ($this->subjects->getsubjecttype() as $subjecttyperow) {
                        echo "<option value='$subjecttyperow->subj_type_id'>$subjecttyperow->subject_type</option>";
                        }
                      ?>
            </select>
        </td>
        <td><input type="text" name="txtsearchshortcut" id="txtsearchshortcut" onkeyup='search(event);' class="form-control" /></td>
        <td>
          <select class="form-control" id='s_schlevelid' name='s_schlevelid' onchange='search(event);'>
                  <option value="">--Select--</option>
                  <?php if (isset($schevels) && count($schevels) > 0) {
                      foreach ($schevels as $sclev) {
                          echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                      }
                    } 
                  ?>
            </select>
        </td>
        <td>
          <select class="form-control" id='s_gradelevelid' name='s_gradelevelid' onchange='search(event);'>
                  <option value=''>--Select--</option>
                  <?php if (isset($schevels) && count($schevels) > 0) {
                      foreach ($schevels as $sclev) {
                          echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                      }
                    } 
                  ?>
            </select>
        </td>
        
         <td>
            <select class="form-control" id='s_is_macro' name='s_is_macro' onchange='search(event);'>
                  <option value=''>--Select--</option>
                  <option value='1'>Yes</option>
                  <option value='0'>No</option>
            </select>                
        </td>         
        <td>
          <select class="form-control" id='s_is_assessment' name='s_is_assessment' onchange='search(event);'>
                  <option value=''>--Select--</option>
                  <option value='1'>Yes</option>
                  <option value='0'>No</option>
            </select> 
        </td>
        <td>
          <select class="form-control" id='s_examtype' name='s_examtype' onchange='search(event);'>
                  <option value=''>--Select--</option>
                  <?php $examtype = $this->extype->examtype();
                      if(count($examtype)>0){
                      foreach ($examtype as $row_examtype){
                    echo '<option value="'.$row_examtype->examtypeid.'">'.$row_examtype->exam_test.'</optoin>';
                      }
                    }
                  ?>
            </select> 
        </td>
        <td></td>
    </tr>
    <tbody id='bodylist'>
    <?php
        $i=1;
    $getGride = $this->subjects->getpagination();
    if(count($getGride)>0){
      $arrSubjid = array();
      $arrGrade = array();
      foreach($getGride as $sub_row){
        $trim='';
        $is_eval='';
        $ed_subtype = "<select style='display:none;' class='form-control' id='ed_subjecttype' name='ed_subjecttype' class='ed_subjecttype' min=1 data-parsley-required-message='Select any subject type'>";                              
                          foreach ($this->subjects->getsubjecttype() as $subjecttyperow) {
                              $ed_subtype .="<option value='".$subjecttyperow->subj_type_id."'". ($subjecttyperow->subj_type_id==$sub_row->subj_type_id?"selected":"").">".$subjecttyperow->subject_type."</option>";
                          }
            $ed_subtype .="</select>";
        //$edit ="<a class='update_row'  href='".site_url('school/subject/editsubject/'.$sub_row->subjectid)."?m=$m&p=$p'><img src='".site_url('../assets/images/icons/edit.png')."' /></a>";
        $edit ="<a class='update_row' onclick='editesubject(event);' at_upate='".$sub_row->subj_grad_id."'><img src='".site_url('../assets/images/icons/edit.png')."' /></a>";

        $del =" | <a class='del_row' href='JavaScript:void(0);' rel='$sub_row->subj_grad_id'><img src='".site_url('../assets/images/icons/delete.png')."' /></a>";

        echo "
        
          <tr>
            <td align=center width=40>". (!in_array($sub_row->subjectid,$arrSubjid)?$i:'') ."</td>
            <td width=170>
              <input type='text' name='ed_subject' style='display:none;' value='".$sub_row->subject."' class='ed_subject'>
              <lable class='hid_edite'>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->subject:'') ."</label>
            </td>
            <td width=170>
              ".$ed_subtype."
              <lable class='hid_edite'>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->subject_type:'') ."</label>
            </td>
            <td>
              <input type='text' name='ed_sub_shoort' style='display:none;' value='".$sub_row->subject."' class='ed_sub_shoort'>
               <lable class='hid_edite'>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->short_sub:'') ."</label>
            </td>
            <td>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->sch_level:'') ."</td>
            <td>              
              <lable class='hid_edite'>".($sub_row->is_assessment==1?"Yes":"No")."</label>
            </td> 
            <td>              
              <lable class='hid_edite'>".($sub_row->max_score!=""?$sub_row->max_score:"0")."</label>
            </td>
            <td align=center width=130>".$edit.$del."</td>
          </tr>" ;
        
          
          $i++;
         
        $arrSubjid[] = $sub_row->subjectid;
      }
    }else{
      echo '<tr>
              <td colspan="11" align="center">
                  <h4><i>No result</i></h4>
              </td>
            </tr>';
    }
    ?>
    <tr>
      <td colspan='11' id='pgt'>
        <ul class='pagination'>
          <?php echo $this->pagination->create_links();?>
        </ul>
      </td>
    </tr>
   </tbody> 
</table>

<script type="text/javascript">
    $(document).ready(function(e) {
       $("body").delegate("#s_schlevelid", "change", function () {
           sgetGradeLevels($(this).val());
        }); 
        $('body').delegate('.del_row','click',function(){
            var obj = $(this);
            var subjectid=$(this).attr("rel");
            deletesubject(obj,subjectid);
        });    
    });
    function editesubject(event){
          var r = confirm("Are you sure to delete this item?");
          if (r == true) {
              var id=jQuery(event.target).attr("at_upate");
               //school/subject is call controller
              //location.href="<?PHP //echo site_url('school/subject/deletesubject');?>/"+id+"?<?php echo "m=$m&p=$p" ?>";
           } else {
               txt = "You pressed Cancel!";
           }      
    }
    
    function deletesubject(obj,subjectid){
          var r = confirm("Are you sure to delete this item?");  
          var tr= obj.closest("tr");                
          if (r == true) {              
               $.ajax({
                    url: "<?php echo site_url('school/c_subject_iep/deletesubject');?>",
                    dataType: "Json",
                    type: "POST",
                    async: false,
                    data: {
                       'subjectid': subjectid
                    },
                    success: function (data) { 
                        if(data==1){
                            toastr["success"]("Subject has been deleted !");
                            tr.remove();
                        }else{
                            toastr["warmning"]("Subject can't delete !");
                        } 
                    }
                });   
           }else{
              return false;
           }
            
    }
    function search(event){

        var subject=$('#txtsearchsubject').val();
        var subjecttype=$('#cbosearchsubjecttype').val();
        var shortcut=$('#txtsearchshortcut').val();
        var schlevelid=$('#s_schlevelid').val();
        var gradelevelid=$('#s_gradelevelid').val();
        var is_macro=$('#s_is_macro').val();
        var is_assessment=$('#s_is_assessment').val();
        var examtype=$('#s_examtype').val();
        $.ajax({
           url:"<?php echo base_url(); ?>index.php/school/c_subject_iep/search",    
           data: {
                  'subject':subject,
                  'subjecttype':subjecttype,
                  'shortcut':shortcut,
                  'schlevelid':schlevelid,
                  'gradelevelid':gradelevelid,
                  'is_macro':is_macro,
                  'is_assessment':is_assessment,
                  'examtype':examtype
                },
           type: "POST",
           success: function(data){
              //alert(data);
              jQuery('#bodylist').html(data);                   
           }
         });
     }
    function sgetGradeLevels(schlevelid) {
        $("#tbody_gradlevel").html("");
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo base_url(); ?>index.php/school/c_subject_iep/sgetgradlevels",
                data: {'schlevelid':schlevelid},
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (data) {                    
                    $("#s_gradelevelid").html(data);
                }
            })
        }
    } 
</script>