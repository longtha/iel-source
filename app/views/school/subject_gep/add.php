<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
        <div id="main_content">

            <div class="result_info">
                <div class="col-sm-6">
                    <strong>Setup Subject</strong>
                </div>
                <div class="col-sm-6" style="text-align: right">
                    <!-- Block message -->
                    <?php if (isset($exist)) echo $exist ?>
                    <?PHP if (isset($_GET['save'])) {
                        echo "<p>Your data has been saved successfully...!</p>";
                    } else if (isset($_GET['edit'])) {
                        echo "<p>Your data has been updated successfully...!</p>";
                    } else if (isset($_GET['delete'])) {
                        echo "<p style='color:red'>Your data has been deleted successfully...!</p>";
                    }
                    $m = '';
                    $p = '';
                    if (isset($_GET['m'])) {
                        $m = $_GET['m'];
                    }
                    if (isset($_GET['p'])) {
                        $p = $_GET['p'];
                    }
                    ?>
                    <!-- End block message -->
                    <span class="res_sms"></span>
                </div>
            </div>
            <form method="post" accept-charset="utf-8" action="#" class="tdrow" id="fsubject">
                    <div class="col-sm-6">
                        <div class="panel-body">
                            <div class="form_sep">
                                <label class="req" for="classid">School<span style="color:red">*</span></label>
                                <select class="form-control" id='cboschool' name='cboschool' min=1 required
                                        data-parsley-required-message="Select any school">
                                    <option value=''>Select School</option>
                                    <?php foreach ($this->subjects->getschool() as $schoolrow) { ?>
                                        <option
                                            value='<?php echo $schoolrow->schoolid; ?>' <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel-body">
                            <div class="form_sep">
                                <label class="req" for="schlevel_id">School Level<span style="color:red">*</span></label>
                                <select class="form-control"  id="schlevel_id" required name="schlevel_id">
                                    <option value=""></option>
                                    <?php if (isset($schevels) && count($schevels) > 0) {
                                        foreach ($schevels as $sclev) {
                                            echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                                        }
                                    } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel-body">
                            <div class="form_sep">
                                <label class="req" for="cboschool">Subject Group<span style="color:red">*</span></label>
                                <select class="form-control" id='cbosubjecttypeid' name='cbosubjecttypeid' min=1
                                        required data-parsley-required-message="Select any subject group">
                                    <option value=''></option>
                                    <?php
                                    foreach ($this->subjects->getsubjecttype() as $subjecttyperow) {
                                        echo "<option value='$subjecttyperow->subj_type_id'>$subjecttyperow->subject_type</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="cboschool">Subject<span style="color:red">*</span></label>
                                <input type="text" name="txtsubject" id="txtsubject" class="form-control" required
                                       data-parsley-required-message="Please fill the subject"/>
                                <input type="hidden" id="subjectid"/>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="cboschool">Subject (KH)</label>
                                <input type="text" name="txtsubjectkh" id="txtsubjectkh" class="form-control" data-parsley-required-message="Please fill the subject"/>
                            </div>                            
                            <div class="form_sep hide">
                                <label class="req" for="coefficient">Coefficient</label>
                                <input type="text" class="form-control" id="coefficient" name="coefficient"/>
                                <label>
                                        <input type="checkbox" name="is_trimester" class="is_trimester"  id="is_trimester" value="0" >Is Core
                                </label>
                            </div>                             
                            <div class="form_sep">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="is_core" class="is_core"  id="is_core" value="1" checked>Is Core
                                    </label><label>&nbsp;</label>
                                    <label>
                                        <input type="checkbox" name="is_skill" class="is_skill"  id="is_skill" value="1" checked>Is Skill
                                    </label><label>&nbsp;</label>
                                    <label>
                                        <input type="checkbox" name="is_assessment" class="is_assessment"  value="1" id="is_assessment" checked>Is Assessment
                                    </label>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel-body">
                            <div class="form_sep hide">
                                <label class="req" for="yearid">Academic Year<span style="color:red">*</span></label>
                                <select class="form-control" id="yearid" name="yearid">
                                    <option value=""></option>
                                    <?php if (isset($schyears) && count($schyears) > 0) {
                                        foreach ($schyears as $schyear) {
                                            echo '<option value="' . $schyear->yearid . '" ' . ($this->session->userdata('year') == $schyear->yearid ? "selected=selected" : "") . ' >' . $schyear->sch_year . '</option>';
                                        }
                                    } ?>
                                </select>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="student_num">Shortcut<span style="color:red">*</span></label>
                                <input type="text" name="txtshort_sub" id="txtshort_sub" class="form-control" required
                                       data-parsley-required-message="Please fill the short cut"/>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="orders">Order</label>
                                <input type="text" class="form-control" id="orders" name="orders"/>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="cboschool">Max Score</label>
                                <input type="number" name="txtmaxscore" required id="txtmaxscore" class="form-control" data-parsley-required-message="Max score"/>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="txtcalscore">Calculate Score</label>
                                <input type="number" name="txtcalscore" id="txtcalscore" class="form-control" data-parsley-required-message="Max score"/>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-12 hide">                        
                        <div class="panel-body" style="">
                            <div class="form_sep">
                            <div class="table-responsive">
                                <table  class="table table-bordered" id="tbl_gradlevel">
                                    <thead>
                                        <tr>
                                            <th style="text-align:center">Grade Level<br><input type="checkbox" id="all_is_grade" class="all_is_grade" checked="checked"></th>
                                            <th style="text-align:center">Is Skill<br><input type="checkbox" id="all_is_skill" class="all_is_skill" checked="checked"></th>
                                            <th style="text-align:center">Is Core<br><input type="checkbox" id="all_is_core" class="all_is_core" checked="checked"></th>
                                            <th style="text-align:center">Is Assessment<br><input type="checkbox" id="all_is_assessment" class="all_is_assessment" checked="checked"></th>
                                            <th style="text-align:center">Examination Type</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_gradlevel">         
                                    </tbody>                                    
                                </table>
                            </div>
                            </div>
                        </div>                        
                    </div>
                               
                <div class="col-sm-12">

                    <div class="panel-body">
                        <div class="form_sep">
                            <?php if ($this->green->gAction("C")) { ?>
                                <input type="button" name="btnsavesubject" id='btnsavesubject' value="Save"
                                       class="btn btn-primary"/>
                            <?php } ?>
                            <input type="button" name="btncancel" id='btncancel' value="Cancel"
                                   class="btn btn-warning"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<style>
    .tdrow tr, .tdrow th, .tdrow td {
        border: none !important;
    }

    .res_sms {
        font-size: 16px;
        font-family: "Comic Sans MS", "Times New Roman", Arial;
        color: #c12e2a;
    }

</style>

<script type="text/javascript">

    $(function () {
        //--------- parseley validation -----
        $('#fsubject').parsley();

        //---------- end of validation ----

        $("#btncancel").click(function () {
            var r = confirm("Do you want to cancel?");
            if (r == true) {
                location.href = ("<?php echo site_url('school/c_subject_gep/?m='.$m.'&p='.$p.'');?>");
            }
        });
       
        $('body').delegate('#is_assessment', 'click', function(){            
            if($(this).prop("checked") == false) {
                $(this).val(0);                
            }else{
                $(this).val(1);                
            }
        });
        $('body').delegate('#is_core', 'click', function(){            
            if($(this).prop("checked") == true) {
                $(this).val(1);
            }else{
                $(this).val(0);                
            }
        });
        $('body').delegate('#is_skill', 'click', function(){            
            if($(this).prop("checked") == false) {
                $(this).val(0);
            }else{
                $(this).val(1);                
            }
        });
        //========== ajax save with submit form ===========

        //$("#btnsavesubject").click(function () {
        //    $('#fsubject').submit();
        //});
        $('body').delegate('#btnsavesubject', 'click', function(e){
            e.preventDefault();
            if($('#fsubject').parsley().validate()){
                var j= 0;
                var n= 0;
               
                    if(confirm("Do you want to save !")){
                        var subjectid = $('#subjectid').val();
                        var subject = $('#txtsubject').val();
                        var subject_kh = $('#txtsubjectkh').val();
                        var subjecttypeid = $('#cbosubjecttypeid').val();
                        var short_sub = $('#txtshort_sub').val();
                        var schlevelid = $('#schlevel_id').val();
                        var schoolid = $('#cboschool').val();                        
                        var is_core = $('#is_core').val();
                        var is_skill = $('#is_skill').val();
                        var is_assessment = $('#is_assessment').val();
                        var max_score = $('#txtmaxscore').val();
                        var calc_score = $('#txtcalscore').val();
                        var programid = 3;

                        var is_trimester = 0;
                        var is_eval = 0;
                        var orders = $('#orders').val();
                        if ($('#is_trimester').prop("checked") == true) {
                            is_trimester = 1;
                        }
                        if ($('#is_eval').prop("checked") == true) {
                            is_eval = 1;
                        }
                        $.ajax({
                            url: "<?php echo site_url('school/c_subject_gep/save');?>",
                            dataType: "Json",
                            type: "POST",
                            async: false,
                            data: {
                                'subjectid': subjectid,
                                'subject': subject,
                                'subject_kh': subject_kh,
                                'subjecttypeid': subjecttypeid,
                                'short_sub': short_sub,
                                'schlevelid': schlevelid,
                                'schoolid': schoolid,
                                'programid': programid,
                                'is_trimester': is_trimester,
                                'is_eval': is_eval,
                                'orders': orders,
                                'is_core': is_core,
                                'is_skill': is_skill,
                                'is_assessment': is_assessment,
                                'max_score':max_score,
                                'calc_score':calc_score
                            },
                            success: function (data) {
                                $("#fsubject").clearForm();
                                if (subjectid == "") {
                                    $('#txtsearchsubject').val(subject);
                                }
                                
                                if (data.res == 1) {
                                    toastr["success"]('Subject was save!');
                                    location.href=("<?php echo site_url('school/c_subject_gep/?m='.$m.'&p='.$p.''); ?>"); 
                                }else{
                                    toastr["warning"]("Subject is already existed !");
                                }
                                search(e);
                            }
                        });
                }else{          
                        return false;
                }
                
            }
        });
        //========= end of ajax save ======================
        $("#cbosubjecttypeid").on("change", function () {
            if ($(this).val() != "") {
                getOrders($(this).val());
            }
        });
        
        $("body").delegate("#schlevel_id", "change", function () {   
            var myval = $(this).val();     
            //getGradeLevels(myval);
           // getSchYear(myval);
            $(this).val(myval);
        });
    });
    function getOrders(subtypeid) {
        $.ajax({
            url: "<?php echo site_url('school/c_subject_gep/orders');?>",
            dataType: "Json",
            type: "POST",
            async: false,
            data: {
                'subtypeid': subtypeid
            },
            success: function (data) {
                $("#orders").val(data);
            }
        })
    }
    function getGradeLevels(schlevelid) {
        $("#tbody_gradlevel").html("");
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo base_url(); ?>index.php/school/c_subject_gep/getgradlevels",
                data: {'schlevelid':schlevelid},
                type: "POST",
                dataType: 'json',
                async: false,
                success: function (data) {                    
                    $("#tbody_gradlevel").html(data);
                }
            })
        }
    }
    function getSchYear(schlevelid) {

        $("#yearid").html("");
        if (schlevelid != "") {
            $.ajax({
                url: "<?php echo site_url('school/academic_year/get_year'); ?>",
                type: "POST",
                dataType: 'Json',
                async: false,
                data:{
                    schlevelid:schlevelid
                },
                success: function (res) {
                    var data = res.year;
                    var tr = "";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var row = data[i];
                            tr += '<option value="'+row.yearid+'">'+row.sch_year+'</option>';
                        }
                    }
                    $("#yearid").html(tr);
                }
            })
        }
    }
    function checkAll(m_selector, sub_selector, add_class){        
        if(m_selector.is(':checked')==true){
            sub_selector.each(function(){
                $(this).addClass(add_class);
                $(this).prop('checked', true);
                $(this).val(1);
            });
        }else{
            sub_selector.removeClass(add_class);
            sub_selector.prop('checked', false);
            sub_selector.val(0);
        }
    }

    //function check one
    function checkOne(m_selector, sub_selector, count_check, add_class){
        var addClass = "."+add_class;
        var count_all = count_check.size();
        if(sub_selector.is(':checked')==true){
            sub_selector.addClass(add_class);               
            sub_selector.prop('checked', true);
            sub_selector.val(1);

            //var counts = $('.seldom:checked').size();
            var counts = $(addClass).size();
            if(count_all == counts){
                m_selector.prop('checked', true);
            }
        }else{
            m_selector.prop('checked', false);
            sub_selector.removeClass(add_class);
            sub_selector.prop('checked', false);
            sub_selector.val(0);
        }

    }
</script>