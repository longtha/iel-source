<style>
    .gradecls {
        margin-top: 20px;
    }

    .gradecls td {
        padding: 0 10px;
        width: 40px;

    }

    .gradecls ul {
        display: none !important;
    }

    .ex {
        padding: 3px !important;
        vertical-align: center !important;
    }

    .tdrow tr, .tdrow th, .tdrow td {
        border: none !important;
    }

    .exlabel {
        width: 20px;
    }
</style>

<div class="wrapper">
    <div class="clearfix" id="main_content_outer">
        <div id="main_content">

            <div class="result_info">
                <div class="col-sm-6">
                    <strong>Setup Class</strong>
                </div>
                <div class="col-sm-6" style="text-align: right">

                    <?php if (isset($error)) echo $error ?>

                    <!-- Block message -->
                    <?PHP if (isset($_GET['save'])) {
                        echo "<p id='exist'>Your data has been saved successfully...!</p>";
                    } else if (isset($_GET['edit'])) {
                        echo "<p id='exist'>Your data has been updated successfully...!</p>";
                    } else if (isset($_GET['delete'])) {
                        echo "<p style='color:red'>Your data has been deleted successfully...!</p>";
                    }
                    echo "<p id='exist'></p>";
                    ?>
                </div>
            </div>
            <?php
            $m = '';
            $p = '';
            if (isset($_GET['m'])) {
                $m = $_GET['m'];
            }
            if (isset($_GET['p'])) {
                $p = $_GET['p'];
            }
            ?>
            <form method="post" accept-charset="utf-8"
                  action="<?php echo site_url('school/classes/saves?m=' . $m . '&p=' . $p . ''); ?>" class="tdrow"
                  id="fschyear">


                    <div class="col-sm-6">
                        <div class="panel-body">
                            <!-- <div class="form_sep">
                              <label class="req" for="cboschool">Class name<span style="color:red">*</span></label>
                              <input type="text" name="txtglabel" id="txtglabel" class="form-control" required data-parsley-required-message="Enter grade level" />
                            </div> -->
                            <div class="form_sep">
                                <label class="req" for="cboschool">School<span style="color:red">*</span></label>
                                <select class="form-control" id='cboschool' name='cboschool' min='1' required
                                        data-parsley-required-message="Select any school">
                                    <option value=''>Select School</option>
                                    <?php foreach ($this->getclassmode->getschool() as $schoolrow) { ?>
                                        <option
                                            value="<?php echo $schoolrow->schoolid; ?>" <?php if ($schoolrow->schoolid == $this->session->userdata('schoolid')) echo "selected"; ?> > <?php echo $schoolrow->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="sclevelid">School Level<span style="color:red">*</span></label>
                                <select class="form-control" id="sclevelid" required name="sclevelid">
                                    <option value=""></option>
                                    <?php if (isset($schevels) && count($schevels) > 0) {
                                        foreach ($schevels as $sclev) {
                                            echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                                        }
                                    } ?>
                                </select>
                            </div>
                            <div class="form_sep">
                                <label class="req" for="cboschool">Grade Label<span style="color:red">*</span></label>
                                <select class="form-control" id='cbogradelabel' name='cbogradelabel' min=1 required
                                        data-parsley-required-message="Select any grade label">
                                    <option value=''>Select Label</option>
                                    <?php
                                    foreach ($this->getclassmode->getgradelabel() as $row) {
                                        echo "<option value='$row->grade_labelid' rel='$row->grade_label' id='opt'>$row->grade_label</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        <div class="form_sep">
                            <label class="req" for="cboschool">Class Type</label>
                            <select class="form-control is_pt" id='is_pt' name='is_pt'>
                                    <option value="0">Full Time</option>
                                    <option value="1">Part Time</option>
                                    
                            </select>
                        </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="panel-body" style="padding-left:0;">

                            <div class="form_sep">
                                <div class='table-responsive' style='margin-top:-15px;'>
                                    <table class="classname">
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="form_sep">
                                <div class='table-responsive' style='margin-top:0px; padding-left: 20px;'>
                                    <table class="gradecls">
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>



                    <div class="col-sm-12">

                        <div class="panel-body">
                            <div class="form_sep">
                                <?php if ($this->green->gAction("C")) { ?>
                                    <input type="submit" name="btnsave" id='btnsave' value="Save"
                                           class="btn btn-primary"/>
                                <?php } ?>

                                <input type="button" name="btncancel" id='btncancel' value="Cancel"
                                       class="btn btn-warning"/>
                            </div>
                        </div>

                    </div>


            </form>
        </div>

    </div>
</div>

<script type="text/javascript">

    $(function () {
        $('#fschyear').parsley();

        $("#btncancel").click(function () {
            var r = confirm("Do you want to cancel?");
            if (r == true) {
                location.href = ("<?php echo site_url('school/classes/?m='.$m.'&p='.$p.'');?>");
            } else {

            }
        });

    //----- Checkbox pass value to textbox ----
        $("body").delegate(".gradecls :checkbox","click",function(){
            var id = $(this).attr('rel');
            var grade_level=$(this).attr('grade_level');
            var glabel = $("#sp_label").text();

            if ($('.chk' + id).is(':checked')) {
                $("#ch" + id).val(grade_level + glabel);
            } else {
                $("#ch" + id).val("");
            }
        });

    //---- End Checkbox ------------------------

    //----- Select label to display for label textbox ----
        $("#cbogradelabel").change(function () {

            var sbox = $("#cbogradelabel option:selected").text();
            var grade_labelid = $(this).val();

            $(".gradecls :text").val("");
            $(".gradecls :checkbox").attr("checked", false);
            if (sbox != "Select Label") {
                $("#sp_label").text(sbox);
                $("#l_label").val(sbox);
                $("#grade_labelid").val(grade_labelid);

                $(".gradecls :checkbox").removeAttr('disabled');

            } else {
                $("#sp_label").text("");
                $(".gradecls :checkbox").attr('disabled', 'disabled');
            }

        });
    //----- End select -----------------------------------
        $("#sclevelid").change(function(){
            levelbyschlevelid($(this).val());
            labelbyschlevelid($(this).val());
        });
    });
    function levelbyschlevelid(schlevelid){
        $.ajax({
            url:"<?php echo site_url('school/gradelevel/getlevels')?>/"+schlevelid,
            type:"POST",
            dataType:"Json",
            data:{
                getlevel:1
            },
            success:function(res){
                $(".gradecls tbody").html("");
                $(".classname tbody").html("");

                //alert(res.length)
				
                if(res.length>0){
                    var th_level="<tr>";
                    var tr_level="<tr>";
                    var tr_class_box="<tr><td class='ex exlabel'><input name='l_label' type='text' class='hide' id='l_label' />";
                        tr_class_box+="<input type='hidden' value='' name='grade_labelid' id='grade_labelid' />";
                        tr_class_box+="<span id='sp_label'></span></td>";
                    for(var j=0;j<res.length;j++){
                        var row=res[j];
                        var grade_levelid=row.grade_levelid;
                        th_level+='<td>'+row.grade_level+'</td>';
                        tr_level+='<td><input type="checkbox" rel="'+grade_levelid+'" id="chk" name="chk'+grade_levelid+'" class="chk'+grade_levelid+'" value="'+grade_levelid+'"?? grade_level="'+row.grade_level+'"/></td>';
                        tr_class_box+='<td class="ex"><input type="text" id="ch'+grade_levelid+'" class="ex" name="glabel[]" style="width:32px"/>';
                        tr_class_box+='<input type="hidden" value="'+grade_levelid+'" name="grade_levelid[]" />';
                        tr_class_box+='</td>';
                    }
                    th_level+='</tr>';
                    tr_level+='</tr>';
                    tr_class_box+='</tr>';
                    //alert(th_level+tr_level);

                    $(".gradecls tbody").html(th_level+tr_level);
                    $(".classname tbody").html(tr_class_box);
                }

            }
        })

    }
    function labelbyschlevelid(schlevelid){
        $.ajax({
            url:"<?php echo site_url('school/gradelabel/getlabels')?>/"+schlevelid,
            type:"POST",
            dataType:"Json",
            data:{
                getlabel:1
            },
            success:function(res){
                //alert(res.length)
                if(res.length>0){
                    this.op_label="<option value=''></option>";
                    for(var j=0;j<res.length;j++){
                        var row=res[j];
                        var grade_labelid=row.grade_labelid;
                        this.op_label+='<option value="'+grade_labelid+'">'+row.grade_label+'</option>';
                    }
                    $("#cbogradelabel").html(this.op_label);
                }

            }
        })

    }
</script>