<style type="text/css"> 
 
  a{
    cursor: pointer;
  }
 table tbody tr td img{
 	width: 20px;	
 }
 table th{
 	font-weight: normal;
 }
 </style>
 <?php
	$m='';
	$p='';
	if(isset($_GET['m'])){
	    $m=$_GET['m'];
	}
	if(isset($_GET['p'])){
	    $p=$_GET['p'];
}
?>

 <div class="col-sm-12">    
 	<div class="panel panel-default">
 		  	
	  <div class="panel-body">	          		 
		<div class="table-responsive">

			<table border="0"​ align="center" id='listsubject' class="table">
			    <thead>
			    	<th>School</th>
                    <th>Study Program</th>
                    <th>School Level</th>
                    <th>Class</th>
                     <th colspan="2">
			        <!------- Button Preview ------>
			        <?php if($this->green->gAction("P")){ ?>
		    	 	<a class="hide">
		    	 		<img onclick='preview(event);' style='margin-right:40px' align='right' src="<?php echo base_url('assets/images/icons/preview.png')?>" />
		    	 	</a>
		    	 	<?php } ?>
		    	 	<!------ End button ------>
		    	 	</th>
			    </thead>
				<thead>
                    <th colspan="2"></th>
                    <th>


                        <div class="col-xs-10" style=""><select class="form-control " id="s_sclevelid" name="s_sclevelid" style="width:100px;">
                                <option value=""></option>
                                <?php if (isset($schevels) && count($schevels) > 0) {
                                    foreach ($schevels as $sclev) {
                                        echo '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
                                    }
                                } ?>
                            </select></div>


                    </th>
				</thead>
				
				<tbody id='bodylist'>


			</tbody>
            <!---- Start pagination ---->
            <tfoot>
                <tr>
                    <td colspan='5' id='pgt'>
                        <ul class='pagination'>

                        </ul>
                    </td>
                </tr>
            </tfoot>
            <!---- End Pagination ---->
		</table>
			
		</div>
  	 </div>
  	</div>
  	
  </div>

<script type="text/javascript">
    search();
    function deleteclass(event){  
           var r = confirm("Are you sure to delete this item?");
           var schoolid="<?php echo $this->session->userdata('schoolid') ?>";
	       var glabelid=$(event.target).attr('rel');

           if (r == true) {
               var id=jQuery(event.target).attr("rel");
              location.href="<?PHP echo site_url('school/classes/deleteclass/');?>/"+glabelid+"/"+schoolid+"?<?php echo "m=$m&p=$p" ?>";
           } else {
               txt = "You pressed Cancel!";
           }             
    }
    function preview(event){
    	var year=jQuery('#year').val();
    	//location.href="<?PHP echo site_url('school/tearcherclass/preview/');?>/0/"+year;
    	window.open("<?php echo site_url('school/classes/preview');?>/0/"+year,"_blank");
    }

    function search(startpage){
        var s_sclevelid=$('#s_sclevelid').val();

        var roleid=<?php echo $this->session->userdata('roleid');?>;
		var m=<?php echo $m;?>;
		var p=<?php echo $p;?>;

        $.ajax({
           url:"<?php echo site_url('school/classes/search'); ?>",
           data: {
                  's_sclevelid':s_sclevelid,
                  'roleid':roleid,
				  'm':m,
			      'p':p,
                    'startpage':startpage
                 },
           type: "POST",
           success: function(res){

               var data=res.datas;
               var paging=res.paging;
               var tr="";
               if(data.length>0){
                   var arrschool=[];
                   var arrprogram=[];
                   var arrlevel=[];

                   for(var i=0;i<data.length;i++){
                       var row=data[i];
                       var name="";
                       var program="";
                       var sch_level="";

                        if($.inArray(row.schoolid,arrschool)=='-1'){
                            name=row.name;
                        }
                       if($.inArray(row.programid,arrprogram)=='-1'){
                           program=row.program;
                       }
                       if($.inArray(row.schlevelid,arrlevel)=='-1'){
                           sch_level=row.sch_level;
                       }
					   
					 
					  
                       tr+='<tr>'+
                           '<td>'+name+'</td>'+
                           '<td>'+program+'</td>'+
                           '<td>'+sch_level+'</td>'+
						   
                           '<td><span class="lblclassname">'+row.class_name+'</span>'+
                           '<input type="text" class="h_classname form-control" value="'+row.class_name+'" style="display:none;width:120px;" /> </td>'+
						   
						   '<td width="40" class="idhtml_pt1" style="display:none1;width:120px;">'+((row.is_pt==1)?'Part Time':'<span style="color:blue">Full Time</span>')+'</td>'+						  
                           '<td width="40" class="idhtml_pt" style="display:none-;width:120px;"><select class="idseis_pt" id="idseis_pt"><option '+(row.is_pt==0?'selected="selected"':"")+' value="0">Full Time</option><option '+(row.is_pt==1?'selected="selected"':"")+' value="1">Part Time</option></select></td>'+						  
                           
						   '<td width="40"><a href="javascript:void(0)" class="link_save" rel="'+row.classid+'" style="display:none">'+
                           '<td width="40"><a href="javascript:void(0)" class="link_save" rel="'+row.classid+'" style="display:none">'+
                           '<img src="<?php echo base_url('assets/images/icons/save.png') ?>" ></a>'+
                           '<a href="javascript:void(0)" class="link_edit" rel="'+row.classid+'">'+
                           '<img src="<?php echo base_url('assets/images/icons/edit.png') ?>"></a></td>'+
                           
						   '<td width="40"><a href="javascript:void(0)" class="link_delete" rel="'+row.classid+'">'+
						   
                           '<img src="<?php echo base_url('assets/images/icons/delete.png') ?>"></a></td>'+
                           '</tr>';

                       arrschool[i]=row.schoolid;
                       arrprogram[i]=row.programid;
                       arrlevel[i]=row.schlevelid;
                   }

               }

              $('#bodylist').html(tr);
              $('.pagination').html(paging.pagination);
           }
         });
     }
    function updateclass(classid,classname,idseis_pt)
    {
        if(classid!="") {
            $.ajax({
                url: "<?php echo site_url('school/classes/updateclass'); ?>/" + classid,
                type: "POST",
                dataType: 'json',
                data: {classname:classname,idseis_pt:idseis_pt},
                async: false,
                success: function (res) {
                    if(res.up==1){
                        alert("Class was updated.");
                    }else{
                        alert("Class not update.");
                    }
                }
            })
        }
    }
    $(function(){
        $("body").delegate(".pagenav","click",function(){
            var page=$(this).attr("id");
            search(page);
        })
        $("body").delegate("#s_sclevelid","change",function(){
            search();
        })
        $("body").delegate(".link_delete","click",function(){
            if(window.confirm("Sure! Do you want to delete? ")){
                var classid=$(this).attr("rel");

                $.ajax({
                    url:"<?php echo site_url('school/classes/deleteclass'); ?>/"+classid,
                    type: "POST",
                    dataType:'json',
                    async:false,
                    success: function(res){
                        if(res.del==9){
                            alert("Class was deleted.");
                            search(0);
                        }else if(res.del==1){
                            alert("Class was add to student.Can't delete.");
                        }else{
                            alert("Can't delete class.");
                        }
                    }
                })
            }
        })
        $("body").delegate(".link_edit","click",function(){
            var tr=$(this).closest("tr");
            tr.find(".link_save").show();
            tr.find(".h_classname").show()
            tr.find(".lblclassname").hide();
            $(this).hide();
        });
        $("body").delegate(".link_save","click",function(){
			// alert(11);
            var tr=$(this).closest("tr");
            tr.find(".link_save").hide();
            tr.find(".h_classname").hide()
            tr.find(".lblclassname").show();
            tr.find(".link_edit").show();
            $(this).hide();
            updateclass($(this).attr("rel"),tr.find(".h_classname").val(),tr.find(".idseis_pt").val());
            search(0);
        });
    })
</script>