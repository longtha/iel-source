<style type="text/css">
    table tbody tr td img{width: 20px; margin-right: 10px}
    a,.sort,.panel-heading span{cursor: pointer;}
    .panel-heading span{margin-left: 10px;}
    .cur_sort_up{
        background-image: url('<?php echo base_url('assets/images/icons/sort-up.png')?>');
        background-position: left;
        background-repeat: no-repeat;
        padding-left: 15px !important;
    }
    .cur_sort_down{
        background-image: url('<?php echo base_url('assets/images/icons/sort-down.png')?>');
        background-position: left;
        background-repeat: no-repeat;
        padding-left: 15px !important;
    }
    .ui-autocomplete{z-index: 9999;}

    .panel-body{
        /*height: 650px;*/
        max-height: calc(100vh - 200px);
        overflow-y: auto;
    }

    .t_border{
        border:none !important;
    }
/*
    input[type=text]{
      border:none !important;
    }*/
    #guardian,#academic,#teacher{
      border: none; 
      border-bottom: 1px dashed black;
      text-align:center;
    }

    #list th {vertical-align: middle;}
    #list td {vertical-align: middle;}
    .hide_controls{display:none;}
</style>
<?php  
    $m='';
    $p='';
    if(isset($_GET['m'])){
        $m=$_GET['m'];
    }
    if(isset($_GET['p'])){
        $p=$_GET['p'];
    }    
?>

<div class="container-fluid" style="width:100% !important;padding-bottom: 20px;">
    <div class="row">
      <div class="col-xs-12">
          <div class="result_info">
              <div class="col-xs-6">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                  <strong>Score List Report</strong>  
              </div>
              <div class="col-xs-6" style="text-align: right">
                  <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                      <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
                  </a>
                  <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                     <img src="<?= base_url('assets/images/icons/print.png') ?>">
                  </a>
                  <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                     <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
                  </a>
              </div>         
          </div>
      </div>
    </div>
<div class="collapse in" id="colapseform">
    <div class="collapse in" id="colapseform">
        <div class="col-sm-12">
          <div class="panel panel-default" style="margin-left: -14px;margin-right: -14px;">
            <div class="panel-body">
              <div class="col-sm-4">
                <div class="form-group">
                <label>Student ID:</label>
                <input type='text' value="" class='form-control input-sm' name='s_student_id' id='s_student_id'/>
              </div>
                <div class="form-group">          
                    <label>Program:</label>
                  <select class="form-control" id="program">
                    <option value=""></option>
                      <?php echo $opt_program;?>
                </select>
              </div>
              <div class="form-group">          
                <label>Grad Level:</label>
                <select class="form-control" id="grad_level"></select>
              </div>
              <label>From Date:</label>
            <div class="input-group input-append date">         
                <input type="text" id="from_date"  value="<?php echo date('d-m-Y');?>" class="form-control">
                <span class="input-group-addon add-on">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>                                                
            </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label>Name(Khmer/English):</label>
                <input type='text' value="" class='form-control input-sm' name='s_full_name' id='s_full_name'/>
              </div>          
              <div class="form-group">          
                <label>School Level:</label>
                  <select class="form-control" id="sch_level"></select>
                </div>
              <div class="form-group">            
                <label>Class:</label>
                <select class="form-control" id="classid"></select>
              </div> 
              <label>To Date:</label>
            <div class="input-group input-append date">         
                <input type="text" id="to_date"  value="<?php echo date('d-m-Y');?>" class="form-control">
                <span class="input-group-addon add-on">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>                                                
            </div>             
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label>Gender:</label>
              <select class="form-control" id="gender">
                  <option value=""></option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
              </select>
              </div> 
              <div class="form-group">          
                <label>Year:</label>
                  <select class="form-control" id="years"></select>
              </div>        
            </div>          
            <div class="col-sm-12" style="padding-top:20px;">
                <div class="form-group">
                    <input type="button" class="btn btn-primary" value="Search" id="search">
                </div>            
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- List Data -->
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="table-responsive">
          <table class="table" border='0'>
            <thead >
              <tr>
                <?php
                    foreach ($thead as $th => $val) {
                        if ($th == 'Action'){
                            echo "<th class='remove_tag'>" . $th . "</th>";
                        }
                        else{
                            echo "<th class='sort $val no_wrap' onclick='sort(event);' rel='$val' sortype='ASC'>" . $th . "</th>";
                        }
                    }
                ?>
              </tr>
            </thead>
            <tbody class='listbody'>

            </tbody>
          </table>
        </div>
      </div>
      <div class="fg-toolbar">
        <div style='margin-top:20px; width:11%; float:left;'>
          Display : <select id='perpage' onchange='show_list(1);' style='padding:5px; margin-right:0px;'>
            <?PHP
                for ($i = 10; $i < 500; $i += 10) {           
                   echo "<option value='$i' selected>$i</option>";                                     
                }
            ?>                        
          </select>
        </div>
          <div class='paginate' style="float:right;"></div>
      </div> 
    </div>
  </div>          
</div>
<div class="modal fade" id="myModal" role="dialog" width="50%" >
  <div class="modal-dialog" style=" padding-top:20px; width: 71%;">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Evaluation for Semester</h4>
      </div>
    <div class="modal-body">
        <div class="wrapper" style="border:0px solid #f00;overflow:auto">
           <div class="col-sm-12"> 
              <div id="tap_print">
                  <div class="col-sm-12 ti_semester">
                      <div class="col-sm-4" style="width:200px !important">
                        <img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="width:200px !important">
                      </div>
                      <div class="col-sm-6 dv_semester">
                        <h4 class="kh_font">របាយការណ៍សិក្សាប្រចាំឆមាសមាស</h4>
                        <h4 style="font-size:23px;">Semester Acadimic Report</h4>           
                        <h4 style="font-size:23px;"><input type="text" id="semester" style="border:none;"></h4>
                      </div>    
                      <input type="hidden" id="studentid">
                      <input type="hidden" id="programid">
                      <input type="hidden" id="schoolid">
                      <input type="hidden" id="slevelid">
                      <input type="hidden" id="yearid">
                      <input type="hidden" id="gradelevelid">
                      <input type="hidden" id="semesterid">
                      <input type="hidden" id="c_id">
                      <input type="hidden" id="semesterid">         
                  </div>    
                  <div class="col-sm-12" style="padding-left:40px; padding-right:40px;">      
                      <table class='table head_name' style=" margin-bottom: 0px !important;">                          
                          <tbody>
                            <tr>
                              <td style="width:10%;">Student's Name:</td>
                              <td style="width:20%"><input type="text" class="t_name t_border" readonly></td>
                              <td style="width:5%">Level:</td>
                              <td style="width:10%"><input type="text" class="t_level t_border" readonly></td>
                              <td style="width:8%">Yearly:</td>
                              <td style="width:20%"><input type="text" class="t_year t_border" readonly></td>
                            </tr>
                            <tr>
                              <td>សិស្សឈ្មោះ</td>
                              <td class="kh_font"><input type="text" class="t_namekh t_border" readonly></td>
                              <td>កម្រិត:</td>
                              <td><input type="text" class="t_levelkh t_border" readonly></td>
                              <td>ឆ្នាំសិក្សា:</td>
                              <td><input type="text" class="t_yearkh t_border" readonly></td>
                            </tr>
                          </tbody>
                      </table>
                  </div>  
                  <div class="col-sm-12" style="text-align:center; padding-bottom:15px">
                    <p style="font-size:13px;">Score and Evaluation of Each Subject / ពិន្ទុ និងការវាយតម្លៃមុខវិជ្ជានីមួយៗ</p>
                  </div>  
                  <div class="col-sm-12">     
                    <div class="form-group">
                      <table class='table table-bordered'>
                        <thead>
                          <tr>
                            <th class="kh_font" colspan="5">Evaluation for Semestern 1 /  ការវាយតម្លៃក្នុងឆមាសទី១</th>
                          </tr>
                          <tr>
                            <th>Work Habits and Social Skills <br> ទម្លាប់ក្នុងការសិក្សា និងជំនាញសង្គម</th>
                            <th>Seldom <br> កម្រ</th>
                            <th> Sometimes <br> ម្ដងម្កាល</th>
                            <th> Usually <br> តែងតែ </th>
                            <th> Consistently <br> ជាប្រចាំ </th>
                          </tr>
                        </thead>
                        <tbody id="list_description">
                                                  
                        </tbody>
                      </table>
                    </div>
                    <div class="form-group">
                       <textarea class="form-control" rows="5" id="t_comment" placeholder="Teacher's Comments / មតិយោបល់របស់គ្រូ"></textarea>            
                    </div>
                    <div class="col-sm-12"​​ style="margin-bottom: 60px; text-align:center;">
                      <div class="col-sm-6" style="margin-top:20px; ">
                        <div class="form-inline"> 
                            <label>Date:</label>        
                            <input type="text" id="academic"  value="<?php echo date('d-m-Y');?>" class="form-control">                                               
                        </div>
                        <label style="margin-right: 35px;">Academic Manager</label>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-inline"> 
                            <label>Date:</label>        
                            <input type="text" id="teacher"  value="<?php echo date('d-m-Y');?>" class="form-control">                                               
                        </div>            
                        <label>Teacher's Signature</label>
                      </div>
                    </div>
                    <div class="form-group" style="margin-bottom:3px !important">
                      <textarea class="form-control" rows="4" id="g_comment" placeholder="Guardian's Comments / មតិមាតាបិតាសិស្ស"></textarea> 
                    </div>
                    <div class="col-sm-12" style="padding-left:0px; ">
                      <div class="col-sm-6" style="padding-bottom: 70px; padding-left:0px;">
                        <p>Please return the book to school before</p>
                        <p>សូមប្រគល់សៀវភៅតាមដាននេះមកសាលាមុនថ្ងៃទី៖</p>          
                        <div class="col-sm-6" style="padding-left:0px;">
                          <input type="text" id="return_date"  value="<?php echo date('d-m-Y');?>" class="form-control" style="text-align:center;">                           
                        </div>
                      </div>
                      <div class="col-sm-6" style="text-align:center;padding:0px;">
                        <div class="form-inline"> 
                          <label>Date:</label>        
                          <input type="text" id="guardian"  value="<?php echo date('d-m-Y');?>" class="form-control">                                               
                        </div>
                        <p>Guardian's Signature / ហត្ថលេខាមាតាមិតាសិស្ស</p>
                      </div>
                    </div>        
                </div>                
            </div>      
        </div>
      </div>
          <div class="modal-footer" style="padding:37px;">                       
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <input type="button" class="btn btn-primary" value="Save" id="save">
          </div>
    </div>          
  </div>
</div>
<script type="text/javascript">
  $( document ).ready(function() {

      list_description();

      //----------------Pagenation---------------------
    $(document).on('click', '.pagenav', function(){
        var page = $(this).attr("id");
      show_list(page);     
    });
    //---------------End------------------------------
    $('#perpage').val(10);

      $("#from_date, #to_date,#return_date,#teacher, #guardian, #academic").datepicker({
        onSelect: function(date) {
          $(this).val(formatDate(date));
          getdata($(this).val());
        } 
      });

      $("#a_addnew").click(function(){
          $("#colapseform").collapse('toggle');
      });

      $("#refresh").click(function(){
          location.reload();
      });

      $( "#search" ).click(function() {         
            show_list(1);
      });

      $('#a_print').click(function(){
         var data = $("#tab_print").html();
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint_book("", export_data);
      });

      $('#program').change(function(){
        var program = $(this).val();
         var sch_level = $("#sch_level").val();
         //alert($(this).val());​​​​​​​​​​​   
         if(program != ""){
            get_school_level(program,sch_level);  
         }else{
            $('#years').html('');
            $('#grad_level').html('');
            $('#sch_level').html('');
         }
      });

      $('#sch_level').change(function(){        
        var sch_level = $(this).val(); 
        var program = $('#program').val();
        var grad_level = $('#grad_level').val();
        if(sch_level !=""){
            get_year(program,sch_level); 
            get_gradlevel(program,sch_level);
            get_class(sch_level,grad_level);              
        }
      });

      $('#grad_level').change(function(){
        var sch_level = $('#sch_level').val();
        var grad_level = $(this).val();  
        if(grad_level !=""){
             get_class(sch_level,grad_level);            
        }
      });
      // --------------------------New-------------------------    

      $("#save").click(function(){            
        var studentid = $("#studentid").val();
        var programid = $("#programid").val();
        var schoolid = $("#schoolid").val();
        var schlevelid = $("#slevelid").val();
        var yearid = $("#yearid").val();
        var gradelevelid = $("#gradelevelid").val();
        var classid = $("#c_id").val();
        var g_comment = $('#g_comment').val();
        var t_comment = $('#t_comment').val();
        var teacher_date = $('#teacher').val();
        var academic_date = $('#academic').val();
        var guardian_date = $('#guardian').val();
        var return_date = $('#return_date').val();
        var semesterid = $('#semesterid').val();
        var arr_detail = [];

        $('.des').each(function(i){ 
          var des_id =  $(this).closest('tr').find('.des').attr('attr_des_id');         
            var seldom = $(this).parent().find('#seldom').is(":checked") ? 1 : 0;
            var sometimes = $(this).parent().find('#sometimes').is(":checked") ? 1 : 0;
            var usaully = $(this).parent().find('#usually').is(":checked") ? 1 : 0;
            var consistently = $(this).parent().find('#consistently').is(":checked") ? 1 : 0;
            arr_detail[i] = {
                            des_id: des_id,                             
                            seldom: seldom,
                            sometimes:sometimes,
                            usaully:usaully,
                            consistently:consistently
                        }         
        });
        $.ajax({ 
                  type : 'POST',
                  url : "<?= site_url('school/c_evaluation_semester_iep/save_evaluation')?>",
                  dataType : 'JSON',
                  async : false,
                  data : {
                    'studentid':studentid,
                    'programid':programid,
                    'schoolid':schoolid,
                    'schlevelid':schlevelid,
                    'yearid':yearid,
                    'gradelevelid':gradelevelid,
                    'classid':classid, 
                    'g_comment':g_comment,
                    't_comment':t_comment,
                    'teacher_date':teacher_date,
                    'academic_date':academic_date,
                    'guardian_date':guardian_date,
                    'return_date':return_date,
                    'semesterid':semesterid,                    
                      arr_detail:arr_detail
                  },
                  success:function(data){
                    if(data.insert == 'success'){  
                      toastr["success"]("Save successful !");
                      $("#myModal").modal("hide");
                    }else{
                      toastr["error"]("Save fail !");
                    }
                  }
              });
      });
      // --------------------------End New---------------------

  });

  $('body').delegate(".ch_only","click",function(){
    $(this).closest('tr').find('input').not(this).prop('checked', false);                                  
  });

  function  add(event){
    var studentid = jQuery(event.target).attr("rel");
    var programid = jQuery(event.target).attr("p_id");  
    var main = jQuery(event.target).attr("main");
    list_description(studentid);
    show_student(studentid);
    var mains = main.split("_");
    var schoolid = mains[0];
    var schlevelid = mains[1];
    var yearid = mains[2];
    var gradelevelid = mains[3];
    var semesterid = mains[4];
    var classid=mains[5];
    $("#studentid").val(studentid);
    $("#programid").val(programid);
    $("#schoolid").val(schoolid);
    $("#slevelid").val(schlevelid);
    $("#yearid").val(yearid);
    $("#gradelevelid").val(gradelevelid);
    $("#semesterid").val(semesterid);  
    $("#c_id").val(classid); 
    $("#myModal").modal("show");
  }

  function show_student(id){
    $.ajax({
      url: '<?php echo site_url('school/c_evaluation_semester_iep_list/show_student') ?>',
      datatype:'JSON',
      type:'POST',
      async:false,
      data:{
            'id': id
      },
      success:function(data){
        $(".t_name").val(data['name']);
        $(".t_namekh").val(data['namekh']);
        $(".t_level").val(data['sch_level']);
        $(".t_levelkh").val(data['sch_level']);
        $(".t_year").val(data['year']);
        $(".t_yearkh").val(data['year']);
        $("#semester").val(data['semester']);
        $("#t_comment").val(data['techer_comment']);
        $("#g_comment").val(data['guardian_comment']);
        $('#teacher').val(data['techer_date']);
        $('#academic').val(data['academic_date']);
        $('#guardian').val(data['guardian_date']);
        $('#return_date').val(data['return_date']);
      }
    });
  }

  function preview(event){
    site_url('school/c_evaluation_semester_iep');
  }

  function  get_school_level(program){
    $.ajax({
      url: '<?php echo site_url('school/c_evaluation_semester_iep_list/school_level') ?>',
      datatype:'JSON',
      type:'POST',
      async:false,
      data:{
            'program':program
      },
      success:function(data){
          $('#sch_level').html(data.schlevel);    
          get_year(program,$("#sch_level").val());
      }
    });
  }

  function get_year(program,sch_level){
    $.ajax({
      url:'<?php echo site_url('school/c_evaluation_semester_iep_list/get_years') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'program':program,
              'sch_level':sch_level
      },
      success:function (data){
          $('#years').html(data.schyear);
          get_gradlevel(program,sch_level);
      },
    });
  }

  function get_gradlevel(program,sch_level){
    $.ajax({
      url:'<?php echo site_url('school/c_evaluation_semester_iep_list/get_gradlevel') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'program':program,
              'sch_level':sch_level
      },
      success:function (data){
          $('#grad_level').html(data.gradlevel);         
      },
    });
  }

  function get_class(sch_level,grad_level){
    $.ajax({
      url:'<?php echo site_url('school/c_evaluation_semester_iep_list/get_class') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'sch_level':sch_level,
              'grad_level':grad_level
      },
      success:function (data){
          $('#classid').html(data.getclass);
      },
    });
  }

  function clear_form(){
    $('school_level').val('');
    $('#grad_level').val('');
    $('#years').val('');

  }

  function formatDate(date) {
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [year, month, day].join('-');
   }

  function sort(event) {
        this.sortby = $(event.target).attr("rel");
        if ($(event.target).attr("sortype") == "ASC") {
            $(event.target).attr("sortype", "DESC");
            this.sorttype = "ASC";
            $('.sort').removeClass('cur_sort_down');
            $(event.target).addClass('cur_sort_up');
        } else if ($(event.target).attr("sortype") == "DESC") {
            $(event.target).attr("sortype", "ASC");
            this.sorttype = "DESC";
            $('.sort').removeClass('cur_sort_up');
            $(event.target).addClass('cur_sort_down');
        }

        show_list(this.fn, this.sortby, this.sorttype);       
  }

  function list_description(studentid) {
    //var studentid = $("#studentid").val();
      var url = "<?php echo site_url('school/c_evaluation_semester_iep/list_description')?>";  
      $.ajax({
          url: url,
          type: "POST",
          datatype: "JSON",
          async: false,
          data: {
              'studentid':studentid
          },
          success: function (data) {
              if(data.tr_data ==""){
                  var table = '<table class="table table-bordered table-striped table-hover">'+
                                  '<tr>'+
                                      '<td colspan="11" style="color:red;text-align:center;font-size:14px;"><i>Result Not Data</i></td>'+
                                  '</tr>'+
                                  '<tr><td colspan="11"></td></tr>'+
                              '</table>';
                  $('#list_description').html(table);

              }else{
                  $("#list_description").html(data.tr_data);
              }
          }
      });
  }

  function show_list(page, sortby, sorttype){
        var url = "<?php echo site_url('school/c_evaluation_semester_iep_list/show_list') ?>";
        var m = "<?PHP echo $m?>";
        var p = "<?PHP echo $p?>";
        var program = $('#program').val();
        var sch_level = $('#sch_level').val();
        var years = $('#years').val();
        var classid = $('#classid').val();
        var gradlevel = $('#grad_level').val();
        var gender =$('#gender').val();
        var perpage = $('#perpage').val();
        var s_student_id = $('#s_student_id').val();
        var s_full_name = $('#s_full_name').val();  
        $.ajax({
          url: url,
          type: "POST",
          datatype: "JSON",
          async: false,
          data: {
              'm': m,
              'p': p,
              'page': page,
              's_sortby': sortby,
              's_sorttype': sorttype,
              'program': program,
              'sch_level':sch_level,
              'years':years,
              'classid':classid,
              'gradlevel':gradlevel,
              'gender':gender,
              's_student_id':s_student_id,
              's_full_name':s_full_name,
              'perpage': perpage
          },
          success: function (data) {
              if(data.tr_data ==""){
                  var table = '<table class="table table-bordered table-striped table-hover">'+
                                  '<tr>'+
                                      '<td colspan="11" style="color:red;text-align:center;font-size:14px;"><i>We did not find anything to show here!</i></td>'+
                                  '</tr>'+
                                  '<tr><td colspan="11"></td></tr>'+
                              '</table>';
                  $('.listbody').html(table);
              }else{
                  $(".listbody").html(data.tr_data);
              }                   
              $('.paginate').html(data.pagina.pagination);
          }
      });
    }
  
</script>