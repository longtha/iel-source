	      
<div class="row">
	<div class="col-xs-12">
     <div class="result_info">
        <div class="col-xs-6">
        <span class="icon">
        </span>
            <strong>GEP Part-Time Report</strong>  
        </div>
        <div class="col-xs-6" style="text-align: right">			               
           <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
              <img src="<?= base_url('assets/images/icons/print.png') ?>">
           </a>
           <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
              <img src="<?= base_url('assets/images/icons/export.png') ?>">
           </a> 
           <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
              <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
           </a>
        </div>         
     </div>
	</div>
</div>
<div id="print">
	<center>
		<div class="wrapper" style="border:0px solid #f00;width:95%;">   
			   	<style type="text/css">
					
					.tbscore > tr >td { 
						line-height: 8px !important;
						font-size: 11px !important; 
					}
					.tbskill > tr >td { 
						line-height: 17px !important; 
					}
					#color{
						background-color:#447ca8 !important;
						color:#ffffff !important;
					}
					#color_hight{
						background-color:#447ca8 !important;
						color:#ffffff !important;
						line-height: 5px !important;
					}
					#hights{
						line-height: 2px !important;
					}
				</style>
		   		<table border="0" style="width:100% !important;">
					<tr>
						<td style="width:20%;vertical-align: top;">
							<div class="" style="padding-left:20px;margin: 0px;padding-top: 15px;">
								<img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="width:200px !important">
							</div>
						</td>
						<td>
							<div class="title_full_time">
					   			<h4 class="" style="font-family: 'Khmer OS Muol light' !important;margin-top: 5px !important;">របាយការណ៍បញ្ចប់វគ្គសិក្សា</h4>
					   			<h4 class="" style="font-family: 'Khmer OS Muol light' !important;margin-top: 5px !important;">ភាសាអង់គ្លេសទូទៅថ្នាក់ក្រៅម៉ោង</h4>	
					   			<h4 class="f_sizes">STUDENT'S GEP PART~TIME FINAL REPORT</h4>
					   			<img src="<?php echo base_url('assets/images/sambol-line.png')?>" style="width:150px !important">		   					  		
					   		</div>	
						</td>
						<td style="width:20%;"></td>
					</tr>
				</table>	 
		   			<?php
		   				$studentid = isset($_GET['studentid']) ? $_GET['studentid'] : '';
		   				$programid = isset($_GET['programid']) ? $_GET['programid'] : '';
		   				$termid = isset($_GET['termid']) ? $_GET['termid'] : '';
		   				$is_pt = isset($_GET['is_pt']) ? $_GET['is_pt'] : '';
		   				 
		   				// main_tranno =====
		   				if(isset($_GET['main_tranno'])){
							$main_tranno = $_GET['main_tranno'];
							$main_tranno_ = explode('_', $main_tranno);
							$schoolid = $main_tranno_[0];
							$schlevelid = $main_tranno_[1];
							$yearid = $main_tranno_[2];
							$gradelevelid = $main_tranno_[3];
							$classid = $main_tranno_[4];
						}						

						$where = "";
						$where .= "st.studentid = '".$studentid."' ";
						$where .= "AND st.schoolid = '".$schoolid."' ";
						$where .= "AND st.programid = '".$programid."' ";						
						$where .= "AND st.schlevelid = '".$schlevelid."' ";
						$where .= "AND st.year = '".$yearid."' ";
						$where .= "AND st.grade_levelid = '".$gradelevelid."' ";
						$where .= "AND st.classid = '".$classid."' ";

		   				$row_stu = $this->db->query("SELECT * FROM v_student_enroment AS st WHERE $where ")->row();
		   				$sql_comand = $this->db->query("SELECT command FROM sch_studend_command_gep 
				   										WHERE 1=1
														AND student_id = {$studentid}
														AND schoolid   = {$schoolid}
														AND schlevelid = {$schlevelid}
														AND yearid     = {$yearid}
														AND gradelevelid= {$gradelevelid}
														AND classid    = {$classid}
														AND termid     = {$termid}
														AND is_partime = {$is_pt}
													")->row();
		   				$command_student = isset($sql_comand->command)?$sql_comand->command:"";

		   			?> 
		   			<table border="0" style="width:95% !important;margin-left: 20px;">
			   			<tr>
			   				<td style="width:50%;"><p style="font-size: 14px;float:left;">First name:</p><span style="border-bottom: 1px solid black; width:60% !important;float:left;">&nbsp;<b><?= $row_stu->first_name ?></b></span></td>
			   				<td style="width:50%;"><p style="font-size: 14px;float:left;">Last name:</p><span style="border-bottom: 1px solid black; width:60% !important;float:left;">&nbsp;<b><?= $row_stu->last_name ?></b></span></td>
			   			</tr>
			   			<tr>
			   				<td><p style="font-size: 14px;float:left;">Student's ID:</p><span style="border-bottom: 1px solid black; width:57% !important;float:left;">&nbsp;<b><?= $row_stu->student_num ?></b></span></td>
			   				<td><p style="font-size: 14px;float:left;">Sex:</p><span style="border-bottom: 1px solid black; width:69% !important;float:left;">&nbsp;<b><?= ucwords($row_stu->gender) ?></b></span></td>
			   			</tr>
			   			<tr>
			   				<td><p style="font-size: 14px;float:left;">Level:</p><span style="border-bottom: 1px solid black; width:67% !important;float:left;">&nbsp;<b><?= $row_stu->class_name ?></b></span></td>
			   				<td><p style="font-size: 14px;float:left;">Time:</p><span style="border-bottom: 1px solid black; width:67% !important;float:left;">&nbsp;</span></td>
			   			</tr>
			   		</table>
					
	                   	 			<?php
	               //     	 			$q_t = $this->db->query("SELECT
																// 	t.term,
																// 	t.start_date,
																// 	t.end_date AS end_date
																// FROM
																// 	sch_school_term AS t
																// WHERE
																// 	t.schoolid = '{$schoolid}'
																// AND t.programid = '{$programid}'
																// AND t.schlevelid = '{$schlevelid}'
																// AND t.yearid = '{$yearid}' ")->row();
	                   	 			$sql_subj = $this->db->query("SELECT `subjectid`, 
									                   	 				`subject`, 
									                   	 				`subj_type_id`, 
									                   	 				`short_sub`, 
									                   	 				`schoolid`, 
									                   	 				`programid`, 
									                   	 				`schlevelid`, 
									                   	 				`yearid`, 
									                   	 				`subject_kh`,
									                   	 				`is_eval`,
									                   	 				`max_score`, 
									                   	 				`is_core`, 
									                   	 				`is_skill`, 
									                   	 				`is_assessment`, 
									                   	 				`calc_score` 
									                   	 			FROM `sch_subject_gep` 
									                   	 			WHERE 1=1
									                   	 			AND `schoolid`='{$schoolid}'
									                   	 			AND `programid`='{$programid}' 
									                   	 			AND `schlevelid`='$schlevelid'");
	                   	 			$arr_usbj = array();
	                   	 			if($sql_subj->num_rows() > 0){
	                   	 				foreach($sql_subj->result() as $row_subj){
	                   	 					if($row_subj->is_assessment == 0){
		                   	 					if($row_subj->is_core == 1 && $row_subj->is_skill == 0){
													$arr_usbj['core'][$row_subj->subjectid]  = array("max_score"=>$row_subj->max_score,"cal_score"=>$row_subj->calc_score);
		                   	 					}else{
		                   	 						$arr_usbj['skill'][$row_subj->subjectid] = $row_subj->calc_score;
		                   	 					}
	                   	 					}else{
	                   	 						
	                   	 					}
	                   	 				}
	                   	 			}
	                   	 			$q_score = $this->db->query("SELECT
																	o.tran_date,
																	score_num,
																	d.studentid,
																	d.subjectid,
																	o.is_pt,
																	o.course_type,
																	o.exam_typeid,
																	SUM(score_num) AS score_num,
																	COUNT(*) AS count_t
																FROM
																	sch_score_gep_entryed_order AS o
																INNER JOIN sch_score_gep_entryed_detail AS d ON o.typeno = d.typeno
																WHERE 1=1
																AND o.termid='".$termid."'
																AND o.is_pt = '{$is_pt}' 
																GROUP BY o.exam_typeid,o.course_type,d.subjectid,d.studentid");
					   				$arr_sub = array();
					   				$arr_skill = array();
					   				$arr_mid_core = 0;
					   				$arr_mid_skil = 0;
					   				if($q_score->num_rows() > 0){
					   					foreach($q_score->result() as $row_s){
					   						if($row_s->course_type == 0){ // core
					   							if($row_s->exam_typeid == 1){ // mid-term
					   								//$arr_mid_core += $row_s->score_num;
													$val_cp_core_m = isset($arr_count_m[$row_s->course_type][$row_s->subjectid])?$arr_count_m[$row_s->course_type][$row_s->subjectid]:0;
													if($row_s->count_t > $val_cp_core_m){
														$arr_count_m[$row_s->course_type][$row_s->subjectid] = $row_s->count_t;
													}
					   								if(isset($arr_sub_m[$row_s->studentid][$row_s->subjectid])){
														$arr_sub_m[$row_s->studentid][$row_s->subjectid] = $arr_sub[$row_s->studentid][$row_s->subjectid]+$row_s->score_num;
					   								}else{
					   									$arr_sub_m[$row_s->studentid][$row_s->subjectid] = $row_s->score_num;	
					   								}

					   							}else{ // final
					   								$val_cp_core = isset($arr_count_f[$row_s->course_type][$row_s->subjectid])?$arr_count_f[$row_s->course_type][$row_s->subjectid]:0;
													if($row_s->count_t > $val_cp_core){
														$arr_count_f[$row_s->course_type][$row_s->subjectid] = $row_s->count_t;
													}
													$score_sub = 0;
													if($row_s->course_type == 0){
				   										if(isset($arr_usbj['core'][$row_s->subjectid])){
				   											$cal_s = $arr_usbj['core'][$row_s->subjectid]['cal_score'];
				   											$max_s = $arr_usbj['core'][$row_s->subjectid]['max_score'];
				   											$score_sub = ($row_s->score_num*$cal_s)/$max_s;
				   										}
				   									}
					   								if(isset($arr_sub[$row_s->studentid][$row_s->subjectid])){
					   									$arr_sub[$row_s->studentid][$row_s->subjectid] = $arr_sub[$row_s->studentid][$row_s->subjectid]+$score_sub;
														//$arr_sub[$row_s->studentid][$row_s->subjectid] = $arr_sub[$row_s->studentid][$row_s->subjectid]+$row_s->score_num;
													}else{
					   									$arr_sub[$row_s->studentid][$row_s->subjectid] = $score_sub;	
					   								}
					   							}
					   							
					   						}
						   											   						
					   					}
					   				}
						    	
	                   	 			$tr_cp1 = "";
	                   	 			$tr_cp2 = "";
	                   	 			$total_att_hw = 0;
	                   	 			$total_cp_amt = 0;
	                   	 			$total_score_cp1 = 0;
	                   	 			$total_score_cp2 = 0;	
	                   	 			$total_score_m_subj = 0;  
	                   	 			$ii   = 0; 
	                   	 			//$ii_sub  = 0;  
	                   	 			$jj = 0;      

	                   	 			$grand_total_att_hw = 0;  
	                   	 			$grand_fin_sub = 0;      	 			
								    foreach ($query_core as $row) {
								    	//$score = isset($arr_sub[$row->subjectid]) ? $arr_sub[$row->subjectid] : 0;

								    	$q_meaning = $this->db->query("SELECT
																				sm.submenid,
																				sm.schoolid,
																				sm.schlevelid,
																				sm.yearid,
																				sm.subjectid,
																				sm.gradelevelid,
																				sm.mentionid,
																				sm.max_score,
																				sm.minscrore,
																				sm.active,
																				cm.mention
																			FROM
																				sch_subject_mention AS sm
																			LEFT JOIN sch_score_mention AS cm ON sm.mentionid = cm.menid
																			WHERE sm.subjectid = '{$row->subjectid}' ");

										$count_f = isset($arr_count_f[1][$row->subjectid])?$arr_count_f[1][$row->subjectid]:1;
								    	$count_m = isset($arr_count_m[1][$row->subjectid])?$arr_count_m[1][$row->subjectid]:1;

										$score_f = isset($arr_sub[$studentid][$row->subjectid]) ? ($arr_sub[$studentid][$row->subjectid]/$count_f) : 0;
										$score_m = isset($arr_sub_m[$studentid][$row->subjectid]) ? ($arr_sub_m[$studentid][$row->subjectid]/$count_m) : 0;
										
										if($row->is_class_participation == 3)
										{
											$meaning_ = "";
											$result_score_att = round(($score_f+$score_m)/2,2);
											if($q_meaning->num_rows() > 0){
									    		foreach ($q_meaning->result() as $row_meaning) {
									    			$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : '';
									    			$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : '';
									    			if(($min_score - 0 <= $result_score_att - 0) && ($result_score_att - 0 <= $max_score - 0)){
														$meaning_ = $row_meaning->mention;
									    			}
												}
								    		}	
											$tr_cp1.= '<tr><td>'.$row->subject.'</td><td style="text-align:center;">'.$result_score_att.'</td><td>'.$meaning_.'</td></tr>';
											$total_att_hw += $result_score_att;
											$grand_total_att_hw += round(($result_score_att*$row->calc_score)/$row->max_score,2);
										
										}
										else if($row->is_class_participation == 2)
										{
											$meaning_ = "";
											$result_score_hw = round(($score_f+$score_m)/2,2);
											if($q_meaning->num_rows() > 0){
									    		foreach ($q_meaning->result() as $row_meaning) {
									    			$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : '';
									    			$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : '';
									    			if(($min_score - 0 <= $result_score_hw - 0) && ($result_score_hw - 0 <= $max_score - 0)){
														$meaning_ = $row_meaning->mention;
									    			}
												}
								    		}	
											$tr_cp1.= '<tr><td>'.$row->subject.'</td><td style="text-align:center;">'.$result_score_hw.'</td><td>'.$meaning_.'</td></tr>';
											$total_att_hw += $result_score_hw;
											$grand_total_att_hw += round(($result_score_hw*$row->calc_score)/$row->max_score,2);
										}	
										else if($row->is_class_participation == 1)
										{
											$result_score_cp = round(($score_f+$score_m)/2,2);
											$meaning_ = '';
											if($q_meaning->num_rows() > 0){
									    		foreach ($q_meaning->result() as $row_meaning) {
									    			$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : '';
									    			$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : '';
									    			if(($min_score - 0 <= $result_score_cp - 0) && ($result_score_cp - 0 <= $max_score - 0)){
														$meaning_ = $row_meaning->mention;
									    			}
												}
								    		}	
											$tr_cp1.= '<tr><td>'.$row->subject.'</td><td style="text-align:center;">'.$result_score_cp.'</td><td>'.$meaning_.'</td></tr>';
											$total_score_cp1 += $result_score_cp;
											$total_cp_amt += round(($result_score_cp*$row->calc_score)/$row->max_score,2);
											$ii++;
								    	}
								    	else
								    	{
								    		$meaning_ = '';
								    		if($q_meaning->num_rows() > 0){
									    		foreach ($q_meaning->result() as $row_meaning) {
									    			$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : '';
									    			$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : '';
									    			if(($min_score - 0 <= $score_f - 0) && ($score_f - 0 <= $max_score - 0)){
														$meaning_ = $row_meaning->mention;
									    			}
												}
								    		}	
								    		$tr_cp2.= '<tr><td>'.$row->subject.'</td><td style="text-align:center;">'.round($score_f,2).' pppp</td><td>'.$meaning_.'</td></tr>';
								    		$total_score_cp2 += $score_f;
								    		$total_score_m_subj += $score_m;
								    		$grand_fin_sub += round(($score_f*$row->calc_score)/$row->max_score,2);
								    		//$grand_mid += round(($score_m*$row->calc_score)/$row->max_score,2)-0;
								    		
								    		$jj++;
								    	}
								    }
									
								    $total_pc_all = $total_att_hw+$total_score_cp1;
								   	$total_pc  = round(($total_cp_amt/$ii),2);
								   	//$result_pc = round(($total_pc*10)/100,2);
								   	$total_m   = round(($total_score_m_subj*15)/100,2);
									//$total_s   = round(($total_score_cp2*60)/100,2);

								   	$grand_total = $grand_total_att_hw+$total_pc+$grand_fin_sub+$total_m;
								 	?>

					<table style="width:100%;margin-top: 10px;">	
						<tr style="vertical-align: top;">					
							<td style="width:50%;height: 50%;">
							   <table border="1" style="width:100%;height:100%;">
			                   	 	<thead id="color">
			                   	 		<tr style="height: 30px;">
			                   	 			<th id="color">Core Subject with Sub Skills</th>
			                   	 			<th id="color">Score​</th>
			                   	 			<th id="color">Meaning</th>
			                   	 		</tr>
			                   	 	</thead>
			                   	 	<tbody class="tbscore">
			                   	 		<?php
		                   	 			echo $tr_cp2;
									 	?>  
			                   	 		<tr style="height:20px;">
			                   	 			<td class="totals">Total Score</td>
			                   	 			<td style="font-weight: bold;text-align: center;"><?= $total_score_cp2 ?></td>
			                   	 			<td></td>
			                   	 		</tr>
			                   	 		<tr style="height:20px;">
			                   	 			<td class="totals">Mid-Term</td>
			                   	 			<td style="font-weight: bold;text-align: center;"><?php echo $total_score_m_subj;?></td>
			                   	 			<td></td>
			                   	 		</tr>		                   	 			                   	 		
			                   	 	</tbody>
			                   </table>
				        	</td>	
							<td style="width:50%;height: 50%;">
			                   	<table border="1" style="height:100%;">
			                   	 	<thead>
			                   	 		<tr style="height: 30px;">
			                   	 			<th id="color">Core On-going assessment</th>
			                   	 			<th id="color">Score​</th>
			                   	 			<th id="color">Meaning</th>
			                   	 		</tr>
			                   	 	</thead>
			                   	 	<tbody>   
			                   	 		<?php echo $tr_cp1;?>
									 	<tr>
			                   	 			<td class="totals">Total Score</td>
			                   	 			<td style="font-weight: bold;text-align: center;"><?php echo $total_pc_all; ?></td>
			                   	 			<td></td>
			                   	 		</tr> 
			                   	 		 	 			                   	 		
			                   	 	</tbody>
			                   	</table>
							</td>
						</tr>	
					</table>	
				<?php
       	 			$sql_result_mention = $this->db->query("SELECT
																sch_score_mention_gep.menid,
																sch_score_mention_gep.mention,
																sch_score_mention_gep.men_type,
																sch_score_mention_gep.schlevelid,
																sch_score_mention_gep.grade,
																sch_score_mention_gep.min_score,
																sch_score_mention_gep.max_score,
																sch_score_mention_gep.is_past
																FROM
																sch_score_mention_gep
																WHERE 1=1 
																AND schlevelid = '".$schlevelid."'");
				    $grade_v = "";
				    $meaning = "";
				    $result_meaning = "";
				    if($sql_result_mention->num_rows() > 0){
					    foreach ($sql_result_mention->result() as $row_m) {
					    	if($row_m->min_score <= $grand_total && $row_m->max_score >= $grand_total){
					    		if($row_m->is_past == 1){
					    			$result_meaning = "Past";
					    		}else{
					    			$result_meaning = "Fail";
					    		}
								$grade_v = $row_m->grade;
					    	}	
					   		$meaning.= '<tr><td>'.$row_m->min_score.'-'.$row_m->max_score.'</td>
								   		  <td style="text-align:center;">'.$row_m->grade.'</td>
								   		  <td>'.$row_m->mention.'</td>
								   		</tr>';							
					   	} 
					}
			 	?>            		
				<table style='width:99.5%;margin-top: 15px;margin-left: 3px;' border="1">
					 	<thead id="color">
					 		<tr style="height:25px;">
					 			<th id="color_hight">Rank</th>
					 			<th id="color_hight">Grade</th>
					 			<th id="color_hight">Grand Total Score</th>
					 			<th id="color_hight">Result</th>
					 			<th id="color_hight">Next Level</th>
					 		</tr>
					 	</thead>
					 	<tbody>
					 		<tr>
					 			<td></td>
					 			<td style="text-align: center;"><?php echo $grade_v;?></td>
					 			<td style="text-align: center;"><?php echo $grand_total;?></td>
					 			<td style="text-align: center;"><?php echo $result_meaning;?></td>
					 			<td></td>
					 		</tr>
					 	</tbody>
				</table>	
				<table style="width:100%;margin-top: 10px;">
					<tr style="vertical-align: top;">
						<td style="width:40%;">	
		                   	<table style="max-height: 5px;" border="1">
		                   	 	<thead>
		                   	 		<tr style="height: 20px;">
		                   	 			<th colspan="3" id="color_hight">Grading System</th>
		                   	 		</tr>
		                   	 		<tr style="height: 20px;">
		                   	 			<th id="hights">Score</th>
		                   	 			<th id="hights">Grade</th>
		                   	 			<th id="hights">Meaning</th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody class="tbscore">
		                   	 		<?php echo $meaning;?>	 			                   	 		
		                   	 	</tbody>
		                   	</table>
				        </td>
						<td>
						    <table border="1" style="width:100%;">
		                   	 	<thead id="color">
		                   	 		<tr style="height: 20px;"><th id="color_hight">Teacher's Comment</th></tr>
		                   	 	</thead>
		                   	 	<tbody>
		      						<tr style="vertical-align: top;"><td style="height: 138px;"><?php echo $command_student;?></td></tr>
		                   	 	</tbody>
		                    </table>            
						</td>
					</tr>
				</table>
				<table style="width:100%;margin-bottom: 100px;">
					<tr>
						<td style="text-align: left;">
							<label>Date: ......../......../............</label><br>
							<label style="padding-left:5%;">Academic Manager</label>
						</td>
						<td style="text-align: right;">
							<label>Date: ......../......../............</label><br>
							<label style="padding-right:5%;">Teacher's Signature</label>
						</td>
					</tr>
					<tr>
						<td style="text-align: left;">
							<h5 class="l_foter" style="margin-left: 50px;margin-top: 50px;">HUOTH PEO</h5>
						</td>
						<td></td>
					</tr>
				</table>
		</div>
	</center>
</div>
<script type="text/javascript">
	$(function(){
		$("#a_print").on("click", function (e) {
	           gPrint('print');
    	}); 
	});
</script>