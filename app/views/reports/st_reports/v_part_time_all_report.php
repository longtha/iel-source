<div class="row">
	<div class="col-xs-12">
		<div class="result_info">
			<div class="col-xs-6">
			    <strong>GEP Part-Time Report</strong>  
			</div>
			<div class="col-xs-6" style="text-align: right">			               
			    <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
			      <img src="<?= base_url('assets/images/icons/print.png') ?>">
			    </a>
			    <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
			      <img src="<?= base_url('assets/images/icons/export.png') ?>">
			    </a> 
			    <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
			      <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
			    </a>
			</div>         
		</div>
	</div>
</div>
<div id="print_all">
<?php 
	
	$sql_stu = $this->db->query("SELECT
									st.student_num,
									st.first_name,
									st.last_name,
									st.first_name_kh,
									st.last_name_kh,
									st.gender,
									st.class_name,
									st.studentid,
									st.schoolid,
									st.`year`,
									st.classid,
									st.schlevelid,
									st.rangelevelid,
									st.feetypeid,
									st.programid,
									st.sch_level,
									st.rangelevelname,
									st.program,
									st.sch_year,
									st.grade_levelid,
									st.is_pt,
									st.from_date,
									st.to_date
									FROM v_student_enroment AS st
									WHERE 1=1
									AND st.schoolid='".$infor['schoolid']."'
									AND st.schlevelid='".$infor['schoollavel']."'
									AND st.grade_levelid='".$infor['gradelavel']."'
									AND st.classid='".$infor['classid']."'
									AND st.`year`='".$infor['years']."'
									AND st.`is_pt`='".$infor['is_partime']."'
									GROUP BY st.studentid
									ORDER BY st.studentid ASC
									LIMIT {$infor['numrow']},{$infor['perpage']}
								");
	if($sql_stu->num_rows() > 0){
		foreach($sql_stu->result() as $row_st){
			$sql_comand = $this->db->query("SELECT command FROM sch_studend_command_gep 
	   										WHERE 1=1
											AND student_id = '".$row_st->studentid."'
											AND schoolid   = '".$infor['schoolid']."'
											AND schlevelid = '".$infor['schoollavel']."'
											AND yearid     = '".$infor['years']."'
											AND gradelevelid= '".$infor['gradelavel']."'
											AND classid    = '".$infor['classid']."'
											AND termid     = '".$infor['termid']."'
											AND is_partime = '".$infor['is_partime']."'
										")->row();
			$command = isset($sql_comand->command)?$sql_comand->command:"";
			$data = array("schoolid"=>$infor['schoolid'],
							"schoollavel"=>$infor['schoollavel'],
							"gradelavel"=>$infor['gradelavel'],
							"classid"=>$infor['classid'],
							"years"=>$infor['years'],
							"termid"=>$infor['termid'],
							"is_partime"=>$infor['is_partime'],
							"studentid"=>$row_st->studentid,
							"first_name"=>$row_st->first_name,
							"last_name"=>$row_st->last_name,
							"student_num"=>$row_st->student_num,
							"gender"=>$row_st->gender,
							"class_name"=>$row_st->class_name,
							"command"=>$command
						);
			$this->load->view('reports/st_reports/v_part_time_print_all_report',$data);
		}
	}
?>
</div>
<script type="text/javascript">
	$(function(){
		$("#a_print").on("click", function (e) {
	           gPrint('print_all');
    	}); 
	});
</script>