<div class="row">
	<div class="result_info">
	    <div class="col-xs-6" style="text-align: left;">
	        <strong>GEP Full-Time Report</strong>  
	    </div>
	    <div class="col-xs-6" style="text-align: right">			               
	       <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
	          <img src="<?= base_url('assets/images/icons/print.png') ?>">
	       </a>
	       <a href="javascript:;" id="a_export" data-toggle="tooltip" data-placement="top" title="Export">
	          <img src="<?= base_url('assets/images/icons/export.png') ?>">
	       </a> 
	       <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
	          <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
	       </a>
	    </div>         
	 </div>
</div>

<div id="print_all">
	<center>
	<div style="border:0px solid #f00;width:95%;">
	
      
		   	<style type="text/css">
				.tbscore > tr >td { 
					line-height: 8px !important;
					font-size: 11px !important; 
				}
				.tbskill > tr >td { 
					line-height: 17px !important; 
				}
				#color{
					background-color:#447ca8 !important;
					color:#ffffff !important;
				}
				#color_hight{
					background-color:#447ca8 !important;
					color:#ffffff !important;
					line-height: 5px !important;
				}
				#hights{
					line-height: 2px !important;
				}
			</style>
				<table border="0" style="width:100% !important;">
					<tr>
						<td style="width:20%;vertical-align: top;">
							<div class="" style="padding-left:20px;margin: 0px;padding-top: 15px;">
								<img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="width:200px !important">
							</div>
						</td>
						<td>
							<div class="title_full_time">
					   			<h4 class="" style="font-family: 'Khmer OS Muol light' !important;margin-top: 5px !important;">របាយការណ៍បញ្ចប់វគ្គសិក្សា</h4>
					   			<h4 class="" style="font-family: 'Khmer OS Muol light' !important;margin-top: 5px !important;">ភាសាអង់គ្លេសទូទៅថ្នាក់ពេញម៉ោង</h4>	
					   			<h4 class="f_sizes">STUDENT'S GEP FULL~TIME FINAL REPORT</h4>
					   			<img src="<?php echo base_url('assets/images/sambol-line.png')?>" style="width:150px !important">		   					  		
					   		</div>	
						</td>
						<td style="width:20%;"></td>
					</tr>
				</table>	
		   			<?php
		   				$studentid = isset($_GET['studentid']) ? $_GET['studentid'] : 0;
		   				$programid = isset($_GET['programid']) ? $_GET['programid'] : 0;
						//$schlevelid = isset($_GET['schlevelid']) ? $_GET['schlevelid'] : '';
						$termid = isset($_GET['termid']) ? $_GET['termid'] : 0;
		   				$is_pt = isset($_GET['is_pt']) ? $_GET['is_pt'] : 0;
		   				
		   				// main_tranno =====
		   				if(isset($_GET['main_tranno'])){
							$main_tranno = $_GET['main_tranno'];
							$main_tranno_ = explode('_', $main_tranno);
							$schoolid = $main_tranno_[0];
							$schlevelid = $main_tranno_[1];
							$yearid = $main_tranno_[2];
							$gradelevelid = $main_tranno_[3];
							$classid = $main_tranno_[4];
						}						

						$where = "";
						$where .= " st.studentid = '".$studentid."' ";
						$where .= " AND st.schoolid = '".$schoolid."' ";
						$where .= " AND st.programid = '".$programid."' ";						
						$where .= " AND st.schlevelid = '".$schlevelid."' ";
						$where .= " AND st.year = '".$yearid."' ";
						$where .= " AND st.grade_levelid = '".$gradelevelid."' ";
						$where .= " AND st.classid = '".$classid."' ";						
						
		   				$row_stu = $this->db->query("SELECT * FROM v_student_enroment AS st WHERE $where ")->row();

		   				$sql_comand = $this->db->query("SELECT command,academicid,userid,date_academic,date_teacher FROM sch_studend_command_gep 
				   										WHERE 1=1
														AND student_id = {$studentid}
														AND schoolid   = {$schoolid}
														AND schlevelid = {$schlevelid}
														AND yearid     = {$yearid}
														AND gradelevelid= {$gradelevelid}
														AND classid    = {$classid}
														AND termid     = {$termid}
														AND is_partime = {$is_pt}
													")->row();
		   				$command_student = isset($sql_comand->command)?$sql_comand->command:"";
		   				$date_academic   = isset($sql_comand->date_academic)?$this->green->formatSQLDate($sql_comand->date_academic):"";
		   				$date_teacher    = isset($sql_comand->date_teacher)?$this->green->formatSQLDate($sql_comand->date_teacher):"";
		   				$academicname = "";
		   				$teachername  = "";
		   				if(isset($sql_comand->academicid)){
		   					$sql_name = $this->db->query("SELECT
														sch_user.userid,
														sch_user.user_name
														FROM
														sch_user
														WHERE 1=1 AND match_con_posid='acca'
														AND userid='".$sql_comand->academicid."'")->row();
		   					$academicname = isset($sql_name->user_name)?$sql_name->user_name:"";
		   				}
		   				if(isset($sql_comand->userid)){
		   					$sql_name1 = $this->db->query("SELECT
														sch_user.userid,
														sch_user.user_name
														FROM
														sch_user
														WHERE 1=1
														AND userid='".$sql_comand->userid."'")->row();
		   					$teachername = isset($sql_name1->user_name)?$sql_name1->user_name:"";
		   				}
		   				$arr_rank_s = array("schoolid"=>$schoolid,
											"schlevelid"=>$schlevelid,
											"gradelevelid"=>$gradelevelid,
											"classid"=>$classid,
											"yearid"=>$yearid,
											"termid"=>$termid,
											"studentid"=>$studentid
											);
		   			?>	 
		   			<table border="0" style="width:95% !important;">
			   			<tr>
			   				<td style="width:50%;"><p style="font-size: 14px;float:left;">First name:</p><span style="border-bottom: 1px solid black; width:60% !important;float:left;">&nbsp;<b><?= $row_stu->first_name ?></b></span></td>
			   				<td style="width:50%;"><p style="font-size: 14px;float:left;">Last name:</p><span style="border-bottom: 1px solid black; width:60% !important;float:left;">&nbsp;<b><?= $row_stu->last_name ?></b></span></td>
			   			</tr>
			   			<tr>
			   				<td><p style="font-size: 14px;float:left;">Student's ID:</p><span style="border-bottom: 1px solid black; width:57% !important;float:left;">&nbsp;<b><?= $row_stu->student_num ?></b></span></td>
			   				<td><p style="font-size: 14px;float:left;">Sex:</p><span style="border-bottom: 1px solid black; width:69% !important;float:left;">&nbsp;<b><?= ucwords($row_stu->gender) ?></b></span></td>
			   			</tr>
			   			<tr>
			   				<td><p style="font-size: 14px;float:left;">Level:</p><span style="border-bottom: 1px solid black; width:67% !important;float:left;">&nbsp;<b><?= $row_stu->class_name ?></b></span></td>
			   				<td><p style="font-size: 14px;float:left;">Time:</p><span style="border-bottom: 1px solid black; width:67% !important;float:left;">&nbsp;</span></td>
			   			</tr>
			   		</table>	
						<?php
	               //     	 			$q_t = $this->db->query("SELECT
																// 	t.term,
																// 	t.start_date,
																// 	t.end_date AS end_date
																// FROM
																// 	sch_school_term AS t
																// WHERE
																// 	t.schoolid = '{$schoolid}'
																// AND t.programid = '{$programid}'
																// AND t.schlevelid = '{$schlevelid}'
																// AND t.yearid = '{$yearid}' ")->row();

					   				$q_score = $this->db->query("SELECT
																	o.tran_date,
																	score_num,
																	d.studentid,
																	d.subjectid,
																	o.is_pt,
																	o.course_type,
																	o.exam_typeid,
																	SUM(score_num) AS score_num,
																	COUNT(*) AS count_t
																FROM
																	sch_score_gep_entryed_order AS o
																INNER JOIN sch_score_gep_entryed_detail AS d ON o.typeno = d.typeno
																WHERE 1=1
																AND o.termid='".$termid."'
																AND o.is_pt = '{$is_pt}' 
																GROUP BY o.exam_typeid,o.course_type,d.subjectid,d.studentid");	
					   			
					   				$arr_sub = array();
					   				$arr_skill = array();
					   				$arr_count_f = array();
					   				$arr_count_m = array();
					   				$arr_mid_core = 0;
					   				$arr_mid_skil = 0;
									$count_subject = array();

									$arr_sub_m = array();
									$arr_sub_m_skil = array();
					   				if($q_score->num_rows() > 0){
					   					foreach($q_score->result() as $row_s){
					   						if($row_s->course_type == 0){ // core
					   							if($row_s->exam_typeid == 1){ // mid-term
					   								//$arr_mid_core += $row_s->score_num;
													$val_cp_core_m = isset($arr_count_m[$row_s->course_type][$row_s->subjectid])?$arr_count_m[$row_s->course_type][$row_s->subjectid]:0;
													if($row_s->count_t > $val_cp_core_m){
														$arr_count_m[$row_s->course_type][$row_s->subjectid] = $row_s->count_t;
													}
					   								if(isset($arr_sub_m[$row_s->studentid][$row_s->subjectid])){
														$arr_sub_m[$row_s->studentid][$row_s->subjectid] = $arr_sub[$row_s->studentid][$row_s->subjectid]+$row_s->score_num;
					   								}else{
					   									$arr_sub_m[$row_s->studentid][$row_s->subjectid] = $row_s->score_num;	
					   								}

					   							}else{ // final
					   								$val_cp_core = isset($arr_count_f[$row_s->course_type][$row_s->subjectid])?$arr_count_f[$row_s->course_type][$row_s->subjectid]:0;
													if($row_s->count_t > $val_cp_core){
														$arr_count_f[$row_s->course_type][$row_s->subjectid] = $row_s->count_t;
													}
					   								if(isset($arr_sub[$row_s->studentid][$row_s->subjectid])){
														$arr_sub[$row_s->studentid][$row_s->subjectid] = $arr_sub[$row_s->studentid][$row_s->subjectid]+$row_s->score_num;
					   								}else{
					   									$arr_sub[$row_s->studentid][$row_s->subjectid] = $row_s->score_num;	
					   								}
					   							}
					   							
					   						}else{ // skill
					   							if($row_s->exam_typeid == 1){ // Mid-term
					   								//$arr_mid_skil += $row_s->score_num;
					   								$val_cp_core_m = isset($arr_count_m[$row_s->course_type][$row_s->subjectid])?$arr_count_m[$row_s->course_type][$row_s->subjectid]:0;
													if($row_s->count_t > $val_cp_core_m){
														$arr_count_m[$row_s->course_type][$row_s->subjectid] = $row_s->count_t;
													}
													//echo $row_s->subjectid."=>".$arr_count_m[$row_s->exam_typeid][$row_s->subjectid]."<br>";
					   								if(isset($arr_sub_m_skil[$row_s->studentid][$row_s->subjectid])){
														$arr_sub_m_skil[$row_s->studentid][$row_s->subjectid] = $arr_sub_m_skil[$row_s->studentid][$row_s->subjectid]+$row_s->score_num;
					   								}else{
					   									$arr_sub_m_skil[$row_s->studentid][$row_s->subjectid] = $row_s->score_num;	
					   								}
					   							}else{ // final
					   								$val_cp_skill = isset($arr_count_f[$row_s->course_type][$row_s->subjectid])?$arr_count_f[$row_s->course_type][$row_s->subjectid]:0;
													if($row_s->count_t > $val_cp_skill){
														$arr_count_f[$row_s->course_type][$row_s->subjectid] = $row_s->count_t;
													}
					   								if(isset($arr_skill[$row_s->studentid][$row_s->subjectid])){
														$arr_skill[$row_s->studentid][$row_s->subjectid] = $arr_skill[$row_s->studentid][$row_s->subjectid]+$row_s->score_num;
					   								}else{
					   									$arr_skill[$row_s->studentid][$row_s->subjectid] = $row_s->score_num;	
					   								}	
					   							}
					   						}	
						   											   						
					   					}
					   				}

						    	
	                   	 			$tr_cp1 = "";
	                   	 			$tr_cp2 = "";
	                   	 			$total_score_cp1 = 0;
	                   	 			$total_score_cp2 = 0;

	                   	 			$total_score_m_s = 0;	
	                   	 			$total_cp_amt    = 0;
	                   	 			$grand_total     = 0;
	                   	 			$grand_mid_sub = 0;
									$grand_fin_sub = 0;
	                   	 			$ii = 0;      
	                   	 			$jj = 0;    	

	                   	 			$total_percent_cp = 0; 			
								    foreach($query_core as $row) 
								    {
								    	$q_meaning = $this->db->query("SELECT
																				sm.submenid,
																				sm.schoolid,
																				sm.schlevelid,
																				sm.yearid,
																				sm.subjectid,
																				sm.gradelevelid,
																				sm.mentionid,
																				sm.max_score,
																				sm.minscrore,
																				sm.active,
																				cm.mention
																			FROM
																				sch_subject_mention AS sm
																			LEFT JOIN sch_score_mention AS cm ON sm.mentionid = cm.menid
																			WHERE sm.subjectid = '{$row->subjectid}' ");
								    	$max_score_calc    = $row->max_score;
								    	$res_score_calc    = $row->calc_score;
								    	$count_n = isset($arr_count_f[0][$row->subjectid])?$arr_count_f[0][$row->subjectid]:1;
								    	$count_m = isset($arr_count_m[0][$row->subjectid])?$arr_count_m[0][$row->subjectid]:1;
								    	//echo $count_m;
										$score_f = isset($arr_sub[$studentid][$row->subjectid]) ? ($arr_sub[$studentid][$row->subjectid]/$count_n) : 0;
										$score_m = isset($arr_sub_m[$studentid][$row->subjectid]) ? ($arr_sub_m[$studentid][$row->subjectid]/$count_m) : 0;
																	    	
										if($row->is_class_participation == 3) // attendance
										{
											$result_score_att = floor((($score_f+$score_m)/2)*100)/100;
											$meaning_="";
											if($q_meaning->num_rows() > 0){
									    		foreach ($q_meaning->result() as $row_meaning) {
									    			$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : '';
									    			$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : '';
									    			if(($min_score - 0 <= $result_score_att - 0) && ($result_score_att - 0 <= $max_score - 0)){
														$meaning_ = $row_meaning->mention;
									    			}
												}
								    		}	
								    		$total_att_subj = floor((($result_score_att*$row->gr_percent)/$row->max_score)*100)/100;
											$tr_cp1.= '<tr><td>'.$row->subject.'</td><td style="text-align:center;">'.$total_att_subj.'</td><td>'.$meaning_.'</td></tr>';
											$total_score_cp1 += $total_att_subj;
										}
										else if($row->is_class_participation == 2) // homework
										{
											
											$result_score_hw = floor((($score_f+$score_m)/2)*100)/100;
											//echo ($result_score_hw*$row->gr_percent)/$row->max_score;
											$meaning_= "";
											if($q_meaning->num_rows() > 0){
									    		foreach ($q_meaning->result() as $row_meaning) {
									    			$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : '';
									    			$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : '';
									    			if(($min_score - 0 <= $result_score_hw - 0) && ($result_score_hw - 0 <= $max_score - 0)){
														$meaning_ = $row_meaning->mention;
									    			}
												}
								    		}
								    		$subj_hw_score = floor((($result_score_hw*$row->gr_percent)/$row->max_score)*100)/100;
											$tr_cp1.= '<tr><td>'.$row->subject.'</td><td style="text-align:center;">'.$subj_hw_score.'</td><td>'.$meaning_.'</td></tr>';
											$total_score_cp1 += $subj_hw_score;
											//$grand_total     += $subj_hw_score;
										}	
										else if($row->is_class_participation == 1)// class participation
										{
											$result_score_cp = floor((($score_f+$score_m)/2)*100)/100;
											$meaning_ = '';
											if($q_meaning->num_rows() > 0){
									    		foreach ($q_meaning->result() as $row_meaning) {
									    			$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : '';
									    			$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : '';
									    			if(($min_score - 0 <= $result_score_cp - 0) && ($result_score_cp - 0 <= $max_score - 0)){
														$meaning_ = $row_meaning->mention;
									    			}
												}
								    		}
								    		$res_calcu_cp = floor(((($result_score_cp*$res_score_calc)/$max_score_calc)/3)*100)/100;
											$tr_cp1.= '<tr><td>'.$row->subject.'</td><td style="text-align:center;">'.$res_calcu_cp.'</td><td>'.$meaning_.'</td></tr>';
											$total_score_cp1 += $res_calcu_cp;
											$total_cp_amt += $result_score_cp;
											$ii++;
								    	}
								    	else
								    	{
								    		$meaning_ = "";
								    		if($q_meaning->num_rows() > 0){
									    		foreach ($q_meaning->result() as $row_meaning) {
									    			$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : '';
									    			$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : '';
									    			if(($min_score - 0 <= $score_f - 0) && ($score_f - 0 <= $max_score - 0)){
														$meaning_ = $row_meaning->mention;
									    			}
												}
								    		}	
								    	 	$result_final_subj = (($score_f*$res_score_calc)/$max_score_calc);
								    		$tr_cp2.= '<tr><td>'.$row->subject.'</td><td style="text-align:center;">'.$result_final_subj.'</td><td>'.$meaning_.'</td></tr>';
								    		$total_score_cp2 += $result_final_subj;
								    		$total_score_m_s += $score_m;
								    		$grand_fin_sub   += floor((($score_f*$row->calc_score)/$row->max_score)*100)/100;
								    		//$grand_mid += round(($score_m*$row->calc_score)/$row->max_score,2)-0;
								    		
								    		$jj++;
								    	}
								    							
								   	} 
								   	$result_mid = floor((($total_score_m_s*15)/100)*100)/100;
									$total_score_core_all = ($result_mid+$total_score_cp1+$total_score_cp2);
							?>
				<table style="width:100%; height:50%;" border="0">					
					<tr style="vertical-align: top;">
						<td style="width:50%;;">
						    <table border="1" style="height: 100%">
		                   	 	<thead id="color">
		                   	 		<tr style="height:25px;">
		                   	 			<th id="color" style="width:50%;">Core Subject with Sub Skill</th>
		                   	 			<th id="color" style="width:15%;">Score​</th>
		                   	 			<th id="color" style="width:35%;">Meaning</th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody class="tbscore">
		                   	 		<?php
	                   	 			echo $tr_cp2;
								 	?>   
		                   	 		<tr style="height:20px !important;">
		                   	 			<td class="totals">Total Score</td>
		                   	 			<td style="font-weight: bold;text-align: center;"><?php echo $total_score_cp2;?></td>
		                   	 			<td></td>
		                   	 		</tr>
		                   	 		<tr style="height:20px !important;">
		                   	 			<td class="totals">Mid-Term</td>
		                   	 			<td style="font-weight: bold;text-align: center;"><?php echo $result_mid;?></td>
		                   	 			<td></td>
		                   	 		</tr>		                   	 			                   	 		
		                   	 	</tbody>
		                    </table>
		        		</td>	
						<td style="width:50%;">
		                    <table border="1" style="height: 100%">
		                   	 	<thead>
		                   	 		<tr style="height:25px;">
		                   	 			<th id="color" style="width:50%;">Core On-going assessment</th>
		                   	 			<th id="color" style="width:15%;">Score​</th>
		                   	 			<th id="color" style="width:35%;">Meaning</th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody>
	                   	 			  
									<?php echo $tr_cp1;?>
								 	<tr>
		                   	 			<td class="totals">Total Score</td>
		                   	 			<td style="font-weight: bold;text-align: center;"><?php echo $total_score_cp1; ?></td>
		                   	 			<td></td>
		                   	 		</tr>  	 			                   	 		
		                   	 	</tbody>
		                    </table>
						</td>
					</tr>
				</table>
				
				<?php
					$tr_skill1 = "";
					$tr_skill2 = "";
					$total_score_sub_skill = 0;
					$total_score_cp_skill  = 0;	

					$total_score_m_skil = 0;
					$total_cp_m_sk      = 0;

					// ============
					$grand_total_skill  = 0;
					$result_total_skill = 0;
					$total_pc_skill     = 0;
					$jj = 0;
					foreach ($query_skill as $row_skill) {

						$q_meaning = $this->db->query("SELECT
															sm.submenid,
															sm.schoolid,
															sm.schlevelid,
															sm.yearid,
															sm.subjectid,
															sm.gradelevelid,
															sm.mentionid,
															sm.max_score,
															sm.minscrore,
															sm.active,
															cm.mention
														FROM
															sch_subject_mention AS sm
														LEFT JOIN sch_score_mention AS cm ON sm.mentionid = cm.menid
														WHERE sm.subjectid = '{$row_skill->subjectid}' ");		
						$count_skill    = isset($arr_count_f[1][$row_skill->subjectid]) ? $arr_count_f[1][$row_skill->subjectid] : 1;
						$count_skill_m  = isset($arr_count_m[1][$row_skill->subjectid]) ? $arr_count_m[1][$row_skill->subjectid] : 1;
						
						$score_skill_f = isset($arr_skill[$studentid][$row_skill->subjectid]) ? ($arr_skill[$studentid][$row_skill->subjectid]/$count_skill) : 0;
						$score_skill_m = isset($arr_sub_m_skil[$studentid][$row_skill->subjectid]) ? ($arr_sub_m_skil[$studentid][$row_skill->subjectid]/$count_skill_m) : 0;
						
						$res_score_calc_skill = $row_skill->calc_score;
						$max_score_calc_skill = $row_skill->max_score;
						if($row_skill->is_class_participation == 3 ){
							$result_att_skill = floor((($score_skill_f+$score_skill_m)/2)*100)/100;
							$meaning_ = '';
							if($q_meaning->num_rows() > 0){
								foreach ($q_meaning->result() as $row_meaning) {
									$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : '';
									$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : '';

									if(($min_score - 0 <= $result_att_skill - 0) && ($result_att_skill - 0 <= $max_score - 0)){
										$meaning_ = $row_meaning->mention;
									}
								}
							}
							$res_att_skill = floor((($result_att_skill*$res_score_calc_skill)/$max_score_calc_skill)*100)/100;
							$tr_skill1 .= '<tr><td>'.$row_skill->subject.'</td><td style="text-align:center;">'.$res_att_skill.'</td><td>'.$meaning_.'</td></tr>';
							$grand_total_skill += $res_att_skill;
							$result_total_skill	 += $res_att_skill;
							
						}
						else if($row_skill->is_class_participation == 2)
						{
							$result_hw_skill = floor((($score_skill_f+$score_skill_m)/2)*100)/100;
							$meaning_ = '';
							if($q_meaning->num_rows() > 0){
								foreach ($q_meaning->result() as $row_meaning) {
									$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : 0;
									$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : 0;

                                  	if(($min_score - 0 <= $result_hw_skill - 0) && ($result_hw_skill - 0 <= $max_score - 0)){
										$meaning_ = $row_meaning->mention;
									}
								}
							}
							$res_hw_skill = floor((($result_hw_skill*$res_score_calc_skill)/$max_score_calc_skill)*100)/100;
							$tr_skill1 .= '<tr><td>'.$row_skill->subject.'</td><td style="text-align:center;">'.$res_hw_skill.'</td><td>'.$meaning_.'</td></tr>';
							$grand_total_skill += $res_hw_skill;
							$result_total_skill+= $res_hw_skill;
							//echo $result_total_skill;
						}
						else if($row_skill->is_class_participation == 1)
						{
							$result_pc_skill = floor((($score_skill_f+$score_skill_m)/2)*100)/100;
							$meaning_ = '';
							if($q_meaning->num_rows() > 0){
								foreach ($q_meaning->result() as $row_meaning) {
									$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : '';
									$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : '';

									if(($min_score - 0 <= $result_pc_skill - 0) && ($result_pc_skill - 0 <= $max_score - 0)){
										$meaning_ = $row_meaning->mention;
									}
								}
							}
							$subj_pc_skill = floor(((($result_pc_skill*$res_score_calc_skill)/$max_score_calc_skill)/3)*100)/100;
							$tr_skill1 .= '<tr><td>'.$row_skill->subject.'</td><td style="text-align:center;">'.$subj_pc_skill.'</td><td>'.$meaning_.'</td></tr>';
							$grand_total_skill += $subj_pc_skill;
							//$total_pc_skill    += $subj_pc_skill;
							$jj++;
						}
						else{
							$meaning_ = '';
							if($q_meaning->num_rows() > 0){
								foreach ($q_meaning->result() as $row_meaning) {
									$min_score = isset($row_meaning->minscrore) ? $row_meaning->minscrore : '';
									$max_score = isset($row_meaning->max_score) ? $row_meaning->max_score : '';

									if(($min_score - 0 <= $score_skill_f - 0) && ($score_skill_f - 0 <= $max_score - 0)){
										$meaning_ = $row_meaning->mention;
									}
								}
							}
							$re_final_subj_skill = (($score_skill_f*$res_score_calc_skill)/$max_score_calc_skill);
							$tr_skill2 .= '<tr><td>'.$row_skill->subject.'</td><td style="text-align:center;">'.$re_final_subj_skill.'</td><td>'.$meaning_.'</td></tr>';
							$total_score_sub_skill += $re_final_subj_skill;
							$total_score_m_skil    += $score_skill_m;
						}						
					}
					$result_mid_skill   = floor((($total_score_m_skil*15)/100)*100)/100;
					$total_all_skill = ($result_mid_skill+$grand_total_skill+$total_score_sub_skill);
					$grand_total_all = floor((($total_all_skill+$total_score_core_all)/2)*100)/100;
					if($grand_total_all >= 99.99){
						$grand_total_all = 100;
					}

				?>
				<table style="width:100%;height:50%;" border="0">		
					<tr style="vertical-align: top;">
						<td style="width:50%;">
						   	<table style="width:100%; height: 100% !important;" border="1">
		                   	 	<thead id="color">
		                   	 		<tr style="height:25px;">
		                   	 			<th id="color" style="width:50%;">Skill Subject</th>
		                   	 			<th id="color" style="width:15%;">Score​</th>
		                   	 			<th id="color" style="width:35%;">Meaning</th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody class="tbskill">
		                   	 		<?php
									echo $tr_skill2;
								 	?>  
		                   	 		<tr>
		                   	 			<td class="totals">Total Score</td>
		                   	 			<td style="font-weight: bold;text-align: center;"><?php echo $total_score_sub_skill ?></td>
		                   	 			<td></td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td class="totals">Mid-Term</td>
		                   	 			<td style="font-weight: bold;text-align: center;"><?php echo $result_mid_skill;?></td>
		                   	 			<td></td>
		                   	 		</tr>		                   	 			                   	 		
		                   	 	</tbody>
		                   	</table>
				        </td>
						<td style="width:50%;">
		                   <table style="width:100%; height: 100% !important;" border="1">
		                   	 	<thead id="color">
		                   	 		<tr style="height:25px;">
		                   	 			<th id="color" style="width:50%;">Skill On-going assessment</th>
		                   	 			<th id="color" style="width:15%;">Score​</th>
		                   	 			<th id="color" style="width:35%;">Meaning</th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody>
		                   	 		  
									<?php echo $tr_skill1;?>
		                   	 		<tr>
		                   	 			<td class="totals">Total Score</td>
		                   	 			<td style="font-weight: bold;text-align: center;"><?= $grand_total_skill ?></td>
		                   	 			<td></td>
		                   	 		</tr>		                   	 			                   	 		
		                   	 	</tbody>
		                   </table>
						</td>
				</table>
				<?php
       	 			$sql_result_mention = $this->db->query("SELECT
																sch_score_mention_gep.menid,
																sch_score_mention_gep.mention,
																sch_score_mention_gep.men_type,
																sch_score_mention_gep.schlevelid,
																sch_score_mention_gep.grade,
																sch_score_mention_gep.min_score,
																sch_score_mention_gep.max_score,
																sch_score_mention_gep.is_past
																FROM
																sch_score_mention_gep
																WHERE 1=1 
																AND schlevelid = '".$schlevelid."'");
				    $grade_v = "";
				    $meaning = "";
				    $result_meaning = "";
				    if($sql_result_mention->num_rows() > 0){
					    foreach ($sql_result_mention->result() as $row_m) {
					    	if($row_m->min_score <= $grand_total_all && $row_m->max_score >= $grand_total_all){
					    		if($row_m->is_past == 1){
					    			$result_meaning = "Passed";
					    		}else{
					    			$result_meaning = "Fail";
					    		}
								$grade_v = $row_m->grade;
					    	}	
					   		$meaning.= '<tr><td>'.$row_m->min_score.'-'.$row_m->max_score.'</td>
								   		  <td style="text-align:center;">'.$row_m->grade.'</td>
								   		  <td>'.$row_m->mention.'</td>
								   		</tr>';							
					   	} 
					}
			 	?>            		
				<table style='width:99.5%;margin-top: 15px;margin-left: 3px;' border="1">	
					<thead id="color">
						<tr style="height:25px;">
							<th id="color_hight">Rank</th>
							<th id="color_hight">Grade</th>
							<th id="color_hight">Grand Total Score</th>
							<th id="color_hight">Result</th>
							<th id="color_hight">Next Level</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="text-align: center;"><?php echo $this->green->show_rank_gep($arr_rank_s);?></td>
							<td style="text-align: center;"><?php echo $grade_v;?></td>
							<td style="text-align: center;"><?php echo $grand_total_all;?></td>
							<td style="text-align: center;"><?php echo $result_meaning;?></td>
							<td></td>
						</tr>		                   	 			                   	 		
					</tbody>
				</table>			
				<table style="width:100%;margin-top: 10px;">
					<tr style="vertical-align: top;">
						<td style="width:40%;">	
		                   	<table style="max-height: 5px;" border="1">
		                   	 	<thead>
		                   	 		<tr style="height: 20px;">
		                   	 			<th colspan="3" id="color_hight">Grading System</th>
		                   	 		</tr>
		                   	 		<tr style="height: 20px;">
		                   	 			<th id="hights">Score</th>
		                   	 			<th id="hights">Grade</th>
		                   	 			<th id="hights">Meaning</th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody class="tbscore">
		                   	 		<?php echo $meaning;?>	 			                   	 		
		                   	 	</tbody>
		                   	</table>
				        </td>
						<td>
						    <table border="1" style="width:100%;">
		                   	 	<thead id="color">
		                   	 		<tr style="height: 20px;"><th id="color_hight">Teacher's Comment</th></tr>
		                   	 	</thead>
		                   	 	<tbody>

		      						<tr style="vertical-align: top;"><td style="height: 138px;">
		      							<?php echo $command_student; ?>
		      						</td></tr>
		                   	 	</tbody>
		                    </table>            
						</td>
					</tr>
				</table>
				<table style="width:100%;margin-bottom: 100px;">
					<tr>
						<td style="text-align: left;">
							<label>Date:&nbsp;&nbsp;&nbsp; <?php echo $date_academic;?></label><br>
							<label style="padding-left:5%;">Academic Manager</label>
							<h5 class="l_foter" style="margin-left: 50px;margin-top: 50px;"><?php echo strtoupper($academicname);?></h5>
						</td>
						<td style="text-align: right;">
							<label style="margin-right: 30px;">Date: <?php echo $date_teacher;?></label><br>
							<label style="padding-right:5%;">Teacher's Signature</label>
							<h5 class="l_foter" style="margin-right: 50px;margin-top: 50px;"><?php echo strtoupper($teachername);?></h5>
						</td>
					</tr>
					<!-- <tr>
						<td style="text-align: left;">
							<h5 class="l_foter" style="margin-left: 50px;margin-top: 50px;">HUOTH PEO</h5>
						</td>
						<td></td>
					</tr> -->
				</table>
		</div>
	</center>		
</div>

<script type="text/javascript">
	$(function(){
		$("#a_print").on("click", function (e) {
	           gPrint('print_all');
    	}); 
	});
</script>