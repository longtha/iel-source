<!-- <div id='msg' style='color:white; background-color:#286090;height:30px;  text-align:center'></div> -->

<!--show form search-->
<div class="wrapper">
 	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
    		<div class="result_info">
                <div class="col-sm-6" style="line-height:37px;">
                    <strong><?php echo $this->lang->line("general khmer language report");?></strong>                    
                </div>
            </div>
			<!--form element-->
			<!--<div class="alert alert-danger show_alert" style="margin:5px 0 5px 0; padding:5px 0 7px 12px;"></div>-->
	      	<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" class="tdrow" id="frm_search_report">    
		        <div class="col-sm-12">
		        	<div class="row" style="margin-bottom:-19px;">
		        		<div class="panel panel-default">
		        			<div class="panel-body">					        						        	
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_id">School</label>
					        			<select name="school_id" id="school_id" minlength='1' class="form-control">
					                    	<?php
												$row = $this->school->getschinfor_row(1);											
												echo '<option value="'. $row->schoolid .'">'. $row->name .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="program_id">Program</label>
					        			<select name="program_id" id="program_id" minlength='1' class="form-control" required data-parsley-required-message="Select Program">
					                    	<?php
												$row = $this->program->getprogram(1);											
												echo '<option value="'. $row->programid .'">'. $row->program .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_level_id">School Level<!--<span style="color:red">*</span>--></label>
					        			<select name="school_level_id" id="school_level_id" minlength='1' class="form-control" required data-parsley-required-message="Select School Level">
		                                    <option value=""></option>
					                    	<?php
												$get_level_data = $this->school_level->getsch_level(1);												
												foreach($get_level_data as $r_level){
													echo '<option value="'. $r_level->schlevelid .'">'. $r_level->sch_level .'</option>';
												}
											?>	                      
		                                </select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="adcademic_year_id">Academic Year<!--<span style="color:red">*</span>--></label>
	                                    <select name="adcademic_year_id" id="adcademic_year_id" minlength='1' class="form-control" ></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="grade_level_id">Grade Level<!--<span style="color:red">*</span>--></label>
	                                    <select name="grade_level_id" id="grade_level_id" minlength='1' class="form-control" required data-parsley-required-message="Select Grade Level"></select>
					        		</div>
					        	</div>					        				        	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="class_id">Class Name<!--<span style="color:red">*</span>--></label>					        			
	                                    <select name="class_id" id="class_id" minlength='1' class="form-control" required data-parsley-required-message="Select Class Name"></select>
					        		</div>
					        	</div>					    			        					        	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="student_id">Student Name</label>
	                                    <select name="student_id" id="student_id" class="form-control"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="student_id">Type of Report</label>
	                                    <select name="report_type" id="report_type" class="form-control" required data-parsley-required-message="Select Type of Report">
	                                    	<option value=""></option>
	                                    	<option value="1">Monthly report - class ranking</option>
	                                    	<option value="2">Monthly report - individual ranking</option>
	                                    	<option value="3">Semester report - class ranking</option>
	                                    	<!-- <option value="4">Semester report - individual ranking</option>
	                                    	<option value="5">Yearly report - class ranking</option>
	                                    	<option value="6">Yearly report - individual ranking</option>	 -->
	                                    </select>
					        		</div>
					        	</div>
					        	<div class="col-md-12">
					        		<div class="form-group">
					        			<label class="req" for="">Type of Result<!--<span style="color:red">*</span>--></label><br>
					        			<div class="result_type" id='result_type' style="text-align:center; border:1px #337ab7 solid; padding:5px 5px 0 5px;"> 						
											<table border="0"​ align="center" id='tbl-result-type' class="tbl-result-type table table-bordered">
												<tr style="color:#337ab7 !important;">
					        						<td>
					        							<label class="radio-inline">
			                                            	<input type="radio" name="select_result_type" value='1' class='select_result_type checked'>Monthly
			                                            </label> 
					        						</td>
					        						<td>
					        							<label class="radio-inline">
			                                            	<input type="radio" name="select_result_type" value='2' class='select_result_type checked'>Semester
			                                            </label> 
					        						</td>
					        						<td>
					        							<label class="radio-inline">
			                                            	<input type="radio" name="select_result_type" value='3' class='select_result_type checked'>Final
			                                            </label> 
					        						</td>
					        					</tr>
					        					<tr class="show_resutlt_type" style="display:none;"></tr>	
											</table>
										</div>                                    
					        		</div>
					        	</div>					        	
		        			
					        	<!--button-->
						        <div class="col-sm-12">	
					                <div class="form-group" style="text-align:center;">
						                <?php if($this->green->gAction("R")){ ?>
						                	<input type="button" name="btnsearch" id='btnsearch' value="Search" class="btn btn-primary" />
						                <?php } ?>
						                <input type="button" name="btncancel" id='btncancel' value="Cancel" class="btn btn-warning" />
					                </div>                            
						        </div><!--end button-->			
		        			</div>		        			
		        		</div>		        				        					        	
		        	</div>      
		        </div>		             
		    </form><!--end form element-->		     	
	    </div>    
	</div>  
</div><!--end show form search-->

<!--show result report-->
<div class="wrapper div_report" style="display:none;">
 	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
    		<div class="result_info">
                <div class="col-sm-6" style="line-height:37px;">
                    <strong><?php echo $this->lang->line("result report");?></strong>                    
                </div>
                <div class="col-sm-6" style="text-align:right; line-height:35px;">
                    <span class="top_action_button">
		      			<?php if($this->green->gAction("P")){ ?>
							<a href="javascript:void(0)" id="print" title="Print">
				    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
				    		</a>
				    	<?php } ?>
		      		</span>                  
                </div>

                <!--show result of monthly report-->
				<div class="col-sm-12">          
					<div class="row show_result">

					</div>
				</div><!--end show result of monthly report-->
            </div>
        </div>
    </div>
</div><!--end show result report-->

<style type="text/css">	
	.tbl-result-type td{
		text-align: left;
		vertical-align: middle !important;
		border: hidden !important;		
	}
</style>

<script type="text/javascript">
	$(function(){
		$('#year, #schlevelid').hide();
		$("#school_level_id").val($("#school_level_id first").val());

		//get year and class data---------------------------------
		$('#school_level_id').change(function(){
			var school_level_id = $(this).val();
			var school_id = $('#school_id').val();
			var program_id = $('#program_id').val();

			if(school_level_id != ""){
				$.ajax({
		            url: "<?php echo site_url('student/c_subject_score_entry_kgp/get_year_and_grade_data') ?>",	            
		            type: "post",
		            dataType: 'json',
		            data: {'school_id': school_id, 'program_id': program_id, 'school_level_id': school_level_id},
		            success: function (data) {
		            	//console.log(data);
		            	//get year--------------------------------
		            	var getYear = '';	            	
		            	$.each(data["year"], function(k,v){
		            		getYear += '<option value="'+ v.yearid +'">'+ v.sch_year+'</option>';		            		
		            	}); $('#adcademic_year_id').html(getYear);

		            	//get grade--------------------------------
		            	var getGrade = '';
		            	$.each(data["grade"], function(ke,re){
		            		getGrade += '<option value="'+ re.grade_levelid +'">'+ re.grade_level+'</option>';
		            	});	$('#grade_level_id').html(getGrade);

		            	//get teacher------------------------------
		            	var grade_level_id = $('#grade_level_id').val();
		            	var adcademic_year_id = $('#adcademic_year_id').val();
		            	var url = "<?php echo site_url('reports/generalkhmerlanguage/get_class_data') ?>";
						var data = 'program_id='+program_id+'&school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
						getDataByAjax($('#class_id'), url, data);
						$('#class_id').html('');
						$('#student_id').html('');

						//remove message require-------------------
						var aId = $('#adcademic_year_id').data('parsley-id');
						var gId = $('#grade_level_id').data('parsley-id');

						$('#parsley-id-'+aId).remove();	   
						$('#adcademic_year_id').removeClass('parsley-error');

						$('#parsley-id-'+gId).remove();	   
						$('#grade_level_id').removeClass('parsley-error');        	
				    }
		        });	
			}else{
				$('#adcademic_year_id').html('');
				$('#grade_level_id').html('');
				$('#class_id').html('');
				$('#student_id').html('');
			}
		});
		
		//get class data--------------------------------------------
		$('#grade_level_id').change(function(){
			var program_id = $('#program_id').val();
			var school_level_id = $('#school_level_id').val();
			var adcademic_year_id = $('#adcademic_year_id').val();
			var grade_level_id = $(this).val();
			var url = "<?php echo site_url('reports/generalkhmerlanguage/get_class_data') ?>";
			var data = 'program_id='+program_id+'&school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
			getDataByAjax($('#class_id'), url, data);
		});

		//get student data------------------------------------------
		$('#class_id').change(function(){
			var class_id = $(this).val();		
			var schoolid = $("#school_id").val();
			var programid = $("#program_id").val();
			var schlevelid = $("#school_level_id").val();
			var yearid = $("#adcademic_year_id").val();	
			var gradlavelid = $("#grade_level_id").val();	
			if(class_id == ""){
				$('#student_id').html('');
			}else{
				//var url = "<?php //echo site_url('student/c_subject_score_entry_kgp/get_student_data') ?>";
				var url = "<?php echo site_url('reports/generalkhmerlanguage/get_student_data') ?>";
				var data = {'class_id':class_id,
							'schoolid':schoolid,
							'programid':programid,
							'schlavelid':schlevelid,
							'yearid':yearid,
							'gradlavelid':gradlavelid
						};
				getDataByAjax($('#student_id'), url, data);
			}
		});

		//choose exame type-----------------------------------------
		$('.select_result_type').click(function(){
			$('.show_resutlt_type').html('');	
			var month_data = ['January', 'February', 'March', 'April-May', 'June', 'July', 'August', 'September', 'Octomber', 'November', 'December'];
			var getVal = $(this).val();
			var opt = ''; 
			var elements = '';
			var school_id = $('#school_id').val();
			var program_id = $('#program_id').val(); 
			var school_level_id = $('#school_level_id').val();
			var adcademic_year_id = $('#adcademic_year_id').val();

			//monthly=========
			if(getVal == 1){
				for(i = 1; i <= month_data.length; i++) {
					opt += '<option value="'+ i +'">'+ month_data[i-1] +'</option>';
				}

				elements += '<td colspan="3">'+
								'<select name="get_result_type" class="form-control get_result_type" style="width:910px; border-color:#337ab7;">'+ opt +'</select>'+
					        '</td>';
			}
			//semester========
			else if(getVal == 2){

				elements += '<td colspan="3"><select name="get_result_type" class="form-control get_result_type get_semester_data" style="width:910px; border-color:#337ab7;"></select></td>';
				$.ajax({
					url:"<?php echo site_url('student/c_subject_score_entry_kgp/get_semester') ?>",
					type:'POST',
					dataType: 'json',
		            data:{	'school_id':school_id,
		            		'program_id':program_id,
		            		'school_level_id':school_level_id,
		            		'adcademic_year_id':adcademic_year_id
		            	 },
		            success: function (data){
		            	$.each(data, function(k, v){
		            		opt += '<option value="'+ v.semesterid +'">'+ v.semester +'</option>';
		            	});
		            	           	
				        $('.get_semester_data').html(opt);
		            }
				});
			}
			//final===========
			// else{
			// 	elements += '<td colspan="3" class="show_semester_data"></td>';

			// }			

			$('.show_resutlt_type').show().append(elements);
		});


		$('#report_type').change(function() {
			var reportType = $('#report_type').val();
			if (reportType == 1 || reportType == '') {
				var url = "<?php echo site_url('reports/generalkhmerlanguage/get_student_data') ?>";
				//var data = 'class_id='+ $('#class_id').val();
				var class_id = $('#class_id').val();		
				var schoolid = $("#school_id").val();
				var programid = $("#program_id").val();
				var schlevelid = $("#school_level_id").val();
				var yearid = $("#adcademic_year_id").val();	
				var gradlavelid = $("#grade_level_id").val();
				var data = {'class_id':class_id,
							'schoolid':schoolid,
							'programid':programid,
							'schlavelid':schlevelid,
							'yearid':yearid,
							'gradlavelid':gradlavelid
							};
				getDataByAjax($('#student_id'), url, data);
			}
			else {
				
				if ($("#student_id").find("option").eq(0).val() == '') {
					$("#student_id").find("option").eq(0).remove();
				}
			}
		});


		//search report---------------------------------------------
		$('#btnsearch').click(function(){

			if($('#frm_search_report').parsley().validate()){
				$('.show_alert').html('').hide();			
				var data = $("#frm_search_report").serialize();			
				var program_id = $('#program_id').val();
				var school_level_id = $('#school_level_id').val();
				var adcademic_year_id = $('#adcademic_year_id').val();
				var grade_level_id = $('#grade_level_id').val();
				var class_id   = $('#class_id').val();			
				var student_id = $('#student_id').val();
				var get_result_type = $('.get_result_type').val();
				var reportType  = $('#report_type').val();
				
				var url = "";

				if(school_level_id != ''  && adcademic_year_id != '' &&grade_level_id != '' && reportType != '' && get_result_type != ''){

					if(reportType == 1){

						// $('#student_id').removeAttr('required');
						// if($('.radio-inline').val("")){
						//  	toastr["warning"](" Please check type of result first before process...!");
						// }
						// if(class_id == '-1'){
						// 	url = "<?php //echo site_url('reports/generalkhmerlanguage/primary_monthly_report_class_ranking_all') ?>";
						// }else{							

							url = "<?php echo site_url('reports/generalkhmerlanguage/primary_monthly_report_class_ranking') ?>";

						//}	

					}
					else if(reportType == 2){
					
						url = "<?php echo site_url('reports/generalkhmerlanguage/primary_monthly_report_individual_ranking') ?>";
					}
					else if(reportType == 3){
					
						url = "<?php echo site_url('reports/generalkhmerlanguage/primary_yearly_report_individual_ranking') ?>";
					}

					$.ajax({
			            url: url,	            
			            type: "post",
			            dataType: 'html',
			            data: data,
			            success: function (data) {	
			            	$('.div_report').show();
			            	$('.show_result').html(data);
			            }
			        });

				}else{
					
					$('.required').each(function(){
						var getValue = $(this).val();
						if(getValue == ''){
							var mess = "<?php echo $this->lang->line('please choose');?> "+ $(this).attr('name') +" <?php echo $this->lang->line('that you want to show');?>";
							$('.show_alert').show().html(mess);
						}
					});
				}
			}			
		});

		//print report------------------------------------------
		$("#print").on("click",function(){
		   	var data = $(".show_result").html();
		   	gsPrint(data);
		});
	});

	/*-------------------------------------------FUNCTION------------------------------------------*/

	//function ajax to get data
	function getDataByAjax(selector, url, data){
		$.ajax({
            url: url,	            
            type: "post",
            dataType: 'html',
            data: data,
            success: function (data) {
            	selector.html(data);		            	
            }
        });	
	}

	//function print---------------------------------
	function gsPrint(data){
		var element = "<div id='print_area' style='margin:0 5px 0 5px;'>"+data+"</div>";
		$("<center>"+element+"</center>").printArea({
			mode:"popup",  //printable window is either iframe or browser popup              
			popHt: 1000 ,  // popup window height
			popWd: 1024,  // popup window width
			popX: 0 ,  // popup window screen X position
			popY: 0 , //popup window screen Y position
			popTitle:"General Khmer Language", // popup window title element
			popClose: false,  // popup window close after printing
			strict: false 
		});
	}

</script>

