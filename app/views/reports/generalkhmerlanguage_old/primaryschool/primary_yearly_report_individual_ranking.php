<div class="panel panel-default" style="margin-top:50px;">    
	<div class="panel-body" style=" border: 1px solid #000">
		<!-- *************************************header report***************************************** -->		
		<!-- <div class="col-sm-12"> -->
			<div class="col-sm-12" style="padding-top:5px;">
				<a class="navbar-brand" href="javascript:">
                    <img src="<?php echo base_url('assets/images/logo/logo.png')?>"  />
                </a>
			</div>
		<!-- </div> -->
		
		<div class="col-sm-12" style="text-align:center;​ margin-bottom:50px;">
			<span class="titles_2">របាយការណ៍សិក្សាប្រចាំឆ្នាំ</span><br>
			<span style="font-size:16pt;">Annual Academic Report</span>
		</div>
		
		<!-- ***********************************end header report************************************** -->

		<!-- ***************************************content report************************************* -->
		
		<!--student info-->
		<div class="col-sm-12" style="margin-top:50px; margin-bottom:10px;">
			<div class="col-sm-8">
				<span>សិស្សឈ្មោះ: 
					<?php 

						echo "<b>".(isset($row->studentname) ? $row->studentname : '')."</b>";
					?>
				</span>
			</div>

			<div class="col-sm-4"​>
				<span>រៀនថ្នាក់ទី: 
					<?php 
						
						echo "<b>".(isset($row->class_name) ? $row->class_name : '')."</b>";
					?>
				</span>
			</div>

			<div class="col-sm-8" style="margin-bottom:25px;">
				<span>អត្តលេខ: 
					<?php 
						
						echo "<b>".(isset($row->student_num) ? $row->student_num : '')."</b>";
					
					?>
				</span>
			</div>

			<div class="col-sm-4" style="margin-bottom:25px;">
				<span>ឆ្នាំសិក្សា: 
					<?php
						if(isset($row->sch_year)){
							$arr_year = explode('(', $row->sch_year);
							echo($arr_year[0]);
						}
					?>
				</span>
			</div>
			<!--space-->
			<div class="col-sm-8" style="font-size:11pt; font-weight:600;">
				<span>មធ្យមភាគប្រចាំឆ្នាំ: 
					<?php 
						
						echo "<b>".(isset($row->average_score) ? $row->average_score : '')."</b>";

					
					?>
				</span>
			</div>
			
			<div class="col-sm-4" style="font-size:11pt; font-weight:600;">
				<span>ចំណាត់ថ្នាក់ប្រចាំឆ្នាំៈ
					<?php 
						echo "<b>".(isset($row->ranking_bysubj) ? $row->ranking_bysubj : '')."</b>";
						
					?>
				</span>
			</div>

			<div class="col-sm-8">ចំនួនអវត្តមានប្រចាំឆ្នាំៈ
					<?php
						echo "<b>".(isset($row->absent) ? $row->absent : '')."</b>";
					?>
				ដង
			</div>

			<div class="col-sm-4">
				<span>មានច្បាប់សរុបៈ 
					<?php 
						echo "<b>".(isset($row->present) ? $row->present : '')."</b>";
					?>
				ដង
				</span>
			</div>	
		</div><!--end student info-->

		<!--teacher comment-->
		<div class="col-sm-12">
			<div class="panel panel-default" style=" border: 1px solid #000">
				<div class="span8" style="margin:5px 20px -2px 20px;">មតិយោបល់របស់គ្រូ / Teacher's Comments</div>
				<div class="span12" style="margin:3px 20px 20px 20px; line-height:20px;">
					<input type="text" class="text-comment" />
					<input type="text" class="text-comment" />
					<input type="text" class="text-comment" />
					<input type="text" class="text-comment" />
					<input type="text" class="text-comment" />
					<input type="text" class="text-comment" />
					<input type="text" class="text-comment" />
					<input type="text" class="text-comment" />
				</div>
			</div>	 							
		</div><!--end teacher comment-->
		<!-- *************************************end content report********************************** -->

		<!-- ***************************************footer report************************************* -->
		<div class="col-sm-12"​ style="text-align: right; line-height: 22px; margin-top:-10px;">
			<span style="margin-right:25px;">ថ្ងៃទី...................ខែ...................ឆ្នាំ...................<br>
			<span class="texts" style="margin-right:120px;">គ្រូប្រចាំថ្នាក់</span>
			<div class="span8" style="margin-left:115px; margin-top:40px;"></div>
			<span style="margin-right:70px;​"><strong>ឈ្មោះ</strong></span>
		</div>

		<div class="col-sm-12"​ style="text-align: center; line-height: 22px; margin-top:-65px;">
			<span style="margin-left:20px;">បានឃើញ និង ពិនិត្យត្រឹមត្រូវ</span><br>
			<span style="margin-left:10px;">ថ្ងៃទី...................ខែ...................ឆ្នាំ...................</span><br>
			<span class="texts" style="margin-left:20px;​">ប្រធានការិយាល័យសិក្សា</span><br>
			<div class="span8" style="margin-left:115px; margin-top:40px;"></div>
			<span style="margin-left:150px;​"><strong>ឈ្មោះ</strong></span>
		</div>

		<div class="col-sm-12"​ style="line-height: 22px; margin-top:-50px; margin-bottom:50px;">
			<span style="margin-left:80px;">បានឃើញ និង ឯកភាព</span><br>
			<span style="margin-left:10px;">ថ្ងៃទី...................ខែ...................ឆ្នាំ...................</span><br>
			<span class="texts" style="margin-left:110px;​">នាយក</span><br>
			<div class="span8" style="margin-left:115px; margin-top:40px;"></div>
			<span style="margin-left:170px;​"><strong>ឈ្មោះ</strong></span>
		</div>	
		<!-- **************************************end footer report********************************** -->
	</div>
</div>

<style>
   /*--table--*/
   .texts{
   		font-family:'Kh Muol';
   }
   .titles_1{
	   	font-family: 'Kh Muol';
	   	font-size:13pt;
	   	text-align: center;
	   	line-height: 25px;  	
   }
   .titles_2{
	   	font-family: 'Khmer Kolab';
	   	font-size:18pt;
	   	text-align: center;
	   	color: #ec971f;   	
   }
   .text-comment{
   		border-top: hidden;
   		border-right: hidden;
   		border-left: hidden;
   		border-bottom: 1px #ccc dotted;
   		width: 900px;
   }
   .col-sm-6, .col-sm-4{
   		line-height: 22px;
   }
   th{
	   	text-align: center;
	   	vertical-align: middle !important;
   }

</style>

<script type="text/javascript">
	$(function(){
		$('body').delegate('.text-comment', 'keyup', function() {
			var comment = $(this).val();
			$(this).attr('value', comment);
		});
	});
</script>