<div class="panel panel-default" style="margin-top:5px;">    
	<div class="panel-body" style="border: 1px solid #000">
		<!-- *************************************header report***************************************** -->
		<div class="col-sm-12">
			<img width="50" height="50" src="<?php echo base_url('assets/images/logoiel.png')?>" />
			<span​​​ class='titles' style="padding-top:5px;">សាលាអន្តរជាតិ អាយ អ៊ី អ៊ែល</span>
		</div>

		<div class="col-sm-12 titles" style="margin-bottom:5px;padding-top:10px; text-align: center;">
			<span>របាយការណ៏សិក្សាប្រចាំខែ
				<?php 
					echo "<b>".$this->lang->line("month_".$month_name)."</b>";
				?>
			</span><br>
			<span>Study Report in December</span>
		</div>

		<div class="col-sm-12" style="margin-top:25px;">
			<div class="col-sm-4" style="text-align:right;​">
				ឈ្មោះសិស្សៈ <span class="texts">
						<?php
							echo "<b>".(isset($row->studentname) ? $row->studentname : '')."</b>";
						?>
				</span>
			</div>

			<div class="col-sm-4" style="text-align:center;">
				<span>រៀនថ្នាក់ទីៈ 
					<?php
						echo "<b>".(isset($row->class_name) ? $row->class_name : '')."</b>";
					?>
				</span>
			</div>

			<div class="col-sm-4" style="text-align:center;">
				<span>ឆ្នាំសិក្សាៈ
					<?php
						// if(!empty($year)){
						// 	$arr_year = explode('(', $year['year']);
						// 	echo($arr_year[0]);
						// }

						if(isset($row->sch_year)){
							$arr_year = explode('(', $row->sch_year);
							echo($arr_year[0]);
						}
					?>
				</span>
			</div>

			<div class="col-xs-6 col-md-4"></div>
			<div class="col-xs-6 col-md-8" style="text-align:right; margin-left:90px;">ពិន្ទុ និងការវាយតម្លៃមុខវិជ្ជានីមួយៗ / Score and Evaluation of Each Subject</div>
		</div>		
		<!-- ***********************************end header report************************************** -->

		<!-- ***************************************content report************************************* -->
		<!--table 1 table table-bordered-->
		<div class="col-sm-6" style="margin-bottom:-10px;">
			<div class="span8">ផ្នែកៈ ចំណេះទូទៅភាសាខ្មែរ</div>
			<div class="table-responsive">							
			<table border="1"​ align="center" id='listsubject' class="">
				<?php
					echo $tr;
				?>
			    <tbody class="tbodys">
			    	
			    </tbody>
			</table>
		</div>	 							
		</div><!--end table 1-->

		<!--table 2-->
		<div class="col-sm-6" style="margin-bottom:-10px;">
			<div class="span8">ផ្នែកៈ ភាសាអង់គ្លេស ចិន កុំព្យូទ័រ និង ជំនាញផ្សេងៗ</div>
			<div class="table-responsive">							
			<table border="1"​ align="center" id='listsubject' class="">

				<?php
					echo $trr;
				?>
			   
			    <tbody class="tbodys">
			    	
			    </tbody>
			</table>
		</div>	 		
		</div><!--end table 2-->

		<!--total score and average-->
		<div class="col-sm-12" style="margin-top:15px;">
			<div class="col-sm-4">
				<span>ពិន្ទុសរុប​ / Total:  
				<?php 
						echo "<b>".(isset($gtotal) ? $gtotal : '0.00')."</b>";
				?>
				</span>
			</div>

			<div class="col-sm-4" style="text-align:center;">
				<span>មធ្យមភាគ / Average: 
					<?php
						echo "<b>".(isset($rows->average_score) ? $rows->average_score : '0')."</b>";
					?>
				</span>
			</div>

			<div class="col-sm-4" style="text-align:right;">
				<span>ចំណាត់ថ្នាក់ / Rank: 
					<?php
						echo "<b>".(isset($rows->ranking_bysubj) ? $rows->ranking_bysubj : '')."/".(isset($total_row) ? $total_row : '')."</b>";
					?>
				</span>
			</div>
			<!--space-->
			<div class="col-sm-4">
				<span>ចំនួនអវត្តមានសរុបៈ 
				<?php
					echo "<b>".(isset($rows->absent) ? $rows->absent : '0')."</b>";
				?>
				 ដង</span>
			</div>

			<div class="col-sm-4"></div>

			<div class="col-sm-4" style="text-align:right;">
				<span>មានច្បាប់ៈ 
					<?php
						echo "<b>".(isset($rows->present) ? $rows->present : '0')."</b>";
					?>
				 ដង</span>
			</div>
		</div>
	<!--end total score and average-->

	<!--teacher comment-->
	<div class="col-sm-12" style="margin-top:10px;">
		<div class="panel panel-default" style=" border: 1px solid #000">
			<div class="span8" style="margin:5px 20px -3px 20px;">មតិយោបល់របស់គ្រូ / Teacher's Comments</div>
			<div class="span12" style="margin:3px 20px 20px 20px; line-height:20px;">
				<input type="text" class="text-comment" />
				<input type="text" class="text-comment" />
				<input type="text" class="text-comment" />
				<input type="text" class="text-comment" />
			</div>
		</div>	 							
		</div><!--end teacher comment-->
		<!-- *************************************end content report********************************** -->

		<!-- ***************************************footer report************************************* -->
		<div class="col-sm-6"​ style="line-height: 22px; margin-top:-15px;">
			<span style="margin-left:80px;">បានឃើញ និង ឯកភាព</span><br>
			<span style="margin-left:10px;">ថ្ងៃទី...................ខែ...................ឆ្នាំ...................</span><br>
			<span class="texts" style="margin-left:80px;​">ប្រធានការិយាល័យសិក្សា</span><br>
			<div class="span8" style="margin-left:115px; margin-top:40px;"></div>
			<span style="margin-left:170px;​"><strong>ឈ្មោះ</strong></span>
		</div>

		<div class="col-sm-6"​ style="text-align: right; line-height: 22px; margin-top:-15px;">
			<span style="margin-right:25px;">ថ្ងៃទី...................ខែ...................ឆ្នាំ...................<br>
			<span class="texts" style="margin-right:120px;">គ្រូប្រចាំថ្នាក់</span>
			<div class="span8" style="margin-left:115px; margin-top:40px;"></div>
			<span style="margin-right:70px;​"><strong>ឈ្មោះ</strong></span>
		</div> 

		<!--parents comment-->
		<div class="col-sm-12">
			<div class="panel panel-default" style=" border: 1px solid #000">
				<div class="span8" style="margin:5px 20px -3px 20px;">មតិមាតាបិតាសិស្ស / Guardian's Comments</div>
				<div class="span12" style="margin:3px 20px 20px 20px; line-height:20px;">
					<input type="text" class="text-comment" />
					<input type="text" class="text-comment" />
					<input type="text" class="text-comment" />
					<input type="text" class="text-comment" />
				</div>
			</div>	 							
		</div><!--end parents comment-->

		<div class="col-sm-6"​ style="line-height: 22px; margin:-15px 0 40px 0;">
			<span><strong>សូមប្រគល់សៀវភៅតាមដាននេះមកសាលាអោយបានមុនថ្ងៃទី៖</strong></span><br>
			<span>Please Return the book to school before:</span><br>
			<span><input type="text" class="text-comment"  style="width:255px;" /></span>
		</div>

		<div class="col-sm-6"​ style="text-align: right; line-height: 22px; margin:-15px 0 40px 0;">
			<span>ថ្ងៃទី...................ខែ...................ឆ្នាំ...................<br>
			<span><strong>ហត្ថាលេខា និង ឈ្មោះមាតាបិតា រឺ អាណាព្យាបាល</strong></span>		 						
		</div> 		
		<!-- **************************************end footer report********************************** -->
	</div>
</div>

<style>
   /*--table--*/
   .texts{
   		font-family:'Kh Muol';
   }
   .titles{
	   	font-family: 'Kh Muol';
	   	font-size:13pt;
	   	text-align: center;
	   	line-height: 25px;   	
   }
   .text-comment{
   		border-top: hidden;
   		border-right: hidden;
   		border-left: hidden;
   		border-bottom: 1px #ccc dotted;
   		width: 900px;
   }
   .col-sm-6, .col-sm-4{
   		line-height: 22px;
   }
   th{
	   	text-align: center;
	   	vertical-align: middle !important;
   }

</style>

<script type="text/javascript">
	$(function(){
		$('body').delegate('.text-comment', 'keyup', function() {
			var comment = $(this).val();
			$(this).attr('value', comment);
		});
	});
</script>