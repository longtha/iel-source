<?php
	for($i=0;$i<4;$i++){
?>
	<div class="panel panel-default" style="margin-top:5px;">
		<div class="panel-body">
			<!-- *************************************header report***************************************** -->
			<div class="col-sm-12 titles">
				<span>ព្រះរាជាណាចក្រកម្ពុជា</span><br>
				<span>ជាតិ   សាសនា   ព្រះមហាក្សត្រ</span>
			</div>

			<div class="col-sm-4 sub-titles">
				<span>មន្ទីរអប់រំ  យុវជន និងកីឡារាជធានីភ្នំពេញ</span><br>
				<span>សាលាអន្តរជាតិ អាយ អ៊ី អ៊ែល</span><br>
				<span style="margin-left:20px;">ថ្នាក់ទីៈ  
					<?php 
						if(!empty($class_name)){
							echo "<b>".$class_name["class_name"]."</b>";
						}
					?>
				</span>
			</div>

			<div class="col-sm-4 sub-titles" style="text-align: center; margin-top:35px; text-decoration: underline;">
				<span>ចំណាត់ថ្នាក់ប្រចាំខែ  
					<?php 
						//echo $month_name;
						echo "<b>".$this->lang->line("month_".$month_name)."</b>";
					?>
				</span>
			</div>

			<div class="col-sm-4 sub-titles" style="text-align: right; margin-top:20px;">
				<span>ឆ្នាំសិក្សា  
					<?php 
						//if(isset($sch_year)){
							$arr_year = explode('(', $sch_year['sch_year']);
							echo($arr_year[0]);
						//}
					?>
				</span>
			</div>
			<!-- ***********************************end header report************************************** -->

			<!-- ***************************************content report************************************* -->
			<div class="col-sm-12" style="margin-top:10px;">
				<div class="table-responsive">							
					<table border="0"​ align="center" id='listsubject' class="table table-bordered">
					    <thead>
					    	<tr>								        
						        <th width="70"><?php echo $this->lang->line("no");?></th>
						        <th><?php echo $this->lang->line("studentname");?></th>
						        <th width="50"><?php echo $this->lang->line("gender");?></th>
						        <th width="70"><?php echo $this->lang->line("classname");?></th>
						        <th width="90"><?php echo $this->lang->line("average");?></th>
						        <th width="90"><?php echo $this->lang->line("ranking");?></th>
						        <th width="90"><?php echo $this->lang->line("grade");?></th>
						        <th width="200"><?php echo $this->lang->line("remark");?></th>
						    </tr>
					    </thead>
			    		<tbody class="tbodys"></tbody>
					</table>
				</div>
			</div>
			<!-- *************************************end content report********************************** -->

			<!-- ***************************************footer report************************************* -->

			<div class="col-sm-6"​ style="line-height: 22px; margin-bottom:50px;">
				<span style="margin-left:80px;">បានឃើញ និង ឯកភាព</span><br>
				<span style="margin-left:10px;">រាជធានីភ្នំពេញ ថ្ងៃទី..............ខែ.............ឆ្នាំ.............</span><br>
				<span style="margin-left:100px;​"><strong>ជ.នាយកសាលា</strong></span><br>
				<span style="margin-left:115px;"><strong>នាយករង</strong></span>
				<div class="span8" style="margin-left:115px; margin-top:60px;"></div>
				<span style="margin-left:170px;​"><strong>ឈ្មោះ</strong></span>
			</div>

			<div class="col-sm-6"​ style=" margin-bottom:50px; text-align: right; line-height: 22px; margin-top:-15px;">
				<span style="margin-right:25px;">រាជធានីភ្នំពេញ ថ្ងៃទី..............ខែ.............ឆ្នាំ.............</span><br>
				<span style="margin-right:120px;"><strong>គ្រូទទួលបន្ទុកថ្នាក់</strong></span>
				<div class="span8" style="margin-left:115px; margin-top:60px;"></div>
				<span style="margin-right:70px;​"><strong>ឈ្មោះ</strong></span>
			</div> 		
			<!-- **************************************end footer report********************************** -->
		</div>
	</div>

<?php
	}
?>

<style>
   /*--table--*/
   .titles{
	   	font-family: 'Kh Muol Pali';
	   	font-size:13pt;
	   	text-align: center;
	   	line-height: 32px;   	
   }
   .sub-titles{
	   	font-family: 'Kh Bokor'!important;
	   	font-size:10pt;
		line-height: 22px;  
   }
   th, td{
   		text-align: center;
   }
</style>