<!-- <div id='msg' style='color:white; background-color:#286090;height:30px;  text-align:center'></div> -->

<!--show form search-->
<div class="wrapper">
 	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
    		<div class="result_info">
                <div class="col-sm-6" style="line-height:37px;">
                    <strong><?php echo $this->lang->line("general khmer language report");?>---q</strong>                    
                </div>
            </div>
			<!--form element-->
			<!--<div class="alert alert-danger show_alert" style="margin:5px 0 5px 0; padding:5px 0 7px 12px;"></div>-->

	      	<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" class="tdrow" id="frm_search_report">    
		        <div class="col-sm-12">
		        	<div class="row-1" style="margin-bottom:-19px;">
		        		<div class="panel panel-default">
		        			<div class="panel-body">					        						        	
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_id">School</label>
					        			<select name="school_id" id="school_id" minlength='1' class="form-control">
					                    	<?php
												$row = $this->school->getschinfor_row(1);											
												echo '<option value="'. $row->schoolid .'">'. $row->name .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="program_id">Program</label>
					        			<select name="program_id" id="program_id" minlength='1' class="form-control" required data-parsley-required-message="Select Program">
					                    	<?php
												$row = $this->program->getprogram(1);											
												echo '<option value="'. $row->programid .'">'. $row->program .'</option>';
											?>	                      
		                                </select>
					        		</div>
					        	</div>
		        				<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="school_level_id">School Level<!--<span style="color:red">*</span>--></label>
					        			<select name="school_level_id" id="school_level_id" minlength='1' class="form-control" required data-parsley-required-message="Select School Level">
		                                    <option value=""></option>
					                    	<?php
												$get_level_data = $this->school_level->getsch_level(1);												
												foreach($get_level_data as $r_level){
													echo '<option value="'. $r_level->schlevelid .'">'. $r_level->sch_level .'</option>';
												}
											?>	                      
		                                </select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="adcademic_year_id">Academic Year<!--<span style="color:red">*</span>--></label>
	                                    <select name="adcademic_year_id" id="adcademic_year_id" minlength='1' class="form-control" ></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="grade_level_id">Grade Level<!--<span style="color:red">*</span>--></label>
	                                    <select name="grade_level_id" id="grade_level_id" minlength='1' class="form-control" required data-parsley-required-message="Select Grade Level"></select>
					        		</div>
					        	</div>					        				        	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="class_id">Class Name</label>					        			
	                                    <select name="class_id" id="class_id" minlength='1' class="form-control" required data-parsley-required-message="Select Class Name"></select>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="result_type">Type of Result</label>
	                                    <select name="result_type" id="result_type" class="form-control">
	                                    	<option value=""></option>
	                                    	<option value="1">Monthly</option>
	                                    	<option value="2">Semester</option>
	                                    	<option value="3">Final</option>
	                                    </select>
					        		</div>
					        	</div>	
					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="report_type">Type of Report</label>
	                                    <select name="report_type" id="report_type" class="form-control" required data-parsley-required-message="Select Type of Report">
	                                    </select>
					        		</div>
					        	</div>

					        	<div class="col-md-4">
					        		<div class="form-group">
					        			<label for="student_id">Student Name</label>
	                                    <select name="student_id" id="student_id" class="form-control"></select>
					        		</div>
					        	</div>
					        	<div class="col-sm-12" id="show_data_month">
					        		
					        	</div>
					        	<!--button-->
						        <div class="col-sm-12">	
					                <div class="form-group" style="text-align:center;">
						                <?php if($this->green->gAction("R")){ ?>
						                	<input type="button" name="btnsearch" id='btnsearch' value="Search" class="btn btn-primary" />
						                <?php } ?>
						                <input type="button" name="btncancel" id='btncancel' value="Cancel" class="btn btn-warning" />
					                </div>                            
						        </div><!--end button-->			
		        			</div>		        			
		        		</div>		        				        					        	
		        	</div>      
		        </div>		             
		    </form><!--end form element-->		     	
	    </div>    
	</div>  
</div><!--end show form search-->

<!--show result report-->
<div class="wrapper div_report" style="display:none;">
 	<div class="clearfix" id="main_content_outer">
    	<div id="main_content">
    		<div class="result_info">
                <div class="col-sm-6" style="line-height:37px;">
                    <strong><?php echo $this->lang->line("result report");?></strong>                    
                </div>
                <div class="col-sm-6" style="text-align:right; line-height:35px;">
                    <span class="top_action_button" style="">
                    	<a id="export" href="javascript:void(0)" title="Export">
			    			<img src="<?php echo base_url('assets/images/icons/export.png');?>">
			    		</a>
						<a href="javascript:void(0)" id="print" title="Print">
			    			<img src="<?php echo base_url('assets/images/icons/print.png')?>" />
			    		</a>
		      		</span>                  
                </div>

                <!--show result of monthly report-->
				<div class="col-sm-12">          
					<div class="row show_result">

					</div>
				</div><!--end show result of monthly report-->
            </div>
        </div>
    </div>
</div><!--end show result report-->

<style type="text/css">	
	.tbl-result-type td{
		text-align: left;
		vertical-align: middle !important;
		border: hidden !important;		
	}
</style>

<script type="text/javascript">
	$(function(){


		$("#report_type").val($("#report_type option:first").val());
		//getTextEditor($('.school_invitation'),50);
		$('#year, #schlevelid').hide();
		$("#school_level_id").val($("#school_level_id first").val());
		$("#result_type").val($("#result_type option:first").val());
		//get year and class data---------------------------------
		$('#school_level_id').change(function(){
			var school_level_id = $(this).val();
			var school_id = $('#school_id').val();
			var program_id = $('#program_id').val();

			if(school_level_id != ""){
				$.ajax({
		            url: "<?php echo site_url('student/c_subject_score_entry_kgp/get_year_and_grade_data') ?>",	            
		            type: "post",
		            dataType: 'json',
		            data: {'school_id': school_id, 'program_id': program_id, 'school_level_id': school_level_id},
		            success: function (data) {
		            	//console.log(data);
		            	//get year--------------------------------
		            	var getYear = '';	            	
		            	$.each(data["year"], function(k,v){
		            		getYear += '<option value="'+ v.yearid +'">'+ v.sch_year+'</option>';		            		
		            	}); $('#adcademic_year_id').html(getYear);

		            	//get grade--------------------------------
		            	var getGrade = '';
		            	$.each(data["grade"], function(ke,re){
		            		getGrade += '<option value="'+ re.grade_levelid +'">'+ re.grade_level+'</option>';
		            	});	$('#grade_level_id').html(getGrade);

		            	//get teacher------------------------------
		            	var grade_level_id = $('#grade_level_id').val();
		            	var adcademic_year_id = $('#adcademic_year_id').val();
		            	var url = "<?php echo site_url('reports/generalkhmerlanguage/get_class_data') ?>";
						var data = 'program_id='+program_id+'&school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
						getDataByAjax($('#class_id'), url, data);
						$('#class_id').html('');
						$('#student_id').html('');

						//remove message require-------------------
						var aId = $('#adcademic_year_id').data('parsley-id');
						var gId = $('#grade_level_id').data('parsley-id');

						$('#parsley-id-'+aId).remove();	   
						$('#adcademic_year_id').removeClass('parsley-error');

						$('#parsley-id-'+gId).remove();	   
						$('#grade_level_id').removeClass('parsley-error');        	
				    }
		        });	
			}else{
				$('#adcademic_year_id').html('');
				$('#grade_level_id').html('');
				$('#class_id').html('');
				$('#student_id').html('');
			}
		});
		$("body").delegate("#seldom","click",function(){
			var tr = $(this).parent().parent();
			if($(this).is(":checked")){
				tr.find("#sometime,#usually,#consistently").prop('checked', false);
				tr.find("#sometime,#usually,#consistently").val(0);
				$(this).val(1);
			}else{
				$(this).val(0);
			}
		});
		$("body").delegate("#sometime","click",function(){
			var tr = $(this).parent().parent();
			if($(this).is(":checked")){
				tr.find("#seldom,#usually,#consistently").prop('checked', false);
				tr.find("#seldom,#usually,#consistently").val(0);
				$(this).val(1);
			}else{
				$(this).val(0);
			}
		})
		$("body").delegate("#usually","click",function(){
			var tr = $(this).parent().parent();
			if($(this).is(":checked")){
				tr.find("#sometime,#seldom,#consistently").prop('checked', false);
				tr.find("#sometime,#seldom,#consistently").val(0);
				$(this).val(1);
			}else{
				$(this).val(0);
			}
		})
		$("body").delegate("#consistently","click",function(){
			var tr = $(this).parent().parent();
			if($(this).is(":checked")){
				tr.find("#sometime,#usually,#seldom").prop('checked', false);
				tr.find("#sometime,#usually,#seldom").val(0);
				$(this).val(1);
			}else{
				$(this).val(0);
			}
		})
		//get class data--------------------------------------------
		$('#grade_level_id').change(function(){
			var program_id = $('#program_id').val();
			var school_level_id = $('#school_level_id').val();
			var adcademic_year_id = $('#adcademic_year_id').val();
			var grade_level_id = $(this).val();
			var url = "<?php echo site_url('reports/generalkhmerlanguage/get_class_data') ?>";
			var data = 'program_id='+program_id+'&school_level_id='+school_level_id+'&adcademic_year_id='+adcademic_year_id+'&grade_level_id='+grade_level_id;
			getDataByAjax($('#class_id'), url, data);
		});
		//================ information comments
		$("body").delegate("#print_all","click",function(){
			//alert("Thanks");
			var class_id        = $("#class_id").val();
			var schoolid        = $("#school_id").val();
			var programid       = $("#program_id").val();
			var schlevelid      = $("#school_level_id").val();
			var yearid          = $("#adcademic_year_id").val();
			var gradlavelid     = $("#grade_level_id").val();
			var type_monthly    = $(".get_result_type").val();
						
			var url = "<?php echo site_url("reports/generalkhmerlanguage/print_all_report_monthly");?>?classid="+class_id+"&schoolid="+schoolid+"&programid="+programid+"&schlevelid="+schlevelid+"&yearid="+yearid+"&gradlavelid="+gradlavelid+"&monthly="+type_monthly;
			window.open(url, '_blank');
			//window.location.href = "http://stackoverflow.com","_blank";
		});
		$("body").delegate("#add_comm","click",function()
		{
			
			$("#director").val("");
			$("#hstudentid").val("");
			$("#hevaluationid").val("");
			$("#comment_teacher").val("");
			$("#comment_parent").val("");
			$("#managername").val("");
			$("#date_director").val("<?php echo date("d-m-Y"); ?>");
			$("#date_approve").val("<?php echo date("d-m-Y"); ?>");
			$("#date_teacher").val("<?php echo date("d-m-Y"); ?>");
			$("#date_parents").val("<?php echo date("d-m-Y"); ?>");
			$("#date_return").val("<?php echo date("d-m-Y"); ?>");
			$(".seldom,.sometime,.usually,.consistently").prop("checked",false);
			$(".seldom,.sometime,.usually,.consistently").val(0);

			var studentid = $(this).attr("studentid_a");
			var studentName = $(this).attr("studentName");
			$("#hstudentid").val(studentid);
			$("#show_studentname").html(studentName);
			var type_exam = $("#result_type").val();
			if(type_exam == 1){ // monthly
					var data = {
								class_id        : $("#class_id").val(),		
								schoolid        : $("#school_id").val(),
								programid       : $("#program_id").val(),
								schlevelid      : $("#school_level_id").val(),
								yearid          : $("#adcademic_year_id").val(),
								gradlavelid     : $("#grade_level_id").val(),
								type_exam       : type_exam,
								studentid      : studentid,
								type_monthly   : $("#get_result_type").val()
								}
			}else if(type_exam == 2){ // semester
					var data = {
								class_id        : $("#class_id").val(),		
								schoolid        : $("#school_id").val(),
								programid       : $("#program_id").val(),
								schlevelid      : $("#school_level_id").val(),
								yearid          : $("#adcademic_year_id").val(),
								gradlavelid     : $("#grade_level_id").val(),
								semester        : $(".check_semeste:checked").val(),
								type_exam      : type_exam,
								studentid      : studentid
								}
			}else if(type_exam == 3){
				var stuent_name = $(this).parent().parent().find("#student_name").html();
				$("#show_student_name").html("<b>"+stuent_name+"</b>");
				var data = {
								class_id        : $("#class_id").val(),		
								schoolid        : $("#school_id").val(),
								programid       : $("#program_id").val(),
								schlevelid      : $("#school_level_id").val(),
								yearid          : $("#adcademic_year_id").val(),
								gradlavelid     : $("#grade_level_id").val(),
								type_exam       : type_exam,
								studentid      : studentid
							}
			}
			
			$.ajax({
				url:"<?php echo site_url('reports/generalkhmerlanguage/select_data_comment') ?>",
				type:"POST",
				dataType:"json",
				data:data,
				success:function(data){
					if(data['ord'] != "" && data.ord != undefined)
					{
						if(type_exam == 1){
							$("#hstudentid").val(data['ord'][0]['student_id']);
							$("#comment_teacher").val(data['ord'][0]['command_teacher']);
							$("#comment_parent").val(data['ord'][0]['command_guardian']);
							$("#managername").val(3);
							$("#teachername").val(3);
							$("#date_approve").val(data['ord'][0]['date_academic']);
							$("#date_teacher").val(data['ord'][0]['date_teacher']);
							$("#date_parents").val(data['ord'][0]['date_guardian']);
							$("#date_return").val(data['ord'][0]['date_return']);
							
						}else if(type_exam == 2){
							//alert(data['ord'][0]['techer_comment']);
							$("#hstudentid").val(data['ord'][0]['studentid']);
							$("#hevaluationid").val(data['ord'][0]['evaluationid']);
							$("#comment_teacher").val(data['ord'][0]['techer_comment']);
							$("#comment_parent").val(data['ord'][0]['guardian_comment']);
							$("#managername").val(1);
							$("#teachername").val(1);
							$("#date_approve").val(data['ord'][0]['date_academic']);
							$("#date_teacher").val(data['ord'][0]['date_teacher']);
							$("#date_parents").val(data['ord'][0]['date_guardian']);
							$("#date_return").val(data['ord'][0]['date_return']);
							if(data['detail'] != ""){
								$.each(data['detail'],function(kk,vv){
									var descrition_id = vv['descrition_id'];
									if(vv['seldom'] == 1){
										$(".seldom_"+descrition_id).prop("checked",true);
										$(".seldom_"+descrition_id).val(1);
									}else if(vv['sometimes'] == 1){
										$(".sometime_"+descrition_id).prop("checked",true);
										$(".sometime_"+descrition_id).val(1);
									}else if(vv['usually'] == 1){
										$(".usually_"+descrition_id).prop("checked",true);
										$(".usually_"+descrition_id).val(1);
									}else if(vv['consistently'] == 1){
										$(".consistently_"+descrition_id).prop("checked",true);
										$(".consistently_"+descrition_id).val(1);
									}
								})
							}
							
						}else if(type_exam == 3){
							$("#hstudentid").val(data['ord'][0]['student_id']);
							$("#comment_teacher").val(data['ord'][0]['command_teacher']);
							$("#managername").val(2);
							$("#director").val(data['ord'][0]['directorid']);
							$("#teachername").val(2);
							$("#date_approve").val(data['ord'][0]['date_academic']);
							$("#date_teacher").val(data['ord'][0]['date_teacher']);
							$("#date_director").val(data['ord'][0]['date_director']);
						}
					}
					$("#myModal").modal("show");
					$("#date_director,#date_approve,#date_teacher,#date_return,#date_parents").datepicker({
		                autoclose:true,
		                showOnFocus:true,
		                format:'dd/mm/yyyy'
		            });
				}
			});
			
			
			
		});
		$("body").delegate("#save_comment","click",function(){
			var select_result_type = $("#result_type").val();
			var data = new Object();
			if(select_result_type == 1){
					data = {
							class_id        : $("#class_id").val(),		
							schoolid        : $("#school_id").val(),
							programid       : $("#program_id").val(),
							schlevelid      : $("#school_level_id").val(),
							yearid          : $("#adcademic_year_id").val(),
							gradlavelid     : $("#grade_level_id").val(),
							type_monthly    : $(".get_result_type").val(),
							comment_teacher: $("#comment_teacher").val(),
							date_approve   : $("#date_approve").val(),
							date_teacher   : $("#date_teacher").val(),
							managername    : $("#managername").val(),
							teachername    : $("#teachername").val(),
							comment_parent : $("#comment_parent").val(),
							date_parents   : $("#date_parents").val(),
							date_return    : $("#date_return").val(),
							studentid      : $("#hstudentid").val(),
							select_result_type : select_result_type
						}
			}else if(select_result_type == 2){
				var arr_val = new Array();
				var ii =0;
				$(".iddescription").each(function(){
					var tr = $(this).parent().parent();
					var value_id_desc = $(this).val();
					if(tr.find("#seldom").val() == 1){
						arr_val[ii++] = {"iddiscr":value_id_desc,"type_ev":"seldom"};
					}else if(tr.find("#sometime").val() == 1){
						arr_val[ii++] = {"iddiscr":value_id_desc,"type_ev":"sometime"};
					}else if(tr.find("#usually").val() == 1){
						arr_val[ii++] = {"iddiscr":value_id_desc,"type_ev":"usually"};
					}else if(tr.find("#consistently").val() == 1){
						arr_val[ii++] = {"iddiscr":value_id_desc,"type_ev":"consistently"};
					}
				});
				data = {
							class_id        : $("#class_id").val(),		
							schoolid        : $("#school_id").val(),
							programid       : $("#program_id").val(),
							schlevelid      : $("#school_level_id").val(),
							yearid          : $("#adcademic_year_id").val(),
							gradlavelid     : $("#grade_level_id").val(),
							semester       : $(".check_semeste:checked").val(),
							comment_teacher: $("#comment_teacher").val(),
							date_approve   : $("#date_approve").val(),
							date_teacher   : $("#date_teacher").val(),
							managername    : $("#managername").val(),
							teachername    : $("#teachername").val(),
							comment_parent : $("#comment_parent").val(),
							date_parents   : $("#date_parents").val(),
							date_return    : $("#date_return").val(),
							studentid      : $("#hstudentid").val(),
							hevaluationid  : $("#hevaluationid").val(),
							arr_eval : arr_val,
							select_result_type : select_result_type
						};
			}else if(select_result_type == 3){
				data = {
							class_id        : $("#class_id").val(),		
							schoolid        : $("#school_id").val(),
							programid       : $("#program_id").val(),
							schlevelid      : $("#school_level_id").val(),
							yearid          : $("#adcademic_year_id").val(),
							gradlavelid     : $("#grade_level_id").val(),
							//type_monthly    : $(".get_result_type").val(),
							comment_teacher: $("#comment_teacher").val(),
							date_director  : $("#date_director").val(),
							date_approve   : $("#date_approve").val(),
							date_teacher   : $("#date_teacher").val(),
							director      : $("#director").val(),
							managername    : $("#managername").val(),
							teachername    : $("#teachername").val(),
							studentid      : $("#hstudentid").val(),
							select_result_type : select_result_type
						}
			}
			
			var conf = confirm("Do you want to save ?");
			if(conf == true)
			{
				$.ajax({
					url:"<?php echo site_url('reports/generalkhmerlanguage/save_comment_student_monthly') ?>",
					type:"POST",
					dataType:"json",
					data:data,
					success:function(data){
						if(data.alert_result == 100){
							alert("Save success..");
							$("#hstudentid").val("");
							$("#comment_teacher").val("");
							$("#comment_parent").val("");
							$("#managername").val("");
							$("#date_approve").val("<?php echo date("d-m-Y"); ?>");
							$("#date_teacher").val("<?php echo date("d-m-Y"); ?>");
							$("#date_parents").val("<?php echo date("d-m-Y"); ?>");
							$("#date_return").val("<?php echo date("d-m-Y"); ?>");
							$("#myModal").modal("hide");
						}else{
							alert("Save is has completed.");
						}
					}
				})
			}else{
				return false;
			}
		})
		$("body").delegate(".check_semeste","click",function(){
			var result_type = $("#result_type").val();
			if(result_type == 3){
				$('input:checkbox').prop('checked', true);
			}else{
				$('input:checkbox').not(this).prop('checked', false);
			}
			
		})
		$("body").delegate("#result_type","change",function(){
			
			var val_result_type = $(this).val();
			var opt_m = "";
			
			if(val_result_type == 1) // Monthly
			{
				opt_m = '<option value=""></option>'+
		            	'<option value="1">Monthly report list</option>'+
		            	'<option value="2">Monthly average report</option>'+
		            	'<option value="3">Monthly rank report</option>';
		        $("#show_data_month").html("");
			}else if(val_result_type == 2) // Semester
			{
				
				opt_m = '<option value=""></option>'+
		            	'<option value="1">Semester report list</option>'+
		            	'<option value="2">Semester average report</option>'+
		            	'<option value="3">Semester rank report</option>';
		        
		        var month_data = ['January', 'February', 'March', 'April-May', 'June', 'July', 'August', 'September', 'Octomber', 'November', 'December'];
				var opt = "";
				for(i = 1; i <= month_data.length; i++) {
					opt += '<option value="'+ i +'">'+ month_data[i-1] +'</option>';
				}   	
				var elements = 	'<label for="get_result_type">Choose Month</label><br>'+
								'<div class="form-group col-sm-12" style="padding:10px;">'+
									'<div class="col-sm-4" id="display_semester"></div>'+
									'<div class="col-sm-4" style="padding-left:0px;padding-right:20px;">'+
					           			'<select name="get_result_type[]" class="form-control get_result_type" style="border-color:#337ab7;" multiple>'+ opt +'</select>'+
							 		'</div>'+
							 	'</div>';
				$("#show_data_month").html("");
				$("#show_data_month").html(elements);
				fn_display_semester();
			}else // Final
			{
				opt_m = '<option value=""></option>'+
		            	'<option value="1">Annual report list</option>'+
		            	'<option value="2">Annual average report</option>'+
		            	'<option value="3">Annual rank report</option>';
		        var elements = 	'<label for="get_result_type">Semester</label><br>'+
								'<div class="form-group col-sm-12" style="padding:10px;">'+
									'<div class="col-sm-4" id="display_semester"></div>'+
								'</div>';
		        $("#show_data_month").html("");
		        $("#show_data_month").html(elements);
		        fn_display_semester();
			}
			$("#report_type").html(opt_m);
		});
		
		//=============== end information comments

		//get student data------------------------------------------
		$('#class_id').change(function(){
			var class_id = $(this).val();		
			var schoolid = $("#school_id").val();
			var programid = $("#program_id").val();
			var schlevelid = $("#school_level_id").val();
			var yearid = $("#adcademic_year_id").val();	
			var gradlavelid = $("#grade_level_id").val();	
			if(class_id == ""){
				$('#student_id').html('');
			}else{
				//var url = "<?php //echo site_url('student/c_subject_score_entry_kgp/get_student_data') ?>";
				var url = "<?php echo site_url('reports/generalkhmerlanguage/get_student_data') ?>";
				var data = {'class_id':class_id,
							'schoolid':schoolid,
							'programid':programid,
							'schlavelid':schlevelid,
							'yearid':yearid,
							'gradlavelid':gradlavelid
						};
				getDataByAjax($('#student_id'), url, data);
			}
		});

		$('#report_type').change(function() {
			var month_data = ['January', 'February', 'March', 'April-May', 'June', 'July', 'August', 'September', 'Octomber', 'November', 'December'];
			var report_type = $(this).val();
			var opt = "";
	        for(i = 1; i <= month_data.length; i++) {
	        	if(i == 1){
					opt += '<option value="'+ i +'" selected="selected">'+ month_data[i-1] +'</option>';
	        	}else{
	        		opt += '<option value="'+ i +'">'+ month_data[i-1] +'</option>';
	        	}
			}
			var result_type = $("#result_type").val();
			if(result_type == 1) // Monthly
			{
					if(report_type == 1){
						
						var elements = 	'<div class="form-group col-sm-4" style="padding-left:0px;padding-right:20px;">'+
							           	'<label for="get_result_type">Choose Month</label>'+
									 	'<select name="get_result_type" class="form-control get_result_type" id="get_result_type" style="border-color:#337ab7;">'+ opt +'</select>'+
									 	'</div>';
						$("#show_data_month").html("");
						$("#show_data_month").html(elements);
					}else if(report_type == 2){
						
						var elements = 	'<div class="form-group col-sm-4" style="padding-left:0px;padding-right:20px;">'+
							           	'<label for="get_result_type">Choose Month</label>'+
									 	'<select name="get_result_type[]" class="form-control get_result_type" id="get_result_type" style="border-color:#337ab7;" multiple>'+ opt +'</select>'+
									 	'</div>';
						$("#show_data_month").html("");
						$("#show_data_month").html(elements);
					}else if(report_type == 3){
						
						var elements = 	'<div class="form-group col-sm-4" style="padding-left:0px;padding-right:20px;">'+
							           	'<label for="get_result_type">Choose Month</label>'+
									 	'<select name="get_result_type" class="form-control get_result_type" id="get_result_type" style="border-color:#337ab7;">'+ opt +'</select>'+
									 	'</div>';
						$("#show_data_month").html("");
						$("#show_data_month").html(elements);
					}
			}else if(result_type == 2) // Semester
			{ 
				// if(report_type == 1){
				// 	var elements = 	'<label for="get_result_type">Choose Month</label><br>'+
				// 					'<div class="form-group col-sm-12" style="padding:10px;">'+
				// 						'<div class="col-sm-4" id="display_semester"></div>'+
				// 						'<div class="col-sm-4" style="padding-left:0px;padding-right:20px;">'+
				// 		           			'<select name="get_result_type[]" class="form-control get_result_type" style="border-color:#337ab7;" multiple>'+ opt +'</select>'+
				// 				 		'</div>'+
				// 				 	'</div>';
				// 	$("#show_data_month").html("");
				// 	$("#show_data_month").html(elements);
				// 	fn_display_semester();
				// }
			}
			//var reportType = $('#report_type').val();
			// var url = "<?php echo site_url('reports/generalkhmerlanguage/get_student_data') ?>";
			// var class_id = $('#class_id').val();		
			// var schoolid = $("#school_id").val();
			// var programid = $("#program_id").val();
			// var schlevelid = $("#school_level_id").val();
			// var yearid = $("#adcademic_year_id").val();	
			// var gradlavelid = $("#grade_level_id").val();
			// var data = {'class_id':class_id,
			// 			'schoolid':schoolid,
			// 			'programid':programid,
			// 			'schlavelid':schlevelid,
			// 			'yearid':yearid,
			// 			'gradlavelid':gradlavelid
			// 			};
			// getDataByAjax($('#student_id'), url, data);
			
		});


		//search report---------------------------------------------
		$('#btnsearch').click(function(){
				$('.show_alert').html('').hide();			
				//var data       = $("#frm_search_report").serialize();		
				var school_id  = $("#school_id").val();	
				var program_id = $('#program_id').val();
				var school_level_id   = $('#school_level_id').val();
				var adcademic_year_id = $('#adcademic_year_id').val();
				var grade_level_id    = $('#grade_level_id').val();
				var class_id          = $('#class_id').val();			
				var student_id        = $('#student_id').val();
				var type_of_result    = $("#result_type").val();
				var type_of_report    = $("#report_type").val();
				//var select_result_type = $('.select_result_type:checked').val();
				var class_label = $('#class_id').find("option:selected").text();
				var url = "";
				var show_semester = "";
				var data_inf = new Object();
				if(type_of_result == 1)
				{ // Monthly
					
					if(type_of_report == 1){ // report list
						url = "<?php echo site_url('reports/generalkhmerlanguage/primary_monthly_report_list') ?>";
						data_inf = {
									'school_id':school_id,
									'program_id':program_id,
									'school_level_id':school_level_id,
									'adcademic_year_id':adcademic_year_id,
									'grade_level_id':grade_level_id,
									'class_id':class_id,
									'student_id':student_id,
									'program_id':program_id,
									'type_of_report':type_of_report,
									'month':$('#get_result_type').val()
									};
					}else if(type_of_report == 2){ // average report
						url = "<?php echo site_url('reports/generalkhmerlanguage/primary_monthly_report_average') ?>";
						var get_result_type   = $('#get_result_type').val();
						var ii = 0;
						var arr_m = new Array();
						$(".get_result_type option:selected").each(function(){
							arr_m[ii] = $(this).val();
							ii++;
						})
						data_inf = {
									'school_id':school_id,
									'program_id':program_id,
									'school_level_id':school_level_id,
									'adcademic_year_id':adcademic_year_id,
									'grade_level_id':grade_level_id,
									'class_id':class_id,
									'student_id':student_id,
									'program_id':program_id,
									'type_of_report':type_of_report,
									'month':arr_m
									};
					}else if(type_of_report == 3 ){ // rank report
						url = "<?php echo site_url('reports/generalkhmerlanguage/primary_monthly_report_ranking') ?>";
						data_inf = {
									'school_id':school_id,
									'program_id':program_id,
									'school_level_id':school_level_id,
									'adcademic_year_id':adcademic_year_id,
									'grade_level_id':grade_level_id,
									'class_id':class_id,
									'student_id':student_id,
									'program_id':program_id,
									'type_of_report':type_of_report,
									'month':$('#get_result_type').val()
									};
						
					}
				}else if(type_of_result == 2)
				{
					if(type_of_report == 1){
						url = "<?php echo site_url('reports/generalkhmerlanguage/primary_smester_report_list'); ?>";
						var ii = 0;
						var arr_m = new Array();
						$(".get_result_type option:selected").each(function(){
							arr_m[ii] = $(this).val();
							ii++;
						});
						data_inf = {
									'school_id':school_id,
									'program_id':program_id,
									'school_level_id':school_level_id,
									'adcademic_year_id':adcademic_year_id,
									'grade_level_id':grade_level_id,
									'class_id':class_id,
									'student_id':student_id,
									'program_id':program_id,
									'type_of_report':type_of_report,
									'semester':$(".check_semeste:checked").val(),
									'month':arr_m
									};
					}else if(type_of_report == 2){
						url = "<?php echo site_url('reports/generalkhmerlanguage/primary_semester_report_average'); ?>";
						var ii = 0;
						var arr_m = new Array();
						$(".get_result_type option:selected").each(function(){
							arr_m[ii] = $(this).val();
							ii++;
						});
						data_inf = {
									'school_id':school_id,
									'program_id':program_id,
									'school_level_id':school_level_id,
									'adcademic_year_id':adcademic_year_id,
									'grade_level_id':grade_level_id,
									'class_id':class_id,
									'student_id':student_id,
									'program_id':program_id,
									'type_of_report':type_of_report,
									'semester':$(".check_semeste:checked").val(),
									'month':arr_m
									};
					}else{
						url = "<?php echo site_url('reports/generalkhmerlanguage/primary_semester_report_ranking'); ?>";
						var ii = 0;
						var arr_m = new Array();
						$(".get_result_type option:selected").each(function(){
							arr_m[ii] = $(this).val();
							ii++;
						});
						data_inf = {
									'school_id':school_id,
									'program_id':program_id,
									'school_level_id':school_level_id,
									'adcademic_year_id':adcademic_year_id,
									'grade_level_id':grade_level_id,
									'class_id':class_id,
									'student_id':student_id,
									'program_id':program_id,
									'type_of_report':type_of_report,
									'semester':$(".check_semeste:checked").val(),
									'month':arr_m
									};
					}
				}else if(type_of_result == 3){
					if(type_of_report == 1){
						url = "<?php echo site_url('reports/generalkhmerlanguage/primary_final_report_list'); ?>";
						var ii = 0;
						var arr_semester = new Array();
						$(".check_semeste").each(function(){
							arr_semester[ii] = $(this).val();
							ii++;
						});
						data_inf = {
									'school_id':school_id,
									'program_id':program_id,
									'school_level_id':school_level_id,
									'adcademic_year_id':adcademic_year_id,
									'grade_level_id':grade_level_id,
									'class_id':class_id,
									'student_id':student_id,
									'program_id':program_id,
									'type_of_report':type_of_report,
									'semester':arr_semester
									};
					}else if(type_of_report == 3){
						url = "<?php echo site_url('reports/generalkhmerlanguage/primary_final_report_ranking'); ?>";
						var ii = 0;
						var arr_semester = new Array();
						$(".check_semeste").each(function(){
							arr_semester[ii] = $(this).val();
							ii++;
						});
						data_inf = {
									'schoolid':school_id,
									'program_id':program_id,
									'school_level_id':school_level_id,
									'adcademic_year_id':adcademic_year_id,
									'grade_level_id':grade_level_id,
									'class_id':class_id,
									'student_id':student_id,
									'program_id':program_id,
									'type_of_report':type_of_report,
									'semester':arr_semester
									};

						
					}
				}
				// console.log(data_inf);
				// if(select_result_type == 1){
				// 	if(reportType == 1){
				// 			url = "<?php echo site_url('reports/generalkhmerlanguage/primary_monthly_report_class_ranking') ?>";
				// 	}
				// 	else if(reportType == 2){
				// 		//url = "<?php //echo site_url('reports/generalkhmerlanguage/primary_monthly_report_individual_ranking') ?>";
				// 		url = "<?php echo site_url('reports/generalkhmerlanguage/primary_monthly_report_list') ?>";
				// 	}
				// 	else if(reportType == 3){
				// 		url = "<?php echo site_url('reports/generalkhmerlanguage/primary_yearly_report_individual_ranking') ?>";
				// 	}
				// }else{
				// 	show_semester = $(".get_semester_data option:selected").text();
				// 	if(reportType == 1){
				// 		url = "<?php echo site_url('reports/generalkhmerlanguage/primary_semester_report_class_ranking') ?>";
				// 	}else{
				// 		

				// 	}
				 	
				// }
				if(url != ""){
					$.ajax({
			            url: url,	            
			            type: "post",
			            dataType: 'html',
			            data: data_inf,
			            success: function (data) {	
			            	$('.div_report').show();
			            	$('.show_result').html(data);
			            	//$("#semester").html(show_semester);
			            	//$("#semester_list").html(show_semester);
			            	//$("#show_title").html("របាយការណ៏សិក្សាប្រចាំ&nbsp;"+show_semester);
			            }
			        });
				}
				
		});

		//print report------------------------------------------
		$("#print").on("click",function(){
		   	var data = $(".print_show_result").html();
		   	gsPrint(data);
		});
		$('body').delegate('#export', 'click', function(e){
			var header_title = $(".header_title").val();
            var data = $(".print_show_result").html();    
            var data_export  =$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
            var export_data = $("<center>"+data_export+"</center>").clone().find(".remove_tag").remove().end().html();         
            this.href = "data:application/vnd.ms-excel,"+encodeURIComponent(export_data);
            this.download = header_title+".xls";
            $('.table').attr('border', 0);
        });      
	});

	/*-------------------------------------------FUNCTION------------------------------------------*/

	//function ajax to get data
	function getDataByAjax(selector, url, data){
		$.ajax({
            url: url,	            
            type: "post",
            dataType: 'html',
            data: data,
            success: function (data) {
            	selector.html(data);		            	
            }
        });	
	}

	//function print---------------------------------
	function gsPrint(data){
		var element = "<div id='print_area' style='margin:0 5px 0 5px;'>"+data+"</div>";
		$("<center>"+element+"</center>").printArea({
			mode:"popup",  //printable window is either iframe or browser popup              
			popHt: 1000 ,  // popup window height
			popWd: 670,  // popup window width
			popX: 0 ,  // popup window screen X position
			popY: 0 , //popup window screen Y position
			popTitle:"General Khmer Language", // popup window title element
			popClose: false,  // popup window close after printing
			strict: false 
		});
	}
	function fn_display_semester(){
		$.ajax({
            url: "<?php echo site_url('reports/generalkhmerlanguage/show_semester');?>",	            
            type: "post",
            dataType: 'html',
            data: {
            	para :1,
            	schlevelid :$("#school_level_id").val(),
            	yearid :$("#adcademic_year_id").val(),
            	type_result:$("#result_type").val()
            },
            success: function (data) {	
            	$("#display_semester").html(data);            	
            }
        });	
	}
</script>

