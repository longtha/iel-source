<div class="panel panel-default" style="margin-top:30px !important;width: 670px; margin: 0 auto;">
	<div class="panel-body" style="border: 0px solid #000">
		<!-- *************************************header report***************************************** -->
		<div class="print_show_result">
		<style>
		   /*--table--*/
		   .titles{
			   	font-size:14px;
			   	text-align: right;
			   	line-height: 25px;
			   	font-weight: bolder;   	
		   }
		   .border{
		   	border: 1px;
		   }
		   .sub-titles{
			   	font-size:12pt;
				line-height: 22px;  
		   }
		   th, td{
		   		text-align: center;
		   		height: 27px;
		   		vertical-align: middle !important;
		   }
		</style>
		<div class="col-sm-6 sub-titles" style="margin-top:10px;border:0px solid #f00;">
			<span class="class_format_fontBokor">មន្ទីរអប់រំ  យុវជន និងកីឡារាជធានីភ្នំពេញ</span><br>
			<span class="class_format_fontBokor">សាលាអន្តរជាតិ អាយ អ៊ី អ៊ែល</span><br>
			<span style="margin-left:20px;" class="class_format_fontBokor">ថ្នាក់ទីៈ  
				<?php 
					if(!empty($class_name)){
						echo "<b>".$class_name["class_name"]."</b>";
					}
				?>
			</span>
		</div>
		<div class="col-sm-6 sub-titles" style="text-align: right; margin-top:6px;border:0px solid #f00;">
			<div class="col-sm-12 titles">
				<span class="class_format_fontBokor">ព្រះរាជាណាចក្រកម្ពុជា</span><br>
				<span class="class_format_fontBokor">ជាតិ   សាសនា   ព្រះមហាក្សត្រ</span>
			</div>
		</div>
		<div class="col-sm-12 sub-titles" style="text-align: center; text-decoration: underline;">
			<span class="class_format_fontBokor">ចំណាត់ថ្នាក់ប្រចាំខែ
				<?php 
					echo "<b>".$month_name."</b>";
				?>
			</span>
			<input type="hidden" class="header_title" value="មធ្យមភាគប្រចាំខែ&nbsp;<?php echo $month_name;?>">
			<br>
			<span class="class_format_fontBokor">ឆ្នាំសិក្សា
				<?php
					echo "<b>".$sch_year."</b>";
				?>
			</span>
		</div>

		
		<!-- ***********************************end header report************************************** -->

		<!-- ***************************************content report************************************* -->
		<div class="col-sm-12" style="margin-top:10px;">
			<div class="table-responsive">							
				<table border="1"​ align="center" id='listsubject' class="">
				    <thead>
				    	<tr>								        
					        <th width="70" class="class_format_fontBattambang">ល.រ</th>
					        <th width="250" class="class_format_fontBattambang"><?php echo $this->lang->line("studentname");?></th>
					        <th width="50" class="class_format_fontBattambang"><?php echo $this->lang->line("gender");?></th>
					        <th width="90" class="class_format_fontBattambang"><?php echo $this->lang->line("average");?></th>
					        <th width="90" class="class_format_fontBattambang"><?php echo $this->lang->line("ranking");?></th>
					        <th width="90" class="class_format_fontBattambang"><?php echo $this->lang->line("grade");?></th>
					        <th width="280" class="class_format_fontBattambang"><?php echo $this->lang->line("remark");?></th>
					    </tr>
				    </thead>
		    		<tbody class="tbodys">
		    			<?php 
							$i = 1;
							$arr_rank_v = array();
							$arr_sort_rank = array();
							if(count($getStuData) > 0){
								foreach($getStuData as $row){
									$show_mention = "";
									if(count($mention) > 0){
										foreach($mention as $v_mention){
											$avg_show = isset($avg[$row->studentid])?$avg[$row->studentid]:0;
											if($v_mention['Max_m'] >= $avg_show && $v_mention['Min_m'] <= $avg_show){
												$show_mention = $v_mention['Mention'];
											}
										}
									}
									$arr_rank_v[$row->studentid] = isset($avg[$row->studentid])?$avg[$row->studentid]:0;
									$funll_name = $row->last_name_kh.' &nbsp;&nbsp;&nbsp; '.$row->first_name_kh;
									$gender     = $row->gender == "female" ? "ស" : "ប";
									$avg_scor   = isset($avg[$row->studentid])?$avg[$row->studentid]:0;
									$diplay_men = $show_mention;
									$arr_sort_rank[$row->studentid] = array($funll_name,$gender,$avg_scor,$diplay_men);
								}
							}
							arsort($arr_rank_v);
					        $arr_n = array();
					        $trr = "";
					        if(count($arr_rank_v)>0){
					        	$ii = 1;
					        	$arr_stor = array();
					        	$nn = 1;
					        	$i  = 1;
					        	foreach($arr_rank_v as $k=>$v){
					        		if(in_array($v,$arr_stor)){
										$arr_n[$k] = $nn;
										//$trr.= $arr_sort_rank[$k];
										$trr.='<tr>
													<td class="class_format_fontBattambang">'.$i.'</td>
													<td class="class_format_fontBattambang" style="text-align: left !important;">'.$arr_sort_rank[$k][0].'</td>
													<td class="class_format_fontBattambang">'. $arr_sort_rank[$k][1] .'</td>
													<td class="class_format_fontBattambang" style="text-align: center !important;">'.$arr_sort_rank[$k][2].'</td>
													<td class="class_format_fontBattambang" style="text-align: center !important;">'.$arr_n[$k].'</td>
													<td class="class_format_fontBattambang" style="text-align: center !important;">'.$arr_sort_rank[$k][3].'</td>
													<td class="class_format_fontBattambang"> &nbsp;</td>									
												 </tr>';
					        		}else{
					        			$nn = $ii;
					        			$arr_n[$k] = $ii;
					        			//$trr.= $arr_sort_rank[$k];
					        			$trr.='<tr>
													<td class="class_format_fontBattambang">'.$i.'</td>
													<td class="class_format_fontBattambang" style="text-align: left !important;">'.$arr_sort_rank[$k][0].'</td>
													<td class="class_format_fontBattambang">'. $arr_sort_rank[$k][1] .'</td>
													<td class="class_format_fontBattambang" style="text-align: center !important;">'.$arr_sort_rank[$k][2].'</td>
													<td class="class_format_fontBattambang" style="text-align: center !important;">'.$arr_n[$k].'</td>
													<td class="class_format_fontBattambang" style="text-align: center !important;">'.$arr_sort_rank[$k][3].'</td>
													<td class="class_format_fontBattambang"> &nbsp;</td>									
												 </tr>';
					        		}
									$arr_stor[$ii] = $v;
									$ii++;
									$i++;
					        	}
					        }	
					        echo $trr;						
		    			?>
		    		</tbody>
				</table>
			</div>
		</div>
		<!-- *************************************end content report********************************** -->

		<!-- ***************************************footer report************************************* -->
		<div style="margin: 0 auto; width: 630px; border:0px solid #f00;overflow: hidden;">
		<div class="col-sm-6"​  style="border:1px solid #fff;width: 310px !important; float: left;">
			<span style="margin-left:80px;" class="class_format_fontBattambang">បានឃើញ និង ឯកភាព</span><br>
			<span style="margin-left:10px;" class="class_format_fontBattambang">រាជធានីភ្នំពេញ ថ្ងៃទី...........ខែ..........ឆ្នាំ..........</span><br>
			<span style="margin-left:100px;​" class="class_format_fontMuolLight"><strong>ជ.នាយកសាលា</strong></span><br>
			<span style="margin-left:115px;" class="class_format_fontMuolLight"><strong>នាយករង</strong></span>
			<div class="span8 class_format_fontMuolLight" style="margin-left:115px; margin-top:60px;"></div>
			<!-- <span style="margin-left:170px;​"><strong>ឈ្មោះ</strong></span> -->
		</div>

		<div class="col-sm-6"​  style="border:1px solid #fff;width: 315px !important; float: left;">
			<span style="margin-right:25px;" class="class_format_fontBattambang">រាជធានីភ្នំពេញ ថ្ងៃទី...........ខែ..........ឆ្នាំ..........</span><br>
			<span style="margin-right:120px;" class="class_format_fontMuolLight"><strong>គ្រូទទួលបន្ទុកថ្នាក់</strong></span>
			<div class="span8 class_format_fontMuolLight" style="margin-left:115px; margin-top:60px;"></div>
			<!-- <span style="margin-right:70px;​"><strong>ឈ្មោះ</strong></span> -->
		</div> 
		</div>		
		<!-- **************************************end footer report********************************** -->
		</div>
	</div>
</div>


