
<?php 
		$class_name = "";
		$tr = "";
		$i = 1;
		if($student_inf->num_rows() > 0){
			foreach($student_inf->result() as $row){
				$class_name = $row->class_name;
				$total_semester = (isset($avg_sem['sem_total'][$row->studentid]) ? $avg_sem['sem_total'][$row->studentid] : 0);
				$avg_semester = (isset($avg_sem['sem_avg'][$row->studentid]) ? $avg_sem['sem_avg'][$row->studentid] : 0);
				$avg_month = (isset($avg_sem['month'][$row->studentid]) ? $avg_sem['month'][$row->studentid] : 0);

				$result_m = "";
				if(isset($count_m[$row->studentid])){
					$cm = $count_m[$row->studentid];
					$result_m = round($avg_month/$cm,2);
				}else{
					$result_m = 0;
				}
				$result_s = round($result_m+$avg_semester,2);
				$tr.= '<tr>'.
						'<td class="class_format_fontBattambang">'.$i.'</td>'.
						'<td class="class_format_fontBattambang" style="text-align: left !important;">'.$row->studentname.'</td>'.
						'<td class="class_format_fontBattambang">'. ($row->gender == "female" ? "ស" : "ប") .'</td>'.
						'<td class="class_format_fontBattambang" style="text-align: center !important;"><b>'.$total_semester.'</b></td>'.
						'<td class="class_format_fontBattambang" style="text-align: center !important;"><b>'.$avg_semester.'</b></td>'.
						'<td class="class_format_fontBattambang" style="text-align: center !important;"><b>'.$result_m.'</b></td>'.
						'<td class="class_format_fontBattambang"><b>'.$result_s.'</b></td>'.										
					 '</tr>';
				$i++;
			}
		}							
	?>
<div style="margin-top:5px;width: 670px; margin: 0 auto;" class="print_show_result">
	<div class="panel-body">
		<!-- *************************************header report***************************************** -->
	<div class="col-sm-12" style="margin: 0 auto; width: 630px; border:0px solid #f00;overflow: hidden;">
		<div class="col-sm-6 titles" style="border: 0px solid #f00;text-align: center;width: 220px;float: left;">
			<span  class="class_format_fontBokor"  style="font-size: 16px">ព្រះរាជាណាចក្រកម្ពុជា</span><br>
			<span  class="class_format_fontBokor"  style="font-size: 16px">ជាតិ   សាសនា   ព្រះមហាក្សត្រ</span>
		</div>

		<div class="col-sm-6 sub-titles" style="border: 0px solid #f00;text-align: center;width: 252px;float: right;">
			<span  class="class_format_fontBokor"  style="font-size: 16px">មន្ទីរអប់រំ  យុវជន និងកីឡារាជធានីភ្នំពេញ</span><br>
			<span  class="class_format_fontBokor"  style="font-size: 16px">សាលាអន្តរជាតិ អាយ អ៊ី អ៊ែល</span><br>
			
		</div>
	</div>
		<div class="col-sm-12 sub-titles" style="margin: 0 auto; width: 630px; border:0px solid #f00;overflow: hidden;">
			<div class="col-sm-4 sub-titles">
				<span style="margin-left:20px;font-size: 16px" class="class_format_fontBokor">ថ្នាក់ទីៈ  
				<?php 
					if(!empty($class_name)){
						echo "<b>".$class_name."</b>";
					}
				?>
				</span>
			</div>
		</div>
		<div class="col-sm-12 sub-titles">
			<div class="sub-titles" style="text-align: left; text-decoration: underline;margin-top: 20px;border: 0px solid #f00;width: 400px; margin: 0 auto;text-align: center;">
				<span class="class_format_fontMuolLight" style="font-size: 14px">សរុបពិន្ទុប្រឡងឆមាស មធ្យមភាគប្រឡងឆមាស ខែប្រចាំឆមាស <span id="semester"></span>
					<?php 
						echo "<b>".$title_semester."</b>";
					?>
				</span>
			</div>
		</div>
		

		<div class="col-sm-12 sub-titles" style="text-align: center; margin-top:20px;">
			<span class="class_format_fontMuolLight" style="font-size: 14px">ឆ្នាំសិក្សា​
				<?php 
					echo "<b>  ".$year."</b>";
				?>
			</span>
		</div>
		<!-- ***********************************end header report************************************** -->

		<!-- ***************************************content report************************************* -->
		
		<div class="col-sm-12" style="margin-top:10px;">
			<div class="table-responsive">							
				<table border="1"​ align="center" id='listsubject' class="">
				    <thead>
				    	<tr>								        
					        <th width="50"  class="class_format_fontBattambang"><?php echo $this->lang->line("no");?></th>
					        <th  class="class_format_fontBattambang"><?php echo $this->lang->line("studentname");?></th>
					        <th  class="class_format_fontBattambang"><?php echo $this->lang->line("gender");?></th>
					        <th  class="class_format_fontBattambang">ស.ពិ .ប្រ .ឆមា</th>
					        <th  class="class_format_fontBattambang">ម.ប្រ .ឆមាស</th>
					        <th  class="class_format_fontBattambang">ខែប្រចាំឆមាស</th>
					        <th  class="class_format_fontBattambang">ម.សរុប</th>
					    </tr>
				    </thead>
		    		<tbody class="tbodys">
		    			<?php echo $tr;?>
		    		</tbody>
				</table>
			</div>
		</div>
		<!-- *************************************end content report********************************** -->

		<!-- ***************************************footer report************************************* -->
		<div style="margin: 0 auto; width: 630px; border:0px solid #f00;overflow: hidden;">
		<div class="col-sm-6"​ style="border:1px solid #fff;width: 310px !important; float: left;">
			<span style="margin-left:80px;" class="class_format_fontBattambang">បានឃើញ និង ឯកភាព</span><br>
			<span style="margin-left:10px;" class="class_format_fontBattambang">រាជធានីភ្នំពេញ ថ្ងៃទី...........ខែ..........ឆ្នាំ..........</span><br>
			<span style="margin-left:100px;​"><strong class="class_format_fontMuolLight">ជ.នាយកសាលា</strong></span><br>
			<span style="margin-left:115px;"><strong class="class_format_fontMuolLight">នាយករង</strong></span>
			<div class="span8 class_format_fontMuolLight" style="margin-left:115px; margin-top:60px;"></div>
			<!-- <span style="margin-left:170px;​"><strong>ឈ្មោះ</strong></span> -->
		</div>

		<div class="col-sm-6"​ style=" margin-bottom:50px; text-align: right; line-height: 22px; margin-top:1px;border:1px solid #fff;width: 315px !important; float: left;">
			<span style="margin-right:25px;" class="class_format_fontBattambang">រាជធានីភ្នំពេញ ថ្ងៃទី...........ខែ..........ឆ្នាំ..........</span><br>
			<span style="margin-right:120px;"><strong class="class_format_fontMuolLight">គ្រូទទួលបន្ទុកថ្នាក់</strong></span>
			<div class="span8 class_format_fontMuolLight" style="margin-left:115px; margin-top:60px;"></div>
			<!-- <span style="margin-right:70px;​"><strong>ឈ្មោះ</strong></span> -->
		</div> 
		</div>		
		<!-- **************************************end footer report********************************** -->
	</div>
</div>

<style>
   /*--table--*/
   .titles{
	   	font-size:14pt;
	   	line-height: 32px;   	
   }
   .border{
   	border: 1px;
   }
   .sub-titles{

	   	font-size:12pt;
		line-height: 22px;  
   }
   th, td{
   		text-align: center;
   		height: 27px;
   		vertical-align: middle !important;
   }
</style>
