<?php 
		$class_name = "";
		$year       = "";
		$tr = "";
		$i  = 1;
		// if($student_inf->num_rows() > 0){
		// 	foreach($student_inf->result() as $row){
		// 		$year       = $row->from_date."-".$row->to_date;
		// 		$class_name = $row->class_name;
		// 		$total_semester = (isset($avg_sem['sem_total'][$row->studentid]) ? $avg_sem['sem_total'][$row->studentid] : 0);
		// 		$avg_semester = (isset($avg_sem['sem_avg'][$row->studentid]) ? $avg_sem['sem_avg'][$row->studentid] : 0);
		// 		//$avg_month = (isset($avg_sem['month'][$row->studentid]) ? $avg_sem['month'][$row->studentid] : 0);
		// 		$show_rank = "";
		// 		if(isset($rank[$row->studentid])){
		// 			$show_rank = $rank[$row->studentid];
		// 		}
		// 		$show_mention = "";
		// 		if(isset($mention)){
		// 			foreach($mention as $kmention){
		// 				if($kmention['Min_m']<=$avg_semester && $kmention['Max_m']>=$avg_semester){
		// 					$show_mention = $kmention['Mention'];
		// 				}
		// 			}
		// 		}
		// 		$result_s = 0;
		// 		$tr.= '<tr>'.
		// 				'<td>'.$i.'</td>'.
		// 				'<td style="text-align: left !important;">'.$row->studentname.'</td>'.
		// 				'<td>'. ($row->gender == "female" ? "ស" : "ប") .'</td>'.
		// 				'<td style="text-align: center !important;"><b>'.$avg_semester.'</b></td>'.
		// 				'<td style="text-align: center !important;"><b>'.$show_rank.'</b></td>'.
		// 				'<td style="text-align: center !important;"><b>'.$show_mention.'</b></td>'.
		// 				'<td><b></b></td>'.										
		// 			 '</tr>';
		// 		$i++;
		// 	}
		// }	
		if(isset($avg_sem['sem_total'])){
			foreach($avg_sem['sem_total'] as $id=>$value){
				if(isset($student_inf[$id])){
					$class_name = $student_inf[$id]['classname'];
					$year       = $student_inf[$id]['year_name'];
					$avg_semester = (isset($avg_sem['sem_avg'][$id]) ? $avg_sem['sem_avg'][$id] : 0);
					$show_rank = "";
					if(isset($rank[$id])){
						$show_rank = $rank[$id];
					}
					$show_mention = "";
					if(isset($mention)){
						foreach($mention as $kmention){
							if($kmention['Min_m']<=$avg_semester && $kmention['Max_m']>=$avg_semester){
								$show_mention = $kmention['Mention'];
							}
						}
					}
					$tr.= '<tr>'.
							'<td class="class_format_fontBattambang">'.$i.'</td>'.
							'<td class="class_format_fontBattambang" style="text-align: left !important;">'.$student_inf[$id]['student_name'].'</td>'.
							'<td class="class_format_fontBattambang">'. ($student_inf[$id]['gender'] == "female" ? "ស" : "ប") .'</td>'.
							'<td class="class_format_fontBattambang" style="text-align: center !important;"><b>'.$avg_semester.'</b></td>'.
							'<td class="class_format_fontBattambang" style="text-align: center !important;"><b>'.$show_rank.'</b></td>'.
							'<td class="class_format_fontBattambang" style="text-align: center !important;"><b>'.$show_mention.'</b></td>'.
							'<td class="class_format_fontBattambang"><b></b></td>'.										
						 '</tr>';
					$i++;
				}
			}
		}else{
			foreach($student_inf as $stid=>$stvalue){
				$tr.= '<tr>'.
							'<td class="class_format_fontBattambang">'.$i.'</td>'.
							'<td class="class_format_fontBattambang" style="text-align: left !important;">'.$stvalue['student_name'].'</td>'.
							'<td class="class_format_fontBattambang">'. ($stvalue['gender'] == "female" ? "ស" : "ប") .'</td>'.
							'<td class="class_format_fontBattambang" style="text-align: center !important;"></td>'.
							'<td class="class_format_fontBattambang" style="text-align: center !important;"></td>'.
							'<td class="class_format_fontBattambang" style="text-align: center !important;"></td>'.
							'<td class="class_format_fontBattambang"><b></b></td>'.										
						 '</tr>';
					$i++;
			}
		}					
	?>
<div class="panel panel-default print_show_result" style="margin-top:5px;width: 670px; margin: 0 auto;">
	<div class="panel-body">
		<!-- *************************************header report***************************************** -->
		<div class="col-sm-12"  style="margin: 0 auto; width: 630px; border:0px solid #f00;overflow: hidden;">
			<div class="titles" style="border: 0px solid #f00;text-align: center;width: 255px;float: left;">
				<span class="class_format_fontBokor" style="font-size: 16px">ព្រះរាជាណាចក្រកម្ពុជា</span><br>
				<span class="class_format_fontBokor" style="font-size: 16px">ជាតិ   សាសនា   ព្រះមហាក្សត្រ</span>
			</div>

			<div class="sub-titles" style="border: 0px solid #f00;text-align: center;width: 320px;float: right;">
				<span class="class_format_fontBokor" style="font-size: 16px">មន្ទីរអប់រំ  យុវជន និងកីឡារាជធានីភ្នំពេញ</span><br>
				<span class="class_format_fontBokor" style="font-size: 16px">សាលាអន្តរជាតិ អាយ អ៊ី អ៊ែល</span><br>
				
			</div>
		</div>
		<div class="col-sm-12" style="margin: 0 auto; width: 630px; border:0px solid #f00;overflow: hidden;">
			<div class="col-sm-4 sub-titles" >
				<span style="margin-left:60px;font-size: 16px" class="class_format_fontBokor">ថ្នាក់ទីៈ  
				<?php 
					if(!empty($class_name)){
						echo "<b>".$class_name."</b>";
					}
				?>
				</span>
			</div>
		</div>
		<div class="col-sm-12 sub-titles" style="margin: 0 auto; width: 630px; border:0px solid #f00;overflow: hidden;">
			<div class="sub-titles " style="text-decoration: underline;margin-top: 20px;border: 0px solid #f00;width: 400px; margin: 0 auto;text-align: center;">
				<span class="class_format_fontMuolLight" style="font-size: 14px;">ចំណាត់ថ្នាក់ប្រចាំ
					<?php 
						echo "<b>".$title_semester."</b>";
					?>
				</span>
			</div>
		</div>
		

		<div class="col-sm-12 sub-titles" style="text-align: center; margin-top:20px;">
			<span class="class_format_fontMuolLight" style="font-size: 14px;">ឆ្នាំសិក្សា​
				<?php 
					echo "<b>  ".$year."</b>";
				?>
			</span>
		</div>
		<!-- ***********************************end header report************************************** -->

		<!-- ***************************************content report************************************* -->
		
		<div class="col-sm-12" style="margin-top:10px;">
			<div class="table-responsive">							
				<table border="1"​ align="center" id='listsubject' class="">
				    <thead>
				    	<tr>								        
					        <th class="class_format_fontBattambang" width="50"><?php echo $this->lang->line("no");?></th>
					        <th class="class_format_fontBattambang">ឈ្មោះសិស្ស</th>
					        <th class="class_format_fontBattambang">ភេទ</th>
					        <th class="class_format_fontBattambang">មធ្យភាគ</th>
					        <th class="class_format_fontBattambang">ចំណាត់ថ្នាក់</th>
					        <th class="class_format_fontBattambang">និទេ្ទស</th>
					        <th class="class_format_fontBattambang">ផ្សេង​ៗ</th>
					    </tr>
				    </thead>
		    		<tbody class="tbodys">
		    			<?php echo $tr;?>
		    		</tbody>
				</table>
			</div>
		</div>
		<!-- *************************************end content report********************************** -->

		<!-- ***************************************footer report************************************* -->
		<div style="margin: 0 auto; width: 630px; border:0px solid #f00;overflow: hidden;">
			<div class="col-sm-6"​ style="border:1px solid #fff;width: 310px !important; float: left;">
				<span style="margin-left:80px;" class="class_format_fontBattambang">បានឃើញ និង ឯកភាព</span><br>
				<span style="margin-left:10px;" class="class_format_fontBattambang">រាជធានីភ្នំពេញ ថ្ងៃទី...........ខែ..........ឆ្នាំ..........</span><br>
				<span style="margin-left:100px;​"><strong class="class_format_fontMuolLight">ជ.នាយកសាលា</strong></span><br>
				<span style="margin-left:115px;"><strong class="class_format_fontMuolLight">នាយករង</strong></span>
				<div class="span8 class_format_fontMuolLight" style="margin-left:115px; margin-top:60px;"></div>
				<!-- <span style="margin-left:170px;​"><strong>ឈ្មោះ</strong></span> -->
			</div>

			<div class="col-sm-6"​ style=" margin-bottom:50px; text-align: right; line-height: 22px; margin-top:1px;border:1px solid #fff;width: 315px !important; float: left;">
				<span style="margin-right:25px;" class="class_format_fontBattambang">រាជធានីភ្នំពេញ ថ្ងៃទី...........ខែ..........ឆ្នាំ..........</span><br>
				<span style="margin-right:120px;"><strong class="class_format_fontMuolLight">គ្រូទទួលបន្ទុកថ្នាក់</strong></span>
				<div class="span8 class_format_fontMuolLight" style="margin-left:115px; margin-top:60px;"></div>
				<!-- <span style="margin-right:70px;​"><strong>ឈ្មោះ</strong></span> -->
			</div> 
		</div>		
		<!-- **************************************end footer report********************************** -->
	</div>
</div>

<style>
   /*--table--*/
   .titles{
	   	font-size:14pt;
	   	text-align: center;
	   	line-height: 32px;   	
   }
   .border{
   	border: 1px;
   }
   .sub-titles{
	   	font-size:12pt;
		line-height: 22px;  
   }
   th, td{
   		text-align: center;
   		height: 27px;
   		vertical-align: middle !important;
   }
</style>
