<div class="row1" id="div_export_print" style="margin-top:30px !important; width: 670px; margin: 0 auto; border:1px solid #fff; overflow: hidden; ">  
<style>
   /*--table--*/
   .texts{
   		font-family:'Khmer OS Muol Light';
   }
   .titles{
	   	font-family: 'Kh Muol';
	   	font-size:13pt;
	   	text-align: center;
	   	line-height: 25px;   	
   }
   .text-comment{
   		border-top: hidden;
   		border-right: hidden;
   		border-left: hidden;
   		border-bottom: 1px #ccc dotted;
   		width: 900px;
   }
   .col-sm-6, .col-sm-4{
   		line-height: 22px;
   }
   th{
	   	text-align: center;
	   	vertical-align: middle !important;
   }
  table tr td {
  	line-height: 15px !important;
  }
  .check_printing {
  	width: 670px;
  	height: 100% !important; 
  	margin: 0 auto;
  	border: 1px solid #fff !important; 
  	overflow: hidden;
  }
  .check_normal_printing {
  	width: 655px;
  	height: 100% !important; 
  	margin: 0 auto;
  	border: 0px solid #00f; 
  }
  .text_padding {
  	padding: 5px 0px !important;
  }
  

</style>  
	<div class="" style="border: 0px solid #000;">
		<!-- *************************************header report***************************************** -->
		<div class="col-sm-12">
			<img width="200" src="<?php echo base_url('assets/images/logo/logo.png')?>" />
		</div>

		<div class="col-sm-12 titles" style="margin-bottom:5px;padding-top:10px; text-align: center;">
			<span  style="font-family: Khmer Os Battambang;"><b>របាយការណ៏សិក្សាប្រចាំខែ</b>
				<?php 
					echo "<b>".$this->lang->line("month_".$month_name)."</b>";
				?>
			</span><br>
			<?php
				$str_monthly="";
				$arr_monthly = explode(',', $_GET['monthly']);
				if(isset($arr_monthly) && count($arr_monthly)>1){
					foreach ($arr_monthly as $value) {
					   $str_monthly.=DateTime::createFromFormat('!m', $value)->format('F')." ";
					}
				}else{
					 $str_monthly.=DateTime::createFromFormat('!m', $_GET['monthly'])->format('F')." ";
				}

			?>
			<span><b>Study Report in <?php echo $str_monthly; ?></b></span>
			
		</div>

		<div class="col-sm-12 check_printing" style="margin-top:20px;border: 0px solid #f00">
			<div class="col-sm-6" style="text-align:center;​width:320px !important; float: left;border: 1px solid #fff">
				<span  style="font-family: Khmer Os Battambang;">ឈ្មោះសិស្សៈ</span> <span class="texts">
						<?php
							echo "<span style='font-family: Khmer OS Muol Light;font-size:12px;'><b>".(isset($row->studentname) ? $row->studentname : '')."</b></span>";
						?>
				</span>
			</div>

			<div class="col-sm-3" style="text-align:center;width:150px; float: left;border: 1px solid #fff">
				<span style="font-family: Khmer Os Battambang;">រៀនថ្នាក់ទីៈ 
					<?php
						echo "<span style='font-family: Khmer Os Battambang;'><b>".(isset($row->class_name) ? $row->class_name : '')."</b></span>";
					?>
				</span>
			</div>

			<div class="col-sm-3" style="text-align:right;width:150px; float: left;border: 1px solid #fff">
				<span style="font-family: Khmer Os Battambang;">ឆ្នាំសិក្សាៈ
					<?php echo "<b>".$row->from_date.'-'.$row->to_date."</b>";?>
				</span>
			</div>
			
		</div>		
		<div class="col-sm-12" style="text-align:center;font-family: Khmer Os Battambang;">ពិន្ទុ និងការវាយតម្លៃមុខវិជ្ជានីមួយៗ / Score and Evaluation of Each Subject</div>
		<!-- ***********************************end header report************************************** -->

		<!-- ***************************************content report************************************* -->
		<!--table 1 table table-bordered-->
		<div class="col-sm-12" style="width: 670px;height: 100% !important; margin: 0 auto;border: 0px solid #00f; overflow: hidden;">
		<div style="width: 305px; border: 0px solid #f00;float: left;">
			<div class="span8" style="font-family: Khmer Os Battambang;">ផ្នែកៈ ចំណេះទូទៅភាសាខ្មែរ</div>
			<div class="table-responsive">							
			<table border="1"​ align="center" id='listsubject' width="60%">
				<?php
					echo $tr;
				?>
			    <tbody class="tbodys">
			    	
			    </tbody>
			</table>
		</div>	 							
		</div><!--end table 1-->

		<!--table 2-->
		<div style="width: 305px; border: 0px solid #000;float: right;">
			<div class="span8" style="font-family: Khmer Os Battambang;">ផ្នែកៈ ភាសាអង់គ្លេស កុំព្យូទ័រ និង ជំនាញផ្សេងៗ</div>
			<div class="table-responsive">							
			<table border="1"​ align="center" id='listsubject' class="">

				<?php
					echo $trr;
				?>
			   
			    <tbody class="tbodys">
			    	
			    </tbody>
			</table>
		</div>	 		
		</div><!--end table 2-->
</div>
		<!--total score and average-->
		<div class="col-sm-12" style="margin-top:5px; border: 0px solid #f00;overflow: hidden;">
			<div class="text_padding" style="float: left; border: 0px solid #000; width: 155px; font-size: 12px">
				<span style="font-family: Khmer Os Battambang;">ពិន្ទុសរុប​ / Total:  
				<?php 
						echo "<b>".(isset($gtotal) ? $gtotal : '0.00')."</b>";
				?>
				</span>
			</div>

			<div class="text_padding" style="float: left; border: 0px solid #000; width: 175px; font-size: 12px">
				<span style="font-family: Khmer Os Battambang;">មធ្យមភាគ / Average: 
					<?php
						echo "<b>".(isset($rows->average_score) ? $rows->average_score : '0')."</b>";
					?>
				</span>
			</div>
			<?php 
				$average_score = isset($rows->average_score) ? $rows->average_score : 0;
				$show_grade = "";
				if($grade_kh->num_rows() > 0){
					foreach($grade_kh->result() as $kg){
						if($kg->min_score <= $average_score && $kg->max_score >= $average_score){
							$show_grade = $kg->mention_kh;
						}
					}
				}
			?>
			<div class="text_padding" style="float: left; border: 0px solid #000; width: 135px; font-size: 12px; text-align: left;">
				<span style="font-family: Khmer Os Battambang;">និទ្ទេស&nbsp;: 
					<?php
						echo "<b>".$show_grade."</b>";
					?>
				</span>
			</div>
			<div class="text_padding" style="float: left; border:0px solid #000; width: 160px; font-size: 12px">
				<span style="font-family: Khmer Os Battambang;">ចំណាត់ថ្នាក់ / Rank: 
					<?php
						echo "<b>".(isset($rows->ranking_bysubj) ? $rows->ranking_bysubj : '')."/".(isset($total_row) ? $total_row : '')."</b>";
					?>
				</span>
			</div>
			<!--space-->
			<div class="text_padding" style="text-align:right;​width:207px; float: left;border: 0px solid #000">
				<span style="font-family: Khmer Os Battambang;">ចំនួនអវត្តមានសរុបៈ 
				<?php
					$absent  = isset($rows->absent) ? $rows->absent : 0;
					$present = isset($rows->present) ? $rows->present : 0;
					$total_attendance = ($absent+$present);
					echo "<b>".$total_attendance."</b>";
				?>
				 ដង</span>
			</div>

			<div class="text_padding" style="text-align:center;width:207px; float: left;border: 0px solid #000">
				<span style="font-family: Khmer Os Battambang;">គ្មានច្បាប់ : <?php echo "<b>".$absent."</b>";?> ដង</span>
			</div>

			<div class="text_padding" style="text-align:right;width:207px; float: left;border: 0px solid #000">
				<span style="font-family: Khmer Os Battambang;">មានច្បាប់ៈ <?php echo "<b>".$present."</b>";?> ដង</span>
			</div>
		</div>
	<!--end total score and average-->
	<?php
	// show comment student
	$comm_teacher = "";
	$comm_guadian = "";
	$date_academic = "";
	$date_teacher  = "";
	$date_return   = "";
	$date_guardian = "";
	$academic_name   = "";
	$teacherename    = "";
	if($comment->num_rows() > 0){
		foreach($comment->result() as $row_comm){
			$comm_teacher = $row_comm->command_teacher;
			$comm_guadian = $row_comm->command_guardian;
			$date_academic = explode("-",$row_comm->date_academic);
			$date_teacher  = explode("-",$row_comm->date_teacher);
			$date_return   = $row_comm->date_return;
			$date_guardian = explode("-",$row_comm->date_guardian);
			$academic_name   = $row_comm->academic_name;
			$teacherename   = $row_comm->teacher_name;
		}
	}

	?>
	<!--teacher comment-->
	<div class="col-sm-12" style="margin-top:10px;">
		<div class="panel panel-default" style=" border: 1px solid #000">
			<div class="span8" style="margin:5px 20px -3px 20px;font-family: Khmer Os Battambang;">មតិយោបល់របស់គ្រូ / Teacher's Comments</div>
			<div class="span12" style="margin:3px 20px 20px 20px;height: 70px;">
				<p><?php echo $comm_teacher;?></p>
			</div>
		</div>	 							
		</div><!--end teacher comment-->
		<!-- *************************************end content report********************************** -->

		<!-- ***************************************footer report************************************* -->
		<div style="border: 1px solid #fff; height: 100%; overflow: hidden;">
		<div class="col-sm-6"​ style="line-height: 22px; border: 1px solid #fff; width: 325px; float: left;">
			<span style="margin-left:80px;font-family: Khmer Os Battambang;">បានឃើញ និង ឯកភាព</span><br>
			<span style="margin-left:55px;font-family: Khmer Os Battambang;">ថ្ងៃទី&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_academic[0])?$date_academic[0]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ខែ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_academic[1])?$date_academic[1]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ឆ្នាំ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_academic[2])?$date_academic[2]:"";?></span><br>
			<span class="texts" style="margin-left:70px;​font-family: Khmer OS Muol Light;">ប្រធានការិយាល័យសិក្សា</span><br>
			<div class="span8" style="margin-left:115px; margin-top:30px;"></div>
			<span style="margin-left:120px;​"><strong style="font-family: Khmer Os Battambang;">ឈ្មោះ&nbsp;&nbsp;&nbsp;<?php echo $academic_name;?></strong></span>
		</div>

		<div class="col-sm-6"​ style="text-align: right; line-height: 22px; border: 1px solid #fff; width: 325px; float: left;">
			<div style="margin-right:40px;font-family: Khmer Os Battambang;">ថ្ងៃទី&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_teacher[0])?$date_teacher[0]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ខែ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_teacher[1])?$date_teacher[1]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ឆ្នាំ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_teacher[2])?$date_teacher[2]:"";?><br>
			</div>
			<span class="texts" style="margin-right:90px;font-family: Khmer OS Muol Light">គ្រូប្រចាំថ្នាក់</span>
			<div class="span8" style="margin-left:100px; margin-top:30px;"></div>
			<span style="margin-right:85px;​"><strong style="font-family: Khmer Os Battambang;">ឈ្មោះ&nbsp;&nbsp;&nbsp;<?php echo $teacherename;?></strong></span>
		</div> 
	</div>
		<!--parents comment-->
		<div class="col-sm-12">
			<div class="panel panel-default" style=" border: 1px solid #000">
				<div class="span8" style="margin:5px 20px -3px 20px;font-family: Khmer Os Battambang;">មតិមាតាបិតាសិស្ស / Guardian's Comments</div>
				<div class="span12" style="margin:3px 20px 20px 20px;height:70px;">
					<p><?php echo $comm_guadian;?></p>
				</div>
			</div>	 							
		</div><!--end parents comment-->

		<div class="col-sm-6"​ style="line-height: 22px; margin:-15px 0 40px 0;width: 320px; float:left;">
			<span><strong style="font-family: Khmer Os Battambang;">សូមប្រគល់សៀវភៅតាមដាននេះមកសាលាអោយបានមុនថ្ងៃទី៖</strong></span>
			<span>Please Return the book to school before:</span><br>
			<span><input type="text" class="text-comment remove_tag"  style="width:255px;text-align: center;" value="<?php echo $date_return;?>" /></span>
		</div>

		<div class="col-sm-6"​ style="text-align: right; line-height: 22px; margin:-15px 0 40px 0;width: 320px; float:left;">
			<span style="font-family: Khmer Os Battambang;">ថ្ងៃទី&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_guardian[0])?$date_guardian[0]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ខែ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_guardian[1])?$date_guardian[1]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ឆ្នាំ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_guardian[2])?$date_guardian[2]:"";?><br>
			<span><strong style="font-family: Khmer Os Battambang;">ហត្ថលេខា និង ឈ្មោះមាតាបិតា រឺ អាណាព្យាបាល</strong></span>		 						
		</div> 		
		<!-- **************************************end footer report********************************** -->
	</div>
</div>
<div style="page-break-after: always;"></div>

<script type="text/javascript">
    $(function(){
    	$("#export").on("click", function(){
            var data=$('.table').attr('border',1);
            var data=$('.table').attr('border',1);
                data = $("#div_export_print").html().replace(/<img[^>]*>/gi,"");
            var export_data=$("<center><meta charset='utf-8'>"+data+"</center>").clone().find(".remove_tag").remove().end().html(); 
                this.href = "data:application/vnd.ms-excel,"+encodeURIComponent(export_data);
                this.download = "របាយការណ៏សិក្សាប្រចាំខែ មករា.xls";
                $('.table').attr('border',0);
        });
	});
</script>
