<div class="row1" id="div_export_print" style="margin-top:30px !important;width: 670px; margin: 0 auto;">  
<style>
   /*--table--*/
   .texts{
   }
   .titles{
	   	font-size:13pt;
	   	text-align: center;
	   	line-height: 25px;   	
   }
   .text-comment{
   		border-top: hidden;
   		border-right: hidden;
   		border-left: hidden;
   		border-bottom: 1px #ccc dotted;
   		/*width: 900px;*/
   }
   .col-sm-6, .col-sm-4{
   		line-height: 22px;
   }
   th{
	   	text-align: center;
	   	vertical-align: middle !important;
   }
  table tr td {
  	line-height: 15px !important;
  }
</style>  
	<div class="" style="border: 1px solid #fff">
		<!-- *************************************header report***************************************** -->
		<div class="col-sm-12">
			<img width="200" src="<?php echo base_url('assets/images/logo/logo.png')?>" />
		</div>

		<div class="col-sm-12 titles" style="margin-bottom:5px;padding-top:10px; text-align: center;border: 0px solid #f00;">
			<span class="class_format_fontMuolLight"><b>របាយការណ៏សិក្សាប្រចាំឆ្នាំ</b>
				<?php 
					echo "<b>".$showyear."</b>";
				?>
			</span>
			<!-- <span>Study Report in December</span> -->
		</div>
		<div class="col-sm-12" style=" text-align: center;border: 0px solid #f00;font-size: 14px;padding-bottom: 10px;">
			<span>Final Report for Academeic Year <?php echo $showyear;?>
			</span><br>
			<!-- <span>Study Report in December</span> -->
		</div>
		<div class="col-md-12" style="width: 670px !important;overflow: hidden;border: 0px solid #f00; margin: 0 auto;">
			<div class="col-md-5 class_format_fontBattambang" style="margin-left:100px ;​width:300px !important; float: left;border: 0px solid #f00;padding-bottom: 10px;">
				ឈ្មោះសិស្សៈ <span class="texts">
						<?php
							echo "<span class='class_format_fontMuolLight'><b>".(isset($row->studentname) ? $row->studentname : '')." </b><span>";
						?>
				</span>
			</div>

			<div class="col-md-2" style="text-align:center;​width:100px !important; float: left;border: 0px solid #f00;padding-bottom: 10px;">
				<span class="class_format_fontBattambang">រៀនថ្នាក់ទីៈ 
					<?php
						echo "<b>".(isset($row->class_name) ? $row->class_name : '')." </b>";
					?>
				</span>
			</div>

			<div class="col-md-3" style="text-align:left;​width:300px !important; float: right;border: 0px solid #f00;padding-bottom: 10px;">
				<span class="class_format_fontBattambang">ឆ្នាំសិក្សាៈ
					<?php echo "<b>".$showyear."</b>";?>
				</span>
			</div>
			
		</div>		
		<div class="class_format_fontBattambang" style="text-align:center;width: 670px !important;margin: 0 auto;padding-bottom: 10px;">ពិន្ទុ និងការវាយតម្លៃមុខវិជ្ជានីមួយៗ / Score and Evaluation of Each Subject</div>
		<!-- ***********************************end header report************************************** -->

		<!-- ***************************************content report************************************* -->
		<!--table 1 table table-bordered-->
		<div style="margin: 0 auto; width: 670px; border:0px solid #fff;overflow: hidden;">
		<div style="width: 328px;margin-left: 1px; float: left;border: 0px solid #fff;">
			<div class="span8 class_format_fontBattambang">ផ្នែកៈ ចំណេះទូទៅភាសាខ្មែរ</div>
			<div class="table-responsive">							
			<table border="1"​ align="center" id='listsubject' class="">
				<?php
					echo $tr;
				?>
			    <tbody class="tbodys">
			    	
			    </tbody>
			</table>
		</div>	 							
		</div><!--end table 1-->

		<!--table 2-->
		<div style="margin-bottom:-10px;margin-left: 10px;width: 328px; float: left;border: 0px solid #fff;">
			<div class="span8 class_format_fontBattambang">ផ្នែកៈ ភាសាអង់គ្លេស កុំព្យូទ័រ និង ជំនាញផ្សេងៗ</div>
			<div class="table-responsive">							
			<table border="1"​ align="center" id='listsubject' class="">

				<?php
					echo $trr;
				?>
			   
			    <tbody class="tbodys">
			    	
			    </tbody>
			</table>
		</div>	 		
		</div><!--end table 2-->
	</div>
		<!--total score and average-->
		<div class="col-sm-12" style="margin: 0 auto; width: 670px; border:0px solid #f00;overflow: hidden;">
			<div class="col-sm-4" style="margin-left: 1px;margin-top: 10px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">ពិន្ទុសរុប​ / Total:  
				<?php 
						echo "<b>".(isset($gtotal) ? $gtotal : '0.00')."</b>";
				?>
				</span>
			</div>
			<div class="col-sm-4" style="margin-top: 10px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">មធ្យមភាគប្រចាំឆមាសទី១ :  
				<?php 
						echo "<b>".(isset($avg_smt_1) ? $avg_smt_1 : '0.00')."</b>";
				?>
				</span>
			</div>
			<div class="col-sm-4" style="margin-top: 10px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">មធ្យមភាគប្រចាំឆមាសទី២ :  
				<?php 
						echo "<b>".(isset($avg_smt_2) ? $avg_smt_2 : '0.00')."</b>";
				?>
				</span>
			</div>
		</div>
		<div class="col-sm-12" style="margin: 0 auto; width: 670px; border:0px solid #f00;overflow: hidden;">
			<div class="col-sm-4"  style="margin-top: 0px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">មធ្យមភាគប្រចាំឆ្នាំ : 
					<?php
					$smt1 = isset($avg_smt_1)?$avg_smt_1:0;
					$smt2 = isset($avg_smt_2)?$avg_smt_2:0;
					$final_y = ($smt1+$smt2)/2;
					$show_y = number_format($final_y,2);
						echo "<b>".$show_y."</b>";
					?>
				</span>
			</div>
			<div class="col-sm-4"  style="margin-top: 0px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">និទ្ទេស : 
					<?php
						$show_mention_f = "";
						if(count($mention_final)>0){
							foreach($mention_final as $m_f){
								if($m_f['Max_s'] >= $show_y && $m_f['Min_s'] <= $show_y){
									$show_mention_f = $m_f['Mention_kh'];
								}
							}
						}
						echo "<b>".$show_mention_f."</b>";
					?>
				</span>
			</div>
			<div class="col-sm-4"  style="margin-top: 0px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">ចំណាត់ថ្នាក់ / Rank: 
					<?php
						echo "<b>".(isset($rows->ranking_bysubj) ? $rows->ranking_bysubj : '')."/".(isset($total_row) ? $total_row : '')."</b>";
					?>
				</span>
			</div>
		</div>
		<div class="col-sm-12" style="margin: 0 auto; width: 670px; border:0px solid #f00;overflow: hidden;">
			<!--space-->
			<div class="col-sm-4"  style="margin-top: 0px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">ចំនួនអវត្តមានសរុបៈ 
				<?php
					echo "<b>".(isset($rows->absent) ? $rows->absent : '0')."</b>";
				?>
				 ដង</span>
			</div>
			<div class="col-sm-4"  style="margin-top: 0px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">មានច្បាប់ៈ 
					<?php
						echo "<b>".(isset($rows->present) ? $rows->present : '0')."</b>";
					?>
				 ដង</span>
			</div>
			<div class="col-sm-4"  style="margin-top: 0px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">គ្មានច្បាប់ : 0 ដង</span>
			</div>
		</div>
	<!--end total score and average-->
	<?php
	// show comment student
	$comm_teacher = "";
	$comm_guadian = "";
	$date_academic = "";
	$date_teacher  = "";
	$date_director   = "";
	$director_name   = "";
	$academic_name   = "";
	$teacherename    = "";
	if($comment->num_rows() > 0){
		foreach($comment->result() as $row_comm){
			$comm_teacher = $row_comm->command_teacher;
			$date_academic = explode("-",$row_comm->date_academic);
			$date_teacher  = explode("-",$row_comm->date_teacher);
			$date_director = explode("-",$row_comm->date_director);
			$directorid = $row_comm->directorid;
			$academicid = $row_comm->academicid;
			$teacherid = $row_comm->teacherid;
			$sql_name = $this->db->query("SELECT
											sch_user.userid,
											sch_user.user_name,
											sch_user.last_name,
											sch_user.first_name,
											sch_user.emp_id,
											sch_user.match_con_posid
											FROM
											sch_user
											WHERE 1=1 
											AND match_con_posid ='tch'
											OR match_con_posid ='acca'");
			if($sql_name->num_rows() > 0){
				foreach($sql_name->result() as $ruser){
					if($ruser->userid == $directorid){
						$director_name = $ruser->last_name." ".$ruser->first_name;
					}
					if($ruser->userid == $academicid){
						$academic_name = $ruser->last_name." ".$ruser->first_name;
					}
					if($ruser->userid == $teacherid){
						$teacherename = $ruser->last_name." ".$ruser->first_name;
					}
				}
			}
			//$academic_name = $row_comm->academic_name;
			//$teacherename  = $row_comm->teacher_name;
		}
	}
	?>
	<!--teacher comment-->
	<div class="col-sm-12" style="margin-top:10px;">
		<div class="panel panel-default" style=" border: 1px solid #000">
			<div class="span8 class_format_fontBattambang" style="margin:5px 20px -3px 20px;">មតិយោបល់របស់គ្រូ / Teacher's Comments</div>
			<div class="span12" style="margin:3px 20px 20px 20px;height: 70px;">
				<p class="class_format_fontBattambang"><?php echo $comm_teacher;?></p>
			</div>
		</div>	 							
		</div><!--end teacher comment-->
		<!-- *************************************end content report********************************** -->

		<!-- ***************************************footer report************************************* -->
		<div class="col-sm-4"​ style="line-height: 22px; margin-top:60px;text-align: center;width: 212px !important; float: left;">
			<span style="" class="class_format_fontBattambang">បានឃើញ និង ឯកភាព</span><br>
			<span style="" class="class_format_fontBattambang">ថ្ងៃទី&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_director[2])?$date_director[2]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ខែ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_director[1])?$date_director[1]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ឆ្នាំ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_director[0])?$date_director[0]:"";?></span><br>
			<span class="texts"><strong class="class_format_fontMuolLight">នាយក</strong></span><br>
			<div class="span8" style=" margin-top:30px;"></div>
			<span class="class_format_fontBattambang"><strong><?php echo $director_name;?></strong></span>
		</div>
		<div class="col-sm-4"​ style="text-align: center; line-height: 22px; margin:25px 0 40px 0;width: 212px !important; float: left;">
			<span class="class_format_fontBattambang">បានឃើញ និងពិនិត្យត្រឹមត្រូវ</span><br>
			<span class="class_format_fontBattambang">ថ្ងៃទី&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_academic[2])?$date_academic[2]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ខែ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_academic[1])?$date_academic[1]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ឆ្នាំ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_academic[0])?$date_academic[0]:"";?><br>
			<span class="class_format_fontMuolLight"><b>ប្រធានការិយាល័យសិក្សា</b></span>
			<div class="span8" style=" margin-top:30px;"></div>
			<span style=""><strong class="class_format_fontBattambang"><?php echo $academic_name;?></strong></span>						
		</div> 		
		<div class="col-sm-4"​ style="text-align: center; line-height: 22px; margin-top:-15px;width: 212px !important; float: left;">
			<div class="class_format_fontBattambang">ថ្ងៃទី&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_teacher[2])?$date_teacher[2]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ខែ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_teacher[1])?$date_teacher[1]:"";?>&nbsp;&nbsp;&nbsp;&nbsp;ឆ្នាំ&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($date_teacher[0])?$date_teacher[0]:"";?><br>
			</div>
			<span class="texts"><strong class="class_format_fontMuolLight">គ្រូប្រចាំថ្នាក់</strong></span>
			<div class="span8" style=" margin-top:30px;"></div>
			<span style=""><strong><?php echo $teacherename;?></strong></span>
		</div> 

		
		<!-- **************************************end footer report********************************** -->
	</div>
</div>
<div style="page-break-after: always;"></div>

<script type="text/javascript">
    $(function(){
    	$("#export").on("click", function(){
            var data=$('.table').attr('border',1);
            var data=$('.table').attr('border',1);
                data = $("#div_export_print").html().replace(/<img[^>]*>/gi,"");
            var export_data=$("<center><meta charset='utf-8'>"+data+"</center>").clone().find(".remove_tag").remove().end().html(); 
                this.href = "data:application/vnd.ms-excel,"+encodeURIComponent(export_data);
                this.download = "របាយការណ៏សិក្សាប្រចាំខែ មករា.xls";
                $('.table').attr('border',0);
        });
	});
</script>
