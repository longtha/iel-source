<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
			  	<button type="button" class="close" data-dismiss="modal">&times;</button>
			  	<h4 class="modal-title">Add comment student monthly</h4>
			</div>
			<div class="modal-body">
                <div class="row">
                	<div class="col-sm-12">
                		<input type="hidden" name="hstudentid" id="hstudentid">
                		<div class="form-group">
				           	<label for="inputEmail">មតិយោបល់របស់គ្រូ / Teacher's Comments</label>
				           	<textarea rows="5" class="form-control" id="comment_teacher"></textarea>
				        </div>
                	</div>
                	<div class="col-sm-12">
                		<div class="col-sm-6" style="text-align: center;display: none;"><label>បានឃើញ និង ឯកភាព</label></div>
                		<div class="col-sm-6">
                			<label class="col-sm-4">ថ្ងៃទី</label>
                			<div class="col-sm-8"><input type="text" class="form-control" name="date_approve" id="date_approve" value="<?php echo date('d-m-Y');?>"></div>
                		</div>
                		<div class="col-sm-6">
                			<label class="col-sm-4">ថ្ងៃទី</label>
                			<div class="col-sm-8"><input type="text" class="form-control" name="date_teacher" id="date_teacher" value="<?php echo date('d-m-Y');?>"></div>
                		</div>
                	</div>
                	<div class="col-sm-12">
                		<div class="col-sm-6">
                			<label class="col-sm-4">ប្រធានការិយាល័យ</label>
                			<div class="col-sm-8">
                				<select class="form-control" id="managername">
		                		<?php 
		                			$sql_m = $this->db->query("SELECT
																sch_user.user_name,
																sch_user.userid,
																concat(
																	sch_user.last_name,
																	_utf8 ' ',
																	sch_user.first_name
																) AS fullname,
																sch_user.match_con_posid
																FROM
																sch_user
																WHERE match_con_posid='acca'");
		                			$option_man = "<option value=''></option>";
		                			if($sql_m->num_rows() > 0){
		                				foreach($sql_m->result() as $row_man){
		                					$option_man.= "<option value='".$row_man->userid."'>".$row_man->fullname."</option>";
		                				}
		                			}
		                			echo $option_man;
		                		?>
		                		</select>
                			</div>
                		</div>
                		<div class="col-sm-6">
                			<label class="col-sm-4">គ្រូប្រចាំថ្នាក់</label>
	                		<div class="col-sm-8">
		                		<select class="form-control" id="teachername">
		                			<?php 
		                				$userid = $this->session->userdata('userid');
			                			$sql_teacher = $this->db->query("SELECT
																	sch_user.user_name,
																	sch_user.userid,
																	concat(
																		sch_user.last_name,
																		_utf8 ' ',
																		sch_user.first_name
																	) AS fullname,
																	sch_user.match_con_posid
																	FROM
																	sch_user
																	WHERE userid='".$userid."'");
			                			$option_teacher = "";
			                			if($sql_teacher->num_rows() > 0){
			                				foreach($sql_teacher->result() as $row_t){
			                					$option_teacher.= "<option value='".$row_t->userid."'>".$row_t->fullname."</option>";
			                				}
			                			}
			                			echo $option_teacher;
			                		?>
		                		</select>
		                	</div>
                		</div>
                	</div>
                	<div class="col-sm-12">
                		<div class="form-group">
				           	<label for="inputEmail">មាតាបិតាសិស្ស / Guardian's Comments</label>
				           	<textarea rows="5" class="form-control" id="comment_parent"></textarea>
				        </div>
                	</div>
                	<div class="col-sm-12">
                		<div class="col-sm-6">
                			<p>សូមប្រគល់សៀវភៅតាមដាននេះមកសាលាអោយមុនថ្ងៃទី</p>
                			<p>Please return the book to school before:</p>
                			<div class="col-sm-8">
                				<input type="text" class="form-control" name="date_return" id="date_return" value="<?php echo date('d-m-Y');?>">
                			</div>
                		</div>
                		<div class="col-sm-6">
                			<label class="col-sm-4">ថ្ងៃទី</label>
                			<div class="col-sm-8" style="padding-left: 0px;"><input type="text" class="form-control" name="date_parents" id="date_parents" value="<?php echo date('d-m-Y');?>"></div>
                		</div>
                	</div>
                </div>
            </div>
              <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save_comment">Save</button>
            </div>
		</div>
	</div>
</div>

<div class="panel panel-default" style="margin-top:5px;">
	<div class="table-responsive">							
		<table border="1"​ align="center" id='listsubject' class="col-sm-12">
		    <thead>
		    	<tr>								        
			        <th width="50">#</th>
			        <th width="200">Student Name</th>
			        <th width="150">School lavel</th>
			        <th width="90">Year</th>
			        <th width="90">Class</th>
			        <th width="90">Monthly</th>
			        <th width="80">Score</th>
			        <th width="100"><a href="javascript:void(0)" id="print_all" title="Print all"><img src="<?php echo base_url("assets/images/icons/print.png"); ?>"></a></th>
			    </tr>
		    </thead>
			<tbody class="tbodys">
				<?php 
					//print_r($score_m);
					$tr = "";
					if($result->num_rows() >0){
						$i = 1;
						foreach($result->result() as $row_r){
							$score_show = isset($score_m[$row_r->studentid])?$score_m[$row_r->studentid]:0;
							$tr.="<tr>
									<td>".($i++)."</td>
									<td>".$row_r->studentname."</td>
									<td>".$row_r->sch_level."</td>
									<td>".$row_r->sch_year."</td>
									<td>".$row_r->class_name."</td>
									<td>".$monthly."</td>
									<td>".$score_show."</td>
									<td>
										<a href='javascript:void(0)' id='add_comm' studentid_a='".$row_r->studentid."'><img src='".base_url('assets/images/icons/add.png')."' style='margin-right:0;'></a>
										&nbsp;
										<a href='".site_url("reports/generalkhmerlanguage/primary_monthly_report_individual_ranking")."?classid=".$row_r->classid."&yearid=".$row_r->year."&schoolid=".$row_r->schoolid."&schlavelid=".$row_r->schlevelid."&grandlavelid=".$row_r->grade_levelid."&programid=".$row_r->programid."&studentid=".$row_r->studentid."&monthly=".$monthlyid."' target='_blank' id='add_print' studentid_v='".$row_r->studentid."'><img src='".base_url('assets/images/icons/a_preview.png')."' style='margin-right:0;'></a>
									</a>
									</td>
								</tr>";
						}
					}
					echo $tr;		
				?>
			</tbody>
		</table>
	</div>
</div>

<style>
   /*--table--*/
   .titles{
	   	font-family: 'Kh Muol Pali';
	   	font-size:14pt;
	   	text-align: center;
	   	line-height: 32px;   	
   }
   .border{
   	border: 1px;
   }
   .sub-titles{
	   	font-family: 'Kh Bokor'!important;
	   	font-size:12pt;
		line-height: 22px;  
   }
   th, td{
   		text-align: center;
   		height: 27px;
   		vertical-align: middle !important;
   }
</style>
