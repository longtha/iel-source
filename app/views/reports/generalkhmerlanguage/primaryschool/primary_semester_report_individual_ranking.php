<div style="margin-top:30px !important;width: 670px; margin: 0 auto;">    
	<div class="panel-body" style="border: 0px solid #000">
		<!-- *************************************header report***************************************** -->
		<div class="row col-sm-12">
			<img width="200" src="<?php echo base_url('assets/images/logo/logo.png')?>" />
		</div>

		<div class="col-sm-12 titles" style="margin-bottom:5px;padding-top:10px; text-align: center;border: 0px solid #f00;">
			<span class ="class_format_fontBattambang"><strong>របាយការណ៏សិក្សាប្រចាំ
				<?php
					echo $semestername;
					//echo "<b>".$this->lang->line("month_".$month_name)."</b>";
				?>
			</strong></span><br>
			<span style="font-size: 18px;">Semester | Report</span>
		</div>

		<div class="col-md-12" style="width: 650px !important;overflow: hidden;border: 0px solid #f00; margin: 0 auto;">
			<div class="col-md-4​​ class_format_fontBattambang" style="text-align:right;​width:230px !important; float: left;border: 0px solid #f00;margin-left: 50px">
				ឈ្មោះសិស្សៈ <span class="texts">
						<?php
							echo "<span class='class_format_fontMuolLight'><b>".(isset($row->studentname) ? $row->studentname : '')."</b></span>";
						?>
				</span>
			</div>

			<div class="col-md-1" style="text-align:center;width:150px !important; float: left;border: 0px solid #f00;">
				<span class="class_format_fontBattambang">រៀនថ្នាក់ទីៈ 
					<?php
						echo "<b>".(isset($row->class_name) ? $row->class_name : '')."</b>";
					?>
				</span>
			</div>

			<div class="col-md-4" style="text-align:left;​width:212px !important; float: right;border: 0px solid #f00;">
				<span class="class_format_fontBattambang">ឆ្នាំសិក្សាៈ<b>
					<?php
						echo $showyear;
					?>
				</b></span>
			</div>
		</div>
			<div class="col-md-12 class_format_fontBattambang" style="text-align:center;">ពិន្ទុ និងការវាយតម្លៃមុខវិជ្ជានីមួយៗ / Score and Evaluation of Each Subject</div>
				
		<!-- ***********************************end header report************************************** -->

		<!-- ***************************************content report************************************* -->
		<!--table 1 table table-bordered-->
		<div style="margin: 0 auto; width: 670px; border:0px solid #fff;overflow: hidden;">
			<div  style="width: 328px;margin-left: 1px; float: left;border: 0px solid #fff;">
				<div class="span8 class_format_fontBattambang">ផ្នែកៈ ចំណេះទូទៅភាសាខ្មែរ</div>
				<div class="table-responsive">							
				<table border="1"​ align="center" id='listsubject' class="">
					<?php
						echo $tr;
					?>
				    <tbody class="tbodys">
				    	
				    </tbody>
				</table>
			</div>	 							
			</div><!--end table 1-->

			<!--table 2-->
			<div style="margin-bottom:-10px;margin-left: 10px;width: 328px; float: left;border: 0px solid #fff;">
				<div class="span8 class_format_fontBattambang">ផ្នែកៈ ភាសាអង់គ្លេស ចិន កុំព្យូទ័រ និង ជំនាញផ្សេងៗ</div>
				<div class="table-responsive">							
				<table border="1"​ align="center" id='listsubject' class="">

					<?php
						echo $trr;
					?>
				   
				    <tbody class="tbodys">
				    	
				    </tbody>
				</table>
			</div>	 		
			</div><!--end table 2-->
		</div>
		<!--total score and average-->
		<div class="col-sm-12"  style="margin: 0 auto; width: 670px; border:0px solid #f00;overflow: hidden;">
			<div class="col-sm-4" style="margin-left: 1px;margin-top: 10px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">ពិន្ទុសរុប​ / Total:  
				<?php 
						echo "<b>".(isset($gtotal) ? $gtotal : '0.00')."</b>";
				?>
				</span>
			</div>

			<div class="col-sm-4" style="margin-top: 10px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">មធ្យមភាគប្រលងឆមាស:
					<?php
						echo "<b>".(isset($rows->average_score) ? $rows->average_score : '0')."</b>";
					?>
				</span>
			</div>
			<div class="col-sm-4" style="margin-top: 10px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">មធ្យមភាគប្រចាំឆមាស:
					<?php
						echo "<b>".(isset($rows->average_score) ? $rows->average_score : '0')."</b>";
					?>
				</span>
			</div>
		</div>
		<div class="col-sm-12" style="margin: 0 auto; width: 670px; border:0px solid #f00;overflow: hidden;">
			
			<div class="col-sm-4" style="margin-top: 0px;width: 230px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">មធ្យមភាគខែប្រចាំឆមាស:
					<?php
						echo "<b>".(isset($rows->average_score) ? $rows->average_score : '0')."</b>";
					?>
				</span>
			</div>
			<div class="col-sm-4" style="margin-top: 0px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">ចំណាត់ថ្នាក់ / Rank: 
					<?php
						echo "<b>".(isset($rows->ranking_bysubj) ? $rows->ranking_bysubj : '')."/".(isset($total_row) ? $total_row : '')."</b>";
					?>
				</span>
			</div>
			<div class="col-sm-4"  style="margin-top: 0px;width: 150px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">និទ្ទេស: 
					<?php
						echo "";
					?>
				</span>
			</div>
		</div>
		<div class="col-sm-12" style="margin: 0 auto; width: 670px; border:0px solid #f00;overflow: hidden;">
			<!--space-->
			<div class="col-sm-4" style="margin-top: 0px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">ចំនួនអវត្តមានសរុបៈ 
				<?php
					echo "<b>".(isset($rows->absent) ? $rows->absent : '0')."</b>";
				?>
				 ដង</span>
			</div>

			<div class="col-sm-4" style="margin-top: 0px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">គ្មានច្បាប់: 
					<?php
						echo "<b>0</b>";
					?>
				 ដង</span>
			</div>

			<div class="col-sm-4" style="margin-top: 0px;width: 212px !important; border: 0px solid #f00; float: left;">
				<span class="class_format_fontBattambang">មានច្បាប់ៈ 
					<?php
						echo "<b>".(isset($rows->present) ? $rows->present : '0')."</b>";
					?>
				 ដង</span>
			</div>
		</div>
	<!--end total score and average-->
	<div class="col-sm-12" style="margin: 0 auto; width: 670px; border:0px solid #f00;overflow: hidden;">
		<div class="col-sm-12 class_format_fontBattambang" style=" border: 1px solid #000;text-align: center;"><h4>ការវាយតម្លៃក្នុង</h4></div>
		
		<table border="1"​ align="center" style="width: 100%;">
			<thead>
				<tr>
					<th  class="class_format_fontBattambang">ទម្លាប់ក្នុងការសិក្សា និងជំនាញសង្គម<br>(Work Habits and Social Skills)</th>
					<th  class="class_format_fontBattambang">កម្រ<br>(Seldom)</th>
					<th  class="class_format_fontBattambang">ម្តងម្កាល<br>(Sometimes)</th>
					<th  class="class_format_fontBattambang">តែងតែ<br>(Usually)</th>
					<th  class="class_format_fontBattambang">ជាប្រចាំ<br>(Consistently)</th>
				</tr> 
			</thead>
			<tbody>
			<?php
				$teacher_comment = "";
				$guardian_comment = "";
				$academic_date   = "";
				$techer_date     = "";
				$guardian_date   = "";
				$return_date     = "";
				$managername     = "";
				$teachername     = "";
				$arr_eval = array();
				if($comment->num_rows() > 0){
					foreach($comment->result() as $row_com){
						$managername = $this->db->query("SELECT user_name FROM sch_user WHERE match_con_posid='acca' AND userid='".$row_com->academicid."'")->row()->user_name;
						$teachername = $this->db->query("SELECT user_name FROM sch_user WHERE 1=1 AND userid='".$row_com->userid."'")->row()->user_name;
						$teacher_comment = $row_com->techer_comment;
						$guardian_comment = $row_com->guardian_comment;
						$academic_date   = date('d/m/Y',strtotime($row_com->academic_date));
						$techer_date     = date('d/m/Y',strtotime($row_com->techer_date));
						$guardian_date   = date('d/m/Y',strtotime($row_com->guardian_date));
						$return_date     = date('d-m-Y',strtotime($row_com->return_date));
						$sql_show_eval   = $this->db->query("SELECT * FROM sch_evaluation_semester_kgp_detail WHERE 1=1 AND evaluationid='".$row_com->evaluationid."'");
						if($sql_show_eval->num_rows() > 0){
							foreach($sql_show_eval->result() as $row_s_ev){
								$arr_eval[$row_s_ev->descrition_id] = array("seldom"=>$row_s_ev->seldom,"sometimes"=>$row_s_ev->sometimes,"usually"=>$row_s_ev->usually,"consistently"=>$row_s_ev->consistently);
							}
						}
					}
				}
				//print_r($arr_eval);
				$sql_evaluation = $this->db->query("SELECT
														sch_evaluation_semester_kgp_description.description_id,
														sch_evaluation_semester_kgp_description.description
														FROM
														sch_evaluation_semester_kgp_description
														");
				$tr_evaluat = "";
				if($sql_evaluation->num_rows() > 0){
					foreach($sql_evaluation->result() as $row_evaluat){
						$seldom_check = "";
						$sometimes_check = "";
						$usually_check = "";
						$consistently_check = "";
						if(isset($arr_eval[$row_evaluat->description_id]["seldom"]) and $arr_eval[$row_evaluat->description_id]["seldom"]==1){
							$seldom_check = "checked='checked'";
						}else if(isset($arr_eval[$row_evaluat->description_id]["sometimes"]) and $arr_eval[$row_evaluat->description_id]["sometimes"]==1){
							$sometimes_check = "checked='checked'";
						}else if(isset($arr_eval[$row_evaluat->description_id]["usually"]) and $arr_eval[$row_evaluat->description_id]["usually"]==1){
							$usually_check = "checked='checked'";
						}else if(isset($arr_eval[$row_evaluat->description_id]["consistently"]) and $arr_eval[$row_evaluat->description_id]["consistently"]==1){
							$consistently_check = "checked='checked'";
						}
						$tr_evaluat.= "<tr>
											<td class='class_format_fontBattambang'>".$row_evaluat->description."</td>
											<td style='text-align:center;' class='class_format_fontBattambang'><input type='checkbox' ".$seldom_check."  onClick='return chdisable()' class='ch_eval' value='".$row_evaluat->description_id."' id='seldom'></td>
											<td style='text-align:center;' class='class_format_fontBattambang'><input type='checkbox' ".$sometimes_check." onClick='return chdisable()' class='ch_eval' value='".$row_evaluat->description_id."' id='sometime'></td>
											<td style='text-align:center;' class='class_format_fontBattambang'><input type='checkbox' ".$usually_check."  onClick='return chdisable()' class='ch_eval' value='".$row_evaluat->description_id."' id='usually'></td>
											<td style='text-align:center;' class='class_format_fontBattambang'><input type='checkbox' ".$consistently_check."  onClick='return chdisable()' class='ch_eval' value='".$row_evaluat->description_id."' id='consistenly'></td>
									</tr>";
					}
				}
				echo $tr_evaluat;
			?>
			</tbody>
		</table>
	</div>
	<!--teacher comment-->
	<div style="page-break-after: always;"></div>
	<div class="col-sm-12" style="margin-top:10px;">
		<div class="panel panel-default" style=" border: 1px solid #000">
			<div class="span8 class_format_fontBattambang" style="margin:5px 20px -3px 20px;">មតិយោបល់របស់គ្រូ / Teacher's Comments</div>
			<div class="span12 class_format_fontBattambang" style="margin:3px 20px 20px 20px;height:60px;">
				<?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$teacher_comment;?>
			</div>
		</div>	 							
		</div><!--end teacher comment-->
		<!-- *************************************end content report********************************** -->

		<!-- ***************************************footer report************************************* -->
		<div class="col-sm-12" style="margin: 0 auto; width: 670px; border:0px solid #f00;overflow: hidden;">
		<div class="col-sm-4"​ style="line-height: 22px; margin-top:60px;text-align: center;width: 212px !important; float: left;border:0px solid #f00;">
			<span  class="class_format_fontBattambang">បានឃើញ និង ឯកភាព</span><br>
			<span  class="class_format_fontBattambang">ថ្ងៃទី&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $academic_date;?></span><br>
			<span class="texts​ class_format_fontMuolLight">នាយក</span><br>
			<div class="span8" style="margin-bottom: 70px"></div>
			<span><strong class="class_format_fontBattambang">ឈ្មោះ&nbsp;&nbsp;<?php echo ucfirst($managername);?></strong></span>
		</div>
		<div class="col-sm-4"​ style="text-align: center; line-height: 22px; margin:25px 0 40px 0;width: 212px !important; float: left;border:0px solid #f00;">
			<span class="class_format_fontBattambang">បានឃើញ និង ពិនិត្យត្រឹមត្រូវ</span><br>
			<span class="class_format_fontBattambang">ថ្ងៃទី&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $academic_date;?></span><br>
			<span class="texts​ class_format_fontMuolLight">ប្រធានការិយាល័យសិក្សា</span><br>
			<div class="span8" style="margin-bottom: 70px"></div>
			<span style="margin-left:100px;​"><strong class="class_format_fontBattambang">ឈ្មោះ&nbsp;&nbsp;<?php echo ucfirst($managername);?></strong></span>
		</div>

		<div class="col-sm-4"​ style="text-align: center; line-height: 22px;width: 212px !important; float: left;border:0px solid #f00;">
			<span class="class_format_fontBattambang">ថ្ងៃទី&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $techer_date;?></span><br>
			<span class="texts​ class_format_fontMuolLight">គ្រូប្រចាំថ្នាក់</span>
			<div class="span8" style="margin-bottom: 70px"></div>
			<span><strong class="class_format_fontBattambang">ឈ្មោះ&nbsp;<?php echo ucfirst($teachername);?></strong></span>
		</div> 
	</div>
		<!--parents comment-->
		<div class="col-sm-12">
			<div class="panel panel-default" style=" border: 1px solid #000">
				<div class="span8 class_format_fontBattambang" style="margin:5px 20px -3px 20px;">មតិមាតាបិតាសិស្ស / Guardian's Comments</div>
				<div class="span12 class_format_fontBattambang" style="margin:3px 20px 20px 20px; height:60px;">
					<?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$guardian_comment;?>
				</div>
			</div>	 							
		</div><!--end parents comment-->
		<div class="col-sm-12" style="margin: 0 auto; width: 670px; border:1px solid #fff;overflow: hidden;">
		<div class="col-sm-7"​ style="border:1px solid #fff;width: 335px !important; float: left;">
			<span><strong class="class_format_fontBattambang">សូមប្រគល់សៀវភៅតាមដាននេះមកសាលាអោយបានមុនថ្ងៃទី៖</strong></span><br>
			<span>Please Return the book to school before:</span><br>
			<span><input type="text" class="text-comment" value="<?php echo $return_date; ?>" style="width:255px;text-align: center;" /></span>
		</div>

		<div class="col-sm-5"​ style="border:1px solid #fff;width: 275px !important; float: left;">
			<span style="margin-right:40px;" class="class_format_fontBattambang">ថ្ងៃទី &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $guardian_date;?></span><br>
			<span><strong class="class_format_fontBattambang">ហត្ថាលេខា និង ឈ្មោះមាតាបិតា រឺ អាណាព្យាបាល</strong></span>		 						
		</div> 
		</div>		
		<!-- **************************************end footer report********************************** -->
	</div>
</div>

<style>
   /*--table--*/
   .texts{
   }
   .titles{
	   	font-size:13pt;
	   	text-align: center;
	   	line-height: 25px;   	
   }
   .text-comment{
   		border-top: hidden;
   		border-right: hidden;
   		border-left: hidden;
   		border-bottom: 1px #ccc dotted;
   		width: 900px;
   }
   .col-sm-6, .col-sm-4{
   		line-height: 22px;
   }
   th{
	   	text-align: center;
	   	vertical-align: middle !important;
   }

</style>

<script type="text/javascript">
	$(function(){
		$('body').delegate('.text-comment', 'keyup', function() {
			var comment = $(this).val();
			$(this).attr('value', comment);
		});
		//alert("thank");
		
	});
	function chdisable() {
		return false;
	}
</script>