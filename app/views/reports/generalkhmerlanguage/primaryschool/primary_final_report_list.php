<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
			  	<button type="button" class="close" data-dismiss="modal">&times;</button>
			  	<h4 class="modal-title">Add comment student final</h4>
			</div>
			
				<?php 
        			$sql_m = $this->db->query("SELECT
												sch_user.user_name,
												sch_user.userid,
												sch_user.match_con_posid
												FROM
												sch_user
												WHERE match_con_posid='acca'");
        			$option_man = "<option value=''></option>";
        			if($sql_m->num_rows() > 0){
        				foreach($sql_m->result() as $row_man){
        					$option_man.= "<option value='".$row_man->userid."'>".$row_man->user_name."</option>";
        				}
        			}
        			
        		?>
			
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12" style="text-align: center;"><b>ឈ្មោះសិស្ស :</b>&nbsp;<span id="show_student_name" style="font-size: 18px;"></span></div>
				</div>
                <div class="row">
                	<div class="col-sm-12">
                		<input type="hidden" name="hstudentid" id="hstudentid">
                		<div class="form-group">
				           	<label for="inputEmail">មតិយោបល់របស់គ្រូ / Teacher's Comments</label>
				           	<textarea rows="5" class="form-control" id="comment_teacher"></textarea>
				        </div>
                	</div>
                	<div class="col-sm-12">
                		<div class="col-sm-4">
                			<div style="display: inline;">ថ្ងៃទី :<input type="text" name="date_director" id="date_director" value="<?php echo date('d-m-Y');?>" style="border:none;text-align: center;border-bottom: 1px dotted black;"></div>
                			<p style="text-align: center;"><b>នាយក</b></p>
                		</div>
                		<div class="col-sm-4">
                			<div style="display: inline;">ថ្ងៃទី :<input type="text" name="date_approve" id="date_approve" value="<?php echo date('d-m-Y');?>" style="border:none;text-align: center;border-bottom: 1px dotted black;"></div>
                			<p style="text-align: center;"><b>ប្រធានការិយាល័យ</b></p>
                		</div>
                		<div class="col-sm-4">
                			<div style="display: inline;">ថ្ងៃទី :<input type="text" name="date_teacher" id="date_teacher" value="<?php echo date('d-m-Y');?>" style="border:none;text-align: center;border-bottom: 1px dotted black;"></div>
                			<p style="text-align: center;"><b>គ្រូប្រចាំថ្នាក់</b></p>
                		</div>
                	</div>
                	<div class="col-sm-12">
                		<div class="col-sm-4">
                			<select class="form-control" id="director"><?php echo $option_man;?></select>
                		</div>
                		<div class="col-sm-4">
                				<select class="form-control" id="managername">
		                		<?php echo $option_man;?>
		                		</select>
                		</div>
                		<div class="col-sm-4">
		                		<select class="form-control" id="teachername">
		                			<?php 
		                				$userid = $this->session->userdata('userid');
			                			$sql_teacher = $this->db->query("SELECT
																	sch_user.user_name,
																	sch_user.userid,
																	sch_user.match_con_posid
																	FROM
																	sch_user
																	WHERE userid='".$userid."'");
			                			$option_teacher = "";
			                			if($sql_teacher->num_rows() > 0){
			                				foreach($sql_teacher->result() as $row_t){
			                					$option_teacher.= "<option value='".$row_t->userid."'>".$row_t->user_name."</option>";
			                				}
			                			}
			                			echo $option_teacher;
			                		?>
		                		</select>
                		</div>
                	</div>
                	
                	
                </div>
            </div>
              <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save_comment">Save</button>
            </div>
		</div>
	</div>
</div>

<div class="panel panel-default" style="margin-top:5px;">
	<div class="table-responsive">							
		<table border="1"​ align="center" id='listsubject' class="col-sm-12">
		    <thead>
		    	<tr>								        
			        <th width="50">#</th>
			        <th>Student Name</th>
			        <th>School lavel</th>
			        <th>Year</th>
			        <th>Class</th>
			        <th>Total Score</th>
			        <th><a href="javascript:void(0)" id="print_all" title="Print all"><img src="<?php echo base_url("assets/images/icons/print.png"); ?>"></a></th>
			    </tr>
		    </thead>
			<tbody class="tbodys">
				<?php 
					//print_r($score_m);
					$tr = "";
					if($result->num_rows() >0){
						$i = 1;
						foreach($result->result() as $row_r){
							//$score_show = isset($score_m[$row_r->studentid])?$score_m[$row_r->studentid]:0;
							$total_score = 0;
							$kk = 0;
							$url_semester = "";
							if(count($arr_semester) > 0){
								foreach($arr_semester as $ksemter){
									if(isset($total_score_s[$ksemter][$row_r->studentid])){
										$total_score+=$total_score_s[$ksemter][$row_r->studentid];
									}
									$url_semester.= "&semester".$kk."=".$ksemter;
									
									$kk++;
								}
							}
							if($kk == 0){
								$kk=1;
							}
							$result_total = round(($total_score/$kk),2);
							$tr.="<tr>
									<td>".($i++)."</td>
									<td><span id='student_name'>".$row_r->studentname."</span></td>
									<td>".$row_r->sch_level."</td>
									<td>".$row_r->sch_year."</td>
									<td>".$row_r->class_name."</td>
									<td>".$result_total."</td>
									<td>
										<a href='javascript:void(0)' id='add_comm' studentid_a='".$row_r->studentid."'><img src='".base_url('assets/images/icons/add.png')."' style='margin-right:0;'></a>
										&nbsp;
										<a href='".site_url("reports/generalkhmerlanguage/primary_final_report_individual_ranking")."?classid=".$row_r->classid."&yearid=".$row_r->year."&schoolid=".$row_r->schoolid."&schlavelid=".$row_r->schlevelid."&grandlavelid=".$row_r->grade_levelid."&programid=".$row_r->programid."&studentid=".$row_r->studentid.$url_semester."' target='_blank' id='add_print' studentid_v='".$row_r->studentid."'><img src='".base_url('assets/images/icons/a_preview.png')."' style='margin-right:0;'></a>
									</a>
									</td>
								</tr>";
						}
					}
					echo $tr;		
				?>
			</tbody>
		</table>
	</div>
</div>

<style>
   /*--table--*/
   .titles{
	   	font-family: 'Kh Muol Pali';
	   	font-size:14pt;
	   	text-align: center;
	   	line-height: 32px;   	
   }
   .border{
   	border: 1px;
   }
   .sub-titles{
	   	font-family: 'Kh Bokor'!important;
	   	font-size:12pt;
		line-height: 22px;  
   }
   th, td{
   		text-align: center;
   		height: 27px;
   		vertical-align: middle !important;
   }
</style>
