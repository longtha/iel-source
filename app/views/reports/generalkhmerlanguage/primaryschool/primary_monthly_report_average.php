<?php 
$sql_st = isset($student_inf)?$student_inf:"";
$tr = "";
$i=1;
$class_name = "";
if($sql_st->num_rows() > 0){
	foreach($sql_st->result() as $row_st){
		$class_name = $row_st->class_name;
		$td_avg = "";
		$total_average=0;
		if(count($month) > 0){
			foreach($month as $k_month){
				if(isset($average[$row_st->studentid][$k_month])){
					$total_average+=($average[$row_st->studentid][$k_month]-0);
					$td_avg.="<td class='class_format_fontBattambang'>".$average[$row_st->studentid][$k_month]."</td>";
				}else{
					$td_avg.="<td class='class_format_fontBattambang'>0.00</td>";
					$total_average='0.00';
				}
				
			}
		}
		$tr.="<tr>
					<td class='class_format_fontBattambang'>".($i++)."</td>
					<td style='text-align:left;' class='class_format_fontBattambang'>".$row_st->studentname."</td>
					<td class='class_format_fontBattambang'>".($row_st->gender=='female'?'ស':'ប')."</td>
					".$td_avg."
					<td class='class_format_fontBattambang'>".(intval($total_average)>0?((($total_average/($total_all_month-1)))):$total_average)."</td>
			</tr>";
	}
}else{
	$tr.="<tr><td colspan='3'><strong>Data no have</strong></td></tr>";
}

?>
<div class="print_show_result">
<div class="row" style="margin:10px;margin: 0 auto; width: 655px;">
	<div class="col-sm-12">
		<div class="col-sm-6">
			<div style="font-size: 14px;" class="class_format_fontBokor">មន្ទីអប់រំ យុវជននិងកីឡារាជធានីភ្នំពេញ</div>
			<div style="font-size: 14px;" class="class_format_fontBokor">សាសាអន្តរជាតិអាយ អ៊ី អ៊ែល</div>
			<div style="font-size: 14px; margin-left: 15px" class="class_format_fontBokor">ថ្នាក់ទី <?php echo $class_name;?></div>
		</div>
		<div class="col-sm-6" style="text-align: right;">
			<div style="font-size: 14px;margin-right: 20px" class="class_format_fontBokor">ព្រះរាជាណាចក្រកម្ពុជា</div>
			<div style="font-size: 14px;" class="class_format_fontBokor">ជាតិ សាសនា ព្រះមហាក្សត្រ</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4"></div>
		<div class="col-sm-4" style="text-align: center;">
			<p style="font-size: 14px;" class="class_format_fontBokor">មធ្យមភាគប្រចាំខែ&nbsp;<?php echo $title_m;?></p>
			<p style="font-size: 14px;" class="class_format_fontBokor">ឆ្នាំសិក្សា&nbsp;&nbsp;&nbsp;<?php echo $year;?></p>
		</div>
		<div class="col-sm-4">
			<input type="hidden" class="header_title" value="មធ្យមភាគប្រចាំខែ&nbsp;<?php echo $title_m;?>">
		</div>
	</div>
</div>
<div class="panel panel-default" style="margin-top:5px;border: 0px solid #f00;margin-left: 5px; margin-right: 15px;margin: 0 auto; width: 655px;">
	<div class="table-responsive">							
		<table border="1"​ id='listsubject' align="center">
		    <thead>
		    	<tr>								        
			        <th class="class_format_fontBattambang">ល.រ</th>
			        <th class="class_format_fontBattambang">ឈ្មោះសិស្ស</th>
			        <th class="class_format_fontBattambang">ភេទ</th>
			       	<?php echo $th_month;?>
			       	<th class="class_format_fontBattambang">សរុប</th>
			    </tr>
		    </thead>
			<tbody class="tbodys">
			<?php echo $tr;?>
			</tbody>
		</table>
	</div>
</div>
<div style="margin: 0 auto; width: 670px; border:1px solid #fff;overflow: hidden;">
	<div class="col-sm-5" style="text-align: center; width: 265px;float: left; border: 1px solid #fff;">
		<p style="font-size: 12px" class="class_format_fontBattambang">បានឃើញ និង ឯកភាព</p>
		<p style="font-size: 12px" class="class_format_fontBattambang">ភ្នំពេញថ្ងៃទី..............ខែ.............ឆ្នាំ...................</p>
		<p style="font-size: 12px" class="class_format_fontMuolLight"><strong>នាយកសាលា</strong></p>
	</div>	
	<div class="col-sm-2" style="width: 120px;float: left; border: 1px solid #fff;"></div>
	<div class="col-sm-5" style="text-align: center; width:265px; float: left; border: 1px solid #fff;">
		<p style="font-size: 12px" class="class_format_fontBattambang">ភ្នំពេញថ្ងៃទី..............ខែ.............ឆ្នាំ...................</p>
		<p style="font-size: 12px" class="class_format_fontMuolLight"><strong>គ្រូបន្ទុកថ្នាក់</strong></p>
	</div>	
</div>
<style>
   /*--table--*/
   .titles{
	   	font-size:14pt;
	   	text-align: center;
	   	line-height: 32px;   	
   }
   .border{
   	border: 1px;
   }
   .sub-titles{
	   	font-size:12pt;
		line-height: 22px;  
   }
   th, td{
   		text-align: center;
   		height: 27px;
   		vertical-align: middle !important;
   }
</style>
</div>