<div class="result_info">
	 <div class="col-xs-6">
              <strong id='title'>
              		<i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;Report student
			</strong>
            </div> 
	<div class="col-sm-6" style="text-align:right; line-height:35px;">
		<a href="javascript:void(0)" id="export" title="Export">
			<img src="<?php echo base_url('assets/images/icons/import.png');?>">
		</a>
		<a href="javascript:void(0)" id="print_report_m" title="Print">
			<img src="<?php echo base_url('assets/images/icons/print.png');?>">
		</a>

	</div>
</div>
<div id="tbl_report">