 
 <div class="container-fluid" style="width:100% !important;padding-bottom: 20px;">
    <div class="row">
      <div class="col-xs-12">
          <div class="result_info">
              <div class="col-xs-6">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                  <strong>Score List Report</strong>  
              </div>
              <div class="col-xs-6" style="text-align: right">                  
                  <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                     <img src="<?= base_url('assets/images/icons/print.png') ?>">
                  </a>
                  <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                     <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
                  </a>
              </div>         
          </div>
      </div>
    </div>
<div id="dv_print">
<style type="text/css">
 	.h4, .h5, .h6, h4, h5, h6{
 		margin-top:-9px !important;
 	}
 </style>
<?php 
	$studentid = isset($_GET['id']) ? $_GET['id'] : '';
    $programid = isset($_GET['p_id']) ? $_GET['p_id'] : '';
    if(isset($_GET['main'])){
    $main = $this->input->get('main');
    $mains = explode('_', $main);
    $schoolid = $mains[0];
    $schlevelid = $mains[1];
    $yearid = $mains[2];
    $gradelevelid = $mains[3];
    $semesterid = $mains[4];
    $classid=$mains[5];    

    $where = "";
		$where .= "st.studentid = '".$studentid."' ";
		$where .= "AND st.schoolid = '".$schoolid."' ";
		$where .= "AND st.programid = '".$programid."' ";						
		$where .= "AND st.schlevelid = '".$schlevelid."' ";
		$where .= "AND st.yearid = '".$yearid."' ";
		$where .= "AND st.grade_levelid = '".$gradelevelid."' ";
		$where .= "AND st.classid = '".$classid."' ";			
		$row_stu = $this->db->query("SELECT * FROM v_student_profile AS st 
										INNER JOIN sch_school_level 
										ON st.schlevelid=sch_school_level.schlevelid
										LEFT JOIN sch_school_semester as sem
										ON st.semesterid=sem.semesterid
										WHERE $where ")->row();
  }
 ?>
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
	   <div class="col-sm-12"> 
		   	<div id="tap_print">
			<div class="col-sm-12 ti_semester">
		   		<div class="col-sm-4" style="width:200px !important">
		   			<img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="width:200px !important">
		   		</div>
		   		<div class="col-sm-6 dv_semester">
		   			<h4 class="kh_font" style="margin-top:3px !important">របាយការណ៍សិក្សាប្រចាំឆមាស</h4>
		   			<h4 style="font-size:23px;">Semester Acadimic Report</h4>		   			
		   			<h4 style="font-size:23px;"><?php echo $row_stu->semester;?></h4>
		   		</div>  	
		   		<input type="hidden" id="studentid" value="<?php echo $studentid?>">
		   		<input type="hidden" id="programid" value="<?php echo $programid?>">
		   		<input type="hidden" id="schoolid" value="<?php echo $schoolid?>">
		   		<input type="hidden" id="slevelid" value="<?php echo $schlevelid?>">
		   		<input type="hidden" id="yearid" value="<?php echo $yearid?>">
		   		<input type="hidden" id="gradelevelid" value="<?php echo $gradelevelid?>">
		   		<input type="hidden" id="classid" value="<?php echo $classid?>">
		   		<input type="hidden" id="semesterid" value="<?php echo $semesterid?>">		   		
		   	</div>	 	
			<div class="col-sm-12" style="padding-left:40px; padding-right:40px;">			
		                <table class='table head_name' style=" margin-bottom: 0px !important;">
		                   	
		                   	<tbody>
		                   		<tr>
		                   			<td style="width:15%;">Student's Name:</td>
		                   			<td style="width:20%"><?php echo $row_stu->fullname;?></td>
		                   			<td style="width:5%">Level:</td>
		                   			<td style="width:10%"><?php echo $row_stu->sch_level;?></td>
		                   			<td style="width:10%">Yearly:</td>
		                   			<td style="width:20%"><?php echo $row_stu->sch_year;?></td>
		                   		</tr>
		                   		<tr>
		                   			<td>សិស្សឈ្មោះ</td>
		                   			<td class="kh_font"><?php echo $row_stu->fullname_kh ?></td>
		                   			<td>កម្រិត:</td>
		                   			<td><?php echo $row_stu->sch_level; ?></td>
		                   			<td>ឆ្នាំសិក្សា:</td>
		                   			<td><?php echo $row_stu->sch_year;?></td>
		                   		</tr>
		                   	</tbody>
		                </table>
			</div>	
			<div class="col-sm-12" style="text-align:center; padding-bottom:15px">
				<p style="font-size:13px;">Score and Evaluation of Each Subject / ពិន្ទុ និងការវាយតម្លៃមុខវិជ្ជានីមួយៗ</p>
			</div>
			<div class="col-sm-12">				
				<div class="col-sm-6" style="padding-left:0px;">
							<label>Assessment and English Exam</label>
		                   <table class='table table-bordered'>
		                   	 	<thead>
		                   	 		<tr>
		                   	 			<th id="head">Other Assessment</th>
		                   	 			<th>Score​ <br> ពិន្ទុ</th>
		                   	 			<th>Evaluation <br> ការវាយតម្លៃ</th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody>
		                   	 		<tr>
		                   	 			<td>Class-Participation / សកម្មភាពសិក្សា</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Home Work / កិច្ចការផ្ទះ</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Portfolio Diligence / រៀបចំស៊ីម៉ី</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Vocabulary / វាក្យសព្ទ</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Grammar / វេយ្យាករណ៍</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Listening / ការស្ដាប់</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Speacking / កានិយាយ</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Reading / ការអាន</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Writing / ការសរសេរ</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>		                   	 		
		                   	 	</tbody>
		                   </table>
				</div>
				<div class="col-sm-6" style="padding-right: 0px;">					
					<div class="form-group" style="margin-bottom:60px;">
						   <label>Maths-Science  Exam</label>
		                   <table class='table table-bordered'>
		                   	 	<thead>
		                   	 		<tr>
		                   	 			<th id="head">Subjects / មុវិជ្ជា</th>
		                   	 			<th id="scor">Score​ <br> ពិន្ទុ</th>
		                   	 			<th id="scor">Evaluation <br> ការវាយតម្លៃ</th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody>
		                   	 		<tr>
		                   	 			<td>Mathematics / គណិតវិទ្យា</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Science / វិទ្យាសាស្ត្រ</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 	</tbody>
		                   </table>
		            </div>
		            <div class="form-group">		            	   
		                   <table class='table table-bordered'>
		                   	 	<tbody>
		                   	 		<tr>
		                   	 			<td >Assessment & English Test:</td>
		                   	 			<td style="width:50%" id="scor">100</td>		                   	 			
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<tr>
		                   	 			<td>Maths-Science Test:</td>
		                   	 			<td id="scor">100</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td style="background: #808080 !important;">Total Mid-term(ពិន្ទុសរុប)</td>
		                   	 			<td id="scor" style="background: #808080 !important;">100</td>
		                   	 		</tr>		                   	 		
		                   	 		</tr>
		                   	 	</tbody>
		                   </table>
		            </div>
				</div>
				<div class="form-group" style="padding-bottom:40px;">
					<table class='table table-bordered'>
						<thead>
							<tr>
								<th class="kh_font" colspan="5">Evaluation for Semestern 1 /  ការវាយតម្លៃក្នុងឆមាសទី១</th>
							</tr>
							<tr>
								<th>Work Habits and Social Skills <br> ទម្លាប់ក្នុងការសិក្សា និងជំនាញសង្គម</th>
								<th>Seldom <br> កម្រ</th>
								<th> Sometimes <br> ម្ដងម្កាល</th>
								<th> Usually <br> តែងតែ </th>
								<th> Consistently <br> ជាប្រចាំ </th>
							</tr>
						</thead>
						<tbody id="list_description">
																			
						</tbody>
					</table>
				</div>
				<div class="form-group">
					 <textarea rows="10" class="form-control" readonly id="t_comment" placeholder="Teacher's Comments / មតិយោបល់របស់គ្រូ" style="background-color: transparent !important;"></textarea>					 	
				</div>
				<div class="col-sm-12"​​ style="margin-bottom: 60px; text-align:center;">
					<div class="col-sm-6" style="margin-top:20px; ">
						<div class="form-inline">	
							<label>Date:</label>				
			                <input type="text" id="academic" readonly style="border: none; font-size:14px">                                               
			            </div>
						<label style="margin-right: 100px;">Academic Manager</label>
					</div>
					<div class="col-sm-6">
						<div class="form-inline">	
							<label>Date:</label>				
			                <input type="text" id="teacher" readonly style="border: none; font-size:14px">                                               
			            </div>						
						<label style="margin-right: 88px;">Teacher's Signature</label>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:3px !important">
					<textarea rows="8" class="form-control" readonly id="g_comment" placeholder="Guardian's Comments / មតិមាតាបិតាសិស្ស" style="background-color: transparent !important;"></textarea>	
				</div>
				<div class="col-sm-12" style="padding-left:0px; ">
					<div class="col-sm-6" style="padding-left: 0px;">
						<p>Please return the book to school before</p>
						<p>សូមប្រគល់សៀវភៅតាមដាននេះមកសាលាមុនថ្ងៃទី៖</p>					
						<div class="col-sm-6" style="padding-left:0px;">
							<input type="text" id="return_date" class="form-control" readonly style="font-size:14px;background-color: transparent !important;"> 
							<!-- <input type="text" value="10-09-2015" class="form-control" style="text-align:center; width:100%; font-size: 11px; "> -->
						</div>
					</div>
					<div class="col-sm-6" style="text-align:center;padding:0px;">
						<div class="form-inline">	
							<label>Date:</label>				
			                <input type="text" id="guardian" readonly style="border: none; font-size:14px">                                            
			            </div>
						<p>Guardian's Signature / ហត្ថលេខាមាតាមិតាសិស្ស</p>
					</div>
				</div>				
			</div>		   		   		
	   	</div>	   	
	</div>
	<!-- <div class="col-sm-12" style="margin:25px !important;">
   		<div class="form-group">
   			<button class="btn btn-primary" id="save"><i class="glyphicon glyphicon-floppy-save"></i>Save</button>
   		</div>-->
   	</div> 
</div>
<script type="text/javascript">
$(function(){
	list_description();
	list_st($('#studentid').val());

    $("#a_print").on("click", function (e) {
       gPrint('dv_print');
	}); 

	$("#refresh").click(function(){
          location.reload();
      });

	$('#seldom[readonly],#sometimes[readonly],#usually[readonly],#consistently[readonly]').click(function(){
        return false;	           
    });
});

function list_st(sid){
	$.ajax({
      url: '<?php echo site_url('school/c_evaluation_semester_iep_list/show_student') ?>',
      datatype:'JSON',
      type:'POST',
      async:false,
      data:{
            'id': sid
      },
      success:function(data){
        $("#t_comment").val(data['techer_comment']);
        $("#g_comment").val(data['guardian_comment']);
        $('#teacher').val(data['techer_date']);
	    $('#academic').val(data['academic_date']);
	    $('#guardian').val(data['guardian_date']);
	    $('#return_date').val(data['return_date']);
      }
    });
}

function list_description() {
	var studentid = $("#studentid").val();
	var url = "<?php echo site_url('school/c_evaluation_semester_iep/list_description')?>";  
	$.ajax({
	    url: url,
	    type: "POST",
	    datatype: "JSON",
	    async: false,
	    data: {
	        'studentid':studentid
	    },
	    success: function (data) {
	        if(data.tr_data ==""){
	            var table = '<table class="table table-bordered table-striped table-hover">'+
	                            '<tr>'+
	                                '<td colspan="11" style="color:red;text-align:center;font-size:14px;"><i>Result Not Data</i></td>'+
	                            '</tr>'+
	                            '<tr><td colspan="11"></td></tr>'+
	                        '</table>';
	            $('#list_description').html(table);

	        }else{
	            $("#list_description").html(data.tr_data);
	        }
	    }
	});
}
</script>