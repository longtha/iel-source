
    <div class="row">
      <div class="col-xs-12">
          <div class="result_info">
              <div class="col-xs-6">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                  <strong>Final-Term Report</strong>  
              </div>
              <div class="col-xs-6" style="text-align: right">
                  <a href="javascript:void(0);" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                     <img src="<?= base_url('assets/images/icons/print.png') ?>">
                  </a>
              </div>         
          </div>
      </div>
    </div>
	<?php 
		$studentid  = isset($_GET['id']) ? $_GET['id'] : '';	
		$programeid = isset($_GET['p_id']) ? $_GET['p_id'] : '';	
		$classid    = isset($_GET['classid']) ? $_GET['classid'] : '';	
		$termid     = isset($_GET['termid']) ? $_GET['termid'] : '';
		$schlavelid = isset($_GET['schlid']) ? $_GET['schlid'] : '';
		$schoolid   = isset($_GET['schoolid']) ? $_GET['schoolid'] : '';
		$grandlavelid = isset($_GET['grandid']) ? $_GET['grandid'] : '';
		$yearid = isset($_GET['yearid']) ? $_GET['yearid'] : '';
		$examtype = isset($_GET['examtype']) ? $_GET['examtype'] : '';
		
		$row_stu = $this->db->query("SELECT
											vse.student_num,
											vse.first_name,
											vse.last_name,
											vse.first_name_kh,
											vse.last_name_kh,
											vse.gender,
											vse.class_name,
											vse.studentid,
											vse.schoolid,
											vse.`year`,
											vse.classid,
											vse.schlevelid,
											vse.rangelevelid,
											vse.feetypeid,
											vse.programid,
											vse.sch_level,
											vse.rangelevelname,
											vse.program,
											vse.sch_year,
											vse.grade_levelid,
											tt.term,
											tt.termid,
											gl.grade_level,
											YEAR(vse.from_date) AS fyear,
											YEAR(vse.to_date) AS tyear
											FROM
											v_student_enroment AS vse
											INNER JOIN sch_school_term AS tt ON tt.yearid = vse.`year`
											INNER JOIN sch_grade_level AS gl ON gl.grade_levelid = vse.`grade_levelid`
											WHERE 1=1
											AND vse.schoolid='".$schoolid."'
											AND vse.programid = '".$programeid."'
											AND vse.classid = '".$classid."' 
											AND tt.termid = '".$termid."'
											AND vse.studentid ='".$studentid."'
											AND vse.schlevelid ='".$schlavelid."'
											AND vse.grade_levelid ='".$grandlavelid."'
											AND tt.yearid ='".$yearid."'
											GROUP BY vse.studentid")->row();	
	?>  
<div style="border:0px solid #f00;width:97%;float: right;" id="tap_print">
		   <div> 
			   <div style="padding-top:0px;" id="titles">
			   		<div style="width:200px !important">
			   			<img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="width:200px !important">
			   		</div>
			   		<div class="header">
			   			<h4 class="kh_font">របាយការណ៏សិក្សាបញ្ចប់ត្រីមាស</h4>
			   			<h4 class="kh_font">ចំណេះទូទៅភាសាអង់គ្លេស</h4>			   			
			   			<h4 style="font-size:20px;">STUDENT'S IEP FINAL TERM REPORT</h4>
			   			<!-- <h4 style="font-size:18px;"><?php echo isset($row_stu->term) ? $row_stu->term:"";?></h4> -->	
			   		</div>  			   		
			   	</div>	 	
				<div style="">			
			                <table class='head_name' style=" margin-bottom: 0px !important;">
			                   	<tbody>
			                   		<tr>
			                   			<td style="width:15%;">First name&nbsp;:</td>
			                   			<td style="width:30%;font-size: 15px !important;font-weight: bold !important;"><b><?php echo $row_stu->first_name;?></b></td>
			                   			<td style="width:10%;"></td>
			                   			<td style="width:15%;">Last name&nbsp;:</td>
			                   			<td style="width:30%;font-size: 15px !important;font-weight: bold !important;"><b><?php echo $row_stu->last_name;?></b></td>
			                   		</tr>
			                   		<tr>
		                   				<td>Student's ID&nbsp;:</td>
		                   				<td><?php echo $row_stu->student_num;?></td>
		                   				<td></td>
		                   				<td>Sex&nbsp;:</td>
		                   				<td><?php echo ucwords($row_stu->gender);?></td>
			                   		</tr>
			                   		<tr>
		                   				<td>Grade&nbsp;:</td>
		                   				<td><?php echo $row_stu->class_name;?></td>
		                   				<td></td>
		                   				<td>Time&nbsp;:</td>
		                   				<td></td>
			                   		</tr>
			                   		
			                   	</tbody>
			                </table>
				</div>	
				<div style="text-align:center; padding-bottom:15px">
					<!-- <p style="font-size:13px;">Score and Evaluation of Each Subject / ពិន្ទុ និងការវាយតម្លៃមុខវិជ្ជានីមួយៗ</p> -->
				</div>
				<table style="width:100%;">				
					<tr style="padding-left:0px;">
						<td style="width:40%;">
			                   	<table border="1" style="width:100%;" cellpadding="0" cellspacing="0">
			                   	 	<?php
			                   	 			$sql_score = $this->db->query("SELECT
																				ord.type,
																				ord.typeno,
																				ord.main_tranno,
																				det.studentid,
																				si.`subject`,
																				det.subjectid,
																				si.subj_type_id,
																				det.score_num,
																				si.max_score,
																				det.group_subject,
																				ord.exam_typeid,
																				ord.weekid,
																				sch_subject_type_iep.is_class_participation
																				FROM
																				sch_score_iep_entryed_order AS ord
																				INNER JOIN sch_score_iep_entryed_detail AS det ON ord.typeno = det.typeno AND ord.type = det.type
																				INNER JOIN sch_subject_iep AS si ON det.subjectid = si.subjectid
																				INNER JOIN sch_subject_type_iep ON ord.group_subject = sch_subject_type_iep.subj_type_id
																				WHERE 1=1  
																				AND ord.termid='".$termid."' 
																				AND ord.classid ='".$classid."'
																				AND ord.schoolid = '".$schoolid."'
																				AND ord.schlevelid ='".$schlavelid."'
																				AND ord.gradelevelid = '".$grandlavelid."'
																				AND ord.yearid = '".$yearid."'
																				-- AND det.studentid='{$studentid}' 
																				-- AND ord.exam_typeid = '{$examtype}'
																				");

			                   	 			$arr_score     = array();
			                   	 			$arr_sub_exam  = array();
			                   	 			//$max_score_sub = array();
			                   	 			// ====================
			                   	 			$score_att_cp      = array();
			                   	 			$max_att        = array();
			                   	 			$max_score_att = array();

			                   	 			$max_att_h     = array();
			                   	 			if($sql_score->num_rows() > 0){
			                   	 				foreach($sql_score->result() as $row_sc){ 
							                   	 	if($row_sc->is_class_participation == 3 ){
							                   	 		if(isset($score_att_cp['att'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid])){
				                   	 						$score_att_cp['att'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $score_att_cp['att'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]+$row_sc->score_num;
				                   	 					}else{
				                   	 						$score_att_cp['att'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $row_sc->score_num;
				                   	 					}
				                   	 					// sum max score subject
				                   	 					if(isset($max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid])){
				                   	 						$max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]+$row_sc->max_score;
				                   	 					}else{
				                   	 						$max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $row_sc->max_score;
				                   	 					}
				                   	 					// compare max score subject
				                   	 					if(isset($max_score_att[$row_sc->exam_typeid][$row_sc->group_subject])){
				                   	 						if($max_score_att[$row_sc->exam_typeid][$row_sc->group_subject] < $max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]){
				                   	 							$max_score_att[$row_sc->exam_typeid][$row_sc->group_subject] = $max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid];
				                   	 						}
				                   	 					}else{
				                   	 						$max_score_att[$row_sc->exam_typeid][$row_sc->group_subject] = $row_sc->max_score;
				                   	 					}
				                   	 				}else if($row_sc->is_class_participation == 1 ){
				                   	 					if(isset($score_att_cp['cp'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid])){
				                   	 						$score_att_cp['cp'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $score_att_cp['cp'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]+$row_sc->score_num;
				                   	 					}else{
				                   	 						$score_att_cp['cp'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $row_sc->score_num;
				                   	 					}
				                   	 					// sum max score subject
				                   	 					if(isset($max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid])){
				                   	 						$max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]+$row_sc->max_score;
				                   	 					}else{
				                   	 						$max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $row_sc->max_score;
				                   	 					}
				                   	 					// compare max score subject
				                   	 					if(isset($max_score_att[$row_sc->exam_typeid][$row_sc->group_subject])){
				                   	 						if($max_score_att[$row_sc->exam_typeid][$row_sc->group_subject] < $max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]){
				                   	 							$max_score_att[$row_sc->exam_typeid][$row_sc->group_subject] = $max_att[$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid];
				                   	 						}
				                   	 					}else{
				                   	 						$max_score_att[$row_sc->exam_typeid][$row_sc->group_subject] = $row_sc->max_score;
				                   	 					}
							        
				                   	 				}
				                   	 				else if($row_sc->is_class_participation == 2 ) // Homework
				                   	 				{
				                   	 					if(isset($score_att_cp['hw'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid])){
				                   	 						$score_att_cp['hw'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $score_att_cp['hw'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]+$row_sc->score_num;
				                   	 					}else{
				                   	 						$score_att_cp['hw'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $row_sc->score_num;
				                   	 					}
				                   	 					// sum max score subject
				                   	 					if(isset($max_att_h['hw_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid])){
				                   	 						$max_att_h['hw_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $max_att_h['hw_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]+$row_sc->max_score;
				                   	 					}else{
				                   	 						$max_att_h['hw_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $row_sc->max_score;
				                   	 					}
				                   	 					// compare max score subject
				                   	 					
				                   	 					if(isset($max_att_h['comp'][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->weekid])){
				                   	 						if($max_att_h['comp'][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->weekid] < $max_att_h['hw_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]){
				                   	 							$max_att_h['comp'][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->weekid] = $max_att_h['hw_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid];
				                   	 						}
				                   	 					}else{
				                   	 						$max_att_h['comp'][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->weekid] = $row_sc->max_score;
				                   	 					}
				                   	 				}
				                   	 				else if($row_sc->is_class_participation == 4 ) // deligance
				                   	 				{
				                   	 					if(isset($score_att_cp['dl'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid])){
				                   	 						$score_att_cp['dl'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $score_att_cp['dl'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]+$row_sc->score_num;
				                   	 					}else{
				                   	 						$score_att_cp['dl'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $row_sc->score_num;
				                   	 					}
				                   	 					// sum max score subject
				                   	 					if(isset($max_att_h['dl_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid])){
				                   	 						$max_att_h['dl_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $max_att_h['dl_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]+$row_sc->max_score;
				                   	 					}else{
				                   	 						$max_att_h['dl_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $row_sc->max_score;
				                   	 					}
				                   	 					// compare max score subject
				                   	 					
				                   	 					if(isset($max_att_h['comp'][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->weekid])){
				                   	 						if($max_att_h['comp'][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->weekid] < $max_att_h['dl_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]){
				                   	 							$max_att_h['comp'][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->weekid] = $max_att_h['dl_ma_sc'][$row_sc->exam_typeid][$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid];
				                   	 						}
				                   	 					}else{
				                   	 						$max_att_h['comp'][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->weekid] = $row_sc->max_score;
				                   	 					}
				                   	 				}else if($row_sc->is_class_participation == 0){
				                   	 					if(isset($arr_sub_exam[$row_sc->studentid][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->subjectid])){
															$arr_sub_exam[$row_sc->studentid][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->subjectid] = $arr_sub_exam[$row_sc->studentid][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->subjectid];
				                   	 					}else{
				                   	 						$arr_sub_exam[$row_sc->studentid][$row_sc->exam_typeid][$row_sc->group_subject][$row_sc->subjectid] = $row_sc->score_num;
				                   	 					}
				                   	 					
				                   	 				} 
			                   	 				}
			                   	 			}
			         						//print_r($arr_sub_exam[$studentid][2]);
			                   	 			$sql_score_mention = $this->db->query("SELECT
																						sm.submenid,
																						sm.schoolid,
																						sm.schlevelid,
																						sm.subjectid,
																						sm.gradelevelid,
																						sm.mentionid,
																						sm.max_score,
																						sm.minscrore,
																						cm.mention
																				FROM
																				sch_subject_mention AS sm
																				LEFT JOIN sch_score_mention AS cm ON sm.mentionid = cm.menid
																				WHERE 1=1 
																				AND sm.schoolid='".$schoolid."'
																				AND sm.schlevelid='".$schlavelid."'
																				AND sm.gradelevelid='".$grandlavelid."'
																				");
			                   	 			$arr_subj_ment = array();
			                   	 			if($sql_score_mention->num_rows() > 0){
			                   	 				foreach($sql_score_mention->result() as $row_m){
			                   	 					$arr_subj_ment[$row_m->subjectid][] = array('Max_score'=>$row_m->max_score,'Min_score'=>$row_m->minscrore,'meaning'=>$row_m->mention);
			                   	 				}
			                   	 			}
			                  
			                   	 			$sql_group = $this->db->query("SELECT
																				sch_subject_type_iep.subj_type_id,
																				sch_subject_type_iep.subject_type,
																				sch_subject_type_iep.main_type,
																				sch_subject_type_iep.schlevelid,
																				sch_subject_type_iep.schoolid,
																				sch_subject_type_iep.is_class_participation,
																				sch_subject_type_iep.type_subject,
																				sch_subject_type_iep.is_group_calc_percent
																				FROM
																				sch_subject_type_iep
																				WHERE schoolid='".$schoolid."' AND schlevelid='".$schlavelid."'
																				ORDER BY sch_subject_type_iep.`subject_type`
																				");
			                   	 			$tr_att = "";
			                   	 			$tr_cp  = "";
			                   	 			$tr_hw  = "";
			                   	 			$tr_dl  = "";
			                   	 			$tr_sub = "";
			                   	 			$tr_mscien = "";
			                   	 			$sum_assement = 0;
			                   	 			$sum_subject  = 0;
			                   	 			$total_score_mid  = 0;
			                   	 			if($sql_group->num_rows() > 0)
			                   	 			{
			                   	 				
			                   	 				foreach($sql_group->result() as $row_group)
			                   	 				{
			                   	 					if($row_group->is_class_participation == 3) // attendance
			                   	 					{
			                   	 						
			                   	 						$max_scor_att = 1;
			                   	 						$max_score_group = $row_group->is_group_calc_percent;
			                   	 						$sum_att = 0;
			                   	 						for($k=1;$k<=2;$k++){
			                   	 							$score = 0;
															if(isset($score_att_cp['att'][$k][$studentid][$row_group->subj_type_id])){
				                   	 							foreach($score_att_cp['att'][$k][$studentid][$row_group->subj_type_id] as $v_att){
				                   	 								$score+=$v_att;
				                   	 							}
				                   	 						}
				                   	 						if(isset($max_score_att[$k][$row_group->subj_type_id])){
				                   	 							$max_scor_att = $max_score_att[$k][$row_group->subj_type_id];
				                   	 						}
				                   	 						$result_att = floor((($score*$max_score_group)/$max_scor_att)*100)/100;
				                   	 						$sum_att += $result_att;
			                   	 						}
			                   	 						$total_att = floor(($sum_att/2)*100)/100;			                   	 						
			                   	 						$mean = "";
			                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
	                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
	                   	 										if($subj_score['Min_score'] <= $total_att && $subj_score['Max_score'] >= $total_att){
	                   	 											$mean = $subj_score['meaning'];
	                   	 										}
	                   	 									}
	                   	 								}
	                   	 								$sum_assement+=$total_att;
														$tr_att.="<tr style='line-height: 10px !important;'><td>".$row_group->subject_type."</td><td id='scor'>".$total_att."</td><td style='text-align:center;'>".$mean."</td></tr>";
			                   	 						
			                   	 					}
			                   	 					else if($row_group->is_class_participation == 1)// class participation
			                   	 					{
			                   	 						$max_scor_cp = 1;
			                   	 						$max_score_group = $row_group->is_group_calc_percent;
			                   	 						$sum_cp = 0;
			                   	 						for($c=1;$c<=2;$c++){
			                   	 							$score = 0;
															if(isset($score_att_cp['cp'][$c][$studentid][$row_group->subj_type_id])){
				                   	 							foreach($score_att_cp['cp'][$c][$studentid][$row_group->subj_type_id] as $v_att){
				                   	 								$score+=$v_att;
				                   	 							}
				                   	 						}
				                   	 						if(isset($max_score_att[$c][$row_group->subj_type_id])){
				                   	 							$max_scor_cp = $max_score_att[$c][$row_group->subj_type_id];
				                   	 						}
				                   	 						$result_cp = floor((($score*$max_score_group)/$max_scor_cp)*100)/100;
				                   	 						$sum_cp += $result_cp;
			                   	 						}
			                   	 						$total_cp = floor(($sum_cp/2)*100)/100;			                   	 						
			                   	 						$mean = "";
			                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
	                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
	                   	 										if($subj_score['Min_score'] <= $total_cp && $subj_score['Max_score'] >= $total_cp){
	                   	 											$mean = $subj_score['meaning'];
	                   	 										}
	                   	 									}
	                   	 								}
	                   	 								$sum_assement+=$total_cp;
														$tr_cp.="<tr style='line-height: 10px !important;'><td>".$row_group->subject_type."</td><td id='scor'>".$total_cp."</td><td style='text-align:center;'>".$mean."</td></tr>";
			                   	 						
			                   	 					}
			                   	 					else if($row_group->is_class_participation == 2) // homework
			                   	 					{
			                   	 						$max_group = $row_group->is_group_calc_percent;
			                   	 						$sum_hw = 0;
			                   	 						for($h=1;$h<=2;$h++){
				                   	 						$score = 0;
				                   	 						if(isset($score_att_cp['hw'][$h][$studentid][$row_group->subj_type_id]))
				                   	 						{
				                   	 							foreach($score_att_cp['hw'][$h][$studentid][$row_group->subj_type_id] as $w=>$score_sub){
				                   	 								$m_subj2 = 0;
						                    	 					if(isset($max_att_h['comp'][$h][$row_group->subj_type_id][$w])){
						                    	 						$m_subj2 += $max_att_h['comp'][$h][$row_group->subj_type_id][$w];
				                   	 								}
				                   	 								$sum_w = floor((($score_sub*$max_group)/$m_subj2)*100)/100;
																	$score+= $sum_w;
				                   	 							}
				                   	 							$ii = (count($max_att_h['comp'][$h][$row_group->subj_type_id]) >0?count($max_att_h['comp'][$h][$row_group->subj_type_id]):1);
				                   	 							$score   = floor(($score/($ii==0?1:$ii))*100)/100;
				                   	 							$sum_hw += $score;
				                    	 					}
			                    	 					}
			                    	 					$total_hw = floor(($sum_hw/2)*100)/100;
			                   	 						$mean = "";
			                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
	                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
	                   	 										if($subj_score['Min_score'] <= $total_hw && $subj_score['Max_score'] >= $total_hw){
	                   	 											$mean= $subj_score['meaning'];
	                   	 										}
	                   	 									}
	                   	 								}
	                   	 								$sum_assement+=$total_hw;
														$tr_hw.="<tr style='line-height: 10px !important;'><td>".$row_group->subject_type."</td><td id='scor'>".$total_hw."</td><td style='text-align:center;'>".$mean."</td></tr>";
			                   	 						
			                   	 					}
			                   	 					else if($row_group->is_class_participation == 4)
			                   	 					{
			                   	 						$max_group = $row_group->is_group_calc_percent;
			                   	 						$sum_dl = 0;
			                   	 						for($d=1;$d<=2;$d++){
				                   	 						$score = 0;
				                   	 						if(isset($score_att_cp['dl'][$d][$studentid][$row_group->subj_type_id]))
				                   	 						{
				                   	 							foreach($score_att_cp['dl'][$d][$studentid][$row_group->subj_type_id] as $w=>$score_sub){
				                   	 								$m_subj2 = 0;
						                    	 					if(isset($max_att_h['comp'][$d][$row_group->subj_type_id][$w])){
						                    	 						$m_subj2 += $max_att_h['comp'][$d][$row_group->subj_type_id][$w];
				                   	 								}
				                   	 								$sum_w = floor((($score_sub*$max_group)/$m_subj2)*100)/100;
																	$score+= $sum_w;
				                   	 							}
				                   	 							$ii = (count($max_att_h['comp'][$d][$row_group->subj_type_id]) >0?count($max_att_h['comp'][$d][$row_group->subj_type_id]):1);
				                   	 							$score   = floor(($score/($ii==0?1:$ii))*100)/100;
				                   	 							$sum_dl += $score;
				                    	 					}
			                    	 					}
			                    	 					$total_dl = floor(($sum_dl/2)*100)/100;
			                   	 						$mean = "";
			                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
	                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
	                   	 										if($subj_score['Min_score'] <= $total_dl && $subj_score['Max_score'] >= $total_dl){
	                   	 											$mean= $subj_score['meaning'];
	                   	 										}
	                   	 									}
	                   	 								}
	                   	 								$sum_assement+=$total_dl;
														$tr_dl.="<tr style='line-height: 10px !important;'><td>".$row_group->subject_type."</td><td id='scor'>".$total_dl."</td><td style='text-align:center;'>".$mean."</td></tr>";
			                   	 					}
			                   	 					else if($row_group->is_class_participation == 0 and $row_group->type_subject==1)
			                   	 					{        	                   	 						
			                   	 							$sql_subject = $this->db->query("SELECT
																							si.subjectid,
																							si.`subject`,
																							si.subj_type_id,
																							si.short_sub,
																							si.max_score,
																							si.is_assessment
																							FROM
																							sch_subject_iep AS si
																							WHERE si.subj_type_id='".$row_group->subj_type_id."'
																							");
			                   	 							
			                   	 							if($sql_subject->num_rows() > 0){
			                   	 								$score_m = 0;
			                   	 								$n = 0;
			                   	 								foreach($sql_subject->result() as $row_s){
			                   	 									if(isset($arr_sub_exam[$studentid][1][$row_group->subj_type_id][$row_s->subjectid])){
							                   	 						$total_score_mid+=$arr_sub_exam[$studentid][1][$row_group->subj_type_id][$row_s->subjectid];
							                   	 					}
							                   	 					if(isset($arr_sub_exam[$studentid][2][$row_group->subj_type_id][$row_s->subjectid])){
			                   	 										$score_m = $arr_sub_exam[$studentid][2][$row_group->subj_type_id][$row_s->subjectid];
							                   	 					}
							                   	 					$mean = "";
							                   	 					if(isset($arr_subj_ment[$row_s->subjectid])){ // Max_score Min_score  meaning
				                   	 									foreach($arr_subj_ment[$row_s->subjectid] as $subj_score){
				                   	 										if($subj_score['Min_score'] <= $score_m && $subj_score['Max_score'] >= $score_m){
				                   	 											$mean= $subj_score['meaning'];
				                   	 										}
				                   	 									}
				                   	 								}
				                   	 								if($n == 0){
																		$tr_sub.="<tr style='line-height: 10px !important;'><td rowspan='6' style='vertical-align:middle;'>English</td><td>".$row_s->subject."</td><td id='scor'>".$score_m."</td><td style='text-align:center;'>".$mean."</td></tr>"; 
				                   	 								}else{
				                   	 									$tr_sub.="<tr style='line-height: 10px !important;'><td>".$row_s->subject."</td><td id='scor'>".$score_m."</td><td style='text-align:center;'>".$mean."</td></tr>"; 
				                   	 								}
							                   	 					$n++;
							                   	 					$sum_subject+=$score_m;
			                   	 								}
			                   	 							}
			                   	 					}
			                   	 					else if($row_group->is_class_participation == 0 and $row_group->type_subject==2)
			                   	 					{
			                   	 							$sql_subject = $this->db->query("SELECT
																							si.subjectid,
																							si.`subject`,
																							si.subj_type_id,
																							si.short_sub,
																							si.max_score,
																							si.is_assessment
																							FROM
																							sch_subject_iep AS si
																							WHERE si.subj_type_id='".$row_group->subj_type_id."'
																							");
			                   	 							
			                   	 							if($sql_subject->num_rows() > 0){
			                   	 								$score_m = 0;
			                   	 								foreach($sql_subject->result() as $row_s){
			                   	 									if(isset($arr_sub_exam[$studentid][1][$row_group->subj_type_id][$row_s->subjectid])){
							                   	 						$total_score_mid+= $arr_sub_exam[$studentid][1][$row_group->subj_type_id][$row_s->subjectid];
							                   	 					}
							                   	 					if(isset($arr_sub_exam[$studentid][2][$row_group->subj_type_id][$row_s->subjectid])){
			                   	 										$score_m = $arr_sub_exam[$studentid][2][$row_group->subj_type_id][$row_s->subjectid];
							                   	 					}
							                   	 					$mean = "";
							                   	 					if(isset($arr_subj_ment[$row_s->subjectid])){ // Max_score Min_score  meaning
				                   	 									foreach($arr_subj_ment[$row_s->subjectid] as $subj_score){
				                   	 										if($subj_score['Min_score'] <= $score_m && $subj_score['Max_score'] >= $score_m){
				                   	 											$mean= $subj_score['meaning'];
				                   	 										}
				                   	 									}
				                   	 								}
				                   	 								$sum_subject+=$score_m;
							                   	 					$tr_mscien.="<tr style='line-height: 10px !important;'><td colspan='2' style='padding:0px;white-space: nowrap;'>".$row_s->subject."</td><td id='scor'>".$score_m."</td><td style='text-align:center;'>".$mean."</td></tr>"; 
			                   	 								}
			                   	 							}
			                   	 					}
			                   	 				}
			                   	 			}
			                   	 			$total_f = floor(($sum_subject/2)*100)/100-0;
			                   	 			$total_result_m = floor(($total_score_mid/2)*100)/100-0;
			                   	 			$sum_result = floor((($total_f+$total_result_m+$sum_assement)/2)*100)/100-0;
			                   	 			//$sum_result = ($total_s+$sum_assement)-0;
			                   	 			
			                   	 			unset($arr_score);
			                   	 			unset($arr_sub_exam);
			                   	 			unset($score_att_cp);
			                   	 			unset($max_att);
			                   	 			unset($max_score_att);
			                   	 			unset($max_att_h);
			                   	 	?> 
									<thead>
			                   	 		<tr style="background-color: #cdd6dd !important;padding: 5px;">
			                   	 			<th colspan="2" style="padding: 5px;"><i>Subject</i></th>
			                   	 			<th style="padding: 5px;"><i>Score</i>​</th>
			                   	 			<th style="padding: 5px;"><i>Meaning</i></th>
			                   	 		</tr>
			                   	 	</thead>
			                   	 	<tbody>
			                   	 		<?php echo $tr_mscien.$tr_sub;?>
			                   	 		<tr style='line-height: 10px !important;'>
			                   	 			<td colspan='2' style='text-align:center;'><b>Total Score</b></td>
			                   	 			<td style='text-align:center;'><b><?php echo $sum_subject;?></b></td>
			                   	 			<td></td>
			                   	 		</tr>
			                   	 		<tr style='line-height: 10px !important;'>
			                   	 			<td colspan='2' style='text-align:center;'><b>Mid-Term</b></td>
			                   	 			<td style='text-align:center;'><b><?php echo $total_score_mid;?></b></td>
			                   	 			<td></td>
			                   	 		</tr>
			                   	 	</tbody>
			                   </table>
						</td>
						<td style="width:40%; vertical-align: top;">	
							<table border="1" style="width:100%;">
							 	<thead>
							 		<tr style="background-color: #cdd6dd !important;">
							 			<th id="head" style="padding: 5px;"><i>Core On-going assessment</i></th>
							 			<th id="scor" style="padding: 5px;"><i>Score​</i></th>
							 			<th id="scor" style="padding: 5px;"><i>Meaning</i></th>
							 		</tr>
							 	</thead>
							 	<tbody>
							 		<?php echo $tr_att.$tr_cp.$tr_hw.$tr_dl;?>
							 		<tr style='line-height: 10px !important;'><td style="text-align: center;"><b>Total Score</b></td><td style='text-align:center;'><b><?php echo $sum_assement;?></b></td><td></td></tr>
							 	</tbody>
							</table>
				            
						</td>
					</table>
					<?php 
						
						$sql_grade = $this->db->query("SELECT
														sch_score_mention_gep.menid,
														sch_score_mention_gep.mention,
														sch_score_mention_gep.schlevelid,
														sch_score_mention_gep.grade,
														sch_score_mention_gep.is_past,
														sch_score_mention_gep.min_score,
														sch_score_mention_gep.max_score
														FROM
														sch_score_mention_gep
														WHERE schlevelid='".$schlavelid."'
														ORDER BY min_score ASC");
						$tr_g = "";
						$grade_st  = "";
						$past_fail = "";
						if($sql_grade->num_rows() > 0){
							foreach($sql_grade->result() as $row_g){
								if($row_g->min_score <= $sum_result AND $row_g->max_score >= $sum_result){
									$grade_st  = $row_g->grade;
									if($row_g->is_past == 1){
										$past_fail = "Past";
									}else{
										$past_fail = "Fail";
									}
								}
								if($row_g->max_score <1){
									$tr_g.="<tr style='line-height: 10px !important;'>
												<td>0</td>
												<td style='text-align:center;'>".$row_g->grade."</td>
												<td>".$row_g->mention."</td>
											</tr>";
								}else{
									$tr_g.="<tr style='line-height: 10px !important;'>
												<td>".$row_g->min_score."-".$row_g->max_score."</td>
												<td style='text-align:center;'>".$row_g->grade."</td>
												<td>".$row_g->mention."</td>
											</tr>";
								}
								
							}
						}
						
					?>
					<table border="1" style="width: 99.2%;margin: 4px;">
						<thead>
							<tr style="background:#cdd6dd !important;padding: 6px;">
								<th style="padding: 5px;">Rank</th>
								<th style="padding: 5px;">Grade</th>
								<th style="padding: 5px;">Grand Total Score</th>
								<th style="padding: 5px;">Result</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="text-align: center;">
									<b>
									<?php 
									$arr_para = array("schoolid"=>$schoolid,"schlevelid"=>$schlavelid,"yearid"=>$yearid,"gradelevelid"=>$grandlavelid,"classid"=>$classid,"termid"=>$termid,"studentid"=>$studentid);
									echo $this->green->rank_final($arr_para);
									?>  
									</b>
								</td>
								<td style="text-align: center;"><b><?php echo $grade_st;?></b></td>
								<td style="text-align: center;"><b><?php echo $sum_result;?></b></td>
								<td style="text-align: center;"><b><?php echo $past_fail;?></b></td>
							</tr>
						</tbody>
					</table>
					<?php 
						$show_comment = $this->db->query("SELECT 
														sch_studend_command_iep.command_teacher,
														sch_studend_command_iep.command_guardian,
														sch_studend_command_iep.examp_type,
														sch_studend_command_iep.academicid,
														sch_studend_command_iep.userid,
														sch_studend_command_iep.date_academic,
														sch_studend_command_iep.date_teacher,
														sch_studend_command_iep.date_return,
														sch_studend_command_iep.date_guardian,
														sch_studend_command_iep.date_create
														FROM
														sch_studend_command_iep
														WHERE 1=1
														AND student_id='".$studentid."'
												        AND schoolid  = '".$schoolid."'
												        AND schlevelid='".$schlavelid."'
												        AND yearid    ='".$yearid."'
												        AND gradelevelid='".$grandlavelid."'
												        AND classid   = '".$classid."'
												        AND termid    = '".$termid."'
												        AND examp_type='".$examtype."'
												        ")->row();
						$acedamic  = isset($show_comment->academicid)?$show_comment->academicid:"";
						$teacherid = isset($show_comment->userid)?$show_comment->userid:"";
						$sql_user_acad = "";
						$sql_user_id= "";
						if($acedamic != ""){
							$sql_user_acad = $this->db->query("SELECT
						                                  sch_user.user_name
						                                  FROM
						                                  sch_user
						                                  WHERE match_con_posid='acca' AND userid='".$acedamic."'")->row()->user_name;
						}
						if($teacherid != ""){
							$sql_user_id = $this->db->query("SELECT
						                                  sch_user.user_name
						                                  FROM
						                                  sch_user
						                                  WHERE userid='".$teacherid."'")->row()->user_name;
						}
						
					?>
					<table style="width:100%;">
						<tr>
							<td style="vertical-align: top;width:40%;">
								<table style="width:100%" border="1">
									<tr style="background-color: #cdd6dd !important;">
										<th colspan="3" style="padding: 5px;">Grading System</th>
									</tr>
									<tr>
										<th style="padding: 3px;">Score</th>
										<th style="padding: 3px;">Grade</th>
										<th style="padding: 3px;">Meaning</th>
									</tr>
									<?php
										echo $tr_g;
									?>
								</table>
							</td>
							<td style="vertical-align: top;">
								<table style="width:100%;" border="1">
									<tr style="background-color: #cdd6dd !important;">
										<th colspan="3" style="padding: 5px;">Teacher's Comment</th>
									</tr>
									<tr>
										<td style="height: 157px;vertical-align: top;">
											<?php echo isset($show_comment->command_teacher)?$show_comment->command_teacher:""; ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					<table style="width:99.2%;margin:4px;">
						<tr style="vertical-align: top;height:70px;">
							<td style="">
								<div style="float: left;width:200px;border:0px solid black;">
									<p style="float: left;">Date:</p>
									<p style="float: left;width:150px;border-bottom: 1px dotted black;text-align:center;"><?php echo isset($show_comment->date_academic)?($this->green->formatSQLDate($show_comment->date_academic)):"";?>&nbsp;</p>
									<p style="float: left;width:100%;text-align: center;">Academic Manager</p>
								</div>
							</td>
							<td style="text-align: right;">
								<div style="float: right;width:200px;border:0px solid black;">
									<p style="float: left;">Date:</p>
									<p style="float: left;width:150px;border-bottom: 1px dotted black;text-align:center;"><?php echo isset($show_comment->date_teacher)?($this->green->formatSQLDate($show_comment->date_teacher)):"";?>&nbsp;</p>
									<p style="float: left;width:100%;text-align: center;">Teacher's Signature</p>
								</div>
							</td>
						</tr>
						<tr>
							<td><p style="text-align: left;margin-left: 80px;"><b><?php echo strtoupper($sql_user_acad);?></b></p></td>
							<td><p style="text-align: right;margin-right: 80px;"><b><?php echo strtoupper($sql_user_id);?></b></p></td>
						</tr>
						<tr>
							<td colspan="2" style="height: 100px;vertical-align: top;border:1px solid black;padding: 5px;"><?php echo isset($show_comment->command_guardian)?$show_comment->command_guardian:""; ?></td>
						</tr>
					</table>
					<table style="width:99.2%;margin:4px;">
						<tr>
							<td style="text-align:left;">
								<p>Please return the book to school before</p>			
								<span style="text-align: center;border-bottom: 1px dotted black;width:200px;float:left;"><?php echo isset($show_comment->date_return)?($this->green->formatSQLDate($show_comment->date_return)):"";?>&nbsp;</span>
							</td>
							<td style="text-align:right;">
								<div style="float: right;width:200px;border:0px solid black;">
									<p style="float: left;">Date:</p>
									<p style="float: left;width:150px;border-bottom: 1px dotted black;text-align:center;"><?php echo isset($show_comment->date_guardian)?($this->green->formatSQLDate($show_comment->date_guardian)):"";?>&nbsp;</p>
									<p style="float: left;width:100%;text-align: center;">Guardian's Signature</p>
								</div>
							</td>
						</tr>
					</table>
			</div>		   		   		
	</div>
	<!-- <div style="page-break-after: always;"></div> -->

<div style="page-break-after: always;"></div>
<script type="text/javascript">
	$(function(){
		$("#a_print").on("click", function () {
        gPrint("tap_print");
	    });

	    $("#date").datepicker({
	        onSelect: function(date) {
	          $(this).val(formatDate(date));
	          getdata($(this).val());
	        } 
	    });
	});

	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();
	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;
	    return [year, month, day].join('-');
    }
</script>

