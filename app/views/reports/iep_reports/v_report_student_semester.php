
    <div class="row">
      <div class="col-xs-12">
          <div class="result_info">
              <div class="col-xs-6">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                  <strong>Semester IEP Report</strong>  
              </div>
              <div class="col-xs-6" style="text-align: right">
                  <a href="javascript:void(0);" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                     <img src="<?= base_url('assets/images/icons/print.png') ?>">
                  </a>
              </div>         
          </div>
      </div>
    </div>
	<?php 
		$studentid  = isset($_GET['id']) ? $_GET['id'] : '';	
		$programeid = isset($_GET['p_id']) ? $_GET['p_id'] : '';	
		$classid    = isset($_GET['classid']) ? $_GET['classid'] : '';	
		$semesterid = isset($_GET['semesterid']) ? $_GET['semesterid'] : '';
		$schlavelid = isset($_GET['schlid']) ? $_GET['schlid'] : '';
		$schoolid   = isset($_GET['schoolid']) ? $_GET['schoolid'] : '';
		$grandlavelid = isset($_GET['grandid']) ? $_GET['grandid'] : '';
		$yearid = isset($_GET['yearid']) ? $_GET['yearid'] : '';
		$title_semester = $this->db->query("SELECT
												sch_school_semester.semester
												FROM
												sch_school_semester
												WHERE 1=1 AND semesterid='".$semesterid."'")->row()->semester;
		$row_stu = $this->db->query("SELECT
											vse.student_num,
											vse.first_name,
											vse.last_name,
											vse.first_name_kh,
											vse.last_name_kh,
											vse.gender,
											vse.class_name,
											vse.studentid,
											vse.schoolid,
											vse.`year`,
											vse.classid,
											vse.schlevelid,
											vse.rangelevelid,
											vse.feetypeid,
											vse.programid,
											vse.sch_level,
											vse.rangelevelname,
											vse.program,
											vse.sch_year,
											vse.grade_levelid,
											tt.term,
											tt.termid,
											gl.grade_level,
											YEAR(vse.from_date) AS fyear,
											YEAR(vse.to_date) AS tyear
											FROM
											v_student_enroment AS vse
											INNER JOIN sch_school_term AS tt ON tt.yearid = vse.`year`
											INNER JOIN sch_grade_level AS gl ON gl.grade_levelid = vse.`grade_levelid`
											WHERE 1=1
											AND vse.schoolid='".$schoolid."'
											AND vse.programid = '".$programeid."'
											AND vse.classid = '".$classid."' 
											AND vse.studentid ='".$studentid."'
											AND vse.schlevelid ='".$schlavelid."'
											AND vse.grade_levelid ='".$grandlavelid."'
											AND tt.yearid ='".$yearid."'
											GROUP BY vse.studentid")->row();	
	?>  
	
	<div  id="show_print" ><!-- div pppp -->
		<center>   
		<div style="border:0px solid #f00;width:95% !important;margin-top: 20px;">
				   	<table style="width:100%;" border="0">
				   		<tr style="vertical-align: top;">
				   			<td style="width:25%;"><img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="width:200px !important"></td>
				   			<td style="text-align: center;">
								<h4 class="kh_font">របាយការណ៏សិក្សាបញ្ចប់ត្រីមាស</h4>	
					   			<h4 class="kh_font">ចំណេះទូទៅភាសាអង់គ្លេស</h4>		   			
					   			<h4 style="font-family: 'Arial Black' !important;"><?php echo $title_semester;?></h4>
				   			</td>
				   			<td style="width:25%;"></td>
				   		</tr>	   		
				   	</table>	 	
						
	                <table border="0" style="width:90%;">
	                   	<tbody>
	                   		<tr>
	                   			<td><p style="float: left;font-size: 13px;">First name&nbsp;:</p><p style="float: left;margin-left: 10px;font-size: 13px;"><b><?php echo $row_stu->first_name;?></b></p></td>
	                   			<td><p style="float: left;font-size: 13px;">Last name&nbsp;:</p><p style="float: left;margin-left: 10px;font-size: 13px;"><b><?php echo $row_stu->last_name;?></b></p></td>
	                   		</tr>
	                   		<tr>
                   				<td><p style="float: left;font-size: 13px;">Student's ID&nbsp;:</p><p style="float: left;margin-left: 10px;font-size: 13px;"><b><?php echo $row_stu->student_num;?></b></p></td>
                   				<td><p style="float: left;font-size: 13px;">Sex&nbsp;:</p><p style="float: left;margin-left: 10px;font-size: 13px;"><b><?php echo ucfirst($row_stu->gender);?></b></p></td>
                   			</tr>
	                   		<tr>
                   				<td><p style="float: left;font-size: 13px;">Grade&nbsp;:</p><p style="float: left;margin-left: 10px;font-size: 13px;"><b><?php echo $row_stu->class_name;?></b></p></td>
                   				<td>Time&nbsp;:</td>
                   			</tr>
	                   		
	                   	</tbody>
	                </table>
					<table style="width:100%;"><!-- 	div aaaaa	 -->		
						<tr style="vertical-align: top;">
							<td style="width:50%">
			                    <table width="100%" border="1">
			                   	 	<thead>
			                   	 		<tr style="background-color: #cdd6dd !important;line-height: 25px !important;">
			                   	 			<th id="head" colspan="2">Subject</th>
			                   	 			<th>Score​</th>
			                   	 			<th>Meaning</th>
			                   	 		</tr>
			                   	 	</thead>
			                   	 	<tbody>
			                   	 	<?php
			                   	 			$sql_score = $this->db->query("SELECT
																				ord.type,
																				ord.typeno,
																				ord.main_tranno,
																				det.studentid,
																				si.`subject`,
																				det.subjectid,
																				si.subj_type_id,
																				det.score_num,
																				det.group_subject,
																				ord.exam_typeid,
																				ord.termid,
																				sch_school_term.semesterid,
																				ord.count_subject,
																				sch_subject_type_iep.is_class_participation
																				FROM
																				sch_score_iep_entryed_order AS ord
																				INNER JOIN sch_score_iep_entryed_detail AS det ON ord.typeno = det.typeno AND ord.type = det.type
																				INNER JOIN sch_subject_iep AS si ON det.subjectid = si.subjectid
																				INNER JOIN sch_school_term ON ord.termid = sch_school_term.termid
																				INNER JOIN sch_subject_type_iep ON si.subj_type_id = sch_subject_type_iep.subj_type_id
																				WHERE 1 = 1
																				AND ord.classid ='".$classid."'
																				AND ord.schoolid = '".$schoolid."'
																				AND ord.schlevelid ='".$schlavelid."'
																				AND ord.gradelevelid = '".$grandlavelid."'
																				AND ord.yearid = '".$yearid."'
																				AND sch_school_term.semesterid = '".$semesterid."'
																				AND det.studentid='{$studentid}' 
																				");

			                   	 			
			                   	 			$arr_score = array();
			                   	 			$arr_sub_exam = array();
			                   	 			$jj = 0;
			                   	 			if($sql_score->num_rows() > 0){
			                   	 				foreach($sql_score->result() as $row_sc){ 
			                   	 					if($row_sc->is_class_participation !=0 ){
				                   	 					if(isset($arr_score[$row_sc->group_subject])){
				                   	 						$arr_score[$row_sc->group_subject] = $arr_score[$row_sc->group_subject]+$row_sc->score_num;
				          								}else{
				                   	 						$arr_score[$row_sc->group_subject] = $row_sc->score_num;
				                   	 					}
				                   	 				}else{
				                   	 					if(isset($arr_sub_exam[$row_sc->group_subject][$row_sc->subjectid])){
															$arr_sub_exam[$row_sc->group_subject][$row_sc->subjectid] = $arr_sub_exam[$row_sc->group_subject][$row_sc->subjectid]+$row_sc->score_num;
				                   	 					}else{
				                   	 						$arr_sub_exam[$row_sc->group_subject][$row_sc->subjectid] = $row_sc->score_num;
				                   	 					}
				                   	 					
				                   	 				}
			                   	 				}
			                   	 			}

			                   	 			$sql_mess = $this->db->query("SELECT
																			total_assess.schoolid,
																			total_assess.programid,
																			total_assess.schlevelid,
																			total_assess.yearid,
																			total_assess.termid,
																			total_assess.gradeid,
																			total_assess.classid,
																			total_assess.studentid,
																			SUM(total_assess.attandance) AS attandance,
																			SUM(total_assess.class_participation) AS class_participation,
																			SUM(total_assess.homework) AS homework,
																			SUM(total_assess.diligence) AS diligence,
																			SUM(total_assess.`subject`) AS `subject`,
																			SUM(total_assess.total_score) AS total_score,
																			sch_school_term.semesterid
																			FROM
																			sch_iep_total_score_entry AS total_assess
																			INNER JOIN sch_school_term ON total_assess.termid = sch_school_term.termid
																			WHERE 1=1
																			AND total_assess.schoolid = '".$schoolid."'
																			AND total_assess.schlevelid = '".$schlavelid."'
																			AND total_assess.yearid = '".$yearid."'
																			AND sch_school_term.semesterid = '".$semesterid."'
																			AND total_assess.gradeid = '".$grandlavelid."'
																			AND total_assess.classid = '".$classid."'
																			AND total_assess.studentid = '".$studentid."'
																			GROUP BY total_assess.studentid
																		");
			                   	 			$arr_total = array();
			                   	 			if($sql_mess->num_rows() > 0){
			                   	 				foreach($sql_mess->result() as $row_semm){
			                   	 					$arr_total=array("attendance"=>$row_semm->attandance,
			                   	 									"class_participation"=>$row_semm->class_participation,
			                   	 									"homework"=>$row_semm->homework,
			                   	 									"diligence"=>$row_semm->diligence,
			                   	 									"total_score"=>$row_semm->total_score
			                   	 									);
			                   	 				}
			                   	 			}
			                   	 			$sql_score_mention = $this->db->query("SELECT
																						sm.submenid,
																						sm.schoolid,
																						sm.schlevelid,
																						sm.subjectid,
																						sm.gradelevelid,
																						sm.mentionid,
																						sm.max_score,
																						sm.minscrore,
																						cm.mention
																				FROM
																				sch_subject_mention AS sm
																				LEFT JOIN sch_score_mention AS cm ON sm.mentionid = cm.menid
																				WHERE 1=1 
																				AND sm.schoolid='".$schoolid."'
																				AND sm.schlevelid='".$schlavelid."'
																				AND sm.gradelevelid='".$grandlavelid."'
																				");
			                   	 			$arr_subj_ment = array();
			                   	 			if($sql_score_mention->num_rows() > 0){
			                   	 				foreach($sql_score_mention->result() as $row_m){
			                   	 					$arr_subj_ment[$row_m->subjectid][] = array('Max_score'=>$row_m->max_score,'Min_score'=>$row_m->minscrore,'meaning'=>$row_m->mention);
			                   	 				}
			                   	 			}
			                  
			                   	 			$sql_group = $this->db->query("SELECT
																				sch_subject_type_iep.subj_type_id,
																				sch_subject_type_iep.subject_type,
																				sch_subject_type_iep.main_type,
																				sch_subject_type_iep.schlevelid,
																				sch_subject_type_iep.schoolid,
																				sch_subject_type_iep.is_class_participation,
																				sch_subject_type_iep.type_subject,
																				sch_subject_type_iep.is_group_calc_percent
																				FROM
																				sch_subject_type_iep
																				WHERE schoolid='".$schoolid."' AND schlevelid='".$schlavelid."'
																				-- AND is_class_participation=2
																				ORDER BY sch_subject_type_iep.`subject_type`
																				");
			                   	 			$tr_att = "";
			                   	 			$tr_cp = "";
			                   	 			$tr_hw = "";
			                   	 			$tr_dl = "";
			                   	 			$tr_mscien  = "";
			                   	 			$tr_subject = "";
			                   	 			$sum_subject    = 0;
			                   	 			$sum_assessment = 0;
			                   	 			if($sql_group->num_rows() > 0)
			                   	 			{
			                   	 				foreach($sql_group->result() as $row_group)
			                   	 				{
			                   	 					if($row_group->is_class_participation == 3) // Attendance
			                   	 					{
			                   	 						
			                   	 						$score_att  = 0;
		                   	 							if(isset($arr_total['attendance'])){
		                   	 								$score_att = $arr_total['attendance'];
		                   	 							}
		                   	 							$result_att = floor(($score_att/4)*100)/100;			                   	 						
			                   	 						$mean = "";
			                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
	                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
	                   	 										if($subj_score['Min_score'] <= $result_att && $subj_score['Max_score'] >= $result_att){
	                   	 											$mean.= $subj_score['meaning'];
	                   	 										}
	                   	 									}
	                   	 								}
	                   	 								$sum_assessment+=$result_att;
			                   	 						$tr_att.="<tr><td>".$row_group->subject_type."</td><td id='scor'>".$result_att."</td><td style='text-align:center;'>".$mean."</td></tr>";
			                   	 						
			                   	 					}
			                   	 					else if($row_group->is_class_participation == 1) // Class participation
			                   	 					{
			                   	 						$score_pc = 0;
		                   	 							if(isset($arr_total['class_participation'])){
		                   	 								$score_pc = $arr_total['class_participation'];
		                   	 							}
		                   	 							$result_cp = floor(($score_pc/4)*100)/100;		                   	 						
			                   	 						$mean = "";
			                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
	                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
	                   	 										if($subj_score['Min_score'] <= $result_cp && $subj_score['Max_score'] >= $result_cp){
	                   	 											$mean.= $subj_score['meaning'];
	                   	 										}
	                   	 									}
	                   	 								}
	                   	 								$sum_assessment+=$result_cp;
														$tr_cp.="<tr><td>".$row_group->subject_type."</td><td id='scor'>".$result_cp."</td><td style='text-align:center;'>".$mean."</td></tr>";
			                   	 						
			                   	 					}
			                   	 					else if($row_group->is_class_participation == 2) // homework
			                   	 					{
			           									$score_hw = 0;
		                   	 							if(isset($arr_total['homework'])){
		                   	 								$score_hw = $arr_total['homework'];
		                   	 							}
		                   	 							$result_hom = floor(($score_hw/4)*100)/100;
			                   	 						$mean = "";
			                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
	                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
	                   	 										if($subj_score['Min_score'] <= $result_hom && $subj_score['Max_score'] >= $result_hom){
	                   	 											$mean.= $subj_score['meaning'];
	                   	 										}
	                   	 									}
	                   	 								}
	                   	 								$sum_assessment+=$result_hom;
														$tr_hw.="<tr><td>".$row_group->subject_type."</td><td id='scor'>".round($result_hom,2)."</td><td style='text-align:center;'>".$mean."</td></tr>";
			                   	 						
			                   	 					}
			                   	 					else if($row_group->is_class_participation == 4) //Diligence
			                   	 					{
			                   	 						$result_delig = 0;
			                   	 						$score_dl = 0;
		                   	 							if(isset($arr_total['diligence'])){
		                   	 								$score_dl = $arr_total['diligence'];
		                   	 							}
		                   	 							$result_delig = floor(($score_dl/4)*100)/100;
			                   	 						$mean = "";
			                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
	                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
	                   	 										if($subj_score['Min_score'] <= $result_delig && $subj_score['Max_score'] >= $result_delig){
	                   	 											$mean= $subj_score['meaning'];
	                   	 										}
	                   	 									}
	                   	 								}
	                   	 								$sum_assessment+=$result_delig;
			                   	 						$tr_dl.="<tr><td>".$row_group->subject_type."</td><td id='scor'>".$result_delig."</td><td style='text-align:center;'>".$mean."</td></tr>";
			                   	 						
			                   	 					}
			                   	 					else if($row_group->is_class_participation == 0 and $row_group->type_subject==1)
			                   	 					{           	
			                   	 						$sql_subject = $this->db->query("SELECT
																							si.subjectid,
																							si.`subject`,
																							si.subj_type_id,
																							si.short_sub,
																							si.max_score,
																							si.is_assessment
																							FROM
																							sch_subject_iep AS si
																							WHERE si.subj_type_id='".$row_group->subj_type_id."'	
																						");
						                   	 			
						                   	 			if($sql_subject->num_rows() > 0){
						                   	 				$b=0;
						                   	 				foreach($sql_subject->result() as $row_s){ 
						                   	 					$score_s = 0;
						                   	 					if(isset($arr_sub_exam[$row_group->subj_type_id][$row_s->subjectid])){
						                   	 						$score_s = $arr_sub_exam[$row_group->subj_type_id][$row_s->subjectid];
						                   	 					}
						                   	 					$score_s = floor(($score_s/4)*100)/100;
					                   	 						$mean_s = "";
			                   	 								if(isset($arr_subj_ment[$row_s->subjectid])){ // Max_score Min_score  meaning
			                   	 									foreach($arr_subj_ment[$row_s->subjectid] as $subj_score){
			                   	 										if($subj_score['Min_score']<=$score_s && $subj_score['Max_score']>=$score_s){
			                   	 											$mean_s= $subj_score['meaning'];
			                   	 										}
			                   	 									}
			                   	 								}
			                   	 								//$sum_assesment_eng_test+=$score;
			                   	 								$sum_subject+=$score_s;
			                   	 								if($b==0){
																	$tr_subject.="<tr style='line-height: 10px !important;'><td rowspan='6' style='vertical-align:middle;'>English</td><td>".$row_s->subject."</td><td id='scor'>".$score_s."</td><td style='text-align:center;'>".$mean_s."</td></tr>";
			                   	 								}else{
						                   	 						$tr_subject.="<tr style='line-height: 10px !important;'><td>".$row_s->subject."</td><td id='scor'>".$score_s."</td><td style='text-align:center;'>".$mean_s."</td></tr>";
						                   	 					}
						                   	 					$b++;
						                   	 				}
						                   	 			}
			                   	 					}
			                   	 					else if($row_group->is_class_participation == 0 and $row_group->type_subject==2)
			                   	 					{

			                   	 						$sql_subject = $this->db->query("SELECT
																							si.subjectid,
																							si.`subject`,
																							si.subj_type_id,
																							si.short_sub,
																							si.max_score,
																							si.is_assessment
																							FROM
																							sch_subject_iep AS si
																							WHERE si.subj_type_id='".$row_group->subj_type_id."'	
																						");
						                   	 			
						                   	 			if($sql_subject->num_rows() > 0){
						                   	 				foreach($sql_subject->result() as $row_s){ 
						                   	 					$score = 0;
						                   	 					if(isset($arr_sub_exam[$row_group->subj_type_id][$row_s->subjectid])){
						                   	 						$score = $arr_sub_exam[$row_group->subj_type_id][$row_s->subjectid];
						                   	 					}
						                   	 					//if(isset($arr_m_cal[$row_group->subj_type_id][$row_s->subjectid])){
						                   	 					//	$cal_m = $arr_m_cal[$row_group->subj_type_id][$row_s->subjectid];
																	//$score = ($score*$row_s->max_score)/$cal_m;
						                   	 					//}
						                   	 					//$score = floor($score*100)/100;
						                   	 					$score = floor(($score/4)*100)/100;
					                   	 						$mean  = "";
			                   	 								if(isset($arr_subj_ment[$row_s->subjectid])){ // Max_score Min_score  meaning
			                   	 									foreach($arr_subj_ment[$row_s->subjectid] as $subj_score){
			                   	 										if($subj_score['Min_score']<=$score && $subj_score['Max_score']>=$score){
			                   	 											$mean = $subj_score['meaning'];
			                   	 										}
			                   	 									}
			                   	 								}
			                   	 								//$sum_math_scien_exam+=$score;
			                   	 								$sum_subject+=$score;
						                   	 					$tr_mscien.="<tr style='line-height: 10px !important;'><td colspan='2'>".$row_s->subject."</td><td id='scor'>".$score."</td><td style='text-align:center;'>".$mean."</td></tr>";
						                   	 				}
						                   	 			}

			                   	 					}
			                   	 				}
			                   	 			}
			                   	 			//$total_result = round(($sum_assesment_eng_test+$sum_math_scien_exam)/2,2);
			                   	 			$total_result     = $sum_subject;
			                   	 			$total_sessmester = ($sum_assessment+$sum_subject);
			                   	 			$grand_total_score= floor((($sum_assessment+$sum_subject)/2)*100)/100;
			                   	 			echo $tr_mscien.$tr_subject;
			                   	 			echo "<tr style='line-height: 10px !important;'>
			                   	 						<td colspan='2'><b>Total Score</b></td>
			                   	 						<td style='text-align:center;'><b>".$sum_subject."</b></td>
			                   	 						<td></td>
			                   	 					</tr>
			                   	 				 	<tr style='line-height: 10px !important;'><td colspan='2'><b>Total Semester</b></td>
			                   	 				 		<td style='text-align:center;'><b>".$total_sessmester."</b></td>
			                   	 				 		<td></td>
			                   	 				 	</tr>";		        
									?>
			                   	 	</tbody>
			                   </table>
							</td>
							<td style="width:50%;"">	
			                   <table width="100%" border="1">
			                   	 	<thead>
			                   	 		<tr style="background-color: #cdd6dd !important;line-height: 25px !important;">
			                   	 			<th id="head">Core On-going assessment</th>
			                   	 			<th id="scor">Score​</th>
			                   	 			<th id="scor">Meaning</th>
			                   	 		</tr>
			                   	 	</thead>
			                   	 	<tbody>
			                   	 		<?php echo $tr_att.$tr_cp.$tr_hw.$tr_dl;?>
			                   	 		<tr><td><b>Total Score</b></td><td style="text-align: center;"><b>
			                   	 			<?php echo $sum_assessment;?></b>
			                   	 		</td><td></td></tr>
			                   	 	</tbody>
			                   </table>
							</td> <!-- end div sm-6 -->
						</tr>
					<!-- 	div aaaaa	 -->
					<?php 
						$sql_grade = $this->db->query("SELECT
														sch_score_mention_gep.menid,
														sch_score_mention_gep.mention,
														sch_score_mention_gep.schlevelid,
														sch_score_mention_gep.grade,
														sch_score_mention_gep.is_past,
														sch_score_mention_gep.min_score,
														sch_score_mention_gep.max_score
														FROM
														sch_score_mention_gep
														WHERE schlevelid='".$schlavelid."'
														ORDER BY min_score ASC");
						$tr_g = "";
						$grade_st  = "";
						$past_fail = "";
						if($sql_grade->num_rows() > 0){
							foreach($sql_grade->result() as $row_g){
								if($row_g->min_score<=$grand_total_score AND $row_g->max_score>=$grand_total_score){
									$grade_st  = $row_g->grade;
									if($row_g->is_past == 1){
										$past_fail = "Past";
									}else{
										$past_fail = "Fail";
									}
								}
								if($row_g->max_score <1){
									$tr_g.="<tr style='line-height: 10px !important;'>
												<td>0</td>
												<td style='text-align:center;'>".$row_g->grade."</td>
												<td>".$row_g->mention."</td>
											</tr>";
								}else{
									$tr_g.="<tr style='line-height: 10px !important;'>
												<td>".$row_g->min_score."-".$row_g->max_score."</td>
												<td style='text-align:center;'>".$row_g->grade."</td>
												<td>".$row_g->mention."</td>
											</tr>";
								}
								
							}
						}
					?>
						<tr>
							<td colspan="2">
								<table style="width:100%;" border="1">
									<tr style="background-color: #cdd6dd !important;line-height: 25px !important;">
										<th>Rank</th>
										<th>Grade</th>
										<th>Grand Total Score</th>
										<th>Result</th>
									</tr>
									<tr>
										<td style="text-align: center;"><b>
										<?php 

											$arr_semester = array("schoolid"=>$schoolid,"schlevelid"=>$schlavelid,
																"yearid"=>$yearid,"grade_levelid"=>$grandlavelid,
																"classid"=>$classid,"studentid"=>$studentid-0,
																"semesterid"=>$semesterid
																);
											$total_sem = $this->green->total_semester($arr_semester);
											echo $total_sem['rank'];
										?></b>
										</td>
										<td style="text-align: center;"><b><?php echo $grade_st;?></b></td>
										<td style="text-align: center;"><b><?php echo $grand_total_score;?></b></td>
										<td style="text-align: center;"><b><?php echo $past_fail;?></b></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table style="width:100%;">
						<tr style="vertical-align: top;">
							<td style="width:40%;height:30%;">
								<table style="width:100%;" border="1">
									<tr style="background-color: #cdd6dd !important;line-height: 25px !important;">
										<th colspan="3">Granding System</th>
									</tr>
									<tr style="line-height: 25px !important;">
										<th>Score</th><th>Grade</th><th>Meaning</th>
									</tr>
									<?php
										echo $tr_g;
									?>
								</table>
							</td>
							<?php
										$sql_valu = $this->db->query("SELECT
																		eval_des.description_id,
																		eval_des.description
																		FROM
																		sch_evaluation_semester_iep_description AS eval_des");
										$tr_ev = "";
										$comment_teacher = "";
										$comment_guademic= "";
										$date_academic   = "";
										$date_teacher    = "";
										$date_return     = "";
										$date_guardian   = "";
										$user_acad       = "";
										$username       = "";
										if($sql_valu->num_rows() > 0)
										{
											foreach($sql_valu->result() as $row_valu)
											{
															
															$sql_desc = $this->db->query("SELECT
																							evoder.studentid,
																							evoder.schoolid,
																							evoder.schlevelid,
																							evoder.grade_levelid,
																							evoder.yearid,
																							evoder.semester,
																							evoder.classid,
																							evoder.academicid,
																							evoder.userid,
																							evdet.seldom,
																							evdet.descrition_id,
																							evdet.sometimes,
																							evdet.usually,
																							evdet.consistently,
																							evoder.techer_comment,
																							evoder.guardian_comment,
																							DATE_FORMAT(evoder.academic_date,'%d/%m/%Y') AS academic_date,
																							DATE_FORMAT(evoder.techer_date,'%d/%m/%Y') AS techer_date,
																							DATE_FORMAT(evoder.guardian_date,'%d/%m/%Y') AS guardian_date,
																							DATE_FORMAT(evoder.return_date,'%d/%m/%Y') AS return_date
																							FROM
																							sch_evaluation_semester_iep_order AS evoder
																							INNER JOIN sch_evaluation_semester_iep_detail AS evdet ON evoder.evaluationid = evdet.evaluationid
																							WHERE 1=1 
																							AND evoder.schoolid= '".$schoolid."'
																							AND evoder.schlevelid ='".$schlavelid."'
																							AND evoder.grade_levelid = '".$grandlavelid."'
																							AND evoder.yearid = '".$yearid."'
																							AND evoder.semester = '".$semesterid."'
																							AND evdet.descrition_id = '".$row_valu->description_id."'
																							AND evoder.studentid='".$studentid."'
																						");
															
															if($sql_desc->num_rows() > 0)
															{
																foreach($sql_desc->result() as $row_d)
																{
																	$comment_teacher = $row_d->techer_comment;
																	$comment_guademic= $row_d->guardian_comment;
																	$date_academic   = $row_d->academic_date;
																	$date_teacher    = $row_d->techer_date;
																	$date_return     = $row_d->return_date;
																	$date_guardian   = $row_d->guardian_date;
																	$academicid   = $row_d->academicid;
																	$userid       = $row_d->userid;
																	$user_acad   = $this->db->query("SELECT user_name FROM sch_user WHERE userid='".$academicid."' AND match_con_posid='acca'")->row();
																	$username    = $this->db->query("SELECT user_name FROM sch_user WHERE userid='".$userid."'")->row();
																	$ch_seldom   = "";
																	$ch_sometime = "";
																	$ch_usually  = "";
																	$ch_consist  = "";
																	if($row_d->seldom == 1){
																		$ch_seldom = 'checked="checked"';
																	}
																	if($row_d->sometimes == 1){
																		$ch_sometime = 'checked="checked"';
																	}
																	if($row_d->usually == 1){
																		$ch_usually = 'checked="checked"';
																	}
																	if($row_d->consistently == 1){
																		$ch_consist = 'checked="checked"';
																	}

																	$tr_ev.= "<tr style='line-height:10px !important;'>
																				<td>".$row_valu->description."</td>
																				<td style='text-align: center;'><input type='checkbox' ".$ch_seldom." onClick='return chdisable()' name='seldom' id='seldom' class='seldom'></td>
																				<td style='text-align: center;'><input type='checkbox' ".$ch_sometime." onClick='return chdisable()' name='sometim' id='sometim' class='sometim'></td>
																				<td style='text-align: center;'><input type='checkbox' ".$ch_usually." onClick='return chdisable()' name='usually' id='usually' class='usually'></td>
																				<td style='text-align: center;'><input type='checkbox' ".$ch_consist." onClick='return chdisable()' name='consistently' id='consistently' class='consistently'></td>
																			</tr>";
																}
															}else{
																$tr_ev.= "<tr style='line-height:10px !important;'>
																				<td>".$row_valu->description."</td>
																				<td style='text-align: center;'><input type='checkbox' onClick='return chdisable()' name='seldom' id='seldom' class='seldom'></td>
																				<td style='text-align: center;'><input type='checkbox' onClick='return chdisable()' name='sometim' id='sometim' class='sometim'></td>
																				<td style='text-align: center;'><input type='checkbox' onClick='return chdisable()' name='usually' id='usually' class='usually'></td>
																				<td style='text-align: center;'><input type='checkbox' onClick='return chdisable()' name='consistently' id='consistently' class='consistently'></td>
																			</tr>";
															}
											}
										}
							?>
							<td style="width:60%;height:20%;">
								<table style="width:100%;" border="1">
									<tr style="background-color: #cdd6dd !important;line-height: 25px !important;">
										<th colspan="3">Teacher's Comment</th>
									</tr>
									<tr>
										<td style="height: 160px;vertical-align: top;">
											<!-- <textarea rows="8" class="form-control"></textarea> -->
											<?php echo $comment_teacher;?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table style="width:100%;">
						<tr>
							<td colspan="2">
								<table style="width:100%;" border="1"><!-- div vvvvv -->
									<thead>
										<tr style="">
											<th colspan="5" style="padding:5px;">
												<table>
													<tr>
														<td style="text-align: right;"><p style="">Evaluation for Semester 1 /&nbsp;</p></td>
														<td style="text-align: left;"><p style="font-family: 'Khmer OS Muol Light' !important;">ការវាយតម្លៃក្នុងឆមាសទី ១</p></td>
													</tr>
												</table>
											</th>
										</tr>
									</thead>
									<tbody>
										
										<tr>
											<td style="text-align: center;"><b>Work Habits and Social Skills</b><br>ទំលាប់ក្នុងការសិក្សា និងជំនាញសង្គម</td>
											<td style="text-align: center;"><b>Seldom</b><br>កក្រ</td>
											<td style="text-align: center;"><b>Sometimes</b><br>ម្តងម្កាល</td>
											<td style="text-align: center;"><b>Usually</b><br>តែងតែ</td>
											<td style="text-align: center;"><b>Consistently</b><br>ជាប្រចាំ</td>
										</tr>
										<?php echo $tr_ev;?>	
									</tbody>
								</table><!-- end div vvvvv -->
							</td>
						</tr>
						<tr>
							<td style="text-align: left;">
								<div style="width:300px;border:0px solid black;text-align: center;">
									<p style="font-size: 12px;">Date:&nbsp;&nbsp;&nbsp;<span style="border-bottom: 0px dotted black;font-size: 12px;"><?php echo $date_academic; ?></span></p>				
						            <p>Academic Manager</p>
						            
		                            <p style="margin-top: 50px;font-size: 14px;"><b><?php echo isset($user_acad->user_name)?(ucwords($user_acad->user_name)):"";?></b></p>
					            </div>
							</td>
							<td style="text-align: right;">
								<div style="border:0px solid black;text-align: center;">
									<p>Date:&nbsp;&nbsp;&nbsp;<span style="border-bottom: 0px dotted black;font-size: 12px;"><?php echo $date_teacher; ?></span></p>				
						            <p>Teacher's Signature</p>
						            <p style="margin-top: 50px;font-size: 14px;"><b><?php echo isset($username->user_name)?(ucwords($username->user_name)):"";?></b></p>
						        </div>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="">
								<textarea rows="8" readonly="" id="g_comment" style="border:1px solid black;width:100%;margin-top:0px;"><?php echo $comment_guademic;?></textarea>
							</td>
						</tr>
						<tr>
							<td>
								<div style="padding-left:0;">
									<p>Please return the book to school before:</p>
									<p>Date:&nbsp;&nbsp;&nbsp;<span style="border-bottom: 0px dotted black;font-size: 12px;"><?php echo $date_return; ?></span></p>
								</div>
							</td>
							<td>
								<div>
									<center>
										<p>Date:&nbsp;&nbsp;&nbsp;<span style="border-bottom: 0px dotted black;font-size: 12px;"><?php echo $date_guardian; ?></span></p>
										<p>Guardian's Signature</p>
									</center>
								</div>
							</td>
						</tr>
					</table>	
			</div>
		</center>	
	</div> <!-- div pppp -->
	
<div style="page-break-after: always;"></div>
<!-- </div> end div main -->
<script type="text/javascript">
	$(function(){
		$("#a_print").on("click", function () {
        	gPrint("show_print");
	    });

	});
	function chdisable() {
	   return false;
	}
	
</script>

