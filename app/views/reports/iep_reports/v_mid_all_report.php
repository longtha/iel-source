  <div style="border:0px solid #f00;margin-bottom: 20px;">
  		<div>
			   <div style="padding-top:0px;" id="titles">
			   		<div style="width:200px !important">
			   			<img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="width:200px !important">
			   		</div>
			   		<div class="header">
			   			<h4 style="font-family: 'Khmer OS Muol light' !important;margin-top: 5px !important;">របាយការណ៍សិក្សាពាក់កណ្ដាលត្រីមាស</h4>		
			   			<h4 style="font-family: 'Khmer OS Muol light' !important;margin-top: 5px !important;">ចំណេះទូទៅភាសាអង់គ្លេស</h4>		   			
			   			<h4 style="font-size:20px;">STUDENT'S IEP Mid-TERM REPORT</h4>
			   			<!-- <h4 style="font-size:18px;"><?php echo isset($row_stu->term) ? $row_stu->term:"";?></h4> -->	
			   		</div>  			   		
			   	</div>
			   	<table border="0" style=" margin-bottom: 0px !important;">
                   	<tbody>
                   		<tr>
                   			<td style="width:15%;">First name&nbsp;:</td>
                   			<td style="width:30%;font-size: 15px !important;font-weight: bold !important;"><b><?php echo $firstname;?></b></td>
                   			<td style="width:15%;"></td>
                   			<td style="width:15%;">Last name&nbsp;:</td>
                   			<td style="width:30%;font-size: 15px !important;font-weight: bold !important;"><b><?php echo $lastname;?></b></td>
                   		</tr>
                   		<tr>
               				<td>Student's ID&nbsp;:</td>
               				<td><?php echo $studentnumber;?></td>
               				<td></td>
               				<td>Sex&nbsp;:</td>
               				<td><?php echo ucwords($gender);?></td>
                   		</tr>
                   		<tr>
               				<td>Grade&nbsp;:</td>
               				<td><?php echo $classname;?></td>
               				<td></td>
               				<td>Time&nbsp;:</td>
               				<td></td>
                   		</tr>
                   	
                   	</tbody>
                </table>
				<div style="text-align:center; padding-bottom:15px">
					<!-- <p style="font-size:13px;">Score and Evaluation of Each Subject / ពិន្ទុ និងការវាយតម្លៃមុខវិជ្ជានីមួយៗ</p> -->
				</div>
				<table style="width:100%;">				
					<tr style="vertical-align: top;">
						<td>
		                   	<table style="width:100%;" border="1">
		                   	 	<thead>
		                   	 		<tr style="background-color: #cdd6dd !important;line-height: 25px !important;">
		                   	 			<th id="head" colspan="2"><i>Subject</i></th>
		                   	 			<th><i>Score</i>​</th>
		                   	 			<th><i>Meaning</i></th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody>
		                   	 	<?php
		                   	 			
		                   	 			$sql_score = $this->db->query("SELECT
																			ord.type,
																			ord.typeno,
																			ord.main_tranno,
																			det.studentid,
																			si.`subject`,
																			det.subjectid,
																			si.subj_type_id,
																			si.max_score,
																			det.score_num,
																			det.group_subject,
																			ord.exam_typeid,
																			ord.weekid,
																			sch_subject_type_iep.is_class_participation
																			FROM
																			sch_score_iep_entryed_order AS ord
																			INNER JOIN sch_score_iep_entryed_detail AS det ON ord.typeno = det.typeno AND ord.type = det.type
																			INNER JOIN sch_subject_iep AS si ON det.subjectid = si.subjectid
																			INNER JOIN sch_subject_type_iep ON ord.group_subject = sch_subject_type_iep.subj_type_id
																			WHERE 1=1 
																			AND ord.termid='".$termid."' 
																			AND ord.classid ='".$classid."'
																			AND ord.schoolid = '".$schoolid."'
																			AND ord.schlevelid ='".$schlavelid."'
																			AND ord.gradelevelid = '".$grandlavelid."'
																			AND ord.yearid = '".$yearid."'
																			-- AND det.studentid='{$studentid}' 
																			AND ord.exam_typeid = '".$examtype."'");

		                   	 			
		                   	 			$arr_score    = array();
		                   	 			$arr_sub_exam = array();
		                   	 			$max_score_sub= array();

		                   	 			$max_score_att = array();

		                   	 			// ------
		                   	 			$score_att = array();
		                   	 			$score_cp  = array();
		                   	 			$max_att   = array();
		                   	 			$max_att_h   = array();
										$max_score_hw = array();
										$count_w   = array();
		                   	 			if($sql_score->num_rows() > 0)
		                   	 			{
		                   	 				foreach($sql_score->result() as $row_sc){ 
						                   	 	if($row_sc->is_class_participation == 2){ // homework
						                   	 		
													if(isset($arr_score[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid])){
			                   	 						$arr_score[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $arr_score[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]+$row_sc->score_num;
			                   	 					}else{
			                   	 						$arr_score[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $row_sc->score_num;
			                   	 					}
			                   	 					// sum max score subject
			                   	 					if(isset($max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid])){
			                   	 						$max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]+$row_sc->max_score;
			                   	 					}else{
			                   	 						$max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $row_sc->max_score;
			                   	 					}
			                   	 					// compare max score subject
			                   	 					
			                   	 					if(isset($max_score_hw[$row_sc->group_subject][$row_sc->weekid])){
			                   	 						if($max_score_hw[$row_sc->group_subject][$row_sc->weekid] < $max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]){
			                   	 							$max_score_hw[$row_sc->group_subject][$row_sc->weekid] = $max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid];
			                   	 						}
			                   	 					}else{
			                   	 						$max_score_hw[$row_sc->group_subject][$row_sc->weekid] = $row_sc->max_score;
			                   	 					}

						                   	 	}else if($row_sc->is_class_participation == 4){ // deligen
													if(isset($arr_score[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid])){
			                   	 						$arr_score[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $arr_score[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]+$row_sc->score_num;
			                   	 					}else{
			                   	 						$arr_score[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $row_sc->score_num;
			                   	 					}
			                   	 					// sum max score subject
			                   	 					if(isset($max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid])){
			                   	 						$max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]+$row_sc->max_score;
			                   	 					}else{
			                   	 						$max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid] = $row_sc->max_score;
			                   	 					}
			                   	 					// compare max score subject
			                   	 					if(isset($max_score_hw[$row_sc->group_subject][$row_sc->weekid])){
			                   	 						if($max_score_hw[$row_sc->group_subject][$row_sc->weekid] < $max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid]){
			                   	 							$max_score_hw[$row_sc->group_subject][$row_sc->weekid] = $max_att_h[$row_sc->studentid][$row_sc->group_subject][$row_sc->weekid];
			                   	 						}
			                   	 					}else{
			                   	 						$max_score_hw[$row_sc->group_subject][$row_sc->weekid] = $row_sc->max_score;
			                   	 					}
			                   	 				}
			                   	 				else if($row_sc->is_class_participation == 1){ // class participation
													if(isset($score_cp[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid])){
			                   	 						$score_cp[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $score_cp[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]+$row_sc->score_num;
			                   	 					}else{
			                   	 						$score_cp[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $row_sc->score_num;
			                   	 					}
			                   	 					// sum max score subject
			                   	 					if(isset($max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid])){
			                   	 						$max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]+$row_sc->max_score;
			                   	 					}else{
			                   	 						$max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $row_sc->max_score;
			                   	 					}
			                   	 					// compare max score subject
			                   	 					if(isset($max_score_att[$row_sc->group_subject])){
			                   	 						if($max_score_att[$row_sc->group_subject] < $max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]){
			                   	 							$max_score_att[$row_sc->group_subject] = $max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid];
			                   	 						}
			                   	 					}else{
			                   	 						$max_score_att[$row_sc->group_subject] = $row_sc->max_score;
			                   	 					}
			                   	 				}else if($row_sc->is_class_participation == 3){ // attendance
			                   	 					if(isset($score_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid])){
			                   	 						$score_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $score_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]+$row_sc->score_num;
			                   	 					}else{
			                   	 						$score_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $row_sc->score_num;
			                   	 					}
			                   	 					// sum max score subject
			                   	 					if(isset($max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid])){
			                   	 						$max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]+$row_sc->max_score;
			                   	 					}else{
			                   	 						$max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $row_sc->max_score;
			                   	 					}
			                   	 					// compare max score subject
			                   	 					if(isset($max_score_att[$row_sc->group_subject])){
			                   	 						if($max_score_att[$row_sc->group_subject] < $max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]){
			                   	 							$max_score_att[$row_sc->group_subject] = $max_att[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid];
			                   	 						}
			                   	 					}else{
			                   	 						$max_score_att[$row_sc->group_subject] = $row_sc->max_score;
			                   	 					}
			                   	 				}else{ // subject example
			                   	 					if(isset($arr_sub_exam[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid])){
														$arr_sub_exam[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $arr_sub_exam[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid]+$row_sc->score_num;
			                   	 					}else{
			                   	 						$arr_sub_exam[$row_sc->studentid][$row_sc->group_subject][$row_sc->subjectid] = $row_sc->score_num;
			                   	 					}
			                   	 					
			                   	 				} 
		                   	 				}
		                   	 			}
		       
		                   	 			$sql_score_mention = $this->db->query("SELECT
																					sm.submenid,
																					sm.schoolid,
																					sm.schlevelid,
																					sm.subjectid,
																					sm.gradelevelid,
																					sm.mentionid,
																					sm.max_score,
																					sm.minscrore,
																					cm.mention
																			FROM
																			sch_subject_mention AS sm
																			LEFT JOIN sch_score_mention AS cm ON sm.mentionid = cm.menid
																			WHERE 1=1 
																			AND sm.schoolid='".$schoolid."'
																			AND sm.schlevelid='".$schlavelid."'
																			AND sm.gradelevelid='".$grandlavelid."'
																			");
		                   	 			$arr_subj_ment = array();
		                   	 			if($sql_score_mention->num_rows() > 0){
		                   	 				foreach($sql_score_mention->result() as $row_m){
		                   	 					$arr_subj_ment[$row_m->subjectid][] = array('Max_score'=>$row_m->max_score,'Min_score'=>$row_m->minscrore,'meaning'=>$row_m->mention);
		                   	 				}
		                   	 			}
		                  
		                   	 			$sql_group = $this->db->query("SELECT
																			sch_subject_type_iep.subj_type_id,
																			sch_subject_type_iep.subject_type,
																			sch_subject_type_iep.main_type,
																			sch_subject_type_iep.schlevelid,
																			sch_subject_type_iep.schoolid,
																			sch_subject_type_iep.is_class_participation,
																			sch_subject_type_iep.type_subject,
																			sch_subject_type_iep.is_group_calc_percent
																			FROM
																			sch_subject_type_iep
																			WHERE schoolid='".$schoolid."' AND schlevelid='".$schlavelid."'
																			ORDER BY sch_subject_type_iep.`subject_type`
																			");
		                   	 			$tr_att = "";
		                   	 			$tr_cp = "";
		                   	 			$tr_hw = "";
		                   	 			$tr_dl = "";
		                   	 			$tr_sub = "";
		                   	 			$tr_mscien = "";
		                   	 			$sum_assement = 0;
		                   	 			$sum_subject  = 0;
		                   	 			if($sql_group->num_rows() > 0)
		                   	 			{
		                   	 				foreach($sql_group->result() as $row_group)
		                   	 				{
		                   	 					if($row_group->is_class_participation == 3) // attendance
		                   	 					{
		                   	 						$score        = 0;
		                   	 						$max_scor_att = 1;
		                   	 						$max_score_group = $row_group->is_group_calc_percent;
		                   	 						if(isset($score_att[$studentid][$row_group->subj_type_id])){
		                   	 							foreach($score_att[$studentid][$row_group->subj_type_id] as $v_att){
		                   	 								$score+=$v_att;
		                   	 							}
		                   	 						}
		                   	 						if(isset($max_score_att[$row_group->subj_type_id])){
		                   	 							$max_scor_att = $max_score_att[$row_group->subj_type_id];
		                   	 						}
		                   	 						$result_att = floor((($score*$max_score_group)/$max_scor_att)*100)/100;
		                   	 						$mean = "";
		                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
                   	 										if($subj_score['Min_score'] <= $result_att && $subj_score['Max_score'] >= $result_att){
                   	 											$mean = $subj_score['meaning'];
                   	 										}
                   	 									}
                   	 								}
                   	 								
                   	 								$sum_assement+= $result_att;
		                   	 						$tr_att.="<tr style='line-height: 10px !important;'><td>".$row_group->subject_type."</td><td id='scor'>".$result_att."</td><td style='text-align:center;'>".$mean."</td></tr>";
		                   	 						
		                   	 					}
		                   	 					else if($row_group->is_class_participation == 1) // class participation
		                   	 					{
		                   	 						$score = 0;
		                   	 						$max_scor_cp = 1;
		                   	 						$max_score_group = $row_group->is_group_calc_percent;
		                   	 						if(isset($score_cp[$studentid][$row_group->subj_type_id])){
		                   	 							foreach($score_cp[$studentid][$row_group->subj_type_id] as $v_att){
		                   	 								$score+=$v_att;
		                   	 							}
		                   	 							//$score  = $score_cp[$row_group->subj_type_id];
		                   	 						}	
	                   	 							
		                   	 						if(isset($max_score_att[$row_group->subj_type_id])){
		                   	 							$max_scor_cp = $max_score_att[$row_group->subj_type_id];
		                   	 						}
		                   	 						$result_cp = floor((($score*$max_score_group)/$max_scor_cp)*100)/100;
		                   	 						
		                   	 						$mean = "";
		                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
                   	 										if($subj_score['Min_score'] <= $result_cp && $subj_score['Max_score'] >= $result_cp){
                   	 											$mean= $subj_score['meaning'];
                   	 										}
                   	 									}
                   	 								}
                   	 								$sum_assement+=$result_cp;
													$tr_cp.="<tr style='line-height: 10px !important;'><td>".$row_group->subject_type."</td><td id='scor'>".$result_cp."</td><td style='text-align:center;'>".$mean."</td></tr>";
		                   	 					
		                   	 					}
		                   	 					else if($row_group->is_class_participation == 2) // homework
		                   	 					{
		                   	 						$max_group = $row_group->is_group_calc_percent;
		                   	 						$score = 0;
		                   	 						if(isset($arr_score[$studentid][$row_group->subj_type_id]))
		                   	 						{
		                   	 							foreach($arr_score[$studentid][$row_group->subj_type_id] as $w=>$score_sub){
		                   	 								$m_subj2 = 0;
				                    	 					if(isset($max_score_hw[$row_group->subj_type_id][$w])){
				                    	 						$m_subj2 += $max_score_hw[$row_group->subj_type_id][$w];
		                   	 								}
		                   	 								$sum_w = floor((($score_sub*$max_group)/$m_subj2)*100)/100;
															$score+= $sum_w;
		                   	 							}
		                   	 							$ii= (count($max_score_hw[$row_group->subj_type_id]) >0?count($max_score_hw[$row_group->subj_type_id]):1);
		                   	 							$score = floor(($score/($ii==0?1:$ii))*100)/100;
		             
		                    	 					}
		                    	 					
		                   	 						$mean = "";
		                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
                   	 										if($subj_score['Min_score'] <= $score && $subj_score['Max_score'] >= $score){
                   	 											$mean= $subj_score['meaning'];
                   	 										}
                   	 									}
                   	 								}
                   	 								$sum_assement+=$score;
													$tr_hw.="<tr style='line-height: 10px !important;'><td>".$row_group->subject_type."</td><td id='scor'>".$score."</td><td style='text-align:center;'>".$mean."</td></tr>";
		                   	 						
		                   	 					}
		                   	 					else if($row_group->is_class_participation == 4) // deligen
		                   	 					{
		                   	 						$max_group = $row_group->is_group_calc_percent;
		                   	 						$score = 0;
		                   	 						if(isset($arr_score[$studentid][$row_group->subj_type_id]))
		                   	 						{
		                   	 							foreach($arr_score[$studentid][$row_group->subj_type_id] as $w=>$score_sub){
		                   	 								$m_subj1 =0;
				                    	 					if(isset($max_score_hw[$row_group->subj_type_id][$w])){
		                   	 									$m_subj1 += $max_score_hw[$row_group->subj_type_id][$w];
		                   	 								}
															$sum_w = floor((($score_sub*$max_group)/$m_subj1)*100)/100;
		                   	 								$score+= $sum_w;
		                   	 							}
		                   	 							$ii= (count($max_score_hw[$row_group->subj_type_id]) >0?count($max_score_hw[$row_group->subj_type_id]):1);
		                   	 							$score = floor(($score/($ii==0?1:$ii))*100)/100;
		             
		                    	 					}
		                    	 					
		                   	 						$mean = "";
		                   	 						if(isset($arr_subj_ment[$row_group->subj_type_id])){ // Max_score Min_score  meaning
                   	 									foreach($arr_subj_ment[$row_group->subj_type_id] as $subj_score){
                   	 										if($subj_score['Min_score'] <= $score && $subj_score['Max_score'] >= $score){
                   	 											$mean= $subj_score['meaning'];
                   	 										}
                   	 									}
                   	 								}
                   	 								$sum_assement+=$score;
													$tr_dl.="<tr style='line-height: 10px !important;'><td>".$row_group->subject_type."</td><td id='scor'>".$score."</td><td style='text-align:center;'>".$mean."</td></tr>";
		                   	 					
		                   	 					}
		                   	 					else if($row_group->is_class_participation == 0 and $row_group->type_subject==1)
		                   	 					{           	 							
		                   	 							$sql_subject = $this->db->query("SELECT
																						si.subjectid,
																						si.`subject`,
																						si.subj_type_id,
																						si.short_sub,
																						si.max_score,
																						si.is_assessment
																						FROM
																						sch_subject_iep AS si
																						WHERE si.subj_type_id='".$row_group->subj_type_id."'
																						");
		                   	 							
		                   	 							if($sql_subject->num_rows() > 0){
		                   	 								$score_m = 0;
		                   	 								$n = 0;
		                   	 								foreach($sql_subject->result() as $row_s){
		                   	 									if(isset($arr_sub_exam[$studentid][$row_group->subj_type_id][$row_s->subjectid])){
						                   	 						$score_m = $arr_sub_exam[$studentid][$row_group->subj_type_id][$row_s->subjectid];
						                   	 					}
						                   	 					$mean = "";
						                   	 					if(isset($arr_subj_ment[$row_s->subjectid])){ // Max_score Min_score  meaning
			                   	 									foreach($arr_subj_ment[$row_s->subjectid] as $subj_score){
			                   	 										if($subj_score['Min_score'] <= $score_m && $subj_score['Max_score'] >= $score_m){
			                   	 											$mean= $subj_score['meaning'];
			                   	 										}
			                   	 									}
			                   	 								}
			                   	 								if($n == 0){
																	$tr_sub.="<tr style='line-height: 10px !important;'><td rowspan='6' style='vertical-align:middle;'>English</td><td>".$row_s->subject."</td><td id='scor'>".$score_m."</td><td style='text-align:center;'>".$mean."</td></tr>"; 
			                   	 								}else{
			                   	 									$tr_sub.="<tr style='line-height: 10px !important;'><td>".$row_s->subject."</td><td id='scor'>".$score_m."</td><td style='text-align:center;'>".$mean."</td></tr>"; 
			                   	 								}
						                   	 					$n++;
						                   	 					$sum_subject+=$score_m;
		                   	 								}
		                   	 							}
		                   	 					}
		                   	 					else if($row_group->is_class_participation == 0 and $row_group->type_subject==2)
		                   	 					{
		                   	 							$sql_subject = $this->db->query("SELECT
																						si.subjectid,
																						si.`subject`,
																						si.subj_type_id,
																						si.short_sub,
																						si.max_score,
																						si.is_assessment
																						FROM
																						sch_subject_iep AS si
																						WHERE si.subj_type_id='".$row_group->subj_type_id."'
																						");
		                   	 							
		                   	 							if($sql_subject->num_rows() > 0){
		                   	 								$score_m = 0;
		                   	 								foreach($sql_subject->result() as $row_s){
		                   	 									if(isset($arr_sub_exam[$studentid][$row_group->subj_type_id][$row_s->subjectid])){
						                   	 						$score_m = $arr_sub_exam[$studentid][$row_group->subj_type_id][$row_s->subjectid];
						                   	 					}
						                   	 					$mean = "";
						                   	 					if(isset($arr_subj_ment[$row_s->subjectid])){ // Max_score Min_score  meaning
			                   	 									foreach($arr_subj_ment[$row_s->subjectid] as $subj_score){
			                   	 										if($subj_score['Min_score'] <= $score_m && $subj_score['Max_score'] >= $score_m){
			                   	 											$mean= $subj_score['meaning'];
			                   	 										}
			                   	 									}
			                   	 								}
			                   	 								$sum_subject+=$score_m;
						                   	 					$tr_mscien.="<tr style='line-height: 10px !important;'><td colspan='2'>".$row_s->subject."</td><td id='scor'>".$score_m."</td><td style='text-align:center;'>".$mean."</td></tr>"; 
		                   	 								}
		                   	 							}
		                   	 					}
		                   	 				}
		                   	 			}
		                   	 			$total_result = floor((($sum_subject+$sum_assement)/2)*100)/100;
		                   	 			echo $tr_mscien.$tr_sub;
		                   	 			echo "<tr style='line-height: 10px !important;'><td colspan='2' style='text-align:center;'><b>Total Score</b></td><td style='text-align:center;'><b>".$sum_subject."</b></td></td></td></tr>";		        
								?> 
		                   	 	</tbody>
		                   	</table>
						</td>
						<td>		
		                   	<table style="width:100%;" border="1">
		                   	 	<thead>
		                   	 		<tr style="background-color: #cdd6dd !important;line-height: 25px !important;">
		                   	 			<th id="head"><i>Core On-goring assessment</i></th>
		                   	 			<th id="scor"><i>Score​</i></th>
		                   	 			<th id="scor"><i>Meaning</i></th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody>
		                   	 		<?php echo $tr_att.$tr_cp.$tr_hw.$tr_dl;?>
		                   	 		<tr style='line-height: 10px !important;'><td style="text-align: center;"><b>Total Score</b></td><td style='text-align:center;'><b><?php echo $sum_assement;?></b></td><td></td></tr>
		                   	 	</tbody>
		                   </table>
				        </td>
					</tr>
				</table>
					<?php 
						
						$sql_grade = $this->db->query("SELECT
														sch_score_mention_gep.menid,
														sch_score_mention_gep.mention,
														sch_score_mention_gep.schlevelid,
														sch_score_mention_gep.grade,
														sch_score_mention_gep.is_past,
														sch_score_mention_gep.min_score,
														sch_score_mention_gep.max_score
														FROM
														sch_score_mention_gep
														WHERE schlevelid='".$schlavelid."'
														ORDER BY min_score ASC");
						$tr_g = "";
						$grade_st  = "";
						$past_fail = "";
						if($sql_grade->num_rows() > 0){
							foreach($sql_grade->result() as $row_g){
								if($row_g->min_score <= $total_result AND $row_g->max_score >= $total_result){
									$grade_st  = $row_g->grade;
									if($row_g->is_past == 1){
										$past_fail = "Past";
									}else{
										$past_fail = "Fail";
									}
								}
								if($row_g->max_score <1){
									$tr_g.="<tr style='line-height: 10px !important;'>
												<td>0</td>
												<td style='text-align:center;'>".$row_g->grade."</td>
												<td>".$row_g->mention."</td>
											</tr>";
								}else{
									$tr_g.="<tr style='line-height: 10px !important;'>
												<td>".$row_g->min_score."-".$row_g->max_score."</td>
												<td style='text-align:center;'>".$row_g->grade."</td>
												<td>".$row_g->mention."</td>
											</tr>";
								}
								
							}
						}
						//============================== rank ==============================
						$sql_rank_mid = $this->db->query("SELECT
													sch_iep_total_score_entry.rank
													FROM
													sch_iep_total_score_entry
													WHERE 1=1 
													AND schoolid = '".$schoolid."'
													AND schlevelid = '".$schlavelid."'
													AND yearid = '".$yearid."'
													AND termid = '".$termid."'
													AND classid = '".$classid."'
													AND examtypeid = '".$examtype."'
													AND gradeid   = '".$grandlavelid."'
													AND studentid = '".$studentid."'
													ORDER BY examtypeid,total_score DESC")->row();
						$rank_mid = "";
						if(isset($sql_rank_mid->rank)){
							$rank_mid = $sql_rank_mid->rank;
						}else{
							$sql_rank_mid1 = $this->db->query("SELECT
															MAX(rank) as max_rank
															FROM
															sch_iep_total_score_entry
															WHERE 1=1 
															AND schoolid = '".$schoolid."'
															AND schlevelid = '".$schlavelid."'
															AND yearid = '".$yearid."'
															AND termid = '".$termid."'
															AND classid = '".$classid."'
															AND examtypeid = '".$examtype."'
															AND gradeid   = '".$grandlavelid."'
															-- AND studentid = '".$studentid."'
															ORDER BY examtypeid,total_score DESC")->row();
							$rank_mid = ($sql_rank_mid1->max_rank+1)-0;
						}
					?>
					<table border="1" style="width:99.2%;margin:4px;">
						<thead>
							<tr style="background-color: #cdd6dd !important;line-height: 25px !important;">
								<th>Rank</th><th>Grade</th><th>Grand Total Score</th><th>Result</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="text-align: center;"><b><?php echo $rank_mid;?></b></td>
								<td style="text-align: center;"><b><?php echo $grade_st;?></b></td>
								<td style="text-align: center;"><b><?php echo $total_result;?></b></td>
								<td style="text-align: center;"><b><?php echo $past_fail;?></b></td>
							</tr>
						</tbody>
					</table>
					<table style="padding-left: 0px;">
						<tr style="vertical-align: top;">
							<td style="width:40%">
								<table style="width:100%;" border="1">
									<tr style="background-color: #cdd6dd !important;line-height: 25px !important;">
										<th colspan="3">Grading System</th>
									</tr>
									<tr>
										<th>Score</th><th>Grade</th><th>Meaning</th>
									</tr>
									<?php
										echo $tr_g;
									?>
								</table>
							</td>
							<?php 
								$show_comment = $this->db->query("SELECT 
																sch_studend_command_iep.command_teacher,
																sch_studend_command_iep.command_guardian,
																sch_studend_command_iep.examp_type,
																sch_studend_command_iep.academicid,
																sch_studend_command_iep.userid,
																sch_studend_command_iep.date_academic,
																sch_studend_command_iep.date_teacher,
																sch_studend_command_iep.date_return,
																sch_studend_command_iep.date_guardian,
																sch_studend_command_iep.date_create
																FROM
																sch_studend_command_iep
																WHERE 1=1
																AND student_id='".$studentid."'
														        AND schoolid  = '".$schoolid."'
														        AND schlevelid='".$schlavelid."'
														        AND yearid    ='".$yearid."'
														        AND gradelevelid='".$grandlavelid."'
														        AND classid   = '".$classid."'
														        AND termid    = '".$termid."'
														        AND examp_type='".$examtype."'
														        ")->row();
								$acedamic  = isset($show_comment->academicid)?$show_comment->academicid:"";
								$teacherid = isset($show_comment->userid)?$show_comment->userid:"";
								$sql_user_acad = "";
								$sql_user_id= "";
								if($acedamic != ""){
									$sql_user_acad = $this->db->query("SELECT
								                                  sch_user.user_name
								                                  FROM
								                                  sch_user
								                                  WHERE match_con_posid='acca' AND userid='".$acedamic."'")->row()->user_name;
								}
								if($teacherid != ""){
									$sql_user_id = $this->db->query("SELECT
								                                  sch_user.user_name
								                                  FROM
								                                  sch_user
								                                  WHERE userid='".$teacherid."'")->row()->user_name;
								}
								
							?>
							<td style="width:60%">
								<table style="width:100%;" border="1">
									<tr style="background-color: #cdd6dd !important;line-height: 25px !important;">
										<th colspan="3">Teacher's Comment</th>
									</tr>
									<tr>
										<td style="height: 150px; vertical-align: top;">
											<?php echo isset($show_comment->command_teacher)?$show_comment->command_teacher:""; ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					<table style="width:99.2%;margin:4px;">
						<tr style="vertical-align: top;height:70px;">
							<td style="">
								<div style="float: left;width:200px;border:0px solid black;">
									<p style="float: left;">Date:</p>
									<p style="float: left;width:150px;border-bottom: 1px dotted black;text-align:center;"><?php echo isset($show_comment->date_academic)?($this->green->formatSQLDate($show_comment->date_academic)):"";?>&nbsp;</p>
									<p style="float: left;width:100%;text-align: center;">Academic Manager</p>
								</div>
							</td>
							<td style="text-align: right;">
								<div style="float: right;width:200px;border:0px solid black;">
									<p style="float: left;">Date:</p>
									<p style="float: left;width:150px;border-bottom: 1px dotted black;text-align:center;"><?php echo isset($show_comment->date_teacher)?($this->green->formatSQLDate($show_comment->date_teacher)):"";?>&nbsp;</p>
									<p style="float: left;width:100%;text-align: center;">Teacher's Signature</p>
								</div>
							</td>
						</tr>
						<tr>
							<td><p style="text-align: left;margin-left: 80px;"><b><?php echo strtoupper($sql_user_acad);?></b></p></td>
							<td><p style="text-align: right;margin-right: 80px;"><b><?php echo strtoupper($sql_user_id);?></b></p></td>
						</tr>
						<tr>
							<td colspan="2" style="height: 100px;vertical-align: top;border:1px solid black;padding: 5px;"><?php echo isset($show_comment->command_guardian)?$show_comment->command_guardian:""; ?></td>
						</tr>
					</table>
					<table style="width:99.2%;margin:4px;">
						<tr>
							<td style="text-align:left;">
								<p>Please return the book to school before</p>					
								<span style="text-align: center;border-bottom: 1px dotted black;width:200px;float:left;"><?php echo isset($show_comment->date_return)?($this->green->formatSQLDate($show_comment->date_return)):"";?>&nbsp;</span>
							</td>
							<td style="text-align:right;">
								<div style="float: right;width:200px;border:0px solid black;">
									<p style="float: left;">Date:</p>
									<p style="float: left;width:150px;border-bottom: 1px dotted black;text-align:center;"><?php echo isset($show_comment->date_guardian)?($this->green->formatSQLDate($show_comment->date_guardian)):"";?>&nbsp;</p>
									<p style="float: left;width:100%;text-align: center;">Guardian's Signature</p>
								</div>
							</td>
						</tr>
					</table>
		</div>
</div>
<div style="page-break-after: always;"></div>

<!-- <script type="text/javascript">
	$(function(){
		$("#a_print").on("click", function () {
        	gPrint("tap_print");
	    });

	    $("#date").datepicker({
	        onSelect: function(date) {
	          $(this).val(formatDate(date));
	          getdata($(this).val());
	        } 
	    });
	});

	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();
	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;
	    return [year, month, day].join('-');
    }
</script> -->