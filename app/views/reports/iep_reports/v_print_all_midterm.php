<div class="container-fluid" style="width:100% !important;padding-bottom: 20px;">
    <div class="row">
      <div class="col-xs-12">
          <div class="result_info">
              <div class="col-xs-6">
                  <strong>Mid-term Report</strong>  
              </div>
              <div class="col-xs-6" style="text-align: right">
                  <a href="javascript:void(0);" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                     <img src="<?= base_url('assets/images/icons/print.png') ?>">
                  </a>
              </div>         
          </div>
      </div>
    </div>
</div>
<div id="tap_print" style="border:0px solid blue;width:97%;float: right;">
<?php 
$row_stu1 = $this->db->query("SELECT
								vse.student_num,
								vse.first_name,
								vse.last_name,
								vse.first_name_kh,
								vse.last_name_kh,
								vse.gender,
								vse.class_name,
								vse.studentid,
								vse.schoolid,
								vse.`year`,
								vse.classid,
								vse.schlevelid,
								vse.rangelevelid,
								vse.feetypeid,
								vse.programid,
								vse.sch_level,
								vse.rangelevelname,
								vse.program,
								vse.sch_year,
								vse.grade_levelid,
								tt.term,
								tt.termid,
								gl.grade_level,
								YEAR(vse.from_date) AS fyear,
								YEAR(vse.to_date) AS tyear
								FROM
								v_student_enroment AS vse
								INNER JOIN sch_school_term AS tt ON tt.yearid = vse.`year`
								INNER JOIN sch_grade_level AS gl ON gl.grade_levelid = vse.`grade_levelid`
								WHERE 1=1
								AND vse.schoolid='".$schoolid."'
								AND vse.programid = '".$programeid."'
								AND vse.classid = '".$classid."' 
								AND tt.termid = '".$termid."'
								AND vse.schlevelid ='".$schlavelid."'
								AND vse.grade_levelid ='".$grandlavelid."'
								AND tt.yearid ='".$yearid."'
								ORDER BY vse.studentid
								LIMIT {$numrow},{$page}
							");
if($page > 0){
	if($row_stu1->num_rows() > 0){
		foreach($row_stu1->result() as $row_st){
			$data = array("studentid"=>$row_st->studentid,
						"programeid"=>$programeid,
						"classid"=>$classid,
						"classname"=>$row_st->class_name,
						"termid"=>$termid,
						"schlavelid"=>$schlavelid,
						"schoolid"=>$schoolid,
						"grandlavelid"=>$grandlavelid,
						"yearid"=>$yearid,
						"examtype"=>$examtype,
						"page"=>$page,
						"firstname"=>$row_st->first_name,
						"lastname"=>$row_st->last_name,
						"gender"=>$row_st->gender,
						"studentnumber"=>$row_st->student_num
						);
			$this->load->view('reports/iep_reports/v_mid_all_report',$data);
		}
	}
}
?>
</div>
<script type="text/javascript">
	$(function(){
		$("#a_print").on("click", function () {
        	gPrint("tap_print");
	    });

	    // $("#date").datepicker({
	    //     onSelect: function(date) {
	    //       $(this).val(formatDate(date));
	    //       getdata($(this).val());
	    //     } 
	    // });
	});

	// function formatDate(date) {
	//     var d = new Date(date),
	//         month = '' + (d.getMonth() + 1),
	//         day = '' + d.getDate(),
	//         year = d.getFullYear();
	//     if (month.length < 2) month = '0' + month;
	//     if (day.length < 2) day = '0' + day;
	//     return [year, month, day].join('-');
 //    }
</script>