<div class="row">
	<div class="col-xs-12">
		<div class="result_info">
		  	<div class="col-xs-6">
			    <span class="icon">
			        <i class="fa fa-th"></i>
			    </span>
		      	<strong>Semester IEP Report</strong>  
		  	</div>
		  	<div class="col-xs-6" style="text-align: right">
		      	<a href="javascript:void(0);" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
		         	<img src="<?= base_url('assets/images/icons/print.png') ?>">
		      	</a>
		  	</div>         
		</div>
	</div>
</div>
<div  id="show_print">
<?php

	$row_stu = $this->db->query("SELECT
									vse.student_num,
									vse.first_name,
									vse.last_name,
									vse.first_name_kh,
									vse.last_name_kh,
									vse.gender,
									vse.class_name,
									vse.studentid,
									vse.schoolid,
									vse.`year`,
									vse.classid,
									vse.schlevelid,
									vse.rangelevelid,
									vse.feetypeid,
									vse.programid,
									vse.sch_level,
									vse.rangelevelname,
									vse.program,
									vse.sch_year,
									vse.grade_levelid,
									tt.term,
									tt.termid,
									gl.grade_level,
									YEAR(vse.from_date) AS fyear,
									YEAR(vse.to_date) AS tyear
									FROM
									v_student_enroment AS vse
									INNER JOIN sch_school_term AS tt ON tt.yearid = vse.`year`
									INNER JOIN sch_grade_level AS gl ON gl.grade_levelid = vse.`grade_levelid`
									WHERE 1=1
									AND vse.schoolid='".$schoolid."'
									AND vse.programid = '".$programid."'
									AND vse.classid = '".$classid."' 
									AND vse.schlevelid ='".$schlid."'
									AND vse.grade_levelid ='".$grandlevel."'
									AND tt.yearid ='".$yearid."'
									AND tt.semesterid='".$semesterid."'
									GROUP BY vse.studentid
									LIMIT {$startpage},{$limitpage}
								");	

	if($row_stu->num_rows() > 0){
		foreach($row_stu->result() as $rows){
			$title_semester = $this->db->query("SELECT
												sch_school_semester.semester
												FROM
												sch_school_semester
												WHERE 1=1 AND semesterid='".$semesterid."'")->row()->semester;
			$data = array("first_name"=>$rows->first_name,
							"last_name"=>$rows->last_name,
							"student_num"=>$rows->student_num,
							"gender"=>ucfirst($rows->gender),
							"class_name"=>$rows->class_name,
							"studentid"=>$rows->studentid,
							"semesterid"=>$semesterid,
							"title_semester"=>$title_semester,
							"schoolid"=>$schoolid,
							"programid"=>$programid,
							"classid"=>$classid,
							"schlavelid"=>$schlid,
							"grandlavelid"=>$grandlevel,
							"yearid"=>$yearid
						);
			$this->load->view("reports/iep_reports/v_report_semester_print_all",$data);
		}
	}
?>
</div>
<script type="text/javascript">
	$(function(){
		$("#a_print").on("click", function () {
        	gPrint("show_print");
	    });

	});
</script>
