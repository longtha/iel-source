<div class="container-fluid" style="width:100% !important;padding-bottom: 20px;">
    <div class="row">
      <div class="col-xs-12">
          <div class="result_info">
              <div class="col-xs-6">
                  <strong>Annual Academic Report</strong>  
              </div>
              <div class="col-xs-6" style="text-align: right">
                  <a href="javascript:void(0);" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                     <img src="<?= base_url('assets/images/icons/print.png') ?>">
                  </a>
              </div>         
          </div>
      </div>
    </div>
</div>
<div id="tap_print" style="border:0px solid blue;width:97%;float: right;">
<?php 
$schoolid  = isset($_GET['schid'])?$_GET['schid']:"";
$sch_level = isset($_GET['schlavel'])?$_GET['schlavel']:"";
$gradlevel = isset($_GET['gradlavel'])?$_GET['gradlavel']:"";
$classid   = isset($_GET['classid'])?$_GET['classid']:"";
$yearid    = isset($_GET['yearid'])?$_GET['yearid']:"";
$sql = $this->db->query("SELECT
										vp.student_num,
										vp.first_name,
										vp.last_name,
										CONCAT(
											vp.last_name,
											' ',
											vp.first_name
										) AS fullname,
										vp.first_name_kh,
										vp.last_name_kh,
										CONCAT(
											vp.last_name_kh,
											' ',
											vp.first_name_kh
										) AS fullname_kh,
										vp.gender,
										vp.class_name,
										vp.studentid,
										vp.schoolid,
										vp.`year`,
										vp.classid,
										vp.schlevelid,
										vp.rangelevelid,
										vp.feetypeid,
										vp.programid,
										vp.sch_level,
										vp.rangelevelname,
										vp.program,
										vp.sch_year,
										vp.grade_levelid,
										g.grade_level,
										tt.yearid
									FROM
										v_student_enroment AS vp
									INNER JOIN sch_school_term AS tt ON tt.yearid = vp.`year`
									AND vp.schlevelid = tt.schlevelid
									INNER JOIN sch_grade_level AS g ON vp.grade_levelid = g.grade_levelid
									WHERE
										1 = 1
									AND vp.schoolid =".($schoolid==''?'NULL':$schoolid)."
									AND vp.schlevelid ='".$sch_level."'
									AND vp.grade_levelid ='".$gradlevel."' 
									AND vp.classid ='".$classid."' 
									AND vp.`year` = '".$yearid."'
									GROUP BY
										vp.studentid
									ORDER BY
										vp.studentid ASC");

	if($sql->num_rows() > 0){
		foreach($sql->result() as $row_st){
			$data = array("studentid"=>$row_st->studentid,
						"classid"=>$classid,
						"schlavelid"=>$sch_level,
						"schoolid"=>$schoolid,
						"grandlavelid"=>$gradlevel,
						"yearid"=>$yearid,
						"sch_level"=>$row_st->sch_level,
						"fullname_eng"=>$row_st->fullname,
						"fullname_kh"=>$row_st->fullname_kh
						);
			$this->load->view('reports/iep_reports/v_annual_academic_all_report.php',$data);
		}
	}

?>
</div>
<script type="text/javascript">
	$(function(){
		$("#a_print").on("click", function () {
        	gPrint("tap_print");
	    });

	    // $("#date").datepicker({
	    //     onSelect: function(date) {
	    //       $(this).val(formatDate(date));
	    //       getdata($(this).val());
	    //     } 
	    // });
	});

	// function formatDate(date) {
	//     var d = new Date(date),
	//         month = '' + (d.getMonth() + 1),
	//         day = '' + d.getDate(),
	//         year = d.getFullYear();
	//     if (month.length < 2) month = '0' + month;
	//     if (day.length < 2) day = '0' + day;
	//     return [year, month, day].join('-');
 //    }
</script>