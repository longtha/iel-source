
<div class="wrapper" style="border:0px solid #f00;overflow:auto">
	   <div class="col-sm-12"> 
		   	<div id="tap_print">
			<div class="col-sm-12 ti_semester">
		   		<div class="col-sm-4" style="width:200px !important">
		   			<img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="width:200px !important">
		   		</div>
		   		<div class="col-sm-6 dv_semester">
		   			<h4 class="kh_font">របាយការណ៍សិក្សាប្រចាំឆមាសមាស</h4>
		   			<h4 style="font-size:23px;">Semester Acadimic Report</h4>		   			
		   			<h4 style="font-size:23px;">Semester II</h4>	
		   		</div>  			   		
		   	</div>	 	
			<div class="col-sm-12" style="padding-left:40px; padding-right:40px;">			
		                <table class='table head_name' style=" margin-bottom: 0px !important;">
		                   	
		                   	<tbody>
		                   		<tr>
		                   			<td style="width:15%;">Student's Name:</td>
		                   			<td style="width:20%"><?php echo $student_name;?></td>
		                   			<td style="width:5%">Level:</td>
		                   			<td style="width:10%"><?php echo $gradlevel ?></td>
		                   			<td style="width:10%">Yearly:</td>
		                   			<td style="width:20%"><?php echo $year ?></td>
		                   		</tr>
		                   		<tr>
		                   			<td>សិស្សឈ្មោះ</td>
		                   			<td class="kh_font"><?php echo $student_namekh ?></td>
		                   			<td>កម្រិត:</td>
		                   			<td><?php echo $gradlevel ?></td>
		                   			<td>ឆ្នាំសិក្សា:</td>
		                   			<td><?php echo $year ?></td>
		                   		</tr>
		                   	</tbody>
		                </table>
			</div>	
			<div class="col-sm-12" style="text-align:center; padding-bottom:15px">
				<p style="font-size:13px;">Score and Evaluation of Each Subject / ពិន្ទុ និងការវាយតម្លៃមុខវិជ្ជានីមួយៗ</p>
			</div>
			<div class="col-sm-12">				
				<div class="col-sm-6" style="padding-left:0px;">
							<label>Assessment and English Exam</label>
		                   <table class='table table-bordered'>
		                   	 	<thead>
		                   	 		<tr>
		                   	 			<th id="head">Other Assessment</th>
		                   	 			<th>Score​ <br> ពិន្ទុ</th>
		                   	 			<th>Evaluation <br> ការវាយតម្លៃ</th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody>
		                   	 		<tr>
		                   	 			<td>Class-Participation / សកម្មភាពសិក្សា</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Home Work / កិច្ចការផ្ទះ</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Portfolio Diligence / រៀបចំស៊ីម៉ី</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Vocabulary / វាក្យសព្ទ</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Grammar / វេយ្យាករណ៍</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Listening / ការស្ដាប់</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Speacking / កានិយាយ</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Reading / ការអាន</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Writing / ការសរសេរ</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>		                   	 		
		                   	 	</tbody>
		                   </table>
				</div>
				<div class="col-sm-6" style="padding-right: 0px;">					
					<div class="form-group" style="margin-bottom:60px;">
						   <label>Maths-Science  Exam</label>
		                   <table class='table table-bordered'>
		                   	 	<thead>
		                   	 		<tr>
		                   	 			<th id="head">Subjects / មុវិជ្ជា</th>
		                   	 			<th id="scor">Score​ <br> ពិន្ទុ</th>
		                   	 			<th id="scor">Evaluation <br> ការវាយតម្លៃ</th>
		                   	 		</tr>
		                   	 	</thead>
		                   	 	<tbody>
		                   	 		<tr>
		                   	 			<td>Mathematics / គណិតវិទ្យា</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td>Science / វិទ្យាសាស្ត្រ</td>
		                   	 			<td id="scor">10</td>
		                   	 			<td id="scor">Excellent</td>
		                   	 		</tr>
		                   	 	</tbody>
		                   </table>
		            </div>
		            <div class="form-group">		            	   
		                   <table class='table table-bordered'>
		                   	 	<tbody>
		                   	 		<tr>
		                   	 			<td >Assessment & English Test:</td>
		                   	 			<td style="width:50%" id="scor">100</td>		                   	 			
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<tr>
		                   	 			<td>Maths-Science Test:</td>
		                   	 			<td id="scor">100</td>
		                   	 		</tr>
		                   	 		<tr>
		                   	 			<td style="background: #808080 !important;">Total Mid-term(ពិន្ទុសរុប)</td>
		                   	 			<td id="scor" style="background: #808080 !important;">100</td>
		                   	 		</tr>		                   	 		
		                   	 		</tr>
		                   	 	</tbody>
		                   </table>
		            </div>
				</div>
				<div class="form-group" style="padding-bottom:40px;">
					<table class='table table-bordered'>
						<thead>
							<tr>
								<th class="kh_font" colspan="5">Evaluation for Semestern 1 /  ការវាយតម្លៃក្នុងឆមាសទី១</th>
							</tr>
							<tr>
								<th>Work Habits and Social Skills <br> ទម្លាប់ក្នុងការសិក្សា និងជំនាញសង្គម</th>
								<th>Seldom <br> កម្រ</th>
								<th> Sometimes <br> ម្ដងម្កាល</th>
								<th> Usually <br> តែងតែ </th>
								<th> Consistently <br> ជាប្រចាំ </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Display positive Attitude / អាកប្បកិរិយាល្អ</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>
							<tr>
								<td>Display self confidence / មានទំនុកចិត្តក្នុងការសិក្សា</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>	
							<tr>
								<td>Organizes his/her self well / ចេះរៀបចំខ្លួនបានល្អ</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>	
							<tr>
								<td>Carries through instructions / យកចិត្តទុកដាក់ស្ដាប់ការណែនាំ</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>	
							<tr>
								<td>Can work independently / អាចធវ្ើកិច្ចការដោយឯករាជ្យ</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>	
							<tr>
								<td>Cooperates well with others / សហប្រតិបត្តិការល្អជាមួយសិស្សដទៃ</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>	
							<tr>
								<td>Shows oractical thinking skills / បង្ហាញជំនាញចេះគិតវិភាគ</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>	
							<tr>
								<td>Supprtive to peers / ហ៊ានសាកល្បង</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>	
							<tr>
								<td>Responds positively to discipline / គោរពវិន័យ</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>	
							<tr>
								<td>Wompletes Work on time / ធវ្ើកិច្ចការទាន់ពេលវេលា</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>	
							<tr>
								<td>Completes Homework / ធ្វើកិច្ចការងារផ្ទះ</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>	
							<tr>
								<td>Accepts Responsibility / ចេះទទួលខុសត្រូវ</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>	
							<tr>
								<td>Looks after equipment / ចេះថែរក្សាស្ភារៈសិក្សា</td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox"></td>
								<td id="scor"><input type="checkbox" checked></td>
							</tr>								
						</tbody>
					</table>
				</div>
				<div class="form-group">
					 <textarea class="form-control" rows="10" id="comment" >Teacher's Comments / មតិយោបល់របស់គ្រូ</textarea>
				</div>
				<div class="col-sm-12"​​ style="margin-bottom: 60px; text-align:center;">
					<div class="col-sm-6" style="margin-top:20px; ">
						<label>Date: ......../......../............</label><br>
						<label>Academic Manager</label>
					</div>
					<div class="col-sm-6">
						<label>Date: ......../......../............</label><br>
						<label>Teacher's Signature</label>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:3px !important">
					 <textarea class="form-control" rows="8" id="comment"​​>​Guardian's Comments / មតិមាតាបិតាសិស្ស</textarea>
				</div>
				<div class="col-sm-12" style="padding-left:0px; ">
					<div class="col-sm-6">
						<p>Please return the book to school before</p>
						<p>សូមប្រគល់សៀវភៅតាមដាននេះមកសាលាមុនថ្ងៃទី៖</p>					
						<div class="col-sm-6" style="padding-left:0px;">
							<input type="text" value="10-09-2015" class="form-control" style="text-align:center; width:100%; font-size: 11px; ">
						</div>
					</div>
					<div class="col-sm-6" style="text-align:center;padding:0px;">
						<p>Date: ......../......../........</p>
						<p>Guardian's Signature / ហត្ថលេខាមាតាមិតាសិស្ស</p>
					</div>
				</div>
			</div>		   		   		
	   	</div>
	</div>
	<script type="text/javascript"></script>