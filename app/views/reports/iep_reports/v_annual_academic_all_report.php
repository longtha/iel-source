
<?php 
$sql_show_command = $this->db->query("SELECT
										tbl.id,
										tbl.student_id,
										tbl.schoolid,
										tbl.schlevelid,
										tbl.yearid,
										tbl.gradelevelid,
										tbl.classid,
										tbl.termid,
										tbl.command_teacher,
										tbl.academicid,
										tbl.userid,
										tbl.date_academic,
										tbl.date_teacher,
										tbl.date_director,
										tbl.date_create,
										tbl.directorid,
										tbl.teacherid
										FROM
										sch_studend_com_ann_aca_iep AS tbl
										WHERE 1=1
										AND tbl.student_id='".$studentid."'
										AND tbl.schoolid='".$schoolid."'
										AND tbl.schlevelid='".$schlavelid."'
										AND tbl.gradelevelid='".$grandlavelid."'
										AND tbl.yearid='".$yearid."'
										AND tbl.classid='".$classid."'
									")->row();
$comment = isset($sql_show_command->command_teacher)?$sql_show_command->command_teacher:"";
$directorid = isset($sql_show_command->directorid)?$sql_show_command->directorid:"";
$academicid = isset($sql_show_command->academicid)?$sql_show_command->academicid:"";
$teacherid 	= isset($sql_show_command->teacherid)?$sql_show_command->teacherid:"";
$date_director = isset($sql_show_command->date_director)?$this->green->convertSQLDate($sql_show_command->date_director):"";
$date_academic = isset($sql_show_command->date_academic)?$this->green->convertSQLDate($sql_show_command->date_academic):"";
$date_teacher  = isset($sql_show_command->date_teacher)?$this->green->convertSQLDate($sql_show_command->date_teacher):"";

$directorname = $this->db->query("SELECT user_name FROM sch_user WHERE 1=1 AND match_con_posid='admin' AND userid='".$directorid."'")->row();
$academicname = $this->db->query("SELECT user_name FROM sch_user WHERE 1=1 AND match_con_posid='acca' AND userid='".$academicid."'")->row();
$teachname    = $this->db->query("SELECT user_name FROM sch_user WHERE 1=1 AND userid='".$teacherid."'")->row();

$sql_score = $this->db->query("SELECT
									tbl.score_entry_id,
									tbl.schoolid,
									tbl.schlevelid,
									tbl.yearid,
									tbl.termid,
									tbl.gradeid,
									tbl.classid,
									tbl.studentid,
									tbl.total_score,
									sch_school_term.semesterid
									FROM
									sch_iep_total_score_entry AS tbl
									INNER JOIN sch_school_term ON tbl.termid = sch_school_term.termid
									WHERE 1=1
										AND tbl.classid='".$classid."'
										AND tbl.schoolid='".$schoolid."'
										AND tbl.schlevelid='".$schlavelid."'
										AND tbl.gradeid='".$grandlavelid."'
										AND tbl.studentid='".$studentid."'
										AND tbl.yearid = '".$yearid."'
									ORDER BY
										tbl.examtypeid ASC,
										tbl.total_score DESC
									");
$arr_semester = array();
if($sql_score->num_rows() > 0){
	foreach($sql_score->result() as $rsc){
		$score = floor(($rsc->total_score/2)*100)/100;
		if(isset($arr_semester[$rsc->studentid][$rsc->semesterid])){
			$arr_semester[$rsc->studentid][$rsc->semesterid] = $arr_semester[$rsc->studentid][$rsc->semesterid]+$score;
		}
		else
		{
			$arr_semester[$rsc->studentid][$rsc->semesterid] = $score;
		}
	}
}
$arr_show = array();
$total_semester = 0;
if(isset($arr_semester[$studentid])){
	foreach($arr_semester[$studentid] as $va_sc){
		$total_sec = floor(($va_sc/4)*100)/100;
		$arr_show[] = $total_sec;
		$total_semester+=$total_sec;
	}
}
// test
?>
<div class="wrapper" style="border:0px solid #f00;overflow:auto;">
	<div id="tap_print">
				<div class="form-group dv_img">
					<img src="<?php echo base_url('assets/images/logo/logo.png')?>" style="width:250px !important">
				</div>
				<div class="form-group" id="titles">
					<center>
			   		<h4 class="kh_font" style="margin:0px;">របាយការណ៍សិក្សាប្រចាំឆ្នាំ</h4>
			   		<h4 style="font-size:23px;margin:0px;">Annual Acadimic Report</h4>		   			
			   		<h4 style="font-size:23px;margin:0px;">Yearly 2015-2016</h4>  
			   		</center> 		
			   	</div>	 	
			<div class="col-sm-12" style="padding:20px; padding-left:50px;">			
		        <table class='table head_name' style=" margin-bottom: 0px !important;">	
		             <tbody>
                   		<tr>
                   			<td style="">Student's Name:</td>
                   			<td style=""><?php echo $fullname_eng?></td>
                   			<td style="">Level:</td>
                   			<td style=""><?php echo $sch_level; ?></td>
                   			
                   		</tr>
                   		<tr>
                   			<td>សិស្សឈ្មោះ</td>
                   			<td class="kh_font"><?php echo $fullname_kh; ?></td>
                   			<td>កម្រិត:</td>
                   			<td><?php echo $sch_level; ?></td>
                   			
                   		</tr>
                   	</tbody>
		        </table>
			</div>
			<div class="col-sm-12">			
		        <table class='table head_name' style="margin-left: 50px !important;width:70%;">
		            <tbody>
		                <tr>
		                   	<td style="width:60%;">- Total Semester I (មធ្យមភាគប្រចាំឆមាសទី១):</td>
		                   	<td style="text-align: right;"><?php echo (isset($arr_show[0])?$arr_show[0]:0); ?></td>
		                </tr>
		                <tr>
		                   	<td style="width:60%;">- Total Semester II (មធ្យមភាគប្រចាំឆមាសទី២):</td>
		                   	<td style="text-align: right;"><?php echo (isset($arr_show[1])?$arr_show[1]:0); ?></td>
		                </tr>
		                <tr>
		                	<td style="width:60%;">=> Yearly (មធ្យមភាគប្រចាំឆ្នាំ):</td>
		                	<td style="text-align: right;"><?php echo floor(($total_semester/2)*100)/100; ?></td>
		                </tr>
		             </tbody>
		        </table>
			</div>
			<div class="form-group col-sm-12">
				<center>
				<div style="width:95%;border:1px solid black;height: 150px;text-align: left;"><p>&nbsp;&nbsp;<?php echo $comment;?></p></div>
				</center>
			</div>
			<div class="col-sm-12">
				<div class="col-sm-4" style="text-align: center;">
					<label>Date:&nbsp;&nbsp;&nbsp;<?php echo $date_director;?></label><br>
					<label style="margin-left:20px;">Director</label><br>
					<label style="margin-left:30px;margin-top:50px;"><?php echo isset($directorname->user_name)?ucwords($directorname->user_name):"";?></label>
				</div>
				<div class="col-sm-4" style="text-align: center;">
					<label>Date:&nbsp;&nbsp;&nbsp;<?php echo $date_academic;?></label><br>
					<label style="margin-left:20px;">Academic Manager</label><br>
					<label style="margin-left:40px;margin-top:50px;"><?php echo isset($academicname->user_name)?ucwords($academicname->user_name):"";?></label>
				</div>
				<div class="col-sm-4" style="text-align: center;">
					<label>Date:&nbsp;&nbsp;&nbsp;<?php echo $date_teacher;?></label><br>
					<label style="margin-left:20px;">Teacher's Signature</label><br>
					<label style="margin-left:40px;margin-top:50px;"><?php echo isset($teachname->user_name)?ucwords($teachname->user_name):"";?></label>
				</div>
			</div>
	</div>
</div>
<div style="page-break-after: always;"></div>
