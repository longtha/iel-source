<div class="container-fluid" style="width:100% !important;padding-bottom: 20px;">     
   <div class="row">
      <div class="col-xs-12">
         <div class="result_info">
            <div class="col-xs-6">
              <span class="icon">
                  <i class="fa fa-th"></i>
              </span>
                <strong>Student's Study Record</strong>  
            </div>
            <div class="col-xs-6" style="text-align: right">
               <a href="javascript:;" id="a_addnew" data-toggle="tooltip" data-placement="top" title="Search...">
                  <img src="<?= base_url('assets/images/icons/preview.png') ?>" width="24px">
               </a>
               <a href="javascript:;" id="a_print" data-toggle="tooltip" data-placement="top" title="Print">
                  <img src="<?= base_url('assets/images/icons/print.png') ?>">
               </a>
               <a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="top" title="Refresh">
                  <img src="<?= base_url('assets/images/icons/refresh.png') ?>">
               </a>
            </div>         
         </div>
      </div>
   </div>
  <div class="collapse in" id="colapseform">
     <div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-body">
  				<div class="col-sm-3">
  					<div class="form-group">					
  					<label>Program:</label>
  						<select class="form-control" id="program">
  							<option value=""></option>
  							<?php echo $opt_program;?> 
  						</select>
  					</div>
  					<div class="form-group">					
  					<label>Class:</label>
  						<select class="form-control" id="classid">
  						</select>
  					</div>					
  				</div>
  				<div class="col-sm-3">
  					<div class="form-group">					
    					<label>School Level:</label>
    						<select class="form-control" id="sch_level">
    						</select>
  					</div>
  					<div class="form-group">					
    					<label>Exam Type:</label>
    						<select class="form-control" id="exam_type">
    							<option value=""></option>
    							<?php echo $opt_examtyp;?>
    						</select>
  					</div>					
  				</div>
  				<div class="col-sm-3">
  					<div class="form-group">					
    					<label>Year:</label>
    						<select class="form-control" id="years">
    						</select>
  					</div>
  					<label>From Date:</label>
  					<div class="input-group input-append date">					
                <input type="text" id="from_date"  value="<?php echo date('d-m-Y');?>" class="form-control">
                <span class="input-group-addon add-on">
                	<span class="glyphicon glyphicon-calendar"></span>
                </span>                                                
            </div>
  				</div>
  				<div class="col-sm-3">
  					<div class="form-group">					
    					<label>Gread Level:</label>
    						<select class="form-control" id="grad_level">
    							
    						</select>
  					</div>
    					<label>To Date:</label>
    					<div class="input-group input-append date">					
                  <input type="text" id="to_date"  value="<?php echo date('d-m-Y');?>" class="form-control">
                  <span class="input-group-addon add-on">
                  	<span class="glyphicon glyphicon-calendar"></span>
                  </span>                                                
              </div>
  				</div>
  				<div class="col-sm-12" style="padding-top:20px;">
              <div class="form-group">
                 <input type="button" class="btn btn-primary" value="Search" id="search">
              </div>            
          </div>
  			</div>
  		</div>
  	</div>
  </div>
  <div class="form-group" id="prints">
      <div class="col-sm-12">
         <div class="table-responsive">
            <table border="0"​ cellspacing="5" cellpadding="0" style="width: 100%;">
               <tr>
                  <td align="center">
                     <div style="width: 25cm;border:0; overflow:auto;display: inline-block; max-height:1100px;">
                        <table id="tab_print" border="0" cellspacing="0" cellpadding="0" align="right" class="table-condensed" style="width: 88%;font-family: khmer mef1;font-size: 16px;margin-right: 25px;">
                           <tbody id="list_data">
                              
                           </tbody>
                        </table>
                     </div>
                  </td>
               </tr>
            </table>
         </div>
      </div>
  </div>
</div>    
  <div class="col-sm-12">
    <div class='col-sm-3'>
    <label>Show Page
        <select id='perpage' onchange='show_report();' class="form-control select2-offscreen" style="padding-top:0px !important; padding-bottom: 0px !important; height:auto !important;">
             <?PHP
                for ($i = 1; $i < 500; $i += 1) {                                        
                    if ($i == $this->iep->getnumberpage()) {
                        echo "<option value='$i' selected>$i</option>";
                    } else {
                        echo "<option value='$i'>$i</option>";
                    }
                }
            ?>
           <!--  <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option> -->
        </select>
    </label>
  </div>
      <div class="col-sm-2" style="margin-top: 8px !important;">
        <div id="sp_page" class="btn-group pagination btn-group-xs" role="group" aria-label="..." style="display: inline;"></div>
    </div>  
  </div> 

<script type="text/javascript">
	$( document ).ready(function() {
  		$("#from_date, #to_date").datepicker({
        onSelect: function(date) {
          $(this).val(formatDate(date));
          getdata($(this).val());
        } 
      });

      $("#a_addnew").click(function(){
          $("#colapseform").collapse('toggle');
      });

      $("#refresh").click(function(){
          location.reload();
      });

      $( "#search" ).click(function() {       	
  		      show_report();
      });

      $('#a_print').click(function(){
         var data = $("#tab_print").html();
         var data_print=$("<div>"+data+"</div>").html().replace(/<A[^>]*>|<\/A>/gi,"");
         var export_data = $("<center>"+data_print+"</center>").clone().find(".remove_tag").remove().end().html();
         gsPrint_book("", export_data);
      });

      $('#program').change(function(){
        var program = $(this).val();
         var sch_level = $("#sch_level").val();
         //alert($(this).val());​​​​​​​​​​​   
         if(program != ""){
            get_school_level(program,sch_level);  
         }else{
            $('#years').html('');
            $('#grad_level').html('');
            $('#sch_level').html('');
         }
      });

      $('#sch_level').change(function(){        
        var sch_level = $(this).val(); 
        var program = $('#program').val();
        var grad_level = $('#grad_level').val();
        if(sch_level !=""){
            get_year(program,sch_level); 
            get_gradlevel(program,sch_level);
            get_class(sch_level,grad_level);              
        }

      });

      $('#grad_level').change(function(){
        var sch_level = $('#sch_level').val();
        var grad_level = $(this).val();  
        if(grad_level !=""){
             get_class(sch_level,grad_level);            
        }

      });

	});


  function  get_school_level(program){
    $.ajax({
      url: '<?php echo site_url('reports/c_iep_entryed_order_report/school_level') ?>',
      datatype:'JSON',
      type:'POST',
      async:false,
      data:{
            'program':program
      },
      success:function(data){
          $('#sch_level').html(data.schlevel);    
          get_year(program,$("#sch_level").val());
      }
    });
  }

  function get_year(program,sch_level){
    $.ajax({
      url:'<?php echo site_url('reports/c_iep_entryed_order_report/get_years') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'program':program,
              'sch_level':sch_level
      },
      success:function (data){
          $('#years').html(data.schyear);
          get_gradlevel(program,sch_level);
      },
    });
  }

  function get_gradlevel(program,sch_level){
    $.ajax({
      url:'<?php echo site_url('reports/c_iep_entryed_order_report/get_gradlevel') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'program':program,
              'sch_level':sch_level
      },
      success:function (data){
          $('#grad_level').html(data.gradlevel);         
      },
    });
  }

  function get_class(sch_level,grad_level){
    $.ajax({
      url:'<?php echo site_url('reports/c_iep_entryed_order_report/get_class') ?>',
      type:'POST',
      datatype:'JSON',     
      async:false,
      data:{
              'sch_level':sch_level,
              'grad_level':grad_level
      },
      success:function (data){
          $('#classid').html(data.getclass);
      },
    });
  }

  function clear_form(){
    // $('#from_date').val('<?php echo date('d-m-Y');?>');
    // $('#to_date').val('<?php echo date('d-m-Y');?>');
    $('school_level').val('');
    $('#grad_level').val('');
    $('#years').val('');
  }

  function formatDate(date) {
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [year, month, day].join('-');
   }
  function show_report(){
  	var program   = $('#program').val();
  	var classid   = $('#classid').val();
  	var from_date = $(' #from_date').val();
  	var to_date   = $('#to_date').val();
  	var sch_level = $('#sch_level').val();
  	var exam_type = $('#exam_type').val();
  	var years  	  = $('#years').val();
  	var grad_level= $('#grad_level').val();
    var perpage   = $('#perpage').val();

  	$.ajax({
      	url: '<?php echo site_url('reports/c_iep_entryed_order_report/show_report') ?>',
      	type: 'POST',
      	datatype: 'JSON',
      	async: false,
      	data: {
          'perpage':perpage,
      		'program':program,
      		'classid':classid,
      		'from_date':from_date,
      		'to_date':to_date,
      		'sch_level':sch_level,
      		'exam_type':exam_type,
      		'years':years,
      		'grad_level':grad_level
      	},
      	success: function(data) {          
      		$('#list_data').html(data);
         // $('#list_data').html(data.list_data);
      	}
  	});
  }
</script>