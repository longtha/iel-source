<?php
    $lang['studentid']="អត្តលេខសិស្ស";
    $lang['register_date']="ថ្ងៃខែឆ្នាំ ចុះឈ្មោះ";
    $lang['first_name']="នាមត្រកូល";
	$lang['last_name']="នាមខ្លួន";
	$lang['first_name_kh']="នាមត្រកូល(ខ្មែរ)";
	$lang['last_name_kh']="នាមខ្លួន(ខ្មែរ)";
	$lang['first_name_ch']="នាមត្រកូល(ចិន)";
	$lang['last_name_ch']="នាមខ្លួន(ចិន)";
	$lang['dob']="ថ្ងៃខែ ឆ្នាំ កំណើត";
	$lang['gender']="ភេទ";
	$lang['nationality']="សញ្ជាតិ";
	$lang['home_no']="ផ្ទះលេខ";
	$lang['street']="ផ្លូវលេខ";
	$lang['village']="ភូមិ";
	$lang['commune']="ឃុំ/សង្កាត់";
	$lang['district']="ស្រុក/ខណ្ឌ";
	$lang['province']="ខេត្ត/ក្រុង";
	$lang['staywith']="ស្នាក់នៅជាមួយ";
	$lang['phone1']="លេខទូរស័ព្ទ";
	$lang['email']="អីម៉ែល";
	$lang['registered_number']="អ្នកចុះឈ្មោះទី";
	$lang['registered_discount']="បញ្ចុះតំលៃ(%)";
	$lang['leave_sch_date']="ថ្ងៃចាកចេញ";  
	$lang['leave_reason']="មូលហេតុ";
	$lang['reg_input_logo']="រូបថត";
	$lang['is_vtc']="ចាកចេញ";

	$lang['place_of_birth']="ទីកន្លែងកំណើត";
	$lang['current_address']="អាសយដ្ឋានបច្ចុប្បន្ន";

	$lang['familyid']="អត្តលេខគ្រួសារ";
	$lang['family_name']="ឈ្មោះគ្រួសារ";
	$lang['family_book_record_no']="លេខសៀវភៅគ្រួសារ";
	$lang['father_name']="ឈ្មោះឪពុក";
	$lang['father_ocupation']="មុខរបរ";
	$lang['father_dob']="ថ្ងៃខែឆ្នាំកំណើតឪពុក";
	$lang['father_home']="ផ្ទះលេខ";
	$lang['father_street']="ផ្លូវលេខ";
	$lang['father_village']="ភូមិ";
	$lang['father_commune']="ឃុំ/សង្កាត់";
	$lang['father_district']="ស្រុក/ខណ្ឌ";
	$lang['father_province']="ខេត្ត/ក្រុង";
	$lang['father_phone']="លេខទូរស័ព្ទ";

	$lang['mother_name']="ឈ្មោះម្តាយ";
	$lang['mother_ocupation']="មុខរបរ";
	$lang['mother_dob']="ថ្ងៃខែឆ្នាំកំណើតម្តាយ";
	$lang['mother_home']="ផ្ទះលេខ";
	$lang['mother_street']="ផ្លូវលេខ";
	$lang['mother_village']="ភូមិ";
	$lang['mother_commune']="ឃុំ/សង្កាត់";
	$lang['mother_district']="ស្រុក/ខណ្ឌ";
	$lang['mother_province']="ខេត្ត/ក្រុង";
	$lang['mother_phone']="លេខទូរស័ព្ទ";

	$lang['home_no']="ផ្ទះលេខ";
	$lang['street']="ផ្លូវលេខ";
	$lang['village']="ភូមិ";
	$lang['commune']="ឃុំ/សង្កាត់";
	$lang['district']="ស្រុក/ខណ្ឌ";
	$lang['province']="ខេត្ត/ក្រុង";

	$lang['respon_name']="ឈ្មោះអាណាព្យាបាល";
	$lang['occupation']="មុខរបរ";
	$lang['tel1']="លេខទូរស័ព្ទ";
	$lang['tel2']="លេខទូរស័ព្ទបន្ទាន់";
	$lang['email']="អីម៉ែល";
	$lang['home_no']="ផ្ទះលេខ";
	$lang['street']="ផ្លូវលេខ";
	$lang['village']="ភូមិ";
	$lang['commune']="ឃុំ/សង្កាត់";
	$lang['district']="ស្រុក/ខណ្ឌ";
	$lang['province']="ខេត្ត/ក្រុង";
	$lang['consultant_by']="អ្នកចុះឈ្មោះ";
	$lang['get_info_from']="ទទួលពត៌មាន";

	$lang['program']="កម្មវិធីសិក្សា";
	$lang['class_name']="ថ្នាក់";
	$lang['sch_year']="ឆ្នាំសិក្សា";
	$lang['sch_level']="កម្រិតសិក្សា";
	$lang['rangelevelname']="កម្រិតថ្នាក់";

	$lang['acadtype']="កាសិក្សាចំណេះទូទៅតាមភូមិសិក្សា";
	$lang['`name`']="ឈ្មោះសាលា";
	$lang['Current Level Of English']="កំរឹតភាសាអង់គ្លេសបច្ចុប្បន្ន";

	$lang['interview_by']="សំភាសន៍ដោយ";
	$lang['introduced_by']="ណែនាំដោយ";
	$lang['student']="សិស្ស";
	$lang['teacher']="គ្រូបង្រៀន";
	$lang['Other']="ផ្សេងៗ";

	$lang['is_owner_by']="ដោយខ្លួនឯង";
	$lang['is_sch_car_no']="រថយន្តសាលា";

	$lang['Personal Details']="ព​ត៍​មាន​ផ្ទាល់ខ្លួន";
	$lang['Father Information']="ព​ត៍​មាន​របស់ឪពុក";
	$lang['Mother Information']="ព​ត៍​មាន​របស់ម្តាយ";
	$lang['Guardain Information']="ព​ត៍​មាន​របស់អាណាព្យាបាល";
	$lang['Academic Year']="ឆ្នាំសិក្សា";
	$lang['Office Use Only']="សម្រាប់បុគ្គលិកបំពេញប៉ុណ្ណោះ";
	$lang['Transportation']="មធ្យោបាយធ្វើដំណើរ";
	$lang['Academic Background']="ប្រវត្តិនៃការសិក្សា";
	$lang['General English']="ការសិក្សាភាសាអង់គ្លេស";
	$lang['New Family Information']="ព​ត៍​មាន​របស់គ្រួសារថ្មី";
	$lang['Primary School(Grade1-6)']="បឋមសិក្សា​ (ថ្នាក់ទី១-៦)";
	$lang['Lower Secondary School(Grade7-9)']="អនុវិទ្យាល័យ (ថ្នាក់ទី៧-៩)";
	$lang['Upper Secondary School(Grade10-12)']="វិទ្យាល័យ (ថ្នាក់ទី១០-១២)";

?>