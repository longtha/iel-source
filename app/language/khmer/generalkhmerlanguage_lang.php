<?php
    //form element---------------------------------------------------------------
    $lang['general khmer language report']="របាយការណ៏ចំនេះដឹងទូទៅភាសាខ្មែរ";
    $lang['academic year']="ឆ្នាំសិក្សា";
    $lang['study program']="កម្មវិធីសិក្សា";
    $lang['study level']="កម្រិតសិក្សា";
    $lang['class name']="ថ្នាក់";
    $lang['choose month for result']="ជ្រើសរើសខែនីមួយៗសំរាប់លទ្ធផល";
    $lang['type of report']="ប្រភេទរបាយការណ៏";
    $lang['student name']="ឈ្មោះសិស្ស";

    /*--months data--*/
    $lang['month_1']="មករា";
    $lang['month_2']="កុម្ភៈ";
    $lang['month_3']="មិនា";
    $lang['month_4']="មេសា-ឧសភា";
    $lang['month_5']="មិថុនា";
    $lang['month_6']="កក្តដា";
    $lang['month_7']="សីហា";
    $lang['month_8']="កញ្ញា";
    $lang['month_9']="តុលា";
    $lang['month_10']="វិច្ឆកា";
    $lang['month_11']="ធ្នូ";
    
    // $lang['semester1']="ឆមាសទី ១";
    // $lang['semester2']="ឆមាសទី ២";
    // $lang['yearly']="ប្រចាំឆ្នាំ";

    /*--type of report--*/
    $lang['monthly report class ranking']="ចំណាត់ថ្នាក់ប្រចាំខែ";
    $lang['monthly report individual ranking']="របាយការណ៏សិក្សាប្រចាំខែ";
    $lang['semester report class ranking']="ចំណាត់ថ្នាក់ប្រចាំឆមាស";
    $lang['semester report individual ranking']="របាយការណ៏សិក្សាប្រចាំឆមាស";
    $lang['yearly report class ranking']="ចំណាត់ថ្នាក់ប្រចាំឆ្នាំ";
    $lang['yearly report individual ranking']="របាយការណ៏សិក្សាប្រចាំឆមាស";

    /*--message required--*/
    $lang['please choose']= "សូមជ្រើសរើស";
    $lang['that you want to show']= "ដែលអ្នកចង់បង្ហាញលទ្ធផល";
    
    //report data for primary school---------------------------------------------
    $lang['result report']="លទ្ធផលនៃរបាយការណ៏";

    /*--table header--*/
    $lang['no']="លេខរៀង";
    $lang['studentname']="គោត្តនាមនិងនាម";
    $lang['gender']="ភេទ";
    $lang['classname']="ថ្នាក់";
    $lang['score']="ពិន្ទុ";
    $lang['average']="មធ្យមភាគ";
    $lang['ranking']="ចំណាត់ថ្នាក់";
    $lang['grade']="និទ្ទេស";
    $lang['remark']="ផ្សេងៗ";

    /*--table body--*/
    $lang['f']="ស";
    $lang['m']="ប";
    
?>