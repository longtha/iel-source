<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');	
	class Green{
		
		public $roleid;
		public $roleinfor;
		public $moduleids;
		public $moduleinfos;
		public $pageids;
		public $pageinfos;		
		
		public $active_role;
		public $active_module;
		public $active_page;
		public $active_user;
		public $totaluser;

		public function runSQL($sql){
			$ci=& get_instance();               
	        $query = $ci->db->query($sql);
			return $query ;
		}
		
		public function getTable($sql){
			$arrDatas=array();		         
	       	$query = $this->runSQL($sql);
			foreach ($query->result_array() as $row)		
			{
				$arrDatas[]=$row;			   
			}		
			return $arrDatas;
		}
		public function getOneRow($sql){
			$row = $this->runSQL($sql)->row_array();		
			return $row;
		}
		
		public function getValue($sql){		
			$row = $this->runSQL($sql)->row_array();
			$num_arr = array_values($row);			
			return isset($num_arr[0])?$num_arr[0]:"";				
		}		
		public function getTotalRow($sql){		
	        $row = $this->runSQL($sql)->num_rows();
			return $row;
		}
		public function getFieldName($sql){		
			$query = $this->runSQL($sql)->list_fields();
			return $query;
		}	
		public function create_temp($sql){
			return $this->runSQL($sql);
		}
		public function drop_temp($tem_name){
			$ci=& get_instance();               
	        $query = $this->runSQL("DROP TEMPORARY TABLE IF EXISTS $tem_name");		
		}
			
		public function gictEnc($str){
			$ci=& get_instance(); 
			$key=$ci->config->item('encryption_key');		
			return $ci->encrypt->encode($str,$key);
		}
		public function gictDec($str){
			$ci=& get_instance(); 
			$key=$ci->config->item('encryption_key');
			return $ci->encrypt->decode($str,$key);
		}
		public function goToPage($page){
			  redirect($page, 'refresh');
		}
		public function clearSession(){
			$ci=& get_instance();
			foreach (array_keys($ci->session->userdata) as $key)
			{
			  $ci->session->unset_userdata($key);
			}
		}		

		public function exportToXls($data,$fileName="Sheet"){			
			header('ETag: etagforie7download'); //IE7 requires this header
			header('Content-type: application/octet_stream');
			header('Content-disposition: attachment;filename="'.$fileName.' '.date('Y-m-d').'.xls');
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';	
			echo $data;
			die();	
		}
		
		
		public function myUpload($file,$new_name,$is_thumb=0){
			$ci=& get_instance();
			//================================ upload image ============================
			$path =$ci->config->item('image_upload_path')."/Promotions";	
			$sqlcheck = "SELECT * FROM folder where foldername='Promotions' and folderpath='".$path."'";
			$folder_check=$ci->db->query($sqlcheck);
			if($folder_check->num_rows()<=0){
				$sqlinsert = "INSERT INTO folder SET  foldername='Promotions',folderpath='".$path."'";
				$ci->db->query($sqlinsert);	
				mkdir($path,0777); 						
			}
			//============= Checking File To Upload ==============================
			if (isset($_FILES[$file]) AND $_FILES[$file]['name'] != '') {
				$AllowType=array("image/gif","image/jpeg","image/pjpeg","image/png","image/bmp");
				$result = $_FILES[$file]['error'];
				$UploadTheFile = 'Yes'; 
				$filename = $path . '/' .$new_name ;
			   if($_FILES[$file]['size'] > $ci->config->item('max_size_upload') * 1024) {       
					$UploadTheFile = 'No';
					echo 'size';
				}elseif(!(in_array($_FILES[$file]['type'],$AllowType))){
					$UploadTheFile = 'No';
					echo 'Type';
				}
				elseif (file_exists($filename)) {       
					$result = unlink($filename);
					if (!$result) {           
						$UploadTheFile = 'No';
					}
				}
				if ($UploadTheFile == 'Yes') {
					$result = move_uploaded_file($_FILES[$file]['tmp_name'], $filename);     
				}
			}
			if($is_thumb==1){
				createThumbImg($new_name);
			}
				
		}
		public function createThumbImg($source_image,$height=70,$width=60){
			$ci=& get_instance();
			#============= Create Thumb Image ============
			$path =$ci->config->item('image_upload_path')."/Promotions";
			$config['image_library'] = 'gd2';
			$config['source_image']	= $path.'/'.$source_image;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width']	 = $width;
			$config['height']	= $height;
			
			#$config['new_image'] = $path.'/'.$filename;
			
			$ci->load->library('image_lib', $config);
			$ci->image_lib->initialize($config); 
			$ci->image_lib->resize();
			
			if ( ! $ci->image_lib->resize())
			{
				echo $ci->image_lib->display_errors();
			}
		}
		public function getCombo($source="",$key="",$display="",$sql=""){
			#==== $source=tablename,$key=idfield,display=field for show 
			$result=array();
			if($sql==""){
				$data=$this->getTable("SELECT `{$key}`,`{$display}` FROM `{$source}` ORDER BY `{$display}`");
			}else{
				$data=$this->getTable($sql);
			}
			if(count($data)>0){
				foreach($data as $rkey =>$rval){
					$result[$rkey]=$rval;
				}
			}
			return $result;
		}		
		

		public function getModule($roleid){
			
			$this->roleinfor=$this->getOneRow("SELECT
													sch_z_role.roleid,
													sch_z_role.role,
													sch_z_role.is_admin
												FROM
													sch_z_role
												WHERE
													is_active = 1
												AND roleid = '".$roleid."'
											");
			
			$where=" WHERE roleid = '".$roleid."'";
			if($roleid==1){
				$where="";
			}
			$arrModules=$this->getTable("SELECT
											DISTINCT
											rolmod.moduleid
										FROM
											sch_z_role_module_detail as rolmod
											INNER JOIN sch_z_module zmod ON zmod.moduleid=rolmod.moduleid
										{$where}
										ORDER BY zmod.`order`
										");

			$this->moduleids=$arrModules;
			$this->roleid=$this->roleinfor['roleid'];						
					
		}
		public function getModuleInfo($moduleid){			
			
			$where=" AND moduleid = '".$moduleid."'";			
				
			$arrModule=$this->getOneRow("SELECT
											sch_z_module.moduleid,
											sch_z_module.module_name,
											sch_z_module.sort_mod,
											sch_z_module.mod_position
											FROM
											sch_z_module											
											WHERE
												is_active = 1
											{$where}
											ORDER BY `order`;
										");
			return $arrModule;
		}
				
		public function getRolePage($moduleid){
			$where="";				
			if($this->roleid==1){
				$where.=" AND moduleid = '".$moduleid."'";					
				
				$arrPages=$this->getTable("SELECT
											page.pageid,
											page.page_name,
											page.link,
											page.moduleid,
											page.`order`,
											page.is_insert,
											page.is_update,
											page.is_delete,
											page.is_show as is_read,
											page.is_print,
											page.is_export,
											page.created_by,
											page.created_date
										FROM
											sch_z_page AS page
										WHERE
											is_active = 1											
										{$where}										
										ORDER BY moduleid,`order`																				
									");				
			}else{
				$where.=" AND rolepage.roleid = '".$this->roleid."'";							
				$where.=" AND rolepage.moduleid = '".$moduleid."'";	
					
				$arrPages=$this->getTable("SELECT
											rolepage.pageid,
											rolepage.moduleid,
											rolepage.roleid,
											rolepage.is_read,
											rolepage.is_insert,
											rolepage.is_delete,
											rolepage.is_update,
											rolepage.is_print,
											rolepage.is_export,
											rolepage.is_import
										FROM
											sch_z_role_page as rolepage	
										INNER JOIN sch_z_page page ON page.pageid=rolepage.pageid							
										WHERE
											1 = 1
										{$where}	
										ORDER BY page.moduleid,page.`order`									
									");	
			}			
								
			$this->pageids=$arrPages;
			
		}
		public function getPageInfo($pageid){
			$where=" AND pageid = '".$pageid."'";					
			$arrPages=$this->getOneRow("SELECT
											page.pageid,
											page.page_name,
											page.link,
											page.moduleid,
											page.`order`,
											page.is_insert,
											page.is_update,
											page.is_delete,
											page.is_show,
											page.is_print,
											page.is_export,
											page.created_by,
											page.created_date,
											page.icon
										FROM
											sch_z_page AS page
										WHERE
											is_active = 1											
										{$where}
										ORDER BY moduleid,`order`										
									");
			$this->pageinfos=$arrPages;
		}

		function setActiveRole($role){
			$this->active_role=$role;
		}
		function getActiveRole(){
			return $this->active_role;
		}
		function setActiveModule($module){
			$this->active_module=$module;
		}
		function getActiveModule(){
			return $this->active_module;
		}
		function setActivePage($page){
			$this->active_page=$page;
		}
		function getActivePage(){
			return $this->active_page;
		}
		function setActiveUser($user){
			$this->active_user=$user;
		}
		function getActiveUser(){
			return $this->active_user;
		}

		public function gAction($action){			
			$arrAct=array('C'=>'is_insert',
							'D'=>'is_delete',
							'U'=>'is_update',
							'R'=>'is_read',
							'E'=>'is_export',	
							'P'=>'is_print',
							'I'=>'is_import'
						);

			$sqlAction="SELECT ".$arrAct[$action]."					
							FROM sch_z_role_page 
							WHERE roleid='".$this->getActiveRole()."' 
							AND moduleid='".$this->getActiveModule()."' 
							AND pageid='".$this->getActivePage()."'";

			// echo $sqlAction;
					
			$res=$this->getValue($sqlAction)-0;		
			
			if($res==1 || $this->getActiveRole()==1){
				return true;
			}else{
				return false;
			}

			
		}

		public function displayDate($date){
			$date=date_create($date);
			return date_format($date,"d-m-Y");
		}

		public function GetSchool()
		{
			
			$sql="SELECT
					sch_school_infor.schoolid,
					sch_school_infor.`name`
				FROM
					sch_school_infor
				WHERE
					is_active = 1
				";
			$data=$this->getTable($sql);	
			$op='';
			if(count($data)>0){				
				foreach ($data as $row) {
					$op.='<option value="'.$row['schoolid'].'">'.$row['name'].'</option>';
				}
			}	

			return $op;
		}

		public function GetSchYear($schid="",$yearid="")
		{
			$WHERE="";
			if($schid!=""){
				$WHERE.=" AND schoolid='".$schid."'";
			}
			$sql="SELECT
					sch_school_year.yearid,
					sch_school_year.sch_year,
					sch_school_year.from_date,
					sch_school_year.to_date,
					sch_school_year.schoolid
				FROM
					sch_school_year
				ORDER BY to_date DESC
				";
			$data=$this->getTable($sql);	
			$op='';

			if(count($data)>0){				

				foreach ($data as $row) {
					$op.='<option value="'.$row['yearid'].'" '.($yearid==$row['yearid']?"selected":"").'>'.$row['sch_year'].'</option>';
				}
			}	

			return $op;
		}

		public function GetSchLevel($schid="",$selected="",$blank=1)
		{
			$WHERE="";
			if($schid!=""){
				$WHERE.=" AND schoolid='".$schid."'";
			}
			$sql="SELECT
					sch_school_level.schlevelid,
					sch_school_level.sch_level,
					sch_school_level.note,
					sch_school_level.schoolid,
					sch_school_level.orders
				FROM
					sch_school_level WHERE 1=1 {$WHERE}
				ORDER BY
					schoolid,
					orders
				";
			$data=$this->getTable($sql);	

			$op='';			
			if(count($data)>0){			
				$i=0;	
				foreach ($data as $row) {
					if($this->getActiveRole()<>1){
						$getSchlevelByUser=$this->getValue("SELECT count(schlevelid) 
																	FROM sch_user_schlevel 
																	WHERE userid='".$this->getActiveUser()."'
																	AND schlevelid='".$row['schlevelid']."'
																	");
						if($getSchlevelByUser<>0){
							$op.='<option value="'.$row['schlevelid'].'" '.($selected==$row['schlevelid']?"selected":"").'>'.$row['sch_level'].'</option>';
						}
					}else{
						if($i==0 && $blank==1) {
							$op.='<option value=""></option>';
						}
						$op.='<option value="'.$row['schlevelid'].'" '.($selected==$row['schlevelid']?"selected":"").'>'.$row['sch_level'].'</option>';
				
					}	

					$i++;
				}
			}	

			return $op;
		}		

		function getEmpInf($empid){
			if($empid!=""){
				return $this->getOneRow("SELECT empcode,last_name,first_name,last_name_kh,first_name_kh FROM sch_emp_profile WHERE empid='".$empid."'");
			}
		}
		function getEmpID($empcode){
			$empid='';	
			if($empcode!=""){
				$data=$this->getTable("SELECT empid FROM sch_emp_profile WHERE empcode like '".$empcode."%'");
				if(count($data)>0){					
					foreach($data as $row){
						$empid.=$row['empid'].',';
					}
					$empid=trim($empid,",");					
				}	
			}
			return $empid;
		}

		//---------------- sonearth Get 
		// Ranking of Result by Subject or by Class---------------
		public function FconvertClaID($rangfeeid,$programid,$schlevelid,$yearid,$fromclassid,$toclassid,$studentid,$enromentID){
				$wh="";
				$wh1="";

				$exam_type=1;
				///---------------- ---------------- -------------- for enroment
				$wh.=" AND programid='".$programid."'";
				$wh.=" AND schlevelid='".$schlevelid."'";
				$wh.=" AND year='".$yearid."'";
				
				$wh.=" AND classid='".$fromclassid."'";				
				$wh.=" AND studentid='".$studentid."'";
				//---------------- ---------------- ---------------- for payment
				$wh1.=" AND programid='".$programid."'";
				$wh1.=" AND schooleleve='".$schlevelid."'";
				$wh1.=" AND acandemic='".$yearid."'";
				
				$wh1.=" AND classid='".$fromclassid."'";				
				$wh1.=" AND studentid='".$studentid."'";
				//---------------- ---------------- ----------------
				$slqldatas="UPDATE sch_student_enrollment SET classid='".$toclassid."' WHERE 1=1 {$wh}";	
				//echo $slqldatas.';';								
				$this->runSQL($slqldatas);

				$sql1="UPDATE sch_student_fee SET classid='".$toclassid."' WHERE 1=1 {$wh1}";	
				//echo "this:".$sql1;'<br>';									
				$this->runSQL($sql1);

				$sql2="UPDATE sch_student_fee_rec_order SET classid='".$toclassid."' WHERE 1=1 {$wh1}";	
				//echo "this:".$sql2;'<br>';								
				$this->runSQL($sql2);

				$sql3="UPDATE sch_student_fee_deleted SET classid='".$toclassid."' WHERE 1=1 {$wh1}";	
				//echo "this:".$sql3;'<br>';								
				$this->runSQL($sql3);

				// $countmethod="SELECT
				// COUNT(classid) AS classid
				// FROM
				// sch_school_rangelevelclass
				// WHERE 1=1
				// AND rangelevelid ='".$rangfeeid."'
				// AND classid ='".$toclassid."'
				// ";
				// // echo $countmethod; exit();

				// $ispayexist = $this->db->query($countmethod)->row()->classid;

				// if($ispayexist==0){
				// $datas=array('rangelevelid'=>$rangfeeid,'classid'=>$toclassid);
				// $this->db->insert('sch_school_rangelevelclass', $datas);
				// }
		}

		public function getmention($school_level_id,$subjectid,$scoreinput){
			$selectdata="SELECT
							sch_score_mention.mention
						FROM
							sch_score_mention
						INNER JOIN sch_subject_mention ON sch_score_mention.menid = sch_subject_mention.mentionid
						WHERE
							1 = 1						
						AND sch_subject_mention.max_score >= '".$scoreinput."'
						AND sch_subject_mention.minscrore <= '".$scoreinput."'
						";
			$mention = $this->db->query($selectdata)->row()->mention;
			$sql2="UPDATE sch_subject_score_monthly_kgp_multi SET mentions='".$mention."' 
					WHERE 1=1
					AND school_level_id = '".$school_level_id."'
					AND subject_id = '".$subjectid."'					
				  ";	
			$this->runSQL($sql2);
		}

		public function getRankresult_by_class($program_id,$school_level_id,$adcademic_year_id,$school_id,$class_id,$exam_type){
				
				$wh="";
				$exam_type=1;
				$wh.=" AND program_id='".$program_id."'";
				$wh.=" AND school_level_id='".$school_level_id."'";
				$wh.=" AND adcademic_year_id='".$adcademic_year_id."'";
				$wh.=" AND school_id='".$school_id."'";
				$wh.=" AND class_id='".$class_id."'";
				if($exam_type==1){				
			//------------- get ranking by Class monthly-------------------------------
					$selelsql2="SELECT	
									m_class.student_id,									
									FIND_IN_SET(
										average_score,
										(
											SELECT
												GROUP_CONCAT(
													average_score
													ORDER BY
													average_score 
													DESC
												)
											FROM
												sch_subject_score_monthly_kgp_multi
											WHERE
												1 = 1 
										)
									) AS rank
								FROM
									sch_subject_score_monthly_kgp_multi AS m_class
								WHERE 1=1
								{$wh}
								ORDER BY rank
								";
				//echo  "this:--".$selelsql2;				
			$data2=$this->getTable($selelsql2);
			if(count($data2)>0){					
					foreach($data2 as $row2){						
						$student_id=$row2['student_id'];											
						$rank=$row2['rank'];
						$slqldatas="UPDATE sch_subject_score_monthly_kgp_multi SET ranking_bysubj='".$rank."'
										WHERE 1=1
										AND student_id='".$student_id."'
										{$wh}																			
									";	
						//echo "this:".$slqldatas;								
						$this->runSQL($slqldatas);
					}										
			}
			//end ----------1 get ranking by Class-------------------------------	
		  }
		}// end ==================================================================
		
		public function RankResult($program_id="",$school_level_id="",$adcademic_year_id="",$school_id="",$class_id="",$subject_id="",$student_id="",$exam_type=""){
				//-------------I get ranking by subject ------------
				$wh="";
				if($program_id !=""){
					$wh=" AND program_id='".$program_id."'";
				}
				if($school_level_id !=""){
					$wh=" AND school_level_id='".$school_level_id."'";
				}
				if($adcademic_year_id !=""){
					$wh=" AND adcademic_year_id='".$adcademic_year_id."'";
				}
				if($school_id !=""){
					$wh=" AND school_id='".$school_id."'";
				}
				if($class_id !=""){
					$wh=" AND class_id='".$class_id."'";
				}
						$selelsql="SELECT				
										sch_subject_score_kgp.student_id,
										sch_subject_score_kgp.total_score,
										FIND_IN_SET(
													total_score,
													(
													SELECT
													GROUP_CONCAT(
													total_score
													ORDER BY
													total_score DESC
													)
													FROM
													sch_subject_score_kgp
													WHERE 1=1
														{$wh}
														AND subject_id='".$subject_id."'
													)
										)AS rank
										FROM
											sch_subject_score_kgp
										WHERE 1=1
										{$wh}
										AND subject_id='".$subject_id."'				
										ORDER BY
										total_score DESC
										";
			
			$data=$this->getTable($selelsql);
				if(count($data)>0){					
					foreach($data as $row){ 						
						$student_id=$row['student_id'];
						$rank=$row['rank'];
						
						$this->runSQL("UPDATE sch_subject_score_kgp SET
										ranking_bysubj='".$rank."'
										WHERE 1=1
										{$wh}
										AND subject_id='".$subject_id."'
										");
					}										
				}// end ==========================================================
				//-------------II get score group by class----------------------------
						 $selelsql1="SELECT							
										SUM(total_score) AS total_score,
										SUM(averag_coefficient) AS averag_coefficient,
										sch_subject_score_kgp.student_id
										FROM
											sch_subject_score_kgp
										WHERE 1=1							
										{$wh}
										AND subject_id='".$subject_id."'
										GROUP BY
										student_id,
										class_id
										";
			$data1=$this->getTable($selelsql1);
			if(count($data1)>0){					
					foreach($data1 as $row1){
						$student_id=$row1['student_id'];
						$total_score=$row1['total_score'];
						$averag_coefficient=($row1['averag_coefficient']==0?1:$row1['averag_coefficient']);
									
						$this->runSQL("DELETE FROM sch_class_score_kgp
										WHERE 1=1
										{$wh}
										AND subject_id='".$subject_id."'
										AND student_id='".$student_id."'
										");
						$this->runSQL("INSERT INTO sch_class_score_kgp SET 
										school_level_id='".$school_level_id."',
										adcademic_year_id='".$adcademic_year_id."',
										school_id='".$school_id."',
										class_id='".$class_id."',
										subject_id='".$subject_id."',										
										student_id='".$student_id."',					
										avg_score='".$total_score/$averag_coefficient."',
										averag_coefficient='".$averag_coefficient."',
										total_score='".$total_score."'						
										");
					}										
				}// end ==========================================================
				//-------------III get ranking by Class-------------------------------
					$selelsql2="SELECT
										sch_class_score_kgp.avg_score,
										sch_class_score_kgp.student_id,																				
										FIND_IN_SET(
													avg_score,
													(
													SELECT
													GROUP_CONCAT(
													avg_score
													ORDER BY
													avg_score DESC
													)
													FROM
													sch_class_score_kgp
													WHERE 1=1
														{$wh}
													)
										)AS rank
										FROM sch_class_score_kgp
										WHERE 1=1
											{$wh}
										ORDER BY avg_score DESC
										";
			$data2=$this->getTable($selelsql2);
			if(count($data2)>0){					
					foreach($data2 as $row2){						
						$student_id=$row2['student_id'];
						$rank=$row2['rank'];									
						$this->runSQL("UPDATE sch_class_score_kgp SET ranking_byclass='".$rank."'
										WHERE 1=1
										AND student_id='".$student_id."'
										{$wh}										
										");
					}										
				}
			//end ----------III get ranking by Class-------------------------------			
		}// end funciotn 
		
		//----------------------------------------
		function getStatusOp($isblank,$selected,$isYesNo){
			$opStat="";
			if($isblank==1){
				//$opStat.="<option value=''></option>";
			}

			if($isYesNo==1){
				$active="Yes";
				$inactive="No";
			}else{
				$active="Active";
				$inactive="Inactive";
			}

			$opStat.="<option value='1' ".($selected==1?'selected="selected"':'').">".$active."</option>";
			$opStat.="<option value='0' ".($selected==0?'selected=selected':'').">".$inactive."</option>";
			echo $opStat;
		}

		function gOption($arr,$selected=""){
			if(count($arr)>0){
				$op="";
				foreach ($arr as $key => $value) {
					$op.="<option value='".$key."' ".($key==$selected?"selected":"").">$value</option>";
				}
			}
			return $op;
		}

		function formatSQLDate($date){
			if($date!=""){
				if(strpos($date,"-")!== false){
					$datepart=explode("-", $date);
				}else if(strpos($date,".")!== false){
					$datepart=explode(".", $date);
				}
				else{
					$datepart=explode("/", $date);
				}
				return $datepart[2].'-'.$datepart[1].'-'.$datepart[0];
			}else{
				return "";
			}
		}

		function convertSQLDate($date){
			if($date!="" || $date!='0000-00-00'){
				$datepart=explode("-", $date);
				return $datepart[2].'-'.$datepart[1].'-'.$datepart[0];
			}
			
		}

		function nextTran($typeid,$type){
			//$transno=nextTran("14","Time Table");
			$last_seq=$this->getValue("SELECT sequence FROM sch_z_systype WHERE typeid='".$typeid."'");
			if($last_seq==""){
				$this->runSQL("INSERT INTO sch_z_systype SET sequence=1,type='".$type."',typeid='".$typeid."'");
				$last_seq=1;				
			}
			$this->runSQL("UPDATE sch_z_systype SET sequence=sequence+1 WHERE typeid='".$typeid."'");

			return $last_seq;
		}
	
		
		public function RankSubjMonthKgp($program_id="",$school_level_id="",$adcademic_year_id="",$school_id="",$classid="",$subject_id="",$studenid="",$exam_type=""){
				//-------------I get ranking by subject ------------
						$selelsql="SELECT				
										sch_subject_score_kgp.studenid,
										sch_subject_score_kgp.total_score,
										FIND_IN_SET(
													total_score,
													(
													SELECT
													GROUP_CONCAT(
													total_score
													ORDER BY
													total_score DESC
													)
													FROM
													sch_subject_score_kgp
													)
										)AS rank
										FROM
											sch_subject_score_kgp
										WHERE 1=1
										AND program_id='".$program_id."'
										AND school_level_id='".$school_level_id."'
										AND adcademic_year_id='".$adcademic_year_id."'
										AND school_id='".$school_id."'
										AND classid='".$classid."'
										AND subject_id='".$subject_id."'				
										ORDER BY
										total_score DESC
										";
			
			$data=$this->getTable($selelsql);
				if(count($data)>0){					
					foreach($data as $row){ 						
						$student_id=$row['studenid'];
						$rank=$row['rank'];
						$this->runSQL("DELETE FROM sch_subject_score_kgp
									   WHERE 1=1
										AND student_id='".$student_id."'
										AND program_id='".$program_id."'
										AND school_level_id='".$school_level_id."'
										AND adcademic_year_id='".$adcademic_year_id."'
										AND school_id='".$school_id."'
										AND classid='".$classid."'
										AND subject_id='".$subject_id."'
									   ");
						$this->runSQL("UPDATE sch_subject_score_kgp SET
										ranking='".$rank."'
										WHERE 1=1
										AND student_id='".$student_id."'
										AND program_id='".$program_id."'
										AND school_level_id='".$school_level_id."'
										AND adcademic_year_id='".$adcademic_year_id."'
										AND school_id='".$school_id."'
										AND classid='".$classid."'
										AND subject_id='".$subject_id."'
										");
					}										
				}// end ==========================================================
				//-------------II get score group by class----------------------------
						 $selelsql1="SELECT							
										SUM(total_score) AS total_score,
										SUM(averag_coefficient) AS averag_coefficient,
										sch_subject_score_kgp.student_id
										FROM
											sch_subject_score_kgp
										WHERE 1=1							
										AND program_id='".$program_id."'
										AND school_level_id='".$school_level_id."'
										AND adcademic_year_id='".$adcademic_year_id."'
										AND school_id='".$school_id."'
										AND classid='".$classid."'
										AND subject_id='".$subject_id."'
										GROUP BY
										student_id
										";
			$data1=$this->getTable($selelsql1);
			if(count($data1)>0){					
					foreach($data1 as $row1){
						$studenid=$row1['student_id'];
						$student_id=$row1['student_id'];
						$total_score=$row1['total_score'];
						$averag_coefficient=($row1['averag_coefficient']==0?1:$row1['averag_coefficient']);
									
						$this->runSQL("DELETE FROM sch_class_score_kgp
										WHERE 1=1
										AND school_level_id='".$school_level_id."'
										AND adcademic_year_id='".$adcademic_year_id."'
										AND school_id='".$school_id."'
										AND classid='".$classid."'
										AND subject_id='".$subject_id."'
										AND student_id='".$student_id."'
										");
						$this->runSQL("INSERT INTO sch_class_score_kgp SET 
										school_level_id='".$school_level_id."',
										adcademic_year_id='".$adcademic_year_id."',
										school_id='".$school_id."',
										classid='".$classid."',
										subject_id='".$subject_id."',										
										student_id='".$student_id."',					
										avg_score='".$total_score/$averag_coefficient."',
										averag_coefficient='".$averag_coefficient."',
										total_score='".$total_score."'						
										");
					}										
				}// end ==========================================================
				//-------------III get ranking by Class-------------------------------
					$selelsql2="SELECT
										sch_class_score_kgp.avg_score,										
										FIND_IN_SET(
													avg_score,
													(
													SELECT
													GROUP_CONCAT(
													avg_score
													ORDER BY
													avg_score DESC
													)
													FROM
													sch_class_score_kgp
													)
										)AS rank
										FROM sch_class_score_kgp
										ORDER BY avg_score DESC
										";
			$data2=$this->getTable($selelsql2);
			if(count($data2)>0){					
					foreach($data2 as $row2){						
						$student_id=$row2['student_id'];
						$rank=$row2['rank'];									
						$this->runSQL("UPDATE sch_class_score_kgp SET ranking_class='".$rank."'
										WHERE 1=1
										AND student_id='".$student_id."'
										AND school_level_id='".$school_level_id."'
										AND adcademic_year_id='".$adcademic_year_id."'
										AND school_id='".$school_id."'
										AND classid='".$classid."'
										AND subject_id='".$subject_id."'										
										");
					}										
				}
			//end ----------III get ranking by Class-------------------------------			
		}// end funciotn 

		public function RankClassMonthKgp($programid="",$schlevelid="",$yearid="",$classid="",$subjid="",$studenid="",$totStudnum=""){
				$selelsql="SELECT
							sch_class_score_kgp.avg_score,
							sch_class_score_kgp.ranking_class,
							sch_class_score_kgp.student_id,
							sch_class_score_kgp.teacher_id,
							sch_class_score_kgp.subject_id,
							sch_class_score_kgp.class_id,
							sch_class_score_kgp.grade_level_id,
							sch_class_score_kgp.adcademic_year_id,
							sch_class_score_kgp.school_level_id,
							sch_class_score_kgp.program_id,
							sch_class_score_kgp.school_id,
							sch_class_score_kgp.exam_type,
							sch_class_score_kgp.total_score,
							FIND_IN_SET(
										avg_score,
										(
										SELECT
										GROUP_CONCAT(
										avg_score
										ORDER BY
										avg_score DESC
										)
										FROM
										sch_class_score_kgp
										)
							)AS rank
									FROM
							sch_class_score_kgp
							ORDER BY avg_score DESC
									";
			$data=$this->getTable($selelsql);
			if(count($data)>0){					
					foreach($data as $row){
						$programid=$row['programid'];
						$schlevelid=$row['schlevelid'];
						$yearid=$row['yearid'];
						$classid=$row['classid'];
						$subjid=$row['subjid'];
						$student_id=$row['student_id'];
						$rank=$row['rank'];
						$this->runSQL("UPDATE sch_class_score_kgp SET ranking_class='".$rank."' WHERE student_id='".$student_id."'");
					}										
				}
	
		}
//---------------------------
		public function ScorClassMonthKgp($programid="",$schlevelid="",$yearid="",$classid="",$subjid="",$studenid="",$totStudnum=""){
				$selelsql="SELECT
							sch_subject_score_kgp.class_id,
							sch_subject_score_kgp.grade_level_id,
							sch_subject_score_kgp.adcademic_year_id,
							sch_subject_score_kgp.school_level_id,
							sch_subject_score_kgp.program_id,
							sch_subject_score_kgp.school_id,
							sch_subject_score_kgp.exam_type,
							SUM(total_score) AS total_score,
							SUM(averag_coefficient) AS averag_coefficient,
							sch_subject_score_kgp.student_id
							FROM
								sch_subject_score_kgp
							GROUP BY
								student_id

				";
			$data=$this->getTable($selelsql);
			if(count($data)>0){					
					foreach($data as $row){
						$programid=$row['programid'];
						$schlevelid=$row['schlevelid'];
						$yearid=$row['yearid'];
						$student_id=$row['student_id'];
						$class_id=$row['class_id'];
						$total_score=$row['total_score'];
						$averag_coefficient=($row['averag_coefficient']==0?1:$row['averag_coefficient']);
						
						$this->runSQL("DELETE FROM sch_class_score_kgp  WHERE student_id='".$student_id."'");
						$this->runSQL("INSERT INTO sch_class_score_kgp SET 
										student_id='".$student_id."',
										class_id='".$class_id."',						
										avg_score='".$total_score/$averag_coefficient."',
										averag_coefficient='".$averag_coefficient."',
										total_score='".$total_score."'						
									");
					}										
				}

//-----------------
$selelsql1="SELECT
							sch_class_score_kgp.avg_score,
							sch_class_score_kgp.ranking_class,
							sch_class_score_kgp.student_id,
							sch_class_score_kgp.teacher_id,
							sch_class_score_kgp.subject_id,
							sch_class_score_kgp.class_id,
							sch_class_score_kgp.grade_level_id,
							sch_class_score_kgp.adcademic_year_id,
							sch_class_score_kgp.school_level_id,
							sch_class_score_kgp.program_id,
							sch_class_score_kgp.school_id,
							sch_class_score_kgp.exam_type,
							sch_class_score_kgp.total_score,
							FIND_IN_SET(
										avg_score,
										(
										SELECT
										GROUP_CONCAT(
										avg_score
										ORDER BY
										avg_score DESC
										)
										FROM
										sch_class_score_kgp
										)
							)AS rank
									FROM
							sch_class_score_kgp
							ORDER BY avg_score DESC
									";
			$data1=$this->getTable($selelsql1);
			if(count($data)>0){					
					foreach($data1 as $row1){
						$programid=$row1['programid'];
						$schlevelid=$row1['schlevelid'];
						$yearid=$row1['yearid'];
						$classid=$row1['classid'];
						$subjid=$row1['subjid'];
						$student_id=$row1['student_id'];
						$rank=$row1['rank'];
						$this->runSQL("UPDATE sch_class_score_kgp SET ranking_class='".$rank."' WHERE student_id='".$student_id."'");
					}										
				}
//---------------				
		}
		

		function getStudentID($student_id,$schlevelid){
			return $this->getOneRow("SELECT DISTINCT
											smo.moeys_id
										FROM
											v_student_profile spro
										INNER JOIN sch_student_moeysid smo ON smo.studentid = spro.studentid
										WHERE
											spro.studentid = '".$student_id."'
										AND smo.schlevelid='".$schlevelid."'
										");
									}

	function ajax_pagination($total_row, $url, $limit=5){
		//$limit=10; //********** Number for select from DB **********		
		$start=0; //********** Number for start select from DB **********
		$adjacents = 2;
		
		if (isset($_POST["page"])) {
			$page = $_POST["page"];
		}
		else{
			$page=1;
		}
		
		if($page!=0) 
			$start = ($page - 1) * $limit;
		else
			$start = 0;			
		
		$total_pages = $total_row;
		
		if ($page == 0) $page = 1;
		$prev = $page - 1;
		$next = $page + 1;
		$lastpage = ceil($total_pages/$limit);
		$lpm1 = $lastpage - 1;
		
		//=========================================================================			
		$pagination ="";
		$pagination .= "<div class=\"pagination-links clear\"><ul class='pagination'><li>";
			if($lastpage > 1)
			{	
				if ($page > 1) 
					$pagination.= "<a class=\"pagenav\" id='1' href='javascript:void(0)' id=1><span class='fa fa-fast-backward'></span></a>
									<a class=\"pagenav\" href='javascript:void(0)' id='$prev'><span class='fa fa-backward'></span></a>";
				else
					$pagination.= "<span class='fa fa-fast-backward'></span>
									<span class='fa fa-backward'></span>";
				if ($lastpage < 2 + ($adjacents * 2))
				{	
					for ($counter = 1; $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"pagenav\">$counter</span>";
						else
							$pagination.= "<a class=\"pagenav\" href='javascript:void(0)' id='$counter'>$counter</a>";					
					}
				}
				elseif($lastpage >= ($adjacents * 2))	//enough pages to hide some
				{
					//close to beginning; only hide later pages
					
					if($page <= 2 + ($adjacents) && $page >= $lastpage-$adjacents-1)		
					{
					
						for ($counter = 1; $counter < $page - 1 + ($adjacents * 2); $counter++)
						{
							if ($counter == $page)
								$pagination.= "<span class=\"pagenav\">$counter</span>";
							else
								$pagination.= "<a class=\"pagenav\" href='javascript:void(0)' id='$counter'>$counter</a>";					
						}
						$pagination.= "<a class=\"pagenav\" href='javascript:void(0)' id='$lastpage'>$lastpage</a>";		
					}
					//in middle; hide some front and some back
					elseif($page <= 2 + ($adjacents) && $page < $lastpage-$adjacents-1)
					{
						if ($page<4){$page_=3;}
						else{$page_=$page;}
						for ($counter = 1; $counter <=$page_ + ($adjacents); $counter++)
						{
							if ($counter == $page)
								$pagination.= "<span class=\"pagenav\">$counter</span>";
							else
								$pagination.= "<a class=\"pagenav\" href='javascript:void(0)' id='$counter'>$counter</a>";					
						}
						$pagination.= "<a href='javascript:void(0)'>...</a>";
						$pagination.= "<a class=\"pagenav\" href='javascript:void(0)' id='$lastpage'>$lastpage</a>";		
					}
					elseif($page > 2 + ($adjacents) && $page >= $lastpage-$adjacents-1)		
					{
						$pagination.="<a class=\"pagenav\" href='javascript:void(0)' id=1>1</a>";
						$pagination.= "<a href='javascript:void(0)'>...</a>";
						if($page>$lastpage-3){$page_=$lastpage-4;}
						else{$page_=$page-2;}
						for ($counter = $page_; $counter <= $lastpage; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<span class=\"pagenav\">$counter</span>";
							else
								$pagination.= "<a class=\"pagenav\" href='javascript:void(0)' id='$counter'>$counter</a>";					
						}		
					}
					elseif($page > 2 + ($adjacents) && $page < $lastpage-$adjacents-1)		
					{
						$pagination.="<a class=\"pagenav\" href='javascript:void(0)' id=1>1</a>";
						$pagination.= "<a href='javascript:void(0)'>...</a>";
						for ($counter = $page-2; $counter < $page - 1 + ($adjacents * 2); $counter++)
						{
							if ($counter == $page)
								$pagination.= "<span class=\"pagenav\">$counter</span>";
							else
								$pagination.= "<a class=\"pagenav\" href='javascript:void(0)' id='$counter'>$counter</a>";					
						}
						$pagination.= "<a href='javascript:void(0)'>...</a>";
						$pagination.= "<a class=\"pagenav\" href='javascript:void(0)' id='$lastpage'>$lastpage</a>";		
					}
					//close to end; only hide early pages
				}
				if ($page < $counter - 1) 
					$pagination.= "<a class=\"pagenav\" href='javascript:void(0)' id='$next'><span class='fa fa-forward'></span></a>
									<a class=\"pagenav\" href='javascript:void(0)' id='$lastpage'><span class='fa fa-fast-forward'></span></a>";
				else
					$pagination.="<span class='fa fa-forward'></span>
									<span class='fa fa-fast-forward'></span>";
		
			}
			else
			$pagination.="&nbsp;";
			$pagination.="</li></ul></div>";			
			$arr_pag= array();
			$arr_pag['start']=$start;
			$arr_pag['limit']=$limit;
			$arr_pag['pagination']=$pagination;				
			return $arr_pag;
		}
		
		// ------------------------------------ start long tha working---------------------------------------
		function set_montly_permission($y,$m,$n)
		{
			$this->num_perm = $this->getValue("SELECT
													COUNT(*)
												FROM
													(SELECT
															studentid,
															SUM(DATEDIFF(to_date, from_date) + 1) AS num
														FROM
															sch_student_permission att
														WHERE
															yearid = ".$y."
														AND MONTH (att.to_date) = ".$m."
														-- AND permis_status = 0
														GROUP BY studentid
														HAVING num>=".$n."
													) AS tt;
											")-0;	  	
		}		
		function get_montly_permission()
		{
			if($this->num_perm > 1)
			{
				return $this->num_perm." Students";
			}
			else
			{
				return $this->num_perm." Student";
			}	
		}

		function set_yearly_permission($y,$n)
		{
			/*SELECT
					COUNT(*) AS num_perm
				FROM
					sch_student_permission att
				WHERE
					yearid = '".$y."'
				AND permis_status = 0
				GROUP BY
					studentid
				HAVING
					num_perm >= '".$n."'*/

			$this->re_perm = $this->getValue("SELECT
													COUNT(*)
												FROM
													(	SELECT
																studentid,
																classid,
																yearid,
																SUM(DATEDIFF(to_date, from_date) + 1) AS num_perm
															FROM
																sch_student_permission att
															WHERE
																yearid = ".$y."
															-- AND permis_status = 0				
															GROUP BY studentid,
															classid,
															yearid
															HAVING num_perm>='".$n."'	
													) ssh
												")-0; 	

			

		}
		function get_yearly_permission()
		{
			if($this->re_perm > 1)
			{
				return $this->re_perm." Students";
			}
			else
			{
				return $this->re_perm." Student";
			}	
		}
		function set_montly_permission_bigger_one($y,$m,$n)
		{
			/*SELECT
					studentid,
					COUNT(studentid) AS num_per
				FROM
					sch_student_permission att
				WHERE
					yearid = '".$y."'
				AND MONTH (att.to_date) = '".$m."'
				AND DATEDIFF(to_date, from_date) + 1 >= '".$n."'
				GROUP BY
					studentid*/
			$this->val_perm = $this->getValue("SELECT
													COUNT(*) as num_perm
												FROM
													(	SELECT
															studentid,
															SUM(DATEDIFF(to_date, from_date) + 1) AS num
														FROM
															sch_student_permission att
														WHERE
															yearid = ".$y."
														AND MONTH (att.to_date) = ".$m."
														-- AND permis_status = 0
														GROUP BY studentid
														HAVING num>=".$n."
													) scov
												")-0;	
												  	
		}
		function get_montly_permission_bigger_one()
		{
			if($this->val_perm > 1)
			{
				return $this->val_perm." Students";
			}
			else
			{
				return $this->val_perm." Student";
			}	
		}
		public function GetYear($setyear)
		{
			return $this->getValue("SELECT
									ssy.sch_year
								FROM
									sch_school_year ssy
								WHERE
									ssy.yearid = '".$setyear."'
							");
		}

		public function set_messages($userId='')
		{
			if($userId!="admin"){
				$this->totaluser = $this->getValue("SELECT
														COUNT(schm.user_name) AS totaluser
													FROM
														sch_message schm
													WHERE 1=1
													AND schm.user_name = '".$userId."'
													OR	schm.sent_from = '".$userId."' 
													OR schm.sent_to = '".$userId."'

												");
			}
			else
			{
				$this->totaluser = $this->getValue("SELECT
															COUNT(*) AS totaluser
														FROM
															sch_message schm
														WHERE 1=1
													");	
			}
			
		}
		public function get_messages()
		{
			if($this->totaluser > 0){
				return $this->totaluser;
			}
		}	

		function limit_text($text, $limit) {
			if (str_word_count($text, 0) > $limit) {
				$words = str_word_count($text, 2);
				$pos = array_keys($words);
				$text = substr($text, 0, $pos[$limit]) . '...';
			}
			return $text;
		}
		//==============================
		function update_rank_student($get_arr)
		{
			$ci=& get_instance();  
			/*=============== Read Me ===============
				$v_subj->is_class_participation == 3 is Group Attendance
				$v_subj->is_class_participation == 1 is Group Class Particiption
				$v_subj->is_class_participation == 2 is Group Homework
					(sum score in week*max score group subject/sum score subject)
					- Mid-Term
					- Final
				$v_subj->is_class_participation == 4 is Group Diligence
					- Mid-Term
					- Final
				$v_subj->is_class_participation == 0 is  Group subject for exam mid-term and final
			*/
			
			$sub_xamp = $ci->db->query("SELECT
												sch_subject_iep.subjectid,
												sch_subject_iep.subj_type_id,
												sch_subject_iep.short_sub,
												sch_subject_iep.max_score
												FROM
												sch_subject_iep
												WHERE 1=1 
												AND sch_subject_iep.schoolid   = '".$get_arr['schoolid']."'
												AND sch_subject_iep.schlevelid = '".$get_arr['schlevelid']."'
												ORDER BY subj_type_id ASC");
			$arr_subject = array();
			if($sub_xamp->num_rows() > 0){
				foreach($sub_xamp->result() as $rowg_sub){
					$arr_subject[$rowg_sub->subj_type_id][$rowg_sub->subjectid] = array("short_sub"=>$rowg_sub->short_sub,"max_score"=>$rowg_sub->max_score);
				}
			}

			$sql_test = $ci->db->query("SELECT
											det.studentid,
											ord.exam_typeid,
											ord.group_subject,
											ord.weekid,
											det.score_num,
											sch_subject_type_iep.subject_type,
											sch_subject_type_iep.is_class_participation,
											det.subjectid,
											ord.count_subject,
											ord.typeno,
											sch_subject_iep.max_score
											FROM
											sch_score_iep_entryed_order AS ord
											INNER JOIN sch_score_iep_entryed_detail AS det ON ord.typeno = det.typeno AND ord.type = det.type
											INNER JOIN sch_subject_type_iep ON det.group_subject = sch_subject_type_iep.subj_type_id
											INNER JOIN sch_subject_iep ON det.subjectid = sch_subject_iep.subjectid
											WHERE 1=1
											-- AND ord.weekid = 1
											AND ord.schoolid = '".$get_arr['schoolid']."'
											AND ord.schlevelid = '".$get_arr['schlevelid']."'
											-- AND ord.group_subject= '16'
											AND ord.yearid = '".$get_arr['yearid']."'
											AND ord.gradelevelid = '".$get_arr['gradelevelid']."'
											AND ord.classid = '".$get_arr['classid']."'
											AND ord.termid = '".$get_arr['termid']."'
											AND ord.exam_typeid = '".$get_arr['exam_type']."'
											-- AND det.studentid = 127
											ORDER BY det.studentid,ord.group_subject,ord.weekid");
			
			$arr_hw = array();
			$arr_dl = array();
			$arr_ex = array();
			$arr_cp = array();
			$arr_att= array();
			$arr_week_h = array();
			$arr_week_d = array();
			$score_max_sub    = array();
			$score_max_sub_dl = array();

			$arr_group_su = array();
			$arr_group_cp = array();
			$arr_group_att = array();

			$arr_score_st = array();
			$arr_student  = array();
			if($sql_test->num_rows() > 0)
			{
				foreach($sql_test->result() as $rowt){
					$arr_student[]  = $rowt->studentid;
					if($rowt->is_class_participation == 0){ // subject
						$arr_ex[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]=$rowt->score_num;
					}
					else if($rowt->is_class_participation == 1){ // class participation
						if(isset($arr_cp[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
							$arr_cp[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]=($arr_cp[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]+$rowt->score_num);
						}else{
							$arr_cp[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]=$rowt->score_num;
						}
						// array sum max score
						if(isset($arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
							$arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = ($arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]+$rowt->max_score);
						}else{
							$arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $rowt->max_score;
						}
						// array show max score
						if(isset($arr_group_cp[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
							if($arr_group_cp[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] < $arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]){
								$arr_group_cp[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid];
							}
						}
						else{
							$arr_group_cp[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $rowt->max_score;
						}
					}
					else if($rowt->is_class_participation == 3){ // attendance
						if(isset($arr_att[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
							$arr_att[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]=($arr_att[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]+$rowt->score_num);
						}else{
							$arr_att[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]=$rowt->score_num;
						}
						// array sum max score
						if(isset($arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
							$arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = ($arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]+$rowt->max_score);
						}else{
							$arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $rowt->max_score;
						}
						// array show max score
						if(isset($arr_group_att[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
							if($arr_group_att[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] < $arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]){
								$arr_group_att[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid];
							}
						}
						else{
							$arr_group_att[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $rowt->max_score;
						}
						
					}
					else if($rowt->is_class_participation == 2) // homework
					{
						if(isset($arr_subject[$rowt->group_subject][$rowt->subjectid])){
							if(isset($score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
								$score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]+$arr_subject[$rowt->group_subject][$rowt->subjectid]['max_score'];
							}else{
								$score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $arr_subject[$rowt->group_subject][$rowt->subjectid]['max_score'];
							}
						}
						if(isset($arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
							if($arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]<$score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]){
								$arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid];
							}
						}else{
							$arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = 0;
						}
						if(isset($arr_hw[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
							$arr_hw[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=($arr_hw[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]+$rowt->score_num);
						}else{
							$arr_hw[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=$rowt->score_num;
						}

						if(!in_array($rowt->weekid,$arr_week_h)){
							$arr_week_h[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=$rowt->weekid;
						}
					}else if($rowt->is_class_participation == 4) // diligen
					{
						if(isset($arr_subject[$rowt->group_subject][$rowt->subjectid])){
							if(isset($score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
								$score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]+$arr_subject[$rowt->group_subject][$rowt->subjectid]['max_score'];
							}else{
								$score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $arr_subject[$rowt->group_subject][$rowt->subjectid]['max_score'];
							}
						}
						if(isset($arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
							if($arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]  < $score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]){
								$arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid];
							}
						}else{
							$arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = 0;
						}
						if(isset($arr_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
							$arr_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=($arr_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]+$rowt->score_num);
						}else{
							$arr_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=$rowt->score_num;
						}
						if(!in_array($rowt->weekid,$arr_week_d)){
							$arr_week_d[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=$rowt->weekid;
						}
					}
				}
			}
			
			$sqlg = $ci->db->query("SELECT
										sch_subject_type_iep.subj_type_id,
										sch_subject_type_iep.subject_type,
										sch_subject_type_iep.main_type,
										sch_subject_type_iep.orders,
										sch_subject_type_iep.is_group_calc_percent,
										sch_subject_type_iep.is_class_participation
										FROM
										sch_subject_type_iep
										WHERE 1=1 
										AND schoolid='".$get_arr['schoolid']."'
										AND schlevelid='".$get_arr['schlevelid']."'
										ORDER BY orders");
			$arr_group_sub = array();
			if($sqlg->num_rows() > 0){
				foreach($sqlg->result() as $rowgsub){
					$arr_group_sub[] = array('groupid'=>$rowgsub->subj_type_id,
											'subject_type'=>$rowgsub->subject_type,
											'max_sco_g'=>$rowgsub->is_group_calc_percent,
											'is_cp'=>$rowgsub->is_class_participation
											);
				}
			}
			if(isset($arr_student)){
				foreach($arr_student as $val_studentid){
						$sql_check = $ci->db->query("SELECT count(*) as amt_count
													FROM sch_iep_total_score_entry
													WHERE 1=1 
													AND schoolid='".$get_arr['schoolid']."'
													AND schlevelid='".$get_arr['schlevelid']."'
													AND yearid='".$get_arr['yearid']."'
													AND termid='".$get_arr['termid']."'
													AND gradeid='".$get_arr['gradelevelid']."'
													AND classid='".$get_arr['classid']."'
													AND examtypeid='".$get_arr['exam_type']."'
													AND studentid='".$val_studentid."'
													")->row();
						//if($sql_check->num_rows() > 0){
							if($sql_check->amt_count == 0){
								$ci->db->query("INSERT INTO sch_iep_total_score_entry 
														SET 
														schoolid='".$get_arr['schoolid']."',
														schlevelid='".$get_arr['schlevelid']."',
														yearid='".$get_arr['yearid']."',
														termid='".$get_arr['termid']."',
														gradeid='".$get_arr['gradelevelid']."',
														classid='".$get_arr['classid']."',
														examtypeid='".$get_arr['exam_type']."',
														studentid='".$val_studentid."'");
							}
						//}

						$j = 1;
						$sql_exmp = $ci->db->query("SELECT
														sch_student_examtype_iep.examtypeid
													FROM sch_student_examtype_iep
													WHERE examtypeid='".$get_arr['exam_type']."'
													ORDER BY examtypeid ASC");
						$st_td = "";
						$total_all = 0;
						$sum_sub1 = 0;
						if($sql_exmp->num_rows() > 0)
						{
							foreach($sql_exmp->result() as $rexamp)
							{
								
								///$sum_sub_examp = 0;
								if(count($arr_group_sub) > 0)
								{
									foreach($arr_group_sub as $rg)
									{
										if($rg['is_cp'] == 3){ // attendance
											if(isset($arr_subject[$rg['groupid']])){
												
												foreach($arr_subject[$rg['groupid']] as $kk=>$rsub){
													$sum_att      = isset($arr_att[$val_studentid][$rexamp->examtypeid][$rg['groupid']][$kk])?$arr_att[$val_studentid][$rexamp->examtypeid][$rg['groupid']][$kk]:0;
													$max_group_as = isset($arr_group_att[$rexamp->examtypeid][$rg['groupid']][$kk])?$arr_group_att[$rexamp->examtypeid][$rg['groupid']][$kk]:1;
													$total_att    = floor((($sum_att*$rg['max_sco_g'])/$max_group_as)*100)/100;
													$total_all+=$total_att;
													$ci->db->query("UPDATE sch_iep_total_score_entry 
																		SET attandance='".$total_att."'
																		WHERE 1=1 
																		AND schoolid='".$get_arr['schoolid']."'
																		AND schlevelid='".$get_arr['schlevelid']."'
																		AND yearid='".$get_arr['yearid']."'
																		AND termid='".$get_arr['termid']."'
																		AND gradeid='".$get_arr['gradelevelid']."'
																		AND classid='".$get_arr['classid']."'
																		AND examtypeid='".$get_arr['exam_type']."'
																		AND studentid='".$val_studentid."'");
												}
												
											}
										}else if($rg['is_cp'] == 1){ // class paticipation
											if(isset($arr_subject[$rg['groupid']])){

												foreach($arr_subject[$rg['groupid']] as $kk=>$rsub){
													$sum_cp      = isset($arr_cp[$val_studentid][$rexamp->examtypeid][$rg['groupid']][$kk])?$arr_cp[$val_studentid][$rexamp->examtypeid][$rg['groupid']][$kk]:0;
													$max_group_cp = isset($arr_group_cp[$rexamp->examtypeid][$rg['groupid']][$kk])?$arr_group_cp[$rexamp->examtypeid][$rg['groupid']][$kk]:1;
													$total_cp    = floor((($sum_cp*$rg['max_sco_g'])/$max_group_cp)*100)/100;
													$total_all+=$total_cp;
													$ci->db->query("UPDATE sch_iep_total_score_entry 
																		SET class_participation='".$total_cp."'
																		WHERE 1=1 
																		AND schoolid='".$get_arr['schoolid']."'
																		AND schlevelid='".$get_arr['schlevelid']."'
																		AND yearid='".$get_arr['yearid']."'
																		AND termid='".$get_arr['termid']."'
																		AND gradeid='".$get_arr['gradelevelid']."'
																		AND classid='".$get_arr['classid']."'
																		AND examtypeid='".$get_arr['exam_type']."'
																		AND studentid='".$val_studentid."'");
												}
											}
											
										}else if($rg['is_cp'] == 0){ // Subject
											$sum_sub = 0;
											if(isset($arr_subject[$rg['groupid']])){
												foreach($arr_subject[$rg['groupid']] as $kk=>$rsub){
													$sum_sub+= isset($arr_ex[$val_studentid][$rexamp->examtypeid][$rg['groupid']][$kk])?$arr_ex[$val_studentid][$rexamp->examtypeid][$rg['groupid']][$kk]:0;
												}
												$sum_sub1+=$sum_sub;
											}
											$ci->db->query("UPDATE sch_iep_total_score_entry 
																		SET subject='".$sum_sub1."'
																		WHERE 1=1 
																		AND schoolid='".$get_arr['schoolid']."'
																		AND schlevelid='".$get_arr['schlevelid']."'
																		AND yearid='".$get_arr['yearid']."'
																		AND termid='".$get_arr['termid']."'
																		AND gradeid='".$get_arr['gradelevelid']."'
																		AND classid='".$get_arr['classid']."'
																		AND examtypeid='".$get_arr['exam_type']."'
																		AND studentid='".$val_studentid."'");
											$total_all+=$sum_sub;
										}else if($rg['is_cp'] == 2){ // Homework
											$result_hw = 0;
											if(isset($arr_week_h[$rexamp->examtypeid][$rg['groupid']])){
												$total_h = 0;
												$sum_count_h = 0;
												foreach($arr_week_h[$rexamp->examtypeid][$rg['groupid']] as $ks=>$vs)
												{
													if(isset($arr_hw)){
														$sum_count_h++;
														$max_group  = isset($arr_group_su[$rexamp->examtypeid][$rg['groupid']][$vs])?$arr_group_su[$rexamp->examtypeid][$rg['groupid']][$vs]:1;
														$sum_h  = isset($arr_hw[$val_studentid][$rexamp->examtypeid][$rg['groupid']][$vs])?$arr_hw[$val_studentid][$rexamp->examtypeid][$rg['groupid']][$vs]:0;
														$sum_hw = floor((($sum_h*$rg['max_sco_g'])/$max_group)*100)/100;
														$total_h+=$sum_hw;
													}
												}
												if($sum_count_h == 0){
													$sum_count_h = 1;
												}
												$result_hw += floor(($total_h/$sum_count_h)*100)/100;
												$total_all+=$result_hw;
												$ci->db->query("UPDATE sch_iep_total_score_entry 
																							SET homework='".$result_hw ."'
																							WHERE 1=1 
																							AND schoolid='".$get_arr['schoolid']."'
																							AND schlevelid='".$get_arr['schlevelid']."'
																							AND yearid='".$get_arr['yearid']."'
																							AND termid='".$get_arr['termid']."'
																							AND gradeid='".$get_arr['gradelevelid']."'
																							AND classid='".$get_arr['classid']."'
																							AND examtypeid='".$get_arr['exam_type']."'
																							AND studentid='".$val_studentid."'");
											}
										}else if($rg['is_cp'] == 4){ // Diligence
											if(isset($arr_week_d[$rexamp->examtypeid][$rg['groupid']])){
												$total_d = 0;
												$sum_count_d = 0;
												foreach($arr_week_d[$rexamp->examtypeid][$rg['groupid']] as $ks=>$vs){
													if(isset($arr_dl)){
														$total_count = 1;
														$sum_count_d++;
														$max_group = isset($arr_group_su[$rexamp->examtypeid][$rg['groupid']][$vs])?$arr_group_su[$rexamp->examtypeid][$rg['groupid']][$vs]:1;
														$sum_d = isset($arr_dl[$val_studentid][$rexamp->examtypeid][$rg['groupid']][$vs])?$arr_dl[$val_studentid][$rexamp->examtypeid][$rg['groupid']][$vs]:0;
														$sum_sc_d = round(($sum_d*$rg['max_sco_g'])/$max_group,2);
														$total_d+=$sum_sc_d;
													}
												}
												if($sum_count_d == 0){
													$sum_count_d =1;
												}
												$result_dl = 0;
												$result_dl = floor(($total_d/$sum_count_d)*100)/100;
												$total_all+=$result_dl;
												$ci->db->query("UPDATE sch_iep_total_score_entry 
																							SET diligence='".$result_dl."'
																							WHERE 1=1 
																							AND schoolid='".$get_arr['schoolid']."'
																							AND schlevelid='".$get_arr['schlevelid']."'
																							AND yearid='".$get_arr['yearid']."'
																							AND termid='".$get_arr['termid']."'
																							AND gradeid='".$get_arr['gradelevelid']."'
																							AND classid='".$get_arr['classid']."'
																							AND examtypeid='".$get_arr['exam_type']."'
																							AND studentid='".$val_studentid."'");
											}
										} // end Diligence
									}
								} // end count
							}
						} // end num_rows

						$ci->db->query("UPDATE sch_iep_total_score_entry 
										SET total_score='".$total_all."'
										WHERE 1=1 
										AND schoolid='".$get_arr['schoolid']."'
										AND schlevelid='".$get_arr['schlevelid']."'
										AND yearid='".$get_arr['yearid']."'
										AND termid='".$get_arr['termid']."'
										AND gradeid='".$get_arr['gradelevelid']."'
										AND classid='".$get_arr['classid']."'
										AND examtypeid='".$get_arr['exam_type']."'
										AND studentid='".$val_studentid."'");
				} // end each array
			}// end check array
			$sql_rank = $ci->db->query("SELECT
											sch_iep_total_score_entry.studentid,
											sch_iep_total_score_entry.total_score,
											sch_iep_total_score_entry.rank,
											sch_iep_total_score_entry.is_change_class
											FROM
											sch_iep_total_score_entry
											WHERE 1=1 
											AND schoolid='".$get_arr['schoolid']."'
											AND schlevelid='".$get_arr['schlevelid']."'
											AND yearid='".$get_arr['yearid']."'
											AND termid='".$get_arr['termid']."'
											AND gradeid='".$get_arr['gradelevelid']."'
											AND classid='".$get_arr['classid']."'
											AND examtypeid='".$get_arr['exam_type']."'
											ORDER BY total_score DESC");
			$ii = 1;
			$arr_rank = array();
			$rank  = 1;
			$rank1 = 1;
			if($sql_rank->num_rows > 0){
				foreach($sql_rank->result() as $row_rank){
					if(in_array($row_rank->total_score,$arr_rank)){
						$rank1=$rank;
					}else{
						$rank = $ii;
						$rank1= $ii;
					}
					$arr_rank[] = $row_rank->total_score;
					$ii++;
					$ci->db->query("UPDATE sch_iep_total_score_entry SET rank='".$rank1."' 
											WHERE 1=1 
											AND schoolid='".$get_arr['schoolid']."'
											AND schlevelid='".$get_arr['schlevelid']."'
											AND yearid='".$get_arr['yearid']."'
											AND termid='".$get_arr['termid']."'
											AND gradeid='".$get_arr['gradelevelid']."'
											AND classid='".$get_arr['classid']."'
											AND examtypeid='".$get_arr['exam_type']."'
											AND studentid='".$row_rank->studentid."'");
					
				}
			}
			unset($arr_hw);
			unset($arr_dl);
			unset($arr_ex);
			unset($arr_cp);
			unset($arr_att);
			unset($arr_week_h);
			unset($arr_week_d);
			unset($score_max_sub);
			unset($score_max_sub_dl);

			unset($arr_group_su);
			unset($arr_group_cp);
			unset($arr_group_att);

			unset($arr_score_st);
		// =========================================================
		}// end function
		// =============================
		function rank_final($get_arr_para)
		{
			$ci=& get_instance();
			//$arr_para = array("schoolid"=>$schoolid,"schlevelid"=>$schlavelid,"yearid"=>$yearid,"gradelevelid"=>$grandlavelid,"classid"=>$classid,"termid"=>$termid,"studentid"=>$studentid);
			$sql_m = $ci->db->query("SELECT 
										tbl_max.studentid,
										tbl_max.max_rank
										FROM
										(
										SELECT
										sch_iep_total_score_entry.studentid,
										(SUM(sch_iep_total_score_entry.total_score/2))/2 AS max_rank
										FROM
										sch_iep_total_score_entry
										WHERE 1=1
										AND schoolid='".$get_arr_para['schoolid']."'
											AND schlevelid='".$get_arr_para['schlevelid']."'
											AND yearid='".$get_arr_para['yearid']."'
											AND termid='".$get_arr_para['termid']."'
											AND gradeid='".$get_arr_para['gradelevelid']."'
											AND classid='".$get_arr_para['classid']."'
											-- AND studentid='".$get_arr_para['studentid']."'
										GROUP BY studentid
										ORDER BY examtypeid,total_score DESC
										) AS tbl_max
										ORDER BY tbl_max.max_rank DESC");
			$j = 1;
			$rank  = 1;
			$rank1 = 1;
			$arr_rank_f = array();
			$arr_st_rank= array();
			if($sql_m->num_rows() > 0)
			{
				foreach($sql_m->result() as $row_m){
					if(in_array($row_m->max_rank,$arr_rank_f)){
						$rank1=$rank;
					}else{
						$rank = $j;
						$rank1= $j;
					}
					$j++;
					$arr_rank_f[] = $row_m->max_rank;
					$arr_st_rank[$row_m->studentid]= $rank1;
				}

			}else
			{
				$j++;
				//$arr_st_rank[$get_arr_para['studentid']]= 500;
			}
			if(!isset($arr_st_rank[$get_arr_para['studentid']])){
				return $arr_st_rank[$get_arr_para['studentid']]= $j;
			}else{
				return $arr_st_rank[$get_arr_para['studentid']];
			}
			
		}
		function total_semester($arr_sem){
			$ci=& get_instance();
			$sql_semester = $ci->db->query("SELECT
											tbl.schoolid,
											tbl.schlevelid,
											tbl.yearid,
											tbl.termid,
											tbl.gradeid,
											tbl.classid,
											tbl.examtypeid,
											tbl.studentid,
											(SUM(tbl.attandance)/4) AS attandance,
											(SUM(tbl.class_participation)/4) AS class_participation,
											(SUM(tbl.homework)/4) AS homework,
											(SUM(tbl.diligence)/4) AS diligence,
											(SUM(tbl.`subject`)/4) AS `subject`,
											(SUM(tbl.total_score)/4) AS total_score
											FROM
											sch_iep_total_score_entry AS tbl
											INNER JOIN sch_school_term ON tbl.termid = sch_school_term.termid
											WHERE 1=1
											AND tbl.schoolid='".$arr_sem['schoolid']."'
											AND tbl.schlevelid='".$arr_sem['schlevelid']."'
											AND tbl.yearid='".$arr_sem['yearid']."'
											AND sch_school_term.semesterid='".$arr_sem['semesterid']."'
											AND tbl.gradeid='".$arr_sem['grade_levelid']."'
											AND tbl.classid='".$arr_sem['classid']."'
											GROUP BY
											tbl.studentid
											ORDER BY
											SUM(tbl.total_score) DESC
										");
			$arr_rank   = array();
			$arr_rank_r = array();
			$rank = 0;
			$rank1= 0;
			$j = 1;
			$arr_score = array();
			$total = 0;
			if($sql_semester->num_rows() > 0){
				foreach($sql_semester->result() as $row_sm){
					$attandance  = floor($row_sm->attandance*100)/100;
					$class_participation = floor($row_sm->class_participation*100)/100;
					$homework    = floor($row_sm->homework*100)/100;
					$diligence   = floor($row_sm->diligence*100)/100;
					$subject     = floor($row_sm->subject*100)/100;
					$total       = $attandance+$class_participation+$homework+$diligence+$subject;
					$arr_score[$row_sm->studentid] = $total;
					
					if(in_array($total,$arr_rank)){
						$rank1=$rank;
					}else{
						$rank = $j;
						$rank1= $j;
					}
					$j++;
					$arr_rank[] = $total;
					$arr_rank_r[$row_sm->studentid]= $rank1;
				}
			}
			$result_score = 0;
			if(isset($arr_score[$arr_sem['studentid']])){
				$result_score = $arr_score[$arr_sem['studentid']];
			}
			$result_rank = 0;
			if(isset($arr_rank_r[$arr_sem['studentid']])){
				$result_rank = $arr_rank_r[$arr_sem['studentid']];
			}
			else{
				$result_rank = $j;
			}
			$arr_all = array("score"=>$result_score,"rank"=>$result_rank);
			return $arr_all;
		}
		
		// ======================== function GEP =========================
		function fn_result_gep($arr_gep){
			$ci=& get_instance();
			$sql_subexam = $ci->db->query("SELECT
												gep_order.type,
												gep_order.typeno,
												gep_order.main_tranno,
												gep_order.classid,
												gep_order.exam_typeid,
												gep_order.tran_date,
												gep_order.subjecttypeid,
												gep_order.group_subject,
												gep_order.is_class_participate,
												gep_det.studentid,
												gep_det.subjectid,
												SUM(gep_det.score_num) AS score,
												COUNT(*) AS amt_count,
												SUM(sch_subject_gep.max_score) AS max_score
												FROM
												sch_score_gep_entryed_order AS gep_order
												INNER JOIN sch_score_gep_entryed_detail AS gep_det ON gep_order.type = gep_det.type AND gep_order.typeno = gep_det.typeno
												INNER JOIN sch_subject_gep ON gep_det.subjectid = sch_subject_gep.subjectid
												WHERE 1=1
												AND gep_order.exam_typeid=".$arr_gep['examtype']."
												AND gep_order.course_type = '".$arr_gep['course']."'
												AND gep_order.schoolid   = '".$arr_gep['schoolid'] ."'
												AND gep_order.schlevelid = '".$arr_gep['schoollavel']."'
												AND gep_order.yearid     = '".$arr_gep['yearsid']."'
												AND gep_order.gradelevelid ='".$arr_gep['gradelavel']."'
												AND gep_order.classid = '".$arr_gep['classid']."'
												AND gep_order.termid = '".$arr_gep['termid']."' 
												GROUP BY gep_order.exam_typeid,gep_det.subjectid,gep_det.studentid
											");

			


			$arr_score = array();
			$cp_m  = array();
			$arr_att  = array();
			$arr_mid  = array();
			$arr_hw   = array();
			$max_cp	  = array();
			$max_att = array();
			$max_hw_m = array();
			$max_mid = 0;
			$arr_student = array();
			if($sql_subexam->num_rows() > 0)
			{
				foreach($sql_subexam->result() as $row_exam)
				{
					$arr_student[] = $row_exam->studentid;
					if($row_exam->is_class_participate == 0){ // mid-term subject
						$arr_subj[$row_exam->studentid] = (isset($arr_subj[$row_exam->studentid])?($arr_subj[$row_exam->studentid]+$row_exam->score):$row_exam->score);
					
					}else if($row_exam->is_class_participate == 2)// homework
					{ 
						$arr_hw[$row_exam->studentid] = (isset($arr_hw[$row_exam->studentid])?$arr_hw[$row_exam->studentid]+($row_exam->score):$row_exam->score);
						$val_hw_m = isset($max_hw_m[$row_exam->subjectid])?$max_hw_m[$row_exam->subjectid]:0;
						if($row_exam->max_score > $val_hw_m){
							$max_hw_m[$row_exam->subjectid]= $row_exam->max_score;
						}
					}
					else if($row_exam->is_class_participate == 3)// attandance
					{ 
						$arr_att[$row_exam->studentid] = (isset($arr_att[$row_exam->studentid])?$arr_att[$row_exam->studentid]+($row_exam->score):$row_exam->score);
						$val_att_m = isset($max_att["att_max"])?$max_att["att_max"]:0;
						if($row_exam->max_score > $val_att_m){
							$max_att["att_max"]= $row_exam->max_score;
						}
					}else{ // class participation
						$arr_score[$row_exam->studentid][$row_exam->subjectid] =isset($arr_score[$row_exam->studentid][$row_exam->subjectid])?$arr_score[$row_exam->studentid][$row_exam->subjectid]+$row_exam->score:$row_exam->score;
						
						$val_cp_m = isset($cp_m[$row_exam->subjectid])?$cp_m[$row_exam->subjectid]:0;
						if($row_exam->max_score > $val_cp_m){
							$cp_m[$row_exam->subjectid] = $row_exam->max_score;
						}
					}
				}
			}
			if(count($arr_student) > 0){
				foreach($arr_student as $row_st){
					$check_gep = $ci->db->query("SELECT COUNT(*) as amt_count 
													FROM sch_gep_total_score_entry
													WHERE 1=1 
													AND schoolid  = '".$arr_gep['schoolid'] ."'
													AND schlevelid='".$arr_gep['schoollavel']."'
													AND yearid='".$arr_gep['yearsid']."'
													AND termid='".$arr_gep['termid']."'
													AND gradeid='".$arr_gep['gradelavel']."'
													AND classid='".$arr_gep['classid']."'
													AND cousetype='".$arr_gep['course']."'
													AND examtypeid = '".$arr_gep['examtype']."'
													AND studentid='".$row_st."'")->row();
					
					if($check_gep->amt_count == 0){
						$ci->db->query("INSERT INTO sch_gep_total_score_entry SET 
													schoolid  = '".$arr_gep['schoolid']."',
													schlevelid='".$arr_gep['schoollavel']."',
													yearid='".$arr_gep['yearsid']."',
													termid='".$arr_gep['termid']."',
													gradeid='".$arr_gep['gradelavel']."',
													classid='".$arr_gep['classid']."',
													cousetype='".$arr_gep['course']."',
													examtypeid = '".$arr_gep['examtype']."',
													studentid='".$row_st."'
										");
					}
					$where_sub = "";
					if($arr_gep['course'] != ""){
						if($arr_gep['course']==0){
							$where_sub = " AND sch_subject_gep.is_core=1";
						}else{
							$where_sub = " AND sch_subject_gep.is_skill=1";
						}
					}
					$group_subj = $ci->db->query("SELECT
													sch_subject_gep.subjectid,
													sch_subject_gep.`subject`,
													sch_subject_gep.short_sub,
													sch_subject_gep.subj_type_id,
													sch_subject_gep.max_score,
													sch_subject_gep.calc_score,
													sch_subject_type_gep.is_group_calc_percent AS is_cencent,
													sch_subject_type_gep.is_class_participation AS is_cp,
													sch_subject_type_gep.schlevelid,
													sch_subject_type_gep.schoolid,
													sch_subject_type_gep.yearid,
													sch_subject_type_gep.rangelevelid
													FROM
													 sch_subject_type_gep 
													INNER JOIN sch_subject_gep ON sch_subject_gep.subj_type_id = sch_subject_type_gep.subj_type_id
													WHERE
														1 = 1 {$where_sub}
													AND sch_subject_type_gep.schoolid='".$arr_gep['schoolid']."'
													AND sch_subject_type_gep.schlevelid='".$arr_gep['schoollavel']."'
												");
					$att_data = 0;
					$cp_data  = 0;
					$howework = 0;
					$subj_data= 0;
					$cp=0;
					if($group_subj->num_rows() > 0){
						foreach($group_subj->result() as $row_g){
							if($row_g->is_cp == 3){
								$max_att_v  = isset($max_att["att_max"])?$max_att["att_max"]:1;
								$att_total  = 0;
								if(isset($arr_att[$row_st])){
									$att_total= ($arr_att[$row_st]/$max_att_v);
								}
								$att_data = ($att_total*$row_g->max_score)-0;
							}else if($row_g->is_cp == 1){
								$max_cp_v  = isset($cp_m[$row_g->subjectid])?$cp_m[$row_g->subjectid]:1;
								$total_cp  = isset($arr_score[$row_st][$row_g->subjectid])?$arr_score[$row_st][$row_g->subjectid]:0;
								$result_cp = floor(($total_cp/$max_cp_v)*100)/100;
								$cp_data+=($result_cp*$row_g->max_score)-0;
								$cp++;
							}else if($row_g->is_cp == 2){
								$max_hw_v  = isset($max_hw_m[$row_g->subjectid])?$max_hw_m[$row_g->subjectid]:1;
								$total_hw  = isset($arr_hw[$row_st])?$arr_hw[$row_st]:0;
								$result_hw = floor(($total_hw/$max_hw_v)*100)/100;
								$howework  = ($result_hw*$row_g->max_score)-0;
							}else if($row_g->is_cp == 0){
								$subj_data = isset($arr_subj[$row_st])?$arr_subj[$row_st]:0;
							}
						}
					}
					
					$sum_cp = floor(($cp_data/$cp)*100)/100;
					$grand_total= ($att_data+$sum_cp+$howework+$subj_data)-0;
					$Where_data = array("schoolid"=>$arr_gep['schoolid'],
										"schlevelid"=>$arr_gep['schoollavel'],
										"yearid"=>$arr_gep['yearsid'],
										"termid"=>$arr_gep['termid'],
										"gradeid"=>$arr_gep['gradelavel'],
										"classid"=>$arr_gep['classid'],
										"cousetype"=>$arr_gep['course'],
										"examtypeid"=>$arr_gep['examtype'],
										"studentid"=>$row_st
										);
					$data = array("attandance"=>$att_data,"class_participation"=>$sum_cp,
								"homework"=>$howework,"total_subj"=>$subj_data,
								"grand_total"=>$grand_total
								);
					$ci->db->UPDATE("sch_gep_total_score_entry",$data,$Where_data);					
				}
			}
			
		}
		// ========================= end function GEP ====================

		function show_result_score_gep($get_para){
			$ci=& get_instance();
			$sql_gep = $ci->db->query("SELECT
											gep_result.score_entry_id,
											gep_result.schoolid,
											gep_result.programid,
											gep_result.schlevelid,
											gep_result.yearid,
											gep_result.termid,
											gep_result.gradeid,
											gep_result.classid,
											gep_result.examtypeid,
											gep_result.cousetype,
											gep_result.studentid,
											gep_result.attandance,
											gep_result.class_participation,
											gep_result.homework,
											gep_result.total_subj,
											gep_result.grand_total,
											gep_result.is_change_class
											FROM
											sch_gep_total_score_entry AS gep_result
											WHERE 1=1
											AND studentid='".$get_para['studentid']."'
											AND schoolid='".$get_para['schoolid']."'
											AND schlevelid='".$get_para['schlevelid']."'
											AND yearid='".$get_para['yearid']."'
											AND termid='".$get_para['termid']."'
											AND gradeid='".$get_para['grade_levelid']."'
										");
			$sum_result_core  = 0;
			$sum_result_skill = 0;
			if($sql_gep->num_rows() > 0)
			{
				foreach($sql_gep->result() as $row_r){
					$score_att = $row_r->attandance;
					$score_cp  = $row_r->class_participation;
					$score_hw  = $row_r->homework;
					$score_s   = $row_r->total_subj;
					$examtype  = $row_r->examtypeid;
					$cousetype = $row_r->cousetype;
					$group_sub = $ci->db->query("SELECT
													gr_sub.subj_type_id,
													gr_sub.subject_type,
													gr_sub.schlevelid,
													gr_sub.schoolid,
													gr_sub.rangelevelid,
													gr_sub.is_report_display,
													gr_sub.is_group_calc,
													gr_sub.is_group_calc_percent,
													gr_sub.is_class_participation as is_cp,
													gr_sub.is_attendance_qty
													FROM
													sch_subject_type_gep AS gr_sub
													WHERE 1=1
													AND schoolid='".$get_para['schoolid']."' 
													AND schlevelid='".$get_para['schlevelid']."'
												");
					//$total_sc = 0;
					$att = 0;
					$cp  = 0;
					$hw  = 0;
					$subj = 0;
					if($group_sub->num_rows() > 0){
						foreach($group_sub->result() as $row_gr){
							$amt_percent = $row_gr->is_group_calc_percent;
							if($row_gr->is_cp == 3){
								$att += ($score_att*$amt_percent)/100;
							}
							else if($row_gr->is_cp == 1){
								$cp += ($score_cp*$amt_percent)/100;
							}
							else if($row_gr->is_cp == 2){
								$hw += ($score_hw*$amt_percent)/100;
							}
							if($row_gr->is_cp == 0){
								if($examtype == 1){
									$subj += ($score_s*15)/100;
								}else{
									$subj += ($score_s*$amt_percent)/100;
								}
								
							}
						}
					}
					$total_sc = ($att/2)+($cp/2)+($hw/2)+$subj;
					if($cousetype == 0){
						$sum_result_core+=floor($total_sc*100)/100;
					}else{
						$sum_result_skill+=floor($total_sc*100)/100;
					}
					
				}
			}
			$arr_return = array("skill"=>$sum_result_skill,"core"=>$sum_result_core);
			return $arr_return;
		}
		function show_rank_gep($arr_rank){
			$ci=& get_instance();
			// $arr_rank_s = array("schoolid"=>$schoolid,
			// 									"schlevelid"=>$schoollavel,
			// 									"gradelevelid"=>$gradelavel,
			// 									"classid"=>$classid,
			// 									"yearid"=>$years,
			// 									"termid"=>$termid,
			// 									"studentid"=>$studentid,
			// 									"is_pt"=>$is_partime
			// 									);
			$sql_rank = $ci->db->query("SELECT stse.studentid,SUM(stse.grand_total) AS total_s 
										FROM sch_gep_total_score_entry AS stse
										WHERE 1=1
										AND schoolid='".$arr_rank['schoolid']."'
										AND schlevelid='".$arr_rank['schlevelid']."'
										AND classid='".$arr_rank['classid']."'
										AND yearid='".$arr_rank['yearid']."'
										AND termid='".$arr_rank['termid']."'
										AND gradeid='".$arr_rank['gradelevelid']."'
										GROUP BY stse.studentid
										ORDER BY SUM(stse.grand_total) DESC");
			$arr_store   = array();
			$arr_rank_r = array();
			$rank1 = 1;
			$rank  = 1;
			$j = 1;
			if($sql_rank->num_rows() > 0){
				foreach($sql_rank->result() as $row_r){
					$total = $row_r->total_s;
					if(in_array($total,$arr_store)){
						$rank1=$rank;
					}else{
						$rank = $j;
						$rank1= $j;
					}
					$j++;
					$arr_store[] = $total;
					$arr_rank_r[$row_r->studentid]= $rank1;
				}
			}
			$show_rank = "";
			if(isset($arr_rank_r[$arr_rank['studentid']])){
				$show_rank = $arr_rank_r[$arr_rank['studentid']];
			}
			return $show_rank;
		}
	}
?>