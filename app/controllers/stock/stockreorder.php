<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stockreorder extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		//$this->lang->load('stock', 'english');		
		$this->load->model("setting/usermodel","user");	
		$this->load->model("stock/modstockreorder","stock");	
		$this->thead=array("Code"=>'stockcode',
							 "Name"=>'descr_eng',
							 "Category"=>'categoryid',							 
							 "Expire Date"=>'expired_date',
							 "QOH"=>'quantity',							 
							 "UOM"=>'uom',
							 "Qty.Reorder"=>'reorder_qty',
							 "Modified"=>'modified_date',							 
							 "Action"=>'Action'							 	
							);
		$this->idfield="stockid";
		
	}
	
	function index()
	{
		
		$page=0;
		if(isset($_GET['per_page'])) $page=$_GET['per_page'];
		$m='';
		$p='';
		if(isset($_GET['m'])){
	        $m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }		
		$this->load->library('pagination');	
		$config['base_url'] = site_url()."/stock/stockreorder?pg=1&m=$m&p=$p";		
		$config['total_rows'] = $this->green->getTotalRow("SELECT * FROM v_stock_reorder");
		$config['per_page'] =50;
		//$config['use_page_numbers'] = TRUE;
		$config['num_link']=5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';		
		
		$this->pagination->initialize($config);	
		$limi=" limit ".$config['per_page'];
		if($page>0){
			$limi=" limit ".$page.",".$config['per_page'];
		}		
		$sql_page="SELECT
						stock.stockid,
						stock.stockcode,
						stock.descr_eng,
						stock.expired_date,
						stock.reorder_qty,
						stock.quantity,
						stock.uom,
						stock.categoryid,
						stock.category,
						stock.modified_date
						FROM
						v_stock_reorder AS stock	
						order by categoryid,stockid
						{$limi}";
		//echo $sql_page;
		$data['tdata']=$this->green->getTable($sql_page);
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Stock List";			
		$this->parser->parse('header', $data);
		$this->parser->parse('stock/stock_reorder_list', $data);
		$this->parser->parse('footer', $data);
	}
		
	function search(){
		if(isset($_GET['s_id'])){
			$stockid=$_GET['s_id'];
			$descr_eng=$_GET['des'];
			$category=$_GET['cat'];			
			$sort_num=$_GET['s_num'];
			$m=$_GET['m'];
			$p=$_GET['p'];
			$sort=$this->input->post('sort');
			$data['tdata']=$query=$this->stock->searchstock($stockid,$descr_eng,$category,$sort,$sort_num,$m,$p);
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Stock List";			
			$this->parser->parse('header', $data);
			$this->parser->parse('stock/stock_reorder_list', $data);
			$this->parser->parse('footer', $data);
		}
		if(!isset($_GET['s_id'])){
			$stockcode=$this->input->post('stockcode');
			$descr_eng=$this->input->post('descr_eng');
			$category=$this->input->post('categoryid');
			
			$sort=$this->input->post('sort');
			$sort_num=$this->input->post('sort_num');
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
			$query=$this->stock->searchstock($stockcode,$descr_eng,$category,$sort,$sort_num,$m,$p);
				 $i=1;
				 //echo $query;
				foreach($query as $row){	
																							
					echo "<tr>
						 <td class='stockcode'>".$row['stockcode']."</td>
						 <td class='descr_eng'>".$row['descr_eng']."</td>							
						 <td class='category'>".$row['category']."</td>									 
						 <td class='expired_date'>".$row['expired_date']."</td>
						 <td class='quantity'>".$row['quantity']."</td>
						 <td class='uom'>".$row['uom']."</td>	
						 <td class='reorder_qty'>".$row['reorder_qty']."</td>
						 <td class='uom'>".$row['modified_date']."</td>								
						 <td class='remove_tag'>";
					 	if($this->green->gAction("P")){	
							echo "<a><img rel=".$row['stockid']." onclick='previewstock(event);' src='".site_url('../assets/images/icons/preview.png')."'/></a>";  
						 }
						echo "</td>
						 </tr>
						 ";								 
					$i++;	 
				}
				echo "<tr class='remove_tag'>
							<td colspan='12' id='pgt'>
								<div style='margin-top:20px; width:10%; float:left;'>
								Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
												
												$num=50;
												for($i=0;$i<10;$i++){?>
													<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
													<?php $num+=50;
												}
												
											echo "</select>
								</div>
								<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
									<ul class='pagination' style='text-align:center'>
									 ".$this->pagination->create_links()."
									</ul>
								</div>

							</td>
						</tr> ";
		}
	}


	
}
