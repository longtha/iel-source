<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		//$this->lang->load('stock', 'english');
		$this->load->model("stock/ModStock","stock");	
		$this->load->model("setting/usermodel","user");	
		$this->thead=array("Code"=>'stockcode',
							 "Description"=>'descr_eng',
							 "Category"=>'categoryid',							 
							 "Expired Date"=>'expired_date',
							 "QOH"=>'quantity',							 
							 "UOM"=>'uom',
							 "Reorder QTY."=>'reorder_qty',
							 "Modified"=>'modified_date',							 
							 "Action"=>'Action'							 	
							);
		$this->idfield="stockid";
		
	}
	
	function index()
	{
		
		$page=0;
		if(isset($_GET['per_page'])) $page=$_GET['per_page'];
		$m='';
		$p='';
		if(isset($_GET['m'])){
	        $m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }		
		$this->load->library('pagination');	
		$config['base_url'] = site_url()."/stock/stock?pg=1&m=$m&p=$p";		
		$config['total_rows'] = $this->green->getTotalRow("SELECT * FROM v_stock_profile");
		$config['per_page'] =50;
		//$config['use_page_numbers'] = TRUE;
		$config['num_link']=5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';		
		
		$this->pagination->initialize($config);	
		$limi=" limit ".$config['per_page'];
		if($page>0){
			$limi=" limit ".$page.",".$config['per_page'];
		}		
		$sql_page="SELECT
						stock.stockid,
						stock.stockcode,
						stock.descr_eng,
						stock.expired_date,
						stock.reorder_qty,
						stock.quantity,
						stock.uom,
						stock.categoryid,
						stock.category,	
						date_format(
							`stock`.`modified_date`,
							_utf8 '%d-%m-%Y'
						) AS `modified_date`

						FROM
						v_stock_profile AS stock	
						order by categoryid,stockid
						{$limi}";
		//echo $sql_page;
		$data['tdata']=$this->green->getTable($sql_page);
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Medicine Stock List";	

		$this->parser->parse('header', $data);
		$this->parser->parse('stock/stock_list', $data);
		$this->parser->parse('footer', $data);
	}
	function import($result=0){
		$data['page_header']="Import stock Profile";		
		$data['import_status']=($result==1?"stock profile was imported.":"No stock profile to import.");			
		$this->parser->parse('header', $data);
		$this->parser->parse('stock/import_profile', $data);
		$this->parser->parse('footer', $data);
	}
	function importenroll($result=0){
		$data['page_header']="Import stock Profile";		
		$data['import_status']=($result==1?"Enrollment was imported.":"No Enrollment to import.");			
		$this->parser->parse('header', $data);
		$this->parser->parse('stock/import_enrollment', $data);
		$this->parser->parse('footer', $data);
	}
	function importProfile(){
		$result=$this->stock->saveImport();
		$this->import($result);		
	}
	function importenrollment(){
		$result=$this->stock->saveImportenroll();
		$this->importenroll($result);		
	}
	function add(){
		$data['page_header']="New stock";			
		$this->parser->parse('header', $data);
		$this->parser->parse('stock/stock_edit', $data);
		$this->parser->parse('footer', $data);
	}
	function preview($stockid){
		$data['page_header']="Preview Stock";		
		$data1['stock']=$this->stock->getPreview($stockid);
		$data1['stock_bl']=$this->stock->getStockBl($stockid);		
		$this->parser->parse('header', $data);
		$this->load->view('stock/preview', $data1);
		$this->parser->parse('footer', $data);
	}
	function delete($stockid){
		$this->db->set('is_active',0);
		$this->db->where('stockid',$stockid);
		$this->db->update('sch_stock');		
		$m='';
		$p='';
		if(isset($_GET['m'])){
	        $m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }	
		redirect("stock/stock?m=$m&p=$p");
	}
	
	function edit($stockid){
		$data['page_header']="New stock";
		//$data1['page_header']=array("New stock");			
		$datas['stock']=$this->stock->getStockRow($stockid);		
			
		$this->parser->parse('header', $data);
		$this->parser->parse('stock/stock_edit', $datas);
		$this->parser->parse('footer', $data);
	}
	
	function getStockRow(){
		$std_id=$this->input->post('std_id');
		$this->db->select('s.stockid,s.last_name,s.first_name,s.last_name_kh,c.class_name');
		$this->db->from('sch_student s');
		$this->db->join('sch_class c','s.classid=c.classid','left');
		$this->db->where('s.stockid',$std_id);
		$s_row=$this->db->get()->row();
		header("Content-type:text/x-json");
		echo json_encode($s_row);
	}
	
	function validateuser(){
		$count=$this->stock->getvalidateuser($this->input->post('username'));
		echo $count;
	}
	
	function getstdbyid(){
		$std_num=$this->input->post('std_num');
		$this->db->select('count(*)');
		$this->db->from('sch_student');
		$this->db->where('student_num',$std_num);
		echo $this->db->count_all_results();
	}
	function save(){		
		$stockid=$this->input->post("stockid");				
		$save_result=$this->stock->save($stockid);
		$m='';
		$p='';
		if(isset($_GET['m'])){
	        $m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }	
		redirect("stock/stock?m=$m&p=$p");
	}
	

	function do_upload($student_id,$year)
		{
			$config['upload_path'] ='./assets/upload/students/'.$year;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '5000';
			$config['file_name']  = "$student_id.jpg";
			$config['overwrite']=true;
			$config['file_type']='image/png';
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('file'))
			{
				$error = array('error' => $this->upload->display_errors());			
			}
			else
			{		
				//$this->generate_thumb($this->upload->file_name,$this->upload->upload_path)	;	
				$data = array('upload_data' => $this->upload->data());			
				//redirect('setting/user');
				 	$config2['image_library'] = 'gd2';
                    $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                    $config2['new_image'] = './assets/upload/students/'.$year;
                    $config2['maintain_ratio'] = TRUE;
                    $config2['create_thumb'] = TRUE;
                    $config2['thumb_marker'] = FALSE;
                    $config2['width'] = 120;
                    $config2['height'] = 180;
                    $config2['overwrite']=true;
                    $this->load->library('image_lib',$config2); 

                    if ( !$this->image_lib->resize()){
	                	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
					}else{
						//unlink('./assets/upload/students/'.$student_id.'.jpg');
					}
			}
		}
	function generate_thumb($filename, $path = '')
	{
	    // if path is not given use default path //
	    if (!$path) {
	        $path = FCPATH . 'somedir' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
	    }

	    $config['image_library'] = 'gd2';
	    $config['source_image'] = $path . $filename;
	    $config['create_thumb'] = TRUE;
	    $config['maintain_ratio'] = TRUE;
	    $config['width'] = 120;
	    $config['height'] = 180;

	    $this->load->library('image_lib', $config);

	    if (!$this->image_lib->resize()) {
	        echo $this->image_lib->display_errors();
	        return FALSE;
	    }else{
	    	//unlink('./assets/upload/students/'.$student_id.'.jpg');
	    }
	    // get file extension //
	    preg_match('/(?<extension>\.\w+)$/im', $filename, $matches);
	    $extension = $matches['extension'];
	    // thumbnail //
	    $thumbnail = preg_replace('/(\.\w+)$/im', '', $filename). $extension;
	    return $thumbnail;
	}
	function search(){
		if(isset($_GET['s_id'])){
			$stockid=$_GET['s_id'];
			$descr_eng=$_GET['des'];
			$category=$_GET['cat'];			
			$sort_num=$_GET['s_num'];
			$m=$_GET['m'];
			$p=$_GET['p'];
			$sort=$this->input->post('sort');
			$data['tdata']=$query=$this->stock->searchstock($stockid,$descr_eng,$category,$sort,$sort_num,$m,$p);
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Stock List";			
			$this->parser->parse('header', $data);
			$this->parser->parse('stock/stock_list', $data);
			$this->parser->parse('footer', $data);
		}
		if(!isset($_GET['s_id'])){
			$stockcode=$this->input->post('stockcode');
			$descr_eng=$this->input->post('descr_eng');
			$category=$this->input->post('categoryid');
			
			$sort=$this->input->post('sort');
			$sort_num=$this->input->post('sort_num');
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
			$query=$this->stock->searchstock($stockcode,$descr_eng,$category,$sort,$sort_num,$m,$p);
				 $i=1;
				 //echo $query;
				foreach($query as $row){	
																							
					echo "<tr>
						 <td class='stockcode'>".$row['stockcode']."</td>
						 <td class='descr_eng'>".$row['descr_eng']."</td>							
						 <td class='category'>".$row['category']."</td>									 
						 <td class='expired_date'>".$row['expired_date']."</td>
						 <td class='quantity'>".$row['quantity']."</td>
						 <td class='uom'>".$row['uom']."</td>	
						 <td class='reorder_qty'>".$row['reorder_qty']."</td>
						 <td class='uom'>".$row['modified_date']."</td>								
						 <td class='remove_tag no_wrap'>";
					 	if($this->green->gAction("P")){
							echo "<a><img rel=".$row['stockid']." onclick='previewstock(event);' src='".site_url('../assets/images/icons/preview.png')."'/></a>"; 
						 }
						 if($this->green->gAction("D")){
							echo "<a><img rel=".$row['stockid']." onclick='deletestock(event);' src='".site_url('../assets/images/icons/delete.png')."'/></a>";
						 }
						 if($this->green->gAction("U")){
							echo "<a><img rel=".$row['stockid']." onclick='updatestock(event);' src='".site_url('../assets/images/icons/edit.png')."'/></a>";
						 }
					echo " </td>
						 </tr>
						 ";										 
					$i++;	 
				}
				echo "<tr class='remove_tag'>
							<td colspan='12' id='pgt'>
								<div style='margin-top:20px; width:10%; float:left;'>
								Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
												
												$num=50;
												for($i=0;$i<10;$i++){?>
													<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
													<?php $num+=50;
												}
												
											echo "</select>
								</div>
								<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
									<ul class='pagination' style='text-align:center'>
									 ".$this->pagination->create_links()."
									</ul>
								</div>

							</td>
						</tr> ";
		}
	}


	
}
