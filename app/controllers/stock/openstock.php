<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OpenStock extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		$this->load->library('pagination');	
		$this->lang->load('student', 'english');
		$this->load->model('stock/ModeOpenStock','openstocks');
		$this->load->model('/ModeOpenStock','openstocks');
		$this->thead=array("No"=>'No',
			 				"Stock Name"=>'stock_name',
							"Expired Date"=>'expired_date',
							"Transaction Date"=>'transdate',
							"Quantity"=>'qty',
							"UOM"=>'uom',
							"Wharehouse"=>'where_house',
							"Action"=>'Action'							 	
							);
		$this->idfield="Open Stock";
	}
	function index(){
		$data['tdata']=$this->openstocks->getopenstock();
		$data['thead']=	$this->thead;
		$data['page_header']="Threatment List";	
		$this->parser->parse('header', $data);
		$this->parser->parse('stock/openstock/openstock_list', $data);
		$this->parser->parse('footer', $data);
	}
	function getexpiredinfo(){
		$row=$this->db->where('stockid',$this->input->post('stockid'))
				->where('expired_date',$this->input->post('ex_date'))
				->get('sch_stock_balance')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function delete($tranno){
		$stock_move=$this->db->where('type',8)
				->where('transno',$tranno)
				->get('sch_stock_stockmove')->result();
		foreach ($stock_move as $move) {
			$this->db->query("UPDATE sch_stock_balance SET quantity=quantity - $move->quantity WHERE stockid='$move->stockid' AND expired_date='$move->expired_date'");
		}
		$this->db->where('type',8)
				->where('transno',$tranno)
				->delete('sch_stock_stockmove');
		$this->db->where('type',8)
				->where('transno',$tranno)
				->delete('sch_stock_opening_balance');
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("stock/openstock?m=$m&p=$p");
	}
	function save(){
		$openid=$this->input->post('txttran');
		$stockid=$this->input->post('addstockid');
		$ex_date=$this->input->post('expired_date');
		$qty=$this->input->post('addqty');
		$uom=$this->input->post('adduom');
		$qty=$this->input->post('addqty');
		$whcode=$this->input->post('r_whcode');
		$ref_no=$this->input->post('ref_no');
		$tranno=0;
		$date=date('Y-m-d');
		$user=$this->session->userdata('user_name');
		if($openid!=''){
			for($i=0;$i<count($stockid);$i++) {
				$this->openstocks->removestockmove(8,$openid,$whcode[$i]);
			}
			$tranno=$openid;
			$sql_getCreated="SELECT DISTINCT created_date,created_by from sch_stock_opening_balance 
					WHERE type=8 AND transno='".$tranno."'";
			$oldInf=$this->green->getOneRow($sql_getCreated);
			$user=$oldInf['created_by'];
			$date=$oldInf['created_date'];
			$this->db->where('type',8)
				->where('transno',$tranno)
				->delete('sch_stock_opening_balance');
		}else{
			$tranno=$this->db->select('sequence')
						->from('sch_z_systype')
						->where('typeid',8)
						->get()->row()->sequence;
		}
		$this->do_upload($ref_no);	
		$updatestock=1;
		for($i=0;$i<count($stockid);$i++) {
			if($ex_date[$i]=='new'){
				$ex_date=$this->input->post('new_date');
				$this->openstocks->newstockbalance($stockid[$i],$this->green->formatSQLDate($ex_date[$i]),$qty[$i],$uom[$i],$whcode[$i]);
				$updatestock=0;
			}
			$this->openstocks->save($ref_no,$stockid[$i],$this->green->formatSQLDate($ex_date[$i]),$qty[$i],$uom[$i],$tranno,$openid,$updatestock,$date,$user,$whcode[$i]);
			$ex_date=$this->input->post('expired_date');
			$updatestock=1;
		}
		if($openid==''){
			$this->openstocks->updatetran($tranno+1,8);
		}

		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("stock/openstock?m=$m&p=$p");
	}
	function getlastmodi(){
		$transno=$this->input->post('transno');
		$row=$this->db->query("SELECT DISTINCT modified_date,modified_by FROM sch_stock_opening_balance WHERE transno='$transno'")->row();
		if($row->modified_by!='')
		echo "Last Modified Date :".date_format(date_create($row->modified_date),'d-m-Y H:i:s')." By:".$row->modified_by;
	}
	function edit(){
		$type=$this->input->post('type');
		$transno=$this->input->post('transno');
        $tr="";
		foreach ($this->openstocks->getstockopen($type,$transno) as $row) {
			$stock=$this->openstocks->getstockbalancerow($row->stockid,$row->expire_date);
            $whcode=$row->whcode;
			$tr.="<tr class='expiredrow'>
						<td width='150'>
						    <input type='text' value='$row->stockid' class='addstockid hide' name='addstockid[]'/>$row->descr_eng</td>
						<td width='130px'>
						 	<select onchange='getexpiredinfo(event);' name='expired_date[]' id='expired_date' class='form-control input-sm addexpired_date'>";
								
								foreach ($this->openstocks->getexpire($row->stockid,$stock->whcode) as $rows) {
                                    $tr.="<option value='".$this->green->formatSQLDate($rows->expired_date)."' ".($row->expire_date==$rows->expired_date?'selected':'').">".$this->green->formatSQLDate($rows->expired_date)."</option>";
								}
                            $tr.="<option value='new'>New Expired Date</option>
							</select>
						<td>
							<div class='input-group new_ex hide' id='datetimepicker'>
							    <input onchange='checkex_new(event);' type='text' class='form-control input-sm new_date' name='new_date[]'/>
							    <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'>
							</div>	
						</td>		
						<td width='70'><input onkeypress='return isNumberKey(event);'  type='text' class='form-control input-sm' id='qty' value='$row->qty' name='addqty[]'/></td>
						<td id='s_qty'>$stock->quantity</td>
						<td id='uom'><input type='text' name='adduom[]' class='hide' value='$row->uom'/>$row->uom</td>
						<td id='r_whcode' class='hide'><input type='text' name='r_whcode[]' class='hide' value='$row->whcode'/></td>
						<td>
				    		<a>
				    			<img onclick='removerow(event);' src='".base_url()."assets/images/icons/delete.png' />
				    		</a>
				    	</td>
					</tr>";
		}
        $arr=array();
        $arr['stockopen']=$tr;
        $arr['whcode']=$whcode;
        header("Content-type:text/x-json");
        echo json_encode($arr);
        exit();
	}
	
	function preview($transno){
		$sql="SELECT DISTINCT * from sch_stock_opening_balance 
					WHERE type=8 AND transno='".$transno."' ORDER BY stockid";
		$datas['query']=$this->db->query($sql)->row();
		$data['page_header']="Threatment List";	
		$this->parser->parse('header', $data);
		$this->load->view('stock/openstock/preview_openstock', $datas);
		$this->parser->parse('footer', $data);
	}
	function view_image($refno){
		$datas['refno']=$refno;
		$data['page_header']="Threatment List";	
		$this->parser->parse('header', $data);
		$this->load->view('stock/openstock/view_image', $datas);
		$this->parser->parse('footer', $data);
	}
	function do_upload($ref_no){
		if(!file_exists('./assets/upload/stock/stockin'))
		 {
		    if(mkdir('./assets/upload/stock/stockin',0755,true))
		    {
		         return true;
		    }
		 }
		$config['upload_path'] ='./assets/upload/stock/stockin';
		$config['allowed_types'] = 'png|jpg|gif';
		$config['max_size']	= '5000';
		$config['file_name']="$ref_no.jpg" ;
		$config['overwrite']=true;
		$config['file_type']='image/png';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());			
		}
		else
		{		
			//$this->generate_thumb($this->upload->file_name,$this->upload->upload_path)	;	
			$data = array('upload_data' => $this->upload->data());			
			//redirect('setting/user');
		}
	}
	function getrefno(){
		$refno=$this->input->post('refno');
		$count=$this->db->query("SELECT COUNT(ref_no) as count FROM sch_stock_opening_balance WHERE ref_no='$refno'")->row()->count;
		echo $count;

	}
	function f_date($date){
		if($date!='0000-00-00' && $date!='')
			return date('d-m-Y',strtotime($date));
		else 
			return '00-00-0000';
	}
	function getexpire(){
		$stockid=$this->input->post('stockid');
		$whcode=$this->input->post('whcode');
		$stock=$this->db->where('stockid',$stockid)->get('sch_stock')->row();
		echo "<tr class='expiredrow'>
			<td width='150'><input type='text' class='addstockid hide' value='$stock->stockid' name='addstockid[]'/>$stock->descr_eng</td>
			<td width='130px'>
			 	<select onchange='getexpiredinfo(event);' name='expired_date[]' id='expired_date' class='form-control input-sm addexpired_date'>";
					$i=1;
					$qty='';
					$uom='';
					foreach ($this->openstocks->getexpire($stockid,$whcode) as $row) {
						echo "<option value='".$this->green->formatSQLDate($row->expired_date)."'>".$this->green->formatSQLDate($row->expired_date)."</option>";
						if($i==1){
							$qty=$row->quantity;
							$uom=$row->uom;
						}
					$i++;
					}
				echo"<option value='new'>New Expired Date</option>
				</select>
			<td>
				<div class='input-group new_ex hide' id='datetimepicker'>
				    <input onchange='checkex_new(event);' type='text' class='form-control input-sm new_date' name='new_date[]'/>
				    <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'>
				</div>	
			</td>		
			<td width='70'><input onkeypress='return isNumberKey(event);' required data-parsley-required-message='Enter Quantity'  type='text' class='form-control input-sm' id='qty' name='addqty[]'/></td>
			<td id='s_qty'>$qty</td>
			<td id='uom'><input type='text' name='adduom[]' class='hide' value='$uom'/>$uom</td>
			<td id='r_whcode' class='hide'><input type='text' name='r_whcode[]' class='hide' value='$whcode'/></td>
			<td>
	    		<a>
	    			<img onclick='removerow(event);' src='".base_url()."assets/images/icons/delete.png' />
	    		</a>
	    	</td>
		</tr>";
	}
	function search(){
		$i=1;
		if(isset($_GET['per_page']))
			$i=$_GET['per_page']+1;
		//==================data==============
		if(isset($_GET['s_name'])){
				$s_name 	= $_GET['s_name'];
				$start_date = $_GET['s_date'];
				$end_date 	= $_GET['e_date'];
				$s_num 		= $_GET['s_num'];
				$m 			= $_GET['m'];
				$p 			= $_GET['p'];
				$schlevelid = $_GET['sl'];
				$data['tdata']=$this->openstocks->search($s_name,$start_date,$end_date,$s_num,$m,$p,$schlevelid);
				$data['thead']=	$this->thead;
				$data['page_header']="Threatment List";	
				$this->parser->parse('header', $data);
				$this->parser->parse('stock/openstock/openstock_list', $data);
				$this->parser->parse('footer', $data);
		}else{
				$s_name 	= $this->input->post('s_name');
				$start_date = $this->green->formatSQLDate($this->input->post('start_date'));
				$end_date 	= $this->green->formatSQLDate($this->input->post('end_date'));
				$s_num 		= $this->input->post('s_num');
				$m 			= $this->input->post('m');
				$p 			= $this->input->post('p');
				$schlevelid = $this->input->post('schlevelid');
				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
			        $this->green->setActiveModule($m);
			    }
			    if($p!=''){
			        $this->green->setActivePage($p); 
			    }
				$query=$this->openstocks->search($s_name,$start_date,$end_date,$s_num,$m,$p,$schlevelid);
				foreach ($query as $row) {
						echo "<tr>
							 		<td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
									 <td class='stock_name'>".$row->descr_eng."</td>
									 <td class='expired_date alignright'>".$this->f_date($row->expire_date)."</td>
									 <td class='transdate alignright'>".$this->f_date($row->transdate)."</td>
									 <td class='qty alignright'>".$row->qty."</td>
									 <td class='uom'>$row->uom</td>
									 <td class='where_house'>$row->wharehouse</td>												 
									 <td class='remove_tag no_wrap'>" ;
						if(!isset($arrtran[$row->transno])){
							if($this->green->gAction("P")){
								echo "<a>
									 		<img rel=".$row->ref_no."  onclick='view_image(event);' src='".site_url('../assets/images/icons/gallery.png')."'/>
									 	</a>
										<a>
									 		<img type=".$row->type." tran=".$row->transno."  onclick='preview(event);' src='".site_url('../assets/images/icons/preview.png')."'/>
									 	</a>";
							}
							if($this->green->gAction("D")){
								echo "<a>
									 		<img type=".$row->type." tran=".$row->transno." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
									 	</a>";
							}
							if($this->green->gAction("U")){
								echo "<a>
									 		<img type=".$row->type." tran=".$row->transno." ref_no='".$row->ref_no."' onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
									 	</a>" ;
							}	
						}	
						echo "</td>
						 	</tr>
						 ";
					 	$i++;
					 	$arrtran[$row->transno]=$row->transno;
			}
			//===========================show pagination=======================
				echo "<tr class='remove_tag'>
						<td colspan='12' id='pgt'>
							<div style='margin-top:20px; width:10%; float:left;'>
							Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";		
									$num=50;
									for($i=0;$i<10;$i++){?>
										<option value="<?php echo $num ;?>" <?php if($num==$s_num) echo 'selected';?> ><?php echo $num;?></option>
										<?php $num+=50;
									}			
				echo "</select>
							</div>
							<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
								<ul class='pagination' style='text-align:center'>
								 ".$this->pagination->create_links()."
								</ul>
							</div>

						</td>
					</tr> ";
					
		}
	}
}