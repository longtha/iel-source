<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {	
	public function index()
	{
		$data['page_header']="Here is Index Page";		
		$this->load->view('header',$data);
		$this->load->view('index',$data);
		$this->load->view('footer');	
	}
}

