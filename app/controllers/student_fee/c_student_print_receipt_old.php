<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_student_print_receipt extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("student_fee/m_student_fee_invoice","mstu_fee");
	}
	
	function index(){
		
		 $mo = '';
       $mo = $this->db->where('typeno', $_GET['param_reno'])->get('sch_student_fee_rec_order')->row();
		
			$type="";
			$typeno="";
			$type=$mo->type;
			$data['typeno']=$mo->typeno;
			$data['trandate']=$mo->trandate;
			
			$data['amt_paid']=$mo->amt_paid;

			// $data['duedate']=($this->green->convertSQLDate($mo->duedate));
			$data['trandate']=($this->green->convertSQLDate($mo->trandate));
			
			$data['ranglev']=$mo->ranglev;
			$data['studentid']=$mo->studentid;
			$data['paymentmethod']=$mo->paymentmethod;
			$data['paymenttype']=$mo->paymenttype;
			
			$data['acandemic']=$mo->acandemic;
			$data['typeno_inv']=$mo->typeno_inv;
			$ranglev=$mo->ranglev;
			$classid=$mo->classid;
			$notepaid="";
			if($mo->note!=""){
				$notepaid="<u><strong>NOTE:</strong></u>&nbsp;".$mo->note;				
			}
			$data['notepaid']=$notepaid;
			
			$data['acandemicname']="";
			
			if($data['acandemic']!="" and $data['acandemic']!=0){
			$acandemicname = $this->db->where('yearid', $data['acandemic'])->get('sch_school_year')->row()->sch_year;
				$data['acandemicname']=$acandemicname;
			}
			
			$stuinfo ='';
			$stuinfo = $this->db->where('studentid', $data['studentid'])->get('sch_student')->row();
			
			
			$data['student_num']=$stuinfo->student_num;
			$data['first_name_kh']=$stuinfo->first_name_kh;
			$data['last_name_kh']=$stuinfo->last_name_kh;
			$data['gender']=$stuinfo->gender;
			$data['phone1']=$stuinfo->phone1;
			$data['studentid']=$stuinfo->student_num;
			
			// $data['fullname']=$data['last_name_kh'].'&nbsp;'.$data['first_name_kh'];
			$data['fullname']=$stuinfo->last_name.'&nbsp;'.$stuinfo->first_name;
			
			$data['rangelevelname']="";
			
			if($classid!="" and $classid!=0){
				$getClass="SELECT
						sch_class.class_name
						FROM
						sch_class
						WHERE classid='".$classid."'
						";
						// echo $getClass;
						$data['rangelevelname'] = $this->db->query($getClass)->row()->class_name;
			}
		
			if($ranglev!="" and $ranglev!=0){
				//--------
				$sqltime="SELECT
							sch_time.from_time,
							sch_time.am_pm,
							sch_time.to_time
							FROM
							sch_school_rangelevtime
							INNER JOIN sch_time ON sch_school_rangelevtime.timeid = sch_time.timeid
							where 1=1 
							AND rangelevelid='".$ranglev."'
						  ";
						 //  echo $sqltime;
						 
				$gettime="";
				$gettime = $this->db->query($sqltime)->result();
				
				$tr_time="";
				$data['from_to_time']="";
				if(isset($gettime) && count($gettime)>0){
					foreach($gettime as $rowde){
						$tr_time.='<tr>
										<td class="inv_info" style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">'.$rowde->from_time.'</td>
										<td style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">-</td>
										<td style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">'.$rowde->to_time.'</td>
										<td style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">'.$rowde->am_pm.'</td>
								  </tr>';
					};
					$data['from_to_time']='<table>'.$tr_time.'</table>';
				};
			}// end 
			
			if($data['paymentmethod']!=""){
					$this->db->where('term_sem_year_id', $data['paymentmethod']);
					
				$data['paymenttype']="";	
				if($data['paymenttype']!=""){
					$this->db->where('payment_type', $data['paymenttype']);
				}
					$dperoid=$this->db->get('v_study_peroid')->row();
					
					
					$dperoid=$this->db->get('v_study_peroid')->row();
					
					$data['period']=$dperoid->period;
					
				//	$data['yearid']=$dperoid->yearid;	
					
					$data['start_date']=($this->green->convertSQLDate($dperoid->from_date));
					$data['end_date']=($this->green->convertSQLDate($dperoid->to_date));
					
					
				
			}else{
					$data['period']="";
					$data['start_date']="";
					$data['end_date']="";	
			}
			
			$datas="SELECT
						sch_student_fee_rec_detail.typeno,
						sch_student_fee_rec_detail.type,
						sch_student_fee_rec_detail.description,
						sch_student_fee_rec_detail.`not`,
						sch_student_fee_rec_detail.amt_line,
						sch_student_fee_rec_detail.prices,
						sch_student_fee_rec_detail.amt_dis,
						sch_student_fee_rec_detail.qty
					FROM
						sch_student_fee_rec_detail
					where 1=1
					AND sch_student_fee_rec_detail.typeno='".$_GET['param_reno']."'
					AND prices <> '' 
			
			";
			// echo $datas; exit();
			
			$result = $this->db->query($datas)->result();
			// print_r($classes);
			$trinv="";	
			$amttotal=0;
			$ii=1;
			if(isset($result) && count($result)>0){	
			$des="";	
			foreach($result as $rowde){
				if($rowde->not !=""){
					$des=$rowde->description.'-'.$rowde->not;	
				}else{
					$des=$rowde->description;
				}
				$trinv.='<tr>
							<td colspan="5" style="width:85%">'.$des.'</td>
							<td align="right" style="width:15%; padding-right:10px;">'.number_format($rowde->amt_line,2)."$".'</td>
					</tr>';
					$amttotal+=($rowde->amt_line);
			}
			$typeno_inv_row = $this->db->query("SELECT
													sfee.amt_total,
													sfee.amt_paid,
													sfee.amt_balance,
													sfee.typeno
												FROM
													sch_student_fee AS sfee
												WHERE sfee.typeno='{$mo->typeno_inv}' ")->row_array();

			$trinv.='<tr>
            			<td style="font-family: Times New Roman; font-size: 20px; font-weight:bold;" colspan="1" align="right" class="add-line-height" >ប្រាក់សរុបរួម<br /><span><b> Total Amount</b></span></td>
	   					<td align="right" class="tfoot_amt"> '. number_format($typeno_inv_row['amt_total'],2)."&nbsp;$".' </td>
						<td style="font-family: Times New Roman; font-size: 20px; font-weight:bold;" colspan="1" align="right" class="add-line-height">ប្រាក់ទទួល<br /><span><b>Total Amount Piad</b></span></td>
	   					<td align="right" class="tfoot_amt"> '.number_format($typeno_inv_row['amt_paid'],2)."&nbsp;$".' </td>
                        <td style="font-family: Times New Roman; font-size: 20px; font-weight:bold;" colspan="1" align="right" class="add-line-height">ប្រាក់នៅសល់<br /><span><b>Total Balance</b></span></td>
	   					<td align="right" class="tfoot_amt"> '.number_format($typeno_inv_row['amt_balance'],2)."&nbsp;$".'</td>
					</tr>';
			}
			$data['trinv']=$trinv;
			// $data['trinv']=$trinv;
			
		// $this->load->view('student_fee/v_student_invoice');
		$this->parser->parse('student_fee/v_student_print_receipt',$data);
		
	}// function index(){
		
	
		
}
