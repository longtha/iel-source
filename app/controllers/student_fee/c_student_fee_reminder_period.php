<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_student_fee_reminder_period extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('student_fee/m_student_fee_reminder_period', 'rp');
        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolyearmodel', 'y');
        $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');
        $this->load->model('school/modfeetype', 'f');
        $this->load->model("school/classmodel", "c");
		
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('student_fee/v_student_fee_reminder_period');
		$this->load->view('footer');	
	}

	public function test(){	
		$this->load->view('header');
		$this->load->view('student_fee/test');
		$this->load->view('footer');	
	}

	public function grid(){
    	$rp = $this->rp->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $rp;
    }

    public function get_schlevel(){
        $programid = $this->input->post('programid') - 0;        
        $get_schlevel = $this->rp->get_schlevel($programid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel;
    }

    public function get_year(){
        $programid = $this->input->post('programid') - 0;
        $schlevelid = $this->input->post('schlevelid') - 0;                
        $get_year = $this->rp->get_year($programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    public function get_rangelevel(){
        $yearid = $this->input->post('yearid') - 0;
        $termid = $this->input->post('termid') - 0;

        $get_rangelevel = $this->rp->get_rangelevel($yearid, $termid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_rangelevel;
    }

    public function get_paymentType(){
        $feetypeid = $this->input->post('feetypeid');
        $programid = $this->input->post('programid');
        $schlevelid = $this->input->post('schlevelid');
        $yearid = $this->input->post('yearid');       

        $get_paymentType = $this->rp->get_paymentType($feetypeid, $programid, $schlevelid, $yearid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_paymentType;
    }

}