<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class c_history_deleted_invoice extends CI_Controller{
	    protected $thead;
		protected $idfield;
	    function __construct() {
	        parent::__construct();
	        $this->load->model("student_fee/m_history_deleted_invoice","deleinv");
	        $this->thead = array("No" => 'no',
	        					"Photo"=>'Photo',
                                "Description" => 'student_num',
                                "#Invoice" => 'typeno_inv_rec',
                                "Program"=>'program',
                                "School Level" => 'sch_level',
                                "Year" => 'sch_year',
								"Rang Level​"=> 'rangelevelname',
								"Class"=> 'class_name',
								"Study Period" => 'period',
								"Deleted By"=> 'deleted_byuser',
                                "Deleted Date"=> 'deleted_date',
								"Total" => 'amt_total',
								"Deposit" => 'amt_paid',
								"Balance" => 'amt_balance'
	                            );
	        $this->idfield = "student_num";
	    }
	    function index() {
	        $data['idfield'] = $this->idfield;
        	$data['thead'] = $this->thead;
        	$data['getprogram']=$this->deleinv->getprograms();
        	$data['schooinfor']=$this->deleinv->schooinfor();
        	$data['paymentmethod']=$this->deleinv->paymentmethod();
	        $this->load->view('header',$data);
	        $this->load->view('student_fee/v_history_deleted_invoice',$data);
	        $this->load->view('footer',$data );
	    }

	    // get_schlevel ---------------------------------------------------
	    function get_schlevel(){
			$programid = $this->input->post('programid');
			$get_schlevel = $this->deleinv->get_schlevel($programid);
			header("Content-type:text/x-json");
			echo ($get_schlevel);
		}
		//yera ------------------------------------------------------------
		function get_schyera(){
			$schlevelids = $this->input->post('schlevelids');
			$get_schyera = $this->deleinv->get_schyera($schlevelids);
			header("Content-type:text/x-json");
			echo ($get_schyera);
		}
		//from ranglavel --------------------------------------------------
		function get_schranglavel(){
			$yearid = $this->input->post('yearid');
			$get_schranglavel = $this->deleinv->get_schranglavel($yearid);
			header("Content-type:text/x-json");
			echo ($get_schranglavel);
		}
		// from class -----------------------------------------------------
		function get_schclass(){
			$schlevelids = $this->input->post('schlevelids');
			$get_schclass = $this->deleinv->get_schclass($schlevelids);
			header("Content-type:text/x-json");
			echo ($get_schclass);
		}
		//  -----------------------------------------------------------
		function get_paymentType(){
	        $feetypeid = $this->input->post('feetypeid');
	        $programid = $this->input->post('programid');
	        $schlevelid = $this->input->post('schlevelid');
	        $yearid = $this->input->post('yearid');       

	        $get_paymentType = $this->deleinv->get_paymentType($feetypeid, $programid, $schlevelid, $yearid);
	        header('Content-Type: application/json; charset=utf-8');
	        echo $get_paymentType;
	    }

	    // getdata -----------------------------------------------------
	    function Getdatainv(){
			$total_display =$this->input->post('sort_num');
			$this->green->setActiveRole($this->input->post('roleid'));
	  		$total_display = $this->input->post('total_display');
	  		$sortby=$this->input->post("sortby");
			$sorttype=$this->input->post("sorttype");
			$typeno =$this->input->post("typeno");
			$student_num =$this->input->post("student_num");
			$english_name =$this->input->post("english_name");
			$khmer_name =$this->input->post("khmer_name");
			$gender =$this->input->post("gender");
			$schoolid =$this->input->post("schoolid");
			$programid =$this->input->post("programid");
			$schlevelids =$this->input->post("schlevelids");
			$yearid =$this->input->post("yearid");
			$rangelevelid =$this->input->post("rangelevelid");
			$classid =$this->input->post("classid");
			$feetypeid=$this->input->post("feetypeid");
			$term_sem_year_id=$this->input->post("term_sem_year_id");
			$from_date =$this->input->post("from_date");
			$to_date =$this->input->post("to_date");
	  		$where='';
			$sortstr="";
			 	if($typeno!= ""){
		    		$where .= "AND v_history_deleted.typeno_inv_rec LIKE '%".$typeno."%' ";
		     	}
				if($student_num!= ""){
		    		$where .= "AND v_history_deleted.student_num LIKE '%".$student_num."%' ";
		     	}
				if($english_name!= ""){
		    		$where .= "AND  CONCAT(first_name, ' ', last_name) LIKE '%".$english_name."%' ";
		    	}
		    	if($khmer_name!= ""){
		    		$where .= "AND  CONCAT(first_name_kh, ' ', last_name_kh) LIKE '%".$khmer_name."%' ";
		    	}
		 		if($gender!= ""){
		    		$where .= "AND  v_history_deleted.gender = '".$gender."' ";
		    	}
		    	if($schoolid!= ""){
		    		$where .= "AND  v_history_deleted.schoolid = '".$schoolid."' ";
		    	}
		     	if($programid!= ""){
		    		$where .= "AND  v_history_deleted.programid = '".$programid."' ";
		    	}
		    	if($schlevelids!= ""){
		    		$where .= "AND  v_history_deleted.schooleleve = '".$schlevelids."' ";
		    	}
		    	if($yearid!= ""){
		    		$where .= "AND  v_history_deleted.acandemic = '".$yearid."' ";
		    	}
				if($rangelevelid!= ""){
		    		$where .= "AND  v_history_deleted.ranglev = '".$rangelevelid."' ";
		    	}
		    	if($classid!= ""){
		    		$where .= "AND  v_history_deleted.classid = '".$classid."' ";
		    	}
				if($feetypeid!= ""){
		    		$where .= "AND  v_history_deleted.paymentmethod = '".$feetypeid."' ";
		    	}
				if($term_sem_year_id!= ""){
		    		$where .= "AND  v_history_deleted.paymenttype = '".$term_sem_year_id."' ";
		    	}
				if($from_date != ''){
				 	$where .= "AND DATE_FORMAT(v_history_deleted.deleted_date, '%d-%m-%Y') >= '".$from_date."'";    
				}
				if($to_date != ''){
					 $where .= "AND DATE_FORMAT(v_history_deleted.deleted_date, '%d-%m-%Y') <= '".$to_date."'";     
				}
		        (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

			$sqr="SELECT *,
							DATE_FORMAT(deleted_date, '%d-%m-%Y %h:%i %p') AS deleted_date
			 		FROM
						v_history_deleted 
					WHERE type = 21 {$where}";
			
			$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_history_deleted WHERE type = 21 {$where} ")->row()->numrow;
				$getperpage=0;
				if($total_display==''){
					$getperpage=10;
				}else{
					$getperpage=$total_display;
				}
				$paging    = $this->green->ajax_pagination($total_row,site_url()."student_fee/c_history_deleted_invoice",$getperpage,"icon");
				//return "$sql limit {$paging['start']}, {$paging['limit']}";
				if($sortstr!=""){
					$sqr.=$sortstr;
				}
				$getlimit=10;
				if($paging['limit']!=''){
					$getlimit=$paging['limit'];
				}
			$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
				$j='';
				$table='';
				$gtotal=0;
		    	$gdepos=0;
		    	$gbalan=0;
				if(count($data) > 0){
					foreach($data as $row){  //$this->db->query($sqr)->result() as $row	
						$j++;
						$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
						$no_imgs = base_url()."assets/upload/students/NoImage.png";	
						$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
							if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
								$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
							}
						$table.= "<tr>
									<td>".$j."</td>
									<td>".$img."</td>
									<td><strong>".$row->student_num."</strong>
										</br>".$row->last_name_kh.'  '.$row->first_name_kh."
										</br>".$row->first_name.'  '.$row->last_name."
										</br>".$row->gender."
									</td>
									<td>#".$row->typeno_inv_rec."</td>
									<td>".$row->program."</td>
									<td>".$row->sch_level."</td>
									<td>".$row->sch_year."</td>
									<td>".$row->rangelevelname."</td>
									<td>".$row->class_name."</td>
									<td>".$row->period."</td>
									<td>".$row->deleted_byuser."</td>
									<td>".$row->deleted_date."</td>
									<td style='text-align:right'>".number_format($row->amt_total, 2)."</td>
									<td style='text-align:right'>".number_format($row->amt_paid, 2)."</td>
									<td style='text-align:right'>".number_format($row->amt_balance, 2)."</td>
									<td class='remove_tag no_wrap'>";
						$table.= " </td>
							 	</tr> ";	
						$gtotal+=$row->amt_total;
						$gdepos+=$row->amt_paid;
				 		$gbalan+=$row->amt_balance;	
				}
			}else{
					$table.= "<tr>
								<td colspan='15' style='text-align:center;'> 
									We did not find anything to show here... 
								</td>
							</tr>";
				}

					$table.= " </td>
									<td class='typeno toamt' align='right' colspan='12' style='font-family: Times New Roman; font-size: 16px; font-weight:bold;'><b>Total:</td>
					                <td class='amt_total toamt' style='font-family: Times New Roman; font-size: 16px; font-weight:bold;text-align:right;'><b>".number_format($gtotal,2)."$</b></td>
								    <td class='amt_paid toamt' style='font-family: Times New Roman; font-size: 16px; font-weight:bold;text-align:right;'><b>".number_format($gdepos,2)."$</b></td>
								    <td class='amt_balance toamt' style='font-family: Times New Roman; font-size: 16px; font-weight:bold;text-align:right;'><b>".number_format($gbalan,2)."$</b></td>
							 	</tr> ";	

			$arr['data']=$table;
			header('Content-Type:text/x-json');
			$arr['body']=$table;
			$arr['pagination']=$paging;
			echo json_encode($arr); 
			die();
		}


	}