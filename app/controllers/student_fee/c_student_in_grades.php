<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_student_in_grades extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('student_fee/m_student_in_grades', 'studentgrades');

		$this->thead = array("No" => 'no',
	        					"Photo"=>'Photo',
	        					"Student Name"=>'student_num',
	        					"Program"=>'programid',
	        					"School Level"=>'schlevelid',
	        					"Year"=>'year',
	        					"Range Level"=>'rangelevelid',
	        					"Class"=>'classid',
	        					"<input type='checkbox' name='checkAll[]'' class='checkAll ' value=''>"=>'All'
	                            );
	        $this->idfield = "studentid";
	}

	public function index(){	
		$data['idfield'] = $this->idfield;
        $data['thead'] = $this->thead;
        $data['getprogram']=$this->studentgrades->getprograms();
		$this->load->view('header',$data);
		$this->load->view('student_fee/v_student_in_grades',$data);
		$this->load->view('footer',$data);	
	}
	//save -------------------------------------------
	function savedata(){
		$enrollid = $this->input->post('enrollid');
		$enrollid=$this->studentgrades->savedata($enrollid);
		$arr=array('enrollid'=>$enrollid);
		header("Content-type:text/x-json");
		echo json_encode($arr);
	}
	
	// get_schlevel -----------------------------------
    function get_schlevel(){
		$programid = $this->input->post('programid');
		$get_schlevel = $this->studentgrades->get_schlevel($programid);
		//header("Content-type:text/x-json");
		echo ($get_schlevel);
	}

	//yera --------------------------------------------
	function get_schyera(){
		$schlevelids = $this->input->post('schlevelids');
		$get_schyera = $this->studentgrades->get_schyera($schlevelids);
		echo ($get_schyera);
	}

	//from ranglavel ------------------------------------
	function schranglavel(){
		$yearid = $this->input->post('yearid');
		$schranglavel = $this->studentgrades->schranglavel($yearid);
		echo ($schranglavel);
	}

	// from class -------------------------------------
	function get_schclass(){
		$rangelevelid = $this->input->post('rangelevelid');
		$get_schclass = $this->studentgrades->get_schclass($rangelevelid);
		echo ($get_schclass);
	}

	// to_get_schlevel -------------------------------
    function to_get_schlevel(){
		$to_programid = $this->input->post('to_programid');
		$to_get_schlevel = $this->studentgrades->to_get_schlevel($to_programid);
		echo ($to_get_schlevel);
	}

	//yera --------------------------------------------
	function to_get_schyera(){
		$to_schlevelids = $this->input->post('to_schlevelids');
		$to_get_schyera = $this->studentgrades->to_get_schyera($to_schlevelids);
		echo ($to_get_schyera);
	}

	// from class --------------------------------------
	function to_get_schclass(){
		$to_raglevelids = $this->input->post('to_raglevelids');
		$to_get_schclass = $this->studentgrades->to_get_schclass($to_raglevelids);
		echo ($to_get_schclass);
	}

	//from ranglavel ------------------------------------
	function get_schranglavel(){
		$to_yearid = $this->input->post('to_yearid');
		$get_schranglavel = $this->studentgrades->get_schranglavel($to_yearid);
		echo ($get_schranglavel);
	}

   // getdata -------------------------------------------
    function Getdata(){
    	$this->green->setActiveRole($this->input->post('roleid'));
		$total_display =$this->input->post('sort_num');
  		$total_display = $this->input->post('total_display');
  		$sortby=$this->input->post("sortby");
		$sorttype=$this->input->post("sorttype");
		$student_num = trim($this->input->post('student_num', TRUE));
		$full_name = trim($this->input->post('full_name', TRUE));
		$yearid = trim($this->input->post('yearid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelids = trim($this->input->post('schlevelids', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$full_Khmer = trim($this->input->post('full_Khmer', TRUE));
  		$where='';
		$sortstr="";
			if($student_num != ''){
			 	$where .= "AND student_num LIKE '%{$student_num}%'";
			}
			if($full_name != ''){
				$where .= "AND ( CONCAT(
									last_name,
									' ',
									first_name
								) LIKE '%{$full_name}%')";
			}
			if($full_Khmer !=''){
				$where .= " AND ( CONCAT(
									last_name_kh,
									' ',
									first_name_kh
								) LIKE '%{$full_Khmer}%')";
			}	
			if($yearid != ''){
			 	$where .= "AND year = '{$yearid}'";
			}
			if($programid != ''){
			 	$where .= "AND programid = '{$programid}'";
			}
			if($schlevelids != ''){
			 	$where .= "AND schlevelid = '{$schlevelids}'";
			}
			if($classid != ''){
			 	$where .= "AND classid = '{$classid}'";
			}
			if($rangelevelid != ''){
			 	$where .= "AND rangelevelid = '{$rangelevelid}'";
			}
	    	(isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";
	    	//v_student_fee_create_inv
		$sqr="SELECT
				 * FROM
						v_students_in_grades 
		 		
				WHERE 1 = 1 {$where} ";
		$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_students_in_grades WHERE 1 = 1 {$where} ")->row()->numrow;
			$getperpage=0;
			if($total_display==''){
				$getperpage=50;
			}else{
				$getperpage=$total_display;
			}
			$paging    = $this->green->ajax_pagination($total_row,site_url()."student_fee/c_student_in_grades",$getperpage,"icon");
			if($sortstr!=""){
				$sqr.=$sortstr;
			}
			$getlimit=50;
			if($paging['limit']!=''){
				$getlimit=$paging['limit'];
			}
		$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
			$j='';
			$table='';
			$gtotal=0;
			if(count($data) > 0){
				foreach($data as $row){  
					$j++;
					$have_img = base_url()."assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg';		
					$no_imgs = base_url()."assets/upload/students/NoImage.png";	
					$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
						if (file_exists(FCPATH . "assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg')) {				
							$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
						}
					$table.= "<tr class='.tr'>
								<td>".$j."</td>
								<td>".$img."</td>
								<td><strong>".$row->student_num."</strong>
									</br>".$row->last_name_kh.'  '.$row->first_name_kh."
									</br>".$row->last_name.'  '.$row->first_name."
									</br>".$row->gender."
								</td>
								<td style='text-align:center;'>".$row->program."</td>
								<td style='text-align:center;'>".$row->sch_level."</td>
								<td style='text-align:center;'>".$row->sch_year."</td>
								<td style='text-align:center;'>".$row->rangelevelname."</td>
								<td style='text-align:center;'>".$row->class_name."</td>
								<td style='width:3px; text-align:center;'> <input type='checkbox' name='checksub[]' class='checksub' data-studentId='".$row->studentid."' data-schoolid='".$row->schoolid."'></td>
								<td class='remove_tag no_wrap'>";
					$table.= " </td>
						 	</tr> ";	
					
				}
			}else{
				$table.= "<tr>
							<td colspan='9' style='text-align:center;'> 
								We did not find anything to show here... 
							</td>
						</tr>";
			}

		$arr['data']=$table;
		header('Content-Type:text/x-json');
		$arr['body']=$table;
		$arr['pagination']=$paging;
		echo json_encode($arr); 
		die();
	}

}
