<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_student_fee_history_deleted extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('student_fee/m_student_fee_receipt_lists', 'sf');
        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolyearmodel', 'y');
        $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');
        $this->load->model('school/modfeetype', 'f');
        $this->load->model('setup/modotherfee', 'otherfee');
		$this->load->model("setup/area_model","marea");
        $this->load->model("school/classmodel", "c");
		$this->load->model("school/modrangelevelfee","mranglevelfee");

        $this->thead = array("No" => 'no',
                                "Photo"=>'Photo',
                                "Description"=>'student_num',
                                "Date"=>'trandate',
                                "School Level"=>'sch_level',
                                "Year"=>'sch_year',
                                "Range Level"=>'rangelevelname',
                                "Class"=>'class_name',
                                "Study Period"=>'period',
                                "#Invoice"=>'typeno_inv',
                                "#Receipt"=>'typeno',
                                "Amount"=>'amt_paid'
                                );
            $this->idfield = "student_num";
	}

	public function index(){
        $data['idfield'] = $this->idfield;
        $data['thead'] = $this->thead;	
		$this->load->view('header',$data);		
		$this->load->view('student_fee/v_student_fee_history_deleted',$data);
		$this->load->view('footer',$data);	
	}

	public function test(){	
		$this->load->view('header');
		$this->load->view('student_fee/test');
		$this->load->view('footer');	
	}

	public function grid(){
    	$grid = $this->sf->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $grid;
    }

    public function get_schlevel(){
        $programid = $this->input->post('programid');        
        $get_schlevel = $this->sf->get_schlevel($programid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel;
    }

    public function get_year(){
        $programid = $this->input->post('programid');
        $schlevelid = $this->input->post('schlevelid');

        $get_year = $this->sf->get_year($programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    public function get_rangelevel(){
        $programid = $this->input->post('programid');
        $schlevelid = $this->input->post('schlevelid');

        $get_rangelevel = $this->sf->get_rangelevel($programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_rangelevel;
    }

    public function get_class(){
        $programid = $this->input->post('programid');
        $schlevelid = $this->input->post('schlevelid');
        $yearid = $this->input->post('yearid');

        $get_class = $this->sf->get_class($programid, $schlevelid, $yearid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_class;
    }

    public function get_paymentType(){
        $feetypeid = $this->input->post('feetypeid');
        $programid = $this->input->post('programid');
        $schlevelid = $this->input->post('schlevelid');
        $yearid = $this->input->post('yearid');       

        $get_paymentType = $this->sf->get_paymentType($feetypeid, $programid, $schlevelid, $yearid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_paymentType;
    }


}