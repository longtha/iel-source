<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class c_student_in_bus extends CI_Controller{
	    protected $thead;
		protected $idfield;

	    function __construct() {
	        parent::__construct();
			$this->load->model("student_fee/m_student_in_bus","mbus");
	        $this->thead = array("No" => 'no',
	        					"Photo"=>'Photo',
                                "Student Name" => 'student_num',
                                "Bus Name"=> 'busid',
                                "Area Name" => 'area',
                                "Driver Name" => 'driverid',
                                "Date"=> 'trandate',
                                "Way" =>'otherfee',
                                "Invoice"=>'typeno_inv',
                                "<input type='checkbox' name='checkAll[]'' class='checkAll ' value=''> "=> '<input type="checkbox" name="checkAll[]" class="checkAll" value=""> '
	                            );
	        $this->idfield = "studentid";
	    }
	    function index() {
	        $data['idfield'] = $this->idfield;
        	$data['thead'] = $this->thead;
        	$data['areaname']=$this->mbus->allarea();
	        $this->load->view('header',$data);
	        $this->load->view('student_fee/v_student_in_bus',$data);
	        $this->load->view('footer',$data );
	    }

	    // get_bus---------------------------------------
	    function get_bus(){
			$area_name = $this->input->post('area_name');
			$get_bus = $this->mbus->get_bus($area_name);
			header("Content-type:text/x-json");
			echo ($get_bus);
		}
		// get_driver------------------------------------
		function get_driver(){
			$bus_name = $this->input->post('bus_name');
			$get_driver = $this->mbus->get_driver($bus_name);
			header("Content-type:text/x-json");
			echo ($get_driver);
		}

		 // get_busid---------------------------------------
	    function get_busid(){
			$area_id = $this->input->post('area_id');
			$get_busid = $this->mbus->get_busid($area_id);
			header("Content-type:text/x-json");
			echo ($get_busid);
		}
		// get_driver------------------------------------
		function get_driverid(){
			$bus_id = $this->input->post('bus_id');
			$get_driverid = $this->mbus->get_driverid($bus_id);
			header("Content-type:text/x-json");
			echo ($get_driverid);
		}

		//save -------------------------------------------
		function savedata(){
			$studentid = $this->input->post('studentid');
			$studentid=$this->mbus->savedata($studentid);
			$arr=array('studentid'=>$studentid);
			header("Content-type:text/x-json");
			echo json_encode($arr);
		}

		// Getdata  -----------------------------------------------
	    function Getdata(){

			$total_display =$this->input->post('sort_num');
	  		$total_display = $this->input->post('total_display');
	  		$sortby=$this->input->post("sortby");
			$sorttype=$this->input->post("sorttype");
			$studen_id=$this->input->post("studen_id");
			$formdate=$this->input->post("formdate");
			$todate=$this->input->post("todate");
			$area_name=$this->input->post("area_name");
			$bus_name=$this->input->post("bus_name");
			$driver_name=$this->input->post("driver_name");

	  		$where='';
			$sortstr="";
				if($studen_id!= ""){
		    		$where .= "AND ( CONCAT(last_name, ' ', first_name) LIKE '%".$studen_id."%' ";
		    		$where .= "or CONCAT(last_name_kh, ' ', first_name_kh) LIKE '%".$studen_id."%' ";
		    		$where .= "or student_num LIKE '%".$studen_id."%' ) ";
		    		
		     	}
				if($bus_name!= ""){
		    		$where .= "AND busid = '".$bus_name."' ";
		    	}
				if($area_name!= ""){
					$where .= "AND areaid = '".$area_name."' ";
				}
				if($driver_name!= ""){
					$where .= "AND driverid = '".$driver_name."' ";
				}
				if($formdate != ''){
				 $where .= "AND trandate >= '".$this->green->formatSQLDate($formdate)."'";    
				}
				if($todate != ''){
					 $where .= "AND trandate <= '".$this->green->formatSQLDate($todate)."'";     
				}
		        (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

			$sqr="SELECT
						v_tr.student_num,
						v_tr.first_name,
						v_tr.last_name,
						v_tr.first_name_kh,
						v_tr.last_name_kh,
						v_tr.gender,
						v_tr.studentid,
						v_tr.busno,
						v_tr.area,
						v_tr.busid,
						v_tr.areaid,
						v_tr.is_bus,
						v_tr.otherfee_id,
						DATE_FORMAT(trandate, '%d/%m/%Y') AS trandate,
						v_tr.typeno,
						v_tr.driverid,
						v_tr.acandemic,
						v_tr.otherfee,
						v_tr.bus_way_type,
						v_tr.driver_name
					FROM
						v_sch_stud_transport AS v_tr 
					WHERE 1=1 {$where}";

			
			$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_sch_stud_transport WHERE 1=1 {$where}")->row()->numrow;

				$getperpage=0;
				if($total_display==''){
					$getperpage=50;
				}else{
					$getperpage=$total_display;
				}
				$paging    = $this->green->ajax_pagination($total_row,site_url()."student_fee/c_student_in_bus",$getperpage,"icon");
				//return "$sql limit {$paging['start']}, {$paging['limit']}";
				if($sortstr!=""){
					$sqr.=$sortstr;
				}
				$getlimit=50;
				if($paging['limit']!=''){
					$getlimit=$paging['limit'];
				}
			$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
				$j='';
				$table='';
				if(count($data) > 0){
					foreach($data as $row){  
						$j++;
						$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
						$no_imgs = base_url()."assets/upload/students/NoImage.png";	
						$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
							if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
								$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
							}
							
						$table.= "<tr>
									<td class='no'>".$j."</td>
									<td class='Photo'>".$img."</td>
									<td class='student_num'><strong>".$row->student_num."</strong></br>".$row->last_name_kh.'  '.$row->first_name_kh."</br>".$row->last_name.'  '.$row->first_name."</br>".$row->gender."</td>	
									<td class='busid' align='center'>".$row->busno."</td>
									<td class='area' align='center'>".$row->area."</td>
									<td class='driverid' align='center'>".$row->driver_name."</td>
									<td class='trandate' align='center'>".$row->trandate."</td>
									<td class='otherfee' align='center'>".$row->otherfee."</td>
									<td class='typeno_inv' align='center'><a href='".site_url().'/student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno."' target='_blank'>#".$row->typeno."</a></td>
									<td style='text-align:center'><input type='checkbox' name='checksub[]' class='checksub' data-studenttype='".$row->typeno."' data-othefeeid='".$row->otherfee_id."'  data-studentid='".$row->studentid."'>
							 		</td>";
							$table.= " </tr>";	
							 
					}
				}else{
					$table.= "<tr>
								<td colspan='10' style='text-align:center;'> 
									We did not find anything to show here... 
								</td>
							</tr>";
				}

			$arr['data']=$table;
			header('Content-Type:text/x-json');
			$arr['body']=$table;
			$arr['pagination']=$paging;
			echo json_encode($arr); 
			die();
		}
	}