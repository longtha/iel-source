<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class c_assign_payment_method_list extends CI_Controller{
	    protected $thead;
		protected $idfield;
	    function __construct() {
	        parent::__construct();
	        $this->load->model("student_fee/m_assign_payment_method_list","assignpayment");
	        $this->thead = array("No" => 'no',
	        					"Photo"=>'photo',
                                "Student Name" => 'student_num',
                                "Program"=> 'program',
                                "School Level" => 'sch_level',
                                "Year" => 'sch_year',
								"Rang Level​"=> 'rangelevelname',
								"Class"=> 'class_name',
								"Date" => 'trandate'
	                            );
	        $this->idfield = "studentid";
	    }
	    function index() {
			
	        $data['idfield'] = $this->idfield;
        	$data['thead'] = $this->thead;
        	$data['getprogram']=$this->assignpayment->getprograms();
        	$data['schooinfor']=$this->assignpayment->schooinfor();
	        $this->load->view('header',$data);
	        $this->load->view('student_fee/v_assign_payment_method_list',$data);
	        $this->load->view('footer',$data );
	    }
	    // get_schlevel ----------------------------------------------------------------
	    function get_schlevel(){
			$programid = $this->input->post('programid');
			$get_schlevel = $this->assignpayment->get_schlevel($programid);
			header("Content-type:text/x-json");
			echo ($get_schlevel);
		}
		//yera -------------------------------------------------------------------------
		function get_schyera(){
			$schlevelids = $this->input->post('schlevelids');
			$get_schyera = $this->assignpayment->get_schyera($schlevelids);
			header("Content-type:text/x-json");
			echo ($get_schyera);
		}
		//from ranglavel ---------------------------------------------------------------
		function get_schranglavel(){
			$yearid = $this->input->post('yearid');
			$get_schranglavel = $this->assignpayment->get_schranglavel($yearid);
			header("Content-type:text/x-json");
			echo ($get_schranglavel);
		}
		// from class -----------------------------------------------------------------
		function get_schclass(){
			$schlevelids = $this->input->post('schlevelids');
			$get_schclass = $this->assignpayment->get_schclass($schlevelids);
			header("Content-type:text/x-json");
			echo ($get_schclass);
		}
		// Getdata ---------------------------------------------------------------------------------------
	    function Getdata(){
			$total_display =$this->input->post('sort_num');
			$this->green->setActiveRole($this->input->post('roleid'));
	  		$total_display = $this->input->post('total_display');
	  		$sortby=$this->input->post("sortby");
			$sorttype=$this->input->post("sorttype");
			$student_num = $this->input->post("student_num");
			$english_name = $this->input->post("english_name");
			$khmer_name = $this->input->post("khmer_name");
			$gender = $this->input->post("gender");
			$frompymentmethed = $this->input->post("frompymentmethed");
			$schoolid = $this->input->post("schoolid");
			$programid = $this->input->post("programid");
			$schlevelids = $this->input->post("schlevelids");
			$yearid = $this->input->post("yearid");
			$tompymentmethed = $this->input->post("tompymentmethed");
			$ranglavel = $this->input->post("ranglavel");
			$classname = $this->input->post("classname");
			$fromdate = $this->input->post("fromdate");
			$todate = $this->input->post("todate");
	  		$where='';
			$sortstr="";
			 	if($student_num!= ""){
		    		$where .= "AND v_assign_payment_method_list.student_num LIKE '%".$student_num."%' ";
		     	}
			
		    	if($english_name!= ""){
		    		$where .= "AND  CONCAT(first_name, ' ', last_name) LIKE '%".$english_name."%' ";
		    	}
		    	if($khmer_name!= ""){
		    		$where .= "AND  CONCAT(first_name_kh, ' ', last_name_kh) LIKE '%".$khmer_name."%' ";
		    	}
		 		if($gender!= ""){
		    		$where .= "AND  v_assign_payment_method_list.gender = '".$gender."' ";
		    	}
		    	
				
		    	if($schoolid!= ""){
		    		$where .= "AND  v_assign_payment_method_list.schoolid = '".$schoolid."' ";
		    	}
		     	if($programid!= ""){
		    		$where .= "AND  v_assign_payment_method_list.programid = '".$programid."' ";
		    	}
		    	if($schlevelids!= ""){
		    		$where .= "AND  v_assign_payment_method_list.schooleleve = '".$schlevelids."' ";
		    	}
		    	if($yearid!= ""){
		    		$where .= "AND  v_assign_payment_method_list.acandemic = '".$yearid."' ";
		    	}
		    	if($tompymentmethed!= ""){
		    		$where .= "AND  v_assign_payment_method_list.to_paymentmethod = '".$tompymentmethed."' ";
		    	}
				if($frompymentmethed!= ""){
		    		$where .= "AND  v_assign_payment_method_list.from_paymentmethod = '".$frompymentmethed."' ";
		    	}
				
				if($ranglavel!= ""){
		    		$where .= "AND  v_assign_payment_method_list.rangelevelid = '".$ranglavel."' ";
		    	}
		    	if($classname!= ""){
		    		$where .= "AND  v_assign_payment_method_list.classid = '".$classname."' ";
		    	}
			
				if($fromdate != ''){
				 $where .= "AND v_assign_payment_method_list.trandate >= '".$this->green->formatSQLDate($fromdate)."'";    
				}
				if($todate != ''){
					 $where .= "AND v_assign_payment_method_list.trandate <= '".$this->green->formatSQLDate($todate)."'";     
				}
		        (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

			$sqr="SELECT * FROM
					v_assign_payment_method_list 
				WHERE 1=1 {$where}";

			// echo $sqr;
			$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_assign_payment_method_list WHERE 1=1 {$where}")->row()->numrow;

				$getperpage=0;
				if($total_display==''){
					$getperpage=10;
				}else{
					$getperpage=$total_display;
				}
				$paging    = $this->green->ajax_pagination($total_row,site_url()."student_fee/c_assign_payment_method_list",$getperpage,"icon");
				//return "$sql limit {$paging['start']}, {$paging['limit']}";
				if($sortstr!=""){
					$sqr.=$sortstr;
				}
				$getlimit=10;
				if($paging['limit']!=''){
					$getlimit=$paging['limit'];
				}
			$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
				$j='';
				$table='';
				if(count($data) > 0){
					foreach($data as $row){  //$this->db->query($sqr)->result() as $row						

						$j++;
						$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
						$no_imgs = base_url()."assets/upload/students/NoImage.png";	
						$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
							if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
								$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
							}
							
						$table.= "<tr>
									<td class='no'>".$j."</td>
									<td class='photo'>".$img."</td>
									<td class='student_num'><strong>".$row->student_num."</strong></br>".$row->last_name_kh.'  '.$row->first_name_kh."</br>".$row->first_name.'  '.$row->last_name."</br>".$row->gender."</td>	
									<td class='program'>".$row->program."</td>
									<td class='sch_level'>".$row->sch_level."</td>
									<td class='sch_year'>".$row->sch_year."</td>
									<td class='rangelevelname'>".$row->rangelevelname."</td>
									<td class='class_name'>".$row->class_name."</td>
									<td class='tran_date'>".$row->trandate."</td>
									<td class='remove_tag no_wrap'>";

						// from to ---------------------------------------------------------
						$qr_detail = $this->db->query("SELECT
															d.from_to,
															d.is_paid,
															d.feetypeid,
															d.term_sem_year_id,
															d.description,
															d.transno
														FROM
															sch_assign_payment_method_detail AS d
														WHERE
															d.transno = '{$row->typeno}' ");

						if($qr_detail->num_rows() > 0){
							$fromdial = '';
							$todial = '';
							$i='';
							$g='';
							foreach ($qr_detail->result() as $row_from_to) {
								$i++;
								$paymentmethod='';
								if($row_from_to->feetypeid==1){
									$paymentmethod="Term";
								}
								if($row_from_to->feetypeid==2){
									$paymentmethod="Semester";
								}
								if($row_from_to->feetypeid==3){
									$paymentmethod="Year";
								}



								$paid='Unpaid';
								if($row_from_to->is_paid==1){
									$paid="Paid";
								}
								
								$classadd='';
								
								if($row_from_to->from_to=='From'){
									$classadd="cl_From";
								}

								$fromdial.="<tr class=".$classadd.">
											<td>&nbsp;</td>
											<td style='text-align: center;'>".$i."</td>
											<td style='text-align: center;'>".$row_from_to->from_to."</td>
											<td style='text-align: center;' colspan='3'>".$paymentmethod."</td>
											<td style='text-align: center;' colspan='3'>".$row_from_to->description."</td>
											<td style='text-align: center;' colspan='2'>".$paid."</td>
										</tr>";
								
							}
						}
						//#aadfb4;

						$table.="<tr style='background:#DDD '>
									<td>&nbsp;</td>
									<td style='border-top: 0;text-align: center;'>No</td>
									<td style='border-top: 0;text-align: center;'>Tran Type</td>
									<td colspan='3' style='border-top: 0;text-align: center;'>Payment Method Name</td>
									<td colspan='3' style='border-top: 0;text-align: center;'>Study Period</td>
									<td colspan='2' style='border-top: 0;text-align: center;'>Status</td>
								</tr>";

						$table .= $fromdial;



						$table .= $todial;

						$table.= " </td>
							 	</tr> ";	
							
				}
			}else{
					$table.= "<tr>
								<td colspan='9' style='text-align:center;'> 
									We did not find anything to show here... 
								</td>
							</tr>";
				}

			$arr['data']=$table;
			header('Content-Type:text/x-json');
			$arr['body']=$table;
			$arr['pagination']=$paging;
			echo json_encode($arr); 
			die();
		}

	}