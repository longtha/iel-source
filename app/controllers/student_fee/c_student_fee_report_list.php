<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class c_student_fee_report_list extends CI_Controller{
	    protected $thead;
		protected $idfield;
	    function __construct() {
	        parent::__construct();
	        $this->load->model("student_fee/m_student_fee_report_list","stufeereport");
	        $this->thead = array("No" => 'no',
	        					"Other Fee Name"=>'otherfee',
                                "Student Name" => 'student_num',
                                "Program"=> 'program',
                                "School Level" => 'sch_level',
                                "Year" => 'sch_year',
								"Rang Level​"=> 'rangelevelname',
								"Class"=> 'class_name',
								"Date" => 'trandate',
								"#Invoice" =>'typeno',
								"Discount"=>'amt_dis',
								"Amount"=>'amt_line'
	                            );
	        $this->idfield = "studentid";
	    }
	    function index() {
	        $data['idfield'] = $this->idfield;
        	$data['thead'] = $this->thead;
        	$data['getprogram']=$this->stufeereport->getprograms();
        	$data['schooinfor']=$this->stufeereport->schooinfor();
        	$data['otherfee']=$this->stufeereport->otherfee();
	        $this->load->view('header',$data);
	        $this->load->view('student_fee/v_student_fee_report_list',$data);
	        $this->load->view('footer',$data );
	    }
	    // get_schlevel ---------------------------------------------------
	    function get_schlevel(){
			$programid = $this->input->post('programid');
			$get_schlevel = $this->stufeereport->get_schlevel($programid);
			header("Content-type:text/x-json");
			echo ($get_schlevel);
		}
		//yera ------------------------------------------------------------
		function get_schyera(){
			$schlevelids = $this->input->post('schlevelids');
			$get_schyera = $this->stufeereport->get_schyera($schlevelids);
			header("Content-type:text/x-json");
			echo ($get_schyera);
		}
		//from ranglavel --------------------------------------------------
		function get_schranglavel(){
			$yearid = $this->input->post('yearid');
			$get_schranglavel = $this->stufeereport->get_schranglavel($yearid);
			header("Content-type:text/x-json");
			echo ($get_schranglavel);
		}
		// from class -----------------------------------------------------
		function get_schclass(){
			$schlevelids = $this->input->post('schlevelids');
			$get_schclass = $this->stufeereport->get_schclass($schlevelids);
			header("Content-type:text/x-json");
			echo ($get_schclass);
		}
		// Getdataother -----------------------------------------------------
	    function Getdataother(){
			$total_display =$this->input->post('sort_num');
			$this->green->setActiveRole($this->input->post('roleid'));
	  		$total_display = $this->input->post('total_display');
	  		$sortby=$this->input->post("sortby");
			$sorttype=$this->input->post("sorttype");
			$otherfeeid =$this->input->post("otherfeeid");
			$student_num =$this->input->post("student_num");
			$english_name =$this->input->post("english_name");
			$gender =$this->input->post("gender");
			$schoolid =$this->input->post("schoolid");
			$programid =$this->input->post("programid");
			$schlevelids =$this->input->post("schlevelids");
			$yearid =$this->input->post("yearid");
			$ranglavel =$this->input->post("ranglavel");
			$classname =$this->input->post("classname");
			$fromdate =$this->input->post("fromdate");
			$todate =$this->input->post("todate");
	  		$where='';
			$sortstr="";
			 	if($otherfeeid!= ""){
		    		$where .= "AND otherfee_id = '{$otherfeeid}' ";
		    		
		     	}
				if($student_num!= ""){
		    		$where .= "AND v_student_fee_report_list.student_num LIKE '%".$student_num."%' ";
		     	}
				if($english_name != ''){
					$where .= "AND ( CONCAT(
											first_name,
											' ',
											last_name
										) LIKE '%{$english_name}%' ";
					$where .= "or CONCAT(
											first_name_kh,
											' ',
											last_name_kh
										) LIKE '%{$english_name}%' ) ";	
				}		
		 		if($gender!= ""){
		    		$where .= "AND  gender = '".$gender."' ";
		    	}
		    	if($schoolid!= ""){
		    		$where .= "AND  schoolid = '".$schoolid."' ";
		    	}
		     	if($programid!= ""){
		    		$where .= "AND programid = '".$programid."' ";
		    	}
		    	if($schlevelids!= ""){
		    		$where .= "AND  schooleleve = '".$schlevelids."' ";
		    	}
		    	if($yearid!= ""){
		    		$where .= "AND acandemic = '".$yearid."' ";
		    	}
		  
				if($ranglavel!= ""){
		    		$where .= "AND  ranglev = '".$ranglavel."' ";
		    	}
		    	if($classname!= ""){
		    		$where .= "AND  classid = '".$classname."' ";
		    	}
				if($fromdate != ''){
					//$where .= "AND date(trandate) >= '".$this->green->formatSQLDate($fromdate)."' ";
				 	$where .= "AND trandate >= '".$this->green->formatSQLDate($fromdate)."'";    
				}
				if($todate != ''){
					//$where .= "AND date(trandate) <= '".$this->green->formatSQLDate($todate)."' ";
					$where .= "AND trandate <= '".$this->green->formatSQLDate($todate)."'";     
				}
		        (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

			$sqr="SELECT 
						v_student_fee_report_list.otherfee,
						v_student_fee_report_list.student_num,
						v_student_fee_report_list.first_name,
						v_student_fee_report_list.last_name,
						v_student_fee_report_list.first_name_kh,
						v_student_fee_report_list.last_name_kh,
						v_student_fee_report_list.gender,
						v_student_fee_report_list.sch_level,
						v_student_fee_report_list.sch_year,
						v_student_fee_report_list.class_name,
						v_student_fee_report_list.rangelevelname,
						v_student_fee_report_list.program,
						v_student_fee_report_list.type,
						v_student_fee_report_list.typeno,
						v_student_fee_report_list.studentid,
						--DATE_FORMAT(trandate, '%d/%m/%Y') AS trandate,
						v_student_fee_report_list.trandate,
						v_student_fee_report_list.`not`,
						v_student_fee_report_list.amt_line,
						v_student_fee_report_list.prices,
						v_student_fee_report_list.amt_dis,
						v_student_fee_report_list.qty,
						v_student_fee_report_list.otherfee_id,
						v_student_fee_report_list.paymentmethod,
						v_student_fee_report_list.classid,
						v_student_fee_report_list.schooleleve,
						v_student_fee_report_list.programid,
						v_student_fee_report_list.amt_paid,
						v_student_fee_report_list.acandemic,
						v_student_fee_report_list.ranglev,
						v_student_fee_report_list.schoolid
					FROM
						v_student_fee_report_list
				 	WHERE 1=1 {$where} ";
			
			$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_student_fee_report_list WHERE 1=1 {$where}  ")->row()->numrow;

				$getperpage=0;
				if($total_display==''){
					$getperpage=10;
				}else{
					$getperpage=$total_display;
				}
				$paging  = $this->green->ajax_pagination($total_row,site_url()."student_fee/c_student_fee_report_list",$getperpage,"icon");
				//return "$sql limit {$paging['start']}, {$paging['limit']}";
				if($sortstr!=""){
					$sqr.=$sortstr;
				}
				$getlimit=10;
				if($paging['limit']!=''){
					$getlimit=$paging['limit'];
				}
			$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
				$j='';
				$table='';
				$gdepos=0;
				if(count($data) > 0){
					foreach($data as $row){  //$this->db->query($sqr)->result() as $row	
						$j++;
						$table.= "<tr>
									<td>".$j."</td>
									<td>".$row->otherfee."</td>
									<td><strong>".$row->student_num."</strong>
										</br>".$row->last_name_kh.'  '.$row->first_name_kh."
										</br>".$row->first_name.'  '.$row->last_name."
										</br>".$row->gender."
									</td>
									<td>".$row->program."</td>
									<td>".$row->sch_level."</td>
									<td>".$row->sch_year."</td>
									<td>".$row->rangelevelname."</td>
									<td>".$row->class_name."</td>
									<td>".$row->trandate."</td>
									<td style='text-align:center'>
										<a href='".site_url().'/student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno."' target='_blank'>#".$row->typeno."</a>
									</td>
									<td style='text-align:right'>".$row->amt_dis."%</td>
									<td style='text-align:right'>".number_format($row->amt_line, 2)."</td>
									<td class='remove_tag no_wrap'>";

						$table.= " </td>
							 	</tr> ";
						$gdepos+=$row->amt_line;
					}

				}else{
						$table.= "<tr>
									<td colspan='11' style='text-align:center;'> 
										We did not find anything to show here... 
									</td>
								</tr>";
					}
						$table.='<tr>
					                <td class="typeno toamt" align="right" colspan="11" style="font-family: Times New Roman; font-size: 16px; font-weight:bold;"><b>Total:</td>
					                <td class="amt_paid toamt" style="font-family: Times New Roman; font-size: 16px; font-weight:bold;text-align:right"><b>'.number_format($gdepos,2).'$</b></td>
							   	</tr>'; 

			$arr['data']=$table;
			header('Content-Type:text/x-json');
			$arr['body']=$table;
			$arr['pagination']=$paging;
			echo json_encode($arr); 
			die();
		}


	}