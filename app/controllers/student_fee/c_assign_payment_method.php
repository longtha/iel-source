<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class c_assign_payment_method extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('student_fee/m_assign_payment_method', 'rp');
        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolyearmodel', 'y');
        $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');
        $this->load->model('school/modfeetype', 'f');
        $this->load->model('setup/modotherfee', 'otherfee');
		$this->load->model("setup/area_model","marea");
        $this->load->model("school/classmodel", "c");
		$this->load->model("school/modrangelevelfee","mranglevelfee");

		$this->thead = array("No" => 'no',
                                "Photo"=>'Photo',
                                "Student Name"=>'student_num',
                                "Program"=>'program',
                                "School Level"=>'sch_level',
                                "Year"=>'sch_year',
                                "Range Level"=>'rangelevelname',
                                "Class"=>'class_name',
                                "Payment Method"=>'feetypeid',
                                "Status"=>'Status'
                                );
            $this->idfield = "student_num";
	}

	public function index(){
		$data['idfield'] = $this->idfield;
        $data['thead'] = $this->thead;		
		$this->load->view('header',$data);
		$this->load->view('student_fee/v_assign_payment_method',$data);
		$this->load->view('footer',$data);	
	}

	
	public function grid(){
    	$rp = $this->rp->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $rp;
    }

    public function get_schlevel(){
        $programid = $this->input->post('programid') - 0;        
        $get_schlevel = $this->rp->get_schlevel($programid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel;
    }

    public function get_year(){
        $programid = $this->input->post('programid') - 0;
        $schlevelid = $this->input->post('schlevelid') - 0;                
        $get_year = $this->rp->get_year($programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    public function get_rangelevel(){
        $yearid = $this->input->post('yearid') - 0;
        $termid = $this->input->post('termid') - 0;

        $get_rangelevel = $this->rp->get_rangelevel($yearid, $termid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_rangelevel;
    }

    public function get_paymentType(){
        $feetypeid = $this->input->post('feetypeid');
        $programid = $this->input->post('programid');
        $schlevelid = $this->input->post('schlevelid');
        $yearid = $this->input->post('yearid');       

        $get_paymentType = $this->rp->get_paymentType($feetypeid, $programid, $schlevelid, $yearid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_paymentType;
    }
//--------------
    public function Fcstudentinfo(){
		
        $attr_schlevel = $this->input->post('attr_schlevel');
        $attr_class = $this->input->post('attr_class');
        $studentid = $this->input->post('studentid');
        $get_paymentType = $this->rp->Fcstudentinfo($attr_schlevel,$attr_class,$studentid);
		
        header('Content-Type: application/json; charset=utf-8');
        echo $get_paymentType;
    }
// -----------

	public function FCgetPaymenttype(){
		$paymentmethod = $this->input->post('paymentmethod');
		$schlevelid = $this->input->post('shlevelid');
		$yearid = $this->input->post('acandemicid');
		$h_program = $this->input->post('h_program');
		$schoolid=1;//$this->session->userdata('schoolid');
		
		
		$datas="SELECT DISTINCT
						v_study_peroid.period,
						v_study_peroid.payment_type,
						v_study_peroid.term_sem_year_id
						FROM
						v_study_peroid
						WHERE 1=1
						AND programid='".$h_program."'
						AND schlevelid='".$schlevelid."'
						AND yearid='".$yearid."'
						AND payment_type='".$paymentmethod."'
						AND schoolid='".$schoolid."'
						";
		// echo $datas;				
		$result = $this->db->query($datas)->result();
		
			$tr="";
			$tr1="";
			$i=1;
			if(isset($result) && count($result)>0){		
				foreach($result as $rowbusfee){
						if($rowbusfee->payment_type==1){
						$_paymentype="By Term";
						}else if($rowbusfee->payment_type==2){
						$_paymentype="By Semister";
						}else if($rowbusfee->payment_type==3){
						$_paymentype="By Year";
						}
					$tr.='<tr class="inactive">
							<td style="display:none">
								<input type="text" id="term_sem_year_id" class="term_sem_year_id" value="'.($rowbusfee->term_sem_year_id).'">
								<input type="text" id="payment_type" class="payment_type" value="'.($rowbusfee->payment_type).'">
								
								<input type="text" id="from_to" class="from_to" value="To">
								<input type="text" id="ispaid" class="ispaid">
								<input type="text" id="description" class="description" value="'.$rowbusfee->period.'">
							</td>
							<td>'.($i++).'</td>
							<td class="hmt_paymentype">'.$_paymentype.'</td>
							<td class="hmt_period">'.$rowbusfee->period.'</td>
							<td align="right"><input type="checkbox" class="ch_to" id="ch_to" value="1" /></td>
						  </tr>';
					
				}
				$tr1.='<tr class="success">
							<th colspan="1">To</th>
							<th colspan="4" align="right" style="text-align:right !important"><div><span style="color:blue">if checked it will pay next period.</span> <span style="color:greed">Not check it was paid.</span></div></th>
							</tr>'.$tr;
			}
			$arr['gpaymenttype']=$tr1;
			header("Content-type:text/x-json");
			echo json_encode($arr);	
		// echo $datas; exit();
			
	}
	
	public function FCpaymenttyp_price(){
		$yearid = $this->input->post('acandemicid');
		$schlevelid = $this->input->post('shlevelid');
		$rangelevelid = $this->input->post('ranglevid');
		$feetypeid = $this->input->post('paymentmethod');
		$term_sem_year_id = $this->input->post('paymenttype');
		$h_program=$this->input->post('h_program');
		$h_class=$this->input->post('h_class');
		
		// rangelevelfee($yearid="",$schlevelid="",$rangelevelid="",$feetypeid="",$term_sem_year_id="");
		$rows=$this->mranglevelfee->rangelevfee($yearid,$schlevelid,$rangelevelid,$feetypeid,$term_sem_year_id);
		
		/*
		$countmethod="SELECT
					COUNT(studentid) AS studentid
					FROM
						sch_student_fee
					WHERE 1=1
					AND studentid='".$this->input->post('h_studentid')."'
					AND programid='".$h_program."'
					AND schooleleve='".$schlevelid."'
					AND acandemic='".$yearid."'
					AND ranglev='".$rangelevelid."'
					AND paymentmethod <> ''
					";
		// echo $countmethod; exit();
		
		$ispayexist = $this->db->query($countmethod)->row()->studentid;
		$arr['ispayexist']=$ispayexist;		
		*/
			
		$sql="SELECT
					COUNT(studentid) AS studentid
					FROM
						sch_student_fee
					WHERE 1=1
					AND studentid='".$this->input->post('h_studentid')."'
					AND programid='".$h_program."'
					AND schooleleve='".$schlevelid."'
					AND acandemic='".$yearid."'
					AND ranglev='".$rangelevelid."'
					AND paymentmethod='".$feetypeid."'
					AND paymenttype='".$term_sem_year_id."'
					";
			 $count = $this->db->query($sql)->row()->studentid;
			 
			$studentid=$count;
			// echo $sql;
		
		$arr['studentid']=$studentid;
		$arr['rows']=$rows;
		
		
		header("Content-type:text/x-json");
		echo json_encode($arr);
		// echo json_encode($studentid);
		
	}
	
	public function FCsave(){
		$studentid = $this->input->post('stuid_pos');
		$this->rp->MFsave($studentid);
	}
	
	public function FCDelinv(){
		$deltypeno = $this->input->post('deltypeno');
		$this->rp->FMDelinv($deltypeno);
		header("Content-type:text/x-json");
		echo json_encode($deltypeno);
	}
	
	public function FCbusfee(){
		$areaid = $this->input->post('oparea');	
		$datas="SELECT
						sch_setup_area.area,
						sch_setup_bus.busno,
						sch_setup_busarea.busareaid,
						sch_setup_busarea.busid,
						sch_setup_busarea.areaid
					FROM
						sch_setup_busarea
					INNER JOIN sch_setup_bus ON sch_setup_busarea.busid = sch_setup_bus.busid
					INNER JOIN sch_setup_area ON sch_setup_busarea.areaid = sch_setup_area.areaid
					where 1=1
					AND sch_setup_busarea.areaid='".$areaid."'

				";
		// echo $datas; exit();
			
		$result = $this->db->query($datas)->result();
		 // print_r($classes);
		$databusfee="";	
		if(isset($result) && count($result)>0){		
			foreach($result as $rowbusfee){
				$databusfee.="<option attr_pric=".$rowbusfee->busareaid." value='".$rowbusfee->busid."'>".$rowbusfee->busno."</option>";
			}
		}
		$arr['busfe']=$databusfee;
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}
}// maind