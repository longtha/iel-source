<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_student_fee_receipt extends CI_Controller {
	
	protected $thead;
	protected $thead2;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		$this->load->model("student_fee/m_student_fee_receipt","stufee_re");
		// $this->load->model("student_fee/m_student_fee","mstu_fee");	
		$this->load->model("setup/modotherfee","mstu_other_fee");
		$this->load->model("setting/usermodel","user");
		$this->load->model("school/program","mprogram");		
		$this->load->model("school/schoollevelmodel","mshl");	
		$this->load->model("school/schoolyearmodel","mgeyear");
		$this->load->model("school/modrangelevel","mranglevel");
		$this->load->model("school/classmodel","mclass");
		$this->load->model("setup/area_model","marea");
		$this->load->model("school/modfeetype","mfeetype");
		
		 $this->thead = array("No" => 'no',
                                "Photo" => 'photo',
                                "Descriptoin" => 'last_name_kh',
                                "Date"=> 'trandate',
                                "School Level"=>'sch_level',
                                "Year"=>'acandemic',
                                "Range Level"=>'rangelevelname',
                                "Class"=>'class_name',
                                "Invoice" => 'typeno',
                                "Total"=> 'amt_total',
                                "Deposit"=> 'amt_paid',
                                "Balane"=> 'amt_balance',
                                "Status"=>'Status'
	                            );
	}
	public function FCDelinv(){
	$deltypeno = $this->input->post('deltypeno');
	$this->stufee_re->FMDelinv($deltypeno);
	header("Content-type:text/x-json");
	echo json_encode($deltypeno);
	}
	
	function index(){
		$data['idfield'] = $this->idfield;
        $data['thead'] = $this->thead;
		$data['page_header']="Student Fee List";
		$data['getprogram']=$this->mprogram->getprograms();
		$data['get_sle']=$this->mshl->getsch_level();	
		$data['getyears']=$this->mgeyear->getschoolyear();	
		$data['getranglevel']=$this->mranglevel->rangelevels();
		$data['classes']=$this->mclass->allclass();
		$data['mstu_other_fee']=$this->mstu_other_fee->FMotherfee();
		$data['studentfee']=$this->stufee_re->MgetfeeList();
		// $data['oppayment']=$this->mstu_fee->getpaymentmethod();
		$data['marea']=$this->marea->FMareas();
		$data['schooinfor']=$this->stufee_re->schooinfor();
		$data['status']=$this->stufee_re->status();
		$this->load->view('header',$data);
	    $this->load->view('student_fee/v_student_fee_receipt',$data);
	    $this->load->view('footer',$data );
	}

	
	function Fcstudent(){
		$studentid = $this->input->post('stuid_pos');
		$typeno = $this->input->post('typeno');
		
		$data1['student']=$this->stufee_re->getstudent($studentid);
		
		if($typeno!=""){
			$invorder="";
			
	    	$invorder=$this->db->where("typeno",$typeno)->get('sch_student_fee')->row();
			
			$programid=$invorder->programid;
			$schooleleve=$invorder->schooleleve;
			$acandemic=$invorder->acandemic;
			$ranglev=$invorder->ranglev;
			$classid=$invorder->classid;
			$paymentmethod=$invorder->paymentmethod;
			
			$paymenttype=$invorder->paymenttype;
			$areaid=$invorder->areaid;
			$busfeetypid=$invorder->busfeetypid;
			$trandate=$invorder->trandate;
			$amt_total=$invorder->amt_total;
			$amt_paid=$invorder->amt_paid;
			$amt_balance=$invorder->amt_balance;
	    }        
		
		$program = '';
		if($programid!=""){
		$program = $this->db->where('programid', $programid)->get('sch_school_program')->row()->program;	
		}
		
		$chlevel = '';
		if($schooleleve!=""){
		$chlevel = $this->db->where('schlevelid', $schooleleve)->get('sch_school_level')->row()->sch_level;	
		}
		
		$sch_year = '';		
		if($acandemic!=""){
		$sch_year = $this->db->where('yearid', $acandemic)->get('sch_school_year')->row()->sch_year;	
		}
		//-------------------
		$rangelevelname = '';		
		if($ranglev!=""){
		$rangelevelname = $this->db->where('rangelevelid', $ranglev)->get('sch_school_rangelevel')->row()->rangelevelname;	
		}
		
		$class_name = '';		
		if($classid!=""){
		$class_name = $this->db->where('classid', $classid)->get('sch_class')->row()->class_name;	
		}
		
		$schoolfeetype_kh = '';		
		if($paymentmethod!=""){
		$schoolfeetype_kh = $this->db->where('feetypeid', $paymentmethod)->get('sch_school_feetype')->row()->schoolfeetype_kh;	
		}
		
		$period = '';		
		if($paymenttype!=""){
			$sql_period = $this->db->query("SELECT period FROM v_study_peroid WHERE 1=1 AND term_sem_year_id='".$paymenttype."'");
			if($sql_period->num_rows() > 0){
				foreach($sql_period->result() as $row_per){
					$period = $row_per->period;
				}
			}
			//$period = $this->db->where('term_sem_year_id', $paymenttype)->get('v_study_peroid')->row()->period;	
		}
		
		
		//----------
		$area = '';		
		if($areaid!=""){
		$area = $this->db->where('areaid', $areaid)->get('sch_setup_area')->row()->area;	
		}

		$busno='';
		if($busfeetypid!=""){
			$busno = $this->db->where('busid',$busfeetypid)->get('sch_setup_bus')->row()->busno;
		}
			
		// $arayOrder = array("program" =>$program,
		// 				 "chlevel" =>$chlevel,
		// 				 "sch_year" =>$sch_year,
		// 				 "rangelevelname" =>$rangelevelname,
		// 				 "class_name" =>$class_name,
		// 				 "schoolfeetype_kh" =>$schoolfeetype_kh,
		// 				 "paymenttype" =>$period,
		// 				 "area" =>$area,
		// 				 "areaid" =>$areaid,
		// 				 "busfeetypid" =>$busno,
		// 				 "trandate" =>$trandate,			
		// 				 "amt_total" =>$amt_total,			
		// 				 "amt_paid" =>$amt_paid,				
		// 				 "amt_balance" =>$amt_balance						 					 						 						
		// 				 );
		$arayOrder = array("program" =>$program,
						 "chlevel" =>$chlevel,
						 "sch_year" =>$sch_year,
						 "rangelevelname" =>$rangelevelname,
						 "class_name" =>$class_name,
						 "schoolfeetype_kh" =>$schoolfeetype_kh,
						 "paymenttype" =>$period,
						 "area" =>$area,
						 "areaid" =>$areaid,
						 "busfeetypid" =>$busno,
						 "trandate" =>$trandate,			
						 "amt_total" =>$amt_total,			
						 "amt_paid" =>$amt_paid,				
						 "amt_balance" =>$amt_balance						 					 						 						
						 );
			
			
		if(isset($typeno) && count($typeno)>0){
			$trfeetype="";
			$data="";
				
			if($typeno!=""){
	    		$this->db->where("typeno",$typeno);
	    	}        
			$studentfee= $this->db->get('sch_student_fee_type')->result();
			// print_r($studentfee);
			$i=1;
			foreach($studentfee as $rowstufee){
				if($rowstufee->prices > 0){
				$trfeetype.='<tr class="gform_1"><td>
								<input readonly="readonly" id="paytypeid" class="form-control payment-des schoolfee_des" type="text" placeholder="" value="'.$rowstufee->description.'" style="text-align:left">
								
							</td>
							<td>
								<input readonly="readonly" id="schoolfee" class="form-control payment-note schoolfee_des" type="text" placeholder="" value="'.$rowstufee->not.'"  style="text-align:right">
								
							</td>
							<td>
								<input readonly="readonly" id="numqty" class="form-control numqty" type="text" placeholder="" value="'.$rowstufee->qty.'"  style="text-align:right">
								
							</td>
							
							<td>
								<input readonly="readonly" id="" class="form-control payment-unitprice schoolfee_prices" type="text" placeholder=""  value="'.$rowstufee->prices.'"  style="text-align:right">
								
							</td>
							<td>
								<input readonly="readonly" id="paytypeid" class="form-control payment-dis" type="text" placeholder=""  value="'.$rowstufee->amt_dis.'"  style="text-align:right">
								
							</td>
							<td>
								<input readonly="readonly" id="paytypeid" class="form-control payment-amt" type="text" placeholder=""  value="'.$rowstufee->amt_line.'"  style="text-align:right">
								
							</td>
							
						</tr>';
				}
				
			}
		
		}
		
		 
		$have_img = base_url()."assets/upload/students/".$invorder->acandemic.'/'.$data1['student']->studentid.'.jpg';		
		$no_imgs = base_url()."assets/upload/students/NoImage.png";	
		
		$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:155px;height:155px;">';
		if (file_exists(FCPATH . "assets/upload/students/".$invorder->acandemic.'/'.$data1['student']->studentid.'.jpg')) {				
		$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:155px;height:155px;">';
		}
					
		$arr['image_stu']=$img;
		
		$arr['getpaylist']=$trfeetype;
		$arr['getRow']=$data1['student'];
		$arr['arayOrder']=$arayOrder;
		header("Content-type:text/x-json");
		echo json_encode($arr);
	}

	function FCpaymenttyp_price(){
		$yearid = $this->input->post('acandemicid');
		$schlevelid = $this->input->post('shlevelid');
		$rangelevelid = $this->input->post('ranglevid');
		$feetypeid = $this->input->post('oppaymentmethod');
		$term_sem_year_id = $this->input->post('paymenttype');
		// rangelevelfee($yearid="",$schlevelid="",$rangelevelid="",$feetypeid="",$term_sem_year_id="");
		$rows=$this->mranglevelfee->rangelevelfee($yearid,$schlevelid,$rangelevelid,$feetypeid,$term_sem_year_id);
		//$rows=$this->mranglevelfee->rangelevelfee(1,1,1,1,$term_sem_year_id);
		
		// print_r($rows);
		
		header("Content-type:text/x-json");
		echo json_encode($rows);
		
	}

	function FCsave(){
		$studentid = $this->input->post('stuid_pos');
		$h_typeno = $this->input->post('h_typeno');
		$amt_deposit = $this->input->post('amt_deposit');
		$trandate = $this->input->post('trandate');
		$this->stufee_re->MFsaveRec($h_typeno,$amt_deposit,$trandate);
	}

	function FCgetPaymenttype(){
		$paymentmethod = $this->input->post('paymentmethod');
		$schlevelid = $this->input->post('shlevelid');
		$yearid = $this->input->post('acandemicid');
		$schoolid=$this->session->userdata('schoolid');
		
		$result=$this->mfeetype->getStudyPeriod($yearid,$schoolid,$schlevelid,$paymentmethod);
			$datapayment="";	
			if(isset($result) && count($result)>0){		
				foreach($result as $rowbusfee){
					$datapayment.="<option value='".$rowbusfee->term_sem_year_id."'>".$rowbusfee->period."</option>";
				}
			}
			$arr['gpaymenttype']=$datapayment;
			header("Content-type:text/x-json");
			echo json_encode($arr);	
			
		// echo $datas; exit();
			
	}

	function Fcgetop(){
		$opprpid = $this->input->post('opprpid');
		$opget_sle=$this->mshl->getsch_level($opprpid);	
		$opschlev="";
		if(isset($opget_sle) && count($opget_sle)>0){
			foreach($opget_sle as $rowshle){
				$opschlev.="<option value='".$rowshle->schlevelid."'>".$rowshle->sch_level."</option>";
			}
		}
		$arr['opshlev']=$opschlev;
		
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}

	function Fcgetopshlev(){
		$opprpid = $this->input->post('opprpid');
		$shlevid = $this->input->post('shlevid');
		$opshlev=$this->mgeyear->getschoolyear("",$opprpid,$shlevid);
		
		$datashlev="";	
		if(isset($opshlev) && count($opshlev)>0){		
			foreach($opshlev as $rowshlev){
				$datashlev.="<option value='".$rowshlev->yearid."'>".$rowshlev->sch_year."</option>";
			}
		}
		
		$arr['datashlev']=$datashlev;
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}

	
	function FCbusfee(){
		$areaid = $this->input->post('oparea');	
		$datas="SELECT
					sch_setup_busfee.buspriceid,
					sch_setup_busfee.buswaytype,
					sch_setup_busfee.price,
					sch_setup_area.area,
					sch_family_social_infor.description,
					sch_setup_area.areaid
					FROM
					sch_setup_area
					INNER JOIN sch_setup_busfee ON sch_setup_area.areaid = sch_setup_busfee.areaid
					INNER JOIN sch_family_social_infor ON sch_setup_busfee.buswaytype = sch_family_social_infor.famnoteid
					where 1=1
					AND sch_setup_area.areaid='".$areaid."'

				";
		// echo $datas; exit();
			
		$result = $this->db->query($datas)->result();
		 // print_r($classes);
		$databusfee="";	
		if(isset($result) && count($result)>0){		
			foreach($result as $rowbusfee){
				$databusfee.="<option attr_pric=".$rowbusfee->price." value='".$rowbusfee->buspriceid."'>".$rowbusfee->description."</option>";
			}
		}
		$arr['busfe']=$databusfee;
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}

	function Fcgetopyear(){
		$programid = $this->input->post('opprpid');
		$shlevelid = $this->input->post('shlevelid');
		$getyear=$this->mranglevel->rangelevels($schoolid="",$programid,$shlevelid);
		// print_r($getyear);
		$datayear="";	
		if(isset($getyear) && count($getyear)>0){		
			foreach($getyear as $rowyeare){
				$datayear.="<option value='".$rowyeare->rangelevelid."'>".$rowyeare->rangelevelname."</option>";
			}
		}
		
		$arr['datayears']=$datayear;
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}

	function Fcgetclass(){
		$shlevelid = $this->input->post('shlevelid');	
		$classes=$this->mclass->allclass($shlevelid);
		 // print_r($classes);
		$dataclass="";	
		$dataclass="<option value=''>None</option>";	
		if(isset($classes) && count($classes)>0){		
			foreach($classes as $rowlass){
				$dataclass.="<option value='".$rowlass->classid."'>".$rowlass->class_name."</option>";
			}
		}
		$arr['dataclasss']=$dataclass;
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}
	
	function get_schlevel(){
		$program = $this->input->post('program');
		$get_schlevel = $this->stufee_re->get_schlevel($program);
		header("Content-type:text/x-json");
		echo ($get_schlevel);
	}

	function get_schyera(){
		$schoollavel = $this->input->post('schoollavel');
		$get_schyera = $this->stufee_re->get_schyera($schoollavel);
		header("Content-type:text/x-json");
		echo ($get_schyera);
	}

	function get_schranglavel(){
		$yeae = $this->input->post('yeae');
		$get_schranglavel = $this->stufee_re->get_schranglavel($yeae);
		header("Content-type:text/x-json");
		echo ($get_schranglavel);
	}

	function get_schclass(){
		$schoollavel = $this->input->post('schoollavel');
		$get_schclass = $this->stufee_re->get_schclass($schoollavel);
		header("Content-type:text/x-json");
		echo ($get_schclass);
	}

	function search(){
		$total_display =$this->input->post('sort_num');
		$this->green->setActiveRole($this->input->post('roleid'));
  		$total_display = $this->input->post('total_display');

		$id=$this->input->post('id');
		$invoice = $this->input->post('invoice');
		$studen_id = $this->input->post('studen_id');
		$studentname = $this->input->post('studentname');
		$schoolinfor =$this->input->post('schoolinfor');
		$formdate = $this->input->post('formdate');
		$todate = $this->input->post('todate');
		$program = $this->input->post('program');
		$schoollavel = $this->input->post('schoollavel');
		$yeae = $this->input->post('yeae');
		$classname = $this->input->post('classname');
		$ranglavel = $this->input->post('ranglavel');
		$status = $this->input->post('status');
		$sortby=$this->input->post("sortby");
		$sorttype=$this->input->post("sorttype");
		$sortstr="";
		$where='';
		
		$where.=" AND v_student_fee_create_receipt.type=17";
		
			if($status!= ""){
	    			$where .= " AND  v_student_fee_create_receipt.is_status = '".$status."' ";
	    		}
			if($ranglavel!= ""){
	    		$where .= " AND  v_student_fee_create_receipt.ranglev = '".$ranglavel."' ";
	    	}
			if($classname!= ""){
	    		$where .= " AND  v_student_fee_create_receipt.classid = '".$classname."' ";
	    	}
	    	if($yeae!= ""){
	    		$where .= " AND  v_student_fee_create_receipt.acandemic = '".$yeae."' ";
	    	}
	    	if($schoollavel!= ""){
	    		$where .= " AND  v_student_fee_create_receipt.schooleleve = '".$schoollavel."' ";
	    	}

	        if($schoolinfor!= ""){
	    		$where .= " AND  v_student_fee_create_receipt.schoolid = '".$schoolinfor."' ";
	    	}

			// if($studentname!=''){
	  		//    $this->db->like("CONCAT(first_name, ' ', last_name)", $studentname, "both");
	  		//   }
	        //if($studentname!= ""){
	    		//$where .= "AND  v_student_fee_create_receipt.fullname LIKE '%".$studentname."%' ";
	    	// }
			//--------------
			if($studentname != ''){
			$where .= " AND ( CONCAT(
									first_name,
									' ',
									last_name
								) LIKE '%{$studentname}%' ";
			$where .= " or CONCAT(
									first_name_kh,
									' ',
									last_name_kh
								) LIKE '%{$studentname}%' ) ";			
				// $this->db->like("CONCAT(first_name, ' ', last_name)", $full_name, "both");
			}
			//--------------
			// if($studen_id != ''){ fullname
			//    $this->db->like('student_num', $studen_id,'both');
			// }
			if($studen_id!= ""){
	    		$where .= " AND  v_student_fee_create_receipt.student_num LIKE '%".$studen_id."%' ";
	    	}

	        if($program!= ""){
	    		$where .= " AND  v_student_fee_create_receipt.programid = '".$program."' ";
	    	}
			if($formdate != ''){
				 	$where .= " AND DATE_FORMAT(v_student_fee_create_receipt.trandate, '%d-%m-%Y') >= '".$formdate."'";    
			}
			if($todate != ''){
				 $where .= " AND DATE_FORMAT(v_student_fee_create_receipt.trandate, '%d-%m-%Y') <= '".$todate."'";     
			}

	        if($invoice!= ""){
	    		$where .= " AND  v_student_fee_create_receipt.typeno LIKE '%".$invoice."%' ";
	    	}
        	$sortstr.= ((isset($sortby) && $sortby!="")?" ORDER BY `".$sortby."` ".$sorttype:" ORDER BY v_student_fee_create_receipt.typeno  DESC");
		 // $qr = $this->db->get("v_student_fee_create_receipt");

		$sqr="SELECT *,
					DATE_FORMAT(trandate, '%d-%m-%Y %h:%i %p') AS trandate
		 		FROM
					v_student_fee_create_receipt 
				WHERE 1=1 {$where}";   //limit ".$total_display."
		 
		// echo $sqr;
		$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_student_fee_create_receipt WHERE 1=1 {$where}")->row()->numrow;

			$getperpage=0;
			if($total_display==''){
				$getperpage=10;
			}else{
				$getperpage=$total_display;
			}
			$paging    = $this->green->ajax_pagination($total_row,site_url()."student_fee/c_student_fee_receipt",$getperpage,"icon");
			//return "$sql limit {$paging['start']}, {$paging['limit']}";
			if($sortstr!=""){
				$sqr.=$sortstr;
			}
			$getlimit=10;
			if($paging['limit']!=''){
				$getlimit=$paging['limit'];
			}

		
		$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();

		
			$j='';
			$table='';
		    $gtotal=0;
		    $gdepos=0;
		    $gbalan=0;
			$sqlperiod="";
			$dis_period="";
			
		    if(count($data) > 0){
				foreach($data as $row){  //$this->db->query($sqr)->result() as $row
					$liks="";
					$lik_void_inv="";
					
					$liks="<div data-toggle='modal' data-target='#mdinvoice' class='t_studentcode' attr_id=".$row->studentid."  attr_typeno=".$row->typeno." >Create<br>Receipt</div>";
					$lik_void_inv="<div data-toggle='modal' data-target='#dl_viod' class='void_inv' attr_id=".$row->studentid."  attr_typeno=".$row->typeno." >Void<br>Invoice</div>";
					
					if($row->is_status==2){// partial
					$cl_trstat='warning';
					}else{
					$cl_trstat='inactive';	
					}
					
					$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
					$no_imgs = base_url()."assets/upload/students/NoImage.png";	
					
					$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
					if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
					$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
					}
				
					$j++;
					
					//----------
						$sqlperiod="SELECT
										v_study_peroid.period
										FROM
										v_study_peroid
										WHERE 1=1
										AND term_sem_year_id='".$row->paymenttype."'
										AND payment_type='".$row->paymentmethod."'
									";		
						$period = $this->db->query($sqlperiod);
						$dis_period="";
						if($period->num_rows()){
							$dis_period = $period->row()->period;
						}
						
					//----------
					
					$table.= "<tr class=".$cl_trstat.">
								<td class='no'>".$j."</td>
								<td class='photo'>".$img."</td>
								<td class='last_name_kh'><strong>".$row->student_num."</strong></br>".$row->last_name_kh.'  '.$row->first_name_kh."</br>".$row->first_name.'  '.$row->last_name."</td>	
								<td class='trandate'>".$row->trandate."</td>
								<td class='sch_level'>".$row->sch_level."</td>
								<td class='acandemic'>".$row->sch_year."</td>
								<td class='rangelevelname'>".$row->rangelevelname."</td>
								<td class='class_name'>".$row->class_name."</td>
								<td class='typeno'>".$row->typeno."</td>
								<td class='amt_total'>".number_format($row->amt_total,2)."</td>
								<td class='amt_paid'>".number_format($row->amt_paid,2)."</td>
								<td class='amt_balance'>".number_format($row->amt_balance,2)."</td>
								<td class='remove_tag no_wrap'>".$dis_period."</td>
								<td class='remove_tag no_wrap'>".$liks."</td>
								<td class='remove_tag no_wrap'>".$lik_void_inv."</td>
								
						 ";	
					$gtotal+=$row->amt_total;
					$gdepos+=$row->amt_paid;
			 		$gbalan+=$row->amt_balance;	
				}
			}else{
				$table.= "<tr>
								<td colspan='14' style='text-align:center;'> 
									We did not find anything to show here... 
								</td>
							</tr>";
				}
				$table.='<tr>
							<td class="no"></td>
						​​​​ ​​​​​  	<td class="photo"></td>
							<td class="last_name_kh"></td>
							<td class="trandate"></td>
							<td class="sch_level"></td>
							<td class="acandemic"></td>
							<td class="rangelevelname"></td>
							<td class="class_name"></td>
			                <td class="typeno toamt" align="center" colspan="0" style="font-family: Times New Roman; font-size: 18px; font-weight:bold;"><b>Total:</td>
			                <td class="amt_total toamt"><b>'.number_format($gtotal,2).'</b></td>
						    <td class="amt_paid toamt"><b>'.number_format($gdepos,2).'</b></td>
						    <td class="amt_balance toamt"><b>'.number_format($gbalan,2).'</b></td>
							<td colspan="2"></td>
						</tr>'; 


		header('Content-Type:text/x-json');
		$arr['data']=$table;
		$arr['body']=$table;
		$arr['pagination']=$paging;
		echo json_encode($arr);
		die();
	}

}
