<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_student_receipt_cash_return extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("student_fee/m_student_fee_invoice","mstu_fee");
	}
	
	function index(){
		
		 $mo = '';
       // $mo = $this->db->where('typeno', $_GET['param_reno'])->get('sch_student_fee_rec_order')->row();
	  // $_GET['forinv']="";
	
	  
		 $sqls ="SELECT *, DATE_FORMAT(trandate, '%d-%m-%Y ') AS trandate 
		 							FROM sch_student_fee_rec_order
		 							WHERE 1=1 AND type =32 AND typeno = '".$_GET['param_reno']."' ";
									// echo $sqls;
		 $mo = $this->db->query($sqls)->row();
		
			$type="";
			$typeno="";
			$type=$mo->type;
			$data['typeno']=$mo->typeno;
			$data['typeno_inv']=$mo->typeno_inv;
			$data['trandate']=$mo->trandate;
			$timeid=$data['timeid']=$mo->timeid;
			
			$data['amt_paid']=$mo->amt_paid;
			$data['programid']=$mo->programid;	
			// $data['duedate']=($this->green->convertSQLDate($mo->duedate));
			// $data['trandate']=($this->green->convertSQLDate($mo->trandate));
			
			$data['ranglev']=$mo->ranglev;
			$data['studentcode']=$mo->studentid;
			
			
			$data['paymentmethod']=$mo->paymentmethod;
			$data['paymenttype']=$mo->paymenttype;
			
			$data['acandemic']=$mo->acandemic;
			$data['typeno_inv']=$mo->typeno_inv;
			$ranglev=$mo->ranglev;
			$classid=$mo->classid;
			$notepaid="";
			if($mo->note!=""){
				$notepaid="<u><strong>NOTE:</strong></u>&nbsp;".$mo->note;				
			}
			$data['notepaid']=$notepaid;
			
			$data['acandemicname']="";
			
			if($data['acandemic']!="" and $data['acandemic']!=0){
			$acandemicname = $this->db->where('yearid', $data['acandemic'])->get('sch_school_year')->row()->sch_year;
				$data['acandemicname']=$acandemicname;
			}
			
			$stuinfo ='';
			$stuinfo = $this->db->where('studentid', $data['studentcode'])->get('sch_student')->row();
			
			
			$data['student_num']=$stuinfo->student_num;
			$data['first_name_kh']=$stuinfo->first_name_kh;
			$data['last_name_kh']=$stuinfo->last_name_kh;
			$data['gender']=$stuinfo->gender;
			$data['phone1']=$stuinfo->phone1;
			$data['studentid']=$stuinfo->student_num;
			
			// $data['fullname']=$data['last_name_kh'].'&nbsp;'.$data['first_name_kh'];
			$data['fullname']=$stuinfo->last_name.'&nbsp;'.$stuinfo->first_name;
			
			$data['rangelevelname']="";
			
			if($classid!="" and $classid!=0){
				$getClass="SELECT
						sch_class.class_name
						FROM
						sch_class
						WHERE classid='".$classid."'
						";
						// echo $getClass;
						$data['rangelevelname'] = $this->db->query($getClass)->row()->class_name;
			}
		
			if($ranglev!="" and $ranglev!=0){
				//--------
				$sqltime="SELECT
							sch_time.from_time,
							sch_time.am_pm,
							sch_time.to_time
							FROM
							sch_school_rangelevtime
							INNER JOIN sch_time ON sch_school_rangelevtime.timeid = sch_time.timeid
							where 1=1 
							AND sch_time.timeid='".$timeid."'
							AND rangelevelid='".$ranglev."'
						  ";
						 //  echo $sqltime;
						 
				$gettime="";
				$gettime = $this->db->query($sqltime)->result();
				
				$tr_time="";
				$data['from_to_time']="";
				if(isset($gettime) && count($gettime)>0){
					foreach($gettime as $rowde){
						$tr_time.='<tr>
										<td class="inv_info" style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">'.$rowde->from_time.'</td>
										<td style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">-</td>
										<td style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">'.$rowde->to_time.'</td>
										<td style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">'.$rowde->am_pm.'</td>
								  </tr>';
					};
					$data['from_to_time']='<table>'.$tr_time.'</table>';
				};
			}// end 
			
		//==============================================================
			$data['period']="";
					$data['start_date']="";
					$data['end_date']="";
					
			$w="";
			if($data['paymentmethod']!="" && $data['paymenttype']!=""){
				$datasele="SELECT
							v_study_peroid.period,
							v_study_peroid.from_date,
							v_study_peroid.to_date
						FROM
							v_study_peroid
						WHERE 1=1								
						AND schlevelid=".$mo->schooleleve."									
						AND schoolid=".$mo->schoolid."	
						AND yearid=".$data['acandemic']."	
						AND programid=".$data['programid']."
						AND payment_type=".$data['paymentmethod']."
						AND term_sem_year_id=".$data['paymenttype']."
						";
					// echo $datasele; 
					$getrow = $this->db->query($datasele)->row();
					
					if(count($getrow)>0){
							$data['period']=$getrow->period;
							
							$data['start_date']=($this->green->convertSQLDate($getrow->from_date));
							$data['end_date']=($this->green->convertSQLDate($getrow->to_date));
					}			
			}		
			//==============================================================
			//----------------------------------
			
			/*if(isset($_GET['forinv'])==1){
			$wheres=" AND typeno_inv = '".$_GET['param_reno']."'";
			}else{
			$wheres=" AND typeno = '".$_GET['param_reno']."'";
			}
			*/
			
			// $wheres=" AND typeno = '".$data['typeno_inv']."'";
			$datas="SELECT
						rede.id,
						rede.type,
						rede.typeno,
						rede.studentid,
						rede.trandate,
						rede.description,
						rede.`not`,
						rede.amt_line,
						rede.prices,
						rede.amt_dis,
						rede.qty,
						rede.type_inv,
						rede.typeno_inv,
						rede.otherfee_id,
						rede.is_bus,
						rede.areaid,
						rede.busid,
						rede.student_fee_type_id,
						rede.return_amount
					FROM
						sch_student_fee_rec_detail AS rede
					WHERE
						1 = 1 
					AND rede.type = 32 
					AND rede.typeno='".$_GET['param_reno']."'
					ORDER BY
						rede.typeno ASC 
			
			";
			// echo $datas; exit();
			
			$result = $this->db->query($datas)->result();
			// print_r($classes);
			$trinv="";	
			$amttotal_return=0;
			$ii=1;
			// echo $_GET['param_reno'].'test';

			if(isset($result) && count($result)>0){	
				$des="";	
				foreach($result as $rowde){
					if($rowde->not !=""){
						$des=$rowde->description.'-'.$rowde->not;	
					}else{
						$des=$rowde->description;
					}
					$trinv.='<tr>
								<td colspan="5" style="width:80%;border-right: none;">'.$des.'</td>
								<td style="border-left: none; width:5%">'.($rowde->amt_dis - 0  > 0 ? ($rowde->amt_dis.'%') : '').'</td>
								<td align="right" style="width:15%; padding-right:10px;">'.number_format($rowde->return_amount,2)."$".'</td>
							</tr>';
					$amttotal_return+=($rowde->return_amount);
				}
				$trinv.='<tr>
								<td colspan="6" align="right" style="border-left:none;border-bottom:none;"><b>Total</b></td>
								<td align="right" style="width:15%; padding-right:10px;">'.number_format($amttotal_return,2)."$".'</td>
							</tr>';
				// $trinv.='<tr><td colspan="7" style="border:none;">'.$mo->note.'</td></tr>';
			// $sqldatas="SELECT
			// 				fo.typeno_inv,
			// 				fo.amt_paid,
			// 				fo.typeno,
			// 				fo.trandate,
			// 				DATE_FORMAT(trandate, '%d-%m-%Y ') AS trandate
			// 			FROM
			// 				sch_student_fee_rec_order AS fo
			// 			WHERE 1=1 
			// 			AND fo.typeno_inv ='".$mo->typeno_inv."' 
			// 			-- AND typeno ='".$_GET['param_reno']."'
			// 			ORDER BY typeno,trandate
			// 			";
			// 			// echo $sqldatas;
			// $dataorder=$this->db->query($sqldatas);

			// $sqldata="SELECT
			// 			sfee.amt_total,
			// 			sfee.amt_paid,
			// 			sfee.amt_balance,
			// 			sfee.typeno
			// 		FROM
			// 			sch_student_fee AS sfee
			// 		WHERE 1=1 AND sfee.typeno='{$mo->typeno_inv}'
			// 		order by sfee.typeno
			// 		";
					// echo $sqldata;
			// $typeno_inv_row = $this->db->query($sqldata)->row_array();
												

			// $amt = $typeno_inv_row['amt_total'];
				// if($dataorder->num_rows() > 0){
				// 	$balance = 0;
				// 	$ii = 0;
				// 	foreach($dataorder->result() as $row){ 
				// 			$paid_amt = '';
				// 			$amount ='';
				// 			if($ii == 0){
				// 				$paid_amt = $amt;
				// 				$amount.='<labale>Amount:&nbsp;'.number_format($paid_amt, 2)."$".'</labale>';
				// 			}
				// 			$ii++;
				// 			$balance = $amt-$row->amt_paid;
				// 			if($row->typeno){
				// 				$trinv.='<tr>
				// 							<td>Receipt No:&nbsp;'.$row->typeno.'</td>
				// 							<td>Date:&nbsp;'.$row->trandate.'</td>
				// 							<td>'.$amount.'</td>
				// 							<td>Paid:&nbsp;'.number_format($row->amt_paid, 2)."$".'</td>
				// 							<td colspan="2">Balance:&nbsp;'.number_format($balance, 2)."$".'</td>
				// 						</tr>';
				// 				$amt = $balance;
				// 			}
						
				// 	}

				// }

			}

			$data['trinv']=$trinv;
			// $data['trinv']=$trinv;
		// $this->load->view('student_fee/v_student_invoice');
		// $this->parser->parse('student_fee/v_student_print_receipt',$data);
		$this->parser->parse('student_fee/v_student_print_receipt_cash_return',$data);
		
	}// function index(){
		
	
		
}
