<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class c_treansport extends CI_Controller{
	    protected $thead;
		protected $idfield;

	    function __construct() {
	        parent::__construct();
	        $this->load->model("student_fee/m_treansport","trenspot");
	        $this->thead = array("No" => 'no',
	        					"Photo"=>'Photo',
                                "Student Name" => 'student_num',
                                "Academic Year" => 'sch_year',
                                "Address" => 'student_address',
                                "Bus No"=> 'busid',
                                "Area Name" => 'area',
                                "Driver Name" => 'driverid',
                                "Date"=> 'trandate',
                                "Way" =>'otherfee',
                                "Receipt"=>'typeno',
                                "Note"=> 'note',
                                "Action"=> 'Action'
	                            );
	        $this->idfield = "studentid";
	    }
	    function index() {
	        $data['idfield'] = $this->idfield;
        	$data['thead'] = $this->thead;
        	$data['areaname']=$this->trenspot->allarea();
	        $this->load->view('header',$data);
	        $this->load->view('student_fee/v_treansport',$data);
	        $this->load->view('footer',$data );
	    }
	 	public function get_year_and_grade_data()
		{
			$opt = array();
			$y_opt = array();
			$get_year_data = $this->trenspot->getschoolyear();
			if(count($get_year_data) > 0){
				foreach($get_year_data as $r_y){				
					$y_opt[] = $r_y;
				}
			}			
			$opt["year"] = $y_opt;
			header('Content-Type: application/json');
			echo json_encode($opt);
		}
	// get_bus =================================================
	    function get_bus(){
			$area_name = $this->input->post('area_name');
			$get_bus = $this->trenspot->get_bus($area_name);
			header("Content-type:text/x-json");
			echo ($get_bus);
		}
	// get_driver================================================
		function get_driver(){
			$bus_name = $this->input->post('bus_name');
			$get_driver = $this->trenspot->get_driver($bus_name);
			header("Content-type:text/x-json");
			echo ($get_driver);
		}
	// Getdata===================================================
	    function Getdata(){
			$total_display =$this->input->post('sort_num');
			$this->green->setActiveRole($this->input->post('roleid'));
	  		$total_display = $this->input->post('total_display');
	  		$sortby=$this->input->post("sortby");
			$sorttype=$this->input->post("sorttype");
			$studen_id=$this->input->post("studen_id");
			$bus_name=$this->input->post("bus_name");
			$formdate=$this->input->post('formdate');
			$area_name=$this->input->post("area_name");
			$driver_name=$this->input->post("driver_name");
			$todate=$this->input->post('todate');
			$adcademic_year_id=$this->input->post('adcademic_year_id');
	  		$where='';
			$sortstr="";
				if($studen_id!= ""){
		    		$where .= "AND ( CONCAT(first_name, ' ', last_name) LIKE '%".$studen_id."%' ";
		    		$where .= "or CONCAT(last_name_kh, ' ', first_name_kh) LIKE '%".$studen_id."%' ";
		    		$where .= "or student_num LIKE '%".$studen_id."%' ) ";
		    		
		    	}
		    	if($adcademic_year_id!= ""){
		    		$where .= "AND yearid = '".$adcademic_year_id."' ";
		    	}
				if($bus_name!= ""){
		    		$where .= "AND busid = '".$bus_name."' ";
		    	}
				if($area_name!= ""){
					$where .= "AND  areaid = '".$area_name."' ";
				}
				if($driver_name!= ""){
					$where .= "AND driverid = '".$driver_name."' ";
				}
				if($formdate != ''){
				 $where .= "AND trandate >= '".$this->green->formatSQLDate($formdate)."'";    
				}
				if($todate != ''){
					 $where .= "AND trandate <= '".$this->green->formatSQLDate($todate)."'";     
				}
		        $sortstr.= (isset($sortby) && $sortby!="")?" ORDER BY `".$sortby."` ".$sorttype:" ORDER BY typeno DESC";

			$sqr="SELECT
						v_tr.student_num,
						v_tr.first_name,
						v_tr.last_name,
						v_tr.first_name_kh,
						v_tr.last_name_kh,
						v_tr.gender,
						v_tr.studentid,
						v_tr.busno,
						v_tr.area,
						v_tr.busid,
						v_tr.areaid,
						v_tr.is_bus,
						v_tr.otherfee_id,
						DATE_FORMAT(trandate, '%d/%m/%Y') AS trandate,
						v_tr.typeno,
						v_tr.driverid,
						v_tr.acandemic,
						v_tr.otherfee,
						v_tr.bus_way_type,
						v_tr.driver_name,
						v_tr.sch_year,
						v_tr.note,
						v_tr.address,
						v_tr.rec_driverid,
						v_tr.rec_busid,
						v_tr.rec_areaid
					FROM
						v_sch_stud_transport AS v_tr 
				WHERE 1=1 {$where}";

			
			$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_sch_stud_transport WHERE 1=1 {$where}")->row()->numrow;

				$getperpage=0;
				if($total_display==''){
					$getperpage=10;
				}else{
					$getperpage=$total_display;
				}
				$paging    = $this->green->ajax_pagination($total_row,site_url()."student_fee/c_treansport",$getperpage,"icon");
				//return "$sql limit {$paging['start']}, {$paging['limit']}";
				if($sortstr!=""){
					$sqr.=$sortstr;
				}
				$getlimit=10;
				if($paging['limit']!=''){
					$getlimit=$paging['limit'];
				}
			$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
				$j='';
				$table='';
				if(count($data) > 0){
					foreach($data as $row){  //$this->db->query($sqr)->result() as $row
						
						$j++;
						$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
						$no_imgs = base_url()."assets/upload/students/NoImage.png";	
						$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
							if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
								$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
							}
							
						$table.= "<tr>
									<td class='no'>".$j."</td>
									<td class='Photo'>".$img."</td>
									<td class='student_num'><strong>".$row->student_num."</strong></br>".$row->last_name_kh.'  '.$row->first_name_kh."</br>".$row->first_name.'  '.$row->last_name."</br>".$row->gender."</td>	
									<td class='sch_year'>".$row->sch_year."</td>
									<td class='student_address' align='center'>".$row->address."</td>
									<td class='busid' align='center'>".($row->rec_busid==0?$row->busno:$this->trenspot->get_bus_name($row->rec_busid))."</td>
									<td class='area' align='center'>".($row->rec_areaid==""?$row->driver_name:$this->trenspot->get_area_name($row->rec_areaid))."</td>
									<td class='driverid' align='center'>".($row->rec_driverid==0?$row->area:$this->trenspot->get_driver_name($row->rec_driverid))."</td>
									<td class='trandate'>".$row->trandate."</td>
									
									<td class='otherfee' align='center'>".$row->otherfee."</td>
									<td class='typeno_inv' align='center'><a href='".site_url().'/student_fee/c_student_print_receipt?param_reno='.$row->typeno."' target='_blank'>#".$row->typeno."</a></td>
									<td class='Other'>".$row->note."</td>
									<td class='remove_tag no_wrap' align='center'>
									<p data-placement='top' data-toggle='tooltip' title='Edit'><button class='btn btn-primary btn-xs click_studen_transport_edit'attr_address='".$row->address."' attr_note='".$row->note."' attr_areaid='".$row->areaid."' attr_busid='".$row->busid."' attr_reno='".$row->typeno."' data-title='Edit' data-toggle='modal' data-target='#edit' ><span class='glyphicon glyphicon-pencil'></span></button></p>
									";
							$table.= " </td>
							 </tr>
							 ";	
					}
				}else{
					$table.= "<tr>
								<td colspan='11' style='text-align:center;'> 
									We did not find anything to show here... 
								</td>
							</tr>";
				}

			$arr['data']=$table;
			header('Content-Type:text/x-json');
			$arr['body']=$table;
			$arr['pagination']=$paging;
			echo json_encode($arr); 
			die();
		}
		public function student_transport_info(){
			$receipt_no      = set_value('receipt_no');
			$area_id         = set_value('area_id'); 
			$bus_id          = set_value('bus_id');
			$get_area_name   = set_value('get_area_name');
			$get_bus_no      = set_value('get_bus_no');
			$get_driver_name = set_value('get_driver_name');
			$get_note        = set_value('get_note');
			$get_address     = set_value('get_address');
			$data=array(
		    		'areaid'=>$area_id,
					'busid'=>$get_bus_no,
					'driverid'=>$get_driver_name,
					'note'=>$get_note,
					'address'=>$get_address,
					'updated_at'=>date('Y-m-d')
				);
			$arr = array();
			header('Content-Type:text/x-json');
			$arr['success']=$this->trenspot->update_student_transport($receipt_no,$data);
			echo json_encode($arr); 
			die();
		}
	}

