<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Contract extends CI_Controller{

		function __construct(){
			parent::__construct();				
			$this->load->model('employee/modecontract','contract');
			//$this->load->controller('employee/employee','employee');
			$this->load->helper(array('form', 'url'));			
		}

		function Index(){
			$m='';
			$p='';
			if(isset($_GET['m'])){
			    $m=$_GET['m'];
			}
			if(isset($_GET['p'])){
			    $p=$_GET['p'];
			}

			$where = "";	
			if(isset($_GET['emp_id'])){
				$where = "AND empid=".$_GET['emp_id'];
			}

			$page=0;
			if(isset($_GET['per_page'])) $page=$_GET['per_page'];
				
			$this->load->library('pagination');	
			$config['base_url'] = site_url()."/employee/contract?pg=1&m=$m&p=$p";		
			$config['total_rows'] = $this->green->getTotalRow("SELECT * FROM sch_emp_contract WHERE 1=1 {$where} ");
			$config['per_page'] =50;
			//$config['use_page_numbers'] = TRUE;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';

			$this->pagination->initialize($config);	
			$limi=" limit ".$config['per_page'];
			if($page>0){
				$limi=" limit ".$page.",".$config['per_page'];
			}

			$sql_page = "SELECT * FROM sch_emp_contract WHERE 1=1 {$where} ORDER BY con_id DESC {$limi} ";

			$data['tdata']=$this->green->getTable($sql_page);
			$data['page_header']="Contract List";
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/contract_list', $data);
			$this->parser->parse('footer', $data);
		}
		function add(){
			$data['page_header']="New Contract";			
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/contract_form', $data);
			$this->parser->parse('footer', $data);
		}
		function delete($con_id){
			$extension=$this->db->query("SELECT extension FROM sch_emp_contract WHERE con_id='$con_id'")->row()->extension;
			$this->load->helper('download');

			if ($extension!=''){
				//$data = file_get_contents(base_url("assets/upload/employees/contracts/$con_id$extension"));
				$getdata=base_url("assets/upload/employees/contracts/$con_id$extension");
			}else{
				//---Delete Data in Database
				$this->db->where('con_id',$con_id);
				$this->db->delete('sch_emp_contract');
				$m='';
				$p='';
				if(isset($_GET['m'])){
			   	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }
				redirect("employee/contract?m=$m&p=$p");
				}
				if ($extension=='.pdf'){
				// header("Content-type: application/pdf");
				// header("Content-Length: " . filesize($data));
				// readfile($data);
				unlink("assets/upload/employees/contracts/$con_id$extension");
			}else if ($extension=='.doc'){				
				unlink("assets/upload/employees/contracts/$con_id$extension");
			}else if ($extension=='.docx'){
				unlink("assets/upload/employees/contracts/$con_id$extension");				
			}else if ($extension=='.jpg'){
				unlink("assets/upload/employees/contracts/$con_id$extension");
			}else if ($extension=='.png'){
				unlink("assets/upload/employees/contracts/$con_id$extension");
			}else if ($extension=='.gif'){
				unlink("assets/upload/employees/contracts/$con_id$extension");
			}
			//---Delete Data in Database
			$this->db->where('con_id',$con_id);
			$this->db->delete('sch_emp_contract');
			//$year=$this->session->userdata('year');
			//$path = ('assets/upload/employees/contracts/');
    		//$get_file = $path.$con_id.;
			
			//if(file_exists($get_file)){
			//	unlink('assets/upload/employees/contracts/'.$con_id.'');
			//}

		
			$m='';
			$p='';
			if(isset($_GET['m'])){
		   	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }

			redirect("employee/contract?m=$m&p=$p");
		}

		public function preview($con_id) {
			$extension=$this->db->query("SELECT extension FROM sch_emp_contract WHERE con_id='$con_id'")->row()->extension;
			$this->load->helper('download');

			if ($extension!=''){
				$data = file_get_contents(base_url("assets/upload/employees/contracts/$con_id$extension"));
				$getdata=base_url("assets/upload/employees/contracts/$con_id$extension");
			}else{
				echo "<p style='text-align:center;font-size:18px;color:red;'>No data to display</p>";
			}
			if ($extension=='.pdf'){
				// header("Content-type: application/pdf");
				// header("Content-Length: " . filesize($data));
				// readfile($data);
				
				header("Content-type: application/pdf");
				header("Content-Disposition: inline; filename=$getdata");
				@readfile($getdata);
			}else if ($extension=='.doc'){				
				$name = "$con_id.doc";
				force_download($name, $data);
			}else if ($extension=='.docx'){
				$name = "$con_id.docx";
				force_download($name, $data);				
			}else if ($extension=='.jpg'){
				header('Content-Type:image/jpeg');
				header("Content-Disposition: inline; filename=$getdata");
				@readfile($getdata);
			}else if ($extension=='.png'){
				header('Content-Type:image/png');
				header("Content-Disposition: inline; filename=$getdata");
				@readfile($getdata);
			}else if ($extension=='.gif'){
				header('Content-Type:image/gif');
				header("Content-Disposition: inline; filename=$getdata");
				@readfile($getdata);
			}
		}

		function edit($con_id){
			$data['page_header']="Edit Contract";
			$datas['contract_row']=$this->contract->getcontr_row($con_id);
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/contract_edit', $datas);
			$this->parser->parse('footer', $data);
		}

		function save(){
			date_default_timezone_set("Asia/Phnom_Penh");
			$save_type = $_GET['save'];
			$conid = $this->input->post("conid");
			$contractid = $this->input->post('contractid');
			$empid = $this->input->post('empid');
			$contract_type = $this->input->post('contract_type');
			$contract_attach = $this->input->post('contract_attach');
			$begin_date =$this->input->post('begin_date');
			$end_date = $this->input->post('end_date');

			$year = $this->input->post('con_year');
			$job_type = $this->input->post('job_type');
			$decription = $this->input->post('decription');
			$duration_type = $this->input->post('duration_type');

			$creator = $this->session->userdata('user_name');
			$create_date = date("Y-m-d H:i:s");
			$lbl_date = "";
			$lbl_creator = "";
			$val_date ="";
			$val_creator = "";
			
			if( $save_type !="add" ){

				$lbl_date .= "last_modified_date";
				$lbl_creator .= "last_modified_by";
				
				$val_date .= $create_date;
				$val_creator .= $creator;
			}else{

				$lbl_date .= "created_date";
				$lbl_creator .= "created_by";

				$val_date .= $create_date;
				$val_creator .= $creator;
				
			}

			$data = array(
							'contractid'=>$contractid,
							'empid'=>$empid,
							'contract_type'=>$contract_type,
							'contract_attach'=>$contract_attach,
							'begin_date'=>$this->green->formatSQLDate($begin_date),
							'end_date'=>$this->green->formatSQLDate($end_date),
							'job_type'=>$job_type,
							'year'=>$year,
							'decription'=>$decription,
							'duration_type'=>$duration_type,
							$lbl_creator=>$val_creator,
							$lbl_date=>$val_date
						);

			if($save_type =="add"){
					$transno=$this->green->nextTran(11,"Staff Contract");
					$ex_data=array('type'=>'11',
									'transno'=>$transno
									);
					$data=array_merge($data,$ex_data);
					$this->db->insert('sch_emp_contract', $data);
					$last_id =$this->db->insert_id();
					$this->do_upload($last_id);
			}else{
					$this->db->where('con_id',$conid);
					$this->db->update('sch_emp_contract', $data);
					$this->do_upload($conid);
			}

			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }

			redirect("employee/contract?m=$m&p=$p");

		}
		function validate(){
			$year=$this->input->post('year');
			$empid=$this->input->post('empid');
			$contractid=$this->input->post('contractid');
			$dcontractid = isset($_POST['dconid']) ? $this->input->post('dconid') : "";
			$this->db->select('count(con_id) as count')
					->from('sch_emp_contract')
					->where('contractid',$contractid)
					->where('year',$year);
			if($dcontractid!='')
				$this->db->where_not_in('contractid',$dcontractid);
			$count=$this->db->get()->row()->count;
			echo $count;
		}
		//---------Upload Contract Photo-----
		function do_upload($contract_id)
		{
			$year=$this->session->userdata('year');
			$config['upload_path'] ='./assets/upload/employees/contracts/';
			$config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx';
			$config['max_size']	= '5000';
			$config['file_name']  = "$contract_id";
			$config['overwrite']=true;
			//$config['file_type']='image/jpg';
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('contract_attach'))
			{
				$error = array('error' => $this->upload->display_errors());			
			}
			else
			{				
			 	if($this->upload->do_upload('upload_data')) {
				$data = $this->upload->data();
				echo $data['file_name'].$data['file_ext'];
			}

			//------ get file name and extension when upload ------------
			$data = $this->upload->data();
    		$filename = $data['file_name'];//.$data['file_ext']; 
    		$extension = $data['file_ext'];
    		$this->updatecontractattach($contract_id,$filename,$extension);
   	 	}
	}
		function updatecontractattach($conid,$filename,$extension){
			$data = array(
							'contract_attach' => $filename,
							'extension'		  => $extension
						);
			$this->db->where('con_id',$conid);
			$this->db->update('sch_emp_contract', $data);
		}

		function autoCEmpId(){

			$key=$_GET['term'];
			$this->db->select('*');
			$this->db->from('sch_emp_profile')	;
			$this->db->like('empcode',$key);
			$this->db->or_like('last_name',$key);
			$this->db->or_like('first_name',$key);
			$this->db->or_like('last_name_kh',$key);
			$this->db->or_like('first_name_kh',$key);
			$this->db->or_like('empid',$key);
			$this->db->order_by('empid','desc');			
			$data=$this->db->get()->result();
			//$this->green->getTable($sql_page);
			$array=array();
			foreach ($data as $row) {
				$array[]=array('value'=>"$row->empid",'label'=>"$row->empcode : $row->last_name $row->first_name",'ecode'=>"$row->empcode");
			}
		     echo json_encode($array);
		}
		function search_contr(){

			if(isset($_GET['sort_num'])){
				$sort 			= $_GET['sort'];
				$sort_num 		= $_GET['sort_num'];
				$contr_id 	 	= $_GET['contr_id'];
				$empid 		 	= $_GET['empid'];
				$contract_type  = $_GET['contract_type'];
				$job_type 		= $_GET['job_type'];
				$sort_ad 		=  $this->input->post('sort_ad');
				$yearid			= $_GET['y'];
				$m= $_GET['m'];
				$p= $_GET['p'];

				$data['tdata'] = $query=$this->contract->searchcontr($sort_num, $contr_id, $empid, $contract_type, $job_type,$sort,$sort_ad,$m,$p,$yearid);
				$data['page_header']="Contract List";			
				$this->parser->parse('header', $data);
				$this->parser->parse('employee/contract_list', $data);
				$this->parser->parse('footer', $data);
			}

			if(!isset($_GET['sort_num'])){
				$sort 			= $_POST['sort'];
				$sort_num 		= $_POST['sort_num'];
				$contr_id 		= $_POST['contr_id'];
				$empid 			= $_POST['empid'];
				$contract_type 	= $_POST['contract_type'];
				$job_type 		= $_POST['job_type'];
				$sort_ad 		=  $this->input->post('sort_ad');
				$yearid			= $this->input->post('year');
				$m=$this->input->post('m');
				$p=$this->input->post('p');
				
				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
			        $this->green->setActiveModule($m);
			    }
			    if($p!=''){
			        $this->green->setActivePage($p); 
			    }
				$query=$this->contract->searchcontr($sort_num, $contr_id, $empid, $contract_type, $job_type,$sort,$sort_ad,$m,$p,$yearid);

					$num_contr = 1;
					if(count($query) > 0){

					foreach($query as $row_contr){
						$cre_Bdate  = date_create($row_contr['begin_date']);
						$begin_date = date_format($cre_Bdate, 'd/m/Y');
						$cre_Edate  = date_create($row_contr['end_date']);
						$end_date   = date_format($cre_Edate, 'd/m/Y');

						if($row_contr['begin_date']=='0000-00-00'){
							$begin_date="";
						}
						
						if($row_contr['end_date']=='0000-00-00'){
							$end_date="";
						}

						$arr_contr  = array('VSI'=>'VSI Contract', 'Local'=>'Local Contract');
						$contr_type = $row_contr['contract_type'];

						$arr_job    = array('FT'=>'Full Time', 'PT'=>'Part Time');
						$job_type   = $row_contr['job_type'];

						$empinfo=$this->green->getEmpInf($row_contr['empid']);
						//<td><a target="_blank" href="'.site_url("employee/employee/view_emp/".$contract['empid']."?m=$m&p=$p").'">'.$empinfo['empcode'].'</a></td>
						echo '<tr>
									<td>'.($num_contr++).'</td>
									<td>'.$row_contr['contractid'].'</td>
									<td><a target="_blank" href="'.site_url("employee/employee/view_emp/".$row_contr['empid']."?m=$m&p=$p").'">'.$empinfo['empcode'].'</a></td>
									<td>'.$arr_contr[$contr_type].'</td>
									<td>'.$begin_date.'</td>
									<td>'.$end_date.'</td>									
									<td>'.$arr_job[$job_type].'</td>
									<td>'.$row_contr['decription'].'</td>
									
									<td width="1%" class="remove_tag">';
									if($this->green->gAction("P")){
										echo '<a title="Preview Contract" id="clk_pri" class="clk_pri" >
												<img rel="'.$row_contr['con_id'].'" src="'.site_url('../assets/images/icons/preview.png').'" onclick="preview_contract(event);" style="width:20px;height:20px;">
											  </a>';
									}
									echo '</td>
									<td width="1%" class="remove_tag">';
									if($this->green->gAction("D")){
										echo '<a title="Delete Contract" id="clk_del" class="clk_del" >
												<img rel="'.$row_contr['con_id'].'" src="'.site_url('../assets/images/icons/delete.png').'" onclick="delete_contract(event);" style="width:20px;height:20px;">
											  </a>';
									}
									echo '</td>
									<td width="1%" class="remove_tag">';
									if($this->green->gAction("U")){	
										echo '<a title="Edit Contract">
												<img rel="'.$row_contr['con_id'].'" src="'.site_url('../assets/images/icons/edit.png').'" onclick="edit_contract(event);" style="width:20px;height:20px;">
										 	  </a>';
									}
									echo '</td>
							   </tr>';
					}

					}else{
						echo "<tr style='text-align:center;background:#FFFFFF;border-bottom:solid 2px #EEEEEE;'><td colspan='10' id='no_data' rel='0'><b>We didn't find anything to show here.</b></td></tr>";
					}
					echo "<tr class='remove_tag'>
							<td colspan='12' id='pgt'>
								<div style='margin-top:20px; width:10%; float:left;'>
								Display : <select id='sort_num'  onchange='search();' style='padding:5px; margin-right:0px;'>";
												
												$num=50;
												for($i=0;$i<10;$i++){?>
													<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
													<?php $num+=50;
												}
												
											echo "</select>
								</div>
								<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
									<ul class='pagination' style='text-align:center'>
									 ".$this->pagination->create_links()."
									</ul>
								</div>
							</td>
						</tr> ";
			}
				
		}

	}
?>