<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class C_overal_comment extends CI_Controller{

		function __construct(){
			parent::__construct();				
			$this->load->model('employee/m_overal_comment','mg');		
		}
		function index(){
			//$this->load->view('header');
			$data['title']='Overall Comment';
			$this->parser->parse('header', $data);
			$this->load->view('employee/v_overal_comment');
			$this->parser->parse('footer', $data);
		}
		function save(){
			$this->mg->save();
		}
		function deletedata(){
			$this->mg->deletedata();
		}
		function update(){
			$this->mg->update();
		}
		//  showdata  ==========================================================
		function showdata(){
			$tr ='';
			$i  = 0;
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
			foreach ($this->mg->showdata()->result() as $row){
			    $edit   ='<a href="javascript:void(0)" edit="'.$row->ove_com_id.'" class="edit_data"><image  width="15" height="15" src='.base_url().'/assets/images/icons/edit.png></a>';
			   	$delete = "<a href='javascript:void(0)' delete='".$row->ove_com_id."' class='delete_data'><image  src=".base_url()."/assets/images/icons/delete.png></a>";
			    $i++;
				$tr .='<tr>
						<td>'.$i.'</td>
						<td>'.$row->description_overall.'</td>
						<td>'.$row->description_overall_kh.'</td>
						<td>';
							if($this->green->gAction("U")){
								$tr.=$edit;
							}
						$tr .='</td>';
						$tr.='<td>';
							if($this->green->gAction("D")){
								$tr.=$delete;
							}
						$tr .='</td>
				    </tr>';
			} 
			echo $tr;
			die();
		}
		// editdata  ================================================================
		function editdata(){
			$idhidden = $this->input->post('ove_com_id');
		 	$result   = $this->mg->editdata($idhidden);		
			echo $result->ove_com_id.'|'.$result->description_overall.'|'.$result->description_overall_kh.'|'.$result->isfillout;
			die();
		}
		
	}//class
?>