<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class C_emp_search_evaluation extends CI_Controller{

		function __construct(){
			parent::__construct();	
			$this->load->library('pagination');				
			$this->load->model('employee/m_emp_search_evaluation','mg');	
		}
		function deletedata(){
			$this->mg->deletedata();
		}
		function index(){
			$data['title']='Staff Evaluation Report';
			$this->parser->parse('header', $data);
			$this->load->view('employee/v_emp_search_evaluation');
			$this->parser->parse('footer', $data);
		}
		// edit  ====================================================
		function editdata($typeno = 0){		
			$row = $this->db->query("SELECT
											sch_eva_emp_evalutiont.evalid,
											sch_eva_emp_evalutiont.type,
											sch_eva_emp_evalutiont.transno,
											sch_eva_emp_evalutiont.eval_date,
											sch_eva_emp_evalutiont.empid,
											sch_eva_emp_evalutiont.review_from_date,
											sch_eva_emp_evalutiont.review_to_date,
											sch_eva_emp_evalutiont.eval_by_empid,
											sch_eva_emp_evalutiont.sup_comment,
											sch_eva_emp_evalutiont.sup_emp_id,
											sch_eva_emp_evalutiont.sup_comment_date,
											sch_eva_emp_evalutiont.emp_comment,
											sch_eva_emp_evalutiont.emp_comment_date,
											sch_eva_emp_evalutiont.gm_comment,
											sch_eva_emp_evalutiont.gm_emp_id,
											sch_eva_emp_evalutiont.gm_date,
											sch_eva_emp_evalutiont.ove_com_id,
											sch_eva_emp_evalutiont.ove_comment,
											sch_eva_emp_evalutiont.emp_id
										FROM
											sch_eva_emp_evalutiont 
										WHERE evalid  = '{$typeno}' ")->row();
			$da['row'] = $row;
			$this->parser->parse('header', $data);
			$this->load->view('employee/c_emp_evaluation/v_emp_evaluation', $da);
			$this->parser->parse('footer', $data);
			die();
		}
		// showdata ==================================================
		public function setShowdata(){
				$y = $this->input->post('y');
				$sort_num = 5;
				$p  = $this->input->post('p');
				$m  = $this->input->post('m');
				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
		        	$this->green->setActiveModule($m);
		    	}
		    	if($p!=''){
		        	$this->green->setActivePage($p); 
		    	}
				$fromdate = $this->input->post('fromdate');
				$todate = $this->input->post('todate');
				$search_name = $this->input->post('search_name');
				$total_display = $this->input->post('total_display');
				$search = "";
				if($search_name != ""){
	    			$search .= "AND (e.last_name LIKE '%$search_name%' 
						  OR e.first_name LIKE '%$search_name%')";
	    		} 
	    		if($fromdate != ''){
					 $search .= "AND v.eval_date >= '{$fromdate}' ";    
				}
				if($todate != ''){
					 $search .= "AND v.eval_date <= '{$todate}' ";    
				}
				$sql= "SELECT
							v.evalid,
							v.transno,
							e.sex,
							p.position,
							d.department,
							DATE_FORMAT(v.eval_date,'%d/%m/%Y') AS eval_date,
							CONCAT(e.last_name,' ',e.first_name) AS fullname
						FROM 
							sch_emp_profile AS e
						INNER JOIN sch_emp_position AS p ON e.pos_id = p.posid
						INNER JOIN sch_emp_department AS d ON e.dep_id = d.dep_id
						INNER JOIN sch_eva_emp_evalutiont AS v ON v.empid = e.empid 
						WHERE  1=1 {$search}";
				$total_row = $this->green->getValue("select count(*) as numrow FROM ($sql) as cc ");
				$paging    = $this->green->ajax_pagination($total_row,site_url()."/employee/c_emp_search_evaluation?y=$y&s_num=$sort_num&m=$m&p=$p",$total_display);
				//return "$sql limit {$paging['start']}, {$paging['limit']}";
				$data      = $this->db->query("$sql limit {$paging['start']}, {$paging['limit']}")->result();
				$i=0;
				$tr = "";
				if(count($data) > 0){
					$print = "";
					$edit   ='';
					$delete = "";
					foreach ($data as $row){
					    $i++;
					    $print  ='<a title="print" href='.site_url("employee/c_report_emp_evaluation/index/?transno=".$row->transno."").' class="edit_data"><image  width="25" height="25" src='.base_url().'/assets/images/icons/print.png></a>';
					    $edit   ='<a title="edit" href='.site_url("employee/c_emp_evaluation/edit/?transno=".$row->transno."").' class="edit_data"><image  width="15" height="15" src='.base_url().'/assets/images/icons/edit.png></a>';
						$delete ='<a title="delete" href="#" delete="'.$row->evalid.'" class="delete_data"><image  src='.base_url().'/assets/images/icons/delete.png></a>';
						$tr .='
								<tr>
									<td width="5%">'.$i.'</td>
									<td width="25%">'.$row->fullname.'</td>
									<td width="7%">'.$row->sex.'</td>
									<td>'.$row->position.'</td>
									<td>'.$row->department.'</td>
									<td>'.$row->eval_date.'</td>
									<td>';
										if($this->green->gAction("P")){
											$tr.=$print;
										}
								$tr.='</td>';
								$tr.='<td>';
										if($this->green->gAction("U")){
											$tr.=$edit;
										}
								$tr .='</td>';
								$tr.='<td>';
										if($this->green->gAction("D")){
											$tr.=$delete;
										}
								$tr .='</td>
					    		</tr>';
						   
					} 
				}
				if($i == 0){ 
					$tr.= '<tr>
								<td colspan="9" style="text-align:center; color:#FF0000"> 
									Your Search not have in system. please search try agan.... 
								</td>
							</tr>';
				}
				$arrHtml = array();
				header('Content-type:text/x-json');
				$arrHtml['body']=$tr;
				$arrHtml['pagination']=$paging;
				echo json_encode($arrHtml);
				die();
		}

	}
?>