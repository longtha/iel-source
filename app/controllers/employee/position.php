<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Position extends CI_Controller{

		function __construct(){
			parent::__construct();	
			$this->load->model('employee/modeposition','position');
			$this->load->library('pagination');		
		}

		function Index(){
			$m='';
			$p='';
			if(isset($_GET['m'])){
			    $m=$_GET['m'];
			}
			if(isset($_GET['p'])){
			    $p=$_GET['p'];
			}

			$page=0;
			if(isset($_GET['per_page'])) $page=$_GET['per_page'];
			$config['base_url'] = site_url()."/employee/position?pg=1&m=$m&p=$p";		
			$config['total_rows'] = $this->green->getTotalRow("SELECT * FROM sch_emp_position WHERE is_active=1");
			$config['per_page'] =50;
			//$config['use_page_numbers'] = TRUE;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';

			$this->pagination->initialize($config);	
			$limi=" limit ".$config['per_page'];
			if($page>0){
				$limi=" limit ".$page.",".$config['per_page'];
			}	

			$sql_page = "SELECT 
							sch_emp_position.posid,
							sch_emp_position.position,
							sch_emp_position.position_kh,
							sch_emp_position.description 
						FROM sch_emp_position 
						WHERE is_active=1 
						ORDER BY posid DESC {$limi} ";
						
			$data['tdata']=$this->green->getTable($sql_page);
			$data['page_header']="Contract List";
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/position_list', $data);
			$this->parser->parse('footer', $data);
		}

		function add(){
			$data['page_header']="New Postion";			
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/position_form', $data);
			$this->parser->parse('footer', $data);
		}
		function delete($posid){
			$this->db->set('is_active',0);
			$this->db->where('posid', $posid);
			$this->db->update('sch_emp_position');

			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }

			redirect("employee/position?m=$m&p=$p");
		}

		function edit($posid){
			$data['page_header']="Edit Postion";	
			$datas['pos_row']=$this->position->getpos_row($posid);		
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/position_edit', $datas);
			$this->parser->parse('footer', $data);
		}
		
		// function view_pos($posid){

		// 	$data['page_header']="New Position";		
		// 	$data1['vpos']=$this->position->view_pos($posid);
		// 	$this->parser->parse('header', $data);
		// 	$this->load->view('employee/position_view', $data1);
		// 	$this->parser->parse('footer', $data);
		// }

		function save(){
			date_default_timezone_set("Asia/Bangkok");
			$save_type = $_GET['save'];
			$posid = $this->input->post('posid');
			$position = $this->input->post('position');
			$position_kh = $this->input->post('position_kh');
			$match = $this->input->post('match');
			$description = $this->input->post('description');
			
			$creator = $this->session->userdata('user_name');
			$create_date = date("Y-m-d H:i:s");

			if( $save_type == 'add'){
				
				$lbl_date 		=	"created_date";
				$lbl_creator 	= 	"created_by";
				$val_date 		= 	$create_date;
				$val_creator 	= 	$creator;

			}else{

				$lbl_date ="last_modified_date";
				$lbl_creator = "last_modified_by";
				$val_date = $create_date;
				$val_creator = $creator;
			}

			$data = array(
							'position'		=>	$position,
							'position_kh'	=>	$position_kh,
							'match_con_posid' =>  $match,
							'description'	=>	$description,
							$lbl_date		=>	$val_date,
							$lbl_creator	=>	$val_creator
						);
			if( $save_type =='add'){
				$this->db->insert('sch_emp_position', $data);
			}else{
				$this->db->where('posid', $posid);
				$this->db->update('sch_emp_position', $data);
			}
			// $emp_id =$this->db->insert_id();
			// $this->do_upload($emp_id);

			$m='';
			$p='';
			if(isset($_GET['m'])){
	        	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }	

			redirect("employee/position?m=$m&p=$p");
		}
		function validate(){
			$posid=$this->input->post('posid');
			$position=$this->input->post('position');
			$this->db->select('count(*) as count')
					->from('sch_emp_position')
					->where('position',$position);

			if($posid!='')
				$this->db->where_not_in('posid',$posid);
			$count=$this->db->get()->row()->count;
			echo $count;
		}
		function search_pos(){
			if(isset($_GET['sort_num'])){
				
				$sort 			=  $_GET['sort'];
				$sort_num 	    =  $_GET['sort_num'];
				$position 	    =  $_GET['position'];
				$position_kh    =  $_GET['position_kh'];
				$description    =  $_GET['description'];
				$sort_ad 		=  $this->input->post('sort_ad');
				$m=$_GET['m'];
				$p=$_GET['p'];
				
				$data['tdata']  =  $this->position->searchpos($sort_num, $position, $position_kh, $description,$sort,$sort_ad,$m,$p);

				$data['page_header']="Position List";			
				$this->parser->parse('header', $data);
				$this->parser->parse('employee/position_list', $data);
				$this->parser->parse('footer', $data);
			}
			if(!isset($_GET['sort_num'])){

				$sort 			=  $_POST['sort'];
				$sort_num 		=  $this->input->post('sort_num');
				$position 		=  $_POST['position'];
				$position_kh 	=  $_POST['position_kh'];
				$description 	=  $_POST['description'];
				$sort_ad 		=  $this->input->post('sort_ad');
				$m=$this->input->post('m');
				$p=$this->input->post('p');
				
				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
			        $this->green->setActiveModule($m);
			    }
			    if($p!=''){
			        $this->green->setActivePage($p); 
			    }

				$query=$this->position->searchpos($sort_num, $position, $position_kh, $description,$sort,$sort_ad,$m,$p);
				$num_pos = 1;
				if( count($query) > 0){
					foreach( $query as $rows_pos ){
						echo '<tr class="no_data" rel="1">
									<td class="pos_1">'.($num_pos++).'</td>
									<td class="pos_2"><a target="_blank" href="'.site_url("employee/employee/?m=$m&p=$p&po_id=".$rows_pos['posid']).'">'.$rows_pos['position'].'</a></td>
									<td class="pos_3">'.$rows_pos['position_kh'].'</td>
									<td class="pos_4">'.$rows_pos['description'].'</td>
									<td width="1%" class="remove_tag">';
									if($this->green->gAction("D")){
										echo '<a title="Delete Contract">
												<img class="clk_del" rel="'.$rows_pos['posid'].'" onclick="delete_pos(event);"  src="'.site_url('../assets/images/icons/delete.png').'" style="width:20px;height:20px;">
										  	  </a>';
									}
									echo '</td>
									<td width="1%" class="remove_tag">';
									if($this->green->gAction("U")){
										echo '<a title="Edit Contract">
												<img rel="'.$rows_pos['posid'].'" src="'.site_url('../assets/images/icons/edit.png').'" onclick="edit_pos(event);" style="width:20px;height:20px;">
											  </a>';
									}
									echo '</td>
							  </tr>';
					}
				}else{
					echo "<tr class='no_data' rel='0'><td colspan='8' style='text-align:center;'><b>We didn't find anything to show here</b></td></tr>";
				}
				echo "<tr class='remove_tag'>
							<td colspan='12' id='pgt'>
								<div style='margin-top:20px; width:10%; float:left;'>
								Display : <select id='sort_num'  onchange='search();' style='padding:5px; margin-right:0px;'>";
												
												$num=50;
												for($i=0;$i<10;$i++){?>
													<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
													<?php $num+=50;
												}
												
											echo "</select>
								</div>
								<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
									<ul class='pagination' style='text-align:center'>
									 ".$this->pagination->create_links()."
									</ul>
								</div>
							</td>
						</tr> ";
			}
		}

	}
?>