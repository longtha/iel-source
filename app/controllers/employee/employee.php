<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Employee extends CI_Controller{

		function __construct(){
			parent::__construct();	
			$this->load->model('employee/modemployee','emp');
			$this->load->model('setting/usermodel','user');
			$this->load->model('setting/rolemodel','role');
			$this->load->library('pagination');
		}
		
		//----------- Load page Employee List
		function Index()
		{
			$m='';
			$p='';
			if(isset($_GET['m'])){
			    $m=$_GET['m'];
			}
			if(isset($_GET['p'])){
			    $p=$_GET['p'];
			}			

			$where = "";	
			if(isset($_GET['po_id'])){
				$where = "AND sch_emp_profile.pos_id=".$_GET['po_id'];
			}
			$data['title'] = "Employee List";
			$page=0;
			if(isset($_GET['per_page'])) $page=$_GET['per_page'];
			$config['base_url'] = site_url()."/employee/employee?pg=1&m=$m&p=$p";		
			$config['total_rows'] = $this->green->getTotalRow("SELECT * FROM sch_emp_profile where sch_emp_profile.is_active=1 {$where}");
			$config['per_page'] =50;
			//$config['use_page_numbers'] = TRUE;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';

			$this->pagination->initialize($config);	
			$limi=" limit ".$config['per_page'];
			if($page>0){
				$limi=" limit ".$page.",".$config['per_page'];
			}	
			
			$sql_page="SELECT DISTINCT
							sch_emp_profile.dep_id,
							sch_emp_profile.empid,
							sch_emp_profile.empcode,
							sch_emp_profile.first_name,
							sch_emp_profile.last_name,
							sch_emp_profile.first_name_kh,
							sch_emp_profile.last_name_kh,
							sch_emp_profile.dob,
							sch_emp_profile.employed_date,
							sch_emp_profile.resigned_date,
							sch_emp_profile.is_active,
							sch_emp_position.position
						FROM
							sch_emp_profile
						LEFT JOIN sch_emp_position ON sch_emp_position.posid = sch_emp_profile.pos_id
						WHERE sch_emp_profile.is_active=1 {$where}
						GROUP BY sch_emp_profile.empcode
						ORDER BY sch_emp_profile.empid DESC {$limi}
						";

			$data['tdata']=$this->green->getTable($sql_page);
			$data['page_header']="Employee List";
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/employee_list', $data);
		}
		function import($result=0){
			$data['page_header']="Import Employee Profile";		
			$data['import_status']=($result==1?"Employee profile was imported.":"No employee profile to import.");			
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/emp_import_profile', $data);
			$this->parser->parse('footer', $data);
		}
		function importProfile(){
			$result=$this->emp->saveImport();
			$this->import($result);					
		}
		//------click on button add on page employee list
		function add(){
			$data['page_header']="New Employee";	

			#========== get last contract ==============
			$data['cont_status']=

			$this->parser->parse('header', $data);
			$this->parser->parse('employee/employee_form', $data);
			$this->parser->parse('footer', $data);
		}
		//------------Edit employee--------------
		function edit(){
			$data['page_header']="New Employee";			
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/employee_edit', $data);
			$this->parser->parse('footer', $data);
		}
		//------------delet employee--------------
		function delete($empid){
			$this->db->set('is_active',0);
			$this->db->where('empid',$empid);
			$this->db->update('sch_emp_profile');
			//$login_username=$this->emp->getemprow($empid)->login_username;
			//$this->db->set('is_active',0);
			//$this->db->where('user_name',$login_username);
			//$this->db->update('sch_user');

			$this->db->update("sch_user", array('is_active' => '0'), array('emp_id' => $empid));

			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }

			redirect("employee/employee?m=$m&p=$p");
		}
		
		//---------- save data in page employee form--------
		function save(){

			$empid = $this->input->post("empid");
			$empcode = $this->input->post("empcode");
			$first_name = $this->input->post("first_name");
			$last_name = $this->input->post("last_name");
			
			$first_name_kh = $this->input->post("first_name_kh");
			$last_name_kh = $this->input->post("last_name_kh");

			$sex = $this->input->post("sex");
			$dob = $this->input->post("dob");
			$pob = $this->input->post("pob");

			$annual_leave = $this->input->post("annual_leave");
			$sick_leave = $this->input->post("sick_leave");
			$endoftheyear_hol = $this->input->post("endoftheyear_hol");
			$kh_newyear_hol = $this->input->post("kh_newyear_hol");

			$perm_adr = $this->input->post("perm_adr");
			$village = $this->input->post("village");
			$commune = $this->input->post("commune");
			$district = $this->input->post("district");
			$province = $this->input->post("province");
			$zoon = $this->input->post("zoon");

			$marital_status = $this->input->post("marital_status");
			$nationality = $this->input->post("nationality");
			$emp_type = $this->input->post("emp_type");
			$pos_id = $this->input->post("pos_id");
			$dep_id = $this->input->post("dep_id");
			$phone = $this->input->post("phone");
			$email = $this->input->post("email");
			$leave_school = $this->input->post("leave_school");
			$leave_school_reason = $this->input->post("leave_school_reason");
			$idcard = $this->input->post("idcard");
			$is_foreigner = $this->input->post("is_foreigner");
			$employed_date = $this->input->post('employed_date');

			$resigned_date = $this->input->post('resigned_date');
			$cont_status = $this->input->post('cont_status');
			
			$leave_type = $this->input->post('leave_type');
			$leave_reason= $this->input->post('leave_reason');
			$leave_comment= $this->input->post('leave_come');
			$con_id= $this->input->post('con_id');


			$note = $this->input->post("note");
			$create_date = date("Y-m-d H:i:s");
			$creator = $this->session->userdata('user_name');
			
			$save_type ="";
			$lbl_date = ""; $value_date = "";
			$lbl_creator = ""; $value_creator ="";
			if(isset($_GET['save'])){
				$save_type .= $_GET['save'];
				if( $save_type != "edit" ){
					$lbl_date .= 'created_date';
					$lbl_creator .= 'created_by';
					$value_date .= $create_date;
					$value_creator .= $creator;
				}else{
					$lbl_date .= 'last_modified_date';
					$lbl_creator .= 'last_modified_by';
					$value_date .= $create_date;
					$value_creator .= $creator;
				}
			};
			$data = array(
							'empcode'=>$empcode,
							'first_name'=>$first_name,
							'last_name'=>$last_name,
							'first_name_kh'=>$first_name_kh,
							'last_name_kh'=>$last_name_kh,
							'sex'=>$sex,
							'dob'=>$this->green->formatSQLDate($dob),
							'pob'=>$pob,
							'perm_adr'=>$this->db->escape_str($perm_adr),
							'village'=>$village,
							'commune'=>$commune,
							'district'=>$district,
							'province'=>$province,
							'zoon'=>$zoon,
							'marital_status'=>$marital_status,
							'nationality'=>$nationality,
							'emp_type'=>$emp_type,
							'pos_id'=>$pos_id,
							'dep_id'=>$dep_id,
							'phone'=>$phone,
							'email'=>$email,
							'leave_school'=>$leave_school,
							'leave_school_reason'=>$leave_school_reason,
							'idcard'=>$idcard,
							'is_foreigner'=>$is_foreigner,
							'employed_date'=>$this->green->formatSQLDate($employed_date),
							'resigned_date'=>$this->green->formatSQLDate($resigned_date),
							'note'=>$this->db->escape_str($note),
							'annual_leave'=>$annual_leave,
							'sick_leave'=>$sick_leave,
							'kh_newyear_hol'=>$kh_newyear_hol,
							'endoftheyear_hol'=>$endoftheyear_hol,
							'leave_type'=>$leave_type,
							'leave_reason'=>$leave_reason,
							'leave_comment'=>$leave_comment,
							'con_id'=>$con_id,
							'is_active'=>$cont_status,
							$lbl_date=>$value_date,
							$lbl_creator=>$value_creator
						);

			// users =======
			$userid = $this->input->post("userid");
			$txtu_name = $this->input->post("txtu_name");			
			$txtpwd = $this->input->post("txtpwd");	
			$cborole = $this->input->post("cborole");
			$schlevel = $this->input->post("cboschlevel");
			$dashboard = $this->input->post("dashboard");
			$defpage = $this->input->post("defpage");
			$year = date('Y');
			$modified_date = date("Y-m-d H:i:s");
			$midified_by = $this->session->userdata('user_name');
			if($cborole == 1)
				$admin = 1;
			else
				$admin = 0;
			$match_con_posid_row = $this->db->query("SELECT DISTINCT
															gp.match_con_posid,
															gp.description
														FROM
															sch_z_postion_group AS gp
														LEFT JOIN sch_emp_position AS p ON p.match_con_posid = gp.match_con_posid
														WHERE
															p.posid = '{$pos_id}' ")->row();


			if(isset($_GET['save'])){
				if( $save_type == "edit" ){
					$this->db->where("empid", $empid);
					$this->db->update('sch_emp_profile',$data);
					$this->do_upload($empid);
					/*---- update contract -------- */
					$cont_id=$this->input->post('con_id');
					if(isset($cont_id) && $this->input->post('con_id')!=""){
						$data_contract=array('status' =>$cont_status);
						$this->db->where("con_id", $this->input->post('con_id'));
						$this->db->where("empid", $this->input->post('empid'));
						$this->db->update('sch_emp_contract',$data_contract);
					}

					// users =======	
					if($txtu_name != '' && $txtpwd != ''){				
						$data1 = array('first_name' => $first_name,
										'last_name' => $last_name,
										'user_name' => $txtu_name,
										'password' => md5($txtpwd),
										'email' => $email,							
										'roleid' => $cborole,
										'schoolid' => '1',
										'def_dashboard' => $dashboard,
										'created_date' => $create_date,
										'created_by' => $creator,
										'modified_date' => $modified_date,
										'modified_by' => $midified_by,							
										'year' => $year,
										'def_open_page' => $defpage,
										'is_admin' => $admin,
										'is_active' => '1',
										'match_con_posid' => $match_con_posid_row->match_con_posid,
										'emp_id' => $empid
									);
						if($userid - 0 > 0){
							$u_row = $this->emp->getuserrow($empid);
							if($u_row['password'] != $txtpwd){
								
							}else{
								unset($data1['password']);
							}

							$this->db->update("sch_user", $data1, array("userid" => $userid));

							// school level =====
							$this->db->delete("sch_user_schlevel", array("userid" => $userid));
							if($schlevel){
								foreach ($schlevel as $schlevelid) {
									$this->saveteacherschl($schlevelid, $userid);
								}
							}
							
							$userid_ = $userid;
						}
						else{
							unset($data1['modified_date']);
							unset($data1['modified_by']);
							$this->db->insert("sch_user", $data1);
							$userid_ = $this->db->insert_id();
						}

						// upload =======
						$this->do_upload($empid);
						$this->do_upload_user($userid_);
					}


				}else{		
					// employees =======
					$this->db->insert('sch_emp_profile', $data);
					$empid_last = $this->db->insert_id();

					// users =======				
					if(trim($txtu_name) != '' && trim($txtpwd) != ''){
						$data1 = array(
										'first_name' => $first_name,
										'last_name' => $last_name,
										'user_name' => $txtu_name,
										'password' => md5($txtpwd),
										'email' => $email,							
										'roleid' => $cborole,
										'schoolid' => '1',
										'def_dashboard' => $dashboard,
										'created_date' => $create_date,
										'created_by' => $creator,						
										'year' => $year,
										'def_open_page' => $defpage,
										'is_admin' => $admin,
										'is_active' => '1',
										'emp_id' => $empid_last,
										'match_con_posid' => $match_con_posid_row->match_con_posid
									);
						$this->db->insert("sch_user", $data1);
						$userid_last = $this->db->insert_id();

						// school level ====
						$this->db->delete("sch_user_schlevel", array("userid" => $userid_last));
						if($schlevel){
							foreach ($schlevel as $schlevelid) {
								$this->saveteacherschl($schlevelid, $userid_last);
							}
						}
						// upload =======
						$this->do_upload($empid_last);
						$this->do_upload_user($userid_last);
					}

				}
			}

			

			$m='';
			$p='';
			if(isset($_GET['m'])){
	        	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }	

			redirect("employee/employee?m=$m&p=$p");
			
		}

		// school level =======
		function saveteacherschl($schlevelid, $userid){
			$data = array('userid' => $userid, 'schlevelid' => $schlevelid);
			$this->db->insert('sch_user_schlevel', $data);
		}

		// upload users =======
		function do_upload_user($user_id){
			$config['upload_path'] = './assets/upload/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name']  = "$user_id.png";
			$config['overwrite'] = true;
			$config['file_type'] = 'image/png';
			$this->load->initialize($config);
			$this->load->library('upload', $config);


			if ( ! $this->upload->do_upload('userfile')){
				$error = array('error' => $this->upload->display_errors());			
			}
			else{							
				$config2['image_library'] = 'gd2';
                $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config2['new_image'] = './assets/upload/';
                $config2['maintain_ratio'] = TRUE;
                $config2['create_thumb'] = TRUE;
                $config2['thumb_marker'] = '_thumb';
                $config2['width'] = 120;
                $config2['height'] = 180;
                $config2['overwrite'] = true;
                $this->load->initialize($config2);
                $this->load->library('image_lib',$config2); 

                if ( !$this->image_lib->resize()){
                	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
				}
				
			}

		}

		//  --------- Upload Employee Photo -----
		function do_upload($employee_id)
		{
			$year=$this->session->userdata('year');
			if(!file_exists('./assets/upload/employees/photos/'))
			 {
			    if(mkdir('./assets/upload/employees/photos/',0755,true))
			    {
			                return true;
			    }
			 }
			
			$config['upload_path'] = './assets/upload/employees/photos/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '5000';
			$config['file_name']  = "$employee_id.jpg";		
			$config['overwrite'] = true;
			$config['file_type'] = 'image/jpg';
			$this->load->initialize($config); 
			$this->load->library('upload', $config);


			if ( ! $this->upload->do_upload()){
				$error = array('error' => $this->upload->display_errors());			
			}
			else{				
			 	$config2['image_library'] = 'gd2';
                $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config2['new_image'] = './assets/upload/employees/photos/';
                $config2['maintain_ratio'] = TRUE;
                $config2['create_thumb'] = TRUE;
                $config2['thumb_marker'] = FALSE;
                $config2['width'] = 120;
                $config2['height'] = 180;
                $config2['overwrite'] = true;
                $this->load->initialize($config2); 
                $this->load->library('image_lib',$config2); 

                if ( !$this->image_lib->resize()){
                	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
				}else{
					//unlink('./assets/upload/employees/'.$year.'/'.$employee_id.'.png');
				}
			}
		}
		//------------View Employee-----
		function view_emp($employeeid){
			$data['page_header']="New Employee";		
			$data1['employee']=$this->emp->view_employee($employeeid);
			$this->parser->parse('header', $data);
			$this->load->view('employee/employee_view', $data1);
			$this->parser->parse('footer', $data);
		}
		function getPosition(){			
			$selected=$this->input->post('posid');
			$query = $this->db->get('sch_emp_position')->result();			
			$option ="";
			foreach($query as $posid ){
				$option .='<option value="'.$posid->posid.'" '.($selected==$posid->posid?"selected":"").'>'.$posid->position.'</option>';
			}
			echo $option;
		}

		function get_emp($employeeid){
			$data['page_header']="Employee";				
			$data1['employee']= $this->emp->view_employee($employeeid);
			$data1['query']= $this->emp->getuserrow($employeeid);
			$this->parser->parse('header', $data);
			$this->load->view('employee/employee_edit', $data1);
			$this->parser->parse('footer', $data);
		}
		function getexp(){
			$data='';
			$sch_empId 		 = $this->input->post('sch_empId');
			$sch_fullname 	 = $this->input->post('sch_fullname');
			$sch_fullname_kh = $this->input->post('sch_fullname_kh');
			$s_status 		 = $this->input->post('s_status');
			$pos_id 		 = $this->input->post('pos_id');
			$sort 			 = $this->input->post('sort');
			$sort_num 		 = $this->input->post('sort_num');
			$page 			 = $this->input->post('page');
			$is_all 		 = $this->input->post('is_all');
			$f 				 = $this->input->post('f');
			$data.='<thead>';
			foreach ($f as $key => $value) {
				$data.="<th class='no_wrap'>$value</th>";
			}
			$data.='</thead>';
			$query=$this->emp->getexp(
										$sch_empId,
										$sch_fullname,
										$sch_fullname_kh,
										$s_status,
										$pos_id,
										$sort,
										$sort_num,
										$page,
										$is_all
									);
			$i=1;
			$data.='<tbody>';
			foreach($query as $row){									
				$data.="<tr>";
					if(isset($f['No']))
					 	$data.="<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>";
					foreach ($f as $key => $value) {
						if($key!='No')
							$data.="<td>".$row[$key]."</td>";
					}							 
					$i++;
				$data.="</tr>";	 
			}
			$data.='</tbody>';
			$arr=array('data'=>$data);
			header("Content-type:text/x-json");
			echo json_encode($arr);
		}
		function search_emp(){
			if(isset($_GET['emp_id'])){

				$sort 			 = $_GET['sort'];
				$sort_num 		 = $_GET['s_num'];
				$sch_empId 		 = $_GET['emp_id'];
				$sch_fullname 	 = $_GET['fn'];
				$pos_id 		 = $_GET['pos_id'];
				$sch_fullname_kh = $_GET['fnk'];
				$sort_ad 		 = $this->input->post('sort_ad');
				$m=$_GET['m'];
				$p=$_GET['p'];
				$st=$_GET['st'];
				$data1['tdata']  = $query=$this->emp->searchemployee($sch_empId, $sch_fullname, $sch_fullname_kh, $sort_num,$sort,$sort_ad,$pos_id,$m,$p,$st);
				
				$data['page_header']="Employee List";			
				$this->parser->parse('header', $data);
				$this->parser->parse('employee/employee_list', $data1);
				$this->parser->parse('footer', $data);
			}
			if(!isset($_GET['emp_id'])){
				$sort 			 = $_POST['sort'];
				$sort_num 		 = $_POST['sort_num'];
				$sch_empId 		 = $_POST['sch_empId'];
				$pos_id 		 = $_POST['pos_id'];
				$sch_fullname 	 = $_POST['sch_fullname'];
				$sch_fullname_kh = $_POST['sch_fullname_kh'];
				$sort_ad 		 = $this->input->post('sort_ad');
				$m=$this->input->post('m');
				$p=$this->input->post('p');
				$st = $_POST['s_status'];
				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
			        $this->green->setActiveModule($m);
			    }
			    if($p!=''){
			        $this->green->setActivePage($p); 
			    }

				$query=$this->emp->searchemployee($sch_empId, $sch_fullname, $sch_fullname_kh, $sort_num,$sort,$sort_ad,$pos_id,$m,$p,$st);
				$i = 1;
				if(count($query) > 0){
				foreach($query as $row_emp){

					if($row_emp['dob'] =="0000-00-00"){
						$date = "";
					}else{
						$cdate = date_create($row_emp['dob']);
						$date = date_format($cdate,"d/m/Y");
					}
					
					echo '<tr class="no_data" rel="1">
								<td class="pos_1">'.str_pad($i,2,"0",STR_PAD_LEFT).'</td>
								<td class="pos_2"><a target="_blank" href="'.site_url("employee/contract/?m=$m&p=$p&emp_id=".$row_emp['empid']).'">'.$row_emp['empcode'].'</a></td>
								<td class="pos_3">'.($row_emp['last_name']." ".$row_emp['first_name']).'</td>
								<td class="pos_4">'.($row_emp['last_name_kh']." ".$row_emp['first_name_kh']).'</td>
					       		<td class="pos_5">'.$date.'</td>
					       		<td class="pos_6"><a target="_blank" href="'.site_url("employee/position/?m=$m&p=$p").'">'.$row_emp['position'].'</a></td>
					       		<td width="1%" class="remove_tag">';
					       		if($this->green->gAction("P")){
									echo '<a title="View Employee">
											<img rel="'.$row_emp['empid'].'" src="'.site_url('../assets/images/icons/preview.png').'" onclick="view_employee(event);">
									 	  </a>';
								}
								echo '</td>
								<td width="1%" class="remove_tag">';
								if($this->green->gAction("D")){
									echo '<a title="Delete Employee">
											<img rel="'.$row_emp['empid'].'" src="'.site_url('../assets/images/icons/delete.png').'" onclick="delete_employee(event);">
										  </a>';
								}
								echo '</td>
								<td width="1%" class="remove_tag">';
								if($this->green->gAction("U")){
									echo '<a title="Edit Employee">
											<img rel="'.$row_emp['empid'].'" src="'.site_url('../assets/images/icons/edit.png').'" onclick="edit_employee(event);">
										  </a>';
								}
								echo '</td>
					       </tr>';
					       $i++;
				}
				}else{
					echo "<tr class='no_data' rel='0' style='text-align:center;background:#FFFFFF;border-bottom:solid 2px #EEEEEE;'><td colspan='10' id='no_data' rel='0'><b>We didn't find anything to show here.</b></td></tr>";
				}
				echo "<tr class='remove_tag'>
						<td colspan='12' id='pgt'>
							<div style='margin-top:20px; width:10%; float:left;'>
							Display : <select id='sort_num'  onchange='search();' style='padding:5px; margin-right:0px;'>";
											
											$num=50;
											for($i=0;$i<10;$i++){?>
												<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
												<?php $num+=50;
											}
											
										echo "</select>
							</div>
							<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
								<ul class='pagination' style='text-align:center'>
								 ".$this->pagination->create_links()."
								</ul>
							</div>

						</td>
					</tr> ";
			}	
		}

}
?>