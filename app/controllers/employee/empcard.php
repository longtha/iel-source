<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Empcard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('employee/modempcard','emp');

	}
	public function index()
	{
		$data['page_header']="Employee Card List";
		$this->load->view('header',$data);
		$this->load->view('employee/vempcard');
		$this->load->view('footer');
	}
	public function loadempcard(){
		$emp_department = $this->input->post("emp_department");
		$yearid = $this->input->post("yearid");
		$data = $this->emp->preempcard($emp_department);
		if(count($data) > 0)
		{
			$tr=$this->blockempcard($data,$yearid);
		}

		$arremp = array();
		header('Content-type:text/x-json');
		$arremp['emp'] = $tr;
		echo json_encode($arremp);
		die();
	}
	public function blockempcard($data=array(),$yearid)
	{
		$tr='';
		$i=1;
		foreach($data->result() as $row)
			{
				$empfullname 	= $row->last_name.' '.$row->first_name;
				$empcardid 		= $row->empcode;
				$position 		= $row->position;
				$department 	= $this->green->getValue("SELECT sed.department FROM sch_emp_department sed WHERE 1=1 AND sed.dep_id = '".($row->dep_id)."'");
				$fulladdress	= $row->perm_adr;
				$phoneline 		= $row->phone;
				$empmail 		= $row->email;
				$rulimage 		= $this->emp->urlimagesemp($yearid,$row->empid);
				$tr.=$this->emp->emplistcard($rulimage,$empfullname,$empcardid,$position,$department,$fulladdress,$phoneline,$empmail);
				if(($i%4) ==0)
				{
					$tr.='<tr style="page-break-after:always;">';
				}
				$i++;
			}
		return $tr;
	}
	public function addtoempcardlist(){
		$yearid = $this->input->post("yearid");
		$idcard = $this->input->post("arrayidcard");
		if(count($idcard) > 0)
		{
			$tr='';
			foreach($idcard as $rowemp){
				$data = $this->emp->getpreempcard($rowemp['idcard']);
				if(count($data) > 0)
				{
					$tr.=$this->blockempcard($data,$yearid);
				}
			}
		}
		$ar_dropcardlist = Array();
		header('Content-type:text/x-json');
		$ar_dropcardlist['dropcardlist']=$tr;
		echo json_encode($ar_dropcardlist);
	}
	public function loadingempcardindialog()
	{
		$yearid = $this->input->post("yearid");
		$searchempamulti = $this->input->post('searchempamulti');
		$where="";
		if($searchempamulti!="")
		{
			$where .=" AND sep.empid='".$searchempamulti."'";
		}
		$data = $this->emp->setSql($where);
		if(count($data) > 0)
		{
			$tr='';
			foreach($data->result() as $row){
				$empid = $row->empid;
				$empcode = $row->empcode;
				$empname = $row->last_name.' '.$row->first_name;
				$empposition = $row->position;
				$mobelephone = $row->phone;
				$empemail = $row->email;
				$tr.=$this->emp->showdataindialog($empid,$empcode,$empname,$empposition,$mobelephone);
			}
		}
		$arremp = array();
		header('Content-type:text/x-json');
		$arremp['emplist'] = $tr;
		echo json_encode($arremp);
	}
	public function autoempcard()
	{
		$key = $_GET['term'];
		$where ='';
		if($key!="")
		{
			$where.=" AND (sep.empcode like '".$key."%'
							OR sep.last_name like '".$key."%'
							OR sep.first_name like '".$key."%'
							OR sep.last_name_kh like '".$key."%'
							OR sep.first_name_kh like '".$key."%'
						)";
		}
		$data = $this->emp->setSql($where);
		$emparray = array();
		if(count($data) > 0){
			foreach($data->result() as $row){
				$fullnameemp = $row->last_name.' '.$row->first_name.' ('.$row->last_name_kh.' '.$row->first_name_kh.') ';
				$emparray[]=array(
									'value'=>$fullnameemp,
									'empid'=>$row->empid
								);
			}	
		}
		echo json_encode($emparray);
	}
}