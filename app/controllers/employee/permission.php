<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Permission extends CI_Controller{
		
		function __construct(){
			parent::__construct();		
			$this->load->model('employee/modepermission','perm');
			$this->load->library('pagination');
		}


		function Index(){
			$this->load->library('pagination');

			$m='';
			$p='';
			if(isset($_GET['m'])){
			    $m=$_GET['m'];
			}
			if(isset($_GET['p'])){
			    $p=$_GET['p'];
			}

			$page=0;
			$year=$this->session->userdata('year');
			if(isset($_GET['per_page'])) $page=$_GET['per_page'];
			$config['base_url'] = site_url()."/employee/permission?pg=1&m=$m&p=$p&y=$year";		
			$config['total_rows'] = $this->green->getTotalRow("SELECT * FROM sch_emp_permission_request epr LEFT JOIN sch_emp_profile emp ON(epr.empid=emp.empid) ORDER BY epr.requestid DESC");
			$config['per_page'] =50;
			//$config['use_page_numbers'] = TRUE;
			$config['num_link']=5;
			$config['page_query_string'] = TRUE;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<a><u>';
			$config['cur_tag_close'] = '</u></a>';

			$this->pagination->initialize($config);	
			$limi=" limit ".$config['per_page'];
			if($page>0){
				$limi=" limit ".$page.",".$config['per_page'];
			}	

			$sql_page = "SELECT emp.last_name,
								emp.first_name,
								sup.last_name as sup_last_name,
								sup.first_name as sup_first_name,
								epr.from,
								epr.reason,
								epr.to,
								epr.date_request,
								epr.request_type,
								epr.requestid 
						FROM sch_emp_permission_request epr 
						LEFT JOIN sch_emp_profile emp 
						ON(epr.empid=emp.empid)
						LEFT JOIN sch_emp_profile sup
						ON(sup.empid=epr.immediate_sup_name) 
						ORDER BY epr.from DESC {$limi} ";
			$data['tdata']=$this->green->getTable($sql_page);
			$data['page_header']="Permission List";
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/permission_list', $data);
			$this->parser->parse('footer', $data);
		}
		function edit($perm_id){
			
			$data['page_header']="Edit Permission";	
			$datas['perm_row']=$this->perm->getperm_row($perm_id);		
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/permission_edit', $datas);
			$this->parser->parse('footer', $data);
		}
		function getdays($to,$from){   
		   $days=$this->green->getValue("SELECT DATEDIFF('".$this->green->FormatSQLDate($to)."','".$this->green->FormatSQLDate($from)."')+1 as num ");
		   echo $days;
		}
		function view($perm_id){
			$data['page_header']="Edit Permission";	
			$datas['perm_row']=$this->perm->getperm_row($perm_id);		
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/preview_permission', $datas);
			$this->parser->parse('footer', $data);
		}
		function delete($perm_id){
			$per_row=$this->db->where('requestid',$perm_id)->get('sch_emp_permission_request')->row();
			$this->updateemp($per_row->empid,$per_row->reason,$per_row->total_day,'-');
			$this->db->where('requestid',$perm_id);
			$this->db->delete('sch_emp_permission_request');
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("employee/permission?m=$m&p=$p");
		}

		function add(){
			$data['page_header']="New Permission";			
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/permission_form', $data);
			$this->parser->parse('footer', $data);
		}

		function save(){
			/*-------------------Validate Field----------*/
			$save_type 			 = 	$_GET['save'];
			$request_type		 = 	$this->input->post('request_type');
			/*-------------------Field-------------------*/
			$date 				 =  date("Y-m-d H:i:s");
			$year 				 = 	$this->input->post('year');
			$empid 				 = 	$this->input->post('employeeid');
			$perm_id 			 = 	$this->input->post('requestid');
			$hr_respond			 = 	$this->input->post('hr_respond');
			$job_type			 = 	$this->input->post('h_job_type');
			$creator 			 =  $this->session->userdata('user_name');
			$immediate_sup_name	 = 	$this->input->post('immediate_sup_name');
			$am_pm	 = 	$this->input->post('am_pm');
			$total_days	 = 	$this->input->post('total_days');

			if( $request_type 	 == 'LR' ){
				
				$to 			 =  $this->input->post('to');
				$from 			 = 	$this->input->post('from');
				$reason 		 = 	$this->input->post('reason_LR');
				$comment 		 = 	$this->input->post('comment');
				$request_hour	 = 	$this->input->post('request_hour');
				$date_request 	 =	$this->input->post('date_request');
				$from_to		 =	"";

			}else{
				$comment 		 =  "";
				$request_hour	 = 	"";
				$reason 		 = 	$this->input->post('reason');
				$from_to 		 =  $this->input->post('from_to');
				$to 			 = 	$from_to;
				$from			 = 	$from_to;
				$date_request	 = 	$from_to;
			}
			
			if( $save_type 		 == 'add' ){

				$lbl_date 	 	 =  'created_date';
				$lbl_creator 	 =  'created_by';
				$val_creator 	 =	$creator;

			}else{

				$lbl_date 	 	 =  'modified_date';
				$lbl_creator 	 =  'modified_by';
			}
			//exit();
			$data = array(
							'empid'					=>	$empid,
							'year'					=>	$year,
							'request_type'			=>	$request_type,
							'request_hour'			=>	$request_hour,
							'from'					=>	$this->green->formatSQLDate($from),
							'to'					=>	$this->green->formatSQLDate($to),
							'reason'				=>	$reason,
							'immediate_sup_name'	=>	$immediate_sup_name,
							'hr_respond'			=>	$hr_respond,
							'job_type'				=>	$job_type,
							'total_day'			    =>	$total_days,
							'am_pm'				    =>	$am_pm,
							'date_request'			=>	$this->green->formatSQLDate($date_request),
							'comment'				=>	$comment,
							$lbl_date				=>	$date,
							$lbl_creator			=>	$creator
						);

			if($save_type =='add'){
				$this->db->insert('sch_emp_permission_request', $data);
				$perm_id = $this->db->insert_id();
				$this->do_upload($perm_id);
				$this->updateemp($empid,$reason,$total_days,'+');
				
			}else{
				$this->db->where('requestid', $perm_id );
				$this->db->update('sch_emp_permission_request', $data);
			}

			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			
			redirect("employee/permission?m=$m&p=$p");
		}
		function updateemp($empid,$reason,$total_days,$a){
			if($reason=='AL'){
				$this->db->query("UPDATE sch_emp_profile 
								SET al_taken=coalesce(al_taken,0) $a ($total_days+0)
									WHERE empid='$empid'");
			}
			if($reason=='SL'){
				$this->db->query("UPDATE sch_emp_profile 
								SET sick_taken=coalesce(sick_taken,0) $a ($total_days+0)
								WHERE empid='$empid'");
			}
		}
		function do_upload($perm_id)
		{
			$year=$this->session->userdata('year');
			$config['upload_path'] ='./assets/upload/employees/permissions/'.$year.'/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '5000';
			$config['file_name']  = "$perm_id.jpg";
			$config['overwrite']=true;
			$config['file_type']='image/jpg';
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('medical_attach'))
			{
				$error = array('error' => $this->upload->display_errors());			
			}
			else
			{				
				 	$config2['image_library'] = 'gd2';
                    $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                    $config2['new_image'] = './assets/upload/employees/permissions/'.$year.'/';

                    $config2['maintain_ratio'] = TRUE;
                    $config2['create_thumb'] = TRUE;
                    $config2['thumb_marker'] = FALSE;
                    $config2['width'] = 120;
                    $config2['height'] = 180;
                    $config2['overwrite']=true;
                    $this->load->library('image_lib',$config2); 

                    if ( !$this->image_lib->resize()){
	                	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
					}else{
						//unlink('./assets/upload/employees/'.$year.'/'.$employee_id.'.png');
					}
			}
		}

		function autoCEmpId(){

			$key=$_GET['term'];
			$this->db->select('*');
			$this->db->from('sch_emp_profile');
			$this->db->like('empcode',$key);
			$this->db->or_like('last_name',$key);
			$this->db->or_like('first_name',$key);
			$this->db->or_like('last_name_kh',$key);
			$this->db->or_like('first_name_kh',$key);
			$this->db->or_like('emp_type',$key);

			$this->db->order_by('empid','desc');			
			$data=$this->db->get()->result();
			//$this->green->getTable($sql_page);
			$array=array();
			
			foreach ($data as $row) {
				$job_type=$this->green->getValue("SELECT job_type FROM sch_emp_contract 
											WHERE empid='".$row->empid."' 
											ORDER BY `year` DESC LIMIT 0,1");

				$array[]=array('value'=>"$row->empcode"." | "."$row->first_name"." "."$row->last_name",
								'id'=>$row->empid,
								'job_type'=>$job_type
								);	
			}
		    echo json_encode($array);
		}

		function autoSup(){

			$key=$_GET['term'];
			$where="";
			if($key!=""){
				$where=" AND ( head_dep like '".$key."'
							emp.last_name like '".$key."'
							emp.first_name like '".$key."'
							emp.last_name_kh like '".$key."'
							emp.first_name_kh like '".$key."'
					)";
			}

			$getHoD=$this->green->getTable("SELECT DISTINCT head_dep 
												FROM sch_emp_department hdep
												INNER JOIN sch_emp_profile emp ON emp.empid=hdep.head_dep
												WHERE 
													head_dep<>'' {$where}");
			
			$array=array();
			if(count($getHoD)>0){
				foreach ($getHoD as $empinf) {
					$row=$this->getHoD($empinf['head_dep']);
					$array[]=array('value'=>$row['empcode']." | ".$row['first_name']." ".$row['last_name'],'id'=>$row['empid']);	
				}
			}		
			
		    echo json_encode($array);
		}


		function getHoD($empid){
			return $this->green->getOneRow("SELECT	
												empid,
												empcode,
												last_name,
												first_name
												FROM
													sch_emp_profile AS emp
												WHERE
													emp.empid = '{$empid}'
												");
		}


		function search_perm(){
			if(isset($_GET['sort_num'])){

				$sort 			=	$_GET['sort'];
				$sort_num 		=	$_GET['sort_num'];
				$empid 			=	$_GET['empid'];
				$request_type 	=  	$_GET['request_type'];
				$leave_type 	=	$_GET['leave_type'];
				$year			=	$_GET['y'];
				$sort_ad 		=   $this->input->post('sort_ad');
				$m=$_GET['m'];
				$p=$_GET['p'];

				$data['tdata']  =  $this->perm->searchperm($sort_num,$empid,$request_type,$leave_type,$sort,$sort_ad,$m,$p,$year);

				$data['page_header']="Permission List";			
				$this->parser->parse('header', $data);
				$this->parser->parse('employee/permission_list', $data);
				$this->parser->parse('footer', $data);
			}
			
			if(!isset($_GET['sort_num'])){
				
				$sort 			=	$_POST['sort'];
				$sort_num 		=	$_POST['sort_num'];
				$empid 			=	$_POST['empid'];
				$request_type 	=	$_POST['request_type'];
				$leave_type 	=	$_POST['leave_type'];
				$sort_ad 		=   $this->input->post('sort_ad');
				$year			= 	$this->input->post('year');
				$m=$this->input->post('m');
				$p=$this->input->post('p');
				
				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
			        $this->green->setActiveModule($m);
			    }
			    if($p!=''){
			        $this->green->setActivePage($p); 
			    }

				$query=$this->perm->searchperm($sort_num,$empid,$request_type,$leave_type,$sort,$sort_ad,$m,$p,$year);
				$leave_type=array('AL'=>'Annual Leave',
						'SL'=>'Sick Leave',
						'SPL'=>'Special Leave',
						'MA'=>'Marriage',
						'ML'=>'Maternity',
						'P'=>'Personal',
						'O'=>'Other');
				$num_perm = 1;
				if(count( $query ) > 0 ){
					foreach( $query as $row_perm ){
						$arr_rt = array('LR'=>'Leave Request','OTR'=>'Overtime Request');
						echo '<tr id="no_data" no_data="1">
								<td class="pos_1">'.($num_perm++).'</td>
								<td class="post_2">'.$row_perm['last_name'].' '.$row_perm['first_name'].'</td>
								<td class="pos_3">'.(isset($arr_rt[$row_perm['request_type']])?$arr_rt[$row_perm['request_type']]:'').'</td>
								<td class="post_3">'.(isset($leave_type[$row_perm['reason']])?$leave_type[$row_perm['reason']]:'').'</td>
								<td class="pos_4">'.date_format(date_create($row_perm['from']),'d/m/Y').'</td>
								<td class="pos_5">'.date_format(date_create($row_perm['to']),'d/m/Y').'</td>
								<td class="pos_6">'.date_format(date_create($row_perm['date_request']),'d/m/Y').'</td>
								<td class="post_8">'.$row_perm['sup_last_name'].' '.$row_perm['sup_first_name'].'</td>
								<td width="1%" class="remove_tag">';
								if($this->green->gAction("P")){
									echo '<a target="_blank" title="View Employee">
											<img rel="'.$row_perm['requestid'].'" src="'.site_url('../assets/images/icons/preview.png').'" onclick="view_permission(event);" style="width:20px;height:20px;">
										  </a>';
								}
								echo '</td>
								<td width="1%" class="remove_tag">';
								if($this->green->gAction("D")){
									echo '<a title="Delete Employee">
											<img rel="'.$row_perm['requestid'].'" onclick="delete_permission(event);" src="'.site_url('../assets/images/icons/delete.png').'" style="width:20px;height:20px;">
										  </a>';
								}
								echo '</td>
								<td width="1%" class="remove_tag">';
								if($this->green->gAction("U")){
									echo '<a title="Edit Employee">
											<img rel="'.$row_perm['requestid'].'" src="'.site_url('../assets/images/icons/edit.png').'" onclick="edit_permission(event);" style="width:20px;height:20px;">
										  </a>';
								}
								echo '</td>
						   </tr>';
					}
				}else{
					echo "<tr><td colspan='8' style='text-align:center;' id='no_data' no_data='0'><b>We didn't find anything to show here</b></td></tr>";
				}
				echo "<tr class='remove_tag'>
							<td colspan='12' id='pgt'>
								<div style='margin-top:20px; width:10%; float:left;'>
								Display : <select id='sort_num'  onchange='search();' style='padding:5px; margin-right:0px;'>";
												
												$num=50;
												for($i=0;$i<10;$i++){?>
													<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
													<?php $num+=50;
												}
												
											echo "</select>
								</div>";
								echo "<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
									<ul class='pagination' style='text-align:center'>
									 ".$this->pagination->create_links()."
									</ul>
								</div>
							</td>
						</tr> ";

			}
		}

		
	}


?>