<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class C_group_evaluation extends CI_Controller{

		function __construct(){
			parent::__construct();				
			$this->load->model('employee/m_group_evaluation','pg');		
		}
		function index(){
			//$this->load->view('header');
			$data['title']='Evaluation Key';
			$this->parser->parse('header', $data);
			$this->load->view('employee/v_group_evaluation');
			$this->parser->parse('footer', $data);
		}
		function save(){
			$this->pg->save();
		}
		function deletedata(){
			$this->pg->deletedata();
		}
		function update(){
			$this->pg->update();
		}
		//  showdata  =============================================================
		function showdata(){
			$tr ='';
			$i  = 0;
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
			foreach ($this->pg->showdata()->result() as $row){
			    $edit   ='<a href="javascript:void(0)" edit="'.$row->group_eval_id.'" class="edit_data"><image  width="15" height="15" src='.base_url().'/assets/images/icons/edit.png></a>';
			   	$delete = "<a href='javascript:void(0)' delete='".$row->group_eval_id."' class='delete_data'><image  src=".base_url()."/assets/images/icons/delete.png></a>";
			    $i++;
				$tr .='<tr>
						<td>'.$i.'</td>
						<td>'.$row->description.'</td>
						<td><div>'.$row->area_eval.'</div><div>'.$row->area_eval_kh.'</div></td>.
						<td>';
							if($this->green->gAction("U")){
								$tr.=$edit;
							}
						$tr .='</td>';
						$tr.='<td>';
							if($this->green->gAction("D")){
								$tr.=$delete;
							}
						$tr .='</td>
				    </tr>';
			} 
			if($i == 0){ 
				$tr.= '<tr>
							<td colspan="9" style="text-align:center; color:#FF0000"> 
								Your Search not have in system. please search try agan.... 
							</td>
						</tr>';
			}
			echo $tr;
			die();
		}

		// edit  ================================================================
		function editdata(){

			$idhidden  = $this->input->post('group_eval_id');
		 	$result  =  $this->pg->editdata($idhidden);		
			 	   echo $result->group_eval_id.'|'.$result->main_eval_id.'|'.$result->area_eval.'|'.$result->area_eval_kh;
			 	die();
		}

		// get_description   ====================================================
		function get_description(){
			
	 		$qr = $this->pg->get_description();
		 	if ($qr->num_rows() > 0)
			{
				$opt = "<option value='0'></option>";
			   foreach ($qr->result() as $row)
			   {
			   		$opt .= "<option value='".$row->main_val_id."'>".$row->description."</option>";
			   	}
			   	echo $opt;
			} 
		}
		
	}
?>		