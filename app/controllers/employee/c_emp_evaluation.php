<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class C_emp_evaluation extends CI_Controller{

		function __construct(){
			parent::__construct();				
			$this->load->model('employee/m_emp_evaluation','mg');		
		}
		function index(){
			//$this->load->view('header');
			$data['title']='';
			$this->parser->parse('header', $data);
			$this->load->view('employee/v_emp_evaluation');
			$this->parser->parse('footer', $data);
		}
		function save(){
			$this->mg->save();
		}
		function update(){
			$this->mg->update();
		}
		// show_employee =======================================
		 function show_employee(){
		 	$id = $this->input->post('id');
		 	$row = $this->db->query("SELECT
											p.position,
											d.department,
											e.pos_id,
											e.dep_id,
											e.empid,
											e.sex,
											CONCAT(e.last_name, ' ' ,e.first_name) AS fullname
										FROM
											sch_emp_profile AS e
										INNER JOIN sch_emp_position AS p ON e.pos_id = p.posid
										INNER JOIN sch_emp_department AS d ON e.dep_id = d.dep_id
										WHERE
											e.empid = '{$id}' ")->row();
			header('Content-Type: application/json');			
			echo json_encode($row);
		 }

		// auto  employeename =======================================
		function employeename($emid=''){
			$sql="SELECT
		           e.empid,
		           CONCAT(e.last_name, ' ' ,e.first_name) AS fullname
		          FROM
		          sch_emp_profile AS e
		         WHERE
		         1 = 1 ";   
			if($emid !=''){
    			$data=$this->db->query($sql. " AND e.empid ='".$emid."' ORDER BY e.first_name ASC");
			    return $data->row()->fullname;
			}else{
				$key=$_GET['term'];
				if($key!=""){
					$sql.=" AND last_name like '".$key."%'";
				}
				$data=$this->db->query($sql. " ORDER BY e.first_name ASC");	
				$array=array();
				foreach ($data->result() as $row) {
					$array[]=array('value'=>"$row->fullname",'id'=>$row->empid);
				}
			     echo json_encode($array);
		 	}
		}
		
		function edit(){
			$transno = $_GET['transno']; 
			
			$data['sch_eva_emp_evalutiont'] = $this->mg->sch_eva_emp_evalutiont($transno);
			//$data['typeno'] = $transno;
			$this->parser->parse('header',$data);
			$this->load->view('employee/v_edit_emp_evaluation',$data);
			$this->parser->parse('footer', $data);

		}

	}
?>
