<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class C_mention_evaluation extends CI_Controller{

		function __construct(){
			parent::__construct();				
			$this->load->model('employee/m_mention_evaluation','mm');		
		}
		function index(){
			//$this->load->view('header');
			$data['title']='Evaluation Mention';
			$this->parser->parse('header', $data);
			$this->load->view('employee/v_mention_evaluation');
			$this->parser->parse('footer', $data);
		}
		function deletedata(){
			$this->mm->deletedata();
		}
		function update(){
			$this->mm->update();
		}
		function save(){
			$this->mm->save();
		}

		//  showdata  =============================================================
		function showdata(){
			$tr ='';
			$i  = 0;
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
			foreach ($this->mm->showdata()->result() as $row){
			    $edit   ='<a href="javascript:void(0)" edit="'.$row->mention_id.'" class="edit_data"><image  width="15" height="15" src='.base_url().'/assets/images/icons/edit.png></a>';
			   	$delete = "<a href='javascript:void(0)' delete='".$row->mention_id."' class='delete_data'><image  src=".base_url()."/assets/images/icons/delete.png></a>";
			    $i++;
				$tr .='<tr>
							<td>'.$i.'</td>
							<td>'.$row->score.'</td>
							<td>'.$row->mention.'</td>
							<td>'.$row->mention_kh.'</td>
							<td>'.$row->min_rate.'</td>
							<td>'.$row->max_rate.'</td>
							<td>';
							if($this->green->gAction("U")){
								$tr.=$edit;
							}
						$tr .='</td>';
						$tr.='<td>';
							if($this->green->gAction("D")){
								$tr.=$delete;
							}
						$tr .='</td>
				    </tr>';
			} 
			echo $tr;
			die();
		}
		// editdata  ================================================================
		function editdata(){
			$idhidden  = $this->input->post('mention_id');
		 	$result  =  $this->mm->editdata($idhidden);		
			echo $result->mention_id.'|'.$result->score.'|'.$result->mention.'|'.$result->mention_kh.'|'.$result->min_rate.'|'.$result->max_rate;
			 	die();
		}
	}
?>