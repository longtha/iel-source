<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Department extends CI_Controller{

		function __construct(){
			parent::__construct();	
			$this->load->model("employee/ModDepartment","dep");	
			$this->load->library('pagination');	
			$this->thead=array("No"=>'no',
								"Department"=>'department',
								"Department(KH)"=>'department_kh',
								"Head of Department"=>'head_dep',									 
								 'Action'=>'action'
								);
			$this->idfield="displanid";					
		}
		function index()
		{	

			$data['tdata']=$this->dep->getdepartment();
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Department";			
			$this->parser->parse('header', $data);
			$this->parser->parse('employee/department_list', $data);
			$this->parser->parse('footer', $data);
		}
		function save(){
			date_default_timezone_set("Asia/Phnom_Penh");
			$dep_id=$this->input->post('dep_id');
			$department=$this->input->post('department');
			$department_kh=$this->input->post('department_kh');
			$order=$this->input->post('order');
			$head_dep=$this->input->post('head_dep');

			$c_date=date('Y-m-d H:i:s');
			$user=$this->session->userdata('user_name');
			$data=array('department'=>$department,
						'department_kh'=>$department_kh,
						'order'=>$order,
						'head_dep'=>$head_dep
						);
			//$count=$this->dep->validate($department,$dep_id);
			if($dep_id!=''){
				$data2=array('midified_by'=>$user,
							'modified_date'=>$c_date);
				$this->db->where('dep_id',$dep_id)->update('sch_emp_department',array_merge($data,$data2));
			}else{
				$data2=array('created_by'=>$user,
							'created_date'=>$c_date);
				$this->db->insert('sch_emp_department',array_merge($data,$data2));
			}
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("employee/department?m=$m&p=$p");
		}
		function edit(){
			$dep_id=$this->input->post('dep_id');
			$row=$this->db->where('dep_id',$dep_id)->get('sch_emp_department')->row();
			header("Content-type:text/x-json");
			echo json_encode($row);
		}
		function validate(){
			$dep_id=$this->input->post('dep_id');
			$department=$this->input->post('department');
			$this->db->select('count(dep_id) as count')
					->from('sch_emp_department')
					->where('department',$department);
			if($dep_id!='')
				$this->db->where_not_in('dep_id',$dep_id);
			$count=$this->db->get()->row()->count;
			echo $count;
		}
		function delete($dep_id){
			$this->db->where('dep_id',$dep_id)->delete('sch_emp_department');
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("employee/department?m=$m&p=$p");
		}
		function search(){
			if(isset($_GET['m'])){
					$department=$_GET['dep'];
					$m=$_GET['m'];
					$p=$_GET['p'];
					$sort_num=$_GET['s_num'];
					$data['tdata']=$this->dep->search($department,$sort_num,$m,$p);
					$data['idfield']=$this->idfield;		
					$data['thead']=	$this->thead;
					$data['page_header']="Department";			
					$this->parser->parse('header', $data);
					$this->parser->parse('employee/department_list', $data);
					$this->parser->parse('footer', $data);
			}else{
					
					$department=$this->input->post('department');
					$sort_num=$this->input->post('s_num');
					$i=1;
					$m=$this->input->post('m');
					$p=$this->input->post('p');
					$this->green->setActiveRole($this->input->post('roleid'));
					if($m!=''){
				        $this->green->setActiveModule($m);
				    }
				    if($p!=''){
				        $this->green->setActivePage($p); 
				    }
					foreach ($this->dep->search($department,$sort_num,$m,$p) as $row) {
						echo "<tr>
								<td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
								<td class='department'>".$row->department."</td>
								<td class='department_kh'>".$row->department_kh."</td>
								<td>".$this->dep->getHoD($row->head_dep)."</td>
								<td class='remove_tag'>";
								if($this->green->gAction("D")){
									echo "<a>
									 		<img rel=".$row->dep_id." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
									 	</a>";
								}
								if($this->green->gAction("U")){
									echo "<a>
									 		<img  rel=".$row->dep_id." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
									 	</a>";
								}
							echo "</td>
						</tr>
						 ";
						 $i++;
					}
					echo "<tr class='remove_tag'>
								<td colspan='12' id='pgt'>
									<div style='margin-top:20px; width:10%; float:left;'>
									Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
													$num=50;
													for($i=0;$i<10;$i++){?>
														<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
														<?php $num+=50;
													}
												echo "</select>
									</div>
									<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
										<ul class='pagination' style='text-align:center'>
										 ".$this->pagination->create_links()."
										</ul>
									</div>

								</td>
							</tr> ";
			}
			
		}
		function autoEmpHod(){
			$key=$_GET['term'];
			$this->db->select('*');
			$this->db->from('sch_emp_profile');
			$this->db->like('empcode',$key);
			$this->db->or_like('first_name',$key);
			$this->db->or_like('last_name',$key);
			$this->db->or_like('first_name_kh',$key);			
			$this->db->or_like('emp_type',$key);
			$this->db->order_by('empid','desc');			
			$data=$this->db->get()->result();
			//$this->green->getTable($sql_page);
			$array=array();
			foreach ($data as $row) {
				$array[]=array('value'=>"$row->empcode"." : "."$row->first_name"." "."$row->last_name",'id'=>$row->empid);	
			}
		    echo json_encode($array);
		}
		
}