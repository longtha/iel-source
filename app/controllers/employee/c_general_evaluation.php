<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class C_general_evaluation extends CI_Controller{

		function __construct(){
			parent::__construct();				
			$this->load->model('employee/m_general_evaluation','m');		
		}
		function index(){
			$this->load->view('header');
			$this->load->view('employee/v_general_evaluation');
		}
		function deletedata(){
			$this->m->deletedata();
		}
		function save(){
			$this->m->save();
		}
		function update(){
			$this->m->update();
		}
		//  showdata =======================================================
		function showdata(){
			$tr ='';
			$i  = 0;
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
			foreach ($this->m->showdata()->result() as $row){
			    $edit   ='<a href="javascript:void(0)" edit="'.$row->main_val_id.'" class="edit_data"><image  width="15" height="15" src='.base_url().'/assets/images/icons/edit.png></a>';
			   	$delete = "<a href='javascript:void(0)' delete='".$row->main_val_id."' class='delete_data'><image  src=".base_url()."/assets/images/icons/delete.png></a>";
			    $i++;
				$tr .='<tr>
						<td>'.$i.'</td>
						<td>'.$row->description.'</td>
						<td>';
							if($this->green->gAction("U")){
								$tr.=$edit;
							}
						$tr .='</td>';
						$tr.='<td>';
							if($this->green->gAction("D")){
								$tr.=$delete;
							}
						$tr .='</td>
				    </tr>';
			}
			echo $tr;
			die();
		}
		//   edit ======================================================
		function editdata(){
			$h_custcode  = $this->input->post('main_val_id');
		 	$result  =  $this->m->editdata($h_custcode);		
			 	   echo $result->main_val_id.'|'.$result->description;
			 	die();
		}//edit


	}							
?>