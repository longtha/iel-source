<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Generalkhmerlanguage extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->lang->load('generalkhmerlanguage', 'khmer');	
		$this->load->model('reports/Generalkhmerlanguagemodel','mode');		
		$this->load->model('school/schoolinformodel', 'school');
		$this->load->model('school/program', 'program');
		$this->load->model('school/schoollevelmodel', 'school_level');
		$this->load->model('student/m_evaluate_trans_kgp', 'student');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('reports/generalkhmerlanguage/index');
		$this->load->view('footer');
	}

	//get class data---------------------------------------------
	public function get_class_data(){
		$program_id = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$grade_level_id = $this->input->post('grade_level_id');
		$c_opt = '<option value=""></option>';
		$get_class_data = $this->mode->getClassData('', $school_level_id, $grade_level_id, '', '');
		//getClassData($class_id,$program_id, $school_level_id, $grade_level_id, $adcademic_year_id);
		if(count($get_class_data) > 0){
			foreach($get_class_data as $r_c){
				$c_opt .= '<option value="'. $r_c->classid .'">'. $r_c->class_name .'</option>';
			}
		}
				
		echo $c_opt;
	}


	//get student data-----------------------------------------
		public function get_student_data()
		{
			$class_id = $this->input->post('class_id');
			$schoolid = $this->input->post('schoolid');
			$programid = $this->input->post('programid');
			$schlavelid = $this->input->post('schlavelid');
			$yearid = $this->input->post('yearid');
			$gradlavelid = $this->input->post('gradlavelid');
			$where = "";
			// if($class_id != ""){
			// 	$where .= " AND sl.class_id = ". $class_id;
			// }
			$where .= " schoolid=".$schoolid." AND schlevelid = ".$schlavelid." AND programid=".$programid." AND year=".$yearid." AND grade_levelid=".$gradlavelid." AND classid = ". $class_id;
			$s_opt = '<option value=""></option>';

			$i = 1;
			$get_student_data = $this->mode->getStudent($where);
			if(count($get_student_data) > 0){
				foreach($get_student_data as $r_s){
					$s_opt .= '<option value="'. $r_s->studentid .'">'. $i .'. '. $r_s->last_name .' '. $r_s->first_name .'</option>';
					$i++;
				}
			}
						
			echo $s_opt;
		}
	//for primary school----------------------------------------
	public function primary_monthly_report_class_ranking(){
		$program_id = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$class_id = $this->input->post('class_id');
		$student_id = $this->input->post('student_id');
		$get_result_type = $this->input->post('get_result_type');

		$data = array();
		$where = "";
		if($program_id != ""){
			$where .= " AND sp.programid = ". $program_id;
		}
		if($school_level_id != ""){
			$where .= " AND sl.school_level_id = ". $school_level_id;
		}
		if($adcademic_year_id != ""){
			$where .= " AND sy.yearid = ". $adcademic_year_id;
		}
		if($class_id != ""){
			$where .= " AND sc.classid = ". $class_id;
		}
		if($student_id != ""){
			$where .= " AND s.studentid = ". $student_id;
		}

		if($get_result_type != ""){
			$where .= " AND sl.exam_monthly = ". $get_result_type;
		}


		//get class name
		$class_name = array();
		//$get_class_data = $this->mode->getClassData($program_id, $school_level_id, $grade_level_id, $adcademic_year_id, $class_id);
		$get_class_data = $this->mode->getClassData('', $school_level_id, $grade_level_id, '', '');
		if(count($get_class_data) > 0){
			foreach($get_class_data as $r_c){
				$class_name["class_name"] = $r_c->class_name;
				
			}
		}
		//get student and average score
		$stu_data = array(); 
		$get_student_info = $this->mode->getStudentInformation($where);
		//$get_student_info = $this->student->getStudentInformation($where);
		$sch_year = '';

		$edinboro = 'ល្អ';

		if(count($get_student_info) > 0){
			foreach($get_student_info as $r_stu){
				$avg_score = $this->mode->calculateAverageScoreMonthly($r_stu->student_id, $class_id, $grade_level_id, $adcademic_year_id, $school_level_id, $program_id, $get_result_type);
				$r_stu->{"row_avg"} = $avg_score;
				$sch_year = $r_stu->sch_year;				
				$stu_data[] = $r_stu;
			}			
		}
		$data["class_name"] = $class_name;
		$data["month_name"] = $get_result_type;
		$data["getStuData"] = $stu_data;
		$data["sch_year"] = $sch_year;
		$data["edinboro"] = $edinboro;
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_class_ranking', $data);
	}


	public function primary_monthly_report_individual_ranking(){
	
		$program_id = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$class_id = $this->input->post('class_id');
		$student_id = $this->input->post('student_id');
		$get_result_type = $this->input->post('get_result_type');
		$data1 = array();
		$where = "";
		if($class_id != ""){
			$where .= " AND sl.class_id = ". $class_id;
		}
		if($student_id != ""){
			$where .= " AND sl.student_id = ". $student_id;
		}

		if($program_id != ""){
			$where .= " AND sl.program_id = ". $program_id;
		}
		if($school_level_id != ""){
			$where .= " AND sl.school_level_id = ". $school_level_id;
		}
		if($grade_level_id != ""){
			$where .= " AND sl.grade_level_id = ". $grade_level_id;
		}
		if($adcademic_year_id != ""){
			$where .= " AND sl.adcademic_year_id = ". $adcademic_year_id;
		}
		$studentname="";
		$year = "";
		$gtotal=0;
		$countSubject=1;
		
		$class_name = array();
		$Student = $this->db->query("SELECT DISTINCT
										sl.student_id,
										s.last_name,
										s.first_name,
									CONCAT(s.last_name,'  ',s.first_name) AS studentname,
										s.gender,
										sl.class_id,
										sc.class_name,
										y.sch_year,
										sl.program_id,
										sl.school_level_id,
										sl.adcademic_year_id,
										sl.grade_level_id
									FROM
										sch_subject_score_monthly_kgp_multi AS sl
									INNER JOIN sch_student AS s ON sl.student_id = s.studentid
									INNER JOIN sch_class AS sc ON sl.class_id = sc.classid
									INNER JOIN sch_school_year AS y ON sl.adcademic_year_id = y.yearid
									WHERE 1 = 1 
									AND sl.school_level_id = '".$school_level_id."' 
									AND sl.grade_level_id = '".$grade_level_id."'
									AND sl.program_id = '".$program_id."'
									AND sl.adcademic_year_id = '".$adcademic_year_id."'
									AND sl.class_id = '".$class_id."'
									-- {$where} 
									");
		$row = $Student->row();
		//$year = $row->sch_year;

		$mention ='ល្អ';
		
		// khmer & english part =====
		$q = $this->db->query("SELECT
									st.subject_type,
									st.main_type,
									st.subject_type_kh,
									st.subj_type_id
								FROM
									sch_subject_type AS st 
								WHERE st.main_type = '1' 
								AND st.schlevelid = '".$school_level_id."' 
								ORDER BY orders ASC");
		
		$tr = '';
		$tr .= '<tr style="text-align: center;">
					<td colspan="2" style="vertical-align: middle;">មុខវិជ្ជា Subjects</td>
					<td>ពិន្ទុ <br>​ Score</td>
					<td>ការវាយតម្លៃ <br> Evaluation</td>					
				</tr>';
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $r) {
				// subject ==========
				$q_ = $this->db->query("SELECT
											s.subjectid,
											s.subj_type_id,
											s.subject_kh
										FROM
											sch_subject
										AS s WHERE s.subj_type_id = '".$r->subj_type_id."' ");
				$i = 0;
				if ($q_->num_rows() > 0) {					
					foreach ($q_->result() as $r_) {

						$sql = "SELECT DISTINCT
										--m.total_score,
										d.total_score
									FROM
										sch_subject_score_monthly_kgp_multi AS m
									INNER JOIN sch_subject_score_monthly_detail_kgp_multi AS d ON m.student_id = d.student_id
									WHERE d.subject_mainid = '1'
									AND d.subject_groupid = '".$r_->subj_type_id."'
									AND d.subject_id = '".$r_->subjectid."'
									AND m.student_id = '".$student_id."'
									AND d.program_id = '".$program_id ."'
									AND d.school_level_id = '".$school_level_id."'
									AND d.class_id = '".$class_id."'
									AND d.adcademic_year_id = '".$adcademic_year_id."'
									AND m.exam_monthly = '".$get_result_type."'
									AND d.grade_level_id = '".$grade_level_id."' ";

						// echo ($sql);
						$sm = $this->db->query($sql)->row();
						$countSubject++;

						$total_score = isset($sm->total_score) ? $sm->total_score : 0;
						$gtotal += $total_score - 0;

						$i++;

						$tr .= '<tr>';
						if ($i == 1) {
							$tr .=	'<td rowspan="'.($q_->num_rows()).'" style="vertical-align: middle;text-align:center;">'.$r->subject_type.'</td>';
						}
						$tr .=	'<td style="text-align:center;">'.$r_->subject_kh.'</td>
								<td style="text-align:center;">'.number_format($total_score, 2).' 
								
								</td>
								<td style="text-align:center;">'.$mention.'</td>
		 					</tr>';

					}					
				}// for subject ====
			}	
		}// for sub group ====
		else {
			$tr .= '<tr><td colspan="4" style="text-align:center;">Data not foun .!</td></tr>';
		}

// frorm to --------------------------
		$q = $this->db->query("SELECT
									st.subject_type,
									st.main_type,
									st.subject_type_kh,
									st.subj_type_id
								FROM
									sch_subject_type AS st 
								WHERE st.main_type = '2' AND st.schlevelid = '".$school_level_id."' 
								ORDER BY orders ASC");
		
		$trr = '';
		$trr .= '<tr style="text-align: center;">
					<td colspan="2" style="vertical-align: middle;">មុខវិជ្ជា Subjects</td>
					<td>ពិន្ទុ <br>​ Score</td>
					<td>ការវាយតម្លៃ <br> Evaluation</td>					
				</tr>';
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $r) {
				// subject ==========
				$q_ = $this->db->query("SELECT
											s.subjectid,
											s.subj_type_id,
											s.subject_kh
										FROM
											sch_subject
										AS s WHERE s.subj_type_id = '".$r->subj_type_id."' ");
				$j = 0;
				// $gtotal=0;
				if ($q_->num_rows() > 0) {					
					foreach ($q_->result() as $r_) {

						$sql = "SELECT DISTINCT
										-- m.total_score,
										d.total_score,
										m.absent,
										m.present,
										m.ranking_bysubj
									FROM
										sch_subject_score_monthly_kgp_multi AS m
									INNER JOIN sch_subject_score_monthly_detail_kgp_multi AS d ON m.student_id = d.student_id
									WHERE d.subject_mainid = '2'
									AND d.subject_groupid = '".$r_->subj_type_id."'
									AND d.subject_id = '".$r_->subjectid."'
									AND m.student_id = '".$student_id."'
									AND d.program_id = '".$program_id ."'
									AND d.school_level_id = '".$school_level_id."'
									AND d.class_id = '".$class_id."'
									AND d.adcademic_year_id = '".$adcademic_year_id."'
									AND m.exam_monthly = '".$get_result_type."'
									AND d.grade_level_id = '".$grade_level_id."' ";

						// echo ($sql);
						$sm = $this->db->query($sql)->row();
						// $countSubject++;

						$total_score = isset($sm->total_score) ? $sm->total_score : 0;												
						$gtotal += $total_score - 0;

						$j++;

						$trr .= '<tr>';
						if ($j == 1) {
							$trr .=	'<td rowspan="'.($q_->num_rows()).'" style="vertical-align: middle;text-align:center;">'.$r->subject_type.'</td>';
						}
						$trr .='<td style="text-align:center;">'.$r_->subject_kh.'</td>
								  <td style="text-align:center;">'.number_format($total_score, 2).'</td>
								  <td style="text-align:center;">'.$mention.'</td>
		 						</tr>';
					}		
			
				}// for subject ====
			}	
		}// for sub group ====
		 else {
			$trr .= '<tr><td colspan="4" style="text-align:center;">Data not foun .!</td></tr>';
		}

		// fort ter ---------------
		$sqr = "SELECT DISTINCT
								m.absent,
								m.present,
								m.ranking_bysubj,
								m.average_score
							FROM
								sch_subject_score_monthly_kgp_multi AS m
							INNER JOIN sch_subject_score_monthly_detail_kgp_multi AS d ON m.student_id = d.student_id
							WHERE 1=1
							AND m.student_id = '".$student_id."'
							AND d.program_id = '".$program_id ."'
							AND d.school_level_id = '".$school_level_id."'
							AND d.class_id = '".$class_id."'
							AND d.adcademic_year_id = '".$adcademic_year_id."'
							AND m.exam_monthly = '".$get_result_type."'
							AND d.grade_level_id = '".$grade_level_id."' ";
							
		$rows = $this->db->query($sqr)->row();					

		$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM sch_subject_score_monthly_kgp_multi AS sl  
										WHERE 1=1 
										AND sl.class_id = '".$class_id."' 
										AND sl.program_id = '".$program_id."' 
										AND sl.school_level_id = '".$school_level_id."' 
										AND sl.grade_level_id = '".$grade_level_id."' ")->row()->numrow;
		//print_r($total_row);
		$data1["total_row"] = $total_row ;
		$data1["gtotal"] = $gtotal ;
		$data1["rows"] = $rows;
		$data1["trr"] = $trr;
		$data1["tr"] = $tr;
		$data1["month_name"] = $get_result_type;
		$data1["row"] = $row;
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_individual_ranking',$data1);
	}

	public function primary_yearly_report_individual_ranking(){	
			$class_id = $this->input->post('class_id');
			$student_id = $this->input->post('student_id');
			$where = "";

			if($class_id != ""){
				$where .= " AND f.class_id = ". $class_id;
			}
			if($student_id != ""){
				$where .= " AND f.student_id = ". $student_id;
			}
			$row = $this->mode->getStudentfinal($where);
		$data2['row'] = $row;
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_yearly_report_individual_ranking', $data2);
	}

	
	// public function primary_monthly_report_class_ranking_all(){

	// 	$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_class_ranking_all');
	// }
}
