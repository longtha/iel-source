<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class C_full_time_final_report extends CI_Controller
	{		
		function __construct()
		{
			parent::__construct();
			$this->load->model("reports/mod_full_time_report", "full");
		}

		function index(){
			$data['query']= $this->full->get_assessment();
			$data['query_core']= $this->full->get_core();
			$data['query_skill']= $this->full->get_skill();
			$data['grading']=$this->full->get_grad();
			//$data['bbb'] = $this->input->post("schoollavel");
			$this->load->view('header');
			$this->load->view('reports/st_reports/v_full_time_final_report',$data);
			$this->load->view('footer');
		}
		function print_all_fulltime(){
			$schoolid  = $_GET['schoolid'];
			$schoollavel = $_GET['schoollavel'];
			$gradelavel= $_GET['gradelavel'];
			$classid   = $_GET['classid'];
			$years     = $_GET['years'];
			$is_partime= $_GET['is_partime'];
			$termid    = $_GET['termid'];
			$perpage    = $_GET['perpage'];
			$numrow    = $_GET['numrow'];
			$data['query']       = $this->full->get_assessment();
			$data['query_core']  = $this->full->get_core();
			$data['query_skill'] = $this->full->get_skill();
			$data['grading']     = $this->full->get_grad();
			$data['infor'] = array("schoolid"=>$schoolid,
									"schoollavel"=>$schoollavel,
									"gradelavel"=>$gradelavel,
									"classid"=>$classid,
									"years"=>$years,
									"is_partime"=>$is_partime,
									"termid"=>$termid,
									"perpage"=>$perpage,
									"numrow"=>$numrow
								);
			$this->load->view('header');
			$this->load->view('reports/st_reports/v_full_time_all_report',$data);
			$this->load->view('footer');
		}
	}
