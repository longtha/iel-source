<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class c_annual_academinc_report extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();		
		}
		function index(){
			$opt_schoolid = $this->show_schoolid();
			$data['schoolid']=$opt_schoolid;
			$this->load->view('header');
			$this->load->view("student/v_annual_academic_list",$data);
			$this->load->view('footer');
		}
		
		function show_schoolid(){
			$opt_school = "";
			$sql = $this->db->get('sch_school_infor')->result();
			foreach ($sql as  $row){
				$opt_school .='<option value="'.$row->schoolid.'">'.$row->name.'</option>';
			}
			return $opt_school;
		}
		function show_school_lavel(){
			$schoolid = $this->input->post('schoolid');
			$sql_s = $this->db->query('SELECT schlevelid,sch_level FROM sch_school_level 
																	WHERE 1=1 AND schoolid="'.$schoolid.'" 
																	AND programid=2');
			$option_level ='<option value=""></option>';
			if($sql_s->num_rows() > 0){
				foreach($sql_s->result() as $rs){
					$option_level .='<option value="'.$rs->schlevelid.'">'.$rs->sch_level.'</option>';
				}
			}
			
			header("Content-type:text/x-json");
			$arr_success['schlevel'] = $option_level;
			echo json_encode($arr_success);
		
		}
		function show_year(){
			$schoolid    = $this->input->post('schoolid');
			$schoollavel = $this->input->post('schoollavel');
			$sql_y = $this->db->query('SELECT yearid,sch_year FROM sch_school_year 
																	WHERE 1=1 
																	AND schoolid="'.$schoolid.'"
																	AND schlevelid="'.$schoollavel.'"
																	AND programid=2');
			$option_y ='<option value=""></option>';
			if($sql_y->num_rows() > 0){
				foreach($sql_y->result() as $ry){
					$option_y .='<option value="'.$ry->yearid.'">'.$ry->sch_year.'</option>';
				}
			}
			
			header("Content-type:text/x-json");
			$arr_success['yearid'] = $option_y;
			echo json_encode($arr_success);
		
		}
		function show_grade_lavel(){
			$schoolid    = $this->input->post('schoolid');
			$schoollavel = $this->input->post('schoollavel');
			$sql_gr = $this->db->query('SELECT  grade_levelid,
												grade_level 
										FROM sch_grade_level 
										WHERE 1=1 
										AND schoolid="'.$schoolid.'"
										AND schlevelid="'.$schoollavel.'"
									');
			$option_gr ='<option value=""></option>';
			if($sql_gr->num_rows() > 0){
				foreach($sql_gr->result() as $gr){
					$option_gr .='<option value="'.$gr->grade_levelid.'">'.$gr->grade_level.'</option>';
				}
			}
			
			header("Content-type:text/x-json");
			$arr_success['grade'] = $option_gr;
			echo json_encode($arr_success);
		
		}
		function show_class(){
			$schoolid    = $this->input->post('schoolid');
			$schoollavel = $this->input->post('schoollavel');
			$yearid      = $this->input->post('yearid');
			$gradeid     = $this->input->post('gradeid');
			$sql_cl = $this->db->query('SELECT  classid,
												class_name 
										FROM sch_class 
										WHERE 1=1 
										AND schoolid="'.$schoolid.'"
										AND schlevelid="'.$schoollavel.'"
										AND grade_levelid="'.$gradeid.'"
									');
			$option_cl ='<option value=""></option>';
			if($sql_cl->num_rows() > 0){
				foreach($sql_cl->result() as $rcl){
					$option_cl .='<option value="'.$rcl->classid.'">'.$rcl->class_name.'</option>';
				}
			}
			
			header("Content-type:text/x-json");
			$arr_success['classid'] = $option_cl;
			echo json_encode($arr_success);
		
		}
		function show_student_list(){
			$schoolid  = $this->input->post("schoolid");
			$yearid    = $this->input->post("yearid");
			$sch_level = $this->input->post("schoollavel");
			$gradlevel = $this->input->post("gradeid");
			$classid   = $this->input->post("classid");
			$total_score = $this->db->query("SELECT
												tbl.score_entry_id,
												tbl.schoolid,
												tbl.schlevelid,
												tbl.yearid,
												tbl.semesterid,
												tbl.termid,
												tbl.gradeid,
												tbl.classid,
												tbl.studentid,
												tbl.total_score
												FROM
												sch_iep_total_score_entry AS tbl
												WHERE 1=1 
												AND tbl.classid='".$classid."'
												AND tbl.schoolid='".$schoolid."'
												AND tbl.schlevelid='".$sch_level."'
												AND tbl.gradeid='".$gradlevel."'
												AND tbl.yearid='".$yearid."'
												-- GROUP BY tbl.studentid
												ORDER BY
												tbl.examtypeid ASC,
												tbl.total_score DESC
											");
			$arr_sc = array();
			if($total_score->num_rows() > 0){
				foreach($total_score->result() as $row_s){
					$score = floor(($row_s->total_score/2)*100)/100;
					if(isset($arr_sc[$row_s->studentid])){
						$arr_sc[$row_s->studentid] = $arr_sc[$row_s->studentid]+$score;
					}else{
						$arr_sc[$row_s->studentid] = $score;
					}
					
				}
			}
		if($this->session->userdata('match_con_posid')!='stu'){
			$setSQLString = "SELECT
										vp.student_num,
										vp.first_name,
										vp.last_name,
										CONCAT(
											vp.last_name,
											' ',
											vp.first_name
										) AS fullname,
										vp.first_name_kh,
										vp.last_name_kh,
										CONCAT(
											vp.last_name_kh,
											' ',
											vp.first_name_kh
										) AS fullname_kh,
										vp.gender,
										vp.class_name,
										vp.studentid,
										vp.schoolid,
										vp.`year`,
										vp.classid,
										vp.schlevelid,
										vp.rangelevelid,
										vp.feetypeid,
										vp.programid,
										vp.sch_level,
										vp.rangelevelname,
										vp.program,
										vp.sch_year,
										vp.grade_levelid,
										g.grade_level,
										tt.yearid
									FROM
										v_student_enroment AS vp
									INNER JOIN sch_school_term AS tt ON tt.yearid = vp.`year`
									AND vp.schlevelid = tt.schlevelid
									INNER JOIN sch_grade_level AS g ON vp.grade_levelid = g.grade_levelid
									WHERE
										1 = 1
									AND vp.schoolid =".($schoolid==''?'NULL':$schoolid)."
									AND vp.schlevelid ='".$sch_level."'
									AND vp.grade_levelid ='".$gradlevel."' 
									AND vp.classid ='".$classid."' 
									AND vp.`year` = '".$yearid."'
									GROUP BY
										vp.studentid
									ORDER BY
										vp.studentid ASC";
			}else{
				$setSQLString = "SELECT
										vp.student_num,
										vp.first_name,
										vp.last_name,
										CONCAT(
											vp.last_name,
											' ',
											vp.first_name
										) AS fullname,
										vp.first_name_kh,
										vp.last_name_kh,
										CONCAT(
											vp.last_name_kh,
											' ',
											vp.first_name_kh
										) AS fullname_kh,
										vp.gender,
										vp.class_name,
										vp.studentid,
										vp.schoolid,
										vp.`year`,
										vp.classid,
										vp.schlevelid,
										vp.rangelevelid,
										vp.feetypeid,
										vp.programid,
										vp.sch_level,
										vp.rangelevelname,
										vp.program,
										vp.sch_year,
										vp.grade_levelid,
										g.grade_level,
										tt.yearid
									FROM
										v_student_enroment AS vp
									INNER JOIN sch_school_term AS tt ON tt.yearid = vp.`year`
									AND vp.schlevelid = tt.schlevelid
									INNER JOIN sch_grade_level AS g ON vp.grade_levelid = g.grade_levelid
									WHERE
										1 = 1
									AND vp.studentid='".($this->session->userdata('emp_id'))."' AND vp.programid=2
									GROUP BY
										vp.studentid
									ORDER BY
										vp.studentid ASC";
			}
			$sql = $this->db->query($setSQLString);
			
			$tr = "";
			$ii = 1;
			if($sql->num_rows() > 0){
				foreach($sql->result() as $row_l){
					$show_sc = isset($arr_sc[$row_l->studentid])?$arr_sc[$row_l->studentid]:0;
					$tr.="<tr>
								<td>".($ii++)."</td>
								<td>".$row_l->student_num."</td>
								<td><span id='f_name_eg'>".$row_l->fullname."</span><br><span id='f_name_kh'>".$row_l->fullname_kh."</span>
									<input type='hidden' name='hstudentid' id='hstudentid' value='".$row_l->studentid."'>
								</td>
								<td>".ucwords($row_l->gender)."</td>
								<td>".$row_l->sch_level."</td>
								<td>".$row_l->grade_level."</td>
								<td>".$row_l->sch_year."</td>
								<td>".$row_l->class_name."</td>
								<td style='text-align:right;'>".(floor(($show_sc/8)*100)/100)."</td>
								<td style='text-align:center;'>";
							if($this->green->gAction("C")){ 
								$tr.="<a href='javascript:void(0)' id='add_comment' studentid='".$row_l->studentid."'>
											<img src='".base_url("assets/images/icons/add.png")."' style='margin-right:0;'>
										</a>";
							}
							$tr.="<a href='".site_url("reports/c_annual_academinc_report/show_report_print")."?stid=".$row_l->studentid."&schoolid=".$row_l->schoolid."&schlavel=".$row_l->schlevelid."&gradlevel=".$row_l->grade_levelid."&yearid=".$row_l->yearid."&classid=".$row_l->classid."' target='_blank' id='print_report' studentid='".$row_l->studentid."'>
										<img src='".base_url("assets/images/icons/a_preview.png")."' style='margin-right:0;'>
									</a>";
					$tr.="</td>
						</tr>";
				}
			}else{
				$tr="<tr><td colspan='10' style='text-align:center;'><b><i>Data no have</i></b></td></tr>";
			}
			$link_page = "";
			if($ii > 1){
				$link_page = "<a href='".site_url("reports/c_annual_academinc_report/show_page_all")."?schid=".$schoolid."&schlavel=".$sch_level."&gradlavel=".$gradlevel."&yearid=".$yearid."&classid=".$classid."' target='_blanck'>Print All</a>";
			}
			header("Content-type:text/x-json");
			//header("Content-type:text/x-json");
			$arr["link_p"] = $link_page;
			$arr["tr_tbl"] = $tr;
			echo json_encode($arr);
		}
		function show_command(){
			$studentid = $this->input->post("studentid");
			$classid   = $this->input->post("classid");
			$schoolid  = $this->input->post("schoolid");
			$schlavelid= $this->input->post("schlavelid");
			$yearid    = $this->input->post("yearid");
			$gradeid   = $this->input->post("gradeid");
			//$command   = $this->input->post("command");
			$select_sql = $this->db->query("SELECT
											tbl.id,
											tbl.student_id,
											tbl.schoolid,
											tbl.schlevelid,
											tbl.yearid,
											tbl.gradelevelid,
											tbl.classid,
											tbl.command_teacher,
											tbl.academicid,
											tbl.userid,
											tbl.date_academic,
											tbl.date_teacher,
											tbl.date_director,
											tbl.date_create,
											tbl.directorid,
											tbl.teacherid
											FROM
											sch_studend_com_ann_aca_iep AS tbl
										WHERE 1=1
										AND student_id='".$studentid."'
										AND schoolid = '".$schoolid."'
										AND schlevelid ='".$schlavelid."'
										AND yearid ='".$yearid."'
										AND gradelevelid ='".$gradeid."'
										AND classid ='".$classid."'
										");
			$arr_re = array();
			if($select_sql->num_rows() > 0){
				foreach($select_sql->result() as $row_s){
					$date_academic = $this->green->convertSQLDate($row_s->date_academic);
					$date_teacher  = $this->green->convertSQLDate($row_s->date_teacher);
					$date_director = $this->green->convertSQLDate($row_s->date_director);

					$arr_re = array("student_id"=>$row_s->student_id,
									"schoolid"=>$row_s->schoolid,
									"schlevelid"=>$row_s->schlevelid,
									"yearid"=>$row_s->yearid,
									"gradelevelid"=>$row_s->gradelevelid,
									"classid"=>$row_s->classid,
									"command_teacher"=>$row_s->command_teacher,
									"academicid"=>$row_s->academicid,
									"userid"=>$row_s->userid,
									"date_academic"=>isset($date_academic)?$date_academic:date('d-m-Y'),
									"date_teacher"=>isset($date_teacher)?$date_teacher:date('d-m-Y'),
									"date_director"=>isset($date_director)?$date_director:date('d-m-Y'),
									"directorid"=>$row_s->directorid,
									"teacherid"=>$row_s->teacherid,
									"commendid"=>$row_s->id
									);
				}
			}
			if(count($arr_re)>0){
				$arr_show["show_data"] = $arr_re;
			}else{
				$arr_show["show_data"] = 10;
			}
			
			header("Content-type:text/x-json");
			echo json_encode($arr_show);
		}
		function save_data(){
			$studentid = $this->input->post("studentid");
			$classid   = $this->input->post("classid");
			$schoolid  = $this->input->post("schoolid");
			$schlavelid= $this->input->post("schlavelid");
			$yearid    = $this->input->post("yearid");
			$gradeid   = $this->input->post("gradeid");
			$command   = $this->input->post("command");
			$date_director = $this->input->post("date_director");
			$date_academic = $this->input->post("date_academic");
			$date_teacher  = $this->input->post("date_teacher");
			$directorid    = $this->input->post("directorid");
			$academicid    = $this->input->post("academicid");
			$teacherid     = $this->input->post("teacherid");
			$commentid     = $this->input->post("commentid");
			$userid = $this->session->userdata('userid');
			if($commentid == ""){
					$this->db->query("INSERT INTO sch_studend_com_ann_aca_iep
											SET student_id='".$studentid."',
												schoolid = '".$schoolid."',
												schlevelid ='".$schlavelid."',
												yearid ='".$yearid."',
												gradelevelid ='".$gradeid."',
												classid ='".$classid."',
												command_teacher ='".$command."',
												academicid ='".$academicid."',
												userid ='".$userid."',
												teacherid ='".$teacherid."',
												directorid ='".$directorid."',
												date_academic='".$this->green->formatSQLDate($date_academic)."',
												date_teacher ='".$this->green->formatSQLDate($date_teacher)."',
												date_director='".$this->green->formatSQLDate($date_director)."',
												date_create  ='".date('Y-d-m')."'
											");
			}else{
				$this->db->query("UPDATE sch_studend_com_ann_aca_iep
											SET command_teacher ='".$command."',
												academicid ='".$academicid."',
												userid ='".$userid."',
												teacherid ='".$teacherid."',
												directorid ='".$directorid."',
												date_academic='".$this->green->formatSQLDate($date_academic)."',
												date_teacher ='".$this->green->formatSQLDate($date_teacher)."',
												date_director='".$this->green->formatSQLDate($date_director)."',
												date_create  ='".date('Y-d-m')."'
											WHERE 1=1
											AND student_id='".$studentid."'
											AND schoolid = '".$schoolid."'
											AND schlevelid ='".$schlavelid."'
											AND yearid ='".$yearid."'
											AND gradelevelid ='".$gradeid."'
											AND classid ='".$classid."'
										");
			}
			echo "OK";
		}
		function  show_report_print(){
			$this->load->view('header');
			$this->load->view("reports/iep_reports/v_annual_academic_report.php");
			$this->load->view('footer');
		}
		function show_page_all(){
			$this->load->view('header');
			$this->load->view("reports/iep_reports/annual_academic_all.php");
			$this->load->view('footer');
		}
	}

?>