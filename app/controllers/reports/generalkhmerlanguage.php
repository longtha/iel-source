<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class generalkhmerlanguage extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->lang->load('generalkhmerlanguage', 'khmer');	
		$this->load->model('reports/Generalkhmerlanguagemodel','mode');		
		$this->load->model('school/schoolinformodel', 'school');
		$this->load->model('school/program', 'program');
		$this->load->model('school/schoollevelmodel', 'school_level');
		$this->load->model('student/m_evaluate_trans_kgp', 'student');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('reports/generalkhmerlanguage/index');
		$this->load->view('footer');
	}

	//get class data---------------------------------------------
	public function get_class_data(){
		$program_id = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$grade_level_id = $this->input->post('grade_level_id');
		$c_opt = '<option value=""></option>';
		$get_class_data = $this->mode->getClassData('', $school_level_id, $grade_level_id, '', '');
		//getClassData($class_id,$program_id, $school_level_id, $grade_level_id, $adcademic_year_id);
		if($get_class_data->num_rows() > 0){
			foreach($get_class_data->result() as $r_c){
				$c_opt .= '<option value="'. $r_c->classid .'">'. $r_c->class_name .'</option>';
			}
		}
				
		echo $c_opt;
	}


	//get student data-----------------------------------------
		public function get_student_data()
		{
			$class_id = $this->input->post('class_id');
			$schoolid = $this->input->post('schoolid');
			$programid = $this->input->post('programid');
			$schlavelid = $this->input->post('schlavelid');
			$yearid = $this->input->post('yearid');
			$gradlavelid = $this->input->post('gradlavelid');
			$where = "";
			// if($class_id != ""){
			// 	$where .= " AND sl.class_id = ". $class_id;
			// }
			$where .= " AND schoolid=".$schoolid." AND schlevelid = ".$schlavelid." AND programid=".$programid." AND year=".$yearid." AND grade_levelid=".$gradlavelid." AND classid = ". $class_id;
			$s_opt = '<option value=""></option>';

			$i = 1;
			$get_student_data = $this->mode->getStudent($where);
			if(count($get_student_data) > 0){
				foreach($get_student_data as $r_s){
					$s_opt .= '<option value="'. $r_s->studentid .'">'. $i .'. '. $r_s->last_name .' '. $r_s->first_name .'</option>';
					$i++;
				}
			}
						
			echo $s_opt;
		}
	//for primary school----------------------------------------
	public function primary_monthly_report_average()
	{
		$school_id       = $this->input->post('school_id');
		$program_id      = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id  = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$class_id        = $this->input->post('class_id');
		$student_id      = $this->input->post('student_id');
		$arr_m           = $this->input->post('month');
		$type_of_report  = $this->input->post('type_of_report');
		$sql_year      = $this->db->query("SELECT
												yearid,
												DATE_FORMAT(from_date,'%Y') AS from_date,
												DATE_FORMAT(to_date,'%Y') AS to_date
											FROM sch_school_year
											WHERE yearid='".$adcademic_year_id."'");
		$year_title = "";
		if($sql_year->num_rows()> 0){
			foreach($sql_year->result() as $k_y){
				$year_title = $k_y->from_date." - ".$k_y->to_date;
			}
		}

		$month_data = array();
		$month_data = [ 
						'មករា', 
						'កុម្ភៈ', 
						'មិនា', 
						'មេសា-ឧសភា', 
						'មិថុនា', 
						'កក្កដា', 
						'សីហា', 
						'កញ្ញា', 
						'តុលា', 
						'វិច្ឆិកា', 
						'ធ្នូ'
					];
		$show_m     = "";
		$tital_month = "";
		$total_all_month=1;
		if(count($arr_m)>0){
			foreach($arr_m as $key =>$month){
				$show_m.= "<th class='class_format_fontBattambang'>".$month_data[$month-1]."</th>";	
				$tital_month.= "&nbsp;".$month_data[$month-1];
				$total_all_month++;
			}
		}
		if($this->session->userdata('match_con_posid')!='stu'){
			$sql_average = $this->db->query("SELECT student_id,average_score,exam_monthly 
														FROM sch_subject_score_monthly_kgp_multi 
														WHERE 1=1
														AND program_id='".$program_id."'
														AND school_id='".$school_id."'
														AND school_level_id='".$school_level_id."'
														AND adcademic_year_id='".$adcademic_year_id."'
														AND grade_level_id='".$grade_level_id."'
														AND class_id='".$class_id."' ORDER BY exam_monthly DESC");
			
			$student_inf = $this->db->query("SELECT
												v_student_enroment.student_num,
												v_student_enroment.pass_mobile,
												v_student_enroment.first_name,
												v_student_enroment.last_name,
												CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
												v_student_enroment.gender,
												v_student_enroment.phone1,
												v_student_enroment.class_name,
												v_student_enroment.studentid,
												v_student_enroment.schoolid,
												v_student_enroment.`year`,
												v_student_enroment.classid,
												v_student_enroment.schlevelid,
												v_student_enroment.rangelevelid,
												v_student_enroment.feetypeid,
												v_student_enroment.programid,
												v_student_enroment.sch_level,
												v_student_enroment.rangelevelname,
												v_student_enroment.program,
												v_student_enroment.sch_year,
												v_student_enroment.grade_levelid,
												v_student_enroment.from_date,
												v_student_enroment.to_date
												FROM
												v_student_enroment
												WHERE 1=1 
												AND schoolid='".$school_id."' 
												AND programid='".$program_id."' 
												AND schlevelid='".$school_level_id."'
												AND grade_levelid='".$grade_level_id."'
												AND classid='".$class_id."'
												AND year='".$adcademic_year_id."'
												ORDER BY studentid ASC");
		}else{
			$sql_average = $this->db->query("SELECT student_id,average_score,exam_monthly 
														FROM sch_subject_score_monthly_kgp_multi 
														WHERE 1=1
														AND student_id='".($this->session->userdata('emp_id'))."'
														AND program_id='".$program_id."'
														AND school_id='".$school_id."'
														AND school_level_id='".$school_level_id."'
														AND adcademic_year_id='".$adcademic_year_id."'
														AND grade_level_id='".$grade_level_id."'
														AND class_id='".$class_id."' ORDER BY exam_monthly DESC");
			
			$student_inf = $this->db->query("SELECT
												v_student_enroment.student_num,
												v_student_enroment.pass_mobile,
												v_student_enroment.first_name,
												v_student_enroment.last_name,
												CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
												v_student_enroment.gender,
												v_student_enroment.phone1,
												v_student_enroment.class_name,
												v_student_enroment.studentid,
												v_student_enroment.schoolid,
												v_student_enroment.`year`,
												v_student_enroment.classid,
												v_student_enroment.schlevelid,
												v_student_enroment.rangelevelid,
												v_student_enroment.feetypeid,
												v_student_enroment.programid,
												v_student_enroment.sch_level,
												v_student_enroment.rangelevelname,
												v_student_enroment.program,
												v_student_enroment.sch_year,
												v_student_enroment.grade_levelid,
												v_student_enroment.from_date,
												v_student_enroment.to_date
												FROM
												v_student_enroment
												WHERE 1=1 
												AND studentid='".($this->session->userdata('emp_id'))."'
												AND schoolid='".$school_id."' 
												AND programid='".$program_id."' 
												AND schlevelid='".$school_level_id."'
												AND grade_levelid='".$grade_level_id."'
												AND classid='".$class_id."'
												AND year='".$adcademic_year_id."' 
												ORDER BY studentid ASC");
		}
		$arr_average = array();
		if($sql_average->num_rows()>0){
			foreach($sql_average->result() as $avg){
				$arr_average[$avg->student_id][$avg->exam_monthly] =  $avg->average_score;
			}
		}
		$data = array();
		$data['student_inf'] = $student_inf;
		$data['th_month']    = $show_m;
		$data['title_m']     = $tital_month;
		$data['month']       = $arr_m;
		$data['total_all_month'] = $total_all_month;
		$data['average']     = $arr_average;
		$data['year']        = $year_title;
		$this->load->view("reports/generalkhmerlanguage/primaryschool/primary_monthly_report_average",$data);
	}
	public function primary_semester_report_average()
	{
		$school_id       = $this->input->post('school_id');
		$program_id      = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id  = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$class_id        = $this->input->post('class_id');
		$student_id      = $this->input->post('student_id');
		$arr_m           = $this->input->post('month');
		$semester        = $this->input->post('semester');
		$type_of_report  = $this->input->post('type_of_report');
		$where = "";
		$where_log_main="";
		$where_log_sub="";

		if($this->session->userdata('match_con_posid')!='stu'){
			if($student_id != ""){
				$where.= " AND studentid='".$student_id."'";
			}
		}else{
			$where_log_main.=" AND v_student_enroment.studentid='".($this->session->userdata('emp_id'))."'";
			$where_log_sub.=" AND student_id='".($this->session->userdata('emp_id'))."'";
		}
		$semster_name   = $this->db->query("SELECT semester FROM sch_school_semester 
															WHERE 1=1 
															AND programid=1 
															AND schoolid=1
															AND schlevelid='".$school_level_id."'
															AND yearid='".$adcademic_year_id."'
															AND semesterid='".$semester."'");
		$show_sem = "";
		if($semster_name->num_rows() > 0){
			foreach($semster_name->result() as $m){
				$show_sem = $m->semester;
			}
		}
		$sql_year      = $this->db->query("SELECT
												yearid,
												DATE_FORMAT(from_date,'%Y') AS from_date,
												DATE_FORMAT(to_date,'%Y') AS to_date
											FROM sch_school_year
											WHERE yearid='".$adcademic_year_id."'");
		$year_title = "";
		if($sql_year->num_rows()> 0){
			foreach($sql_year->result() as $k_y){
				$year_title = $k_y->from_date." - ".$k_y->to_date;
			}
		}
		
		$month_data = array();
		$month_data = [
						'មករា',
						'កុម្ភៈ', 
						'មិនា', 
						'មេសា-ឧសភា', 
						'មិថុនា', 
						'កក្កដា', 
						'សីហា', 
						'កញ្ញា', 
						'តុលា', 
						'វិច្ឆិកា', 
						'ធ្នូ'
						];
		$show_m     = "";
		$tital_month = "";
		$total_all_month=1;
		if(count($arr_m)>0){
			foreach($arr_m as $month){
				$show_m.= "<th>".$month_data[$month-1]."</th>";
				$tital_month.= "&nbsp;".$month_data[$month-1];
				$total_all_month++;
			}
		}
		$sql_average = $this->db->query("SELECT student_id,average_score,exam_monthly 
													FROM sch_subject_score_monthly_kgp_multi 
													WHERE 1=1 {$where_log_sub}
													AND program_id='".$program_id."'
													AND school_id='".$school_id."'
													AND school_level_id='".$school_level_id."'
													AND adcademic_year_id='".$adcademic_year_id."'
													AND grade_level_id='".$grade_level_id."'
													AND class_id='".$class_id."' ");
		$arr_month_avg = array();
		if($sql_average->num_rows()>0){
			foreach($sql_average->result() as $avg){
				$arr_month_avg[$avg->student_id][$avg->exam_monthly] =  $avg->average_score;
			}
		}
		$sql_score_sm = $this->db->query("SELECT student_id,total_score,average_score,get_monthly FROM sch_subject_score_semester_kgp_multi
										WHERE 1=1 {$where_log_sub} AND program_id='".$program_id."' 
										AND school_level_id='".$school_level_id."'
										AND grade_level_id='".$grade_level_id."'
										AND class_id='".$class_id."'
										AND school_id='".$school_id."'
										AND adcademic_year_id='".$adcademic_year_id."'
										AND exam_semester='".$semester."'
										");
		$arr_score_sem = array();
		$count_month =  array();
		if($sql_score_sm->num_rows() > 0){
			foreach($sql_score_sm->result() as $row_sm){
				$monthly = explode(",",$row_sm->get_monthly);
				$arr_score_sem['sem_total'][$row_sm->student_id] = $row_sm->total_score;
				$arr_score_sem['sem_avg'][$row_sm->student_id] = $row_sm->average_score;
				if(count($monthly) > 0){
					foreach($monthly as $km){
						$total_month = 0;
						if(isset($arr_month_avg[$row_sm->student_id][$km])){
							$total_month = $arr_month_avg[$row_sm->student_id][$km];
						}
						if(isset($arr_score_sem['month'][$row_sm->student_id])){
							$arr_score_sem['month'][$row_sm->student_id] = $arr_score_sem['month'][$row_sm->student_id]+$total_month;
						}else{
							$arr_score_sem['month'][$row_sm->student_id] = $total_month;
						}
						if(isset($count_month[$row_sm->student_id])){
							$count_month[$row_sm->student_id] = $count_month[$row_sm->student_id]+1;
						}else{
							$count_month[$row_sm->student_id] = 1;
						}
					}
				}
			}
		}
		$student_inf = $this->db->query("SELECT
											v_student_enroment.student_num,
											v_student_enroment.first_name,
											v_student_enroment.last_name,
											CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
											v_student_enroment.gender,
											v_student_enroment.class_name,
											v_student_enroment.studentid,
											v_student_enroment.schoolid,
											v_student_enroment.`year`,
											v_student_enroment.classid,
											v_student_enroment.schlevelid,
											v_student_enroment.programid,
											v_student_enroment.sch_level,
											v_student_enroment.program,
											v_student_enroment.sch_year,
											v_student_enroment.grade_levelid,
											v_student_enroment.from_date,
											v_student_enroment.to_date
											FROM
											v_student_enroment
											WHERE 1=1 {$where} {$where_log_main}
											AND schoolid='".$school_id."' 
											AND programid='".$program_id."' 
											AND schlevelid='".$school_level_id."'
											AND grade_levelid='".$grade_level_id."'
											AND classid='".$class_id."'
											AND year='".$adcademic_year_id."'
											ORDER BY studentid ASC");
		$data = array();
		$data['student_inf'] = $student_inf;
		$data['th_month']    = $show_m;
		$data['total_all_month'] = $total_all_month;
		$data['title_semester'] = $show_sem;
		$data['count_m']      = $count_month;
		$data['avg_sem']     = $arr_score_sem;
		$data['year']        = $year_title;
		$this->load->view("reports/generalkhmerlanguage/primaryschool/primary_semester_report_average",$data);
	}
	public function primary_semester_report_ranking(){
		$school_id       = $this->input->post('school_id');
		$program_id      = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id  = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$class_id        = $this->input->post('class_id');
		$student_id      = $this->input->post('student_id');
		$arr_m           = $this->input->post('month');
		$semester        = $this->input->post('semester');
		$type_of_report  = $this->input->post('type_of_report');
		$where = "";
		$where_log_main="";
		$where_log_sub="";

		if($this->session->userdata('match_con_posid')!='stu'){
			if($student_id != ""){
				$where.= " AND studentid='".$student_id."'";
			}
		}else{
			$where_log_main.=" AND v_student_enroment.studentid='".($this->session->userdata('emp_id'))."'";
			$where_log_sub.=" AND student_id='".($this->session->userdata('emp_id'))."'";
		}
		$semster_name   = $this->db->query("SELECT semester FROM sch_school_semester 
															WHERE 1=1 
															AND programid=1 
															AND schoolid=1
															AND schlevelid='".$school_level_id."'
															AND yearid='".$adcademic_year_id."'
															AND semesterid='".$semester."'");
		$show_sem = "";
		if($semster_name->num_rows() > 0){
			foreach($semster_name->result() as $m){
				$show_sem = $m->semester;
			}
		}
		$sql_score_sm = $this->db->query("SELECT student_id,total_score,average_score,get_monthly 
										FROM sch_subject_score_semester_kgp_multi
										WHERE 1=1 {$where_log_sub} AND program_id='".$program_id."' 
										AND school_level_id='".$school_level_id."'
										AND grade_level_id='".$grade_level_id."'
										AND class_id='".$class_id."'
										AND school_id='".$school_id."'
										AND adcademic_year_id='".$adcademic_year_id."'
										AND exam_semester='".$semester."'
										ORDER BY total_score DESC
										");
		$arr_score_sem = array();
		$arr_rank      = array();
		$show_rank     = array();
		$rank1 = "";
		$rank  = "";
		$ii = 1;
		if($sql_score_sm->num_rows() > 0){
			foreach($sql_score_sm->result() as $row_sm){
				//$monthly = explode(",",$row_sm->get_monthly);
				$arr_score_sem['sem_total'][$row_sm->student_id] = $row_sm->total_score;
				$arr_score_sem['sem_avg'][$row_sm->student_id] = $row_sm->average_score;
				if(isset($arr_rank) AND in_array($row_sm->total_score,$arr_rank)){
					$rank1=$rank;
				}else{
					$rank = $ii;
					$rank1= $ii;
				}
				$ii++;
				$show_rank[$row_sm->student_id] = $rank1;
				$arr_rank[] = $row_sm->total_score;
			}
		}
		$sql_mention  = $this->db->query("SELECT
												sch_score_mention_gep.menid,
												sch_score_mention_gep.mention_kh,
												sch_score_mention_gep.schlevelid,
												sch_score_mention_gep.min_score,
												sch_score_mention_gep.max_score
												FROM
												sch_score_mention_gep
												WHERE schlevelid='".$school_level_id."'");
		$arr_m = array();
		if($sql_mention->num_rows() > 0){
			foreach($sql_mention->result() as $r_mention){
				$arr_m[] = array("Max_m"=>$r_mention->max_score,"Min_m"=>$r_mention->min_score,"Mention"=>$r_mention->mention_kh);
			}
		}
		$student_inf = $this->db->query("SELECT
											v_student_enroment.student_num,
											v_student_enroment.first_name,
											v_student_enroment.last_name,
											CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
											v_student_enroment.gender,
											v_student_enroment.class_name,
											v_student_enroment.studentid,
											v_student_enroment.schoolid,
											v_student_enroment.`year`,
											v_student_enroment.classid,
											v_student_enroment.schlevelid,
											v_student_enroment.programid,
											v_student_enroment.sch_level,
											v_student_enroment.program,
											v_student_enroment.sch_year,
											v_student_enroment.grade_levelid,
											DATE_FORMAT(v_student_enroment.from_date,'%Y') AS from_date,
											DATE_FORMAT(v_student_enroment.to_date,'%Y') AS to_date
											FROM
											v_student_enroment
											WHERE 1=1 {$where} {$where_log_main}
											AND schoolid='".$school_id."' 
											AND programid='".$program_id."' 
											AND schlevelid='".$school_level_id."'
											AND grade_levelid='".$grade_level_id."'
											AND classid='".$class_id."'
											AND year='".$adcademic_year_id."'
											ORDER BY studentid ASC");
		$arr_student_inf = array();
		if($student_inf->num_rows() > 0){
			foreach($student_inf->result() as $kstudent){
				$arr_student_inf[$kstudent->studentid] = array("student_name"=>$kstudent->studentname,
											"gender"=>$kstudent->gender,
											"classname"=>$kstudent->class_name,
											"year_name"=>$kstudent->from_date."-".$kstudent->to_date
											);
			}
		}
		$data = array();
		$data['student_inf'] = $arr_student_inf;
		$data['title_semester'] = $show_sem;
		$data['avg_sem']     = $arr_score_sem;
		$data['rank']        = $show_rank;
		$data['mention']     = $arr_m;
		$this->load->view("reports/generalkhmerlanguage/primaryschool/primary_semester_report_ranking",$data);
	}
	public function primary_monthly_report_ranking(){
		$program_id      = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id  = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$class_id        = $this->input->post('class_id');
		$student_id      = $this->input->post('student_id');
		$get_result_type = $this->input->post('get_result_type');
		$month_show      = $this->input->post('month');
		$data = array();
		$where = "";
		if($program_id != ""){
			$where .= " AND programid = ". $program_id;
		}
		if($school_level_id != ""){
			$where .= " AND schlevelid = ". $school_level_id;
		}
		if($adcademic_year_id != ""){
			$where .= " AND `year` = ". $adcademic_year_id;
		}
		if($grade_level_id != ""){
			$where .= " AND grade_levelid = ". $grade_level_id;
		}
		if($class_id != ""){
			$where .= " AND classid = ". $class_id;
		}
		if($this->session->userdata('match_con_posid')!='stu'){
			if($student_id != ""){
				$where .= " AND studentid = ". $student_id;
			}
		}
		if($get_result_type != ""){
			//$where .= " AND sl.exam_monthly = ". $get_result_type;
		}


		//get class name
		$class_name = array();
		//$get_class_data = $this->mode->getClassData($program_id, $school_level_id, $grade_level_id, $adcademic_year_id, $class_id);
		$get_class_data = $this->mode->getClassData('', $school_level_id, $grade_level_id, '', $class_id);
		if($get_class_data->num_rows() > 0){
			foreach($get_class_data->result() as $r_c){
				$class_name["class_name"] = $r_c->class_name;
			}
		}
		//get student and average score
		$stu_data = array(); 
		$get_student_info = $this->mode->getStudent($where);
		$avg_score = $this->mode->calculateAverageScoreMonthly($class_id, $grade_level_id, $adcademic_year_id, $school_level_id, $program_id, $month_show);
		
		$sql_mention  = $this->db->query("SELECT
												sch_score_mention_gep.menid,
												sch_score_mention_gep.mention_kh,
												sch_score_mention_gep.schlevelid,
												sch_score_mention_gep.min_score,
												sch_score_mention_gep.max_score
												FROM
												sch_score_mention_gep
												WHERE schlevelid='".$school_level_id."'");
		$arr_m = array();
		if($sql_mention->num_rows() > 0){
			foreach($sql_mention->result() as $r_mention){
				$arr_m[] = array("Max_m"=>$r_mention->max_score,"Min_m"=>$r_mention->min_score,"Mention"=>$r_mention->mention_kh);
			}
		}
		$sch_year = '';
		$sql_year = $this->db->query("SELECT 
										DATE_FORMAT(from_date,'%Y') AS from_date,
										DATE_FORMAT(to_date,'%Y') AS to_date
										FROM sch_school_year 
										WHERE 1=1 
										AND yearid='".$adcademic_year_id."'
										AND schoolid='1'
										AND schlevelid='".$school_level_id."'")->row();
		if(isset($sql_year->from_date) && isset($sql_year->to_date)){
			$sch_year = $sql_year->from_date."-".$sql_year->to_date;
		}


		$month_data = array('មករា', 'កុម្ភៈ', 'មិនា', 'មេសា-ឧសភា', 'មិថុនា', 'កក្កដា', 'សីហា', 'កញ្ញា', 'តុលា', 'វិច្ឆិកា', 'ធ្នូ');
		$data["class_name"] = $class_name;
		$data["month_name"] = isset($month_data[$month_show])?$month_data[$month_show]:"";
		$data["getStuData"] = $get_student_info;
		$data["avg"]      = isset($avg_score)?$avg_score:0;
		$data["mention"]  = $arr_m;
		$data["sch_year"] = $sch_year;
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_class_ranking', $data);
	}
	// Semester
	public function primary_semester_report_class_ranking(){
		$school_id       = $this->input->post('school_id');
		$program_id      = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id  = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$class_id        = $this->input->post('class_id');
		$student_id      = $this->input->post('student_id');
		$get_result_type = $this->input->post('get_result_type');
		$select_result_type = $this->input->post('select_result_type');
		$data = array();
		$where = "";
		
		$where .= " AND vse.programid = ".$program_id." 
					AND vse.schlevelid = ".$school_level_id ." 
					AND vse.`year` = ".$adcademic_year_id."
					AND vse.classid = ".$class_id;

		if($student_id != ""){
			$where .= " AND vse.studentid = ".$student_id;
		}


		//get class name
		$class_name = array();
		//$get_class_data = $this->mode->getClassData($program_id, $school_level_id, $grade_level_id, $adcademic_year_id, $class_id);
		$get_class_data = $this->mode->getClassData($program_id, $school_level_id, $grade_level_id, $adcademic_year_id, $class_id);
		if($get_class_data->num_rows() > 0){
			foreach($get_class_data->result() as $r_c){
				$class_name = $r_c->class_name;
				
			}
		}

		//get student and average score
		$stu_data = array(); 
		$get_student_info = $this->mode->getStudentInformationSemester($where);
		
		//$get_student_info = $this->student->getStudentInformation($where);
		$sch_year = '';

		$edinboro = 'ល្អ';

		if(count($get_student_info) > 0){
			foreach($get_student_info as $r_stu){
				$avg_score = $this->mode->calculateAverageScoreSemester($r_stu->studentid, $class_id, $grade_level_id, $adcademic_year_id, $school_level_id, $program_id, $get_result_type);
				$r_stu->{"row_avg"} = $avg_score;
				$sch_year = $r_stu->sch_year;				
				$stu_data[] = $r_stu;
			}			
		}
		$sql_by_m = $this->db->query("SELECT student_id,exam_monthly,SUM(average_score) AS total_avg 
										FROM sch_subject_score_monthly_kgp_multi
										WHERE 1=1 
										AND program_id = '".$program_id."'
										AND school_id  = '".$school_id."'
										AND school_level_id = '".$school_level_id."'
										AND adcademic_year_id = '".$adcademic_year_id."'
										AND grade_level_id = '".$grade_level_id."'
										AND class_id = '".$class_id."'
										GROUP BY student_id,exam_monthly");
		$sum_by_month = array();
		if($sql_by_m->num_rows() > 0){
			foreach($sql_by_m->result() as $row_m){
				$sum_by_month[$row_m->student_id][$row_m->exam_monthly] = $row_m->total_avg;
			}
		}
		
		$data["class_name"] = $class_name;
		$data["month_name"] = $get_result_type;
		$data["getStuData"] = $stu_data;
		$data["sch_year"] = $sch_year;
		$data["edinboro"] = $edinboro;
		$data["sum_month"] = $sum_by_month;
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_semester_report_class_ranking.php', $data);
	}
	public function primary_monthly_report_list(){
		$program_id = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id  = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$class_id   = $this->input->post('class_id');
		$student_id = $this->input->post('student_id');
		$school_id  = $this->input->post('school_id');
		$month  = $this->input->post('month');
		$where = "";
		if($student_id != ""){
			$where.= " AND studentid = '".$student_id."'";
		}
		if($this->session->userdata('match_con_posid')!='stu'){
			$student_inf = $this->db->query("SELECT
												v_student_enroment.student_num,
												v_student_enroment.pass_mobile,
												v_student_enroment.first_name,
												v_student_enroment.last_name,
												CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
												v_student_enroment.gender,
												v_student_enroment.class_name,
												v_student_enroment.studentid,
												v_student_enroment.schoolid,
												v_student_enroment.`year`,
												v_student_enroment.classid,
												v_student_enroment.schlevelid,
												v_student_enroment.programid,
												v_student_enroment.sch_level,
												v_student_enroment.program,
												v_student_enroment.sch_year,
												v_student_enroment.grade_levelid,
												v_student_enroment.from_date,
												v_student_enroment.to_date
												FROM
												v_student_enroment
												WHERE 1=1 {$where}
												AND programid='".$program_id."' 
												AND schlevelid='".$school_level_id."'
												AND grade_levelid='".$grade_level_id."'
												AND classid='".$class_id."'
												AND year='".$adcademic_year_id."'
												ORDER BY studentid ASC");

			$sql_score = $this->db->query("SELECT student_id,total_score
											FROM sch_subject_score_monthly_kgp_multi
											WHERE 1=1 
											AND program_id='".$program_id."' 
											AND school_level_id='".$school_level_id."'
											AND grade_level_id='".$grade_level_id."'
											AND class_id='".$class_id."'
											AND school_id='".$school_id."'
											AND adcademic_year_id='".$adcademic_year_id."'
											AND exam_monthly='".$month."'
											");
		}else{
			$student_inf = $this->db->query("SELECT
												v_student_enroment.student_num,
												v_student_enroment.pass_mobile,
												v_student_enroment.first_name,
												v_student_enroment.last_name,
												CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
												v_student_enroment.gender,
												v_student_enroment.class_name,
												v_student_enroment.studentid,
												v_student_enroment.schoolid,
												v_student_enroment.`year`,
												v_student_enroment.classid,
												v_student_enroment.schlevelid,
												v_student_enroment.programid,
												v_student_enroment.sch_level,
												v_student_enroment.program,
												v_student_enroment.sch_year,
												v_student_enroment.grade_levelid,
												v_student_enroment.from_date,
												v_student_enroment.to_date
												FROM
												v_student_enroment
												WHERE 1=1 
												AND studentid='".($this->session->userdata('emp_id'))."'
												AND programid='".$program_id."' 
												AND schlevelid='".$school_level_id."'
												AND grade_levelid='".$grade_level_id."'
												AND classid='".$class_id."'
												AND year='".$adcademic_year_id."'
												ORDER BY studentid ASC");

			$sql_score = $this->db->query("SELECT student_id,total_score
											FROM sch_subject_score_monthly_kgp_multi
											WHERE 1=1 AND student_id='".($this->session->userdata('emp_id'))."' 
											AND program_id='".$program_id."' 
											AND school_level_id='".$school_level_id."'
											AND grade_level_id='".$grade_level_id."'
											AND class_id='".$class_id."'
											AND school_id='".$school_id."'
											AND adcademic_year_id='".$adcademic_year_id."'
											AND exam_monthly='".$month."'
											");
		}
		$arr_score = array();
		if($sql_score->num_rows() > 0){
			foreach($sql_score->result() as $row_s){
					$arr_score[$row_s->student_id] = $row_s->total_score;
			}
		}
		$data = array();$month_data = array();
		$month_data = ['January', 'February', 'March', 'April-May', 'June', 'July', 'August', 'September', 'Octomber', 'November', 'December'];
		$data['result'] = $student_inf;
		$data['score_m'] = $arr_score;
		$data['monthly'] = $month_data[$month-1];
		$data['monthlyid'] = $month;
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_list.php',$data);
	}
	public function primary_smester_report_list(){
		$program_id      = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id  = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$class_id   = $this->input->post('class_id');
		$student_id = $this->input->post('student_id');
		$school_id  = $this->input->post('school_id');
		$semester   = $this->input->post('semester');
		$arr_monthly = $this->input->post('month');

		$where = "";
		$where_log_main="";
		$where_log_sub ="";
		if($this->session->userdata('match_con_posid')!='stu'){
			if($student_id != ""){
				$where = " AND studentid = '".$student_id."'";
			}
		}else{
			$where_log_main.=" AND v_student_enroment.studentid='".($this->session->userdata('emp_id'))."'";
			$where_log_sub =" AND student_id='".($this->session->userdata('emp_id'))."'";
		}
		$student_inf = $this->db->query("SELECT
											v_student_enroment.student_num,
											v_student_enroment.pass_mobile,
											v_student_enroment.first_name,
											v_student_enroment.last_name,
											CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
											v_student_enroment.gender,
											v_student_enroment.phone1,
											v_student_enroment.class_name,
											v_student_enroment.studentid,
											v_student_enroment.schoolid,
											v_student_enroment.`year`,
											v_student_enroment.classid,
											v_student_enroment.schlevelid,
											v_student_enroment.rangelevelid,
											v_student_enroment.feetypeid,
											v_student_enroment.programid,
											v_student_enroment.sch_level,
											v_student_enroment.rangelevelname,
											v_student_enroment.program,
											v_student_enroment.sch_year,
											v_student_enroment.grade_levelid,
											v_student_enroment.from_date,
											v_student_enroment.to_date
											FROM
											v_student_enroment
											WHERE 1=1 {$where} {$where_log_main}
											AND programid='".$program_id."' 
											AND schlevelid='".$school_level_id."'
											AND grade_levelid='".$grade_level_id."'
											AND classid='".$class_id."'
											AND year='".$adcademic_year_id."'
											ORDER BY studentid ASC");
		$arr_score = array();
		if(count($arr_monthly) > 0){
			foreach($arr_monthly as $r_m){
				$sql_score = $this->db->query("SELECT student_id,total_score,exam_monthly FROM sch_subject_score_monthly_kgp_multi
												WHERE 1=1 {$where_log_sub}
												AND program_id='".$program_id."' 
												AND school_level_id='".$school_level_id."'
												AND grade_level_id='".$grade_level_id."'
												AND class_id='".$class_id."'
												AND school_id='".$school_id."'
												AND adcademic_year_id='".$adcademic_year_id."'
												AND exam_monthly='".$r_m."'
												");
				
				if($sql_score->num_rows() > 0){
					foreach($sql_score->result() as $row_m){
						$arr_score[$row_m->student_id][$row_m->exam_monthly] = $row_m->total_score;
					}
				}		
			}
		}
		

		$sql_score_sm = $this->db->query("SELECT student_id,total_score,get_monthly FROM sch_subject_score_semester_kgp_multi
										WHERE 1=1 {$where_log_sub} AND program_id='".$program_id."' 
										AND school_level_id='".$school_level_id."'
										AND grade_level_id='".$grade_level_id."'
										AND class_id='".$class_id."'
										AND school_id='".$school_id."'
										AND adcademic_year_id='".$adcademic_year_id."'
										AND exam_semester='".$semester."'
										");
		$arr_score_sem = array();
		if($sql_score_sm->num_rows() > 0){
			foreach($sql_score_sm->result() as $row_sm){
				$monthly = explode(",",$row_sm->get_monthly);
				$arr_score_sem['sem'][$row_sm->student_id] = $row_sm->total_score;
				if(count($monthly) > 0){
					foreach($monthly as $km){
						$total_month = 0;
						if(isset($arr_score[$row_sm->student_id][$km])){
							$total_month = $arr_score[$row_sm->student_id][$km];
						}
						if(isset($arr_score_sem['month'][$row_sm->student_id])){
							$arr_score_sem['month'][$row_sm->student_id] = $arr_score_sem['month'][$row_sm->student_id]+$total_month;
						}else{
							$arr_score_sem['month'][$row_sm->student_id] = $total_month;
						}
					}
				}
			}
		}
		$data = array();
		$data['result'] = $student_inf;
		$data['score_m'] = $arr_score_sem;
		$data['semester'] = $semester;
		//$data['arr_semester'] = $arr_score_sem;
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_semester_report_list.php',$data);
	}
	public function primary_final_report_list(){
		$program_id      = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id  = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$class_id   = $this->input->post('class_id');
		$student_id = $this->input->post('student_id');
		$school_id  = $this->input->post('school_id');
		$semester   = $this->input->post('semester');

		$where = "";
		$where_log_main="";
		$where_log_sub ="";
		if($this->session->userdata('match_con_posid')!='stu'){
			if($student_id != ""){
				$where = " AND studentid = '".$student_id."'";
			}
		}else{
			$where_log_main.=" AND v_student_enroment.studentid='".($this->session->userdata('emp_id'))."'";
			$where_log_sub.=" AND student_id='".($this->session->userdata('emp_id'))."'";
		}

		$sql_month = $this->db->query("SELECT student_id,total_score,exam_monthly 
										FROM sch_subject_score_monthly_kgp_multi
										WHERE 1=1 {$where_log_sub}
										AND program_id='".$program_id."'
										AND school_id='".$school_id."'
										AND school_level_id='".$school_level_id."'
										AND adcademic_year_id='".$adcademic_year_id."'
										AND grade_level_id='".$grade_level_id."'
										AND class_id='".$class_id."'");
		$arr_score_month = array();
		if($sql_month->num_rows() > 0){
			foreach($sql_month->result() as $vmonth){
				$arr_score_month[$vmonth->student_id][$vmonth->exam_monthly] = $vmonth->total_score;
			}
		}
		$sql_semester = $this->db->query("SELECT 
												student_id,
												total_score,
												get_monthly,
												exam_semester
												FROM sch_subject_score_semester_kgp_multi 
												WHERE 1=1 {$where_log_sub}
												AND program_id='".$program_id."' 
												AND school_level_id='".$school_level_id."' 
												AND grade_level_id='".$grade_level_id."' 
												AND class_id='".$class_id."' 
												AND school_id='".$school_id."' 
												AND adcademic_year_id='".$adcademic_year_id."' 
												
												");
		$arr_score_semeter = array();
		if($sql_semester->num_rows() > 0){
			foreach($sql_semester->result() as $rsm){
				$total_semeter = $rsm->total_score;
				$get_m         = $rsm->get_monthly;
				$show_m        = explode(",", $get_m);
				$total_score_m = 0;
				$ii = 0;
				if(count($show_m) > 0){
					foreach($show_m as $index_m){
						if(isset($arr_score_month[$rsm->student_id][$index_m])){
							$total_score_m+=$arr_score_month[$rsm->student_id][$index_m];
						}
						$ii++;
					}
				}
				$result_m = round($total_score_m/$ii,2);
				$result_s = round(($total_semeter+$result_m/2),2);
				$arr_score_semeter[$rsm->exam_semester][$rsm->student_id] = $result_s;
			}
		}
		$student_inf = $this->db->query("SELECT
											v_student_enroment.student_num,
											v_student_enroment.pass_mobile,
											v_student_enroment.first_name,
											v_student_enroment.last_name,
											CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
											v_student_enroment.gender,
											v_student_enroment.class_name,
											v_student_enroment.studentid,
											v_student_enroment.schoolid,
											v_student_enroment.`year`,
											v_student_enroment.classid,
											v_student_enroment.schlevelid,
											v_student_enroment.programid,
											v_student_enroment.sch_level,
											v_student_enroment.program,
											v_student_enroment.sch_year,
											v_student_enroment.grade_levelid,
											v_student_enroment.from_date,
											v_student_enroment.to_date
											FROM
											v_student_enroment
											WHERE 1=1 {$where} {$where_log_main}
											AND programid='".$program_id."' 
											AND schlevelid='".$school_level_id."'
											AND grade_levelid='".$grade_level_id."'
											AND classid='".$class_id."'
											AND year='".$adcademic_year_id."'
											ORDER BY studentid ASC");
		$data = array();
		$data['result'] = $student_inf;
		$data['arr_semester'] = $semester;
		$data['total_score_s'] = $arr_score_semeter;
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_final_report_list',$data);
	}
	public function print_all_report_monthly(){
		$classid     = isset($_GET['classid'])?$_GET['classid']:"";
		$schoolid    = isset($_GET['schoolid'])?$_GET['schoolid']:"";
		$programid   = isset($_GET['programid'])?$_GET['programid']:"";
		$schlevelid  = isset($_GET['schlevelid'])?$_GET['schlevelid']:"";
		$yearid      = isset($_GET['yearid'])?$_GET['yearid']:"";
		$gradlavelid = isset($_GET['gradlavelid'])?$_GET['gradlavelid']:"";
		$type_monthly = isset($_GET['type_monthly'])?$_GET['type_monthly']:"";

		$student_inf1 = $this->db->query("SELECT
											v_student_enroment.student_num,
											v_student_enroment.pass_mobile,
											v_student_enroment.first_name,
											v_student_enroment.last_name,
											CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
											v_student_enroment.gender,
											v_student_enroment.class_name,
											v_student_enroment.studentid
											FROM
											v_student_enroment
											WHERE 1=1 
											AND programid='".$programid."' 
											AND schlevelid='".$schlevelid."'
											AND grade_levelid='".$gradlavelid."'
											AND classid='".$classid."'
											AND year='".$yearid."'
											ORDER BY studentid ASC");
		
		$this->load->view('header');
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_header_print');
		if($student_inf1->num_rows() > 0){
			foreach($student_inf1->result() as $row_rol1){
				$studentid = isset($row_rol1->studentid)?$row_rol1->studentid:"";
				$arr_all = array("classid"=>$classid,
								"schoolid"=>$schoolid,
								"programid"=>$programid,
								"schlevelid"=>$schlevelid,
								"yearid"=>$yearid,
								"gradlavelid"=>$gradlavelid,
								"type_monthly"=>$type_monthly,
								"student_id"=>$studentid
							);
				$get_data = $this->show_data_monthly($arr_all);
				$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_individual_ranking',$get_data);
			}
		}
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_footer_print');
		$this->load->view('footer');
	}
	public function print_all_report_semester(){
		$programid    = $_GET['programid'];
		$schlevelid   = isset($_GET['schlevelid'])?$_GET['schlevelid']:"";
		$gradlavelid  = isset($_GET['gradlavelid'])?$_GET['gradlavelid']:"";
		$yearid       = $_GET['yearid'];
		$classid      = $_GET['classid'];
		//$student_id      = $_GET['studentid'];
		$schoolid     = $_GET['schoolid'];
		$smesterid    = $_GET['semester'];

		$student_inf1 = $this->db->query("SELECT
											v_student_enroment.student_num,
											v_student_enroment.pass_mobile,
											v_student_enroment.first_name,
											v_student_enroment.last_name,
											CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
											v_student_enroment.gender,
											v_student_enroment.class_name,
											v_student_enroment.studentid
											FROM
											v_student_enroment
											WHERE 1=1 
											AND programid='".$programid."' 
											AND schlevelid='".$schlevelid."'
											AND grade_levelid='".$gradlavelid."'
											AND classid='".$classid."'
											AND year='".$yearid."'
											ORDER BY studentid ASC");
		
		$this->load->view('header');
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_header_print');
		if($student_inf1->num_rows() > 0){
			foreach($student_inf1->result() as $row_rol1){
				$studentid = isset($row_rol1->studentid)?$row_rol1->studentid:"";
				$semestername   = $this->db->query("SELECT semester FROM sch_school_semester WHERE 1=1 AND semesterid='".$smesterid."'")->row()->semester;
				$arr_all = array("classid"=>$classid,
								"schoolid"=>$schoolid,
								"programid"=>$programid,
								"schlevelid"=>$schlevelid,
								"yearid"=>$yearid,
								"gradlavelid"=>$gradlavelid,
								"smesterid"=>$smesterid,
								"student_id"=>$studentid,
								"semestername"=>$semestername
								);
				$get_data = $this->show_data_semester($arr_all);
				$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_semester_report_individual_ranking',$get_data);
			}
		}
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_footer_print');
		$this->load->view('footer');
	}
	public function primary_monthly_report_individual_ranking(){
		$program_id      = $_GET['programid'];
		$school_level_id = $_GET['schlavelid'];
		$grade_level_id  = $_GET['grandlavelid'];			
		$adcademic_year_id = $_GET['yearid'];
		$class_id        = $_GET['classid'];
		$student_id      = $_GET['studentid'];
		$schoolid        = $_GET['schoolid'];
		$get_result_type = $_GET['monthly'];
		$arr_all = array("classid"=>$class_id,
						"schoolid"=>$schoolid,
						"programid"=>$program_id,
						"schlevelid"=>$school_level_id,
						"yearid"=>$adcademic_year_id,
						"gradlavelid"=>$grade_level_id,
						"type_monthly"=>$get_result_type,
						"student_id"=>$student_id
						);
		$get_data = $this->show_data_monthly($arr_all);
		$this->load->view('header');
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_header_print');
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_individual_ranking',$get_data);
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_footer_print');
		$this->load->view('footer');
	}
	public function primary_semester_report_print(){
		$program_id      = $_GET['programid'];
		$school_level_id = $_GET['schlavelid'];
		$grade_level_id  = $_GET['grandlavelid'];			
		$adcademic_year_id = $_GET['yearid'];
		$class_id        = $_GET['classid'];
		$student_id      = $_GET['studentid'];
		$schoolid        = $_GET['schoolid'];
		$smesterid      = $_GET['semester'];
		$semestername   = $this->db->query("SELECT semester FROM sch_school_semester WHERE 1=1 AND semesterid='".$smesterid."'")->row()->semester;
		$arr_all = array("classid"=>$class_id,
						"schoolid"=>$schoolid,
						"programid"=>$program_id,
						"schlevelid"=>$school_level_id,
						"yearid"=>$adcademic_year_id,
						"gradlavelid"=>$grade_level_id,
						"smesterid"=>$smesterid,
						"student_id"=>$student_id,
						"semestername"=>$semestername
						);
		$get_data = $this->show_data_semester($arr_all);
		$this->load->view('header');
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_header_print');
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_semester_report_individual_ranking',$get_data);
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_footer_print');
		$this->load->view('footer');
	}
	public function primary_final_report_individual_ranking(){
		$program_id      = $_GET['programid'];
		$school_level_id = $_GET['schlavelid'];
		$grade_level_id  = $_GET['grandlavelid'];			
		$adcademic_year_id = $_GET['yearid'];
		$class_id        = $_GET['classid'];
		$student_id      = $_GET['studentid'];
		$schoolid        = $_GET['schoolid'];
		$smesterid1      = $_GET['semester0'];
		$smesterid2      = $_GET['semester1'];
		//$semestername   = $this->db->query("SELECT semester FROM sch_school_semester WHERE 1=1 AND semesterid='".$smesterid."'")->row()->semester;
		$arr_all = array("classid"=>$class_id,
						"schoolid"=>$schoolid,
						"programid"=>$program_id,
						"schlevelid"=>$school_level_id,
						"yearid"=>$adcademic_year_id,
						"gradlavelid"=>$grade_level_id,
						"student_id"=>$student_id,
						"smesterid1"=>$smesterid1,
						"smesterid2"=>$smesterid2
						);
		$get_data = $this->show_data_final($arr_all);
		$this->load->view('header');
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_header_print');
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_yearly_report_individual_ranking',$get_data);
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_footer_print');
		$this->load->view('footer');
	}
	public function show_data_final($get_array)
	{
		$schoolid        = "";
		$school_level_id = "";
		$adcademic_year_id = "";
		$grade_level_id    = "";
		$class_id          = "";
		$smesterid   = "";
		$student_id        = "";
		$program_id        = "";
		$arr_semester      = array();
		if(count($get_array) > 0){
			$schoolid        = $get_array['schoolid'];
			$school_level_id = $get_array['schlevelid'];
			$adcademic_year_id = $get_array['yearid'];
			$grade_level_id    = $get_array['gradlavelid'];
			$class_id          = $get_array['classid'];
			$student_id        = $get_array['student_id'];
			$program_id        = $get_array['programid'];
			$arr_semester['semester'] = array($get_array['smesterid1'],$get_array['smesterid2']);
		}
		
		$studentname="";
		$year = "";
		$gtotal    = 0;
		$total_eng = 0;
		$countSubject=1;
		$class_name = array();
		$total_semester_1 = 0;
		$total_semester_2 = 0;
		$Student = $this->db->query("SELECT DISTINCT
										sl.student_id,
										s.last_name,
										s.first_name,
									CONCAT(s.last_name_kh,'  ',s.first_name_kh) AS studentname,
										s.gender,
										sl.class_id,
										sc.class_name,
										y.sch_year,
										sl.program_id,
										sl.school_level_id,
										sl.adcademic_year_id,
										sl.grade_level_id
									FROM
										sch_subject_score_semester_kgp_multi AS sl
									INNER JOIN sch_student AS s ON sl.student_id = s.studentid
									INNER JOIN sch_class AS sc ON sl.class_id = sc.classid
									INNER JOIN sch_school_year AS y ON sl.adcademic_year_id = y.yearid
									WHERE 1 = 1 
									AND sl.school_level_id = '".$school_level_id."' 
									AND sl.grade_level_id = '".$grade_level_id."'
									AND sl.program_id = '".$program_id."'
									AND sl.adcademic_year_id = '".$adcademic_year_id."'
									AND sl.class_id = '".$class_id."'
									AND sl.student_id = '". $student_id."'
									");
		$row = $Student->row();
		//$year = $row->sch_year;
		$sqll_mention = $this->db->query("SELECT
											scm.mention,
											scm.menid,
											scm.mention_kh,
											ssm.max_score,
											ssm.minscrore,
											ssm.schlevelid,
											ssm.schoolid,
											ssm.subjectid,
											ssm.gradelevelid
											FROM
												sch_score_mention AS scm
											INNER JOIN sch_subject_mention AS ssm ON scm.menid = ssm.mentionid
											WHERE
												1 = 1
											AND ssm.schoolid = '".$schoolid."'
											AND ssm.schlevelid = '".$school_level_id."'
											AND ssm.gradelevelid = '".$grade_level_id."'");
		$arr_m = array();
		if($sqll_mention->num_rows() > 0){
			foreach($sqll_mention->result() as $row_m){
				$arr_m[$row_m->subjectid][$row_m->menid] = array("scoreMin"=>$row_m->minscrore,"scoreMax"=>$row_m->max_score,"mentEng"=>$row_m->mention,"mentKh"=>$row_m->mention_kh);
			}
		}
		
		// khmer & english part =====
		$q = $this->db->query("SELECT
									st.subject_type,
									st.main_type,
									st.subject_type_kh,
									st.subj_type_id
								FROM
									sch_subject_type AS st 
								WHERE st.main_type = '1' 
								AND st.schlevelid = '".$school_level_id."' 
								ORDER BY orders ASC");
		
		$tr = '';
		$semester_1 = isset($arr_semester['semester'][0])?$arr_semester['semester'][0]:"";
		$semester_2 = isset($arr_semester['semester'][1])?$arr_semester['semester'][1]:"";
		$avg_cal1 = 0;
		$avg_cal2 = 0;
		$colspan_h = "";
		$tbody    = '';
		$ch_group = 0;
		if ($q->num_rows() > 0) 
		{
			foreach ($q->result() as $r) {
				// subject ==========
				$q_ = $this->db->query("SELECT
											s.subjectid,
											s.subj_type_id,
											s.subject_kh,
											s.max_score,
											s.score_group,
											s.`subject`
										FROM
											sch_subject AS s 
										INNER JOIN sch_subject_detail ON s.subjectid = sch_subject_detail.subjectid
										WHERE s.subj_type_id = '".$r->subj_type_id."' AND grade_levelid='".$grade_level_id."'");
				$i = 0;
				$arr_group_subj = array();
				if ($q_->num_rows() > 0) {					
					foreach ($q_->result() as $r_) 
					{
						$is_group_subject = $r_->score_group;
						$sum_sub_sem = 0;
						$sql_smt_1 = "SELECT
											total_score,
											rank_subject
										FROM sch_subject_score_semester_detail_kgp_multi
										WHERE subject_mainid = '1'
										AND subject_groupid = '".$r_->subj_type_id."'
										AND subject_id = '".$r_->subjectid."'
										AND student_id = '".$student_id."'
										AND exam_semester = '".$semester_1."'
										AND program_id = '".$program_id."'
										AND school_level_id = '".$school_level_id."'
										AND class_id = '".$class_id."'
										AND adcademic_year_id = '".$adcademic_year_id."'
										AND grade_level_id = '".$grade_level_id."' 
										";
						$sql_smt_2 = "SELECT
											total_score,
											rank_subject
										FROM sch_subject_score_semester_detail_kgp_multi
										WHERE subject_mainid = '1'
										AND subject_groupid = '".$r_->subj_type_id."'
										AND subject_id = '".$r_->subjectid."'
										AND student_id = '".$student_id."'
										AND exam_semester = '".$semester_2."'
										AND program_id = '".$program_id."'
										AND school_level_id = '".$school_level_id."'
										AND class_id = '".$class_id."'
										AND adcademic_year_id = '".$adcademic_year_id."'
										AND grade_level_id = '".$grade_level_id."' 
										";
						$smt_1 = $this->db->query($sql_smt_1)->row();
						$smt_2 = $this->db->query($sql_smt_2)->row();
						$total_score_smt1 = isset($smt_1->total_score) ? $smt_1->total_score : 0;
						$total_score_smt2 = isset($smt_2->total_score) ? $smt_2->total_score : 0;
						$sum_sub_sem      = $total_score_smt1+$total_score_smt2;
						$total_score     = floor(($sum_sub_sem/2)*100)/100;	
						//$rank_subj   = isset($sm->rank_subject) ? $sm->rank_subject : 0;
						$rank_subj = "";
						$gtotal += $total_score - 0;

						$i++;
						/// =========== mention
						$show_mention_1 = "";
						if(isset($arr_m[$r_->subjectid])){
							foreach($arr_m[$r_->subjectid] as $v1){
								if($v1['scoreMin'] <= $total_score && $v1['scoreMax'] >= $total_score){
									$show_mention_1 = $v1['mentKh'];
								}
							}
						}
						/// =========== end mention
						if($is_group_subject == 1){
							$arr_group_subj[$r_->subj_type_id]['group'][$r_->subjectid] = '<td style="text-align:left;" class="class_format_fontBattambang">'.$r_->subject_kh.'</td>
																							<td style="text-align:center;" class="class_format_fontBattambang">'.number_format($total_score, 2).'/'.$r_->max_score.'</td>
																							<td style="text-align:center;" class="class_format_fontBattambang">'.$rank_subj.'</td>
																							<td style="text-align:center;" class="class_format_fontBattambang">'.$show_mention_1.'</td>';
							if(isset($arr_group_subj[$r_->subj_type_id]['count_row'])){
								$arr_group_subj[$r_->subj_type_id]['count_row'] = $arr_group_subj[$r_->subj_type_id]['count_row']+1;
							}else{
								$arr_group_subj[$r_->subj_type_id]['count_row'] = 1;
							}
							if(!isset($arr_group_subj[$r_->subj_type_id]['group_name'])){
								$arr_group_subj[$r_->subj_type_id]['group_name'] = $r->subject_type;
							}
						}else{
							$arr_group_subj[$r_->subj_type_id]['not_group'][$r_->subjectid] = array($r_->subject_kh,number_format($total_score, 2).'/'.$r_->max_score,$rank_subj,$show_mention_1,$r_->subject);
						}
						// $tr .= '<tr>';
						// if ($i == 1) {
						// 	$tr .=	'<td rowspan="'.($q_->num_rows()).'" style="vertical-align: middle;text-align:center;">'.$r->subject_type.'</td>';
						// }
						// $tr .=	'<td style="text-align:left;">'.$r_->subject_kh.'</td>
						// 		<td style="text-align:center;">'.$total_score.'</td>
						// 		<td style="text-align:center;">'.$rank_subj.'</td>
						// 		<td style="text-align:center;">'.$show_mention_1.'</td>
		 			// 		</tr>';

					}					
				}// for subject ====
				$tr_group = "";
				
				if(isset($arr_group_subj[$r->subj_type_id]))
				{
					if(isset($arr_group_subj[$r->subj_type_id]['group'])){
						$k = 0;
						$show_group = "";
						foreach($arr_group_subj[$r->subj_type_id]['group'] as $arr_subj){
							if($k == 0){
								$count_r_in_gr = 0;
								$group_name    = "";
								if(isset($arr_group_subj[$r->subj_type_id]['count_row'])){
									$count_r_in_gr = $arr_group_subj[$r->subj_type_id]['count_row'];
								}
								if(isset($arr_group_subj[$r->subj_type_id]['group_name'])){
									$group_name = $arr_group_subj[$r->subj_type_id]['group_name'];
								}
								$td_show = "";
								if($count_r_in_gr >1){
									$colspan_h = " colspan='2'";
									$ch_group+= $count_r_in_gr;
									$td_show = "<td rowspan='".$count_r_in_gr."'>".$group_name."</td>";
								}
								$tr_group.= "<tr>".$td_show.$arr_subj."</tr>";
							}else{
								$tr_group.= "<tr>".$arr_subj."</tr>";
							}
							$k++;
							
						}
					}
					if(isset($arr_group_subj[$r->subj_type_id]['not_group'])){
						foreach($arr_group_subj[$r->subj_type_id]['not_group'] as $sub_nogroup){
							$colspan_no_g = "";
							if($ch_group >1){
								$colspan_no_g = " colspan='2'";
							}
							$tr_group.='<tr><td style="text-align:left;" '.$colspan_no_g.' class="class_format_fontBattambang">'.$sub_nogroup[0].' '.$sub_nogroup[4].'</td>
								 		<td style="text-align:center;" class="class_format_fontBattambang">'.$sub_nogroup[1].'</td>
								 		<td style="text-align:center;" class="class_format_fontBattambang">'.($sub_nogroup[2]>0?$sub_nogroup[2]:0).'</td>
								 		<td style="text-align:center;" class="class_format_fontBattambang">'.$sub_nogroup[3].'</td>
								 		</tr>';
						}
					}
				}
				$tbody.=$tr_group;
			}
			$tr .= '<tr style="text-align: center;">
						<td '.$colspan_h.' style="vertical-align: middle;" class="class_format_fontBattambang">មុខវិជ្ជា Subjects</td>
						<td class="class_format_fontBattambang">ពិន្ទុ <br>​ Score</td>
						<td class="class_format_fontBattambang">ចំ.</td>
						<td class="class_format_fontBattambang">ការវាយតម្លៃ <br> Evaluation</td>					
					</tr>'.$tbody;	
		}// for sub group ====
		else {
			$tr .= '<tr><td colspan="4" style="text-align:center;">Data not foun .!</td></tr>';
		}

		// frorm to --------------------------
		$q = $this->db->query("SELECT
									st.subject_type,
									st.main_type,
									st.subject_type_kh,
									st.subj_type_id
								FROM
									sch_subject_type AS st 
								WHERE st.main_type = '0' AND st.schlevelid = '".$school_level_id."' 
								ORDER BY orders ASC");
		
		$trr = '';
		$colspan_h_1= "";
		$tbody_1    = '';
		$ch_group_1 = 0;
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $r) {
				// subject ==========
				$q_ = $this->db->query("SELECT
											s.subjectid,
											s.subj_type_id,
											s.subject_kh,
											s.max_score,
											s.score_group,
											s.`subject`
										FROM
											sch_subject AS s 
										INNER JOIN sch_subject_detail ON s.subjectid = sch_subject_detail.subjectid
										WHERE s.subj_type_id = '".$r->subj_type_id."' AND grade_levelid='".$grade_level_id."'");
				$j = 0;
				$arr_group_subj = array();
				if ($q_->num_rows() > 0) 
				{					
					foreach ($q_->result() as $r_) {
						$sum_sub_sem = 0;
						$is_group_subject = $r_->score_group;
						//$semester_1 = isset($arr_semester['semester'][0])?$arr_semester['semester'][0]:"";
						//$semester_2 = isset($arr_semester['semester'][1])?$arr_semester['semester'][1]:"";
						$sql_smt_1 = "SELECT
											total_score,
											rank_subject
										FROM sch_subject_score_semester_detail_kgp_multi
										WHERE subject_mainid = '2'
										AND subject_groupid = '".$r_->subj_type_id."'
										AND subject_id = '".$r_->subjectid."'
										AND student_id = '".$student_id."'
										AND exam_semester = '".$semester_1."'
										AND program_id = '".$program_id."'
										AND school_level_id = '".$school_level_id."'
										AND class_id = '".$class_id."'
										AND adcademic_year_id = '".$adcademic_year_id."'
										AND grade_level_id = '".$grade_level_id."' 
										";
						$sql_smt_2 = "SELECT
											total_score,
											rank_subject
										FROM sch_subject_score_semester_detail_kgp_multi
										WHERE subject_mainid = '2'
										AND subject_groupid = '".$r_->subj_type_id."'
										AND subject_id = '".$r_->subjectid."'
										AND student_id = '".$student_id."'
										AND exam_semester = '".$semester_2."'
										AND program_id = '".$program_id."'
										AND school_level_id = '".$school_level_id."'
										AND class_id = '".$class_id."'
										AND adcademic_year_id = '".$adcademic_year_id."'
										AND grade_level_id = '".$grade_level_id."' 
										";
						$smt_1 = $this->db->query($sql_smt_1)->row();
						$smt_2 = $this->db->query($sql_smt_2)->row();
						$total_score_smt1 = isset($smt_1->total_score) ? $smt_1->total_score : 0;
						$total_score_smt2 = isset($smt_2->total_score) ? $smt_2->total_score : 0;
						$sum_sub_sem      = $total_score_smt1+$total_score_smt2;
						$total_score     = floor(($sum_sub_sem/2)*100)/100;	
								
						$gtotal += $total_score - 0;
						/// =========== mention
						$rank_subj = "";
						$show_mention_2 = "";
						if(isset($arr_m[$r_->subjectid])){
							foreach($arr_m[$r_->subjectid] as $v2){
								if($v2['scoreMin'] <= $total_score && $v2['scoreMax'] >= $total_score){
									$show_mention_2 = $v2['mentKh'];
								}
							}
						}
						/// =========== end mention
						$j++;
						if($is_group_subject == 1){
							$arr_group_subj[$r_->subj_type_id]['group'][$r_->subjectid] = '<td style="text-align:left;"  class="class_format_fontBattambang">'.$r_->subject_kh.'</td>
																							<td style="text-align:center;"  class="class_format_fontBattambang">'.number_format($total_score, 2).'/'.$r_->max_score.'</td>
																							<td style="text-align:center;"  class="class_format_fontBattambang">'.$rank_subj.'</td>
																							<td style="text-align:center;"  class="class_format_fontBattambang">'.$show_mention_2.'</td>';
							if(isset($arr_group_subj[$r_->subj_type_id]['count_row'])){
								$arr_group_subj[$r_->subj_type_id]['count_row'] = $arr_group_subj[$r_->subj_type_id]['count_row']+1;
							}else{
								$arr_group_subj[$r_->subj_type_id]['count_row'] = 1;
							}
							if(!isset($arr_group_subj[$r_->subj_type_id]['group_name'])){
								$arr_group_subj[$r_->subj_type_id]['group_name'] = $r->subject_type;
							}
						}else{
							$arr_group_subj[$r_->subj_type_id]['not_group'][$r_->subjectid] = array($r_->subject_kh,number_format($total_score, 2).'/'.$r_->max_score,$rank_subj,$show_mention_2,$r_->subject);
						}
						// $trr .= '<tr>';
						// if ($j == 1) {
						// 	$trr .=	'<td rowspan="'.($q_->num_rows()).'" style="vertical-align: middle;text-align:center;">'.$r->subject_type.'</td>';
						// }
						
						// $trr .='<td style="text-align:left;">'.$r_->subject_kh.'</td>
						// 		  <td style="text-align:center;">'.$total_score.'</td>
						// 		  <td style="text-align:center;">'.$rank_subj.'</td>
						// 		  <td style="text-align:center;">'.$show_mention_2.'</td>
		 			// 			</tr>';
					}		
			
				}// for subject ====
				$tr_group = "";
				if(isset($arr_group_subj[$r->subj_type_id]))
				{
					if(isset($arr_group_subj[$r->subj_type_id]['group'])){
						$k = 0;
						$show_group = "";
						foreach($arr_group_subj[$r->subj_type_id]['group'] as $arr_subj){
							if($k == 0){
								$count_r_in_gr = 0;
								$group_name    = "";
								if(isset($arr_group_subj[$r->subj_type_id]['count_row'])){
									$count_r_in_gr = $arr_group_subj[$r->subj_type_id]['count_row'];
								}
								if(isset($arr_group_subj[$r->subj_type_id]['group_name'])){
									$group_name = $arr_group_subj[$r->subj_type_id]['group_name'];
								}
								$td_show = "";
								if($count_r_in_gr >1){
									$colspan_h_1 = " colspan='2'";
									$ch_group_1+= $count_r_in_gr;
									$td_show = "<td rowspan='".$count_r_in_gr."'>".$group_name."</td>";
								}
								$tr_group.= "<tr>".$td_show.$arr_subj."</tr>";
							}else{
								$tr_group.= "<tr>".$arr_subj."</tr>";
							}
							$k++;
							
						}
					}
					if(isset($arr_group_subj[$r->subj_type_id]['not_group']))
					{
						foreach($arr_group_subj[$r->subj_type_id]['not_group'] as $sub_nogroup){
							$colspan_no_g = "";
							if($ch_group_1 >1){
								$colspan_no_g = " colspan='2'";
							}
							$tr_group.='<tr><td style="text-align:left;" '.$colspan_no_g.'  class="class_format_fontBattambang">'.$sub_nogroup[0].' '.$sub_nogroup[4].'</td>
								 		<td style="text-align:center;"  class="class_format_fontBattambang">'.$sub_nogroup[1].'</td>
								 		<td style="text-align:center;"  class="class_format_fontBattambang">'.($sub_nogroup[2]>0?$sub_nogroup[2]:0).'</td>
								 		<td style="text-align:center;"  class="class_format_fontBattambang">'.$sub_nogroup[3].'</td>
								 		</tr>';
						}
					}
				}
				$tbody_1.=$tr_group;
			}
			$trr .= '<tr style="text-align: center;">
						<td '.$colspan_h_1.' style="vertical-align: middle;">មុខវិជ្ជា Subjects</td>
						<td>ពិន្ទុ <br>​ Score</td>
						<td>ចំ.</td>
						<td>ការវាយតម្លៃ <br> Evaluation</td>					
					</tr>'.$tbody_1;
		}// for sub group ====
		 else {
			$trr .= '<tr><td colspan="4" style="text-align:center;">Data not foun .!</td></tr>';
		}

		// fort ter ---------------
		$sqr = "SELECT DISTINCT
								m.absent,
								m.present,
								m.ranking_bysubj,
								m.average_score
							FROM
								sch_subject_score_monthly_kgp_multi AS m
							INNER JOIN sch_subject_score_monthly_detail_kgp_multi AS d ON m.student_id = d.student_id
							WHERE 1=1
							AND m.student_id = '".$student_id."'
							AND d.program_id = '".$program_id ."'
							AND d.school_level_id = '".$school_level_id."'
							AND d.class_id = '".$class_id."'
							AND d.adcademic_year_id = '".$adcademic_year_id."'
							AND d.grade_level_id = '".$grade_level_id."' ";
							
		$rows = $this->db->query($sqr)->row();					

		$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM sch_subject_score_monthly_kgp_multi AS sl  
										WHERE 1=1 
										AND sl.class_id = '".$class_id."' 
										AND sl.program_id = '".$program_id."' 
										AND sl.school_level_id = '".$school_level_id."' 
										AND sl.grade_level_id = '".$grade_level_id."' ")->row()->numrow;

		$sql_comment = $this->db->query("SELECT * FROM sch_studend_com_ann_aca_kgp 
										WHERE 1=1
										AND student_id = '".$student_id."'
										AND schoolid = '".$schoolid."'
										AND schlevelid = '".$school_level_id."'
										AND gradelevelid = '".$grade_level_id."'
										AND yearid = '".$adcademic_year_id."'
										AND classid = '".$class_id."'
										");
		
		$year = $this->db->query("SELECT
									DATE_FORMAT(from_date,'%Y') as from_date,
									DATE_FORMAT(to_date,'%Y') as to_date
								FROM sch_school_year WHERE 1=1 AND yearid='".$adcademic_year_id."'")->row();
		$show_y = "";
		if(isset($year->from_date) && isset($year->to_date)){
			$show_y = $year->from_date."-".$year->to_date;
		}
		$avg1 = $this->db->query("SELECT (average_score+average_monthly_score)/2 AS total_avg
									FROM sch_subject_score_semester_kgp_multi 
									WHERE 1=1 AND exam_semester='".$semester_1."'
									AND student_id='".$student_id."'
									AND school_id='".$schoolid."'
									AND program_id='".$program_id."'
									AND school_level_id='".$school_level_id."'
									AND adcademic_year_id='".$adcademic_year_id."'
									AND grade_level_id='".$grade_level_id."'
									AND class_id='".$class_id."'")->row();
		$avg2 = $this->db->query("SELECT (average_score+average_monthly_score)/2 AS total_avg
									FROM sch_subject_score_semester_kgp_multi 
									WHERE 1=1 
									AND exam_semester='".$semester_2."'
									AND student_id='".$student_id."'
									AND school_id='".$schoolid."'
									AND program_id='".$program_id."'
									AND school_level_id='".$school_level_id."'
									AND adcademic_year_id='".$adcademic_year_id."'
									AND grade_level_id='".$grade_level_id."'
									AND class_id='".$class_id."'")->row();
		$sql_ment_fin = $this->db->query("SELECT
											sch_score_mention_gep.menid,
											sch_score_mention_gep.mention,
											sch_score_mention_gep.mention_kh,
											sch_score_mention_gep.schlevelid,
											sch_score_mention_gep.grade,
											sch_score_mention_gep.min_score,
											sch_score_mention_gep.max_score
											FROM
											sch_score_mention_gep
											WHERE schlevelid='".$school_level_id."'");
		$arr_ment_f = array();
		if($sql_ment_fin->num_rows() > 0){
			foreach($sql_ment_fin->result() as $rfin_ment){
				$arr_ment_f[] = array("Mention_kh"=>$rfin_ment->mention_kh,"Max_s"=>$rfin_ment->max_score,"Min_s"=>$rfin_ment->min_score);
			}
		}
		$total_avg_s1 = isset($avg1->total_avg)?$avg1->total_avg:0;
		$total_avg_s2 = isset($avg2->total_avg)?$avg2->total_avg:0;
		$data2 = array();
		$data2["showyear"] = $show_y;
		$data2["total_row"] = $total_row;
		$data2["gtotal"] = $gtotal ;
		$data2["rows"] = $rows;
		$data2["trr"] = $trr;
		$data2["tr"]  = $tr;
		$data2["row"] = $row;
		$data2["avg_smt_1"] = round($total_avg_s1,2);
		$data2["avg_smt_2"] = round($total_avg_s2,2);
		$data2["mention_final"] = $arr_ment_f;
		$data2["comment"] = $sql_comment;
		return $data2;
	}// the end of final report
	public function primary_final_report_ranking(){
		$program_id      = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id  = $this->input->post('grade_level_id');		
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$class_id        = $this->input->post('class_id');
		$student_id      = $this->input->post('student_id');
		$schoolid        = $this->input->post('schoolid');
		$arr_semester    = $this->input->post('semester');
		
		$where = "";
		$where_log_main="";
		$where_log_sub ="";
		if($this->session->userdata('match_con_posid')!='stu'){
			if($student_id != ""){
				$where = " AND studentid = '".$student_id."'";
			}
		}else{
			$where_log_main.=" AND v_student_enroment.studentid='".($this->session->userdata('emp_id'))."'";
			$where_log_sub.=" AND student_id='".($this->session->userdata('emp_id'))."'";
		}
		//$arr_semester = array($smesterid1,$smesterid2);
		$arr_total_avg = array();
		if(count($arr_semester) > 0){
			foreach($arr_semester as $semsterid){
				$avg_rank = $this->db->query("SELECT student_id,(average_score+average_monthly_score) AS total_avg
												FROM sch_subject_score_semester_kgp_multi 
												WHERE 1=1 {$where_log_sub} AND exam_semester='".$semsterid."'
												AND school_id='".$schoolid."'
												AND program_id='".$program_id."'
												AND school_level_id='".$school_level_id."'
												AND adcademic_year_id='".$adcademic_year_id."'
												AND grade_level_id='".$grade_level_id."'
												AND class_id='".$class_id."'");
				if($avg_rank->num_rows() > 0){
					foreach($avg_rank->result() as $kavg){
						$total_avg = number_format($kavg->total_avg/2,2);
						if(isset($arr_total_avg[$kavg->student_id])){
							$arr_total_avg[$kavg->student_id] = $arr_total_avg[$kavg->student_id]+$total_avg;
						}else{
							$arr_total_avg[$kavg->student_id] = $total_avg;
						}
					}
				}
			}
		}
		$sql_mention  = $this->db->query("SELECT
												sch_score_mention_gep.menid,
												sch_score_mention_gep.mention_kh,
												sch_score_mention_gep.schlevelid,
												sch_score_mention_gep.min_score,
												sch_score_mention_gep.max_score
												FROM
												sch_score_mention_gep
												WHERE schlevelid='".$school_level_id."'");
		$arr_m = array();
		if($sql_mention->num_rows() > 0){
			foreach($sql_mention->result() as $r_mention){
				$arr_m[] = array("Max_m"=>$r_mention->max_score,"Min_m"=>$r_mention->min_score,"Mention"=>$r_mention->mention_kh);
			}
		}
		$student_inf = $this->db->query("SELECT
											v_student_enroment.student_num,
											v_student_enroment.first_name,
											v_student_enroment.last_name,
											CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
											v_student_enroment.gender,
											v_student_enroment.class_name,
											v_student_enroment.studentid,
											v_student_enroment.schoolid,
											v_student_enroment.`year`,
											v_student_enroment.classid,
											v_student_enroment.schlevelid,
											v_student_enroment.programid,
											v_student_enroment.sch_level,
											v_student_enroment.program,
											v_student_enroment.sch_year,
											v_student_enroment.grade_levelid,
											DATE_FORMAT(v_student_enroment.from_date,'%Y') AS from_date,
											DATE_FORMAT(v_student_enroment.to_date,'%Y') AS to_date
											FROM
											v_student_enroment
											WHERE 1=1 {$where} {$where_log_main}
											AND schoolid='".$schoolid."' 
											AND programid='".$program_id."' 
											AND schlevelid='".$school_level_id."'
											AND grade_levelid='".$grade_level_id."'
											AND classid='".$class_id."'
											AND year='".$adcademic_year_id."'
											ORDER BY studentid ASC");
		

		$arr_student_inf = array();
		if($student_inf->num_rows() > 0){
			foreach($student_inf->result() as $kstudent){
				$avg_st = 0;
				if(isset($arr_total_avg[$kstudent->studentid])){
					$avg_st = $arr_total_avg[$kstudent->studentid];
				}
				$arr_student_inf[$kstudent->studentid] = array("student_name"=>$kstudent->studentname,
																"gender"=>$kstudent->gender,
																"classname"=>$kstudent->class_name,
																"year_name"=>$kstudent->from_date."-".$kstudent->to_date,
																"avg"=>$avg_st
															);
			}
		}
		arsort($arr_total_avg);
		//var_dump($arr_total_avg);
		$data = array();
		$data['student_inf'] = $arr_student_inf;
		$data['mention']     = $arr_m;
		//$data['rank']        = $show_rank;
		$data['score_sort']  = $arr_total_avg;
		$this->load->view("reports/generalkhmerlanguage/primaryschool/primary_final_report_ranking",$data);
	}
	public function show_data_monthly($get_array)
	{
		$schoolid        = "";
		$school_level_id = "";
		$adcademic_year_id = "";
		$grade_level_id    = "";
		$class_id          = "";
		$get_result_type   = "";
		$student_id        = "";
		$program_id        = "";
		if(count($get_array) > 0){
			$schoolid        = $get_array['schoolid'];
			$school_level_id = $get_array['schlevelid'];
			$adcademic_year_id = $get_array['yearid'];
			$grade_level_id    = $get_array['gradlavelid'];
			$class_id          = $get_array['classid'];
			$get_result_type   = $get_array['type_monthly'];
			$student_id        = $get_array['student_id'];
			$program_id        = $get_array['programid'];
		}
		
		$where = "";
		
		$studentname="";
		$year = "";
		$gtotal=0;
		$countSubject=1;
		
		$class_name = array();
		
		$Student = $this->db->query("SELECT
										v_student_enroment.first_name,
										v_student_enroment.last_name,
										CONCAT(last_name_kh,'  ',first_name_kh) AS studentname,
										DATE_FORMAT(from_date,'%Y') as from_date,
										DATE_FORMAT(to_date,'%Y') as to_date,
										v_student_enroment.class_name
										FROM
										v_student_enroment
										WHERE 1=1 
										AND schoolid =1
										AND schlevelid='".$school_level_id."'
										AND grade_levelid='".$grade_level_id."'
										AND classid='".$class_id."'
										AND year='".$adcademic_year_id."'
										AND studentid='".$student_id."'");
		$row = $Student->row();
		//$year = $row->sch_year;
		$sql_mention = $this->db->query("SELECT
											sum.submenid,
											sum.schoolid,
											sum.schlevelid,
											sum.subjectid,
											sum.gradelevelid,
											sum.mentionid,
											sum.max_score,
											sum.minscrore,
											ssm.mention,
											ssm.mention_kh
											FROM
											sch_subject_mention AS sum
											INNER JOIN sch_score_mention AS ssm ON sum.mentionid = ssm.menid
											WHERE 1=1
											AND ssm.schlevelid ='".$school_level_id."'
											AND sum.gradelevelid ='".$grade_level_id."'
											AND sum.schoolid='1'
										");
		$arr_mention = array();
		if($sql_mention->num_rows() > 0){
			foreach($sql_mention->result() as $rm){
				$arr_mention[$rm->subjectid][] = array('max_score'=>$rm->max_score,'min_score'=>$rm->minscrore,'mention'=>$rm->mention_kh);
			}
		}
		
		// khmer & english part =====
		$q = $this->db->query("SELECT
									st.subject_type,
									st.main_type,
									st.subject_type_kh,
									st.subj_type_id
								FROM
									sch_subject_type AS st 
								WHERE st.main_type = '1' 
								AND st.schlevelid = '".$school_level_id."' 
								ORDER BY orders ASC");
		$tr = "";
		$colspan_h = "";
		$tbody = '';
		$ch_group = 0;
		if ($q->num_rows() > 0) 
		{
			foreach ($q->result() as $r) 
			{
				// subject ==========
				$q_ = $this->db->query("SELECT
											s.subjectid,
											s.subj_type_id,
											s.subject_kh,
											s.max_score,
											s.score_group,
											s.`subject`
										FROM
											sch_subject AS s 
										INNER JOIN sch_subject_detail ON s.subjectid = sch_subject_detail.subjectid
										WHERE s.subj_type_id = '".$r->subj_type_id."' AND grade_levelid='".$grade_level_id."' ORDER BY orders DESC");
				$arr_group_subj = array();
				$i = 0;
				if ($q_->num_rows() > 0) 
				{
					foreach ($q_->result() as $r_) 
					{
						$is_group_subject = $r_->score_group;
						$sql = "SELECT DISTINCT
										d.total_score
									FROM sch_subject_score_monthly_detail_kgp_multi AS d 
									WHERE d.subject_mainid = '1'
									AND d.subject_groupid = '".$r_->subj_type_id."'
									AND d.subject_id = '".$r_->subjectid."'
									AND d.student_id = '".$student_id."'
									AND d.program_id = '".$program_id ."'
									AND d.school_level_id = '".$school_level_id."'
									AND d.class_id = '".$class_id."'
									AND d.adcademic_year_id = '".$adcademic_year_id."'
									AND d.exam_monthly = '".$get_result_type."'
									AND d.grade_level_id = '".$grade_level_id."' ";
						$sm = $this->db->query($sql)->row();
						$countSubject++;

						$total_score = isset($sm->total_score) ? $sm->total_score : 0;
						$gtotal += $total_score - 0;
						$mention = "";
						if(isset($arr_mention[$r_->subjectid])){
							foreach($arr_mention[$r_->subjectid] as $rem){
								if($rem['max_score'] >= $total_score && $rem['min_score'] <= $total_score){
									$mention = $rem['mention'];
								}
							}
						}
						$i++;
						if($is_group_subject == 1){
							$arr_group_subj[$r_->subj_type_id]['group'][$r_->subjectid] = '<td style="text-align:left;">'.$r_->subject_kh.'</td>
																							<td style="text-align:center;">'.number_format($total_score, 2).'/'.$r_->max_score.'</td>
																							<td style="text-align:center;">'.$mention.'</td>';
							if(isset($arr_group_subj[$r_->subj_type_id]['count_row'])){
								$arr_group_subj[$r_->subj_type_id]['count_row'] = $arr_group_subj[$r_->subj_type_id]['count_row']+1;
							}else{
								$arr_group_subj[$r_->subj_type_id]['count_row'] = 1;
							}
							if(!isset($arr_group_subj[$r_->subj_type_id]['group_name'])){
								$arr_group_subj[$r_->subj_type_id]['group_name'] = $r->subject_type;
							}
						}else{
							$arr_group_subj[$r_->subj_type_id]['not_group'][$r_->subjectid] = array($r_->subject_kh,number_format($total_score, 2).'/'.$r_->max_score,$mention,$r_->subject);
						}
					}					
				}// for subject ====
				$tr_group = "";
				
				if(isset($arr_group_subj[$r->subj_type_id]))
				{
					if(isset($arr_group_subj[$r->subj_type_id]['group'])){
						$k = 0;
						$show_group = "";
						foreach($arr_group_subj[$r->subj_type_id]['group'] as $arr_subj){
							if($k == 0){
								$count_r_in_gr = 0;
								$group_name    = "";
								if(isset($arr_group_subj[$r->subj_type_id]['count_row'])){
									$count_r_in_gr = $arr_group_subj[$r->subj_type_id]['count_row'];
								}
								if(isset($arr_group_subj[$r->subj_type_id]['group_name'])){
									$group_name = $arr_group_subj[$r->subj_type_id]['group_name'];
								}
								$td_show = "";
								if($count_r_in_gr >1){
									$colspan_h = " colspan='2'";
									$ch_group+= $count_r_in_gr;
									$td_show = "<td rowspan='".$count_r_in_gr."'>".$group_name."</td>";
								}
								$tr_group.= "<tr>".$td_show.$arr_subj."</tr>";
							}else{
								$tr_group.= "<tr>".$arr_subj."</tr>";
							}
							$k++;
							
						}
					}
					if(isset($arr_group_subj[$r->subj_type_id]['not_group'])){
						foreach($arr_group_subj[$r->subj_type_id]['not_group'] as $sub_nogroup){
							$colspan_no_g = "";
							if($ch_group >1){
								$colspan_no_g = " colspan='2'";
							}
							$tr_group.='<tr><td style="text-align:left;" '.$colspan_no_g.'>'.$sub_nogroup[0].' '.$sub_nogroup[3].'</td>
								 		<td style="text-align:center;">'.$sub_nogroup[1].'</td>
								 		<td style="text-align:center;">'.$sub_nogroup[2].'</td></tr>';
						}
					}
					
				}

				$tbody.=$tr_group;
			}
			$tr .= '<tr style="text-align: center;">
						<th '.$colspan_h.' style="vertical-align: middle;">មុខវិជ្ជា Subjects</th>
						<th>ពិន្ទុ <br>​ Score</th>
						<th>ការវាយតម្លៃ <br> Evaluation</th>					
					</tr>'.$tbody;
		}// for sub group ====
		else {
			$tr .= '<tr><td colspan="4" style="text-align:center;">Data not foun .!</td></tr>';
		}

		// frorm to --------------------------
		$q = $this->db->query("SELECT
									st.subject_type,
									st.main_type,
									st.subject_type_kh,
									st.subj_type_id
								FROM
									sch_subject_type AS st 
								WHERE st.main_type = '0' AND st.schlevelid = '".$school_level_id."' 
								ORDER BY orders ASC");
		
		$trr = '';
		$colspan_h_1 = "";
		$tbody_1 = '';
		$ch_group_1 = 0;
		if ($q->num_rows() > 0) 
		{
			foreach ($q->result() as $r) {
				// subject ==========
				$q_ = $this->db->query("SELECT
											s.subjectid,
											s.subj_type_id,
											s.subject_kh,
											s.max_score,
											s.score_group,
											s.`subject`
										FROM
											sch_subject AS s 
										INNER JOIN sch_subject_detail ON s.subjectid = sch_subject_detail.subjectid
										WHERE s.subj_type_id = '".$r->subj_type_id."' AND grade_levelid='".$grade_level_id."'");
				$j = 0;
				// $gtotal=0;
				$arr_group_subj= array();
				if ($q_->num_rows() > 0) 
				{					
					foreach ($q_->result() as $r_) {
						$is_group_subject = $r_->score_group;
						$sql = "SELECT DISTINCT
										d.total_score
									FROM sch_subject_score_monthly_detail_kgp_multi AS d 
									WHERE d.subject_mainid = '0'
									AND d.subject_groupid = '".$r_->subj_type_id."'
									AND d.subject_id = '".$r_->subjectid."'
									AND d.student_id = '".$student_id."'
									AND d.program_id = '".$program_id ."'
									AND d.school_level_id = '".$school_level_id."'
									AND d.class_id = '".$class_id."'
									AND d.adcademic_year_id = '".$adcademic_year_id."'
									AND d.exam_monthly = '".$get_result_type."'
									AND d.grade_level_id = '".$grade_level_id."' ";
						// echo ($sql);
						$sm = $this->db->query($sql)->row();
						// $countSubject++;

						$total_score = isset($sm->total_score) ? $sm->total_score : 0;												
						$gtotal += $total_score - 0;

						$j++;
						if($is_group_subject == 1){
							$arr_group_subj[$r_->subj_type_id]['group'][$r_->subjectid] = '<td style="text-align:left;">'.$r_->subject_kh.'</td>
																							<td style="text-align:center;">'.number_format($total_score, 2).'/'.$r_->max_score.'</td>
																							<td style="text-align:center;">'.$mention.'</td>';
							if(isset($arr_group_subj[$r_->subj_type_id]['count_row'])){
								$arr_group_subj[$r_->subj_type_id]['count_row'] = $arr_group_subj[$r_->subj_type_id]['count_row']+1;
							}else{
								$arr_group_subj[$r_->subj_type_id]['count_row'] = 1;
							}
							if(!isset($arr_group_subj[$r_->subj_type_id]['group_name'])){
								$arr_group_subj[$r_->subj_type_id]['group_name'] = $r->subject_type;
							}
						}else{
							$arr_group_subj[$r_->subj_type_id]['not_group'][$r_->subjectid] = array($r_->subject_kh,number_format($total_score, 2).'/'.$r_->max_score,$mention,$r_->subject);
						}
						
					}		
				}// for subject ====
				$tr_group = "";
				//$ch_group = 0;
				if(isset($arr_group_subj[$r->subj_type_id]))
				{
					if(isset($arr_group_subj[$r->subj_type_id]['group'])){
						$k = 0;
						$show_group = "";
						foreach($arr_group_subj[$r->subj_type_id]['group'] as $arr_subj){
							if($k == 0){
								$count_r_in_gr = 0;
								$group_name    = "";
								if(isset($arr_group_subj[$r->subj_type_id]['count_row'])){
									$count_r_in_gr = $arr_group_subj[$r->subj_type_id]['count_row'];
								}
								if(isset($arr_group_subj[$r->subj_type_id]['group_name'])){
									$group_name = $arr_group_subj[$r->subj_type_id]['group_name'];
								}
								$td_show = "";
								if($count_r_in_gr >1){
									$colspan_h_1 = " colspan='2'";
									$ch_group_1+= $count_r_in_gr;
									$td_show = "<td rowspan='".$count_r_in_gr."'>".$group_name."</td>";
								}
								$tr_group.= "<tr>".$td_show.$arr_subj."</tr>";
							}else{
								$tr_group.= "<tr>".$arr_subj."</tr>";
							}
							$k++;
							
						}
					}
					if(isset($arr_group_subj[$r->subj_type_id]['not_group'])){
						foreach($arr_group_subj[$r->subj_type_id]['not_group'] as $sub_nogroup){
							$colspan_no_g = "";
							if($ch_group_1 >1){
								$colspan_no_g = " colspan='2'";
							}
							$tr_group.='<tr><td style="text-align:left;" '.$colspan_no_g.'>'.$sub_nogroup[0].' '.$sub_nogroup[3].'</td>
								 		<td style="text-align:center;">'.$sub_nogroup[1].' </td>
								 		<td style="text-align:center;">'.$sub_nogroup[2].'</td></tr>';
						}
					}
					
				}
				$tbody_1.=$tr_group;
			}
			$trr .= '<tr style="text-align: center;">
						<th '.$colspan_h_1.' style="vertical-align: middle;">មុខវិជ្ជា Subjects</th>
						<th>ពិន្ទុ <br>​ Score</th>
						<th>ការវាយតម្លៃ <br> Evaluation</th>					
					</tr>'.$tbody_1;	
		}// for sub group ====
		 else {
			$trr .= '<tr><td colspan="4" style="text-align:center;">Data not foun .!</td></tr>';
		}

		// fort ter ---------------
		$sqr = "SELECT DISTINCT
								m.absent,
								m.present,
								m.ranking_bysubj,
								m.average_score
							FROM
								sch_subject_score_monthly_kgp_multi AS m
							INNER JOIN sch_subject_score_monthly_detail_kgp_multi AS d ON m.student_id = d.student_id
							WHERE 1=1
							AND m.student_id = '".$student_id."'
							AND d.program_id = '".$program_id ."'
							AND d.school_level_id = '".$school_level_id."'
							AND d.class_id = '".$class_id."'
							AND d.adcademic_year_id = '".$adcademic_year_id."'
							AND m.exam_monthly = '".$get_result_type."'
							AND d.grade_level_id = '".$grade_level_id."' ";
							
		$rows = $this->db->query($sqr)->row();	
		$qr_grade = $this->db->query("SELECT
										sch_score_mention_gep.mention_kh,
										sch_score_mention_gep.min_score,
										sch_score_mention_gep.max_score
										FROM
										sch_score_mention_gep
										WHERE 1=1
										AND schlevelid='".$school_level_id."'");				
		$total_row = $this->db->query("SELECT COUNT(*) as numrow 
										FROM v_student_enroment
										WHERE 1=1 
										AND schoolid =1
										AND programid='".$program_id."'
										AND schlevelid='".$school_level_id."'
										AND grade_levelid='".$grade_level_id."'
										AND classid='".$class_id."'
										AND year='".$adcademic_year_id."'
										AND is_active=0")->row()->numrow;
		
		$sql_comment = $this->db->query("SELECT
										sch_studend_command_kgp.id,
										sch_studend_command_kgp.student_id,
										sch_studend_command_kgp.schoolid,
										sch_studend_command_kgp.schlevelid,
										sch_studend_command_kgp.yearid,
										sch_studend_command_kgp.gradelevelid,
										sch_studend_command_kgp.classid,
										sch_studend_command_kgp.termid,
										sch_studend_command_kgp.command_teacher,
										sch_studend_command_kgp.command_guardian,
										sch_studend_command_kgp.monthly,
										--sch_studend_command_kgp.academicid,
										(
											SELECT
												concat(
													last_name,
													_utf8 ' ',
													first_name
												) AS user_name
											FROM
												sch_user
											WHERE
												match_con_posid = 'acca'
											AND userid = sch_studend_command_kgp.academicid
										) AS academic_name, 
										sch_studend_command_kgp.userid,
										(
											SELECT
												concat(
													last_name,
													_utf8 ' ',
													first_name
												) AS user_name
											FROM
												sch_user
											WHERE 1=1
											AND userid = sch_studend_command_kgp.userid
										) AS teacher_name,
										DATE_FORMAT(date_academic,'%d-%m-%Y') AS date_academic,
										DATE_FORMAT(date_teacher,'%d-%m-%Y') AS date_teacher,
										DATE_FORMAT(date_return,'%d-%m-%Y') AS date_return,
										DATE_FORMAT(date_guardian,'%d-%m-%Y') AS date_guardian,
										DATE_FORMAT(date_create,'%d-%m-%Y') AS date_create
										FROM
										sch_studend_command_kgp
										WHERE 1=1
										AND schoolid='".$schoolid."'
										AND schlevelid='".$school_level_id."'
										AND yearid='".$adcademic_year_id."'
										AND gradelevelid='".$grade_level_id."'
										AND classid='".$class_id."'
										AND monthly='".$get_result_type."'
										AND student_id='".$student_id."'");
		$data1 = array();
		$data1["total_row"] = $total_row;
		$data1["gtotal"] = $gtotal ;
		$data1["rows"] = $rows;
		$data1["grade_kh"] = $qr_grade;
		$data1["trr"] = $trr;
		$data1["tr"] = $tr;
		$data1["month_name"] = $get_result_type;
		$data1["row"] = $row;
		$data1["comment"] = $sql_comment;
		return $data1;
		
	} // end show data monthly
		
	public function show_data_semester($get_array){
		$schoolid        = "";
		$school_level_id = "";
		$adcademic_year_id = "";
		$grade_level_id    = "";
		$class_id          = "";
		$smesterid   = "";
		$student_id        = "";
		$program_id        = "";
		$semestername      = "";
		if(count($get_array) > 0){
			$schoolid        = $get_array['schoolid'];
			$school_level_id = $get_array['schlevelid'];
			$adcademic_year_id = $get_array['yearid'];
			$grade_level_id    = $get_array['gradlavelid'];
			$class_id          = $get_array['classid'];
			$smesterid         = $get_array['smesterid'];
			$semestername      = $get_array['semestername'];
			$student_id        = $get_array['student_id'];
			$program_id        = $get_array['programid'];
		}
		
		$studentname="";
		$year = "";
		$gtotal    = 0;
		$total_eng = 0;
		$countSubject=1;
		$class_name = array();
		$Student = $this->db->query("SELECT DISTINCT
										sl.student_id,
										s.last_name,
										s.first_name,
									CONCAT(s.last_name_kh,'  ',s.first_name_kh) AS studentname,
										s.gender,
										sl.class_id,
										sc.class_name,
										y.sch_year,
										sl.program_id,
										sl.school_level_id,
										sl.adcademic_year_id,
										sl.grade_level_id
									FROM
										sch_subject_score_semester_kgp_multi AS sl
									INNER JOIN sch_student AS s ON sl.student_id = s.studentid
									INNER JOIN sch_class AS sc ON sl.class_id = sc.classid
									INNER JOIN sch_school_year AS y ON sl.adcademic_year_id = y.yearid
									WHERE 1 = 1 
									AND sl.school_level_id = '".$school_level_id."' 
									AND sl.grade_level_id = '".$grade_level_id."'
									AND sl.program_id = '".$program_id."'
									AND sl.adcademic_year_id = '".$adcademic_year_id."'
									AND sl.class_id = '".$class_id."'
									AND sl.student_id = '". $student_id."'
									");
		$row = $Student->row();
		//$year = $row->sch_year;
		$sqll_mention = $this->db->query("SELECT
											scm.mention,
											scm.menid,
											scm.mention_kh,
											ssm.max_score,
											ssm.minscrore,
											ssm.schlevelid,
											ssm.schoolid,
											ssm.subjectid,
											ssm.gradelevelid
											FROM
												sch_score_mention AS scm
											INNER JOIN sch_subject_mention AS ssm ON scm.menid = ssm.mentionid
											WHERE
												1 = 1
											AND ssm.schoolid = '".$schoolid."'
											AND ssm.schlevelid = '".$school_level_id."'
											AND ssm.gradelevelid = '".$grade_level_id."'");
		$arr_m = array();
		if($sqll_mention->num_rows() > 0){
			foreach($sqll_mention->result() as $row_m){
				$arr_m[$row_m->subjectid][$row_m->menid] = array("scoreMin"=>$row_m->minscrore,"scoreMax"=>$row_m->max_score,"mentEng"=>$row_m->mention,"mentKh"=>$row_m->mention_kh);
			}
		}
		//$mention ='ល្អ';
		// print_r($arr_m);
		// khmer & english part =====
		$q = $this->db->query("SELECT
									st.subject_type,
									st.main_type,
									st.subject_type_kh,
									st.subj_type_id
								FROM
									sch_subject_type AS st 
								WHERE st.main_type = '1' 
								AND st.schlevelid = '".$school_level_id."' 
								ORDER BY orders ASC");
		
		$tr = '';
		$colspan_h = "";
		$tbody     = '';
		$ch_group = 0;
		if ($q->num_rows() > 0) 
		{
			foreach ($q->result() as $r) {
				// subject ==========
				$q_ = $this->db->query("SELECT
											s.subjectid,
											s.subj_type_id,
											s.subject_kh,
											s.max_score,
											s.score_group,
											s.`subject`
										FROM
											sch_subject AS s 
										INNER JOIN sch_subject_detail ON s.subjectid = sch_subject_detail.subjectid
										WHERE s.subj_type_id = '".$r->subj_type_id."' AND grade_levelid='".$grade_level_id."'");
				$arr_group_subj= array();
				$i = 0;
				if ($q_->num_rows() > 0) 
				{					
					foreach ($q_->result() as $r_) 
					{
						$is_group_subject = $r_->score_group;

						$sql = "SELECT DISTINCT
										--m.total_score,
										d.total_score,
										d.rank_subject
									FROM
										sch_subject_score_semester_kgp_multi AS m
									INNER JOIN sch_subject_score_semester_detail_kgp_multi AS d ON m.student_id = d.student_id
									WHERE d.subject_mainid = '1'
									AND d.subject_groupid = '".$r_->subj_type_id."'
									AND d.subject_id = '".$r_->subjectid."'
									AND m.student_id = '".$student_id."'
									AND m.exam_semester = '".$smesterid."'
									AND d.program_id = '".$program_id."'
									AND d.school_level_id = '".$school_level_id."'
									AND d.class_id = '".$class_id."'
									AND d.adcademic_year_id = '".$adcademic_year_id."'
									AND d.grade_level_id = '".$grade_level_id."' ";

						
						$sm = $this->db->query($sql)->row();
						$countSubject++;

						$total_score = isset($sm->total_score) ? $sm->total_score : 0;
						$rank_subj   = isset($sm->rank_subject) ? $sm->rank_subject : 0;
						$gtotal += $total_score - 0;

						$i++;
						/// =========== mention
						$show_mention_1 = "";
						if(isset($arr_m[$r_->subjectid])){
							foreach($arr_m[$r_->subjectid] as $v1){
								if($v1['scoreMin'] <= $total_score && $v1['scoreMax'] >= $total_score){
									$show_mention_1 = $v1['mentKh'];
								}
							}
						}
						if($is_group_subject == 1){
							$arr_group_subj[$r_->subj_type_id]['group'][$r_->subjectid] = '<td style="text-align:left;">'.$r_->subject_kh.'</td>
																							<td style="text-align:center;">'.number_format($total_score, 2).'/'.$r_->max_score.'</td>
																							<td style="text-align:center;">'.$rank_subj.'</td>
																							<td style="text-align:center;">'.$show_mention_1.'</td>';
							if(isset($arr_group_subj[$r_->subj_type_id]['count_row'])){
								$arr_group_subj[$r_->subj_type_id]['count_row'] = $arr_group_subj[$r_->subj_type_id]['count_row']+1;
							}else{
								$arr_group_subj[$r_->subj_type_id]['count_row'] = 1;
							}
							if(!isset($arr_group_subj[$r_->subj_type_id]['group_name'])){
								$arr_group_subj[$r_->subj_type_id]['group_name'] = $r->subject_type;
							}
						}else{
							$arr_group_subj[$r_->subj_type_id]['not_group'][$r_->subjectid] = array($r_->subject_kh,number_format($total_score, 2).'/'.$r_->max_score,$rank_subj,$show_mention_1,$r_->subject);
						}
					}					
				}// for subject ====
				$tr_group = "";
				if(isset($arr_group_subj[$r->subj_type_id]))
				{
					if(isset($arr_group_subj[$r->subj_type_id]['group'])){
						$k = 0;
						$show_group = "";
						foreach($arr_group_subj[$r->subj_type_id]['group'] as $arr_subj){
							if($k == 0){
								$count_r_in_gr = 0;
								$group_name    = "";
								if(isset($arr_group_subj[$r->subj_type_id]['count_row'])){
									$count_r_in_gr = $arr_group_subj[$r->subj_type_id]['count_row'];
								}
								if(isset($arr_group_subj[$r->subj_type_id]['group_name'])){
									$group_name = $arr_group_subj[$r->subj_type_id]['group_name'];
								}
								$td_show = "";
								if($count_r_in_gr >1){
									$colspan_h = " colspan='2'";
									$ch_group+= $count_r_in_gr;
									$td_show = "<td rowspan='".$count_r_in_gr."'>".$group_name."</td>";
								}
								$tr_group.= "<tr>".$td_show.$arr_subj."</tr>";
							}else{
								$tr_group.= "<tr>".$arr_subj."</tr>";
							}
							$k++;
							
						}
					}
					if(isset($arr_group_subj[$r->subj_type_id]['not_group'])){
						foreach($arr_group_subj[$r->subj_type_id]['not_group'] as $sub_nogroup){
							$colspan_no_g = "";
							if($ch_group >1){
								$colspan_no_g = " colspan='2'";
							}
							$tr_group.='<tr><td style="text-align:left;" '.$colspan_no_g.'>'.$sub_nogroup[0].' '.$sub_nogroup[4].'</td>
								 		<td style="text-align:center;">'.$sub_nogroup[1].'</td>
								 		<td style="text-align:center;">'.$sub_nogroup[2].'</td>
								 		<td style="text-align:center;">'.$sub_nogroup[3].'</td></tr>';
						}
					}
				}
				$tbody.=$tr_group;
			}
			$tr .= '<tr style="text-align: center;">
					<th '.$colspan_h.' style="vertical-align: middle;">មុខវិជ្ជា Subjects</th>
					<th>ពិន្ទុ <br>​ Score</th>
					<th>ចំ.</th>
					<th>ការវាយតម្លៃ <br> Evaluation</th>					
				</tr>'.$tbody;
		}// for sub group ====
		else {
			$tr .= '<tr><td colspan="4" style="text-align:center;">Data not foun .!</td></tr>';
		}

		// frorm to --------------------------
		$q = $this->db->query("SELECT
									st.subject_type,
									st.main_type,
									st.subject_type_kh,
									st.subj_type_id
								FROM
									sch_subject_type AS st 
								WHERE st.main_type = '0' AND st.schlevelid = '".$school_level_id."' 
								ORDER BY orders ASC");
		
		$trr = '';
		$colspan_h_1 = "";
		$tbody_1     = '';
		$ch_group_1  = 0;
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $r) {
				// subject ==========
				$q_ = $this->db->query("SELECT
											s.subjectid,
											s.subj_type_id,
											s.subject_kh,
											s.max_score,
											s.score_group,
											s.`subject`
										FROM
											sch_subject AS s 
										INNER JOIN sch_subject_detail ON s.subjectid = sch_subject_detail.subjectid
										WHERE s.subj_type_id = '".$r->subj_type_id."' AND grade_levelid='".$grade_level_id."'");
				$j = 0;
				$arr_group_subj= array();
				if ($q_->num_rows() > 0) 
				{					
					foreach ($q_->result() as $r_) {
						$is_group_subject = $r_->score_group;
						$sql = "SELECT DISTINCT
										d.total_score,
										d.rank_subject
									FROM
										sch_subject_score_semester_kgp_multi AS m
									INNER JOIN sch_subject_score_semester_detail_kgp_multi AS d ON m.student_id = d.student_id
									WHERE d.subject_mainid = '2'
									AND d.subject_groupid = '".$r_->subj_type_id."'
									AND d.subject_id = '".$r_->subjectid."'
									AND m.student_id = '".$student_id."'
									AND m.exam_semester = '".$smesterid."'
									AND d.program_id = '".$program_id ."'
									AND d.school_level_id = '".$school_level_id."'
									AND d.class_id = '".$class_id."'
									AND d.adcademic_year_id = '".$adcademic_year_id."'
									AND d.grade_level_id = '".$grade_level_id."'  ";

						// echo ($sql);
						$sm = $this->db->query($sql)->row();
						// $countSubject++;

						$total_score = isset($sm->total_score) ? $sm->total_score : 0;	
						$rank_subj   = isset($sm->rank_subject) ? $sm->rank_subject : 0;											
						$gtotal += $total_score - 0;
						/// =========== mention
						$show_mention_2 = "";
						if(isset($arr_m[$r_->subjectid])){
							foreach($arr_m[$r_->subjectid] as $v2){
								if($v2['scoreMin'] <= $total_score && $v2['scoreMax'] >= $total_score){
									$show_mention_2 = $v2['mentKh'];
								}
							}
						}
						/// =========== end mention
						$j++;
						if($is_group_subject == 1){

							$arr_group_subj[$r_->subj_type_id]['group'][$r_->subjectid] = '<td style="text-align:left;"  class="class_format_fontBattambang">'.$r_->subject_kh.'</td>
																							<td style="text-align:center;"  class="class_format_fontBattambang">'.number_format($total_score, 2).'/'.$r_->max_score.'</td>
																							<td style="text-align:center;"  class="class_format_fontBattambang">'.$rank_subj.'</td>
																							<td style="text-align:center;"  class="class_format_fontBattambang">'.$show_mention_2.'</td>';
							if(isset($arr_group_subj[$r_->subj_type_id]['count_row'])){
								$arr_group_subj[$r_->subj_type_id]['count_row'] = $arr_group_subj[$r_->subj_type_id]['count_row']+1;
							}else{
								$arr_group_subj[$r_->subj_type_id]['count_row'] = 1;
							}
							if(!isset($arr_group_subj[$r_->subj_type_id]['group_name'])){
								$arr_group_subj[$r_->subj_type_id]['group_name'] = $r->subject_type;
							}
						}else{
							$arr_group_subj[$r_->subj_type_id]['not_group'][$r_->subjectid] = array($r_->subject_kh,number_format($total_score, 2).'/'.$r_->max_score,$rank_subj,$show_mention_2,$r_->subject);
						}
						
					}		
			
				}// for subject ====
				$tr_group = "";
				if(isset($arr_group_subj[$r->subj_type_id]))
				{
					if(isset($arr_group_subj[$r->subj_type_id]['group'])){
						$k = 0;
						$show_group = "";
						foreach($arr_group_subj[$r->subj_type_id]['group'] as $arr_subj){
							if($k == 0){
								$count_r_in_gr = 0;
								$group_name    = "";
								if(isset($arr_group_subj[$r->subj_type_id]['count_row'])){
									$count_r_in_gr = $arr_group_subj[$r->subj_type_id]['count_row'];
								}
								if(isset($arr_group_subj[$r->subj_type_id]['group_name'])){
									$group_name = $arr_group_subj[$r->subj_type_id]['group_name'];
								}
								$td_show = "";
								if($count_r_in_gr >1){
									$colspan_h_1 = " colspan='2'";
									$ch_group_1+= $count_r_in_gr;
									$td_show = "<td rowspan='".$count_r_in_gr."'>".$group_name."</td>";
								}
								$tr_group.= "<tr>".$td_show.$arr_subj."</tr>";
							}else{
								$tr_group.= "<tr>".$arr_subj."</tr>";
							}
							$k++;
							
						}
					}
					if(isset($arr_group_subj[$r->subj_type_id]['not_group'])){
						foreach($arr_group_subj[$r->subj_type_id]['not_group'] as $sub_nogroup){
							$colspan_no_g = "";
							if($ch_group_1 >1){
								$colspan_no_g = " colspan='2'";
							}
							$tr_group.='<tr><td style="text-align:left;" '.$colspan_no_g.' class="class_format_fontBattambang">'.$sub_nogroup[0].' '.$sub_nogroup[4].'</td>
								 		<td style="text-align:center;" class="class_format_fontBattambang">'.$sub_nogroup[1].'</td>
								 		<td style="text-align:center;" class="class_format_fontBattambang">'.$sub_nogroup[2].'</td>
								 		<td style="text-align:center;" class="class_format_fontBattambang">'.$sub_nogroup[3].'</td></tr>';
						}
					}
				}
				$tbody_1.=$tr_group;
			}
			$trr .= '<tr style="text-align: center;">
						<th '.$colspan_h_1.' style="vertical-align: middle;"  class="class_format_fontBattambang">មុខវិជ្ជា Subjects</th>
						<th  class="class_format_fontBattambang">ពិន្ទុ <br>​ Score</th>
						<th  class="class_format_fontBattambang">ចំ.</th>
						<th  class="class_format_fontBattambang">ការវាយតម្លៃ <br> Evaluation</th>					
					</tr>'.$tbody_1;
		}// for sub group ====
		 else {
			$trr .= '<tr><td colspan="4" style="text-align:center;">Data not foun .!</td></tr>';
		}

		// fort ter ---------------
		$sqr = "SELECT DISTINCT
								m.absent,
								m.present,
								m.ranking_bysubj,
								m.average_score
							FROM
								sch_subject_score_monthly_kgp_multi AS m
							INNER JOIN sch_subject_score_monthly_detail_kgp_multi AS d ON m.student_id = d.student_id
							WHERE 1=1
							AND m.student_id = '".$student_id."'
							AND d.program_id = '".$program_id ."'
							AND d.school_level_id = '".$school_level_id."'
							AND d.class_id = '".$class_id."'
							AND d.adcademic_year_id = '".$adcademic_year_id."'
							AND d.grade_level_id = '".$grade_level_id."' ";
							
		$rows = $this->db->query($sqr)->row();					

		$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM sch_subject_score_monthly_kgp_multi AS sl  
										WHERE 1=1 
										AND sl.class_id = '".$class_id."' 
										AND sl.program_id = '".$program_id."' 
										AND sl.school_level_id = '".$school_level_id."' 
										AND sl.grade_level_id = '".$grade_level_id."' ")->row()->numrow;

		$sql_comment = $this->db->query("SELECT * FROM sch_evaluation_semester_kgp_order 
										WHERE 1=1
										AND studentid = '".$student_id."'
										AND schoolid = '".$schoolid."'
										AND schlevelid = '".$school_level_id."'
										AND grade_levelid = '".$grade_level_id."'
										AND yearid = '".$adcademic_year_id."'
										AND semester = '".$smesterid."'
										AND classid = '".$class_id."'
										");
		$year = $this->db->query("SELECT
									DATE_FORMAT(from_date,'%Y') as from_date,
									DATE_FORMAT(to_date,'%Y') as to_date
								FROM sch_school_year WHERE 1=1 AND yearid='".$adcademic_year_id."'")->row();
		$show_y = "";
		if(isset($year->from_date) && isset($year->to_date)){
			$show_y = $year->from_date."-".$year->to_date;
		}
		$data2 = array();
		$data2["semestername"] = $semestername;
		$data2["showyear"] = $show_y;
		$data2["total_row"] = $total_row;
		$data2["gtotal"] = $gtotal ;
		$data2["rows"] = $rows;
		$data2["trr"] = $trr;
		$data2["tr"] = $tr;
		$data2["row"] = $row;
		$data2["comment"] = $sql_comment;
		return $data2;
	}

	public function primary_yearly_report_individual_ranking(){	
			$class_id = $this->input->post('class_id');
			$student_id = $this->input->post('student_id');
			$where = "";

			if($class_id != ""){
				$where .= " AND f.class_id = ". $class_id;
			}
			if($student_id != ""){
				$where .= " AND f.student_id = ". $student_id;
			}
			$row = $this->mode->getStudentfinal($where);
		$data2['row'] = $row;
		$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_yearly_report_individual_ranking', $data2);
	}
	public function select_data_comment(){
		$class_id    = $this->input->post("class_id");
		$schoolid    = $this->input->post("schoolid");
		$programid   = $this->input->post("programid");
		$schlevelid  = $this->input->post("schlevelid");
		$yearid      = $this->input->post("yearid");
		$gradlavelid = $this->input->post("gradlavelid");
		
		$studentid    = $this->input->post("studentid");
		$select_result_type = $this->input->post("type_exam");
		$sql_return = array();
		if($select_result_type == 1){ // Monthly
				$type_monthly = $this->input->post("type_monthly");
				$sql_select= $this->db->query("SELECT 
													student_id,
													command_teacher,
													command_guardian,
													monthly,
													academicid,
													userid,
													DATE_FORMAT(date_academic,'%d-%m-%Y') AS date_academic,
													DATE_FORMAT(date_teacher,'%d-%m-%Y') AS date_teacher,
													DATE_FORMAT(date_return,'%d-%m-%Y') AS date_return,
													DATE_FORMAT(date_guardian,'%d-%m-%Y') AS date_guardian,
													DATE_FORMAT(date_create,'%d-%m-%Y') AS date_create
												FROM sch_studend_command_kgp
												WHERE 1=1 
												AND student_id='".$studentid."'
												AND schoolid='".$schoolid."'
												AND schlevelid='".$schlevelid."'
												AND yearid='".$yearid."'
												AND gradelevelid='".$gradlavelid."'
												AND classid='".$class_id."'
												AND monthly='".$type_monthly."'
											");
				if($sql_select->num_rows() > 0){
					$sql_return['ord'] = $sql_select->result();
				}else{
					$sql_return = "";
				}
		}else if($select_result_type == 2){ // Semester
				$semester = $this->input->post("semester");
				$sql_select = $this->db->query("SELECT
													id,
													evaluationid,
													studentid,
													schoolid,
													schlevelid,
													grade_levelid,
													yearid,
													semester,
													classid,
													academicid,
													userid,
													techer_comment,
													guardian_comment,
													DATE_FORMAT(academic_date,'%d-%m-%Y') AS date_academic,
													DATE_FORMAT(techer_date,'%d-%m-%Y') AS date_teacher,
													DATE_FORMAT(guardian_date,'%d-%m-%Y') AS date_guardian,
													DATE_FORMAT(return_date,'%d-%m-%Y') AS date_return
													FROM
													sch_evaluation_semester_kgp_order
													WHERE 1=1 
													AND studentid = '".$studentid."'
													AND schoolid = '".$schoolid."'
													AND schlevelid = '".$schlevelid."'
													AND grade_levelid = '".$gradlavelid."'
													AND yearid = '".$yearid."'
													AND semester = '".$semester."'
													AND classid = '".$class_id."'
												");
				if($sql_select->num_rows() > 0){
					$arr_ord = $sql_select->result();
					$sql_return['ord']   = $arr_ord;
					foreach($arr_ord as $row_or){
						$sql_detail = $this->db->query("SELECT
														sch_evaluation_semester_kgp_detail.eva_id,
														sch_evaluation_semester_kgp_detail.evaluationid,
														sch_evaluation_semester_kgp_detail.descrition_id,
														sch_evaluation_semester_kgp_detail.seldom,
														sch_evaluation_semester_kgp_detail.sometimes,
														sch_evaluation_semester_kgp_detail.usually,
														sch_evaluation_semester_kgp_detail.consistently
														FROM
														sch_evaluation_semester_kgp_detail
														WHERE 1=1 
														AND evaluationid='".$row_or->evaluationid."'
													");
						if($sql_detail->num_rows() > 0){
							$sql_return['detail'] = $sql_detail->result();
						}else{
							$sql_return['detail']="";
						}
					}
				}else{
					$sql_return="";
				}
		}else if($select_result_type == 3){
			$sql_com_fin = $this->db->query("SELECT
												sch_studend_com_ann_aca_kgp.id,
												sch_studend_com_ann_aca_kgp.student_id,
												sch_studend_com_ann_aca_kgp.schoolid,
												sch_studend_com_ann_aca_kgp.schlevelid,
												sch_studend_com_ann_aca_kgp.yearid,
												sch_studend_com_ann_aca_kgp.gradelevelid,
												sch_studend_com_ann_aca_kgp.classid,
												sch_studend_com_ann_aca_kgp.termid,
												sch_studend_com_ann_aca_kgp.command_teacher,
												sch_studend_com_ann_aca_kgp.academicid,
												sch_studend_com_ann_aca_kgp.userid,
												sch_studend_com_ann_aca_kgp.directorid,
												sch_studend_com_ann_aca_kgp.teacherid,
												DATE_FORMAT(date_academic,'%d-%m-%Y') as date_academic,
												DATE_FORMAT(date_teacher,'%d-%m-%Y') as date_teacher,
												DATE_FORMAT(date_director,'%d-%m-%Y') as date_director
												FROM
												sch_studend_com_ann_aca_kgp
												WHERE 1=1
												AND student_id='".$studentid."'
												AND schoolid='".$schoolid."'
												AND schlevelid='".$schlevelid."'
												AND yearid='".$yearid."'
												AND gradelevelid='".$gradlavelid."'
												AND classid='".$class_id."'");
			if($sql_com_fin->num_rows() > 0){
				$sql_return['ord'] = $sql_com_fin->result();
			}else{
				$sql_return = "";
			}
		}
		
		header("Content-type:text/x-json");
		echo json_encode($sql_return);
	}

	public function save_comment_student_monthly(){
		$type_monthly = $this->input->post("type_monthly");
		$class_id    = $this->input->post("class_id");
		$schoolid    = $this->input->post("schoolid");
		$programid   = $this->input->post("programid");
		$schlevelid  = $this->input->post("schlevelid");
		$yearid      = $this->input->post("yearid");
		$gradlavelid = $this->input->post("gradlavelid");
		
		$comment_teacher = $this->input->post("comment_teacher");
		$date_approve = $this->input->post("date_approve");
		$date_teacher = $this->input->post("date_teacher");
		$managername  = $this->input->post("managername");
		$teachername  = $this->input->post("teachername");
		$comment_parent = $this->input->post("comment_parent");
		$date_parents   = $this->input->post("date_parents");
		$date_return    = $this->input->post("date_return");
		$studentid      = $this->input->post("studentid");
		$select_result_type = $this->input->post("select_result_type");
		if($select_result_type == 1){ // monthly
			$sql_check = $this->db->query("SELECT student_id FROM sch_studend_command_kgp 
															WHERE 1=1 
															AND student_id='".$studentid."'
															AND schoolid='".$schoolid."'
															AND schlevelid='".$schlevelid."'
															AND yearid='".$yearid."'
															AND gradelevelid='".$gradlavelid."'
															AND classid='".$class_id."'
															AND monthly='".$type_monthly."'
															");
			if($sql_check->num_rows() > 0){
				$data_del = array("student_id"=>$studentid,
									"schoolid"=>$schoolid,
									"schlevelid"=>$schlevelid,
									"yearid"=>$yearid,
									"gradelevelid"=>$gradlavelid,
									"classid"=>$class_id,
									"monthly"=>$type_monthly
									);
				$this->db->delete("sch_studend_command_kgp",$data_del);
			}
			$data = array(
						"student_id"=>$studentid ,
						"schoolid"=>$schoolid,
						"schlevelid"=>$schlevelid,
						"yearid"=>$yearid,
						"gradelevelid"=>$gradlavelid,
						"classid"=>$class_id,
						"termid"=>"",
						"command_teacher"=>$comment_teacher,
						"command_guardian"=>$comment_parent,
						"monthly"=>$type_monthly,
						"academicid"=>$managername,
						"userid"=>$this->session->userdata("userid"),
						"date_academic"=>$this->green->formatSQLDate($date_approve),
						"date_teacher"=>$this->green->formatSQLDate($date_teacher),
						"date_return"=>$this->green->formatSQLDate($date_return),
						"date_guardian"=>$this->green->formatSQLDate($date_parents),
						"date_create"=>date("Y-m-d")
						);
			$succ = $this->db->insert("sch_studend_command_kgp",$data);
		}// end if select_result_type 1
		else if($select_result_type == 2){ // semester
			$semester   = $this->input->post("semester");
			$arr_eval   = $this->input->post("arr_eval");
			$hevaluationid   = $this->input->post("hevaluationid");
			$typno_eval      = $this->green->nextTran(43,"Evaluate Student Kgp");
			if($hevaluationid != ""){
				$this->db->delete("sch_evaluation_semester_kgp_order",array("evaluationid"=>$hevaluationid));
				$this->db->delete("sch_evaluation_semester_kgp_detail",array("evaluationid"=>$hevaluationid));
				$typno_eval = $hevaluationid;
			}
			$data_order = array(
								"evaluationid"=>$typno_eval,
								"studentid"=>$studentid,
								"schoolid"=>$schoolid,
								"schlevelid"=>$schlevelid,
								"grade_levelid"=>$gradlavelid,
								"yearid"=>$yearid ,
								"semester"=>$semester,
								"classid"=>$class_id,
								"academicid"=>$managername,
								"userid"=>$this->session->userdata("userid"),
								"techer_comment"=>$comment_teacher,
								"guardian_comment"=>$comment_parent,
								"academic_date"=>$this->green->convertSQLDate($date_approve),
								"techer_date"=>$this->green->convertSQLDate($date_teacher),
								"guardian_date"=>$this->green->convertSQLDate($date_parents),
								"return_date"=>$this->green->convertSQLDate($date_return)
								);
			//$this->db->insert("sch_evaluation_semester_kgp_order",$data_order);
			$succ = $this->db->insert("sch_evaluation_semester_kgp_order",$data_order);
			if(count($arr_eval) > 0){
				foreach($arr_eval as $row_eval){
					$id_description = $row_eval['iddiscr'];
					$type_description = $row_eval['type_ev'];
					$seldom = 0;$sometime = 0;$usually = 0;$consistently = 0;
					if($type_description == 'seldom'){
						$seldom = 1;
					}else if($type_description == 'sometime'){
					 	$sometime = 1;
					}else if($type_description == 'usually'){
					 	$usually = 1;
					}else if($type_description == 'consistently'){
					 	$consistently = 1;
					}

					$data_eval = array(
									"evaluationid"=>$typno_eval,
									"descrition_id"=>$id_description,
									"seldom"=>$seldom,
									"sometimes"=>$sometime,
									"usually"=>$usually,
									"consistently"=>$consistently
									);
					$this->db->insert("sch_evaluation_semester_kgp_detail",$data_eval);
									
				}
			}

		}else if($select_result_type == 3){
			$date_director = $this->input->post("date_director");
			$director      = $this->input->post("director");
			$data_del = array("student_id"=>$studentid,
								"schoolid"=>$schoolid,
								"schlevelid"=>$schlevelid,
								"yearid"=>$yearid,
								"gradelevelid"=>$gradlavelid,
								"classid"=>$class_id
								);
			$this->db->delete("sch_studend_com_ann_aca_kgp",$data_del);
			$data = array(
						"student_id"=>$studentid ,
						"schoolid"=>$schoolid,
						"schlevelid"=>$schlevelid,
						"yearid"=>$yearid,
						"gradelevelid"=>$gradlavelid,
						"classid"=>$class_id,
						"termid"=>"",
						"command_teacher"=>$comment_teacher,
						"directorid" =>$director,
						"academicid"=>$managername,
						"teacherid"=>$teachername,
						"userid"=>$this->session->userdata("userid"),
						"date_director"=>$this->green->formatSQLDate($date_director),
						"date_academic"=>$this->green->formatSQLDate($date_approve),
						"date_teacher"=>$this->green->formatSQLDate($date_teacher),
						"date_create"=>date("Y-m-d")
						);
			$succ = $this->db->insert("sch_studend_com_ann_aca_kgp",$data);
		}// end if select_result_type 1
		
		$alert = 0;
		if($succ){
			$alert = 100;
		}else{
			$alert = 200;
		}
		$arr['alert_result'] = $alert;
		header("Content-type:text/x-json");
		echo json_encode($arr);
	}
	public function show_semester(){
		$schlevelid = $this->input->post("schlevelid");
		$yearid      = $this->input->post("yearid");
		$type_result = $this->input->post("type_result");
		$sql_semester = $this->db->query("SELECT semesterid,semester 
											FROM sch_school_semester 
											WHERE 1=1 
											AND programid=1
											AND schlevelid='".$schlevelid."'
											AND yearid='".$yearid."'
											ORDER BY semesterid ASC");
		$semter = "<ul style='list-style:none;'>";
		if($sql_semester->num_rows() > 0){
			foreach($sql_semester->result() as $row_sem){
				if($type_result == 3){
					$semter .= "<li><label for='name_".$row_sem->semesterid."'><input type='checkbox' checked class='check_semeste' value='".$row_sem->semesterid."' id='name_".$row_sem->semesterid."'>&nbsp;&nbsp;".$row_sem->semester."</label></li>";
				}else{
					$semter .= "<li><label for='name_".$row_sem->semesterid."'><input type='checkbox' class='check_semeste' value='".$row_sem->semesterid."' id='name_".$row_sem->semesterid."'>&nbsp;&nbsp;".$row_sem->semester."</label></li>";
				}
			}
		}
		$semter .= "</ul>";
		echo $semter;
	}
	// public function primary_monthly_report_class_ranking_all(){

	// 	$this->load->view('reports/generalkhmerlanguage/primaryschool/primary_monthly_report_class_ranking_all');
	// }
}
