<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_iep_entryed_order_report extends CI_Controller
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model("reports/mod_iep_entryed_order_report","iep");
	}
	function index(){
		$data['opt_program'] = $this->get_program(); 
		$data['opt_examtyp']=$this->get_examtype();		
		$this->load->view('header');
		$this->load->view('reports/iep_reports/v__iep_entryed_order_report',$data);
		$this->load->view('footer');
	}
	function get_program(){
		$optionprogram="";
		$query = $this->db->get('sch_school_program')->result();
		foreach($query as $pro_id ){
			$optionprogram .='<option value="'.$pro_id->programid.'">'.$pro_id->program.'</option>';
		}
		return $optionprogram;		
	}

	function school_level(){
		$school_level="";
		$option_level="";
		$program = $this->input->post('program');
		foreach($this->iep->getsch_level($program) as $id ){
			$option_level .='<option value="'.$id->schlevelid.'">'.$id->sch_level.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['schlevel'] = $option_level;
		echo json_encode($arr_success);
	}

	function get_years(){
		$schoolid = $this->session->userdata('schoolid');	
		$sch_program = $this->input->post("program");
		$schlevelid = $this->input->post("sch_level");
		$option_year ='';
		$option_year .='<option value=""></option>';
		foreach ($this->iep->getschoolyear($sch_program,$schoolid,$schlevelid) as $row){

			$option_year .='<option value="'.$row->yearid.'">'.$row->sch_year.'</option>';
		}		
		header("Content-type:text/x-json");
		$arr_success['schyear'] = $option_year;
		echo json_encode($arr_success);
		
	}

	function get_gradlevel(){
		$grad_level="";
		$schoolid = $this->session->userdata('schoolid');	
		$programid = $this->input->post("program");
		$schlevelid = $this->input->post("sch_level");
		$grad_level .='<option value=""></option>';
		foreach($this->iep->getgradelevel($schoolid,$programid,$schlevelid) as $id ){
			$grad_level .='<option value="'.$id->grade_levelid.'">'.$id->grade_level.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['gradlevel'] = $grad_level;
		echo json_encode($arr_success);
	}

	function get_class(){
		$schoolid = $this->session->userdata('schoolid');	
		$grad_level = $this->input->post("grad_level");
		$schlevelid = $this->input->post("sch_level");
		$class="";
		$class .='<option value=""></option>';
		foreach($this->iep->get_class($schoolid,$grad_level,$schlevelid) as $id ){			
			$class .='<option value="'.$id->classid.'">'.$id->class_name.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['getclass'] = $class;
		echo json_encode($arr_success);  
	}

	function get_examtype(){
		$examtype="";
		$query=$this->db->get('sch_student_examtype')->result();
		foreach($query as $id ){

			$examtype .='<option value="'.$id->examtypeid.'">'.$id->exam_test.'</option>';
		}
		return $examtype;
	}
// // ====================================================================
// 	function show_reports(){
// 		$perpage=$this->input->post('perpage');
// 		$program=$this->input->post('program');
//         $classid=$this->input->post('classid');
//         $from_date=$this->green->convertSQLDate($this->input->post('from_date'));
//         $to_date=$this->green->convertSQLDate($this->input->post('to_date'));
//         $sch_level=$this->input->post('sch_level');
//         $exam_type=$this->input->post('exam_type');
//         $years=$this->input->post('years');
//         $grad_level=$this->input->post('grad_level');      
//         $where='';
//         // if($program !=""){
//         // 	$where .=" AND sch_school_program.programid = '".$program."'";
//         // }        
//         if($exam_type != ""){
//         	$where.=" AND exam_typeid='".$exam_type."'";
//         }
// 		if($from_date != ''){ 
// 				$where .= 'AND create_date >= "'.$from_date.'"';
// 			}
// 		if($to_date != ''){ 
// 			$where .= 'AND create_date <= "'.$to_date.'"';
// 		}

// 		if($sch_level !=""){
// 			$where.= " AND schlevelid = '".$sch_level."'";
// 		}

// 		if($years !=""){
// 			$where.= " AND yearid = '".$years."'";
// 		}

// 		if($grad_level !=""){
// 			$where.= " AND gradelevelid = '".$grad_level."'";
// 		}

// 		if($classid !=""){
// 			$where.= " AND classid = '".$classid."'";
// 		}

//            $sql = "SELECT *,
//            					CONCAT(v_score_iep_enties.last_name, ' ', v_score_iep_enties.first_name) AS fullname,
//            					CONCAT(v_score_iep_enties.last_name_kh, ' ',v_score_iep_enties.first_name_kh) AS fullname_kh
//            			FROM v_score_iep_enties 
//      				WHERE  1=1 {$where}";

//      	$table="";
//      	$pagina='';
// 		$getperpage=0;
// 		if($perpage==''){
// 			$getperpage=10;
// 		}else{
// 			$getperpage=$perpage;
// 		}
// 		$paging=$this->green->ajax_pagination(count($this->db->query($sql)->result()),site_url("reports/c_iep_entryed_order_report/show_report"),$getperpage,"icon");
		
// 		$i=1;
// 		$getlimit=10;
// 		if($paging['limit']!=''){
// 			$getlimit=$paging['limit'];
// 		}
// 		$limit=" LIMIT {$paging['start']}, {$getlimit}";   


// 		$sql.=" {$limit}";

// 		$sqlquery= $this->db->query("SELECT * FROM v_score_iep_enties uni 		
// 					 				   WHERE  1=1 {$where}");
// 		$data=array();
// 		if($sqlquery->num_rows() > 0)
// 		{	
// 			foreach($this->db->query($sql)->result() as $row)						
// 			{
// 				$table .='	<div class="wrapper" style="border:0px solid #f00;overflow:auto">
// 								<div class="col-sm-12"> 
// 									<div id="tap_print">	
// 										<div class="col-sm-12" style="padding-top:0px;" id="titles">
// 									   		<div class="col-sm-4" style="width:200px !important">
// 									   			<img src="'.base_url('assets/images/logo/logo.png').'" style="width:200px !important">
// 									   		</div>
// 									   		<div class="col-sm-6 header">
// 									   			<h4 class="kh_font">របាយការណ៍សិក្សាបញ្ចប់ត្រីមាស</h4>			   			
// 									   			<h4 style="font-size:23px;">Final-Term Report</h4>
// 									   			<h4 style="font-size:23px;">Term-02</h4>	
// 									   		</div>  			   		
// 									   	</div>	 	
// 										<div class="col-sm-12" style="padding-left:40px; padding-right:40px;">			
// 							                <table class="table head_name" style=" margin-bottom: 0px !important;">
// 							                   	<tbody>
// 							                   		<tr>
// 							                   			<td style="width:15%;">Student is Name:</td>
// 							                   			<td style="width:20%">'.$row->fullname.'</td>
// 							                   			<td style="width:5%">Level:</td>
// 							                   			<td style="width:10%">'.$row->grade_level.'</td>
// 							                   			<td style="width:10%">Yearly:</td>
// 							                   			<td style="width:20%">'.$row->sch_year.'</td>
// 							                   		</tr>
// 							                   		<tr>
// 							                   			<td>សិស្សឈ្មោះ</td>
// 							                   			<td class="kh_font">'.$row->fullname_kh.'</td>
// 							                   			<td>កម្រិត:</td>
// 							                   			<td>'.$row->grade_level.'</td>
// 							                   			<td>ឆ្នាំសិក្សា:</td>
// 							                   			<td>'.$row->sch_year.'</td>
// 							                   		</tr>
// 							                   	</tbody>
// 							                </table>
// 										</div>	
// 										<div class="col-sm-12" style="text-align:center; padding-bottom:15px">
// 											<p style="font-size:13px;">Score and Evaluation of Each Subject / ពិន្ទុ និងការវាយតម្លៃមុខវិជ្ជានីមួយៗ</p>
// 										</div>
// 										<div class="col-sm-12">				
// 											<div class="col-sm-6" style="padding-left:0px;">
// 												<label>Assessment and English Exam</label>
// 							                   	<table class="table table-bordered">
// 							                   	 	<thead>
// 							                   	 		<tr>
// 							                   	 			<th id="head">Other Assessment</th>
// 							                   	 			<th>Score​ <br> ពិន្ទុ</th>
// 							                   	 			<th>Evaluation <br> ការវាយតម្លៃ</th>
// 							                   	 		</tr>
// 							                   	 	</thead>
// 							                   	 	<tbody>
// 							                   	 		<tr>
// 							                   	 			<td>Class-Participation / សកម្មភាពសិក្សា</td>
// 							                   	 			<td id="scor">10</td>
// 							                   	 			<td id="scor">Excellent</td>
// 							                   	 		</tr>
// 							                   	 		<tr>
// 							                   	 			<td>Home Work / កិច្ចការផ្ទះ</td>
// 							                   	 			<td id="scor">10</td>
// 							                   	 			<td id="scor">Excellent</td>
// 							                   	 		</tr>
// 							                   	 		<tr>
// 							                   	 			<td>Portfolio Diligence / រៀបចំស៊ីម៉ី</td>
// 							                   	 			<td id="scor">10</td>
// 							                   	 			<td id="scor">Excellent</td>
// 							                   	 		</tr>
// 							                   	 		<tr>
// 							                   	 			<td>Vocabulary / វាក្យសព្ទ</td>
// 							                   	 			<td id="scor">10</td>
// 							                   	 			<td id="scor">Excellent</td>
// 							                   	 		</tr>
// 							                   	 		<tr>
// 							                   	 			<td>Grammar / វេយ្យាករណ៍</td>
// 							                   	 			<td id="scor">10</td>
// 							                   	 			<td id="scor">Excellent</td>
// 							                   	 		</tr>
// 							                   	 		<tr>
// 							                   	 			<td>Listening / ការស្ដាប់</td>
// 							                   	 			<td id="scor">10</td>
// 							                   	 			<td id="scor">Excellent</td>
// 							                   	 		</tr>
// 							                   	 		<tr>
// 							                   	 			<td>Speacking / កានិយាយ</td>
// 							                   	 			<td id="scor">10</td>
// 							                   	 			<td id="scor">Excellent</td>
// 							                   	 		</tr>
// 							                   	 		<tr>
// 							                   	 			<td>Reading / ការអាន</td>
// 							                   	 			<td id="scor">10</td>
// 							                   	 			<td id="scor">Excellent</td>
// 							                   	 		</tr>
// 							                   	 		<tr>
// 							                   	 			<td>Writing / ការសរសេរ</td>
// 							                   	 			<td id="scor">10</td>
// 							                   	 			<td id="scor">Excellent</td>
// 							                   	 		</tr>		                   	 		
// 							                   	 	</tbody>
// 							                   	</table>
// 											</div>
// 											<div class="col-sm-6" style="padding-right: 0px;">					
// 												<div class="form-group">
// 												   	<label>Maths-Science  Exam</label>
// 								                   	<table class="table table-bordered">
// 								                   	 	<thead>
// 								                   	 		<tr>
// 								                   	 			<th id="head">Subjects / មុវិជ្ជា</th>
// 								                   	 			<th id="scor">Score​ <br> ពិន្ទុ</th>
// 								                   	 			<th id="scor">Evaluation <br> ការវាយតម្លៃ</th>
// 								                   	 		</tr>
// 								                   	 	</thead>
// 								                   	 	<tbody>
// 								                   	 		<tr>
// 								                   	 			<td>Mathematics / គណិតវិទ្យា</td>
// 								                   	 			<td id="scor">10</td>
// 								                   	 			<td id="scor">Excellent</td>
// 								                   	 		</tr>
// 								                   	 		<tr>
// 								                   	 			<td>Science / វិទ្យាសាស្ត្រ</td>
// 								                   	 			<td id="scor">10</td>
// 								                   	 			<td id="scor">Excellent</td>
// 								                   	 		</tr>
// 								                   	 	</tbody>
// 								                   	</table>
// 									            </div>
// 									            <div class="form-group">
// 								                   	<table class="table table-bordered">
// 								                   	 	<tbody>
// 								                   	 		<tr>
// 								                   	 			<td >Assessment & English Test:</td>
// 								                   	 			<td style="width:50%" id="scor">100</td>		                   	 			
// 								                   	 		</tr>
// 								                   	 		<tr>
// 								                   	 			<td>Maths-Science Test:</td>
// 								                   	 			<td id="scor">100</td>
// 								                   	 		</tr>
// 								                   	 	</tbody>
// 								                   	</table>
// 									            </div>
// 									            <div class="form-group">
// 								                  	<table class="table table-bordered">
// 								                   	 	<tbody>
// 								                   	 		<tr>
// 								                   	 			<td >Total Final (100) :</td>
// 								                   	 			<td style="width:50%" id="scor">100</td>		                   	 			
// 								                   	 		</tr>
// 								                   	 		<tr>
// 								                   	 			<tr>
// 								                   	 			<td>Total Mid-Term (100) :</td>
// 								                   	 			<td id="scor">100</td>
// 								                   	 		</tr>
// 								                   	 		<tr>
// 								                   	 			<td style="background: #808080 !important;">Grand Total Final (ពិន្ទុសរុប) :</td>
// 								                   	 			<td id="scor" style="background: #808080 !important;">100</td>
// 								                   	 		</tr>		                   	 		
// 								                   	 		</tr>
// 								                   	 	</tbody>
// 								                   	</table>
// 									            </div>
// 											</div>
// 										</div>
// 											<div class="form-group dv_area">
// 												 <textarea class="form-control" rows="8" id="comment" >Teacher is Comments / មតិយោបល់របស់គ្រូ</textarea>
// 											</div>
// 											<div class="col-sm-12"​​ style="margin-bottom: 60px; text-align:center;">
// 												<div class="col-sm-6" style="margin-top:20px; ">
// 													<label>Date: ......../......../............</label><br>
// 													<label>Academic Manager</label>
// 												</div>
// 												<div class="col-sm-6">
// 													<label>Date: ......../......../............</label><br>
// 													<label>Teacher is Signature</label>
// 												</div>
// 											</div>
// 											<div class="form-group dv_area">
// 												 <textarea class="form-control" rows="6" id="comment"​​>​Guardian is Comments / មតិមាតាបិតាសិស្ស</textarea>
// 											</div>
// 											<div class="col-sm-12" style="padding-left:0px; ">
// 												<div class="col-sm-6">
// 													<p>Please return the book to school before</p>
// 													<p>សូមប្រគល់សៀវភៅតាមដាននេះមកសាលាមុនថ្ងៃទី៖</p>					
// 													<div class="col-sm-6" style="padding-left:0px;">
// 														<input type="text" value="10-09-2015" class="form-control" style="text-align:center; width:100%; font-size: 11px; ">
// 													</div>
// 												</div>
// 												<div class="col-sm-6" style="text-align:center;padding:0px;">
// 													<p>Date: ......../......../........</p>
// 													<p>Guardian is Signature / ហត្ថលេខាមាតាមិតាសិស្ស</p>
// 												</div>
// 											</div>
// 									</div>		   		   		
// 							   	</div>
// 							</div>
// 							<script type="text/javascript"></script>';	
// 			}						
// 		}
// 		else
// 		{
// 			$table .= '<tr><td colspan="2" style="font-weight: bold;text-align: center;background: #F2F2F2; color: red;">'."We din't find data".'</tr>';
// 		}
// 			header('Content-Type:text/x-json');			
// 			$arr['list_data']=$table;
// 			$arr['pagination']=$paging;
// 			echo json_encode($arr); 		
// 	}	

// // =====================================================================
	function show_report(){
		$perpage=$this->input->post('perpage');
		$program=$this->input->post('program');
        $classid=$this->input->post('classid');
        $from_date=$this->green->convertSQLDate($this->input->post('from_date'));
        $to_date=$this->green->convertSQLDate($this->input->post('to_date'));
        $sch_level=$this->input->post('sch_level');
        $exam_type=$this->input->post('exam_type');
        $years=$this->input->post('years');
        $grad_level=$this->input->post('grad_level');        
        $where='';        
        // if($exam_type != ""){
        // 	$where.=" AND exam_typeid='".$exam_type."'";
        // }
		// if($from_date != ''){ 
		// 		$where .= 'AND create_date >= "'.$from_date.'"';
		// 	}
		// if($to_date != ''){ 
		// 	$where .= 'AND create_date <= "'.$to_date.'"';
		// }	
		if($sch_level !=""){
			$where.= " AND schlevelid = '".$sch_level."'";
		}

		// if($years !=""){
		// 	$where.= " AND yearid = '".$years."'";
		// }

		// if($grad_level !=""){
		// 	$where.= " AND gradelevelid = '".$grad_level."'";
		// }

		// if($classid !=""){
		// 	$where.= " AND classid = '".$classid."'";
		// }

           $sql = "SELECT *,
           					CONCAT(v_score_iep_enties.last_name, ' ', v_score_iep_enties.first_name) AS fullname,
           					CONCAT(v_score_iep_enties.last_name_kh, ' ',v_score_iep_enties.first_name_kh) AS fullname_kh
           			FROM v_score_iep_enties 
     				WHERE  1=1 {$where}";


     	$pagina='';
		$getperpage=0;
		if($perpage==''){
			$getperpage=10;
		}else{
			$getperpage=$perpage;
		}
		$paging=$this->green->ajax_pagination(count($this->db->query($sql)->result()),site_url("reports/c_iep_entryed_order_report/show_report"),$getperpage,"icon");
		
		$i=1;
		$getlimit=10;
		if($paging['limit']!=''){
			$getlimit=$paging['limit'];
		}
		$limit=" LIMIT {$paging['start']}, {$getlimit}";   


		$sql.=" {$limit}";

		$sqlquery= $this->db->query("SELECT * FROM v_score_iep_enties uni 		
					 				   WHERE  1=1 {$where}");
		$data=array();
		if($sqlquery->num_rows() > 0)
		{	
			if($exam_type != ""){
			foreach($this->db->query($sql)->result() as $row)						
			{
				$data['student_name'] 	=	$row->fullname;
				$data['student_namekh'] =	$row->fullname_kh;
				$data['gradlevel'] 		=	$row->grade_level;
				$data['year'] 			=	$row->sch_year;
				if($exam_type == 2){
					$this->load->view('reports/iep_reports/v_mid_term_report',$data);
				}elseif($exam_type == 3){
					$this->load->view('reports/iep_reports/v_final_term_report',$data);
				}elseif($exam_type == 4){	
					$this->load->view('reports/iep_reports/v__semester_acadimic_report',$data);
				}elseif($exam_type == 5){
					$this->load->view('reports/iep_reports/v_annual_academic_report',$data);
				}			
			}
			}
			else{
					echo '<tr><td colspan="2" style="font-weight: bold;text-align: center;background: #F2F2F2; color: red;">'."Please Select Exam Type!".'</tr>';
				}						
		}
		else
		{
			echo '<tr><td colspan="2" style="font-weight: bold;text-align: center;background: #F2F2F2; color: red;">'."We din't find data".'</tr>';
		}
	
	}	
}