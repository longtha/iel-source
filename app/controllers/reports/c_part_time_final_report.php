<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class C_part_time_final_report extends CI_Controller
	{		
		function __construct()
		{
			parent::__construct();
			$this->load->model("reports/mod_part_time_final_report","part_time");
			//$query_core  = $this->part_time->get_core();
		}

		function index(){			
			$data['query']= $this->part_time->get_assessment();
			$data['query_core']= $this->part_time->get_core();
			$data['query_skill']= $this->part_time->get_skill();
			$data['grading']=$this->part_time->get_grad();
			$this->load->view('header');
			$this->load->view('reports/st_reports/v_part_time_final_report',$data);
			$this->load->view('footer');		

		}
		function print_all_parttime(){
			$schoolid  = $_GET['schoolid'];
			$schoollavel = $_GET['schoollavel'];
			$gradelavel= $_GET['gradelavel'];
			$classid   = $_GET['classid'];
			$years     = $_GET['years'];
			$is_partime= $_GET['is_partime'];
			$termid    = $_GET['termid'];
			$perpage    = $_GET['perpage'];
			$numrow    = $_GET['numrow'];
			$data['query']       = $this->part_time->get_assessment();
			$data['query_core']  = $this->part_time->get_core();
			$data['query_skill'] = $this->part_time->get_skill();
			$data['grading']     = $this->part_time->get_grad();
			$data['infor'] = array("schoolid"=>$schoolid,
									"schoollavel"=>$schoollavel,
									"gradelavel"=>$gradelavel,
									"classid"=>$classid,
									"years"=>$years,
									"is_partime"=>$is_partime,
									"termid"=>$termid,
									"perpage"=>$perpage,
									"numrow"=>$numrow
						);
			$this->load->view('header');
			$this->load->view('reports/st_reports/v_part_time_all_report',$data);
			$this->load->view('footer');
		}
	}
