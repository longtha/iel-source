<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_profit_loss extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('profit_loss/v_profit_loss');
		$this->load->view('footer');	
	}

	public function grid(){
		$offset = trim($this->input->post('offset'));
		$limit = trim($this->input->post('limit'));
		$from_date = trim($this->input->post('from_date'));
		$to_date = trim($this->input->post('to_date'));

		// revenues ========
		$q = $this->db->query("SELECT
									SUM(st.amt_line) AS amt,
									st.description
								FROM
									sch_student_fee_type AS st
								WHERE
									date(st.trandate) >= '".$this->green->formatSQLDate($from_date)."'
								AND date(st.trandate) <= '".$this->green->formatSQLDate($to_date)."'
								GROUP BY
									st.otherfee_id
								ORDER BY st.description ASC ");		
		$tr = '';
		$i = 1;
		$total = 0;
		$tr .= '<tr>
					<td colspan="3" style="font-weight: bold;background: #f8f8f8;">Revenues</td>
				</tr>';
		if($q->num_rows() > 0){			
			foreach($q->result() as $row){	
				$total += $row->amt - 0;			
				$tr .= '<tr>
							<td>'.$i++.'</td>
							<td>'.$row->description.'</td>
							<td style="text-align: right;">'.number_format($row->amt, 2).'</td>
						</tr>';
			}
			$tr .= '<tr>
						<td style="text-align: right;" colspan="2">Total revenues</td>
						<td style="text-align: right;font-weight: bold;">'.number_format($total, 2).'</td>
					</tr>';
		}
		else{
			$tr .= '<tr>
						<td colspan="3" style="font-weight: bold;text-align: center;_background: #F2F2F2;">No revenues</td>
					</tr>';
		}

		// expenses =========
		$q_e = $this->db->query("SELECT
										d.otherfeeid,
										m.tran_date,
										SUM(d.amt) AS to_amt,
										se.otherfee_code,
										se.otherfee
									FROM
										sch_other_expense_master AS m
									INNER JOIN sch_other_expense_detail AS d ON m.typeno = d.typeno
									INNER JOIN sch_setup_other_expense AS se ON d.otherfeeid = se.otherfeeid
									WHERE
										date(d.trandate) >= '".$this->green->formatSQLDate($from_date)."'
									AND date(d.trandate) <= '".$this->green->formatSQLDate($to_date)."'
									GROUP BY
										d.otherfeeid
									ORDER BY se.otherfee ASC ");
		
		$total_ex = 0;
		$j = 1;
		$tr .= '<tr>
					<td colspan="3" style="_font-weight: bold;_background: #F2F2F2;border: 0;line-height: 6px;">&nbsp;</td>
				</tr>';
		$tr .= '<tr>
					<td colspan="3" style="font-weight: bold;background: #f8f8f8;">Expenses</td>
				</tr>';
		if($q_e->num_rows() > 0){			
			foreach($q_e->result() as $row_e){
				$total_ex += $row_e->to_amt - 0;				
				$tr .= '<tr>
							<td>'.$j++.'</td>
							<td>'.$row_e->otherfee.'</td>
							<td style="text-align: right;">'.number_format($row_e->to_amt, 2).'</td>
						</tr>';
			}
			$tr .= '<tr>
						<td style="text-align: right;" colspan="2">Total expenses</td>
						<td style="text-align: right;font-weight: bold;">'.number_format($total_ex, 2).'</td>
					</tr>';
		}		
		else{
			$tr .= '<tr>
						<td colspan="3" style="font-weight: bold;text-align: center;_background: #F2F2F2;">No expenses</td>
					</tr>';
		}

		$tr .= '<tr>
					<td colspan="3" style="_font-weight: bold;_background: #F2F2F2;border: 0;line-height: 6px;">&nbsp;</td>
				</tr>';
				
		// profit/loss =======
		$pl = ($total - 0) - ($total_ex - 0) - 0;
		$tr .= '<tr style="text-align: right;background: #f8f8f8;">
					<td colspan="2">Profit / Loss</td>					
					<td style="text-align: right;font-weight: bold;color: '.($total_ex > $total ? 'red' : '').';">'.number_format($pl, 2).'</td>
				</tr>';

		$arr = array('tr' => $tr);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}

	public function graphic(){	
		$this->load->view('header');
		$this->load->view('profit_loss/v_graphic');
		$this->load->view('footer');	
	}

	public function get_data_graphic(){	
		$yyear = $this->input->post('yyear');

		// revenues =======
		// $this->db->query("DROP TABLE IF EXISTS tbl_revenue");
		$sql = "SELECT
					--DATE_FORMAT(st.trandate, '%b') AS m,
					--MONTH(st.trandate) AS num_m,
					SUM(st.amt_line) AS to_amt
				FROM
					sch_student_fee_type AS st
				WHERE
					YEAR(st.trandate) = '{$yyear}'
				GROUP BY
					MONTH(st.trandate)";
		$sql_ = "CREATE TEMPORARY TABLE tbl_revenue (
					".$sql."
					 ) ";
		$this->db->query($sql_);
		$c = $this->db->query($sql)->num_rows();

		// expenses =======
		// $this->db->query("DROP TABLE IF EXISTS tbl_expense");
		$sql1 = "SELECT
					--DATE_FORMAT(m.tran_date, '%b') AS m,
					--MONTH(m.tran_date) AS num_m,
					SUM(d.amt) AS to_amt
				FROM
					sch_other_expense_master AS m
				INNER JOIN sch_other_expense_detail AS d ON m.typeno = d.typeno
				INNER JOIN sch_setup_other_expense AS se ON d.otherfeeid = se.otherfeeid
				WHERE
					YEAR(d.trandate) = '{$yyear}'
				GROUP BY
					MONTH(m.tran_date) ";			
		$sql1_ = "CREATE TEMPORARY TABLE tbl_expense (
					".$sql1."
					 ) ";
		$this->db->query($sql1_);
		$c_ = $this->db->query($sql1)->num_rows();

		$arr_r = [];
		$arr_ex = [];
		$to_revenue = 0;
		$to_expense = 0;		
		
		for($m_ = 1; $m_ <= 12; $m_++){
			// revenues ======
			$to_amt = $this->db->query("SELECT to_amt FROM tbl_revenue WHERE tbl_revenue.num_m = '{$m_}' ");
			if($to_amt->num_rows > 0){
				$to_revenue += $to_amt->row()->to_amt - 0;
				$arr_r[] = $to_amt->row()->to_amt;				
			}
			else{
				$arr_r[] = '';				
			}

			// expenses ======
			$to_amt_ex = $this->db->query("SELECT to_amt FROM tbl_expense WHERE tbl_expense.num_m = '{$m_}' ");			
			if($to_amt_ex->num_rows > 0){
				$to_expense += $to_amt_ex->row()->to_amt - 0;
				$arr_ex[] = $to_amt_ex->row()->to_amt;
			}
			else{
				$arr_ex[] = '';				
			}

		}

		$arr = ['arr_r' => $arr_r, 'arr_ex' => $arr_ex, 'to_revenue' => $to_revenue, 'to_expense' => $to_expense, 'c' => $c, 'c_' => $c_];
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
		$this->db->query("DROP TABLE IF EXISTS tbl_revenue");
		$this->db->query("DROP TABLE IF EXISTS tbl_expense");		
	}

}