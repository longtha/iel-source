<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_profit_loss extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('profit_loss/v_profit_loss');
		$this->load->view('footer');	
	}

	public function grid(){
		$offset = trim($this->input->post('offset'));
		$limit = trim($this->input->post('limit'));
		$from_date = trim($this->input->post('from_date'));
		$to_date = trim($this->input->post('to_date'));

		// revenues ========
		$q = $this->db->query("SELECT
									SUM(st.amt_line) AS amt,
									st.description
								FROM
									sch_student_fee_type AS st
								WHERE
									date(st.trandate) >= '".$this->green->formatSQLDate($from_date)."'
								AND date(st.trandate) <= '".$this->green->formatSQLDate($to_date)."'
								GROUP BY
									st.otherfee_id
								ORDER BY st.description ASC ");		
		$tr = '';
		$i = 1;
		$total = 0;
		$tr .= '<tr>
					<td colspan="3" style="font-weight: bold;background: #f8f8f8;">Revenues</td>
				</tr>';
		if($q->num_rows() > 0){			
			foreach($q->result() as $row){	
				$total += $row->amt - 0;			
				$tr .= '<tr>
							<td>'.$i++.'</td>
							<td>'.$row->description.'</td>
							<td style="text-align: right;">'.number_format($row->amt, 2).'</td>
						</tr>';
			}
			$tr .= '<tr>
						<td style="text-align: right;" colspan="2">Total revenues</td>
						<td style="text-align: right;font-weight: bold;">'.number_format($total, 2).'</td>
					</tr>';
		}
		else{
			$tr .= '<tr>
						<td colspan="3" style="font-weight: bold;text-align: center;_background: #F2F2F2;">No revenues</td>
					</tr>';
		}

		// expenses =========
		$q_e = $this->db->query("SELECT
										d.otherfeeid,
										m.tran_date,
										SUM(d.amt) AS to_amt,
										se.otherfee_code,
										se.otherfee
									FROM
										sch_other_expense_master AS m
									INNER JOIN sch_other_expense_detail AS d ON m.typeno = d.typeno
									INNER JOIN sch_setup_other_expense AS se ON d.otherfeeid = se.otherfeeid
									WHERE
										date(d.trandate) >= '".$this->green->formatSQLDate($from_date)."'
									AND date(d.trandate) <= '".$this->green->formatSQLDate($to_date)."'
									GROUP BY
										d.otherfeeid
									ORDER BY se.otherfee ASC ");
		
		$total_ex = 0;
		$j = 1;
		$tr .= '<tr>
					<td colspan="3" style="_font-weight: bold;_background: #F2F2F2;border: 0;line-height: 6px;">&nbsp;</td>
				</tr>';
		$tr .= '<tr>
					<td colspan="3" style="font-weight: bold;background: #f8f8f8;">Expenses</td>
				</tr>';
		if($q_e->num_rows() > 0){			
			foreach($q_e->result() as $row_e){
				$total_ex += $row_e->to_amt - 0;				
				$tr .= '<tr>
							<td>'.$j++.'</td>
							<td>'.$row_e->otherfee.'</td>
							<td style="text-align: right;">'.number_format($row_e->to_amt, 2).'</td>
						</tr>';
			}
			$tr .= '<tr>
						<td style="text-align: right;" colspan="2">Total expenses</td>
						<td style="text-align: right;font-weight: bold;">'.number_format($total_ex, 2).'</td>
					</tr>';
		}		
		else{
			$tr .= '<tr>
						<td colspan="3" style="font-weight: bold;text-align: center;_background: #F2F2F2;">No expenses</td>
					</tr>';
		}

		$tr .= '<tr>
					<td colspan="3" style="_font-weight: bold;_background: #F2F2F2;border: 0;line-height: 6px;">&nbsp;</td>
				</tr>';
				
		// profit/loss =======
		$pl = ($total - 0) - ($total_ex - 0) - 0;
		$tr .= '<tr style="text-align: right;background: #f8f8f8;">
					<td colspan="2">Profit / Loss</td>					
					<td style="text-align: right;font-weight: bold;color: '.($total_ex > $total ? 'red' : '').';">'.number_format($pl, 2).'</td>
				</tr>';

		$arr = array('tr' => $tr);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}

	public function graphic(){	
		$this->load->view('header');
		$this->load->view('profit_loss/v_graphic');
		$this->load->view('footer');	
	}

	public function get_data_graphic(){	
		$yyear = $this->input->post('yyear');

		// revenues =======
		$qr = $this->db->query("SELECT
										DATE_FORMAT(st.trandate, '%b') AS m,
										MONTH(st.trandate) AS num_m,
										SUM(st.amt_line) AS to_amt
									FROM
										sch_student_fee_type AS st
									WHERE
										YEAR(st.trandate) = '{$yyear}'
									GROUP BY
										MONTH(st.trandate)
									ORDER BY
										MONTH(st.trandate) ASC ");

		$arr_r = [];
		$arr_r1 = [];
		$to_revenue = 0;		
		if($qr->num_rows() > 0){
			foreach ($qr->result() as $row_r) {
				$arr_r[] = $row_r->m;//$row_r->num_m.'-'.;
				$arr_r1[] = $row_r->to_amt - 0;
				$to_revenue += $row_r->to_amt - 0;
			}
		}

		// expenses =======
		$q = $this->db->query("SELECT
									DATE_FORMAT(m.tran_date, '%b') AS m,
									MONTH(m.tran_date) AS num_m,
									SUM(d.amt) AS to_amt
								FROM
									sch_other_expense_master AS m
								INNER JOIN sch_other_expense_detail AS d ON m.typeno = d.typeno
								INNER JOIN sch_setup_other_expense AS se ON d.otherfeeid = se.otherfeeid
								WHERE
									YEAR(d.trandate) = '{$yyear}'
								GROUP BY
									MONTH(m.tran_date)
								ORDER BY
									MONTH(m.tran_date) ASC ");
		$arr_ex = [];
		$arr_ex1 = [];	
		$to_expense = 0;	
		if($q->num_rows() > 0){
			foreach ($q->result() as $row_r) {
				$arr_ex[] = $row_r->m;//$row_r->num_m.'-'.;
				$arr_ex1[] = $row_r->to_amt - 0;
				$to_expense += $row_r->to_amt - 0;
			}
		}


		// $combine_m_ = array_unique(array_merge($arr_r, $arr_ex));//array_combine($arr_r, $arr_ex);//array_merge

		// $cobine_m = array(3,2,1);
		// $cobine_m_ = asort($cobine_m, SORT_NUMERIC);
		// // print_r($cobine_m_);

		// $age = array("Peter"=>"35", "Joe"=>"43", "Ben"=>"37");
		// asort($age);
		

		// $arr_m = [];
		// if(!empty($combine_m_)){	
		// 	$combine_m = arsort($combine_m_);
		// 	foreach($combine_m as $rr) {//$i = 0; $i < COUNT($cobine_m); $i++
		// 	    echo ($rr[0]);
		// 	}
		// }
		
		// print_r($cobine_m_);
		// echo $arr_m;
		// exit();

		$arr = ['arr_r' => $arr_r, 'arr_r1' => $arr_r1, 'arr_ex' => $arr_ex, 'arr_ex1' => $arr_ex1, 'to_revenue' => $to_revenue, 'to_expense' => $to_expense, 'c' => $qr->num_rows(), 'c_' => $q->num_rows()];
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}

}