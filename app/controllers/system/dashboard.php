<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	protected $thead;
	protected $idfield;
	protected $searchrow;
	function __construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__construct();
		$this->load->library('pagination');
		$this->lang->load('student', 'english');
		$this->load->model('system/ModDashBoard','dash');
		$this->load->model('social/modsocialdash','socdash');
		$this->load->model('school/schoollevelmodel','schlevels');
		$this->idfield="studentid";
	}
	function index(){
		$s_date=date('Y-m-d',strtotime("-3 months"));
		$e_date=date('Y-m-d');
		$s_minage='';
		$s_maxage='';

		$year=$this->session->userdata('year');
		
		$sclevelid = 1;
		$present = '';
		$current_date = date("Y-m-d");
		if($present == 1) {
		    $presents = 'absent_day';
		    $having = 'absent_day > 0';
		    $sum = 'sum(pre.absent_day) as sum_absent';
		}
		else if($present == 2) {
		    $presents = 'permission_day';
		    $having = 'permission_day > 0';
		    $sum = 'sum(pre.permission_day) as sum_permission';
		}
		else if($present == 3) {
		    $presents = 'lateday';
		    $having = 'lateday > 0';
		    $sum = 'sum(pre.lateday) as sum_late';
		}
		else {
		    $presents = 'absent_day,permission_day,lateday';
		    $having = '';
		    $sum = 'sum(pre.absent_day) as sum_absent,sum(pre.permission_day) as sum_permission,sum(pre.lateday) as sum_late';
		}
		
		$sql_list = "select pre.yearid,pre.classid,pre.schlevelid,pre.studentid, pre.$presents
            from (select yearid,classid,schlevelid,studentid ,$presents from sch_studatt_dailydetail
            where date = '$current_date' and schlevelid = $sclevelid
            group by $presents,yearid,classid,studentid ";
		
		if($having !='')
		    $sql_list .= " HAVING $presents > 0) as pre ";
		    else
		        $sql_list .= " ) AS pre where 1=1";
		        $sqls= $this->db->query($sql_list);
		        $query= $sqls->result();
		        $data_present = $query;
		        $count = $sqls->num_rows();
		        
		        $sql_sum = "select pre.yearid,pre.classid,pre.schlevelid,pre.studentid, $sum
            from (select yearid,classid,schlevelid,studentid ,$presents from sch_studatt_dailydetail
            where date = '$current_date' and schlevelid = $sclevelid
            group by $presents,yearid,classid,studentid ) AS pre where 1=1";
		        $sqlsum= $this->db->query($sql_sum)->result_array();
		        
        $data['data_present'] = $data_present;
        $data['sqlsum'] = $sqlsum;
        $data['count'] = $count;
		
		$data['data']=$this->dash->getstudentdash($year);
		$data['social']=$this->dash->getsocialdash($s_date,$e_date);
		$data['health']=$this->dash->gethealthdash($s_date,$e_date);
		$data['dis']=$this->dash->getdistribut($s_date,$e_date);


		$data['date']=array('s_date'=>$s_date,'e_date'=>$e_date,'year'=>$year);
		$data['date1']=array('sdate'=>$s_date,'edate'=>$e_date);
		$data['age']=array('s_minage'=>$s_minage,'s_maxage'=>$s_maxage);
		
		$data['schevels']=$this->schlevels->getsch_level();

		$data['emp']=$this->dash->getempdash();
		$data['thead']=	$this->thead;
		$data['page_header']="Disease List";
		$this->parser->parse('header', $data);
		$this->parser->parse('system/dashboard_form', $data);
		$this->parser->parse('footer', $data);
	}
	function search($year,$s_date='',$e_date='',$sclevelid ='' ,$present = '',$s_minage='',$s_maxage=''){
	    $current_date = date("Y-m-d");
		if($s_date=='')
			$s_date=date('Y-m-d',strtotime("-3 months"));
		if($e_date=='')
			$e_date=date('Y-m-d');
		
		if($present == 1) {
		    $presents = 'absent_day';
		    $having = 'absent_day > 0';
		    $sum = 'sum(pre.absent_day) as sum_absent';
		}
		else if($present == 2) {
		    $presents = 'permission_day';
		    $having = 'permission_day > 0';
		    $sum = 'sum(pre.permission_day) as sum_permission';
		}
		else if($present == 3) {
		    $presents = 'lateday';
		    $having = 'lateday > 0';
		    $sum = 'sum(pre.lateday) as sum_late';
		}
		else {
		    $presents = 'absent_day,permission_day,lateday';
		    $having = '';
		    $sum = 'sum(pre.absent_day) as sum_absent,sum(pre.permission_day) as sum_permission,sum(pre.lateday) as sum_late';
		}
		
		$sql_list = "select pre.yearid,pre.classid,pre.schlevelid,pre.studentid, pre.$presents
            from (select yearid,classid,schlevelid,studentid ,$presents from sch_studatt_dailydetail
            where date = '$current_date' and schlevelid = $sclevelid
            group by $presents,yearid,classid,studentid ";
		
		if($having !='')
		    $sql_list .= " HAVING $presents > 0) as pre ";
		    else
		        $sql_list .= " ) AS pre where 1=1";
		        $sqls= $this->db->query($sql_list);
		        $query= $sqls->result();
		        $data_present = $query;
		        $count = $sqls->num_rows();
		        
		        $sql_sum = "select pre.yearid,pre.classid,pre.schlevelid,pre.studentid, $sum
            from (select yearid,classid,schlevelid,studentid ,$presents from sch_studatt_dailydetail
            where date = '$current_date' and schlevelid = $sclevelid
            group by $presents,yearid,classid,studentid ) AS pre where 1=1";
		        $sqlsum= $this->db->query($sql_sum)->result_array();
		
        $data['data_present'] = $data_present;
        $data['sqlsum'] = $sqlsum;
        $data['count'] = $count;
        

		$data['data']=$this->dash->getstudentdash($year);
		$data['social']=$this->dash->getsocialdash($s_date,$e_date);
		$data['health']=$this->dash->gethealthdash($s_date,$e_date);
		$data['date']=array('s_date'=>$s_date,'e_date'=>$e_date,'year'=>$year);
		$data['date1']=array('sdate'=>$s_date,'edate'=>$e_date);
		$data['age']=array('s_minage'=>$s_minage,'s_maxage'=>$s_maxage);
		$data['frevenue']=$this->dash->getFamRevenu();

		$data['dis']=$this->dash->getdistribut($s_date,$e_date);
		$data['schevels']=$this->schlevels->getsch_level();
		$data['emp']=$this->dash->getempdash($year);
		$data['thead']=	$this->thead;
		$data['page_header']="Disease List";
		$this->parser->parse('header', $data);
		$this->parser->parse('system/dashboard_form', $data);
		$this->parser->parse('footer', $data);
	}
	public function serachbyterm(){
		$s_year = $this->input->post("s_year");
		$s_term_type = $this->input->post("s_term_type");
		$s_term = $this->input->post("s_term");
		$year=$this->db->where('yearid',$s_year)->get('sch_school_year')->row()->sch_year;
		$min_age = $this->input->post("min_age");
		$max_age = $this->input->post("max_age");
		$m = $this->input->post("m");
		$p = $this->input->post("p");
		$showage=0;
		if($min_age!="" || $max_age!=""){
			$showage=1;
		}
		$str="";
		$str.='<tr>
   				<td><label class="control-label">Academy Year </label></td>
   				<td><label class="control-label">'.$year.'</label></td>';
   			// $str.=($showage==1?'<td><label class="control-label">Student by Age</label></td>':'');
   		$str.='</tr>';
   		$yearid=$this->session->userdata('year');
   		$t=0;
		foreach ($this->dash->getStudentInGrade($s_year,$s_term_type,$s_term) as $row) {
			$num_by_age="";
			
			$level='Level ';
			$st_gen="Student";
			$st_gen1="Student";
			$num_by_age="";
			$age_str="";
			if($showage==1){
				$age_str=$min_age."__".$max_age;
				$num_by_age=$this->dash->getNumStdByAgeLev($row->gradeid,$s_year,$min_age,$max_age);
			}
			if($row->total_stu>1) $st_gen.="s";
			if($num_by_age>1) $st_gen1.="s";

			$str.="<tr>
       				<td><label class='control-label'> * $level ".$row->grade."</label></td>
       				<td>
       					<a href='".site_url("/student/student/search?s_id=&fn=&fnk=&class=&s_num=150&year=$yearid&m=&p=&l=&b=&le=$row->gradeid&per_page=0&ag=$age_str&pro=&fone=1")."' target='_blank'>".$row->total_stu." $st_gen</a>
       				</td>";
       		if($showage==1){
     //       		if($num_by_age>0){
					// $str.="<td><a href='".site_url("/student/student/search?s_id=&fn=&fnk=&class=&s_num=150&year=$yearid&m=$m&p=$p&l=&b=&le=$row->gradeid&per_page=0&ag=$age_str&pro&fone=1")."' target='_blank'>".$num_by_age." $st_gen1</a></td>";
     //      		}else{
     //      			$str.="<td>".$num_by_age."</td>";
     //      		}

           	}
       		$str.="</tr>";
       		$t+=$row->total_stu;
		}
		if($t==0){
			$str.='<tr>
       				<td colspan="'.($showage==1?'3':'2').'" style="text-align:center"><label class="control-label">No Data Found</label></td>
       	        </tr>';
		}
		$str.='<tr>
       				<td><label class="control-label"> * Total Number Of Student</label></td>
       				<td><label class="control-label">'.$t.' Students</label></td>';
       	$str.='</tr>';
		$arr=array();
		header('Content-type:text/x-json');
		$arr['list']=$str;
		echo json_encode($arr); 	
		die();
	}
	public function getTerm($yearid,$feetypeid){
		echo json_encode ($this->db->where('yearid',$yearid)->where('feetypeid',$feetypeid)->order_by('termid','ASC')->get('sch_school_term')->result());
	}
	public function getSemester($yearid,$feetypeid){
		echo json_encode ($this->db->where('yearid',$yearid)->where('feetypeid',$feetypeid)->order_by('semesterid','ASC')->get('sch_school_semester')->result());
		
	}
	public function getTermSemesterYear($yearid,$feetypeid){
		echo json_encode ($this->db->where("yearid", $yearid)->where("payment_type", $feetypeid)->group_by('term_sem_year_id')->get("v_study_peroid")->result());
		
	}
	function searchpresent($sclevelid ,$present = '',$year='',$s_date='',$e_date=''){
	    
	    $current_date = date("Y-m-d");
	    if($present == 1) {
	        $presents = 'absent_day';
	        $having = 'absent_day > 0';
	        $sum = 'sum(pre.absent_day) as sum_absent';
        }
	    else if($present == 2) {
	        $presents = 'permission_day';
	        $having = 'permission_day > 0';
	        $sum = 'sum(pre.permission_day) as sum_permission';
	    }
	    else if($present == 3) {
	        $presents = 'lateday';
	        $having = 'lateday > 0';
	        $sum = 'sum(pre.lateday) as sum_late';
	    }
	    else {
	        $presents = 'absent_day,permission_day,lateday';
	        $having = '';
	        $sum = 'sum(pre.absent_day) as sum_absent,sum(pre.permission_day) as sum_permission,sum(pre.lateday) as sum_late';
	    }
	   
	    $sql_list = "select pre.yearid,pre.classid,pre.schlevelid,pre.studentid, pre.$presents
                from (select yearid,classid,schlevelid,studentid ,$presents from sch_studatt_dailydetail 
                where date = '$current_date' and schlevelid = $sclevelid
                group by $presents,yearid,classid,studentid ";
                
                if($having !='')
                    $sql_list .= " HAVING $presents > 0) as pre ";
                else 
                    $sql_list .= " ) AS pre where 1=1";
        $sqls= $this->db->query($sql_list);
	    $query= $sqls->result();
	    $data_present = $query;
	    $count = $sqls->num_rows();
	    
	    $sql_sum = "select pre.yearid,pre.classid,pre.schlevelid,pre.studentid, $sum
                from (select yearid,classid,schlevelid,studentid ,$presents from sch_studatt_dailydetail
                where date = '$current_date' and schlevelid = $sclevelid
                group by $presents,yearid,classid,studentid ) AS pre where 1=1";
	    $sqlsum= $this->db->query($sql_sum)->result_array();
	    
	    
	    
	    if($s_date=='')
            $s_date=date('Y-m-d',strtotime("-3 months"));
        if($e_date=='')
            $e_date=date('Y-m-d');
        
        $data['data_present'] = $data_present;
        $data['sqlsum'] = $sqlsum;
        $data['count'] = $count;
        $data['schevels']=$this->schlevels->getsch_level();
        $data['data']=$this->dash->getstudentdash(@$year);
        $data['social']=$this->dash->getsocialdash($s_date,$e_date);
        $data['health']=$this->dash->gethealthdash($s_date,$e_date);
        $data['date']=array('s_date'=>@$s_date,'e_date'=>@$e_date,'year'=>@$year);
        $data['date1']=array('sdate'=>$s_date,'edate'=>$e_date);
        $data['age']=array('s_minage'=>@$s_minage,'s_maxage'=>@$s_maxage);
        $data['frevenue']=$this->dash->getFamRevenu();
        
        $data['dis']=$this->dash->getdistribut($s_date,$e_date);
        $data['sclevelid'] = $sclevelid;
        $data['present'] = $present;
        
        $data['emp']=$this->dash->getempdash(@$year);
        $data['thead']=	$this->thead;
        $data['page_header']="Disease List";
        
        $this->parser->parse('header', $data);              
        $this->parser->parse('system/dashboard_form', $data);
        $this->parser->parse('footer', $data);
	}
	
	function searchdis($sdate='',$edate=''){
		$dis=$this->dash->getdistribution($sdate,$edate);
		echo $dis->tota_rice.".".$dis->oil_total;
	}
	//------  Rothna ---------------------------
	function searchvisited($sdate_visited='',$edate_visited=''){
		$sdate_visited=$this->input->post('sdate_visited');
		$edate_visited=$this->input->post('edate_visited');
		$dis=$this->dash->getvisit($sdate_visited,$edate_visited);
		echo $dis->number_visit;
	}
	//count visit
	function connamevisited($sdate_visited='',$edate_visited=''){
		$dis=$this->dash->connamevi($sdate_visited,$edate_visited);
		$data = "";
		foreach ($dis as $row) {
			$data .= '<tr><td id="assistant">'.$row->Fullname.'</td>
				  <td id="times" class="timesname"><a href="'.site_url("system/dashboard/viewsfamlyvisitedname/".$sdate_visited."/".$edate_visited)."/".$row->empid.'" target="_blank">'.$row->c_visited.' Times </a></td>';
			$data .= '</tr>';
		}
		echo $data;
	}
	function viewsfamlyvisitedname($sdate_visited,$edate_visited,$empid){
		$data['data']=$this->db->query("SELECT
											DATE_FORMAT(fv.date, '%d/%m/%Y') AS date,
											fm.family_name,
											fs.empid,
											CONCAT(
												emp.last_name,
												'  ',
												emp.first_name
											) AS Fullname,
											fm.family_code
										FROM
											sch_family AS fm
										INNER JOIN sch_family_visit AS fv ON fv.familyid = fm.familyid
										INNER JOIN sch_family_visit_staff AS fs ON fv.visitid = fs.visitid
										INNER JOIN sch_emp_profile AS emp ON fs.empid = emp.empid
										WHERE date
										BETWEEN '".$sdate_visited."'
										AND '".$edate_visited."'
										AND emp.empid='".$empid."' ORDER BY fv.date
										DESC ")->result();
		$data['data1']="Student Leave List";
		$this->parser->parse('header', $data);
		$this->parser->parse('system/list_visitedfamly', $data);
		$this->parser->parse('footer', $data);
	}
	function viewsfamlyvisited($sdate_visited,$edate_visited){
		$data['data']=$this->db->query("SELECT
											DATE_FORMAT(fv.date, '%d/%m/%Y') AS date,
											fm.family_name,
											fs.empid,
											CONCAT(
												emp.last_name,
												'  ',
												emp.first_name
											) AS Fullname,
											fm.family_code
										FROM
											sch_family AS fm
										INNER JOIN sch_family_visit AS fv ON fv.familyid = fm.familyid
										INNER JOIN sch_family_visit_staff AS fs ON fv.visitid = fs.visitid
										INNER JOIN sch_emp_profile AS emp ON fs.empid = emp.empid
										WHERE date
										BETWEEN '".$sdate_visited."'
										AND '".$edate_visited."'
										ORDER BY fv.date
										DESC ")->result();
		$data['data1']="Student Leave List";
		$this->parser->parse('header', $data);
		$this->parser->parse('system/list_visitedfamly', $data);
		$this->parser->parse('footer', $data);
	}
	//------ end Rothna  ------------------

	function search_health_detail(){
		if(isset($_GET['sort_num'])){
			$yearid=$_GET['year'];
			$location=$_GET['location'];
			$category=$_GET['category'];
			$medicine=$_GET['medicine'];
			$s_date=$_GET['s_date'];
			$e_date=$_GET['e_date'];
			$sort_num=$_GET['sort_num'];

			$data['medicine']=$this->dash->searchhealthdetail($sort_num,$location,$category,$medicine,$yearid,$s_date,$e_date);
			$data['idfield']=$this->idfield;
			// $data['medicine']=$this->dash->getmedicine($this->session->userdata('year'));
			$data['thead']=	$this->thead;
			$data['page_header']="Health Detail";
			$this->parser->parse('header', $data);
			$this->parser->parse('system/health_detail', $data);
			$this->parser->parse('footer', $data);
		}else{
			$yearid=$this->input->post('yearid');
			$location=$this->input->post('location');
			$category=$this->input->post('category');
			$medicine=$this->input->post('medicine');
			$sort_num=$this->input->post('sort_num');

			$s_date=$this->input->post('s_date');
			$e_date=$this->input->post('e_date');
			$i=1;

			foreach ($this->dash->searchhealthdetail($sort_num,$location,$category,$medicine,$yearid,$s_date,$e_date) as $row) {
				echo "<tr>
						<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
						<td>".$row->wharehouse."</td>
						<td>".$row->description."</td>
						<td>".$row->descr_eng."</td>
						<td>".$row->qty."</td>
						<td>".$row->uom."</td>";

					echo "</td>
				</tr>
				 ";
				 $i++;
			}
			echo "<tr class='remove_tag'>
						<td colspan='12' id='pgt'>
							<div style='margin-top:20px; width:10%; float:left;'>
							Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
											$num=50;
											for($i=0;$i<10;$i++){?>
												<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
												<?php $num+=50;
											}
										echo "</select>
							</div>
							<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
								<ul class='pagination' style='text-align:center'>
								 ".$this->pagination->create_links()."
								</ul>
							</div>

						</td>
					</tr> ";
		}
	}
	function view_std($yearid){
		$data['date']=array('year'=>$yearid);
		$data['data']=$this->dash->getstudentdash($yearid);
		$data['page_header']="Disease List";
		$this->parser->parse('header', $data);
		$this->parser->parse('system/student_detail', $data);
		$this->parser->parse('footer', $data);
	}
	function viewstdleave(){
		$year=$this->session->userdata('year');
		$data['data']=$this->db->query("SELECT CONCAT(ss.last_name,' ',ss.first_name) AS fullname,ss.leave_sch_date,ss.leave_reason FROM sch_student AS ss										
										WHERE ss.leave_school='1' ")->result();
		$data['page_header']="Student Leave List";
		$this->parser->parse('header', $data);
		$this->parser->parse('system/list_stdleave', $data);
		$this->parser->parse('footer', $data);
	}
	
	function viewstdnew(){
	    $year=$this->session->userdata('year');
	    $current_date = date("Y-m-d");
	    $data['data']=$this->db->query("SELECT
				CONCAT(s.last_name,' ',s.first_name) AS fullname,
						s.studentid,
						s.sch_year,
						s.familyid,
						s.yearid,
						s.student_num,
						s.register_date,
						s.class_name,
						s.classid
                FROM v_student_profile AS s
                WHERE  DATEDIFF('$current_date',s.register_date) <= 75 and leave_school = 0 and s.is_active = 1
                GROUP BY s.studentid,
						s.sch_year,
						s.familyid,
						s.yearid,
						s.student_num,
						s.register_date,
						s.class_name,
						s.classid")->result();
	    $data['page_header']="Student New List";
	    $this->parser->parse('header', $data);
	    $this->parser->parse('system/list_stdnew', $data);
	    $this->parser->parse('footer', $data);
	}
	
	function viewstdyetpayment(){
	    $year=$this->session->userdata('year');
	    $current_date = date("Y-m-d");
	    $data['data']=$this->db->query("SELECT *,CONCAT(last_name_kh,' ',first_name_kh) AS fullname FROM v_student_fee_inv_list
                            				WHERE amt_balance > 0")->result();
	    $data['page_header']="Student Yet Payment List";
	    $this->parser->parse('header', $data);
	    $this->parser->parse('system/viewstdyetpayment', $data);
	    $this->parser->parse('footer', $data);
	}
	
	function viewstudentbyprogram(){
	    
	    $page=0;
	    if(isset($_GET['per_page'])) $page=$_GET['per_page'];
	    $p='';
	    $programid = '';
	    $is_pt = '';
	    $sort_num = '';
	   
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
	    if(isset($_GET['programid'])){
	        $programid=$_GET['programid'];
	    }
	    if(isset($_GET['is_pt'])){
	        $is_pt=$_GET['is_pt'];
	    }
	 
	    if(isset($_GET['s_num'])) $sort_num=$_GET['s_num'];
	
	    $year=$this->session->userdata('year');
	    $current_date = date("Y-m-d");
	    if(isset($_GET['is_pt'])){
	       $sql_page = "SELECT 
                            CONCAT(s.last_name,' ',s.first_name) AS fullname,s.studentid,
                        						s.sch_year,
                        						s.familyid,
                        						s.yearid,
                        						s.student_num,
                        						s.register_date,
                        						s.class_name,
                                                sc.program
                         FROM v_student_profile as s
                        LEFT JOIN v_sch_class as sc ON(sc.classid = s.classid)
        				WHERE s.programid = {$programid} and s.is_active = 1 and s.leave_school = 0 and sc.is_active = 1 and sc.is_pt = $is_pt
                         GROUP BY s.studentid,
						s.sch_year,
						s.familyid,
						s.yearid,
						s.student_num,
						s.register_date,
						s.class_name";
	    }else{
	        $sql_page = "SELECT
				CONCAT(s.last_name,' ',s.first_name) AS fullname,
						s.studentid,
						s.sch_year,
						s.familyid,
						s.yearid,
						s.student_num,
						s.register_date,
						s.class_name,
                        p.program
                FROM v_student_profile AS s
                LEFT JOIN sch_school_program as p ON(p.programid = s.programid)
                WHERE  s.leave_school = 0 and s.programid = {$programid} and s.is_active = 1
                GROUP BY s.studentid,
						s.sch_year,
						s.familyid,
						s.yearid,
						s.student_num,
						s.register_date,
						s.class_name ";
	        
	    }
	    
	    
	    $this->load->library('pagination');
	    $config['base_url'] = site_url()."/system/dashboard/viewstudentbyprogram?programid=$programid&s_num=$sort_num&is_pt=$is_pt";
	    $config['total_rows'] = $this->green->getTotalRow($sql_page);
	    $config['per_page'] =  $page>0 ? $page : 10;
	    //$config['use_page_numbers'] = TRUE;
	    $config['num_link']=10;
	    $config['page_query_string'] = TRUE;
	    $config['full_tag_open'] = '<li>';
	    $config['full_tag_close'] = '</li>';
	    $config['cur_tag_open'] = '<a><u>';
	    $config['cur_tag_close'] = '</u></a>';
	    
	    $this->pagination->initialize($config);
	    
	    if($page>0){
	        $limi=" limit ".$page.",".$config['per_page'];
	    }else
	        $limi=" limit ".$config['per_page'];
	    
	    $sql_page.=" {$limi}";
	    $data['data']=$this->green->getTable($sql_page);
	    $data['programid'] = $programid;
	    
	    $data['page_header']="Student By Program";
	    $this->parser->parse('header', $data);
	    $this->parser->parse('system/viewstudentbyprogram', $data);
	    $this->parser->parse('footer', $data);
	}
	
	function dispaybylimit(){
	   
	   
	        $sort 		  = $this->input->post('sort');
	        $sort_num 	  = $this->input->post('sort_num');
	        $programid    = $this->input->post('programid');
	        $is_pt    = $this->input->post('is_pt');
	        
	        if($is_pt != '' || $is_pt = null){
	            $sql = "SELECT CONCAT(s.last_name,' ',s.first_name) AS fullname,s.studentid,
                        						s.sch_year,
                        						s.familyid,
                        						s.yearid,
                        						s.student_num,
                        						s.register_date,
                        						s.class_name,
                                                sc.program
                         FROM v_student_profile as s
                        LEFT JOIN v_sch_class as sc ON(sc.classid = s.classid)
        				WHERE s.programid = {$programid} and s.is_active = 1 and s.leave_school = 0 and sc.is_active = 1 and sc.is_pt = $is_pt
                         GROUP BY s.studentid,
						s.sch_year,
						s.familyid,
						s.yearid,
						s.student_num,
						s.register_date,
						s.class_name";
	        }else{
	            $sql = "SELECT
				CONCAT(s.last_name,' ',s.first_name) AS fullname,
						s.studentid,
						s.sch_year,
						s.familyid,
						s.yearid,
						s.student_num,
						s.register_date,
						s.class_name,
                        p.program
                FROM v_student_profile AS s
                LEFT JOIN sch_school_program as p ON(p.programid = s.programid)
                WHERE  s.leave_school = 0 and s.programid = {$programid} and s.is_active = 1
                GROUP BY s.studentid,
						s.sch_year,
						s.familyid,
						s.yearid,
						s.student_num,
						s.register_date,
						s.class_name ";
	            
	        }
	        
	        $this->load->library('pagination');
	        $config['base_url'] = site_url()."/system/dashboard/viewstudentbyprogram?programid=$programid&s_num=$sort_num&is_pt=$is_pt";
	        $config['total_rows'] = $this->green->getTotalRow($sql);
	        $config['per_page'] = $sort_num;
	        $config['num_link'] = 5;
	        $config['page_query_string'] = TRUE;
	        $config['full_tag_open'] = '<li>';
	        $config['full_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<a><u>';
	        $config['cur_tag_close'] = '</u></a>';
	        $this->pagination->initialize($config);
	        $page = 0;
	        $limi = '';
	        if ($sort_num != 'all') {
	            $limi = " limit " . $config['per_page'];
	            if ($page > 0) {
	                $limi = " limit " . $page . "," . $config['per_page'];
	            }
	        }
	        
	        $sql .= " $sort {$limi}";	        
	        $query = $this->green->getTable($sql);
	        
	        $i=1;
	        $data='';
	        foreach($query as $row){
	            
	                $data.="<tr>
		                					<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
		                					<td>".$row['fullname']."</td>
                                            <td>".$row['sch_year']."</td>
		                					<td>".$row['program']."</td>
		                					<td>".$row['class_name']."</td>";
	                $data.="</tr>";
	                $i++;
	        }
	        $data.="<tr class='remove_tag'>
						<td colspan='12' id='pgt'>
							<div style='margin-top:20px; width:20%; float:left;'>
                            <div style='float: left;margin-right:10px;'>
							     Display : <select id='sort_num'  style='padding:5px; margin-right:0px;'>
	                    
                            ";
	                       
	        $num=10;
	        for($i=0;$i<10;$i++){
	            $select='';
	            if($num==$sort_num)
	                $select='selected';
	                $data.="<option value='".$num."' $select > $num</option>";
	                $num+=10;
	        }
	        $select='';
	        if($num=='all')
	            $select='selected';
	            $data.="<option value='all' $select>All</option>
	            
								</select>
                                
							</div>
                            <input type='button' style='float: left;' value='show' class='btn btn-primary' id='shows' name='Show' onclick='dispaybylimit(event);'>
                            </div>							
                            <div style='text-align:center; verticle-align:center; width:80%; float:right;'>
								<ul class='pagination' style='text-align:center'>
								 ".$this->pagination->create_links()."
								</ul>
							</div>
										    
						</td>
					</tr> ";
	            $arr=array('data'=>$data);
	            header("Content-type:text/x-json");
	            echo json_encode($arr);
	    
	}
	
	function viewteacher(){
	    	   
	    if($_GET['position'] == 'gep') 
	        $position= 'gep teacher';
        elseif($_GET['position'] == 'iep')
            $position= 'iep teacher';
        elseif($_GET['position'] == 'kgp')
            $position= 'kgp teacher';
        elseif($_GET['position'] == 'chinese')
            $position= 'chinese teacher';
	    	   
	    $sql_page = "SELECT
    				CONCAT(s.last_name,' ',s.first_name) AS fullname,
    						s.sex,
    						s.phone
                    FROM sch_emp_profile AS s
                    LEFT JOIN sch_emp_position as p ON(p.posid = s.pos_id)
                    WHERE  p.position = '{$position}' and s.is_active = 1 and p.is_active = 1";
	    	    	    
	        $data['data']=$this->green->getTable($sql_page);
	        
	        $data['page_header']="Teacher By Program";
	        $this->parser->parse('header', $data);
	        $this->parser->parse('system/viewteacher', $data);
	        $this->parser->parse('footer', $data);
	}
	
	function view_staff(){
		//$data['date']=array('year'=>$yearid);
		$data['data']=$this->dash->getempdetail();
		$data['getall']=$this->dash->getallpostion();
		$data['page_header']="Disease List";
		$this->parser->parse('header', $data);
		$this->parser->parse('system/employee_detail', $data);
		$this->parser->parse('footer', $data);
	}
	function view_health($s_date,$e_date){
		$data['page_header']="Social Summary Report";
		$data['date']=array('s_date'=>$s_date,'e_date'=>$e_date);
		$data['health']=$this->dash->gethealthdash($s_date,$e_date);
		$data['medicine']=$this->dash->getmedicine($this->session->userdata('year'));
		$this->parser->parse('header', $data);
		$this->parser->parse('system/healthdash_view', $data);
		$this->parser->parse('footer', $data);
	}
	function view_health_detail(){
		$data['page_header']="Health Detail Report";
		$data['medicine']=$this->dash->getmedicine($this->session->userdata('year'));
		$this->parser->parse('header', $data);
		$this->parser->parse('system/health_detail', $data);
		$this->parser->parse('footer', $data);
	}
	function view_soc($yearid,$sdate,$edate){
		$data['date']=array('year'=>$yearid);
		$data['page_header']="Social Summary Report";
		$data['statistic_header_tittle']="Statistic Family By Location";
		$data['data']=$this->socdash->getStdbyFam($yearid);
		$data['datarice']=$this->socdash->familyRice();
		$data['totalrice']=$this->socdash->familyRicetotal();
		$data['member']=$this->dash->getfmem();
		$data['femalemem']=$this->dash->getFemaleMem();
		$data['frevenue']=$this->dash->getFamRevenu();
		$data['fetchcommunce'] = $this->dash->summarystatfamloc();
		$data['dis']=$this->dash->getDis($sdate,$edate);
		$data['disdate']=array('sdate'=>$sdate,'edate'=>$edate);
		$this->parser->parse('header', $data);
		$this->parser->parse('social/vsocialdash', $data);
		$this->parser->parse('footer', $data);
	}
	public function vsflocation(){
		$data['page_header']="Statistic Family By Location";
		$data['countcommunce']=$this->dash->countcommunce();
		$data['sfl']=$this->dash->staticfamilylocation();
		$this->parser->parse('header', $data);
		$this->parser->parse('social/static_family_location', $data);
		$this->parser->parse('footer', $data);
	}
	
}
