<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sys extends CI_Controller {

	public function __construct(){
		parent::__construct();		
		$this->load->helper(array('form', 'url'));		
	}
	public function dupcheck()
	{	
		$ref_val=$_POST['ref_val'];
		$ref_field=$_POST['ref_field'];
		$ref_table=$_POST['ref_table'];
		$arrJson=array();
		if($ref_val!="" && $ref_field!="" && $ref_table!=""){
			$arrJson['isDup']=$this->green->getValue("SELECT COUNT({$ref_table}.{$ref_field}) as num 
																FROM {$ref_table} 
																WHERE {$ref_table}.{$ref_field}='{$ref_val}'
															")-0;			
			
		}	

		echo header("Content-type:text/x-json");
		echo json_encode($arrJson) ;
		exit();
	}

	//=========== for run script =========

	public function runMultiQuery($query){

		$mysqli = new mysqli("localhost", "greensim", "GrEEnICT#9999", "greensim_v4_01");
		/* check connection */
		if (mysqli_connect_errno()) {
		    printf("Connect failed: %s\n", mysqli_connect_error());
		    exit();
		}	
		if($query!=""){
			$mysqli->multi_query($query);
		}
		/*	muli query 	
		$query="SELECT
				sch_student.studentid,
				sch_student.student_num,
				sch_student.first_name,
				sch_student.last_name
			FROM
				sch_student
			LIMIT 0,
			 10
			;
			SELECT				sch_student.studentid,
				sch_student.student_num,
				sch_student.first_name,
				sch_student.last_name
			FROM
				sch_student
			LIMIT 11,
			 10
			;
			";		
		// execute multi query 
		if ($mysqli->multi_query($query)) {
		    do {
		        // store first result set 
		        if ($result = $mysqli->store_result()) {
		            while ($row = $result->fetch_assoc()) {
		               echo $row['first_name']." ";
		            }
		            $result->free();
		        }
		        // print divider 
		        if ($mysqli->more_results()) {
		            printf("-----------------\n");
		        }
		    } while ($mysqli->next_result());
		}
		
		/* close connection */
		$mysqli->close();

	}
	function runQuery(){
		$sql="UPDATE sch_stud_gifdonate SET transno='53'  where transno='9' and donateid='59';
UPDATE sch_stud_gifdonate SET transno='54'  where transno='11' and donateid='61';
UPDATE sch_stud_gifdonate SET transno='55'  where transno='12' and donateid='62';
UPDATE sch_stud_gifdonate SET transno='56'  where transno='13' and donateid='63';
UPDATE sch_stud_gifdonate SET transno='57'  where transno='14' and donateid='64';
UPDATE sch_stud_gifdonate SET transno='58'  where transno='15' and donateid='65';
UPDATE sch_stud_gifdonate SET transno='59'  where transno='16' and donateid='66';
UPDATE sch_stud_gifdonate_detail SET transno='53'  where transno='9' and donateid='59';
UPDATE sch_stud_gifdonate_detail SET transno='54'  where transno='11' and donateid='61';
UPDATE sch_stud_gifdonate_detail SET transno='55'  where transno='12' and donateid='62';
UPDATE sch_stud_gifdonate_detail SET transno='56'  where transno='13' and donateid='63';
UPDATE sch_stud_gifdonate_detail SET transno='57'  where transno='14' and donateid='64';
UPDATE sch_stud_gifdonate_detail SET transno='58'  where transno='15' and donateid='65';
UPDATE sch_stud_gifdonate_detail SET transno='59'  where transno='16' and donateid='66';

";
		$this->runMultiQuery($sql);
	}
	
	function backupdb(){
		// Load the DB utility class
		$this->load->dbutil();

		// Backup your entire database and assign it to a variable
		$backup =& $this->dbutil->backup();

		// Load the file helper and write the file to your server
		$this->load->helper('file');
		write_file('/path/to/mybackup.gz', $backup);

		// Load the download helper and send the file to your desktop
		$this->load->helper('download');
		force_download('mybackup.gz', $backup); 
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */