<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_student_assignclass extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('student_fee/m_student_assignclass', 'ac');
        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolyearmodel', 'y');
        $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');
        $this->load->model('school/modfeetype', 'f');
        $this->load->model("school/classmodel", "c");
		
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('student_fee/v_student_assignclass');
		$this->load->view('footer');	
	}

	public function test(){	
		$this->load->view('header');
		$this->load->view('student_fee/test');
		$this->load->view('footer');	
	}

	public function grid(){
    	$ac = $this->ac->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $ac;
    }

    public function FsaveRangClass(){
		// alert(+'--'++'--'++'--'++'--'++'--'++'--'++'--'+new_range+'--'+);
		$tran_date=date('Y-m-d');
		$user=$this->session->userdata('user_name');
		$dadats =array(
						'studentid' =>$this->input->post('studentid'),
						'programid' =>$this->input->post('programid'),
						'schoolid' =>$this->input->post('schoolid'),
						'schlevelid' =>$this->input->post('schlevelid'),
						'year' =>$this->input->post('yearid'),
						'from_rangelevelid' =>$this->input->post('rangelevelid'),
						'from_classid' =>$this->input->post('classid'),
						'to_class' =>$this->input->post('new_class'),
						'to_rangle' =>$this->input->post('new_range'),	
						'create_by' =>$user,						
						'tran_date' =>$tran_date,						
						);
		$this->db->insert('sch_student_assign_class', $dadats);
		
		$datas="SELECT
					sch_student_enrollment.studentid,
					sch_student_enrollment.schoolid,
					sch_student_enrollment.`year`,
					sch_student_enrollment.classid,
					sch_student_enrollment.enroll_date,
					sch_student_enrollment.type_of_enroll,
					sch_student_enrollment.is_rollover,
					sch_student_enrollment.rollover_date,
					sch_student_enrollment.rollover_by,
					sch_student_enrollment.type,
					sch_student_enrollment.transno,
					sch_student_enrollment.schlevelid,
					sch_student_enrollment.rangelevelid,
					sch_student_enrollment.feetypeid,
					sch_student_enrollment.programid,
					sch_student_enrollment.is_closed,
					sch_student_enrollment.is_paid,
					sch_student_enrollment.term_sem_year_id
				FROM
					sch_student_enrollment
				where 1=1
					AND studentid='".$this->input->post('studentid')."'
					AND programid='".$this->input->post('programid')."'
					AND schoolid='".$this->input->post('schoolid')."'
					AND schlevelid='".$this->input->post('schlevelid')."'
					AND year='".$this->input->post('yearid')."'
					AND rangelevelid='".$this->input->post('rangelevelid')."'
					AND classid='".$this->input->post('classid')."'
				";
		// echo $datas; exit();
		
		$result = $this->db->query($datas)->result();
		$datapayment="";
		if(isset($result) && count($result)>0){		
			foreach($result as $myrow){
			 $da = array(									
						'studentid' =>$myrow->studentid,
						'schoolid' =>$myrow->schoolid,
						'year' =>$myrow->year,
						'classid' =>$this->input->post('new_class'),
						'enroll_date' =>$myrow->enroll_date,
						'type_of_enroll' =>$myrow->type_of_enroll,
						'is_rollover' =>$myrow->is_rollover,
						'rollover_date' =>$myrow->rollover_date,
						'rollover_by' =>$myrow->rollover_by,
						'type' =>$myrow->type,
						'transno' =>$myrow->transno,
						'schlevelid' =>$myrow->schlevelid,
						'rangelevelid' =>$this->input->post('new_range'),
						'feetypeid' =>$myrow->feetypeid,
						'programid' =>$myrow->programid,
						
						'rangelevelid' =>$myrow->rangelevelid,
						'feetypeid' =>$myrow->feetypeid,
						'programid' =>$myrow->programid,
						'is_closed' =>$myrow->is_closed,
						'is_paid' =>$myrow->is_paid,
						'term_sem_year_id' =>$myrow->term_sem_year_id
						);
				$this->db->insert('sch_student_enrollment', $da);
			}
		}// foreach($result as $rowbusfee){
		$updatsql="UPDATE sch_student_enrollment SET 
						is_closed=1
						WHERE 1=1
						AND studentid='".$this->input->post('studentid')."'
						AND programid='".$this->input->post('programid')."'
						AND schoolid='".$this->input->post('schoolid')."'
						AND schlevelid='".$this->input->post('schlevelid')."'
						AND year='".$this->input->post('yearid')."'
						AND rangelevelid='".$this->input->post('rangelevelid')."'
						AND classid='".$this->input->post('classid')."'						
					  ";
					//  echo $updatsql;
		$this->green->runSQL($updatsql);
    }

    public function get_year(){
        $programid = $this->input->post('programid') - 0;
        $schlevelid = $this->input->post('schlevelid') - 0;                
        $get_year = $this->rp->get_year($programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    public function get_rangelevel(){
        $yearid = $this->input->post('yearid') - 0;
        $termid = $this->input->post('termid') - 0;

        $get_rangelevel = $this->ac->get_rangelevel($yearid, $termid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_rangelevel;
    }

    public function get_rangelevel_list(){
        $schlevelid = $this->input->post('schlevelid') - 0;
        // echo $schlevelid;
        // exit();
        // $termid = $this->input->post('termid') - 0;

        $get_rangelevel_list = $this->ac->get_rangelevel_list('', $schlevelid, '');
        header('Content-Type: application/json; charset=utf-8');
        echo $get_rangelevel_list;
    }

    public function get_paymentType(){
        $feetypeid = $this->input->post('feetypeid');
        $programid = $this->input->post('programid');
        $schlevelid = $this->input->post('schlevelid');
        $yearid = $this->input->post('yearid');       

        $get_paymentType = $this->rp->get_paymentType($feetypeid, $programid, $schlevelid, $yearid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_paymentType;
    }

}