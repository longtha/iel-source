<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_student_fee_invoice extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('student_fee/m_student_fee_invoice', 'rp');
        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolyearmodel', 'y');
        $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');
        $this->load->model('school/modfeetype', 'f');
        $this->load->model('setup/modotherfee', 'otherfee');
		$this->load->model("setup/area_model","marea");
        $this->load->model("school/classmodel", "c");
		$this->load->model("school/modrangelevelfee","mranglevelfee");

		$this->thead = array("No" => 'no',
	        					"Photo"=>'Photo',
	        					"Student Name"=>'student_num',
	        					"Program"=>'programid',
	        					"School Level"=>'schlevelid',
	        					"Year"=>'year',
	        					"Range Level"=>'rangelevelid',
	        					"Class"=>'classid',
	        					"Study Period"=>'period',
	        					"Status"=>'Status'
	                            );
	        $this->idfield = "studentid";
	}

	public function index(){	
		$data['idfield'] = $this->idfield;
        $data['thead'] = $this->thead;
		$this->load->view('header',$data);
		$this->load->view('student_fee/v_student_fee_invoice',$data);
		$this->load->view('footer',$data);	
	}

	public function test(){	
		$this->load->view('header');
		$this->load->view('student_fee/test');
		$this->load->view('footer');	
	}

	public function grid(){
    	$rp = $this->rp->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $rp;
    }

    public function get_schlevel(){
        $programid = $this->input->post('programid') - 0;        
        $get_schlevel = $this->rp->get_schlevel($programid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel;
    }

    public function get_year(){
        $programid = $this->input->post('programid') - 0;
        $schlevelid = $this->input->post('schlevelid') - 0;                
        $get_year = $this->rp->get_year($programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    public function get_rangelevel(){
        $yearid = $this->input->post('yearid') - 0;
        $termid = $this->input->post('termid') - 0;

        $get_rangelevel = $this->rp->get_rangelevel($yearid, $termid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_rangelevel;
    }

    public function get_paymentType(){
        $feetypeid = $this->input->post('feetypeid');
        $programid = $this->input->post('programid');
        $schlevelid = $this->input->post('schlevelid');
        $yearid = $this->input->post('yearid');       

        $get_paymentType = $this->rp->get_paymentType($feetypeid, $programid, $schlevelid, $yearid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_paymentType;
    }
//--------------
    public function Fcstudentinfo(){
        $enrollid = $this->input->post('enrollid');
        $studentid = $this->input->post('studentid');
        $isaddnewinv = $this->input->post('isaddnewinv');
        $attr_is_pt = $this->input->post('attr_is_pt');
        $get_paymentType = $this->rp->Fcstudentinfo($enrollid,$isaddnewinv,$studentid,$attr_is_pt);
		
        header('Content-Type: application/json; charset=utf-8');
        echo $get_paymentType;
    }
// -----------

	public function FCgetPaymenttype(){
		$paymentmethod = $this->input->post('paymentmethod');
		$schlevelid = $this->input->post('shlevelid');
		$yearid = $this->input->post('acandemicid');
		$h_program = $this->input->post('h_program');
		$schoolid=$this->session->userdata('schoolid');
        $ranglevid=$this->input->post('ranglevid');
		
		$datas="SELECT
						v_study_peroid.period,
						v_study_peroid.term_sem_year_id
						FROM
						v_study_peroid
						WHERE 1=1
						AND programid='".$h_program."'
						AND schlevelid='".$schlevelid."'
						AND yearid='".$yearid."'
						AND payment_type='".$paymentmethod."'
						AND schoolid='".$schoolid."'
						AND rangelevelid='".$ranglevid."'
						";
		// echo $datas;				
		$result = $this->db->query($datas)->result();
		
			$datapayment="";
			$datapayment="<option value=''>None</option>";
			if(isset($result) && count($result)>0){		
				foreach($result as $rowbusfee){
					$datapayment.="<option value='".$rowbusfee->term_sem_year_id."'>".$rowbusfee->period."</option>";
				}
			}
			$arr['gpaymenttype']=$datapayment;
			header("Content-type:text/x-json");
			echo json_encode($arr);	
		// echo $datas; exit();
			
	}
	
	public function FCpaymenttyp_price(){
		$yearid = $this->input->post('acandemicid');
		$schlevelid = $this->input->post('shlevelid');
		$rangelevelid = $this->input->post('ranglevid');
		$feetypeid = $this->input->post('paymentmethod');
		$term_sem_year_id = $this->input->post('paymenttype');
		$h_program=$this->input->post('h_program');
		$h_class=$this->input->post('h_class');
		
		// rangelevelfee($yearid="",$schlevelid="",$rangelevelid="",$feetypeid="",$term_sem_year_id="");
		$rows=$this->mranglevelfee->rangelevfee($yearid,$schlevelid,$rangelevelid,$feetypeid,$term_sem_year_id);
		
		
		$sql="SELECT
					COUNT(studentid) AS studentid
					FROM
						sch_student_fee
					WHERE 1=1
					AND studentid='".$this->input->post('h_studentid')."'
					AND programid='".$h_program."'
					AND schooleleve='".$schlevelid."'
					AND acandemic='".$yearid."'
					AND ranglev='".$rangelevelid."'
					AND paymentmethod='".$feetypeid."'
					AND paymenttype='".$term_sem_year_id."'
					";
			 $count = $this->db->query($sql)->row()->studentid;
			 
			$studentid=$count;
			// echo $sql;
		
		$arr['studentid']=$studentid;
		$arr['rows']=$rows;
		
		
		header("Content-type:text/x-json");
		echo json_encode($arr);
		// echo json_encode($studentid);
		
	}
	
	public function FCsave(){
		$studentid = $this->input->post('stuid_pos');
		$this->rp->MFsave($studentid);
	}
	
	public function FCVoid(){
		$deltypeno = $this->input->post('deltypeno');
		$this->rp->FMVoid($deltypeno);
		header("Content-type:text/x-json");
		echo json_encode($deltypeno);
	}
	
	public function FCDelRec(){
		$deltypeno = $this->input->post('deltypeno');
		$this->rp->FMDelRec($deltypeno);
		header("Content-type:text/x-json");
		echo json_encode($deltypeno);
	}
	
	public function FCDelinv(){
		$deltypeno = $this->input->post('deltypeno');
		$this->rp->FMDelinv($deltypeno);
		header("Content-type:text/x-json");
		echo json_encode($deltypeno);
	}
	//------------------------
	public function FCbusfee(){
		$areaid = $this->input->post('oparea');	
		
		$datas="SELECT
						sch_setup_area.area,
						sch_setup_bus.busno,
						sch_setup_busarea.busareaid,
						sch_setup_busarea.busid,
						sch_setup_busarea.areaid
					FROM
						sch_setup_busarea
					INNER JOIN sch_setup_bus ON sch_setup_busarea.busid = sch_setup_bus.busid
					INNER JOIN sch_setup_area ON sch_setup_busarea.areaid = sch_setup_area.areaid
					where 1=1
					AND sch_setup_busarea.areaid='".$areaid."'

				";
		// echo $datas; exit();
			
		$result = $this->db->query($datas)->result();
		 // print_r($classes);
		$databusfee="";	
		if(isset($result) && count($result)>0){		
			foreach($result as $rowbusfee){
				$databusfee.="<option attr_pric=".$rowbusfee->busareaid." value='".$rowbusfee->busid."'>".$rowbusfee->busno."</option>";
			}
		}
		
		$arr['busfe']=$databusfee;
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}
	//-------------------
	
	public function Fcstudytime(){
		$ranglev = $this->input->post('ranglev');	
		$sqltime="SELECT
							sch_time.from_time,
							sch_time.rangelevtimeid,
							sch_time.am_pm,
							sch_time.to_time
							FROM
							sch_school_rangelevtime
							INNER JOIN sch_time ON sch_school_rangelevtime.timeid = sch_time.timeid
							where 1=1 
							AND rangelevelid='".$ranglev."'
						  ";
		// echo $datas; exit();
			
		$result = $this->db->query($datas)->result();
		 // print_r($classes);
		$databusfee="";	
		if(isset($result) && count($result)>0){		
			foreach($result as $rowbusfee){
				$databusfee.="<option attr_pric=".$rowbusfee->busareaid." value='".$rowbusfee->rangelevtimeid."'>".$rowbusfee->busno."</option>";
			}
		}
		$arr['busfe']=$databusfee;
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}
	
	
}// maind