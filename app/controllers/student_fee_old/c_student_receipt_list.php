<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_student_receipt_list extends CI_Controller {
		
	function __construct(){
		parent::__construct();
		$this->lang->load('student', 'english');
		$this->load->model("student_fee/m_student_fee","mstu_fee");	
		$this->load->model("setup/modotherfee","mstu_other_fee");	
		$this->load->model("setting/usermodel","user");
		
		$this->load->model("school/program","mprogram");		
		$this->load->model("school/schoollevelmodel","mshl");	
		$this->load->model("school/schoolyearmodel","mgeyear");
		$this->load->model("school/modrangelevel","mranglevel");
		$this->load->model("school/modrangelevelfee","mranglevelfee");
		$this->load->model("school/classmodel","mclass");
		$this->load->model("school/modfeetype","mfeetype");
		$this->load->model("setup/area_model","marea");
	}
	
	
	function index()
	{
		$data['page_header']="Student Fee List";
		$data['getprogram']=$this->mprogram->getprograms();
		$data['get_sle']=$this->mshl->getsch_level();	
		$data['getyears']=$this->mgeyear->getschoolyear();	
		$data['getranglevel']=$this->mranglevel->rangelevels();
		$data['classes']=$this->mclass->allclass();
		$data['marea']=$this->marea->FMareas();
	
		$get_receiplist=$this->mstu_fee->MgetReceiptlist();
		
		if(isset($get_receiplist) && count($get_receiplist)>0){
		$tr_stulis="";
		$i=1;
		$total=0;
		
		foreach($get_receiplist as $rowreceipt){
			$link_invioce="<a target='_blank' href='".site_url('student_fee/c_student_print_invoice?FCprint_inv='.$rowreceipt->typeno_inv.'')."'>".$rowreceipt->typeno_inv."</a>";
			$link_receipt="<a target='_blank' href='".site_url('student_fee/c_student_print_receipt?param_reno='.$rowreceipt->typeno.'')."'>".$rowreceipt->typeno."</a>";
			
			
			$programid=($rowreceipt->programid);
			
			$program = '';
			if($programid!=""){
				$program = $this->db->where('programid', $programid)->get('sch_school_program')->row()->program;	
			}
			$schooleleve=($rowreceipt->schooleleve);
        	$chlevel = '';
			if($schooleleve!=""){
				$chlevel = $this->db->where('schlevelid', $schooleleve)->get('sch_school_level')->row()->sch_level;	
			}
        	$sch_year = '';
			$yearid=($rowreceipt->acandemic);
			if($programid!=""){
				$sch_year = $this->db->where('yearid', $yearid)->get('sch_school_year')->row()->sch_year;	
			}
        	
			
			
			
			$tr_stulis.='<tr class="actite">
					<td>'.($i++).'</td>
					
					<td style="display:none">'.$rowreceipt->studentid.'</td>
					<td>'.$rowreceipt->student_num.'</td>
					<td>'.$rowreceipt->first_name_kh.'&nbsp;'.$rowreceipt->last_name_kh.'<br>'.$rowreceipt->first_name.'&nbsp;'.$rowreceipt->last_name.'</td>
					
					<td>'.$rowreceipt->gender.'</td>
					<td>'.$rowreceipt->phone1.'</td>
					
					<td>'.$program.'</td>
					<td>'.$chlevel.'</td>
					<td>'.$sch_year.'</td>				
					<td>'.$this->green->convertSQLDate($rowreceipt->trandate).'</td>
					<td>'.$link_invioce.'</td>
					<td>'.$link_receipt.'</td>
					<td align="right">'.number_format($rowreceipt->amt_paid,2).'&nbsp;$</td>
				  </tr>';
				  $total+=($rowreceipt->amt_paid);
		}// foreach($get_receiplist as $rowreceipt){
			$tr_stulis.='<tr>
					<td colspan="10"></td>
					<td>Total</td>
					<td align="right"><strong>'.number_format($total,2).'&nbsp;$</strong></td>
					</tr>';
		$data['tr_stulis']=$tr_stulis;
	}// if(isset($get_receiplist) && count($get_receiplist)>0){
		
	
		$data['oppayment']=$this->mstu_fee->getpaymentmethod();
			
		$this->parser->parse('header', $data);
		$this->parser->parse('student_fee/v_student_receipt_list.php', $data);
		$this->parser->parse('footer', $data);
	}
}
