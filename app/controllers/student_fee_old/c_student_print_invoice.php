<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_student_print_invoice extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("student_fee/m_student_fee_invoice","mstu_fee");
	}
	
	function index(){
		 
		   	$mo = '';
       		//$mo = $this->db->where('typeno', $_GET['FCprint_inv'])->get('sch_student_fee')->row();
       		$mo = $this->db->query("SELECT *, DATE_FORMAT(trandate, '%d-%m-%Y ') AS trandate 
		 							FROM sch_student_fee
		 							WHERE typeno = '".$_GET['FCprint_inv']."' ")->row();
			
			// print_r($mo);
			$type="";
			$typeno="";
			$type=$mo->type;	
			$data['typeno']=$mo->typeno;
			$data['trandate']=$mo->trandate;
			$timeid=$data['timeid']=$mo->timeid;
			
			$data['amt_total']=$mo->amt_total;
			$data['amt_paid']=$mo->amt_paid;
			$data['amt_balance']=$mo->amt_balance;
			
			$data['duedate']=($this->green->convertSQLDate($mo->duedate));
		//	$data['trandate']=($this->green->convertSQLDate($mo->trandate));
			$data['studentcode']=$mo->studentid;
			$data['ranglev']=$mo->ranglev;
			$classid=$mo->classid;			
			$data['acandemic']=$mo->acandemic;		
			$data['programid']=$mo->programid;		
			$data['paymentmethod']=$mo->paymentmethod;
			$data['paymenttype']=$mo->paymenttype;	
			$ranglev=$mo->ranglev;
			
			$data['acandemicname']="";
			if($data['acandemic']!=""){
			$acandemicname = $this->db->where('yearid', $data['acandemic'])->get('sch_school_year')->row()->sch_year;
				$data['acandemicname']=$acandemicname;
			}
			
			
			$notepaid="";
			if($mo->note!=""){
				$notepaid="<u><strong>NOTE:</strong></u>&nbsp;".$mo->note;				
			}
			$data['notepaid']=$notepaid;
			
			$stuinfo ='';
			$stuinfo = $this->db->where('studentid', $data['studentcode'])->get('sch_student')->row();
			
			
			//print_r($stuinfo);
			
			/*$getsql="SELECT
						sch_student.student_num,
						sch_student.first_name,
						sch_student.last_name,
						sch_student.first_name_kh,
						sch_student.last_name_kh,
						sch_student.gender
					FROM
						sch_student
						WHERE 1=1
						AND studentid='".$data['studentid']."'
						";	
			echo $getsql;	*/	
				
				
			$data['student_num']=$stuinfo->student_num;
			$data['first_name_kh']=$stuinfo->first_name_kh;
			$data['last_name_kh']=$stuinfo->last_name_kh;
			$data['gender']=$stuinfo->gender;
			$data['phone1']=$stuinfo->phone1;
			$data['studentid']=$stuinfo->student_num;
			
			$data['fullname']=$stuinfo->last_name.'&nbsp;'.$stuinfo->first_name;
			
			$data['rangelevelname']="";
			
			if($classid!="" and $classid!=0){
				$getClass="SELECT
						sch_class.class_name
						FROM
						sch_class
						WHERE classid='".$classid."'
						";
						// echo $getClass;
						$data['rangelevelname'] = $this->db->query($getClass)->row()->class_name;
			}
			
			if($ranglev!="" and $ranglev!=0){
				//--------
				$sqltime="SELECT
							sch_time.from_time,
							sch_time.am_pm,
							sch_time.to_time
							FROM
							sch_school_rangelevtime
							INNER JOIN sch_time ON sch_school_rangelevtime.timeid = sch_time.timeid
							where 1=1 
							AND rangelevelid='".$ranglev."'
							AND sch_time.timeid='".$timeid."'
						  ";
						 //  echo $sqltime;
						 
				$gettime="";
				$gettime = $this->db->query($sqltime)->result();
				
				$tr_time="";
				$data['from_to_time']="";
				if(isset($gettime) && count($gettime)>0){
					foreach($gettime as $rowde){
						$tr_time.='<tr>
										<td class="inv_info" style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">'.$rowde->from_time.'</td>
										<td style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">-</td>
										<td style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">'.$rowde->to_time.'</td>
										<td style="font-family: Khmer OS battambang;font-size: 12.5px !important;line-height: 11.45px;padding-top: 1px !important;">'.$rowde->am_pm.'</td>
								  </tr>';
					};
					$data['from_to_time']='<table>'.$tr_time.'</table>';
				};
			}// end 
			
		//==============================================================
			$data['period']="";
					$data['start_date']="";
					$data['end_date']="";
					
			$w="";
			if($data['paymentmethod']!="" && $data['paymenttype']!=""){
				$datasele="SELECT
							v_study_peroid.period,
							v_study_peroid.from_date,
							v_study_peroid.to_date
						FROM
							v_study_peroid
						WHERE 1=1								
						AND schlevelid=".$mo->schooleleve."									
						AND schoolid=".$mo->schoolid."	
						AND yearid=".$data['acandemic']."	
						AND programid=".$data['programid']."
						AND payment_type=".$data['paymentmethod']."
						AND term_sem_year_id=".$data['paymenttype']."
						";
					// echo $datasele; 
					$getrow = $this->db->query($datasele)->row();
					
					if(count($getrow)>0){
							$data['period']=$getrow->period;
							
							$data['start_date']=($this->green->convertSQLDate($getrow->from_date));
							$data['end_date']=($this->green->convertSQLDate($getrow->to_date));
					}			
			}		
			//==============================================================
			
			
			$datas="SELECT
			sch_student_fee_type.typeno,
			sch_student_fee_type.type,
			sch_student_fee_type.description,
			sch_student_fee_type.`not`,
			sch_student_fee_type.amt_line,
			sch_student_fee_type.prices,
			sch_student_fee_type.amt_dis,
			sch_student_fee_type.qty
			FROM
			sch_student_fee_type
			where 1=1
			AND sch_student_fee_type.typeno='".$_GET['FCprint_inv']."'
			AND prices <> '' 
			
			";
			// echo $datas; exit();
			
			$result = $this->db->query($datas)->result();
			// print_r($classes);
			$trinv="";	
			$amttotal=0;
			$ii=1;
			if(isset($result) && count($result)>0){	
			$des="";	
			foreach($result as $rowde){
			if($rowde->not !=""){
			$des=$rowde->description.'-'.$rowde->not;	
			}else{
			$des=$rowde->description;
			}
			$trinv.='<tr class="info">
							<td align="center">'.($ii++).'</td>
							<td>'.$des.'</td>
							<td align="right">'.$rowde->qty.'</td>
							<td align="right">'.$rowde->prices.'</td>
							<td align="right">'.$rowde->amt_dis.' %</td>
							<td align="right">'.number_format($rowde->amt_line,2).'$'.'</td>
					</tr>';
					$amttotal+=($rowde->amt_line);
			}
			
			$trinv.='<tr style="display:none">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td align="right" class="">Total</td>
						<td align="right" class="">'.number_format($amttotal,2).'</td>
					</tr>';
			}
			
			
			$data['trinv']=$trinv;
			// $data['trinv']=$trinv;
			
			
		// $this->load->view('student_fee/v_student_invoice');
		$this->parser->parse('student_fee/v_student_print_invoice',$data);
		
	}// function index(){
		
	
		
}
