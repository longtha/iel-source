<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_student_fee extends CI_Controller {
	
	protected $thead;
	protected $thead2;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		$this->lang->load('student', 'english');
		$this->load->model("student_fee/m_student_fee","mstu_fee");	
		$this->load->model("setup/modotherfee","mstu_other_fee");	
		$this->load->model("setting/usermodel","user");
		
		$this->load->model("school/program","mprogram");		
		$this->load->model("school/schoollevelmodel","mshl");	
		$this->load->model("school/schoolyearmodel","mgeyear");
		$this->load->model("school/modrangelevel","mranglevel");
		$this->load->model("school/modrangelevelfee","mranglevelfee");
		$this->load->model("school/classmodel","mclass");
		$this->load->model("school/modfeetype","mfeetype");
		$this->load->model("setup/area_model","marea");
		// $this->load->model("school/modrangelevelfee","mranglevfee");
		
		$this->thead=array("No"=>'No',
							 "StudentID"=>'student_num',
							 "Full Name"=>'last_name',
							 "Full Name(Kh)"=>'last_name_kh',
							 "DOB"=>'dateofbirth',							 
							 "Class"=>'class_name',
							 "Year"=>'yearid',
							 "FamilyID"=>'FamilyID',
							 "Action"=>'Action'							 	
							);
		$this->thead2=array("No"=>'No',
							 "StudentID"=>'student_num',
							 "Full Name"=>'last_name',
							 "Full Name(Kh)"=>'last_name_kh',
							 "Date of Birth"=>'dateofbirth',							 
							 "Class"=>'class_name',
							 "Boarding Date"=>'boarding_date',
							 "FamilyID"=>'FamilyID'					 	
							);
		$this->exp=array("No"=>'No',
							 "StudentID"=>'student_num',
							 "Full Name"=>'fullname',
							 "Full Name(Kh)"=>'fullname_kh',
							 "Date of Birth"=>'dateofbirth',							 
							 "Class"=>'class_name',
							 "Year"=>'sch_year',
							 "Age"=>'age',
							 "Nationality"=>'nationality',
							 "Address"=>'permanent_adr',
							 "Age"=>'age',
							 "Father Name"=>'father_name',
							 "Mother Name"=>'mother_name',
							 "Father Name(KH)"=>'father_name_kh',
							 "Father Ocupation(KH)"=>'father_ocupation_kh',
							 "Mother Name(KH)"=>'mother_name_kh',
							 "Mother Ocupation(KH)"=>'mother_ocupation_kh',	
							 "FamilyID"=>'family_code'					 	
							);
		$this->idfield="studentid";
		
	}
	
	// function getpm(){
	// 	$class=$this->router->fetch_class();
	// 	$folder=$this->uri->segment(1)	;
	// 	$module='/'.$this->router->fetch_method();
	// 	if($module=='/index')
	// 		$module='';
	// 	$url=$folder.'/'.$class.$module;
	// 	//echo $url;
	// 	return $this->db->where('link',$url)->get('sch_z_page')->row();
	// }
	
	function index()
	{
		$data['page_header']="Student Fee List";
		$data['getprogram']=$this->mprogram->getprograms();
		$data['get_sle']=$this->mshl->getsch_level();	
		$data['getyears']=$this->mgeyear->getschoolyear();	
		$data['getranglevel']=$this->mranglevel->rangelevels();
		$data['classes']=$this->mclass->allclass();
		$data['marea']=$this->marea->FMareas();
		
		
		$data['getstudents']=$this->mstu_fee->getstudents();
		$data['mstu_other_fee']=$this->mstu_other_fee->FMotherfee();
		
		// $data['studentfee']=$this->mstu_fee->MgetfeeList();
		$data['oppayment']=$this->mstu_fee->getpaymentmethod();
			
		$this->parser->parse('header', $data);
		$this->parser->parse('student_fee/v_student_fee', $data);
		
		$this->parser->parse('footer', $data);
	}
	
	function FCstudentFeeList(){
		$studentid = $this->input->post('stuid_pos');
		$data1['student']=$this->mstu_fee->MgetfeeList($studentid);
	}
	
	function FCprint_inv(){
		$data1['student']=$this->mstu_fee->FCprint_inv($studentid);
	}// end print inv
	
	function FCinvOrders(){
		$photodel="<img src='".site_url('../assets/images/icons/delete.png')."'/>";
		$studentid = $this->input->post('stuid');
		$invOrders=$this->mstu_fee->FMinvOrders($studentid);
		
		$trinv="";
		// print_r($invOrders);
		if(isset($invOrders) && count($invOrders)>0){
			$i=1;
			$theads="";
			foreach($invOrders as $row){
				$fclassact='fclassact';
				$is_status=($row->is_status);
				
				
				if($is_status==0){// panding
					$tradd='<a id="a_delete_inv" class="a_delete_inv" attr_id='.$row->typeno.' style="text-align:center">'.$photodel.'</a>';
					$classact='panding';
					$ispaid="";
				}if($is_status==2){// partial
					$tradd='<span class="partial">Partial</samp>';
					$classact='partial';
				}else if($is_status==1){// completed
					$tradd='<span class="completed">Completed</samp>';
					$classact='completed';
				}
				if($this->green->gAction("D")){	
				// $theads=$tradd;
				}
				$theads=$tradd;
				$linkprint="<a target='_blank' href='".site_url('student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno.'')."'>".$row->typeno."</a>";
				$trinv.='<tr class="active cl_inv">
							<td class="'.$classact.'">
								<table>
									<tr>
										<td><strong><span style="color:red;">'.($i++).'</span></strong>. Invoice#</td>
										<td>:</td><td style="text-align:right">'.$linkprint.'</td>
									</tr>
									<tr>
										<td>Invoice Date</td>
										<td>:</td><td style="text-align:right">'.$this->green->convertSQLDate($row->trandate).'</td>
									</tr>
								
									<tr class="tr_inv">
										<td colspan="2"></td>
									</tr>								
								</table>
							</td>
							<td colspan="4" align="right">'.$theads.'</td>
						 </tr>';
						 
						$datas="SELECT
								sch_student_fee_type.typeno,
								sch_student_fee_type.type,
								sch_student_fee_type.description,
								sch_student_fee_type.`not`,
								sch_student_fee_type.amt_line,
								sch_student_fee_type.prices,
								sch_student_fee_type.amt_dis,
								sch_student_fee_type.qty
							FROM
								sch_student_fee_type
							where 1=1
							AND sch_student_fee_type.typeno='".$row->typeno."'
							AND prices <> ''
						
						";
						// echo $datas; exit();
						
						$result = $this->db->query($datas)->result();
						// print_r($classes);
						$trinvd="";	
						$amttotal=0;
						$ii=1;
						if(isset($result) && count($result)>0){	
						$des="";	
							foreach($result as $rowde){
								if($rowde->not !=""){
									$des=$rowde->description.'-'.$rowde->not;	
								}else{
									$des=$rowde->description;
								}
								$trinv.='<tr class="info">
									<td>'.$des.'</td>
									<td align="right">'.$rowde->qty.'</td>
									<td align="right">'.$rowde->prices.'</td>
									<td align="right">'.$rowde->amt_dis.' %</td>
									<td align="right">'.number_format($rowde->amt_line,2).'&nbsp;$</td>
								 </tr>';
								 $amttotal+=($rowde->amt_line);
							}
							
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="'.$fclassact.'">Total</td>
									<td align="right" class="'.$fclassact.'">'.number_format($row->amt_total,2).'&nbsp;$</td>
								 </tr>';
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="'.$fclassact.'">Desposit</td>
									<td align="right" class="'.$fclassact.'">'.number_format($row->amt_paid,2).'&nbsp;$</td>
								 </tr>';
							$trinv.='<tr>
									<td></td>
									<td></td>
									
									<td></td>
									<td align="right" class="'.$fclassact.'">Grand&nbsp;Total</td>
									<td align="right" class="'.$fclassact.'">'.number_format($row->amt_balance,2).'&nbsp;$</td>
								 </tr>';
							
								 
						}// if(isset($result) && count($result)>0){
						
			}// foreach($invOrders as $row){
		}
		if(isset($invOrders) && count($invOrders)>0){
		$arr['trinv']=' <table style="width:98%" align="center" class="btl_history">
							<thead>
							<tr class="gform">
							<th style="text-align:left !important; width:70%">Description</th>
							<th style="text-align:right !important; width:10%">Qty</th>
							<th style="text-align:right !important; width:5%">Unit Price</th>
							<th style="text-align:right !important; width:5%">Dis(%)</th>
							<th style="text-align:right !important; width:10%">Amount</th>
							</tr>
							</thead>
							<tbody>
								'.$trinv.'
							</tbody>
					   </table>';
		
		header("Content-type:text/x-json");
		echo json_encode($arr);
		}
		
	}
	
	function Fcgetop(){
		$opprpid = $this->input->post('opprpid');
		$opget_sle=$this->mshl->getsch_level($opprpid);	
		$opschlev="";
		if(isset($opget_sle) && count($opget_sle)>0){
			foreach($opget_sle as $rowshle){
				$opschlev.="<option value='".$rowshle->schlevelid."'>".$rowshle->sch_level."</option>";
			}
		}
		$arr['opshlev']=$opschlev;
		
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}
	
	function Fcgetopshlev(){
		$opprpid = $this->input->post('opprpid');
		$shlevid = $this->input->post('shlevid');
		$opshlev=$this->mgeyear->getschoolyear("",$opprpid,$shlevid);
		
		$datashlev="";	
		if(isset($opshlev) && count($opshlev)>0){		
			foreach($opshlev as $rowshlev){
				$datashlev.="<option value='".$rowshlev->yearid."'>".$rowshlev->sch_year."</option>";
			}
		}
		
		$arr['datashlev']=$datashlev;
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}
	
	function Fcgetopyear(){
		$programid = $this->input->post('opprpid');
		$shlevelid = $this->input->post('shlevelid');
		$getyear=$this->mranglevel->rangelevels($schoolid="",$programid,$shlevelid);
		// print_r($getyear);
		$datayear="";	
		if(isset($getyear) && count($getyear)>0){		
			foreach($getyear as $rowyeare){
				$datayear.="<option value='".$rowyeare->rangelevelid."'>".$rowyeare->rangelevelname."</option>";
			}
		}
		
		$arr['datayears']=$datayear;
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}
	
	function Fcgetclass(){
		$shlevelid = $this->input->post('shlevelid');	
		$classes=$this->mclass->allclass($shlevelid);
		 // print_r($classes);
		$dataclass="";	
		$dataclass="<option value=''>None</option>";	
		if(isset($classes) && count($classes)>0){		
			foreach($classes as $rowlass){
				$dataclass.="<option value='".$rowlass->classid."'>".$rowlass->class_name."</option>";
			}
		}
		$arr['dataclasss']=$dataclass;
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}
	
	function FCgetPaymenttype(){
		$paymentmethod = $this->input->post('paymentmethod');
		$schlevelid = $this->input->post('shlevelid');
		$yearid = $this->input->post('acandemicid');
		$h_program = $this->input->post('h_program');
		$schoolid=$this->session->userdata('schoolid');
		
		$datas="SELECT
						v_study_peroid.period,
						v_study_peroid.term_sem_year_id
						FROM
						v_study_peroid
						WHERE 1=1
						AND programid='".$h_program."'
						AND schlevelid='".$schlevelid."'
						AND yearid='".$yearid."'
						AND payment_type='".$paymentmethod."'
						AND schoolid='".$schoolid."'
						";
		// echo $datas;				
		$result = $this->db->query($datas)->result();
		
			$datapayment="";
			$datapayment="<option value=''>None</option>";
			if(isset($result) && count($result)>0){		
				foreach($result as $rowbusfee){
					$datapayment.="<option value='".$rowbusfee->term_sem_year_id."'>".$rowbusfee->period."</option>";
				}
			}
			$arr['gpaymenttype']=$datapayment;
			header("Content-type:text/x-json");
			echo json_encode($arr);	
		// echo $datas; exit();
			
	}
	
	function FCbusfee(){
		$areaid = $this->input->post('oparea');	
		$datas="SELECT
					sch_setup_busfee.buspriceid,
					sch_setup_busfee.buswaytype,
					sch_setup_busfee.price,
					sch_setup_area.area,
					sch_family_social_infor.description,
					sch_setup_area.areaid
					FROM
					sch_setup_area
					INNER JOIN sch_setup_busfee ON sch_setup_area.areaid = sch_setup_busfee.areaid
					INNER JOIN sch_family_social_infor ON sch_setup_busfee.buswaytype = sch_family_social_infor.famnoteid
					where 1=1
					AND sch_setup_area.areaid='".$areaid."'

				";
		// echo $datas; exit();
			
		$result = $this->db->query($datas)->result();
		 // print_r($classes);
		$databusfee="";	
		if(isset($result) && count($result)>0){		
			foreach($result as $rowbusfee){
				$databusfee.="<option attr_pric=".$rowbusfee->price." value='".$rowbusfee->buspriceid."'>".$rowbusfee->description."</option>";
			}
		}
		$arr['busfe']=$databusfee;
		header("Content-type:text/x-json");
		echo json_encode($arr);		
	}
	
	function Fcstudent(){
		$studentid = $this->input->post('stuid_pos');
		$typeno = $this->input->post('typeno');
		
		$getstudent=$this->mstu_fee->getstudent($studentid);
	
		$arraystudent =array(
								"studentid"=>$getstudent->studentid,
								"student_num"=>$getstudent->student_num,
								"first_name"=>$getstudent->first_name,
								"last_name"=>$getstudent->last_name,
								"first_name_kh"=>$getstudent->first_name_kh,
								"last_name_kh"=>$getstudent->last_name_kh,
								"nationality"=>$getstudent->nationality,
								"religion"=>$getstudent->religion,
								"zoon"=>$getstudent->zoon,
								"leave_school"=>$getstudent->leave_school,
								"phone1"=>$getstudent->phone1,
								"email"=>$getstudent->email,
								"gender"=>$getstudent->gender,
								"current_programid"=>$getstudent->current_programid,
								"current_schoollevelid"=>$getstudent->current_schoollevelid,
								"current_yearid"=>$getstudent->current_yearid,
								"current_ranglevelid"=>$getstudent->current_ranglevelid,
								"classid"=>$getstudent->classid,
								"is_new_old"=>$getstudent->is_new_old
							);
		
		$program = '';
		if($getstudent->current_programid!=""){
		$program = $this->db->where('programid', $getstudent->current_programid)->get('sch_school_program')->row()->program;	
		}
		
		$chlevel = '';
		if($getstudent->current_schoollevelid!=""){
		$chlevel = $this->db->where('schlevelid', $getstudent->current_schoollevelid)->get('sch_school_level')->row()->sch_level;	
		}
		
		$sch_year = '';		
		if($getstudent->current_yearid!=""){
		$sch_year = $this->db->where('yearid', $getstudent->current_yearid)->get('sch_school_year')->row()->sch_year;	
		}
		//-------------------
		$rangelevelname = '';		
		if($getstudent->current_ranglevelid!=""){
		$rangelevelname = $this->db->where('rangelevelid', $getstudent->current_ranglevelid)->get('sch_school_rangelevel')->row()->rangelevelname;	
		}
		
		$class_name = '';		
		if($getstudent->classid!=""){
		$class_name = $this->db->where('classid', $getstudent->classid)->get('sch_class')->row()->class_name;	
		}
		$area = '';		
		if($getstudent->zoon!=""){
		$area = $this->db->where('areaid', $getstudent->zoon)->get('sch_setup_area')->row()->area;	
		}
		
		
		$arayOrder = array("program" =>$program,
						 "chlevel" =>$chlevel,
						 "sch_year" =>$sch_year,
						 "rangelevelname" =>$rangelevelname,
						 "class_name" =>$class_name,						 
						 "area" =>$area	 				 						 						
						 );
		//----------
		
		$arr['image_stu']="<img class='img-responsive img-circle' style='border: 1px solid; width:250px; height:175px; border-radius: 3px;' rel='' src='".site_url('../assets/images/'.$studentid.'.png')."'/>";
		
		
		$arr['getRow']=$arraystudent;
		$arr['arayOrder']=$arayOrder;
		
		header("Content-type:text/x-json");
		echo json_encode($arr);
		
	}
	
	function FCpaymenttyp_price(){
		$yearid = $this->input->post('acandemicid');
		$schlevelid = $this->input->post('shlevelid');
		$rangelevelid = $this->input->post('ranglevid');
		$feetypeid = $this->input->post('paymentmethod');
		$term_sem_year_id = $this->input->post('paymenttype');
		$h_program=$this->input->post('h_program');
		$h_class=$this->input->post('h_class');
		
		// rangelevelfee($yearid="",$schlevelid="",$rangelevelid="",$feetypeid="",$term_sem_year_id="");
		$rows=$this->mranglevelfee->rangelevelfee($yearid,$schlevelid,$rangelevelid,$feetypeid,$term_sem_year_id);
		
		$sql="SELECT
					COUNT(studentid) AS studentid
					FROM
						sch_student_fee
					WHERE 1=1
					AND studentid='".$this->input->post('studentid')."'
					AND programid='".$h_program."'
					AND schooleleve='".$schlevelid."'
					AND acandemic='".$yearid."'
					AND ranglev='".$rangelevelid."'
					AND paymentmethod='".$feetypeid."'
					AND paymenttype='".$term_sem_year_id."'
					";
			 $count = $this->db->query($sql)->row()->studentid;
			$studentid=$count;
			// echo $sql;
		$arr['rows']=$rows;
		$arr['studentid']=$studentid;
		
		
		header("Content-type:text/x-json");
		echo json_encode($arr);
		// echo json_encode($studentid);
		
	}
	
	function FCDelinv(){
		$deltypeno = $this->input->post('deltypeno');
		$this->mstu_fee->FMDelinv($deltypeno);
		header("Content-type:text/x-json");
		echo json_encode($deltypeno);
	}
	
	function FCsave(){
		$studentid = $this->input->post('stuid_pos');
		$this->mstu_fee->MFsave($studentid);
	}
	
	
	//===================================
	
	
	function stdboarding()
	{
		
		$page=0;
		if(isset($_GET['per_page'])) $page=$_GET['per_page'];
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }	
	    $sql_page="SELECT s.studentid,
							s.student_num,
							s.last_name,
							s.first_name,
							s.last_name_kh,
							s.first_name_kh,
							s.dob,
							s.nationality,
							s.class_name,
							s.classid,
							s.sch_year,
							s.familyid,
							s.yearid,
							s.boarding_date
						FROM v_student_profile s					
					where s.is_active=1
					AND s.yearid='".$this->session->userdata('year')."'	
					AND s.boarding_school=1
					order by last_name";

		$this->load->library('pagination');	
		$config['base_url'] = site_url()."/student/student?pg=1&m=$m&p=$p";		
		$config['total_rows'] = $this->green->getTotalRow($sql_page);
		$config['per_page'] =150;
		//$config['use_page_numbers'] = TRUE;
		$config['num_link']=10;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';		
		
		$this->pagination->initialize($config);	
		$limi=" limit ".$config['per_page'];
		if($page>0){
			$limi=" limit ".$page.",".$config['per_page'];
		}		
		
		$sql_page.=" {$limi}";
		$data['tdata']=$this->green->getTable($sql_page);
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead2;
		
		$data['page_header']="Student List";			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/stdboarding_list', $data);
		$this->parser->parse('footer', $data);
	}
	function import($result=0){
		$data['page_header']="Import Student Profile";		
		$data['import_status']=($result==1?"Student profile was imported.":"No student profile to import.");			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/import_profile', $data);
		$this->parser->parse('footer', $data);
	}
	function importenroll($result=0){
		$data['page_header']="Import Student Profile";		
		$data['import_status']=($result==1?"Enrollment was imported.":"No Enrollment to import.");			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/import_enrollment', $data);
		$this->parser->parse('footer', $data);
	}
	function getschlevel(){
		$classid=$this->input->post('classid');
		$schlevelid=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
		echo $this->db->where('schlevelid',$schlevelid)->get('sch_school_level')->row()->is_vtc;
	}
	function importProfile(){
		$result=$this->mstu_fee->saveImport();
		$this->import($result);		
	}
	function importenrollment(){
		$result=$this->mstu_fee->saveImportenroll();
		$this->importenroll($result);		
	}
	function add(){
		$data['page_header']="New Student";			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/student_form', $data);
		$this->parser->parse('footer', $data);
	}
	function preview($studentid){
		$data['page_header']="New Student";
		//$data1['page_header']=array("New Student");	
		$yearid=$_GET['yearid'];
		$data1['student']=$this->mstu_fee->previewstd($studentid,$yearid);
		$this->parser->parse('header', $data);
		$this->load->view('student/preview', $data1);
		$this->parser->parse('footer', $data);
	}
	function previewmulti($classid=''){
		$schoolid=$this->session->userdata("schoolid");
		if($classid==''){
			$classid=$this->db->query("SELECT MIN(classid) as classid 
								FROM sch_class 
								WHERE schoolid='$schoolid'")->row()->classid;
		}
		$data['page_header']="New Student";			
		$yearid=$_GET['yearid'];
		$data['class_id']=$classid;	
		$data['student']=$this->mstu_fee->previewstd('',$yearid,$classid);	
		$this->parser->parse('header', $data);
		$this->load->view('student/preview_multi', $data);
		$this->parser->parse('footer', $data);
	}
	function previewhistory($studentid){
		$data['page_header']="New Student";
		$data1['stdid']=$studentid;
		$this->parser->parse('header', $data);
		$this->load->view('student/preview_history', $data1);
		$this->parser->parse('footer', $data);
	}
	function delete(){

		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
	    $studentid=$this->input->post('stu_id');
	   	#======= check status =====

	   	$status=$this->green->getValue("SELECT is_active FROM sch_student WHERE studentid='{$studentid}'");
	   	if($status==0){
	   		$this->green->runSQL("DELETE FROM sch_student WHERE studentid='{$studentid}'");
	   		$this->green->runSQL("DELETE FROM sch_student_enrollment WHERE studentid='{$studentid}'");
	   	}else{
	   		$this->db->set('is_active',0);
			$this->db->where('studentid',$studentid);
			$this->db->update('sch_student');
	   	}
		
		/*
		$login_username=$this->mstu_fee->getstudentrow($studentid)->login_username;
		$this->db->set('is_active',0);
		$this->db->where('user_name',$login_username);
		$this->db->update('sch_user');
		*/
		redirect("student/student?m=$m&p=$p");
	}
	function savesponsor(){
			$stdspid=$this->input->post('stdspid');
			$sponsorid=$this->input->post('sponsorid');
			$s_date=$this->green->formatSQLDate($this->input->post('s_date'));
			$e_date=$this->green->formatSQLDate($this->input->post('e_date'));
			$classid=$this->input->post('classid');
			$studentid=$this->input->post('studentid');
			$year=$this->db->select('year')->from('sch_student_enrollment')->where('studentid',$studentid)->where('classid',$classid)->get()->row()->year;
			
			$date=date('Y-m-d');
			$user=$this->session->userdata('user_name');
			$data=array('studentid'=>$studentid,
						'start_sp_date'=>$s_date,
						'end_sp_date'=>$e_date,
						'classid'=>$classid,
						'yearid'=>$year,
						'sponsorid'=>$sponsorid);
			if($stdspid!=''){
				$data2=array('modified_date'=>$date,'modified_by'=>$user);
				$this->db->where('stdspid',$stdspid)->update('sch_student_sponsor_detail',array_merge($data,$data2));
			}else{
				$data2=array('created_date'=>$date,'created_by'=>$user);
				$this->db->insert('sch_student_sponsor_detail',array_merge($data,$data2));
				$stdspid=$this->db->insert_id();
			}
			echo $stdspid;
		}
	function filllink(){
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_student')	;
		$this->db->like('last_name',$key);			
		$data=$this->db->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name,'id'=>$row->studentid);
		}
	     echo json_encode($array);
	}
	function edit($studentid){
		$data['page_header']="New Student";
		//$data1['page_header']=array("New Student");			
		$datas['student']=$this->mstu_fee->getstudentrow($studentid);
		$this->parser->parse('header', $data);
		$this->parser->parse('student/student_edit', $datas);
		$this->parser->parse('footer', $data);
	}
	function isstudentbymember($memid){
			$this->db->select('count(*)');
			$this->db->from('sch_student');
			$this->db->where('match_memid',$memid);
			$this->db->where('is_active',1);
			return $this->db->count_all_results();
	}
	function getstdlink(){
			$familyid=$this->input->post('familyid');
			$studentid=$this->input->post('studentid');
			$sql="SELECT * FROM v_student_profile s where s.familyid='$familyid'  AND s.yearid=(SELECT MAX(year) FROM sch_student_enrollment WHERE studentid=s.studentid)";
			// $this->db->select('*');
			// $this->db->from('v_student_profile');
			// $this->db->where('familyid',$familyid);
			if($studentid!=0)
				$sql.=" AND studentid <> '$studentid' ";
			$result=$this->green->getTable($sql);
			foreach ($result as $std_link) {
				echo "
					<tr>
						<td>$std_link[first_name]</td>
						<td>$std_link[last_name]</td>
						<td>$std_link[first_name_kh]</td>
						<td>$std_link[last_name_kh]</td>
						<td>$std_link[class_name]</td>
						<td><a><img yearid='".$std_link['yearid']."' rel=".$std_link['studentid']." onclick='previewstudent(event);' src='".site_url('../assets/images/icons/preview.png')."'/></a>   </td>
					</tr>
				";
			}
	}
	public function fillrespon()
	{
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_student_responsible')	;
		$this->db->like('last_name',$key);			
		$data=$this->db->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name,'id'=>$row->respondid);
		}
	     echo json_encode($array);
	}
	public function fillmember()
	{
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_student_member')	;
		$this->db->like('last_name',$key);			
		$data=$this->db->get()->result();//$this->green->getTable($sql_page);
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name,'id'=>$row->memid);
		}
	     echo json_encode($array);
		//$this->load->view('footer');
	}
	public function fillfamily()
	{
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_family')	;
		$this->db->like('father_name',$key);
		$this->db->or_like('family_code',$key);
		$this->db->order_by('familyid','desc');			
		$data=$this->db->get()->result();//$this->green->getTable($sql_page);
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>"$row->father_name | $row->family_code",'id'=>$row->familyid);
		}
	     echo json_encode($array);
		//$this->load->view('footer');
	}
	public function fillsponsor()
	{
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_family_sponsor')	;
		$this->db->like('last_name',$key);
		$this->db->or_like('first_name',$key);
		$this->db->or_like('sponsor_code',$key);		
		$data=$this->db->get()->result();//$this->green->getTable($sql_page);
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>"$row->last_name $row->first_name | $row->sponsor_code",'id'=>$row->sponsorid);
		}
	     echo json_encode($array);
		//$this->load->view('footer');
	}
	function getresponrow(){
		$respondid=$this->input->post('r_id');
		$this->db->where('respondid',$respondid);
		$row=$this->db->get('sch_student_responsible')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function getmemberrow(){
		$memid=$this->input->post('r_id');
		$this->db->where('memid',$memid);
		$row=$this->db->get('sch_student_member')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function deletesponsor(){
		$stdspid=$this->input->post('stdspid');
		$this->db->where('stdspid',$stdspid)->delete('sch_student_sponsor_detail');
	}
	function getmemberbyfamily(){
			$classid=$this->input->post('classid');
			$schlevelid=0;
			if($classid!='')
				$schlevelid=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
			$this->db->where('familyid',$this->input->post('familyid'));
			$this->db->where('is_active',1);
			echo "<option value=''>Choose Student</option>";
			foreach ($this->db->get('sch_student_member')->result() as $member) {
				if($schlevelid==4){
					echo "<option value='$member->memid'>$member->last_name $member->first_name </option>";
				}else{
					if($member->studentid!=0)
						echo "";
					else
						echo "<option value='$member->memid'>$member->last_name $member->first_name</option>";
				}
				
			}
	}
	function getstudentrow(){
		$std_id=$this->input->post('std_id');
		$this->db->select('s.studentid,s.last_name,s.first_name,s.last_name_kh,c.class_name');
		$this->db->from('sch_student s');
		$this->db->join('sch_class c','s.classid=c.classid','left');
		$this->db->where('s.studentid',$std_id);
		$s_row=$this->db->get()->row();
		header("Content-type:text/x-json");
		echo json_encode($s_row);
	}
	
	function validateuser(){
		$count=$this->mstu_fee->getvalidateuser($this->input->post('username'));
		echo $count;
	}
	function getfamilyrow(){
			$this->db->where('familyid',$this->input->post('familyid'));
			echo json_encode($this->db->get('sch_family')->row());
		}
	function getstdbyid(){
			$std_num=$this->input->post('std_num');
			$this->db->select('count(*)');
			$this->db->from('sch_student');
			$this->db->where('student_num',$std_num);
			echo $this->db->count_all_results();
		}
	function save(){
		$familyid=$this->input->post("familyid");
		$memberid=$this->input->post("student");
		$student_num=$this->input->post("student_num");

		$classid=$this->input->post('classid');
		$schlevelid=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
		$year=$this->input->post("year");
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }	
		//$dob=$this->input->post("dob");
		$studentid=$this->input->post("s_id");
		$update_mem=$this->input->post('update_mem');
		$count=0;
		if($memberid!='' && $memberid>0){
			if($studentid=='')
				$count=$this->mstu_fee->validatestudent($familyid,$memberid);
			else
				$count=$this->mstu_fee->validatestudentupdate($familyid,$memberid,$studentid);
		}
		if($schlevelid==4)
			$count=0;

		if($count>0){

			$data['error']="<div style='text-align:center; color:red;'>This Student Have been Register Before Please check again...!</div>";
			$data['page_header']="New Student";			
			$this->parser->parse('header', $data);
			if($studentid==''){
				$this->parser->parse('student/student_form', $data);

			}else{
				$data['student']=$this->mstu_fee->getstudentrow($studentid);
				$this->parser->parse('student/student_edit', $data);
			}
			$this->parser->parse('footer', $data);
			
		}else{
			$save_result=$this->mstu_fee->save($studentid,$update_mem);
			$this->do_upload($student_num,$year);
            if($save_result!=""){
                redirect(site_url('student/student/preview/'.$save_result.'?yearid='.$year), 'refresh');
            }
		}
			
	}
	function getresponstudent(){

		foreach ($this->mstu_fee->getresponfamily($this->input->post('familyid')) as $family_res) {
			
			echo "
					<tr>									
						<td ><input class='hide' type='text' value='$family_res->first_name $family_res->last_name' name='res_name[]'/>$family_res->first_name $family_res->last_name</td>
						<td ><input class='hide' type='text' value='$family_res->first_name_kh $family_res->last_name_kh' name='res_name_kh[]'/>$family_res->first_name_kh $family_res->last_name_kh</td>
						<td ><input class='hide' type='text' value='$family_res->sex' name='res_sex[]'/>$family_res->sex</td>
						<td ><input class='hide' type='text' value='$family_res->dob' name='res_dob[]'  id='res_dob'/>".$this->green->formatSQLDate($family_res->dob)."</td>
						<td><input class='hide' type='text' value='$family_res->relationship' name='res_relationship[]'/>$family_res->relationship</td>
						<td width='200' style='word-break:break-all;'><input class='hide' type='text' value='$family_res->occupation' name='res_occupation[]'/>$family_res->occupation</td>
						<td ><input class='hide' type='text' value='$family_res->revenue' name='res_revenue[]'/>$family_res->revenue</td>
					</tr>";
		}
	}

	function getmemberstudent(){
			$memid=$this->input->post('memberid');
			$studentid=$this->input->post('studentid');
			$familyid=$this->input->post('familyid');
			$relation=array("Legal Student","brother/sister","Cousin","niece/nephew");
			if($studentid>0)
				$query=$this->mstu_fee->getmemberfamily($familyid,0,$studentid);
			else
				$query=$this->mstu_fee->getmemberfamily($familyid,$memid,0);
		  	foreach ($query as $family_mem) {
		  		if($studentid!=''){
			  		$select_relat=$this->mstu_fee->getrelation($studentid,$family_mem->memid);
		  		}
		  		echo "
					  	<tr>
						  	<td class='hide'><input class='hide mem_id' type='text' value='$family_mem->memid' name='mem_id[]' id='mem_id'/></td>
						  	<td class='hide'><input class='hide std_num' type='text' rel='mem_id[]' value='$family_mem->studentid' name='std_num[]' id='std_num'/></td>
						  	<td ><input class='hide' type='text' value='$family_mem->first_name' name='mem_first_name[]' id='mem_first_name'/>$family_mem->first_name</td>
						  	<td ><input class='hide' type='text' value='$family_mem->last_name' name='mem_last_name[]' id='mem_last_name'/>$family_mem->last_name </td>
						  	<td ><input class='hide' type='text' value='$family_mem->first_name_kh' name='mem_first_name_kh[]'/>$family_mem->first_name_kh</td>
						  	<td ><input class='hide' type='text' value='$family_mem->last_name_kh' name='mem_last_name_kh[]'/>$family_mem->last_name_kh</td>
						  	<td ><input class='hide' type='text' value='$family_mem->sex' name='mem_sex[]'/>$family_mem->sex</td>
						  	<td ><input class='hide' type='text' value='$family_mem->dob' name='mem_dob[]' id='mem_dob'/>".$this->green->formatSQLDate($family_mem->dob)."</td>
						  	<td width='200' style='word-break:break-all;'><input class='hide' type='text' value='$family_mem->occupation' name='mem_occupation[]'/>$family_mem->occupation</td>
						  	<td ><input class='hide' type='text' value='$family_mem->grade_level' name='mem_grade_level[]'/>$family_mem->grade_level</td>
						  	<td ><input class='hide' type='text' value='$family_mem->school' name='mem_school[]'/>$family_mem->school</td>
						  	<td ><select  minlength='1' class='form-control hide' name='mem_relationship[]'>
						  		<option value=''>Select relationship</option>";
						  	foreach ($relation as $relat) {?>
						  		<option value='<?php echo $relat; ?>' <?Php if($select_relat==$relat) echo "selected"; ?>><?php echo $relat; ?></option>";
						  	<?php }
						  	
						echo "</select></td>
								</tr>";
				}
	  	
	}
	function do_upload($student_id,$year)
		{
			if(!file_exists('./assets/upload/students/'.$year))
			 {
			    if(mkdir('./assets/upload/students/'.$year,0755,true))
			    {
			                return true;
			    }
			 }
			$config['upload_path'] ='./assets/upload/students/'.$year;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '5000';
			$config['file_name']  = "$student_id.jpg";
			$config['overwrite']=true;
			$config['file_type']='image/png';
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('file'))
			{
				$error = array('error' => $this->upload->display_errors());			
			}
			else
			{		
				//$this->generate_thumb($this->upload->file_name,$this->upload->upload_path)	;	
				$data = array('upload_data' => $this->upload->data());			
				//redirect('setting/user');
				 	$config2['image_library'] = 'gd2';
                    $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                    $config2['new_image'] = './assets/upload/students/'.$year;
                    $config2['maintain_ratio'] = TRUE;
                    $config2['create_thumb'] = TRUE;
                    $config2['thumb_marker'] = FALSE;
                    $config2['width'] = 120;
                    $config2['height'] = 180;
                    $config2['overwrite']=true;
                    $this->load->library('image_lib',$config2); 

                    if ( !$this->image_lib->resize()){
	                	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
					}else{
						//unlink('./assets/upload/students/'.$student_id.'.jpg');
					}
			}
		}
	function generate_thumb($filename, $path = '')
	{
	    // if path is not given use default path //
	    if (!$path) {
	        $path = FCPATH . 'somedir' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
	    }

	    $config['image_library'] = 'gd2';
	    $config['source_image'] = $path . $filename;
	    $config['create_thumb'] = TRUE;
	    $config['maintain_ratio'] = TRUE;
	    $config['width'] = 120;
	    $config['height'] = 180;

	    $this->load->library('image_lib', $config);

	    if (!$this->image_lib->resize()) {
	        echo $this->image_lib->display_errors();
	        return FALSE;
	    }else{
	    	//unlink('./assets/upload/students/'.$student_id.'.jpg');
	    }
	    // get file extension //
	    preg_match('/(?<extension>\.\w+)$/im', $filename, $matches);
	    $extension = $matches['extension'];
	    // thumbnail //
	    $thumbnail = preg_replace('/(\.\w+)$/im', '', $filename). $extension;
	    return $thumbnail;
	}
	function getedit(){
		$stdspid=$this->input->post('stdspid');
		$spon= $this->db->select('sd.sponsorid,sd.studentid,sd.stdspid,sd.start_sp_date,sd.end_sp_date,fs.last_name,fs.first_name')
					->from('sch_student_sponsor_detail sd')
					->join('sch_family_sponsor fs','sd.sponsorid=fs.sponsorid','inner')
					->where('sd.stdspid',$stdspid)->get()->row();
		header("Content-type:text/x-json");
		echo json_encode($spon);
	}
	function getexp(){
			$data='';
			$studentid=$this->input->post('studentid');
			$full_name=$this->input->post('full_name');
			$full_name_kh=$this->input->post('full_name_kh');
			$classid=$this->input->post('classid');
			$sort_num=$this->input->post('sort_num');
			$page=$this->input->post('page');
			$sort=$this->input->post('sort');
			$year=$this->input->post('year');
			$schlevelid=$this->input->post('schlevelid');
			$level=$this->input->post('level');
			$is_all=$this->input->post('is_all');
			$f=$this->input->post('f');
			$data.='<thead>';
			foreach ($f as $key => $value) {
				$data.="<th class='no_wrap'>$value</th>";
			}
			$data.='</thead>';
			$query=$this->mstu_fee->getexp($studentid,$full_name,$full_name_kh,$classid,$sort,$year,$schlevelid,$level,$sort_num,$page,$is_all);
			$i=1;
			$data.='<tbody>';
			foreach($query as $row){									
				$data.="<tr>";
					if(isset($f['No']))
					 	$data.="<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>";
					foreach ($f as $key => $value) {

						if($key!='No')
							$data.="<td>".$row[$key]."</td>";
					}							 
					$i++;
				$data.="</tr>";	 
			}
			$data.='</tbody>';
			$arr=array('data'=>$data);
			header("Content-type:text/x-json");
			echo json_encode($arr);
	}
	function search(){
		if(isset($_GET['s_id'])){
			$studentid 	  = $_GET['s_id'];
			$full_name 	  = $_GET['fn'];
			$full_name_kh = $_GET['fnk'];
			$classid 	  = $_GET['class'];
			$year 		  = $_GET['year'];
			$schlevelid   = $_GET['l'];
			$boarding     = $_GET['b'];
			$level 		  = $_GET['le'];
			$m 			  = $_GET['m'];
			$p 			  = $_GET['p'];
			$ag 		  = $_GET['ag'];
			$sort_num 	  = $_GET['s_num'];
			$sort 		  = $this->input->post('sort');
			$promot_id	  = $_GET['pro'];
			$data['tdata']=$query=$this->mstu_fee->searchstudent($boarding,$studentid,$full_name,$full_name_kh,$classid,$sort,$sort_num,$year,$schlevelid,$level,$m,$p,$ag,$promot_id);
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['exp']=	$this->exp;
			$data['page_header']="Student List";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/student_list', $data);
			$this->parser->parse('footer', $data);
		}
		if(!isset($_GET['s_id'])){
			$studentid 	  = $this->input->post('studentid');
			$full_name 	  = $this->input->post('full_name');
			$full_name_kh = $this->input->post('full_name_kh');
			$classid 	  = $this->input->post('classid');
			$sort 		  = $this->input->post('sort');
			$sort_num 	  = $this->input->post('sort_num');
			$year 		  = $this->input->post('year');
			$schlevelid   = $this->input->post('schlevelid');
			$level 		  = $this->input->post('level');
			$boarding 	  = $this->input->post('boarding');
			$m 			  = $this->input->post('m');
			$p 			  = $this->input->post('p');
			$ag 		  = $this->input->post('ag');
			$promot_id 	  = $this->input->post('promot_id');

			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
			
			$query=$this->mstu_fee->searchstudent($boarding,$studentid,$full_name,$full_name_kh,$classid,$sort,$sort_num,$year,$schlevelid,$level,$m,$p,$ag,$promot_id);
				 //echo "count:".count($query);
				 //exit();
				 $i=1;
				 $data='';
				foreach($query as $row){	
					if($row['familyid']!=''){
						$this->db->where('familyid',$row['familyid']);
						$family_code=$this->db->get('sch_family')->row()->family_code;	
					}
					else
						$family_code='';															
					$data.="<tr>
						<td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
						 <td class='student_num'>
							<a href='".site_url("/student/student/preview/".$row['studentid']."?yearid=".$row['yearid'])."' target='_blank'>
							".$row['student_num']."</td>
						 <td class='last_name'>".$row['last_name']." ".$row['first_name']."</td>
						 <td class='last_name_kh'>".$row['last_name_kh']." ".$row['first_name_kh']."</td>
						 <td class='dateofbirth'>".$row['dob']."</td>
						 <td class='class_name'>".$row['class_name']."</td>
						 <td class='yearid'>".$row['sch_year']."</td>
						 <td class='FamilyID'><a href='".site_url("social/family/preview/$row[familyid]")."' target='_blank'>$family_code</a></td>";
						if($boarding!=1){
							$data.= "<td class='remove_tag'>";
							if($this->green->gAction("P")){	
								$data.= "<a>
							 		<img rel=".$row['studentid']." onclick='previewhistory(event);' src='".site_url('../assets/images/icons/a_preview.png')."'/>
							 	</a>
							 	<a>
							 		<img yearid='".$row['yearid']."' rel=".$row['studentid']." onclick='previewstudent(event);' src='".site_url('../assets/images/icons/preview.png')."'/>
							 	</a> ";
							}
							if($this->green->gAction("D")){	
								$data.="<a><img rel=".$row['studentid']." onclick='deletestudent(event);' src='".site_url('../assets/images/icons/delete.png')."'/></a>";
							}
							if($this->green->gAction("U")){	
								$edit_link=site_url("student/student/edit/".$row['studentid'])."?yearid=".$row['yearid']."&m=$m&p=$p" ;
                                $data.="<a href='".$edit_link."' target='_blank'>
                                    <img yearid='".$row['yearid']."' rel=".$row['studentid']." src='".site_url('../assets/images/icons/edit.png')."'/>
                                  </a>";

                            }
							$data.="</td>";
						}
						$data.="</tr>";										 
					$i++;	 
					}
					$data.="<tr class='remove_tag'>
						<td colspan='12' id='pgt'>
							<div style='margin-top:20px; width:10%; float:left;'>
							Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
											
											$num=150;
											for($i=0;$i<10;$i++){
												$select='';
												if($num==$sort_num) 
													$select='selected';
												$data.="<option value='".$num."' $select > $num</option>";
												$num+=50;
											} 
											$select='';
											if($num=='all') 
												$select='selected';
											$data.="<option value='all' $select>All</option>
										
								</select>
							</div>
							<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
								<ul class='pagination' style='text-align:center'>
								 ".$this->pagination->create_links()."
								</ul>
							</div>

						</td>
					</tr> ";
					$arr=array('data'=>$data);
					header("Content-type:text/x-json");
					echo json_encode($arr);
		}
	}
	function check_stu_evaluate(){
		$studentid=$this->input->post("stu_id");
		$arr['isdel']=1;
		$geteval=$this->green->getValue("SELECT COUNT(*) num FROM sch_student_evaluated WHERE studentid='{$studentid}'");
		if($geteval>0){
			$arr['isdel']=0;
		}
		header("Content-type:text/x-json");
		echo json_encode($arr);
	}
	
}
