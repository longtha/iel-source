<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class c_treansport extends CI_Controller{
	    protected $thead;
		protected $idfield;

	    function __construct() {
	        parent::__construct();
	        $this->load->model("student_fee/m_treansport","trenspot");
	        $this->thead = array("No" => 'no',
	        					"Photo"=>'Photo',
                                "Student Name" => 'student_num',
                                "Bus Name"=> 'busid',
                                "Area Name" => 'area',
                                "Driver Name" => 'driverid',
                                "Date"=> 'trandate',
                                "Way" =>'otherfee',
                                "Invoice"=>'typeno_inv',
                                "Other"=> 'Other'
	                            );
	        $this->idfield = "studentid";
	    }
	    function index() {
	        $data['idfield'] = $this->idfield;
        	$data['thead'] = $this->thead;
        	$data['areaname']=$this->trenspot->allarea();
	        $this->load->view('header',$data);
	        $this->load->view('student_fee/v_treansport',$data);
	        $this->load->view('footer',$data );
	    }
	// get_bus =================================================
	    function get_bus(){
			$area_name = $this->input->post('area_name');
			$get_bus = $this->trenspot->get_bus($area_name);
			header("Content-type:text/x-json");
			echo ($get_bus);
		}
	// get_driver================================================
		function get_driver(){
			$bus_name = $this->input->post('bus_name');
			$get_driver = $this->trenspot->get_driver($bus_name);
			header("Content-type:text/x-json");
			echo ($get_driver);
		}
	// Getdata===================================================
	    function Getdata(){
			$total_display =$this->input->post('sort_num');
			$this->green->setActiveRole($this->input->post('roleid'));
	  		$total_display = $this->input->post('total_display');
	  		$sortby=$this->input->post("sortby");
			$sorttype=$this->input->post("sorttype");
			$studen_id=$this->input->post("studen_id");
			$bus_name=$this->input->post("bus_name");
			$formdate=$this->input->post('formdate');
			$area_name=$this->input->post("area_name");
			$driver_name=$this->input->post("driver_name");
			$todate=$this->input->post('todate');
	  		$where='';
			$sortstr="";
				if($studen_id!= ""){
		    		$where .= "AND ( CONCAT(first_name, ' ', last_name) LIKE '%".$studen_id."%' ";
		    		$where .= "or CONCAT(last_name_kh, ' ', first_name_kh) LIKE '%".$studen_id."%' ";
		    		$where .= "or student_num LIKE '%".$studen_id."%' ) ";
		    		
		    	}
				if($bus_name!= ""){
		    		$where .= "AND  v_sch_stud_transport.busid = '".$bus_name."' ";
		    	}
				if($area_name!= ""){
					$where .= "AND  v_sch_stud_transport.areaid = '".$area_name."' ";
				}
				if($driver_name!= ""){
					$where .= "AND  v_sch_stud_transport.driverid = '".$driver_name."' ";
				}
				if($formdate != ''){
				 $where .= "AND v_sch_stud_transport.trandate >= '".$this->green->formatSQLDate($formdate)."'";    
				}
				if($todate != ''){
					 $where .= "AND v_sch_stud_transport.trandate <= '".$this->green->formatSQLDate($todate)."'";     
				}
		        (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

			$sqr="SELECT * FROM
					v_sch_stud_transport 
				WHERE 1=1 {$where}";

			
			$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_sch_stud_transport WHERE 1=1 {$where}")->row()->numrow;

				$getperpage=0;
				if($total_display==''){
					$getperpage=10;
				}else{
					$getperpage=$total_display;
				}
				$paging    = $this->green->ajax_pagination($total_row,site_url()."student_fee/c_treansport",$getperpage,"icon");
				//return "$sql limit {$paging['start']}, {$paging['limit']}";
				if($sortstr!=""){
					$sqr.=$sortstr;
				}
				$getlimit=10;
				if($paging['limit']!=''){
					$getlimit=$paging['limit'];
				}
			$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
				$j='';
				$table='';
				if(count($data) > 0){
					foreach($data as $row){  //$this->db->query($sqr)->result() as $row
						$j++;
						$have_img = base_url()."assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg';		
						$no_imgs = base_url()."assets/upload/students/NoImage.png";	
						$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
							if (file_exists(FCPATH . "assets/upload/students/".$row->acandemic.'/'.$row->studentid.'.jpg')) {				
								$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
							}
							
						$table.= "<tr>
									<td class='no'>".$j."</td>
									<td class='Photo'>".$img."</td>
									<td class='student_num'><strong>".$row->student_num."</strong></br>".$row->last_name_kh.'  '.$row->first_name_kh."</br>".$row->first_name.'  '.$row->last_name."</br>".$row->gender."</td>	
									<td class='busid' align='center'>".$row->busno."</td>
									<td class='area' align='center'>".$row->area."</td>
									<td class='driverid' align='center'>".$row->driver_name."</td>
									<td class='trandate'>".$this->green->convertSQLDate($row->trandate)."</td>
									
									<td class='otherfee' align='center'>".$row->otherfee."</td>
									<td class='typeno_inv' align='center'><a href='".site_url().'/student_fee/c_student_print_invoice?FCprint_inv='.$row->typeno."' target='_blank'>#".$row->typeno."</a></td>
									<td class='Other'></td>
									<td class='remove_tag no_wrap'>";
							$table.= " </td>
							 </tr>
							 ";	
					}
				}else{
					$table.= "<tr>
								<td colspan='11' style='text-align:center;'> 
									We did not find anything to show here... 
								</td>
							</tr>";
				}

			$arr['data']=$table;
			header('Content-Type:text/x-json');
			$arr['body']=$table;
			$arr['pagination']=$paging;
			echo json_encode($arr); 
			die();
		}
	}
