<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_student_restor extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('student_fee/m_student_restor', 'restor');
		$this->load->model('student_fee/m_student_fee_invoice', 'rp');
        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolinformodel', 'info'); 
        $this->load->model('school/modfeetype', 'f');

        $this->thead = array("No" => 'no',
                                "Description"=>'student_num',
                                "Delete Date"=>'deleted_date',
                                "Delete By"=>'deleted_byuser',
                                "Class"=>'class_name',
                                "Quantity"=>'qty',
                                "Unit Price"=>'prices',
                                "Dis(%)"=>'amt_dis',
                                "Amount"=>'amt_line'
                                );
            $this->idfield = "student_num";
	}

	function index(){
        $data['idfield'] = $this->idfield;
        $data['thead'] = $this->thead;	
		$this->load->view('header', $data);		
		$this->load->view('student_fee/v_student_restor', $data);
		$this->load->view('footer', $data);	
	}

	// get_schlevel -----------------------------------
    function get_schlevel(){
		$programid = $this->input->post('programid');
		$get_schlevel = $this->restor->get_schlevel($programid);
		echo ($get_schlevel);
	}

	//yera --------------------------------------------
	function get_schyera(){
		$schlevelids = $this->input->post('schlevelids');
		$get_schyera = $this->restor->get_schyera($schlevelids);
		echo ($get_schyera);
	}

	//from ranglavel ------------------------------------
	function schranglavel(){
		$yearid = $this->input->post('yearid');
		$schranglavel = $this->restor->schranglavel($yearid);
		echo ($schranglavel);
	}

	// from class -------------------------------------
	function get_schclass(){
		$rangelevelid = $this->input->post('rangelevelid');
		$get_schclass = $this->restor->get_schclass($rangelevelid);
		echo ($get_schclass);
	}
	// get_paymentType ----------------------------------
	function get_paymentType(){
        $feetypeid = $this->input->post('feetypeid');
        $get_paymentType = $this->restor->get_paymentType($feetypeid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_paymentType;
    }

    //  FrestorDatas-------------------------------------------
     function FrestorDatas(){
		$type = $this->input->post('type');
		$typeno = $this->input->post('typeno');
		$attr_type = $this->input->post('attr_type');
		$attr_typeno = $this->input->post('attr_typeno');
		$this->rp->FrestorDatas($type,$typeno,$attr_type,$attr_typeno);
		header("Content-type:text/x-json");
		echo json_encode($type);
	}

    // schowdata -------------------------------------------
    function schowdata(){
    	$this->green->setActiveRole($this->session->userdata('roleid'));  
    	$m = ($this->input->post('m'));
		$p = ($this->input->post('p'));
		$this->green->setActiveModule($m);  
		$this->green->setActivePage($p);
		$total_display =$this->input->post('sort_num');
  		$total_display = $this->input->post('total_display');
  		$sortby=$this->input->post("sortby");
		$sorttype=$this->input->post("sorttype");
		$typeno = trim($this->input->post('typeno', TRUE));
		$student_num = trim($this->input->post('student_num', TRUE));
		$student_name = trim($this->input->post('student_name', TRUE));
		$gender = trim($this->input->post('gender', TRUE));
		$schoolid = trim($this->input->post('schoolid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelids = trim($this->input->post('schlevelids', TRUE));
		$yearid = trim($this->input->post('yearid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$from_date = trim($this->input->post('from_date', TRUE));
		$to_date = trim($this->input->post('to_date', TRUE));
		$feetypeid = trim($this->input->post('feetypeid', TRUE));
		$term_sem_year_id = trim($this->input->post('term_sem_year_id', TRUE));
		$report_type = trim($this->input->post('report_type', TRUE));

  		$where='';
		$sortstr="";
			if($typeno != ''){
			 	$where .= "AND typeno_inv_rec = '{$typeno}'";
			}
			if($student_num != ''){
			 	$where .= "AND student_num LIKE '%{$student_num}%'";
			}

			if($student_name != ''){
				$where .= "AND ( CONCAT(
									first_name,
									' ',
									last_name
								) LIKE '%{$student_name}%' ";
				$where .= "or CONCAT(
									first_name_kh,
									' ',
									last_name_kh
								) LIKE '%{$student_name}%' ) ";	
			}
			if($gender != ''){
			 	$where .= "AND gender = '{$gender}'";
			}
			if($schoolid != ''){
			 	$where .= "AND schoolid = '{$schoolid}'";
			}
			if($programid != ''){
			 	$where .= "AND programid = '{$programid}'";
			}
			if($schlevelids != ''){
			 	$where .= "AND schooleleve = '{$schlevelids}'";
			}
			if($yearid != ''){
			 	$where .= "AND acandemic = '{$yearid}'";
			}
			if($rangelevelid != ''){
			 	$where .= "AND ranglev = '{$rangelevelid}'";
			}
			if($classid != ''){
			 	$where .= "AND classid = '{$classid}'";
			}
			if($from_date != ''){
				$where .= "AND date(deleted_date) >= '".$this->green->formatSQLDate($from_date)."' ";
			}
			if($to_date != ''){
				$where .= "AND date(deleted_date) <= '".$this->green->formatSQLDate($to_date)."' ";
			}
			if($feetypeid != ''){
			 	$where .= "AND paymentmethod = '{$feetypeid}'";
			}
			if($term_sem_year_id != ''){
			 	$where .= "AND paymenttype = '{$term_sem_year_id}'";
			}
			if($report_type != ''){
			 	$where .= "AND type = '{$report_type}'";
			}
	    	(isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";
	    	
		$sqr = "SELECT DISTINCT
						v_student_restor.studentid,
						v_student_restor.first_name,
						v_student_restor.last_name,
						v_student_restor.typeno,
						v_student_restor.type,
						v_student_restor.student_num,
						v_student_restor.first_name_kh,
						v_student_restor.last_name_kh,
						v_student_restor.gender,
						DATE_FORMAT(deleted_date, '%d/%m/%Y %h:%i %p') AS deleted_date,
						v_student_restor.classid,
						v_student_restor.class_name,
						v_student_restor.acandemic,
						v_student_restor.programid,
						v_student_restor.ranglev,
						v_student_restor.schooleleve,
						v_student_restor.type_inv_rec,
						v_student_restor.typeno_inv_rec,
						v_student_restor.schoolid,
						v_student_restor.paymentmethod,
						v_student_restor.paymenttype,
						v_student_restor.deleted_byuser,
						v_student_restor.add_note

					FROM
						v_student_restor 
					WHERE 1 = 1 {$where} ";
		$total_row = $this->db->query("SELECT  COUNT(*) as numrow FROM sch_student_fee_deleted WHERE 1 = 1 {$where} ")->row()->numrow;
			$getperpage=0;
			if($total_display==''){
				$getperpage=10;
			}else{
				$getperpage=$total_display;
			}
			$paging    = $this->green->ajax_pagination($total_row,site_url()."student_fee/c_student_restor",$getperpage,"icon");
			if($sortstr!=""){
				$sqr.=$sortstr;
			}
			$getlimit=10;
			if($paging['limit']!=''){
				$getlimit=$paging['limit'];
			}
		$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
			$j='';
			$table='';
			$amttotal=0;
			$gtotal=0;
			$gtotal=0;
			$color='';	
			if(count($data) > 0){
				foreach($data as $row){
					
						$type = $this->db->query("SELECT
														sch_z_systype.type
													FROM
														sch_z_systype
					
													WHERE sch_z_systype.typeid = '".$row->type."' ")->row()->type; 
					if($row->type == '20'){
						$color = '#F08080';
					}
					else if($row->type == '21'){  
						$color = '#DC143C';
					}
					else if($row->type == '22'){
						$color = '#A52A2A';
					}
					else if($row->type == '24'){
						$color = '#800000';
					}
					else if($row->type == '42'){
						$color = '#4B0082';
					}
					if($row->type == '43'){
						$color = '#006400';
					}
					
					$j++;
					
					$table.= "<tr class='.tr'>
									<td>".$j."</td>
									<td ><span class='border-data'><strong>".$row->student_num.'<br>
										 '.$row->last_name_kh.' '.$row->first_name_kh.'<br>
										'.$row->last_name.' '.$row->first_name.' <br>
										'.ucfirst($row->gender)."<br></span></strong>
									</td>
									<td style='text-align:center;'>".$row->deleted_date."<br>".$row->add_note."</td>
									<td style='text-align:center;'>".$row->deleted_byuser."</td>
									<td style='text-align:center;' colspan='0'>".$row->class_name."</td>";
							$table.= "<td id='trprint' class='trprint'  colspan='4' style='text-align:right;'><span class='trprint'>".$type."&nbsp;&nbsp;&nbsp;#".$row->typeno_inv_rec."</span></td>";
							$table.="<td class='trhide remove_tag' colspan='4' style='text-align:right;'><a href='javascript:;' class='remove_tag btn btn-sm btn-success Restorshow' id='Restor' type=".$row->type." typeno=".$row->typeno." attr_type=".$row->type_inv_rec." attr_typeno =".$row->typeno_inv_rec."><span class='glyphicon glyphicon-repeat' ></span>&nbsp;Restor<span>&nbsp;#".$row->typeno_inv_rec."</span><br><span style='color: ".$color."'>".$type."</span></a></td>";

							$table.="</tr>";
					// detail -----------------------------
					$sql1 = "SELECT 
									v_student_restor.typeno,
									v_student_restor.type,
									v_student_restor.description,
									v_student_restor.amt_line,
									v_student_restor.prices,
									v_student_restor.amt_dis,
									v_student_restor.qty
								FROM
									v_student_restor
								WHERE v_student_restor.typeno = '".$row->typeno."' ";

					$data1 = $this->db->query($sql1);
					
					$k = 1;
					if(count($data1) > 0){
						foreach($data1->result() as $row_d){
							$table.= "<tr class='.tr'>
											
											<td style='border: 0;'></td>
											<td style='border: 0;'><span class='padleft'>".$row_d->description."</span></td>
											<td style='border: 0;'></td>
											<td style='border: 0;'></td>
											<td style='border: 0;'></td>
											<td style='border: 0;text-align:center;'>".$row_d->qty."</td>
											<td style='border: 0;text-align:center;'>".$row_d->prices."</td>
											<td style='border: 0;text-align:center;'>".$row_d->amt_dis."</td>
											<td style='border: 0;text-align:center;'>".$row_d->amt_line."</td>
										</tr>";

							$amttotal+=($row_d->amt_line);			
						}

							$table.="<tr>
										<td style='border: 0;'></td>
										<td style='border: 0;'></td>
										<td style='border: 0;'></td>
										<td style='border: 0;'></td>
										<td style='border: 0;'></td>
										<td style='border: 0;'></td>
										<td style='border: 0;'></td>
										<td align='right' style='border: 0;'><strong> Total</strong></td>
										<td align='center' style='border: 0;'' ><strong>".number_format($amttotal,2)."&nbsp;$</strong></td>
									</tr>";

							$gtotal+=($amttotal)-0;	
					}

				}
							$table.="<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td><strong>Grand&nbsp;Total</strong></td>
										<td align='center'><strong>".number_format($gtotal,2)."&nbsp;$</strong></td>
									</tr>";
			}else{

					$table.= "<tr>
								<td colspan='9' style='text-align:center;'>
									<span>We did not find anything to show here...</span> 
								</td>
							</tr>";
			}

		$arr['data']=$table;
		header('Content-Type:text/x-json');
		$arr['body']=$table;
		$arr['pagination']=$paging;
		echo json_encode($arr); 
		die();
	}

}