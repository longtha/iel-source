<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class c_student_assigned_class_list extends CI_Controller{
	    protected $thead;
		protected $idfield;
	    function __construct() {
	        parent::__construct();
	        $this->load->model("student_fee/m_student_assigned_class_list","asignclass");
	        $this->thead = array("No" => 'no',
	        					"Photo"=>'photo',
                                "Student Name" => 'student_num',
                                "Program"=> 'Program',
                                "School Level" => 'sch_level',
                                "Year" => 'sch_year',
								"From Rang Level​"=> 'rangelevelname',
								"To Rang Level​"=> 'to_rangle',
								"From Class"=> 'class_name',
								"To Class"=> 'to_class',
								"Assign By"=> 'create_by',
								"Date" => 'tran_date'
	                            );
	        $this->idfield = "studentid";
	    }
	    function index() {
	        $data['idfield'] = $this->idfield;
        	$data['thead'] = $this->thead;
        	$data['getprogram']=$this->asignclass->getprograms();
        	$data['schooinfor']=$this->asignclass->schooinfor();
	        $this->load->view('header',$data);
	        $this->load->view('student_fee/v_student_assigned_class_list',$data);
	        $this->load->view('footer',$data );
	    }
	    // get_schlevel ----------------------------------------------------------------
	    function get_schlevel(){
			$programid = $this->input->post('programid');
			$get_schlevel = $this->asignclass->get_schlevel($programid);
			header("Content-type:text/x-json");
			echo ($get_schlevel);
		}
		//yera -------------------------------------------------------------------------
		function get_schyera(){
			$schlevelids = $this->input->post('schlevelids');
			$get_schyera = $this->asignclass->get_schyera($schlevelids);
			header("Content-type:text/x-json");
			echo ($get_schyera);
		}
		//from ranglavel ---------------------------------------------------------------
		function get_schranglavel(){
			$yearid = $this->input->post('yearid');
			$get_schranglavel = $this->asignclass->get_schranglavel($yearid);
			header("Content-type:text/x-json");
			echo ($get_schranglavel);
		}
		// from class -----------------------------------------------------------------
		function get_schclass(){
			$schlevelids = $this->input->post('schlevelids');
			$get_schclass = $this->asignclass->get_schclass($schlevelids);
			header("Content-type:text/x-json");
			echo ($get_schclass);
		}
	    // Getdata ---------------------------------------------------------------------------------------
	    function Getdata(){
			$total_display =$this->input->post('sort_num');
			$this->green->setActiveRole($this->input->post('roleid'));
	  		$total_display = $this->input->post('total_display');
	  		$sortby=$this->input->post("sortby");
			$sorttype=$this->input->post("sorttype");
			$student_num = $this->input->post("student_num");
			$schoolid = $this->input->post("schoolid");
			$english_name = $this->input->post("english_name");
			$khmer_name = $this->input->post("khmer_name");
			$gender = $this->input->post("gender");
			$assignby = $this->input->post("assignby");
			$programid = $this->input->post("programid");
			$schlevelids = $this->input->post("schlevelids");
			$yearid = $this->input->post("yearid");
			$fromranglavel = $this->input->post("fromranglavel");
			$toranglavel = $this->input->post("toranglavel");
			$fromclass = $this->input->post("fromclass");
			$toclass = $this->input->post("toclass");
			$fromdate = $this->input->post("fromdate");
			$todate = $this->input->post("todate");
	  		$where='';
			$sortstr="";
			 	if($student_num!= ""){
		    		$where .= "AND v_assigned_class_list.student_num LIKE '%".$student_num."%' ";
		     	}
				if($schoolid!= ""){
		    		$where .= "AND  v_assigned_class_list.schoolid = '".$schoolid."' ";
		    	}
		    	if($english_name!= ""){
		    		$where .= "AND  CONCAT(first_name, ' ', last_name) LIKE '%".$english_name."%' ";
		    	}
		    	if($khmer_name!= ""){
		    		$where .= "AND  CONCAT(first_name_kh, ' ', last_name_kh) LIKE '%".$khmer_name."%' ";
		    	}
		 		if($gender!= ""){
		    		$where .= "AND  v_assigned_class_list.gender = '".$gender."' ";
		    	}
		    	if($assignby!= ""){
		    		$where .= "AND v_assigned_class_list.create_by LIKE '%".$assignby."%' ";
		     	}
		     	if($programid!= ""){
		    		$where .= "AND  v_assigned_class_list.programid = '".$programid."' ";
		    	}
		    	if($schlevelids!= ""){
		    		$where .= "AND  v_assigned_class_list.schlevelid = '".$schlevelids."' ";
		    	}
		    	if($yearid!= ""){
		    		$where .= "AND  v_assigned_class_list.year = '".$yearid."' ";
		    	}
				if($fromranglavel!= ""){
		    		$where .= "AND  v_assigned_class_list.from_rangelevelid = '".$fromranglavel."' ";
		    	}
		    	if($toranglavel!= ""){
		    		$where .= "AND  v_assigned_class_list.to_rangle = '".$toranglavel."' ";
		    	}
		    	if($fromclass!= ""){
		    		$where .= "AND  v_assigned_class_list.from_classid = '".$fromclass."' ";
		    	}
				if($toclass!= ""){
		    		$where .= "AND  v_assigned_class_list.to_class = '".$toclass."' ";
		    	}
				if($fromdate != ''){
				 $where .= "AND v_assigned_class_list.tran_date >= '".$this->green->formatSQLDate($fromdate)."'";    
				}
				if($todate != ''){
					 $where .= "AND v_assigned_class_list.tran_date <= '".$this->green->formatSQLDate($todate)."'";     
				}
		        (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

			$sqr="SELECT * FROM
					v_assigned_class_list 
				WHERE 1=1 {$where}";

			
			$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_assigned_class_list WHERE 1=1 {$where}")->row()->numrow;

				$getperpage=0;
				if($total_display==''){
					$getperpage=10;
				}else{
					$getperpage=$total_display;
				}
				$paging    = $this->green->ajax_pagination($total_row,site_url()."student_fee/c_student_assigned_class_list",$getperpage,"icon");
				//return "$sql limit {$paging['start']}, {$paging['limit']}";
				if($sortstr!=""){
					$sqr.=$sortstr;
				}
				$getlimit=10;
				if($paging['limit']!=''){
					$getlimit=$paging['limit'];
				}
			$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
				$j='';
				$table='';
				if(count($data) > 0){
					foreach($data as $row){  //$this->db->query($sqr)->result() as $row

						$qr_class_name= $this->db->query("SELECT
																sch_class.class_name,
																sch_student_assign_class.to_class
															FROM
																sch_student_assign_class
															INNER JOIN sch_class ON sch_student_assign_class.to_class = sch_class.classid
															WHERE
																sch_student_assign_class.to_class = '{$row->to_class}' ")->row()->class_name;
						$qr_rangelevelname = $this->db->query("SELECT
																sch_student_assign_class.to_rangle,
																sch_school_rangelevel.rangelevelname
															FROM
																sch_student_assign_class
															INNER JOIN sch_school_rangelevel ON sch_student_assign_class.to_rangle = sch_school_rangelevel.rangelevelid
															WHERE
																sch_student_assign_class.to_rangle = '{$row->to_rangle}'")->row()->rangelevelname;
						$j++;
						$have_img = base_url()."assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg';		
						$no_imgs = base_url()."assets/upload/students/NoImage.png";	
						$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
							if (file_exists(FCPATH . "assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg')) {				
								$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
							}
							
						$table.= "<tr>
									<td class='no'>".$j."</td>
									<td class='photo'>".$img."</td>
									<td class='student_num'><strong>".$row->student_num."</strong></br>".$row->last_name_kh.'  '.$row->first_name_kh."</br>".$row->first_name.'  '.$row->last_name."</br>".$row->gender."</td>	
									<td class='program'>".$row->program."</td>
									<td class='sch_level'>".$row->sch_level."</td>
									<td class='sch_year'>".$row->sch_year."</td>
									<td class='rangelevelname'>".$row->rangelevelname."</td>
									<td class='to_rangle'>".$qr_rangelevelname."</td>
									<td class='class_name'>".$row->class_name."</td>
									<td class='to_class'>".$qr_class_name."</td>
									<td class='create_by'>".$row->create_by."</td>
									<td class='tran_date'>".$this->green->convertSQLDate($row->tran_date)."</td>
									<td class='remove_tag no_wrap'>";
							$table.= " </td>
							 </tr> ";	
							
				}
			}else{
					$table.= "<tr>
								<td colspan='12' style='text-align:center;'> 
					We did not find anything to show here.
								</td>
							</tr>";
				}

			$arr['data']=$table;
			header('Content-Type:text/x-json');
			$arr['body']=$table;
			$arr['pagination']=$paging;
			echo json_encode($arr); 
			die();
		}

	}