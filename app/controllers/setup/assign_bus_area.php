<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assign_bus_area extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->load->model('setup/assign_bus_area_model', 'm');
		$this->load->model('setup/bus_model', 'b');
		$this->load->model('setup/area_model', 'a');
		
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('setup/assign_bus_area/index');
		$this->load->view('footer');	
	}

	public function save(){
		$i = $this->m->save();		
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	public function grid(){		
		$bus_area = $this->m->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $bus_area;		
	}

	public function edit(){	
		$busareaid = $this->input->post('busareaid') - 0;
		$row = $this->m->edit($busareaid);
		header('Content-Type: application/json; charset=utf-8');
		echo $row;		
	}

	public function delete(){	
		$busareaid = $this->input->post('busareaid') - 0;
		$i = $this->m->delete($busareaid);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	public function get_bus(){	
		$i = $this->m->get_bus();
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	public function get_area(){	
		$i = $this->m->get_area();
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	public function chk_busid(){	
		$busno = trim($this->input->post('busno'));
		$i = $this->m->chk_busid($busno);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	public function chk_areaid(){	
		$area = trim($this->input->post('area'));
		$i = $this->m->chk_areaid($area);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	public function get_bus_area(){		
		$i = $this->m->get_bus_area();
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

}