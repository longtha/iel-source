<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->load->model('setup/location_model', 'm');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('setup/location/index');
		$this->load->view('footer');	
	}

	public function save(){

		$type = $this->input->post("location_type");
		$type_name = $this->input->post("type_name");
		$loc_id = $this->input->post("location_id");
		$province_id = $this->input->post("province_id");
		$i = 0;
		$validateLoc = $this->validateLoc($type, $type_name, $loc_id, $province_id);
		if($validateLoc>0){
			$i = 0;
		}else{
			$i = $this->m->save();
		}			
		header('Content-Type: application/json; charset=utf-8');
		echo $i;
	}

	public function grid(){

		$province_id = $this->input->post('s_province_id');		
		$district_id = $this->input->post('s_district_id');
		$commune_id = $this->input->post('s_commune_id');
		$village_id = $this->input->post('s_village_id');

		$location = $this->m->getlocations($province_id, $district_id, $commune_id, $village_id);
		
		$loc ='';
		if(count($location)>0){
			$arrPro = array();
			$arrDis = array();
			$arrCom = array();
			foreach ($location as $row){				
				
				$loc .='<tr>
					<td><strong>'. (!in_array($row->province_id,$arrPro)?$row->province_name:'') .'</strong></td>                       
                           <td>'. (!in_array($row->district_id,$arrDis)?$row->district_name:'') .'</td>
                           <td>'. (!in_array($row->commune_id,$arrCom)?$row->commune_name:'') .'</td>
                           <td>'.$row->village_name.'</td>
                           <td style="text-align: center;">
                           		<a href="javascript:;" class="edit" data-typeid="'.$row->type.'" data-locid="'.$row->loc_id.'" data-toggle="tooltip" data-placement="left" title="Edit">
                                	<img src="'.base_url('assets/images/icons/edit.png').'" style="width: 16px;height: 16px;">
                              	</a> |
                              	<a href="javascript:;" class="delete" data-typeid="'.$row->type.'" data-locid="'.$row->loc_id.'" data-toggle="tooltip" data-placement="right" title="Delete">
                                	<img src="'.base_url('assets/images/icons/delete.png').'">
                              	</a>
                           </td>
					</tr>';
				$arrPro[] = $row->province_id;
				$arrDis[] = $row->district_id;
				$arrCom[] = $row->commune_id;
			}
		}else{
			$loc.='<tr><td colspan="5">We did not find anything to show here</td></tr>';
		}
		header('Content-Type: application/json; charset=utf-8');
		$arr_success['location'] = $loc;
		echo json_encode($arr_success);	
	}

	public function edit(){	
		$loc_id = $this->input->post('loc_id') - 0;
		$loc_type = $this->input->post('loc_type') - 0;	
		$row = $this->m->getlocation($loc_id, $loc_type);		
		$province_id = $row->province_id;
		$district_id = $row->district_id;
		$commune_id = $row->commune_id;
		$village_id = $row->village_id;
		$loc_name   = $row->loc_name;

		$province ="";
		$district ="";
		$commune ="";
		$village ="";

		$location = $this->m->getlocations($province_id, $district_id, $commune_id, $village_id);
		if(count($location)>0){

			//if($loc_type=='1'){
			foreach ($location as $row_province){

				$province .='<option value="'.$row_province->province_id.'"'.($row_province->province_id==$province_id?"selected=selected":"").'>'.$row_province->province_name.'</optoin>';
			}
			//}		
			if($loc_type=='2'){
				foreach ($location as $row_district){
					$district .='<option value="'.$row_district->district_id.'"'.($row_district->district_id==$district_id?"selected=selected":"").'>'.$row_district->district_name.'</optoin>';
				}	
			}
			if($loc_type=='3' || $loc_type=='4'){
				foreach ($location as $row_district){
					$district .='<option value="'.$row_district->district_id.'"'.($row_district->district_id==$district_id?"selected=selected":"").'>'.$row_district->district_name.'</optoin>';
				}
				foreach ($location as $row_commune){
					$commune .='<option value="'.$row_commune->commune_id.'"'.($row_commune->commune_id==$commune_id?"selected=selected":"").'>'.$row_commune->commune_name.'</optoin>';
				}	
			}
		}		
		
		header('Content-Type: application/json; charset=utf-8');
		$arr_success['province'] = $province;
		$arr_success['district'] = $district;
		$arr_success['commune'] = $commune;
		$arr_success['village'] = $village_id;
		$arr_success['loc_name'] = $loc_name;

		echo json_encode($arr_success);			
	}
	function validateLoc($type="", $type_name="", $loc_id="", $province_id=""){	
		$validateLoc = $this->m->validateLoc($type, $type_name, $loc_id, $province_id);
		return $validateLoc;
	}
	public function delete(){
		$loc_id = $this->input->post('loc_id') - 0;
		$loc_type = $this->input->post('loc_type') - 0;

		$i = $this->m->delete($loc_id, $loc_type);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}
	function cgetprovince(){		
		$province_id = $this->input->post("province_id");
		$option_province='';
		$getprovince = $this->m->getprovince($province_id);
		if(count($getprovince)>0){
			foreach ($getprovince as $row){
				$option_province .='<option value="'.$row->province_id.'">'.$row->name.'</option>';
			}					
		}		
		header('Content-Type: application/json; charset=utf-8');
		$arr_success['province'] = $option_province;
		echo json_encode($arr_success);
	}
	function cgetdistrict(){		
		$province_id = $this->input->post("province_id");
		$option_district='<option value=""></option>';
		$getdistrict = $this->m->getdistrict($province_id);
		if(count($getdistrict)>0){
			foreach ($getdistrict as $row){
				$option_district .='<option value="'.$row->district_id.'">'.$row->name.'</option>';
			}					
		}		
		header('Content-Type: application/json; charset=utf-8');
		$arr_success['district'] = $option_district;
		echo json_encode($arr_success);
	}
	function cgetcommune(){		
		$province_id = $this->input->post("province_id");
		$district_id = $this->input->post("district_id");
		$option_commune='<option value=""></option>';		
		$getcommune = $this->m->getcommune($province_id, $district_id);
		if(count($getcommune)>0){
			foreach ($getcommune as $row){
				$option_commune .='<option value="'.$row->commune_id.'">'.$row->name.'</option>';
			}
		}
		header('Content-Type: application/json; charset=utf-8');
		$arr_success['commune'] = $option_commune;
		echo json_encode($arr_success);
	}
	function cgetvillage(){		
		$province_id = $this->input->post("province_id");
		$district_id = $this->input->post("district_id");
		$commune_id  = $this->input->post("commune_id");
		$option_village='<option value=""></option>';		
		$getvillage = $this->m->getvillage($province_id, $district_id, $commune_id);
		if(count($getvillage)>0){
			foreach ($getvillage as $row){
				$option_village .='<option value="'.$row->village_id.'">'.$row->name.'</option>';
			}
		}
		header('Content-Type: application/json; charset=utf-8');
		$arr_success['village'] = $option_village;
		echo json_encode($arr_success);
	}
}