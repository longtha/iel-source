<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->load->model('setup/area_model', 'm');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('setup/area/index');
		$this->load->view('footer');	
	}

	public function save(){
		$i = $this->m->save();

		// if($this->input->post('save') == 'save'){
		// 	redirect('setup/area/index');
		// }	
		
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	public function grid(){		
		$bus = $this->m->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $bus;		
	}

	public function edit(){	
		$areaid = $this->input->post('areaid') - 0;

		$row = $this->m->edit($areaid);
		header('Content-Type: application/json; charset=utf-8');
		echo $row;		
	}

	public function delete(){	
		$areaid = $this->input->post('areaid') - 0;
		$i = $this->m->delete($areaid);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	

}