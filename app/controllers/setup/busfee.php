<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Busfee extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->load->model('setup/busfee_model', 'm');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('setup/busfee/index');
		$this->load->view('footer');	
	}

	public function save(){
		$i = $this->m->save();
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	public function grid(){		
		$busfee = $this->m->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $busfee;		
	}

	public function edit(){	
		$otherfeeid = $this->input->post('otherfeeid') - 0;
		$row = $this->m->edit($otherfeeid);
		header('Content-Type: application/json; charset=utf-8');
		echo $row;		
	}

	public function delete(){	
		$otherfeeid = $this->input->post('otherfeeid') - 0;
		$i = $this->m->delete($otherfeeid);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	

}