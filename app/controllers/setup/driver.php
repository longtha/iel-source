<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Driver extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->load->model('setup/driver_model', 'm');
		$this->load->model('setup/bus_model', 'b');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('setup/driver/index');
		$this->load->view('footer');	
	}

	public function save(){
		$i = $this->m->save();		
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	public function grid(){		
		$driver = $this->m->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $driver;		
	}

	public function edit(){	
		$driverid = $this->input->post('driverid') - 0;

		$row = $this->m->edit($driverid);
		header('Content-Type: application/json; charset=utf-8');
		echo $row;		
	}

	public function delete(){	
		$driverid = $this->input->post('driverid') - 0;
		$i = $this->m->delete($driverid);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	

}