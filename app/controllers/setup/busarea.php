<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Busarea extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->load->model('setup/busarea_model', 'm');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('setup/busarea/index');
		$this->load->view('footer');	
	}

	public function save(){
		$i = $this->m->save();

		// if($this->input->post('save') == 'save'){
		// 	redirect('setup/busarea/index');
		// }
		header('Content-Type: application/json; charset=utf-8');
		echo $i;
		
	}

	public function grid(){
		$busarea = $this->m->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $busarea;		
	}

	public function edit(){	
		$busareaid = $this->input->post('busareaid') - 0;
		$row = $this->m->edit($busareaid);
		header('Content-Type: application/json; charset=utf-8');
		echo $row;		
	}

	public function delete(){	
		$busareaid = $this->input->post('busareaid') - 0;
		$i = $this->m->delete($busareaid);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}
	
	function checkClass($classid){
	$result=0;
	if($classid!=""){
		/*check in enrollment*/
		$this->db->where("classid",$classid);
		$cls_ent=$this->db->get("sch_student_enrollment")->row();
		if(isset($cls_ent->studentid)){
			$result=1;
		}
		/*check in schedule*/
	}
	return $result;
	}

}