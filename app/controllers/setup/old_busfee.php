<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Old_busfee extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->load->model('setup/Old_busfee_model', 'm');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('setup/old_busfee/index');
		$this->load->view('footer');	
	}

	public function save(){
		$i = $this->m->save();
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	public function grid(){		
		$busfee = $this->m->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $busfee;		
	}

	public function edit(){	
		$buspriceid = $this->input->post('buspriceid') - 0;
		$row = $this->m->edit($buspriceid);
		header('Content-Type: application/json; charset=utf-8');
		echo $row;		
	}

	public function delete(){	
		$buspriceid = $this->input->post('buspriceid') - 0;
		$i = $this->m->delete($buspriceid);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}	

}