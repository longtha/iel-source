<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bus extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->load->model('setup/bus_model', 'm');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('setup/bus/index');
		$this->load->view('footer');	
	}

	public function save(){
		$i = $this->m->save();		
		header('Content-Type: application/json; charset=utf-8');
		echo $i;
	}

	public function grid(){		
		$bus = $this->m->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $bus;
	}

	public function edit(){	
		$busid = $this->input->post('busid') - 0;	
		$row = $this->m->edit($busid);
		header('Content-Type: application/json; charset=utf-8');
		echo $row;		
	}

	public function delete(){	
		$busid = $this->input->post('busid') - 0;
		$i = $this->m->delete($busid);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

	

}