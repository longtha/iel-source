<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class otherfee extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->load->model('setup/modotherfee', 'fe');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('setup/otherfee/v_otherfee');
		$this->load->view('footer');	
	}

	public function save(){
		$i = $this->fe->save();

		// if($this->input->post('save') == 'save'){
		// 	redirect('setup/bus/index');
		// }	
		
		header('Content-Type: application/json; charset=utf-8');
		echo $i;
	}

	public function gatedata(){
		$other = $this->fe->gatedata();
		header('Content-Type: application/json; charset=utf-8');
		echo $other;		
	}

	public function edit(){	
		$otherfeeid = $this->input->post('otherfeeid') - 0;	
		$row = $this->fe->edit($otherfeeid);
		header('Content-Type: application/json; charset=utf-8');
		echo $row;		
	}

	public function delete(){	
		$otherfeeid = $this->input->post('otherfeeid') - 0;
		$i = $this->fe->delete($otherfeeid);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}
}