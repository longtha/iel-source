<?php
    class Login extends CI_Controller {
		public function index()
		{
			$this->session->sess_destroy();
			if($this->session->userdata('user_name')!=""){
				$data['page_header']="GreenSIM";
				$this->load->view('header',$data);
				$this->load->view('index',$data);
				$this->load->view('footer');
			}else{
				$data['page_header']="GreenSIM-Login";
				$this->load->view('login',$data);
			}
		}
		public function getLogin()
		{
			$user_name=$this->input->post('user_name');
			$password=$this->input->post('password');

			
			$sql_userinf="SELECT
				userid,
				user_name,
				`password`,
				roleid,
				last_name,
				first_name,
				def_sch_level,
				def_dashboard,
				def_open_page,
      			schoolid,
      			emp_id,
      			match_con_posid
			FROM
				sch_user
			WHERE
				user_name = '".$user_name."'
			AND `password` =  '".md5($password)."'
			AND is_active = 1";

			$userinf=$this->green->getOneRow($sql_userinf);
			if(count($userinf)>0 && $userinf['userid']!=""){
				// Student login
				if ($this->input->post('status') == 'on') {
					// $schoolid = $this->input->post('schoolid');
					// //$year = $this->db->query("SELECT yearid FROM sch_school_year ORDER BY from_date DESC LIMIT 1")->row()->yearid;
					// // More year
					// $this->green->runSQL("UPDATE sch_user SET last_visit = NOW() WHERE userid='".$userinf['userid']."'");
					// $this->session->set_userdata($userinf);
					// $this->session->set_userdata("roleid",$userinf['roleid']);
					// $this->green->getModule($userinf['roleid']);

					// $this->session->set_userdata("moduleids",$this->green->moduleids);

					if ($userinf['roleid'] == 1) {
						$this->green->goToPage(site_url('login'));
						die();
					}


					$schoolid = $userinf['schoolid'];
					$year = $this->db->query("SELECT yearid FROM sch_school_year ORDER BY from_date DESC LIMIT 1")->row()->yearid;
					$this->session->set_userdata("match_con_posid",$userinf['match_con_posid']);
					$this->session->set_userdata("emp_id",$userinf['emp_id']);
					$this->session->set_userdata($userinf);
					$this->session->set_userdata("roleid",$userinf['roleid']);
					$this->green->getModule($userinf['roleid']);

					$this->session->set_userdata("moduleids",$this->green->moduleids);
					$arrModules=$this->green->moduleids;
					$arrModInfos=array();
					$arrPageInfos=array();
					$arrPageAction=array();

					if(count($arrModules)>0){
						foreach ($arrModules as $moduleid) {
							$this->green->getRolePage($moduleid['moduleid']);
							$arrPages=$this->green->pageids;

							if(count($arrPages)>0){
								foreach($arrPages as $page){
									$this->green->getPageInfo($page['pageid']);
									$arrPageInfos[$moduleid['moduleid']][$page['pageid']]=$this->green->pageinfos;
									$arrPageAction[$moduleid['moduleid']][$page['pageid']]=$page['is_read'];
								}
							}
	// print_r($arrPageInfos);
							$arrModInfos[$moduleid['moduleid']]=$this->green->getModuleInfo($moduleid['moduleid']);
						}
					}

					$this->session->set_userdata("roleid",$userinf['roleid']);
					$this->session->set_userdata("ModuleInfors",$arrModInfos);
					$this->session->set_userdata("PageInfors",$arrPageInfos);
					$this->session->set_userdata("PageAction",$arrPageAction);

					$this->session->set_userdata("schoolid",$schoolid);
					$this->session->set_userdata("year",$year);
					$this->session->set_userdata("def_sch_level",$userinf['def_sch_level']);

					if ($userinf['def_dashboard']!='') {
						if($userinf['def_dashboard']=="/system/dashboard/view_health/"){
							$s_date=date('Y-m-d',strtotime("-3 months"));
							$e_date=date('Y-m-d');
							$param=$s_date.'/'.$e_date;
							$this->green->goToPage($userinf['def_dashboard'].'/'.$param);
						}else{
							if($userinf['def_dashboard']=="/system/dashboard"){
								$this->green->goToPage($userinf['def_dashboard']);
							}else{
								$this->green->goToPage($userinf['def_dashboard'].'/'.$year);
							}
						}
					}else if($userinf['def_open_page'] != ''){
						$this->green->goToPage($userinf['def_open_page']);
	                } else{
						$this->green->goToPage("home");
					}

					// if ($userinf['def_open_page'] != '' && $userinf['roleid'] == 5) {
					// 	$this->green->goToPage(site_url($userinf['def_open_page']));
	    //             } else {
	    //             	$this->green->goToPage(site_url('login'));
	    //             }
				}else {
					// Member or Admin login
					$schoolid=$this->input->post('schoolid');
					$year=$this->input->post('year');
					$this->green->runSQL("UPDATE sch_user SET last_visit = NOW() WHERE userid='".$userinf['userid']."'");
					$this->session->set_userdata($userinf);
					$this->session->set_userdata("roleid",$userinf['roleid']);
					$this->green->getModule($userinf['roleid']);

					$this->session->set_userdata("moduleids",$this->green->moduleids);
					$arrModules=$this->green->moduleids;
					$arrModInfos=array();
					$arrPageInfos=array();
					$arrPageAction=array();

					if(count($arrModules)>0){

						foreach ($arrModules as $moduleid) {
							$this->green->getRolePage($moduleid['moduleid']);
							$arrPages=$this->green->pageids;

							if(count($arrPages)>0){
								foreach($arrPages as $page){
									$this->green->getPageInfo($page['pageid']);
									$arrPageInfos[$moduleid['moduleid']][$page['pageid']]=$this->green->pageinfos;
									$arrPageAction[$moduleid['moduleid']][$page['pageid']]=$page['is_read'];
								}
							}
	// print_r($arrPageInfos);
							$arrModInfos[$moduleid['moduleid']]=$this->green->getModuleInfo($moduleid['moduleid']);
						}
					}

					$this->session->set_userdata("roleid",$userinf['roleid']);
					$this->session->set_userdata("ModuleInfors",$arrModInfos);
					$this->session->set_userdata("PageInfors",$arrPageInfos);
					$this->session->set_userdata("PageAction",$arrPageAction);

					$this->session->set_userdata("schoolid",$schoolid);
					$this->session->set_userdata("year",$year);
					$this->session->set_userdata("def_sch_level",$userinf['def_sch_level']);

					if($userinf['roleid']==1) {
						$this->green->goToPage("system/dashboard");
					}
					elseif ($userinf['def_dashboard']!='') {
						if($userinf['def_dashboard']=="/system/dashboard/view_health/"){
							$s_date=date('Y-m-d',strtotime("-3 months"));
							$e_date=date('Y-m-d');
							$param=$s_date.'/'.$e_date;
							$this->green->goToPage($userinf['def_dashboard'].'/'.$param);
						}else{
							if($userinf['def_dashboard']=="/system/dashboard"){
								$this->green->goToPage($userinf['def_dashboard']);
							}else{
								$this->green->goToPage($userinf['def_dashboard'].'/'.$year);
							}
						}
					}else if($userinf['def_open_page'] != ''){
						$this->green->goToPage($userinf['def_open_page']);
	                }

					else{
						$this->green->goToPage("home");
					}
				}	

			} else {
				$this->green->goToPage(site_url('login'));
			}
			


			// $user_name=$this->input->post('user_name');
			// $password=$this->input->post('password');
			// $schoolid=$this->input->post('schoolid');
			// $year=$this->input->post('year');

			
/*
        $sql_userinf="SELECT
            userid,
            user_name,
            `password`,
            sch_user.roleid,
            sch_user.last_name,
            sch_user.first_name,
            sch_user.def_sch_level,
            sch_user.def_dashboard,
            sch_user.def_open_page
          FROM
            sch_user
          WHERE
            user_name = '".$user_name."'
          AND `password` =  '".md5($password)."'
          AND schoolid= '".$schoolid."'
          AND is_active = 1";
*/

/*
			$userinf=$this->green->getOneRow($sql_userinf);

			if(count($userinf)>0 && $userinf['userid']!=""){
        $schoolid = $userinf['schoolid'];
        $year = $this->db->query("SELECT yearid FROM sch_school_year ORDER BY from_date DESC LIMIT 1")->row()->yearid;

				$this->green->runSQL("UPDATE sch_user SET last_visit = NOW() WHERE userid='".$userinf['userid']."'");
				$this->session->set_userdata($userinf);
				$this->session->set_userdata("roleid",$userinf['roleid']);
				$this->green->getModule($userinf['roleid']);

				$this->session->set_userdata("moduleids",$this->green->moduleids);
				$arrModules=$this->green->moduleids;

				$arrModInfos=array();
				$arrPageInfos=array();
				$arrPageAction=array();

				if(count($arrModules)>0){

					foreach ($arrModules as $moduleid) {
						$this->green->getRolePage($moduleid['moduleid']);
						$arrPages=$this->green->pageids;

						if(count($arrPages)>0){
							foreach($arrPages as $page){
								$this->green->getPageInfo($page['pageid']);
								$arrPageInfos[$moduleid['moduleid']][$page['pageid']]=$this->green->pageinfos;
								$arrPageAction[$moduleid['moduleid']][$page['pageid']]=$page['is_read'];
							}
						}
// print_r($arrPageInfos);
						$arrModInfos[$moduleid['moduleid']]=$this->green->getModuleInfo($moduleid['moduleid']);
					}
				}

				$this->session->set_userdata("roleid",$userinf['roleid']);
				$this->session->set_userdata("ModuleInfors",$arrModInfos);
				$this->session->set_userdata("PageInfors",$arrPageInfos);
				$this->session->set_userdata("PageAction",$arrPageAction);

				$this->session->set_userdata("schoolid",$schoolid);
				$this->session->set_userdata("year",$year);
				$this->session->set_userdata("def_sch_level",$userinf['def_sch_level']);

				if($userinf['roleid']==1) {
					$this->green->goToPage("system/dashboard");
				}
				elseif ($userinf['def_dashboard']!='') {
					if($userinf['def_dashboard']=="/system/dashboard/view_health/"){
						$s_date=date('Y-m-d',strtotime("-3 months"));
						$e_date=date('Y-m-d');
						$param=$s_date.'/'.$e_date;
						$this->green->goToPage($userinf['def_dashboard'].'/'.$param);
					}else{
						if($userinf['def_dashboard']=="/system/dashboard"){
							$this->green->goToPage($userinf['def_dashboard']);
						}else{
							$this->green->goToPage($userinf['def_dashboard'].'/'.$year);
						}
					}
				}else if($userinf['def_open_page'] != ''){
					$this->green->goToPage($userinf['def_open_page']);
                }

				else{
					$this->green->goToPage("home");
				}

			}else{
				$this->green->goToPage(site_url('login'));
			}
*/
		}
		public function logOut(){
			$this->session->sess_destroy();
			$this->green->goToPage(site_url('login'));
		}

    }

?>
