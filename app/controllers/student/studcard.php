<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Studcard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('student/mstudcard','mstd');
		
	}
	public function index()
	{
		$data['page_header'] = 'Student Badge List';		
		$this->load->view('header', $data);
		$this->load->view('student/studcard/vstudcard');
		$this->load->view('footer');
	}
	public function loadstudcard()
	{
		$data['page_header'] = 'Student Badge List';
		$setyear = $this->input->post('yearid');
		$setclass = $this->input->post('classid');
		if(!empty($setyear) && !empty($setclass))
		{
			$yearid=$setyear;
			$class_id =$setclass;
		}
		else
		{
			$yearid=$this->session->userdata('year');
			$class_id = 1;	
		}
		$datas = $this->mstd->priviewstudcard('',$yearid,$class_id);
		if(count($datas) > 0)
		{
			$tr='<tr>';
			$i=1;
			foreach($datas->result() as $student)
			{
				$breakname = $student->len<=16 ?"<br/>&nbsp;":"";
				$this->db->where('yearid',$yearid);
				$year=$this->db->get('sch_school_year')->row()->sch_year;
				$img_path=base_url('assets/upload/No_person.jpg');
			    if(file_exists(FCPATH.'assets/upload/students/'.$yearid.'/'.$student->student_num.'.jpg')){
			        $img_path=base_url('assets/upload/students/'.$yearid.'/'.$student->student_num.'.jpg');
			       
			    } 
			    $fullname = $student->last_name.' '.$student->first_name.$breakname;
				$tr.= $this->stdlistcard($img_path,$student->class_name,$fullname,$year);
				if(($i%2)==0)
				{
					$tr.='<tr>';
				}
				else if(($i%10)==0)
				{
					$tr.='<tr style="page-break-after:always;">';
				}
				$i++;	
			}
			$tr.='</tr>';
		}
		$arraystudcard = Array();
		header('Content-type:text/x-json');
		$arraystudcard['tdatastudcard'] = $tr;
		echo json_encode($arraystudcard);
		die();
	}
	public function getAutoResult()
	{
		$searchdatamulti=$this->input->post("searchdatamulti");
		$yearid=$this->input->post("yearid");
		$classid=$this->input->post("classid");
		if($searchdatamulti !=""){
			$data=$this->mstd->priviewstudcard($searchdatamulti,$yearid,$classid,1);
			$tr='';
			if(count($data)>0){
				foreach ($data->result() as $row) {
				$fullname_en = $row->last_name." ".$row->first_name;
				$fullname_kh = $row->last_name_kh." ".$row->first_name_kh;
				$tr.=$this->studcardindialogList($row->student_num,$row->familyid,$fullname_en,$fullname_kh,$row->classid,$row->class_name);
				}
			}
		}
		$arrJson=array();
		header("Content-type:text/x-json");
		$arrJson['tdata']=$tr;
		echo json_encode($arrJson);

		exit();
	}
	public function studcardindialogList($student_num,$familyid,$fullname_en,$fullname_kh,$classid,$class_name)
	{
		$tr ="<tr>
				<td>
					<input type='checkbox' checked='checked' id='studcardid' class='flat-red foodBox studcardid' name='studcardid' value='".$student_num."' /> 
					<input type='hidden' id='classid' value='".$classid."'/>
					".$student_num."
				</td>
				<td>".$familyid."</td>
				<td>".$fullname_en."</td>
				<td>".$fullname_kh."</td>
				<td>".$class_name."</td>
			</tr>";
		return $tr;
	}
	public function addstudcardtolist()
	{
		$getArrstd = $this->input->post("arrstd");
		if(count($getArrstd) > 0)
		{
			$tr ='<tr>';
			$i=1;
			foreach($getArrstd as $rowstd)
			{
				$datalist=$this->mstd->priviewstudcard($rowstd['stdid'],$rowstd['yearid'],$rowstd['classid'],1);
				if(count($datalist) > 0)
				{	
					foreach($datalist->result() as $student)
					{
						$breakname = $student->len<=16 ?"<br/>&nbsp;":"";
						$this->db->where('yearid',$rowstd['yearid']);
						$year=$this->db->get('sch_school_year')->row()->sch_year;
						$img_path=base_url('assets/upload/No_person.jpg');
					    if(file_exists(FCPATH.'assets/upload/students/'.$rowstd['yearid'].'/'.$student->student_num.'.jpg')){
					        $img_path=base_url('assets/upload/students/'.$rowstd['yearid'].'/'.$student->student_num.'.jpg');
					       
					    } 
					    $fullname = $student->last_name.' '.$student->first_name.$breakname;
						$tr.= $this->stdlistcard($img_path,$student->class_name,$fullname,$year);
					}	
				}
				if(($i%2)==0)
				{
					$tr.='<tr>';
				}
				else if(($i%10)==0)
				{
					$tr.='<tr style="page-break-after:always;">';
				}
				$i++;
			}
			$tr.='</tr>';
		}
		$arrstdJson = array();
		header("Content-type:text/x-json");
		$arrstdJson['student'] = $tr;
		echo json_encode($arrstdJson);
		die();
	}
	public function stdlistcard($img_path,$class_name,$studentfullname,$year)
	{
		return '<td>
					<table  border="0"  style="width:50%;margin:5px 7px; border:1px solid #ccc;">
						<tr>
							<td style="width:155px;height:180px;border-right:none;" align="center" valign="middle">
								<img src="'.$img_path.'" style="width:155px; height:180x; padding:4px;"/>
							</td>
							<td style="border-left:none;">
								<table border="0" width="100%">
									<tr>
										<td height="70"><img src="'.base_url('assets/images/logo/logo.png').'" width="270" height="70"></td>
									</tr>
									<tr>
										<td height="45" valign="bottom" align="center"><label class="control-label larg_font_class">CLASS '.$class_name.' </label></td>
									</tr>
									<tr>
										<td height="45" valign="bottom" align="center">
											<label class="control-label larg_font_name">
											'.$studentfullname.'
											</label>
										</td>
									</tr>
									<tr>
										<td valign="top" align="center"><label class="control-label larg_font_year">'.$year.' </label></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>';	
	}
		
	public function fillstudentcard()
	{
		$key=$_GET['term'];
		$where ="";
		if($key !=""){
			$where.=" AND (s.student_num like '".$key."%'
							OR s.last_name like '".$key."%'
							OR s.first_name like '".$key."%'
							OR s.last_name_kh like '".$key."%'
							OR s.first_name_kh like '".$key."%'
						)";
		}
		$data=$this->mstd->makesqlsyntax($where);
		$array=array();
		foreach ($data->result() as $row) {
			$fullnamestd = $row->last_name.' '.$row->first_name.' ('.$row->last_name_kh.' '.$row->first_name_kh.')';
			$array[]=array(
							'value'=>$fullnamestd,
							'student_num'=>$row->student_num
						);
		}
		echo json_encode($array);
	}
}