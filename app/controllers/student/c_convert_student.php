<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_convert_student extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('student/m_convert_student', 'convert');

		$this->thead = array("No" => 'no',
	        					"Photo"=>'Photo',
	        					"Student Name"=>'student_num',
	        					"Program"=>'programid',
	        					"School Level"=>'schlevelid',
	        					"Year"=>'year',
	        					"Range Level"=>'rangelevelid',
	        					"From Class"=>'classid',
	        					"To Class"=>'class_name'
	                            );
	        $this->idfield = "studentid";
	}

	public function index(){	
		$data['idfield'] = $this->idfield;
        $data['thead'] = $this->thead;
        $data['getprogram']=$this->convert->getprograms();
		$this->load->view('header',$data);
		$this->load->view('student/v_convert_student',$data);
		$this->load->view('footer',$data);	
	}
	//save -------------------------------------------
	function savedata(){
		$enrollid = $this->input->post('enrollid');
		$enrollid=$this->convert->saved($enrollid);
		$arr=array('enrollid'=>$enrollid);
		header("Content-type:text/x-json");
		echo json_encode($arr);
	}
	
	// get_schlevel -----------------------------------
    function get_schlevel(){
		$programid = $this->input->post('programid');
		$get_schlevel = $this->convert->get_schlevel($programid);
		//header("Content-type:text/x-json");
		echo ($get_schlevel);
	}

	//yera --------------------------------------------
	function get_schyera(){
		$schlevelids = $this->input->post('schlevelids');
		$get_schyera = $this->convert->get_schyera($schlevelids);
		echo ($get_schyera);
	}

	//from ranglavel ------------------------------------
	function schranglavel(){
		$yearid = $this->input->post('yearid');
		$schranglavel = $this->convert->schranglavel($yearid);
		echo ($schranglavel);
	}

	// from class -------------------------------------
	function get_schclass(){
		$rangelevelid = $this->input->post('rangelevelid');
		$get_schclass = $this->convert->get_schclass($rangelevelid);
		echo ($get_schclass);
	}
	// to class -------------------------------------
	function get_toschclass(){
		$rangelevelid = $this->input->post('rangelevelid');
		$get_toschclass = $this->convert->get_schclass($rangelevelid);
		echo ($get_toschclass);
	}
	//=============
		function sendmeil(){
			$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => '465',
			'smtp_user' => 'sonearthbbt@gmail.com',
			'smtp_pass' => '069484876'
			);

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");

			$this->email->from('sonearthbbt@gmail.com', 'Welcome GreenICT');
			//$this->email->to('lim.mengleang@gmail.com');			
			$this->email->to('chhourn.sophorn@gmail.com');

			$this->email->subject('Test Mail');
			$this->email->message('Test Message');

			if($this->email->send())
			{
				echo 'Your Email Was Send, Fool';
			}
			else
			{
				show_error($this->email->print_debugger());
			}
		}
	//=============

   // getdata -------------------------------------------
    function Getdata(){
    	
    	$this->green->setActiveRole($this->input->post('roleid'));
		$total_display = $this->input->post('sort_num');
  		$total_display = $this->input->post('total_display');
  		$sortby=$this->input->post("sortby");
		$sorttype=$this->input->post("sorttype");
		$student_num = trim($this->input->post('student_num', TRUE));
		$full_name = trim($this->input->post('full_name', TRUE));
		$yearid = trim($this->input->post('yearid', TRUE));
		$programid = trim($this->input->post('programid', TRUE));
		$schlevelids = trim($this->input->post('schlevelids', TRUE));
		$classid = trim($this->input->post('classid', TRUE));
		$toclassid = trim($this->input->post('toclassid', TRUE));
		$rangelevelid = trim($this->input->post('rangelevelid', TRUE));
		$full_Khmer = trim($this->input->post('full_Khmer', TRUE));
  		$where ='';
		$sortstr ="";
			if($student_num != ''){
			 	$where .= "AND student_num LIKE '%{$student_num}%'";
			}
			if($full_name != ''){
				$where .= "AND ( CONCAT(
									last_name,
									' ',
									first_name
								) LIKE '%{$full_name}%')";
			}
			if($full_Khmer !=''){
				$where .= " AND ( CONCAT(
									last_name_kh,
									' ',
									first_name_kh
								) LIKE '%{$full_Khmer}%')";
			}	
			if($yearid != ''){
			 	$where .= "AND year = '{$yearid}'";
			}
			if($programid != ''){
			 	$where .= "AND programid = '{$programid}'";
			}
			if($schlevelids != ''){
			 	$where .= "AND schlevelid = '{$schlevelids}'";
			}
			if($classid != ''){
			 	$where .= "AND classid = '{$classid}'";
			}
			if($rangelevelid != ''){
			 	$where .= "AND rangelevelid = '{$rangelevelid}'";
			}
	    	(isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

		$sqr = "SELECT
					v_students_in_grades.student_num,
					v_students_in_grades.first_name,
					v_students_in_grades.last_name,
					v_students_in_grades.first_name_kh,
					v_students_in_grades.last_name_kh,
					v_students_in_grades.gender,
					v_students_in_grades.phone1,
					v_students_in_grades.class_name,
					v_students_in_grades.studentid,
					v_students_in_grades.schoolid,
					v_students_in_grades.`year`,
					v_students_in_grades.classid,
					v_students_in_grades.schlevelid,
					v_students_in_grades.rangelevelid,
					v_students_in_grades.feetypeid,
					v_students_in_grades.programid,
					v_students_in_grades.sch_level,
					v_students_in_grades.rangelevelname,
					v_students_in_grades.program,
					v_students_in_grades.sch_year,
					v_students_in_grades.enrollid
				FROM
					v_students_in_grades
				WHERE 1 = 1 {$where} ";
		$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_students_in_grades WHERE 1 = 1 {$where} ")->row()->numrow;
			$getperpage=0;
			if($total_display==''){
				$getperpage=50;
			}else{
				$getperpage=$total_display;
			}
			$paging    = $this->green->ajax_pagination($total_row,site_url()."student_fee/c_student_in_grades",$getperpage,"icon");
			if($sortstr!=""){
				$sqr.=$sortstr;
			}
			$getlimit=50;
			if($paging['limit']!=''){
				$getlimit=$paging['limit'];
			}
		$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
			$j='';
			$table='';
			$gtotal=0;
			if(count($data) > 0){
				foreach($data as $row){ 
					$j++;
					$have_img = base_url()."assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg';		
					$no_imgs = base_url()."assets/upload/students/NoImage.png";	
					$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
						if (file_exists(FCPATH . "assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg')) {				
							$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
						}
					$table.="<tr class='.tr'>
								<td>".$j."</td>
								<td>".$img."</td>
								<td><strong>".$row->student_num."</strong>
									</br>".$row->last_name_kh.'  '.$row->first_name_kh."
									</br>".$row->last_name.'  '.$row->first_name."
									</br>".$row->gender."
								</td>
								<td style='text-align:center;'>".$row->program."</td>
								<td style='text-align:center;'>".$row->sch_level."</td>
								<td style='text-align:center;'>".$row->sch_year."</td>
								<td style='text-align:center;'>".$row->rangelevelname."</td>
								<td style='text-align:center;'>".$row->class_name."</td>
								<td style='text-align:center;color:#f5062a;' cass='class_name'>".($row->class_name != $toclassid ? $toclassid : '')."</td>
								<td style='width:3px; text-align:center;'> 
								<input type='hidden' name='enrollid[]' class='enrollid' data-enrolid='".$row->enrollid."' value='".$row->enrollid."'>
								<input type='hidden' name='schlevelid[]' class='schlevelid' schlevelid='".$row->schlevelid."' value='".$row->schlevelid."'>
								<input type='hidden' name='programid[]' class='programid' programid='".$row->programid."' value='".$row->programid."'>
								<input type='hidden' name='yearid[]' class='yearid' value='".$row->year."'>

								<input type='checkbox' name='checksub[]' class='checksub' data-studentId='".$row->studentid."' data-schoolid='".$row->schoolid."'></td>";
								
					$table.= " </td>
						 	</tr> ";
				}

			}else{

				$table.="<tr>
							<td colspan='9' style='text-align:center;'> 
								We did not find anything to show here... 
							</td>
						</tr>";
			}

		$arr['data']=$table;
		header('Content-Type:text/x-json');
		$arr['body']=$table;
		$arr['pagination']=$paging;
		echo json_encode($arr);
		// sendmeil(); 
		die();
	}

}
