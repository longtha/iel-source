<?php 
	class C_list_student_semester extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model("student/mod_list_student_semester","list_semester");
			$this->thead=array("No"=>'No',
							 "StudentID"=>'student_num',
							 "Name"=>'fullname',
							 "Gender"=>'gender',
							 "School level"=>'sch_level',
							 "Year"=>'sch_year',
							 "Grad level"=>'grade_level',						 
							 "Class"=>'class_name',
							 "Semester"=>'semester',
							 "Score"=>'score'
							 );
		}

		function index(){
			$data['opt_school'] = $this->get_school();
			$data['thead']=	$this->thead;
			$this->load->view('header');
			$this->load->view('student/v_list_student_semester',$data);
			$this->load->view('footer');
		}
		// function add(){
		// 	$this->load->view('header');
		// 	$this->load->view('reports/iep_reports/v_add_evaluation');
		// 	$this->load->view('footer');
		// }
		function show_semester_report(){ 
			$this->load->view('header');
			$this->load->view('reports/iep_reports/v_report_student_semester');
			$this->load->view('footer');
		}
		function veiw_all_report(){
			
			$programid  = $_GET['programid'];
			$schoolid   = $_GET['schoolid'];
			$schlid     = $_GET['schlid'];
			$grandlevel = $_GET['grandlevel'];
			$yearid     = $_GET['yearid'];
			$classid    = $_GET['classid'];
			$semesterid = $_GET['semesterid'];
			$limitpage  = $_GET['limitpage'];
			$startpage  = $_GET['startpage'];
			$data = array('programid'=>$programid,
						'schoolid'=>$schoolid,
						'schlid'=>$schlid,
						'grandlevel'=>$grandlevel,
						'yearid'=>$yearid,
						'classid'=>$classid,
						'semesterid'=>$semesterid,
						'limitpage'=>$limitpage,
						'startpage'=>$startpage
						);
			$this->load->view('header');
			$this->load->view('reports/iep_reports/v_report_semester_all',$data);
			$this->load->view('footer');
		}
		function st_auto(){
		    $name = trim($_REQUEST['name']);
		    $pid = $_REQUEST['pid'];
		    $schlevel = $_REQUEST['schlevel'];
		    $classid = $_REQUEST['classid'];
		    $where = "";
			if($name != ""){
			   $where .= " AND CONCAT(vs.last_name,' ',vs.first_name) like '".$name."%'";    
			}
			if($pid != ""){
	        	$where .= " AND vs.programid = '".$pid."'";    
		    }
		    if($schlevel != ""){
		        $where .= " AND vs.schlevelid = '".$schlevel."'";    
		    }
		    
		  	$sql = "SELECT DISTINCT 
							CONCAT(vs.last_name,' ',vs.first_name) as fullname
							FROM
							v_student_enroment AS vs
							WHERE 1=1 {$where} 
							AND vs.classid = '".$classid."' LIMIT 0, 10"; 
		 	$qr = $this->db->query($sql);
		  	$arr = [];
			if($qr->num_rows() > 0){
			    foreach ($qr->result() as $row){
			    	$arr[] = ['name' => $row->fullname];
			    }
			}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
		}

		public function stid_auto(){
		    $st_num = trim($_REQUEST['id']);
		    $pid = $_REQUEST['pid'];
		    $schlevel = $_REQUEST['schlevel'];
		    $classid = $_REQUEST['classid'];
		    $where = "";
		    if($st_num != ""){
		        $where .= " AND vs.student_num like '".$st_num."%'";    
		    }
		    if($pid != ""){
		        $where .= " AND vs.programid = '".$pid."'";    
		    }
		    if($schlevel != ""){
		        $where .= " AND vs.schlevelid ='".$schlevel."'";    
		    }
		   
		    $sql = "SELECT DISTINCT 
							student_num,
							CONCAT(vs.last_name,' ',vs.first_name) as fullname
							FROM
							v_student_enroment AS vs
							WHERE 1=1 {$where} 
							AND vs.classid = '".$classid."'
							LIMIT 0, 10"; 
			//echo $sql;
		    $qr = $this->db->query($sql);
		    $arr = [];
		    if($qr->num_rows() > 0){
		        foreach ($qr->result() as $row) {
		            $arr[] = ['st_num' => $row->student_num ,'st_name' => $row->fullname];
		        }
		    }
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
		}

		function get_school(){
			$opt_school = "";
			$sql = $this->db->get('sch_school_infor')->result();
			foreach ($sql as  $row){
				$opt_school .='<option value="'.$row->schoolid.'">'.$row->name.'</option>';
			}
			return $opt_school;
		}

		function get_program(){
			$optionprogram="";
			$schoolid = $this->input->post('schoolid');
			$optionprogram .='<option value=""></option>';
			foreach($this->list_semester->get_program($schoolid) as $pro_id ){
				$optionprogram .='<option value="'.$pro_id->programid.'">'.$pro_id->program.'</option>';
			}
			header("Content-type:text/x-json");
			$arr['program'] = $optionprogram;
			echo json_encode($arr);		
		}

		function school_level(){
			$school_level="";
			$option_level="";
			$program = $this->input->post('program');
			$option_level .='<option value=""></option>';
			foreach($this->list_semester->getsch_level($program) as $id ){
				$option_level .='<option value="'.$id->schlevelid.'">'.$id->sch_level.'</option>';
			}
			header("Content-type:text/x-json");
			$arr_success['schlevel'] = $option_level;
			echo json_encode($arr_success);
		}

		function get_years(){
			$schoolid = $this->session->userdata('schoolid');	
			$sch_program = $this->input->post("program");
			$schlevelid = $this->input->post("sch_level");
			$option_year ='';
			$option_year .='<option value=""></option>';
			foreach ($this->list_semester->getschoolyear($sch_program,$schoolid,$schlevelid) as $row){
				$option_year .='<option value="'.$row->yearid.'" att_y="'.$row->fyear.'">'.$row->sch_year.'</option>';
			}		
			header("Content-type:text/x-json");
			$arr_success['schyear'] = $option_year;
			echo json_encode($arr_success);
			
		}

		function get_gradlevel(){
			$grad_level="";
			$schoolid = $this->session->userdata('schoolid');	
			$programid = $this->input->post("program");
			$schlevelid = $this->input->post("sch_level");
			$grad_level .='<option value=""></option>';
			foreach($this->list_semester->getgradelevel($schoolid,$programid,$schlevelid) as $id ){
				$grad_level .='<option value="'.$id->grade_levelid.'">'.$id->grade_level.'</option>';
			}
			header("Content-type:text/x-json");
			$arr_success['gradlevel'] = $grad_level;
			echo json_encode($arr_success);
		}

		function get_class(){
			$schoolid = $this->session->userdata('schoolid');	
			$grad_level = $this->input->post("grad_level");
			$schlevelid = $this->input->post("sch_level");
			$class="";
			$class .='<option value=""></option>';
			foreach($this->list_semester->get_class($schoolid,$grad_level,$schlevelid) as $id ){			
				$class .='<option value="'.$id->classid.'">'.$id->class_name.'</option>';
			}
			header("Content-type:text/x-json");
			$arr_success['getclass'] = $class;
			echo json_encode($arr_success);  
		}

		function get_semester(){
			
			$schoolid = $this->input->post("schoolid");
			$programid = $this->input->post("programid");
			$schlevelid = $this->input->post("schlevelid");
			$yearid = $this->input->post("yearid");
			$term_opt="";
			$sql_sem = $this->db->query("SELECT
											sch_school_semester.semesterid,
											sch_school_semester.semester,
											sch_school_semester.programid,
											sch_school_semester.yearid,
											sch_school_semester.schoolid,
											sch_school_semester.schlevelid
											FROM
											sch_school_semester
											WHERE 1=1
											AND schoolid='".$schoolid."'
											AND schlevelid='".$schlevelid."'
											AND programid='".$programid."'
											AND yearid='".$yearid."'");
			$opt_sm ='<option value=""></option>';
			if($sql_sem->num_rows() > 0){
				foreach($sql_sem->result() as $row_sm){
					$opt_sm.='<option value="'.$row_sm->semesterid.'">'.$row_sm->semester.'</option>';
				}
			}
			
			header("Content-type:text/x-json");
			$arr_sm['get_semester'] = $opt_sm;
			echo json_encode($arr_sm);  
		}
		function show_list(){
			$table='';
			$i    = 1;
			$sql  = $this->list_semester->show_list();
			foreach($this->db->query($sql['sql'])->result() as $row){
				$main = $row->schoolid.'_'.$row->schlevelid.'_'.$row->year.'_'.$row->grade_levelid.'_'.$row->classid;	
				$arr_semester = array("schoolid"=>$row->schoolid,"schlevelid"=>$row->schlevelid,
									"yearid"=>$row->year,"grade_levelid"=>$row->grade_levelid,
									"classid"=>$row->classid,"studentid"=>$row->studentid,
									"semesterid"=>$row->semesterid
									);
				$total_sem = $this->green->total_semester($arr_semester);		
				$programid = $row->programid;
				$full_path = 'c_list_student_semester/show_semester_report';	
				$table .= '<tr>
								<td>'.str_pad($i,2,"0",STR_PAD_LEFT).'</td>
								<td class="student_num">'.$row->student_num.'</td>
								<td class="fullname">
									<span>'.$row->fullname.'</span><br>
									<span>'.$row->fullname_kh.'</span>
									<input type="hidden" name="student_name" id="student_name" name_kh="'.$row->fullname_kh.'" name_eng="'.$row->fullname.'">
								</td>
								<td class="gender">'.ucwords($row->gender).'</td>
								<td class="sch_level">'.$row->sch_level.'</td>
								<td class="year">'.$row->sch_year.'</td>
								<td class="grad_level">'.$row->grade_level.'</td>
								<td class="class_name">'.$row->class_name.'</td>
								<td class="termid">'.$row->semester.'</td>
								<td style="text-align: center;">'.$total_sem['score'].'</td>
								<td class="remove_tag" style="text-align: center;">';
								if($this->green->gAction("C")){ 
							$table .= '<a href="javascript:void(0)" id="add_eval" studentid="'.$row->studentid.'"><img src="'.base_url("assets/images/icons/add.png").'" style="margin-right:0;"></a>';
								}
							$table .= '<a target="_blank" href="'.site_url('student/'.$full_path.'/?id='.$row->studentid.' &p_id='.$row->programid.'&schoolid='.$row->schoolid.'&schlid='.$row->schlevelid.'&grandid='.$row->grade_levelid.'&yearid='.$row->yearid.'&classid='.$row->classid.'&semesterid='.$row->semesterid).'"><img src='.base_url('assets/images/icons/a_preview.png').'>
									</a>';
						$table .= '</td>
							</tr>';							 
				$i++;	 
			}
			$paging = $sql['paging'];
			header("Content-type:text/x-json");
			$arr['tr_data']  = $table;
			$arr['printall'] = $sql['showpage'];
			$arr['pagina']   = $paging;		
			echo json_encode($arr);	
			//echo $this->list_term->show_list();
		}
		function show_infor_evaluation(){
			$schoolid  = $this->input->post("schoolid");
			$programid = $this->input->post("programid");
			$schlevelid = $this->input->post("schlevelid");
			$yearid    = $this->input->post("yearid");
			$gradlevel = $this->input->post("gradlevel");
			$classid   = $this->input->post("classid");
			$semesterid = $this->input->post("semesterid");
			$studentid = $this->input->post("studentid");
			$sql_eval_des = $this->db->query("SELECT
												eval_des.description_id,
												eval_des.description
												FROM
												sch_evaluation_semester_iep_description AS eval_des");
			$tr_des = "";
			$teacher_comment = "";
			$guardian_comment = "";
			$academic_date  = "";
			$techer_date    = "";
			$guardian_date  = "";
			$return_date    = "";
			$evaluationid = "";

			$acadeid  = "";
			$userid   = "";
			if($sql_eval_des->num_rows() > 0)
			{
				foreach($sql_eval_des->result() as $row_des)
				{
					
					$sql_check_desc = $this->db->query("SELECT
															evl_or.evaluationid,
															evl_or.studentid,
															evl_or.techer_comment,
															evl_or.guardian_comment,
															DATE_FORMAT(evl_or.academic_date,'%d-%m-%Y') as academic_date,
															DATE_FORMAT(evl_or.techer_date,'%d-%m-%Y') as techer_date,
															DATE_FORMAT(evl_or.guardian_date,'%d-%m-%Y') as guardian_date,
															DATE_FORMAT(evl_or.return_date,'%d-%m-%Y') as return_date,
															eval_det.descrition_id,
															eval_det.seldom,
															eval_det.sometimes,
															eval_det.usually,
															eval_det.consistently,
															evl_or.academicid,
															evl_or.userid
															FROM
															sch_evaluation_semester_iep_order AS evl_or
															INNER JOIN sch_evaluation_semester_iep_detail AS eval_det ON evl_or.evaluationid = eval_det.evaluationid
															WHERE 1=1 AND eval_det.descrition_id='".$row_des->description_id."'
															AND evl_or.studentid='".$studentid."'
															AND evl_or.semester ='".$semesterid."'
															AND evl_or.schoolid = '".$schoolid."'
															AND evl_or.schlevelid = '".$schlevelid."'
															AND evl_or.grade_levelid = '".$gradlevel."'
															ANd evl_or.yearid = '".$yearid."'
															AND evl_or.classid = '".$classid."'");
					$ch_seldom  = "";
					$ch_sometime= "";
					$ch_usually = "";
					$ch_consist = "";
					$ch_seldom_v  = 0;
					$ch_sometime_v= 0;
					$ch_usually_v = 0;
					$ch_consist_v = 0;
					
					if($sql_check_desc->num_rows() > 0){
						foreach($sql_check_desc->result() as $row_ch){
							$teacher_comment  = $row_ch->techer_comment;
							$guardian_comment = $row_ch->guardian_comment;
							$academic_date  = $row_ch->academic_date;
							$techer_date    = $row_ch->techer_date;
							$guardian_date  = $row_ch->guardian_date;
							$return_date    = $row_ch->return_date;
							$evaluationid   = $row_ch->evaluationid;

							$acadeid  = $row_ch->academicid;
							$userid   = $row_ch->userid;
							if($row_ch->seldom == 1){
								$ch_seldom  = "checked='checked'";
								$ch_seldom_v=1;
							}
							if($row_ch->sometimes == 1){
								$ch_sometime  = "checked='checked'";
								$ch_sometime_v= 1;
							}
							if($row_ch->usually == 1){
								$ch_usually  = "checked='checked'";
								$ch_usually_v = 1;
							}
							if($row_ch->consistently == 1){
								$ch_consist  = "checked='checked'";
								$ch_consist_v = 1;
							}
						}
					}
					$tr_des.= "<tr>
									<td>".$row_des->description."
										<input type='hidden' name='valuation_des' class='id_valuation_des' id='valuation_des' value='".$row_des->description_id."'>
									</td>
									<td style='text-align: center;'><input type='checkbox' ".$ch_seldom." value='".$ch_seldom_v."' name='seldom' id='seldom' class='seldom'></td>
									<td style='text-align: center;'><input type='checkbox' ".$ch_sometime." value='".$ch_sometime_v."'  name='sometim' id='sometim' class='sometim'></td>
									<td style='text-align: center;'><input type='checkbox' ".$ch_usually." value='".$ch_usually_v."'  name='usually' id='usually' class='usually'></td>
									<td style='text-align: center;'><input type='checkbox' ".$ch_consist." value='".$ch_consist_v."'  name='consistently' id='consistently' class='consistently'></td>
								</tr>";
				}
			}
			$tr_head = "<tr><td colspan='5' style='text-align:center;'><p>Evaluation for Semestern 1 / ការវាយតម្លៃក្នុងឆមាសទី១</p></td></tr>
					   <tr>
					   		<td style='text-align:center;'><b>Work Habits and Social Skills</b><br>ទម្លាប់ក្នុងការសិក្សា និងជំនាញសង្គម</td>
					   		<td style='text-align:center;'><b>Seldom</b><br>កម្រ</td>
					   		<td style='text-align:center;'><b>Sometimes</b><br>ម្ដងម្កាល</td>
					   		<td style='text-align:center;'><b>Usually</b><br>តែងតែ</td>
					   		<td style='text-align:center;'><b>Consistently</b><br>ជាប្រចាំ</td>
					   </tr>".$tr_des;
			$userID = ($userid !=""?$userid:$this->session->userdata('userid'));
			$username = $this->db->query("SELECT user_name FROM sch_user WHERE userid='".$userID."'")->row();
			$user_label = isset($username->user_name)?$username->user_name:$this->session->userdata('userid');
			header("Content-type:text/x-json");
			$arr_json['evaluationid']   = $evaluationid;
			$arr_json['comment']   = array("teach"=>$teacher_comment,"guard"=>$guardian_comment);
			$arr_json['date_sign'] = array("acad_date"=>($academic_date!=""?$academic_date:date('d-m-Y')),
											"teach_date"=>($techer_date!=""?$techer_date:date('d-m-Y')),
											"guard_data"=>($guardian_date!=""?$guardian_date:date('d-m-Y')),
											"ret_date"=>($return_date!=""?$return_date:date('d-m-Y')),
											"academicid"=>$acadeid,
											"userid"=>$userID,
											"userlabel"=>ucwords($user_label)
											); 
			$arr_json['tbl_des']   = $tr_head;
			echo json_encode($arr_json);
		}

		function save_desc_modal(){
			$schoolid  	= $this->input->post("schoolid");
			$programid  = $this->input->post("programid");
			$schlevelid = $this->input->post("schlevelid");
			$yearid     = $this->input->post("yearid");
			$gradlevel  = $this->input->post("gradlevel");
			$classid    = $this->input->post("classid");
			$semesterid = $this->input->post("semesterid");
			$studentid  = $this->input->post("studentid");

			$command_teacher  = $this->input->post("command_teacher");
			$commend_guardian  = $this->input->post("commend_guardian");
			$date_acade    = $this->input->post("date_acade");
			$date_teacher  = $this->input->post("date_teacher");
			$guardian_date = $this->input->post("guardian_date");
			$return_date   = $this->input->post("return_date");

			$arr_valuation_des = $this->input->post("arr_valuation_des");
			$evaluationid  = $this->input->post("evaluationid");
			$user_acad  = $this->input->post("user_acad");
			$userid_teacher  = $this->input->post("userid_teacher");
			$typno_eval = "";
			if($evaluationid == ""){
					$typno_eval = $this->green->nextTran(2,"Evaluate Student");
			}else{
				$typno_eval = $evaluationid;
				$this->db->delete("sch_evaluation_semester_iep_order",array("evaluationid"=>$evaluationid,"studentid"=>$studentid));
				$this->db->delete("sch_evaluation_semester_iep_detail",array("evaluationid"=>$evaluationid));
			}
			

			$data_order = array(
								"evaluationid"=>$typno_eval,
								"studentid"=>$studentid,
								"schoolid"=>$schoolid,
								"schlevelid"=>$schlevelid,
								"grade_levelid"=>$gradlevel,
								"yearid"=>$yearid ,
								"semester"=>$semesterid,
								"classid"=>$classid,
								"academicid"=>$user_acad,
								"userid"=>$userid_teacher,
								"techer_comment"=>$command_teacher,
								"guardian_comment"=>$commend_guardian,
								"academic_date"=>$this->green->convertSQLDate($date_acade),
								"techer_date"=>$this->green->convertSQLDate($date_teacher),
								"guardian_date"=>$this->green->convertSQLDate($guardian_date),
								"return_date"=>$this->green->convertSQLDate($return_date)
								);
			$this->db->insert("sch_evaluation_semester_iep_order",$data_order);
			if(count($arr_valuation_des) > 0){
				foreach($arr_valuation_des as $row_valuation){
					if($row_valuation['seldom'] == 1 || $row_valuation['sometim'] == 1 || $row_valuation['usually'] == 1 || $row_valuation['consistently'] == 1){
						$data_det = array(
										'evaluationid'=>$typno_eval,
										'descrition_id'=>$row_valuation['id_valuation_des'],
										'seldom'=>$row_valuation['seldom'],
										'sometimes'=>$row_valuation['sometim'],
										'usually'=>$row_valuation['usually'],
										'consistently'=>$row_valuation['consistently']
										);
						

						$this->db->insert("sch_evaluation_semester_iep_detail",$data_det);
					}
					
				}
			}
			header("Content-type:text/x-json");
			$arr_j['success']="OK";
			echo json_encode($arr_j);
		}
	}
?>