<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class c_evaluate_setup_kgp extends CI_Controller{
	    protected $thead;
		protected $idfield;
	    function __construct() {
	        parent::__construct();
	        $this->load->model("student/m_evaluate_setup_kgp","kgp");
	        $this->thead = array("No" => 'no',
	        					"Evaluate Name Khmer" => 'evaluate_name_kh',
	        					"Evaluate Name English"=>'evaluate_name_eng',
								"Status" => 'is_active',
								"Action"=>'Action'	
	                            );
	        $this->idfield = "studentid";
	    }
	    function index() {
	        $data['idfield'] = $this->idfield;
        	$data['thead'] = $this->thead;
	        $this->load->view('header',$data);
	        $this->load->view('student/v_evaluate_setup_kgp',$data);
	        $this->load->view('footer',$data );
	    }
	    // save ---------------------------------------------
		function save(){
			$evaluate_id = $this->input->post('evaluate_id');
			$evaluate_kh = $this->input->post('evaluate_kh');
			$count=$this->kgp->validate($evaluate_kh,$evaluate_id);			

			$msg='';
			if($count>0){
				$msg="Evaluate kgp is already exist...!";
				$evaluate_id='';
			}else{
				$evaluate_kh=$this->kgp->save($evaluate_id);
				$msg="Evaluate kgp was created...!";
			}
			$arr=array('msg'=>$msg,'evaluate_id'=>$evaluate_id);
			header("Content-type:text/x-json");
			echo json_encode($arr);
			
		}
		// deletedata ------------------------------------------------
		function deletedata(){
			$evaluate_id = $this->input->post('evaluate_id');
			if(intval($evaluate_id) > 0 ) {
			   $this->db->where('evaluate_id',$evaluate_id)->delete('sch_student_evaluate_setup_kgp');
			   $msgbox = 'Data deleted!';
			}else{
				$msgbox = 'errorses';
			}
			$arrDel = array();
			header('Content-type:text/x-json');
			$arrDel['success']=$msgbox;
			echo json_encode($arrDel);
			die();
		}
		// editdata  -------------------------------------------------
		function editdata(){
			$arr = array();			
			$evaluate_id = $this->input->post('evaluate_id');	
			$arr['res']=$this->kgp->editdata($evaluate_id);
			header('Content-type:text/x-json');	
			echo json_encode($arr);
			die();			
		}
		// showdata -------------------------------------------------
		function showdata(){
			$total_display =$this->input->post('sort_num');
			$this->green->setActiveRole($this->input->post('roleid'));
	  		$total_display = $this->input->post('total_display');
	  		$sortby=$this->input->post("sortby");
			$sorttype=$this->input->post("sorttype");
			//$sech_checkboxis_vtc=$this->input->post("sech_checkboxis_vtc");
			//$serch_name_eng=$this->input->post("serch_name_eng");
			//$serch_name_kh=$this->input->post("serch_name_kh");
	  		$where='';
			$sortstr="";
				//if($serch_name_kh != ""){
    			//	$where .= "AND  sch_student_evaluate_setup_kgp.evaluate_name_kh  LIKE '%".$serch_name_kh."%' ";
    			//}
				//if($serch_name_eng != ""){
    			//	$where .= "AND  sch_student_evaluate_setup_kgp.evaluate_name_eng  LIKE '%".$serch_name_eng."%' ";
    			//}
				//if($sech_checkboxis_vtc.'' != ""){
    			//	$where .= "AND  sch_student_evaluate_setup_kgp.is_active = ".$sech_checkboxis_vtc." ";
    			//}
		        (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

			$sqr="SELECT *
			 		FROM
						sch_student_evaluate_setup_kgp 
					WHERE 1=1 {$where}";
			
			$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM sch_student_evaluate_setup_kgp WHERE 1=1 {$where}")->row()->numrow;
			$getperpage=0;
			if($total_display==''){
				$getperpage=10;
			}else{
				$getperpage=$total_display;
			}
			$paging    = $this->green->ajax_pagination($total_row,site_url()."student/c_evaluate_setup_kgp",$getperpage,"icon");
			if($sortstr!=""){
				$sqr.=$sortstr;
			}
			$getlimit=10;
			if($paging['limit']!=''){
				$getlimit=$paging['limit'];
			}
			$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
			$j='';
			$table='';
				if(count($data) > 0){
					foreach($data as $row){  
						$j++;
						$rowcolor='';
						if($row->is_active=='0'){
							$rowcolor.="<td style='text-align:left;color:red;'>".$j."</td>";
							$rowcolor.="<td style='text-align:left;color:red;'>".$row->evaluate_name_kh."</td>";
							$rowcolor.="<td style='text-align:left;color:red;'>".$row->evaluate_name_eng."</td>";
							$rowcolor.="<td style='text-align:left;color:red;'>".($row->is_active == '0' ? 'In_Active' : 'Active')."</td>";
						}else{
							$rowcolor.="<td style='text-align:left;'>".$j."</td>";
							$rowcolor.="<td style='text-align:left;'>".$row->evaluate_name_kh."</td>";
							$rowcolor.="<td style='text-align:left;'>".$row->evaluate_name_eng."</td>";
							$rowcolor.="<td style='text-align:left;'>".($row->is_active == '0' ? 'In_Active' : 'Active')."</td>";
						}
						$table.="<tr>
									</td>";
										$table.=$rowcolor;
										$table.="<td style='width:10px;' class='remove_tag no_wrap'>";
										if($this->green->gAction("U")){
											$table.= "<a data-placement='top' title='Update'><img rel=".$row->evaluate_id." onclick='update(event);' src='".base_url('assets/images/icons/edit.png')."'/></a>";
										}
										if($this->green->gAction("D")){
											$table.= "<a data-placement='top' title='Delete'><img rel=".$row->evaluate_id." onclick='deletedata(event);' src='".base_url('assets/images/icons/delete.png')."'/></a>";
										}	
						$table.= " </td>
							 	</tr> ";	
					}
				}else{
					$table.= "<tr>
								<td colspan='4' style='text-align:center;'> 
									We did not find anything to show here... 
								</td>
							</tr>";
				}	

			$arr['data']=$table;
			header('Content-Type:text/x-json');
			$arr['body']=$table;
			$arr['pagination']=$paging;
			echo json_encode($arr); 
			die();
		}
	}