<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	class C_subject_score_entry_kgp extends CI_Controller{

		function __construct() {
	        parent::__construct();
			$this->load->model('student/m_subject_score_entry_kgp', 'mode');
			$this->load->model('student/m_evaluate_trans_kgp', 'student');
			$this->load->model('school/schoolinformodel', 'school');
			$this->load->model('school/program', 'program');			
			$this->load->model('school/schoollevelmodel', 'school_level');
			$this->load->model('school/gradelevelmodel', 'grade_level');
			$this->load->model('school/schoolyearmodel', 'adcademic_year');
			$this->load->model('school/classmodel', 'class');                			
	    }

	  //   public function index()
	  //   {
	  //   	$this->load->view('header');
			// $this->load->view('student/v_subject_score_entry_kgp');
			// $this->load->view('footer');
	  //   }

	    public function index()
	    {
	    	$this->load->view('header');
			$this->load->view('student/v_subject_score_entry_kgp_');
			$this->load->view('footer');
	    }

	    //get year and grade data----------------------------------
		public function get_year_and_grade_data()
		{
			$school_id = $this->input->post('school_id');
			$program_id = $this->input->post('program_id');
			$school_level_id = $this->input->post('school_level_id');
			
			$opt = array();

			//get year
			$y_opt = array();
			$get_year_data = $this->adcademic_year->getschoolyear($school_id,$program_id,$school_level_id);
			if(count($get_year_data) > 0){
				foreach($get_year_data as $r_y){				
					$y_opt[] = $r_y;
				}
			}			

			//get grade
			$g_opt = array(); 
			$get_grade_data = $this->grade_level->getgradelevels($school_level_id);
			if(count($get_grade_data) > 0){
				foreach($get_grade_data as $r_g){				
					$g_opt[] = $r_g;
				}
			}			

			$opt["year"] = $y_opt;
			$opt["grade"] = $g_opt;

			header('Content-Type: application/json');
			echo json_encode($opt);
		}

		//get teacher data-------------------------------------------
		public function get_teacher_data()
		{
			$school_level_id = $this->input->post('school_level_id');
			$adcademic_year_id = $this->input->post('adcademic_year_id');
			$grade_level_id = $this->input->post('grade_level_id');
			$t_opt = '<option value=""></option>';

			$get_teacher_data = $this->mode->getTeacherInfo("", $school_level_id, $adcademic_year_id, $grade_level_id);
			if(count($get_teacher_data) > 0){
				foreach($get_teacher_data as $r_t){
					$t_opt .= '<option value="'. $r_t->teacher_id .'">'. $r_t->first_name .'  '. $r_t->last_name .'</option>';
				}
			}
						
			echo $t_opt;
		}

		//get class data---------------------------------------------
		public function get_class_data()
		{
			$school_level_id = $this->input->post('school_level_id');
			$adcademic_year_id = $this->input->post('adcademic_year_id');
			$grade_level_id = $this->input->post('grade_level_id');
			$teacher_id = $this->input->post('teacher_id');
			$c_opt = '<option value=""></option>';
			$get_class_data = $this->mode->getClassByTeacher($teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id);
			if($get_class_data->num_rows() > 0){
				foreach($get_class_data->result() as $r_c){
					$c_opt .= '<option value="'. $r_c->classid .'">'. $r_c->class_name .'</option>';
				}
			}	
			echo $c_opt;
		}

		//get subject data-------------------------------------------
		public function get_subject_data()
		{
			$school_level_id = $this->input->post('school_level_id');
			$grade_level_id = $this->input->post('grade_level_id');
			$adcademic_year_id = $this->input->post('adcademic_year_id');
			$teacher_id = $this->input->post('teacher_id');
			$class_id = $this->input->post('class_id');
			$su_opt = '<option value=""></option>';

			$get_subject_data = $this->mode->getSubjectByTeacher("", $class_id, $teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id);
			if(count($get_subject_data) > 0){
				foreach($get_subject_data as $r_su){
					$su_opt .= '<option value="'. $r_su->subjectid .'">'. $r_su->subject_kh .'</option>';
				}
			}			
			
			echo $su_opt;
		}

		//get subject data-------------------------------------------
		public function get_group_main_id_subject()
		{
			$school_level_id = $this->input->post('school_level_id');
			$grade_level_id = $this->input->post('grade_level_id');
			$adcademic_year_id = $this->input->post('adcademic_year_id');
			$teacher_id = $this->input->post('teacher_id');
			$class_id = $this->input->post('class_id');
			$subject_id = $this->input->post('subject_id');
			$opt_id = array();

			$get_id_data = $this->mode->getSubjectByTeacher($subject_id, $class_id, $teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id);
			if(count($get_id_data) > 0){
				foreach($get_id_data as $r_id){
					$opt_id[] = $r_id;
					// $opt_id['sg_id'] = $r_id->subj_type_id;
					// $opt_id['sm_id'] = $r_id->subj_main_id;
				}
			}			
			
			header('Content-Type: application/json');
			echo json_encode($opt_id);
		}

		//get student data-----------------------------------------
		public function get_student_data()
		{
			$class_id = $this->input->post('class_id');
			$where = "";
			if($class_id != ""){
				$where .= " AND sc.classid = ". $class_id;
			}
			$s_opt = '<option value=""></option>';

			$i = 1;
			$get_student_data = $this->student->getStudentInformation($where);
			if(count($get_student_data) > 0){
				foreach($get_student_data as $r_s){
					$s_opt .= '<option value="'. $r_s->studentid .'">'. $i .'. '. $r_s->last_name .' '. $r_s->first_name .'</option>';
					$i++;
				}
			}
						
			echo $s_opt;
		}

		//get semester data----------------------------------------
		public function get_semester(){
			$school_id = $this->input->post('school_id');
			$program_id = $this->input->post('program_id');
			$school_level_id = $this->input->post('school_level_id');
			$adcademic_year_id = $this->input->post('adcademic_year_id');

			$arr_semester = array();
			$get_semester_data = $this->mode->getSemesterData($school_id, $program_id, $school_level_id, $adcademic_year_id);
			if(count($get_semester_data) > 0){
				foreach($get_semester_data as $row){
					$arr_semester[] = $row;
				}
			}

			header('Content-Type: application/json');
			echo json_encode($arr_semester);
			die();
		}

		//get student information----------------------------------
		public function get_student_information()
		{
			$school_id  = $this->input->post('school_id');
			$program_id = $this->input->post('program_id');
			$school_level_id = $this->input->post('school_level_id');
			$grade_level_id = $this->input->post('grade_level_id');			
			$adcademic_year_id = $this->input->post('adcademic_year_id');
			$teacher_id = $this->input->post('teacher_id');
			$class_id = $this->input->post('class_id');
			$subject_id = $this->input->post('subject_id');
			$student_id = $this->input->post('student_id');
			$get_exam_type = $this->input->post('exam_type');
			$data = array();
			
			//get array of exam type
			$exam_type = '';
			if(!empty($get_exam_type)){
				$exam_type = $get_exam_type;
			}else{
				//it die if we do not select exam type (monthly, semester, and final)
				die();	
			}
			
			$where = "";
			if($student_id != ""){
				$where .= " AND studentid = ". $student_id;
			}
			//get subject and teacher name data
			$title_data = array();
			$getSubjectName = $this->mode->getSubjectByTeacher($subject_id, $class_id, $teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id);
			if(count($getSubjectName) > 0){
				foreach($getSubjectName as $r_sub){
					$title_data["subjectid"] = $r_sub->subjectid;
					$title_data["subject"]   = $r_sub->subject_name;
					$title_data["subject_kh"] = $r_sub->subject_kh;
					$title_data["score_percence"] = ($r_sub->score_percence*100);
					$title_data["max_score"] = $r_sub->max_score;
					$head_data = array();
					$getHeader = $this->mode->getAssessmentBySubject($subject_id,$school_id, $school_level_id, $grade_level_id);
					if(count($getHeader) > 0){
						foreach($getHeader as $r_h){
							$head_data[] = $r_h; 
						}	
					}
					$title_data["head_assesment"] = $head_data;
				}
			}

			$getTecherName = $this->mode->getTeacherInfo($teacher_id, $school_level_id, $adcademic_year_id, $grade_level_id);
			if(count($getTecherName) > 0){
				foreach($getTecherName as $r_tea){
					$title_data["first_name"] = $r_tea->first_name;
					$title_data["last_name"] = $r_tea->last_name;
				}
			}
			$check_student_exist = $this->mode->checkStudentMonthlyExsist($school_id, $program_id, $school_level_id, $grade_level_id, $adcademic_year_id,$class_id, $exam_type,$subject_id );		
			// get assemest data in monthly
			$arr_ass = array();
			if($exam_type["get_exam_type"] == "semester"){
				//$exam_semester = $exam_type["exam_semester"];
                $get_monthly_in = implode(',', $exam_type["get_monthly_in"]);
				$qr_ass_core_m = $this->db->query('SELECT 
	                                                    studentid,
	                                                    subjectid,
	                                                    subj_assem_id,
	                                                    percentage,
	                                                    SUM(score_assem) AS score_assem,
	                                                    COUNT(*) AS amt_c
	                                                    FROM sch_subject_score_monthly_detail_kgp
	                                                    WHERE 1=1
	                                                    AND schoolid="'.$school_id.'"
	                                                    AND programid="'.$program_id.'"
	                                                    AND schlevelid="'.$school_level_id.'"
	                                                    AND gradlevelid="'.$grade_level_id.'"
	                                                    AND yearid="'.$adcademic_year_id.'"
	                                                    AND classid="'.$class_id.'"
	                                                    AND subjectid="'.$subject_id.'"
	                                                    AND exam_monthly in('.$get_monthly_in.')
	                                                    GROUP BY studentid,subjectid,subj_assem_id');
	            $arr_ass = array();
	            if($qr_ass_core_m->num_rows() > 0){
	                foreach($qr_ass_core_m->result() as $rass){
	                	$percentage = $rass->percentage;
	                    $total_score_ass = number_format(($rass->score_assem/$rass->amt_c),2);
	                    $arr_ass[$rass->studentid][$rass->subj_assem_id] = $total_score_ass;
	                }
	            }
	        }
			//get student data
			$stu_data = array(); 
			//$get_student_info = $this->student->getStudentInformation($where);
			$get_student_info = $this->db->query("SELECT
												v_student_enroment.studentid,
												v_student_enroment.first_name_kh,
												v_student_enroment.last_name_kh,
												v_student_enroment.gender
												FROM
												v_student_enroment
												WHERE 1=1 {$where}
												AND schoolid = '".$school_id."'
												AND programid = '".$program_id."'
												AND schlevelid = '".$school_level_id."'
												AND year = '".$adcademic_year_id."'
												AND classid = '".$class_id ."'
												AND grade_levelid = '".$grade_level_id."'
												GROUP BY studentid")->result();
			$arr_data_assement = array();
			if(count($get_student_info) > 0)
			{
				foreach($get_student_info as $r_stu)
				{	
					$stu_data[] = $r_stu;
					if(isset($arr_ass[$r_stu->studentid])){
						$arr_data_assement[$r_stu->studentid] = $arr_ass[$r_stu->studentid];
					}
				}
			}
			//print_r($stu_data);
			$data ["title_data"] = $title_data;
			//$data["header"] = $head_data;
			$data["exist_data"] = $check_student_exist;
			$data['data_ass'] = $arr_data_assement;
			$data["student"] = $stu_data;
			header('Content-Type: application/json');
			echo json_encode($data);
			die();
		}

		//save score entry for each student--------------------
		public function saveSubjectScoreEntry(){
	        $saveData = $this->mode->saveScoreEntry();
	        header("Content-type:text/x-json");	
	        $arrJson["res"]=$saveData;	        		
	        echo json_encode($arrJson);
	        exit();
		}

	   

	}
?>