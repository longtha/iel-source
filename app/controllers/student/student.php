<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Student extends CI_Controller {

	protected $thead;
	protected $thead2;
	protected $thead3;
	protected $idfield;
	protected $searchrow;
	function __construct(){
		parent::__construct();
		$this->load->model('setting/rolemodel','role');
		$this->lang->load('student', 'khmer');
		$this->load->model("student/modStudent","std");
		$this->load->model("school/classmodel","cls");
		$this->load->model("school/modsemester","sms");
		$this->load->model("school/modterm","term");
		$this->load->model("school/modrangelevel","ranglev");
		$this->load->model("school/program","pro");
		$this->load->model("social/modfamily","family");
		$this->load->model("school/schoollevelmodel","sch_lev");
		$this->load->model("school/schoolyearmodel","syear");
		//$this->load->library('encrypt');

		$this->load->model("setting/usermodel","user");

		$this->thead=array("No"=>'No',
							 "Photo"=>'photo',
							 "StudentID"=>'student_num',
							 "Full Name"=>'last_name',
							 "Phone"=>'phone1',
							 "DOB"=>'dateofbirth',
							 "Reg-Date"=>'register_date',
							 "Class"=>'class_name',
							 "Year"=>'yearid',
							 "FamilyID"=>'FamilyID',
							 "Action"=>'Action'
							);
		$this->thead2=array("No"=>'No',
							 "StudentID"=>'student_num',
							 "Full Name"=>'last_name',
							 "Full Name(Kh)"=>'last_name_kh',
							 "Date of Birth"=>'dateofbirth',
							 "Class"=>'class_name',
							 "Boarding Date"=>'boarding_date',
							 "FamilyID"=>'FamilyID'
							);
		$this->exp=array("No"=>'No',
							 "StudentID"=>'student_num',
							 "Full Name"=>'fullname',
							 "Full Name(Kh)"=>'fullname_kh',
							 "Date of Birth"=>'dateofbirth',
							 "Class"=>'class_name',
							 "Year"=>'sch_year',
							 "Age"=>'age',
							 "Nationality"=>'nationality',
							 "Address"=>'permanent_adr',
							 "Age"=>'age',
							 "Father Name"=>'father_name',
							 "Mother Name"=>'mother_name',
							 "Father Name(KH)"=>'father_name_kh',
							 "Father Ocupation(KH)"=>'father_ocupation_kh',
							 "Mother Name(KH)"=>'mother_name_kh',
							 "Mother Ocupation(KH)"=>'mother_ocupation_kh',
							 "FamilyID"=>'family_code'
							);
		$this->thead3=array("លេខរៀង"=>'No',
		    "ខែ"=>'month',
		    "សប្ដាហ៍"=>'week',
		    "ចិត្តចលភាព"=>'mentality',
		    "បញ្ញា"=>'intellectual',
		    "អារម្មណ៍"=>'feeling',
		    "សង្គម"=>'society',
		    ""=>'Action'
		);
		$this->idfield="studentid";
	}		

	function index()
	{

		$page=0;
		if(isset($_GET['per_page'])) $page=$_GET['per_page'];
		$m='';
		$p='';

		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
	    $where='';
	    if($this->session->userdata('match_con_posid')=='stu'){
	    	$where.=" AND s.studentid='".$this->session->userdata('emp_id')."'";
	    }else{
	    	$where.=" AND s.yearid='".$this->session->userdata('year')."'";
	    }
		$sql_page="SELECT DISTINCT
                        s.studentid,
                        s.sch_year,
                        s.familyid,
                        s.yearid,
						s.student_num,
						s.last_name,
						s.first_name,
						s.last_name_kh,
						s.first_name_kh,
						s.dob,
                        s.register_date,
						s.nationality,
						s.class_name,
						s.classid,
						s.dateofbirth,
                        s.phone1,
                        s.gender
					FROM
						v_student_profile s
				WHERE s.is_active=1
				{$where}
				ORDER BY s.studentid DESC";

		$this->load->library('pagination');
		$config['base_url'] = site_url()."/student/student?pg=1&m=$m&p=$p";
		$config['total_rows'] = $this->green->getTotalRow($sql_page);
		$config['per_page'] =30;
		//$config['use_page_numbers'] = TRUE;
		$config['num_link']=30;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';

		$this->pagination->initialize($config);
		$limi=" limit ".$config['per_page'];
		if($page>0){
			$limi=" limit ".$page.",".$config['per_page'];
		}

		$sql_page.=" {$limi}";
		$data['tdata']=$this->green->getTable($sql_page);
		$data['idfield']=$this->idfield;
		$data['thead']=	$this->thead;
		$data['exp']=	$this->exp;
		$data['page_header']="Student List";

		$this->parser->parse('header', $data);
		$this->parser->parse('student/student_list', $data);
		$this->parser->parse('footer', $data);
	}

	function generateStudentPassword() {
		// HSC : Set default password is iel007
		/*
		  $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		  $password = array();
		  $alpha_length = strlen($alphabet) - 1;

		  for ($i = 0; $i < 8; $i++)
		  {
		      $n = rand(0, $alpha_length);
		      $password[] = $alphabet[$n];
		  }

		  return implode($password);
		  */
		return "iel007";
	}

	function import($result=0){
		$data['page_header']="Import Student Profile";
		$data['import_status']=($result==1?"Student profile was imported.":"No student profile to import.");
		$this->parser->parse('header', $data);
		$this->parser->parse('student/import_profile', $data);
		$this->parser->parse('footer', $data);
	}
	function importenroll($result=0){
		$data['page_header']="Import Student Profile";
		$data['import_status']=($result==1?"Enrollment was imported.":"No Enrollment to import.");
		$this->parser->parse('header', $data);
		$this->parser->parse('student/import_enrollment', $data);
		$this->parser->parse('footer', $data);
	}
	function getschlevel(){
		$classid=$this->input->post('classid');
		$schlevelid=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
		echo $this->db->where('schlevelid',$schlevelid)->get('sch_school_level')->row()->is_vtc;
	}
	function getgradeLevel(){
		$schlevel  = $this->input->post('schlevel');
		$programid = $this->input->post('sch_program');
		$sql_g = $this->db->query("SELECT grade_levelid,grade_level 
										FROM sch_grade_level 
										WHERE 1=1 
										AND schlevelid='".$schlevel."'");
		$option_gr = "";
		if($sql_g->num_rows() > 0){
			foreach($sql_g->result() as $rg){
				$option_gr.='<option value="'.$rg->grade_levelid.'">'.$rg->grade_level.'</option>';
			}
		}
		echo $option_gr;
	}
	function importProfile(){
		$result=$this->std->saveImport();
		$this->import($result);
	}
	function importenrollment(){
		$result=$this->std->saveImportenroll();
		$this->importenroll($result);
	}
	function add(){
		$data['page_header']="New Student";
		$this->parser->parse('header', $data);

		$data['spass'] = $this->generateStudentPassword();

		$this->parser->parse('student/student_form', $data);
		$this->parser->parse('footer', $data);
	}
	function preview($studentid){
		$data['page_header']="New Student";
		//$data1['page_header']=array("New Student");
		$yearid=$_GET['yearid'];
		$data1['student']=$this->std->previewstd($studentid,$yearid);
		$this->parser->parse('header', $data);
		$this->load->view('student/preview', $data1);
		$this->parser->parse('footer', $data);
	}
	
	function detail($studentid){
	    $data['page_header']="Detail Student";
	    //$data1['page_header']=array("New Student");
	    $yearid=@$_GET['yearid'];
	    $classid=@$_GET['classid'];
	    //$studentid=$_GET['studentid'];
	    
	    $data1['student']=$this->std->previewstd($studentid,$yearid,$classid);
	    $data1['month']=$this->std->getmonth();
	    $data1['week']=$this->std->getweek();
	    $data1['tdata']=$this->std->getbook($studentid,$yearid);
	    $data1['yearid']=$yearid;
	    $data1['studentid']=$studentid;
	    $data1['classid']=$classid;
	    
	    $data['thead']=	$this->thead3;
	    $data['page_header']="Book List";
	    
	    
	    $this->parser->parse('header', $data);
	    $this->load->view('student/detail', $data1);
	    $this->parser->parse('footer', $data);
	}
	
	function bookedit($studentid){
	    $data['page_header']="Detail Student";
	    //$data1['page_header']=array("New Student");
	    $yearid=$_GET['yearid'];
	    $bookid=$_GET['bookid'];
	    //$studentid=$_GET['studentid'];
	    
	    $data1['student']=$this->std->previewstd($studentid,$yearid);
	    $data1['month']=$this->std->getmonth();
	    $data1['week']=$this->std->getweek();
	    //$data1['tdata']=$this->std->getbook($studentid,$yearid);
	    $data1['obj']=$this->std->getbookbyid($studentid,$yearid,$bookid);
	    $data1['yearid']=$yearid;
	    $data1['studentid']=$studentid;
	    $data1['bookid']=$bookid;
	    
	    $data['thead']=	$this->thead3;
	    $data['page_header']="Book List";
	    
	    
	    $this->parser->parse('header', $data);
	    $this->load->view('student/bookedit', $data1);
	    $this->parser->parse('footer', $data);
	}
	
	function deletebook(){
	    $bookid=$this->input->post('bookid');
	    $deleted=$this->std->deletebook($bookid);
	    header("Content-type:text/x-json");
	    echo json_encode($deleted);
	    exit();	
	}
	
	function previewmulti($classid=''){
		$schoolid=$this->session->userdata("schoolid");
		if($classid==''){
			$classid=$this->db->query("SELECT MIN(classid) as classid
								FROM sch_class
								WHERE schoolid='$schoolid'")->row()->classid;
		}
		$data['page_header']="New Student";
		$yearid=$_GET['yearid'];
		$data['class_id']=$classid;
		$data['student']=$this->std->previewstd('',$yearid,$classid);
		$this->parser->parse('header', $data);
		$this->load->view('student/preview_multi', $data);
		$this->parser->parse('footer', $data);
	}
	function previewhistory($studentid){
		$data['page_header']="New Student";
		$data1['stdid']=$studentid;
		$this->parser->parse('header', $data);
		$this->load->view('student/preview_history', $data1);
		$this->parser->parse('footer', $data);
	}

	function savesponsor(){
			$stdspid=$this->input->post('stdspid');
			$sponsorid=$this->input->post('sponsorid');
			$s_date=$this->green->formatSQLDate($this->input->post('s_date'));
			$e_date=$this->green->formatSQLDate($this->input->post('e_date'));
			$classid=$this->input->post('classid');
			$studentid=$this->input->post('studentid');
			$year=$this->db->select('year')->from('sch_student_enrollment')->where('studentid',$studentid)->where('classid',$classid)->get()->row()->year;

			$date=date('Y-m-d');
			$user=$this->session->userdata('user_name');
			$data=array('studentid'=>$studentid,
						'start_sp_date'=>$s_date,
						'end_sp_date'=>$e_date,
						'classid'=>$classid,
						'yearid'=>$year,
						'sponsorid'=>$sponsorid);
			if($stdspid!=''){
				$data2=array('modified_date'=>$date,'modified_by'=>$user);
				$this->db->where('stdspid',$stdspid)->update('sch_student_sponsor_detail',array_merge($data,$data2));
			}else{
				$data2=array('created_date'=>$date,'created_by'=>$user);
				$this->db->insert('sch_student_sponsor_detail',array_merge($data,$data2));
				$stdspid=$this->db->insert_id();
			}
			echo $stdspid;
		}
	function filllink(){
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_student')	;
		$this->db->like('last_name',$key);
		$data=$this->db->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name,'id'=>$row->studentid);
		}
	     echo json_encode($array);
	}
	function edit($studentid){
		$data['page_header']="New Student";
		//$data1['page_header']=array("New Student");
		$datas['student']=$this->std->getstudentrow($studentid);
		$datas['query']= $this->std->getuserrow_($studentid);
		$this->parser->parse('header', $data);
		$this->parser->parse('student/student_edit', $datas);
		$this->parser->parse('footer', $data);
	}
	function isstudentbymember($memid){
			$this->db->select('count(*)');
			$this->db->from('sch_student');
			$this->db->where('match_memid',$memid);
			$this->db->where('is_active',1);
			return $this->db->count_all_results();
	}
	function getstdlink(){
			$familyid=$this->input->post('familyid');
			$studentid=$this->input->post('studentid');
			$sql="SELECT * FROM v_student_profile s where s.familyid='$familyid'  AND s.yearid=(SELECT MAX(year) FROM sch_student_enrollment WHERE studentid=s.studentid)";

			if($studentid!=0)
				$sql.=" AND studentid <> '$studentid' ";
			$result=$this->green->getTable($sql);
			foreach ($result as $std_link) {
				echo "
					<tr>
						<td>$std_link[first_name]</td>
						<td>$std_link[last_name]</td>
						<td>$std_link[first_name_kh]</td>
						<td>$std_link[last_name_kh]</td>
						<td>$std_link[class_name]</td>
						<td><a><img yearid='".$std_link['yearid']."' rel=".$std_link['studentid']." onclick='previewstudent(event);' src='".site_url('../assets/images/icons/preview.png')."'/></a>   </td>
					</tr>
				";
			}
	}
	public function fillrespon()
	{
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_student_responsible')	;
		$this->db->like('last_name',$key);
		$data=$this->db->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name,'id'=>$row->respondid);
		}
	     echo json_encode($array);
	}
	public function fillmember()
	{
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_student_member')	;
		$this->db->like('last_name',$key);
		$data=$this->db->get()->result();//$this->green->getTable($sql_page);
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name,'id'=>$row->memid);
		}
	     echo json_encode($array);
		//$this->load->view('footer');
	}

	public function fillsponsor()
	{
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_family_sponsor')	;
		$this->db->like('last_name',$key);
		$this->db->or_like('first_name',$key);
		$this->db->or_like('sponsor_code',$key);
		$data=$this->db->get()->result();//$this->green->getTable($sql_page);
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>"$row->last_name $row->first_name | $row->sponsor_code",'id'=>$row->sponsorid);
		}
	     echo json_encode($array);
		//$this->load->view('footer');
	}
	function getresponrow(){
		$respondid=$this->input->post('r_id');
		$this->db->where('respondid',$respondid);
		$row=$this->db->get('sch_student_responsible')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function getmemberrow(){
		$memid=$this->input->post('r_id');
		$this->db->where('memid',$memid);
		$row=$this->db->get('sch_student_member')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function deletesponsor(){
		$stdspid=$this->input->post('stdspid');
		$this->db->where('stdspid',$stdspid)->delete('sch_student_sponsor_detail');
	}
	function getmemberbyfamily(){
			$classid=$this->input->post('classid');
			$schlevelid=0;
			if($classid!='')
				$schlevelid=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
			$this->db->where('familyid',$this->input->post('familyid'));
			$this->db->where('is_active',1);
			echo "<option value=''>Choose Student</option>";
			foreach ($this->db->get('sch_student_member')->result() as $member) {
				if($schlevelid==4){
					echo "<option value='$member->memid'>$member->last_name $member->first_name </option>";
				}else{
					if($member->studentid!=0)
						echo "";
					else
						echo "<option value='$member->memid'>$member->last_name $member->first_name</option>";
				}

			}
	}
	function getstudentrow(){
		$std_id=$this->input->post('std_id');
		$this->db->select('s.studentid,s.last_name,s.first_name,s.last_name_kh,c.class_name');
		$this->db->from('sch_student s');
		$this->db->join('sch_class c','s.classid=c.classid','left');
		$this->db->where('s.studentid',$std_id);
		$s_row=$this->db->get()->row();
		header("Content-type:text/x-json");
		echo json_encode($s_row);
	}
	function validateuser(){
		$count=$this->std->getvalidateuser($this->input->post('username'));
		echo $count;
	}
	function validatstudid(){
		$studentid=$this->input->post('studentid');
		$student_num = $this->input->post('student_num');
		$count=$this->std->getvalidatstudid($student_num,$studentid);
		echo $count;
	}
	function Fcgetstuid(){
			$this->db->where('studentid',$this->input->post('stuid'));
			$getOnerow=$this->db->get('sch_student')->row();

			header("Content-type:text/x-json");

	$img_item_show='<img src="'.site_url('../assets/images/background.png').'" width="170px" height="170px">';
    // $arr_json = array();
    $arr_json['image_stu'] = $img_item_show;
    $arr_json['getRow'] = $getOnerow;

			// echo json_encode($getOnerow);
			echo json_encode($arr_json);

	}

	function getstdbyid(){
			$std_num=$this->input->post('std_num');
			$this->db->select('count(*)');
			$this->db->from('sch_student');
			$this->db->where('student_num',$std_num);
			echo $this->db->count_all_results();
	}
	function save(){
		$familyid=$this->input->post("familyid");
		$id_year = $this->input->post('id_year');
		$sch_yearid = $this->input->post('sch_yearid');

		//$memberid=$this->input->post("student");
		$id_number=$this->input->post("id_number");
		$student_lastname = $this->input->post("last_name");
		$student_first = $this->input->post("first_name");
		$student_dob = $this->input->post("dob");
		$student_num = $id_number;
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }

		$studentid=$this->input->post("studentid");
		$update_mem=$this->input->post('update_mem');
		$count=0;

		if($studentid ==''){
			$count=$this->std->validatestudent($student_first,$student_lastname,$student_dob);
		}
		else{
			$count=$this->std->validatestudentupdate($studentid,$student_first,$student_lastname,$student_dob);
		}


		if($count>0){

			$data['error']="<div style='text-align:center; color:red;'>This Student Have been Register Before Please check again...!</div>";
			$data['page_header']="New Student";
			$this->parser->parse('header', $data);
			if($studentid==''){
				$this->parser->parse('student/student_form', $data);

			}else{
				$data['student']=$this->std->getstudentrow($studentid);
				$this->parser->parse('student/student_edit', $data);
			}
			$this->parser->parse('footer', $data);
		}else{
			// Todo
			// HSC : Use md5 instead of encrypt encoding
			//$pwd = $this->encrypt->encode($this->input->post("txtpwd"));

			$save_result = $this->std->save($studentid);
            if($save_result!="0"){
            	// HSC: Error
            	// will fixed it later
            	$this->do_upload($studentid);
                //redirect(site_url('student/student/preview/'.$save_result.'?yearid='.$id_year.'?stuid='.$save_result), 'refresh');
               if($studentid !=""){
               		$year = $this->session->userdata('year');
               		redirect(site_url('student/student?y='.$year), 'refresh');
               }else{
               	 	redirect(site_url('student/student/add'), 'refresh');
               }

            }else{

            	$data['error']="<div style='text-align:center; color:red;'>This Student ID Have been Register Before Please check again...!</div>";
				$data['page_header']="New Student";
				$this->parser->parse('header', $data);
				if($studentid==''){
					$this->parser->parse('student/student_form', $data);

				}else{
					$data['student']=$this->std->getstudentrow($studentid);
					$this->parser->parse('student/student_edit', $data);
				}
				$this->parser->parse('footer', $data);
	        }
		}
	}
	
	function booksave(){
	    
	    $studentid=$this->input->post("studentid");
	    $yearid = $this->input->post('yearid');
	    $classid = $this->input->post('classid');
	    
	    $month=$this->input->post("month");
	    $week = $this->input->post("week");
	    $mentality = $this->input->post("mentality");
	    $intellectual = $this->input->post("intellectual");
	    $feeling = $this->input->post("feeling");
	    $society = $this->input->post("society");	
	    
	    $created_by = $this->session->userdata('user_name');
	    $created_date = date("Y-m-d H:i:s");
	    
	    $data = array(
	        'id_student' => $studentid,
	        'id_year' => $yearid,
	        'month' => $month,
	        'week' => $week,
	        'mentality' => $mentality,
	        'intellectual' => $intellectual,
	        'feeling' => $feeling,
	        'society' => $society,
	        'created_date' => $created_date,
	        'created_by' => $created_by,
	        'last_modified_by' => $created_by,
	        'last_modified_date' => $created_date,
	        'is_active' => '1'
	    );
	    $submit = $this->input->post('add_academic');
	    if(isset($submit)){  	   
	       $count=$this->std->booksave($data);
	       redirect(site_url('student/student/detail/'.$studentid.'?yearid='.$yearid.'&classid='.$classid), 'refresh');
	    }
	}
	
	function bookupdate(){
	    
	    $studentid=$this->input->post("studentid");
	    $yearid = $this->input->post('yearid');
	    $bookid = $this->input->post('bookid');
	    
	    $month=$this->input->post("month");
	    $week = $this->input->post("week");
	    $mentality = $this->input->post("mentality");
	    $intellectual = $this->input->post("intellectual");
	    $feeling = $this->input->post("feeling");
	    $society = $this->input->post("society");
	    
	    $update_by = $this->session->userdata('user_name');
	    $update_date = date("Y-m-d H:i:s");
	    
	    $data = array(
	        'id_student' => $studentid,
	        'id_year' => $yearid,
	        'month' => $month,
	        'week' => $week,
	        'mentality' => $mentality,
	        'intellectual' => $intellectual,
	        'feeling' => $feeling,
	        'society' => $society,
	        'last_modified_by' => $update_by,
	        'last_modified_date' => $update_date,
	        'is_active' => '1'
	    );
	    $submit = $this->input->post('add_academic');
	    if(isset($submit)){
	        $count=$this->std->bookupdate($bookid,$data);
	        redirect(site_url('student/student/detail/'.$studentid.'?yearid='.$yearid), 'refresh');
	    }
	}
	
	function getresponstudent(){

		foreach ($this->std->getresponfamily($this->input->post('familyid')) as $family_res) {

			echo " <tr>
						<td ><input class='hide' type='text' value='$family_res->first_name $family_res->last_name' name='res_name[]'/>$family_res->first_name $family_res->last_name</td>
						<td ><input class='hide' type='text' value='$family_res->first_name_kh $family_res->last_name_kh' name='res_name_kh[]'/>$family_res->first_name_kh $family_res->last_name_kh</td>
						<td ><input class='hide' type='text' value='$family_res->sex' name='res_sex[]'/>$family_res->sex</td>
						<td ><input class='hide' type='text' value='$family_res->dob' name='res_dob[]'  id='res_dob'/>".$this->green->formatSQLDate($family_res->dob)."</td>
						<td><input class='hide' type='text' value='$family_res->relationship' name='res_relationship[]'/>$family_res->relationship</td>
						<td width='200' style='word-break:break-all;'><input class='hide' type='text' value='$family_res->occupation' name='res_occupation[]'/>$family_res->occupation</td>
						<td ><input class='hide' type='text' value='$family_res->revenue' name='res_revenue[]'/>$family_res->revenue</td>
					</tr>" ;
		}
	}

	function getmemberstudent(){
			$memid=$this->input->post('memberid');
			$studentid=$this->input->post('studentid');
			$familyid=$this->input->post('familyid');
			$relation=array("Legal Student","brother/sister","Cousin","niece/nephew");
			if($studentid>0)
				$query=$this->std->getmemberfamily($familyid,0,$studentid);
			else
				$query=$this->std->getmemberfamily($familyid,$memid,0);
		  	foreach ($query as $family_mem) {
		  		if($studentid!=''){
			  		$select_relat=$this->std->getrelation($studentid,$family_mem->memid);
		  		}
		  		echo "
					  	<tr>
						  	<td class='hide'><input class='hide mem_id' type='text' value='$family_mem->memid' name='mem_id[]' id='mem_id'/></td>
						  	<td class='hide'><input class='hide std_num' type='text' rel='mem_id[]' value='$family_mem->studentid' name='std_num[]' id='std_num'/></td>
						  	<td ><input class='hide' type='text' value='$family_mem->first_name' name='mem_first_name[]' id='mem_first_name'/>$family_mem->first_name</td>
						  	<td ><input class='hide' type='text' value='$family_mem->last_name' name='mem_last_name[]' id='mem_last_name'/>$family_mem->last_name </td>
						  	<td ><input class='hide' type='text' value='$family_mem->first_name_kh' name='mem_first_name_kh[]'/>$family_mem->first_name_kh</td>
						  	<td ><input class='hide' type='text' value='$family_mem->last_name_kh' name='mem_last_name_kh[]'/>$family_mem->last_name_kh</td>
						  	<td ><input class='hide' type='text' value='$family_mem->sex' name='mem_sex[]'/>$family_mem->sex</td>
						  	<td ><input class='hide' type='text' value='$family_mem->dob' name='mem_dob[]' id='mem_dob'/>".$this->green->formatSQLDate($family_mem->dob)."</td>
						  	<td width='200' style='word-break:break-all;'><input class='hide' type='text' value='$family_mem->occupation' name='mem_occupation[]'/>$family_mem->occupation</td>
						  	<td ><input class='hide' type='text' value='$family_mem->grade_level' name='mem_grade_level[]'/>$family_mem->grade_level</td>
						  	<td ><input class='hide' type='text' value='$family_mem->school' name='mem_school[]'/>$family_mem->school</td>
						  	<td ><select  minlength='1' class='form-control hide' name='mem_relationship[]'>
						  		<option value=''>Select relationship</option>";
						  	foreach ($relation as $relat) {?>
						  		<option value='<?php echo $relat; ?>' <?Php if($select_relat==$relat) echo "selected"; ?>><?php echo $relat; ?></option>";
						  	<?php }

						echo "</select></td>
								</tr>";
				}
	}

	function do_upload($student_id)
	{
	    $this->load->library('upload');
	    $orders=$this->input->post('order');
		$getfilename = $this->input->post('getfilename');
		$years=$this->input->post('sch_yearid');
	    $files = $_FILES;
	    $cpt = count($_FILES['stufiles']['name']);

	    $index = 1;
	    for($i=0; $i<$cpt; $i++){

			$yearid = $years[$i];
	    	if(!file_exists('./assets/upload/students/'.$yearid))
			{
			    if(mkdir('./assets/upload/students/'.$yearid,0755,true))
			    {
			        return true;
			    }
			}

	        $_FILES['stufiles']['name']= $files['stufiles']['name'][$i];
	        $_FILES['stufiles']['type']= $files['stufiles']['type'][$i];
	        $_FILES['stufiles']['tmp_name']= $files['stufiles']['tmp_name'][$i];
	        $_FILES['stufiles']['error']= $files['stufiles']['error'][$i];
	        $_FILES['stufiles']['size']= $files['stufiles']['size'][$i];

	        $this->upload->initialize($this->set_upload_options($student_id,$_FILES['stufiles']['name'],$yearid));

	        if ( ! $this->upload->do_upload("stufiles")){
				$error = array('error' => $this->upload->display_errors());
			}else{
				$this->createthumb($student_id,$yearid);
				$this->createthumbs($student_id,$yearid);
			}
			$index++;
	    }
	}
	function set_upload_options($student_id,$imagename,$yearid){

		    //upload an image options
		    if(!file_exists('./assets/upload/students/'.$yearid)){
			    if(mkdir('./assets/upload/students/'.$yearid,0755,true)){
			        return true;
			    }
		    	if(mkdir('./assets/upload/students/'.$yearid.'/thumb',0755,true)){
					return true;
			    }
			}
		    $config = array();
		    $config['upload_path'] = './assets/upload/students/'.$yearid;
		    $config['allowed_types'] = 'gif|jpg|png';
		    $config['max_size']      = '0';
		    $config['file_name']  	 = $student_id.".jpg";
			$config['overwrite']	 = true;

		    return $config;
		}

	function createthumb($student_id,$yearid){

		if(!file_exists('./assets/upload/students/'.$yearid.'/thumb')){
			if(mkdir('./assets/upload/students/'.$yearid.'/thumb',0755,true)){
					return true;
			}
		}

		$data = array('upload_data' => $this->upload->data());
	 	$config2['image_library'] = 'gd2';
        $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
        $config2['new_image'] = './assets/upload/students/'.$yearid.'/thumb';
        $config2['maintain_ratio'] = false;
        $config2['create_thumb'] = $student_id.".jpg";
        $config2['thumb_marker'] = false;
        $config2['overwrite']	 = true;
        $config2['height'] = 420;
        $config2['width'] = 420;

        $this->load->library('image_lib');
        $this->image_lib->initialize($config2);
        if ( ! $this->image_lib->resize()){
        	echo $this->image_lib->display_errors();
		}
	}

	function createthumbs($student_id,$yearid)
	{
	    if(!file_exists('./assets/upload/students/'.$yearid.'/thumbs')){
			if(mkdir('./assets/upload/students/'.$yearid.'/thumbs',0755,true)){
					return true;
			}
		}
		$data = array('upload_data' => $this->upload->data());
	 	$config2['image_library'] = 'gd2';
        $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
        $config2['new_image'] = './assets/upload/students/'.$yearid.'/thumbs';
        $config2['maintain_ratio'] = false;
        $config2['create_thumb'] = $student_id.".jpg";
        $config2['thumb_marker'] = false;
        $config2['overwrite']	 = true;
        $config2['height'] = 152;
    	$config2['width'] = 114;

        $this->load->library('image_lib');
        $this->image_lib->initialize($config2);
        if ( ! $this->image_lib->resize()){
        	echo $this->image_lib->display_errors();
		}
	}
	function getedit(){
		$stdspid=$this->input->post('stdspid');
		$spon= $this->db->select('sd.sponsorid,sd.studentid,sd.stdspid,sd.start_sp_date,sd.end_sp_date,fs.last_name,fs.first_name')
					->from('sch_student_sponsor_detail sd')
					->join('sch_family_sponsor fs','sd.sponsorid=fs.sponsorid','inner')
					->where('sd.stdspid',$stdspid)->get()->row();
		header("Content-type:text/x-json");
		echo json_encode($spon);
	}
	
	function search(){
		if(isset($_GET['s_id'])){
			$studentid 	  = $_GET['s_id'];
			$full_name 	  = $_GET['fn'];
			$phone1       = $_GET['fone'];
			$classid 	  = $_GET['class'];
			$year	      = $_GET['year'];
			$schlevelid   = $_GET['l'];
			$boarding     = $_GET['b'];
			$level 		  = $_GET['le'];
			$m 			  = $_GET['m'];
			$p 			  = $_GET['p'];
			$ag 		  = $_GET['ag'];
			$sort_num 	  = $_GET['s_num'];
			$sort 		  = $this->input->post('sort');
			$promot_id	  = $_GET['pro'];
			$data['tdata']=$query=$this->std->searchstudent($boarding,$studentid,$full_name,$phone1,$classid,$sort,$sort_num,$year,$schlevelid,$level,$m,$p,$ag,$promot_id);
			$data['idfield']=$this->idfield;
			$data['thead']=	$this->thead;
			$data['exp']=	$this->exp;
			$data['page_header']="Student List";
			$this->parser->parse('header', $data);
			$this->parser->parse('student/student_list', $data);
			$this->parser->parse('footer', $data);
		}
		if(!isset($_GET['s_id'])){
			$studentid 	  = $this->input->post('studentid');
			$full_name 	  = $this->input->post('full_name');
			$phone1       = $this->input->post('phone1');
			$classid 	  = $this->input->post('classid');
			$sort 		  = $this->input->post('sort');
			$sort_num 	  = $this->input->post('sort_num');
			$year 		  = $this->input->post('year');
			$schlevelid   = $this->input->post('schlevelid');
			$level 		  = $this->input->post('level');
			$boarding 	  = $this->input->post('boarding');
			$m 			  = $this->input->post('m');
			$p 			  = $this->input->post('p');
			$ag 		  = $this->input->post('ag');
			$promot_id 	  = $this->input->post('promot_id');

			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p);
		    }
			$query=$this->std->searchstudent($boarding,$studentid,$full_name,$phone1,$classid,$sort,$sort_num,$year,$schlevelid,$level,$m,$p,$ag,$promot_id);
				 //echo "count:".count($query);
				 //exit();
				 $i=1;
				 $data='';
				foreach($query as $row){
					if($row['familyid']!='' && $row['familyid']!='0'){
						$this->db->where('familyid',$row['familyid']);
						$family_code=$this->db->get('sch_family')->row()->family_code;
					}
					else
					$family_code='';

					$no_imgs = base_url("assets/upload/No_person.jpg");
					$have_img = base_url()."assets/upload/students/".$row["yearid"].'/'.$row["studentid"].'.jpg';
					$img = '<img src="'.$no_imgs.'" class="img-circle" alt="No Image" style="width:70px !important; height:70px !important;">';
					if (file_exists(FCPATH . "assets/upload/students/".$row["yearid"].'/'.$row["studentid"].'.jpg')) {
					$img = '<img src="'.$have_img.'" class="img-circle" alt="No Image" style="width:70px !important; height:70px !important;">';
					}

					$data.="<tr>
						 <td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
						 <td>".$img."</td>
						 <td class='student_num'>
							<a href='".site_url("/student/student/preview/".$row['studentid']."?yearid=".$row['yearid'])."&stuid=".$row['studentid']."' target='_blank'>
							".$row['student_num']."</td>
						 <td class='last_name'>
						 	<span>".$row['last_name_kh']." ".$row['first_name_kh']."</span><br>
						 	<span>".$row['last_name']." ".$row['first_name']."</span>
						 </td>
						 <td class='phone1'>".$row['phone1']."</td>
						 <td class='dateofbirth'>".$row['dob']."</td>
						 <td class='register_date'>".$row['register_date']."</td>
						 <td class='class_name'>".$row['class_name']."</td>
						 <td class='yearid'>".$row['sch_year']."</td>
						 <td class='FamilyID'><a href='".site_url("social/family/preview/$row[familyid]")."' target='_blank'>$family_code</a></td>";
						if($boarding!=1){
							$data.= "<td class='remove_tag'>";
							if($this->green->gAction("P")){
								$data.= "<a>
							 		<img rel=".$row['studentid']." onclick='previewhistory(event);' src='".site_url('../assets/images/icons/a_preview.png')."'/>
							 	</a>
							 	<a>
							 		<img yearid='".$row['yearid']."' rel=".$row['studentid']." onclick='previewstudent(event);' src='".site_url('../assets/images/icons/preview.png')."'/>
							 	</a> ";
							}
							if($this->green->gAction("D")){
								$data.="<a><img rel=".$row['studentid']." onclick='deletestudent(event);' src='".site_url('../assets/images/icons/delete.png')."'/></a>";
							}
							if($this->green->gAction("U")){
								$edit_link=site_url("student/student/edit/".$row['studentid'])."?yearid=".$row['yearid']."&m=$m&p=$p" ;
                                $data.="<a href='".$edit_link."' target='_blank'>
                                    <img yearid='".$row['yearid']."' rel=".$row['studentid']." src='".site_url('../assets/images/icons/edit.png')."'/>
                                  </a>";

                            }
                            $edit_link=site_url("student/student/detail/".$row['studentid'])."?yearid=".$row['yearid']."&classid=".$row['classid'];
                                $data.="<a href='".$edit_link."' target='_blank'>
                                    <img yearid='".$row['yearid']."' rel=".$row['studentid']." src='".site_url('../assets/images/icons/detail.png')."'/>
                                  </a>";
							$data.="</td>";
						}
						$data.="</tr>";
					$i++;
					}
					$data.="<tr class='remove_tag'>
						<td colspan='12' id='pgt'>
							<div style='margin-top:20px; width:10%; float:left;'>
							Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";

											$num=10;
											for($i=0;$i<10;$i++){
												$select='';
												if($num==$sort_num)
													$select='selected';
												$data.="<option value='".$num."' $select > $num</option>";
												$num+=10;
											}
											$select='';
											if($num=='all')
												$select='selected';
											$data.="<option value='all' $select>All</option>

								</select>
							</div>
							<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
								<ul class='pagination' style='text-align:center'>
								 ".$this->pagination->create_links()."
								</ul>
							</div>

						</td>
					</tr> ";
					$arr=array('data'=>$data);
					header("Content-type:text/x-json");
					echo json_encode($arr);
		}
	}
	
	function searchbook(){
	   
	    if(!isset($_GET['s_id'])){
	        $studentid 	  = $this->input->post('studentid');
	        $yearid	  = $this->input->post('yearid');
	        $month       = $this->input->post('s_classid');
	        
	        $query=$this->std->getbookbymonth($studentid,$yearid,$month);
	        
	        $i=1;
	        $data='';
	        
	        foreach($query as $row){
	            $data.="<tr>
							                         <td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
							                         <td class='student_num'>".$row->month_kh."</td>
    											     <td class='student_num'>".$row->week_kh."</td>
                                                    <td class='student_num'>".$row->mentality."</td>
                                                    <td class='student_num'>".$row->intellectual."</td>
                                                    <td class='student_num'>".$row->feeling."</td>
                                                    <td class='student_num'>".$row->society."</td>
				    
        											 <td class='remove_tag'>";
	            
	            //if($this->green->gAction("D")){
	                $data.="<a class='del_row' rel=".$row->id." ><img src='".site_url('../assets/images/icons/delete.png')."'/></a>";
	           // }
	           // if($this->green->gAction("U")){
	                
	                $edit_link=site_url("student/student/edit/".$studentid."?yearid=".$yearid."&bookid=".$row->id);
	                $data.="<a href='".$edit_link."'>
        	                                                    <img src='".site_url('../assets/images/icons/edit.png')."'/>
        	                                                  </a>";
	           // }
	            
	            $data.="</td>
											     </tr>";
	            $i++;
	        }
	        
	       
	           $arr=array('data'=>$data);
	            header("Content-type:text/x-json");
	            echo json_encode($arr);
	    }
	}
	
	function check_stu_evaluate(){
		$studentid=$this->input->post("stu_id");
		$arr['isdel']=1;
		$geteval=$this->green->getValue("SELECT COUNT(*) num FROM sch_student_fee WHERE studentid='{$studentid}'");
		if($geteval>0){
			$arr['isdel']=0;
		}
		header("Content-type:text/x-json");
		echo json_encode($arr);
	}
	function cdelete_acdimice(){

		$studentid = $this->input->post("studentid");
		$sch_programid = $this->input->post("sch_programid");
		$sch_levelid = $this->input->post("sch_levelid");
		$sch_yearid = $this->input->post("sch_yearid");
		$sch_rangelevelid = $this->input->post("sch_rangelevelid");
		$sch_classid = $this->input->post("sch_classid");

		$deleted = $this->std->delete_acadimice($studentid,$sch_programid,$sch_levelid,$sch_yearid,$sch_rangelevelid,$sch_classid);

		header("Content-type:text/x-json");
		echo json_encode($deleted);
	}

	function delete(){

		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
	    $studentid=$this->input->post('stu_id');
	   	#======= check status =====

	   	$status=$this->green->getValue("SELECT is_active FROM sch_student WHERE studentid='{$studentid}'");

	   	if($status==0){
	   		$this->green->runSQL("DELETE FROM sch_student WHERE studentid='{$studentid}'");
	   		$this->green->runSQL("DELETE FROM sch_student_enrollment WHERE studentid='{$studentid}'");
	   	}else{
	   		$this->db->set('is_active',0);
			$this->db->where('studentid',$studentid);
			$this->db->update('sch_student');
	   	}

		/*
		$login_username=$this->std->getstudentrow($studentid)->login_username;
		$this->db->set('is_active',0);
		$this->db->where('user_name',$login_username);
		$this->db->update('sch_user');
		*/
		redirect("student/student?m=$m&p=$p");
	}
	function cgetfamilyrow(){
		$familyid = $this->input->post('familyid');
		$data = $this->family->getfamily($familyid);
		header("Content-type:text/x-json");
		echo json_encode($data);
	}
	public function c_fillfamily()
	{
		$data = $this->family->getfamilyAuto();
		header("Content-type:text/x-json");
		echo json_encode($data);
	}
	function c_savenewfamily(){
		$familyid=$this->input->post("familyid");
		$data = $this->family->savefamily($familyid);
		header("Content-type:text/x-json");
		echo json_encode($data);
	}

	function cgetschlevel(){
		$schoolid = $this->session->userdata('schoolid');
		$sch_program = $this->input->post("sch_program");
		$option_level='';
		$schlevelid ='';
		$i =0;

		foreach ($this->sch_lev->getsch_level($sch_program) as $row_level){
			$option_level .='<option value="'.$row_level->schlevelid.'">'.$row_level->sch_level.'</option>';
			if($i==0){
				$schlevelid = $row_level->schlevelid;
			}
			$i++;
		}

		header("Content-type:text/x-json");
		$arr_success['schlevel'] = $option_level;
		echo json_encode($arr_success);
	}

	public function cgetschoolyear(){
		$schoolid = $this->session->userdata('schoolid');
		$sch_program = $this->input->post("sch_program");
		$schlevelid = $this->input->post("sch_level");
		$i =0;
		$option_year ='';

		foreach ($this->syear->getschoolyear($schoolid,$sch_program,$schlevelid) as $row){
			$option_year .='<option value="'.$row->yearid.'"'.($row->yearid != $this->session->userdata('year')?"":"selected").'>'.$row->sch_year.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['schyear'] = $option_year;
		echo json_encode($arr_success);

	}
	public function cgetschoolrangelevel(){
		$schoolid = $this->session->userdata('schoolid');
		$sch_program = $this->input->post("sch_program");
		$schlevelid = $this->input->post("sch_level");

		$i =0;
		$option_rangelevel ='<option value=""></option>';
		foreach ($this->ranglev->rangelevels($schoolid,$sch_program,$schlevelid) as $row){
			$option_rangelevel .='<option value="'.$row->rangelevelid.'">'.$row->rangelevelname.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['schrangelevel'] = $option_rangelevel;
		echo json_encode($arr_success);

	}
	public function cgetschoolclass(){
		$schlevelid = $this->input->post("sch_level");
		$gradlevel = $this->input->post("gradlevel");
		$i =0;
		$option_class ='<option value=""></option>';
		foreach ($this->cls->allclass($schlevelid,$gradlevel) as $row){
			$option_class .='<option value="'.$row->classid.'" >'.$row->class_name.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['schclass'] = $option_class;
		echo json_encode($arr_success);
	}
	function cgetpaymentmethod(){
		$yearid = $this->input->post("yearid");
		$sch_program = $this->input->post("sch_program");
		$sch_level = $this->input->post("sch_level");
		$sch_id = $this->session->userdata('schoolid');
		$paymentmethod = $this->input->post("paymentmethod");

		$html='';
		if($paymentmethod =="term"){
			foreach ($this->term->terms($sch_id,$yearid,$sch_program,$sch_level,$termid="") as $row_term){
				$html .='';
			}
		}else if($paymentmethod =="semester"){
			foreach ($this->term->semesters($sch_id,$yearid,$sch_program,$sch_level,$semesterid="") as $row_term){
				$html .='';
			}
		}else{

		}
		header("Content-type:text/x-json");
		echo json_encode($html);
	}
}
