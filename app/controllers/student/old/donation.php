<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Donation extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		$this->lang->load('student', 'english');
		$this->load->model("student/ModDonation","donate");
		$this->load->library('pagination');	
		$this->thead=array("No"=>'no',
							 "Date"=>'date',
							 "Student name"=>'fullname',
							 "Donation Type"=>'donate_type',
							 "Class"=>'class_name',
							 "Year"=>'year',
							 "Action"=>'Action'							 	
							);
		$this->idfield="studentid";
	}
	function index(){
		$data['tdata']=array();
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Student List";			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/gift/donation_list', $data);
		$this->parser->parse('footer', $data);
	}
	function gettransno(){
		echo $this->db->where('typeid',12)->get('sch_z_systype')->row()->sequence;
	}
	function save(){
		$this->donate->save();
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("student/donation?m=$m&p=$p");

	}
	function savedonate(){
		$date=$this->input->post('date');
		$donate_type=$this->input->post('donate_type');
		$is_up=$this->input->post('is_up');
		$transno=$this->input->post('transno');
		$yearid=$this->session->userdata('year');
		$school=$this->session->userdata('schoolid');
		$c_date=date('Y-m-d');
		$user=$this->session->userdata('user_name');
		$data=array('date'=>$this->green->convertSQLDate($date),
						'donate_type'=>$donate_type,
						'type'=>12,
						'transno'=>$transno,
						'yearid'=>$yearid,
						'schoolid'=>$school);
		$data2=array('created_by'=>$user,'created_date'=>$c_date);
		$this->db->insert('sch_stud_gifdonate',array_merge($data,$data2));
		$donate_id=$this->db->insert_id();
		$this->donate->updatetran($transno+1);
		echo $donate_id;
	}
	function savedonatedetail(){
		$studentid=$this->input->post('studentid');
		$yearid=$this->session->userdata('year');
		$date=$this->input->post('date');
		$donateid=$this->input->post('donateid');
		$transno=$this->input->post('transno');
		$gifid=$this->input->post('gifid');
		$qty=$this->input->post('qty');
		$unit=$this->input->post('unit');
		$remark=$this->input->post('remark');
		$this->donate->savedetail($donateid,$transno,$date,$studentid,$gifid,$qty,$unit,$remark,$yearid);
	}
	function rollback($transno){
		$this->db->where('transno',$transno)->delete('sch_stud_gifdonate');
		$this->db->where('transno',$transno)->delete('sch_stud_gifdonate_detail');
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("student/donation?m=$m&p=$p");
	}
	function clear(){
		$donateid=$this->input->post('donateid');
		$studentid=$this->input->post('studentid');
		$this->db->where('donateid',$donateid)->where('studentid',$studentid)->delete('sch_stud_gifdonate_detail');
	}
	function getstdrow(){
		$yearid=$this->session->userdata('year');
		$studentid=$this->input->post('studentid');
		$schlevelid=$this->input->post('schlevelid');
		$classid=$this->input->post('classid');
		$arr=$this->input->post('arr');
		$sql="SELECT * FROM v_student_profile WHERE yearid='$yearid'";
		if($schlevelid!='')
			$sql.=" AND schlevelid='$schlevelid'";
		if($classid!='')
			$sql.=" AND classid='$classid'";
		if($studentid!='')
			$sql.=" AND studentid='$studentid'";
		$data='';
		$error='';
		foreach ($this->db->query($sql)->result() as $stdrow) {
			if(!isset($arr[$stdrow->studentid]))
				$data.="<tr >
						<td style='vertical-align:middle; text-align:center;'><input type='checkbox' class='chstd' rel='$stdrow->studentid' id='chk_$stdrow->studentid'/></td>
						<td style='vertical-align:middle;'><input type='text' class='studentid hide' value='$stdrow->studentid' name='studentid[]' id='studentid'/>$stdrow->fullname</td>
						<td colspan='4'>
							<table class='table small'>
								<thead>
									<th width='250'></th>
									<th width='100'></th>
									<th width='100'></th>
									<th style='text-align:right'>
										<img rel='$stdrow->studentid' onclick='addgif(event);' src='".site_url('../assets/images/icons/add.png')."' />
									</th>
								</thead>
								<tbody class='ds_$stdrow->studentid'>
								</tbody>
							</table>
						</td>
						<td style='text-align:center; vertical-align:middle;'>
							<img  onclick='saveinline(event);' src='".site_url('../assets/images/icons/save.png')."'/>
						</td>

						<td style='text-align:center; vertical-align:middle;'>
							<img onclick='removerow(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
						</td>
						
					</tr>";
				else
					$error="Some of Student is Already Exist...!";
		# code...
		}
		$data=array("data"=>$data,'error'=>$error);
		header("Content-type:text/x-json");
		echo json_encode($data);
	}
	function add(){
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Student List";			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/gift/donation_form', $data);
		$this->parser->parse('footer', $data);
	}
	function edit($donateid){
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$datas['row']=$this->db->where('donateid',$donateid)->get('sch_stud_gifdonate')->row();
		$data['page_header']="Student List";			
		$this->parser->parse('header', $data);
		$this->load->view('student/gift/donation_form', $datas);
		$this->parser->parse('footer', $data);
	}
	function preview($donateid,$studentid=''){
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$datas['row']=$this->db->where('donateid',$donateid)->get('sch_stud_gifdonate')->row();
		$data['page_header']="Student List";			
		$this->parser->parse('header', $data);
		if($studentid!=''){
			$datas['stdid']=$studentid;
			$this->load->view('student/gift/preview_one', $datas);
		}else{
			$this->load->view('student/gift/preview_donate', $datas);
		}
		$this->parser->parse('footer', $data);
	}
	function getclassch(){
		$schlevelid=$this->input->post('schlevelid');
			echo "<option value=''>-----Select------</option>";
		foreach ($this->db->where('schlevelid',$schlevelid)->get('sch_class')->result() as $c) {
			echo "<option value='$c->classid'>$c->class_name</option>";
		}
	}
	function deletes($donateid){
		$this->db->where('donateid',$donateid)->delete('sch_stud_gifdonate');
		$this->db->where('donateid',$donateid)->delete('sch_stud_gifdonate_detail');
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("student/donation?m=$m&p=$p");
	}
	function getstd(){
			$key=$_GET['term'];
			$schlevelid=$_GET['l'];
			$classid=$_GET['c'];
			$yearid=$this->session->userdata('year');
			$sql="SELECT * 
								FROM v_student_profile
								WHERE yearid='$yearid' 
								AND (fullname LIKE '%$key%' OR student_num LIKE '%$key%')";
			if($schlevelid!='' || $schlevelid!=null)
				$sql.=" AND schlevelid='$schlevelid'";
			if($classid!='' || $classid!=null)
				$sql.=" AND classid='$classid'";
			$data=$this->db->query($sql)->result();
			$array=array();
			foreach ($data as $row) {
				$array[]=array('value'=>$row->fullname.' | '.$row->student_num,
								'id'=>$row->studentid);
			}
		    echo json_encode($array);
		}
	function getgift(){
			$key=$_GET['term'];
			$data=$this->db->query("SELECT * 
								FROM sch_student_gif
								WHERE gifname LIKE '%$key%'")->result();
			$array=array();
			foreach ($data as $row) {
				$array[]=array('value'=>$row->gifname,
								'id'=>$row->gifid,'unit'=>$row->unit);
			}
		    echo json_encode($array);
		}
	function search(){
			if(isset($_GET['n'])){
					$name=$_GET['n'];
					$classid=$_GET['c'];
					$type=$_GET['t'];
					$m=$_GET['m'];
					$p=$_GET['p'];
					$sort_num=$_GET['s_num'];
					$slv=$_GET['slv'];
					$yearid=$_GET['y'];

					$data['tdata']=$this->donate->search($name,$type,$classid,$s_num,$m,$p,$slv,$yearid);
					$data['idfield']=$this->idfield;		
					$data['semthead']=	$this->semthead;
					$data['page_header']="Donation";			
					$this->parser->parse('header', $data);
					$this->parser->parse('student/donation/donation_list', $data);
					$this->parser->parse('footer', $data);
			}else{
					$name=$this->input->post('name');
					$classid=$this->input->post('classid');
					$type=$this->input->post('type');
					$sort_num=$this->input->post('sort_num'); 
					$i=1;
					$yearid=$this->input->post('yearid');
					$slv=$this->input->post('slv');
					$m=$this->input->post('m');
					$p=$this->input->post('p');
					$this->green->setActiveRole($this->input->post('roleid'));
					if($m!=''){
				        $this->green->setActiveModule($m);
				    }
				    if($p!=''){
				        $this->green->setActivePage($p); 
				    }
					foreach ($this->donate->search($name,$type,$classid,$sort_num,$m,$p,$slv,$yearid) as $row) {
						$tpe='';
						if($row->donate_type=='AG')
							$tpe="Annual Gift";
						else
							$tpe="Sponsor Order";
						$arr[]=array();
						echo "<tr>
							 	 <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
								 <td class='date'>".date('d-m-Y',strtotime($row->date))."</td>
								 <td class='fullname'>".$row->fullname."</td>
								 <td class='donate_type'>".$tpe."</td>
								 <td class='class_name'>".$row->class_name."</td>
								 <td class='year'>".$row->sch_year."</td>											
								 <td class='remove_tag'>";
								if($this->green->gAction("P")){	
								 	echo "<a>
								 		<img rel=".$row->donateid." std=".$row->studentid." onclick='preview(event);' src='".site_url('../assets/images/icons/preview.png')."'/>
								 	</a>";
								}
								if(!isset($arr[$row->transno])){
									if($this->green->gAction("P")){	
									 	echo "<a>
									 		<img rel=".$row->donateid." std='' onclick='preview(event);' src='".site_url('../assets/images/icons/a_preview.png')."'/>
									 	</a>";
									 	echo "<a href='".site_url("student/donation/previewGiftLabel/$row->donateid")."' target='_blank'>
									 		<img rel=".$row->donateid." title='Print Gift Label'  src='".site_url('../assets/images/icons/preview.png')."'/>
									 	</a>";
									}
									if($this->green->gAction("D")){	
									 	echo "<a>
									 		<img rel=".$row->donateid." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
									 	</a>";
									}
									if($this->green->gAction("U")){	
									 	echo "<a>
									 		<img  rel=".$row->donateid." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
									 	</a>";
									 }
								}
								$arr[$row->transno]=$row->transno;
								echo"</td>
						 	</tr>";
						 $i++;
					}
						
					echo "<tr class='remove_tag'>
											<td colspan='12' id='pgt'>
												<div style='margin-top:20px; width:10%; float:left;'>
												Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
																
																$num=50;
																for($i=0;$i<10;$i++){?>
																	<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
																	<?php $num+=50;
																}
																
															echo "</select>
												</div>
												<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
													<ul class='pagination' style='text-align:center'>
													 ".$this->pagination->create_links()."
													</ul>
												</div>

											</td>
										</tr> ";
			}
			
		}
	function previewGiftLabel($donateid){
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$datas['row']=$this->db->where('donateid',$donateid)->get('sch_stud_gifdonate')->row();
		$data['page_header']="Student List";			
		$this->parser->parse('header', $data);		
		$this->load->view('student/gift/giftclasslabel_preview', $datas);		
		$this->parser->parse('footer', $data);
	}
}