<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_subject_score_entry_kgp_complete extends CI_Controller {	

	function __construct() {
		parent::__construct();
		$this->load->model('student/m_subject_score_entry_kgp', 'mode');
		$this->load->model('student/m_evaluate_trans_kgp', 'student');
		$this->load->model('school/schoolinformodel', 'school');
		$this->load->model('school/program', 'program');			
		$this->load->model('school/schoollevelmodel', 'school_level');
		$this->load->model('school/gradelevelmodel', 'grade_level');
		$this->load->model('school/schoolyearmodel', 'adcademic_year');
		$this->load->model('school/classmodel', 'class');
	}

	public function index() {	
		$this->load->view('header');
		$this->load->view('student/v_subject_score_entry_kgp_complete');
		$this->load->view('footer');	
	}

	//get year and grade data----------------------------------
	public function get_year_and_grade_data()
	{
		$school_id = $this->input->post('school_id');
		$program_id = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		
		$opt = array();

		//get year
		$y_opt = array();
		$get_year_data = $this->adcademic_year->getschoolyear($school_id,$program_id,$school_level_id);
		if(count($get_year_data) > 0){
			foreach($get_year_data as $r_y){				
				$y_opt[] = $r_y;
			}
		}			

		//get grade
		$g_opt = array(); 
		$get_grade_data = $this->grade_level->getgradelevels($school_level_id);
		if(count($get_grade_data) > 0){
			foreach($get_grade_data as $r_g){				
				$g_opt[] = $r_g;
			}
		}			

		$opt["year"] = $y_opt;
		$opt["grade"] = $g_opt;

		header('Content-Type: application/json');
		echo json_encode($opt);
	}

	//get teacher data-------------------------------------------
	public function get_teacher_data()
	{
		$school_level_id = $this->input->post('school_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$grade_level_id = $this->input->post('grade_level_id');
		$t_opt = '<option value=""></option>';

		$get_teacher_data = $this->mode->getTeacherInfo("", $school_level_id, $adcademic_year_id, $grade_level_id);
		if(count($get_teacher_data) > 0){
			foreach($get_teacher_data as $r_t){
				$t_opt .= '<option value="'. $r_t->teacher_id .'">'. $r_t->first_name .'  '. $r_t->last_name .'</option>';
			}
		}
					
		echo $t_opt;
	}

	//get class data---------------------------------------------
	public function get_class_data()
	{
		$school_level_id = $this->input->post('school_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$grade_level_id = $this->input->post('grade_level_id');
		$teacher_id = $this->input->post('teacher_id');
		$c_opt = '<option value=""></option>';

		$get_class_data = $this->mode->getClassByTeacher($teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id);
		if(count($get_class_data) > 0){
			foreach($get_class_data as $r_c){
				$c_opt .= '<option value="'. $r_c->classid .'">'. $r_c->class_name .'</option>';
			}
		}
				
		echo $c_opt;
	}

	//get subject data-------------------------------------------
	public function get_subject_data()
	{
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id = $this->input->post('grade_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$teacher_id = $this->input->post('teacher_id');
		$class_id = $this->input->post('class_id');
		$su_opt = '<option value=""></option>';

		$get_subject_data = $this->mode->getSubjectByTeacher("", $class_id, $teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id);
		if(count($get_subject_data) > 0){
			foreach($get_subject_data as $r_su){
				$su_opt .= '<option value="'. $r_su->subjectid .'">'. $r_su->subject_name .'</option>';
			}
		}			
		
		echo $su_opt;
	}

	//get subject data-------------------------------------------
	public function get_group_main_id_subject()
	{
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id = $this->input->post('grade_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$teacher_id = $this->input->post('teacher_id');
		$class_id = $this->input->post('class_id');
		$subject_id = $this->input->post('subject_id');
		$opt_id = array();

		$get_id_data = $this->mode->getSubjectByTeacher($subject_id, $class_id, $teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id);
		if(count($get_id_data) > 0){
			foreach($get_id_data as $r_id){
				$opt_id[] = $r_id;
				// $opt_id['sg_id'] = $r_id->subj_type_id;
				// $opt_id['sm_id'] = $r_id->subj_main_id;
			}
		}			
		
		header('Content-Type: application/json');
		echo json_encode($opt_id);
	}

	//get student data-----------------------------------------
	public function get_student_data()
	{
		$class_id = $this->input->post('class_id');
		$where = "";
		if($class_id != ""){
			$where .= " AND sc.classid = ". $class_id;
		}
		$s_opt = '<option value=""></option>';

		$i = 1;
		$get_student_data = $this->student->getStudentInformation($where);
		if(count($get_student_data) > 0){
			foreach($get_student_data as $r_s){
				$s_opt .= '<option value="'. $r_s->studentid .'">'. $i .'. '. $r_s->last_name .' '. $r_s->first_name .'</option>';
				$i++;
			}
		}
					
		echo $s_opt;
	}

	//get semester data----------------------------------------
	public function get_semester(){
		$school_id = $this->input->post('school_id');
		$program_id = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');

		$arr_semester = array();
		$get_semester_data = $this->mode->getSemesterData($school_id, $program_id, $school_level_id, $adcademic_year_id);
		if(count($get_semester_data) > 0){
			foreach($get_semester_data as $row){
				$arr_semester[] = $row;
			}
		}

		print_r($arr_semester);
		exit();

		header('Content-Type: application/json');
		echo json_encode($arr_semester);
		die();
	}


	// search ==========
	public function search() {
		$school_id = $this->input->post('school_id');
		$program_id = $this->input->post('program_id');		
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$teacher_id = $this->input->post('teacher_id');
		$class_id = $this->input->post('class_id');
		$subject_id = $this->input->post('subject_id');
		$student_id = $this->input->post('student_id');
		$get_exam_type = $this->input->post('exam_type');

		$where = "";
		if($program_id != ""){
			$where .= " AND sp.programid = ". $program_id;
		}
		if($school_level_id != ""){
			$where .= " AND sl.schlevelid = ". $school_level_id;
		}
		if($adcademic_year_id != ""){
			$where .= " AND sy.yearid = ". $adcademic_year_id;
		}
		if($class_id != ""){
			$where .= " AND sc.classid = ". $class_id;
		}
		if($student_id != ""){
			$where .= " AND s.studentid = ". $student_id;
		}
		
		// subject group ========	
		$q_subjectType = $this->db->query("SELECT
												sch_subject_type.subject_type,
												sch_subject.subj_type_id,
												sch_subject_type.main_type,
												COUNT(sch_subject.subjectid) AS c
											FROM
												sch_subject_type
											INNER JOIN sch_subject ON sch_subject_type.subj_type_id = sch_subject.subj_type_id
											GROUP BY
												sch_subject.subj_type_id ");

		$cAll = $this->db->query("SELECT COUNT(s.subj_type_id) AS cc FROM sch_subject_type AS s ")->row()->cc - 0;

		$thead = '';
		$thead .= '<tr>
					<th rowspan="2">No</th>
                    <th rowspan="2" style="min-Width: 120px;">Name</th>
                    <th rowspan="2">Present</th>
                    <th rowspan="2">Absent</th>';
        $j = 1;
        $k = 0;
        $subj_type_id = '';
		if ($q_subjectType->num_rows() > 0) {
			foreach ($q_subjectType->result() as $r) {
				$k += $j++;
				$subj_type_id .= $r->subj_type_id.',';

				$thead .= '<th colspan="'.($r->c - 0 > 1 ? $r->c : 1).'" attr-subj_type_id="'.$r->subj_type_id.'">'.$r->subject_type.'</th>';		

					

				if ($cAll == $k) {// check with last...=====
					$thead .= '<th rowspan="2">Comment</th></tr>';
					$subj_type_id_ = trim($subj_type_id, ',');

					// subject ========
					$q_subject = $this->db->query("SELECT
														s.subjectid,
														s.`subject`,
														s.subj_type_id
													FROM
														sch_subject AS s
													WHERE
														s.subj_type_id IN ($subj_type_id_)
													ORDER BY
														s.subj_type_id,
														s.`subject` ASC ");
					$thead .= '';
					$thead .= '<tr>';
					if ($q_subject->num_rows() > 0) {
						foreach ($q_subject->result() as $r_) {
							$thead .= '<th>'.$r_->subject.'</th>';							
						}
					}
					$thead .= '</tr>';


					// tbody =============
					/* div += '<div class="title-info">
			            		<table border="0" width="100%" style="line-height:20px;">
			            			<tr>
			            				<td>- Co-efficients:</td>
			            				<td><input type="text" class="_form-control _input-sm" name="averag_coefficient_" id="averag_coefficient_" value="" style="_border: 0;_border-bottom: 1px solid;width: 50px;text-align: center;" decimal></td>
		            				</tr>
	            				</table>
        					</div>'; */

					$where_ = "";
					if($program_id != ""){
						$where_ .= " AND m.program_id = ". $program_id;
					}
					if($school_level_id != ""){
						$where_ .= " AND m.school_level_id = ". $school_level_id;
					}
					if($adcademic_year_id != ""){
						$where_ .= " AND m.adcademic_year_id = ". $adcademic_year_id;
					}
					if($class_id != ""){
						$where_ .= " AND m.class_id = ". $class_id;
					}
					
					$jj = 1;
					$tbody = '';

					$get_student_info = $this->student->getStudentInformation($where);

					if(count($get_student_info) > 0)
					{
						foreach($get_student_info as $rowStu)
						{
							$tbody .= '<tr>
										<td class="no_" style="text-align: center;">'.$jj++.'</td>
										<td class="student_id" style="padding-left: 3px !important;" attr-student_id="'.$rowStu->studentid.'" attr-teacher_id="'.$teacher_id.'">'.$rowStu->last_name_kh.' '.$rowStu->first_name_kh.'</td>
										<td><input type="text" class="form-control input-xs present" value="0"></td>
										<td><input type="text" class="form-control input-xs absent" value="0"></td>';

								$n = 1;
								if ($q_subject->num_rows() > 0) {
									foreach ($q_subject->result() as $r_) {
										$sql = "SELECT
														m.total_score
													FROM
														sch_subject_score_monthly_kgp AS m
													WHERE 1 = 1 {$where_} AND m.student_id = '".$rowStu->studentid."' AND m.subject_id = '".$r_->subjectid."'
												";

										$qTotalScore = $this->db->query($sql);
										$total_score = 0;
										if ($qTotalScore->num_rows() > 0) {
											$total_score = $qTotalScore->row()->total_score;
										}

										$tbody .= '<td><input type="text" name="'.$r_->subjectid.'[]" class="form-control input-xs '.$r_->subjectid.' subject_id" value="'.$total_score.'" attr-subject_id="'.$r_->subjectid.'" attr-subject_type_id="'.$r_->subj_type_id.'" readonly></td>'.$r_->subject.'</td>';
									}
								}

							$comment = $this->db->query("SELECT
																m.comments
															FROM
																sch_subject_score_monthly_kgp AS m
															WHERE 1 = 1 {$where_} AND m.student_id = '".$rowStu->studentid."' ")->row()->comments;

							$tbody .= '<td><input type="text" name="comments" class="form-control input-xs comments" value="'.$comment.'" placeholder="Comment..." style="text-align: left;min-width: 100px;" readonly></td>
								</tr>';
						}
					}// tbody =======


				}// if =======				

			}
		}// gr. subjecct =====		

		$arr = array('thead' => $thead, 'tr' => $tbody);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}


	public function save() {
		$teacher_id = $this->input->post('teacher_id');
		$school_id = $this->input->post('school_id');
		$program_id = $this->input->post('program_id');		
		$school_level_id = $this->input->post('school_level_id');
		$academic_year_id = $this->input->post('academic_year_id');
		$grade_level_id = $this->input->post('grade_level_id');			
		$class_id = $this->input->post('class_id');

		$exam_type = $this->input->post('exam_type');

		if (!empty($exam_type)) {
			foreach ($exam_type as $r) {
				print_r($r);
			}
		}
		

		// print_r($exam_type);
		exit();

		$arr = $this->input->post('arr');

		// transaction =====
    	$this->db->trans_begin();		

			if (!empty($arr)) {
				foreach ($arr as $r) {
					if (!empty($r)) {
						foreach ($r as $r_) {
							$data = array(
								'student_id' => $r_['student_id'],
								'comments' => $r_['comments'],
								'teacher_id' => $teacher_id,
								'school_id' => $school_id,
								'program_id' => $program_id,							
								'school_level_id' => $school_level_id,
								'adcademic_year_id' => $academic_year_id,
								'grade_level_id' => $grade_level_id,
								'class_id' => $class_id,
															
								'subject_type_id' => $r_['subject_type_id'],
								'subject_id' => $r_['subject_id'],
								'score' => $r_['score']
							);

							$this->db->insert('sch_subject_score_monthly_detail_kgp_', $data);
						}
					}				
				}
			}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}
		else{
			$this->db->trans_commit();
			$i = 1;
		}	

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($i);	
	}


}