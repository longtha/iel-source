<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		$this->lang->load('student', 'english');
		$this->load->model("student/ModPermission","stdpermis");
		$this->load->library('pagination');	
		$this->thead=array("No"=>'no',
							 "StudentID"=>'s__student_num',
							 "Name"=>'s__fullname',
							 "Class"=>'s__class_name',
							 "Request Date"=>'stdp__date',
							 "From Date"=>'stdp__from_date',							 
							 "To Date"=>'stdp__to_date',
							 "Approve By"=>'emp__last_name',
							 "Reason"=>'stdp__reason',
							 "Action"=>'Action'							 	
							);

		// ------------------------------------ start long tha working---------------------------------------
		$this->theadn=array("No"=>'no',
							 "StudentID"=>'studentid',
							 "Name in English"=>'nameen',
							 "Name in Khmer"=>'namekh',
							 "Class"=>'classid',
							 "Year"=>'yearid',							 
							 "Action"=>'Action'							 	
							);
		// ------------------------------------ end long tha working---------------------------------------

		$this->idfield="studentid";
	}
	function index(){
		$data['tdata']=$this->stdpermis->getstdpermis();
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Student List";			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/permission/permission_list', $data);
		$this->parser->parse('footer', $data);
	}
	function add(){
		$data['page_header']="New Student";			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/permission/permission_form', $data);
		$this->parser->parse('footer', $data);
	}
	function edit($permisid){
		$data['page_header']="New Student";	
		$data1['row']=$this->stdpermis->getstdpermisrow($permisid);		
		$this->parser->parse('header', $data);
		$this->load->view('student/permission/permission_form', $data1);
		$this->parser->parse('footer', $data);
	}
	function preview($permisid){
		$data['page_header']="New Student";	
		$data1['eva']=$this->stdpermis->getstdpermisrow($permisid);		
		$this->parser->parse('header', $data);
		$this->load->view('student/permission/preview', $data1);
		$this->parser->parse('footer', $data);
	}
	function fillstudent($classid){
		$yearid=$this->session->userdata('year');
		$key=$_GET['term'];
		$data=$this->db->select('*')
				->from('v_student_profile')
				->where('classid',$classid)
				->where('yearid',$yearid)
				->like('fullname',$key)->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name.' '.$row->first_name,'id'=>$row->studentid);
		}
	     echo json_encode($array);
	}
	function fillemp(){
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_emp_profile')	;
		$this->db->like('last_name',$key);
		$this->db->or_like('first_name',$key);			
		$data=$this->db->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name.' '.$row->first_name,'id'=>$row->empid);
		}
	     echo json_encode($array);
	}
	function save(){
		$m='';
		$p='';
		if(isset($_GET['m'])){
		    $m=$_GET['m'];
		}
		if(isset($_GET['p'])){
		    $p=$_GET['p'];
		}
		$permisid=$this->input->post('permisid');
		$this->stdpermis->save($permisid);
		redirect("student/permission?m=$m&p=$p");
	}
	function delete($permisid){
		$m='';
		$p='';
		if(isset($_GET['m'])){
		    $m=$_GET['m'];
		}
		if(isset($_GET['p'])){
		    $p=$_GET['p'];
		}
		$this->db->where('permisid',$permisid)->delete('sch_student_permission');
		redirect("student/permission?m=$m&p=$p");
	}
	function search(){
		if(isset($_GET['n'])){
			$student_num=$_GET['num'];
			$s_name=$_GET['n'];
			$classid=$_GET['c'];
			$schlevelid=$_GET['l'];
			$emp_name=$_GET['en'];
			$m=$_GET['m'];
			$p=$_GET['p'];
			$sort_num=$_GET['s_num'];
			$data['tdata']=$this->stdpermis->search($student_num,$s_name,$classid,$schlevelid,$emp_name,$sort_num,$m,$p);
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Student List";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/permission/permission_list', $data);
			$this->parser->parse('footer', $data);
		}else{
			$student_num=$this->input->post('student_num');
			$s_name=$this->input->post('s_name');
			$classid=$this->input->post('classid');
			$schlevelid=$this->input->post('schlevelid');
			$emp_name=$this->input->post('s_emp');
			$sort=$this->input->post('sort');
			$sort=str_replace('__', '.', $sort);
			$sort_num=$this->input->post('sort_num');
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			

			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
		    $i=1;

		    foreach ($this->stdpermis->search($student_num,$s_name,$classid,$schlevelid,$emp_name,$sort_num,$m,$p,$sort) as $row) {
		    	echo "<tr>
						<td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
						<td class='s__student_num'><a target='_blank' href='".site_url("/student/student/preview/$row->studentid?yearid=".$row->yearid)."'>".$row->student_num."</a></td>
						<td class='s__fullname'>".$row->fullname."</td>
						<td class='s__class_name'>".$row->class_name."</td>
						<td class='stdp__date'>".$this->green->convertSQLDate($row->date)."</td>
						<td class='stdp__from_date'>".$this->green->convertSQLDate($row->from_date)."</td>
						<td class='stdp__to_date'>".$this->green->convertSQLDate($row->to_date)."</td>
						<td class='emp__last_name'>".$row->last_name." ".$row->first_name."</td>
						<td class='stdp__reason'>".$row->reason."</td>
						<td class='remove_tag no_wrap'>";
						if($this->green->gAction("P")){	
							echo "<a><img rel=".$row->permisid." onclick='previews(event);' src='".site_url('../assets/images/icons/preview.png')."'/></a> "; 
						}
						if($this->green->gAction("D")){	
							echo "<a><img rel=".$row->permisid." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/></a>";
						}
						if($this->green->gAction("U")){	
							echo "<a><img  rel=".$row->permisid." onclick='update(event);' src='".site_url('../assets/images/icons/edit.png')."'/></a>";
						}
					echo "</td>
						 </tr> ";	
					 $i++;	
		    }
				echo "<tr class='remove_tag'>
					<td colspan='12' id='pgt'>
						<div style='margin-top:20px; width:10%; float:left;'>
						Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
										
										$num=50;
										for($i=0;$i<10;$i++){?>
											<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
											<?php $num+=50;
										} ?>
										<option value='all' <?php if($sort_num=='all') echo 'selected'; ?>>All</option>
									<?php echo "</select>
						</div>
						<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
							<ul class='pagination' style='text-align:center'>
							 ".$this->pagination->create_links()."
							</ul>
						</div>

					</td>
				</tr> ";
		}
	}

	// ------------------------------------ start long tha working--------------------------------------
	function mothlyPerm(){
		$data['theadn']=$this->theadn;
		$data['page_header']="Student Attendance List";	
		$yearid=isset($_GET['y'])?$_GET['y']:$this->session->userdata('year');
		$month=isset($_GET['month'])?$_GET['month']:date('m');
		$num_perm=isset($_GET['n'])?$_GET['n']:'3';
		$data1['n']=$num_perm;
		$data1['tdata']=$this->stdpermis->getMothlyPerm($yearid,$month,$num_perm);		
		$this->parser->parse('header', $data);
		$this->load->view('student/permission/monthly_perm_list', $data1);
		$this->parser->parse('footer', $data);	
	}
	function yearlyPerm(){
		$data['theadn']=$this->theadn;
		$data['page_header']="Student Attendance List";	
		$yearid=isset($_GET['y'])?$_GET['y']:$this->session->userdata('year');
		$num_perm=isset($_GET['n'])?$_GET['n']:'5';
		$data1['n']=$num_perm;
		$data1['tdata']=$this->stdpermis->getYearlyPerm($yearid,$num_perm);		
		$this->parser->parse('header', $data);
		$this->load->view('student/permission/monthly_perm_list', $data1);
		$this->parser->parse('footer', $data);	
	}
	function mothlyPermOver2(){
		$data['theadn']=$this->theadn;
		$data['page_header']="Student Attendance List";	
		$yearid=isset($_GET['y'])?$_GET['y']:$this->session->userdata('year');
		$month=isset($_GET['month'])?$_GET['month']:date('m');
		$num_perm=isset($_GET['n'])?$_GET['n']:'2';
		$data1['n']=$num_perm;
		$data1['tdata']=$this->stdpermis->getMothlyPermOver2($yearid,$month,$num_perm);		
		$this->parser->parse('header', $data);
		$this->load->view('student/permission/monthly_perm_list', $data1);
		$this->parser->parse('footer', $data);	
	}
	// ------------------------------------ end long tha working---------------------------------------

}