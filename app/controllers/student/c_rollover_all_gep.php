<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_rollover_all_gep extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("student/mrollover_all_gep","roall");
	}
	public function index(){
		$this->load->view('header');
		$this->load->view("student/rollover/rollover_form_all_gep");
		$this->load->view('footer');
	}
	public function show_school_level(){
		$opt_schlevel = $this->roall->schlevel();
		echo $opt_schlevel;
	}
	public function show_program(){
		$opt_pro = $this->roall->program();
		echo $opt_pro;
	}
	public function show_school_year(){
		$opt_year = $this->roall->schyear();
		echo $opt_year;
	}
	public function show_sch_ranglevel(){
		$opt_ranglevel = $this->roall->ranglevel();
		echo $opt_ranglevel;
	}
	public function show_school_grade(){
		$opt_grand = $this->roall->grandlevel();
		echo $opt_grand;
	}
	public function show_school_class(){
		$opt_class = $this->roall->classname();
		echo $opt_class;
	}
	public function show_student(){
		header("Content-type/text/x-json");
		$show_list = $this->roall->mshow_student();
		echo json_encode($show_list);
	}
	public function save_data(){

		$arr_inf_old = $this->input->post("arr_inf_old");
		$arr_inf_new = $this->input->post("arr_inf_new");
		$arr_student = $this->input->post("arr_student");
		$par         = $this->input->post("par");
		$user        = $this->session->userdata('user_name');
		$toschoolid   = "";
		$toprogram    = "";
		$toyearid     = "";
		$toranglevel  = "";
		$togradelevel = "";
		$toclassid    = "";
		$toschlevelid = "";
		if(count($arr_inf_new) > 0){
			$toschoolid   = $arr_inf_new['toschoolid'];
			$toprogram  = $arr_inf_new['toprogram'];
			$toyearid     = $arr_inf_new['toyearid'];
			$toclassid    = $arr_inf_new['toclassid'];
			$toschlevelid = $arr_inf_new['toschoollevel'];
			$toranglevel  = $arr_inf_new['toranglevel'];
			$togradelevel = $arr_inf_new['togradelavel'];
		}
		$schoolid   = "";
		$program    = "";
		$yearid     = "";
		$ranglevel  = "";
		$gradelevel = "";
		$classid    = "";
		$schlevelid = "";
		if(count($arr_inf_old) > 0){
			$schoolid   = $arr_inf_old['schoolid'];
			$program    = $arr_inf_old['program'];
			$yearid     = $arr_inf_old['yearid'];
			$classid    = $arr_inf_old['classid'];
			$schlevelid = $arr_inf_old['schoollevel'];
			$ranglevel  = $arr_inf_old['ranglevel'];
			$gradelevel = $arr_inf_old['gradelavel'];
		}
		if($par == 0){
			$sql_enrol = $this->db->query("SELECT * FROM sch_student_enrollment
										WHERE 1=1
										AND schoolid='".$toschoolid."'
										AND year = '".$toyearid."'
										AND classid = '".$toclassid."'
										AND grade_levelid = '".$togradelevel."'
										AND rangelevelid = '".$toranglevel."'
										AND schlevelid = '".$toschlevelid."'
										AND programid = '".$toprogram."'
									");
			$arr_alert = array();
			if($sql_enrol->num_rows() > 0){
				$arr_alert[0] = 1;
			}else{
				$arr_alert[0] = 2;
			}
		}else{
			if(count($arr_student) > 0){
				foreach($arr_student AS $row_st){
						$data_insert = array("studentid"=>$row_st,
											"schoolid"=>$schoolid,
											"programid"=>$program,
											"schlevelid"=>$schlevelid,
											"yearid"=>$yearid,
											"rangelevelid"=>$ranglevel,
											"gradlevelid"=>$gradelevel,
											"classid"=>$classid,
											"create_date"=>date('Y-m-d H:i:s'),
											"create_by"=>$this->session->userdata('userid'),
											"modify_date"=>date('Y-m-d H:i:s')
										);
					$this->db->insert("sch_student_rollover_gep_history",$data_insert);
					$data = array("studentid"=>$row_st,
									"schoolid"=>$toschoolid,
									"year"=>$toyearid,
									"classid"=>$toclassid,
									"grade_levelid"=>$togradelevel,
									"rangelevelid"=>$toranglevel,
									"schlevelid"=>$toschlevelid,
									"programid"=>$toprogram,
									"enroll_date"=>date('Y-m-d'),
									"rollover_date"=>date('Y-m-d'),
									"rollover_by"=>$user,
									"is_rollover"=>100);

					$this->db->where("studentid",$row_st);
					$this->db->where("schoolid",$schoolid);
					$this->db->where("year",$yearid);
					$this->db->where("classid",$classid);
					$this->db->where("grade_levelid",$gradelevel);
					$this->db->where("rangelevelid",$ranglevel);
					$this->db->where("schlevelid",$schlevelid);
					$this->db->where("programid",$program);
					$this->db->update("sch_student_enrollment",$data);
				}
			}
			$arr_alert[0] = 3;
		}
		header("Content-type/text/x-json");
		echo json_encode($arr_alert);
		
	}
	function delete_student_from_class(){
		$arr_obj = $this->input->post("arr_data");
		$msg = "";
		if(count($arr_obj['inf']) > 0){
			$schoolid   = $arr_obj['inf']['schoolid'];
			$program    = $arr_obj['inf']['program'];
			$yearid     = $arr_obj['inf']['yearid'];
			$classid    = $arr_obj['inf']['classid'];
			$schlevelid = $arr_obj['inf']['schoollevel'];
			$gradelevel = $arr_obj['inf']['gradelavel'];
			$ranglevel  = $arr_obj['inf']['ranglevel'];
			// $data_del = array("schoolid"=>$schoolid ,
			// 					"year"=>$yearid,
			// 					"classid"=>$classid,
			// 					"grade_levelid"=>$gradelevel,
			// 					"schlevelid"=>$schlevelid,
			// 					"programid"=>$program,
			// 					"rangelevelid"=>$ranglevel
			// 				);
			
			if(count($arr_obj['studentid']) > 0){
				foreach($arr_obj['studentid'] as $kk=>$val){
					$data_del = array(
										"studentid"=>$val,
										"schoolid"=>$schoolid,
										"year"=>$yearid,
										"classid"=>$classid,
										"grade_levelid"=>$gradelevel,
										"schlevelid"=>$schlevelid,
										"programid"=>$program,
										"rangelevelid"=>$ranglevel
									);
					$delete    = $this->db->delete("sch_student_enrollment",$data_del);
					$data_save = array(
										"student_id"=>$val,
										"schoolid"=>$schoolid,
										"programid"=>$program,
										"schlevelid"=>$schlevelid,
										"yearid"=>$yearid,
										"rangelevelid"=>$ranglevel,
										"gradeid"=>$gradelevel,
										"classid"=>$classid,
										"createdate"=>date('Y-m-d H:i:s'),
										"userid"=>$this->session->userdata('userid'),
										"username"=>$this->session->userdata('user_name')
										);
					$this->db->insert("sch_delete_student_rollover_history",$data_save);
					$msg = "You deleted success.";
				}
			}
			
		}
		echo $msg;		
	}
}