<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Student extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		$this->lang->load('student', 'english');
		$this->load->model("student/ModStudent","std");	
		$this->load->model("setting/usermodel","user");	
		$this->thead=array("No"=>'No',
							 "StudentID"=>'student_num',
							 "Full Name"=>'first_name',
							 "Full Name(Kh)"=>'first_name_kh',
							 "Date of Birth"=>'dobs',
							 "Class"=>'class',
							 "Nationality"=>'Nationality',
							 "Action"=>'Action'							 	
							);
		$this->idfield="studentid";
		
	}
	
	function index()
	{
		
		$page=0;
		if(isset($_GET['per_page'])) $page=$_GET['per_page'];
			
		$this->load->library('pagination');	
		$config['base_url'] = site_url().'/student/student?pg=1';		
		$config['total_rows'] = $this->green->getTotalRow("SELECT * FROM sch_student");
		$config['per_page'] =50;
		//$config['use_page_numbers'] = TRUE;
		$config['num_link']=5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';		
		
		$this->pagination->initialize($config);	
		$limi=" limit ".$config['per_page'];
		if($page>0){
			$limi=" limit ".$page.",".$config['per_page'];
		}		
		$sql_page="SELECT s.studentid,s.student_num,s.last_name,s.first_name,s.last_name_kh,s.first_name_kh,DATE_FORMAT(s.dob,'%d/%m/%Y') as dob,s.nationality,c.class_name,c.classid FROM sch_student s
					left JOIN sch_class c
					ON(s.classid=c.classid)
					where s.is_active=1
					order by studentid desc {$limi}";
		$data['tdata']=$this->green->getTable($sql_page);
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Student List";			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/student_list', $data);
		$this->parser->parse('footer', $data);
	}
	function import($result=0){
		$data['page_header']="Import Student Profile";		
		$data['import_status']=($result==1?"Student profile was imported.":"No student profile to import.");			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/import_profile', $data);
		$this->parser->parse('footer', $data);
	}
	function importenroll($result=0){
		$data['page_header']="Import Student Profile";		
		$data['import_status']=($result==1?"Enrollment was imported.":"No Enrollment to import.");			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/import_enrollment', $data);
		$this->parser->parse('footer', $data);
	}
	function importProfile(){
		$result=$this->std->saveImport();
		$this->import($result);		
	}
	function importenrollment(){
		$result=$this->std->saveImportenroll();
		$this->importenroll($result);		
	}
	function add(){
		$data['page_header']="New Student";			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/student_form', $data);
		$this->parser->parse('footer', $data);
	}
	function preview($studentid){
		$data['page_header']="New Student";
		//$data1['page_header']=array("New Student");			
		$data1['student']=$this->std->previewstd($studentid);
		$data1['father']=$this->std->getfather($studentid);	
		$data1['mother']=$this->std->getmother($studentid);		
		$this->parser->parse('header', $data);
		$this->load->view('student/preview', $data1);
		$this->parser->parse('footer', $data);
	}
	function delete($studentid){
		$this->db->set('is_active',0);
		$this->db->where('studentid',$studentid);
		$this->db->update('sch_student');
		$login_username=$this->std->getstudentrow($studentid)->login_username;
		$this->db->set('is_active',0);
		$this->db->where('user_name',$login_username);
		$this->db->update('sch_user');
		redirect('student/student/');
	}
	function filllink(){
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_student')	;
		$this->db->like('last_name',$key);			
		$data=$this->db->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name,'id'=>$row->studentid);
		}
	     echo json_encode($array);
	}
	function edit($studentid){
		$data['page_header']="New Student";
		//$data1['page_header']=array("New Student");			
		$datas['student']=$this->std->getstudentrow($studentid);
		$this->parser->parse('header', $data);
		$this->parser->parse('student/student_edit', $datas);
		$this->parser->parse('footer', $data);
	}
	public function fillrespon()
	{
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_student_responsible')	;
		$this->db->like('last_name',$key);			
		$data=$this->db->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name,'id'=>$row->respondid);
		}
	     echo json_encode($array);
	}
	public function fillmember()
	{
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_student_member')	;
		$this->db->like('last_name',$key);			
		$data=$this->db->get()->result();//$this->green->getTable($sql_page);
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name,'id'=>$row->memid);
		}
	     echo json_encode($array);
		//$this->load->view('footer');
	}
	function getresponrow(){
		$respondid=$this->input->post('r_id');
		$this->db->where('respondid',$respondid);
		$row=$this->db->get('sch_student_responsible')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function getmemberrow(){
		$memid=$this->input->post('r_id');
		$this->db->where('memid',$memid);
		$row=$this->db->get('sch_student_member')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function getstudentrow(){
		$std_id=$this->input->post('std_id');
		$this->db->select('s.studentid,s.last_name,s.first_name,s.last_name_kh,c.class_name');
		$this->db->from('sch_student s');
		$this->db->join('sch_class c','s.classid=c.classid','left');
		$this->db->where('s.studentid',$std_id);
		$s_row=$this->db->get()->row();
		header("Content-type:text/x-json");
		echo json_encode($s_row);
	}
	function validateuser(){
		$count=$this->std->getvalidateuser($this->input->post('username'));
		echo $count;
	}
	function getstdbyid(){
			$std_num=$this->input->post('std_num');
			$this->db->select('count(*)');
			$this->db->from('sch_student');
			$this->db->where('student_num',$std_num);
			echo $this->db->count_all_results();
		}
	function save(){
		
		$f_name=$this->input->post("first_name");
		$l_name=$this->input->post("last_name");
		$dob=$this->input->post("dob");
		$studentid=$this->input->post("s_id");
		if($studentid=='')
			$count=$this->std->validatestudent($f_name,$l_name,$dob);
		else
			$count=$this->std->validatestudentupdate($f_name,$l_name,$dob,$studentid);
		if($count>0){

			$data['error']="<div style='text-align:center; color:red;'>This Student Have been Register Before Please check again...!</div>";
			$data['page_header']="New Student";			
			$this->parser->parse('header', $data);
			if($studentid==''){
				$this->parser->parse('student/student_form', $data);

			}else{
				$data['student']=$this->std->getstudentrow($studentid);
				$this->parser->parse('student/student_edit', $data);
			}

			$this->parser->parse('footer', $data);
			
		}else{
			$save_result=$this->std->save($studentid);
			$this->do_upload($save_result);
			redirect('student/student/');
		}
			
	}
	function do_upload($student_id)
		{
			$config['upload_path'] ='./assets/upload/students';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '5000';
			$config['file_name']  = "$student_id.png";
			$config['overwrite']=true;
			$config['file_type']='image/png';
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('file'))
			{
				$error = array('error' => $this->upload->display_errors());			
			}
			else
			{				
				//$data = array('upload_data' => $this->upload->data());			
				//redirect('setting/user');
				 	$config2['image_library'] = 'gd2';
                    $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                    $config2['new_image'] = './assets/upload/students';
                    $config2['maintain_ratio'] = TRUE;
                    $config2['create_thumb'] = TRUE;
                    $config2['thumb_marker'] = '_thumb';
                    $config2['width'] = 120;
                    $config2['height'] = 180;
                    $config2['overwrite']=true;
                    $this->load->library('image_lib',$config2); 

                    if ( !$this->image_lib->resize()){
	                	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
					}else{
						unlink('./assets/upload/students/'.$student_id.'.png');
					}
			}
		}
	function search(){
		if(isset($_GET['s_id'])){
			$studentid=$_GET['s_id'];
			$full_name=$_GET['fn'];
			$full_name_kh=$_GET['fnk'];
			$classid=$_GET['class'];
			$sort_num=$_GET['s_num'];
			$sort=$this->input->post('sort');
			$data['tdata']=$query=$this->std->searchstudent($studentid,$full_name,$full_name_kh,$classid,$sort,$sort_num);
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Student List";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/student_list', $data);
			$this->parser->parse('footer', $data);
		}
		if(!isset($_GET['s_id'])){
			$studentid=$this->input->post('studentid');
			$full_name=$this->input->post('full_name');
			$full_name_kh=$this->input->post('full_name_kh');
			$classid=$this->input->post('classid');
			$sort=$this->input->post('sort');
			$sort_num=$this->input->post('sort_num');
			$query=$this->std->searchstudent($studentid,$full_name,$full_name_kh,$classid,$sort,$sort_num);
				 $i=1;
				foreach($query as $row){																
									echo "<tr>
										<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
										 <td>".$row['student_num']."</td>
										 <td>".$row['first_name']." ".$row['last_name']."</td>
										 <td>".$row['first_name_kh']." ".$row['last_name_kh']."</td>
										 <td>".$row['dob']."</td>
										 <td>".$row['class_name']."</td>
										 <td>".$row['nationality']."</td>
										 <td><a><img rel=".$row['studentid']." onclick='previewstudent(event);' src='".site_url('../assets/images/icons/preview.png')."'/></a>   <a><img rel=".$row['studentid']." onclick='deletestudent(event);' src='".site_url('../assets/images/icons/delete.png')."'/></a> <a><img  rel=".$row['studentid']." onclick='updateuser(event);' src='".site_url('../assets/images/icons/edit.png')."'/></a></td>
										 </tr>
										 ";										 
									$i++;	 
								}
							echo "<tr>
										<td colspan='12' id='pgt'>
											<div style='margin-top:20px; width:10%; float:left;'>
											Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
															
															$num=50;
															for($i=0;$i<10;$i++){?>
																<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
																<?php $num+=50;
															}
															
														echo "</select>
											</div>
											<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
												<ul class='pagination' style='text-align:center'>
												 ".$this->pagination->create_links()."
												</ul>
											</div>

										</td>
									</tr> ";
		}
	}

	
}
