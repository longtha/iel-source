<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_student_attendent extends CI_Controller {  

    public function __construct(){
        parent::__construct();
        $this->load->model('student/m_student_attendent', 'attendent');
        $this->thead = array("No" => 'no',
                                "Student" => 'student',
                                "Program" => 'programid',
                                "School Level" => 'schlevelid',
                                "Yare"  => 'sch_year',
                                "Class" => 'classid',
                                "From Date" => 'from_date',
                                "To Date"  => 'to_date',
                                "Approve by" => 'Approveby',
                                "Reason" => 'reason',
                                "Action"=> 'Action'
                                );
            $this->idfield = "studentid";
    }

    public function index(){
        $data['thead'] = $this->thead;  
        $this->load->view('header', $data);
        $this->load->view('student/v_student_attendent', $data);
        $this->load->view('footer', $data); 
    }

    // get_schlevel ---------------------------------------
    public function get_schlevel(){
        $programid = $this->input->post('programid');        
        $get_schlevel = $this->attendent->get_schlevel($programid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel;
    }

    // get_year -------------------------------------------
    public function get_year(){
        $schlevelids = $this->input->post('schlevelids');
        $get_year = $this->attendent->get_year($schlevelids);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    // get_class ------------------------------------------
    public function get_class(){
        $schlevelids = $this->input->post('schlevelids');
        $get_class = $this->attendent->get_class($schlevelids);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_class;
    }
    // form -----------------------------------------------
    // get_schlevel ---------------------------------------
    public function get_school(){
        $program = $this->input->post('program');        
        $get_school = $this->attendent->get_school($program);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_school;
    }

    // get_year -------------------------------------------
    public function get_yearid(){
        $school = $this->input->post('school');
        $get_yearid = $this->attendent->get_yearid($school);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_yearid;
    }

    // get_class ------------------------------------------
    public function get_toclass(){
        $school = $this->input->post('school');
        $get_toclass = $this->attendent->get_toclass($school);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_toclass;
    }
    // save ----------------------------------------------
    function save(){
		$permisid = $this->input->post('permisid');
		$sql = $this->attendent->save($permisid);
		echo json_encode($sql);
        die();
	}
    // aoutocomplete --------------------------------------
	public function get_student(){
		$term = trim($_REQUEST['term']);
		$classid = trim($_REQUEST['classid']);
		//$studentid = trim($_REQUEST['studentid']);
		$w = "";
		$w1 = "";
		if($term != ""){
			$w .= "AND CONCAT(first_name, ' ', last_name) LIKE '%".sqlStr($term)."%' ";    
		}		
		if($classid != ""){
			$w1 .= "AND s.classid = '{$classid}' ";     
		}

		$sql = "SELECT
					s.studentid,
					CONCAT(s.last_name,'  ',s.first_name) AS fullname,
	 				s.classid
				FROM
					v_student_profile AS s
				WHERE 1=1 {$w} {$w1}
				LIMIT 0, 15 ";

		$qr = $this->db->query($sql);

		$arr = array();
		if($qr->num_rows() > 0){
			foreach ($qr->result() as $row) {
				$arr[] = array('studentid' => $row->studentid, 'stu_name' => $row->fullname);
			}
		}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
	}

	// aoutocomplete hr  --------------------------------
	public function get_respond(){
		$term = trim($_REQUEST['term']);
		//$classid = trim($_REQUEST['classid']);
		$hr_respond = trim($_REQUEST['hr_respond']);
		$w = "";
		$w1 = "";
		if($term != ""){
			$w .= "AND CONCAT(last_name,'  ',first_name) LIKE '%".sqlStr($term)."%' ";    
		}		
		
		$sq = "SELECT
					sch_emp_profile.empid,
					CONCAT(last_name,'  ',first_name) AS fullname
				FROM
					sch_emp_profile
				WHERE 1=1 {$w} {$w1}
				LIMIT 0, 15 ";

		$qr = $this->db->query($sq);

		$arr = array();
		if($qr->num_rows() > 0){
			foreach ($qr->result() as $row) {
				$arr[] = array('empid' => $row->empid, 'stu_name' => $row->fullname);
			}
		}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
	}

	// delete ----------------------------------------
    public function delete(){    
        $permisid = $this->input->post('permisid');  - 0;
        $permis = $this->attendent->delete($permisid);
        header('Content-Type: application/json; charset=utf-8');
        echo $permis;        
    }
    // edit ----------------------------------------
    public function editdata(){ 
        $permisid = $this->input->post('permisid') - 0;
        $row = $this->attendent->edit($permisid);
        header('Content-Type: application/json; charset=utf-8');
        echo $row;      
    }
	// Getdata -------------------------------------
    public function Getdata(){
            $m = $this->input->post('m');
            $p = $this->input->post('p');
            $this->green->setActiveRole($this->session->userdata('roleid'));        
            $this->green->setActiveModule($m);      
            $this->green->setActivePage($p);
            $total_display =$this->input->post('sort_num');
            $total_display = $this->input->post('total_display');
            $sortby=$this->input->post("sortby");
            $sorttype=$this->input->post("sorttype");
            $student=$this->input->post("student");
            $approveby=$this->input->post("approveby");
            $program=$this->input->post("program");
            $school=$this->input->post("school");
            $Yare_id=$this->input->post("Yare_id");
            $classname=$this->input->post("classname");
            $from_date=$this->input->post("from_date");
            $to_date=$this->input->post("to_date");
            $where='';
            $sortstr="";
                if($student!= ""){
                    $where .= "AND  CONCAT(first_name_, ' ', last_name_) LIKE '%".$student."%' ";
                }
                if($approveby!= ""){
                    $where .= "AND  CONCAT(first_name, ' ', last_name) LIKE '%".$approveby."%' ";
                }
                if($program!= ""){
                    $where .= "AND programid = '".$program."' ";
                }
                if($school!= ""){
                    $where .= "AND schlevelid = '".$school."' ";
                }
                if($Yare_id!= ""){
                    $where .= "AND yearid = '".$Yare_id."' ";
                }
                if($classname!= ""){
                    $where .= "AND classid = '".$classname."' ";
                }
                if($from_date != ''){
                 $where .= "AND from_date >= '".$this->green->formatSQLDate($from_date)."'";    
                }
                if($to_date != ''){
                     $where .= "AND from_date <= '".$this->green->formatSQLDate($to_date)."'";     
                }
                (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

            $sqr = " SELECT
						atd.permisid,
						atd.schlevelid,
						atd.created_by,
						CONCAT(last_name_,'  ',first_name_) AS student,
						atd.studentid,
						CONCAT(last_name,'  ',first_name) AS Approveby,
						atd.classid,
						atd.programid,
						atd.created_date,
						atd.permis_status,
						atd.reason,
						atd.approve_by,
						DATE_FORMAT(from_date, '%d/%m/%Y') AS from_date,
						DATE_FORMAT(to_date, '%d/%m/%Y') AS to_date,
						atd.yearid,
						DATE_FORMAT(dateby, '%d/%m/%Y') AS dateby,
						atd.program,
						atd.sch_level,
						atd.sch_year,
						atd.class_name
					FROM
						v_student_attendent AS atd
                    WHERE 1=1 {$where}";

            $total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_student_attendent WHERE 1=1 {$where}")->row()->numrow;

            $getperpage=0;
            if($total_display==''){
                $getperpage=50;
            }else{
                $getperpage=$total_display;
            }
            $paging = $this->green->ajax_pagination($total_row,site_url()."student/c_student_attendent",$getperpage,"icon");
            if($sortstr!=""){
                $sqr.=$sortstr;
            }
            $getlimit=50;
            if($paging['limit']!=''){
                $getlimit=$paging['limit'];
            }
            $data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
                $j='';
                $table='';
                if(count($data) > 0){
                    foreach($data as $row){  
                        $j++;
                        $table.= "<tr>
                                    <td class='no'>".$j."</td>
                                    <td >".$row->student."</td>
                                    <td >".$row->program."</td>
                                    <td >".$row->sch_level."</td>
                                    <td >".$row->sch_year."</td>
                                    <td >".$row->class_name."</td>
                                    <td align='center'>".$row->from_date."</td>
                                    <td align='center'>".$row->to_date."</td>
                                    <td >".$row->Approveby."</td>
                                    <td style='width:15px;'>".$row->reason."</td>
                                    <td class='remove_tag no_wrap' align='center'>";
                            if($this->green->gAction("E")){ 
                            $table.= " <a href='javascript:void(0);' class='edit btn btn-xs btn-success' data-permisid='".$row->permisid."' title='Edit'><span class='glyphicon glyphicon-pencil'></span></a>";
                            }
                            if($this->green->gAction("D")){ 
                            $table.= " <a href='javascript:;' class=' btn btn-xs btn-danger delete' data-permisid='".$row->permisid."' title='Delete'><span class='glyphicon glyphicon-trash'></span></a>";
                            }
                        $table.= " </td>
                                </tr>";
         
                    }

                }else{
                    
                    $table.= "<tr>
                                <td colspan='11' style='text-align:center;'> 
                                    We did not find anything to show here... 
                                </td>
                            </tr>";
                }

            $arr['data']=$table;
            header('Content-Type:text/x-json');
            $arr['body']=$table;
            $arr['pagination']=$paging;
            echo json_encode($arr); 
            die();
        }

}