<?PHP if(!defined('BASEPATH')) exit('No direct script access allowed.!');
class Stusponsor extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
        $this->load->database();
        $this->load->library('pagination');
		$this->load->model('student/modstusponsor','msp');
	}
	public function index(){
		$page_header['page_header']="Student List";
		$this->load->view('header',$page_header);
		$this->load->view('student/sponsor/stu_sponsor_list');
		$this->load->view('footer');
	}
	public function views(){
		$page_header['page_header']="Student Sponsor List";
		$data['stuinfo']= $this->msp->setSqlStu($this->input->get('stu'));
		$data['showschoolname']= $this->msp->schinfo();
		$this->load->view('header',$page_header);
		$this->load->view('student/sponsor/stu_sponsor_view',$data);
		$this->load->view('footer');
	}
	public function sponsorlist()
	{
		$variables = $this->input->post('variables');
		$query = $this->msp->setSqlSponsor($variables['stuid']);
		$tr="";
		$i=1;
		foreach($query as $row)
		{
			$tr.=$this->msp->listsponinfo($i,$row->sponsor_code,$row->sponsor_fullname,$row->position,$row->phone);
			$i++;
		}
		$arrSpo =array();
		header('Content-type:text/x-json');
		$arrSpo['tr']=$tr;
		echo json_encode($arrSpo);
		die();
	}
	public function stusponsor()
	{
		$variables  			= $this->input->post('variables');
		$yid 					= $variables['yid'];
		$stu_code 				= $variables['stu_code'];
		$stu_fullname 			= $variables['stu_fullname'];
		$sort_num 				= $variables['sort_num'];
		$stu_level 				= $variables['stu_level'];
		$stu_classid 			= $variables['stu_classid'];
		if($sort_num !="all")
			$sort_num = $sort_num;
		else
			$sort_num = $this->msp->GetTotalRow($yid,$variables['slid'],$stu_code,$stu_fullname,$stu_level,$stu_classid);

		if($variables['slid'] 	!="" || !empty($variables['slid']))
			$variables['slid'] 	=$variables['slid'];
		else
			$variables['slid'] 	="";

		$sql=$this->msp->setSqlStuSponsor($yid,$variables['slid'],$stu_code,$stu_fullname,$stu_level,$stu_classid);
		$total_row=$this->msp->GetTotalRow($yid,$variables['slid'],$stu_code,$stu_fullname,$stu_level,$stu_classid);
		$paging=$this->green->ajax_pagination($total_row,site_url('student/stusponsor?y={$_GET["y"]}&m={$_GET["m"]}&p={$_GET["p"]}'),$sort_num);
		$data=$this->green->getTable("$sql limit {$paging['start']}, {$paging['limit']}");
		$_tr ="";
		$i=1;
		if(count($data)>0){		
			foreach ($data as $row) {
				$_tr.=$this->msp->listbody($i,$row['student_num'],$row['fullname'],$row['grade_levelid'],$row['class_name'],$row['sch_year'],$row['studentid']);
				$i++;
			}
		}
		$arrJson=array();
		header("Content-type:text/x-json");
		$arrJson['tr']=$_tr;
		$arrJson['paging']=$paging;
		echo json_encode($arrJson);
		die();
	}
}
