<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_home_work extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        //$this->load->model('school/schoollevelmodel','sl');
        //$this->load->model('school/gradelevelmodel','gm');
        $this->load->model('student/m_home_work', 'hw');
        $this->load->helper(array('form', 'url'));
				$this->load->model('school/schoolinformodel','sch');
  }

	public function index()
	{
	    

	    
    $data['header']="Home Work";
    $this->load->view('header');

    $this->load->view('student/v_home_work', $data);
    $this->load->view('footer');
	}

  public function reload() {
   
      
		$parameters = array(
			'school_id' => $this->input->post("school"),
			'school_level_id' => $this->input->post("school_level"),
			'academic_year_id' => $this->input->post("academic_year"),
			'grade_level_id' => $this->input->post("grade_level"),
			'class_id' => $this->input->post("school_class"),
			'subject_id' => $this->input->post("subject")
		);
		$isParameter = $this->input->post("parameter");

    $homeworks = $this->hw->reload($parameters, $isParameter);

    header("Content-type:text/x-json");
    echo json_encode($homeworks);
    exit();
  }
  
  public function filter() {
       
          $parameters2=json_decode($_POST['data']);
          
          $homeworks2 = $this->hw->filter($parameters2);
          
          header("Content-type:text/x-json");
          echo json_encode($homeworks2);
      
  }

  public function save() {
  
      $d=0;
      $m=1;
      $y=2;
      
      $txt_deadline=$this->input->post("deadline");
      
      $deadlines=explode("-", $txt_deadline);
      $deadline = new DateTime($deadlines[$y]."-".$deadlines[$m]."-".$deadlines[$d]);
      //date("Y-m-d H:i:s")
    $homework_data = array (
      'school_id' => $this->input->post("school"),
      'school_level_id' => $this->input->post("school-level"),
      'academic_year_id' => $this->input->post("academic-year"),
      'grade_level_id' => $this->input->post("grade-level"),
      'class_id' => $this->input->post("class"),
      'subject_id' => $this->input->post("subject"),
      'title' => $this->input->post("title"),
      'description' => $this->input->post("note"),
      'attach_file' => 'no file',
      'transaction_date' => date("Y-m-d"),
      'type' => 43,
      'type_no' => $this->green->nextTran(43, "Student Home Work"),
      'created_by' => $this->session->userdata('userid'),
      'deadline' => $deadline->format('Y-m-d')
    );

    $upload_file = $this->do_upload("file");

    if (!array_key_exists("error", $upload_file)) {
			$file_type = explode('.', $upload_file["file_name"]);
            $homework_data["attach_file"] = $upload_file["file_name"];
			$homework_data["file_type"] = $file_type[1];

      $result = $this->hw->save($homework_data);

			$result_function = $result == 1 ? "window.parent.reload(1)" : "window.parent.error()";

			echo <<<SCRIPT
							<script type="text/javascript">
								$result_function;
							</script>
SCRIPT;

    }else{
        echo <<<SCRIPT
							<script type="text/javascript">
                                alert("Please check attachment!");
							</script>
SCRIPT;
			}

    exit();
  }
  
  
  public function update() {
      
      $d=0;
      $m=1;
      $y=2;
      
      $txt_deadline=$this->input->post("deadline_edit");
      
      $deadlines=explode("-", $txt_deadline);
      $deadline = new DateTime($deadlines[$y]."-".$deadlines[$m]."-".$deadlines[$d]);
      
      $homework_data = array (
          'id' => $this->input->post("id_edit"),
          'school_id' => $this->input->post("school_edit"),
          'school_level_id' => $this->input->post("school-level_edit"),
          'academic_year_id' => $this->input->post("academic-year_edit"),
          'grade_level_id' => $this->input->post("grade-level_edit"),
          'class_id' => $this->input->post("class_edit"),
          'subject_id' => $this->input->post("subject_edit"),
          'title' => $this->input->post("title_edit"),
          'description' => $this->input->post("note_edit"),
          'type' => 43,
          'type_no' => $this->green->nextTran(43, "Student Home Work"),
          'created_by' => $this->session->userdata('userid'),
          'deadline' => $deadline->format('Y-m-d')
      );
      try {
          $upload_file = $this->do_upload("file_edit");
     
          if (!array_key_exists("error", $upload_file)) {
              $file_type = explode('.', $upload_file["file_name"]);
              $homework_data["attach_file"] = $upload_file["file_name"];
              $homework_data["file_type"] = $file_type[1];
          }
      }catch(Exception $ex2){
          
      }
    
      try{
          
          $this->db->where('id', $homework_data["id"]);
          $this->db->update('sch_student_home_work',$homework_data);
          
          $result_function="window.parent.reload(1)";
      }catch (Exception $ex){
          $result_function="window.parent.error()";
      }
      echo <<<SCRIPT
							<script type="text/javascript">
                                $result_function   ;
							</script>
SCRIPT;
  }
  

	public function getEditData() {
		$id = $this->input->post("id");
		$homework = $this->hw->getEditData($id);


		echo json_encode($homework);
    exit();
	}

	public function delete() {
		$id = $this->input->post("id");

		$delete = $this->hw->delete($id);

		echo json_encode($delete);
	  exit();
	}

  public function getSubject() {
    $schoolLevel = $this->input->post("school_level");
    $programId = $this->input->post("program_id");
    //$subject = $this->hw->getSubject($schoolLevel, $programId);
    $subject = $this->hw->getSubject($schoolLevel,$programId);
    if($subject!=null && !empty($subject)){
        echo json_encode($subject);
    }else{
        echo json_encode(array());
    }
		
    exit();
  }

  public function do_upload($file) {

    $config['upload_path']   = './assets/upload/homeworks/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
    $config['max_size']      = 2048000;
    $config['max_width']     = 1024;
    $config['max_height']    = 768;

    $this->load->library('upload', $config);
    
    $upload_data = array();

    if (!$this->upload->do_upload($file)) {
      $error = array('error' => $this->upload->display_errors());
      $upload_data = $error;
			echo $error;
    }

    else {
      $upload_data = $this->upload->data();
    } 

    return $upload_data;
  }

	public function getSchoolLevel() {
		$schoolId = $this->input->post("school");

		$schoolLevels = $this->hw->getSchoolLevel($schoolId);

		echo json_encode($schoolLevels);
    exit();
	}
	function get_class() {
	    $school_level = $this->input->post("school_level");
	    $grade_level = $this->input->post("grade_level");
	    $class = $this->hw->callClass($school_level, $grade_level);
	    
	    echo json_encode($class);
	    exit();
	}
}
