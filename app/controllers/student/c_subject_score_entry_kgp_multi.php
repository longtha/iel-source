<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class C_subject_score_entry_kgp_multi extends CI_Controller{

	function __construct() {
        parent::__construct();
		$this->load->model('student/m_subject_score_entry_kgp', 'mode');
		$this->load->model('student/m_evaluate_trans_kgp', 'student');
		$this->load->model('school/schoolinformodel', 'school');
		$this->load->model('school/program', 'program');			
		$this->load->model('school/schoollevelmodel', 'school_level');
		$this->load->model('school/gradelevelmodel', 'grade_level');
		$this->load->model('school/schoolyearmodel', 'adcademic_year');
		$this->load->model('school/classmodel', 'class');                			
    }

    public function index()
    {
    	$this->load->view('header');
		$this->load->view('student/v_subject_score_entry_kgp_multi');
		$this->load->view('footer');
    }

    //get year and grade data----------------------------------
	public function get_year_and_grade_data()
	{
		$school_id = $this->input->post('school_id');
		$program_id = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		
		$opt = array();

		//get year
		$y_opt = array();
		$get_year_data = $this->adcademic_year->getschoolyear($school_id,$program_id,$school_level_id);
		if(count($get_year_data) > 0){
			foreach($get_year_data as $r_y){				
				$y_opt[] = $r_y;
			}
		}			

		//get grade
		$g_opt = array(); 
		$get_grade_data = $this->grade_level->getgradelevels($school_level_id);
		if(count($get_grade_data) > 0){
			foreach($get_grade_data as $r_g){				
				$g_opt[] = $r_g;
			}
		}			

		$opt["year"] = $y_opt;
		$opt["grade"] = $g_opt;

		header('Content-Type: application/json');
		echo json_encode($opt);
	}

	//get teacher data-------------------------------------------
	public function get_teacher_data()
	{
		$school_level_id = $this->input->post('school_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$grade_level_id = $this->input->post('grade_level_id');
		$t_opt = '<option value=""></option>';

		$get_teacher_data = $this->mode->getTeacherInfo("", $school_level_id, $adcademic_year_id, $grade_level_id);
		if(count($get_teacher_data) > 0){
			foreach($get_teacher_data as $r_t){
				$t_opt .= '<option value="'. $r_t->teacher_id .'">'. $r_t->first_name .'  '. $r_t->last_name .'</option>';
			}
		}
					
		echo $t_opt;
	}

	//get class data--------------------------------------------- 
	public function get_class_data()
	{
		$school_level_id = $this->input->post('school_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$grade_level_id = $this->input->post('grade_level_id');
		//$teacher_id = $this->input->post('teacher_id');
		$c_opt = '<option value=""></option>';
		$teacher_id = $this->session->userdata("userid");
		$get_class_data = $this->mode->getClassByTeacher($teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id);

		if($get_class_data->num_rows()> 0){
			foreach($get_class_data->result() as $r_c){
				$c_opt .= '<option value="'. $r_c->classid .'">'. $r_c->class_name .'</option>';
			}
		}
		echo $c_opt;
	}

	//get subject data-------------------------------------------
	public function get_subject_data()
	{
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id = $this->input->post('grade_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$teacher_id = $this->input->post('teacher_id');
		$class_id = $this->input->post('class_id');
		$su_opt = '<option value=""></option>';

		$get_subject_data = $this->mode->getSubjectByTeacher("", $class_id, $teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id);
		if(count($get_subject_data) > 0){
			foreach($get_subject_data as $r_su){
				$su_opt .= '<option value="'. $r_su->subjectid .'">'. $r_su->subject_name .'</option>';
			}
		}			
		
		echo $su_opt;
	}

	//get subject data-------------------------------------------
	public function get_group_main_id_subject()
	{
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id = $this->input->post('grade_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$teacher_id = $this->input->post('teacher_id');
		$class_id = $this->input->post('class_id');
		$subject_id = $this->input->post('subject_id');
		$opt_id = array();

		$get_id_data = $this->mode->getSubjectByTeacher($subject_id, $class_id, $teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id);
		if(count($get_id_data) > 0){
			foreach($get_id_data as $r_id){
				$opt_id[] = $r_id;
				// $opt_id['sg_id'] = $r_id->subj_type_id;
				// $opt_id['sm_id'] = $r_id->subj_main_id;
			}
		}			
		
		header('Content-Type: application/json');
		echo json_encode($opt_id);
	}

	//get student data-----------------------------------------
	public function get_student_data()
	{
		$class_id = $this->input->post('class_id');
		$where = "";
		if($class_id != ""){
			$where .= " AND sc.classid = ". $class_id;
		}
		$s_opt = '<option value=""></option>';

		$i = 1;
		$get_student_data = $this->student->getStudentInformation($where);
		if(count($get_student_data) > 0){
			foreach($get_student_data as $r_s){
				$s_opt .= '<option value="'. $r_s->studentid .'">'. $i .'. '. $r_s->last_name .' '. $r_s->first_name .'</option>';
				$i++;
			}
		}
					
		echo $s_opt;
	}

	//get semester data----------------------------------------
	public function get_semester(){
		$school_id = $this->input->post('school_id');
		$program_id = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$adcademic_year_id = $this->input->post('adcademic_year_id');

		$arr_semester = array();
		$get_semester_data = $this->mode->getSemesterData($school_id, $program_id, $school_level_id, $adcademic_year_id);
		if(count($get_semester_data) > 0){
			foreach($get_semester_data as $row){
				$arr_semester[] = $row;
			}
		}

		header('Content-Type: application/json');
		echo json_encode($arr_semester);
		die();
	}

	//get student information----------------------------------
	public function get_student_information()
	{
		$program_id = $this->input->post('program_id');
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$teacher_id = $this->input->post('teacher_id');
		$class_id = $this->input->post('class_id');
		$subject_id = $this->input->post('subject_id');
		$student_id = $this->input->post('student_id');
		$get_exam_type = $this->input->post('exam_type');
		$data = array();
		
		//get array of exam type
		$exam_type = '';
		if(!empty($get_exam_type)){
			$exam_type = $get_exam_type;
		}else{
			//it die if we do not select exam type (monthly, semester, and final)
			die();	
		}
		
		$where = "";
		if($program_id != ""){
			$where .= " AND sp.programid = ". $program_id;
		}
		if($school_level_id != ""){
			$where .= " AND sl.schlevelid = ". $school_level_id;
		}
		if($adcademic_year_id != ""){
			$where .= " AND sy.yearid = ". $adcademic_year_id;
		}
		if($class_id != ""){
			$where .= " AND sc.classid = ". $class_id;
		}
		if($student_id != ""){
			$where .= " AND s.studentid = ". $student_id;
		}

		//get subject and teacher name data
		$title_data = array();
		$getSubjectName = $this->mode->getSubjectByTeacher($subject_id, $class_id, $teacher_id, $adcademic_year_id, $grade_level_id, $school_level_id);
		if(count($getSubjectName) > 0){
			foreach($getSubjectName as $r_sub){
				$title_data["subjectid"] = $r_sub->subjectid;
				$title_data["subject"] = $r_sub->subject_name;
				$title_data["subject_kh"] = $r_sub->subject_kh;
				$title_data["score_percence"] = $r_sub->score_percence;
				$title_data["max_score"] = $r_sub->max_score;
			}
		}

		$getTecherName = $this->mode->getTeacherInfo($teacher_id, $school_level_id, $adcademic_year_id, $grade_level_id);
		if(count($getTecherName) > 0){
			foreach($getTecherName as $r_tea){
				$title_data["first_name"] = $r_tea->first_name;
				$title_data["last_name"] = $r_tea->last_name;
			}
		}

		//get table header data
		$head_data = array();
		$getHeader = $this->mode->getAssessmentBySubject($subject_id, $grade_level_id, $adcademic_year_id, $school_level_id);
		if(count($getHeader) > 0){
			foreach($getHeader as $r_h){
				$head_data[] = $r_h; 
			}	
		}

		//get student data
		$stu_data = array(); 
		$get_student_info = $this->student->getStudentInformation($where);
		if(count($get_student_info) > 0)
		{
			foreach($get_student_info as $r_stu)
			{	
				$getAss_array = array();
				$check_student_exist = $this->mode->checkStudentIfExsist($r_stu->studentid, $subject_id, $teacher_id, $class_id, $adcademic_year_id, $exam_type);		
				if($check_student_exist->num_rows() > 0)
				{	
					$row = $check_student_exist->row();
					if($exam_type["get_exam_type"] != "final"){
						$check_assessment = $this->mode->checkSubjectAssessmentIfExsits($row->subject_score_id, $exam_type);
						if(count($check_assessment) > 0)
						{
							foreach($check_assessment as $rs){
								$getAss_array[] = $rs;
							}
						}
						$row->{"check_assessment"} = $getAss_array;

					}else{
						if($row->total_avg_score_s1 == 0 || $row->total_avg_score_s2 == 0){
							$row->{"get_semester_score"} = $this->mode->getSemesterScore($r_stu->studentid, $subject_id, $teacher_id, $class_id, $adcademic_year_id, $exam_type);
						}
					}
					$stu_data[] = $row;
				}
				else
				{	
					if($exam_type["get_exam_type"] != "final"){
						$r_stu->{"check_assessment"} = $getAss_array;
						$r_stu->{"none_subject"} = "";
					}else{
						//get semester score (semester 1 and semester 2)
						$r_stu->{"total_avg_score_s1"} = "";
						$r_stu->{"total_avg_score_s2"} = "";
						$r_stu->{"get_semester_score"} = $this->mode->getSemesterScore($r_stu->studentid, $subject_id, $teacher_id, $class_id, $adcademic_year_id, $exam_type);
					}																
					$stu_data[] = $r_stu;
				}				
			}
		}
		//print_r($stu_data);
		$data ["title_data"] = $title_data;
		$data["header"] = $head_data;
		$data["student"] = $stu_data;

		header('Content-Type: application/json');
		echo json_encode($data);
		die();
	}

	//save score entry for each student--------------------
	public function saveSubjectScoreEntry(){
        $saveData = $this->mode->saveScoreEntry();
        header("Content-type:text/x-json");	
        $arrJson["res"]=$saveData;	        		
        echo json_encode($arrJson);
        exit();
	}


	// search ==========
	public function search() {
		$school_id = $this->input->post('school_id');
		$program_id = $this->input->post('program_id');		
		$school_level_id = $this->input->post('school_level_id');
		$grade_level_id = $this->input->post('grade_level_id');			
		$adcademic_year_id = $this->input->post('adcademic_year_id');
		$teacher_id = $this->input->post('teacher_id');
		$class_id = $this->input->post('class_id');
		$subject_id = $this->input->post('subject_id');
		$student_id = $this->input->post('student_id');
		$get_exam_type = $this->input->post('exam_type');

		$where = "";
		if($program_id != ""){
			$where .= " AND sp.programid = ". $program_id;
		}
		if($school_level_id != ""){
			$where .= " AND sl.schlevelid = ". $school_level_id;
		}
		if($adcademic_year_id != ""){
			$where .= " AND se.year = ". $adcademic_year_id;
		}
		if($class_id != ""){
			$where .= " AND sc.classid = ". $class_id;
		}
		if($student_id != ""){
			$where .= " AND s.studentid = ". $student_id;
		}


		$where_ = "";
		$where_ .= " AND m.program_id = ". $program_id;
		$where_ .= " AND m.school_level_id = ". $school_level_id;
		$where_ .= " AND m.adcademic_year_id = ". $adcademic_year_id;
		$where_ .= " AND m.class_id = ". $class_id;
		

		// db condition ==========
		$exam_type_ = $this->input->post('exam_type');
		$exam_type = $exam_type_['get_exam_type'];

		$myDB = '';
		if ($exam_type == 'monthly') {
			$myDB = 'sch_subject_score_monthly_kgp';

			$exam_monthly = $exam_type_['exam_monthly'];
			$where_ .= " AND m.exam_monthly = ". $exam_monthly; 
		}
		else if ($exam_type == 'semester') {
			$myDB = 'sch_subject_score_semester_kgp'; 
			
			$exam_semester = $exam_type_['exam_semester'];
			$get_monthly_in = implode(',', $exam_type_['get_monthly_in']);
			$where_ = ' AND m.exam_semester = '. $exam_semester .' AND m.get_monthly IN('. $get_monthly_in .')';
		}
		else if($exam_type == "final"){
			$get_monthly_in = implode(',', $exam_type_['get_semester_in']);
			$where_ .= " AND m.get_semester_id IN(".$get_monthly_in.") ";
		}

		$thead = '';

		// final =======
		if ($exam_type == 'final') 
		{
			$thead .= '<tr>
							<th><span>ល.រ</span> <br> <span>No</span></th>
		                    <th style="min-Width: 120px;"><span>ឈ្មោះសិស្ស</span> <br> <span>Name</span></th>
		                    <th><span>ភេទ</span><br><span>Sex</span></th>
	                   		<th style="width: 30px;">No permission</th>		                    
		                    <th style="width: 30px;">Permission</th>
		                    <th><span>ពិន្ទុមធ្យមភាគឆមាសទី១</span><br><span>Average Score of ​<br> Semester1</span></th>
		                    <th><span>ពិន្ទុមធ្យមភាគឆមាសទី២</span> <br> <span>Average Score of <br> Semester2</span></th>
		                    <th><span>ពិន្ទុមធ្យមភាគសរុប</span> <br> Total Average Score</span></th>
		                </tr>';

            // tbody ============
			$jj = 1;
			$get_student_info = $this->student->getStudentInformation($where);
			$averag_coefficient = 1;
			if(count($get_student_info) > 0)
			{
				foreach($get_student_info as $rowStu)
				{
					$g_total_score = 0;
					$g_average_score = 0;
					$where_avg = "  AND school_id='".$school_id."'
									AND program_id='".$program_id."'
									AND school_level_id='".$school_level_id."'
									AND adcademic_year_id='".$adcademic_year_id."'
									AND grade_level_id='".$grade_level_id."'
									AND student_id='".$rowStu->studentid."'
									AND class_id='".$class_id."'";
					$sql_attendance = $this->db->query("SELECT absent,present FROM sch_subject_score_final_kgp_multi 
														WHERE 1=1
														AND student_id='".$rowStu->studentid."'
														AND class_id='".$class_id."'
														AND grade_level_id='".$grade_level_id."'
														AND adcademic_year_id='".$adcademic_year_id."'
														AND school_level_id='".$school_level_id."'
														AND program_id='".$program_id."'
														AND school_id='".$school_id."'
														")->row();
					$absent = isset($sql_attendance->absent)?$sql_attendance->absent:0;
					$present = isset($sql_attendance->present)?$sql_attendance->present:0;
					$sql_smt_1 = $this->db->query("SELECT
													(average_score+average_monthly_score)/2 AS total_avg
													FROM
													sch_subject_score_semester_kgp_multi
													WHERE 1=1 {$where_avg}
													AND exam_semester='".$exam_type_['get_semester_in'][0]."'")->row();
					$sql_smt_2 = $this->db->query("SELECT
													(average_score+average_monthly_score)/2 AS total_avg
													FROM
													sch_subject_score_semester_kgp_multi
													WHERE 1=1 {$where_avg}
													AND exam_semester='".$exam_type_['get_semester_in'][1]."'")->row();
					$q_smt1 = isset($sql_smt_1)?$sql_smt_1->total_avg:0;
					$q_smt2 = isset($sql_smt_2)?$sql_smt_2->total_avg:0;
					$total_avg = ($q_smt1+$q_smt2)/2;
					$thead .= '<tr>
								<td style="text-align: center;" class="no_">'.$jj++.'</td>
								<td style="padding: 3px !important;" class="student_id" attr-student_id="'.$rowStu->studentid.'">'.$rowStu->last_name_kh.' '.$rowStu->first_name_kh.'</td>
								<td style="text-align: center;" class="gender">'.($rowStu->gender == 'female' ? 'F' : 'M').'</td>
								<td><input type="text" class="form-control input-xs absent" value="'.$absent.'"></td>
								<td style="min-width: 30px !important;"><input type="text" class="form-control input-xs present" value="'.$present.'"></td>
								<td style="text-align: right;padding: 3px !important;" class="semester1">'.number_format($q_smt1, 2).'</td>
								<td style="text-align: right;padding: 3px !important;" class="semester2">'.number_format($q_smt2, 2).'</td>
								<td style="text-align: right;padding: 3px !important;" class="total_avg">'.number_format($total_avg, 2).'</td>
							</tr>';

				}
			}// tbody =======

		}
		else {// !final =======
				
			// subject type =======
			$q_subjectType = $this->db->query("SELECT
													t.subj_type_id,
													t.subject_type
												FROM
													sch_subject_type AS t
												WHERE t.schoolid='".$school_id."' AND schlevelid='".$school_level_id."'
												ORDER BY
													t.subject_type ASC ");
			
			$thead .= '<tr>
						<th rowspan="2">No</th>
	                    <th rowspan="2" style="min-Width: 120px;">Name</th>
	                    <th rowspan="2">No permission</th>
	                    <th rowspan="2">Permission</th>';
			
			if ($q_subjectType->num_rows() > 0) {
				foreach ($q_subjectType->result() as $r) {
					$q = $this->db->query("SELECT s.subjectid FROM sch_subject AS s WHERE s.subj_type_id = '".$r->subj_type_id."' AND schlevelid='".$school_level_id."'");
					$thead .= '<th colspan="'.$q->num_rows().'" attr-subj_type_id="'.$r->subj_type_id.'">'.$r->subject_type.'</th>';
				}
			}
			$thead .= '<th rowspan="2">Total Score</th></tr>';
			//$thead .= '<th rowspan="2">Comment</th></tr>';
			// ================					


			// subject =========
			$thead .= '<tr>';					
			if ($q_subjectType->num_rows() > 0) {
				foreach ($q_subjectType->result() as $r_gr) {
					$q_subject = $this->db->query("SELECT
														s.subjectid,
														s.subject_kh as subject,
														s.subj_type_id
													FROM
														sch_subject AS s
													WHERE
														s.subj_type_id = '".$r_gr->subj_type_id."' ");

					if ($q_subject->num_rows() > 0) {
						foreach ($q_subject->result() as $r_subject) {
							$thead .= '<th>'.$r_subject->subject.'</th>';						
						}
					}
					else {
						$thead .= '<th>&nbsp;</th>';
					}// subject ====

				}
			}// subject type ====			
			$thead .= '</tr>';
			// ===================


			// tbody ============
			$jj = 1;
			$get_student_info = $this->student->getStudentInformation($where);						
			$averag_coefficient = 0;
			if(count($get_student_info) > 0)
			{			
				foreach($get_student_info as $rowStu)
				{
					$present = 0;
					$absent  = 0;
					$g_average_score    = 0;
					
					if($exam_type == 'monthly'){
						$check_attendance = $this->db->query("SELECT
															present,
															absent,
															average_score,
															averag_coefficient 
															FROM
															sch_subject_score_monthly_kgp_multi
															WHERE 1=1 
															AND student_id='".$rowStu->studentid."'
															AND program_id = '".$program_id."'
															AND school_id = '".$school_id."'
															AND school_level_id = '".$school_level_id."'
															AND adcademic_year_id = '".$adcademic_year_id."'
															AND grade_level_id = '".$grade_level_id."'
															AND class_id = '".$class_id."' 
															AND exam_monthly = '".$exam_type_['exam_monthly']."'
															")->row();
						$g_average_score = isset($check_attendance->average_score)?$check_attendance->average_score:0;
						$averag_coefficient = isset($check_attendance->averag_coefficient)?$check_attendance->averag_coefficient:0;
						$present = isset($check_attendance->present)?$check_attendance->present:0;
						$absent  = isset($check_attendance->absent)?$check_attendance->absent:0;
					}else if($exam_type == 'semester'){
						$check_attendance = $this->db->query("SELECT
															present,
															absent,
															average_score,
															averag_coefficient 
															FROM
															sch_subject_score_semester_kgp_multi 
															WHERE 1=1 
															AND student_id='".$rowStu->studentid."'
															AND program_id = '".$program_id."'
															AND school_id = '".$school_id."'
															AND school_level_id = '".$school_level_id."'
															AND adcademic_year_id = '".$adcademic_year_id."'
															AND grade_level_id = '".$grade_level_id."'
															AND class_id = '".$class_id."' 
															AND exam_semester = '".$exam_semester."' 
															AND get_monthly IN('".$get_monthly_in."')
															")->row();
						$g_average_score = isset($check_attendance->average_score)?$check_attendance->average_score:0;
						$averag_coefficient = isset($check_attendance->averag_coefficient)?$check_attendance->averag_coefficient:0;
						$present = isset($check_attendance->present)?$check_attendance->present:0;
						$absent  = isset($check_attendance->absent)?$check_attendance->absent:0;
					}
					
					$thead .= '<tr>
								<td class="no_" style="text-align: center;">'.$jj++.'</td>
								<td class="student_id" attr-student_id="'.$rowStu->studentid.'" style="padding-left: 3px !important;">'.$rowStu->last_name_kh.' '.$rowStu->first_name_kh.'</td>
								<td><input type="text" class="form-control input-xs present" value="'.$present.'"></td>
								<td><input type="text" class="form-control input-xs absent" value="'.$absent.'"></td>';

					// subject =========
					$g_total_score   = 0;
					//$g_average_score = 0;
								
					if ($q_subjectType->num_rows() > 0) 
					{
						foreach ($q_subjectType->result() as $r_gr) {

							$q_subject = $this->db->query("SELECT
																s.subjectid,
																s.`subject`,
																s.subj_type_id,
																s.subject_mainid
															FROM
																sch_subject AS s
															WHERE
																s.subj_type_id = '".$r_gr->subj_type_id."' ");

							if ($q_subject->num_rows() > 0) {
								foreach ($q_subject->result() as $r_) {

									$rowScore = $this->db->query("SELECT
																		m.total_score,
																		m.average_score
																	FROM
																		".$myDB." AS m
																	WHERE 1 = 1 {$where_} 
																	AND m.student_id = '".$rowStu->studentid."' 
																	AND m.subject_id = '".$r_->subjectid."' 
																");
									$total_score   = isset($rowScore->row()->total_score) ? $rowScore->row()->total_score : '0';
									$average_score = isset($rowScore->row()->average_score) ? $rowScore->row()->average_score : '0';
									
									// g. total & g. avg ========									
									$g_total_score += ($total_score - 0);
									//$g_average_score += ($average_score - 0);

									$thead .= '<td><input type="text" class="form-control input-xs '.$r_->subjectid.' subject_id" value="'.number_format($total_score, 2).'" attr-subject_id="'.$r_->subjectid.'" attr-subject_type_id="'.$r_->subj_type_id.'" attr-subject_mainid="'.$r_->subject_mainid.'" readonly></td>'.$r_->subject.'</td>';
								}
							}
							else {
								$thead .= '<td>&nbsp;</td>';
							}// subject ====

						}
					}// subject type ====
					if($averag_coefficient == 0){
						$averag_coefficient = 1;
					}
					$grand_total_avg = round(($g_total_score/$averag_coefficient),2);
					$thead .= '<td style="text-align:center;"><span class="total_score_sub" attr-g_total_score="'.$g_total_score.'" attr-g_average_score="'.$grand_total_avg.'">'.$g_total_score.'</span></td></tr>';
				}// student info. ===

			}// tbody =======			

		}// exam type =======	

		$arr = array('tbl' => $thead,'averag_coefficient'=>$averag_coefficient);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}
	public function check_data(){
		$arr_info     = $this->input->post('arr_info');
		$exam_monthly = $this->input->post('exam_type');
		$show_check = 0;
		if(count($arr_info) > 0){
			if($exam_monthly['get_exam_type'] =='monthly')
			{
					$where = "";
					if($arr_info['student_id'] != ""){
						$where .= " AND msk.student_id='".$arr_info['student_id']."'";
					}
					$check_sql = $this->db->query("SELECT
												msk.student_id
												FROM
												sch_subject_score_monthly_kgp_multi AS msk
												WHERE 1=1 
												AND msk.program_id = '".$arr_info['program_id']."'
												AND msk.school_id = '".$arr_info['school_id']."'
												AND msk.school_level_id = '".$arr_info['school_level_id']."'
												AND msk.adcademic_year_id = '".$arr_info['academic_year_id']."'
												AND msk.grade_level_id = '".$arr_info['grade_level_id']."'
												AND msk.class_id = '".$arr_info['class_id']."' 
												AND msk.exam_monthly = '".$exam_monthly['exam_monthly']."'
												{$where}")->num_rows();
					
					if($check_sql > 0){
						$show_check = 100;
					}else{
						$show_check = 200;
					}
			} // end if monthly
			else if($exam_monthly['get_exam_type'] =='semester')
				{ // semester
				$where = "";
				if($arr_info['student_id'] != ""){
					$where .= " AND fmkp.student_id='".$arr_info['student_id']."'";
				}
				$check_sql = $this->db->query("SELECT
											fmkp.student_id
											FROM
											sch_subject_score_semester_kgp_multi AS fmkp
											WHERE 1=1 
											AND fmkp.program_id = '".$arr_info['program_id']."'
											AND fmkp.school_id = '".$arr_info['school_id']."'
											AND fmkp.school_level_id = '".$arr_info['school_level_id']."'
											AND fmkp.adcademic_year_id = '".$arr_info['academic_year_id']."'
											AND fmkp.grade_level_id = '".$arr_info['grade_level_id']."'
											AND fmkp.class_id = '".$arr_info['class_id']."'
											AND fmkp.exam_semester = '".$exam_monthly['exam_semester']."' {$where}")->num_rows();

				if($check_sql > 0){
					$show_check = 100;
				}else{
					$show_check = 200;
				}
			} // end semester
		}
		$arr = array();
		$arr['checkdata'] = $show_check;
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}

	public function save() {
		// $teacher_id = $this->input->post('teacher_id');
		// $school_id = $this->input->post('school_id');
		// $program_id = $this->input->post('program_id');		
		// $school_level_id = $this->input->post('school_level_id');
		// $academic_year_id = $this->input->post('academic_year_id');
		// $grade_level_id = $this->input->post('grade_level_id');			
		// $class_id = $this->input->post('class_id');
		// $student_id = $this->input->post('student_id');	
		$arr_infor = $this->input->post('get_infor');	
		$exam_type = $this->input->post('exam_type');
		$averag_coefficient = trim($this->input->post('co_effecient'));

		$arr_detail = $this->input->post('arr');
		$arrOrd= $this->input->post('arr_ord');
		// $exam_type_ = $this->input->post('exam_type');
		// $exam_type = $exam_type_['get_exam_type'];	

		$get_monthly = '';
		$exam_semester = '';
		if ($exam_type['get_exam_type'] == 'monthly') {
			$get_monthly = $exam_type['exam_monthly'];
		}
		else if ($exam_type['get_exam_type'] == 'semester') {	
			$exam_semester = $exam_type['exam_semester'];
			$get_monthly = implode(',', $exam_type['get_monthly_in']);
		}
		else {			
			$get_monthly = implode(',', $exam_type['get_semester_in']);
		}

		$where = "";
		if($arr_infor['program_id'] != ""){
			$where .= " AND m.program_id = ". $arr_infor['program_id'];
		}
		if($arr_infor['school_level_id'] != ""){
			$where .= " AND m.school_level_id = ". $arr_infor['school_level_id'];
		}
		if($arr_infor['academic_year_id'] != ""){
			$where .= " AND m.adcademic_year_id = ". $arr_infor['academic_year_id'];
		}
		if($arr_infor['class_id'] != ""){
			$where .= " AND m.class_id = ". $arr_infor['class_id'];
		}
		if($arr_infor['student_id'] != ""){
			//$where .= " AND m.student_id = ". $arr_infor['student_id'];
		}

		// transaction ======
    	$this->db->trans_begin();

    		if ($exam_type['get_exam_type'] == 'final') 
    		{
    			$semester_arr = $this->input->post("exam_type");
				if (!empty($arr_detail)) {
					$data = array(
									'student_id' => $arr_detail['student_id'],
									'school_id' => $arr_infor['school_id'],
									'program_id' => $arr_infor['program_id'],							
									'school_level_id' => $arr_infor['school_level_id'],
									'adcademic_year_id' => $arr_infor['academic_year_id'],
									'grade_level_id' => $arr_infor['grade_level_id'],
									'class_id' => $arr_infor['class_id'],
									'present' => $arr_detail['present'],
									'absent' => $arr_detail['absent'],
									'total_avg_score_s1' => $arr_detail['semester1'],
									'total_avg_score_s2' => $arr_detail['semester2'],
									'total_average_score' => $arr_detail['total_avg'],
									'averag_coefficient' => $averag_coefficient
								);
					$sql_check = $this->db->query("SELECT count(*) as c FROM sch_subject_score_final_kgp_multi 
														WHERE 1=1
															AND student_id = '".$arr_detail['student_id']."'
															AND school_id = '".$arr_infor['school_id']."'
															AND program_id = '".$arr_infor['program_id']."'						
															AND school_level_id = '".$arr_infor['school_level_id']."'
															AND adcademic_year_id = '".$arr_infor['academic_year_id']."'
															AND grade_level_id = '".$arr_infor['grade_level_id']."'
															AND class_id = '".$arr_infor['class_id']."'")->row();
					if($sql_check->c > 0){
						$this->db->where("student_id",$arr_detail['student_id']);
						$this->db->where("school_id",$arr_infor['school_id']);
						$this->db->where("program_id",$arr_infor['program_id']);
						$this->db->where("school_level_id",$arr_infor['school_level_id']);
						$this->db->where("adcademic_year_id",$arr_infor['academic_year_id']);
						$this->db->where("grade_level_id",$arr_infor['grade_level_id']);
						$this->db->where("class_id",$arr_infor['class_id']);
						$this->db->update('sch_subject_score_final_kgp_multi', $data);
					}else{
						$this->db->insert('sch_subject_score_final_kgp_multi', $data);
					}
					$where_f = " AND school_id = '".$arr_infor['school_id']."'
								AND program_id = '".$arr_infor['program_id']."'						
								AND school_level_id = '".$arr_infor['school_level_id']."'
								AND adcademic_year_id = '".$arr_infor['academic_year_id']."'
								AND grade_level_id = '".$arr_infor['grade_level_id']."'
								AND class_id = '".$arr_infor['class_id']."'";
					$this->update_rank_final($where_f);

					$arr_sum_score_f = array();
					if(count($semester_arr['get_semester_in']) > 0)
					{
						foreach($semester_arr['get_semester_in'] as $vsmt){
							$sql_final_detail = $this->db->query("SELECT student_id,subject_id,total_score 
																FROM sch_subject_score_semester_detail_kgp_multi
																WHERE 1=1
																AND student_id = '".$arr_detail['student_id']."'
																AND school_id = '".$arr_infor['school_id']."'
																AND program_id = '".$arr_infor['program_id']."'						
																AND school_level_id = '".$arr_infor['school_level_id']."'
																AND adcademic_year_id = '".$arr_infor['academic_year_id']."'
																AND grade_level_id = '".$arr_infor['grade_level_id']."'
																AND class_id = '".$arr_infor['class_id']."'
																AND exam_semester='".$vsmt."'");
							
							if($sql_final_detail->num_rows() > 0){
								foreach($sql_final_detail->result() as $rf){
									if(isset($arr_sum_score_f[$rf->student_id][$rf->subject_id])){
										$arr_sum_score_f[$rf->student_id][$rf->subject_id] = $arr_sum_score_f[$rf->student_id][$rf->subject_id]+$rf->total_score;
									}else{
										$arr_sum_score_f[$rf->student_id][$rf->subject_id] = $rf->total_score;
									}
								}
							}
						}
					}
					$sql_semester_show = $this->db->query("SELECT
																student_id,
																subject_mainid,
																subject_groupid,
																subject_id
																FROM
																	sch_subject_score_semester_detail_kgp_multi
																WHERE 1=1
																AND student_id='".$arr_detail['student_id']."'
																AND school_id = '".$arr_infor['school_id']."'
																AND program_id = '".$arr_infor['program_id']."'						
																AND school_level_id = '".$arr_infor['school_level_id']."'
																AND adcademic_year_id = '".$arr_infor['academic_year_id']."'
																AND grade_level_id = '".$arr_infor['grade_level_id']."'
																AND class_id = '".$arr_infor['class_id']."'
																GROUP BY student_id,subject_groupid,subject_id");
					if($sql_semester_show->num_rows() > 0){
						foreach($sql_semester_show->result() as $rowsm){
							$studentid = $rowsm->student_id;
							$show_total_score = isset($arr_sum_score_f[$studentid][$rowsm->subject_id])?$arr_sum_score_f[$studentid][$rowsm->subject_id]:0;
							$data_smt = array('student_id'=>$studentid,
												'schoolid'=>$arr_infor['school_id'],
												'programid'=>$arr_infor['program_id'],
												'schlevelid'=>$arr_infor['school_level_id'],
												'yearid'=>$arr_infor['academic_year_id'],
												'grade_level_id'=>$arr_infor['grade_level_id'],
												'classid'=>$arr_infor['class_id'],
												'subject_mainid'=>$rowsm->subject_mainid,
												'subject_group'=>$rowsm->subject_groupid,
												'subjectid'=>$rowsm->subject_id,
												'total_score'=>round(($show_total_score/2),2)
											);
							$where_delete   = array("student_id"=>$studentid,
													"schoolid"=>$arr_infor['school_id'],
													"programid"=>$arr_infor['program_id'],
													"schlevelid"=>$arr_infor['school_level_id'],
													"yearid"=>$arr_infor['academic_year_id'],
													"grade_level_id"=>$arr_infor['grade_level_id'],
													"classid"=>$arr_infor['class_id'],
													"subject_group"=>$rowsm->subject_groupid,
													"subjectid"=>$rowsm->subject_id);

							$this->db->delete('sch_subject_score_final_detail_kgp_multi',$where_delete);						
							$this->db->insert('sch_subject_score_final_detail_kgp_multi',$data_smt);
							$where_rank_subj = " AND schoolid = '".$arr_infor['school_id']."'
												AND programid = '".$arr_infor['program_id']."'
												AND schlevelid = '".$arr_infor['school_level_id']."'
												AND yearid = '".$arr_infor['academic_year_id']."'
												AND grade_level_id = '".$arr_infor['grade_level_id']."'
												AND classid = '".$arr_infor['class_id']."'
												AND subject_group = '".$rowsm->subject_groupid."'
												AND subjectid = '".$rowsm->subject_id."'
												";
							$this->update_rank_subject_final($where_rank_subj);
						}
					}
					
				}
			}
			else 
			{// !final ======
				if ($exam_type['get_exam_type'] == 'monthly') 
				{// monthly =====		
					//$where .= " AND m.exam_monthly = ". $get_monthly;
						
						if (count($arrOrd)>0) 
						{
	    					$dataOrd = array(
											'student_id' => $arrOrd['student_id'],
											'school_id' => $arr_infor['school_id'],
											'program_id' => $arr_infor['program_id'],						
											'school_level_id' => $arr_infor['school_level_id'],
											'adcademic_year_id' => $arr_infor['academic_year_id'],
											'grade_level_id' => $arr_infor['grade_level_id'],
											'class_id' => $arr_infor['class_id'],
											'exam_monthly' => $get_monthly,
											'total_score' => $arrOrd['g_total_score'],
											'average_score' => $arrOrd['g_average_score'],
											'present' => $arrOrd['present'],
											'absent' => $arrOrd['absent'],
											'averag_coefficient'=>$averag_coefficient
										);

							$this->db->insert('sch_subject_score_monthly_kgp_multi', $dataOrd);
						
							if (count($arr_detail) > 0) {
								foreach($arr_detail as $r_) {
									$data = array(
										'student_id' => $r_['student_id'],
										'teacher_id' => "",
										'school_id' => $arr_infor['school_id'],
										'program_id' => $arr_infor['program_id'],							
										'school_level_id' => $arr_infor['school_level_id'],
										'adcademic_year_id' => $arr_infor['academic_year_id'],
										'grade_level_id' => $arr_infor['grade_level_id'],
										'class_id' => $arr_infor['class_id'],
										'subject_mainid' => isset($r_['subject_mainid'])?$r_['subject_mainid']:"",
										'subject_groupid' => isset($r_['subject_type_id'])?$r_['subject_type_id']:"",
										'subject_id' => isset($r_['subject_id'])?$r_['subject_id']:"",
										'total_score' => isset($r_['total_score'])?$r_['total_score']:"",
										'average_score' => "",
										'exam_monthly' => $get_monthly,
										'averag_coefficient' => $averag_coefficient
									);
									$this->db->insert('sch_subject_score_monthly_detail_kgp_multi', $data);
									
								}
							}
						} // end count array Order
						// ==========
				}				
				else if ($exam_type['get_exam_type'] == 'semester') 
				{// semester =========

					// master =========
		    		if (!empty($arrOrd)) {
						//foreach ($arrOrd as $r__) {
		    				$explode_m = explode(",",$get_monthly);
		    				$sum_avg_m = 0;
		    				$count_m   = 0;
		    				for($jj=0;$jj<=count($explode_m);$jj++){
		    					$sql_month = $this->db->query("SELECT average_score FROM sch_subject_score_monthly_kgp_multi
									    						WHERE 1=1 
									    						AND exam_monthly='".$explode_m[$jj]."'
																AND student_id='".$arrOrd['student_id']."'
																AND program_id='".$arr_infor['program_id']."'
																AND school_id='".$arr_infor['school_id']."'
																AND school_level_id='".$arr_infor['school_level_id']."'
																AND adcademic_year_id='".$arr_infor['academic_year_id']."'
																AND grade_level_id='".$arr_infor['grade_level_id']."'
																AND class_id='".$arr_infor['class_id']."'")->row();
		    					$m_avg = isset($sql_month->average_score)?$sql_month->average_score:0;
		    					$sum_avg_m+=$m_avg;
		    					$count_m++;
		    				}
							$data__ = array(
											'student_id' => $arrOrd['student_id'],
											'school_id' => $arr_infor['school_id'],
											'program_id' => $arr_infor['program_id'],							
											'school_level_id' => $arr_infor['school_level_id'],
											'adcademic_year_id' => $arr_infor['academic_year_id'],
											'grade_level_id' => $arr_infor['grade_level_id'],
											'class_id' => $arr_infor['class_id'],
											'get_monthly' => $get_monthly,									
											'exam_semester' => $exam_semester,
											'total_score' => $arrOrd['g_total_score'],
											'average_score' => round($arrOrd['g_average_score']/$averag_coefficient,2),
											'averag_coefficient'=> $averag_coefficient,
											'average_monthly_score'=> round(($sum_avg_m/$count_m),2),
											'present' => $arrOrd['present'],
											'absent' => $arrOrd['absent'],
											'comments' => $arrOrd['comments']
										);
							
							$this->db->insert('sch_subject_score_semester_kgp_multi', $data__);
						//}
					}		

					// detail ==========
					if (!empty($arr_detail)) {
						foreach ($arr_detail as $r_) {
							$data = array(
								'student_id' => $r_['student_id'],
								'teacher_id' => "",
								'school_id' => $arr_infor['school_id'],
								'program_id' => $arr_infor['program_id'],							
								'school_level_id' => $arr_infor['school_level_id'],
								'adcademic_year_id' => $arr_infor['academic_year_id'],
								'grade_level_id' => $arr_infor['grade_level_id'],
								'class_id' => $arr_infor['class_id'],
								'subject_mainid' => $r_['subject_mainid'],
								'subject_groupid' => $r_['subject_type_id'],
								'subject_id' => $r_['subject_id'],
								'total_score' => $r_['total_score'],
								'average_score' => "",
								'exam_monthly' => $get_monthly,
								'averag_coefficient' => $averag_coefficient,
								'exam_semester'=>$exam_semester
							);
							
							$this->db->insert('sch_subject_score_semester_detail_kgp_multi', $data);
						}	
					}
					// end detail ==========
					
				}// end if check =====		    		

			}// end if exam type =====			
		$i = 1 ;	
		if ($this->db->trans_status() === FALSE){
			//$this->db->trans_rollback();
		}
		else{
			$this->db->trans_commit();			
			//$this->green->getRankresult_by_class($arr_infor['program_id'], $arr_infor['school_level_id'], $arr_infor['academic_year_id'], $arr_infor['school_id'], $arr_infor['class_id'], $exam_type);
			$i;
		}		
		
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($i);	
	}
	function update_rank_final($get_where){
			$sql_final = $this->db->query("SELECT student_id,total_average_score FROM sch_subject_score_final_kgp_multi WHERE 1=1 {$get_where} ORDER BY total_average_score DESC");
			$ii=1;
			$arr_rank = array();
			$rank  = 1;
			$rank1 = 1;
			if($sql_final->num_rows() > 0)
			{
				foreach($sql_final->result() as $row_f)
				{
					if(isset($arr_rank) AND in_array($row_f->total_average_score,$arr_rank)){
						$rank1=$rank;
					}else{
						$rank = $ii;
						$rank1= $ii;
					}
					$arr_rank[] = $row_f->total_average_score;
					
					$this->db->query("UPDATE sch_subject_score_final_kgp_multi SET ranking='".$rank1."' WHERE 1=1 AND student_id='".$row_f->student_id."' {$get_where}");
					$ii++;
				}
			}
	}
	function update_rank_subject_final($get_where){
			$sql_final = $this->db->query("SELECT student_id,total_score FROM sch_subject_score_final_detail_kgp_multi WHERE 1=1 {$get_where} ORDER BY total_score DESC");
			$ii=1;
			$arr_rank = array();
			$rank  = 1;
			$rank1 = 1;
			if($sql_final->num_rows() > 0)
			{
				foreach($sql_final->result() as $row_f)
				{
					if(isset($arr_rank) AND in_array($row_f->total_score,$arr_rank)){
						$rank1=$rank;
					}else{
						$rank = $ii;
						$rank1= $ii;
					}
					$arr_rank[] = $row_f->total_score;
					
					$this->db->query("UPDATE sch_subject_score_final_detail_kgp_multi SET rank='".$rank1."' WHERE 1=1 AND student_id='".$row_f->student_id."' {$get_where}");
					$ii++;
				}
			}
			$dd = "SELECT student_id,total_score FROM sch_subject_score_final_detail_kgp_multi WHERE 1=1 {$get_where} ORDER BY total_score DESC";
			return $dd;
	}
	function update_rank_semester(){
		
		$arr_infor1 = $this->input->post("arr_info");
		$exam_type1 = $this->input->post("exam_type");
		$get_monthly   = '';
		$exam_semester = '';
		$dd = "";
		//if(count($exam_type) > 0 ){
			if ($exam_type1['get_exam_type'] == 'monthly') {
				$get_monthly = $exam_type1['exam_monthly'];
			}
			else if ($exam_type1['get_exam_type'] == 'semester') {			
				$exam_semester = $exam_type1['exam_semester'];
				$get_monthly = implode(',', $exam_type1['get_monthly_in']);
			}
			else {			
				//$get_monthly = implode(',', $exam_type1['get_semester_in']);
			}	
		//}
		$kk = 0;

		if(count($arr_infor1) > 0)
		{
			$school_id        = isset($arr_infor1['school_id'])?$arr_infor1['school_id']:"";
			$program_id       = isset($arr_infor1['program_id'])?$arr_infor1['program_id']:"";
			$school_level_id  = isset($arr_infor1['school_level_id'])?$arr_infor1['school_level_id']:"";
			$academic_year_id = isset($arr_infor1['academic_year_id'])?$arr_infor1['academic_year_id']:"";
			$grade_level_id   = isset($arr_infor1['grade_level_id'])?$arr_infor1['grade_level_id']:"";
			$class_id         = isset($arr_infor1['class_id'])?$arr_infor1['class_id']:"";
			$sql_subj11 = $this->db->query("SELECT subject_id,student_id 
											FROM sch_subject_score_semester_detail_kgp_multi
											WHERE school_id='".$school_id ."'
			 								AND program_id='".$program_id."'				
			 								AND school_level_id='".$school_level_id."'
			 								AND adcademic_year_id='".$academic_year_id."'
			 								AND grade_level_id='".$grade_level_id."'
			 								AND class_id='".$class_id."'
			 								AND exam_monthly='".$get_monthly."'
			 								GROUP BY subject_id");
			if($sql_subj11->num_rows() > 0)
			{
				foreach($sql_subj11->result() as $row_s)
				{
					
						$sql_subj_rank = $this->db->query("SELECT total_score,student_id,subject_id 
																	FROM sch_subject_score_semester_detail_kgp_multi
																 	WHERE school_id='".$school_id ."'
									 								AND program_id='".$program_id."'				
									 								AND school_level_id='".$school_level_id."'
									 								AND adcademic_year_id='".$academic_year_id."'
									 								AND grade_level_id='".$grade_level_id."'
									 								AND class_id='".$class_id."'
									 								AND exam_monthly='".$get_monthly."'
																	AND subject_id='".$row_s->subject_id."'
																	ORDER BY total_score DESC");
						$ii=1;
						$arr_rank = array();
						$rank  = 1;
						$rank1 = 1;
						if($sql_subj_rank->num_rows() > 0)
						{
							foreach($sql_subj_rank->result() as $row_subj)
							{
								if(isset($arr_rank) AND in_array($row_subj->total_score,$arr_rank)){
									$rank1=$rank;
								}else{
									$rank = $ii;
									$rank1= $ii;
								}
								$arr_rank[] = $row_subj->total_score;
								
								$this->db->query("UPDATE sch_subject_score_semester_detail_kgp_multi SET rank_subject='".$rank1."'
													 	WHERE 
														school_id='".$school_id ."'
						 								AND program_id='".$program_id."'				
						 								AND school_level_id='".$school_level_id."'
						 								AND adcademic_year_id='".$academic_year_id."'
						 								AND grade_level_id='".$grade_level_id."'
						 								AND class_id='".$class_id."'
						 								AND exam_monthly='".$get_monthly."'
														AND subject_id ='".$row_subj->subject_id."'
														AND student_id='".$row_subj->student_id."'
														");
								$ii++;
							}
						}
								// =============== start rank
				} // end earch group subject
			} // end if $sql_subj
		}
		echo $kk;
	}
	function update_rank_monthly(){
		$arr_infor = $this->input->post("arr_info");
		$exam_type = $this->input->post("exam_type");
		if(count($arr_infor) > 0){
			$qr_update_rank_monthly = $this->db->query("SELECT
															student_id,
															total_score
															FROM
															sch_subject_score_monthly_kgp_multi AS msk
															WHERE 1=1 
															AND msk.program_id = '".$arr_infor['program_id']."'
															AND msk.school_id = '".$arr_infor['school_id']."'
															AND msk.school_level_id = '".$arr_infor['school_level_id']."'
															AND msk.adcademic_year_id = '".$arr_infor['academic_year_id']."'
															AND msk.grade_level_id = '".$arr_infor['grade_level_id']."'
															AND msk.class_id = '".$arr_infor['class_id']."' 
															AND msk.exam_monthly = '".$exam_type['exam_monthly']."'
															ORDER BY total_score DESC
															");
			$ii=1;
			$arr_rank = array();
			$rank  = 1;
			$rank1 = 1;
			if($qr_update_rank_monthly->num_rows() > 0){
				foreach($qr_update_rank_monthly->result() as $rup){
					if(isset($arr_rank) AND in_array($rup->total_score,$arr_rank)){
						$rank1=$rank;
					}else{
						$rank = $ii;
						$rank1= $ii;
					}
					$arr_rank[] = $rup->total_score;

					$this->db->query("UPDATE sch_subject_score_monthly_kgp_multi SET ranking_bysubj='".$rank1."'
											WHERE 1=1
											AND student_id = '".$rup->student_id."'
											AND program_id = '".$arr_infor['program_id']."'
											AND school_id = '".$arr_infor['school_id']."'
											AND school_level_id = '".$arr_infor['school_level_id']."'
											AND adcademic_year_id = '".$arr_infor['academic_year_id']."'
											AND grade_level_id = '".$arr_infor['grade_level_id']."'
											AND class_id = '".$arr_infor['class_id']."' 
											AND exam_monthly = '".$exam_type['exam_monthly']."'
											");
					$ii++;
				}
			}
			echo $ii;
		}
	}
	// delete ==========
	public function delete_data() {
		$school_id = $this->input->post('school_id');
		$program_id = $this->input->post('program_id');		
		$school_level_id = $this->input->post('school_level_id');
		$academic_year_id = $this->input->post('academic_year_id');
		$grade_level_id = $this->input->post('grade_level_id');			
		$class_id = $this->input->post('class_id');
		$student_id = $this->input->post('student_id');
		$exam_type_ = $this->input->post('exam_type');
		$exam_type = $exam_type_['get_exam_type'];	

		$get_monthly = '';
		$exam_semester = '';

		if ($exam_type == 'monthly') {
			$get_monthly = $exam_type_['exam_monthly'];
		}
		else if ($exam_type == 'semester') {			
			$exam_semester = $exam_type_['exam_semester'];
			$get_monthly = implode(',', $exam_type_['get_monthly_in']);
		}
		else {			
			$get_monthly = implode(',', $exam_type_['get_semester_in']);
		}

		$where = "";
		if($program_id != ""){
			$where .= " program_id = ". $program_id;
		}
		if($school_level_id != ""){
			$where .= " AND school_level_id = ". $school_level_id;
		}
		if($academic_year_id != ""){
			$where .= " AND adcademic_year_id = ". $academic_year_id;
		}
		if($class_id != ""){
			$where .= " AND class_id = ". $class_id;
		}
		if($student_id != ""){
			$where .= " AND student_id = ". $student_id;
		}

		// transaction ======
    	$this->db->trans_begin();
    		if ($exam_type == 'final') {
    			$where .= " AND get_semester_id IN(".$get_monthly.") ";
    			$this->db->query("DELETE FROM sch_subject_score_final_kgp_multi WHERE {$where} ");
    			/* $this->db->query("DELETE FROM sch_subject_score_final_detail_kgp_multi WHERE {$where} "); */
    			$i = 1;
    		}
    		else if ($exam_type == 'monthly') {
    			$where .= " AND exam_monthly = ". $get_monthly;
    			$this->db->query("DELETE FROM sch_subject_score_monthly_kgp_multi WHERE {$where} ");
    			$this->db->query("DELETE FROM sch_subject_score_monthly_detail_kgp_multi WHERE {$where} ");
    			$i = 1;
    		}
    		else if ($exam_type == 'semester') {
    			$where1 = '';    			
    			$where1 .= " AND exam_semester = '".$exam_semester."' ";
				$where1 .= " AND get_monthly IN(".$get_monthly.") ";

				$where2 = '';
    			$where2 .= " AND exam_monthly IN(".$get_monthly.") ";    			

				$this->db->query("DELETE FROM sch_subject_score_semester_kgp_multi WHERE {$where} {$where1} ");
    			$this->db->query("DELETE FROM sch_subject_score_semester_detail_kgp_multi WHERE {$where} {$where2} ");
    			$i = 1;
    		}

    	if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		}
		else {
			$this->db->trans_commit();			
			$i;
		}		
		
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($i);

	}
   

}
