<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class C_evaluate_trans_kgp extends CI_Controller{

		function __construct() {
	        parent::__construct();
			$this->load->model('student/m_evaluate_trans_kgp', 'mode');
			$this->load->model('school/schoolinformodel', 'school');
			$this->load->model('school/program', 'program');			
			$this->load->model('school/schoollevelmodel', 'school_level');
			$this->load->model('school/gradelevelmodel', 'grade_level');
			$this->load->model('school/schoolyearmodel', 'adcademic_year');
			$this->load->model('school/classmodel', 'class');                			
	    }

	    public function index()
	    {
	    	$this->load->view('header');
			$this->load->view('student/v_evaluate_trans_kgp');
			$this->load->view('footer');
	    }

	    //get year and grade data----------------------------------
		public function get_year_and_class_data()
		{
			$school_id = $_POST['school_id'];
			$program_id = $_POST['program_id'];
			$school_level_id = $_POST['school_level_id'];
			$opt = array();

			//get year
			$y_opt = array();
			$get_year_data = $this->adcademic_year->getschoolyear($school_id,$program_id,$school_level_id);
			foreach($get_year_data as $r_y){				
				$y_opt[] = $r_y;
			}

			//get grade
			$g_opt = array(); 
			$get_grade_data = $this->grade_level->getgradelevels($school_level_id);
			foreach($get_grade_data as $r_g){				
				$g_opt[] = $r_g;
			}

			$opt["year"] = $y_opt;
			$opt["grade"] = $g_opt;

			header('Content-Type: application/json');
			echo json_encode($opt);
		}

		//get class data-------------------------------------------
		public function get_class_data()
		{
			$grade_level_id = $_POST['grade_level_id'];
			$c_opt = '<option value=""></option>';

			$get_class_data = $this->class->getclassData($grade_level_id, "");
			foreach($get_class_data as $r_c){
				$c_opt .= '<option value="'. $r_c->classid .'">'. $r_c->class_name .'</option>';
			}
			
			echo $c_opt;
		}

		//get student data-----------------------------------------
		public function get_student_data()
		{
			$class_id = $_POST['class_id'];
			$where = "";
			if($class_id != ""){
				$where .= " AND sc.classid = ". $class_id;
			}
			$s_opt = '<option value=""></option>';

			$i = 1;
			$get_student_data = $this->mode->getStudentInformation($where);
			foreach($get_student_data as $r_s){
				$s_opt .= '<option value="'. $r_s->studentid .'">'. $i .'. '. $r_s->last_name .' '. $r_s->first_name .'</option>';
				$i++;
			}
			
			echo $s_opt;
		}

		//get student information----------------------------------
		public function get_student_information()
		{
			$program_id = $this->input->post('program_id');
			$school_level_id = $this->input->post('school_level_id');
			$grade_level_id = $this->input->post('grade_level_id');			
			$academic_year_id = $this->input->post('academic_year_id');
			$class_id = $this->input->post('class_id');
			$student_id = $this->input->post('student_id');
			$semester = $this->input->post('semester');
			$data = array();

			$where = "";
			if($program_id != ""){
				$where .= " AND sp.programid = ". $program_id;
			}
			if($school_level_id != ""){
				$where .= " AND sl.schlevelid = ". $school_level_id;
			}
			if($academic_year_id != ""){
				$where .= " AND sy.yearid = ". $academic_year_id;
			}
			if($class_id != ""){
				$where .= " AND sc.classid = ". $class_id;
			}
			if($student_id != ""){
				$where .= " AND s.studentid = ". $student_id;
			}

			$stu_data = array();			
			$get_student_info = $this->mode->getStudentInformation($where);

			foreach($get_student_info as $r_stu){
				$eval_data = array();
				$get_student= $this->mode->checkStudentEvaluate($academic_year_id, $class_id, $r_stu->studentid, $semester);
				if(count($get_student) > 0){
					foreach($get_student as $getRows){
						$eval_data[] = $getRows;
					}
				}else{
					$get_evaluate_name= $this->mode->getEvaluateName();				
					foreach($get_evaluate_name as $rows){
						$eval_data[] = $rows;
					}
				}	
				$r_stu->{"evaluate"} = $eval_data;
				$stu_data[] = $r_stu;
			}

			$data[] = $stu_data;

			header('Content-Type: application/json');
			echo json_encode($data);
		}

		//save evaluate for each student----------------------------
		public function saveStudentEvaluate(){
	        $saveData = $this->mode->saveEvaluate();
	        header("Content-type:text/x-json");
			$arrJson["res"]=$saveData;
	        echo json_encode($arrJson);
	        exit();
		}

	}
?>