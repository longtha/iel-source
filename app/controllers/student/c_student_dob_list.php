<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class c_student_dob_list extends CI_Controller{
	    protected $thead;
		protected $idfield;
	    function __construct() {
	        parent::__construct();
	        $this->load->model("student/m_student_dob_list","studob");
	        $this->thead = array("No" => 'no',
	        					"Photo"=>'Photo',
                                "Student Name" => 'student_num',
                                "Date Of Birth"=> 'dob',
                                "Program"=>'program',
                                "School Level" => 'sch_level',
                                "Year" => 'sch_year',
								"Rang Level​"=> 'rangelevelname',
								"Class"=> 'class_name'
	                            );
	        $this->idfield = "studentid";
	    }
	    function index() {
	        $data['idfield'] = $this->idfield;
        	$data['thead'] = $this->thead;
        	$data['getprogram']=$this->studob->getprograms();
        	$data['schooinfor']=$this->studob->schooinfor();
	        $this->load->view('header',$data);
	        $this->load->view('student/v_student_dob_list',$data);
	        $this->load->view('footer',$data );
	    }
	    // get_schlevel ---------------------------------------------------
	    function get_schlevel(){
			$programid = $this->input->post('programid');
			$get_schlevel = $this->studob->get_schlevel($programid);
			header("Content-type:text/x-json");
			echo ($get_schlevel);
		}
		//yera ------------------------------------------------------------
		function get_schyera(){
			$schlevelids = $this->input->post('schlevelids');
			$get_schyera = $this->studob->get_schyera($schlevelids);
			header("Content-type:text/x-json");
			echo ($get_schyera);
		}
		//from ranglavel --------------------------------------------------
		function get_schranglavel(){
			$yearid = $this->input->post('yearid');
			$get_schranglavel = $this->studob->get_schranglavel($yearid);
			header("Content-type:text/x-json");
			echo ($get_schranglavel);
		}
		// from class -----------------------------------------------------
		function get_schclass(){
			$schlevelids = $this->input->post('schlevelids');
			$get_schclass = $this->studob->get_schclass($schlevelids);
			header("Content-type:text/x-json");
			echo ($get_schclass);
		}

	     // getdata -----------------------------------------------------
	    function Getdatastudent(){
			$total_display =$this->input->post('sort_num');
			$this->green->setActiveRole($this->input->post('roleid'));
	  		$total_display = $this->input->post('total_display');
	  		$sortby=$this->input->post("sortby");
			$sorttype=$this->input->post("sorttype");
			$student_num =$this->input->post("student_num");
			$english_name =$this->input->post("english_name");
			$khmer_name =$this->input->post("khmer_name");
			$gender =$this->input->post("gender");
			$schoolid =$this->input->post("schoolid");
			$programid =$this->input->post("programid");
			$schlevelids =$this->input->post("schlevelids");
			$yearid =$this->input->post("yearid");
			$rangelevelid =$this->input->post("rangelevelid");
			$classid =$this->input->post("classid");
			$from_dob =$this->input->post("from_dob");
			$to_dob =$this->input->post("to_dob");
	  		$where='';
			$sortstr="";
				if($student_num!= ""){
		    		$where .= "AND v_student_dob_list.student_num LIKE '%".$student_num."%' ";
		     	}
				if($english_name!= ""){
		    		$where .= "AND  CONCAT(first_name,' ',last_name) LIKE '%".$english_name."%' ";
		    	}
		    	if($khmer_name!= ""){
		    		$where .= "AND  CONCAT(first_name_kh,' ',last_name_kh) LIKE '%".$khmer_name."%' ";
		    	}
		 		if($gender!= ""){
		    		$where .= "AND  v_student_dob_list.gender = '".$gender."' ";
		    	}
		    	if($schoolid!= ""){
		    		$where .= "AND  v_student_dob_list.schoolid = '".$schoolid."' ";
		    	}
		     	if($programid!= ""){
		    		$where .= "AND  v_student_dob_list.programid = '".$programid."' ";
		    	}
		    	if($schlevelids!= ""){
		    		$where .= "AND  v_student_dob_list.schlevelid = '".$schlevelids."' ";
		    	}
		    	if($yearid!= ""){
		    		$where .= "AND  v_student_dob_list.year = '".$yearid."' ";
		    	}
				if($rangelevelid!= ""){
		    		$where .= "AND  v_student_dob_list.rangelevelid = '".$rangelevelid."' ";
		    	}
		    	if($classid!= ""){
		    		$where .= "AND  v_student_dob_list.classid = '".$classid."' ";
		    	}
				if($from_dob != ''){
				 	$where .= "AND v_student_dob_list.dob >= '".$this->green->formatSQLDate($from_dob)."'";    
				}
				if($to_dob != ''){
					 $where .= "AND v_student_dob_list.dob <= '".$this->green->formatSQLDate($to_dob)."'";     
				}

		        (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

			$sqr="SELECT * FROM
						v_student_dob_list 
					WHERE 1 = 1 {$where}";
			
			$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_student_dob_list WHERE 1 = 1 {$where}")->row()->numrow;
				$getperpage=0;
				if($total_display==''){
					$getperpage=10;
				}else{
					$getperpage=$total_display;
				}
				$paging    = $this->green->ajax_pagination($total_row,site_url()."student/c_student_dob_list",$getperpage,"icon");
				//return "$sql limit {$paging['start']}, {$paging['limit']}";
				if($sortstr!=""){
					$sqr.=$sortstr;
				}
				$getlimit=10;
				if($paging['limit']!=''){
					$getlimit=$paging['limit'];
				}
			$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
				$j='';
				$table='';
				if(count($data) > 0){
					foreach($data as $row){  //$this->db->query($sqr)->result() as $row	
						$j++;
						$have_img = base_url()."assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg';		
						$no_imgs = base_url()."assets/upload/students/NoImage.png";	
						$img = '<img src="'.$no_imgs.'" class="img-circle img-responsive-" alt="No Image"  style="width:70px;height:70px;">';
							if (file_exists(FCPATH . "assets/upload/students/".$row->year.'/'.$row->studentid.'.jpg')) {				
								$img = '<img src="'.$have_img.'" class="img-circle img-responsive-" alt="No Image" style="width:70px;height:70px;">';
							}
						$table.= "<tr>
									<td>".$j."</td>
									<td>".$img."</td>
									<td><strong>".$row->student_num."</strong>
										</br>".$row->last_name_kh.'  '.$row->first_name_kh."
										</br>".$row->first_name.'  '.$row->last_name."
										</br>".$row->gender."
									</td>
									<td>".$this->green->convertSQLDate($row->dob)."</td>
									<td>".$row->program."</td>
									<td>".$row->sch_level."</td>
									<td>".$row->sch_year."</td>
									<td>".$row->rangelevelname."</td>
									<td>".$row->class_name."</td>
									<td class='remove_tag no_wrap'>";

						$table.= " </td>
							 	</tr> ";
					}
				}else{
					$table.= "<tr>
								<td colspan='14' style='text-align:center;'> 
									We did not find anything to show here... 
								</td>
							</tr>";
				}


			$arr['data']=$table;
			header('Content-Type:text/x-json');
			$arr['body']=$table;
			$arr['pagination']=$paging;
			echo json_encode($arr); 
			die();
		}

	}