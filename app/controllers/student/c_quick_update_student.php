<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_quick_update_student extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("student/m_quick_update_student","quick_up");
		$this->thead=array("No"=>'No',
							 "Photo"=>'',
							 "StudentID"=>'student_num',
							 "Full Name"=>'last_name',
							 "Gender"=>'gender',						 
							 "Class"=>'class_name',
							 "Action"=>'Action'							 	
							);
	}

	function index(){
		$data['opt_program'] = $this->get_program();
		$data['s_class']=$this->s_class();
		$data['thead']=	$this->thead;
		$this->load->view('header');
		$this->load->view('student/v_quick_update_student',$data);
		$this->load->view('footer');
	}
	function get_program(){
		$optionprogram="";
		$query = $this->db->get('sch_school_program')->result();
		foreach($query as $pro_id ){
			$optionprogram .='<option value="'.$pro_id->programid.'">'.$pro_id->program.'</option>';
		}
		return $optionprogram;		
	}

	function s_class(){
		$s_class="";
		$s_class .='<option value=""></option>';
		foreach($this->quick_up->s_class() as $id ){			
			$s_class .='<option value="'.$id->classid.'">'.$id->class_name.'</option>';
		}
		return $s_class;
	}

	function school_level(){
		$option_level="";
		$program = $this->input->post('program');
		$option_level.='<option value=""></option>';
		foreach($this->quick_up->getsch_level($program) as $id ){
			$option_level .='<option value="'.$id->schlevelid.'">'.$id->sch_level.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['schlevel'] = $option_level;
		echo json_encode($arr_success);
	}

	function get_gradlevel(){
		$grad_level="";
		$schoolid = $this->session->userdata('schoolid');	
		$programid = $this->input->post("program");
		$schlevelid = $this->input->post("sch_level");
		$grad_level .='<option value=""></option>';
		foreach($this->quick_up->getgradelevel($schoolid,$programid,$schlevelid) as $id ){
			$grad_level .='<option value="'.$id->grade_levelid.'">'.$id->grade_level.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['gradlevel'] = $grad_level;
		echo json_encode($arr_success);
	}

	function get_years(){
		$schoolid = $this->session->userdata('schoolid');	
		$sch_program = $this->input->post("program");
		$schlevelid = $this->input->post("sch_level");
		$option_year ='';
		$option_year .='<option value=""></option>';
		foreach ($this->quick_up->getschoolyear($sch_program,$schoolid,$schlevelid) as $row){
			$option_year .='<option value="'.$row->yearid.'">'.$row->sch_year.'</option>';
		}		
		header("Content-type:text/x-json");
		$arr_success['schyear'] = $option_year;
		echo json_encode($arr_success);
		
	}

	function get_class(){
		$schoolid = $this->session->userdata('schoolid');	
		$grad_level = $this->input->post("grad_level");
		$schlevelid = $this->input->post("sch_level");
		$class="";
		$class .='<option value=""></option>';
		foreach($this->quick_up->get_class($schoolid,$grad_level,$schlevelid) as $id ){			
			$class .='<option value="'.$id->classid.'">'.$id->class_name.'</option>';
		}
		header("Content-type:text/x-json");
		$arr_success['getclass'] = $class;
		echo json_encode($arr_success);  
	}

	function get_student_list(){
		$table='';
		$i=1;
		$sql  = $this->quick_up->get_student_list();
		foreach($this->db->query($sql['sql'])->result() as $row){			
			$no_imgs = base_url()."assets/upload/students/NoImage.png";	
			$have_img = base_url()."assets/upload/students/".$row->yearid.'/thumbs/'.$row->studentid.'.jpg';
			$img = '<img src="'.$no_imgs.'" class="img-circle" alt="No Image" style="width:70px !important; height:70px !important;">';
			if (file_exists(FCPATH . "assets/upload/students/".$row->yearid.'/thumbs/'.$row->studentid.'.jpg')) {				
				$img = '<img src="'.$have_img.'" class="img-circle" alt="No Image" style="width:70px !important; height:70px !important;">';
			}
			$table.= "<tr>
							<td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
							<td>".$img."</td>	
							<td class='student_num'>".$row->student_num."</td>
							<td class='last_name'>
								<span>".$row->last_name_kh." ".$row->first_name_kh."</span><br>
								<span>".$row->last_name." ".$row->first_name."</span>
							</td>
							<td class='gender'>
								<span class='data_gender'>".ucwords($row->gender)."</span>
								<div class='col-sm-8'>
								<select  class='form-control hide_controls gender_id' name='gender_id' id='gender_id'>
									<option value='male' ".($row->gender=="male"?"selected":"").">Male</option>
									<option value='female' ".($row->gender=="female"?"selected":"").">Female</option>
								</select>
								</div>
								<div div='col-sm-4'>
								<a><img class='hide_controls' rel='".$row->studentid."' onclick='hide_select(event);' src='".base_url('assets/images/icons/delete.png')."' style='margin-top: 10px;'/></a>
								</div>
							</td>
							<td class='class_name'>".$row->class_name."</td>		 
							<td class='remove_tag'>				
							<a class='hide_edit'><img rel='".$row->studentid."' onclick='up_gender(event);'  src='".base_url('assets/images/icons/edit.png')."'/></a>
							<a class='hide_controls'><img rel='".$row->studentid."' onclick='save_gender(event);' src='".base_url('assets/images/icons/save.png')."'/></a>
							</td>
						</tr>";								 
			$i++;	 
		}
		$paging = $sql['paging'];
		header("Content-type:text/x-json");
		$arr['tr_data']=$table;
		$arr['pagina']= $paging;		
		echo json_encode($arr);	

	}

	function save_gender(){
		echo $this->quick_up->save_gender();
	}

	function up_gender(){
		$studentid=$this->input->post('studentid');
		$row=$this->db->where('studentid',$studentid)->get('v_student_profile')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}

}