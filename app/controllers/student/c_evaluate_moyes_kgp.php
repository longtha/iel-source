<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class c_evaluate_moyes_kgp extends CI_Controller{
	    protected $thead;
		protected $idfield;
		function __construct() {
	        parent::__construct();
	        $this->load->model("student/m_evaluate_moyes_kpg","eval");
	        $this->idfield = "studentid";
			$this->load->model('school/modsetupexam', 'e');

			$this->load->model('school/program', 'p');
			$this->load->model('school/schoolyearmodel', 'y');
			$this->load->model('school/schoolinformodel', 'info');                
			$this->load->model('school/schoollevelmodel', 'level');
	
			$this->load->model('school/gradelevelmodel', 'g');
		
	    }
	    
	    function index() {
	        // $data['idfield'] = $this->idfield;
	        // $data['className']=$this->eval->allclass();
			
			$data=array("idfield"=>$this->idfield,"className"=>$this->eval->allclass());
	        $this->load->view('header',$data);
	        $this->load->view('student/v_evaluate_moyes_kgp',$data);
	        $this->load->view('footer',$data );
	    }
		
		 function getClassbygradeid(){
			$schlevelid=$this->input->post('schlevelid');
			$gradeid=$this->input->post('gradeid');
			// print_r($this->eval->allclass_gradeid($schlevelid,$gradeid));
			
			foreach ($this->eval->allclass_gradeid($schlevelid,$gradeid) as $row) {
				echo "<option value='".$row->classid."'>".$row->class_name."</option>";
			}
		}
		
		function getSubid(){
					$schlevelid=$this->input->post('schlevelid');
					$yearid=$this->input->post('yearid');	
						echo "<option value=''>--All--</option>";				
					foreach ($this->eval->getsubtypeid($schlevelid,$yearid) as $row) {
						echo "<option value='".$row->subj_type_id."'>".$row->subject_type."</option>";
					}
				}
				
	    function getmonthclass(){
			$classid=$this->input->post('classid');
			$yearid=$this->input->post('yearid');
			foreach ($this->eval->getmonthclass($classid,$yearid) as $row) {
				echo "<option value='$row[transno]'>$row[month]</option>";
			}
		}

		function getteacher(){
			$classid=$this->input->post('classid');
			$yearid=$this->session->userdata('year');
			if(count($this->eval->getteacherbyclass($classid,$yearid))>0)
				foreach ($this->eval->getteacherbyclass($classid,$yearid) as $t) {
					echo "<tr>
							<td>".$t->last_name.' '.$t->first_name."</td>
						</tr>";
				}
			else
				echo "<label style='color:red'>No Teacher in this Class</label>";
		}

		function getmonth(){
			$month=array(array('month'=>'Jan'),
	    				array('month'=>'Feb'),
	    				array('month'=>'Mar'),
	    				array('month'=>'Apr'),
	    				array('month'=>'May'),
	    				array('month'=>'Jun'),
	    				array('month'=>'Jul'),
	    				array('month'=>'Aug'),
	    				array('month'=>'Sep'),
	    				array('month'=>'Oct'),
	    				array('month'=>'Nov'),
	    				array('month'=>'Dec'));
			foreach ($month as $months) { 	   		
				echo "<option value='".$months['month']."'>".$months['month']."</option>";
			 }
		}
		
		function Fgetm_s_y(){
				$is_mes_year=$this->input->post("is_mes_year");
				$schoolid=$this->input->post("schoolid");
				$schlevelid=$this->input->post("schlevelid");
				$yearid=$this->input->post("yearid"); 
				$result="";
				$sql1="";
				
				if($is_mes_year==1){
					$result=array(array('month'=>'Jan'),
									array('month'=>'Feb'),
									array('month'=>'Mar'),
									array('month'=>'Apr'),
									array('month'=>'May'),
									array('month'=>'Jun'),
									array('month'=>'Jul'),
									array('month'=>'Aug'),
									array('month'=>'Sep'),
									array('month'=>'Oct'),
									array('month'=>'Nov'),
									array('month'=>'Dec')
								);
					$tr='<tr>';
					$i=0;
					$po="";
					foreach ($result as $months) {
						$tr.='<td align="center" style="text-align:center" class="cl_tmap cl_month" attr_month='.$months['month'].'>		
								<button style="height:40px; width:40px;" >'.($months['month']=='Oct'?'<span style="color:red">'.$months['month'].'</span>':'<span style="color:blue">'.$months['month'].'</span>').'</button>
								</td>	
								';
								$i++;
						if($i%12==0){
								$tr.='</tr><tr>';
						}			
					}
						$tr.='</tr>' ;
					$table='<table>'.$tr.'</table>';
					
					header("content-type:text/x-json");
					$arr_='<select class="po_examtype form-control" id="po_examtype">'.$po.'</select>';
					
					$arr_mesure = array("tr"=>$table,"is_mes_year"=>$is_mes_year);				
					echo json_encode($arr_mesure);
													
				}else{
				if($is_mes_year==3){
					
				$sql1= "SELECT
							sch_school_year.yearid as id,
							sch_school_year.sch_year as name
							FROM
							sch_school_year
							WHERE 1=1
						AND yearid='".$yearid."'
						AND schlevelid='".$schlevelid."'
						";	
					// echo $sql1;	
				}elseif($is_mes_year==2){
				$sql1= "SELECT
						sch_school_semester.semesterid as id,
						sch_school_semester.semester as name
						FROM
						sch_school_semester
						WHERE 1=1
						AND yearid='".$yearid."'
						AND schlevelid='".$schlevelid."'
						";
						// echo $sql1;
				}
			// echo $sql1;
			
				if(count($sql1)>0){
					$result = $this->db->query($sql1)->result();	
						$po="";			
					foreach($result as $row_header){				
						$po.='<option value="'.$row_header->id.'">'.$row_header->name.'</option>';
					}
					header("content-type:text/x-json");
					$arr_='<select class="po_examtype form-control" id="po_examtype">'.$po.'</select>';
					
					$arr_mesure = array("tr"=>$arr_,"is_mes_year"=>$is_mes_year);				
					echo json_encode($arr_mesure);
				}
				
			}// }else{
			
			die();
		}
	 	
		function FgetMonthBC(){
			$month=array(array('month'=>'Jan'),
	    				array('month'=>'Feb'),
	    				array('month'=>'Mar'),
	    				array('month'=>'Apr'),
	    				array('month'=>'May'),
	    				array('month'=>'Jun'),
	    				array('month'=>'Jul'),
	    				array('month'=>'Aug'),
	    				array('month'=>'Sep'),
	    				array('month'=>'Oct'),
	    				array('month'=>'Nov'),
	    				array('month'=>'Dec'));
						$tr='<tr>';
						$i=0;
						
						foreach ($month as $months) {
						$schlevelid=$_REQUEST['schlevelid'];
						$yearid=$_REQUEST['yearid'];
						$classname=$_REQUEST['classname'];
						$examtypecode=$_REQUEST['examtypecode'];
						
						$tr.='<td align="center" style="text-align:center" class="cl_tmap">		
						<button style="height:40px; width:40px;" >'.($months['month']=='Oct'?'<span style="color:red">'.$months['month'].'</span>':'<span style="color:blue">'.$months['month'].'</span>').'</button>
						</td>	
						';
						$i++;
						if($i%12==0){
						$tr.='</tr><tr>';
						}			
						}
						$tr.='</tr>' ;
						echo '<table>'.$tr.'</table>';
			
		}
		function Fcstu_inclass(){			
				// $unitofmeasur  = $_POST['unitofmeasur1'];
				
				
				$classid="";
				$subjectgroup="";
				$wsubje_grop="";
				
				$subjectgroup=$this->input->post('subjectgroup');
				$classname=$this->input->post('classname');
				
				if($subjectgroup!=""){
					$wsubje_grop.=" AND sch_subject_detail.subjecttypeid='".$subjectgroup."'";
				}
				
				if($classname!=""){
					$classid.=" AND v_scores_entry.classid='".$classname."'";
				}
				
				$thead1="";
				$thead1.='<thead><tr>
						 <td rowspan="2">No</td>
						 <td rowspan="2">ID</td>
						 <td rowspan="2" align="center">Name</td>
						 <td rowspan="2" align="center">ចំនួនអវត្តមាន</td>
						 <td rowspan="2" align="center">មានច្បាប់</td>';
					$sql1= "SELECT
							sch_subject_detail.subj_grad_id,
							sch_subject_detail.subjecttypeid as subj_type_id,
							sch_subject_detail.subjectid,
							sch_subject_detail.grade_levelid,
							sch_subject.`subject`,
							COUNT(
									sch_subject_detail.subjectid
								) AS sub_conid,
							sch_subject_type.subject_type
							FROM
							sch_subject_detail
							INNER JOIN sch_subject ON sch_subject_detail.subjectid = sch_subject.subjectid
							INNER JOIN sch_subject_type ON sch_subject_detail.subjecttypeid = sch_subject_type.subj_type_id
							WHERE
								exam_type_id = 1
							GROUP BY
								sch_subject_detail.subjectid
	
							";
						// echo $sql1;
					$result = $this->db->query($sql1)->result();
					
					foreach($result as $row_header){				
						$thead1.='<th style="color:red;" colspan="'.$row_header->sub_conid.'" align="center" class="th_head1"><div align="center" style="text-align:center">'.$row_header->subject_type.'</div></th>';
					}
				$thead1.="</tr><tr>";
				//-----------
				$sql2= "SELECT
							sch_subject_detail.subj_grad_id,
							sch_subject_detail.subjecttypeid as subj_type_id,
							sch_subject_detail.subjectid,
							sch_subject_detail.grade_levelid,
							sch_subject.`subject`,
							COUNT(
									sch_subject_detail.subjectid
								) AS sub_conid,
							sch_subject_type.subject_type
							FROM
							sch_subject_detail
							INNER JOIN sch_subject ON sch_subject_detail.subjectid = sch_subject.subjectid
							INNER JOIN sch_subject_type ON sch_subject_detail.subjecttypeid = sch_subject_type.subj_type_id
							WHERE
								exam_type_id = 1
							GROUP BY
								sch_subject_detail.subjectid						
							";
				// echo $sql2;			
				$result2 = $this->db->query($sql2)->result();
				$thead2="";	
					foreach($result2 as $row_header2){
						
						$sql3 = "SELECT
									v_subject_detail.subjecttypeid,
									v_subject_detail.subject
									FROM
									v_subject_detail
									where v_subject_detail.subjecttypeid='".$row_header2->subj_type_id."'
									AND exam_type_id = 1
									";
									
						$result3 = $this->db->query($sql3)->result();	
										
						foreach($result3 as $row_header3){
									$thead2.='<th style="color:blue;" class="th_head3">'.$row_header3->subject.'</th>';
								}
					}
				$thead2.="</tr></thead>";
				//-----------
				
				$sql4 = "SELECT
										v_scores_entry.student_num,
										v_scores_entry.studentid,
										v_scores_entry.last_name_kh,
										v_scores_entry.classid,
										v_scores_entry.first_name_kh
										FROM
										v_scores_entry
										WHERE 1=1 {$classid}
										ORDER BY student_num";
										
				$result4 = $this->db->query($sql4)->result();
				$i=1;
				$tabindex=0;
				$tr3="";
				$action='return isNumberKey(event);';
				foreach($result4 as $row_header4){	
					//-----------
				$sql2= "SELECT
							sch_subject_detail.subj_grad_id,
							sch_subject_detail.subjecttypeid as subj_type_id,
							sch_subject_detail.subjectid,
							sch_subject_detail.grade_levelid,
							sch_subject.`subject`,
							COUNT(
									sch_subject_detail.subjectid
								) AS sub_conid,
							sch_subject_type.subject_type
							FROM
							sch_subject_detail
							INNER JOIN sch_subject ON sch_subject_detail.subjectid = sch_subject.subjectid
							INNER JOIN sch_subject_type ON sch_subject_detail.subjecttypeid = sch_subject_type.subj_type_id
							WHERE
								exam_type_id = 1
							GROUP BY
								sch_subject_detail.subjectid
							";
					$td3="";
					$arr_subjid="";
					$subtype="";
					$arr_score="";
					
				$result2 = $this->db->query($sql2)->result();
					foreach($result2 as $row_header2){
						// $subtype.=$row_header2['subject_type']."###";$row_header2->subj_type_id
						$sql3 = "SELECT
									v_subject_detail.subjecttypeid,
									v_subject_detail.subject,
									v_subject_detail.subjectid
									FROM
									v_subject_detail
									where v_subject_detail.subjecttypeid='".$row_header2->subj_type_id."'
									AND exam_type_id = 1
									";
						// echo $sql3; exit();
						
						$result3 = $this->db->query($sql3)->result();						
						foreach($result3 as $row_header3){
									$w="";
									$arr_subjid.=$row_header3->subjectid."###";
									$arr_score.=$row_header3->subjectid;
									
									if($row_header4->studentid!=""){
										$w.=" AND studentid ='".($row_header4->studentid)."'";
									}
									
									if($row_header3->subjectid!=""){
										$w.=" AND subjectid ='".($row_header3->subjectid)."'";
									}
									if($row_header3->subjecttypeid!=""){
										$w.=" AND subjecttypeid ='".($row_header3->subjecttypeid)."'";
									}
									
									$getsqlOne="SELECT
												sch_score_entryed.score_num
												FROM
												sch_score_entryed
												WHERE 1=1 {$w}										
												";
									// echo $getsqlOne; 
									
									$scored="";
											
										if($this->db->query($getsqlOne)->row()){
											$scored = $this->db->query($getsqlOne)->row()->score_num;
										}	
									 
									$td3.='<th style="color:blue; bordercolor:#0000FF;">
												<input type="hidden" class="subj_id'.$row_header4->studentid.'____'.$row_header3->subjectid.'" value="'.$row_header3->subjectid.'">
												<input style="text-align:right" onkeypress="return isNumberKey(event);" type="text" subjtypeid="'.$row_header3->subjecttypeid.'" class="form-control input-sm txtmention score'.$row_header4->studentid.'____'.$row_header3->subjectid.' '.$row_header4->studentid.'" value="'.($scored==""?"0":$scored).'" tabindex="'.$tabindex.'"/>
																				
											</th>';
											$tabindex++;
								}
								
					}
					$tr3.='<tr>
								<td style="display:none"><input type="checkbox"  checked="checked" class="ch_pro" arr_subjid="'.$arr_subjid.'" arr_score="'.$arr_score.'" value="'.$row_header4->studentid.'"></td>
								<td style="display:none-">'.str_pad($i,2,"0",STR_PAD_LEFT).'</td>
								<td class="studentid_click" attr_stuid_click="'.$row_header4->studentid.'">
								'.$row_header4->student_num.'
								</td>
								<td style="width:20% !important;">/'.$row_header4->last_name_kh.'</td>
								<td>
									<input type="text" class="absenty_total form-control input-sm txtmention" style="size:5px;">
								</td>
								<td>
									<input type="text" class="absenty_with_permison form-control input-sm txtmention" style="size:5px;">
								</td>
								'.$td3.'
								<td style="display:none"  class="score_total_line" id="score_total_line"></td>
				</tr>';
				//-----------
				$i++;
				}//while($row_mesure = DB_fetch_assoc($query_mesure)){ 4
				//-----------
				
				header("content-type:text/x-json");
				$arr_mesure = array();
				$arr_mesure['tr_mesure'] = $thead1.$thead2.$tr3;
				echo json_encode($arr_mesure);
				die();
	}// end public function Fcstu_inclass(){
	
		//==========================
	
	function Fsave_Data(){
		$attr_student = $_POST['attr_student'];	
		
		// print_r($attr_student);
			if(count($attr_student)>0){ // 
			foreach($attr_student as $val1){
				$da0 = array(									
							'classid' =>$val1['studen_code'],
							'studentid'=>$val1['studen_code'],
							'exam_type'=>$val1['studen_code'],
							'averag_coefficient' => $val1['studen_code'],
							'num_month' => $val1['studen_code'],									
							'yearid' => $val1['studen_code'],
							'schlevelid' => $val1['studen_code'],									
							'absenty_total' => $val1['absenty_total'],
							'absenty_with_permison' => $val1['absenty_with_permison'],
							'score_total_line' => $val1['score_total_line'],
							'grade_levelid' => $val1['studen_code']
							);
				$this->db->insert('sch_absenty_entried_kgp', $da0);
									
				//-------------------
				foreach($val1['arr_subjid'] as $val3){				
				$this->db->where('studentid',$val3['stuidline']);
				$this->db->where('subjectid',$val3['subjectid']);
				$this->db->where('subjecttypeid',$val3['subjtypeid']);
       			$this->db->delete('sch_score_entryed');
							
						if($val3['enterscore']>0){
						 $da = array(									
									'studentid' =>$val3['stuidline'],
									'subjectid'=>$val3['subjectid'],
									'subjecttypeid' => $val3['subjtypeid'],
									'score_num' => $val3['enterscore']
									);
							$this->db->insert('sch_score_entryed', $da);
						}
					}
				//------------------
			}//1
		}
	die();	
 }	
 
}
