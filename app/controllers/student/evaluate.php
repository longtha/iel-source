<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Evaluate extends CI_Controller{

		function __construct(){
			parent::__construct();	
			$this->load->model("student/modevaluate","evaluate");	
			$this->load->library('pagination');	
			
			$this->thead=array("No"=>'no',
								"Date"=>'se__date',
								"Student Name"=>'s__fullname',
								 "Class"=>'s__class_name',
								 'Action'=>'Action'
								);
			$this->semthead=array("No"=>'no',
								"Student Name"=>'s__last_name',
								"Class"=>'se__classid',
								 "Month"=>'month',
								 "Semester"=>'semester',
								 "Exam Average"=>'se__sem_exam_avg',
								 "Month Average"=>'se__months_avg',
								 "Average"=>'se__sem_avg',
								 "Mention"=>'se__sem_mention',
								 "Result"=>'se__sem_result',
								 "Rank"=>'sem_rank',
								 'Preview'=>'preview'
								);
			$this->rethead=array("No"=>'no',
								"Student Name"=>'s__fullname',
								"Class"=>'s__class_name',
								 "Semester 1th"=>'semester_one',
								 "Semester 2nd"=>'semester_two',
								 "Average"=>'total',
								 "Mention"=>'mention',
								 "Result"=>'result'
								);
			$this->idfield="displanid";					
		}
		function index()
		{	
			$data['tdata']=$this->evaluate->getevaluate();
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluate";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/evaluate/evaluate_list', $data);
			$this->parser->parse('footer', $data);
		}
		function semesterlist(){
			$data['tdata']=$this->evaluate->getevasemester();
			$data['idfield']=$this->idfield;		
			$data['semthead']=	$this->semthead;
			$data['page_header']="Evaluate";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/evaluate/semester_list', $data);
			$this->parser->parse('footer', $data);
		}
		function getteacher(){
			$classid=$this->input->post('classid');
			$yearid=$this->session->userdata('year');
			if(count($this->evaluate->getteacherbyclass($classid,$yearid))>0)
				foreach ($this->evaluate->getteacherbyclass($classid,$yearid) as $t) {
					echo "<tr>
							<td>".$t->last_name.' '.$t->first_name."</td>
						</tr>";
				}
			else
				echo "<label style='color:red'>No Teacher in this Class</label>";
		}
		function resultlist(){
			$data['tdata']=$this->evaluate->getresult();
			$data['idfield']=$this->idfield;		
			$data['semthead']=	$this->rethead;
			$data['page_header']="Evaluate";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/evaluate/result_list', $data);
			$this->parser->parse('footer', $data);
		}
		function add(){	
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluate";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/evaluate/evaluate_form', $data);
			$this->parser->parse('footer', $data);
		}
		function edit($transno){
			$data['idfield']= $this->idfield;		
			$data['thead']=	$this->thead;
			$datas['data']= $this->evaluate->getedit($transno);
			$data['page_header']="Evaluate";			
			$this->parser->parse('header', $data);
			$this->load->view('student/evaluate/evaluate_form', $datas);
			$this->parser->parse('footer', $data);
		}
		function getschlevel(){
			$classid=$this->input->post('classid');
			$schlevelid=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
			echo $this->db->where('schlevelid',$schlevelid)->get('sch_school_level')->row()->is_vtc;
		}
		function preview($evaluateid){	
			$data['idfield']=$this->idfield;
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluate";
			$datas['eva']=$this->evaluate->getrow($evaluateid);
			$schlevel=$this->db->where('classid',$this->evaluate->getrow($evaluateid)->classid)->get('sch_class')->row()->schlevelid;
   			$isvtc=$this->db->where('schlevelid',$schlevel)->get('sch_school_level')->row()->is_vtc;
			$this->parser->parse('header', $data);
			
			if($isvtc==1)
				$this->load->view('student/evaluate/preview_vtc', $datas);
			else
				$this->load->view('student/evaluate/preview', $datas);
			$this->parser->parse('footer', $data);
		}
		function previewmulti($evaluateid){	
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluate";
			$datas['eva']=$this->evaluate->getrow($evaluateid);	
			$schlevel=$this->db->where('classid',$this->evaluate->getrow($evaluateid)->classid)->get('sch_class')->row()->schlevelid;
   			$isvtc=$this->db->where('schlevelid',$schlevel)->get('sch_school_level')->row()->is_vtc;		
			$this->parser->parse('header', $data);
			if($isvtc==1)
				$this->load->view('student/evaluate/preview_vtc', $datas);
			else
				$this->load->view('student/evaluate/preview', $datas);
			$this->parser->parse('footer', $data);
		}
		function getmonthclass(){
			$classid=$this->input->post('classid');
			$yearid=$this->input->post('yearid');
			foreach ($this->evaluate->getmonthclass($classid,$yearid) as $row) {
				echo "<option value='$row[transno]'>$row[month]</option>";
			}
		}
		function previewclass($transno){	
			$data['idfield']=$this->idfield;
			$data['page_header']="Evaluate";
			$isvtc=$this->db->query("SELECT 
										DISTINCT sl.is_vtc 
									FROM sch_student_evaluated e
									INNER JOIN sch_class c ON(e.classid=c.classid)
									INNER JOIN sch_school_level sl ON(c.schlevelid=sl.schlevelid)
									WHERE e.transno='$transno'
									")->row()->is_vtc;
			$datas['eval']=$this->evaluate->getrow('',$transno);
			$this->parser->parse('header', $data);
			if($isvtc==1)
				$this->load->view('student/evaluate/preview_vtc_class', $datas);
			else
				$this->load->view('student/evaluate/preview_class', $datas);
			$this->parser->parse('footer', $data);
		}
		function previewbook($evaluateid){	
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluate";
			$datas['eva']=$this->evaluate->getrow($evaluateid);			
			$this->parser->parse('header', $data);
			$this->load->view('student/evaluate/preview_book', $datas);
			$this->parser->parse('footer', $data);
		}
		function previewsem($semid){	
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluate";
			$datas['eva']=$this->evaluate->getsemrow($semid);			
			$this->parser->parse('header', $data);
			$this->load->view('student/evaluate/preview_sem', $datas);
			$this->parser->parse('footer', $data);
		}
		function getsub_type(){
			$classid 	   = $this->input->post('classid');
			$schoolid 	   = $this->session->userdata('schoolid');
			$evaluate_type = $this->input->post('evaluate_type');
			$month 		   = $this->input->post('month');
			$year 		   = $this->input->post('year');
			$promot_id	   = $this->input->post('promot_id');
			$mention 	   = array('A','B','C','D','E','F');
			$action 	   = '';
			if($month!='')
				$month=implode(',',$month);

			$schlevelid=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
			$isvtc=$this->db->where('schlevelid',$schlevelid)->get('sch_school_level')->row()->is_vtc;
			$count=$this->validateeva($year,$classid,$schoolid,$evaluate_type,$month);
			$trimester=0;
			if($count==0){
				if($evaluate_type!='Three Month')
					$action='return isNumberKey(event);';
				else
					$trimester=1;
				echo "<thead>
							<tr>
			        			<th rowspan='2' style='vertical-align: middle !important;'><input type='text' class='hide' value='$isvtc' id='schlevelids'/>No</th>
			        			<th rowspan='2' style='vertical-align: middle !important;'>Student</th>";
			        	// echo "<th rowspan='2' width='70' style='vertical-align: middle !important;'>Comment</th>";
		        //==================subject type========================
				foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
					echo "<th colspan='$sub->s_total' style='text-align:center !important;'>$sub->subject_type</th>";
				}
					echo "<th  rowspan='2' style='vertical-align: middle !important;' width='100'>Teacher Comment</th>
						  <!--<th  rowspan='2' style='vertical-align: middle !important;'>Save</th>-->
						</tr>
						<tr>";
				////=========================subject========================
				foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
					foreach ($this->evaluate->getsubject($classid,$sub->subj_type_id,$year,$trimester) as $subject) {
						echo "<th width='70'>$subject->short_sub</th>";
					}
				}
				echo "<th width='70' class='hide'>Forign</th>
						<th width='70' class='hide'>Khmer</th>
				</tr></thead>
						<tbody>";
				$i=1;
				
				// echo "<tr><td>";
				// echo "VTC:".$isvtc;
				// echo "Pro:".$promot_id;
				// echo "year:".$year;
				// echo "Class".$classid;
				// echo "</td></tr>";

				if ($isvtc==1){
					foreach ($this->evaluate->getstudent($classid,$year,$promot_id) as $std) {
						echo "<tr>
								<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
								<td><input class='form-control input-sm hide studentid' value='$std->studentid' name='studentid[]' id='studentid' />".$std->last_name.' '.$std->first_name."</td>";
								// echo "<td><input class='form-control input-sm cmd$std->studentid' rel='cmd$std->studentid' name='cmd[]' onclick='classcomment(event);'/></td>";
								foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
									foreach ($this->evaluate->getsubject($classid,$sub->subj_type_id,$year,$trimester) as $subject) {
										echo "<td>
												<input type='text' onkeypress='".$action."'  onkeypress='return isNumberKey(event);' class='form-control input-sm txtmention' rel='$subject->subjectid' name='$subject->subjectid[]' id='txtmention'/>
											</td>";
									}
								}
						echo "<td><input class='form-control input-sm teacher_kh_cmd$std->studentid' rel='$std->studentid' onclick='t_comment(event);' name='teacher_kh_cmd[]'/></td>
								<td class='hide'><input class='form-control input-sm hide teacher_f_cmd$std->studentid' rel='$std->studentid' onclick='t_comment(event);' name='teacher_f_cmd[]'/>
									<input class='teacher_fr_cmd$std->studentid' rel='$std->studentid' onclick='t_comment(event);' name='teacher_fr_cmd[]'/>
									<input class='technical_cmd$std->studentid' rel='$std->studentid' onclick='t_comment(event);' name='technical_cmd[]'/>
									<input class='sp_cmd$std->studentid' rel='$std->studentid' onclick='t_comment(event);' name='sp_cmd[]'/>
								</td>
								<!-- <td style='text-align:center !important;' width='50'>
									<a>
							 			<img onclick='saveinline(event);' src='".site_url('../assets/images/icons/save.png')."'/>
							 		</a>
							 	</td> -->
							</tr>";
							$i++;
					}
				}else{
					foreach ($this->evaluate->getstudent($classid,$year) as $std) {
						echo "<tr>
								<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
								<td><input class='form-control input-sm hide studentid' value='$std->studentid' name='studentid[]' id='studentid' />".$std->last_name.' '.$std->first_name."</td>";
								// echo "<td><input class='form-control input-sm cmd$std->studentid' rel='cmd$std->studentid' name='cmd[]' onclick='classcomment(event);'/></td>";
								foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
									foreach ($this->evaluate->getsubject($classid,$sub->subj_type_id,$year,$trimester) as $subject) {
										echo "<td>
												<input type='text' onkeypress='".$action."'  onkeypress='return isNumberKey(event);' class='form-control input-sm txtmention' rel='$subject->subjectid' name='$subject->subjectid[]' id='txtmention'/>
											</td>";
									}
								}
						echo "<td><input class='form-control input-sm teacher_kh_cmd$std->studentid' rel='$std->studentid' onclick='t_comment(event);' name='teacher_kh_cmd[]'/></td>
								<td class='hide'><input class='form-control input-sm hide teacher_f_cmd$std->studentid' rel='$std->studentid' onclick='t_comment(event);' name='teacher_f_cmd[]'/>
									<input class='teacher_fr_cmd$std->studentid' rel='$std->studentid' onclick='t_comment(event);' name='teacher_fr_cmd[]'/>
									<input class='technical_cmd$std->studentid' rel='$std->studentid' onclick='t_comment(event);' name='technical_cmd[]'/>
									<input class='sp_cmd$std->studentid' rel='$std->studentid' onclick='t_comment(event);' name='sp_cmd[]'/>
								</td>
								<!-- <td style='text-align:center !important;' width='50'>
									<a>
							 			<img onclick='saveinline(event);' src='".site_url('../assets/images/icons/save.png')."'/>
							 		</a>
							 	</td> -->
							</tr>";
							$i++;
					}
				}
				echo "</tbody>";

			}else{
				echo 'false';
			}
		}
		function validateeva($yearid,$classid,$schoolid,$evaluate_type,$month){
			$where='';
			if($evaluate_type=='month')
				$where.=" AND month='$month'";
			$count=$this->db->query("SELECT count(T.times) AS count FROM
											(SELECT count(evaluateid) as times
													FROM sch_student_evaluated
													WHERE yearid='$yearid' AND classid='$classid' AND evaluate_type='$evaluate_type' AND schoolid='$schoolid' {$where}
													GROUP BY transno) AS T")->row()->count;
			if($evaluate_type=='month')
				if($count>0)
					return 1;
				else
					return 0;
			if($evaluate_type=='semester')
				if($count>=2)
					return 1;
				else
					return 0;
			if($evaluate_type=='Three Month')
				if($count>=3)
					return 1;
				else
					return 0;

		}
		function save(){
            $transno=$this->evaluate->save();
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("student/evaluate/previewclass/".$transno."?m=$m&p=$p");
		}
		function getmonth(){
			$month=array(array('month'=>'Jan'),
	    				array('month'=>'Feb'),
	    				array('month'=>'Mar'),
	    				array('month'=>'Apr'),
	    				array('month'=>'May'),
	    				array('month'=>'Jun'),
	    				array('month'=>'Jul'),
	    				array('month'=>'Aug'),
	    				array('month'=>'Sep'),
	    				array('month'=>'Oct'),
	    				array('month'=>'Nov'),
	    				array('month'=>'Dec'));
			foreach ($month as $months) { 	   		
				echo "<option value='".$months['month']."'>".$months['month']."</option>";
			}
		}
		function saveinline(){
			$evaluateid='';
			$c_date=date('Y-m-d');
			$user=$this->session->userdata('user_name');
			$schoolid=$this->session->userdata('schoolid');
			$date=$this->green->convertSQLDate($this->input->post('date'));
			$upd=$this->input->post('upd');
	  		$eva_type=$this->input->post('eva_type');
	  		$classid=$this->input->post('classid');
	  		$month=$this->input->post('month');
	  		$eva_semester=$this->input->post('semester');
	  		$teacher_kh=$this->input->post('teacher_kh');
	  		$teacher_en=$this->input->post('teacher_en');
	  		$teacher_fr=$this->input->post('teacher_fr');
	  		$transno=$this->input->post('transno');
	  		$teacher_en_cmd=$this->input->post('teacher_en_cmd');
	  		$teacher_kh_cmd=$this->input->post('teacher_kh_cmd');
	  		$teacher_fr_cmd=$this->input->post('teacher_fr_cmd');
	  		$technical_cmd=$this->input->post('technical_cmd');
	  		$sp_cmd=$this->input->post('sp_cmd');
	  		$class_cmd=$this->input->post('class_cmd');
	  		$studentid=$this->input->post('studentid');
	  		$count=$this->evaluate->validate($studentid,$transno);
	  		$yearid=$this->db->select('year')->from('sch_student_enrollment')->where('studentid',$studentid)->where('classid',$classid)->get()->row()->year;
	  		$data=array('class_comment'=>$class_cmd,
							'studentid'=>$studentid,
							'classid'=>$classid,
							'yearid'=>$yearid,
							'date'=>$date,
							'month'=>$month,
							'eval_semester'=>$eva_semester,
							'evaluate_type'=>$eva_type,
							'kh_teacher_comment'=>$teacher_kh_cmd,
							'eng_teach_com'=>$teacher_fr_cmd,
							'eng_teach_name'=>$teacher_fr,
							'forign_teacher_comment'=>$teacher_en_cmd,
							'technic_com'=>$technical_cmd,
							'supple_com'=>$sp_cmd,
							'kh_teacher'=>$teacher_kh,
							'schoolid'=>$schoolid,
							'forign_teacher'=>$teacher_en
							);
	  		if($count>0){
	  			$data2=array('modified_date'=>$c_date,'modified_by'=>$user);
				$this->db->where('studentid',$studentid)->where('transno',$transno)->update('sch_student_evaluated',array_merge($data,$data2));
	  			$evaluateid=$this->db->where('transno',$transno)->where('studentid',$studentid)->get('sch_student_evaluated')->row()->evaluateid;
	  			$this->evaluate->clearmention($evaluateid);
	  		}else{
	  			$data2=array('created_date'=>$c_date,'created_by'=>$user,'transno'=>$transno);
				$this->db->insert('sch_student_evaluated',array_merge($data,$data2));
				$evaluateid=$this->db->insert_id();
				if($upd=='')
					$this->evaluate->updatetran($transno+1);
	  		}
	  		$level=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
	  		$data=array('evaluateid'=>$evaluateid,
	  			'level'=>$level,
	  			'yearid'=>$yearid);

			header("Content-type:text/x-json");
			echo json_encode($data);
		}
		function savemention(){
			$mention=$this->input->post('mention');
			$e_type=$this->input->post('eva_type');
			if($e_type!='Three Month')
				$mention=$this->evaluate->getstdmention($mention,$this->input->post('level'))->mention;
			$data=array('evaluateid'=>$this->input->post('evaluateid'),
						'subjectid'=>$this->input->post('subjectid'),
						'studentid'=>$this->input->post('studentid'),
						'mention'=>$mention,
						'type'=>2,
						'transno'=>$this->input->post('transno'),
						'score'=>$this->input->post('mention'));
			$this->db->insert('sch_student_evaluated_mention',$data);
			
		}
		function savesemester(){
			$studentid=$this->input->post('studentid');
			$transno=$this->input->post('transno');
			$classid=$this->input->post('classid');
			$yearid=$this->input->post('yearid');
			$months=$this->input->post('month');
			$month_tran=$this->input->post('tranmonth');
			$eval_semester=$this->input->post('semester');
			$totalscore=$this->input->post('total_score');
			$countsub=$this->input->post('countsub');
			$srtM=$month_tran;
			$month=explode(",", $srtM);
			// print_r($month);
			$month_num=count($month)-1;//get number of months
			for($i=0;$i<$month_num;$i++) {
				$each_months_avg+=($this->evaluate->getstdscore($month[$i],$studentid)->score)/$countsub;
				echo "$countsub each month :".$each_months_avg."/tr:".$month[$i]." /std/$studentid <br>";
			}
			$months_avg=number_format($each_months_avg/$month_num, 2, '.', '');//get month average
			$sem_exam_avg=number_format($totalscore/$countsub, 2, '.', '');//get average of semester exam
			$sem_avg=number_format(($months_avg+$sem_exam_avg)/2, 2, '.', '');
			$this->evaluate->savesemester($transno,$studentid,$classid,$yearid,$eval_semester,$months,$sem_exam_avg,$months_avg,$month_num,$sem_avg);
			
		}
		function rollback($transno){
			foreach ($this->db->where('transno',$transno)->get('sch_student_evaluated')->result() as $eva) {
				$this->evaluate->clearmention($eva->evaluateid);
			}
			$this->db->where('transno',$transno)->delete('sch_student_evaluated');
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("student/evaluate?m=$m&p=$p");
		}
		function gettransno(){
			echo $this->db->where('typeid',2)->get('sch_z_systype')->row()->sequence;
		}
		function deletes($transno){
			foreach ($this->db->where('transno',$transno)->get('sch_student_evaluated')->result() as $row) {
				$this->db->where('evaluateid',$row->evaluateid)->delete('sch_student_evaluated_mention');
				# code...
			}
			$this->db->where('transno',$transno)->delete('sch_stud_evalsemester');
			$this->db->where('transno',$transno)->delete('sch_student_evaluated');
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("student/evaluate?m=$m&p=$p");
		}
		
		function fillteacher(){
			$key=$_GET['term'];
			$data=$this->db->query("SELECT * 
								FROM sch_emp_profile emp
								INNER JOIN sch_emp_position p
								ON(emp.pos_id=p.posid)
								Where p.match_con_posid='tch' 
								AND (emp.last_name LIKE '%$key%' OR emp.first_name LIKE '%$key%')")->result();
			$array=array();
			foreach ($data as $row) {
				$array[]=array('value'=>$row->last_name.' '.$row->first_name,
								'id'=>$row->empid,
								'position'=>$row->position);
			}
		    echo json_encode($array);
		}
		function search(){
			if(isset($_GET['n'])){
					$name 		= $_GET['n'];
					$classid 	= $_GET['c'];
					$promot_id 	= $_GET['pro'];
					$schlevelid = $_GET['l'];
					$month 		= $_GET['mo'];
					$semester 	= $_GET['sem'];
					$year 		= $_GET['y'];
					$m 			= $_GET['m'];
					$p 			= $_GET['p'];
					$eva_type 	= $_GET['type'];
					$sort_num 	= $_GET['s_num'];
					$data['tdata']=$this->evaluate->search($name,$eva_type,$semester,$schlevelid,$classid,$month,$year,$sort_num,$m,$p);
					$data['idfield']=$this->idfield;		
					$data['thead']=	$this->thead;
					$data['page_header']="Evaluate";			
					$this->parser->parse('header', $data);
					$this->parser->parse('student/evaluate/evaluate_list', $data);
					$this->parser->parse('footer', $data);
			}else{
					$name 		= $this->input->post('name');
					$classid 	= $this->input->post('classid');
					$promot_id	= $this->input->post('promot_id');
					$schlevelid = $this->input->post('schlevelid');
					$month 		= $this->input->post('month');
					$semester 	= $this->input->post('semester');
					$eva_type 	= $this->input->post('eva_type');
					$yearid 	= $this->input->post('yearid');
					$sort_num 	= $this->input->post('sort_num');
					$sort 		= $this->input->post('sort');
					$m 			= $this->input->post('m');
					$p 			= $this->input->post('p');
					$sort 		= str_replace('__', '.', $sort);
					$i=1;
					$sch=$this->session->userdata('schoolid');
					$arrtran[]=array();
					$this->green->setActiveRole($this->input->post('roleid'));
					if($m!=''){
				        $this->green->setActiveModule($m);
				    }
				    if($p!=''){
				        $this->green->setActivePage($p); 
				    }
					foreach ($this->evaluate->search($name,$eva_type,$semester,$schlevelid,$classid,$month,$yearid,$sort_num,$m,$p,$sort,$promot_id) as $row) {
						$g_level=$this->db->where('classid',$row->classid)->get('sch_class')->row()->grade_levelid;
						$count_grade=$this->db->query("SELECT COUNT(grade_levelid) as c 
														FROM sch_level_subject_detail 
														WHERE grade_levelid='".$g_level."'
														AND year='".$row->yearid."'
														AND schoolid='".$sch."'")->row()->c;		
						echo "<tr>
							 	  <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td><td class='se__date'>";
                        if(!isset($arrtran[$row->transno])){
                            echo $this->green->formatSQLDate($row->date);
                        }
						echo " </td><td class='s__fullname'>".$row->last_name.' '.$row->first_name."</td>
								 <td class='s__class_name'>".$row->class_name."</td>";
								echo "<td class='remove_tag no_wrap'>";
									if($this->green->gAction("P")){	
											echo "<a>
												 	<img rel='".$row->evaluateid."' onclick='preview(event);' src='".site_url('../assets/images/icons/preview.png')."'/>
												 </a>
												";
									}
									if(!isset($arrtran[$row->transno])){
										if($this->green->gAction("P")){	
											 echo "<a>
												 	<img rel='".$row->transno."' onclick='previewclass(event);' src='".site_url('../assets/images/icons/preview_all.png')."'/>
												   </a>";
										}
										if($this->green->gAction("D")){	
											echo "<a>
												 	<img rel='".$row->transno."' onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
												  </a>";
										}
										if($this->green->gAction("U")){	
											echo "<a>
												 	<img rel='".$row->transno."' onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
												  </a>" ;
										}
									}	
									echo "</td></tr>";
									 	$i++;
									 	$arrtran[$row->transno]=$row->transno;
					}	
					echo "<tr class='remove_tag'>
							<td colspan='12' id='pgt'>
								<div style='margin-top:20px; width:10%; float:left;'>
								Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
												$num=50;
												for($i=0;$i<10;$i++){?>
													<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
													<?php $num+=50;
												}
											echo "</select>
								</div>
								<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
									<ul class='pagination' style='text-align:center'>
									 ".$this->pagination->create_links()."
									</ul>
								</div>
							</td>
						</tr> ";
			}			
		}
		function searchsemester(){
			if(isset($_GET['n'])){
					$name=$_GET['n'];
					$classid=$_GET['c'];
					$year=$_GET['y'];
					$semester=$_GET['se'];
					$schlevelid=$_GET['l'];
					$m=$_GET['m'];
					$p=$_GET['p'];
					$sort_num=$_GET['s_num'];
					$data['tdata']=$this->evaluate->searchsemester($name,$schlevelid,$classid,$year,$semester,$sort_num,$m,$p);
					$data['idfield']=$this->idfield;		
					$data['semthead']=	$this->semthead;
					$data['page_header']="Evaluate";			
					$this->parser->parse('header', $data);
					$this->parser->parse('student/evaluate/semester_list', $data);
					$this->parser->parse('footer', $data);
			}else{
					$name=$this->input->post('name');
					$classid=$this->input->post('classid');
					$schlevelid=$this->input->post('schlevelid');
					$sort_num=$this->input->post('sort_num');  
					$semester=$this->input->post('semester');
					$sort=$this->input->post('sort');
					$sort=str_replace('__', '.', $sort);
					$i=1;
					$yearid=$this->input->post('yearid');
					$sch=$this->session->userdata('schoolid');
					$m=$this->input->post('m');
					$p=$this->input->post('p');
					$this->green->setActiveRole($this->input->post('roleid'));
					if($m!=''){
				        $this->green->setActiveModule($m);
				    }
				    if($p!=''){
				        $this->green->setActivePage($p); 
				    }
					foreach ($this->evaluate->searchsemester($name,$schlevelid,$classid,$yearid,$semester,$sort_num,$m,$p,$sort) as $row) {

                        $this->db->where("classid",$row->classid);
                        $class_name=$this->db->get("sch_class")->row()->class_name;

							echo "<tr>
								 	 <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
									 <td class='s__fullname'>".$row->last_name .' '.$row->first_name ."</td>
									 <td class='s__class_name  '>".$class_name."</td>
									 <td class='month'>".$row->months."</td>
									 <td class='semester'>".$row->semester."</td>
									 <td class='se__sem_exam_avg'>".$row->sem_exam_avg."</td>
									 <td class='se__months_avg'>".$row->months_avg."</td>
									 <td class='se__sem_avg'>".$row->sem_avg."</td>
									 <td class='se__sem_mention'>".$row->sem_mention."</td>
									 <td class='se__sem_result'>".$row->sem_result."</td>
									 <td class='rank'>".$this->evaluate->getsemrank($row->transno,$row->semid)."</td>
									 <td class='remove_tag no_wrap'>";
								 	if($this->green->gAction("P")){	
										 	echo "<a>
											 		<img rel='".$row->semid."' onclick='preview(event);' src='".site_url('../assets/images/icons/preview.png')."'/>
											</a>";
									}
										echo "</td> 
									 	</tr>
									 ";
							 	$i++;
					}
						
					echo "<tr class='remove_tag'>
											<td colspan='12' id='pgt'>
												<div style='margin-top:20px; width:10%; float:left;'>
												Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
																
																$num=50;
																for($i=0;$i<10;$i++){?>
																	<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
																	<?php $num+=50;
																}
																
															echo "</select>
												</div>
												<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
													<ul class='pagination' style='text-align:center'>
													 ".$this->pagination->create_links()."
													</ul>
												</div>

											</td>
										</tr> ";
			}
			
		}
		function searchresult(){
			if(isset($_GET['n'])){
					$name=$_GET['n'];
					$classid=$_GET['c'];
					$year=$_GET['y'];
					$schlevelid=$_GET['l'];
					$sort_num=$_GET['s_num'];
					$s_result=$_GET['r'];
					$data['tdata']=$this->evaluate->searchsemester($name,$schlevelid,$classid,$year,$s_result,$sort_num);
					$data['idfield']=$this->idfield;		
					$data['thead']=	$this->semthead;
					$data['page_header']="Evaluate";			
					$this->parser->parse('header', $data);
					$this->parser->parse('student/evaluate/semester_list', $data);
					$this->parser->parse('footer', $data);
			}else{
					$name=$this->input->post('name');
					$classid=$this->input->post('classid');
					$schlevelid=$this->input->post('schlevelid');
					$sort_num=$this->input->post('sort_num');
					$s_result=$this->input->post('s_result');
					$sort=$this->input->post('sort');
					$sort=str_replace('__', '.', $sort);
					$i=1;
					$yearid=$this->input->post('yearid');
					$sch=$this->session->userdata('schoolid');
					foreach ($this->evaluate->searchresult($name,$schlevelid,$classid,$yearid,$s_result,$sort_num,$sort) as $row) {
							$semester=explode(',',$row->avg);
							$level=$this->db->where('classid',$row->classid)->get('sch_class')->row()->grade_levelid;
							$result='';
							$minscore=5;
							$avg=$row->total/2;
							if($row->total/2>=$minscore)
								$result='Pass';
							else
								$result='Fail';						
							echo "<tr>
								 	 <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
									 <td class='s__fullname'>".$row->fullname."</td>
									 <td class='s__class_name  '>".$row->class_name."</td>
									 <td class='semester_one'>".number_format($semester[0], 2, '.', '')."</td>
									 <td class='semester_two'>".number_format($semester[1], 2, '.', '')."</td>
									 <td class='total'>".number_format($avg, 2, '.', '')."</td>
									 <td class='mention'>".$this->evaluate->getstdmention($avg,$level)->mention."</td>
									 <td class='result'>".$result."</td>
								
									 
								 	</tr>
								 ";
							 	$i++;
					}
						
					echo "<tr class='remove_tag'>
											<td colspan='12' id='pgt'>
												<div style='margin-top:20px; width:10%; float:left;'>
												Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
																
																$num=50;
																for($i=0;$i<10;$i++){?>
																	<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
																	<?php $num+=50;
																}
																
															echo "</select>
												</div>
												<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
													<ul class='pagination' style='text-align:center'>
													 ".$this->pagination->create_links()."
													</ul>
												</div>

											</td>
										</tr> ";
			}
			
		}
}