<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VtcStar extends CI_Controller {
	protected $thead;
	function __construct()
	{
		parent::__construct();	
		$this->load->model('student/ModVtcstar','star');
		$this->load->library('pagination');
		$this->thead=array("No"=>'no',
							 "From Date"=>'from_date',
							 "To Date"=>'to_date', 
							 "Partner"=>'partner',
							 "Promotion"=>'promotion',
							 "Action"=>'Action'							 	
							);
	}
	public function index()
	{
		$yearid=$this->session->userdata('year');
		$this->load->view('header');
		$data['tdata']=$this->star->getstar($yearid);
		$data['thead']=$this->thead;
		$this->load->view('student/vtcstar/vtcstar_list',$data);
		$this->load->view('footer');
	}
	function save(){
		$this->star->save();
		$m='';
		$p='';
		if(isset($_GET['m'])){
		    $m=$_GET['m'];
		}
		if(isset($_GET['p'])){
		    $p=$_GET['p'];
		}
		redirect("student/vtcstar?m=$m&p=$p");
	}
	function delete($starid){
		$this->db->where('starid',$starid)->delete('sch_stud_vtcstar');
		$this->star->cleasdetail($starid);
		$m='';
		$p='';
		if(isset($_GET['m'])){
		    $m=$_GET['m'];
		}
		if(isset($_GET['p'])){
		    $p=$_GET['p'];
		}
		redirect("student/vtcstar?m=$m&p=$p");
	}
	function add(){
		$this->load->view('header');
		$this->load->view('student/vtcstar/form_vtcstar');
		$this->load->view('footer');
	}
	function edit($starid){
		$data['data']=$this->db->query("SELECT * 
										FROM sch_stud_vtcstar sv
										INNER JOIN sch_stud_partner pa
										ON(sv.partnerid=pa.partnerid)
										WHERE sv.starid='$starid'")->row();
		$this->load->view('header');
		$this->load->view('student/vtcstar/form_vtcstar',$data);
		$this->load->view('footer');
	}
	function preview($starid){
		$data['data']=$this->db->query("SELECT * 
										FROM sch_stud_vtcstar sv
										INNER JOIN sch_stud_partner pa
										ON(sv.partnerid=pa.partnerid)
										INNER JOIN sch_school_promotion sp
										ON(sv.promot_id=sp.promot_id)
										WHERE sv.starid='$starid'")->row();
		$this->load->view('header');
		$this->load->view('student/vtcstar/preview_star',$data);
		$this->load->view('footer');
	}
	function getstdtolist(){
		$from_date=$this->green->convertSQLDate($this->input->post('f_date'));
		$to_date=$this->green->convertSQLDate($this->input->post('e_date'));
		$partnerid=$this->input->post('partnerid');
		$promot_id=$this->input->post('promot_id');
		$classid=$this->input->post('classid');
		$studentid=$this->input->post('studentid');
		$stdarr=$this->input->post('stdarr');
		$error='';
		$data='';
		foreach ($this->star->getstdlist($promot_id,$classid,$studentid) as $std) {
			if(!isset($stdarr[$std->studentid])){
				$data.="<tr>
						<td>
							<input type='text' name='student_id[]' value='$std->studentid' id='student_id' class='hide student_id'/>".$std->fullname."</td>
						<td>
							<input type='text' name='class_id[]' value='$std->classid' id='class_id' class='hide'/>".$std->class_name."</td>
						<td style='text-align:center; vertical-align:middle;'>
							<img onclick='removerow(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
						</td>
				</tr>";
			}else{
			 	$error="Some Of student is Already Exist...!";
			}
		}
		$data=array("data"=>$data,'error'=>$error);
		header("Content-type:text/x-json");
		echo json_encode($data);

	}
	function fillpartner(){
			$key=$_GET['term'];
			$data=$this->db->select('*')
					->from('sch_stud_partner')
					->like('company_name',$key)->get()->result();
			$array=array();
			foreach ($data as $row) {
				$array[]=array('value'=>$row->company_name,
								'id'=>$row->partnerid);
			}
		    echo json_encode($array);
		}
	function fillstd($promot_id,$classid){
			$key=$_GET['term'];
			$yearid=$this->session->userdata('year');
			$where='';
			if($promot_id!='0')
				$where.=" AND promot_id ='$promot_id'";
			if($classid!='0')
				$where.=" AND classid='$classid'";
			$data=$this->db->query("SELECT * 
									FROM v_student_profile
									WHERE is_vtc='1' AND yearid='$yearid'
									{$where}")->result();
			$array=array();
			foreach ($data as $row) {
				$array[]=array('value'=>$row->last_name.' '.$row->first_name,
								'id'=>$row->studentid);
			}
		    echo json_encode($array);
		}
	function search(){
		if(isset($_GET['p'])){
				$promot_id=$_GET['pr'];
				$partnerid=$_GET['pa'];
				$sort_num=$_GET['s_num'];
				$m=$_GET['m'];
				$p=$_GET['p'];
				$yearid=$_GET['y'];
				$data['tdata']=$this->star->search($promot_id,$partnerid,$sort_num,$m,$p,$yearid);
				$data['idfield']=$this->idfield;		
				$data['thead']=	$this->thead;
				$data['page_header']="Student List";			
				$this->parser->parse('header', $data);
				$this->parser->parse('student/vtcstar/vtcstar_list', $data);
				$this->parser->parse('footer', $data);
		}else{
				$promot_id=$this->input->post('promot_id');
				$partnerid=$this->input->post('partner');
				$sort_num=$this->input->post('sort_num');
				$yearid=$this->input->post('yearid');
				$i=1;
				$m=$this->input->post('m');
				$p=$this->input->post('p');
				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
			        $this->green->setActiveModule($m);
			    }
			    if($p!=''){
			        $this->green->setActivePage($p); 
			    }
				foreach ($this->star->search($promot_id,$partnerid,$sort_num,$m,$p,$yearid) as $row) {
					echo "<tr>
					 	 <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
						 <td class='from_date'>".$this->green->convertSQLDate($row->from_date)."</td>
						 <td class='to_date'>".$this->green->convertSQLDate($row->to_date)."</td>
						 <td class='partner'>".$row->company_name."</td>
						 <td class='promotion'>".$row->proname."</td>									
						 <td class='remove_tag'>";
							if($this->green->gAction("P")){	
							 	echo "<a>
							 		<img rel=".$row->starid." std='' onclick='preview(event);' src='".site_url('../assets/images/icons/preview.png')."'/>
							 	</a>";
							}
							if($this->green->gAction("D")){	
							 	echo "<a>
							 		<img rel=".$row->starid." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
							 	</a>";
							}
							if($this->green->gAction("U")){	
							 	echo "<a>
							 		<img  rel=".$row->starid." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
							 	</a>";
							}
						
						echo "</td>
				 	</tr>";
					 $i++;
				}
				//===========================show pagination=======================
						echo "<tr class='remove_tag'>
								<td colspan='6' id='pgt'>
									<div style='margin-top:20px; width:10%; float:left;'>
									Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
													
													$num=10;
													for($i=0;$i<20;$i++){?>
														<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
														<?php $num+=10;
													}
													
												echo "</select>
									</div>
									<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
										<ul class='pagination' style='text-align:center'>
										 ".$this->pagination->create_links()."
										</ul>
									</div>

								</td>
							</tr> ";
		}
		
	}
}