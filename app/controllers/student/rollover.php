<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class RollOver extends CI_Controller{

		function __construct(){
			parent::__construct();	
			$this->load->model("student/modrollover","rollover");	
			$this->load->library('pagination');	
			$this->thead=array("No"=>'no',
								"Date"=>'date',
								"From Year"=>'from_year',
								 "To Year"=>'to_year',
								 // "School Level"=>'school_level',<td class='school_level setalingn'>".$row->sch_level."</td>
								 "Class"=>'class_name',
								 "Verified By"=>'verified_by',
								 "Verified Date"=>'verified_date',
								 "Verified"=>'is_verified',
								 'Delete'=>'Delete'
								);
			$this->idfield="displanid";					
		}
		function index()
		{	
			$data['tdata']=$this->rollover->getrollover();
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Rollover List";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/rollover/rollover_list', $data);
			$this->parser->parse('footer', $data);
		}
		function add()
		{	
			
			$data['idfield']=$this->idfield;	
			$data['page_header']="Counseling";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/rollover/rollover_form',$data);
			$this->parser->parse('footer', $data);
		}
		function save(){
			$this->rollover->save();
			redirect('student/rollover?y='.$_GET["y"].'&m='.$_GET["m"].'&p='.$_GET["p"].'');
		}

		function verified(){ 
			$rolloverid   = $this->input->post('rolloverid');
			$is_verified  = 1;
			$verified_by  = $this->input->post('verified_by');
			$verified_dat = $this->input->post('verified_dat');

			$verified = array(
				'is_verified'   => $is_verified,
				'verified_by'   => $verified_by,
				'verified_date'  => $verified_dat
			);
			return $this->db->update('sch_student_rollover',$verified,"rolloverid = '".$rolloverid."'");
		}

		function delete_verified(){ 
			$transno = $this->input->post('transno');
			$this->db->delete('sch_student_rollover',"transno = '".$transno."'");
			$this->db->delete('sch_emp_contract',"transno = '".$transno."'");
			$this->db->delete('sch_time_table',"transno = '".$transno."'");
			$this->db->delete('sch_student_enrollment',"transno = '".$transno."'");
		}

		function deletes($rolloverid){
			$row=$this->db->where('rolloverid',$rolloverid)
						->get('sch_student_rollover')->row();
			$this->deletestd($row);
			$this->deletetime($row);
			$this->deletecontract($row);
			$this->db->where('rolloverid',$rolloverid)
						->delete('sch_student_rollover');
			$m=$_GET['m'];
			$p=$_GET['p'];		
			$y=$_GET['y'];
			redirect("student/rollover?y=$y&m=$m&p=$p");
						
		}
		function deletestd($row){
			$this->db->where('transno',$row->transno)
					->delete('sch_student_enrollment');
		}
		function deletetime($row){
			$this->db->where('transno',$row->transno)
					->delete('sch_time_table');
		}
		function deletecontract($row){
			$this->db->where('transno',$row->transno)
					->delete('sch_emp_contract');
		}
		function search(){
			if(isset($_GET['f_id'])){
					$f_year=$_GET['fy'];
					$t_year=$_GET['ty'];
					$schlevel=$_GET['sl'];
					$classid=$_GET['c'];
					$sort_num=$_GET['s_num'];
					$m=$_GET['m'];
					$p=$_GET['p'];
					$data['tdata']=$this->rollover->search($f_year,$t_year,$schlevel,$classid,$m,$p,$sort_num);
					$data['idfield']=$this->idfield;		
					$data['thead']=	$this->thead;
					$data['page_header']="Counseling";			
					$this->parser->parse('header', $data);
					$this->parser->parse('student/rollover/rollover_list', $data);
					$this->parser->parse('footer', $data);
			}else{
					$f_year=$this->input->post('f_year');
					$t_year=$this->input->post('t_year');
					$schlevel=$this->input->post('schlevel');
					$classid=$this->input->post('classid');
					$sort_num=$this->input->post('sort_num');
					$i=1;
					$m=$this->input->post('m');
					$p=$this->input->post('p');
					$this->green->setActiveRole($this->input->post('roleid'));
					if($m!=''){
				        $this->green->setActiveModule($m);
				    }
				    if($p!=''){
				        $this->green->setActivePage($p); 
				    }
					foreach ($this->rollover->search($f_year,$t_year,$schlevel,$classid,$m,$p,$sort_num) as $row) {
						echo "<tr>
							 	 <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
								 <td class='date'>".$this->green->formatSQLDate($row->created_date)."</td>
								 <td class='from_year'>".$row->from_year."</td>
								  <td class='to_year  '>".$row->to_year."</td>
								 <td class='school_level'>".$row->sch_level."</td>
								 <td class='class_name'>".$row->class_name."</td>
								 <td class='school  '>".$row->name."</td>
								 <td class='remove_tag no_wrap'>
								 	<a>
									 	<img rel='".$row->rolloverid."' onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
									</a>
								 </td>
							 	</tr>
							 ";
						 	$i++;
					}
					echo "<tr class='remove_tag'>
							<td colspan='12' id='pgt'>
								<div style='margin-top:20px; width:10%; float:left;'>
								Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
											
												$num=10;
												for($i=0;$i<20;$i++){?>
													<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
													<?php $num+=10;
												}
												
					echo "</select>
								</div>
								<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
									<ul class='pagination' style='text-align:center'>
									 ".$this->pagination->create_links()."
									</ul>
								</div>

							</td>
						</tr> ";
			}
			
		}
	}