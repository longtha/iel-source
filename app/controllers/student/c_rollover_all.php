<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_rollover_all extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("student/mrollover_all","roall");
	}
	public function index(){
		$this->load->view('header');
		$this->load->view("student/rollover/rollover_form_all");
		$this->load->view('footer');
	}
	public function show_school_level(){
		$opt_schlevel = $this->roall->schlevel();
		echo $opt_schlevel;
	}
	public function show_program(){
		$opt_pro = $this->roall->program();
		echo $opt_pro;
	}
	public function show_school_year(){
		$opt_year = $this->roall->schyear();
		echo $opt_year;
	}
	public function show_sch_ranglevel(){
		$opt_ranglevel = $this->roall->ranglevel();
		echo $opt_ranglevel;
	}
	public function show_school_grade(){
		$opt_grand = $this->roall->grandlevel();
		echo $opt_grand;
	}
	public function show_school_class(){
		$opt_class = $this->roall->classname();
		echo $opt_class;
	}
	public function show_student(){
		header("Content-type/text/x-json");
		$show_list = $this->roall->mshow_student();
		echo json_encode($show_list);
	}
	public function save_data(){
		$arr_inf     = $this->input->post("arr_data");
		$arr_student = $this->input->post("arr_student");
		$user        = $this->session->userdata('user_name');
		$schoolid   = "";
		$toprogram  = "";
		$yearid     = "";
		$ranglevel = "";
		$gradelevel = "";
		$classid    = "";
		$schlevelid = "";
		if(count($arr_inf) > 0){
			$schoolid   = $arr_inf['toschoolid'];
			$toprogram  = $arr_inf['toprogram'];
			$yearid     = $arr_inf['toyearid'];
			$classid    = $arr_inf['toclassid'];
			$schlevelid = $arr_inf['toschoollevel'];
			$ranglevel  = $arr_inf['ranglevel'];
			$gradelevel = $arr_inf['togradelavel'];
		}
		
		if(count($arr_student) > 0){
			foreach($arr_student AS $row_st){
				// $ch_data = $this->db->query("SELECT
				// 							Count(*) AS amt
				// 							FROM
				// 							sch_student_enrollment AS sse
				// 							INNER JOIN sch_class ON sse.classid = sch_class.classid
				// 							WHERE
				// 								1 = 1
				// 							AND sse.studentid='".$row_st."'
				// 							AND sse.schoolid='".$schoolid."'
				// 							AND sse.year = '".$yearid."'
				// 							AND sse.classid = '".$classid."'
				// 							AND sch_class.grade_levelid ='".$gradelevel."'
				// 							AND sse.schlevelid = '".$schlevelid."'
				// 							AND sse.programid = '".$toprogram."'")->row()->amt;
				//if($ch_data == 0){
					$data = array("studentid"=>$row_st,
								"schoolid"=>$schoolid ,
								"year"=>$yearid,
								"classid"=>$classid,
								"grade_levelid"=>$gradelevel,
								"rangelevelid"=>$ranglevel,
								"schlevelid"=>$schlevelid,
								"programid"=>$toprogram,
								"enroll_date"=>date('Y-m-d'),
								"rollover_date"=>date('Y-m-d'),
								"rollover_by"=>$user,
								"is_rollover"=>100);
					$this->db->insert("sch_student_enrollment",$data);
				//}
			}
		}
		echo "success..";
	}
	function delete_student_from_class(){
		$arr_obj = $this->input->post("arr_data");
		$msg = "";
		if(count($arr_obj['inf']) > 0){
			$schoolid   = $arr_obj['inf']['schoolid'];
			$program    = $arr_obj['inf']['program'];
			$yearid     = $arr_obj['inf']['yearid'];
			$classid    = $arr_obj['inf']['classid'];
			$schlevelid = $arr_obj['inf']['schoollevel'];
			$gradelevel = $arr_obj['inf']['gradelavel'];
			$ranglevel  = $arr_obj['inf']['ranglevel'];
			// $data_del = array("schoolid"=>$schoolid ,
			// 					"year"=>$yearid,
			// 					"classid"=>$classid,
			// 					"grade_levelid"=>$gradelevel,
			// 					"schlevelid"=>$schlevelid,
			// 					"programid"=>$program,
			// 					"rangelevelid"=>$ranglevel
			// 				);
			
			if(count($arr_obj['studentid']) > 0){
				foreach($arr_obj['studentid'] as $kk=>$val){
					$data_del = array(
										"studentid"=>$val,
										"schoolid"=>$schoolid,
										"year"=>$yearid,
										"classid"=>$classid,
										"grade_levelid"=>$gradelevel,
										"schlevelid"=>$schlevelid,
										"programid"=>$program,
										"rangelevelid"=>$ranglevel
									);
					$delete    = $this->db->delete("sch_student_enrollment",$data_del);
					$data_save = array(
										"student_id"=>$val,
										"schoolid"=>$schoolid,
										"programid"=>$program,
										"schlevelid"=>$schlevelid,
										"yearid"=>$yearid,
										"rangelevelid"=>$ranglevel,
										"gradeid"=>$gradelevel,
										"classid"=>$classid,
										"createdate"=>date('Y-m-d H:i:s'),
										"userid"=>$this->session->userdata('userid'),
										"username"=>$this->session->userdata('user_name')
										);
					$this->db->insert("sch_delete_student_rollover_history",$data_save);
					$msg = "You deleted success.";
				}
			}
			
		}
		echo $msg;		
	}
}