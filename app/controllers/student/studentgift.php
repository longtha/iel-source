<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class StudentGift extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		$this->lang->load('student', 'english');
		$this->load->model("student/ModStudentGift","gift");
		$this->load->library('pagination');	
		$this->thead=array("No"=>'no',
							 "Gift Name"=>'gifname',
							 "Gift Name KH"=>'gifname_kh',
							 "Min Level"=>'min_level',
							 "Unit"=>'unit',
							 "Remark"=>'remark',
							 "Action"=>'Action'							 	
							);
		$this->idfield="studentid";
	}
	function index(){
		$data['tdata']=$this->gift->getstdgift();
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Student List";			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/gift/studentgift_list', $data);
		$this->parser->parse('footer', $data);
	}
	function add(){
		$data['tdata']=$this->gift->getstdgift();
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Student List";			
		$this->parser->parse('header', $data);
		$this->parser->parse('student/gift/studentgift_list', $data);
		$this->parser->parse('footer', $data);
	}
	function edit(){
		$gifid=$this->input->post('gifid');
		$row=$this->db->where('gifid',$gifid)->get('sch_student_gif')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function delete($gifid){
		$this->db->where('gifid',$gifid)->delete('sch_student_gif');
		$m='';
		$p='';
		if(isset($_GET['m'])){
		    $m=$_GET['m'];
		}
		if(isset($_GET['p'])){
		    $p=$_GET['p'];
		}
		redirect("student/studentgift?m=$m&p=$p");

	}
	function save(){
		$gifid=$this->input->post('gifid');
		$gifname=$this->input->post('gifname');
		$gifname_kh=$this->input->post('gifname_kh');
		$min_level=$this->input->post('min_level');
		$unit=$this->input->post('unit');
		$remark=$this->input->post('remark');
		$user=$this->session->userdata('user_name');
		$c_date=date('Y-m-d');
		$data=array('gifname'=>$gifname,
					'gifname_kh'=>$gifname_kh,
					'min_level'=>$min_level,
					'unit'=>$unit,
					'remark'=>$remark);
		if($gifid!=''){
			$data2=array('modify_date'=>$c_date,
						'modify_by'=>$user);
			$this->db->where('gifid',$gifid)->update('sch_student_gif',array_merge($data,$data2));
		}else{
			$data2=array('created_date'=>$c_date,
						'created_by'=>$user);
			$this->db->insert('sch_student_gif',array_merge($data,$data2));
		}
		$m='';
		$p='';
		if(isset($_GET['m'])){
		    $m=$_GET['m'];
		}
		if(isset($_GET['p'])){
		    $p=$_GET['p'];
		}
		redirect("student/studentgift?m=$m&p=$p");
	}
	function search(){
		if(isset($_GET['n'])){
				$gifname=$_GET['n'];
				$sort_num=$_GET['s_num'];
				$m=$_GET['m'];
				$p=$_GET['p'];
				$data['tdata']=$this->gift->search($gifname,$sort_num,$m,$p);
				$data['idfield']=$this->idfield;		
				$data['thead']=	$this->thead;
				$data['page_header']="Student List";			
				$this->parser->parse('header', $data);
				$this->parser->parse('student/gift/studentgift_list', $data);
				$this->parser->parse('footer', $data);
		}else{
				$gifname=$this->input->post('gifname');
				$sort_num=$this->input->post('s_num');
				$i=1;
				$m=$this->input->post('m');
				$p=$this->input->post('p');
				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
			        $this->green->setActiveModule($m);
			    }
			    if($p!=''){
			        $this->green->setActivePage($p); 
			    }
				foreach ($this->gift->search($gifname,$sort_num,$m,$p) as $row) {
					echo "<tr>
						 	 <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
							 <td class='gifname'>".$row->gifname."</td>
							 <td class='gifname_kh'>".$row->gifname_kh."</td>
							 <td class='min_level'>".$row->min_level."</td>
							 <td class='unit'>".$row->unit."</td>
							 <td class='remark'>".$row->remark."</td>
							 <td class='remove_tag'>";
							if($this->green->gAction("D")){	
							 	echo "<a>
							 		<img rel=".$row->gifid." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
							 	</a>";
							}
							if($this->green->gAction("U")){	
							 	echo "<a>
							 		<img  rel=".$row->gifid." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
							 	</a>";
							 }
							echo "</td>
					 	</tr>
					 ";
					 $i++;
				}
				//===========================show pagination=======================
						echo "<tr class='remove_tag'>
								<td colspan='12' id='pgt'>
									<div style='margin-top:20px; width:10%; float:left;'>
									Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
													
													$num=10;
													for($i=0;$i<20;$i++){?>
														<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
														<?php $num+=10;
													}
													
												echo "</select>
									</div>
									<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
										<ul class='pagination' style='text-align:center'>
										 ".$this->pagination->create_links()."
										</ul>
									</div>

								</td>
							</tr> ";
		}
		
	}
}