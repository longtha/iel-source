<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class attendance extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model('student/modattendance','mdatt');
        $this->load->model('school/schoolinformodel','sch');
        $this->load->model('school/schoolyearmodel','schyear');
        $this->load->model('school/schoollevelmodel','schlevels');
        $this->load->model('school/classmodel','cls');
        $this->load->model('school/modfeetype','mfeetype');
        $this->load->model('school/modrangelevel','mrgnlv');
        $this->load->model('school/modweek','mdweek');
        $this->load->model('school/subjectmodel','msubj');
        $this->load->model('school/subjectmodel_iep','msubjiep');

    }
	public function index()
	{
        $data['header']="Student Attendance";
        $this->load->view('header');
        $data['schyears']=$this->schyear->getschoolyear();
        $data['schevels']=$this->schlevels->getsch_level();
        $data['class']=$this->cls->allclass();
        $data['attnote']=$this->mdatt->attnotes();
        $data['attbases']=$this->mdatt->attbase();
		$this->load->view('student/attendance/vattendance_list',$data);
		$this->load->view('footer');
	}
    public function add($transno="",$programid ="",$classid ="")
    {
        if($transno==""){
            $data['header']="Add Student Attendance";
        }else{
            $data['header']="Edit Student Attendance";
        }

        $data['schyears']=$this->schyear->getschoolyear();
        $data['schevels']=$this->schlevels->getsch_level();
        $data['class']=$this->cls->allclass();
        $data['attnote']=$this->mdatt->attnotes();
        $data['attbases']=$this->mdatt->attbase();
        $data['transno']=$transno;
        $data['programid']=$programid;
        $data['classid']=$classid;
        $this->load->view('header');
        $this->load->view('student/attendance/vattendance',$data);
        $this->load->view('footer');
    }
    function preview($transno,$programid,$yearid,$classid){
        $data['header']="Preview";
        $data['rep_title']="Daily Student Attendance";
        $data['attdailyrow']=$this->mdatt->getattdailyrow($transno,$programid);
        $data['data']=$this->mdatt->getattdailydetrowpreview($transno,$programid,$yearid,$classid);
        $this->load->view('header');
        $this->load->view('student/attendance/vpreview',$data);
        $this->load->view('footer');
    }
	function save()
	{
        $transno=$this->input->post('transno');
         $this->mdatt->save($transno);
	}
	function delete($transno){
        $arr=$this->mdatt->delete($transno);
        echo json_encode($arr);
        exit();
	}

    function edit_attendance($transno,$programid,$classid){
        $rngdata['attdailyrow']=$this->mdatt->getattdailyrow($transno,$programid,$classid);
        $rngdata['attdailydetrow']=$this->mdatt->getattdailydetrow($transno,$programid,$classid);
        $rngdata['attnote']=$this->db->get("sch_stud_attnote")->result();
       echo json_encode($rngdata);
        exit();
    }
	function search(){
        $start=$this->input->post('startpage');
        $this->mdatt->search($start);
	}
    function getClass(){
        $schlevelid=$this->input->post("schlevelid");
        $arr['class']=$this->cls->allclass($schlevelid);
        echo json_encode($arr);
        exit();
    }
    function getStudyPeriod(){
        $yearid=$this->input->post("yearid");
        $schoolid=$this->input->post("schoolid");
        $schlevelid=$this->input->post("sclevelid");
        $payment_type=$this->input->post("payment_type");
        $classid=$this->input->post("classid");
        $rangelevelids=$this->mrgnlv->getRangeByClass($classid);
        if(count($rangelevelids)>0){
            $rangelevelid=$rangelevelids->rangelevelid;
        }else{
            $rangelevelid="";
        }

        $arr['datas']=$this->mfeetype->getStudyPeriod("",$yearid,$schoolid,$schlevelid,$payment_type,$rangelevelid);
        echo json_encode($arr);
        exit();
    }
    function getweeks(){
        $termid=$this->input->post("termid");
        $arr['weeks']=$this->mdweek->weekbyterm($termid);
        echo json_encode($arr);
        exit();
    }
    function group_subjectkgp(){
        $sch_levelid=$this->input->post("sch_levelid");
        
        $arr['gs_kgp']=$this->msubj->getsubjecttype($sch_levelid);
        echo json_encode($arr);
        exit();
    }
    function group_subjectiep(){
        $sch_levelid=$this->input->post("sch_levelid");
        $arr['gs_iep']=$this->msubjiep->getsubjecttype($sch_levelid);
        echo json_encode($arr);
        exit();
    }
    function autosubjectkgp(){
        $subject_group =$this->input->post("subject_group");
        $arr['sub_kgp']=$this->msubj->subjectskgp($this->green->getActiveUser(), "", "", $subject_group);
        echo json_encode($arr);
        exit();
    }
    function autosubjectiep(){
        $subject_group =$this->input->post("subject_group");
        $arr['sub_iep']=$this->msubjiep->subjectsiep($this->green->getActiveUser(), $subject_group);
        echo json_encode($arr);
        exit();
    }
    function getstdbycls(){
        $schlevelid=$this->input->post("sclevelid");
        $yearid=$this->input->post("yearid");
        $classid=$this->input->post("classid");
        $termid=$this->input->post("termid");
        $stud=$this->studentbyterm($schlevelid,$yearid,$classid,$termid);

        $arr['stdbycls']=$stud;
        $arr['attnote']=$this->db->get("sch_stud_attnote")->result();

        echo json_encode($arr);
        exit();
    }
    function studentbyterm($schlevelid="",$yearid,$classid,$termid=""){
        if($schlevelid!=""){
            $this->db->where("schlevelid",$schlevelid);
        }
        if($termid!=""){
            $this->db->where("termid",$termid);
        }
        $this->db->where("yearid",$yearid);
        $this->db->where("classid",$classid);
        $this->db->select('studentid, student_num, fullname, fullname_kh, fullname_ch, phone1,programid');
        $this->db->distinct();
        $this->db->order_by("last_name", "asc");
        $query = $this->db->get('v_student_profile');

        return $query->result();

    }
    function getattbase($schlevelid){
        $arr['attbases']=$this->mdatt->attbase($schlevelid);
        echo json_encode($arr);
        exit();
    }
}