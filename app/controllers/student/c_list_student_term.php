<?php 
	class C_list_student_term extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model("student/mod_list_student_term","list_term");
			$this->thead=array("No"=>'No',
							 "StudentID"=>'student_num',
							 "Name"=>'fullname',
							 "Gender"=>'gender',
							 "School level"=>'sch_level',
							 "Year"=>'sch_year',
							 "Grad level"=>'grade_level',						 
							 "Class"=>'class_name',
							 "Term"=>'term',
							 "Score"=>'total_score'					 	
							);
		}

		function index(){
			$data['opt_school'] = $this->get_school();
			//$data['opt_program'] = $this->get_program(); 
			$data['opt_examtyp']=$this->get_examtype();
			$data['thead']=	$this->thead;
			$this->load->view('header');
			$this->load->view('student/v_list_student_term',$data);
			$this->load->view('footer');
		}

		function show_term_report(){ 
			$this->load->view('header');
			$this->load->view('reports/iep_reports/v_mid_term_report');
			$this->load->view('footer');
		}

		function show_final_term_report(){
			$this->load->view('header');
			$this->load->view('reports/iep_reports/v_final_term_report');
			$this->load->view('footer');
		}
		function pint_list_final(){
			$this->load->view('header');
			$programeid = isset($_GET['p_id']) ? $_GET['p_id'] : '';	
			$classid    = isset($_GET['classid']) ? $_GET['classid'] : '';	
			$termid     = isset($_GET['termid']) ? $_GET['termid'] : '';
			$schlavelid = isset($_GET['schlid']) ? $_GET['schlid'] : '';
			$schoolid   = isset($_GET['schoolid']) ? $_GET['schoolid'] : '';
			$grandlavelid = isset($_GET['grandid']) ? $_GET['grandid'] : '';
			$yearid     = isset($_GET['yearid']) ? $_GET['yearid'] : '';
			$examtype   = isset($_GET['examtype']) ? $_GET['examtype'] : '';
			$page   = isset($_GET['page']) ? $_GET['page'] : 0;
			$numrow   = isset($_GET['numrow']) ? $_GET['numrow'] : 0;
			$data = array(
							"programeid"=>$programeid,
							"classid"=>$classid,
							"termid"=>$termid,
							"schlavelid"=>$schlavelid,
							"schoolid"=>$schoolid,
							"grandlavelid"=>$grandlavelid,
							"yearid"=>$yearid,
							"examtype"=>$examtype,
							"page"=>$page,
							"numrow"=>$numrow
							// "firstname"=>$row_st->first_name,
							// "lastname"=>$row_st->last_name,
							// "gender"=>$row_st->gender,
							// "studentnumber"=>$row_st->student_num
							);
			$this->load->view('reports/iep_reports/v_print_all_final',$data);
			$this->load->view('footer');
		}
		function pint_list_midterm(){
			$this->load->view('header');
			$programeid = isset($_GET['p_id']) ? $_GET['p_id'] : '';	
			$classid    = isset($_GET['classid']) ? $_GET['classid'] : '';	
			$termid     = isset($_GET['termid']) ? $_GET['termid'] : '';
			$schlavelid = isset($_GET['schlid']) ? $_GET['schlid'] : '';
			$schoolid   = isset($_GET['schoolid']) ? $_GET['schoolid'] : '';
			$grandlavelid = isset($_GET['grandid']) ? $_GET['grandid'] : '';
			$yearid     = isset($_GET['yearid']) ? $_GET['yearid'] : '';
			$examtype   = isset($_GET['examtype']) ? $_GET['examtype'] : '';
			$page   = isset($_GET['page']) ? $_GET['page'] : 0;
			$numrow   = isset($_GET['numrow']) ? $_GET['numrow'] : 0;
			$data = array(
							"programeid"=>$programeid,
							"classid"=>$classid,
							"termid"=>$termid,
							"schlavelid"=>$schlavelid,
							"schoolid"=>$schoolid,
							"grandlavelid"=>$grandlavelid,
							"yearid"=>$yearid,
							"examtype"=>$examtype,
							"page"=>$page,
							"numrow"=>$numrow
							// "firstname"=>$row_st->first_name,
							// "lastname"=>$row_st->last_name,
							// "gender"=>$row_st->gender,
							// "studentnumber"=>$row_st->student_num
							);
			$this->load->view('reports/iep_reports/v_print_all_midterm',$data);
			$this->load->view('footer');
		}
		function st_auto(){
		    $name = trim($_REQUEST['name']);
		    $pid = $_REQUEST['pid'];
		    $schlevel = $_REQUEST['schlevel'];
		    $classid = $_REQUEST['classid'];
		    $where = "";
			if($name != ""){
			   $where .= " AND CONCAT(vs.last_name,' ',vs.first_name) like '".$name."%'";    
			}
			if($pid != ""){
	        	$where .= " AND vs.programid = '".$pid."'";    
		    }
		    if($schlevel != ""){
		        $where .= " AND vs.schlevelid = '".$schlevel."'";    
		    }
		    // if($classid != ""){
		    //     $where .= " AND vs.classid = '".$classid."'";    
		    // }
		  	$sql = "SELECT DISTINCT 
							CONCAT(vs.last_name,' ',vs.first_name) as fullname
							FROM
							v_student_enroment AS vs
							WHERE 1=1 {$where} 
							AND vs.classid = '".$classid."' LIMIT 0, 10"; 
		 	$qr = $this->db->query($sql);
		  	$arr = [];
			if($qr->num_rows() > 0){
			    foreach ($qr->result() as $row){
			    	$arr[] = ['name' => $row->fullname];
			    }
			}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
		}

		public function stid_auto(){
		    $st_num = trim($_REQUEST['id']);
		    $pid = $_REQUEST['pid'];
		    $schlevel = $_REQUEST['schlevel'];
		    $classid = $_REQUEST['classid'];
		    $where = "";
		    if($st_num != ""){
		        $where .= " AND vs.student_num like '".$st_num."%'";    
		    }
		    if($pid != ""){
		        $where .= " AND vs.programid = '".$pid."'";    
		    }
		    if($schlevel != ""){
		        $where .= " AND vs.schlevelid ='".$schlevel."'";    
		    }
		    // if($classid != ""){
		    //     $where .= " AND vs.classid = '".$classid."'";    
		    // }
		    $sql = "SELECT DISTINCT 
							student_num,
							CONCAT(vs.last_name,' ',vs.first_name) as fullname
							FROM
							v_student_enroment AS vs
							WHERE 1=1 {$where} 
							AND vs.classid = '".$classid."'
							LIMIT 0, 10"; 
			//echo $sql;
		    $qr = $this->db->query($sql);
		    $arr = [];
		    if($qr->num_rows() > 0){
		        foreach ($qr->result() as $row) {
		            $arr[] = ['st_num' => $row->student_num ,'st_name' => $row->fullname];
		        }
		    }
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
		}

		function get_school(){
			$opt_school = "";
			$sql = $this->db->get('sch_school_infor')->result();
			foreach ($sql as  $row){
				$opt_school .='<option value="'.$row->schoolid.'">'.$row->name.'</option>';
			}
			return $opt_school;
		}

		function get_program(){
			$optionprogram="";
			$schoolid = $this->input->post('schoolid');
			$optionprogram .='<option value=""></option>';
			foreach($this->list_term->get_program($schoolid) as $pro_id ){
				$optionprogram .='<option value="'.$pro_id->programid.'">'.$pro_id->program.'</option>';
			}
			header("Content-type:text/x-json");
			$arr['program'] = $optionprogram;
			echo json_encode($arr);		
		}

		function school_level(){
			$school_level="";
			$option_level="";
			$program = $this->input->post('program');
			$option_level .='<option value=""></option>';
			foreach($this->list_term->getsch_level($program) as $id ){
				$option_level .='<option value="'.$id->schlevelid.'">'.$id->sch_level.'</option>';
			}
			header("Content-type:text/x-json");
			$arr_success['schlevel'] = $option_level;
			echo json_encode($arr_success);
		}

		function get_years(){
			$schoolid = $this->session->userdata('schoolid');	
			$sch_program = $this->input->post("program");
			$schlevelid = $this->input->post("sch_level");
			$option_year ='';
			$option_year .='<option value=""></option>';
			foreach ($this->list_term->getschoolyear($sch_program,$schoolid,$schlevelid) as $row){

				$option_year .='<option value="'.$row->yearid.'">'.$row->sch_year.'</option>';
			}		
			header("Content-type:text/x-json");
			$arr_success['schyear'] = $option_year;
			echo json_encode($arr_success);
			
		}

		function get_gradlevel(){
			$grad_level="";
			$schoolid = $this->session->userdata('schoolid');	
			$programid = $this->input->post("program");
			$schlevelid = $this->input->post("sch_level");
			$grad_level .='<option value=""></option>';
			foreach($this->list_term->getgradelevel($schoolid,$programid,$schlevelid) as $id ){
				$grad_level .='<option value="'.$id->grade_levelid.'">'.$id->grade_level.'</option>';
			}
			header("Content-type:text/x-json");
			$arr_success['gradlevel'] = $grad_level;
			echo json_encode($arr_success);
		}

		function get_class(){
			$schoolid = $this->session->userdata('schoolid');	
			$grad_level = $this->input->post("grad_level");
			$schlevelid = $this->input->post("sch_level");
			$class="";
			$class .='<option value=""></option>';
			foreach($this->list_term->get_class($schoolid,$grad_level,$schlevelid) as $id ){			
				$class .='<option value="'.$id->classid.'">'.$id->class_name.'</option>';
			}
			header("Content-type:text/x-json");
			$arr_success['getclass'] = $class;
			echo json_encode($arr_success);  
		}

		function get_term(){
			$classid = $this->input->post("classid");
			$term_opt="";
			$term_opt .='<option value=""></option>';
			foreach($this->list_term->get_term($classid) as $id ){			
				$term_opt .='<option value="'.$id->termid.'">'.$id->term.'</option>';
			}
			header("Content-type:text/x-json");
			$arr_term['get_termid'] = $term_opt;
			echo json_encode($arr_term);  
		}

		function get_examtype(){
			$examtype="";
			$query=$this->db->get('sch_student_examtype_iep')->result();
			foreach($query as $id ){

				$examtype .='<option value="'.$id->examtypeid.'">'.$id->exam_test.'</option>';
			}
			return $examtype;
		}

		function show_list(){
			$table='';
			$i=1;
			$exam_type = $this->input->post('exam_type');
			$sql  = $this->list_term->show_list();
			foreach($this->db->query($sql['sql'])->result() as $row){
				if($exam_type == 1){
					$sql_score = $this->db->query("SELECT
														sch_iep_total_score_entry.total_score
														FROM
														sch_iep_total_score_entry
														WHERE 1=1 
														AND schoolid = '".$row->schoolid."'
														AND schlevelid = '".$row->schlevelid."'
														AND yearid = '".$row->yearid."'
														AND termid = '".$row->termid."'
														AND classid = '".$row->classid."'
														AND examtypeid = '".$exam_type."'
														AND gradeid   = '".$row->grade_levelid."'
														AND studentid = '".$row->studentid."'
														ORDER BY examtypeid,total_score DESC")->row();
				}else{
					$sql_score = $this->db->query("SELECT 
														tbl_max.total_score
														FROM
														(
														SELECT
															sch_iep_total_score_entry.studentid,
															-- SUM(sch_iep_total_score_entry.total_score) AS total_score
															SUM(sch_iep_total_score_entry.total_score)/2 AS total_score
														FROM
															sch_iep_total_score_entry
														WHERE 1=1
															AND schoolid = '".$row->schoolid."'
															AND schlevelid = '".$row->schlevelid."'
															AND yearid = '".$row->yearid."'
															AND termid = '".$row->termid."'
															AND classid = '".$row->classid."'
															AND gradeid   = '".$row->grade_levelid."'
															AND studentid = '".$row->studentid."'
															GROUP BY studentid
															ORDER BY total_score DESC
														) AS tbl_max
														ORDER BY tbl_max.total_score DESC")->row();
				}
				$total_score = isset($sql_score->total_score)?$sql_score->total_score:0;
				$main = $row->schoolid.'_'.$row->schlevelid.'_'.$row->year.'_'.$row->grade_levelid.'_'.$row->classid.'_'.$row->termid;			
				$programid = $row->programid;
				$f_t_date  = $row->fdate.'-'.$row->tdate;
				$full_path = $exam_type  == 1 ? 'c_list_student_term/show_term_report' : 'c_list_student_term/show_final_term_report';	
				$table .= '<tr>
								<td>'.str_pad($i,2,"0",STR_PAD_LEFT).'</td>
								<td class="student_num">'.$row->student_num.'</td>
								<td class="fullname">
									<span id="fullname_st">'.$row->fullname.'</span><br>
									<span>'.$row->fullname_kh.'</span>
									<input type="hidden" id="ftdate" name="ftdate" value="'.$f_t_date.'">
								</td>
								<td class="gender">'.ucwords($row->gender).'</td>
								<td class="sch_level">'.$row->sch_level.'</td>
								<td class="year">'.$row->sch_year.'</td>
								<td class="grad_level">'.$row->grade_level.'</td>
								<td class="class_name">'.$row->class_name.'</td>
								<td class="termid">'.$row->term.'</td>
								<td style="text-align:center;">'.(floor(($total_score/2)*100)/100).'</td>
								<td class="remove_tag"style="text-align: center;">';
						if($this->green->gAction("C")){ 
							$table .= '<a id="add_comment" student_id="'.$row->studentid.'" href="javascript:void(0)"><img src='.base_url('assets/images/icons/add.png').' style="margin-right:0;width:17px;height:17px;"></a>&nbsp;';
						}
						$table .= '<a target="_blank" href="'.site_url('student/'.$full_path.'/?id='.$row->studentid.'&p_id='.$row->programid.'&schoolid='.$row->schoolid.'&schlid='.$row->schlevelid.'&grandid='.$row->grade_levelid.'&yearid='.$row->yearid.'&classid='.$row->classid.'&termid='.$row->termid.'&examtype='.$exam_type).'"><img src='.base_url('assets/images/icons/a_preview.png').' style="width:17px;height:17px;">
									</a>';
						$table .= '</td>
							</tr>';							 
				$i++;	 
			}
			$paging = $sql['paging'];
			header("Content-type:text/x-json");
			$arr['tr_data']=$table;
			$arr['pagina']= $paging;		
			echo json_encode($arr);	
			//echo $this->list_term->show_list();
		}
		function save_comment(){
			$schoolid    = $this->input->post("schoolid");
            $program     = $this->input->post("program");
            $sch_level   = $this->input->post("sch_level");
            $years       = $this->input->post("years");
            $grad_level  = $this->input->post("grad_level");
            $classid     = $this->input->post("classid");
            $termid      = $this->input->post("termid");
            $exam_type   = $this->input->post("exam_type");

            $student_id      = $this->input->post("student_id");
            $comment_teacher = $this->input->post("comment_teacher");
            $comment_guardian= $this->input->post("comment_guardian");
            $date_academic   = $this->input->post("date_academic");
            $date_teacher    = $this->input->post("date_teacher");
            $date_return     = $this->input->post("date_return");
            $date_guardian   = $this->input->post("date_guardian");
            $academicid      = $this->input->post("academicid");
            $userid_teacher  = $this->input->post("userid_teacher");

            $data = array(
			            	"student_id"=>$student_id,
			            	"schoolid"=>$schoolid,
			            	"schlevelid"=>$sch_level,
			            	"yearid"=>$years,
			            	"gradelevelid"=>$grad_level,
			            	"classid"=>$classid,
			            	"termid"=>$termid,
			            	"command_teacher"=>$comment_teacher,
			            	"command_guardian"=>$comment_guardian,
			            	"examp_type"=>$exam_type,
			            	"academicid"=>$academicid,
			            	"userid"=>$userid_teacher,
			            	"date_academic"=>$this->green->convertSQLDate($date_academic),
			            	"date_teacher"=>$this->green->convertSQLDate($date_teacher),
			            	"date_return"=>$this->green->convertSQLDate($date_return),
			            	"date_guardian"=>$this->green->convertSQLDate($date_guardian),
			            	"date_create"=>date("Y-m-d")
			            	);
            $check_data = $this->db->query("SELECT COUNT(*) AS amt_count 
            								FROM sch_studend_command_iep 
            								WHERE 1=1
            								AND student_id='".$student_id."'
									        AND schoolid  = '".$schoolid."'
									        AND schlevelid='".$sch_level."'
									        AND yearid    ='".$years."'
									        AND gradelevelid='".$grad_level."'
									        AND classid   = '".$classid."'
									        AND termid    = '".$termid."'
									        AND examp_type='".$exam_type."'")->row();
            $ch_dat = isset($check_data->amt_count)?$check_data->amt_count:0;
            if($ch_dat > 0){
            	$data_update = array("student_id"=>$student_id,
					            	"schoolid"=>$schoolid,
					            	"schlevelid"=>$sch_level,
					            	"yearid"=>$years,
					            	"gradelevelid"=>$grad_level,
					            	"classid"=>$classid,
					            	"termid"=>$termid
					            	);
            	$this->db->update("sch_studend_command_iep",$data,$data_update);
            }else{
            	$this->db->insert("sch_studend_command_iep",$data);
            }
           	
   			echo "Ok";
		}
		function show_comment(){
			$schoolid    = $this->input->post("schoolid");
            $student_id  = $this->input->post("student_id");
            $sch_level   = $this->input->post("sch_level");
            $years       = $this->input->post("years");
            $grad_level  = $this->input->post("grad_level");
            $classid     = $this->input->post("classid");
            $termid      = $this->input->post("termid");
            $exam_type   = $this->input->post("exam_type");
            
            $show_db = $this->db->query("SELECT * FROM sch_studend_command_iep
						            		WHERE 1=1
						            		AND student_id='".$student_id."'
									        AND schoolid  = '".$schoolid."'
									        AND schlevelid='".$sch_level."'
									        AND yearid    ='".$years."'
									        AND gradelevelid='".$grad_level."'
									        AND classid   = '".$classid."'
									        AND termid    = '".$termid."'
									        AND examp_type='".$exam_type."'
									    ");
           	$arr_com = array();
           	if($show_db->num_rows() > 0){
           		foreach($show_db->result() as $row_com){
           			$arr_com['comment']= array(
           									"student_id"=>$row_com->student_id,
           									"command_teacher"=>$row_com->command_teacher,
           									"command_guardian"=>$row_com->command_guardian,
							            	"academicid"=>$row_com->academicid,
							            	"userid"=>$row_com->userid,
							            	"date_academic"=>$this->green->formatSQLDate($row_com->date_academic),
							            	"date_teacher"=>$this->green->formatSQLDate($row_com->date_teacher),
							            	"date_return"=>$this->green->formatSQLDate($row_com->date_return),
							            	"date_guardian"=>$this->green->formatSQLDate($row_com->date_guardian)
           									);
           		}
           	}else{
           		$arr_com['comment']= array(
           									"student_id"=>$student_id,
           									"command_teacher"=>"",
           									"command_guardian"=>"",
							            	"academicid"=>"",
							            	"userid"=>$this->session->userdata('userid'),
							            	"date_academic"=>date('d-m-Y'),
							            	"date_teacher"=>date('d-m-Y'),
							            	"date_return"=>date('d-m-Y'),
							            	"date_guardian"=>date('d-m-Y')
           								);
           	}
           	header("Content-type:text/x-json");
            echo json_encode($arr_com);
		}
	}
?>