<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_study_record extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('student/modstudy_record', 'c');

        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolyearmodel', 'y');
        // $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');
        // $this->load->model('school/gradelevelmodel', 'g');
        $this->load->model('school/classmodel', 'cl');

        $this->load->model("social/modfamily","family");
        $this->load->model("student/modStudent","std");
	}

	public function index(){
		$this->load->view('header');		
		$this->load->view('student/study_record/v_study_record');
		$this->load->view('footer');	
	}

	public function grid(){
    	$grid = $this->c->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $grid;
    }

    public function get_schlevel(){
        $programid = $this->input->post('programid');
        $get_schlevel = $this->c->get_schlevel($programid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel;
    }

    public function get_year(){
        $programid = $this->input->post('programid');
        $schlevelid = $this->input->post('schlevelid');
        
        $get_year = $this->c->get_year($programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    public function get_class(){
        $schlevelid = $this->input->post('schlevelid');       
        $get_class = $this->c->get_class($schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_class;
    }    

}