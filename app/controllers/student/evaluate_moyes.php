<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Evaluate_moyes extends CI_Controller{

		function __construct(){
			parent::__construct();	
			$this->load->model("student/modevaluate_moyes","evaluate");	
			$this->load->library('pagination');	
			$this->load->library('upload');
			$this->thead=array("No"=>'no',
								"Date"=>'se__date',
								"Name in Khmer"=>'s__fullname',	
								"Name in English"=>'s__fullname',													
								 "Class"=>'s__class_name',
								 "Month"=>'month',
								 /*"Score"=>'score',
								 "Average"=>'average',
								 "Rank"=>'rank',*/
								 'Action'=>'Action'
								);
			$this->semthead=array("No"=>'no',
								"Student Name"=>'s__fullname',
								"Class"=>'s__class_name',
								 "Month"=>'month',
								 "Semester"=>'semester',
								 "Exam Average"=>'se__sem_exam_avg',
								 "Month Average"=>'se__months_avg',
								 "Average"=>'se__sem_avg',
								 "Mention"=>'se__sem_mention',
								 "Result"=>'se__sem_result',
								 "Rank"=>'sem_rank',
								 'Preview'=>'preview'
								);
			$this->rethead=array("No"=>'no',
								"Student Name"=>'s__fullname',
								"Class"=>'s__class_name',
								 "Semester 1th"=>'semester_one',
								 "Semester 2nd"=>'semester_two',
								 "Average"=>'total',
								 "Mention"=>'mention',
								 "Result"=>'result'
								);
			$this->idfield="displanid";					
		}

		function index()
		{	
			//$data['tdata']=;//$this->evaluate->getevaluate();
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluate";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/evaluate/evaluate_list_moyes', $data);
			$this->parser->parse('footer', $data);
		}
		function semesterlist(){
			$data['tdata']=$this->evaluate->getevasemester();
			$data['idfield']=$this->idfield;		
			$data['semthead']=	$this->semthead;
			$data['page_header']="Evaluate";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/evaluate/semester_list_moyes', $data);
			$this->parser->parse('footer', $data);
		}
		function getteacher(){
			$classid=$this->input->post('classid');
			$yearid=$this->session->userdata('year');
			if(count($this->evaluate->getteacherbyclass($classid,$yearid))>0)
				foreach ($this->evaluate->getteacherbyclass($classid,$yearid) as $t) {
					echo "<tr>
							<td>".$t->last_name.' '.$t->first_name."</td>
						</tr>";
				}
			else
				echo "<label style='color:red'>No Teacher in this Class</label>";
		}
		function resultlist(){
			$data['tdata']=$this->evaluate->getresult();
			$data['idfield']=$this->idfield;		
			$data['semthead']=	$this->rethead;
			$data['page_header']="Evaluate";			
			$this->parser->parse('header', $data);
			$this->parser->parse('student/evaluate/result_list', $data);
			$this->parser->parse('footer', $data);
		}
		function add($data='')
		{	
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluate";
			$data['upload_download']="Import";
			$this->parser->parse('header', $data);
			$this->parser->parse('student/evaluate/evaluate_form_moyes', $data);
			$this->parser->parse('footer', $data);
		}
		function preview($transno)
		{	
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluation Preview";

            $data['fyearid']=$_GET['y'];
            $data['fschlevelid']=$_GET['sy'];
            $data['classid']=$_GET['cl'];
            $data['month']=$_GET['m'];
            $data['transno']=$transno;

            $data['subtype']=$this->evaluate->gets_type($_GET['cl'],$_GET['y'],0);
			$this->parser->parse('header', $data);
			$this->load->view('student/evaluate/preview_moeys', $data);
			$this->parser->parse('footer', $data);
		}
		function preview_result($transno)
		{	
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluation Preview";

            $data['fyearid']=$_GET['y'];
            $data['fschlevelid']=$_GET['sy'];
            $data['classid']=$_GET['cl'];
            $data['month']=$_GET['m'];
            $data['transno']=$transno;

            $data['subtype']=$this->evaluate->gets_type($_GET['cl'],$_GET['y'],0);
			$this->parser->parse('header', $data);
			$this->load->view('student/evaluate/preview_moeys_result', $data);
			$this->parser->parse('footer', $data);
		}
		function previewmulti($evaluateid)
		{	
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluate";
			$datas['eva']=$this->evaluate->getrow($evaluateid);	
			$schlevel=$this->db->where('classid',$this->evaluate->getrow($evaluateid)->classid)->get('sch_class')->row()->schlevelid;
   			$isvtc=$this->db->where('schlevelid',$schlevel)->get('sch_school_level')->row()->is_vtc;		
			$this->parser->parse('header', $data);
			if($isvtc==1)
				$this->load->view('student/evaluate/preview_vtc', $datas);
			else
				$this->load->view('student/evaluate/preview_moeys', $datas);
			$this->parser->parse('footer', $data);
		}
		function getmonthclass(){
			$classid=$this->input->post('classid');
			$yearid=$this->input->post('yearid');
			foreach ($this->evaluate->getmonthclass($classid,$yearid) as $row) {
				echo "<option value='$row[transno]'>$row[month]</option>";
			}
		}
		function previewclass($transno)
		{	
			$data['idfield']=$this->idfield;
			$data['page_header']="Evaluate";
			$isvtc=$this->db->query("SELECT DISTINCT sl.is_vtc 
									FROM sch_student_evaluated e
									INNER JOIN sch_class c
									ON(e.classid=c.classid)
									INNER JOIN sch_school_level sl 
									  ON(c.schlevelid=sl.schlevelid)
									WHERE e.transno='$transno'
									")->row()->is_vtc;
			$datas['eval']=$this->evaluate->getrow('',$transno);
			$this->parser->parse('header', $data);
			if($isvtc==1)
				$this->load->view('student/evaluate/preview_vtc_class', $datas);
			else
				$this->load->view('student/evaluate/preview_class_moeys', $datas);
			$this->parser->parse('footer', $data);
		}
		function previewbook($evaluateid)
		{	
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluate";
			$datas['eva']=$this->evaluate->getrow($evaluateid);			
			$this->parser->parse('header', $data);
			$this->load->view('student/evaluate/preview_book_moeys', $datas);
			$this->parser->parse('footer', $data);
		}
		function previewsem($semid)
		{	
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Evaluate";
			$datas['eva']=$this->evaluate->getsemrow($semid);			
			$this->parser->parse('header', $data);
			$this->load->view('student/evaluate/preview_sem', $datas);
			$this->parser->parse('footer', $data);
		}
		function getsub_type(){
			$classid=$this->input->post('classid');
			$schoolid=$this->session->userdata('schoolid');
			$evaluate_type=$this->input->post('evaluate_type');
			$month=$this->input->post('month');
			$year=$this->input->post('year');
			$mention=array('A','B','C','D','E','F');
			$action='';
			if($month!='')
				$month=implode(',',$month);

			$schlevelid=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
			$isvtc=$this->db->where('schlevelid',$schlevelid)->get('sch_school_level')->row()->is_vtc;
			$count=$this->validateeva($year,$classid,$schoolid,$evaluate_type,$month);
			$trimester=0;
			if($count==0){
				if($evaluate_type!='Three Month')
					$action='return isNumberKey(event);';
				else
					$trimester=1;
				echo "<thead>
							<tr>
			        			<th rowspan='2' style='vertical-align: middle !important;'><input type='text' class='hide' value='$isvtc' id='schlevelids'/>No</th>
			        			<th rowspan='2' style='vertical-align: middle !important;'>Student</th>";
			        	// echo "<th rowspan='2' width='70' style='vertical-align: middle !important;'>Comment</th>";
		        //==================subject type========================
				foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
					echo "<th colspan='$sub->s_total' style='text-align:center !important;'>$sub->subject_type</th>";
				}
					echo "</tr>
						<tr>";
				////=========================subject========================
				foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
					foreach ($this->evaluate->getsubject($classid,$sub->subj_type_id,$year,$trimester) as $subject) {
						echo '<th><div><span class="th_kh" style="font-size:12px;">'.$subject->subject_kh.'</span></div></th>';
					}
				}
				echo "</tr></thead>
						<tbody>";
				$i=1;
				$tabindex=0;
				foreach ($this->evaluate->getstudent($classid,$year) as $std) {
					echo "<tr>
							<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
							<td class='no_wrap'><input class='form-control input-sm hide studentid' value='$std->studentid' name='studentid[]' id='studentid' />".$std->last_name_kh.' '.$std->first_name_kh."</td>";

							foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
								foreach ($this->evaluate->getsubject($classid,$sub->subj_type_id,$year,$trimester) as $subject) {
									echo "<td>
											<input type='text' onkeypress='".$action."'  onkeypress='return isNumberKey(event);' class='form-control input-sm txtmention' rel='$subject->subjectid' name='$subject->subjectid[]' id='txtmention' style='width:40px;' onfocus='this.select();' tabindex='".$tabindex."'/>
										</td>";
										$tabindex++;
								}
							}
					echo"<!-- <td style='text-align:center !important;' width='50'>
								<a>
						 			<img onclick='saveinline(event,$std->studentid);' src='".site_url('../assets/images/icons/save.png')."'/>
						 		</a>
						 	</td> -->
						</tr>";
						$i++;
				}
				echo "</tbody>";

			}else{
				echo 'false';
			}
		}


		function validateeva($yearid,$classid,$schoolid,$evaluate_type,$month){
			$where='';
			if($evaluate_type=='month')
				$where.=" AND month='$month'";
			$count=$this->db->query("SELECT count(T.times) as count FROM
					(SELECT count(evaluateid) as times 
					from sch_student_evaluated
					where yearid='$yearid' AND classid='$classid' AND evaluate_type='$evaluate_type' AND schoolid='$schoolid' {$where}
					GROUP BY transno) as T")->row()->count;
			if($evaluate_type=='month')
				if($count>0)
					return 1;
				else
					return 0;
			if($evaluate_type=='semester')
				if($count>=2)
					return 1;
				else
					return 0;
			if($evaluate_type=='Three Month')
				if($count>=3)
					return 1;
				else
					return 0;

		}
		function save(){
			
			$this->evaluate->save();
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("student/evaluate_moyes?m=$m&p=$p");
		}
		function getmonth(){
			$month=array(array('month'=>'Jan'),
	    				array('month'=>'Feb'),
	    				array('month'=>'Mar'),
	    				array('month'=>'Apr'),
	    				array('month'=>'May'),
	    				array('month'=>'Jun'),
	    				array('month'=>'Jul'),
	    				array('month'=>'Aug'),
	    				array('month'=>'Sep'),
	    				array('month'=>'Oct'),
	    				array('month'=>'Nov'),
	    				array('month'=>'Dec'));
			foreach ($month as $months) { 	   		
				echo "<option value='".$months['month']."'>".$months['month']."</option>";
			 }
		}
		function saveinline(){
			$evaluateid='';
			$c_date=date('Y-m-d');
			$user=$this->session->userdata('user_name');
			$schoolid=$this->session->userdata('schoolid');
			$date=$this->green->convertSQLDate($this->input->post('date'));
			$upd=$this->input->post('upd');
	  		$eva_type=$this->input->post('eva_type');
	  		$classid=$this->input->post('classid');
	  		$month=$this->input->post('month');
	  		$eva_semester=$this->input->post('semester');
	  		$teacher_kh=$this->input->post('teacher_kh');
	  		$teacher_en=$this->input->post('teacher_en');
	  		$teacher_fr=$this->input->post('teacher_fr');
	  		$transno=$this->input->post('transno');
	  		$studentid=$this->input->post('studentid');
            $avg_coefficient=$this->input->post('avg_coefficient');

	  		$count=$this->evaluate->validate($studentid,$transno);
	  		$yearid=$this->db->select('year')->from('sch_student_enrollment')->where('studentid',$studentid)->where('classid',$classid)->get()->row()->year;
	  		$data=array(
							'studentid'=>$studentid,
							'classid'=>$classid,
							'yearid'=>$yearid,
							'date'=>$date,
							'month'=>$month,
							'eval_semester'=>$eva_semester,
							'evaluate_type'=>$eva_type,
							'eng_teach_name'=>$teacher_fr,
							'kh_teacher'=>$teacher_kh,
							'schoolid'=>$schoolid,
							'forign_teacher'=>$teacher_en,
                            'avg_coefficient'=>$avg_coefficient
							);
	  		if($count>0){
	  			$data2=array('modified_date'=>$c_date,'modified_by'=>$user);
				$this->db->where('studentid',$studentid)->where('transno',$transno)->update('sch_student_evaluated',array_merge($data,$data2));
	  			$evaluateid=$this->db->where('transno',$transno)->where('studentid',$studentid)->get('sch_student_evaluated')->row()->evaluateid;
	  			$this->evaluate->clearmention($evaluateid);
	  		}else{
	  			$data2=array('created_date'=>$c_date,'created_by'=>$user,'transno'=>$transno);
				$this->db->insert('sch_student_evaluated',array_merge($data,$data2));
				$evaluateid=$this->db->insert_id();
				if($upd=='')
					$this->evaluate->updatetran($transno+1);
	  		}
	  		$level=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
	  		$data=array('evaluateid'=>$evaluateid,
	  			'level'=>$level,
	  			'yearid'=>$yearid);

			header("Content-type:text/x-json");
			echo json_encode($data);
		}
		function savemention(){
			$mention=$this->input->post('mention');
			$e_type=$this->input->post('eva_type');
			if($e_type!='Three Month')
				$mention=$this->evaluate->getstdmention($mention,$this->input->post('level'))->mention;
			$data=array('evaluateid'=>$this->input->post('evaluateid'),
						'subjectid'=>$this->input->post('subjectid'),
						'studentid'=>$this->input->post('studentid'),
						'mention'=>$mention,
						'type'=>2,
						'transno'=>$this->input->post('transno'),
						'score'=>$this->input->post('mention'));
			$this->db->insert('sch_student_evaluated_mention',$data);
			
		}
		function savesemester(){
			$studentid=$this->input->post('studentid');
			$transno=$this->input->post('transno');
			$classid=$this->input->post('classid');
			$yearid=$this->input->post('yearid');
			$months=$this->input->post('month');
			$month_tran=$this->input->post('tranmonth');
			$eval_semester=$this->input->post('semester');
			$totalscore=$this->input->post('total_score');
			$countsub=$this->input->post('countsub');
			$srtM=$month_tran;
			$month=explode(",", $srtM);
			// print_r($month);
			$month_num=count($month)-1;//get number of months
			for($i=0;$i<$month_num;$i++) {
				$each_months_avg+=($this->evaluate->getstdscore($month[$i],$studentid)->score)/$countsub;
				echo "$countsub each month :".$each_months_avg."/tr:".$month[$i]." /std/$studentid <br>";
			}
			$months_avg=number_format($each_months_avg/$month_num, 2, '.', '');//get month average
			$sem_exam_avg=number_format($totalscore/$countsub, 2, '.', '');//get average of semester exam
			$sem_avg=number_format(($months_avg+$sem_exam_avg)/2, 2, '.', '');
			$this->evaluate->savesemester($transno,$studentid,$classid,$yearid,$eval_semester,$months,$sem_exam_avg,$months_avg,$month_num,$sem_avg);
			
		}
		function rollback($transno){
			foreach ($this->db->where('transno',$transno)->get('sch_student_evaluated')->result() as $eva) {
				$this->evaluate->clearmention($eva->evaluateid);
			}
			$this->db->where('transno',$transno)->delete('sch_student_evaluated');
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("student/evaluate_moyes?m=$m&p=$p");
		}
		function gettransno(){
			echo $this->db->where('typeid',2)->get('sch_z_systype')->row()->sequence;
		}
		function edit($transno){
			$data['idfield']= $this->idfield;		
			$data['thead']=	$this->thead;
			$datas['data']= $this->evaluate->getedit($transno);
			$data['page_header']="Evaluate";			
			$this->parser->parse('header', $data);
			$this->load->view('student/evaluate/evaluate_form_moyes', $datas);
			$this->parser->parse('footer', $data);
		}
		function deletes(){
            $transno=$this->input->post('transno');
            $json['del']=0;
            if($transno!=""){
                foreach ($this->db->where('transno',$transno)->get('sch_student_evaluated')->result() as $row) {
                    $this->db->where('evaluateid',$row->evaluateid)->delete('sch_student_evaluated_mention');
                }
                $this->db->where('transno',$transno)->delete('sch_stud_evalsemester');
                $this->db->where('transno',$transno)->delete('sch_student_evaluated');
                $json['del']=1;
            }
            header("Content-type:text/x-json");
            echo json_encode($json);
            exit();
		}
		
		function fillteacher(){
			$key=$_GET['term'];
			$data=$this->db->query("SELECT * 
								FROM sch_emp_profile emp
								INNER JOIN sch_emp_position p
								ON(emp.pos_id=p.posid)
								Where p.match_con_posid='tch' 
								AND (emp.last_name LIKE '%$key%' OR emp.first_name LIKE '%$key%')")->result();
			$array=array();
			foreach ($data as $row) {
				$array[]=array('value'=>$row->last_name.' '.$row->first_name,
								'id'=>$row->empid,
								'position'=>$row->position);
			}
		    echo json_encode($array);
		}
		function search(){
			if(isset($_GET['n'])){
					$name=$_GET['n'];
					$classid=$_GET['c'];
					$schlevelid=$_GET['l'];
					$month=$_GET['mo'];
					$semester=$_GET['sem'];
					$year=$_GET['y'];
					$m=$_GET['m'];
					$p=$_GET['p'];
					$eva_type=$_GET['type'];
					$sort_num=$_GET['s_num'];
					$data['tdata']=$this->evaluate->search($name,$eva_type,$semester,$schlevelid,$classid,$month,$year,$sort_num,$m,$p);
					$data['idfield']=$this->idfield;		
					$data['thead']=	$this->thead;
					$data['page_header']="Evaluate";			
					$this->parser->parse('header', $data);
					$this->parser->parse('student/evaluate/evaluate_list_moyes', $data);
					$this->parser->parse('footer', $data);
			}else{
					$name=$this->input->post('name');
					$classid=$this->input->post('classid');
					$schlevelid=$this->input->post('schlevelid');
					$month=$this->input->post('month');
					$semester=$this->input->post('semester');
					$eva_type=$this->input->post('eva_type');
					$yearid=$this->input->post('yearid');
					$sort_num=$this->input->post('sort_num');
					$sort=$this->input->post('sort');
					$m=$this->input->post('m');
					$p=$this->input->post('p');
					$sort=str_replace('__', '.', $sort);
					$i=1;
					$sch=$this->session->userdata('schoolid');
					$arrtran[]=array();
					$this->green->setActiveRole($this->input->post('roleid'));
					if($m!=''){
				        $this->green->setActiveModule($m);
				    }
				    if($p!=''){
				        $this->green->setActivePage($p); 
				    }
					foreach ($this->evaluate->search($name,$eva_type,$semester,$schlevelid,$classid,$month,$yearid,$sort_num,$m,$p,$sort) as $row) {

                        /*$g_level=$this->db->where('classid',$row->classid)->get('sch_class')->row()->grade_levelid;
						$count_grade=$this->db->query("SELECT COUNT(grade_levelid) as c 
														FROM sch_level_subject_detail 
														WHERE grade_levelid='".$g_level."'
														AND year='".$row->yearid."'
														AND schoolid='".$sch."'")->row()->c;	*/
						echo "<tr>
							 	  <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td><td>";

								 if(!isset($arrtran[$row->transno])){
                                     echo $this->green->formatSQLDate($row->date);
                                 }

                        echo "</td><td class='s__fullname'>".$row->last_name_kh.' '.$row->first_name_kh."</td>
                        		<td class='s__fullname'>".$row->last_name.' '.$row->first_name."</td>
								 <td class='s__class_name'>".$row->class_name."</td>
								 <td class='month'>".$row->month."</td>";

								if($row->evaluate_type!='Three Month'){
									/*echo  "<td class='score'>".$this->evaluate->getaverage($row->transno,$row->evaluateid)->score."</td>
									 <td class='average'>".number_format($this->evaluate->getaverage($row->transno,$row->evaluateid)->score/$count_grade, 2, '.', '')."</td>
									 <td class='rank'>".$this->evaluate->getrank($row->transno,$row->evaluateid)."</td>";*/
								}else{
									echo  "<td class='score'></td>
									 <td class='average'></td>
									 <td class='rank'></td>";
									}
								echo "<td class='remove_tag no_wrap'>";
								$tr="";
									if(!isset($arrtran[$row->transno])){

                                        if($this->green->gAction("P")){
                                            $preview_link=site_url("student/evaluate_moyes/preview/".$row->transno."?y=".$row->yearid."&sy=".$row->schlevelid."&cl=".$row->classid."&m=".$row->month);
                                            $tr.="<a title='Preview All This Class' href='".$preview_link."' target='_blank'>
												 		<img title='Preview All This Class' src='".site_url('../assets/images/icons/preview.png')."'/>
												</a>";
                                        }
                                        if($this->green->gAction("P")){
                                            $preview_link=site_url("student/evaluate_moyes/preview_result/".$row->transno."?y=".$row->yearid."&sy=".$row->schlevelid."&cl=".$row->classid."&m=".$row->month);
                                            $tr.="<a title='Preview Result result for monthly' href='".$preview_link."' target='_blank'>
												 		<img title='Preview result for monthly' src='".site_url('../assets/images/icons/preview.png')."'/>
												</a>";
                                        }
										if($this->green->gAction("D")){	
											$tr.="<a  title='Delete All This Class' class='link_delete' href='javascript:void(0)' rel='".$row->transno."'>
												 		<img   title='Delete All This Class' src='".site_url('../assets/images/icons/delete.png')."'/>
												 </a>";
										}
										if($this->green->gAction("U")){
                                            $transno=$row->transno;
                                            $edit_link=site_url("student/evaluate_moyes/edit/".$transno."?m=$m&p=$p");
											$tr.="<a  title='Edit All This Class' href='".$edit_link."' target='_blank'>
												 		<img  title='Edit All This Class' rel='".$row->transno."' src='".site_url('../assets/images/icons/edit.png')."'/>
												 </a>" ;
										}
									}	
									echo $tr;
									echo "</td>
												 
										 	</tr>
										 ";
									 	$i++;
									 	$arrtran[$row->transno]=$row->transno;
					}
						
					echo "<tr class='remove_tag'>
											<td colspan='12' id='pgt'>
												<div style='margin-top:20px; width:10%; float:left;'>
												Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
																
																$num=50;
																for($i=0;$i<10;$i++){?>
																	<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
																	<?php $num+=50;
																}
																
															echo "</select>
												</div>
												<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
													<ul class='pagination' style='text-align:center'>
													 ".$this->pagination->create_links()."
													</ul>
												</div>

											</td>
										</tr> ";
			}
			
		}
		function searchsemester(){
			if(isset($_GET['n'])){
					$name=$_GET['n'];
					$classid=$_GET['c'];
					$year=$_GET['y'];
					$semester=$_GET['se'];
					$schlevelid=$_GET['l'];
					$m=$_GET['m'];
					$p=$_GET['p'];
					$sort_num=$_GET['s_num'];
					$data['tdata']=$this->evaluate->searchsemester($name,$schlevelid,$classid,$year,$semester,$sort_num,$m,$p);
					$data['idfield']=$this->idfield;		
					$data['semthead']=	$this->semthead;
					$data['page_header']="Evaluate";			
					$this->parser->parse('header', $data);
					$this->parser->parse('student/evaluate/semester_list_moyes', $data);
					$this->parser->parse('footer', $data);
			}else{
					$name=$this->input->post('name');
					$classid=$this->input->post('classid');
					$schlevelid=$this->input->post('schlevelid');
					$sort_num=$this->input->post('sort_num');  
					$semester=$this->input->post('semester');
					$sort=$this->input->post('sort');
					$sort=str_replace('__', '.', $sort);
					$i=1;
					$yearid=$this->input->post('yearid');
					$sch=$this->session->userdata('schoolid');
					$m=$this->input->post('m');
					$p=$this->input->post('p');
					$this->green->setActiveRole($this->input->post('roleid'));
					if($m!=''){
				        $this->green->setActiveModule($m);
				    }
				    if($p!=''){
				        $this->green->setActivePage($p); 
				    }
					foreach ($this->evaluate->searchsemester($name,$schlevelid,$classid,$yearid,$semester,$sort_num,$m,$p,$sort) as $row) {
							echo "<tr>
								 	 <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
									 <td class='s__fullname'>".$row->fullname."</td>
									 <td class='s__class_name  '>".$row->class_name."</td>
									 <td class='month'>".$row->months."</td>
									 <td class='semester'>".$row->semester."</td>
									 <td class='se__sem_exam_avg'>".$row->sem_exam_avg."</td>
									 <td class='se__months_avg'>".$row->months_avg."</td>
									 <td class='se__sem_avg'>".$row->sem_avg."</td>
									 <td class='se__sem_mention'>".$row->sem_mention."</td>
									 <td class='se__sem_result'>".$row->sem_result."</td>
									 <td class='rank'>".$this->evaluate->getsemrank($row->transno,$row->semid)."</td>
									 <td class='remove_tag no_wrap'>";
								 	if($this->green->gAction("P")){	
										 	echo "<a>
											 		<img rel='".$row->semid."' onclick='preview(event);' src='".site_url('../assets/images/icons/preview.png')."'/>
											</a>";
									}
										echo "</td> 
									 	</tr>
									 ";
							 	$i++;
					}
						
					echo "<tr class='remove_tag'>
											<td colspan='12' id='pgt'>
												<div style='margin-top:20px; width:10%; float:left;'>
												Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
																
																$num=50;
																for($i=0;$i<10;$i++){?>
																	<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
																	<?php $num+=50;
																}
																
															echo "</select>
												</div>
												<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
													<ul class='pagination' style='text-align:center'>
													 ".$this->pagination->create_links()."
													</ul>
												</div>

											</td>
										</tr> ";
			}
			
		}

		function searchresult(){
			if(isset($_GET['n'])){
					$name=$_GET['n'];
					$classid=$_GET['c'];
					$year=$_GET['y'];
					$schlevelid=$_GET['l'];
					$sort_num=$_GET['s_num'];
					$s_result=$_GET['r'];
					$data['tdata']=$this->evaluate->searchsemester($name,$schlevelid,$classid,$year,$s_result,$sort_num);
					$data['idfield']=$this->idfield;		
					$data['thead']=	$this->semthead;
					$data['page_header']="Evaluate";			
					$this->parser->parse('header', $data);
					$this->parser->parse('student/evaluate/semester_list_moyes', $data);
					$this->parser->parse('footer', $data);
			}else{
					$name=$this->input->post('name');
					$classid=$this->input->post('classid');
					$schlevelid=$this->input->post('schlevelid');
					$sort_num=$this->input->post('sort_num');
					$s_result=$this->input->post('s_result');
					$sort=$this->input->post('sort');
					$sort=str_replace('__', '.', $sort);
					$i=1;
					$yearid=$this->input->post('yearid');
					$sch=$this->session->userdata('schoolid');
					foreach ($this->evaluate->searchresult($name,$schlevelid,$classid,$yearid,$s_result,$sort_num,$sort) as $row) {
							$semester=explode(',',$row->avg);
							$level=$this->db->where('classid',$row->classid)->get('sch_class')->row()->grade_levelid;
							$result='';
							$minscore=5;
							$avg=$row->total/2;
							if($row->total/2>=$minscore)	
								$result='Pass';
							else
								$result='Fail';						
							echo "<tr>
								 	 <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
									 <td class='s__fullname'>".$row->fullname."</td>
									 <td class='s__class_name  '>".$row->class_name."</td>
									 <td class='semester_one'>".number_format($semester[0], 2, '.', '')."</td>
									 <td class='semester_two'>".number_format($semester[1], 2, '.', '')."</td>
									 <td class='total'>".number_format($avg, 2, '.', '')."</td>
									 <td class='mention'>".$this->evaluate->getstdmention($avg,$level)->mention."</td>
									 <td class='result'>".$result."</td>
								
									 
								 	</tr>
								 ";
							 	$i++;
					}
						
					echo "<tr class='remove_tag'>
							<td colspan='12' id='pgt'>
								<div style='margin-top:20px; width:10%; float:left;'>
								Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
												
												$num=50;
												for($i=0;$i<10;$i++){?>
													<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
													<?php $num+=50;
												}
												
											echo "</select>
								</div>
								<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
									<ul class='pagination' style='text-align:center'>
									 ".$this->pagination->create_links()."
									</ul>
								</div>

							</td>
						</tr> ";
			}
			
		}

        public function getStudentInfo()
        {

            $yearid 		= $this->input->post('yearid');
            $schlevelid 	= $this->input->post('schlevelid');
            $classid 		= $this->input->post('classid');
            $transno 		= $this->input->post('transno');

            $students = $this->evaluate->getStudentInfo($yearid,$classid,$schlevelid,$transno);
            $subj_type=$this->evaluate->gets_type($classid,$yearid,0);
            $arrScores=$this->evaluate->getMoYScoreByTran($transno);
            $tdata='';
            if(count($students)>0){
                $i=1;
                foreach($students as $sturow){

                    $total_score=0;
                    $avg=0;
                    $tdata.='<tr><td>'.$i.'</td>
                                <td class="no_wrap">'.$sturow->fullname_kh.'</td>';
                    if(count($subj_type)>0){

                        $arrSubByClass=array();
                        $count_sub=0;
                        foreach($subj_type as $stype){

                            if(!isset($arrSubByClass[$classid][$stype->subj_type_id][$yearid])){
                                $getSubjbyClass=$this->evaluate->getsubject($classid,$stype->subj_type_id,$yearid,0);
                            }
                            if(count($getSubjbyClass)>0){
                                foreach($getSubjbyClass as $sub){

                                    $score=isset($arrScores[$transno][$sturow->studentid][$sub->subjectid])?$arrScores[$transno][$sturow->studentid][$sub->subjectid]:"";
                                    if($score > 0)$score = $score; else $score ='';
                                    $tdata.='<td style="text-align: center">'.$score.'</td>';
                                    $total_score+=$score;
                                }
                            }
                            $arrSubByClass[$classid][$stype->subj_type_id][$yearid]=$sturow->studentid;
                            $count_sub+=$stype->s_total;
                        }


			        	if($sturow->avg_coefficient>0){
                            $count_sub=$sturow->avg_coefficient;
                        }
                        $avg=$total_score/($count_sub==0?1:$count_sub);
                        $rank_score = $this->evaluate->getrank($transno,$sturow->evaluateid);
                        $mention="";
                        if($this->evaluate->getstdmention($avg,$schlevelid)){
							$mention=$this->evaluate->getstdmention($avg,$schlevelid)->mention_kh;
                        }                        

						$tdata.='<td style="font-size:12px !important;font-weight:bold;" align="right">'.number_format($total_score,2).'</td>
								 <td style="font-size:12px !important;font-weight:bold;" align="right">'.number_format($avg,2).'</td>
								 <td style="font-size:12px !important;font-weight:bold;" align="right">'.$rank_score.'</td>
								 <td style="font-size:12px !important;font-weight:bold;" align="right">'.$mention.'</td>
								';
                    }
                    $tdata.='</tr>';
                    $i++;
                }

            }

            $arrJson = array();
            $arrJson['tdata']=$tdata;
            header('Content-type:text/x-json');
            echo json_encode($arrJson);
            die();
        }

        function getsub_type_export(){
            $classid=$this->input->post('classid');
            $schoolid=$this->session->userdata('schoolid');
            $evaluate_type=$this->input->post('evaluate_type');
            $month=$this->input->post('month');
            $date=date('Y-m-d',strtotime($this->input->post('eva_date')));
            $year=$this->input->post('year');
            $avg_coefficient=$this->input->post('avg_coefficient');
            $mention=array('A','B','C','D','E','F');
            $action='';
            if($month!='')
                $month=implode(',',$month);

            $schlevelid=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
            $isvtc=$this->db->where('schlevelid',$schlevelid)->get('sch_school_level')->row()->is_vtc;
            $count=$this->validateeva($year,$classid,$schoolid,$evaluate_type,$month);
            $countgetstudent = $this->evaluate->getstudent($classid,$year);
            $trimester=0;
            $eval_semester='';

            if($evaluate_type!='month'){
                $eval_semester=$this->input->post('eva_semester');
            }

            if($count==0){
                if($evaluate_type!='Three Month')
                    $action='return isNumberKey(event);';
                else
                    $trimester=1;
                $html='<div class="col-sm-12">
			            <div class="col-sm-12 set_font_kh" align="center" style="font-size: 16px;font-weight:bold;"><b>ព្រះរាជាណាចក្រកម្ពុជា</b></div>
			            <div class="col-sm-12 set_font_kh" align="center" style="font-size: 16px;font-weight:bold;"><b>ជាតិ សាសនា ព្រះមហាក្សត្រ</b></div>
			            <div class="col-sm-12" style="height:10px;" style="font-size: 16px;font-weight:bold;">&nbsp;</div>
			           	<div class="col-sm-12 set_font_kh" align="center" style="font-size: 16px;font-weight:bold;"><b>សាលាហេបភីច័ន្ទតារានារីព្រែកថ្មី</b></div>
			            <div class="col-sm-12 set_font_kh underlinebouble" align="center" style="font-size: 16px;font-weight:bold;"><b>ចំណាត់ថ្នាក់ប្រចាំ ខែ '.$this->evaluate->khmonth($month).' ថ្នាក់ទី "​ '.($this->evaluate->setClassName($classid)).' "​ ឆ្នាំសិក្សា '.$this->green->GetYear($year).' </div>
			        </div>
			        <table>
			        	<tr>
			        		<td><b>Year:&nbsp;</b></td><td align="left">'.$year.'</td>
			        	</tr>
			        	<tr>
			        		<td><b>Month:&nbsp;</b></td><td align="left">'.$month.'</td>
			        	</tr>
			        	<tr>
			        		<td><b>Evaluate Date:&nbsp;</b></td><td align="left">'.$date.'</td>
			        		<td style="display:none">=YEAR(B8) &"-"&MONTH(B8)&"-"&DAY(B8)</td>
			        	</tr>
			        	<tr>
			        		<td><b>Evaluate Type:&nbsp;</b></td><td align="left">'.$evaluate_type.'</td>
			        	</tr>
			        	<tr>
			        		<td><b>Class:&nbsp;</b></td><td align="left">'.$classid.'</td>
			        	</tr>
			        	<tr>
			        		<td><b>School ID:&nbsp;</b></td><td align="left">'.$schoolid.'</td>
			        	</tr>
			        	<tr>
			        		<td><b>School Level:&nbsp;</b></td><td align="left">'.$schlevelid.'</td>
			        	</tr>
			        	<tr>
			        		<td><b>Semester:&nbsp;</b></td><td align="left">'.$eval_semester.'</td>
			        	</tr>
			        	<tr>
			        		<td><b>Average Coefficient:&nbsp;</b></td><td align="left">'.$avg_coefficient.'</td>
			        	</tr>
			        </table>
                  	<table class="table" id="tblstdmentionexport" border="1">';
                $html.="<thead>
							<tr>
			        			<th rowspan='3' style='vertical-align: middle !important;'>លេខ</th>
			        			<th rowspan='3' style='vertical-align: middle !important;'>ឈ្មោះ</th>
			        			<th rowspan='3' style='vertical-align: middle !important;'>កូដ</th>";
                //==================subject type========================
                foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
                    $html.="<th colspan='$sub->s_total' style='text-align:center !important;'>$sub->subject_type</th>";
                }
                $html.="</tr>";
                ////=========================subject========================
                $html.="<tr>";
                foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
                    foreach ($this->evaluate->getsubject($classid,$sub->subj_type_id,$year,$trimester) as $subject) {
                        $html.='<th>'.$subject->subjectid.'</th>';
                    }
                }
                $html.="</tr>";
                $html.="<tr>";
                foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
                    foreach ($this->evaluate->getsubject($classid,$sub->subj_type_id,$year,$trimester) as $subject) {
                        $html.='<th>'.$subject->subject_kh.'</th>';
                    }
                }
                $html.="</tr>";
                $html.="</thead>";
                $html.="<tbody>";
                $i=1;
                if(count($countgetstudent) > 0){
	                foreach ($countgetstudent as $std) {
	                    $html.="<tr>
									<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
									<td>".$std->fullname_kh."</td>
									<td>".$std->studentid."</td>";
	                    foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
	                        foreach ($this->evaluate->getsubject($classid,$sub->subj_type_id,$year,$trimester) as $subject) {
	                            $html.="<td>&nbsp;</td>";
	                        }

	                    }
	                    $html.="</tr>";
	                    $i++;
	                }

	                
	             }
                	$html.="</tbody></table>";
                	$html.="<table border='0'><tbody><tr><td colspan='16'>&nbsp;</td></tr><td colspan='16'>&nbsp;</td></tr>";
                	foreach ($this->evaluate->gets_type($classid,$year,$trimester) as $sub) {
	                    foreach ($this->evaluate->getsubject($classid,$sub->subj_type_id,$year,$trimester) as $subject) {
	                        $html.='<tr><td>'.$subject->subject_kh.'</td><td align="left">'.$subject->subjectid.'</td></tr>';
	                    }
           			}
           			$html.="<td colspan='16'>&nbsp;</td></tr><td colspan='16'>&nbsp;</td></tr></tbody></table>";
                echo $html;
            }

        }

        public function importStudentScore()
        {

            $result=$this->evaluate->saveImportStudentScore();
            if($result==1 || $result==true)
            {
                $data['error']="Student score was imported.";
            }else
            {
                $data['error']="No student score to import.";
            }
            $this->add($data);

        }

        public function getResultPointMonthly()
        {

            $yearid 		= $this->input->post('yearid');
            $schlevelid 	= $this->input->post('schlevelid');
            $classid 		= $this->input->post('classid');
            $transno 		= $this->input->post('transno');

            $students = $this->evaluate->getStudentInfo($yearid,$classid,$schlevelid,$transno);
            $subj_type=$this->evaluate->gets_type($classid,$yearid,0);
            $arrScores=$this->evaluate->getMoYScoreByTran($transno);
            $tdata='';
            if(count($students)>0){
                $i=1;
                foreach($students as $sturow){

                    $total_score=0;
                    $avg=0;
                    $tdata.='<tr><td align="center">'.$i.'</td>
                                <td>'.$sturow->fullname_kh.'</td>';
                    if(count($subj_type)>0){

                        $arrSubByClass=array();
                        $count_sub=0;
                        foreach($subj_type as $stype){

                            if(!isset($arrSubByClass[$classid][$stype->subj_type_id][$yearid])){
                                $getSubjbyClass=$this->evaluate->getsubject($classid,$stype->subj_type_id,$yearid,0);
                            }
                            if(count($getSubjbyClass)>0){
                                foreach($getSubjbyClass as $sub){

                                    $score=isset($arrScores[$transno][$sturow->studentid][$sub->subjectid])?$arrScores[$transno][$sturow->studentid][$sub->subjectid]:"";
                                    if($score > 0)$score = $score; else $score ='';
                                    $total_score+=$score;
                                }
                            }
                            $arrSubByClass[$classid][$stype->subj_type_id][$yearid]=$sturow->studentid;
                            $count_sub+=$stype->s_total;
                        }


			        	if($sturow->avg_coefficient>0){
                            $count_sub=$sturow->avg_coefficient;
                        }
                        $avg=$total_score/($count_sub==0?1:$count_sub);
                        $rank_score = $this->evaluate->getrank($transno,$sturow->evaluateid);
                        $mention="";
                        if(count($this->evaluate->getstdmention($avg,$schlevelid))>0){
                            $mention=$this->evaluate->getstdmention($avg,$schlevelid)->mention_kh;
                        }
						$tdata.='<td style="font-weight:bold;" align="right">'.$total_score.'</td>
								 <td style="font-weight:bold;" align="right">'.number_format($avg,2).'</td>
								 <td style="font-weight:bold;" align="right">'.$rank_score.'</td>
								 <td style="font-weight:bold;" align="right">'.$mention.'</td>
								​​​​​​​​<th>&nbsp;</th>
								';
                    }
                    $tdata.='</tr>';
                    $i++;
                }
               		 
            }

            $arrJson = array();
            $arrJson['tdata']=$tdata;
            header('Content-type:text/x-json');
            echo json_encode($arrJson);
            die();
        }
}
