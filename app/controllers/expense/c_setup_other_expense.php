<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_setup_other_expense extends CI_Controller {	

	function __construct(){
		parent::__construct();
		// $this->load->model('setup/area_model', 'm');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('expense/v_setup_other_expense');
		$this->load->view('footer');	
	}

	public function v_graphic_test(){	
		$this->load->view('header');
		$this->load->view('expense/v_graphic_test');
		$this->load->view('footer');	
	}

	public function edit(){	
		$otherfeeid = $this->input->post('otherfeeid');
		$r = $this->db->query("SELECT
									f.otherfeeid,
									f.otherfee_code,
									f.otherfee,
									f.note
								FROM
									sch_setup_other_expense AS f  
								WHERE f.otherfeeid = '{$otherfeeid}' ")->row();
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($r);		
	}

	public function delete(){	
		$otherfeeid = $this->input->post('otherfeeid');
		$i = 0;
		$i = $this->db->delete("sch_setup_other_expense", ['otherfeeid' => $otherfeeid]);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;	
	}

	public function grid(){
		$m = $this->input->post('m');
		$p = $this->input->post('p');
		$this->green->setActiveRole($this->session->userdata('roleid'));		
		$this->green->setActiveModule($m);		
		$this->green->setActivePage($p);

		$offset = trim($this->input->post('offset'));
		$limit = trim($this->input->post('limit'));
		$search_otherfee_code = trim($this->input->post('search_otherfee_code'));
		$search_otherfee = trim($this->input->post('search_otherfee'));
		
		$w = "";
		if($search_otherfee_code != ""){			
			$w .= "AND f.otherfee_code LIKE '%".sqlStr($search_otherfee_code)."%' ";
		}
		if($search_otherfee != ""){			
			$w .= "AND f.otherfee LIKE '%".sqlStr($search_otherfee)."%' ";
		}

		$totalRecord = $this->db->query("SELECT f.otherfeeid FROM sch_setup_other_expense AS f WHERE 1=1 {$w} ")->num_rows();
		$totalPage = ceil($totalRecord/$limit) - 0;		

		$q = $this->db->query("SELECT
									f.otherfeeid,
									f.otherfee_code,
									f.otherfee,
									f.note
								FROM
									sch_setup_other_expense AS f
								WHERE 1=1 {$w}
								LIMIT $offset, $limit ");		
		$tr = '';
		$i = 1;
		if($q->num_rows() > 0){
			foreach($q->result() as $row){
				$whcode = $this->db->query("SELECT fd.otherfeeid FROM sch_other_expense_detail AS fd WHERE fd.otherfeeid = '{$row->otherfeeid}' ")->num_rows();

				$tr .= '<tr>
							<td>'.($i++ + $offset).'</td>
							<td>'.$row->otherfee_code.'</td>
							<td>'.$row->otherfee.'</td>							
							<td>'.$row->note.'</td>
							<td class="remove_tag no_wrap">';
						if($this->green->gAction("U")){ 
					$tr .= '<a href="javascript:;" class="btn btn-xs btn-success edit" data-otherfeeid="'.$row->otherfeeid.'" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>';
						}
					$tr .= '</td>
							<td class="remove_tag no_wrap">';
						if($this->green->gAction("D")){ 
					$tr .= '<a href="javascript:;" class="btn btn-xs btn-danger delete" data-otherfeeid="'.$row->otherfeeid.'" data-otherfeeid_chk="'.$whcode.'" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>';
						}
					$tr .= '</td>							
						</tr>';
			}
		}
		else{
			$tr .= '<tr>
						<td colspan="5" style="font-weight: bold;text-align: center;background: #F2F2F2;">No data</td>
					</tr>';
		}
		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}

	public function save(){
		$otherfeeid = $this->input->post('otherfeeid');
		$otherfee_code = trim($this->input->post('otherfee_code'));
		$otherfee = trim($this->input->post('otherfee'));		
		$note = trim($this->input->post('note'));
		
		$data = ['otherfee_code' => $otherfee_code,
					"otherfee" => $otherfee,
					"note" => $note
				];

		$w = "";
		if($otherfeeid - 0 > 0){			
			$w .= "AND f.otherfeeid != '{$otherfeeid}' ";
		}
		$q = $this->db->query("SELECT f.otherfeeid FROM sch_setup_other_expense AS f WHERE f.otherfee_code = '".sqlStr($otherfee_code)."' {$w} ");
		$q_ = $this->db->query("SELECT f.otherfeeid FROM sch_setup_other_expense AS f WHERE f.otherfee = '".sqlStr($otherfee)."' {$w} ");

		if($q->num_rows() == 0){
			if($q_->num_rows() == 0){
				if($otherfeeid - 0 > 0){
					$data['modified_date'] = date('Y-m-d H:i:s');
		        	$data['modified_by'] = $this->session->userdata('user_name');
					$this->db->update("sch_setup_other_expense", $data, ["otherfeeid" => $otherfeeid]);
					$dd['updated'] = 'Updated!';
				}
				else{
					$data['created_date'] = date('Y-m-d H:i:s');
		        	$data['created_by'] = $this->session->userdata('user_name');
					$this->db->insert("sch_setup_other_expense", $data);					
					$dd['saved'] = 'Saved!';
				}
			}
			else{
				$dd['existed'] = 'Other fee "'.$otherfee.'"'.' existed!';
			}
		}
		else{
			$dd['existed'] = 'Other fee code "'.$otherfee_code.'"'.' existed!';
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($dd);
	}

	public function get_category(){
		$q = $this->db->query("SELECT * FROM sch_stock_category AS c ORDER BY c.cate_name ASC");

		$opt = "";
		$opt .= '<option></option>';
		if($q->num_rows() > 0){
			foreach ($q->result() as $row) {
				$opt .= '<option value="'.$row->categoryid.'">'.$row->cate_name.'</option>';
			}
		}
		echo $opt;
	}


}