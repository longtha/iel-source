<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_other_expense extends CI_Controller {	

	function __construct(){
		parent::__construct();
		// $this->load->model('setup/area_model', 'm');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('expense/v_other_expense');
		$this->load->view('footer');	
	}

	public function graphic(){	
		$this->load->view('header');
		$this->load->view('expense/v_graphic');
		$this->load->view('footer');	
	}

	public function get_data_graphic(){	
		$yyear = $this->input->post('yyear');

		// expenses =======
		$sql = "SELECT
					--DATE_FORMAT(m.tran_date, '%b') AS m,
					--MONTH(m.tran_date) AS num_m,
					SUM(d.amt) AS to_amt
				FROM
					sch_other_expense_master AS m
				INNER JOIN sch_other_expense_detail AS d ON m.typeno = d.typeno
				INNER JOIN sch_setup_other_expense AS se ON d.otherfeeid = se.otherfeeid
				WHERE
					YEAR(d.trandate) = '{$yyear}'
				GROUP BY
					MONTH(m.tran_date) ";			
		$sql_ = "CREATE TEMPORARY TABLE tbl_expense (
					".$sql."
					 ) ";
		$this->db->query($sql_);
		$c = $this->db->query($sql)->num_rows();

		$arr_ex = [];
		$to_expense = 0;		
		
		for($m_ = 1; $m_ <= 12; $m_++){
			$to_amt_ex = $this->db->query("SELECT to_amt FROM tbl_expense WHERE tbl_expense.num_m = '{$m_}' ");			
			if($to_amt_ex->num_rows > 0){
				$to_expense += $to_amt_ex->row()->to_amt - 0;
				$arr_ex[] = $to_amt_ex->row()->to_amt;
			}
			else{
				$arr_ex[] = '';				
			}

		}

		$arr = ['arr_ex' => $arr_ex, 'to_expense' => $to_expense, 'c' => $c];
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
		$this->db->query("DROP TABLE IF EXISTS tbl_expense");
	}

	public function edit(){	
		$type = '41';
		$typeno = $this->input->post('typeno');
		$row = $this->db->query("SELECT
										m.type,
										m.typeno,
										m.note,
										m.total,
										DATE_FORMAT(m.tran_date, '%d/%m/%Y') AS tran_date
									FROM
										sch_other_expense_master AS m
									WHERE m.type= '{$type}' AND m.typeno = '{$typeno}' ")->row();
		$q = $this->db->query("SELECT
									e.otherfee,
									e.otherfee_code,
									d.remark,
									d.dis,
									d.qty,
									d.price,
									d.amt,
									d.otherfeeid
								FROM
									sch_other_expense_detail AS d
								INNER JOIN sch_setup_other_expense AS e ON e.otherfeeid = d.otherfeeid
								WHERE d.type= '{$type}' AND d.typeno = '{$typeno}' ")->result();
		$arr = ['row' => $row, 'q' => $q];
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);		
	}

	public function delete(){
		// transaction =====
		$this->db->trans_begin();

			$type = '41';
			$typeno = $this->input->post('typeno');		
			$this->db->delete("sch_other_expense_master", array('type' => $type, 'typeno' => $typeno));
			$this->db->delete("sch_other_expense_detail", array('type' => $type, 'typeno' => $typeno));
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$i = 0;
		}
		else{
			$this->db->trans_commit();
			$i = 1;
		}		
		header('Content-Type: application/json; charset=utf-8');
		echo $i;
	}

	public function grid(){

		$m = $this->input->post('m');
		$p = $this->input->post('p');
		$this->green->setActiveRole($this->session->userdata('roleid'));		
		$this->green->setActiveModule($m);		
		$this->green->setActivePage($p);

		$type = '41';
		$offset = trim($this->input->post('offset'));
		$limit = trim($this->input->post('limit'));
		$otherfee_code = trim($this->input->post('otherfee_code'));
		$otherfee_ = trim($this->input->post('otherfee_'));		
		$qty = trim($this->input->post('qty'));
		$from_date = trim($this->input->post('from_date'));
		$to_date = trim($this->input->post('to_date'));
		$whcode = trim($this->input->post('whcode'));
		
		$w = "";
		$ws = "";
		if($from_date != "" && $to_date != ""){			
			$w .= "AND date(m.tran_date) >= '".$this->green->formatSQLDate($from_date)."' ";
			$w .= "AND date(m.tran_date) <= '".$this->green->formatSQLDate($to_date)."' ";
		}
		// ==============		
		if($otherfee_code != ""){			
			$ws .= "AND e.otherfee_code LIKE '%".sqlStr($otherfee_code)."%' ";
		}
		if($otherfee_ != ""){			
			$ws .= "AND e.otherfee LIKE '%".sqlStr($otherfee_)."%' ";
		}			
		if($qty != ""){			
			$ws .= "AND d.qty = '{$qty}' ";
		}
		/*if($whcode != ""){			
			$ws .= "AND d.whcode = '{$whcode}' ";
		}*/	
		
		$totalRecord = $this->db->query("SELECT DISTINCT
												m.id
											FROM
												sch_other_expense_detail AS d
											INNER JOIN sch_setup_other_expense AS e ON d.otherfeeid = e.otherfeeid
											INNER JOIN sch_other_expense_master AS m ON m.typeno = d.typeno
											AND m.type = d.type
											WHERE 1=1 {$w} {$ws} ")->num_rows();
		$totalPage = ceil($totalRecord/$limit) - 0;		

		$sql = "SELECT DISTINCT
						m.typeno,
						DATE_FORMAT(m.tran_date, '%d/%m/%Y') AS tran_date
					FROM
						sch_other_expense_detail AS d
					INNER JOIN sch_setup_other_expense AS e ON d.otherfeeid = e.otherfeeid
					INNER JOIN sch_other_expense_master AS m ON m.typeno = d.typeno
					AND m.type = d.type
					WHERE 1=1 {$w} {$ws}
					ORDER BY m.typeno DESC
					LIMIT $offset, $limit ";
		$q = $this->db->query($sql);

		$tr = '';
		$g_total_qty = 0;
		$g_total_dis = 0;
		$g_total_amt = 0;		
		if($q->num_rows() > 0){
			foreach($q->result() as $row){
				$tr .='<tr class="trprint" style="_background: #4cae4c;color: #2e6da4;display: none;">
								<td colspan="8">
									<span style="text-align:right;"> #'.$row->typeno.'</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="_font-weight: bold; text-align:center;">Date : &nbsp;&nbsp;'.$row->tran_date.'</span>
								</td>
						   </tr>';

					$tr .= '<tr class="remove_tag trhide" style="_background: #4cae4c;color: #2e6da4;">
								<td colspan="8">';
								if($this->green->gAction("P")){ 
								$tr .= '<a href="'.site_url('expense/c_summary_expense_report/?typeno='.$row->typeno).'" class="btn btn-xs btn-success" title="Print..." target="_blank"><span class="glyphicon glyphicon-print"></span>&nbsp;#<span style="font-weight: bold;">'.$row->typeno.'</span></a>&nbsp;&nbsp; ';
								}
								$tr .= ' Date:<span style="_font-weight: bold;"> '.$row->tran_date.'</span> ';
								$tr .= '</td>
								<td>';
								if($this->green->gAction("U")){ 
								$tr .= '<a href="javascript:;" class="edit btn btn-xs btn-success" data-typeno="'.$row->typeno.'" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>';
								}
								$tr .= '</td>
								<td>';
								if($this->green->gAction("D")){ 
								$tr .= '<a href="javascript:;" class=" btn btn-xs btn-danger delete" data-typeno="'.$row->typeno.'" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>';
								}
								$tr .= '</td>
							</tr>';

				$sql1 = "SELECT DISTINCT
								d.id,
								d.type,
								d.typeno,
								d.otherfeeid,
								d.remark,
								d.dis,
								d.qty,
								d.price,
								d.amt,
								e.otherfee,
								e.otherfee_code
							FROM
								sch_other_expense_detail AS d
							INNER JOIN sch_setup_other_expense AS e ON d.otherfeeid = e.otherfeeid
							INNER JOIN sch_other_expense_master AS m ON m.typeno = d.typeno
							AND m.type = d.type
							WHERE 1=1 {$w} {$ws} AND d.type = '{$type}' AND d.typeno =  '{$row->typeno}' ";
				$q_d = $this->db->query($sql1);

				$i = 1;
				$total_qty = 0;
				$total_dis = 0;
				$total_amt = 0;
				if($q_d->num_rows() > 0){
					foreach ($q_d->result() as $row_d) {
						$amount = 0;
						$amount += ($row_d->qty - 0) * ($row_d->price- 0) * (1 - $row_d->dis/100);
						$total_qty += $row_d->qty - 0;
						$total_dis += $row_d->dis - 0;
						$total_amt += $amount - 0;
						$tr .= '<tr>
									<td style="border: 0;">'.$i++.'</td>
									<td style="border: 0;">'.$row_d->otherfee_code.'</td>
									<td style="border: 0;">'.$row_d->otherfee.'</td>
									<td style="border: 0;">'.$row_d->remark.'</td>
									<td style="text-align: right;border: 0;">'.$row_d->qty.'</td>
									<td style="text-align: right;border: 0;">'.number_format($row_d->price, 2).'</td>
									<td style="text-align: right;border: 0;">'.number_format($row_d->dis, 2).'</td>
									<td style="text-align: right;border: 0;">'.number_format($amount, 2).'</td>
									<td colspan="2" class="remove_tag" style="border: 0;">&nbsp;</td>
								</tr>';						
					}
					$tr .= '<tr style="font-weight: bold;">
								<td style="border: 0;text-align: right;font-weight: normal;" colspan="4">Total</td>
								<td style="text-align: right;border: 0;">'.number_format($total_qty, 0).'</td>
								<td style="text-align: right;border: 0;">&nbsp;</td>
								<td style="text-align: right;border: 0;">'.number_format($total_dis, 2).'</td>
								<td style="text-align: right;border: 0;">'.number_format($total_amt, 2).'</td>
								<td class="remove_tag" colspan="2" style="border: 0;">&nbsp;</td>
							</tr>';
				}
				else{

					

				}// detail ========	


				$g_total_qty += $total_qty - 0;
				$g_total_dis += $total_dis - 0;
				$g_total_amt += $total_amt - 0;				
			}
			$tr .= '<tr style="font-weight: bold;background: #F2F2F2;">
						<td style="text-align: right;font-weight: normal;_border: 0;" colspan="4">Grand total</td>
						<td style="text-align: right;_border: 0;">'.number_format($g_total_qty, 0).'</td>
						<td style="text-align: right;_border: 0;">&nbsp;</td>
						<td style="text-align: right;_border: 0;">'.number_format($g_total_dis, 2).'</td>
						<td style="text-align: right;_border: 0;">'.number_format($g_total_amt, 2).'</td>
						<td class="remove_tag" colspan="2" style="_border: 0;">&nbsp;</td>
					</tr>';
		}
		else{
			$tr .= '<tr>
						<td colspan="10" style="font-weight: bold;text-align: center;background: #F2F2F2;">No data!</td>
					</tr>';
		}
		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}

	// master_detail =============
	public function edit_category(){
		$id = $this->input->post('id');
		$r = $this->db->query("SELECT * FROM sch_inv_categories AS c WHERE c.id = '{$id}' ")->row();
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($r);
	}

	public function save_op_md(){
		$type = "41";		
		$tran_date = $this->green->formatSQLDate(trim($this->input->post('tran_date'))).' '.date('H:i:s');
		$typeno_ = trim($this->input->post('typeno'));		
		$note = trim($this->input->post('note'));
		$total = trim($this->input->post('total'));		
		$arr = $this->input->post('arr');
		$user_name = $this->session->userdata('user_name');

		// transaction =====
    	$this->db->trans_begin();

			// master ======
			$data_ms = ["type" => $type,
						"created_by" => $user_name,
						"tran_date" => $tran_date,						
						"note" => $note,
						"total" => $total						
					];		

			$i = 0;
			if($typeno_ != ""){
				$typeno = $typeno_;
				$data_ms['modified_by'] = $user_name;
				$data_ms['modified_date'] = $tran_date;			
				$this->db->update("sch_other_expense_master", $data_ms, array("type" => $type, 'typeno' => $typeno));

				// delete =======
				$this->db->delete("sch_other_expense_detail", array("type" => $type, 'typeno' => $typeno));			
				$i = 2;
			}
			else{
				$typeno = $this->green->nextTran($type, "Others Expenses");
				$data_ms["typeno"] = $typeno;
				$this->db->insert("sch_other_expense_master", $data_ms);				
				$i = 1;
			}

			if(!empty($arr)){
				foreach ($arr as $row) {
					if($row["otherfee_code"] != ""){
						$data_d = ["type" => $type,
									"typeno" => $typeno,
									"trandate" => $tran_date,
									"otherfeeid" => $row["otherfeeid"],
									"remark" => $row["remark"],
									"qty" => $row["qty"],
									"price" => $row["price"],
									"dis" => $row["dis"],
									"amt" => $row["amt"],
									"created_by" => $user_name,
									"created_date" => $tran_date
								];
						$this->db->insert("sch_other_expense_detail", $data_d);		
					}	
				}
			}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$i = 4;
		}
		else{
			$this->db->trans_commit();
			$i;
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($i);
	}

	public function get_category(){
		$q = $this->db->query("SELECT * FROM sch_stock_category AS c ORDER BY c.cate_name ASC");

		$opt = "";
		$opt .= '<option></option>';
		if($q->num_rows() > 0){
			foreach ($q->result() as $row) {
				$opt .= '<option value="'.$row->categoryid.'">'.$row->cate_name.'</option>';
			}
		}
		echo $opt;
	}

	// autocomplete =======
	public function get_std(){
		$term = trim($_REQUEST['term']);
		$where = "";
		if($term != ""){
			$where .= "AND ( e.otherfee_code LIKE '%".sqlStr($term)."%' ";
			$where .= "or e.otherfee LIKE '%".sqlStr($term)."%' ) ";	     
		}
		$qr = $this->db->query("SELECT
									e.otherfeeid,
									e.otherfee_code,
									e.otherfee
								FROM
									sch_setup_other_expense AS e
								WHERE
									1 = 1 {$where} LIMIT 0, 10 ");
		$arr = [];
		if($qr->num_rows() > 0){
			foreach ($qr->result() as $row) {
				$arr[] = ['otherfeeid' => $row->otherfeeid, 'otherfee_code' => $row->otherfee_code, 'otherfee' => $row->otherfee];            
			}
		}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
	}

	// check stockcode =======
	public function chk_stockcode(){
		$otherfee_code = trim($this->input->post('otherfee_code'));

		if($otherfee_code != ""){
			$q = $this->db->query("SELECT * FROM sch_setup_other_expense AS e WHERE e.otherfee_code = '{$otherfee_code}' ");			
			if($q->num_rows() > 0){
				$d['r'] = $q->row();
				$d['y'] = 'Correct!';
			}
			else{
				$d['n'] = 'Other fee code "'.$otherfee_code.'" incorrect!';
			}
			header("Content-type: application/json; charset=utf-8");
			echo json_encode($d);
		}		
	}


}