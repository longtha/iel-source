<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_summary_expense_report extends CI_Controller {
	function __construct(){
		parent::__construct();
	}

  	function index(){
  		    $mo = $this->db->query("SELECT
                                        sch_other_expense_master.created_by,
                                        sch_other_expense_master.typeno,
                                        sch_other_expense_master.total,
                                        sch_other_expense_master.type,
                                        sch_other_expense_master.id,
                                        sch_other_expense_detail.dis,
                                        sch_other_expense_detail.qty,
                                        sch_other_expense_detail.price,
                                        sch_other_expense_detail.amt,
                                    	DATE_FORMAT(trandate, '%d/%m/%Y ') AS trandate,
                                        sch_other_expense_detail.otherfeeid,
                                        sch_other_expense_detail.remark,
                                        sch_setup_other_expense.otherfee_code,
                                        sch_setup_other_expense.otherfee
                                    FROM
                                        sch_other_expense_master
                                    LEFT JOIN sch_other_expense_detail ON sch_other_expense_master.typeno = sch_other_expense_detail.typeno
                                    LEFT JOIN sch_setup_other_expense ON sch_other_expense_detail.otherfeeid = sch_setup_other_expense.otherfeeid
                                    WHERE
                                        sch_other_expense_master.typeno = '".$_GET['typeno']."' ");


  		    $data['typeno']=$mo->row()->typeno;
            $data['trandate']=$mo->row()->trandate;
            $data['created_By']=$mo->row()->created_by;

            $trinv="";    
            $amttotal=0;
            $ii=1;
			
			 if($mo->num_rows() > 0){
                foreach($mo->result() as $rowde){
                    $trinv.='<tr class="info">
                            <td align="center">'.($ii++).'</td>
                            <td align="right">'.$rowde->otherfee_code.'</td>
                            <td align="right">'.$rowde->otherfee.'</td>
                            <td align="right">'.$rowde->qty.'</td>
                            <td align="right">'.$rowde->price.'</td>
                            <td align="right">'.$rowde->dis.'%</td>
                            <td align="right">'.number_format($rowde->amt,2).'$'.'</td>
                    </tr>';
                    $amttotal+=($rowde->amt);
                }
            }


		$data['amttotal']=$amttotal;
		$data['trinv']=$trinv;
	 	$this->load->view('expense/v_summary_expense_report',$data);

	 }


}