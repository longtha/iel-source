<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class illnesstype extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		
		parent::__construct();
		$this->load->library('pagination');	
		$this->lang->load('student', 'english');
		$this->load->model('health/modillnesstype','illness');
		$this->thead=array("No"=>'No',
			 				"Illness Type"=>'illness_type',
							"Consultation Type"=>'type_of_consultation',
							"Description"=>'description',
							"Action"=>'Action'							 	
							);
		$this->idfield="studentid";
	}
	function index(){
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		$data['tdata']=$this->illness->getillness($m,$p);
		$data['thead']=	$this->thead;
		$data['page_header']="Illness Type List";	
		$this->parser->parse('header', $data);
		$this->parser->parse('health/disease/illnesstype_list', $data);
		$this->parser->parse('footer', $data);
	}
	function delete($illnesstypeid){
		$this->db->where('illness_typeid',$illnesstypeid)
				->delete('sch_medi_illness_type');
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("health/illnesstype?m=$m&p=$p");
	}
	function save(){
		$illnesstypeid=$this->input->post('illnesstypeid');
		$illnesstype=$this->input->post('illness_type');
		$exist=false;
		if($illnesstypeid!=''){
			$count=$this->illness->validate($illnesstype,$illnesstypeid);
			if($count>0)
				$exist=true;
			else
				$this->illness->save($illnesstypeid);				
		}else{
			$count=$this->illness->validate($illnesstype);
			if($count>0)
				$exist=true;
			else
				$this->illness->save();
		}
		if($exist==true){
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			$data['tdata']=$this->illness->getillness($m,$p);
			$data['thead']=	$this->thead;
			$data['page_header']="Illness Type List";
			$data['exist']="Data has already exist...!";	
			$this->parser->parse('header', $data);
			$this->parser->parse('health/disease/illnesstype_list', $data);
			$this->parser->parse('footer', $data);
		}else{
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("health/illnesstype?m=$m&p=$p");
		}
		
	}
	function edit(){
		$illnesstypeid=$this->input->post('illness_typeid');
		$row=$this->db->where('illness_typeid',$illnesstypeid)->get('sch_medi_illness_type')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function search(){
		if(isset($_GET['s_num'])){
			$illness_type=$_GET['dc'];
			$ctype=$_GET['d'];
			$sort_num=$_GET['s_num'];
			$m=$_GET['m'];
			$p=$_GET['p'];
			$data['tdata']=$this->illness->search($illness_type,$ctype,$sort_num,$m,$p);
			$data['thead']=	$this->thead;
			$data['page_header']="Illness  Type List";	
			$this->parser->parse('header', $data);
			$this->parser->parse('health/disease/illnesstype_list', $data);
			$this->parser->parse('footer', $data);
		}else{
			$illness_type=$this->input->post('illness_type');
			$ctype=$this->input->post('type_of_consultation');
			$sort_num=$this->input->post('s_num');
			$i=1;
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
			foreach ($this->illness->search($illness_type,$ctype,$sort_num,$m,$p) as $row) {
				echo "<tr>
						<td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
						<td class='disease_code'><a target='_blank' href='".site_url("health/disease/?m=$m&p=$p&illness_typeid=".$row->illness_typeid)."'>".$row->illness_type."</a></td>
						<td class='illness_type'>";
							if ($row->type_of_consultation=='gr'){
								echo 'General';
							}else if ($row->type_of_consultation=='sp'){
								echo 'Special';
							}
						echo "</td>
						<td class='disease'>".$row->description."</td>
						<td class='remove_tag'>";
						 	if($this->green->gAction("D")){
								 echo "<a>
								 		<img rel=".$row->illness_typeid." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
								 	</a>";
							}
							if($this->green->gAction("U")){
								 echo "<a>
								 		<img  rel=".$row->illness_typeid." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
								 	</a>";
							}
					echo "</td>
				</tr>
				 ";
			 $i++;
			}
			//===========================show pagination=======================
			echo "<tr class='remove_tag'>
					<td colspan='12' id='pgt'>
						<div style='margin-top:20px; width:10%; float:left;'>
						Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
										
										$num=10;
										for($i=0;$i<20;$i++){?>
											<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
											<?php $num+=10;
										}
										
									echo "</select>
						</div>
						<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
							<ul class='pagination' style='text-align:center'>
							 ".$this->pagination->create_links()."
							</ul>
						</div>

					</td>
				</tr> ";
		}
		
	}
}