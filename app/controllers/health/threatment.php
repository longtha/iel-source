<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ThreatMent extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		
		parent::__construct();
		$this->load->library('pagination');	
		$this->lang->load('student', 'english');
		$this->load->model('health/ModeThreatment','threatments');
		$this->thead=array("No"=>'No',
			 				"Date"=>'date',
							"Patient Name"=>'patient_name',
							 "Gender"=>'sex',
							 "Patient"=>'patient',
							 "Class"=>'class_name',
							 "Medicine"=>'medicine',
							 "Action"=>'Action'							 	
							);
		$this->idfield="studentid";
	}
	function index(){
		
		$data['tdata']=$this->threatments->gettreatment();
		$data['thead']=	$this->thead;
		$data['page_header']="Threatment List";	
		$this->parser->parse('header', $data);
		$this->parser->parse('health/threatment/health_list', $data);
		$this->parser->parse('footer', $data);
	}
	function add(){
		$data['page_header']="New Threatment";
		$this->parser->parse('header', $data);
		$this->parser->parse('health/threatment/health_form', $data);
		$this->parser->parse('footer', $data);
	}
	function preview($treatmentid){
		$data['treatment']=$this->threatments->gettreatmentrow($treatmentid);
		$data['thead']=	$this->thead;
		$data['page_header']="Threatment List";	
		$this->parser->parse('header', $data);
		$this->parser->parse('health/threatment/preview_health', $data);
		$this->parser->parse('footer', $data);
	}
	function edit($treatid){
		$data['page_header']="New Threatment";	
		$data['query']=$this->threatments->gettreatmentrow($treatid);		
		$this->parser->parse('header', $data);
		$this->parser->parse('health/threatment/health_edit', $data);
		$this->parser->parse('footer', $data);
	}
	function getmedicine(){
		$catid=$this->input->post('catid');
		echo "<option value=''>Select Medicine</option>";
		foreach ($this->threatments->getmedicinebycat($catid) as $row) {
			echo "<option value='$row->stockid'>$row->descr_eng</option>";
		}
	}
	function getuom(){
		$ex_date=$this->input->post('ex_date');
		$stockid=$this->input->post('stockid');
		$query=$this->db->where('expired_date',$ex_date)
				->where('stockid',$stockid)
				->get('sch_stock_balance')->row();
		echo $query->uom;
	}
	function getexpire(){
		$stockid=$this->input->post('stockid');
		$whcode=$this->input->post('whcode');
		foreach ($this->threatments->getexpire($stockid,$whcode) as $row) {
			echo "<tr class='expiredrow'>
						<td id='expired_date'>".$this->green->formatSQLDate($row->expired_date)."</td>
						<td width='70'><input onkeyup='culculateqty(event);' type='text' class='form-control input-sm'  id='qty'/></td>
						<td id='s_qty'>$row->quantity</td>
						<td id='uom'>$row->uom</td>

				</tr>";
		}
	}
	function getdisease(){
		$illnesid=$this->input->post('illnessid')-0;
		echo "<option value=''>Select Disease</option>";
		foreach ($this->threatments->getdisease($illnesid) as $row) {
			echo "<option value='$row->diseaseid'>$row->disease</option>";
		}

		if($illnesid==10 || $illnesid==9){
			echo "<option value='freetext'>Free Text</option>";
		}

	}

	function getillness(){
		$consult=$this->input->post('consult');
		echo "<option value=''>Select illness</option>";
		foreach ($this->threatments->getillness($consult) as $row) {
			echo "<option value='$row->illness_typeid'>$row->illness_type</option>";
		}
	}
	function fillemp(){
		$key=$_GET['term'];
		$this->db->select('*')
				->from('sch_emp_profile emp')
				->join('sch_emp_position emp_p','emp.pos_id=emp_p.posid','inner')
				->like('last_name',$key)
				->or_like('first_name',$key);			
		$data=$this->db->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name.' '.$row->first_name,
							'id'=>$row->empid,
							'sex'=>$row->sex,
							'dob'=>$row->dob,
							'position'=>$row->position);
		}
	     echo json_encode($array);
	}
	function fillmedicine($catid){
		$key=$_GET['term'];
		$array=array();
		foreach ($this->threatments->getmedicinebycatname($catid,$key) as $row) {
			$array[]=array('value'=>$row->descr_eng,
							'id'=>$row->stockid);
		}
	     echo json_encode($array);
	}
	function fillstd($schlevelid=''){
		$key=$_GET['term'];
		
		$data=$this->db->query("SELECT DISTINCT *
											FROM
												v_student_profile s
											WHERE
												s.fullname LIKE '".$key."%'
											AND yearid = '".$this->session->userdata('year')."'									
											AND schlevelid='$schlevelid'
											ORDER BY yearid DESC ")->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->last_name.' '.$row->first_name,
							'id'=>$row->studentid,
							'sex'=>'Female',
							'dob'=>$this->green->ConvertSQLDate($row->dob),
							'position'=>$row->class_name,
							'classid'=>$row->classid,
							);
		}
	     echo json_encode($array);
	}
	function getschlevel(){
		$classid=$this->input->post('classid');
		echo $this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
	}
	function save(){
		$treatmentid=$this->input->post('treatmentid');
		if($treatmentid>0){
			$this->threatments->save($treatmentid);
		}else{
			$treatmentid=$this->threatments->save();
		}
		$this->do_upload($treatmentid);
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("health/threatment?m=$m&p=$p");
	}
	function delete($treatmentid){
		$this->db->where('treatmentid',$treatmentid)->delete('sch_medi_treatment');
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("health/threatment?m=$m&p=$p");
	}
	function do_upload($treatmentid){
		$config['upload_path'] ='./assets/upload/Health/treatment';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']= '5000';
		$config['file_name']= "$treatmentid.jpg";
		$config['overwrite']=true;
		$config['file_type']='image/png';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());			
		}
		else
		{		
			//$this->generate_thumb($this->upload->file_name,$this->upload->upload_path)	;	
			$data = array('upload_data' => $this->upload->data());			
			//redirect('setting/user');
			 	$config2['image_library'] = 'gd2';
                $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config2['new_image'] = './assets/upload/Health/treatment';
                $config2['maintain_ratio'] = TRUE;
                $config2['create_thumb'] = TRUE;
                $config2['thumb_marker'] = FALSE;
                $config2['width'] = 120;
                $config2['height'] = 180;
                $config2['overwrite']=true;
                $this->load->library('image_lib',$config2); 

                if ( !$this->image_lib->resize()){
                	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
				}else{

					//unlink('./assets/upload/students/'.$student_id.'.jpg');
				}
		}
	}
	function search(){
		$i=1;
		if(isset($_GET['per_page']))
			$i=$_GET['per_page']+1;
		//==================data==============
		if(isset($_GET['s_type'])){
				$doctorid=$_GET['doc'];
				$patient_name=$_GET['pat'];
				$classid=$_GET['class'];
				$schlevelid=$_GET['l'];
				$posid=$_GET['pos'];
				$s_type=$_GET['s_type'];
				$sort_num=$_GET['s_num'];
				$yearid=$_GET['y'];
				$m=$_GET['m'];
				$p=$_GET['p'];
				$data['tdata']=$this->threatments->search($doctorid,$patient_name,$posid,$schlevelid,$classid,$s_type,$sort_num,$m,$p,$yearid);
				$data['thead']=	$this->thead;
				$data['page_header']="Threatment List";	
				$this->parser->parse('header', $data);
				$this->parser->parse('health/threatment/health_list', $data);
				$this->parser->parse('footer', $data);
		}else{
				$doctorid=$this->input->post('doctor');
				$patient_name=$this->input->post('p_name');
				$classid=$this->input->post('class');
				$schlevelid=$this->input->post('schlevelid');
				$posid=$this->input->post('posid');
				$s_type=$this->input->post('s_type');
				$sort_num=$this->input->post('s_num');
				$yearid=$this->input->post('yearid');
				$m=$this->input->post('m');
				$p=$this->input->post('p');
				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
			        $this->green->setActiveModule($m);
			    }
			    if($p!=''){
			        $this->green->setActivePage($p); 
			    }
			    echo "sort:".$sort_num;
			    //exit();
				$query=$this->threatments->search($doctorid,$patient_name,$posid,$schlevelid,$classid,$s_type,$sort_num,$m,$p,$yearid);
				foreach ($query as $row) {
						echo "<tr>
								 <td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
									 <td class='date'>".$row->date."</td>
									 <td class='patient_name'>".$row->first_name." ".$row->last_name."</td>
									 <td class='sex'>".$row->sex."</td>
									 <td class='patient'>".$row->position."</td>
									 <td class='class_name'>$row->class_name</td>
									 <td class='medicine'>";
									 foreach ($this->threatments->gettreatmidecinegroup($row->treatmentid) as $medi) {
										echo "$medi->descr_eng <br>";
									}
								echo "</td>
										<td class='remove_tag'>";
										if($this->green->gAction("P")){	
											echo "<a>
											 		<img rel=".$row->treatmentid." onclick='preview(event);' src='".site_url('../assets/images/icons/preview.png')."'/>
											 	</a>";
										}
										if($this->green->gAction("D")){	
											echo "<a>
											 		<img rel=".$row->treatmentid." onclick='deletetreat(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
											 	</a>";
										}
										if($this->green->gAction("U")){	
											echo "<a>
											 		<img  rel=".$row->treatmentid." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
											 	</a>";
										}
									echo "</td>
									 </tr>
									 ";
							 $i++;
			}
			//===========================show pagination=======================
			echo "<tr class='remove_tag'>
					<td colspan='12' id='pgt'>
						<div style='margin-top:20px; width:10%; float:left;'>
						Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
										
										$num=50;
										for($i=0;$i<10;$i++){?>
											<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
											<?php $num+=50;
										}
										
									echo "</select>
						</div>
						<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
							<ul class='pagination' style='text-align:center'>
							 ".$this->pagination->create_links()."
							</ul>
						</div>
					</td>
				</tr>";		
		}
	}	
}