<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DiSease extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		
		parent::__construct();
		$this->load->library('pagination');	
		$this->lang->load('student', 'english');
		$this->load->model('health/ModDiSease','disease');
		$this->thead=array("No"=>'No',
			 				"Disease Code"=>'disease_code',
							"Disease"=>'disease',
							 "Illness Type"=>'illness_type',
							 "Action"=>'Action'							 	
							);
		$this->idfield="studentid";
	}
	function index(){
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }

		$data['tdata']=$this->disease->getdisease($m,$p);
		$data['thead']=	$this->thead;
		$data['page_header']="Disease List";	
		$this->parser->parse('header', $data);
		$this->parser->parse('health/disease/disease_list', $data);
		$this->parser->parse('footer', $data);
	}
	function delete($dis_id){
		$this->db->where('diseaseid',$dis_id)
				->delete('sch_medi_disease');
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("health/disease?m=$m&p=$p");
	}
	function save(){
		$diseaseid=$this->input->post('diseaseid');
		$disease=$this->input->post('disease');
		$exist=false;
		if($diseaseid!=''){
			$count=$this->disease->validate($disease,$diseaseid);
			if($count>0)
				$exist=true;
			else
				$this->disease->save($diseaseid);
				
		}else{
			$count=$this->disease->validate($disease);
			if($count>0)
				$exist=true;
			else
				$this->disease->save();
		}
		if($exist==true){
			$data['tdata']=$this->disease->getdisease($m,$p);
			$data['thead']=	$this->thead;
			$data['page_header']="Disease List";
			$data['error']="Data that you enter is already exist...!";	
			$this->parser->parse('header', $data);
			$this->parser->parse('health/disease/disease_list', $data);
			$this->parser->parse('footer', $data);
		}else{
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("health/disease?m=$m&p=$p");
		}
		
	}
	function edit(){
		$diseaseid=$this->input->post('diseaseid');
		$row=$this->db->where('diseaseid',$diseaseid)->get('sch_medi_disease')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function search(){
		if(isset($_GET['dc'])){
				$disease_code=$_GET['dc'];
				$disease=$_GET['d'];
				$illness_typeid=$_GET['it'];
				$sort_num=$_GET['s_num'];
				$m=$_GET['m'];
				$p=$_GET['p'];
				$data['tdata']=$this->disease->search($disease_code,$disease,$illness_typeid,$sort_num,$m,$p);
				$data['thead']=	$this->thead;
				$data['page_header']="Disease List";	
				$this->parser->parse('header', $data);
				$this->parser->parse('health/disease/disease_list', $data);
				$this->parser->parse('footer', $data);
		}else{
				$disease_code=$this->input->post('disease_code');
				$disease=$this->input->post('disease');
				$illness_typeid=$this->input->post('illness_typeid');
				$sort_num=$this->input->post('s_num');
				$i=1;
				$m=$this->input->post('m');
				$p=$this->input->post('p');
				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
			        $this->green->setActiveModule($m);
			    }
			    if($p!=''){
			        $this->green->setActivePage($p); 
			    }
				foreach ($this->disease->search($disease_code,$disease,$illness_typeid,$sort_num,$m,$p) as $row) {
					echo "<tr>
									<td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
									<td class='disease_code'>".$row->disease_code."</td>
									<td class='disease'>".$row->disease."</td>
									<td class='illness_type'>".$row->illness_type."</td>
									<td class='remove_tag'>";
									 	if($this->green->gAction("D")){
											 echo "<a>
											 		<img rel=".$row->diseaseid." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
											 	</a>";
										}
										if($this->green->gAction("U")){
											 echo "<a>
											 		<img  rel=".$row->diseaseid." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
											 	</a>";
										}
								echo "</td>
							</tr>
							 ";
						 $i++;
				}
				//===========================show pagination=======================
						echo "<tr class='remove_tag'>
								<td colspan='12' id='pgt'>
									<div style='margin-top:20px; width:10%; float:left;'>
									Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
													
													$num=10;
													for($i=0;$i<20;$i++){?>
														<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
														<?php $num+=10;
													}
													
												echo "</select>
									</div>
									<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
										<ul class='pagination' style='text-align:center'>
										 ".$this->pagination->create_links()."
										</ul>
									</div>

								</td>
							</tr> ";
		}
		
	}
}