<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Visit extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		$this->lang->load('student', 'english');
		$this->load->model("social/Modvisit","visit");	
		$this->load->model("setting/usermodel","user");	
		$this->thead=array("No"=>'No',
							 "Family ID"=>'family_code',
							 "Family Name"=>'family_name',
							 "Visit Times"=>'visit_times',
							 "Visit Date"=>'visit_date',
							 "Action"=>'Action'							 	
							);
		$this->idfield="studentid";
		
	}
	function index()
	{
		
		$page=0;
		if(isset($_GET['per_page'])) $page=$_GET['per_page'];
		$this->load->library('pagination');	
		$config['base_url'] = site_url().'/social/family?pg=1';		
		$config['total_rows'] = $this->green->getTotalRow("SELECT * FROM sch_family_visit");
		$config['per_page'] =50;
		//$config['use_page_numbers'] = TRUE;
		$config['num_link']=5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';		
		
		$this->pagination->initialize($config);	
		$limi=" limit ".$config['per_page'];
		if($page>0){
			$limi=" limit ".$page.",".$config['per_page'];
		}		
		$sql_page="SELECT DATE_FORMAT(fv.date,'%d-%m-%Y') as dates,
						fv.visitid,
						fv.intervation_type,
						fv.visit_reason,
						fv.update_info,
						fv.sw_activities,
						fv.outcome,
						fv.familyid,
						fv.visit_times,
						fv.yearid,
						fv.schoolid
					FROM sch_family_visit fv
					INNER JOIN sch_family_visit_detail fvd
					ON(fv.visitid=fvd.visitid)
					order by fv.visitid desc {$limi}";
		$data['tdata']=$this->green->getTable($sql_page);
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Visit List";			
		$this->parser->parse('header', $data);
		$this->parser->parse('social/visit/visit_list', $data);
		$this->parser->parse('footer', $data);
	}
	function add(){
		$data['page_header']="Visit List";	
		$this->parser->parse('header', $data);
		$this->parser->parse('social/visit/visit_form', $data);
		$this->parser->parse('footer', $data);
	}
	function getstdbyfamily(){
		$familyid=$this->input->post('familyid');
		$i=1;
		foreach ($this->visit->getstdbyfamily($familyid) as $row) {
			$class=$this->visit->getclassbystd($row->studentid)->class_name;
			echo "
				<tr>
					<td>$i</td>
					<td>".$row->first_name." ".$row->last_name."</td>
					<td>".$row->dob."</td>
					<td>".$class."</td>
				</tr>
			";
			# code...
			$i++;
		}
	}
	function getvisitrow(){
		$visitid=$this->input->post('visitid');
		$this->db->where('visitid',$visitid);
		$row=$this->db->get('sch_family_visit')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
		
	function getfamilyrow(){
		$familyid=$this->input->post('familyid');
		$row=$this->visit->getfamilyrow($familyid);
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function savevisit(){
		$visitid=$this->input->post('visitid');
		$schoolid=$this->session->userdata('schoolid');
		$this->db->where('familyid',$this->input->post('familyid'));
		$data=array(
			'date'=>$this->green->formatSQLDate($this->input->post('visit_date')),
			'intervation_type'=>$this->input->post('intervation_type'),
			'visit_reason'=>$this->input->post('reason'),
			'update_info'=>$this->input->post('update_info'),
			'sw_activities'=>$this->input->post('sw_activity'),
			'outcome'=>$this->input->post('outcome'),
			'familyid'=>$this->input->post('familyid'),
			'yearid'=>$this->input->post('yearid'),
			'schoolid'=>$schoolid
			);
		if($visitid!=''){
			$last_modified_by=$this->session->userdata('user_name');
			$last_modified_date=date('Y-m-d H:i:s');
			$data2=array('modified_by'=>$last_modified_by,
							'modified_date'=>$last_modified_date);
			$this->db->where('visitid',$visitid);
			$this->db->update('sch_family_visit',array_merge($data, $data2));
			$this->clearstdvisit($visitid);
			$this->clearemp($visitid);
			$this->clearstdvisit($visitid);
			$this->clearspon($visitid);
		}else{
			$visit_times=$this->db->get('sch_family_visit')->num_rows();
			$created_by=$this->session->userdata('user_name');
			$created_date=date('Y-m-d H:i:s');
			$data2=array('visit_times'=>$visit_times+1,
						'created_by'=>$created_by,
						'created_date'=>$created_date);
			$this->db->insert('sch_family_visit',array_merge($data, $data2));
			$visitid=$this->db->insert_id();
		}
		$year=$this->session->userdata('yearid');
		$this->do_upload($visitid,$this->input->post('familyid'));
		$this->savevisitstudent($this->input->post('familyid'),$visitid);
		echo $visitid;
	}
	function saveemp(){
		$empid=$this->input->post('empid');
		$visitid=$this->input->post('visitid');
		$data=array('visitid'=>$visitid,'empid'=>$empid);
		$this->db->insert('sch_family_visit_staff',$data);
	}
	function savespon(){
		$sponid=$this->input->post('sponid');
		$visitid=$this->input->post('visitid');
		$data=array('visitid'=>$visitid,'sponsorid'=>$sponid);
		$this->db->insert('sch_family_visit_sponsor',$data);
	}
	function getempvisit(){
		$visitid=$this->input->post('visitid');
		$result=$this->db->select("*")
				 ->from('sch_emp_profile emp')
				 ->join('sch_emp_position emp_p','emp.pos_id=emp_p.posid','inner')
				 ->join('sch_family_visit_staff s','emp.empid=s.empid','inner')
				 ->where('s.visitid',$visitid)->get()->result();
		foreach ($result as $row) {
			echo "<tr>
					<td class='hide'><input type='text' class='empid hide' name='empid[]'  value='$row->empid'/>$row->empcode</td>
					<td>$row->first_name $row->last_name</td>
					<td>
			    		<a>
						<img onclick='removerow(event);' src='".base_url()."assets/images/icons/delete.png'/>
			    		</a>
			    	</td>
				</tr>";
		}
	}
	function getsponvisit(){
		$visitid=$this->input->post('visitid');
		$result=$this->db->select("*")
				 ->from('sch_family_sponsor sp')
				 ->join('sch_family_visit_sponsor s','sp.sponsorid=s.sponsorid','inner')
				 ->where('s.visitid',$visitid)->get()->result();
		foreach ($result as $row) {
			echo "<tr>
					<td class='hide'><input type='text' class='sponid' name='sponid[]'  value='$row->sponsorid'/></td>
					<td>$row->first_name $row->last_name</td>
					<td>
			    		<a>
						<img onclick='removerow(event);' src='".base_url()."assets/images/icons/delete.png'/>
			    		</a>
			    	</td>
				</tr>";
		}
	}
	function clearemp($visitid){
		$this->db->where('visitid',$visitid);
		$this->db->delete('sch_family_visit_staff');
	}
	function clearspon($visitid){
		$this->db->where('visitid',$visitid);
		$this->db->delete('sch_family_visit_sponsor');
	}
	function savevisitstudent($familyid,$visitid){
		$this->db->where('familyid',$familyid);
		foreach ($this->db->get('sch_student')->result() as $row) {
			$class=$this->db->query("SELECT MAX(classid) as classid FROM sch_student_enrollment WHERE studentid='$row->studentid'")->row()->classid;
			$data=array(
				'visitid'=>$visitid,
				'studentid'=>$row->studentid,
				'classid'=>$class
				);
			# code...
			$this->db->insert('sch_family_visit_detail',$data);
		}
	}
	function clearstdvisit($visitid){
		$this->db->where('visitid',$visitid);
		$this->db->delete('sch_family_visit_detail');
	}
	function preview($familyid){
		$this->db->where('familyid',$familyid);
		$data['family']=$this->db->get('sch_family')->row_array();
		$this->parser->parse('header', $data);
		$this->parser->parse('social/family/preview_visit', $data);
		$this->parser->parse('footer', $data);

	}
	function previewimage($familyid){
		$this->db->where('familyid',$familyid);
		$data['family']=$this->db->get('sch_family')->row_array();
		$this->parser->parse('header', $data);
		$this->parser->parse('social/family/visitimage', $data);
		$this->parser->parse('footer', $data);

	}
	function deletevisit(){
		$visitid=$this->input->post('visitid');
		$this->db->where('visitid',$visitid);
		$this->db->delete('sch_family_visit');
	}
	function do_upload($visitid,$familyid){	
		$year=$this->session->userdata('year');
		if(!file_exists('./assets/upload/family/'.$familyid.'/'.$year))
		 {
		    if(mkdir('./assets/upload/family/'.$familyid.'/'.$year,0755,true))
		    {
		                return true;
		    }
		 }
			$config['upload_path'] ='./assets/upload/family/'.$familyid.'/'.$year;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '2000';
			$config['file_name']  = "$visitid.jpg";
			$config['overwrite']=true;
			$config['file_type']='image/png';
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('userfile'))
			{
				$error = array('error' => $this->upload->display_errors());			
			}
			else
			{		
				//$this->generate_thumb($this->upload->file_name,$this->upload->upload_path)	;	
				$data = array('upload_data' => $this->upload->data());			
				//redirect('setting/user');
				 // 	$config2['image_library'] = 'gd2';
     //                $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
     //                $config2['new_image'] = './assets/upload/family/'.$year;
     //                $config2['maintain_ratio'] = TRUE;
     //                $config2['create_thumb'] = TRUE;
     //                $config2['thumb_marker'] = FALSE;
     //                $config2['width'] = 120;
     //                $config2['height'] = 180;
     //                $config2['overwrite']=true;
     //                $this->load->library('image_lib',$config2); 

     //                if ( !$this->image_lib->resize()){
	    //             	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
					// }else{
					// 	//unlink('./assets/upload/students/'.$student_id.'.jpg');
					// }
			}
		}
	
}
