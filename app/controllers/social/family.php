<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Family extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		$this->load->model('employee/modemployee','emp');
		$this->load->model('setting/usermodel','user');
		$this->load->model('setting/rolemodel','role');

			
		$this->lang->load('student', 'english');
		$this->load->model("social/modfamily","family");
		$this->load->model("social/Modvisit","visit");	
		$this->load->model("setting/usermodel","user");	
		$this->thead=array("No"=>'No',
							"Family ID"=>'family_code',
							"Family Name"=>'family_name',
							"Father Name"=>'father_name',
							"Father Occupation"=>'father_ocupation',
							"Mother Name"=>'mother_name',
							"Mother Occupation"=>'mother_ocupation',
							"Action"=>'Action'							 	
							);
		$this->idfield="studentid";
		$this->exp=array("No"=>'No',
						 "FamilyID"=>'family_code',
						 "Family Name"=>'family_name',
						 "Zoon"=>'zoon',
						 "Address"=>'permanent_adr',
						 "Father Name"=>'father_name',
						 "Father Name(KH)"=>'father_name_kh',
						 "Father DOB"=>'father_dob',
						 "Father Occupation(Kh)"=>'father_ocupation_kh',
						 "Father Revenue"=>'father_revenu',							 
						 "Mother Name"=>'mother_name',
						 "Mother Name(KH)"=>'mother_name_kh',
						 "Mother DOB"=>'mother_dob',
						 "Mother Occupation(Kh)"=>'mother_ocupation_kh',							 
						 "mother Revenue"=>'mother_revenu',		
						 "FamilyID"=>'family_code'					 	
						);

		$sy=isset($_GET['y'])?$_GET['y']:"";
		$sm=isset($_GET['m'])?$_GET['p']:"";
		$sp=isset($_GET['p'])?$_GET['p']:"";
	}
	function index()
	{
				
		$page=0;
		if(isset($_GET['per_page'])) $page=$_GET['per_page'];
		
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }

		$this->load->library('pagination');	
		$config['base_url'] = site_url()."/social/family?pg=1&m=$m&p=$p";		
		$config['total_rows'] = $this->green->getTotalRow("SELECT * FROM sch_family where is_active=1");
		$config['per_page'] =20;
		//$config['use_page_numbers'] = TRUE;
		$config['num_link']=5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';		
		
		$this->pagination->initialize($config);	
		$limi=" limit ".$config['per_page'];
		if($page>0){
			$limi=" limit ".$page.",".$config['per_page'];
		}		
		$sql_page="SELECT family_revenu_currcode,
							sch_family.familyid,							
							family_code,
							family_name,
							father_name,
							father_name_kh,
							father_ocupation,
							father_ocupation_kh,
							mother_name,
							mother_name_kh,
							mother_ocupation,
							mother_ocupation_kh,
							revenue,
							father_revenu,
							mother_revenu,
							stud.num_stu
						FROM sch_family
						LEFT JOIN (SELECT familyid,
										COUNT(sch_student.studentid) as num_stu 
										FROM sch_student 
										GROUP BY familyid 
									) as stud ON stud.familyid=sch_family.familyid
					 WHERE is_active=1
					 ORDER BY familyid DESC {$limi}";
		$data['tdata']=$this->green->getTable($sql_page);
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['exp']=	$this->exp;
		$data['page_header']="Family List";	

		$this->parser->parse('header', $data);
		$this->parser->parse('social/family/family_list', $data);
		$this->parser->parse('footer', $data);
	}
	function getexrate(){
		$curr_code=$this->input->post('currcode');
		$this->db->select('ex_rate');
		$this->db->from('sch_z_currency');
		$this->db->where('currcode',$curr_code);
		echo $this->db->get()->row()->ex_rate;
	}
	function fillemp(){
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_emp_profile');
		$this->db->like('last_name',$key);
		$this->db->or_like('first_name',$key);	
		$data=$this->db->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->first_name.' '.$row->last_name,'id'=>$row->empid);
		}
	     echo json_encode($array);
	}
	function fillspon(){
		$key=$_GET['term'];
		$this->db->select('*');
		$this->db->from('sch_family_sponsor');
		$this->db->like('last_name',$key);
		$this->db->or_like('first_name',$key);			
		$data=$this->db->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->first_name.' '.$row->last_name,'id'=>$row->sponsorid);
		}
	     echo json_encode($array);
	}
	function getemprow(){
		$empid=$this->input->post('empid');
		$row=$this->db->select("*")
				 ->from('sch_emp_profile emp')
				 ->join('sch_emp_position emp_p','emp.pos_id=emp_p.posid','inner')
				 ->where('empid',$empid)->get()->row();

		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function import($result=0){
		$data['page_header']="Import Family Profile";		
		$data['import_status']=($result==1?"Family profile was imported.":"No Family profile to import.");			
		$this->parser->parse('header', $data);
		$this->parser->parse('social/family/import_profile', $data);
		$this->parser->parse('footer', $data);
	}
	function preview($familyid){
		$data['page_header']="New Student";	
		$data1['family']=$this->family->previewfamily($familyid);
		$this->db->where('familyid',$familyid);
		$data2['family']=$this->db->get('sch_family')->row_array();
		$this->parser->parse('header', $data);
		$this->load->view('social/family/preview', $data1);
		$this->parser->parse('social/family/preview_visit', $data2);
		$this->parser->parse('footer', $data);
	}
	function multipreview(){
		$max_r=$_GET['mar'];
		$min_r=$_GET['mr'];
		$max_m=$_GET['mam'];
		$min_m=$_GET['mm'];
		$data['page_header']="New Student";
		//$data1['page_header']=array("New Student");	
		$data1['familys']=$this->family->previewmulti($min_r,$max_r,$min_m,$max_m);
		$this->parser->parse('header', $data);
		$this->load->view('social/family/preview_multi', $data1);
		$this->parser->parse('footer', $data);
	}
	function getmemberrow(){
		$memid=$this->input->post('memid');
		$this->db->where('memid',$memid);
		$row=$this->db->get('sch_student_member')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function getresponrow(){
		$respondid=$this->input->post('resid');
		$this->db->where('respondid',$respondid);
		$row=$this->db->get('sch_student_responsible')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}
	function saverespon(){
			$familyid=$this->input->post('familyid');
			$resid=$this->input->post('res_id');
			$last_name=$this->input->post('last_name');
			$first_name=$this->input->post('first_name');
			$last_name_kh=$this->input->post('last_name_kh');
			$first_name_kh=$this->input->post('first_name_kh');
			$sex=$this->input->post('sex');
			$dob=$this->green->convertSQLDate($this->input->post('dob'));
			$occupation=$this->input->post('occupation');
			$revenue=$this->input->post('revenue');
			$tel1=$this->input->post('tel1');
			$tel2=$this->input->post('tel2');
			$village=$this->input->post('village');
			$commune=$this->input->post('commune');
			$district=$this->input->post('district');
			$province=$this->input->post('province');
			$social_status=$this->input->post('social_status');
			$health=$this->input->post('health');
			$civil_status=$this->input->post('civil_status');
			$education=$this->input->post('education');
			$relationship=$this->input->post('relationship');
			$last_modified_by=$this->session->userdata('user_name');
			$last_modified_date=date('Y-m-d H:i:s');
				$data=array(
							'last_name'=>$last_name,
							'first_name'=>$first_name,
							'last_name_kh'=>$last_name_kh,
							'first_name_kh'=>$first_name_kh,
							'sex'=>$sex,
							'dob'=>$dob,
							'occupation'=>$occupation,
							'revenue'=>$revenue,
							'tel1'=>$tel1,
							'tel2'=>$tel2,
							'village'=>$village,
							'commune'=>$commune,
							'district'=>$district,
							'province'=>$province,
							'social_status'=>$social_status,
							'health'=>$health,
							'civil_status'=>$civil_status,
							'education'=>$education,
							'familyid'=>$familyid,
							'relationship'=>$this->db->escape_str($relationship),
							'respond_type'=>1,
							'last_modified_by'=>$last_modified_by,
							'last_modified_date'=>$last_modified_date,
							'is_active'=>1
							);
			if($resid!=''){
				$last_modified_by=$this->session->userdata('user_name');
				$last_modified_date=date('Y-m-d H:i:s');
				$data2=array('last_modified_by'=>$last_modified_by,
								'last_modified_date'=>$last_modified_date);

				$this->db->where('respondid',$resid);
				$this->db->update('sch_student_responsible',array_merge($data, $data2));
			}else{
				$created_by=$this->session->userdata('user_name');
				$created_date=date('Y-m-d H:i:s');
				$data2=array('created_by'=>$created_by,
							'created_date'=>$created_date);
				$this->db->insert('sch_student_responsible',array_merge($data, $data2));
				echo $this->db->insert_id();
			}
	}
	function savemember(){
		$familyid=$this->input->post('familyid');
		$memid=$this->input->post('memid');
		$last_name=$this->input->post('last_name');
		$first_name=$this->input->post('first_name');
		$last_name_kh=$this->input->post('last_name_kh');
		$first_name_kh=$this->input->post('first_name_kh');
		$sex=$this->input->post('sex');
		$dob= $this->input->post('dob');
		$dob=$this->green->convertSQLDate($dob);
		$occupation=$this->input->post('occupation');
		$note=$this->input->post('note');
		$tel1=$this->input->post('tel1');
		$tel2=$this->input->post('tel2');
		$grade_level=$this->input->post('grade_level');
		$social_status=$this->input->post('social_status');
		$health=$this->input->post('health');
		$civil_status=$this->input->post('civil_status');
		$school=$this->input->post('school');
		$leave_school=$this->input->post('leave_school');
		$relationship=$this->input->post('relationship');
		$revenue=$this->input->post('revenue');
		$leave_school_reason=$this->input->post('leave_school_reason');

        $update_student=$this->input->post('update_student');
        $studentid=$this->input->post('studentid');

		$data=array(
					'last_name'=>$last_name,
					'first_name'=>$first_name,
					'last_name_kh'=>$last_name_kh,
					'first_name_kh'=>$first_name_kh,
					'sex'=>$sex,
					'dob'=>$dob,
					'occupation'=>$occupation,
					'note'=>$note,
					'tel1'=>$tel1,
					'tel2'=>$tel2,
					'social_status'=>$social_status,
					'health'=>$health,
					'civil_status'=>$civil_status,
					'grade_level'=>$grade_level,
					'school'=>$school,
					'familyid'=>$familyid,
					'leave_school'=>$leave_school,
					'leave_school'=>$leave_school,
					'relationship'=>$this->db->escape_str($relationship),
					'revenue'=>$revenue,
					'leave_school_reason'=>$leave_school_reason,
					'is_active'=>1
				);
           $st_data=array('last_name'=>$last_name,
                           'first_name'=>$first_name,
                           'last_name_kh'=>$last_name_kh,
                           'first_name_kh'=>$first_name_kh,
                           'dob'=>$dob
                       );

		if($memid!=''){
			$last_modified_by=$this->session->userdata('user_name');
			$last_modified_date=date('Y-m-d H:i:s');
			$data2=array('last_modified_by'=>$last_modified_by,
						'last_modified_date'=>$last_modified_date);
			$this->db->where('memid',$memid);
			$this->db->update('sch_student_member',array_merge($data, $data2));

            if($studentid!=0 && $update_student==1){
                $this->db->where('studentid',$studentid);
                $this->db->update('sch_student',$st_data);
            }

		}else{
			$created_by=$this->session->userdata('user_name');
			$created_date=date('Y-m-d H:i:s');
			$data2=array('created_by'=>$created_by,
						'created_date'=>$created_date);
			$this->db->insert('sch_student_member',array_merge($data, $data2));
			echo $this->db->insert_id();
		}
	}
	function importProfile(){
		$result=$this->family->saveImport();
		$this->import($result);		
	}
	function add(){
		$data['page_header']="New Student";			
		$this->parser->parse('header', $data);
		$this->parser->parse('social/family/family_form', $data);
		$this->parser->parse('footer', $data);
	}
	function delete($familyid){

		$y=$_GET['y'];
		$m=$_GET['m'];
		$p=$_GET['p'];
		
		$this->db->set('is_active',0);
		$this->db->where('familyid',$familyid);
		$this->db->update('sch_family');

		redirect("social/family/?y=".$y."&m=".$m."&p=".$p);
	}
	function deletemember(){
		$memberid=$this->input->post('memid');
		$this->db->set('is_active',0);
		$this->db->where('memid',$memberid);
		$this->db->update('sch_student_member');
	}
	function deleterespon(){
		$resid=$this->input->post('resid');
		$this->db->set('is_active',0);
		$this->db->where('respondid',$resid);
		$this->db->update('sch_student_responsible');
	}
	function updaterevenue(){
		$familyid=$this->input->post('familyid');
		$family_revenue=$this->input->post('family_revenue');
		$this->db->set('revenue',$family_revenue);
		$this->db->where('familyid',$familyid);
		$this->db->update('sch_family');
	}
	function getfamilybycode(){
			$family_code=$this->input->post('std_num');
			$this->db->select('count(*)');
			$this->db->from('sch_family');
			$this->db->where('family_code',$family_code);
			echo $this->db->count_all_results();
		}
	function edit($familyid){
		$data['page_header']="New Family";
		//$data1['page_header']=array("New Student");			
		$datas['family']=$this->family->getfamilyrow($familyid);
		$datas['query']= $this->family->getuserrow_($familyid);
		$this->parser->parse('header', $data);
		$this->parser->parse('social/family/family_edit', $datas);
		$this->parser->parse('footer', $data);
	}
	function savefamily(){
		$family_code=$this->input->post("family_id");
		$family_name=$this->input->post("family_name");

		//$dob=$this->input->post("dob");
		$familyid=$this->input->post("familyid");
		
    	$y=$this->input->post('y');
    	$m=$this->input->post('m');
    	$p=$this->input->post('p'); 

		if($familyid=='')
			$count=$this->family->validatefamily($family_code,$family_name);
		else
			$count=$this->family->validatefamilyupdate($family_code,$family_name,$familyid);
		if($count>0){

			$data['error']="<div style='text-align:center; color:red;'>This Family Have been Register Before Please check again...!</div>";
			$data['page_header']="New Family";			
			$this->parser->parse('header', $data);
			if($familyid==''){
				$this->parser->parse('social/family/family_form', $data);
			}else{
				$data['family']=$this->family->getfamilyrow($familyid);
				$this->parser->parse('social/family/family_edit', $data);
			}

			$this->parser->parse('footer', $data);
			
		}else{

			$save_result=$this->family->savefamily($familyid);
			//$this->do_upload($save_result);
			redirect("social/family?y=".$y."&m=".$m."&p=".$p);
		}			
	}
	function getexp(){
			$data='';
			$family_code=$this->input->post('family_code');
			$father_name=$this->input->post('father_name');
			$father_name_kh=$this->input->post('father_name_kh');
			$father_ocupation=$this->input->post('father_ocupation');
			$mother_name=$this->input->post('mother_name');
			$mother_name_kh=$this->input->post('mother_name_kh');
			$mother_ocupation=$this->input->post('mother_ocupation');
			$sort=$this->input->post('sort');
			$sort_num=$this->input->post('sort_num');
			$page=$this->input->post('page');
			$is_all=$this->input->post('is_all');
			$f=$this->input->post('f');
			$data.='<thead>';
			foreach ($f as $key => $value) {
				$data.="<th class='no_wrap'>$value</th>";
			}
			$data.='</thead>';
			$query=$this->family->getexp(
											$father_name,
											$father_name_kh,
											$father_ocupation,
											$mother_name,
											$mother_name_kh,
											$mother_ocupation,
											$sort,$sort_num,
											$family_code,
											$page,
											$is_all
										);
			$i=1;
			$data.='<tbody>';
			foreach($query as $row){									
				$data.="<tr>";
					if(isset($f['No']))
					 	$data.="<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>";
					foreach ($f as $key => $value) {

						if($key!='No')
							$data.="<td>".$row[$key]."</td>";
					}							 
					$i++;
				$data.="</tr>";	 
			}
			$data.='</tbody>';
			$arr=array('data'=>$data);
			header("Content-type:text/x-json");
			echo json_encode($arr);
	}
	function search(){
		if(isset($_GET['fn'])){
			$family_code=$_GET['fcode'];
			$father_name=$_GET['fn'];
			$father_name_kh=$_GET['fnk'];
			$father_ocupation=$_GET['fc'];
			$mother_name=$_GET['mn'];
			$mother_name_kh=$_GET['mnk'];
			$mother_ocupation=$_GET['mc'];
			$sort_num=$_GET['s_num'];
			$sort=$this->input->post('sort');
			$sort_ad=$this->input->post('sort_ad');
			$num_stu=$_GET['nst'];
			$str_revenu=$_GET['srev'];
			$num_mem=$_GET['nm'];
			$fmem=$_GET['fm'];
			
			$m='';
			$p='';
			if(isset($_GET['m'])){
			    $m=$_GET['m'];
			}
			if(isset($_GET['p'])){
			    $p=$_GET['p'];
			}

			$data['tdata']=$this->family->searchfamily($father_name,$father_name_kh,$father_ocupation,$mother_name,$mother_name_kh,$mother_ocupation,$sort,$sort_num,$sort_ad,$family_code,$num_stu,$str_revenu,$num_mem,$fmem,$m,$p);
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['exp']=	$this->exp;
			$data['page_header']="Student List";			
			$this->parser->parse('header', $data);
			$this->parser->parse('social/family/family_list', $data);
			$this->parser->parse('footer', $data);
		}
		if(!isset($_GET['fn'])){
			$family_code=$this->input->post('family_code');
			$father_name=$this->input->post('father_name');
			$father_name_kh=$this->input->post('father_name_kh');
			$father_ocupation=$this->input->post('father_ocupation');
			$mother_name=$this->input->post('mother_name');
			$mother_name_kh=$this->input->post('mother_name_kh');
			$mother_ocupation=$this->input->post('mother_ocupation');
			$sort=$this->input->post('sort');
			$sort_num=$this->input->post('sort_num');
			$sort_ad=$this->input->post('sort_ad');
			$num_stu=$this->input->post('nst');
			$str_revenu=$this->input->post('srev');
			$num_mem='';
			$fmem="";

			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }

			$query=$this->family->searchfamily($father_name,$father_name_kh,$father_ocupation,$mother_name,$mother_name_kh,$mother_ocupation,$sort,$sort_num,$sort_ad,$family_code,$num_stu,$str_revenu,$num_mem,$fmem,$m,$p);
			$i=1;
			foreach($query as $row){
                $total_responsible_revenue=0;
                $total_member_revenue=0;
                $total_responsible_revenue=$this->db->query("SELECT SUM(revenue) AS total_revenue
																				FROM sch_student_responsible
																				WHERE 1=1 AND is_active=1
																				AND familyid='".$row['familyid']."'")->row()->total_revenue;
                $total_member_revenue=$this->db->query("SELECT SUM(revenue) AS total_revenue
																				FROM sch_student_member
																				WHERE 1=1 AND is_active=1
																				AND familyid='".$row['familyid']."'")->row()->total_revenue;


                $total_revenue=$total_responsible_revenue+$total_member_revenue+$row['father_revenu']+$row['mother_revenu'];
				echo "<tr>
						 <td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
						 <td class='family_code'>".$row['family_code']."</td>
						 <td class='father_name'>".$row['father_name']."</td>
						 <td class='father_name_kh'>".$row['father_name_kh']."</td>
						 <td class='father_ocupation'>".$row['father_ocupation']."</td>
						 <td class='mother_name'>".$row['mother_name']."</td>
						 <td class='mother_name_kh'>".$row['mother_name_kh']."</td>
						 <td class='mother_ocupation'>".$row['mother_ocupation']."</td>
						 <td class='revenue'>".$total_revenue." ".$row['family_revenu_currcode']."</td>
						 <td class='remove_tag no_wrap'>";
						 if($this->green->gAction("P")){
						 	echo "<a class='read' href='".site_url('social/family/preview').'/'.$row['familyid']."' target='_balnk'>
						 		<img rel=".$row['familyid']."  src='".site_url('../assets/images/icons/preview.png')."'/>
						 	</a>";
						 }
						 if($this->green->gAction("D")){
						 	echo "<a class='delete'>
						 		<img rel=".$row['familyid']." onclick='deletestudent(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
						 	</a>";
						 }
						 if($this->green->gAction("U")){
						 	echo "<a class='update'>
						 		<img  rel=".$row['familyid']." onclick='updatefamily(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
						 	</a>";
						 }
						 echo "
						 </td>
					 </tr>
					 ";										 
				$i++;	 
			}
			echo "<tr class='remove_tag'>
						<td colspan='12' id='pgt'>
							<div style='margin-top:20px; width:10%; float:left;'>
							Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
											
											$num=20;
											for($i=0;$i<10;$i++){?>
												<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
												<?php $num+=20;
											}
											if($num=='all') 
												$select='selected';
											echo "<option value='all' $select>All</option>"	;														
										echo "</select>
							</div>
							<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
								<ul class='pagination' style='text-align:center'>
								 ".$this->pagination->create_links()."
								</ul>
							</div>

						</td>
					</tr> ";
		}
	}

	function importMember($result=0){
		$data['page_header']="Import Family Member";		
		$data['import_status']=($result==1?"Member was imported.":"No Member to import.");			
		$this->parser->parse('header', $data);
		$this->parser->parse('social/family/import_member', $data);
		$this->parser->parse('footer', $data);
	}
	function saveImportMem(){
		$result=$this->family->saveImportMem();
		$this->importMember($result);	
	}
}