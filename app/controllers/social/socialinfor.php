<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Socialinfor extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	protected $soc_type;
	function __construct(){
		parent::__construct();
		$this->lang->load('student', 'english');
		$this->load->model("social/ModSocialInfor","socinf");	
		$this->load->model("setting/usermodel","user");	
		$this->thead=array("No"=>'famnoteid',
							 "Description"=>'description',
							 "Type"=>'familynote_type',							 						 
							 "Action"=>'Action'							 	
							);
		$this->soc_type=array("house_type"=>"House Type",
							"drinking_water"=>"Drinking Water",
							"sleeping_place"=>"Sleeping Place",
							"invironment"=>"Environment",
							"relationship"=>"Relationship",
							"social_status"=>"Social Status",
							"Health"=>"Health",
							"civil_status"=>"Civil Status",
                            "stay_with"=>"Stay With",
                            "transport"=>"Transport",
                            "busway"=>"Bus Way"
                    );
		$this->idfield="famnoteid";
		
	}
	
	function index()
	{
		
		$page=0;
		if(isset($_GET['per_page'])) $page=$_GET['per_page'];
		$m='';
		$p='';
		if(isset($_GET['m'])){
	        $m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }		
		$this->load->library('pagination');
		$config['base_url'] = site_url()."/social/socialinfor?pg=1&m=$m&p=$p";		
		$config['total_rows'] = $this->green->getTotalRow("SELECT * FROM sch_family_social_infor");
		$config['per_page'] =50;
		//$config['use_page_numbers'] = TRUE;
		$config['num_link']=5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';		
		
		$this->pagination->initialize($config);	
		$limi=" limit ".$config['per_page'];
		if($page>0){
			$limi=" limit ".$page.",".$config['per_page'];
		}		
		$sql_page="SELECT
						socinfo.famnoteid,
						socinfo.description,
						socinfo.familynote_type,
						socinfo.orders
					FROM
						sch_family_social_infor AS socinfo
					ORDER BY
						famnoteid DESC
						{$limi} ";
		//echo $sql_page;
		$data['tdata']=$this->green->getTable($sql_page);
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Social Infor List";	

		$this->parser->parse('header', $data);
		$this->parser->parse('social/socialinfor/socinfor_list', $data);
		$this->parser->parse('footer', $data);
	}

	function add(){
		$data['soc_type']=$this->soc_type;	
		$data['page_header']="New Social Info";			
		$this->parser->parse('header', $data);
		$this->parser->parse('social/socialinfor/socinfor_form', $data);
		$this->parser->parse('footer', $data);
	}
	
	function delete($famnoteid){
		$this->green->runSQL("DELETE FROM sch_family_social_infor where famnoteid='".$famnoteid."' ");	
		$m='';
		$p='';
		if(isset($_GET['m'])){
	        $m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }	
		redirect("social/socialinfor?m=$m&p=$p");
	}
	
	function edit($famnoteid){
		$data['page_header']="New Social Infor";				
		$datas['socinfo']=$this->getSocInfoRow($famnoteid);
		$datas['soc_type']=$this->soc_type;			
		$this->parser->parse('header', $data);
		$this->parser->parse('social/socialinfor/socinfor_form', $datas);
		$this->parser->parse('footer', $data);
	}	
	function getSocInfoRow($famnoteid){		
		$s_row=$this->green->getOneRow("SELECT famnoteid,description,familynote_type,orders FROM sch_family_social_infor where famnoteid='".$famnoteid."' ");		
		return $s_row;
	}
	
	function save(){		
		$famnoteid=$this->input->post("famnoteid");				
		$save_result=$this->socinf->save($famnoteid);
		$m='';
		$p='';
		if(isset($_GET['m'])){
	        $m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }	
		redirect("social/socialinfor?m=$m&p=$p");
	}
	
	
	function search(){
		if(isset($_GET['s_id'])){
			$famnoteid=$_GET['s_id'];
			$description=$_GET['des'];
			$familynote_type=$_GET['faty'];			
			$sort_num=$_GET['s_num'];
			$m=$_GET['m'];
			$p=$_GET['p'];
			$sort=$this->input->post('sort');
			$data['tdata']=$query=$this->socinf->searchsocinf($famnoteid,$description,$familynote_type,$sort,$sort_num,$m,$p);
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Social Infor List";			
			$this->parser->parse('header', $data);
			$this->parser->parse('social/socialinfor/socinfor_list', $data);
			$this->parser->parse('footer', $data);
		}
		if(!isset($_GET['s_id'])){
			$famnoteid=$this->input->post('famnoteid');
			$description=$this->input->post('description');
			$familynote_type=$this->input->post('familynote_type');
			
			$sort=$this->input->post('sort');
			$sort_num=$this->input->post('sort_num');
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
			$query=$this->socinf->searchsocinf($famnoteid,$description,$familynote_type,$sort,$sort_num,$m,$p);
				 $i=1;
				 //echo $query;
				foreach($query as $row){	
																							
					echo "<tr>
						 <td class='famnoteid'>".$row['famnoteid']."</td>
						 <td class='description'>".$row['description']."</td>							
						 <td class='familynote_type'>".$row['familynote_type']."</td> 												
						 <td class='remove_tag'>";
					 	
						 if($this->green->gAction("D")){
							echo "<a><img rel=".$row['famnoteid']." onclick='deletestock(event);' src='".site_url('../assets/images/icons/delete.png')."'/></a>";
						 }
						 if($this->green->gAction("U")){
							echo "<a><img rel=".$row['famnoteid']." onclick='updatestock(event);' src='".site_url('../assets/images/icons/edit.png')."'/></a>";
						 }
					echo " </td>
						 </tr>
						 ";										 
					$i++;	 
				}
				echo "<tr class='remove_tag'>
							<td colspan='12' id='pgt'>
								<div style='margin-top:20px; width:10%; float:left;'>
								Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
												
												$num=50;
												for($i=0;$i<10;$i++){?>
													<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
													<?php $num+=50;
												}
												
											echo "</select>
								</div>
								<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
									<ul class='pagination' style='text-align:center'>
									 ".$this->pagination->create_links()."
									</ul>
								</div>

							</td>
						</tr> ";
		}
	}


	
}
