<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Counseling extends CI_Controller{

		function __construct(){
			parent::__construct();	
			$this->load->model("social/modcounseling","counsel");	
                        $this->load->model('school/schoolyearmodel','schyear');
                        $this->load->model('school/schoollevelmodel','schlevels');
                        $this->load->model('school/classmodel','cls');
                        $this->load->model('school/schoollevelmodel', 'school_level');
                        $this->load->library('pagination');	
                        
			$this->thead=array("No"=>'no',
                                            "ឈ្មោះសិស្ស"=>'student_name',
                                            "អត្តលេខ" => 'student_id',
                                            "ភេទ"=> 'student_gender',
                                            "រៀនថ្នាក់ទី" => 'student_class',
                                            "បានប្រព្រឹត្តកំហុលឆ្គង" => 'mistake',
                                            "បានផ្ដល់ដំបូន្មាន"=> 'advised',
                                            "អ្នកផ្ដល់ដំបូន្មាន"=> 'advice',
                                            "Date" => 'create_at',
                                            'Action'=>'action'
                                           );
			$this->idfield="displanid";					
		}
		function index()
		{	
                    $student=$this->db->query("SELECT
				v_assign_class.student_num,
				v_assign_class.first_name,
				v_assign_class.last_name,
				v_assign_class.first_name_kh,
				v_assign_class.last_name_kh,
				v_assign_class.gender,
				v_assign_class.studentid,
                                v_assign_class.sch_level,
                                v_assign_class.sch_year,
				v_assign_class.`year`,
				v_assign_class.classid,
                                v_assign_class.class_name,
				v_assign_class.schlevelid,
				v_assign_class.rangelevelid,
				v_assign_class.sch_level,
				v_assign_class.sch_year
			FROM
				v_assign_class	"
                        )->result();
                        $sql=$this->db->query("SELECT
					sch_school_level.schlevelid,
					sch_school_level.sch_level,
					sch_school_level.note,
					sch_school_level.schoolid,
					sch_school_level.orders
				FROM
					sch_school_level 
				ORDER BY
					schoolid,
					orders
				" )->result();
                        $sql_teacher = $this->db->query("SELECT
                                                sep.empid,
                                                sep.empcode,
                                                sep.first_name,
                                                sep.last_name,
                                                sepo.match_con_posid
                                        FROM sch_emp_profile as sep 
                                        INNER JOIN sch_emp_position as sepo ON sep.pos_id=sepo.posid
                                        WHERE sepo.match_con_posid='tch'")->result();
                        $data['getteacher']=$sql_teacher;
                        $data['getschlevel']=$sql;
                        $data['studentsql']=$student;
                        $data['getadvice']=$this->counsel->getadvice();
                        $data['schevels']=$this->schlevels->getsch_level();
                        $data['schyears']=$this->schyear->getschoolyear();
                        $data['class']=$this->cls->allclass();
			$data['tdata']=$this->counsel->getcountselling();
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Counseling";			
			$this->parser->parse('header', $data);
			$this->parser->parse('social/counseling/counseling_list', $data);
			$this->parser->parse('footer', $data);
		}
                function get_year_and_grade_data(){
                    
			$school_level_id = $this->input->post('studentlevel');
			$opt = array();

			//get year
                        
			$y_opt = array();
                        $get_year_data = $this->db->query("SELECT * FROM sch_school_year WHERE schlevelid=$school_level_id")->result();
			if(count($get_year_data) > 0){
				foreach($get_year_data as $r_y){				
					$y_opt[] = $r_y;
				}
			}			

			//get grade
			$g_opt = array(); 
			$get_grade_data = $this->counsel->getgradelevels($school_level_id);
			if(count($get_grade_data) > 0){
				foreach($get_grade_data as $r_g){				
					$g_opt[] = $r_g;
				}
			}			

			$opt["year"] = $y_opt;
			$opt["grade"] = $g_opt;

			header('Content-Type: application/json');
			echo json_encode($opt);
		}
                public function get_class_data(){
                      //  $program_id = $this->input->post('program_id');
                        $school_level_id = $this->input->post('studentlevel');
                        $adcademic_year_id = $this->input->post('adcademic_year_id');
                        $grade_level_id = $this->input->post('grade_level_id');
                        $c_opt = '<option value=""></option>';
                        $get_class_data = $this->counsel->getClassData( $school_level_id, $grade_level_id, '', '');
                        if($get_class_data->num_rows() > 0){
                                foreach($get_class_data->result() as $r_c){
                                        $c_opt .= '<option value="'. $r_c->classid .'" data-classname="'. $r_c->class_name .'">'. $r_c->class_name .'</option>';
                                }
                        }

                        echo $c_opt;
                }
       public function get_student_data(){
			$class_id = $this->input->post('class_id');
			$schlavelid = $this->input->post('schlavelid');
			$yearid = $this->input->post('yearid');
			$gradlavelid = $this->input->post('gradlavelid');
			$programid = $this->input->post('programid');
			$where = "";
			$where .= "AND schlevelid = ".$schlavelid." AND year=".$yearid." AND grade_levelid=".$gradlavelid." AND classid = ". $class_id;
			$s_opt = '<option value=""></option>';

			$i = 1;
			$get_student_data = $this->counsel->getStudent($where);
			if(count($get_student_data) > 0){
				foreach($get_student_data as $r_s){
					$s_opt .= '<option data-studentnum="'. $r_s->student_num .'" data-studentgender="'. $r_s->gender .'" data-studentname="'. $r_s->last_name .' '. $r_s->first_name .'" value="'. $r_s->studentid .'">'. $i .'. '.($programid==1?$r_s->last_name_kh .' '. $r_s->first_name_kh:$r_s->last_name .' '. $r_s->first_name).'</option>';
					$i++;
				}
			}
						
			echo $s_opt;
		}
		function add()
		{	
                        $student=$this->db->query("SELECT
				v_assign_class.student_num,
				v_assign_class.first_name,
				v_assign_class.last_name,
				v_assign_class.first_name_kh,
				v_assign_class.last_name_kh,
				v_assign_class.gender,
				v_assign_class.studentid,
                                v_assign_class.sch_level,
                                v_assign_class.sch_year,
				v_assign_class.`year`,
				v_assign_class.classid,
                                v_assign_class.class_name,
				v_assign_class.schlevelid,
				v_assign_class.rangelevelid,
				v_assign_class.sch_level,
				v_assign_class.sch_year
			FROM
				v_assign_class	"
                        )->result();
                        $sql=$this->db->query("SELECT
					sch_school_level.schlevelid,
					sch_school_level.sch_level,
					sch_school_level.note,
					sch_school_level.schoolid,
					sch_school_level.orders
				FROM
					sch_school_level 
				ORDER BY
					schoolid,
					orders
				" )->result();
                        $sql_teacher = $this->db->query("SELECT
                                                sep.empid,
                                                sep.empcode,
                                                sep.first_name,
                                                sep.last_name,
                                                sepo.match_con_posid
                                        FROM sch_emp_profile as sep 
                                        INNER JOIN sch_emp_position as sepo ON sep.pos_id=sepo.posid
                                        WHERE sepo.match_con_posid='tch'")->result();
                        $data['getteacher']=$sql_teacher;
                        $data['getschlevel']=$sql;
                        $data['studentsql']=$student;
                        $data['getadvice']=$this->counsel->getadvice();
                        $data['schevels']=$this->counsel->getsch_level();
                        $data['schyears']=$this->schyear->getschoolyear();
                        $data['class']=$this->cls->allclass();
			$data['page_header']="Counseling";			
			$this->parser->parse('header', $data);
			$this->parser->parse('social/counseling/counseling_form',$data);
			$this->parser->parse('footer', $data);
		}
		function edit($id)
		{	
            $student=$this->db->query("SELECT
											v_assign_class.student_num,
											v_assign_class.first_name,
											v_assign_class.last_name,
											v_assign_class.first_name_kh,
											v_assign_class.last_name_kh,
											v_assign_class.gender,
											v_assign_class.studentid,
							                                v_assign_class.sch_level,
							                                v_assign_class.sch_year,
											v_assign_class.`year`,
											v_assign_class.classid,
							                                v_assign_class.class_name,
											v_assign_class.schlevelid,
											v_assign_class.rangelevelid,
											v_assign_class.sch_level,
											v_assign_class.sch_year
										FROM
											v_assign_class")->result();
            $sql=$this->db->query("SELECT
										sch_school_level.schlevelid,
										sch_school_level.sch_level,
										sch_school_level.note,
										sch_school_level.schoolid,
										sch_school_level.orders
									FROM
										sch_school_level 
									ORDER BY
										schoolid,
										orders
									" )->result();
            $sql_teacher = $this->db->query("SELECT
			                                    sep.empid,
			                                    sep.empcode,
			                                    sep.first_name,
			                                    sep.last_name,
			                                    sepo.match_con_posid
			                            FROM sch_emp_profile as sep 
			                            INNER JOIN sch_emp_position as sepo ON sep.pos_id=sepo.posid
			                            WHERE sepo.match_con_posid='tch'")->result();
            $data['getteacher']=$sql_teacher;
            $data['getschlevel']=$sql;
            $data['schevels']=$this->counsel->getsch_level();
        //    $data['getsch_levelupdate']=$this->counsel->getsch_levelupdate();
            $data['schyears']=$this->schyear->getschoolyear();
            $data['class']=$this->cls->allclass();
            
            $data['studentsql']=$this->counsel->getstudentcounseling();
            $data['getadvice']=$this->counsel->getadvice();
			$data['page_header']="Counseling";
			$data['data']=$this->counsel->getcounsulrow($id);		
			$this->parser->parse('header', $data);
			$this->parser->parse('social/counseling/counseling_form',$data);
			$this->parser->parse('footer', $data);
		}
		function preview($id)
		{	
			$data['data']=$this->db->where('counsel_id',$id)->get('sch_counselling')->row_array();
			$data['page_header']="Counseling";			
			$this->parser->parse('header', $data);
			$this->parser->parse('social/counseling/preview',$data);
			$this->parser->parse('footer', $data);
		}
		function fillfamily(){
			$key=$_GET['term'];
			$this->db->select('*')
					->from('sch_family')
					->like('family_name',$key)
					->or_like('family_code',$key);			
			$data=$this->db->get()->result();
			$array=array();
			foreach ($data as $row) {
				$array[]=array('value'=>$row->family_name.' | '.$row->family_code,
								'id'=>$row->familyid);
			}
		    echo json_encode($array);
		}
		function fillemp(){
			$key=$_GET['term'];
			$data=$this->db->query("SELECT * 
								FROM sch_emp_profile emp
								INNER JOIN sch_emp_position p
								ON(emp.pos_id=p.posid) 
								WHERE p.match_con_posid='soc'
								AND (CONCAT(emp.last_name,' ',emp.first_name) LIKE '%$key%' OR CONCAT(emp.first_name,' ',emp.last_name) LIKE '%$key%')")->result();
			$array=array();
			foreach ($data as $row) {
				$array[]=array('value'=>$row->last_name.' '.$row->first_name,
								'id'=>$row->empid,
								'position'=>$row->position);
			}
		    echo json_encode($array);
		}
                function getsubbyteacher(){
                    $teacherid=$this->input->post('teacherid');
                    $class_id=$this->input->post('class_id');
                    $gradlavelid=$this->input->post('gradlavelid');
                    $schlevelid=$this->input->post('schlevelid');
                    $datasubject=$this->counsel->getteachersubject($teacherid,$schlevelid);
                    if(count($datasubject)>0){
                    	echo '<option data-subjectname="" value="">--- Select Teacher Subject ---</option>';
						foreach ($datasubject as $row) {
                           echo "<option value='$row->subjectid' data-subjectname='$row->subject'>$row->subject</option>";
                        }
                    }else{
                    	echo '<option data-subjectname="" value="">--- Select Teacher Subject ---</option>';
                    }
                        
                }
            function getmember(){
                    
			$con_type=$this->input->post('co_type');
			$familyid=$this->input->post('familyid');
			$data=$this->counsel->getfamilyrow($familyid);
			if($con_type=='mother'){
				echo "<option value=''>".$data['mother_name']."</option>";
			}
			elseif ($con_type=='father') {
				echo "<option value=''>".$data['father_name']."</option>";
			}else{
				foreach ($this->counsel->getmember($familyid) as $row) {
					echo "<option value='$row->memid' data-student='$row->last_name $row->first_name'> $row->last_name $row->first_name</option>";
				}
			}
		}
		function save(){
			$id=$this->input->post('id');
			if($id!=''){
				$this->counsel->save($id);
			}else{
				$this->counsel->save();
			}
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
                    
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("social/counseling?m=$m&p=$p");
		}
		function deletes($id){
			$this->db->where('id',$id)->delete('sch_student_counseling');
			$this->db->where('id',$id)->delete('sch_student_counseling');
			$m='';
			$p='';
			if(isset($_GET['m'])){
                            $m=$_GET['m'];
                        }
                        if(isset($_GET['p'])){
                            $p=$_GET['p'];
                        }
			redirect("social/counseling?m=$m&p=$p");
		}
                function searchcounseling(){
                    $schoollevel=$this->input->post('schoollevel');
                    $classname=$this->input->post('classname');
                    $studyyear=$this->input->post('studyyear');
                    $grandlevel=$this->input->post('grandlevel');
                    $teachername=$this->input->post('teachername');
                    $teachersubject=$this->input->post('teachersubject');
                    $sort_num = $this->input->post('sort_num');
                    $m=$this->input->post('m');
                    $p=$this->input->post('p');
                    if($teachername!="" && $teachersubject!=""){
                        $where=" AND teacher= '".$teachername."' AND subject= '".$teachersubject."'";
                    }elseif($grandlevel!="" && $classname!="" && $studyyear!="" && $schoollevel!=""){
                        $where=" AND grade_level_id ='".$grandlevel."' AND student_class ='".$classname."' AND study_year = '".$studyyear."'";
                    }elseif ($teachername!="" && $teachersubject="") {                        
                        $where=" AND teacher= '".$teachername."'";
                    }
                    else {
                        $where="";
                    }
                    $sql = "SELECT * FROM sch_student_counseling WHERE 1=1 {$where} ";
                    
                    $i=1;
                     foreach($this->db->query($sql)->result() as $row){
                       echo "<tr class='no_data'>
                                    <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
                                    <td class='student_name'>".$row->student_name."</td> 
                                    <td class='student_id'>".$row->student_id."</td>
                                    <td class='student_gender'>".$row->student_gender."</td>
                                    <td class='student_class'>".$row->student_class."</td>
                                    <td class='mistake'>".$row->mistake."</td>
                                    <td class='advised'>".$row->advised."</td>
                                    <td class='advice'>".$row->advice."</td>
                                    <td class='date'>".date('d-m-Y',strtotime($row->create_at))."</td>
                                        
                                    <td class='remove_tag no_wrap'>";
                                     //   if($this->green->gAction("D")){	
                                                echo "<a>
                                                        <img rel=".$row->id." onclick='delete_counseling(event);' class='delcounseling' src='".site_url('../assets/images/icons/delete.png')."'/>
                                                </a>";
                                      //  }
                                       // if($this->green->gAction("U")){	
                                                echo "<a>
                                                        <img  rel=".$row->id." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
                                                </a>";
                                    //     }

                                    echo "</td>
                                </tr>
                         ";
                         $i++;
                    }
                }
                function search(){
                    $id=$this->input->post('id');
                    $student_name=$this->input->post('student_name');
                    $student_id=$this->input->post('student_id');
                    $sort_num = $this->input->post('sort_num');
                    $m=$this->input->post('m');
                    $p=$this->input->post('p');
                    
                    if($student_name!=""){
                            $where=" AND student_name like '%".$student_name."%' ";
                    }
                    if($student_id!=""){
                            $where=" AND student_id like '%".$student_id."%'";
                    }
                if ($student_id=="" or $student_name=""){
                        $where="";
                    }
                    
                    $sql="SELECT * FROM sch_student_counseling WHERE 1=1 {$where} ORDER BY id DESC LIMIT {$sort_num}";	
                    $i=1;
                     foreach($this->db->query($sql)->result() as $row){
                       echo "<tr class='no_data'>
                                    <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
                                    <td class='student_name'>".$row->student_name."</td> 
                                    <td class='student_id'>".$row->student_id."</td>
                                    <td class='student_gender'>".$row->student_gender."</td>
                                    <td class='student_class'>".$row->student_class."</td>
                                    <td class='mistake'>".$row->mistake."</td>
                                    <td class='advised'>".$row->advised."</td>
                                    <td class='advice'>".$row->advice."</td>
                                    <td class='date'>".date('d-m-Y',strtotime($row->create_at))."</td>
                                    <td class='remove_tag no_wrap'>";
                                        if($this->green->gAction("D")){	
                                                $tr.="<a>
                                                        <img rel=".$row->id." onclick='delete_counseling(event);' class='delcounseling' src='".site_url('assets/images/icons/delete.png')."'/>
                                                </a>";
                                        }
                                        if($this->green->gAction("U")){	
                                                $tr.="<a>
                                                        <img  rel=".$row->id." onclick='edit(event);' src='".site_url('assets/images/icons/edit.png')."'/>
                                                </a>";
                                         }

                                    echo "</td>
                                </tr>
                         ";
                         $i++;
                    }
                }
                function searchOld(){
                    $sort_num = $this->input->post('sort_num');
			if(isset($_GET['f_id'])){
					$student_name=$this->input->post('student_name');
					$student_id=$this->input->post('student_id');
                                        $sort_num = $this->input->post('sort_num');
					$m=$this->input->post('m');
					$p=$this->input->post('p');
					$data['tdata']=$this->counsel->search($student_id,$student_name,$m,$p,$sort_num);
					$data['idfield']=$this->idfield;		
					$data['thead']=	$this->thead;
					$data['page_header']="Counseling";			
					$this->parser->parse('header', $data);
					$this->parser->parse('social/counseling/counseling_list', $data);
					$this->parser->parse('footer', $data);
			}else{
					$student_name=$this->input->post('student_name');
					$student_id=$this->input->post('student_id');
					$i=1;
					$m=$this->input->post('m');
					$p=$this->input->post('p');
					$this->green->setActiveRole($this->input->post('roleid'));
					if($m!=''){
				        $this->green->setActiveModule($m);
				    }
				    if($p!=''){
				        $this->green->setActivePage($p); 
				    }
                                    
					foreach ($this->counsel->search($student_id,$student_name,$m,$p) as $row) {
                                       foreach($tdata as $row){
                                            $tr.="<tr class='no_data'>
                                                        <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
                                                        <td class='student_name'>".$row->student_name."</td> 
                                                        <td class='student_id'>".$row->student_id."</td>
                                                        <td class='student_gender'>".$row->student_gender."</td>
                                                        <td class='student_class'>".$row->student_class."</td>
                                                        <td class='mistake'>".$row->mistake."</td>
                                                        <td class='advised'>".$row->advised."</td>
                                                        <td class='advice'>".$row->advice."</td>
                                                        <td class='date'>".date('d-m-Y',strtotime($row->create_at))."</td>
                                                        <td class='remove_tag no_wrap'>";

                                                            if($this->green->gAction("D")){	
                                                                    $tr.="<a>
                                                                            <img rel=".$row->id." onclick='delete_counseling(event);' class='delcounseling' src='".site_url('assets/images/icons/delete.png')."'/>
                                                                    </a>";
                                                            }
                                                            if($this->green->gAction("U")){	
                                                                    $tr.="<a>
                                                                            <img  rel=".$row->id." onclick='edit(event);' src='".site_url('assets/images/icons/edit.png')."'/>
                                                                    </a>";
                                                             }
                                                            $tr.="</td>
                                                    </tr>
                                             ";
                                             $i++;
                                    }
						echo "<tr class='no_data'>
							 	 <td class='No'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
								 <td class='date'>".$row->student_name."</td>
								 <td class='patient_name'>".$row->student_id."</td>
								 <td class='gender'>".$row->student_gender."</td>
								 <td class='class_name'>".$row->student_class."</td>
								 <td class='class_name'>".$row->mistake."</td>
                                                                 <td class='class_name'>".$row->advised."</td>
                                                                 <td class='class_name'>".$row->advice."</td>
								 <td class='remove_tag no_wrap'>";
							 	if($this->green->gAction("D")){	
										 	echo "<a>
										 		<img rel=".$row->id." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
										 	</a>";
										}
										if($this->green->gAction("U")){	
										 	echo "<a>
										 		<img  rel=".$row->id." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
										 	</a>";
										 }
										echo "</td>
								 	</tr>
								 ";
						 	$i++;
					}
                                        ///////////////////
                                        
                                         foreach($this->counsel->search($student_id,$student_name,$m,$p) as $row){
                                                $tr.="<tr class='no_data'>
                                                            <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
                                                            <td class='student_name'>".$row->student_name."</td> 
                                                            <td class='student_id'>".$row->student_id."</td>
                                                            <td class='student_gender'>".$row->student_gender."</td>
                                                            <td class='student_class'>".$row->student_class."</td>
                                                            <td class='mistake'>".$row->mistake."</td>
                                                            <td class='advised'>".$row->advised."</td>
                                                            <td class='advice'>".$row->advice."</td>
                                                            <td class='date'>".date('d-m-Y',strtotime($row->create_at))."</td>
                                                            <td class='remove_tag no_wrap'>";

                                                                if($this->green->gAction("D")){	
                                                                        $tr.="<a>
                                                                                <img rel=".$row->id." onclick='delete_counseling(event);' class='delcounseling' src='".site_url('../assets/images/icons/delete.png')."'/>
                                                                        </a>";
                                                                }
                                                                if($this->green->gAction("U")){	
                                                                        $tr.="<a>
                                                                                <img  rel=".$row->id." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
                                                                        </a>";
                                                                 }
                                                                $tr.="</td>
                                                        </tr>
                                                 ";
                                                 $i++;
                                        }
                                        ///////////////////
					echo "<div class='remove_tag'>
                                                <div colspan='12' id='pgt'>
                                                        <div style='margin-top:20px; width:10%; float:left;'>
                                                        Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";

                                                                                        $num=10;
                                                                                        for($i=0;$i<20;$i++){?>
                                                                                                <option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
                                                                                                <?php $num+=10;
                                                                                        }

                                                                                echo "</select>
                                                        </div>
                                                        <div style='text-align:center; verticle-align:center; width:90%; float:right;'>
                                                                <ul class='pagination' style='text-align:center'>
                                                                 ".$this->pagination->create_links()."
                                                                </ul>
                                                        </div>

                                                </div>
                                        </div> ";
			}
			
		}
		function getSesCs(){
			$familyid=$this->input->post('familyid');
			$ses=$this->green->getValue("SELECT COUNT(*)+1 as num FROM sch_counselling WHERE familyid='".$familyid."'");
			header("Content-type:text/x-json");
			$arj['num']=$ses;
			echo json_encode($arj);
			exit();
		}
}
