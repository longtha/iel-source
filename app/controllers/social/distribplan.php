<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distribplan extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		//$this->lang->load('dis', 'english');
		$this->load->model("social/moddisplan","dis");	
		$this->load->model("setting/usermodel","user");	
		$this->thead=array(	"No"=>'id',
							"FamilyID"=>'family_code',
							 "Father Name"=>'father_name',
							 "Father Name(Kh)"=>'father_name_kh',
							 "Mother Name"=>'mother_name',
							 "Mother Name(Kh)"=>'mother_name_kh',							 
							 "Rice (kg)"=>'amt_rice',
							 "Oil (L)"=>'oil',	
							 "Year"=>'year',						 
							 "Remark"=>'remark',							
							 "Modified"=>'modified_date',
							 "Preview"=>'preview'
							);
		$this->idfield="displanid";
		
	}
	
	function index()
	{		
		$page=0;
		if(isset($_GET['per_page'])) $page=$_GET['per_page'];
		$m='';
		$p='';
		if(isset($_GET['m'])){
	        $m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }		
		$this->load->library('pagination');	
		$config['base_url'] = site_url()."/social/Distribplan?pg=1&m=$m&p=$p";		
		$config['total_rows'] = $this->green->getTotalRow("SELECT * FROM v_fam_distrib_plan");
		$config['per_page'] =200;
		//$config['use_page_numbers'] = TRUE;
		$config['num_link']=5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';		
		
		$this->pagination->initialize($config);	
		$limi=" limit ".$config['per_page'];
		if($page>0){
			$limi=" limit ".$page.",".$config['per_page'];
		}		
		$sql_page="SELECT
					dsplan.displanid,
					dsplan.familyid,
					dsplan.family_code,
					dsplan.father_name,
					dsplan.father_name_kh,
					dsplan.mother_name,
					dsplan.mother_name_kh,
					dsplan.amt_rice,
					dsplan.rice_uom,
					dsplan.oil,
					dsplan.oil_uom,
					dsplan.`year`,
					dsplan.distrib_type,
					dsplan.modified_by,
					dsplan.modified_date,
					dsplan.remark,
					dsplan.created_date,
					dsplan.created_by
				FROM
					v_fam_distrib_plan AS dsplan
					ORDER BY  dsplan.`year`,dsplan.distrib_type,dsplan.familyid
				{$limi}";
		//echo $sql_page;
		$data['tdata']=$this->green->getTable($sql_page);
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Distribute Proposal";			
		$this->parser->parse('header', $data);
		$this->parser->parse('social/distribute/distribplan_list', $data);
		$this->parser->parse('footer', $data);
	}
	function import($result=0){
		$data['page_header']="Import Distribute Profile";		
		$data['import_status']=($result==1?"Distribution was imported.":"No distribution to import.");			
		$this->parser->parse('header', $data);
		$this->parser->parse('social/distribute/import_profile', $data);
		$this->parser->parse('footer', $data);
	}
	
	function importProfile(){
		$result=$this->dis->saveImport();
		$this->import($result);		
	}	

	function add(){
		$data['page_header']="New Distribute";			
		$this->parser->parse('header', $data);
		$this->parser->parse('dis/dis_edit', $data);
		$this->parser->parse('footer', $data);
	}
	function previews($disid){
		$data['page_header']="Preview Stock";		
		$data1['dis']=$this->dis->getPreview($disid);
		$data1['dis_bl']=$this->dis->getStockBl($disid);		
		$this->parser->parse('header', $data);
		$this->load->view('dis/preview', $data1);
		$this->parser->parse('footer', $data);
	}
	function previewone($familyid,$year){
		$data['page_header']="Preview Stock";
		$data1['family']=$this->db->where('familyid',$familyid)->get('sch_family')->row();
		$data1['year']=$year;
		$this->parser->parse('header', $data);
		$this->load->view('social/distribute/preview_one', $data1);
		$this->parser->parse('footer', $data);
	}
	function getfromyeardata(){
		$fromyear=$this->input->post('fromyear');
		$count=$this->db->where('year',$fromyear)
					->from('sch_family_distribute_plan')->count_all_results();
		if($count>0)
			echo 'true';
	}
	function getexistdate(){
		//$year=$this->input->post('year');
		$fromyear=$this->input->post('fromyear');
		$toyear=$this->input->post('toyear');

		$count=$this->db->where('year',$toyear)
					->from('sch_family_distribute_plan')->count_all_results();
		if($count>0){
			echo 'true';
		}else{
			foreach ($this->dis->getallrow($fromyear) as $row) {
				$data=array('familyid'=>$row->familyid,
							'amt_rice'=>$row->amt_rice,
							'rice_uom'=>$row->rice_uom,
							'oil'=>$row->oil,
							'oil_uom'=>$row->oil_uom,
							'is_invited'=>$row->is_invited,
							'remark'=>$row->remark,
							'created_date'=>$row->created_date,
							'created_by'=>$row->created_by,
							'is_newadd'=>$row->is_newadd,
							'modified_by'=>$row->modified_by,
							'modified_date'=>$row->modified_date,
							'distrib_type'=>$row->distrib_type,
							'year'=>$toyear,
							'times_received'=>$row->times_received);
				$this->db->insert('sch_family_distribute_plan',$data);
			}
			echo 'false';
		}
	}
	// function getexistdate(){
	// 	$year=$this->input->post('year');
	// 	$count=$this->db->where('year',$year+1)
	// 				->from('sch_family_distribute_plan')->count_all_results();
	// 	if($count>0){
	// 		echo 'true';
	// 	}else{
	// 		foreach ($this->dis->getallrow($year) as $row) {
	// 			$data=array('familyid'=>$row->familyid,
	// 						'amt_rice'=>$row->amt_rice,
	// 						'rice_uom'=>$row->rice_uom,
	// 						'oil'=>$row->oil,
	// 						'oil_uom'=>$row->oil_uom,
	// 						'is_invited'=>$row->is_invited,
	// 						'remark'=>$row->remark,
	// 						'created_date'=>$row->created_date,
	// 						'created_by'=>$row->created_by,
	// 						'is_newadd'=>$row->is_newadd,
	// 						'modified_by'=>$row->modified_by,
	// 						'modified_date'=>$row->modified_date,
	// 						'distrib_type'=>$row->distrib_type,
	// 						'year'=>$year+1,
	// 						'times_received'=>$row->times_received);
	// 			$this->db->insert('sch_family_distribute_plan',$data);
	// 		}
	// 		echo 'false';
	// 	}
	// }
	function preview(){
		$f_id=$_GET['f_name'];
		$m_name=$_GET['m_name'];
		$dis_type=$_GET['dis_type'];
		$f_name=$_GET['f_name'];
		$year=$_GET['year'];
		$data['page_header']="Preview Stock";
		$data1['dis_bl']=$this->dis->getpreview($f_id,$f_name,$m_name,$year,$dis_type);		
		$this->parser->parse('header', $data);
		$this->load->view('social/distribute/preview_dis', $data1);
		$this->parser->parse('footer', $data);
	}
	function delete(){
		$arr['del']=0;

		$del=$this->db->query("DELETE FROM sch_family_distribute_plan 
											WHERE  familyid='".$this->input->post('familyid')."' 
											AND year='".$this->input->post('year')."'  
											AND distrib_type='".$this->input->post('distrib_type')."' 
											");
		if($del){
			$arr['del']=1;
		}
		header("Content-type:text/x-json");
		echo json_encode($arr);
	}
	
	function edit($disid){
		$data['page_header']="New dis";					
		$datas['dis']=$this->dis->getStockRow($disid);		
			
		$this->parser->parse('header', $data);
		$this->parser->parse('dis/dis_edit', $datas);
		$this->parser->parse('footer', $data);
	}
	
	function save(){		
		$disid=$this->input->post("disid");				
		$save_result=$this->dis->save($disid);
		redirect('dis/dis/');		
	}
	function getyear($yearid){
		echo $this->db->query("SELECT year(to_date) AS years FROM sch_school_year WHERE yearid='$yearid'")->row()->years;
	}
	function search(){
		if(isset($_GET['fid'])){
			$family_code=$_GET['fid'];
			$father_name=$_GET['fn'];
			$mother_name=$_GET['mn'];
			$distrib_type=$_GET['dt'];
			$year=$_GET['y'];
			$yearid=$_GET['yid'];	
			$m=$_GET['m'];
			$p=$_GET['p'];
			$sort_num=$_GET['s_num'];
			$sort=$this->input->post('sort');
			$data['tdata']=$query=$this->dis->searchdisplan($family_code,$father_name,$mother_name,$year,$distrib_type,$sort,$sort_num,$m,$p,$yid);

			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Distribute Plan List";			
			$this->parser->parse('header', $data);
			$this->parser->parse('social/distribute/distribplan_list', $data);
			$this->parser->parse('footer', $data);
		}
		if(!isset($_GET['fid'])){

			$family_code=$this->input->post('family_code');
			$father_name=$this->input->post('father_name');
			$mother_name=$this->input->post('mother_name');
			$distrib_type=$this->input->post('distrib_type');
			$year=$this->input->post('year');
			$yearid=$this->input->post('yearid');
			$sort=$this->input->post('sort');
			$sort_num=$this->input->post('sort_num');
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
			$query=$this->dis->searchdisplan($family_code,$father_name,$mother_name,$year,$distrib_type,$sort,$sort_num,$m,$p,$yearid);


			$i=1;
			$tr="";	

				foreach($query as $row){
					echo "<tr>
                            <td class='family_code'>".$i."</td>
							 <td class='family_code'>".$row['family_code']."</td>
							 <td class='father_name'>".$row['father_name']."</td>										 
							 <td class='father_name_kh'>".$row['father_name_kh']."</td>

							 <td class='mother_name'>".$row['mother_name']."</td>
							 <td class='mother_name_kh'>".$row['mother_name_kh']."</td>

							 <td class='amt_rice'>".$row['amt_rice']." ".$row['rice_uom']."</td>
							 <td class='oil'>".$row['oil']." ".$row['oil_uom']."</td>
							 <td class='year'>".$row['year']."</td>
							 <td class='remark'>".substr($row['remark'],0,17)."...</td>

							 <td class='modified_date no_wrap'>".$row['modified_date']."</td>
							 <td style='min-width:100px;' class='remove_tag no_wrap'>";
							if($this->green->gAction("P")){	
								echo "<a><img rel=".$row['familyid']." year='".$row['year']."' onclick='previewonefam(event);' src='".site_url('../assets/images/icons/preview.png')."'/></a> ";  
							}	
							if($this->green->gAction("D")){	
								echo " <a href='javascript:void(0)' class='link_delete' onclick='deletePlan(".$row['familyid'].",".$row['year'].",".$row['distrib_type'].")'>
										<img  src='".site_url('../assets/images/icons/delete.png')."'/>
										</a> ";  
							}	
							echo "</td>
						 </tr>";		
							 // <td class='remove_tag'>
								//  <a><img rel=".$row['displanid']." onclick='previewstock(event);' src='".site_url('../assets/images/icons/preview.png')."'/></a>   
								//  <a><img rel=".$row['displanid']." onclick='deletestock(event);' src='".site_url('../assets/images/icons/delete.png')."'/></a> 
								//  <a><img rel=".$row['displanid']." onclick='updatestock(event);' src='".site_url('../assets/images/icons/edit.png')."'/></a>
							 // </td>									 
					$i++;	 
				}
			

			echo "<tr class='remove_tag'>
						<td colspan='12' id='pgt'>
							<div style='margin-top:20px; width:10%; float:left;'>
							Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
											
											$num=50;
											for($i=0;$i<10;$i++){?>
												<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
												<?php $num+=50;
											}
											
										echo "</select>
							</div>
							<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
								<ul class='pagination' style='text-align:center'>
								 ".$this->pagination->create_links()."
								</ul>
							</div>

						</td>
					</tr> ";
		}
	}


	
}
