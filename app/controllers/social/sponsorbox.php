<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sponsorbox extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		$this->lang->load('student', 'english');
		$this->load->model("social/modsponsorbox","sb");
		$this->load->library('pagination');	
		$this->thead=array("No"=>'no',
							 "Student"     => 'student',
							 "Class"       => 'class',
							 "Sponsor"     => 'sponsor',							 
							 "Receive Date"=> 'rdate',
							 "Reply Date"  => 'tdate',
							 "Visit Date"  => 'vdate',
							 "Action"      => 'Action'							 	
							);
	}
	function index(){
		$data['tdata']=$this->sb->getsponsorbox();
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Sponsor Box List";			
		$this->parser->parse('header', $data);
		$this->parser->parse('social/sponsorbox/sponsorbox_list', $data);
		$this->parser->parse('footer', $data);
	}
	function add(){
		$data['page_header']="New Student";			
		$this->parser->parse('header', $data);
		$this->parser->parse('social/sponsorbox/sponsorbox_form', $data);
		$this->parser->parse('footer', $data);
	}
	function edit($boxid){
		$data['page_header']="New Sponsor Box";	
		$data1['row']=$this->sb->sponsorboxrow($boxid);	
		$data1['drow']=$this->sb->sponsorboxdetailrow($boxid);	
		$this->parser->parse('header', $data);
		$this->load->view('social/sponsorbox/sponsorbox_form', $data1);
		$this->parser->parse('footer', $data);
	}
	//----- Note use any more -----
	function preview_old($studentid){
		$data['page_header']="New Sponsor Box";	
		$data1['tdata']=$this->sb->getsponsor($studentid);
		$data1['student']=$this->sb->getStudentName($studentid);
		$data1['class']=$this->sb->getClassName($studentid);		
		$this->parser->parse('header', $data);
		$this->load->view('social/sponsorbox/preview_old', $data1);
		$this->parser->parse('footer', $data);
	}
	//----- end ----------------------
	function preview($boxid){

		//echo $boxid;
		$studentid=$this->db->query("SELECT studentid FROM sch_sponsor_box WHERE boxid='".$boxid."'")->row()->studentid;
		$sponsorid=$this->db->query("SELECT sponsorid FROM sch_sponsor_box WHERE boxid='".$boxid."'")->row()->sponsorid;
		$replydate=$this->db->query("SELECT date_sendthanks_paris FROM sch_sponsor_box WHERE boxid='".$boxid."'")->row()->date_sendthanks_paris;

		$data['page_header']="New Sponsor Box";	
		$data1['tdata']=$this->sb->getsponsor($boxid);
		$data1['student']=$this->sb->getStudentName($studentid);
		$data1['sponsor']=$this->sb->getSponsorName($sponsorid);
		$data1['boxid']=$boxid;
		$data1['replydate']=$replydate;		
		$this->parser->parse('header', $data);
		$this->load->view('social/sponsorbox/preview', $data1);
		$this->parser->parse('footer', $data);
	}
	function validate(){
		$boxid=$this->input->post('boxid');
		$sponsor=$this->input->post('sponsor');
		$student=$this->input->post('student');
		$this->db->select('count(boxid) as count')
				->from('sch_sponsor_box')
				->where('sponsorid',$sponsor)
                ->where('studentid',$student);
		if($boxid!='')
			$this->db->where_not_in('boxid',$boxid);
        $count=$this->db->get()->row()->count;
		echo $count;
	}
	function getClass($studentid,$yearid){
		echo $this->db->query("SELECT MAX(sse.classid) as s_classid FROM sch_class sc 
									INNER JOIN	sch_student_enrollment sse ON sc.classid=sse.classid
									WHERE sse.studentid='".$studentid."' AND sse.YEAR='".$yearid."'")->row()->s_classid;
	}
	function fillstudent(){
		$key=$_GET['term'];
		// $wheres ='';
		// if($sponsorid!=''){
		// 	$wheres .=" AND sd.sponsorid = '".$sponsorid."'";
		// }
		// $setSQL = "SELECT DISTINCT
		// 				s.studentid,
		// 				CONCAT(s.last_name,' ',s.first_name) AS student_name
		// 			FROM
		// 				sch_student_sponsor_detail AS sd
		// 			INNER JOIN sch_student s ON s.studentid = sd.studentid
		// 			WHERE 1 = 1 {$wheres}";

		$setSQL = "SELECT
		 				studentid,
		 				CONCAT(last_name,' ',first_name) AS student_name
					FROM
						sch_student
		 			WHERE 1=1 AND is_active='1' AND leave_school='0'
		 			AND  CONCAT(last_name,' ',first_name)  LIKE '%$key%'
					OR  CONCAT(first_name,' ',last_name)  LIKE '%$key%'";

		$data=$this->db->query($setSQL)->result();
		//query-->result();
		//select -->get()->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->student_name,'id'=>$row->studentid);
		}
	     echo json_encode($array);
	     die();
	}
	function fillsponsor($studentid){
		$key=$_GET['term'];
		// $this->db->select('*');
		// $this->db->from('sch_family_sponsor');
		// $this->db->like('last_name',$key);
		// $this->db->or_like('first_name',$key);			
		// $data=$this->db->get()->result();

		$sql="SELECT DISTINCT sd.sponsorid, 
							  CONCAT(fs.last_name,' ',fs.first_name) AS spon_fullname
				FROM sch_family_sponsor AS fs
				INNER JOIN sch_student_sponsor_detail AS sd ON fs.sponsorid=sd.sponsorid
				WHERE 1=1 AND sd.studentid='$studentid'";
		$data=$this->db->query($sql)->result();
		$array=array();
		foreach ($data as $row) {
			$array[]=array('value'=>$row->spon_fullname,'id'=>$row->sponsorid);
		}
	     echo json_encode($array);
	}
	function getsponsortolist(){
	 	// $classid=$this->input->post('classid');
		// $class=$this->db->where('classid',$classid)->get('sch_class')->row();
		echo "<tr style='border-bottom: 1px solid #DDDDDD !important;'>
				<td><input style='display:none;' type='text' class='sponsordetailen' name='sponsordetailen[]' value='' />
					<textarea class='form-control'  name='descen_de[]' required  data-parsley-required-message='Enter Descrition English'></textarea>
				</td>
				<td><input style='display:none;' type='text' class='sponsordetailfr' name='sponsordetailfr[]' value='' />
					<textarea class='form-control'  name='descfr_de[]' required  data-parsley-required-message='Enter Description France'></textarea>
				</td>
				<td style='text-align: right;'>
		    		<a>
		    			<img onclick='removerow(event);' src='".base_url()."assets/images/icons/delete.png' />
		    		</a>
		    	</td>
			 </tr>";
	}
	function save(){
		$m='';
		$p='';
		if(isset($_GET['m'])){
		    $m=$_GET['m'];
		}
		if(isset($_GET['p'])){
		    $p=$_GET['p'];
		}
		$boxid=$this->input->post('s_boxid');
		$this->sb->save($boxid);
		redirect("social/sponsorbox?m=$m&p=$p");
	}
	function delete($boxid){
		$m='';
		$p='';
		if(isset($_GET['m'])){
		    $m=$_GET['m'];
		}
		if(isset($_GET['p'])){
		    $p=$_GET['p'];
		}
		$this->db->where('boxid',$boxid)->delete('sch_sponsor_box');
		$this->db->where('boxid',$boxid)->delete('sch_sponsor_boxdetail');

		//------ delete image -------
		$path = ('assets/upload/sponsorbox/');
    	$get_file = $path.$boxid.'.jpg';
		if(file_exists($get_file)){
			unlink('assets/upload/sponsorbox/'.$boxid.'.jpg');
		}

		redirect("social/sponsorbox?m=$m&p=$p");
	}
	function search(){
		if(isset($_GET['n'])){
			$year=$_GET['y'];
			$s_name=$_GET['n'];
			$classid=$_GET['l'];
			$sponsorid=$_GET['s'];
			$m=$_GET['m'];
			$p=$_GET['p'];
			$sort_num=$_GET['s_num'];
			$sort=$this->input->post('sort');
			$sort=str_replace('__', '.', $sort);
			$sdate=$this->green->formatSQLDate($_GET['sd']);
			$edate=$this->green->formatSQLDate($_GET['ed']);
			$data['tdata']=$this->sb->search($s_name,$classid,$sponsorid,$sort_num,$m,$p,$sort,$year,$sdate,$edate);
			$data['idfield']=$this->idfield;	
			$data['thead']=	$this->thead;
			$data['page_header']="Sponsor Box List";			
			$this->parser->parse('header', $data);
			$this->parser->parse('social/sponsorbox/sponsorbox_list', $data);
			$this->parser->parse('footer', $data);
		}else{
			$s_name=$this->input->post('s_name');
			$classid=$this->input->post('class');
			$sponsorid=$this->input->post('sponsor');
			$sort=$this->input->post('sort');
			$sort=str_replace('__', '.', $sort);
			$sort_num=$this->input->post('sort_num');
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$year=$this->input->post('year');
			$sdate=$this->green->formatSQLDate($this->input->post('sdate'));
			$edate=$this->green->formatSQLDate($this->input->post('edate'));
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
		    $i=1;

		    foreach ($this->sb->search($s_name,$classid,$sponsorid,$sort_num,$m,$p,$sort,$year,$sdate,$edate) as $row) {
		    	echo "<tr>
						<td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
						<td class='student no_wrap'>".$row->stu_fullname."</td>
						<td class='class'>".$row->class_name."</td>
						<td class='sponsor'>".$row->sp_fullname."</td>
						<td class='rdate'>".$this->green->convertSQLDate($row->reception_date)."</td>
						<td class='tdate'>".$this->green->convertSQLDate($row->date_sendthanks_paris)."</td>
						<td class='vdate'>";
							if ($row->visite_date == '00-00-0000' || $row->visite_date =='0000-00-00'){
							 	echo "";
							}else{
								echo $this->green->convertSQLDate($row->visite_date);
							}
						echo "</td>
						<td class='remove_tag no_wrap'>";
						if($this->green->gAction("P")){	
							echo "<a><img rel=".$row->boxid." onclick='preview(event);' src='".site_url('../assets/images/icons/preview.png')."'/></a>";
						}
						if($this->green->gAction("U")){	
							echo "<a><img  rel=".$row->boxid." onclick='update(event);' src='".site_url('../assets/images/icons/edit.png')."'/></a>";
						}
						if($this->green->gAction("D")){	
							echo "<a><img rel=".$row->boxid." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/></a>";
						}
					echo "</td>
						 </tr>";
					 $i++;	
		    }
				echo "<tr class='remove_tag'>
					<td colspan='12' id='pgt'>
						<div style='margin-top:20px; width:10%; float:left;'>
						Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
										
										$num=50;
										for($i=0;$i<10;$i++){?>
											<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
											<?php $num+=50;
										} ?>
										<option value='all' <?php if($sort_num=='all') echo 'selected'; ?>>All</option>
									<?php echo "</select>
						</div>
						<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
							<ul class='pagination' style='text-align:center'>
							 ".$this->pagination->create_links()."
							</ul>
						</div>

					</td>
				</tr> ";
		}
	}
	function getexp(){
		$data='';
		$s_student_num 	 = $this->input->post('s_student_num');
		$s_classid 	 	 = $this->input->post('s_classid');
		$s_sponsor 	 	 = $this->input->post('s_sponsor');
		$sdate 		 	 = $this->input->post('sdate');
		$edate 		 	 = $this->input->post('edate');
		$sort 			 = $this->input->post('sort');
		$sort_num 		 = $this->input->post('sort_num');
		$page 			 = $this->input->post('page');
		$is_all 		 = $this->input->post('is_all');
		$f 				 = $this->input->post('f');
		$year 			 = $this->input->post('year');
		$data.='<thead>';
		foreach ($f as $key => $value) {
			$data.="<th class='no_wrap'>$value</th>";
		}
		$data.='</thead>';
		$query=$this->sb->getexp(
									$s_student_num,
									$s_classid,
									$s_sponsor,
									$sdate,
									$edate,
									$sort,
									$sort_num,
									$page,
									$is_all,
									$year
								);
		$i=1;
		$data.='<tbody>';
		foreach($query as $row){									
			$data.="<tr>";
				if(isset($f['no']))
				 	$data.="<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>";
				foreach ($f as $key => $value) {
					if($key!='no')
						$data.="<td>".$row[$key]."</td>";
				}							 
				$i++;

			$data.="</tr>";	 
		}	
		$data.='</tbody>';
		$arr=array('data'=>$data);
		header("Content-type:text/x-json");
		echo json_encode($arr);
	}
}