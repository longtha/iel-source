<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sponsorboxs extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		$this->lang->load('student', 'english');
		$this->load->model("social/modsponsorbox","sb");
		$this->load->library('pagination');	
		$this->thead=array("No"=>'no',
							 "Student"=>'student',
							 "Class"=>'class',
							 "Sponsor"=>'sponsor',
							 "Gift"=>'g',
							 "Letter"=>'l'							 	
							);
	}
	function index(){
		$data['tdata']=$this->sb->getsponsorboxs();
		$data['idfield']=$this->idfield;		
		$data['thead']=	$this->thead;
		$data['page_header']="Sponsor Box List";			
		$this->parser->parse('header', $data);
		$this->parser->parse('social/sponsorbox/sponsorboxs_list', $data);
		$this->parser->parse('footer', $data);
	}
	function search(){
		if(isset($_GET['n'])){
			$year  	   		 	 = $_GET['y'];
			$s_name    		 	 = $_GET['n'];
			$classid   		 	 = $_GET['l'];
			$sponsorid 		 	 = $_GET['s'];
			$m 		   		 	 = $_GET['m'];
			$p 		   		 	 = $_GET['p'];
			$sort_num  		 	 = $_GET['s_num'];
			$sort 	   		 	 = $this->input->post('sort');
			$sort 	   		 	 = str_replace('__', '.', $sort);
			$sdate				 = $this->green->formatSQLDate($_GET['sd']);
			$edate				 = $this->green->formatSQLDate($_GET['ed']);
			$data['tdata']  	 = $this->sb->searchs($s_name,$classid,$sponsorid,$sort_num,$m,$p,$sort,$year,$sdate,$edate);
			$data['idfield'] 	 = $this->idfield;	
			$data['thead']   	 =	$this->thead;
			$data['page_header'] = "Sponsor Box List";			
			$this->parser->parse('header', $data);
			$this->parser->parse('social/sponsorbox/sponsorbox_list', $data);
			$this->parser->parse('footer', $data);
		}else{
			$s_name    = $this->input->post('s_name');
			$classid   = $this->input->post('class');
			$sponsorid = $this->input->post('sponsor');
			$sort 	   = $this->input->post('sort');
			$sort 	   = str_replace('__', '.', $sort);
			$sort_num  = $this->input->post('sort_num');
			$m 		   = $this->input->post('m');
			$p 		   = $this->input->post('p');
			$year 	   = $this->input->post('year');
			$sdate 	   = $this->green->formatSQLDate($this->input->post('sdate'));
			$edate 	   = $this->green->formatSQLDate($this->input->post('edate'));
			
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }
		    $i=1;

		    foreach ($this->sb->searchs($s_name,$classid,$sponsorid,$sort_num,$m,$p,$sort,$year,$sdate,$edate) as $row) {
		    	echo "<tr>
						<td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
						<td class='student'>".$row->stu_fullname."</td>
						<td class='class'>".$row->class_name."</td>
						<td class='sponsor'>".$row->sp_fullname."</td>
						<td class='g'>".$row->gift."</td>
						<td class='l'>".$row->lettertype."</td>	
					</tr> ";
					 $i++;	
		    }
				echo "<tr class='remove_tag'>
					<td colspan='12' id='pgt'>
						<div style='margin-top:20px; width:10%; float:left;'>
						Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
										
										$num=50;
										for($i=0;$i<10;$i++){?>
											<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
											<?php $num+=50;
										} ?>
										<option value='all' <?php if($sort_num=='all') echo 'selected'; ?>>All</option>
									<?php echo "</select>
						</div>
						<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
							<ul class='pagination' style='text-align:center'>
							 ".$this->pagination->create_links()."
							</ul>
						</div>

					</td>
				</tr> ";
		}
	}
}