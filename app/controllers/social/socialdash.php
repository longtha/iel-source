<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Socialdash extends CI_Controller {
	function __construct(){
		parent::__construct();	
		$this->load->model("social/modsocialdash","socdash");	
	}
	function index(){
		$data['page_header']="Social Summary Report";
		$year=$this->session->userdata('year');		
		$data['data']=$this->socdash->getStdbyFam($year);
		
		$data['datarice']=$this->socdash->familyRice();
		$data['totalrice']=$this->socdash->familyRicetotal();
		$this->parser->parse('header', $data);
		$this->parser->parse('social/vsocialdash', $data);
		$this->parser->parse('footer', $data);
	}

}
