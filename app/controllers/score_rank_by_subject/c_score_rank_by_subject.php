<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class c_score_rank_by_subject extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('score_rank_by_subject/m_score_rank_by_subject', 'srs');
			$this->load->model('school/schoolinformodel', 'school');
			$this->load->model('school/program', 'program');
			$this->load->model('school/schoollevelmodel', 'school_level');		
		}
		function index(){
			$data['title']='ពិន្ទុ និងចំណាត់ថ្នាក់តាមមុខវិជ្ជា';
			$data['sub_title']='សម្រាប់ ឆមាសទី';
			$this->load->view('header');
			$this->load->view("score_rank_by_subject/v_score_rank_by_subject_for_semester",$data);
			$this->load->view('footer');
		}
		
		//get class by teacher
    	public function getClassName(){
    		$school_id = $this->input->post("school_id");
    		$school_level_id = $this->input->post("school_level_id");
			$adcademic_year_id = $this->input->post("adcademic_year_id");
			$grade_level_id = $this->input->post("grade_level_id");
    		$sqlList = $this->srs->modGetClass($school_id,$school_level_id,$grade_level_id);
    		$arr_list=array();
			if(count($sqlList)){
				foreach ($sqlList as $row_class) {
					$arr_list[]= array(
										"classid" =>$row_class->classid,
										"class_name" =>$row_class->class_name
										);
					
				}
			}
			$arr=array();
			header('Content-type:text/x-json');
			$arr['list']=$arr_list;
			echo json_encode($arr); 	
			die();
    	}
    	//get student data-----------------------------------------
		public function getStudentData()
		{
			$schoolid = $this->input->post('school_id');
			$programid = $this->input->post('program_id');
			$schlavelid = $this->input->post('school_level_id');
			$yearid = $this->input->post('adcademic_year_id');
			$gradlavelid = $this->input->post('grade_level_id');
			$class_id = $this->input->post('class_id');
			$i = 1;
			$get_student_data = $this->srs->getStudentInformationSearch($schoolid,$programid,$schlavelid,$yearid,$gradlavelid,$class_id);
			$arr_list=array();
			if(count($get_student_data)){
				foreach ($get_student_data as $row_student) {
					$arr_list[]= array(
										"studentid" =>$row_student->student_id,
										"fullname" =>$row_student->fullname
										);
					
				}
			}
			$arr=array();
			header('Content-type:text/x-json');
			$arr['list']=$arr_list;
			echo json_encode($arr); 	
			die();
		}
		public function setReportSemesterRank()
		{
			$schoolid = $this->input->post('school_id');
			$programid = $this->input->post('program_id');
			$schlavelid = $this->input->post('school_level_id');
			$yearid = $this->input->post('adcademic_year_id');
			$gradlavelid = $this->input->post('grade_level_id');
			$class_id = $this->input->post('class_id');
			$studentid = $this->input->post('student_id');
			$check_semeste = $this->input->post('check_semeste');
			$getMention = $this->srs->modMetion($schlavelid);
			$arr_m = array();
			if(count($getMention)){
				foreach($getMention as $r_mention){
					$arr_m[] = array(
									"Max_m"=>$r_mention->max_score,
									"Min_m"=>$r_mention->min_score,
									"Mention"=>$r_mention->mention_kh
								);
				}
			}
			$getSchoolYear = $this->srs->modGetSchoolYear($schoolid,$programid,$schlavelid,$yearid);
			$get_sql_subject = $this->srs->getSubjectName($schoolid,$programid,$schlavelid,$gradlavelid);
			$arr_header=array();
			$arrSubjectID='';
			if(count($get_sql_subject)){
				foreach ($get_sql_subject as $row_subject) {
					$arr_header[]= array(
										"subjectid" =>$row_subject->subjectid,
										"short_sub" =>$row_subject->short_sub,
										"grade_levelid" =>$row_subject->grade_levelid
										);
					$arrSubjectID.=$row_subject->subjectid.",";
				}
			}

			$get_sql_student = $this->srs->getStudentInformationBySubject($schoolid,$programid,$schlavelid,$yearid,$gradlavelid,$class_id,$studentid,$check_semeste);
			$arr_student=array();
			if(count($get_sql_student)){
				foreach ($get_sql_student as $row_std) {
					$arr_student[]= array(
										"studentid" =>$row_std->student_id,
										"fullname" =>$row_std->fullname,
										"gender" =>$row_std->gender,
										"sch_year" =>$row_std->sch_year,
										"class_name" =>$row_std->class_name,
										"exam_semester" =>$row_std->exam_semester
										);
				}
			}
			$arr_point=array();
			$get_sql_std_detail = $this->srs->getSubjectPoint($schoolid,$programid,$schlavelid,$yearid,$gradlavelid,$class_id,$studentid,$arrSubjectID,$check_semeste);
			if(count($get_sql_std_detail)){
				foreach ($get_sql_std_detail as $key => $row_point) {
					$arr_point[]= array(
									"student_id" =>$row_point->student_id,
									"subject_id"=>$row_point->subject_id,
									"total_score" =>$row_point->total_score,
									"rank" =>$row_point->rank,
									"average_score" =>$row_point->average_score,
									"averag_coefficient" =>$row_point->averag_coefficient,
									"exam_semester" =>$row_point->exam_semester
									);
				}
			}
			$arr=array();
			header('Content-type:text/x-json');
			$arr['school_year']=$getSchoolYear;
			$arr['header']=$arr_header;
			$arr['student']=$arr_student;
			$arr['point']=$arr_point;
			$arr['mention']=$arr_m;
			echo json_encode($arr); 	
			die();
		}

		public function setReportFinal()
		{
			$schoolid = $this->input->post('school_id');
			$programid = $this->input->post('program_id');
			$schlavelid = $this->input->post('school_level_id');
			$yearid = $this->input->post('adcademic_year_id');
			$gradlavelid = $this->input->post('grade_level_id');
			$class_id = $this->input->post('class_id');
			$studentid = $this->input->post('student_id');
			$check_semeste = $this->input->post('check_semeste');
			$getMention = $this->srs->modMetion($schlavelid);
			$arr_m = array();
			if(count($getMention)){
				foreach($getMention as $r_mention){
					$arr_m[] = array(
									"Max_m"=>$r_mention->max_score,
									"Min_m"=>$r_mention->min_score,
									"Mention"=>$r_mention->mention_kh
								);
				}
			}
			$getSchoolYear = $this->srs->modGetSchoolYear($schoolid,$programid,$schlavelid,$yearid);
			$get_sql_subject = $this->srs->getSubjectName($schoolid,$programid,$schlavelid,$gradlavelid);
			$arr_header=array();
			$arrSubjectID='';
			if(count($get_sql_subject)){
				foreach ($get_sql_subject as $row_subject) {
					$arr_header[]= array(
										"subjectid" =>$row_subject->subjectid,
										"short_sub" =>$row_subject->short_sub,
										"grade_levelid" =>$row_subject->grade_levelid
										);
					$arrSubjectID.=$row_subject->subjectid.",";
				}
			}
			$get_sql_student = $this->srs->getStudentGetScoreFinalKGP($schoolid,$programid,$schlavelid,$yearid,$gradlavelid,$class_id,$studentid);
			$arr_student=array();
			if(count($get_sql_student)){
				foreach ($get_sql_student as $row_std) {
					$arr_student[]= array(
										"studentid" =>$row_std->student_id,
										"fullname" =>$row_std->fullname,
										"gender" =>$row_std->gender,
										"sch_year" =>$row_std->sch_year,
										"class_name" =>$row_std->class_name,
										"exam_semester" =>0
										);
				}
			}

			$arr_point=array();
			$get_sql_std_detail = $this->srs->getSubjectScoreFinalKGPDetail($schoolid,$programid,$schlavelid,$yearid,$gradlavelid,$class_id,$studentid);
			if(count($get_sql_std_detail)){
				foreach ($get_sql_std_detail as $key => $row_point) { 
					$arr_point[]= array(
									"student_id" =>$row_point->student_id,
									"subject_id"=>$row_point->subjectid,
									"total_score" =>$row_point->total_score,
									"rank" =>$row_point->rank,
									"average_score" =>0,
									"averag_coefficient" =>0,
									"exam_semester" =>0
									);
				}
			}
			$arr=array();
			header('Content-type:text/x-json');
			$arr['school_year']=$getSchoolYear;
			$arr['header']=$arr_header;
			$arr['student']=$arr_student;
			$arr['point']=$arr_point;
			$arr['mention']=$arr_m;
			echo json_encode($arr); 	
			die();
		}
	}

?>
