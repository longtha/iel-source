<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class message extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('message/messagemodel','sms');
		$this->load->helper('date');
		$this->load->library('pagination');		
	}
	public function index()
	{
		$dthaead['page_header']="New Message";
		$this->load->view('header',$dthaead);
		$data['query']=$this->sms->getpagination();
		$this->load->view('message/view',$data);
		$this->load->view('footer');
	}
	function addnew()
	{
		$this->load->view('header');
		$this->load->view('message/message/view');
		$this->load->view('footer');	
	}
	function preview($id){
		$this->load->view('header');
		$data['query']=$this->sms->getmessagerow($id);
		$this->load->view('message/preview',$data);
		$this->load->view('footer');
	}
	function deleteall($id){
		$this->db->where('smsid', $id);
        $this->db->delete('sch_message');
	}
	function deletes($id)
	{
		$this->db->where('smsid', $id);
        $this->db->delete('sch_message');
        redirect("message/message");
	}
	function searchs(){
		if(isset($_GET['un'])){
		   $uname=$_GET['un'];
		   $sfrom=$_GET['sf'];
		   $sdate=$_GET['sd'];
		   $data['query']=$this->sms->searchs($uname,$sfrom,$sdate);
		   $this->load->view('header');
		   $this->load->view('message/view',$data);
		   $this->load->view('footer');
		}
		if(!isset($_GET['un'])){
		    $uname=$this->input->post('uname');
		    $sfrom=$this->input->post('sfrom');
		    $sdate=$this->input->post('sdate');

		   $query=$this->sms->searchs($uname,$sfrom,$sdate);
		   echo $this->sms->loadingHtmlData($query);
		}
		 
		
	}
	public function getautoemail(){       
		$key = $_GET['term'];
		$data = $this->sms->sqlGetUser($key);
		$arr_data = Array();
		foreach($data as $row)
		{
			$arr_data[]= Array(
								"value"=>$row->fullname,
								"username"=>$row->user_name,
								"email"=>$row->email
							);
		}
		header("Content-Type:text/x-json");
		echo json_encode($arr_data);
	}  
	
	public function savedata(){
		$user_name 	= $this->session->userdata('user_name');
		$fulldate  	= date('Y-m-d H:i:s');
		$emailto 	= $_POST['emailto'];
		$emailcc 	= $_POST['emailcc']; // return is array, keep for next work.
		$subject 	= $_POST['subject'];
		$message 	= $_POST['message'];
		$sqlQuery 	= $this->sms->saveData($user_name,$user_name,$emailto,'',$subject,$message,$fulldate);
		$arr_rs = Array();
		header("Content-Type:text/x-json");
		$arr_rs['result'] = $sqlQuery;
		echo json_encode($arr_rs);
		die();
	}
	public function saveReplyData(){
		$user_name 	= $this->session->userdata('user_name');
		$fulldate  	= date('Y-m-d H:i:s');
		$emailto 	= $_POST['emailto']; 
		$emailcc 	= $_POST['emailcc'];// return is array, keep for next work.
		$subject 	= $_POST['subject'];
		$message 	= $_POST['message'];
		$emailreplyto = $_POST['emailreplyto'];
		$replysms 	= $_POST['replysms'];
		$sqlQuery 	= $this->sms->saveData($user_name,$emailreplyto,$emailto,'',$subject,$message,$fulldate,$replysms);
		$arr_rs = Array();
		header("Content-Type:text/x-json");
		$arr_rs['result'] = $sqlQuery;
		echo json_encode($arr_rs);
		die();
	}
}