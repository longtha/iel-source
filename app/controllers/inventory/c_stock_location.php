<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_stock_location extends CI_Controller {	

	function __construct(){
		parent::__construct();
		// $this->load->model('setup/area_model', 'm');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('inventory/v_stock_location');
		$this->load->view('footer');	
	}

	public function edit(){	
		$whcode = $this->input->post('whcode');
		$r = $this->db->query("SELECT
									wh.whcode,
									wh.wharehouse,
									wh.address,
									wh.contact_person,
									wh.contact_tel,
									wh.note
								FROM
									sch_stock_wharehouse AS wh  
								WHERE wh.whcode = '{$whcode}' ")->row();
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($r);		
	}

	public function delete(){	
		$whcode = $this->input->post('whcode');
		$i = 0;
		$i = $this->db->delete("sch_stock_wharehouse", ['whcode' => $whcode]);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;	
	}

	public function grid(){
		$m = $this->input->post('m');
		$p = $this->input->post('p');
		$this->green->setActiveRole($this->session->userdata('roleid'));		
		$this->green->setActiveModule($m);		
		$this->green->setActivePage($p);

		$offset = trim($this->input->post('offset'));
		$limit = trim($this->input->post('limit'));
		$search_wherehouse = trim($this->input->post('search_wherehouse'));
		
		$w = "";
		if($search_wherehouse != ""){			
			$w .= "AND wh.wharehouse LIKE '%".sqlStr($search_wherehouse)."%' ";
		}

		$totalRecord = $this->db->query("SELECT wh.whcode FROM sch_stock_wharehouse AS wh WHERE 1=1 {$w} ")->num_rows();
		$totalPage = ceil($totalRecord/$limit) - 0;		

		$q = $this->db->query("SELECT
									wh.whcode,
									wh.wharehouse,
									wh.address,
									wh.contact_person,
									wh.contact_tel,
									wh.note
								FROM
									sch_stock_wharehouse AS wh
								WHERE 1=1 {$w}
								LIMIT $offset, $limit ");		
		$tr = '';
		$i = 1;
		if($q->num_rows() > 0){
			foreach($q->result() as $row){
				$whcode = $this->db->query("SELECT sb.whcode FROM sch_stock_balance AS sb WHERE sb.whcode = '{$row->whcode}' ")->num_rows();

				$tr .= '<tr>
							<td>'.($i++ + $offset).'</td>
							<td>'.$row->wharehouse.'</td>
							<td>'.$row->note.'</td>
							<td class="remove_tag no_wrap">';
						if($this->green->gAction("U")){ 
					$tr .='<a href="javascript:;" class="btn btn-xs btn-success edit" data-whcode="'.$row->whcode.'" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>';
						}
					$tr .='</td>
							<td class="remove_tag no_wrap">';
						if($this->green->gAction("D")){ 
					$tr .='<a href="javascript:;" class="btn btn-xs btn-danger delete" data-whcode="'.$row->whcode.'" data-whcode_chk="'.$whcode.'" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>';
						}
					$tr .='</td>
						</tr>';
			}
		}
		else{
			$tr .= '<tr>
						<td colspan="5" style="font-weight: bold;text-align: center;background: #F2F2F2;">No data !</td>
					</tr>';
		}
		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}

	public function save(){
		$whcode = $this->input->post('whcode');
		$wherehouse = trim($this->input->post('wherehouse'));
		$address = trim($this->input->post('address'));
		$contact_person = trim($this->input->post('contact_person'));
		$contact_tel = trim($this->input->post('contact_tel'));
		$note = trim($this->input->post('note'));
		
		$data = ['wharehouse' => $wherehouse,
					"address" => $address,
					"contact_person" => $contact_person,
					"contact_tel" => $contact_tel,
					"note" => $note
				];

		$w = "";
		if($whcode - 0 > 0){			
			$w .= "AND w.whcode != '{$whcode}' ";
		}
		$q = $this->db->query("SELECT w.whcode AS c FROM sch_stock_wharehouse AS w WHERE w.wharehouse = '".sqlStr($wherehouse)."' {$w} ");

		if($q->num_rows() == 0){
			if($whcode - 0 > 0){
				$data['modified_date'] = date('Y-m-d H:i:s');
	        	$data['modified_by'] = $this->session->userdata('user_name');
				$this->db->update("sch_stock_wharehouse", $data, ["whcode" => $whcode]);
				$dd['updated'] = 'Updated!';
			}
			else{
				$data['created_date'] = date('Y-m-d H:i:s');
	        	$data['created_by'] = $this->session->userdata('user_name');
				$this->db->insert("sch_stock_wharehouse", $data);					

				// stock balance =====
				$whcode_last = $this->db->insert_id();
				/* $recorder_qty = $this->db->query("SELECT s.recorder_qty FROM sch_stock AS s WHERE s.stockid = '{$stockid_last}' ")->row()->recorder_qty;*/
				$this->db->query("INSERT INTO sch_stock_balance(
										whcode,
										stockid,
										categoryid)
									SELECT
										'{$whcode_last}',
										sch_stock.stockid,
										sch_stock.categoryid
									FROM
										sch_stock ");
				$dd['saved'] = 'Saved!';
			}
		}
		else{
			$dd['existed'] = 'Location "'.$wherehouse.'"'.' existed!';
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($dd);
	}

	public function get_category(){
		$q = $this->db->query("SELECT * FROM sch_stock_category AS c ORDER BY c.cate_name ASC");

		$opt = "";
		$opt .= '<option></option>';
		if($q->num_rows() > 0){
			foreach ($q->result() as $row) {
				$opt .= '<option value="'.$row->categoryid.'">'.$row->cate_name.'</option>';
			}
		}
		echo $opt;
	}


}