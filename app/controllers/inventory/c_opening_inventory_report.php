<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_opening_inventory_report extends CI_Controller {
	function __construct(){
		parent::__construct();
	}

 	function index(){
       		$mo = $this->db->query("SELECT
									sch_stock_opening_master.id,
									sch_stock_opening_master.created_by,
									DATE_FORMAT(tran_date, '%d/%m/%Y ') AS tran_date,
									sch_stock_opening_master.typeno,
									sch_stock_opening_balance.type,
									sch_stock_opening_balance.openid,
									sch_stock_opening_balance.stockid,
									sch_stock_opening_balance.qty,
									sch_stock_opening_balance.qty_unit,
									sch_stock_opening_balance.transno,
									sch_stock_opening_balance.cate_id,
									sch_stock_opening_balance.selling_price,
									sch_stock.stockcode,
									sch_stock.descr_eng,
									sch_stock.note
								FROM
									sch_stock_opening_master
								LEFT JOIN sch_stock_opening_balance ON sch_stock_opening_balance.transno = sch_stock_opening_master.typeno
								LEFT JOIN sch_stock ON sch_stock_opening_balance.stockid = sch_stock.stockid
								WHERE
									typeno = '".$_GET['typeno']."' ");

			$data['typeno']=$mo->row()->typeno;
			$data['tran_date']=$mo->row()->tran_date;
			$data['created_By']=$mo->row()->created_by;
			// print_r($classes);
			$trinv="";	
			$amttotal=0;
			$ii=1;
			
			if($mo->num_rows() > 0){
				foreach($mo->result() as $rowde){
					$trinv.='<tr class="info">
							<td align="center">'.($ii++).'</td>
							<td align="right">'.$rowde->stockcode.'</td>
							<td align="right">'.$rowde->descr_eng.'</td>
							<td align="right">'.$rowde->qty.'</td>
							<td align="right">'.$rowde->selling_price.'</td>
							<td align="right">'.number_format($rowde->selling_price,2).'$'.'</td>
					</tr>';
					$amttotal+=($rowde->selling_price);
				}
			}

		$data['amttotal']=$amttotal;
		$data['trinv']=$trinv;
		$this->load->view('inventory/v_opening_inventory_report',$data);

	}


}