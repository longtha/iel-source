<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_stock_balance extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('inventory/v_stock_balance');
		$this->load->view('footer');	
	}

	public function grid(){		
		$offset = trim($this->input->post('offset'));
		$limit = trim($this->input->post('limit'));
		$categoryid = trim($this->input->post('categoryid'));		
		$stockcode = trim($this->input->post('stockcode'));		
		$descr_eng = trim($this->input->post('descr_eng'));		
		$whcode = trim($this->input->post('whcode'));
		$quantity = trim($this->input->post('quantity')) - 0;		
		$others = trim($this->input->post('others'));		
		$value_compare = trim($this->input->post('value_compare')) - 0;
		$is_active = trim($this->input->post('is_active'));		

		// ==============
		$m = $this->input->post('m');
		$p = $this->input->post('p');
		$this->green->setActiveRole($this->session->userdata('roleid'));		
		$this->green->setActiveModule($m);		
		$this->green->setActivePage($p);
		
		$w = "";
		if($categoryid != ""){			
			$w .= "AND sb.categoryid LIKE '%{$categoryid}%' ";
		}
		if($stockcode != ""){			
			$w .= "AND s.stockcode LIKE '%".sqlStr($stockcode)."%' ";
		}
		if($descr_eng != ""){			
			$w .= "AND s.descr_eng LIKE '%".sqlStr($descr_eng)."%' ";
		}
		if($whcode != ""){			
			$w .= "AND sb.whcode = '{$whcode}' ";
		}
		if($others != "all"){
			$w .= "AND sb.quantity ".$others." '".sqlStr($value_compare)."' ";
		}
		if($is_active != ""){			
			$w .= "AND sb.is_active = '{$is_active}' ";
		}		

		$totalRecord = $this->db->query("SELECT
												sb.balanceid
											FROM
												sch_stock_balance AS sb
											LEFT JOIN sch_stock_category AS c ON sb.categoryid = c.categoryid
											LEFT JOIN sch_stock AS s ON sb.stockid = s.stockid
											LEFT JOIN sch_stock_wharehouse AS w ON sb.whcode = w.whcode
											WHERE 1=1 {$w} ")->num_rows();
		$totalPage = ceil($totalRecord/$limit) - 0;		

		$sql = "SELECT
					sb.balanceid,
					sb.stockid,
					sb.quantity,
					sb.whcode,
					sb.is_active,
					sb.categoryid,
					c.cate_name,
					s.stockcode,
					s.descr_eng,
					w.wharehouse
				FROM
					sch_stock_balance AS sb
				LEFT JOIN sch_stock_category AS c ON sb.categoryid = c.categoryid
				LEFT JOIN sch_stock AS s ON sb.stockid = s.stockid
				LEFT JOIN sch_stock_wharehouse AS w ON sb.whcode = w.whcode 
				WHERE 1=1 {$w}
				ORDER BY c.cate_name ASC
				LIMIT $offset, $limit ";
		// echo $sql;
		// exit();
		$q = $this->db->query($sql);

		$tr = '';
		$i = 1;
		$to_qty = 0;
		if($q->num_rows() > 0){
			foreach($q->result() as $row){
				$to_qty += $row->quantity - 0;
				$tr .= '<tr>
							<td>'.($i++ + $offset).'</td>
							<td>'.$row->cate_name.'</td>
							<td>'.$row->wharehouse.'</td>							
							<td>'.$row->stockcode.'</td>
							<td>'.$row->descr_eng.'</td>
							<td style="text-align: right;">'.($row->quantity != null ? $row->quantity : '0').'</td>
							<td style="text-align: right;"><span class="label label-'.($row->is_active - 0 == 1 ? 'success' : 'danger').'">'.($row->is_active - 0 == 1 ? 'Active' : 'Inactive').'</span></td>
						</tr>';
			}
			$tr .= '<tr style="background: #F2F2F2;">
						<td style="text-align: right;" colspan="5">Total</td>
						<td style="text-align: right;font-weight: bold;">'.number_format($to_qty, 0).'</td>
						<td>&nbsp;</td>
					</tr>';
		}
		else{
			$tr .= '<tr>
						<td colspan="8" style="font-weight: bold;text-align: center;background: #F2F2F2;">No data!</td>
					</tr>';
		}
		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}

	public function get_category(){
		$last_categoryid = $this->input->post('last_categoryid');
		$q = $this->db->query("SELECT * FROM sch_stock_category AS c ORDER BY c.cate_name ASC");

		$opt = "";
		$opt .= '<option></option>';
		if($q->num_rows() > 0){
			foreach ($q->result() as $row) {
				$opt .= '<option value="'.$row->categoryid.'" '.($row->categoryid == $last_categoryid ? 'selected="selected"' : '').'>'.$row->cate_name.'</option>';
			}
		}
		echo $opt;
	}


	public function get_stock(){
		$term = trim($_REQUEST['term']);
		$categoryid = trim($_REQUEST['categoryid']);

		$w = "";
		$w1 = "";
		if($term != ""){
			$w .= "AND s.stockcode LIKE '%".sqlStr($term)."%' ";    
		}		
		if($categoryid != ""){
			$w1 .= "AND s.categoryid = '{$categoryid}' ";     
		}

		$sql = "SELECT
					s.stockcode
				FROM
					sch_stock AS s
				WHERE 1=1 {$w} {$w1} LIMIT 0, 10 ";
		$qr = $this->db->query($sql);

		$arr = [];
		if($qr->num_rows() > 0){
			foreach ($qr->result() as $row) {
				$arr[] = ['stockcode' => $row->stockcode];
			}
		}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
	}

	public function get_stock_(){
		$term = trim($_REQUEST['term']);
		$categoryid = trim($_REQUEST['categoryid']);

		$w = "";
		$w1 = "";
		if($term != ""){
			$w .= "AND s.descr_eng LIKE '%".sqlStr($term)."%' ";    
		}		
		if($categoryid != ""){
			$w1 .= "AND s.categoryid = '{$categoryid}' ";     
		}

		$sql = "SELECT
					s.descr_eng
				FROM
					sch_stock AS s
				WHERE 1=1 {$w} {$w1} LIMIT 0, 10 ";
		$qr = $this->db->query($sql);

		$arr = [];
		if($qr->num_rows() > 0){
			foreach ($qr->result() as $row) {
				$arr[] = ['descr_eng' => $row->descr_eng];
			}
		}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
	}


}