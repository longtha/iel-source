<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_using_stock_report extends CI_Controller {
	function __construct(){
		parent::__construct();
		
	}

 	function index(){
       		$mo = $this->db->query("SELECT
										sch_using_stock_master.id,
										sch_using_stock_master.typeno,
										DATE_FORMAT(tran_date, '%d/%m/%Y ') AS tran_date,
										sch_using_stock.stockid,
										sch_stock.stockcode,
										sch_stock.descr_eng,
										sch_stock.note,
										sch_using_stock_master.created_by,
										sch_using_stock.openid,
										sch_using_stock.qty,
										sch_using_stock.qty_unit,
										sch_using_stock.cate_id,
										sch_using_stock.selling_price,
										sch_using_stock.type
									FROM
										sch_using_stock_master
									LEFT JOIN sch_using_stock ON sch_using_stock_master.typeno = sch_using_stock.transno
									LEFT JOIN sch_stock ON sch_using_stock.stockid = sch_stock.stockid
								WHERE
									typeno = '".$_GET['typeno']."' ");

			$data['typeno']=$mo->row()->typeno;
			$data['tran_date']=$mo->row()->tran_date;
			$data['created_By']=$mo->row()->created_by;
			// print_r($classes);
			$trinv="";	
			$amttotal=0;
			$ii=1;
			
			if($mo->num_rows() > 0){
				foreach($mo->result() as $rowde){
					$trinv.='<tr class="info">
							<td align="center">'.($ii++).'</td>
							<td align="right">'.$rowde->stockcode.'</td>
							<td align="right">'.$rowde->descr_eng.'</td>
							<td align="right">'.$rowde->qty.'</td>
							<td align="right">'.$rowde->selling_price.'</td>
							<td align="right">'.number_format($rowde->selling_price,2).'$'.'</td>
					</tr>';
					$amttotal+=($rowde->selling_price);
				}
			}

		$data['amttotal']=$amttotal;
		$data['trinv']=$trinv;
		$this->load->view('inventory/v_using_stock_report',$data);

	}


}