<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_stock_transaction extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('inventory/v_stock_transaction');
		$this->load->view('footer');	
	}

	public function edit(){	
		$whcode = $this->input->post('whcode');
		$r = $this->db->query("SELECT
									*
								FROM
									sch_stock_wharehouse AS w  
								WHERE w.whcode = '{$whcode}' ")->row();
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($r);		
	}

	public function delete(){	
		$whcode = $this->input->post('whcode');
		
		$i = 0;
		$i = $this->db->delete("sch_stock_wharehouse", array('whcode' => $whcode));
		header('Content-Type: application/json; charset=utf-8');
		echo $i;	
	}

	public function grid(){
		$type = trim($this->input->post('type'));
		$offset = trim($this->input->post('offset'));
		$limit = trim($this->input->post('limit'));
		$from_date = trim($this->input->post('from_date'));
		$to_date = trim($this->input->post('to_date'));
		$categoryid = trim($this->input->post('categoryid'));
		$descr_eng = trim($this->input->post('descr_eng'));
		$whcode = trim($this->input->post('whcode'));		
		
		$w = "";
		$wd = "";		
		if($categoryid != ""){			
			$w .= "AND mo.cate_id = '{$categoryid}' ";
		}		
		if($from_date != "" && $to_date != ""){			
			$w .= "AND date(mo.date) >= '".$this->green->formatSQLDate($from_date)."' ";
			$w .= "AND date(mo.date) <= '".$this->green->formatSQLDate($to_date)."' ";			
		}		
		if($whcode != ""){			
			$w .= "AND mo.whcode = '{$whcode}' ";			
		}
		if($type != ""){			
			$w .= "AND mo.type = '{$type}' ";			
		}
		// ============
		if($descr_eng != ""){			
			$wd .= "AND s.descr_eng LIKE '%".sqlStr($descr_eng)."%' ";			
		}

		$totalRecord = $this->db->query("SELECT DISTINCT
												mo.cate_id
											FROM
												sch_stock_stockmove AS mo
											INNER JOIN sch_stock_category AS c ON mo.cate_id = c.categoryid
											INNER JOIN sch_stock as s ON mo.stockid = s.stockid	
											WHERE 1=1 {$w} {$wd}")->num_rows();
		
		$totalPage = ceil($totalRecord/$limit) - 0;		

		$sql = "SELECT DISTINCT
						mo.cate_id,
						c.cate_name
					FROM
						sch_stock_stockmove AS mo
					INNER JOIN sch_stock_category AS c ON mo.cate_id = c.categoryid
					INNER JOIN sch_stock as s ON mo.stockid = s.stockid
					WHERE 1=1 {$w} {$wd}
					ORDER BY c.cate_name ASC
					LIMIT $offset, $limit ";
		$q = $this->db->query($sql);

		$tr = '';
		if($q->num_rows() > 0){
			foreach($q->result() as $row){
				$tr .= '<tr>
							<td colspan="7">Category: <span style="font-weight: bold;">'.$row->cate_name.'</span></td>
						</tr>';

				$sql1 = "SELECT
								s.descr_eng,
								mo.date,
								mo.type,
								mo.quantity,
								mo.selling_price,
								DATE_FORMAT(mo.date, '%d/%m/%Y') AS date
							FROM
								sch_stock_stockmove AS mo
							INNER JOIN sch_stock AS s ON mo.stockid = s.stockid
							WHERE 1=1 AND mo.cate_id = '{$row->cate_id}' {$w} {$wd}
							ORDER BY mo.transno DESC ";
				$q_d = $this->db->query($sql1);

				$i = 1;
				$total_amt = 0;
				$total_qty = 0;
				if($q_d->num_rows() > 0){
					foreach($q_d->result() as $row_d){
						$type = $this->db->query("SELECT * FROM sch_z_systype AS t WHERE t.typeid = '{$row_d->type}' ")->row()->type;
						$total_qty += $row_d->quantity - 0;
						$amt = 0;
						$amt += ($row_d->quantity - 0)*($row_d->selling_price - 0);
						$total_amt += $amt - 0;
						$tr .= '<tr>
									<td style="border: 0;">'.$i++.'</td>
									<td style="border: 0;">'.$row_d->descr_eng.'</td>
									<td style="border: 0;">'.$row_d->date.'</td>
									<td style="border: 0;">'.$type.'</td>
									<td style="border: 0;text-align: right;">'.$row_d->quantity.'</td>
									<td style="border: 0;text-align: right;">'.number_format($row_d->selling_price, 2).'</td>		
									<td style="border: 0;text-align: right;">'.number_format($amt, 2).'</td>
								</tr>';
					}
					$tr .= '<tr style="text-align: right;">
							<td style="border: 0;" colspan="4">Total '.$row->cate_name.': </td>
							<td style="border: 0;font-weight: bold;">'.number_format($total_qty, 0).'</td>		
							<td style="border: 0;">&nbsp;</td>		
							<td style="border: 0;font-weight: bold;">'.number_format($total_amt, 2).'</td>
						</tr>';
				}
				else{
					
				}// detail ======
				
			}
		}
		else{
			$tr .= '<tr>
						<td colspan="7" style="font-weight: bold;text-align: center;background: #F2F2F2;">No data!</td>
					</tr>';
		}
		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}

	public function save(){
		$whcode = $this->input->post('whcode');
		$wherehouse = trim($this->input->post('wherehouse'));
		$address = trim($this->input->post('address'));
		$contact_person = trim($this->input->post('contact_person'));
		$contact_tel = trim($this->input->post('contact_tel'));
		$note = trim($this->input->post('note'));
		
		$data = ['wharehouse' => $wherehouse,
					"address" => $address,
					"contact_person" => $contact_person,
					"contact_tel" => $contact_tel,
					"note" => $note
				];

		$w = "";
		if($whcode - 0 > 0){			
			$w .= "AND w.whcode != '{$whcode}' ";
		}
		$q = $this->db->query("SELECT w.whcode AS c FROM sch_stock_wharehouse AS w WHERE w.wharehouse = '".sqlStr($wherehouse)."' {$w} ");

		if($q->num_rows() == 0){
			if($whcode - 0 > 0){
				$data['modified_date'] = date('Y-m-d H:i:s');
	        	$data['modified_by'] = $this->session->userdata('user_name');
				$this->db->update("sch_stock_wharehouse", $data, ["whcode" => $whcode]);
				$dd['updated'] = 'Updated!';
			}
			else{
				$data['created_date'] = date('Y-m-d H:i:s');
	        	$data['created_by'] = $this->session->userdata('user_name');
				$this->db->insert("sch_stock_wharehouse", $data);					

				// stock balance =====
				$whcode_last = $this->db->insert_id();
				$this->db->query("INSERT INTO sch_stock_balance(
										whcode,
										stockid)
									SELECT
										'{$whcode_last}',
										sch_stock.stockid
									FROM
										sch_stock ");
				$dd['saved'] = 'Saved!';
			}
		}
		else{
			$dd['existed'] = 'Location "'.$wherehouse.'"'.' existed!';
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($dd);
	}

	// autocomplete =======
	public function get_stock(){
		$term = trim($_REQUEST['term']);
		$categoryid = trim($_REQUEST['categoryid']);

		$w = "";
		$w1 = "";
		if($term != ""){
			$w .= "AND ( s.stockcode LIKE '%".sqlStr($term)."%' ";
			$w .= "or s.descr_eng LIKE '%".sqlStr($term)."%' ) ";	     
		}		
		if($categoryid != ""){
			$w1 .= "AND s.categoryid = '{$categoryid}' ";     
		}

		$sql = "SELECT
					s.stockcode,
					s.descr_eng
				FROM
					sch_stock AS s
				WHERE 1=1 {$w} {$w1} LIMIT 0, 10 ";
		$qr = $this->db->query($sql);

		$arr = [];
		if($qr->num_rows() > 0){
			foreach ($qr->result() as $row) {
				$arr[] = ['stockcode' => $row->stockcode, 'descr_eng' => $row->descr_eng];
			}
		}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
	}

	public function get_category(){
		$q = $this->db->query("SELECT * FROM sch_stock_category AS c ORDER BY c.cate_name ASC");

		$opt = "";
		$opt .= '<option></option>';
		if($q->num_rows() > 0){
			foreach ($q->result() as $row) {
				$opt .= '<option value="'.$row->categoryid.'">'.$row->cate_name.'</option>';
			}
		}
		echo $opt;
	}


}