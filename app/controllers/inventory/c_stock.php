<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_stock extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('inventory/v_stock');
		$this->load->view('footer');	
	}

	public function edit(){	
		$stockid = $this->input->post('stockid');
		$r = $this->db->query("SELECT
									s.stockid,
									s.stockcode,
									s.descr_eng,
									s.descr_kh,
									s.recorder_qty,
									s.categoryid,
									s.is_active,
									s.specification,
									s.note,
									s.sale_price,
									c.cate_name
								FROM
									sch_stock AS s
								INNER JOIN sch_stock_category AS c ON s.categoryid = c.categoryid  
								WHERE s.stockid = '{$stockid}' ")->row();
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($r);		
	}

	public function delete(){	
		$stockid = $this->input->post('stockid');		

		// transaction =====
		$this->db->trans_begin();

			$this->db->delete("sch_stock", array('stockid' => $stockid));
			$this->db->delete("sch_stock_balance", array('stockid' => $stockid));

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$i = 0;
		}
		else{
			$this->db->trans_commit();
			$i = 1;
		}		
		header('Content-Type: application/json; charset=utf-8');
		echo $i;	
	}

	public function grid(){		
		$m = $this->input->post('m');
		$p = $this->input->post('p');
		$this->green->setActiveRole($this->session->userdata('roleid'));		
		$this->green->setActiveModule($m);		
		$this->green->setActivePage($p);

		$offset = trim($this->input->post('offset'));
		$limit = trim($this->input->post('limit'));
		$search_cate = trim($this->input->post('search_cate'));		
		$search_stockcode = trim($this->input->post('search_stockcode'));
		$search_descr_eng = trim($this->input->post('search_descr_eng'));

		// ==============
		$m = $this->input->post('m');
		$p = $this->input->post('p');
		$this->green->setActiveRole($this->session->userdata('roleid'));		
		$this->green->setActiveModule($m);		
		$this->green->setActivePage($p);
		
		$w = "";
		if($search_cate != ""){			
			$w .= "AND c.cate_name LIKE '%".sqlStr($search_cate)."%' ";
		}
		if($search_stockcode != ""){			
			$w .= "AND s.stockcode LIKE '%".sqlStr($search_stockcode)."%' ";
		}
		if($search_descr_eng != ""){			
			$w .= "AND s.descr_eng LIKE '%".sqlStr($search_descr_eng)."%' ";
		}

		$totalRecord = $this->db->query("SELECT
												s.stockid
											FROM
												sch_stock AS s
											INNER JOIN sch_stock_category AS c ON s.categoryid = c.categoryid 
											WHERE 1=1 {$w} ")->num_rows();
		$totalPage = ceil($totalRecord/$limit) - 0;		

		$q = $this->db->query("SELECT
									s.stockid,
									s.stockcode,
									s.descr_eng,
									s.descr_kh,
									s.recorder_qty,
									s.categoryid,
									s.is_active,
									s.specification,
									s.note,
									s.sale_price,
									c.cate_name
								FROM
									sch_stock AS s
								INNER JOIN sch_stock_category AS c ON s.categoryid = c.categoryid 
								WHERE 1=1 {$w}
								LIMIT $offset, $limit ");		
		$tr = '';
		$i = 1;
		if($q->num_rows() > 0){
			foreach($q->result() as $row){

				$stockid_chk = $this->db->query("SELECT m.stockid FROM sch_stock_stockmove AS m WHERE m.stockid = '{$row->stockid}' ")->num_rows();

				$tr .= '<tr>
							<td>'.($i++ + $offset).'</td>
							<td>'.$row->cate_name.'</td>
							<td>'.$row->stockcode.'</td>							
							<td>'.$row->descr_eng.'</td>
							<td style="text-align: right;">'.$row->recorder_qty.'</td>
							<td style="text-align: right;">'.number_format($row->sale_price, 2).'</td>
							<td class="remove_tag no_wrap">';
						if($this->green->gAction("U")){
						$tr .= '<a href="javascript:;" class="btn btn-xs btn-success edit" data-stockid="'.$row->stockid.'" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>';
						}
						$tr .= '</td>
							<td class="remove_tag no_wrap">';
						if($this->green->gAction("D")){ 
						$tr .= '<a href="javascript:;" class="btn btn-xs btn-danger delete" data-stockid="'.$row->stockid.'" data-stockid_chk="'.$stockid_chk.'" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>';
						}	
						$tr .= '</td>							
						</tr>';
			}
		}
		else{
			$tr .= '<tr>
						<td colspan="8" style="font-weight: bold;text-align: center;background: #F2F2F2;">No data!</td>
					</tr>';
		}
		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}

	public function save(){
		$stockid = $this->input->post('stockid');
		$categoryid = $this->input->post('categoryid');
		$stockcode = trim($this->input->post('stockcode'));
		$descr_eng = trim($this->input->post('descr_eng'));
		$recorder_qty = trim($this->input->post('recorder_qty'));
		$sale_price = trim($this->input->post('sale_price'));		
		$descr_kh = trim($this->input->post('descr_kh'));
		$specification = trim($this->input->post('specification'));
		$note = trim($this->input->post('note'));
		
		$data = array("categoryid" => $categoryid,
						"stockcode" => $stockcode,
						"descr_eng" => $descr_eng,
						"recorder_qty" => $recorder_qty,
						"sale_price" => $sale_price,
						"descr_kh" => $descr_kh,
						"specification" => $specification,
						"note" => $note
					);

		$w = "";
		if($stockid - 0 > 0){			
			$w .= "AND c.stockid != '{$stockid}' ";
		}
		$c = $this->db->query("SELECT c.stockid FROM sch_stock AS c WHERE c.stockcode = '".sqlStr($stockcode)."' {$w} ")->num_rows();
		$cc = $this->db->query("SELECT c.stockid FROM sch_stock AS c WHERE c.descr_eng = '".sqlStr($descr_eng)."' {$w} ")->num_rows();

		if($c - 0 == 0){
			if($cc - 0 == 0){
				if($stockid - 0 > 0){
					$data['modified_date'] = date('Y-m-d H:i:s');
		        	$data['modified_by'] = $this->session->userdata('user_name');
					$this->db->update("sch_stock", $data, array("stockid" => $stockid));

					// otherfee type ======
					$this->db->update("sch_setup_otherfee", ['otherfee' => $descr_eng, 'prices' => $sale_price, 'typefee' => 3, 'modified_date' => date('Y-m-d H:i:s'), 'modified_by' => $this->session->userdata('user_name')], ["stockid" => $stockid]);

					$dd['updated'] = 'Updated!';
				}
				else{
					$data['created_date'] = date('Y-m-d H:i:s');
		        	$data['created_by'] = $this->session->userdata('user_name');
					$this->db->insert("sch_stock", $data);					
					$stockid_last = $this->db->insert_id();

					// stock balance =====
					$this->db->query("INSERT INTO sch_stock_balance(
											whcode,
											stockid,
											categoryid)
										SELECT		
											sch_stock_wharehouse.whcode,
											'{$stockid_last}',
											$categoryid									
										FROM
											sch_stock_wharehouse ");

					// otherfee type ======
					$this->db->insert("sch_setup_otherfee", ['stockid' => $stockid_last, 'otherfee' => $descr_eng, 'prices' => $sale_price, 'typefee' => 3, 'created_date' => date('Y-m-d H:i:s'), 'created_by' => $this->session->userdata('user_name')]);
					$dd['saved'] = 'Saved!';
				}
			}
			else{
				$dd['existed'] = 'Item name "'.$descr_eng.'"'.' existed!';
			}
		}
		else{
			$dd['existed'] = 'Item code "'.$stockcode.'"'.' existed!';
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($dd);
	}



	// category ================
	public function edit_category(){
		$categoryid = $this->input->post('categoryid');
		$r = $this->db->query("SELECT
									c.categoryid,
									c.cate_name,
									c.description
								FROM
									sch_stock_category AS c 
								WHERE c.categoryid = '{$categoryid}' ")->row();
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($r);
	}

	public function delete_category(){
		$categoryid = $this->input->post('categoryid');
		$i = 0;
		$i = $this->db->delete("sch_stock_category", array('categoryid' => $categoryid));
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($i);
	}

	public function grid_category(){
		$this->green->setActiveRole($this->session->userdata('roleid'));
		$cate_name = trim($this->input->post('search_cate_name'));
		
		$w = "";
		if($cate_name != ""){			
			$w .= "AND c.cate_name LIKE '%".sqlStr($cate_name)."%' ";
		}

		$q = $this->db->query("SELECT
									c.categoryid,
									c.cate_name,
									c.description
								FROM
									sch_stock_category AS c
								WHERE 1=1 {$w} ORDER BY c.cate_name ASC");		
		$tr = '';
		$i = 1;
		if($q->num_rows() > 0){
			foreach($q->result() as $row){
				$categoryid_c = $this->db->query("SELECT s.categoryid FROM sch_stock AS s WHERE s.categoryid = '{$row->categoryid}' ")->num_rows();

				$tr .= '<tr>
							<td>'.$i++.'</td>
							<td>'.$row->cate_name.'</td>
							<td>'.$row->description.'</td>
							<td>';
							if($this->green->gAction("U")){ 
							$tr .= '<a href="javascript:;" class="btn btn-xs btn-success edit_cate" data-categoryid="'.$row->categoryid.'" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>';
							}	
							$tr .= '</td>
								<td>';
							if($this->green->gAction("D")){ 
							$tr .= '<a href="javascript:;" class="btn btn-xs btn-danger delete_cate" data-categoryid="'.$row->categoryid.'" data-categoryid_c="'.($categoryid_c).'" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>';
							}	
						$tr .= '</td>
						</tr>';
			}
		}
		else{
			$tr .= '<tr>
						<td colspan="5" style="font-weight: bold;text-align: center;background: #F2F2F2;">No data!</td
					</tr>';
		}
		$arr = array('tr' => $tr);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}

	public function save_category(){
		$categoryid = $this->input->post('categoryid');
		$cate_name = trim($this->input->post('cate_name'));
		$description = trim($this->input->post('description'));
		
		$data = array("cate_name" => $cate_name, "description" => $description);

		$w = "";
		if($categoryid - 0 > 0){			
			$w .= "AND c.categoryid != '{$categoryid}' ";
		}	
		$c = $this->db->query("SELECT c.categoryid FROM sch_stock_category AS c WHERE c.cate_name = '".sqlStr($cate_name)."' {$w} ")->num_rows();

		if($c == 0){
			if($categoryid - 0 > 0){
				$data['modified_date'] = date('Y-m-d H:i:s');
	        	$data['modified_by'] = $this->session->userdata('user_name');
				$this->db->update("sch_stock_category", $data, array("categoryid" => $categoryid));
				$d['last_categoryid'] = $categoryid;
				$d['updated'] = 'Updated!';
			}
			else{
				$data['created_date'] = date('Y-m-d H:i:s');
	        	$data['created_by'] = $this->session->userdata('user_name');
				$this->db->insert("sch_stock_category", $data);
				$last_categoryid = $this->db->insert_id();
				$d['last_categoryid'] = $last_categoryid;
				$d['saved'] = 'Saved!';
			}
		}
		else{
			$d['existed'] = 'Category "'.$cate_name.'" existed!';
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($d);
	}

	public function get_category(){
		$last_categoryid = $this->input->post('last_categoryid');
		$q = $this->db->query("SELECT * FROM sch_stock_category AS c ORDER BY c.cate_name ASC");

		$opt = "";
		$opt .= '<option></option>';
		if($q->num_rows() > 0){
			foreach ($q->result() as $row) {
				$opt .= '<option value="'.$row->categoryid.'" '.($row->categoryid == $last_categoryid ? 'selected="selected"' : '').'>'.$row->cate_name.'</option>';
			}
		}
		echo $opt;
	}


}