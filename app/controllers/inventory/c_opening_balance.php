<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_opening_balance extends CI_Controller {	

	function __construct(){
		parent::__construct();
		// $this->load->model('setup/area_model', 'm');
	}

	public function index(){	
		$this->load->view('header');
		$this->load->view('inventory/v_opening_balance');
		$this->load->view('footer');	
	}

	public function edit(){	
		$type = '8';
		$typeno = $this->input->post('typeno');
		$row = $this->db->query("SELECT
										ms.typeno,
										DATE_FORMAT(ms.tran_date, '%d/%m/%Y') AS tran_date,
										ms.note
									FROM
										sch_stock_opening_master AS ms
									WHERE ms.type= '{$type}' AND ms.typeno = '{$typeno}' ")->row();
		$q = $this->db->query("SELECT
									d.qty,
									d.note,
									d.cate_id,
									d.selling_price,
									s.stockcode,
									d.stockid,
									d.whcode,
									d.transno
								FROM
									sch_stock_opening_balance AS d
								INNER JOIN sch_stock AS s ON d.stockid = s.stockid
								WHERE d.type= '{$type}' AND d.transno = '{$typeno}' ")->result();
		$arr = ['row' => $row, 'q' => $q];
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);		
	}

	public function delete(){
		// transaction =====
		$this->db->trans_begin();

			$type = '8';
			$typeno = $this->input->post('typeno');
			$this->db->delete("sch_stock_opening_master", array('type' => $type, 'typeno' => $typeno));
			$this->db->delete("sch_stock_opening_balance", array('type' => $type, 'transno' => $typeno));
			$this->db->delete("sch_stock_stockmove", array('type' => $type, 'transno' => $typeno));

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$i = 0;
		}
		else{
			$this->db->trans_commit();
			$i = 1;
		}		
		header('Content-Type: application/json; charset=utf-8');
		echo $i;
	}

	public function grid(){
		$m = $this->input->post('m');
		$p = $this->input->post('p');
		$this->green->setActiveRole($this->session->userdata('roleid'));		
		$this->green->setActiveModule($m);		
		$this->green->setActivePage($p);

		$type = '8';
		$offset = trim($this->input->post('offset'));
		$limit = trim($this->input->post('limit'));
		$categoryid = trim($this->input->post('categoryid'));
		$stockcode = trim($this->input->post('stockcode'));
		$descr_eng = trim($this->input->post('descr_eng'));
		$qty = trim($this->input->post('qty'));
		$selling_price = trim($this->input->post('selling_price'));
		$from_date = trim($this->input->post('from_date'));
		$to_date = trim($this->input->post('to_date'));
		$whcode = trim($this->input->post('whcode'));
		
		$w = "";
		$wd = "";
		if($from_date != "" && $to_date != ""){			
			$w .= "AND date(m.tran_date) >= '".$this->green->formatSQLDate($from_date)."' ";
			$w .= "AND date(m.tran_date) <= '".$this->green->formatSQLDate($to_date)."' ";			
		}
		// ==============
		if($categoryid != ""){			
			$wd .= "AND d.cate_id = '{$categoryid}' ";
		}
		if($stockcode != ""){			
			$wd .= "AND s.stockcode LIKE '%".sqlStr($stockcode)."%' ";
		}
		if($descr_eng != ""){			
			$wd .= "AND s.descr_eng LIKE '%".sqlStr($descr_eng)."%' ";
		}
		if($qty != ""){			
			$wd .= "AND d.qty = '{$qty}' ";
		}
		if($selling_price != ""){			
			$wd .= "AND d.selling_price = '{$selling_price}' ";
		}
		if($whcode != ""){			
			$wd .= "AND d.whcode = '{$whcode}' ";
		}		
		
		$totalRecord = $this->db->query("SELECT DISTINCT
												m.id
											FROM
												sch_stock_opening_master AS m
											INNER JOIN sch_stock_opening_balance AS d ON m.typeno = d.transno
											INNER JOIN sch_stock_category AS c ON d.cate_id = c.categoryid
											INNER JOIN sch_stock AS s ON d.stockid = s.stockid
											WHERE 1=1 {$w} {$wd} ")->num_rows();
		$totalPage = ceil($totalRecord/$limit) - 0;		
		
		$sql = "SELECT DISTINCT
						m.typeno,
						DATE_FORMAT(m.tran_date, '%d/%m/%Y') AS tran_date
					FROM
						sch_stock_opening_master AS m
					INNER JOIN sch_stock_opening_balance AS d ON m.typeno = d.transno
					INNER JOIN sch_stock_category AS c ON d.cate_id = c.categoryid
					INNER JOIN sch_stock AS s ON d.stockid = s.stockid
					WHERE 1=1 {$w} {$wd}
					ORDER BY m.typeno DESC
					LIMIT $offset, $limit ";
		// echo $sql;
		// exit();
		$q = $this->db->query($sql);	

		$tr = '';		
		if($q->num_rows() > 0){
			foreach($q->result() as $row){
				$tr .='<tr class="trprint" style="_background: #4cae4c;color: #2e6da4;display: none;">
							<td colspan="7">
								<span style="text-align:right;"> #'.$row->typeno.'</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="_font-weight: bold; text-align:center;">Date : &nbsp;&nbsp;'.$row->tran_date.'</span>
							</td>
					   </tr>';

				$tr .= '<tr class=" trhide remove_tag" style="_background: #4cae4c;color: #2e6da4;">
							<td colspan="7">';
					if($this->green->gAction("P")){ 
						$tr .= '<a href="'.site_url('inventory/c_opening_inventory_report/?typeno='.$row->typeno).'" class="btn btn-xs btn-success" title="Print..." target="_blank"><span class="glyphicon glyphicon-print"></span>&nbsp;#<span style="font-weight: bold;">'.$row->typeno.'</span></a>&nbsp;&nbsp; ';
					}	
					$tr .= 'Date:<span style="_font-weight: bold;"> '.$row->tran_date.'</span>';
					$tr .= '</td>
						<td>';
					if($this->green->gAction("U")){ 	
						$tr .= '<a href="javascript:;" class="edit btn btn-xs btn-success" data-typeno="'.$row->typeno.'" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>';
					}
					$tr .= '</td>
						<td>';
					if($this->green->gAction("D")){ 	
						$tr .= '<a href="javascript:;" class=" btn btn-xs btn-danger delete" data-typeno="'.$row->typeno.'" title="Delete"><span class="glyphicon glyphicon-trash"></span></a></td>';
					}

				$tr .= '</tr>';

				$sql_d = "SELECT DISTINCT
								s.stockcode,
								s.descr_eng,
								d.qty,
								d.selling_price,
								d.whcode,
								c.cate_name
							FROM
								sch_stock_opening_master AS m
							INNER JOIN sch_stock_opening_balance AS d ON m.typeno = d.transno
							INNER JOIN sch_stock_category AS c ON d.cate_id = c.categoryid
							INNER JOIN sch_stock AS s ON d.stockid = s.stockid
							WHERE 1=1 {$w} {$wd} AND d.type = '{$type}' AND d.transno = '{$row->typeno}'
							ORDER BY c.cate_name ASC ";
				// echo $sql_d;
				// exit();
				$q_d = $this->db->query($sql_d);

				$i = 1;
				$total_qty = 0;
				$total_price = 0;
				$total_amt = 0;
				if($q_d->num_rows() > 0){
					foreach ($q_d->result() as $row_d){
						$amount = 0;
						$amount += ($row_d->qty - 0)*($row_d->selling_price - 0);
						$total_qty += $row_d->qty - 0;
						$total_amt += $amount - 0;
						$tr .= '<tr>
									<td style="border: 0;">'.$i++.'</td>
									<td style="border: 0;">'.$row_d->cate_name.'</td>
									<td style="border: 0;">'.$row_d->stockcode.'</td>
									<td style="border: 0;">'.$row_d->descr_eng.'</td>
									<td style="text-align: right;border: 0;">'.$row_d->qty.'</td>
									<td style="text-align: right;border: 0;">'.number_format($row_d->selling_price, 2).'</td>
									<td style="text-align: right;border: 0;">'.number_format($amount, 2).'</td>
									<td class="remove_tag no_wrap" colspan="2" style="border: 0;">&nbsp;</td>
								</tr>';
					}
					$tr .= '<tr style="font-weight: bold;">
								<td style="border: 0;text-align: right;font-weight: normal;" colspan="4">Total</td>
								<td style="text-align: right;border: 0;">'.number_format($total_qty, 0).'</td>
								<td style="text-align: right;border: 0;">&nbsp;</td>
								<td style="text-align: right;border: 0;">'.number_format($total_amt, 2).'</td>
								<td class="remove_tag no_wrap"colspan="2" style="border: 0;">&nbsp;</td>
							</tr>';					
				}
				else{
					
				}// detail ======

			}
		}
		else{
			$tr .= '<tr>
						<td colspan="8" style="font-weight: bold;text-align: center;background: #F2F2F2;">No data!</td>
					</tr>';
		}
		$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}


	// master_detail =============
	public function edit_category(){
		$id = $this->input->post('id');
		$r = $this->db->query("SELECT * FROM sch_inv_categories AS c WHERE c.id = '{$id}' ")->row();
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($r);
	}

	public function save_op_md(){
		$type = "8";
		$typeno_ = trim($this->input->post('typeno'));
		$tran_date = $this->green->formatSQLDate(trim($this->input->post('tran_date'))).' '.date('H:i:s');
		$whcode_cate = trim($this->input->post('whcode_cate'));
		$note = trim($this->input->post('note'));		
		$arr = $this->input->post('arr');
		$user_name = $this->session->userdata('user_name');

		// transaction =====
    	$this->db->trans_begin();

			// master ======
			$data_ms = ["type" => $type,
						"created_by" => $user_name,
						"tran_date" => $tran_date,						
						"note" => $note
					];		

			$i = 0;
			if($typeno_ != ""){
				$typeno = $typeno_;
				$this->db->update("sch_stock_opening_master", $data_ms, array("type" => $type, 'typeno' => $typeno));
				// delete =========
				$this->db->delete("sch_stock_opening_balance", array("type" => $type, 'transno' => $typeno));
				$this->db->delete("sch_stock_stockmove", array("type" => $type, 'transno' => $typeno));
				$i = 2;
			}
			else{
				$typeno = $this->green->nextTran($type, "Open Stock");
				$data_ms["typeno"] = $typeno;
				$this->db->insert("sch_stock_opening_master", $data_ms);
				$i = 1;
			}

			if(!empty($arr)){				
				foreach ($arr as $row) {
					if($row["stockcode"] != ""){
						$data_d = ["type" => $type,
									"transno" => $typeno,
									"cate_id" => $row["categoryid"],
									"stockid" => $row["stockid"],
									"note" => $row["remark"],
									"qty" => $row["qty"],
									"selling_price" => $row["selling_price"],
									"transdate" => $tran_date,
									"whcode" => $whcode_cate,
									"created_by" => $user_name,
									"created_date" => $tran_date
								];
						$this->db->insert("sch_stock_opening_balance", $data_d);

						$data_m = ["type" => $type,
									"transno" => $typeno,
									"cate_id" => $row["categoryid"],
									"stockid" => $row["stockid"],
									"quantity" => $row["qty"],
									"selling_price" => $row["selling_price"],
									"date" => $tran_date,
									"whcode" => $whcode_cate,
									"created_by" => $user_name
								];
						$this->db->insert("sch_stock_stockmove", $data_m);		
					}	
				}
			}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$i = 4;
		}
		else{
			$this->db->trans_commit();
			$i;
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($i);
	}

	public function get_category(){
		$q = $this->db->query("SELECT * FROM sch_stock_category AS c ORDER BY c.cate_name ASC");
		$opt = "";
		$opt .= '<option></option>';
		if($q->num_rows() > 0){
			foreach ($q->result() as $row) {
				$opt .= '<option value="'.$row->categoryid.'">'.$row->cate_name.'</option>';
			}
		}
		echo $opt;
	}

	// autocomplete =======
	public function get_std(){
		$term = trim($_REQUEST['term']);
		$where = "";
		if($term != ""){
			$where .= "AND ( s.stockcode LIKE '%".sqlStr($term)."%' ";
			$where .= "or s.descr_eng LIKE '%".sqlStr($term)."%' ) ";	     
		}
		$sql = "SELECT
						s.stockid,
						s.stockcode,
						s.descr_eng,
						s.recorder_qty,
						s.sale_price,
						s.categoryid
					FROM
						sch_stock AS s
					WHERE
						1 = 1 {$where} 
					LIMIT 0, 10 ";
		$qr = $this->db->query($sql);

		$arr = [];
		if($qr->num_rows() > 0){
			foreach ($qr->result() as $row){
				$arr[] = ['stockid' => $row->stockid, 'categoryid' => $row->categoryid, 'stockcode' => $row->stockcode, 'descr_eng' => $row->descr_eng, 'qty' => $row->recorder_qty, 'sale_price' => $row->sale_price];            
			}
		}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
	}

	public function get_stock(){
		$term = trim($_REQUEST['term']);
		$categoryid = trim($_REQUEST['categoryid']);

		$w = "";
		$w1 = "";
		if($term != ""){
			$w .= "AND s.stockcode LIKE '%".sqlStr($term)."%' ";    
		}		
		if($categoryid != ""){
			$w1 .= "AND s.categoryid = '{$categoryid}' ";     
		}

		$sql = "SELECT
					s.stockcode
				FROM
					sch_stock AS s
				WHERE 1=1 {$w} {$w1} LIMIT 0, 10 ";
		$qr = $this->db->query($sql);

		$arr = [];
		if($qr->num_rows() > 0){
			foreach ($qr->result() as $row) {
				$arr[] = ['stockcode' => $row->stockcode];
			}
		}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
	}

	public function get_stock_(){
		$term = trim($_REQUEST['term']);
		$categoryid = trim($_REQUEST['categoryid']);

		$w = "";
		$w1 = "";
		if($term != ""){
			$w .= "AND s.descr_eng LIKE '%".sqlStr($term)."%' ";    
		}		
		if($categoryid != ""){
			$w1 .= "AND s.categoryid = '{$categoryid}' ";     
		}

		$sql = "SELECT
					s.descr_eng
				FROM
					sch_stock AS s
				WHERE 1=1 {$w} {$w1} LIMIT 0, 10 ";
		$qr = $this->db->query($sql);

		$arr = [];
		if($qr->num_rows() > 0){
			foreach ($qr->result() as $row) {
				$arr[] = ['descr_eng' => $row->descr_eng];
			}
		}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($arr);
	}

	// check stockcode =======
	public function chk_stockcode(){
		$stockcode = trim($this->input->post('stockcode'));

		if($stockcode != ""){
			$q = $this->db->query("SELECT * FROM sch_stock AS s WHERE s.stockcode = '".sqlStr($stockcode)."' ");			
			if($q->num_rows() > 0){
				$d['r'] = $q->row();
				$d['y'] = 'Correct!';
			}
			else{
				$d['n'] = 'Item code "'.$stockcode.'" incorrect!';
			}
			header("Content-type: application/json; charset=utf-8");
			echo json_encode($d);
		}		
	}


}