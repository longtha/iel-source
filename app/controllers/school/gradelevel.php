<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class gradelevel extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('school/gradelevelmodel','gradelevels');
		$this->load->library('pagination');
	}
	public function index()
	{
		$this->load->view('header');
		//school/schoolinfor is call view

		//for pagination is use $data, if not use $data
		$data['query']=$this->gradelevels->getpagination();
		$this->load->view('school/gradelevel/add');
		$this->load->view('school/gradelevel/view',$data);
		$this->load->view('footer');
	}
	function addnew()
	{
		$this->load->view('header');
		//school/schoolinfor is call view
		$this->load->view('school/schoolinfor/schoolinfor/add');
		$this->load->view('footer');
	}
	function saves()
	{
		$schoolid=$this->input->post('schoolid');
		$glevel=$this->input->post('glevel');
		$nlevel=$this->input->post('nlevel');
		$schlevelid=$this->input->post('schlevelid');

		$count=$this->gradelevels->getvalidate($schoolid, $glevel, $schlevelid);

		if ($count!=0){
			echo 'false';
		}else{
			$data=array(
			  		'schoolid'=>$schoolid,
			  		'grade_level'=>$glevel,
			  		'next_grade_level'=>$nlevel,
			  		'schlevelid'=>$schlevelid,
			  );
			$this->db->insert('sch_grade_level',$data);

			echo'true';
		}
	}
	function edits($gradelevel_id)
	{
		$row=$this->gradelevels->getgradelevel_row($gradelevel_id);
		$this->load->view('header');
		$data['query']=$row;
		//$data1['query']=$this->db->get('sch_subject')->result();
		$this->load->view('school/gradelevel/edit',$data);
		$data['query']=$this->gradelevels->getpagination();
		$this->load->view('school/gradelevel/view',$data);
		$this->load->view('footer');
	}
	function updates()
	{
		$glevelid=$this->input->post('glevelid');
		$schoolid=$this->input->post('schoolid');
		$glevel=$this->input->post('glevel');
		$nlevel=$this->input->post('nlevel');
		$schlevelid=$this->input->post('schlevelid');

		$count=$this->gradelevels->getvalidateup($glevel, $glevelid, $schoolid, $schlevelid);

			if ($count!=0){
				echo 'false';
			}else{
					$this->db->where('grade_levelid',$glevelid);

					$data=array(
								'grade_level'=>$glevel,
								'schoolid'=>$schoolid,
								'next_grade_level'=>$nlevel,
								'schlevelid'=>$schlevelid,
					);
					$this->db->update('sch_grade_level',$data);

			echo'true';
			}
	}
	function deletes($grade_levelid)
	{
		// $this->db->where('grade_levelid',$grade_levelid);
		// $data=array('is_active'=>0);
		// $this->db->update('sch_grade_level',$data);

		$this->db->where('grade_levelid', $grade_levelid);
  		$this->db->delete('sch_grade_level');

        $m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }

        redirect("school/gradelevel/?delete&m=$m&p=$p");
	}
	function searchs(){
		  //----------- Pagination -----------
		  //  if(isset($_GET['s'])){
			 //   $data['query']=$this->gradelevels->searchgrade_level($schoolid,$schoollevelid,$schlevelid);

			 //   $this->load->view('header');
			 //   $this->load->view('school/schoolinfor/add');
			 //   $this->load->view('school/schoolinfor/view',$data);
			 //   $this->load->view('footer');
		  // }

		  // if(!isset($_GET['s'])){
		   $schoolid=$this->input->post('schoolid');
		   $schoollevelid=$this->input->post('schoollevelid');
		   // $schlevelids=$this->input->post('schlevelid');

		   $m=$this->input->post('m');
		   $p=$this->input->post('p');
		   $this->green->setActiveRole($this->input->post('roleid'));
		   if($m!=''){
		        $this->green->setActiveModule($m);
		   }
		   if($p!=''){
		        $this->green->setActivePage($p);
		   }

		   $query=$this->gradelevels->searchgrade_level($schoolid,$schoollevelid);
		    $i=1;
		    foreach ($query as $row) {
		     echo "
			        <tr>
			          <td align=center width=40>$i</td>
			          <td width=200>$row->name</td>
			          <td width=170>$row->grade_level</td>
			          <td width=130>";
			          	if($row->next_grade_level=='0' || $row->next_grade_level==''){
			          		echo '';
			          	}else{
			          		echo $row->next_grade_level;
			          	}
			          echo "</td>
			          <td width=170>$row->sch_level</td>
			          <td align=center width=130>";
			          	  if($this->green->gAction("U")){
			              	echo "<a href='".site_url('school/gradelevel/edits/'.$row->grade_levelid)."?m=$m&p=$p"."'><img src='".site_url('../assets/images/icons/edit.png')."' />
			              	</a> |";
			              }
			              if($this->green->gAction("D")){
			              	echo "<a><img onclick='deletegradelevel(event);' rel='$row->grade_levelid' src='".site_url('../assets/images/icons/delete.png')."' />
			              	</a>";
			              }
			          echo "</td>
			        </tr>" ;
			        $i++;
		 // }
   //	  echo "<tr>
   //   			<td colspan='12' id='pgt'>
   //   				<div style='text-align:left'>
   //   					<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
   //   				</div></td>
   //  		   </tr>";
		}
	}

    function getlevels($schlevelid){
        $arr=array();
        $data=$this->gradelevels->getgradelevels($schlevelid);
        header("Content-type:text/x-json");
        echo json_encode($data);
        exit();
    }

    function getGradeLevel(){
			$school_level_id = $this->input->post("school_level");

      $grade_level = $this->gradelevels->getgradelevels($school_level_id);
      header("Content-type:text/x-json");
      echo json_encode($grade_level);
      exit();
    }
}
