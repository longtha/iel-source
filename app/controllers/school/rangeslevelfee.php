<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rangeslevelfee extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model('school/schoolinformodel','sch');
        $this->load->model('school/schoolyearmodel','schyear');
        $this->load->model('school/schoollevelmodel','schlevels');
        $this->load->model('school/modrangelevel','rnglev');
        $this->load->model('school/modrangelevelfee','rnglevfee');
        $this->load->model('school/modfeetype','mfeetype');
    }
	public function index()
	{
        $data['schyears']=$this->schyear->getschoolyear();
        $data['schevels']=$this->schlevels->getsch_level();
        $data['rangelevs']=$this->rnglev->rangelevels();
        $data['feetypes']=$this->mfeetype->getfeetypes();
        $this->load->view('header');
		$this->load->view('school/rangelevelfee/vrangeslevelfee',$data);
		$this->load->view('footer');
	}
	function save()
	{
         $this->rnglevfee->save();
	}
	function delete($ranglevfeeid){
        $arr=$this->rnglevfee->delete($ranglevfeeid);
        echo json_encode($arr);
        exit();
	}
    function  edit($ranglevfeeid){
        $arr=$this->rnglevfee->edit($ranglevfeeid);
        echo json_encode($arr);
        exit();
    }
	function search(){
        $start=$this->input->post('startpage');
        $this->rnglevfee->search($start);
	}
    function getStudyPeriod(){
        $yearid=$this->input->post("yearid");
        $schoolid=$this->input->post("schoolid");
        $schlevelid=$this->input->post("sclevelid");
        $payment_type=$this->input->post("payment_type");
        $rangelevelid=$this->input->post("rangelevelid");
        $arr['datas']=$this->mfeetype->getStudyPeriod("",$yearid,$schoolid,$schlevelid,$payment_type,$rangelevelid);
        echo json_encode($arr);
        exit();
    }
    function getRangeLevels($sclevelid){
        $arr['datas']=$this->rnglev->rangelevels("","",$sclevelid);
        echo json_encode($arr);
        exit();

    }


}