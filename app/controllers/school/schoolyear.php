<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class schoolyear extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/schoolyearmodel','schoolyears');
        $this->load->model('school/schoollevelmodel','schlevels');
		$this->load->library('pagination');		
	}
	public function index()
	{
		$this->load->view('header');
        $data['schevels']=$this->schlevels->getsch_level();

		$this->load->view('school/schoolyear/add',$data);
		$data['query']=$this->schoolyears->getpagination();
		$this->load->view('school/schoolyear/view',$data);
		$this->load->view('footer');
	}
	function addnew()
	{
		$this->load->view('header');
		$this->load->view('school/schoolyear/schoolyear/add');
		$this->load->view('footer');	
	}
	function saveschoolyear()
	{
		$year=$this->input->post('year');

		$fromdate=date_create($this->input->post('fromdate'));
	 	$todate=date_create($this->input->post('todate'));
        $sclevelid=$this->input->post('sclevelid');

		$school=$this->input->post('school');
        $programid= $this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='".$this->input->post('sclevelid')."'")->row()->programid;

		$count=$this->schoolyears->schoolyearvalidate($year,date_format($fromdate, 'Y-m-d'),date_format($todate, 'Y-m-d'),$school,$sclevelid);
		if ($count!=0){
			echo 'false';
		}else{


			$data=array(
			  		'sch_year'=>$this->input->post('year'),
			  		'from_date'=>date_format($fromdate, 'Y-m-d'),
			  		'to_date'=>date_format($todate, 'Y-m-d'),
					'schoolid'=>$this->input->post('school'),
                    'schlevelid'=>$sclevelid,
                    'programid'=>$programid
			  );	
				$this->db->insert('sch_school_year',$data);

			echo'true';	
		}
	}
	function editschoolyear($year_id)
	{
		$row=$this->schoolyears->getschoolyearrow($year_id);
        $data['schevels']=$this->schlevels->getsch_level();
		$this->load->view('header');
		$data['query']=$row;
		$this->load->view('school/schoolyear/edit',$data);
		$data['query']=$this->schoolyears->getpagination();
		$this->load->view('school/schoolyear/view',$data);
		$this->load->view('footer');	
	}
	function updateschoolyear()
	{
		$yearid=$this->input->post('yearid');
		$year=$this->input->post('year');
        $sclevelid=$this->input->post('sclevelid');

		$fromdate=date_create($this->input->post('fromdate'));
	 	$todate=date_create($this->input->post('todate'));

		$school=$this->input->post('school');
		$count=$this->schoolyears->getvalidateup($year,date_format($fromdate, 'Y-m-d'),$school,date_format($todate, 'Y-m-d'),$yearid,$sclevelid);
        $programid= $this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='".$sclevelid."'")->row()->programid;
			if ($count!=0){
				echo 'false';
			}else{
					$this->db->where('yearid',$yearid);

					$data=array(
								'sch_year'=>$this->input->post('year'),
								'from_date'=>date_format($fromdate, 'Y-m-d'),
								'to_date'=>date_format($todate, 'Y-m-d'),
								'schoolid'=>$this->input->post('school'),
                                'schlevelid'=>$sclevelid,
                                'programid'=>$programid
					);
					$this->db->update('sch_school_year',$data);

			echo'true';	
			}
	}
	function deleteschoolyear($yearid)
	{
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }

		$this->db->where('yearid', $yearid);
        $this->db->delete('sch_school_year');
        redirect("school/schoolyear/?delete&m=$m&p=$p");
	}
	function search(){
		   $year=$this->input->post('year');
		   $schoolid=$this->input->post('school');
		   $yearid=$this->input->post('yearid');

		   $m=$this->input->post('m');
		   $p=$this->input->post('p');
		   $this->green->setActiveRole($this->input->post('roleid'));
		   if($m!=''){
		        $this->green->setActiveModule($m);
		   }
		   if($p!=''){
		        $this->green->setActivePage($p); 
		   }

		   $query=$this->schoolyears->searchschoolyears($year,$schoolid,$yearid);
		    $i=1;
		    foreach ($query as $schoolyearrow) {
		    	$fromdate=date_create($schoolyearrow->from_date);
				$todate=date_create($schoolyearrow->to_date);
		     	
		     	echo "		      
			        <tr>
			          <td align=center width=40>$i</td>
			          <td width=170>$schoolyearrow->sch_year</td>
			          <td>".date_format($fromdate, 'd-m-Y')."</td>
				      <td>".date_format($todate, 'd-m-Y')."</td>
			          <td>$schoolyearrow->name</td>
			          <td>$schoolyearrow->sch_level</td>
			          <td align=center width=130>";
			              if($this->green->gAction("U")){
			              	echo "<a href='".site_url('school/schoolyear/editschoolyear/'.$schoolyearrow->yearid)."?m=$m&p=$p"."'>
					              <img src='".site_url('../assets/images/icons/edit.png')."' />
					              </a> |";
			          	  }
			          	  if($this->green->gAction("D")){
			              	echo "<a><img onclick='deleteschoolyear(event);' rel='$schoolyearrow->yearid' src='".site_url('../assets/images/icons/delete.png')."' />
			              		  </a>"; 
			          	  }
			          echo "</td>
			        </tr>
			        " ;
			        $i++;		 	
	//	 	   echo "<tr>
    //  			<td colspan='12' id='pgt'>
    //  				<div style='text-align:left'>
    //  					<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
    //  				</div></td>
    // 		   </tr>";
		}
	}
}