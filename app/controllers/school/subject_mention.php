<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subject_mention extends CI_Controller {
    
    function __construct(){
        parent::__construct();
        $conf['admin_menu_cache_client'] = FALSE;
        ini_set('max_input_vars', 10000);

        $this->load->model('school/mod_subject_mention', 'sm');
        // $this->load->model('school/program', 'p');
        // $this->load->model('school/schoolyearmodel', 'y');
        $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');
        $this->load->model('school/subjectmodel', 'sub');

        $this->load->model('school/gradelevelmodel', 'g');
    }

    public function index(){    
        $this->load->view('header');        
        $this->load->view('school/subjectmention/v_subjectmention');
        $this->load->view('footer');    
    }    

    public function grid(){
        $grid = $this->sm->grid();
        header('Content-Type: application/json; charset=utf-8');
        echo $grid;
    }
    
    public function save(){
        $i = $this->sm->save();       
        header('Content-Type: application/json; charset=utf-8');
        echo $i;
    }

    public function edit(){ 
        $examid = $this->input->post('examid') - 0;
        $row = $this->sm->edit($examid);
        header('Content-Type: application/json; charset=utf-8');
        echo $row;      
    }

    public function delete(){   
        $examid = $this->input->post('examid') - 0;
        $i = $this->sm->delete($examid);
        header('Content-Type: application/json; charset=utf-8');
        echo $i;
    }

    public function get_subject(){ 
        $schlevelid = $this->input->post('schlevelid');
        $programid = $this->input->post('programid');
        $groupsubjectid = $this->input->post('groupsubjectid');
        $get_subject    = $this->sm->get_subject($schlevelid,$programid,$groupsubjectid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_subject;      
    }
    public function get_subject_group(){ 
        $schlevelid = $this->input->post('schlevelid');
        $programid  = $this->input->post('programid');
        
        $subject_group = $this->sm->m_group_subject($schlevelid,$programid);
        header('Content-Type: application/json; charset=utf-8');
        $opt = "<option value=''></option>";
        if($programid == 2){
            if($subject_group->num_rows() > 0){
                foreach($subject_group->result() as $row_g){
                    $opt.= "<option value='".$row_g->subj_type_id."' cp_participat='".$row_g->is_class_participation."'>".$row_g->subject_type."</option>";
                }
            }
        }else{
            if($subject_group->num_rows() > 0){
                foreach($subject_group->result() as $row_g){
                    $opt.= "<option value='".$row_g->subj_type_id."' cp_participat='0'>".$row_g->subject_type."</option>";
                }
            }
        }
        echo $opt;      
    }
    public function get_grade(){
        $schlevelid = $this->input->post('schlevelid');
        $get_grade = $this->sm->get_grade($schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_grade;
    }    

}