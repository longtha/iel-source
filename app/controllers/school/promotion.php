<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Promotion extends CI_Controller{

		function __construct(){
			parent::__construct();	
			$this->load->model("school/Modpromotion","promo");	
			$this->load->library('pagination');	
			$this->thead=array("No"=>'no',
								"Promotion"=>'proname',
								"Promotion KH"=>'proname_kh',
								"School Level"=>'sch_level',
								"Start Date"=>'start_date',
								"End Date"=>'end_date',
								'Action'=>'action'
								);
			$this->idfield="displanid";					
		}
		function index()
		{	

			$data['tdata']=$this->promo->getpromotion();
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Promotion";			
			$this->parser->parse('header', $data);
			$this->parser->parse('school/promotion/promotion_list', $data);
			$this->parser->parse('footer', $data);
		}
		function save(){
			$promot_id=$this->input->post('promot_id');
			$promotion=$this->input->post('promotion');
			$promotion_kh=$this->input->post('promotion_kh');
			$schlevelid=$this->input->post('schlevelid');
			$start_date=$this->input->post('start_date');
			$end_date=$this->input->post('end_date');
			$desc=$this->input->post('desc');
			$year=$this->session->userdata('year');
			$school=$this->session->userdata('schoolid');
			$is_closed=0;
			if(isset($_POST['is_close']))
				$is_closed=1;
			$c_date=date('Y-m-d');
			$user=$this->session->userdata('user_name');
			$data=array('proname'=>$promotion,
						'proname_kh'=>$promotion_kh,
						'yearid'=>$year,
						'schoolid'=>$school,
						'schlevelid'=>$schlevelid,
						'start_date'=>$this->green->convertSQLDate($start_date),
						'is_closed'=>$is_closed,
						'description'=>$desc,
						'end_date'=>$this->green->convertSQLDate($end_date));
			//$count=$this->dep->validate($department,$dep_id);
			if($promot_id!=''){
				$data2=array('modified_by'=>$user,
							'modified_date'=>$c_date);
				$this->db->where('promot_id',$promot_id)->update('sch_school_promotion',array_merge($data,$data2));
			}else{
				$data2=array('created_by'=>$user,
							'create_date'=>$c_date);
				$this->db->insert('sch_school_promotion',array_merge($data,$data2));
			}
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("school/promotion?m=$m&p=$p");
		}
		function edit(){
			$promot_id=$this->input->post('promot_id');
			$row=$this->db->where('promot_id',$promot_id)->get('sch_school_promotion')->row();
			header("Content-type:text/x-json");
			echo json_encode($row);
		}
		function validate(){
			$promot_id=$this->input->post('promot_id');
			$promotion=$this->input->post('promotion');
			$school=$this->session->userdata('schoolid');
			$this->db->select('count(promot_id) as count')
					->from('sch_school_promotion')
					->where('proname',$promotion)
					->where('schoolid',$school);
			if($promot_id!='')
				$this->db->where_not_in('promot_id',$promot_id);
			$count=$this->db->get()->row()->count;
			echo $count;
		}
		function delete($promot_id){
			$this->db->where('promot_id',$promot_id)->delete('sch_school_promotion');
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("school/promotion?m=$m&p=$p");
		}
		function search(){
			if(isset($_GET['m'])){
					$promotion=$_GET['pro'];
					$schlevelid=$_GET['l'];
					$m=$_GET['m'];
					$p=$_GET['p'];
					$sort_num=$_GET['s_num'];
					$yearid=$_GET['y'];
					$data['tdata']=$this->promo->search($promotion,$schlevelid,$sort_num,$m,$p,$yearid);
					$data['idfield']=$this->idfield;		
					$data['thead']=	$this->thead;
					$data['page_header']="Promotion";			
					$this->parser->parse('header', $data);
					$this->parser->parse('school/promotion/promotion_list', $data);
					$this->parser->parse('footer', $data);
			}else{
					
					$schlevelid=$this->input->post('schlevelid');
					$promotion=$this->input->post('promotion');
					$sort_num=$this->input->post('s_num');
					$yearid=$this->input->post('yearid');
					$i=1;
					$m=$this->input->post('m');
					$p=$this->input->post('p');
					$this->green->setActiveRole($this->input->post('roleid'));
					if($m!=''){
				        $this->green->setActiveModule($m);
				    }
				    if($p!=''){
				        $this->green->setActivePage($p); 
				    }
					foreach ($this->promo->search($promotion,$schlevelid,$sort_num,$m,$p,$yearid) as $row) {
						echo "<tr>
								<td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
								<td class='proname'>".$row->proname."</td>
								<td class='proname_kh'>".$row->proname_kh."</td>
								<td class='sch_level'>".$row->sch_level."</td>
								<td class='start_date'>".$this->green->convertSQLDate($row->start_date)."</td>
								<td class='end_date'>".$this->green->convertSQLDate($row->end_date)."</td>
								<td class='remove_tag'>";
								if($this->green->gAction("D")){
									 echo "<a>
									 		<img rel=".$row->promot_id." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
									 	</a>";
								}
								if($this->green->gAction("U")){
									 echo "<a>
									 		<img  rel=".$row->promot_id." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
									 	</a>";
								}
							echo "</td>
						</tr>
						 ";
						 $i++;
					}
					echo "<tr class='remove_tag'>
								<td colspan='12' id='pgt'>
									<div style='margin-top:20px; width:10%; float:left;'>
									Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
													
													$num=10;
													for($i=0;$i<20;$i++){?>
														<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
														<?php $num+=10;
													}
													
												echo "</select>
									</div>
									<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
										<ul class='pagination' style='text-align:center'>
										 ".$this->pagination->create_links()."
										</ul>
									</div>

								</td>
							</tr> ";
			}
			
		}
}