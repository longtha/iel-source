<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Academic_year extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('school/modacademic_year', 'year');
        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolyearmodel', 'y');
        $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');
        $this->load->model('school/modfeetype', 'f');
        $this->load->model('school/modrangelevel', 'r');

        $this->load->library('pagination');
    }

    public function index(){
    	$this->load->view('header');
		$this->load->view('school/academic_year/index');
		$this->load->view('footer');
    }

    public function save(){
 		$i = $this->year->save();		
		header('Content-Type: application/json; charset=utf-8');
		echo $i;

    }

    public function edit(){	
		$yearid = $this->input->post('yid') - 0;
		$row = $this->year->edit($yearid);
		header('Content-Type: application/json; charset=utf-8');
		echo $row;		
	}

	public function delete(){	
		$yearid = $this->input->post('yearid') - 0;
		$i = $this->year->delete($yearid);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

    public function grid(){
    	$year = $this->year->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $year;
    }

    public function get_schlevel(){
        $programid = $this->input->post('programid') - 0;        
        $get_schlevel = $this->year->get_schlevel($programid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel;
    }

    public function get_year(){
        $programid = $this->input->post('programid') - 0;
        $schlevelid = $this->input->post('schlevelid') - 0;                
        $get_year = $this->year->get_year($programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    public function get_rangelevel(){
        $schlevelid = $this->input->post('schlevelid') - 0;
        $yid = $this->input->post('yid') - 0;
        // echo $schlevelid.' | '.$yid;
        // exit();
        $get_rangelevel = $this->year->get_rangelevel($schlevelid, $yid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_rangelevel;
    }

    // ===============
    public function get_program_search(){
        $programid_search = $this->input->post('programid_search') - 0;        
        $get_schlevel_search = $this->year->get_schlevel($programid_search);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel_search;
    }
    public function get_year_search(){
        $programid_search = $this->input->post('programid_search') - 0;
        $schlevelid_search = $this->input->post('schlevelid_search') - 0;                
        $get_year_search = $this->year->get_year($programid_search, $schlevelid_search);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year_search;
    }


}