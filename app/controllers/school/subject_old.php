<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class subject extends CI_Controller {

	function __construct()
	{
		parent::__construct();			
		$this->load->model('school/subjectmodel','subjects');
        $this->load->model('school/schoolinformodel','sch');
        $this->load->model('school/schoolyearmodel','schyear');
        $this->load->model('school/schoollevelmodel','schlevels');
        $this->load->model('school/gradelevelmodel','gdlevel');
        $this->load->model('school/modsetupexam','extype');
		$this->load->library('pagination');		
	}
	public function index()
	{
        $data['schyears']=$this->schyear->getschoolyear();
        $data['schevels']=$this->schlevels->getsch_level("1");        
        $data['gradelevels']=$this->gdlevel->getgradelevels();
		$this->load->view('header');
		$this->load->view('school/subject/add',$data);
		$data['query']=$this->subjects->getpagination();
		$this->load->view('school/subject/view',$data);
		$this->load->view('footer');
	}
	
	function addnew()
	{
		$this->load->view('header');
		//school/subjecttype is call view
		$this->load->view('school/subject/subject/add');
		$this->load->view('footer');	
	}
	function  save(){
        $subjectid = $this->input->post('subjectid');
        $save = $this->subjects->save($subjectid);
        header("Content-type:text/x-json");
		$arrJson["res"]=$save;
        echo json_encode($arrJson);
        exit();
    }
	
	function editsubject($subject_id)
	{
		$subject=$this->subjects->getsubjectrow($subject_id);		
		$this->load->view('header');
		$data['query']= $subject;		
		//school/subjecttype is call view
		//$data1['query']=$this->db->get('sch_subject')->result();
		$this->load->view('school/subject/edit',$data);
		$data['query']=$this->subjects->getpagination();
		$this->load->view('school/subject/view',$data);
		$this->load->view('footer');	
	}
	function updatesubject()
	{
		$subject=$this->input->post('txtsubject');
		$subjecttypeid=$this->input->post('cbosubjecttype');
		$shortcut=$this->input->post('txtshort_sub');
		$school=$this->input->post('cboschool');
		$is_trim=0;
		if(isset($_POST['is_trimester']))
			$is_trim=1;
		$is_eval=0;
		if(isset($_POST['is_eval']))
			$is_eval=1;
		$subjectid=$this->input->post('txtsubjectid');
		$subjkh=$this->input->post('txtsubjectkh');

		$count=$this->subjects->getvalidateup($subject,$subjecttypeid,$school,$subjectid,$is_trim);
		
			if ($count!=0){
				$this->load->view('header');
				$data['exist']="<p style='color:red;'>Your data has already exist...!</p>";
				$data['query']=$this->subjects->getpagination();
				$this->load->view('school/subject/add',$data);
				$this->load->view('school/subject/view',$data);
				$this->load->view('footer');
			}else{
					$this->db->where('subjectid',$subjectid);

					$data=array(
								'subject'=>$subject,
								'subj_type_id'=>$subjecttypeid,
								'short_sub'=>$shortcut,
								'schoolid'=>$school,
								'is_trimester_sub'=>$is_trim,
								'is_eval'=>$is_eval,
								'subject_kh'=>$subjkh,
								'modified_by'=>$this->session->userdata('userid'),
								'modified_date'=>date('Y-m-d H:i:s')
					);
					
					$this->db->update('sch_subject',$data);

				$m='';
				$p='';
				if(isset($_GET['m'])){
			    	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }
				redirect("school/subject/?edit&m=$m&p=$p");		
			}
	}
	
	function search(){
			$subject= $this->input->post('subject');
			$subjecttype = $this->input->post('subjecttype');
			$shortcut = $this->input->post('shortcut');
			$schlevelid = $this->input->post('schlevelid');
			$gradelevelid = $this->input->post('gradelevelid');
			$examtype = $this->input->post('examtype');

			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			$sk=$this->input->post('sk');

			if($m!=''){
			    $this->green->setActiveModule($m);
			}
			if($p!=''){
				$this->green->setActivePage($p); 
			}
		  	$query=$this->subjects->searchsubjects($subject,$subjecttype,$shortcut,$schlevelid,$gradelevelid,$m,$p);

		   	$i=1;
		    if(count($query)>0){
			      $arrSubjid = array();
			      $arrGrade = array();
			      foreach($query as $sub_row){
			        $trim='';
			        $is_eval='';
			        
			        echo "<tr>
				            <td align=center width=40>". (!in_array($sub_row->subjectid,$arrSubjid)?$i:'') ."</td>
				            <td>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->sch_level:'') ."</td>
				            <td width=170>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->subject_type:'') ."</td>
				            <td width=170>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->subject:'') ."</td>
				            <td>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->short_sub:'') ."</td>
				            <td>". (!in_array($sub_row->subj_grad_id,$arrGrade)?$sub_row->grade_level:'') ."</td>			            
				            
				            <td align=center width=130>
				                <a href='".site_url('school/subject/editsubject/'.$sub_row->subjectid)."?m=$m&p=$p'><img src='".site_url('../assets/images/icons/edit.png')."' /></a> |
				                <a onclick='deletesubjecttype(event);' rel='$sub_row->subjectid'><img src='".site_url('../assets/images/icons/delete.png')."' /></a> 
				            </td>
				          </tr> " ;
			          $i++;
			         
			        $arrSubjid[] = $sub_row->subjectid;
			        $arrGrade[] = $sub_row->subj_grad_id;
			      }
			    }else{
			      echo '=<tr>
			              <td colspan="8">
			                  <h4><i>No result</i></h4>
			              </td>
			            </tr>';
			    }
		echo "<tr>
     			<td colspan='8' id='pgt'>
     				<div style='text-align:left;display:none;'>
     					<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
     				</div></td>
    		   </tr>";
		
	}
	function searchpage(){
		$m = $_GET['m'];
		$p = $_GET['p'];
		$subjectid = $_GET['subj'];
		$groupsubj = $_GET['subj_type'];
		$shortcut  = $_GET['short'];
		$schlevelid = $_GET['schlid'];
		$gradlevelid = $_GET['gradlid'];
		if($m!=''){
		    $this->green->setActiveModule($m);
		}
		if($p!=''){
			$this->green->setActivePage($p); 
		}
		$page=0;
		if(isset($_GET['per_page']))
			$page=$_GET['per_page'];
		$config['base_url']=site_url("school/subject/searchpage?m=$m&p=$p&subj=$subjectid&subj_type=$groupsubj&short=$shortcut&schlid=$schlevelid&gradlid=$gradlevelid");
		$config['page_query_string'] = TRUE;
		$config['per_page']=100;
		$config['num_link']=3;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] ='<a><span>';
		$config['cur_tag_close'] ='</span></a>';
		$where ="";

		if($subjectid!=""){
			$where .=" AND subject like '%".$subjectid."%' OR subject_kh like '%".$subjectid."%'";
			$this->db->like('subject',$subjectid);
			//$this->db->like('subject_kh',$subject);
		}
		if($groupsubj!=""){
			$where .=" AND subj_type_id='".$groupsubj."'";
			$this->db->where('subj_type_id',$groupsubj);
		}

		if($shortcut!=""){
			$where .=" AND short_sub like'%".$shortcut."%'";
			$this->db->like('short_sub',$shortcut);
		}
		if($schlevelid!=""){
			$where .=" AND schlevelid='".$schlevelid."'";
			$this->db->where('schlevelid',$schlevelid);
		}
		if($gradlevelid!=""){
			$where .=" AND grade_levelid='".$gradlevelid."'";
			$this->db->where('grade_levelid',$gradlevelid);
		}			
		$config['total_rows']=$this->db->get('v_school_subject')->num_rows();
		$this->pagination->initialize($config);

		$query = $this->db->query("SELECT
							v_sub.subjectid,
							v_sub.`subject`,
							v_sub.subj_type_id,
							v_sub.short_sub,
							v_sub.schlevelid,
							v_sub.subject_kh,
							v_sub.orders,
							v_sub.sch_level,
							v_sub.subject_type,
							v_sub.grade_level,
							v_sub.subj_grad_id,
							v_sub.exam_type_id,
							v_sub.grade_levelid,
							v_sub.is_trimester_sub,
							v_sub.is_eval,
							v_sub.exam_name,
                            v_sub.max_score
						FROM
							v_school_subject AS v_sub
						WHERE 1=1 {$where}
						ORDER BY v_sub.`subject`, v_sub.subjectid,v_sub.grade_levelid
						-- LIMIT {$page},{$config['per_page']}
						");

		$data['schyears']=$this->schyear->getschoolyear();
        $data['schevels']=$this->schlevels->getsch_level("1");        
        $data['gradelevels']=$this->gdlevel->getgradelevels();
		$this->load->view('header');
		$this->load->view('school/subject/add',$data);
		$data['query']=$query;
		$this->load->view('school/subject/view',$data);
		$this->load->view('footer');
	}
    function deletesubject()
	{
		$subjectid=$this->input->post('subjectid');
        $deleted=$this->subjects->deletesubject($subjectid);
        header("Content-type:text/x-json");
        echo json_encode($deleted);
        exit();	
	}
    function delete()
    {
        $arrJson=array();
        $subjectid=$this->input->post('subjectid');
        $this->db->where('subjectid', $subjectid);
        $this->db->delete('sch_subject');
        $arrJson['res']=0;
        if($this->db->_error_number()==0){
            $arrJson['res']=1;
        };
        header("Content-type:text/x-json");
        echo json_encode($arrJson);
        exit();
    }
    function select(){
        $subjectid=$this->input->post('subjectid');
        $subinf=array();
        $subinf=$this->subjects->getSubinfo($subjectid);
        header("Content-type:text/x-json");
        echo json_encode($subinf);
        exit();
    }
	function getgradlevels(){
		// school/gradelevel/getlevels  sch_grade_level
        $schlevelid=$this->input->post('schlevelid');
        $gradelevel=$this->gdlevel->getgradelevels($schlevelid);
        $html = "";
        if(count($gradelevel)>0){
        	foreach ($gradelevel as $row_grade) {
        		$html .='<tr>
	                        <td>
	                        	<label><input type="checkbox" name="grade_levelid[]" id="grade_levelid" class="grade_levelid" checked value="'.$row_grade->grade_levelid.'" data-grade_levelid="'.$row_grade->grade_levelid.'">&nbsp;'.$row_grade->grade_level.'</label>
	                        </td>
	                    </tr>';     
	            //$examtype=$this->extype->examtype();            
	        	//	$html .='<tr>
	         	//                <td>
	         	//                	<label><input type="checkbox" name="grade_levelid[]" id="grade_levelid" class="grade_levelid" checked value="'.$row_grade->grade_levelid.'" data-grade_levelid="'.$row_grade->grade_levelid.'">&nbsp;'.$row_grade->grade_level.'</label>
	         	//                </td>	                        
	         	//                <td align="center">';
	         	//                	if(count($examtype)>0){
        		// 					foreach ($examtype as $row_examtype) {
										// $html .='<label><input type="checkbox" name="examtype[]" id="examtype" class="examtype" checked data-examtype="'.$row_examtype->examtypeid.'">&nbsp;'.$row_examtype->exam_test.'&nbsp;&nbsp;&nbsp;</label>';
        		// 					}
        		// 				}	
	         	//    $html .='</td></tr>';
            }
        }

        header("Content-type:text/x-json");
        echo json_encode($html);
        exit();
    }

	function getsubjecttype(){
		//school/gradelevel/getlevels  sch_grade_level
		$schlevelid=$this->input->post('schlevelid');
		$subjecttype=$this->subjects->getsubjecttype($schlevelid);
		$html = "";
		$html .='<option value="">Select Subject Group</optoin>';		
		if(count($subjecttype)>0){
			foreach ($subjecttype as $subjecttyperow) {
				$html .='<option value="'.$subjecttyperow->subj_type_id.'" maintype="'.$subjecttyperow->main_type.'" >'.$subjecttyperow->subject_type.'</option>';
			}
		}
		header("Content-type:text/x-json");
		echo json_encode($html);
		exit();
	}
    function sgetgradlevels(){
        $schlevelid=$this->input->post('schlevelid');
        $gradelevel=$this->gdlevel->getgradelevels($schlevelid);        
        $option ='<option value=""></optoin>';
        if(count($gradelevel)>0){        	
        	foreach ($gradelevel as $row_grade) {        		
	        	$option .='<option value="'.$row_grade->grade_levelid.'" >'.$row_grade->grade_level.'</option>';
            }
        }
        header("Content-type:text/x-json");
        echo json_encode($option);
        exit();
    }

    function orders(){
        $subtypeid=$this->input->post('subtypeid');
        $orders=$this->green->getValue("SELECT MAX(orders) FROM sch_subject WHERE subj_type_id='".$subtypeid."'");
        header("Content-type:text/x-json");
        echo json_encode($orders);
        exit();
    }

}