<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class semester extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('school/modsemester', 'semes');
        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolyearmodel', 'y');
        $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');
        $this->load->model('school/modfeetype', 'f');
        $this->load->model('school/modrangelevel', 'r');

        $this->load->library('pagination');
    }

    public function index(){
    	$this->load->view('header');
		$this->load->view('school/semester/index');
		$this->load->view('footer');
    }

    public function save(){
 		$i = $this->semes->save();		
		header('Content-Type: application/json; charset=utf-8');
		echo $i;

    }

    public function edit(){	
		$semesterid = $this->input->post('semesterid') - 0;
		$row = $this->semes->edit($semesterid);
		header('Content-Type: application/json; charset=utf-8');
		echo $row;		
	}

	public function delete(){	
		$semesterid = $this->input->post('semesterid') - 0;
		$i = $this->semes->delete($semesterid);
		header('Content-Type: application/json; charset=utf-8');
		echo $i;		
	}

    public function grid(){
    	$semes = $this->semes->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $semes;
    }

    public function get_schlevel(){
        $programid = $this->input->post('programid') - 0;        
        $get_schlevel = $this->semes->get_schlevel($programid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel;
    }

    public function get_year(){
        $programid = $this->input->post('programid') - 0;
        $schlevelid = $this->input->post('schlevelid') - 0;                
        $get_year = $this->semes->get_year($programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    public function get_rangelevel(){
        $schlevelid = $this->input->post('schlevelid') - 0;
        $semesterid = $this->input->post('semesterid') - 0;

        $get_rangelevel = $this->semes->get_rangelevel($schlevelid, $semesterid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_rangelevel;
    }

    // ===============
    public function get_program_search(){
        $programid_search = $this->input->post('programid_search') - 0;        
        $get_schlevel_search = $this->semes->get_schlevel($programid_search);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel_search;
    }
    public function get_year_search(){
        $programid_search = $this->input->post('programid_search') - 0;
        $schlevelid_search = $this->input->post('schlevelid_search') - 0;                
        $get_year_search = $this->semes->get_year($programid_search, $schlevelid_search);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year_search;
    }


}