<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class student_sched extends CI_Controller {
	protected $thead;
	protected $thead2;
	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/ModStudent_sch','sched');
		$this->load->library('pagination');
		$this->thead=array("No"=>'no',
							 "TeacherID"=>'employeeid',
							 "Full Name"=>'fullname',
							 "Date Of birth"=>'dob',
							 "Position"=>'position',
							 "View Schedule"=>'Action'							 	
							);
		$this->thead2=array("No"=>'no',
							 "Class Name"=>'class_name',
							 "Title"=>'title',
							 "School Level"=>'sch_level',
							 "Action"=>'Action'							 	
							);		
	}
	public function index()
	{
		
		$data['year']=$this->db->where('yearid',$this->session->userdata('year'))->get('sch_school_year')->row();
		$this->load->view('header');
		$this->load->view('school/timetable/form_stdschedule',$data);
		$this->load->view('footer');
	}
	function teachersched()
	{	
		$page=0;
		if(isset($_GET['per_page']))
				$page=$_GET['per_page'];
		$m='';
		$p='';
		if(isset($_GET['m'])){
	        $m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }	
	    $config['base_url'] =site_url()."/school/student_sched/teachersched?pg=1&m=$m&p=$p";	
		$config['total_rows'] = count($this->sched->getteacherlist());
		$config['per_page'] =50;
		$config['num_link']=5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';
		$this->pagination->initialize($config);
		$limi=" limit ".$config['per_page'];
		if($page>0){
				$limi=" limit ".$page.",".$config['per_page'];
			}
		$data['tdata']=$this->sched->getteacherlist('',$limi);
		$data['thead']=$this->thead;
		$this->load->view('header');
		$this->load->view('school/timetable/teachersched_list',$data);
		$this->load->view('footer');
	}
	function deleteclass($classid,$yearid,$transno){		
		$del=$this->db->where('year',$yearid)
				->where('classid',$classid)
				->where('transno',$transno)
				->delete('sch_time_table');

		header("Content-type:text/x-json");
		$arrJson=array();
		$arrJson['stat']=0;
		if($del){
			$arrJson['stat']=1;
		}
		echo json_encode($arrJson);
		exit();	
	    
	}
	function classlist(){

		$yearid=isset($_GET['y'])?$_GET['y']:$yearid=$this->session->userdata('year');
		
		$data['tdata']=$this->sched->getclasslist("",$yearid);
		$data['thead']=$this->thead2;
		$this->load->view('header');
		$this->load->view('school/timetable/studentsched_list',$data);
		$this->load->view('footer');
	}
	public function scheclass($classid,$yearid='',$transno='')
	{
		
		$schoolid=$this->session->userdata('schoolid');
		if($yearid!='')
			$year=$this->db->where('yearid',$yearid)->get('sch_school_year')->row();
		else
			$year=$this->db->where('yearid',$this->session->userdata('year'))->get('sch_school_year')->row();

		$data['classid']=$this->db->where('classid',$classid)->get('sch_class')->row();
		$data['year']=$year;		
		$data['transno']=$transno;
		
		$data['timetable']=$this->db->where('transno',$transno)->get('sch_time_table')->row();

		$data['modi']=$this->db->query("SELECT DISTINCT modified_by,modified_date 
	 									FROM sch_time_table 
	 									WHERE classid='$classid'
	 									AND year='$yearid'
	 									AND schoolid='$schoolid'
										AND transno='$transno'
	 									")->row();
		$this->load->view('header');
		$this->load->view('school/timetable/form_stdschedule',$data);
		$this->load->view('footer');
	}
	public function previewstdsched($classid,$yearid,$transno='')
	{		
		$data['classid']=$classid;
		$data['yearid']=$yearid;	
		$data['classinf']=$this->db->where('classid',$classid)->get('sch_class')->row();
		$data['yearinf']=$this->db->where('yearid',$yearid)->get('sch_school_year')->row();
		$data['timetable']=$this->db->where('transno',$transno)->get('sch_time_table')->row();		

		$data['transno']=$transno;

		$this->load->view('header');
		$this->load->view('school/timetable/preview_stdsched',$data);
		$this->load->view('footer');
	}

	function getteacher(){
		
		$subjectid=$this->input->post('subjectid');
		$timeid=$this->input->post('timeid');
		$dayid=$this->input->post('dayid');
		$yearid=$this->input->post('yearid');
		$classid=$this->input->post('classid');

		foreach ($this->sched->getteacher($subjectid,$yearid,$classid) as $s) {
			$ishaveclass=$this->sched->ishaveteacher($s->teacher_id,$classid,$yearid)->count;
			$is_teach_inclass=$this->sched->istchinclass($s->teacher_id,$yearid,$classid);

			$is_bc=0;
			if($this->sched->isteacherbc($s->teacher_id,$timeid,$dayid,$yearid)){
				$is_bc=$this->sched->isteacherbc($s->teacher_id,$timeid,$dayid,$yearid)->count;
			}			

			if($ishaveclass>0 && $is_bc==0){
				echo "<option value='$s->teacher_id' selected>$s->last_name $s->first_name</option>";
			}else{					
				if($is_teach_inclass>0){
					echo "<option value='$s->teacher_id'>$s->last_name $s->first_name</option>";
				}				
			}	
		}
	}
	function fillteacher(){
		$key=$_GET['term'];
		$array=array();
		foreach ($this->sched->getteacherlist($key) as $row) {
			$array[]=array('value'=>$row->last_name.' '.$row->first_name,
							'id'=>$row->empid);
		}
	    echo json_encode($array);
	}
	public function previewteach($teacherid,$yearid='')
	{
		$this->load->view('header');
		$year=$this->db->where('yearid',$yearid)->get('sch_school_year')->row();
		$data['emp']=$this->db->where('empid',$teacherid)->get('sch_emp_profile')->row();
		$data['year']=$year;
		$this->load->view('school/timetable/preview_teachersched',$data);
		$this->load->view('footer');
	}
	function save(){
		date_default_timezone_set("Asia/Bangkok");
		$timeid=$this->input->post('timeid');
		$classid=$this->input->post('s_classid');
		$schlevelid=$this->db->where('classid',$classid)->get('sch_class')->row()->schlevelid;
		$schoolid=$this->session->userdata('schoolid');
		$yearid=$this->input->post('yearid');
		$stitle=$this->input->post('stitle');
		
		$transno=$this->input->post('transno');

		if($transno!=""){
			$this->deletesch($classid,$yearid,$transno);
		}else{
			$transno=$this->green->nextTran('14',"Time Table");	
		}
		

		for ($i=0; $i < count($timeid) ; $i++) {
			$dayid=$this->input->post('dayid_'.$timeid[$i]);			
			for ($j=0; $j < count($dayid) ; $j++) { 
				$subjectid=$this->input->post('subjectid_'.$timeid[$i].'_'.$dayid[$j]);
				$teacher_id=$this->input->post('teacherid_'.$timeid[$i].'_'.$dayid[$j]);
				$data=array('dayid'=>$dayid[$j],
							'classid'=>$classid,
							'subjectid'=>$subjectid,
							'timeid'=>$timeid[$i],
							'teacherid'=>$teacher_id,
							'year'=>$yearid,
							'schlevelid'=>$schlevelid,
							'schoolid'=>$schoolid,
							'created_by'=>$this->session->userdata('userid'),
							'create_date'=>date('Y-m-d H:i:s'),
							'modified_by'=>$this->session->userdata('userid'),
							'modified_date'=>date('Y-m-d H:i:s'),
							'title'=>$stitle,
							'transno'=>$transno,
							'type'=>'14'
							);
				if($subjectid!='' || $teacher_id!='')
					$this->db->insert('sch_time_table',$data);
			}
		}
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("school/student_sched?m=$m&p=$p");
	}
	function deletesch($classid,$yearid,$transno=''){
		$schoolid=$this->session->userdata('schoolid');
		$this->db->where('classid',$classid)
				->where('year',$yearid)
				->where('schoolid',$schoolid)
				->where('transno',$transno)
				->delete('sch_time_table');
	}
	function search(){
		if(isset($_GET['n'])){
				$name=$_GET['n'];
				$empcode=$_GET['ecode'];
				$m=$_GET['m'];
				$p=$_GET['p'];
				$sort_num=$_GET['s_num'];
				$y=$_GET['y'];
				$data['tdata']=$this->sched->search($name,$empcode,$sort_num,$m,$p,$y);
				$data['thead']=$this->thead;
				$this->load->view('header');
				$this->load->view('school/timetable/teachersched_list',$data);
				$this->load->view('footer');
		}else{
				$name=$this->input->post('name');
				$empcode=$this->input->post('empcode');
				$sort_num=$this->input->post('sort_num');
				$i=1;
				$m=$this->input->post('m');
				$p=$this->input->post('p');
				$y=$this->input->post('year');

				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
			        $this->green->setActiveModule($m);
			    }
			    if($p!=''){
			        $this->green->setActivePage($p); 
			    }
				foreach ($this->sched->search($name,$empcode,$sort_num,$m,$p,$y) as $row) {
					echo "<tr>
						 	 <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
							 <td class='employeeid'>".$row->empcode."</td>
							 <td class='fullname'>".$row->last_name.' '.$row->first_name."</td>
							 <td class='dob'>".$this->green->convertSQLDate($row->dob)."</td>
							 <td class='position'>".$row->position."</td>
							 <td class='remove_tag'>";
							if($this->green->gAction("P")){	
							 	echo "<a>
							 		<img rel=".$row->empid." onclick='preview(event);' src='".site_url('../assets/images/icons/preview.png')."'/>
							 	</a>";
							}
							
							echo "</td>
					 	</tr>
					 ";
					 $i++;
				}
				//===========================show pagination=======================
						echo "<tr class='remove_tag'>
								<td colspan='12' id='pgt'>
									<div style='margin-top:20px; width:10%; float:left;'>
									Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
													
													$num=10;
													for($i=0;$i<20;$i++){?>
														<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
														<?php $num+=10;
													}
													
												echo "</select>
									</div>
									<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
										<ul class='pagination' style='text-align:center'>
										 ".$this->pagination->create_links()."
										</ul>
									</div>

								</td>
							</tr> ";
		}
		
	}
	function search_class(){
		
				$class_name=$this->input->post('class_name');
				$i=1;
				$m=$this->input->post('m');
				$p=$this->input->post('p');
				$this->green->setActiveRole($this->input->post('roleid'));
				if($m!=''){
			        $this->green->setActiveModule($m);
			    }
			    if($p!=''){
			        $this->green->setActivePage($p); 
			    }
			    $yearid=$this->input->post('y');
			    $tr="";
				foreach ($this->sched->getclasslist($class_name,$yearid) as $row) {					

					$tr.="<tr>
						 	 <td class='no'>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
							 <td class='class_name'>".$row->class_name."</td>
							 <td class='title'>".$row->title."</td>
							 <td class='sch_level'>".$row->sch_level."</td>
							 <td class='remove_tag'>";

							 $link=site_url("school/student_sched/previewstdsched/".$row->classid.'/'.$row->year.'/'.$row->transno."?m=$m&p=$p");
							 $link_edit=site_url("school/student_sched/scheclass/".$row->classid.'/'.$row->year.'/'.$row->transno."?m=$m&p=$p");
							 $link_delete=site_url("school/student_sched/deleteclass/".$row->classid.'/'.$row->year.'/'.$row->transno."?m=$m&p=$p");
							if($this->green->gAction("P")){	
							 	$tr.="<a href='".$link."' target='_blank'>
							 		<img rel=".$row->classid."  src='".site_url('../assets/images/icons/preview.png')."'/>
							 	</a>";
							}
							if($this->green->gAction("D")){	
							 	$tr.="<a class='delete' rel='".$link_delete."' href='javascript:void(0)'>
							 			<img src='".site_url('../assets/images/icons/delete.png')."'/>
							 	</a>";
							}
							if($this->green->gAction("U")){	
							 	$tr.="<a class='edit' href='".$link_edit."' target='_blank'>
							 			<img src='".site_url('../assets/images/icons/edit.png')."'/>
							 	</a>";
							}
							$tr.="</td>
							
					 	</tr>
					 ";

					 $i++;
				}
				echo $tr;
	}

	function getSched($classid,$yearid){
		$sql="SELECT DISTINCT
							sch_class.class_name,
							sch.classid,
							sch.`year`,
							sch.title,
							sch.transno,
							sch_level
						FROM
							sch_time_table sch
						INNER JOIN sch_class ON sch.classid = sch_class.classid
						INNER JOIN sch_school_level schl ON (
							sch_class.schlevelid = schl.schlevelid
						)
						WHERE sch.`year`={$yearid} 
						AND sch.classid={$classid}
						ORDER BY
							classid";
		$data=$this->green->getTable($sql);
		$op="";
		if(count($data)>0){			
			foreach ($data as $row) {
				if($row['transno']!=""){
					$op.='<option value="'.$row['transno'].'" >'.$row['title'].'</option>';
				}
				
			}			
		}
		header("Content-type:text/x-json");
		$arrJson=array();
		$arrJson['stat']=$op;
		if($op!=""){
			$arrJson['stat']=$op;
		}
		echo json_encode($arrJson);
		exit();	
	}
}