<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_score_final_mention extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("school/mod_score_final_mention","mention");
		$this->load->model('school/schoollevelmodel','schlevels');
		$this->thead=array("No"=>'no',
							"Mention"=>'mention',
							"Mentionkh"=>'mention_kh',
							"School level"=>'schlevelid',
							"Grade"=>'grade',
							"Meaning"=>'meaning',
							"Minimum Score"=>'min_score',
							"Maximum Score" => 'max_score',
							"Is Past" => '',
							"Action"=>'Action'							 	
							);
	}

	function index(){
		$data['thead']=	$this->thead;
		$data['schevels']=$this->schlevels->getsch_level();
		$this->load->view('header');
		$this->load->view('school/scoremention/v_score_final_mention_list',$data);
		$this->load->view('footer');
	}

	function s_school_level(){
	   $this->load->database();
       $this->db->select("*");
       $this->db->from("sch_school_level");
	   $this->db->where('programid',3);
       $query=$this->db->get()->result();
		$s_sch_level="";
		if (isset($schevels) && count($schevels) > 0) {
			foreach ($schevels as $sclev) {
				$s_sch_level .= '<option value="' . $sclev->schlevelid . '">' . $sclev->sch_level . '</option>';
			}
		}
		return $s_sch_level;
	}

	function save_gep(){
		$men_id = $this->input->post('men_id');
		$schlevel = $this->input->post('schlevel');
		$arr = $this->input->post('arr');
		$i = 0;

		if(COUNT($arr) > 0){
			foreach ($arr as $row){
				if($schlevel != '' || trim($row['mentionen']) != '' || trim($row['mentionkh']) != '' || trim($row['grade']) != '' || trim($row['min_score']) != '' || trim($row['max_score']) != '' || trim($row['meaning']) != '' || trim($row['is_past']) != ''){
					if($this->vaidate($row['menid'], $schlevel, $row['mentionen'], $row['mentionkh']) > 0){
						$i = 2;
						break;
					}														
				}
			}
		}

		if(COUNT($arr) > 0){
			foreach ($arr as $row){
				if($schlevel != '' || trim($row['mentionen']) != '' || trim($row['mentionkh']) != '' || trim($row['grade']) != '' || trim($row['min_score']) != '' || trim($row['max_score']) != '' || trim($row['meaning']) != '' || trim($row['is_past']) != ''){
					if($i - 0 != 2){
						$data = array('mention' => $row['mentionen'],
									  	'mention_kh' => quotes_to_entities($row['mentionkh']),
										'schlevelid' => $schlevel,
										'grade'=>$row['grade'],										
										'meaning'=> quotes_to_entities($row['meaning']),
										'min_score'=>$row['min_score'],
										'max_score'=>$row['max_score'],
										'is_past'=>$row['is_past']
									);

						if($row['menid'] - 0 > 0){
							$i = $this->db->update('sch_score_mention_gep', $data, array('menid' => $row['menid']));
						}else{
							$i = $this->db->insert('sch_score_mention_gep', $data);
						}
					}														
				}
			}
		}

		header('Content-Type: application/json; charset=utf-8');
    	echo json_encode($i);
	}

	function edit(){
		$men_id=$this->input->post('menid');
		$row=$this->db->where('menid',$men_id)->get('sch_score_mention_gep')->row();
		header("Content-type:text/x-json");
		echo json_encode($row);
	}

	function vaidate($menid, $schlevelid, $mention, $mention_kh){
        $where = '';
        if($menid - 0 > 0){
            $where .= "AND m.menid != '{$menid}' ";
        }
        
        $c = $this->db->query("SELECT
                                    COUNT(*) AS c
                                FROM
                                    sch_score_mention_gep AS m
                                WHERE
                                    	m.schlevelid 	= '{$schlevelid}' 
                                    AND m.mention 		= '{$mention}' 
                                    AND m.mention_kh 	= '{$mention_kh}'
                                    {$where} ")->row()->c - 0;
        return $c;
    }

    function delete_gep(){
    	echo $this->mention->delete_gep();
    }

	function getdata_mention(){
		$table='';
		$i=1;
		$sql  = $this->mention->getdata_mention();

		foreach($this->db->query($sql['sql'])->result() as $row){
			$past = "";
			if ($row->is_past ==1 ) {
				 //$past="checked='checked'";
				$past="<img src='".base_url('assets/images/checked.png')."'/>";	
			}else{
				$past="<img src='".base_url('assets/images/unchecked.png')."'/>";	
			}

			$table.= "<tr>
				<td class='No'>".$i."</td>
				<td class='mention'>".$row->mention."</td>
				<td class='mention_kh'>".$row->mention_kh."</td>
				<td class='schlevelid'>".$row->sch_level."</td>
				<td class='grade'>".$row->grade."</td>
				<td class='meaning'>".$row->meaning."</td>
				<td class='min_score'>".$row->min_score."</td>
				<td class='max_score'>".$row->max_score." </td>
				<td>".$past."</td>				 
				<td class='remove_tag'>
				<a><img rel='".$row->menid."' onclick='deletes(event);' src='".base_url('assets/images/icons/delete.png')."'/></a>
				<a><img rel='".$row->menid."' onclick='update(event);'  src='".base_url('assets/images/icons/edit.png')."'/></a>
				</td></tr>";								 
			$i++;	 
		}
		$paging = $sql['paging'];
		header("Content-type:text/x-json");
		$arr['tr_data']=$table;
		$arr['pagina']= $paging;		
		echo json_encode($arr);	

	}
}
