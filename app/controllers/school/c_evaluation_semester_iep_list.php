<?php 
	class C_evaluation_semester_iep_list extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model("school/m_evaluation_semester_iep","eva");
			$this->thead=array("No"=>'No',
							 "StudentID"=>'student_num',
							 "Full Name"=>'fullname',
							 "Gender"=>'gender',
							 "School level"=>'schlevel',
							 "Year"=>'year',
							 "Grad level"=>'grade_levelid',						 
							 "Class"=>'class_name',
							 "Score"=>'score',
							 "Preview"=>'preview'							 	
							);
		}

		function index(){
			$data['opt_program'] = $this->get_program(); 
			$data['opt_examtyp']=$this->get_examtype();
			$data['thead']=	$this->thead;
			$this->load->view('header');
			$this->load->view('school/scoreiep/v_iep_entryed_order_list',$data);
			$this->load->view('footer');
		}

		function get_program(){
			$optionprogram="";
			$query = $this->db->get('sch_school_program')->result();
			foreach($query as $pro_id ){
				$optionprogram .='<option value="'.$pro_id->programid.'">'.$pro_id->program.'</option>';
			}
			return $optionprogram;		
		}

		function school_level(){
			$school_level="";
			$option_level="";
			$program = $this->input->post('program');
			foreach($this->eva->getsch_level($program) as $id ){
				$option_level .='<option value="'.$id->schlevelid.'">'.$id->sch_level.'</option>';
			}
			header("Content-type:text/x-json");
			$arr_success['schlevel'] = $option_level;
			echo json_encode($arr_success);
		}

		function get_years(){
			$schoolid = $this->session->userdata('schoolid');	
			$sch_program = $this->input->post("program");
			$schlevelid = $this->input->post("sch_level");
			$option_year ='';
			$option_year .='<option value=""></option>';
			foreach ($this->eva->getschoolyear($sch_program,$schoolid,$schlevelid) as $row){

				$option_year .='<option value="'.$row->yearid.'">'.$row->sch_year.'</option>';
			}		
			header("Content-type:text/x-json");
			$arr_success['schyear'] = $option_year;
			echo json_encode($arr_success);
			
		}

		function get_gradlevel(){
			$grad_level="";
			$schoolid = $this->session->userdata('schoolid');	
			$programid = $this->input->post("program");
			$schlevelid = $this->input->post("sch_level");
			$grad_level .='<option value=""></option>';
			foreach($this->eva->getgradelevel($schoolid,$programid,$schlevelid) as $id ){
				$grad_level .='<option value="'.$id->grade_levelid.'">'.$id->grade_level.'</option>';
			}
			header("Content-type:text/x-json");
			$arr_success['gradlevel'] = $grad_level;
			echo json_encode($arr_success);
		}

		function get_class(){
			$schoolid = $this->session->userdata('schoolid');	
			$grad_level = $this->input->post("grad_level");
			$schlevelid = $this->input->post("sch_level");
			$class="";
			$class .='<option value=""></option>';
			foreach($this->eva->get_class($schoolid,$grad_level,$schlevelid) as $id ){			
				$class .='<option value="'.$id->classid.'">'.$id->class_name.'</option>';
			}
			header("Content-type:text/x-json");
			$arr_success['getclass'] = $class;
			echo json_encode($arr_success);  
		}

		function get_examtype(){
			$examtype="";
			$query=$this->db->get('sch_student_examtype')->result();
			foreach($query as $id ){

				$examtype .='<option value="'.$id->examtypeid.'">'.$id->exam_test.'</option>';
			}
			return $examtype;
		}

		function show_student(){
			echo $this->eva->show_student();
		}

		function show_list(){
			$table='';
		$i=1;
		$sql  = $this->eva->show_list();
		foreach($this->db->query($sql['sql'])->result() as $row){
			$main = $row->schoolid.'_'.$row->schlevelid.'_'.$row->yearid.'_'.$row->grade_levelid.'_'.$row->semesterid.'_'.$row->classid;			
			$programid = $row->programid;	
			$table .= '<tr>
							<td>'.str_pad($i,2,"0",STR_PAD_LEFT).'</td>
							<td class="student_num">'.$row->student_num.'</td>
							<td class="fullname">
								<span>'.$row->fullname.'</span><br>
								<span>'.$row->fullname_kh.'</span>
							</td>
							<td class="gender">'.ucwords($row->gender).'</td>
							<td class="sch_level">'.$row->sch_level.'</td>
							<td class="year">'.$row->sch_year.'</td>
							<td class="grad_level">'.$row->grade_level.'</td>
							<td class="class_name">'.$row->class_name.'</td>
							<td></td>
							<td class="remove_tag"style="text-align: center;">
								<a><img rel='.$row->studentid.' main='.$main.' p_id='.$programid.' onclick="add(event);" src="'.base_url('assets/images/icons/add.png').'"/></a>
								<a href="'.site_url('school/c_evaluation_semester_iep/index/?id='.$row->studentid.'&main='.$main.' &p_id='.$programid.'').'"><img src='.base_url('assets/images/icons/a_preview.png').'>
								</a>
							</td>
						</tr>';							 
			$i++;	 
		}
		$paging = $sql['paging'];
		header("Content-type:text/x-json");
		$arr['tr_data']=$table;
		$arr['pagina']= $paging;		
		echo json_encode($arr);	
		}

	}
?>