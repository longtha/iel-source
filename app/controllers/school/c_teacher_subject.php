<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_teacher_subject extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/m_teacher_subject','tclass');
		$this->load->library('pagination');		
	}
	public function index()
	{
		
		$yearid=$this->session->userdata('year');
		$data['schlevelid']="";

		//$yearid=$this->input->post('year');
		$data_count = $this->tclass->getteacher('',$yearid);
		$table_list = $this->tclass->show_data();
		$page=0;
		if(isset($_GET['per_page']))
			$page=$_GET['per_page'];
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }

		// $config['base_url']=site_url("school/tearcherclass/index?m=$m&p=$p");
		// $config['page_query_string'] = TRUE;
		// $config['per_page']=50;
		// $config['num_link']=3;
		// $config['full_tag_open'] = '<li>';
		// $config['full_tag_close'] = '</li>';
		// $config['cur_tag_open'] = '<a><span>';
		// $config['cur_tag_close'] = '</span></a>';
		// $config['total_rows']=count($data_count);
		// $this->pagination->initialize($config);
		// $limi=" limit ".$config['per_page'];
		// if($page>0){
		// 	$limi=" limit ".$page.",".$config['per_page'];
		// }	
		// $data['query']      = $this->tclass->getteacher('',$yearid,'',$limi);
		$data['yearid']     = $yearid;
		$data['table_list'] = $table_list;
		$this->load->view('header');
		$this->load->view('school/teachersubject/add');
		$this->load->view('school/teachersubject/view',$data);
		$this->load->view('footer');
	}
	// ================= sophorn ===============
	function get_grandLavel(){
		$sql_grandlavel= $this->tclass->getGrandOpt();
		$get_arr['arr_class'] = $sql_grandlavel['grandclass'];
		$get_arr['arr_subj'] = $sql_grandlavel['subject'];
		$get_arr['ch_classhandle'] = $sql_grandlavel['check_h'];
		echo json_encode($get_arr);
	}
	function grandOption(){
		$sql_grandlavel= $this->tclass->grandOpt();
		$grand_arr['arr_gran'] = $sql_grandlavel;
		echo json_encode($grand_arr);
	}
	function save_handle_assign_subject(){
		$para_save = $this->tclass->save_data();
		$table_list = $this->tclass->show_data();
		$arr['typeno'] = $para_save;
		$arr['list']   = $table_list;
		echo json_encode($arr);
	}
	function delete_record(){
		$typeno = $this->input->post("typeno");
		$this->db->delete("sch_teacher_summary",array("tch_transno"=>$typeno));
		$this->db->delete("sch_teacher_class",array("transno"=>$typeno));
		$this->db->delete("sch_teacher_classhandle",array("transno"=>$typeno));
		$this->db->delete("sch_teacher_subject",array("transno"=>$typeno));
		$parth_pic = "./assets/upload/teacher/signature/".$typeno.".jpg";
		if(file_exists($parth_pic)){
			unlink('./assets/upload/teacher/signature/'.$typeno.'.jpg');
		}
		$table_list = $this->tclass->show_data();
		echo $table_list;
	}
	// ================= end sophorn ===========

	function addnew()
	{
		$this->load->view('header');
		$this->load->view('school/teachersubject/add');
		$this->load->view('footer');	
	}

	function uploate_save(){
			
			if(isset($_FILES['userfile']['name']) OR isset($_POST['htypeno_img']))
			{
				$teacherid = $_POST['htypeno_img'];
				$upload_file = true;
				$arr_name = explode("##", $teacherid);
				if(count($arr_name) > 0){
					for($i=0;$i<=count($arr_name);$i++){
						// if ($_FILES['userfile']['size'][$i] > (120 * 180)) {
						// 	//$upload_img = 'The file size is over the maximum allowed. The maximum size allowed in KB is ' . $_SESSION['MaxImageSize'].'.';
						// 	$upload_file = false;
						// }
						// if ($_FILES['userfile']['type'][$i] == "text/plain") {
						// 	$upload_img = 'Only graphics files can be uploaded';
						// 	$upload_file = false;
						// }		
						if($upload_file){
							$path = "./assets/upload/teacher/signature/".$arr_name[$i].".jpg";
							move_uploaded_file($_FILES['userfile']['tmp_name'][$i],$path);
						}
					}
				}
				//echo print_r($arr_name);
			}
		die();
		//}
		

	}

	//---- autocomplete -----
	
	//---- end autocomplete -----
	
	//---------- save & update use the same function --------
	

	//------ upate image signature ------------ 
	function do_upload($id)
	{
		$config['upload_path'] ='./assets/upload/teacher/signature/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name']  = "$id.jpg";
		$config['overwrite']=true;
		$config['file_type']='image/jpg';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());			
		}
		else
		{				
			//$data = array('upload_data' => $this->upload->data());			
			$config2['image_library'] = 'gd2';
            $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
            $config2['new_image'] = './assets/upload/teacher/signature/';
            $config2['maintain_ratio'] = true;
            $config2['create_thumb'] = true;
            $config2['thumb_marker'] = false;
            $config2['width'] = 120;
            $config2['height'] = 180;
            $config2['overwrite']=true;
            $this->load->library('image_lib',$config2);

            if ( !$this->image_lib->resize()){
            	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
			}else{
				//unlink('./assets/upload/teacher/signature/'.$id.'.jpg');
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("school/tearcherclass/?m=$m&p=$p");
			}	
		}
	}
	//------ end upate image signature ------------

	// function preview(){
	// 	$this->load->view('header');		

	// 	// $data['query3']=$this->tclass->getschoolyearrow($this->session->userdata('year'));
	// 	// $data['query4']=$this->tclass->getschoolrow($this->session->userdata('schoolid'));
	// 	// $data['query']=$this->tclass->getteacher($teacherid,$yearid,$schlevelid);
	// 	// $data['yearid']=$yearid;
	// 	//$table_list = $this->tclass->show_data();
	// 	//$data['list_tbl'] = $table_list;
	// 	$teacher = $this->input->GET("teacher");
	// 	$p 		 = $this->input->GET("p");
	// 	$m 		 = $this->input->GET("m");
	// 	$data['teacher'] = $teacher;
	// 	$data['p'] = $p;
	// 	$data['m'] = $m;
	// 	$this->load->view('school/teachersubject/preview',$data);
		
	// 	$this->load->view('footer');
	// }
	function seach(){
		$table_list = $this->tclass->show_data();
		echo $table_list;
	}
	function fillteacher(){
		header("Content-type:text/x-json");
		$key   =  $_GET['term'];
		$where = "";
		if($key != ""){
			$where = " AND CONCAT(sep.first_name,' ',sep.last_name) LIKE '%".$key."%'";
		}
		$sql_seachteacher = $this->db->query("SELECT
												sep.empid,
												sep.empcode,
												CONCAT(sep.first_name,' ',sep.last_name) as fullname,
												sepo.match_con_posid
											FROM sch_emp_profile as sep 
											INNER JOIN sch_emp_position as sepo ON sep.pos_id=sepo.posid
											WHERE 1=1 AND sepo.match_con_posid='tch' {$where}");
		$arr_search = array();
		if($sql_seachteacher->num_rows() > 0){
			foreach($sql_seachteacher->result() as $row) {
				$arr_search[]=array('value'=>$row->fullname,
					  		        'id'=>$row->empcode);
			}
		}	
		echo json_encode($arr_search);
		die();
	}
	
}