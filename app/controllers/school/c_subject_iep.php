<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_subject_iep extends CI_Controller {

	function __construct()
	{
		parent::__construct();			
		$this->load->model('school/subjectmodel_iep','subjects');
        $this->load->model('school/schoolinformodel','sch');
        $this->load->model('school/schoolyearmodel','schyear');
        $this->load->model('school/schoollevelmodel','schlevels');
        $this->load->model('school/gradelevelmodel','gdlevel');
        $this->load->model('school/modsetupexam','extype');
		$this->load->library('pagination');		
	}
	public function index()
	{
        $data['schyears']=$this->schyear->getschoolyear();
        $data['schevels']=$this->schlevels->getsch_level("2");        
        $data['gradelevels']=$this->gdlevel->getgradelevels();
		$this->load->view('header');
		$this->load->view('school/subject_iep/add',$data);
		$data['query']=$this->subjects->getpagination();
		$this->load->view('school/subject_iep/view',$data);
		$this->load->view('footer');
	}
	function addnew()
	{
		$this->load->view('header');
		//school/subjecttype is call view
		$this->load->view('school/subject/subject_iep/add');
		$this->load->view('footer');	
	}
	function  save(){
        //$subjectid=$this->input->post('subjectid');
        $save = $this->subjects->save();
        header("Content-type:text/x-json");
		$arrJson["res"]=$save;
        echo json_encode($arrJson);
        exit();
    }
	
	function editsubject()
	{
		$subjectid = $this->input->GET("subjid");
		$schlevelid = $this->input->GET("schlevelid");
		$groupsubjid = $this->input->GET("groupsubj");
		$row       = $this->subjects->getsubjectrow($subjectid);
		$data['sql_edit'] = $row;
		$data['schyears'] = $this->schyear->getschoolyear();
        $data['schevels'] = $this->schlevels->getsch_level("2");        
        $data['gradelevels'] = $this->gdlevel->getgradelevels();
		$data['groupsubject'] = $this->subjects->getsubjecttype($schlevelid);
		$data['groupsubjectid'] = $groupsubjid;
        $this->load->view('header');
		$this->load->view('school/subject_iep/add',$data);
		$data['query'] = $this->subjects->getpagination();
		$this->load->view('school/subject_iep/view',$data);
		$this->load->view('footer');	
	}
	function updatesubject()
	{			
        $update = $this->subjects->updatesubject();
        header("Content-type:text/x-json");
		$arrJson["res"]=$update;
        echo json_encode($arrJson);
        exit();
	}
	
	function search(){
			$subject= $this->input->post('subject');
			$subjecttype = $this->input->post('subjecttype');
			$shortcut = $this->input->post('shortcut');
			$schlevelid = $this->input->post('schlevelid');
			$is_assessment = $this->input->post('is_assessment');
		  
			$m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			$sk=$this->input->post('sk');

			if($m!=''){
			    $this->green->setActiveModule($m);
			}
			if($p!=''){
				$this->green->setActivePage($p); 
			}
			$query=$this->subjects->searchsubjects($subject,$subjecttype,$shortcut,$schlevelid,$is_assessment,$m,$p);
			$i=1;
			$tr = "";
			if(count($query)>0){
				$arrSubjid = array();
				$arrGrade = array();
				foreach($query as $sub_row){
			        $trim='';
			        $is_eval='';
			        $tr.="<tr>
				            <td align=center width=40>". (!in_array($sub_row->subjectid,$arrSubjid)?$i:'') ."</td>
							<td>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->sch_level:'') ."</td>	
							<td width=170>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->subject_type:'') ."</td>
				            <td width=170>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->subject:'') ."</td>
				            <td>". (!in_array($sub_row->subjectid,$arrSubjid)?$sub_row->short_sub:'') ."</td>
				            <td style='text-align:center;'>".($sub_row->is_assessment==1?"Yes":"No")."</td>
				            <td style='text-align:center;'>".$sub_row->max_score."</td>
				            <td align=center width=130>
				                <a  class='update_row' href='".site_url('school/c_subject_iep/editsubject?subjid='.$sub_row->subjectid)."&schlevelid=".$sub_row->schlevelid."&groupsubj=".$sub_row->subj_type_id."&m=$m&p=$p'><img src='".site_url('../assets/images/icons/edit.png')."' /></a> |
				                <a  class='del_row' rel='$sub_row->subjectid'><img src='".site_url('../assets/images/icons/delete.png')."' /></a> 
				            </td>
				          </tr>" ;
			        $i++;
			        $arrSubjid[] = $sub_row->subjectid;
			    }
		    }else{
		      $tr='<tr>
		              <td colspan="8" style="text-align:center;">
		                  <h4><i>No result</i></h4>
		              </td>
		            </tr>';
		    }
			$tr.="<tr>
					<td colspan='11' id='pgt'>
						<div style='text-align:left'>
							<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
						</div></td>
			   	</tr>";
			echo $tr;
	}
   
    function delete()
    {
        $arrJson=array();
        $subjectid=$this->input->post('subjectid');
        $this->db->where('subjectid', $subjectid);
        $this->db->delete('sch_subject_iep');
        
        if($this->db->_error_number()==0){
            $arrJson['res']=1;
        };	
        header("Content-type:text/x-json");
        echo json_encode($arrJson);
        exit();
    }
    function deletesubject()
	{
        $subjectid=$this->input->post('subjectid');
        $deleted=$this->subjects->deletesubject($subjectid);
        header("Content-type:text/x-json");
        $arr_del['del'] = $deleted;
        echo json_encode($arr_del);
        exit();
	}
    function select(){
        $subjectid=$this->input->post('subjectid');
        $subinf=array();
        $subinf=$this->subjects->getSubinfo($subjectid);
        header("Content-type:text/x-json");
        echo json_encode($subinf);
        exit();
    }
	function getgradlevels(){
    }
	
	function getsubjecttype(){
		//school/gradelevel/getlevels  sch_grade_level
		$schlevelid=$this->input->post('schlevelid');
		$subjecttype=$this->subjects->getsubjecttype($schlevelid);
		$html = "";
		$html .='<option value="">Select Subject Group</optoin>';
		if(count($subjecttype)>0){
			foreach ($subjecttype as $subjecttyperow) {
				$html .='<option value="'.$subjecttyperow->subj_type_id.'" maintype="'.$subjecttyperow->main_type.'" >'.$subjecttyperow->subject_type.'</option>';
			}
		}
		header("Content-type:text/x-json");
		echo json_encode($html);
		exit();
	
	}
	
    function sgetgradlevels(){
        $schlevelid=$this->input->post('schlevelid');
        $gradelevel=$this->gdlevel->getgradelevels($schlevelid);        
        $option ='<option value=""></optoin>';
        if(count($gradelevel)>0){        	
        	foreach ($gradelevel as $row_grade) {        		
	        	$option .='<option value="'.$row_grade->grade_levelid.'" >'.$row_grade->grade_level.'</option>';
            }
        }
        header("Content-type:text/x-json");
        echo json_encode($option);
        exit();
    }

    function orders(){
        $subtypeid=$this->input->post('subtypeid');
        $orders=$this->green->getValue("SELECT MAX(orders) FROM sch_subject WHERE subj_type_id='".$subtypeid."'");
        header("Content-type:text/x-json");
        echo json_encode($orders);
        exit();
    }
	function searchsubject(){
		$groupsubjid = $this->input->post("groupsubjid");
		$this->db->where("subj_type_id",$groupsubjid);
		$sql_subject = $this->db->get("sch_subject_iep");
		$opt_s = "<option value=''></option>";
		if($sql_subject->num_rows() > 0){
			foreach($sql_subject->result() as $row_s){
				$opt_s.='<option value="'.$row_s->subjectid.'">'.$row_s->subject.'</option>';
			}
		}
		 header("Content-type:text/x-json");
        echo json_encode($opt_s);
        exit();
	}
}