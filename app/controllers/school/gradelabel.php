<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class gradelabel extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('school/gradelabelmodel', 'gradelabels');
        $this->load->model('school/schoollevelmodel', 'schlevels');
        $this->load->library('pagination');
    }

    public function index()
    {
        $this->load->view('header');
        //school/schoolinfor is call view

        //for pagination is use $data, if not use $data
        $data['query'] = $this->gradelabels->getpagination();
        $data['schevels'] = $this->schlevels->getsch_level();
        $this->load->view('school/gradelabel/add', $data);
        $this->load->view('school/gradelabel/view', $data);
        $this->load->view('footer');
    }

    function addnew()
    {
        $this->load->view('header');
        //school/schoolinfor is call view
        $this->load->view('school/gradelabel/gradelabel/add');
        $this->load->view('footer');
    }

    function saves()
    {
        $schoolid = $this->input->post('schoolid');
        $glabel = $this->input->post('glabel');
        $sclevelid = $this->input->post('sclevelid');
        $programid = $this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='" . $sclevelid . "'")->row()->programid;

        $count = $this->gradelabels->getvalidate($schoolid, $glabel,$sclevelid);

        if ($count != 0) {
            echo 'false';
        } else {

            $data = array(
                'schoolid' => $schoolid,
                'grade_label' => $glabel,
                'schlevelid' => $sclevelid,
                'programid' => $programid
            );
            $this->db->insert('sch_grade_label', $data);

            echo 'true';
        }
    }

    function edits($gradelabel_id)
    {
        $row = $this->gradelabels->getgradelabel_row($gradelabel_id);
        $data['schevels'] = $this->schlevels->getsch_level();
        $this->load->view('header');
        $data['query'] = $row;
        //school/gradelevel is call view
        //$data1['query']=$this->db->get('sch_subject')->result();
        $this->load->view('school/gradelabel/edit', $data);
        $data1['query'] = $this->gradelabels->getpagination();
        $this->load->view('school/gradelabel/view', $data1);
        $this->load->view('footer');

    }

    function updates()
    {
        $glabelid = $this->input->post('glabelid');
        $schoolid = $this->input->post('schoolid');
        $glabel = $this->input->post('glabel');
        $sclevelid = $this->input->post('sclevelid');
        $count = $this->gradelabels->getvalidateup($glabel, $glabelid, $schoolid,$sclevelid);

        $programid = $this->db->query("SELECT programid FROM sch_school_level WHERE schlevelid ='" . $sclevelid . "'")->row()->programid;
        if ($count != 0) {
            echo 'false';
        } else {
            $this->db->where('grade_labelid', $glabelid);

            $data = array(
                'grade_label' => $glabel,
                'schoolid' => $schoolid,
                'schlevelid' => $sclevelid,
                'programid' => $programid
            );
            $this->db->update('sch_grade_label', $data);

            echo 'true';
        }
    }

    function deletes($grade_labelid)
    {
        $m = '';
        $p = '';
        if (isset($_GET['m'])) {
            $m = $_GET['m'];
        }
        if (isset($_GET['p'])) {
            $p = $_GET['p'];
        }

        $this->db->where('grade_labelid', $grade_labelid);
        $this->db->delete('sch_grade_label');
        //--- delete is variable for message ---
        redirect("school/gradelabel/?delete&m=$m&p=$p");
    }

    function searchs()
    {
        //----------- Pagination -----------
        if (isset($_GET['s'])) {
            $y = $_GET['y'];
            //$sch=$_GET['sch'];
            $data['query'] = $this->gradelabels->searchgrade_label($y);

            $this->load->view('header');
            $this->load->view('school/gradelabel/add');
            $this->load->view('school/gradelabel/view', $data);
            $this->load->view('footer');
        }

        if (!isset($_GET['s'])) {
            $schoolid = $this->input->post('schoolid');

            $m = $this->input->post('m');
            $p = $this->input->post('p');
            $this->green->setActiveRole($this->input->post('roleid'));
            if ($m != '') {
                $this->green->setActiveModule($m);
            }
            if ($p != '') {
                $this->green->setActivePage($p);
            }

            $query = $this->gradelabels->searchgrade_label($schoolid);
            $i = 1;
            foreach ($query as $row) {
                echo "
			        <tr>
			          <td align=center >$i</td>
			          <td>$row->name</td>
			          <td>$row->program</td>
			          <td>$row->sch_level</td>
			          <td>$row->grade_label</td>
			          <td align=center width=130>";
                if ($this->green->gAction("U")) {
                    echo "<a href='" . site_url('school/gradelabel/edits/' . $row->grade_labelid) . "?m=$m&p=$p" . "'>
			              <img src='" . site_url('../assets/images/icons/edit.png') . "' />
			              </a> |";
                }
                if ($this->green->gAction("D")) {
                    echo "<a><img onclick='deletegradelabel(event);' rel='$row->grade_labelid' src='" . site_url('../assets/images/icons/delete.png') . "' />
			              </a>";
                }
                echo "</td>
			        </tr>
			        ";
                $i++;
            }
            //	  echo "<tr>
            //   			<td colspan='12' id='pgt'>
            //   				<div style='text-align:left'>
            //   					<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
            //   				</div></td>
            //  		   </tr>";
        }
    }
    function getlabels($schlevelid){
        $arr=array();
        $data=$this->gradelabels->getgradelabels($schlevelid);
        header("Content-type:text/x-json");
        echo json_encode($data);
        exit();
    }
}