<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class input_score_class_iep extends CI_Controller {
	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/tearcherclassmodel','tclass');
		$this->load->library('pagination');
	}
	public function index()
	{
		
		//$yearid=$this->session->userdata('year');
		//$data_count = $this->tclass->getteacher('',$yearid);
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }

		//$data['yearid'] = $this->select_y();
		$data['schoolid'] = $this->select_sch();
		$data['schoolid_lav'] = $this->select_sch_lav();
		$data['exam_type'] = $this->exam_type($id="");
		$this->load->view('header');
		$active_user = $this->green->getActiveUser();
		$data['user_act'] = $active_user;
		$this->load->view('school/score/vinput_score_class_iep',$data);
		$this->load->view('footer');
	}
	function show_report(){
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		$data['schoolid'] = $this->select_sch();
		$data['schoolid_lav'] = $this->select_sch_lav();
		$data['exam_type'] = $this->exam_type($id="");
		$this->load->view('header');
		$this->load->view('school/score/vreport_input_score_iep',$data);
		$this->load->view('footer');
	}
	function show_report_midterm(){
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		$data['schoolid'] = $this->select_sch();
		$data['schoolid_lav'] = $this->select_sch_lav();
		$data['exam_type'] = $this->exam_type($id=1);
		$this->load->view('header');
		$this->load->view('school/score/vreport_mid_term_iep',$data);
		$this->load->view('footer');
	}
	function show_report_term(){
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		$data['schoolid'] = $this->select_sch();
		$data['schoolid_lav'] = $this->select_sch_lav();
		$data['exam_type'] = $this->exam_type($id="");
		$this->load->view('header');
		$this->load->view('school/score/vreport_term_result_iep',$data);
		$this->load->view('footer');
	}
	function select_y(){
		$para_obj  = $this->input->post("para_obj");
		$sql_y = $this->db->query("SELECT
									sch_school_year.yearid,
									sch_school_year.sch_year,
									CONCAT(YEAR(STR_TO_DATE(from_date,'%Y')),'-',YEAR(STR_TO_DATE(to_date,'%Y'))) as st_y
									FROM
									sch_school_year
									WHERE 1=1 AND schlevelid='".$para_obj['schoollavel']."'
									");
		$opt_y = "<option value=''></option>";
		if($sql_y->num_rows() > 0){
			foreach($sql_y->result() as $row_y){
				$opt_y.="<option value='".$row_y->yearid."' st_to_date='".$row_y->st_y."'>".$row_y->sch_year."</option>";
			}
		}
		echo $opt_y;
	}
	function select_sch(){
		$sql_sch = $this->db->query("SELECT
										sch_school_infor.schoolid,
										sch_school_infor.`name`
										FROM
										sch_school_infor
									");
		$opt_sch = "";
		if($sql_sch->num_rows() > 0){
			foreach($sql_sch->result() as $row_sch){
				$opt_sch.="<option value='".$row_sch->schoolid."'>".$row_sch->name."</option>";
			}
		}
		return $opt_sch;
	}
	function select_sch_lav(){
		$sql_sch_schlav = $this->db->query("SELECT
										sch_school_level.schlevelid,
										sch_school_level.sch_level,
										sch_school_level.schoolid
										FROM
										sch_school_level
										WHERE programid=2
										");
		$opt_sch_lav = "<option value=''></option>";
		if($sql_sch_schlav->num_rows() > 0){
			foreach($sql_sch_schlav->result() as $row_sch_l){
				$opt_sch_lav.="<option value='".$row_sch_l->schlevelid."'>".$row_sch_l->sch_level."</option>";
			}
		}
		return $opt_sch_lav;
	}
	function grade_lavel(){
		$para_obj        = $this->input->post("para_obj");
		$sql_gradelav = $this->db->query("SELECT
											sch_grade_level.grade_levelid,
											sch_grade_level.grade_level,
											sch_grade_level.schoolid,
											sch_grade_level.next_grade_level,
											sch_grade_level.schlevelid
											FROM
											sch_grade_level
											WHERE 1=1 AND schoolid='".$para_obj['schoolid']."' 
											AND schlevelid='".$para_obj['schoollavel']."'
										");
		$opt_grade_lavel = "<option value=''></option>";
		if($sql_gradelav->num_rows() > 0){
			foreach($sql_gradelav->result() as $row_gr){
				$opt_grade_lavel.="<option value='".$row_gr->grade_levelid."' att_grandlavel='".$row_gr->grade_level."'>".$row_gr->grade_level."</option>";
			}
		}
		echo $opt_grade_lavel;
	}
	function grade_class(){
		$para_obj   = $this->input->post("para_obj"); 
		$sql_class = $this->db->query("SELECT
											sch_class.classid,
											sch_class.class_name,
											sch_class.grade_levelid,
											sch_class.grade_labelid,
											sch_class.schoolid,
											sch_class.schlevelid,
											sch_class.is_pt
											FROM
											sch_class
											WHERE 1=1 AND schoolid='".$para_obj['schoolid']."' 
											AND schlevelid='".$para_obj['schoollavel']."'
											AND grade_levelid='".$para_obj['gradelavel']."'
										");
		$opt_class = "<option value=''></option>";
		if($sql_class->num_rows() > 0){
			foreach($sql_class->result() as $row_class){
				$opt_class.="<option value='".$row_class->classid."' att_class='".$row_class->class_name."' is_pt='".$row_class->is_pt."'>".$row_class->class_name."</option>";
			}
		}
		echo $opt_class;
	}
	function exam_type($id){
		$wh = "";
		if($id != ""){
			$wh = " AND examtypeid=1";
		}
		$sql_exam_type = $this->db->query("SELECT
												sch_student_examtype_iep.examtypeid,
												sch_student_examtype_iep.exam_test,
												sch_student_examtype_iep.maintype,
												sch_student_examtype_iep.is_assissment
												FROM
												sch_student_examtype_iep
												WHERE 1=1 {$wh}
										");
		$opt_exam = "<option value=''></option>";
		if($sql_exam_type->num_rows() > 0){
			foreach($sql_exam_type->result() as $row_exam){
				$opt_exam.="<option value='".$row_exam->examtypeid."' att_label='".$row_exam->exam_test."' is_assigment='".$row_exam->is_assissment."'>".$row_exam->exam_test."</option>";
			}
		}
		return $opt_exam;
	}
	function option_week(){
		$termid   = $this->input->post("termid");
		$examid   = $this->input->post("examid");
		$sql_week = $this->db->query("SELECT
										sch_school_termweek.weektermid,
										sch_school_termweek.termid,
										sch_school_termweek.weekid,
										sch_z_week.`week`
										FROM
										sch_school_termweek
										INNER JOIN sch_z_week ON sch_school_termweek.weekid = sch_z_week.weekid
										WHERE 1=1 
										AND sch_school_termweek.termid='".$termid."'
										AND sch_school_termweek.examtypeid='".$examid."'
										ORDER BY weekid ASC");
		$option_w = "";
		if($sql_week->num_rows() > 0){
			foreach($sql_week->result() as $row_w){
				$option_w.='<option value="'.$row_w->weekid.'">'.$row_w->week.'</option>';
			}
		}
		echo $option_w;
	}
	function sub_exam_type(){
		$para_obj     = $this->input->post("para_obj"); 
		$sql_sub_exam = $this->db->query("SELECT
											sch_school_term.termid,
											sch_school_term.term,
											sch_school_term.term_kh,
											sch_school_term.programid,
											sch_school_term.yearid,
											sch_school_term.schoolid,
											sch_school_term.schlevelid,
											sch_school_term.semesterid,
											sch_school_term.start_date,
											sch_school_term.end_date
											FROM
											sch_school_term
											WHERE 1=1
											AND schoolid='".$para_obj['schoolid']."' AND schlevelid='".$para_obj['schoollavel']."'
											AND yearid='".$para_obj ['yearsid']."'
										");
		$opt_sub_exam = "<option value=''></option>";
		if($sql_sub_exam->num_rows() > 0){
			foreach($sql_sub_exam->result() as $row_subexam){
				$opt_sub_exam.="<option value='".$row_subexam->termid."' label_term='".$row_subexam->term."' fdate='".($this->green->convertSQLDate($row_subexam->start_date))."' tdate='".$this->green->convertSQLDate($row_subexam->end_date)."'>".$row_subexam->term."</option>";
			}
		}
		echo $opt_sub_exam;
	}
	function subject_group(){
		$para_obj = $this->input->post("para_obj"); 
		$display_r = $this->input->post("display_r"); 
		$is_assigment = $this->input->post("is_assigment");
		$w_diplay = "";
		if($display_r == 2){
			$w_diplay.=" AND is_class_participation in(2,4)";
		}
		$sql_sub  = $this->db->query("SELECT
									sch_subject_type_iep.subj_type_id,
									sch_subject_type_iep.subject_type,
									sch_subject_type_iep.is_class_participation
									FROM
									sch_subject_type_iep
									WHERE 1=1 {$w_diplay}
									AND schoolid='".$para_obj['schoolid']."' 
									AND schlevelid='".$para_obj['schoollavel']."' 
									");
		$opt_subject_group = "<option value=''></option>";
		if($sql_sub->num_rows() > 0){
			foreach($sql_sub->result() as $row_subj_g){
				$opt_subject_group.="<option value='".$row_subj_g->subj_type_id."' is_class_participation='".$row_subj_g->is_class_participation."'>".$row_subj_g->subject_type."</option>";
			}
		}
		echo $opt_subject_group;
	}
	// function fn_weekname(){
	// 	//$subject_group = $this->input->post("subject_group");
	// 	$obj_para     = $this->input->post("obj_para");
		
	// 	$sql_sub_subj  = $this->db->query("SELECT
	// 											sch_subject_iep.subjectid,
	// 											sch_subject_iep.`subject`,
	// 											sch_subject_iep.short_sub

	// 											FROM
	// 											sch_subject_iep
	// 										WHERE 1=1
	// 										AND subj_type_id='".$obj_para['subject_group']."'");
	// 	$opt_sub_subject = "<option value=''></option>";
	// 	if($sql_sub_subj->num_rows() > 0){
	// 		foreach($sql_sub_subj->result() as $row_sub_subj){
	// 			$opt_sub_subject.="<option value='".$row_sub_subj->subjectid."' att_label='".$row_sub_subj->short_sub."'>".$row_sub_subj->subject."</option>";
	// 		}
	// 	}
	// 	echo $opt_sub_subject;
	// }
	function search_studen(){
		$para_obj = $this->input->post("para_obj"); 
		//$is_assigment = $para_obj['is_assigment']; 
		$active_user = $this->input->post("user_active");
		$where = "";
		$where_d = "";
		$where.=" AND vse.schoolid='".$para_obj['schoolid'] ."'";
		$where.=" AND vse.schlevelid='".$para_obj['schoollavel']."'";
		$where.=" AND vse.year='".$para_obj['yearsid']."'";
		$where.=" AND vse.classid='".$para_obj['classid']."'";
		$where.=" AND vse.grade_levelid='".$para_obj['gradelavel']."'";

		$w_course = "";
				
		
		$sql_student = "SELECT
								vse.student_num,
								vse.first_name,
								vse.last_name,
								vse.gender,
								vse.studentid,
								vse.class_name
								FROM
								v_student_enroment AS vse
								WHERE 1=1 {$where} 
								GROUP BY vse.studentid
								ORDER BY vse.student_num ASC";
		
		
		$user_id    = $this->db->query("SELECT is_admin,emp_id FROM sch_user WHERE userid='".$active_user."'")->row();
		$w_user     = "";
		$sql_h_subj = "";
		
		if($user_id->is_admin == 0){ // check user type admin or not admin
			$w_subjid   = "";
			$w_user.= " AND sch_teacher_subject.teacher_id='".$user_id->emp_id."'";
			$w_user.= " AND sch_teacher_subject.classid='".$para_obj['classid']."'";
			$sql_h_subj = $this->db->query("SELECT
											sch_teacher_subject.subject_id as subjectid,
											sch_teacher_subject.classid,
											sch_subject_iep.max_score,
											sch_subject_iep.`subject`
											FROM
											sch_subject_iep
											INNER JOIN sch_teacher_subject ON sch_teacher_subject.subject_id = sch_subject_iep.subjectid
											WHERE 1=1 {$w_user}
											AND sch_teacher_subject.classid='".$para_obj['classid']."'
											AND sch_subject_iep.subj_type_id='".$para_obj['subject_group']."'
											");
		}else{
			
			$sql_h_subj = $this->db->query("SELECT
												sch_subject_iep.subjectid,
												sch_subject_iep.`subject`,
												sch_subject_iep.subj_type_id,
												sch_subject_iep.short_sub,
												sch_subject_iep.max_score
											FROM
												sch_subject_iep
												WHERE 1=1
												AND sch_subject_iep.subj_type_id='".$para_obj['subject_group']."'
												");
		}
		

		$tr = "";
		$i = 1;
		$h = "";
		$query_student = $this->db->query($sql_student);
		if($query_student->num_rows() > 0){
			foreach($query_student->result() as $row_st){
				$d = "";
				$typeno = 0;
				if($sql_h_subj->num_rows() > 0)
				{
					foreach($sql_h_subj->result() as $row_h_s){
						$max_score = $row_h_s->max_score;
						if($i == 1){
							$h.="<th style='text-align:center;'>".$row_h_s->subject."</th>";
						}
						$sql_score = $this->db->query("SELECT
															ssor.score_id,
															ssor.tran_date,
															ssor.typeno,
															ssgd.score_num,
															ssgd.studentid,
															ssgd.subjectid
														FROM
														sch_score_iep_entryed_order as ssor
														INNER JOIN sch_score_iep_entryed_detail as ssgd ON ssor.typeno = ssgd.typeno
														WHERE 1=1{$where_d}
														AND ssgd.subjectid='".$row_h_s->subjectid."'
														AND ssgd.studentid = '".$row_st->studentid."'
														AND ssor.weekid = '".$para_obj['show_week']."'
														AND ssor.group_subject='".$para_obj['subject_group']."' 
														AND ssor.tran_date='".$this->green->formatSQLDate($para_obj['date_add'])."'
														AND main_tranno='".$para_obj['schoollavel']."_".$para_obj['yearsid']."_".$para_obj['gradelavel']."_".$para_obj['classid']."_".$para_obj['examtype']."_".$para_obj['subexamtype']."'");
						if($sql_score->num_rows() > 0){
							foreach($sql_score->result() as $row_score){
								$d.='<td><input type="text" max_score="'.$max_score.'" id="score" class="form-control score" subj_id="'.$row_h_s->subjectid.'" name="score" value="'.$row_score->score_num.'" style="text-align: center;"></td>';
								$typeno = $row_score->typeno;
							}
						}else{
							$d.='<td><input type="text" max_score="'.$max_score.'" id="score" class="form-control score" name="score" subj_id="'.$row_h_s->subjectid.'" value="0" style="text-align: center;"></td>';
						}
						//$d.='<td><input type="text" max_score="'.$max_score.'" id="score" class="form-control score" name="score" subj_id="'.$row_h_s->subjectid.'" value="0" style="text-align: center;"></td>';
					}
				}
				$tr.='<tr>
						<td style="text-align:center;">'.$i.'
							<input type="hidden" name="htypno" id="htypno" class="htypno" value="'.$typeno.'">
							<input type="hidden" name="studentid" id="studentid" class="studentid" value="'.$row_st->studentid.'">
						</td>
					    <td>'.$row_st->last_name.' '.$row_st->first_name.'</td><td>'.$row_st->gender.'</td>'.$d.'

					    </tr>';
				$i++; 

			}
		}
		//$arr['arr_score'] = $arr_json;
		$arr['arr_hsubj'] = $h;
		$arr['arr_tr'] = $tr;
		//$arr['gff'] = $sql_l;
		echo json_encode($arr);
	}
	function save_data(){
		$obj    = $this->input->post("para_obj"); 
		$arr_student = $this->input->post("arr_student");
		$typeno_tr = $this->input->post("typeno_tr");
		$sql_check = $this->db->query("SELECT COUNT(*) as amt FROM sch_school_term WHERE termid='".$obj['subexamtype']."' AND start_date <= '".$this->green->convertSQLDate($obj['date_add'])."' AND end_date >='".$this->green->convertSQLDate($obj['date_add'])."'")->row()->amt;
		$check_finale = 0;
		// if($obj['is_assigment'] == 0){
		// 	$check_finale = $this->db->query("SELECT COUNT(*) AS count_final FROM sch_score_iep_entryed_order WHERE main_tranno='".$obj['schoollavel']."_".$obj['yearsid']."_".$obj['gradelavel']."_".$obj['classid']."_".$obj['examtype']."_".$obj['subexamtype']."' AND course_type='".$obj['course']."' AND typeno<>'".$typeno_tr."'")->row()->count_final;
		// }
		
		$m = 0;
		if($sql_check == 1)
		{
			
			$typeno = "";
			if($typeno_tr != 0){
				//$typeno_del = $tran->row()->typeno;
				$table = array("sch_score_iep_entryed_order","sch_score_iep_entryed_detail");
				$this->db->where("typeno",$typeno_tr);
				$this->db->delete($table);
				$typeno = $typeno_tr;
			}else{
				$typeno = $this->green->nextTran(41,"IEP Score");
			}
			
			if(count($obj) > 0)
			{
				$m=1;
				$count_subject = 0;
				$arr_c = array();
				if(count($arr_student) > 0)
				{
					foreach($arr_student as $arr_st){
						if(count($arr_st['score']) > 0){
							foreach($arr_st['score'] as $arr_sc){
									$data_st = array("studentid"=>$arr_st['studentid'],
													"subjectid"=>$arr_sc['subjectid'],
													"group_subject"=>$obj['subject_group'],
													"score_num"=>$arr_sc['val_score'],
													"type"=>41,
													"typeno"=>$typeno,
													"score_oderid"=>""
													);
									$this->db->insert("sch_score_iep_entryed_detail",$data_st);
									if(!in_array($arr_sc['subjectid'],$arr_c)){
										$arr_c[] = $arr_sc['subjectid'];
										$count_subject++;
									}
							}
						}
						
					}
				}
				$data = array("type"=>41,
								"typeno"=>$typeno,
								"main_tranno"=>$obj['schoollavel']."_".$obj['yearsid']."_".$obj['gradelavel']."_".$obj['classid']."_".$obj['examtype']."_".$obj['subexamtype'],
								"schoolid"=>$obj['schoolid'],
								"schlevelid"=>$obj['schoollavel'],
								"yearid"=>$obj['yearsid'],
								"gradelevelid"=>$obj['gradelavel'],
								"classid"=>$obj['classid'],
								"exam_typeid"=>$obj['examtype'],
								"weekid"=>$obj['show_week'],
								"tran_date"=>$this->green->convertSQLDate($obj['date_add']),
								"create_by"=>$this->session->userdata('userid'),
								"create_date"=>date('Y-m-d H:i:s'),
								"modify_by"=>$this->session->userdata('userid'),
								"modify_date"=>date('Y-m-d H:i:s'),
								"group_subject"=>$obj['subject_group'],
								"termid"=>$obj['subexamtype'],
								"count_subject"=>$count_subject
								);
				$this->db->insert("sch_score_iep_entryed_order",$data);
				$arr_total_score = array("schoolid"=>$obj['schoolid'],"schlevelid"=>$obj['schoollavel'],"yearid"=>$obj['yearsid'],"gradelevelid"=>$obj['gradelavel'],"classid"=>$obj['classid'],"termid"=>$obj['subexamtype'],"exam_type"=>$obj['examtype']);
				$this->green->update_rank_student($arr_total_score);
			}

		}else if($check_finale == 1){
			$m = 2;
		}
		$arr_json['m'] = $m;
		echo json_encode($arr_json);
	}
	function show_report_list()
	{
		$para_obj = $this->input->post("para_obj"); 
		$para_w = $this->input->post("arr_w"); 
		$where   = "";
		$where_g = "";
		$where.=" AND iep_ord.schoolid   = '".$para_obj['schoolid']."'";
		$where.=" AND iep_ord.schlevelid = '".$para_obj['schoollavel']."'";
		$where.=" AND iep_ord.yearid     = '".$para_obj['yearsid']."'";
		$where.=" AND iep_ord.classid    = '".$para_obj['classid']."'";
		$where.=" AND iep_ord.gradelevelid ='".$para_obj['gradelavel']."'";
		$where.=" AND iep_ord.exam_typeid ='".$para_obj['examtype']."'";
		$where.=" AND iep_ord.termid ='".$para_obj['termname']."'";
		if($para_obj['date_add'] != ""){
			$where.=" AND iep_ord.tran_date ='".$this->green->convertSQLDate($para_obj['date_add'])."'";
		}
		
		if($para_obj['subject_group'] != ""){
			$where.=" AND iep_ord.group_subject ='".$para_obj['subject_group']."'";
			$where_g.=" AND sch_subject_type_iep.subj_type_id ='".$para_obj['subject_group']."'";
		}
		$sql_head = $this->db->query("SELECT
												sch_subject_type_iep.subj_type_id,
												sch_subject_type_iep.subject_type,
												sch_subject_type_iep.is_report_display,
												sch_subject_type_iep.is_group_calc_percent,
												sch_subject_type_iep.is_class_participation
												FROM
												sch_subject_type_iep
												WHERE 1=1 {$where_g}
												AND schoolid='".$para_obj['schoolid']."'
												AND schlevelid='".$para_obj['schoollavel']."'
												AND is_class_participation in(2,4)
												");
		$arr_group = array();
		if($sql_head->num_rows() > 0){
			foreach($sql_head->result() as $rowgroup){
				$arr_group[] = array("subgroupid"=>$rowgroup->subj_type_id,
									"subjecttype"=>$rowgroup->subject_type,
									"isgroupclalcpercent"=>$rowgroup->subject_type,
									"group_calc_percent"=>$rowgroup->is_group_calc_percent,
									"isclassparticipat"=>$rowgroup->is_class_participation
									);
			}
		}
		$subjh = $this->db->query("SELECT
										iep_ord.weekid,
										iep_ord.group_subject,
										iep_det.subjectid,
										iep_ord.typeno,
										iep_ord.count_subject,
										iep_det.score_num,
										iep_det.studentid,
										sch_subject_iep.short_sub,
										sch_subject_iep.max_score
										FROM
											sch_score_iep_entryed_order AS iep_ord
											INNER JOIN sch_score_iep_entryed_detail AS iep_det ON iep_ord.typeno = iep_det.typeno
											INNER JOIN sch_subject_iep ON iep_det.subjectid = sch_subject_iep.subjectid
										WHERE
											1 = 1 {$where}
										ORDER BY iep_ord.weekid,iep_det.subjectid");
		$arr_score_s = array();
		$arr_headsubject     = array();
		$arr_max_score_subj  = array();
		//$arr_sum_max_student = array();
		
		if($subjh->num_rows() > 0){
			foreach($subjh->result() as $rowsh){
				if(!isset($arr_headsubject[$rowsh->weekid][$rowsh->group_subject][$rowsh->subjectid][$rowsh->typeno])){
					$arr_headsubject[$rowsh->weekid][$rowsh->group_subject][$rowsh->subjectid][$rowsh->typeno] = $rowsh->short_sub;
				}
				$arr_score_s[$rowsh->studentid][$rowsh->weekid][$rowsh->group_subject][$rowsh->subjectid][$rowsh->typeno] = $rowsh->score_num;
				
				// if(isset($arr_sum_max_student[$rowsh->studentid][$rowsh->weekid][$rowsh->group_subject][$rowsh->subjectid][$rowsh->typeno])){
				// 	$arr_sum_max_student[$rowsh->studentid][$rowsh->weekid][$rowsh->group_subject][$rowsh->subjectid][$rowsh->typeno] = ($arr_sum_max_student[$rowsh->studentid][$rowsh->weekid][$rowsh->group_subject][$rowsh->subjectid][$rowsh->typeno]+$rowsh->max_score);
				// }else{
				// 	$arr_sum_max_student[$rowsh->studentid][$rowsh->weekid][$rowsh->group_subject][$rowsh->subjectid][$rowsh->typeno] = $rowsh->max_score;
				// }

				// if(isset($arr_group_student[$rowsh->weekid][$rowsh->group_subject])){
				// 	if($arr_group_student[$rowsh->weekid][$rowsh->group_subject]  < $arr_sum_max_student[$rowsh->studentid][$rowsh->weekid][$rowsh->group_subject][$rowsh->subjectid][$rowsh->typeno]){
				// 		$arr_group_student[$rowsh->weekid][$rowsh->group_subject] = $arr_sum_max_student[$rowsh->studentid][$rowsh->weekid][$rowsh->group_subject][$rowsh->subjectid][$rowsh->typeno];
				// 	}
				// }else{
				// 	$arr_group_student[$rowsh->weekid][$rowsh->group_subject] = 0;
				// }

				$arr_max_score_subj[$rowsh->studentid][$rowsh->weekid][$rowsh->group_subject][$rowsh->subjectid][$rowsh->typeno] = $rowsh->max_score;
			}
		}

		$sql_list = $this->db->query("SELECT
										vse.student_num,
										vse.first_name,
										vse.last_name,
										vse.gender,
										vse.studentid,
										vse.class_name
										FROM
										v_student_enroment AS vse
										WHERE 1=1 
										AND vse.schoolid   = '".$para_obj['schoolid'] ."'
										AND vse.schlevelid = '".$para_obj['schoollavel']."'
										AND vse.year    = '".$para_obj['yearsid']."'
										AND vse.classid    = '".$para_obj['classid']."'
										AND vse.grade_levelid ='".$para_obj['gradelavel']."'
										ORDER BY vse.student_num ASC");
		
		$tr = "";
		$p  = 1;
		$arrw=array();
		$arrg = array();
		$arrsub = array();
		$arr_group_student = array();
		if($sql_list->num_rows() >0)
		{
			foreach($sql_list->result() as $row_st)
			{
				$tdd = "";
				if(isset($para_w)){
		 			foreach($para_w as $row_w)
		 			{
		 				if(isset($arr_group))
		 				{
		 					foreach($arr_group as $k=>$v_group)
		 					{
		 						$sum_g = 0;
		 						//$g     = 0;
		 						$sum_max_s = 0;
		 						$result_m = 0;
		 						if(isset($arr_headsubject[$row_w['weekid']][$v_group['subgroupid']]))
		 						{
				 					foreach($arr_headsubject[$row_w['weekid']][$v_group['subgroupid']] as $kk=>$vsh)
				 					{
				 						
				 						foreach($vsh as $k11=>$vsubject){
				 							
											if(isset($arr_score_s[$row_st->studentid][$row_w['weekid']][$v_group['subgroupid']][$kk][$k11])){
												$tdd.="<td style='text-align:center;'>".$arr_score_s[$row_st->studentid][$row_w['weekid']][$v_group['subgroupid']][$kk][$k11]."</td>";
												$sum_g+=$arr_score_s[$row_st->studentid][$row_w['weekid']][$v_group['subgroupid']][$kk][$k11];
												$sum_max_s+=$arr_max_score_subj[$row_st->studentid][$row_w['weekid']][$v_group['subgroupid']][$kk][$k11];
												//$sum_max_s=$arr_group_student[$row_w['weekid']][$v_group['subgroupid']];
												
											}else{
												$tdd.="<td>&nbsp;</td>";
											}
				 						}
				 					}
				 					if(isset($arr_group_student[$row_w['weekid']][$v_group['subgroupid']]))
				 					{
			 							if($arr_group_student[$row_w['weekid']][$v_group['subgroupid']] < $sum_max_s)
			 							{
											$arr_group_student[$row_w['weekid']][$v_group['subgroupid']] = $sum_max_s;
			 							}
			 						}else{
			 							$arr_group_student[$row_w['weekid']][$v_group['subgroupid']] = $sum_max_s;
			 						} 
			 						$result_m = isset($arr_group_student[$row_w['weekid']][$v_group['subgroupid']])?$arr_group_student[$row_w['weekid']][$v_group['subgroupid']]:1;
				 				
				 				}else{
				 					$tdd.="<td>&nbsp;</td>";
				 				}
				 				
		 						$tdd.="<td style='text-align:center;background-color: #eaecee;'><b>".($sum_g==0?"":round(($sum_g*$v_group['group_calc_percent'])/$result_m,2))."</b></td>";
		 					}// end loop group
		 					
		 				}
		 				
		 			}
				}
				$tr.='<tr>
						<td style="text-align:center;">'.($p++).'</td>
						<td>'.$row_st->last_name.' '.$row_st->first_name.'</td>
						<td>'.$row_st->gender.'</td>'.$tdd.'</tr>';
			}
		}
		$th_w = "";
		$th_g = "";
		$colw = 0;
		$tr_s = "";
		$col_no = 0;
		if(isset($para_w)){
		 	foreach($para_w as $kw=>$roww)
		 	{
		 		$colw = 0;
	 			if(isset($arr_group)){
		 			foreach($arr_group as $kg=>$rowgh)
		 			{
		 				$col_subject = 0;
		 				if(isset($arr_headsubject[$roww['weekid']][$rowgh['subgroupid']])){
		 					foreach($arr_headsubject[$roww['weekid']][$rowgh['subgroupid']] as $vsh){
		 						foreach($vsh as $k11=>$vsubject){
									$tr_s.="<th>".$vsubject."</th>";
									$col_subject++;
		 						}
		 					}
		 				}else{
		 					$tr_s.="<th></th>";
		 					$colw++;
		 				}
		 				$th_g.="<th colspan='".$col_subject."' style='background-color: #d6eaf8;'>".$rowgh['subjecttype']."</th>";
		 				$th_g.="<th rowspan='2' style='background-color: #eaecee;'>Total&nbsp;".$rowgh['group_calc_percent']."</th>";
		 				$colw=$colw+$col_subject;
		 				$colw++;
		 			}
		 		}
		 		$th_w.="<th colspan='".($colw)."' style='text-align:center;background-color: #aed6f1;'>".$roww['weekname']."</th>";
		 		$col_no+=$colw;
		 	}
		}
		if($tr==""){
			$tr.="<tr><td colspan='".($col_no+3)."' style='text-align:center;'><b><i>Data not found.</i></b></td></tr>";
		}
		$th = "<tr><th rowspan='3' style='vertical-align:middle;'>No</th>
					<th rowspan='3' style='vertical-align:middle;'>Student's Name</th>
					<th rowspan='3' style='vertical-align:middle;'>Sex</th>
					".$th_w."
				</tr><tr>".$th_g."</tr><tr>".$tr_s."</tr>".$tr;
				
		

		$arr['tr'] = $th;
		//$arr['tr_h'] = $arr_headsubject;
		//$arr['test'] = $arr_score_s;
		echo json_encode($arr);
	}
	function show_list_term()
	{
		/*=============== Read Me ===============
			$v_subj->is_class_participation == 3 is Group Attendance
			$v_subj->is_class_participation == 1 is Group Class Particiption
			$v_subj->is_class_participation == 2 is Group Homework
				(sum score in week*max score group subject/sum score subject)
				- Mid-Term
				- Final
			$v_subj->is_class_participation == 4 is Group Diligence
				- Mid-Term
				- Final
			$v_subj->is_class_participation == 0 is  Group subject for exam mid-term and final
		*/
		$para_obj = $this->input->post("para_obj"); 
		$para_w   = $this->input->post("arr_w"); 
		$where   = "";
		$where_g = "";
		$where.=" AND ord.schoolid   = '".$para_obj['schoolid'] ."'";
		$where.=" AND ord.schlevelid = '".$para_obj['schoollavel']."'";
		$where.=" AND ord.yearid     = '".$para_obj['yearsid']."'";
		$where.=" AND ord.classid    = '".$para_obj['classid']."'";
		$where.=" AND ord.gradelevelid ='".$para_obj['gradelavel']."'";
		$where.=" AND ord.termid ='".$para_obj['subexamtype']."'";
		$classname = $para_obj['classname'];
		
		$sub_xamp = $this->db->query("SELECT
											sch_subject_iep.subjectid,
											sch_subject_iep.subj_type_id,
											sch_subject_iep.short_sub,
											sch_subject_iep.max_score
											FROM
											sch_subject_iep
											WHERE 1=1 
											AND sch_subject_iep.schoolid   = '".$para_obj['schoolid']."'
											AND sch_subject_iep.schlevelid = '".$para_obj['schoollavel']."'
											ORDER BY subj_type_id ASC");
		$arr_subject = array();
		if($sub_xamp->num_rows() > 0){
			foreach($sub_xamp->result() as $rowg_sub){
				$arr_subject[$rowg_sub->subj_type_id][$rowg_sub->subjectid] = array("short_sub"=>$rowg_sub->short_sub,"max_score"=>$rowg_sub->max_score);
			}
		}

		$sql_test = $this->db->query("SELECT
										det.studentid,
										ord.exam_typeid,
										ord.group_subject,
										ord.weekid,
										det.score_num,
										sch_subject_type_iep.subject_type,
										sch_subject_type_iep.is_class_participation,
										det.subjectid,
										ord.count_subject,
										ord.typeno,
										sch_subject_iep.max_score
										FROM
										sch_score_iep_entryed_order AS ord
										INNER JOIN sch_score_iep_entryed_detail AS det ON ord.typeno = det.typeno AND ord.type = det.type
										INNER JOIN sch_subject_type_iep ON det.group_subject = sch_subject_type_iep.subj_type_id
										INNER JOIN sch_subject_iep ON det.subjectid = sch_subject_iep.subjectid
										WHERE 1=1 {$where}
										ORDER BY det.studentid,ord.group_subject,ord.weekid");
		$arr_hw = array();
		$arr_dl = array();
		$arr_ex = array();
		$arr_cp = array();
		$arr_att= array();
		$arr_week_h = array();
		$arr_week_d = array();
		$arr_count_h  = array();
		$arr_count_d  = array();
		$arr_count_cp = array();

		$score_max_sub    = array();
		$score_max_sub_dl = array();

		$arr_group_su = array();
		$arr_group_cp = array();
		$arr_group_att = array();

		$arr_score_st = array();
		if($sql_test->num_rows() > 0)
		{
			foreach($sql_test->result() as $rowt){
				if($rowt->is_class_participation == 0){ // subject
					$arr_ex[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]=$rowt->score_num;
				}
				else if($rowt->is_class_participation == 1){ // class participation
					if(isset($arr_cp[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
						$arr_cp[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]=($arr_cp[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]+$rowt->score_num);
					}else{
						$arr_cp[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]=$rowt->score_num;
					}
					// array sum max score
					if(isset($arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
						$arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = ($arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]+$rowt->max_score);
					}else{
						$arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $rowt->max_score;
					}
					// array show max score
					if(isset($arr_group_cp[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
						if($arr_group_cp[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] < $arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]){
							$arr_group_cp[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid];
						}
					}
					else{
						$arr_group_cp[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $rowt->max_score;
					}

					// if(isset($arr_group_as[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
					// 	if(isset($arr_att[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
					// 		if($arr_group_as[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]  < $arr_att[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]){
					// 			$arr_group_as[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $arr_att[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid];
					// 		}
					// 	}
						
					// }else{
					// 	$arr_group_as[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = 0;
					// }
					
					// if(!array_key_exists($rowt->typeno,$arr_count_cp)){
					// 	$arr_count_cp[$rowt->exam_typeid][$rowt->typeno] = $rowt->count_subject;
					// }
				}
				else if($rowt->is_class_participation == 3){ // attendance
					if(isset($arr_att[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
						$arr_att[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]=($arr_att[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]+$rowt->score_num);
					}else{
						$arr_att[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]=$rowt->score_num;
					}
					// array sum max score
					if(isset($arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
						$arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = ($arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]+$rowt->max_score);
					}else{
						$arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $rowt->max_score;
					}
					// array show max score
					if(isset($arr_group_att[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid])){
						if($arr_group_att[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] < $arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid]){
							$arr_group_att[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $arr_score_st[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid];
						}
					}
					else{
						$arr_group_att[$rowt->exam_typeid][$rowt->group_subject][$rowt->subjectid] = $rowt->max_score;
					}
					
				}
				else if($rowt->is_class_participation == 2) // homework
				{
					if(isset($arr_subject[$rowt->group_subject][$rowt->subjectid])){
						if(isset($score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
							$score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]+$arr_subject[$rowt->group_subject][$rowt->subjectid]['max_score'];
						}else{
							$score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $arr_subject[$rowt->group_subject][$rowt->subjectid]['max_score'];
						}
					}
					if(isset($arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
						if($arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]<$score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]){
							$arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $score_max_sub[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid];
						}
					}else{
						$arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = 0;
					}
					if(isset($arr_hw[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
						$arr_hw[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=($arr_hw[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]+$rowt->score_num);
					}else{
						$arr_hw[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=$rowt->score_num;
					}

					// if(!array_key_exists($rowt->typeno,$arr_count_h)){
					// 	$arr_count_h[$rowt->exam_typeid][$rowt->weekid][$rowt->typeno] = $rowt->count_subject;
					// }
					if(!in_array($rowt->weekid,$arr_week_h)){
						$arr_week_h[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=$rowt->weekid;
					}
				}else if($rowt->is_class_participation == 4) // diligen
				{
					if(isset($arr_subject[$rowt->group_subject][$rowt->subjectid])){
						if(isset($score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
							$score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]+$arr_subject[$rowt->group_subject][$rowt->subjectid]['max_score'];
						}else{
							$score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $arr_subject[$rowt->group_subject][$rowt->subjectid]['max_score'];
						}
					}
					if(isset($arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
						if($arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]  < $score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]){
							$arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = $score_max_sub_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid];
						}
					}else{
						$arr_group_su[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid] = 0;
					}
					if(isset($arr_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid])){
						$arr_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=($arr_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]+$rowt->score_num);
					}else{
						$arr_dl[$rowt->studentid][$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=$rowt->score_num;
					}
					// if(!array_key_exists($rowt->typeno,$arr_count_d)){
					// 	$arr_count_d[$rowt->exam_typeid][$rowt->weekid][$rowt->typeno] = $rowt->count_subject;
					// }
					if(!in_array($rowt->weekid,$arr_week_d)){
						$arr_week_d[$rowt->exam_typeid][$rowt->group_subject][$rowt->weekid]=$rowt->weekid;
					}
				}
			}
		}
		

		$sqlg = $this->db->query("SELECT
									sch_subject_type_iep.subj_type_id,
									sch_subject_type_iep.subject_type,
									sch_subject_type_iep.main_type,
									sch_subject_type_iep.orders,
									sch_subject_type_iep.is_group_calc_percent,
									sch_subject_type_iep.is_class_participation
									FROM
									sch_subject_type_iep
									WHERE 1=1 
									AND schoolid='".$para_obj['schoolid']."'
									AND schlevelid='".$para_obj['schoollavel']."'
									ORDER BY orders");
		$arr_group_sub = array();
		if($sqlg->num_rows() > 0){
			foreach($sqlg->result() as $rowgsub){
				$arr_group_sub[] = array('groupid'=>$rowgsub->subj_type_id,
										'subject_type'=>$rowgsub->subject_type,
										'max_sco_g'=>$rowgsub->is_group_calc_percent,
										'is_cp'=>$rowgsub->is_class_participation
										);
			}
		}

		$sql_list = $this->db->query("SELECT
										vse.student_num,
										vse.first_name,
										vse.last_name,
										vse.gender,
										vse.studentid,
										vse.class_name
										FROM
										v_student_enroment AS vse
										WHERE 1=1 
										AND vse.schoolid   = '".$para_obj['schoolid'] ."'
										AND vse.schlevelid = '".$para_obj['schoollavel']."'
										AND vse.year    = '".$para_obj['yearsid']."'
										AND vse.classid    = '".$para_obj['classid']."'
										AND vse.grade_levelid ='".$para_obj['gradelavel']."'
										ORDER BY vse.student_num ASC");
		$tr_list = "";
		$j = 1;
		$arr_total = array();
		$arr_test  = array();
		if($sql_list->num_rows() >0)
		{
			foreach($sql_list->result() as $row_st)
			{
				$sql_exmp = $this->db->query("SELECT
												sch_student_examtype_iep.examtypeid
											FROM sch_student_examtype_iep
											ORDER BY examtypeid ASC");
				$st_td = "";
				$total_all = 0;
				$tr_att   = "";				
				$tr_cp    = "";
				
				if($sql_exmp->num_rows() > 0)
				{
					foreach($sql_exmp->result() as $rexamp)
					{
						
						$sum_sub_examp = 0;
						if(count($arr_group_sub) > 0)
						{
							foreach($arr_group_sub as $rg)
							{
								if($rg['is_cp'] == 3){ // attendance
									if(isset($arr_subject[$rg['groupid']])){

										foreach($arr_subject[$rg['groupid']] as $kk=>$rsub){
											$sum_att      = isset($arr_att[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$kk])?$arr_att[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$kk]:0;
											$max_group_as = isset($arr_group_att[$rexamp->examtypeid][$rg['groupid']][$kk])?$arr_group_att[$rexamp->examtypeid][$rg['groupid']][$kk]:1;
											$total_att    = floor((($sum_att*$rg['max_sco_g'])/$max_group_as)*100)/100;
											$tr_att.="<td style='text-align:center;background-color: #ebedef;'>".($total_att == 0?"":$total_att)."</td>";
											$arr_total[$row_st->studentid][$rexamp->examtypeid] = isset($arr_total[$row_st->studentid][$rexamp->examtypeid])?($arr_total[$row_st->studentid][$rexamp->examtypeid]+$total_att):$total_att;
										}
									}else{
										$tr_att.="<td></td>";
									}
								}else if($rg['is_cp'] == 1){ // class paticipation
									if(isset($arr_subject[$rg['groupid']])){

										foreach($arr_subject[$rg['groupid']] as $kk=>$rsub){
											$sum_cp      = isset($arr_cp[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$kk])?$arr_cp[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$kk]:0;
											$max_group_cp = isset($arr_group_cp[$rexamp->examtypeid][$rg['groupid']][$kk])?$arr_group_cp[$rexamp->examtypeid][$rg['groupid']][$kk]:1;
											$total_cp    = floor((($sum_cp*$rg['max_sco_g'])/$max_group_cp)*100)/100;
											$tr_cp.="<td style='text-align:center;background-color: #ebedef;'>".($total_cp == 0?"":$total_cp)."</td>";
											$arr_total[$row_st->studentid][$rexamp->examtypeid] = isset($arr_total[$row_st->studentid][$rexamp->examtypeid])?($arr_total[$row_st->studentid][$rexamp->examtypeid]+$total_cp):$total_cp;
										}

										// $sum_cp = 0;
										// if(isset($arr_count_cp[$rexamp->examtypeid])){
										// 	foreach($arr_count_cp[$rexamp->examtypeid] as $v_c){
										// 		$sum_cp+=$v_c; 
										// 	}
										// }
										// foreach($arr_subject[$rg['groupid']] as $kk=>$rsub){
										// 	$sum_s1 = isset($arr_cp[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$kk])?$arr_cp[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$kk]:0;
										// 	if($sum_cp == 0){
										// 		$sum_cp=1;
										// 	}
										// 	$result_cp = round($sum_s1/$sum_cp,2);
										// 	$tr_cp.="<td style='text-align:center;background-color: #ebedef;'>".($result_cp==0?"":$result_cp)."__".$sum_cp."</td>";
										// 	$arr_total[$row_st->studentid][$rexamp->examtypeid] = isset($arr_total[$row_st->studentid][$rexamp->examtypeid])?($arr_total[$row_st->studentid][$rexamp->examtypeid]+$result_cp):$result_cp;
										// }
									}else{
										$tr_cp.="<td></td>";
									}
									
								}else if($rg['is_cp'] == 0){ // Subject
									if(isset($arr_subject[$rg['groupid']])){
										$p = 1;
										$sum_sub1=0;
										foreach($arr_subject[$rg['groupid']] as $kk=>$rsub){
											$sum_sub = isset($arr_ex[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$kk])?$arr_ex[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$kk]:0;
											$st_td.="<td style='text-align:center;'>".($sum_sub==0?"":$sum_sub)."</td>";
											$arr_total[$row_st->studentid][$rexamp->examtypeid] = isset($arr_total[$row_st->studentid][$rexamp->examtypeid])?($arr_total[$row_st->studentid][$rexamp->examtypeid]+$sum_sub):$sum_sub;
										}
									}else{
										$st_td.="<td></td>";
									}
								}else if($rg['is_cp'] == 2){ // Homework
									if(isset($arr_week_h[$rexamp->examtypeid][$rg['groupid']])){
										$total_h = 0;
										$sum_count_h = 0;
										foreach($arr_week_h[$rexamp->examtypeid][$rg['groupid']] as $ks=>$vs)
										{
											if(isset($arr_hw)){
												//$total_count = 1;
												// if(isset($arr_count_h)){
												// 	foreach($arr_count_h[$rexamp->examtypeid][$vs] as $v_sum){
												// 		$total_count+=$v_sum;
												// 	}
												// }
												$sum_count_h++;
												$max_group   = isset($arr_group_su[$rexamp->examtypeid][$rg['groupid']][$vs])?$arr_group_su[$rexamp->examtypeid][$rg['groupid']][$vs]:1;
												//$max_scor_subj = isset($score_max_sub[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$vs])?$score_max_sub[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$vs]:1;
												
												$sum_h  = isset($arr_hw[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$vs])?$arr_hw[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$vs]:0;
												$sum_hw = floor((($sum_h*$rg['max_sco_g'])/$max_group)*100)/100;
												$st_td.="<td style='text-align:center;'>".($sum_h==0?"":$sum_hw)."</td>";
												$total_h+=$sum_hw;
											}
										}
										if($sum_count_h == 0){
											$sum_count_h = 1;
										}
										$result_hw = floor(($total_h/$sum_count_h)*100)/100;
										$arr_total[$row_st->studentid][$rexamp->examtypeid] = isset($arr_total[$row_st->studentid][$rexamp->examtypeid])?($arr_total[$row_st->studentid][$rexamp->examtypeid]+$result_hw):$result_hw;
										$st_td.="<td style='text-align:center;background-color: #ebedef;'><b>".($total_h==0?"":$result_hw)."</b></td>";
									}else{
										$st_td.="<td style='background-color: #ebedef;'></td>";
									}
								}else if($rg['is_cp'] == 4){ // Diligence
									if(isset($arr_week_d[$rexamp->examtypeid][$rg['groupid']])){
										$total_d = 0;
										$sum_count_d = 0;
										foreach($arr_week_d[$rexamp->examtypeid][$rg['groupid']] as $ks=>$vs){
											if(isset($arr_dl)){
												$total_count = 1;
												// if(isset($arr_count_d)){
												// 	foreach($arr_count_d[$rexamp->examtypeid][$vs] as $v_sum){
												// 		$total_count+=$v_sum;
												// 	}
												// }
												$sum_count_d++;
												$max_group = isset($arr_group_su[$rexamp->examtypeid][$rg['groupid']][$vs])?$arr_group_su[$rexamp->examtypeid][$rg['groupid']][$vs]:1;
												//$max_scor_subj_d = isset($score_max_sub_dl[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$vs])?$score_max_sub_dl[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$vs]:1;
												$sum_d = isset($arr_dl[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$vs])?$arr_dl[$row_st->studentid][$rexamp->examtypeid][$rg['groupid']][$vs]:0;
												$sum_sc_d = floor((($sum_d*$rg['max_sco_g'])/$max_group)*100)/100;
												$st_td.="<td style='text-align:center;'>".($sum_d==0?"":$sum_sc_d)."</td>";
												$total_d+=$sum_sc_d;
											}
										}
										if($sum_count_d == 0){
											$sum_count_d =1;
										}
										$arr_total[$row_st->studentid][$rexamp->examtypeid] = isset($arr_total[$row_st->studentid][$rexamp->examtypeid])?($arr_total[$row_st->studentid][$rexamp->examtypeid]+($total_d/$sum_count_d)):($total_d/$sum_count_d);
										$st_td.="<td style='text-align:center;background-color: #ebedef;'><b>".($total_d==0?"":floor(($total_d/$sum_count_d)*100)/100)."</b></td>";
									}else{
										$st_td.="<td style='background-color: #ebedef;'></td>";
									}
								}
							}
						}
						$result_total = isset($arr_total[$row_st->studentid][$rexamp->examtypeid])?$arr_total[$row_st->studentid][$rexamp->examtypeid]:0;
						$st_td.="<td style='text-align:center;background-color: #bfc9ca ;'><b>".($result_total==0?"":floor(($result_total/2)*100)/100)."</b></td>";
						$total_all+=floor(($result_total/2)*100)/100;
					}
				}
				//$result_round = $total_all;
				$st_td.="<td style='text-align:center;background-color:#c7ebf0;'><b>".($total_all==0?"":floor(($total_all/2)*100)/100)."</b></td>";
				$tr_list.= '<tr>
								<td style="text-align:center;">'.($j++).'</td>
								<td>'.$row_st->last_name.' '.$row_st->first_name.'
									<input type="hidden" name="student_id" value="'.$row_st->studentid.'" id="student_id">
								</td>
								
								'.$tr_att.$tr_cp.$st_td.'
							</tr>';
			}
		}else{
			$tr_list.= '<tr><td colspan="29" style="text-align:center;"><i>Data not found !</i></td></tr>';
		}
		
		//$head_att_pc = "";
		$head_att = "";
		$head_pc = "";
		$head_hw_m = "";
		$head_hw_f = "";

		$head_dl_m = "";
		$head_dl_f = "";
		$head_sub= "";

		$tr_hw_m = "";
		$tr_hw_f = "";
		$tr_dl_m = "";
		$tr_dl_f = "";

		$total_w_hw = "";
		$total_w_hw_f  = "";
		$total_w_dl = "";
		$total_w_dl_f = "";

		$th_scor_sub = "";
		$th_sub = "";

		$col_midterm = 0;
		$col_midterm_xam = 0;
		$col_final   = 0;
		if(count($arr_group_sub) > 0){
			foreach($arr_group_sub as $v_subj){
				if($v_subj['is_cp'] == 3){
					$head_att.= "<th colspan='2'>".$v_subj['subject_type']."</th>";
				}else if($v_subj['is_cp'] == 1){
					$head_pc.= "<th colspan='2'>".$v_subj['subject_type']."</th>";
				}else if($v_subj['is_cp'] == 2){
					$k1 = 0;
					$k2 = 0;
					if(isset($arr_week_h[1][$v_subj['groupid']])){
						foreach($arr_week_h[1][$v_subj['groupid']] as $vhwm){
							$tr_hw_m.='<th>HW'.$vhwm.'</th>';
							$k1++;
						}
					}
					if(isset($arr_week_h[2][$v_subj['groupid']])){
						foreach($arr_week_h[2][$v_subj['groupid']] as $vhwf){
							$tr_hw_f.='<th>HW'.$vhwf.'</th>';
							$k2++;
						}
					}
					$col_midterm+=($k1+1);
					$col_final+=($k2+1);
					$head_hw_m  .= '<th colspan="'.($k1+1).'">'.$v_subj['subject_type'].'</th>';
					$total_w_hw .= '<th colspan="'.($k1+1).'">Weeks 1-5(10pts)</th>';
					$head_hw_f  .= '<th colspan="'.($k2+1).'">'.$v_subj['subject_type'].'</th>';
					$total_w_hw_f.= '<th colspan="'.($k2+1).'">Weeks 6-10(10pts)</th>';
					
				}else if($v_subj['is_cp'] == 4){
					$k3 = 0;
					$k4 = 0;
					if(isset($arr_week_d[1][$v_subj['groupid']])){
						foreach($arr_week_d[1][$v_subj['groupid']] as $vdlm){
							$tr_dl_m.='<th>DL'.$vdlm.'</th>';
							$k3++;
						}
					}
					if(isset($arr_week_d[2][$v_subj['groupid']])){
						foreach($arr_week_d[2][$v_subj['groupid']] as $vdlf){
							$tr_dl_f.='<th>DL'.$vdlf.'</th>';
							$k4++;
						}
					}
					$col_midterm+=($k3+1);
					$col_final+=($k4+1);
					$head_dl_m.='<th colspan="'.($k3+1).'">'.$v_subj['subject_type'].'</th>';
					$total_w_dl .= '<th colspan="'.($k3+1).'">Weeks 1-5(10pts)</th>';
					$head_dl_f.='<th colspan="'.($k4+1).'">'.$v_subj['subject_type'].'</th>';
					$total_w_dl_f .= '<th colspan="'.($k4+1).'">Weeks 6-10(10pts)</th>';
				}else if($v_subj['is_cp'] == 0){
					$c = 0;
					if(isset($arr_subject[$v_subj['groupid']])){
						foreach($arr_subject[$v_subj['groupid']] as $kg=>$rowhs){
								$th_scor_sub.='<th>'.$rowhs['max_score'].'</th>';
								$th_sub.='<th>'.$rowhs['short_sub'].'</th>';
								$c++;
						}
					}else{
						$th_scor_sub.='<th></th>';
						$th_sub.='<th></th>';
						$c++;
					}
					$col_midterm_xam+=$c;
					$head_sub.= '<th colspan="'.$c.'">'.$v_subj['subject_type'].'</th>';
				}
			}
		}
		
		$tr_head = '<tr>
						<th rowspan="3" colspan="2" style="vertical-align:middle;">Grade:&nbsp;'.$classname.'</th>
						'.$head_att.$head_pc.'
						<th colspan="'.$col_midterm.'">Mid-Term</th>
						<th colspan="'.$col_midterm_xam.'">Mid-Term Exam</th>
						<th rowspan="4" style="writing-mode: sideways-lr;padding:20px 25px 0 10px !important;background-color: #bfc9ca;">Total Mid-Term 100 pts</th>
						<th colspan="'.$col_final.'">Final</th>
						<th colspan="'.$col_midterm_xam.'">Final Exam</th>
						<th rowspan="4" style="writing-mode: sideways-lr;padding:20px 25px 0 10px;background-color:#bfc9ca;">Total Final 100 pts</th>
						<th rowspan="4" style="writing-mode: sideways-lr;padding:10px 25px 0 10px;background-color:#c7ebf0;">Grand Total Final 100 pts</th>
					</tr>
					<tr>
						<th rowspan="3" style="writing-mode: sideways-lr;padding-right:10px;background-color: #ebedef;">Att(Mid-term) 10pts</th>
						<th rowspan="3" style="writing-mode: sideways-lr;padding-right:10px;background-color: #ebedef;">Att(Final) 10pts</th>
						<th rowspan="3" style="writing-mode: sideways-lr;padding-right:10px;background-color: #ebedef;">CP(Mid-term) 10pts</th>
						<th rowspan="3" style="writing-mode: sideways-lr;padding-right:10px;background-color: #ebedef;">CP(Final) 10pts</th>
						'.$head_hw_m.'
						'.$head_dl_m.'
						'.$head_sub.'
						'.$head_hw_f.'
						'.$head_dl_f.'
						'.$head_sub.'
					</tr>
					<tr>
						'.$total_w_hw.'
						'.$total_w_dl.'
						'.$th_scor_sub.'
						'.$total_w_hw_f.'
						'.$total_w_dl_f.'
						'.$th_scor_sub.'
					</tr>
					<tr>
						<th>No</th>
						<th>Student name</th>
						'.$tr_hw_m.'
						<th style="background-color:#ebedef;">HW 1-5(10)</th>
						'.$tr_dl_m.'
						<th style="background-color:#ebedef;">PF 1-5(10)</th>
						'.$th_sub.'
						'.$tr_hw_f.'
						<th style="background-color:#ebedef;">HW 6-10(10)</th>
						'.$tr_dl_f.'
						<th style="background-color:#ebedef;">DL 6-10(10)</th>
						'.$th_sub.'
					</tr>'.$tr_list;
		$tbl = '<table>'.$tr_head.'</table>';
		$arr_json['tbl_list'] = $tbl;
		//$arr_json['dd1'] = $arr_test;
		//$arr_json['dd2'] = $arr_count_d;
		//$arr_json['cc'] = $arr_count_cp;
		
		echo json_encode($arr_json);

		
		//echo json_encode($arr_js);
		
	}
	
}