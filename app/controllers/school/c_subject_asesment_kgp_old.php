<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class c_subject_asesment_kgp extends CI_Controller{
	    protected $thead;
		protected $idfield;
	    function __construct() {
	        parent::__construct();
	        $this->load->model("school/m_subject_asesment_kgp","sakgp");
	        $this->thead = array("No" => 'no',
	        					"Assesment Name Khmer" => 'assess_name_kh',
	        					"Asesment Name English"=>'assess_name_en',
	        					//"Percentag"=>'def_percentag',
								"Status" => 'is_active',
								"Action"=>'Action'	
	                            );
	        $this->idfield = "assess_id";
	    }
	    function index() {
	        $data['idfield'] = $this->idfield;
        	$data['thead'] = $this->thead;
	        $this->load->view('header',$data);
	        //$this->load->view('school/subjectasesment/v_subject_asesment_kgp',$data);
	        $this->load->view('school/subjectasesment/v_subject_asesment_percentag_kgp',$data);
	        $this->load->view('footer',$data );
	    }
	    // save --------------------------------------------------------
		function save(){
			$assess_id = $this->input->post('assess_id');
			$asesment_kh = $this->input->post('asesment_kh');
			$count=$this->sakgp->validate($asesment_kh,$assess_id);	
			$msg='';
			if($count>0){
				$msg="Asesment kgp is already exist...!";
				$assess_id='';
			}else{
				$asesment_kh=$this->sakgp->save($assess_id);
				$msg="Asesment kgp was created...!";
			}
			$arr=array('msg'=>$msg,'assess_id'=>$assess_id);
			header("Content-type:text/x-json");
			echo json_encode($arr);
		}
		// deletedata ------------------------------------------------
		function deletedata(){
			$assess_id = $this->input->post('assess_id');
			if(intval($assess_id) > 0 ) {
			   $this->db->where('assess_id',$assess_id)->delete('sch_subject_assessment_kgp');
			   $msgbox = 'Data deleted!';
			}else{
				$msgbox = 'errorses';
			}
			$arrDel = array();
			header('Content-type:text/x-json');
			$arrDel['success']=$msgbox;
			echo json_encode($arrDel);
			die();
		}
		// editdata  -------------------------------------------------
		function editdata(){
			$arr = array();	
			$assess_id = $this->input->post('assess_id');	
			$arr['res']=$this->sakgp->editdata($assess_id);
			header('Content-type:text/x-json');	
			echo json_encode($arr);
			die();			
		}
		// showdata -------------------------------------------------
		function showdata(){
			$total_display =$this->input->post('sort_num');
			$this->green->setActiveRole($this->input->post('roleid'));
	  		$total_display = $this->input->post('total_display');
	  		$sortby=$this->input->post("sortby");
			$sorttype=$this->input->post("sorttype");
	  		$where='';
			$sortstr="";
		        (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

			$sqr="SELECT *
			 		FROM
						sch_subject_assessment_kgp 
					WHERE 1=1 {$where}";
			
			$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM sch_subject_assessment_kgp WHERE 1=1 {$where}")->row()->numrow;
			$getperpage=0;
			if($total_display==''){
				$getperpage=10;
			}else{
				$getperpage=$total_display;
			}
			$paging    = $this->green->ajax_pagination($total_row,site_url()."school/c_subject_asesment_kgp",$getperpage,"icon");
			if($sortstr!=""){
				$sqr.=$sortstr;
			}
			$getlimit=10;
			if($paging['limit']!=''){
				$getlimit=$paging['limit'];
			}
			$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
			$j='';
			$table='';
			//$Percentag='';
				if(count($data) > 0){
					foreach($data as $row){  
						$j++;
						//$Percentag = ($row->def_percentag*100);
						$rowcolor='';
						if($row->is_active=='0'){
							$rowcolor.="<td style='text-align:left;color:red;'>".$j."</td>";
							$rowcolor.="<td style='text-align:center;color:red;'>".$row->assess_name_kh."</td>";
							$rowcolor.="<td style='text-align:center;color:red;'>".$row->assess_name_en."</td>";
							//$rowcolor.="<td style='text-align:left;color:red;'>".$Percentag."%</td>";
							$rowcolor.="<td style='text-align:center;color:red;'>".($row->is_active == '0' ? 'In_Active' : 'Active')."</td>";
						}else{
							$rowcolor.="<td style='text-align:left;'>".$j."</td>";
							$rowcolor.="<td style='text-align:center;'>".$row->assess_name_kh."</td>";
							$rowcolor.="<td style='text-align:center;'>".$row->assess_name_en."</td>";
							//$rowcolor.="<td style='text-align:left;'>".$Percentag."%</td>";
							$rowcolor.="<td style='text-align:center;'>".($row->is_active == '0' ? 'In_Active' : 'Active')."</td>";
						}
						$table.="<tr>
									</td>";
										$table.=$rowcolor;
										$table.="<td style='width:10px;' class='remove_tag no_wrap'>";
										if($this->green->gAction("U")){
											$table.= "<a data-placement='top' title='Update'><img rel=".$row->assess_id." onclick='update(event);' src='".base_url('assets/images/icons/edit.png')."'/></a>";
										}
										if($this->green->gAction("D")){
											$table.= "<a data-placement='top' title='Delete'><img rel=".$row->assess_id." onclick='deletedata(event);' src='".base_url('assets/images/icons/delete.png')."'/></a>";
										}	
						$table.= " </td>
							 	</tr> ";	
					}
				}else{
					$table.= "<tr>
								<td colspan='5' style='text-align:center;'> 
									We did not find anything to show here... 
								</td>
							</tr>";
				}	

			$arr['data']=$table;
			header('Content-Type:text/x-json');
			$arr['body']=$table;
			$arr['pagination']=$paging;
			echo json_encode($arr); 
			die();
		}
	}
