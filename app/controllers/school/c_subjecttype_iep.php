<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_subjecttype_iep extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/subjecttypemodel_iep','subjecttypes');
        $this->load->model('school/schoolinformodel','sch');
        $this->load->model('school/schoolyearmodel','schyear');
        $this->load->model('school/schoollevelmodel','schlevels');
        $this->load->model('school/modrangelevel','rnglev');
        $this->load->model('school/gradelevelmodel','gradlev');
		$this->load->library('pagination');	
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }	
	     $this->green->setActiveRole($this->session->userdata('roleid'));
		   if($m!=''){
		        $this->green->setActiveModule($m);
		   }
		   if($p!=''){
		        $this->green->setActivePage($p); 
		   }
	}
	public function index()
	{
		$this->load->view('header');

        $data['schevels']=$this->schlevels->getsch_level("2");
        $data['schyears']=$this->schyear->getschoolyear();
        $data['rangelevs']=$this->rnglev->rangelevels();
        $data['search_data']=$this->search();
		$this->load->view('school/subjectgroup_iep/v_subject_group_iep',$data);
		$this->load->view('footer');
	}
	function searchall(){
		$list = $this->search();
		echo $list;
		die();
	}
	function savesubjecttype()
	{
		$subj_type_id =$this->input->post('subj_type_id');
        $save = $this->subjecttypes->save($subj_type_id);
        header("Content-type:text/x-json");
		$arrJson["res"]=$save;
        echo json_encode($arrJson);
        exit();		
	}
	function editsubjecttype()
	{
		$groupid  = $this->input->post("groupid");
		$sql_data = $this->db->query("SELECT * FROM sch_subject_type_iep WHERE subj_type_id='".$groupid."'")->row();
		echo json_encode($sql_data);
		die();	
	}
	
	function deletesubjecttype()
	{
		$groupid = $this->input->post("groupid");
		$this->db->where('subj_type_id', $groupid);
        $this->db->delete('sch_subject_type_iep');
        echo "success..";
	}
	function search(){
		  //----------- Pagination -----------
		   	if(isset($_GET['main_type'])){
			   $subjecttype=$_GET['subjecttype'];
			   $maintype=$_GET['main_type'];

	           $schoolid=$_GET('sid');
	           $schlevelid=$_GET('slid');
	           $rangelevelid=$_GET('rgid');
	           $yearid=$_GET('yid');

			   $data['query']=$this->subjecttypes->searchsubjecttype($subjecttype,$maintype,$schoolid,$schlevelid,$yearid,$rangelevelid);
			   $this->load->view('header');
			   //$this->load->view('school/subjecttype_gep/add');
			   $this->load->view('school/v_subject_group_iep/search',$data);
			   $this->load->view('footer');
		  	}
			if(!isset($_GET['main_type']))
			{
			   $subjecttype=$this->input->post('subjecttype');
			   $maintype=$this->input->post('maintype');

	           $schoolid=$this->input->post('schoolid');
	           $schlevelid=$this->input->post('schlevelid');
	           $rangelevelid=$this->input->post('rangelevelid');
	           $yearid=$this->input->post('yearid');

			   $m=$this->input->post('m');
			   $p=$this->input->post('p');
			   $this->green->setActiveRole($this->input->post('roleid'));
			   if($m!=''){
			        $this->green->setActiveModule($m);
			   }
			   if($p!=''){
			        $this->green->setActivePage($p); 
			   }

		   		$query=$this->subjecttypes->searchsubjecttype($subjecttype,$maintype,$schoolid,$schlevelid,$yearid,$rangelevelid);

			    $i=1;
	            $arrMo=array("TAE","MoEYS");
	            $tr = "";
			    foreach ($query as $subjecttyperow) {
				 	$cp = $subjecttyperow->is_class_participation;
			    	$tr.="<tr>
					          <td align=center width=40>$i</td>
					          <td width=170>$subjecttyperow->subject_type</td>
					          <td>$subjecttyperow->note</td>
					          <td>$subjecttyperow->main_type</td>
					          <td width=170>$subjecttyperow->sch_level</td>
					          <td style='text-align:center;'>".($subjecttyperow->is_group_calc==1?"Yes":"No")."</td>
					          <td>".$subjecttyperow->is_group_calc_percent."</td>
					          <td>".($cp==1?"CP":($cp==0?"SUB":"HW"))."</td>
					          <td width=170 class='hide'>".$arrMo[$subjecttyperow->is_moeys]."</td>
					          <td align=center width=130>";
					          	  //if($this->green->gAction("U")){
					              	$tr.="<a href='javascript:void(0)' id='edit_r' groupid='".$subjecttyperow->subj_type_id."'>
					              		<img src='".site_url('../assets/images/icons/edit.png')."' />
					              </a> |";
					          	 // }
					          	  //if($this->green->gAction("D")){
					              	$tr.="<a href='javascript:void(0)' id='delete_tr' groupid_del='".$subjecttyperow->subj_type_id."'><img src='".site_url('../assets/images/icons/delete.png')."' />
					              	</a>";
					              //} 
					          	$tr.="</td>
					        </tr>";
				        $i++;
			 	}
				$tr.="<tr>
	     			<td colspan='9' id='pgt'>
	     				<div style='text-align:left'>
	     					<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
	     				</div></td>
	    		   </tr>";
			}
			return $tr;
	}
	public function cgetgradelevels(){	
		$schlevelid = $this->input->post("schlevelid");		
		$option_gradlev ="";
		foreach ($this->gradlev->getgradelevels($schlevelid) as $row){
			$option_gradlev .='<option value="'.$row->grade_levelid.'">'.$row->grade_level.'</option>';
		}		
		header("Content-type:text/x-json");
		$arr_success['gradid'] = $option_gradlev;
		echo json_encode($arr_success);
				
	}
	public function cgetschoolyear(){		
		$schoolid = $this->input->post('schoolid');	
		$sch_program = "";
		$schlevelid = $this->input->post("sch_level");		
		$i =0;		
		$option_year ='';

		foreach ($this->schyear->getschoolyear($schoolid,$sch_program,$schlevelid) as $row){
			$option_year .='<option value="'.$row->yearid.'"'.($row->yearid != $this->session->userdata('year')?"":"selected").'>'.$row->sch_year.'</option>';
		}		
		header("Content-type:text/x-json");
		$arr_success['schyear'] = $option_year;
		echo json_encode($arr_success);
				
	}
	public function show_group_type_iep(){
		$is_assessment = $this->input->post("is_ass");
		$sql = "SELECT
					sch_group_type_iep.id,
					sch_group_type_iep.grouptypeid,
					sch_group_type_iep.group_type,
					sch_group_type_iep.is_assessment
					FROM
					sch_group_type_iep
					WHERE 1=1 AND is_assessment = '".$is_assessment."'";
		$sql_grouptypeiep = $this->db->query($sql);
		$opt_type = "<option value=''></option>";
		if($sql_grouptypeiep->num_rows() > 0){
			foreach($sql_grouptypeiep->result() as $row_g){
				$opt_type.= "<option value='".$row_g->grouptypeid."'>".$row_g->group_type."</option>";
			}
		}
		header("Content-type:text/x-json");
		$arr['opt'] = $opt_type;
		echo json_encode($arr);
	}
}