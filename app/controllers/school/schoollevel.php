<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class schoollevel extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/schoollevelmodel','schoollevels');
        $this->load->model('school/program','prog');
		$this->load->library('pagination');		
	}
	public function index()
	{
		$this->load->view('header');
	
		//for pagination is use $data, if not use $data
        $datap['programs']=$this->prog->getprograms();
		$data['query']=$this->schoollevels->getpagination();
		$this->load->view('school/schoollevel/add',$datap);
		$this->load->view('school/schoollevel/view',$data);
		$this->load->view('footer');
	}
	function addnew()
	{
        $data['programs']=$this->prog->getprograms();
		$this->load->view('header');
		$this->load->view('school/schoollevel/schoollevel/add',$data);
		$this->load->view('footer');	
	}
	function filldirector(){
		$key=$_GET['term'];
		$array=array();
		$query="SELECT * 
				FROM sch_emp_profile 
				WHERE CONCAT(last_name,' ',first_name) LIKE '%$key%'";
		foreach ($this->db->query($query)->result() as $row) {
			$array[]=array('value'=>$row->last_name.' '.$row->first_name,
							'id'=>$row->empid);
		}
	    echo json_encode($array);
	}
	
	function edits($level_id)
	{
        $data['programs']=$this->prog->getprograms();
		$row=$this->db->query("SELECT * 
								FROM sch_school_level sch 
								left JOIN sch_emp_profile emp 
								ON(sch.schlv_director=emp.empid) 
								WHERE sch.schlevelid='$level_id'")->row();

		$this->load->view('header');
		$data['query']=$row;
		//$data1['query']=$this->db->get('sch_subject')->result();
		$this->load->view('school/schoollevel/edit',$data);
		$data['query']=$this->schoollevels->getpagination();
		$this->load->view('school/schoollevel/view',$data);
		$this->load->view('footer');	
	}
	function save()
	{
		$levelid=$this->input->post('txtlevel_id');
		$level=$this->input->post('txtlevel');
		$school=$this->input->post('cboschool');
		$directorid=$this->input->post('directorid');
		$order=$this->input->post('txtorder');
		$desc=$this->input->post('txtdesc');
        $programid=$this->input->post('programid');
		$vtc=0;
		if(isset($_POST['chbvtc']))
			$vtc=1;

		if ($directorid==0 || $directorid==''){
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			//redirect("school/schoollevel/?director&m=$m&p=$p");
		}
			
		$count=$this->schoollevels->getvalidateup($level, $school , $levelid);
		
		if ($count!=0){
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("school/schoollevel/?exist&m=$m&p=$p");
		}else{
				$this->db->where('schlevelid',$levelid);

				$data=array(
							'sch_level'=>$level,
							'orders'=>$order,
							'schlv_director'=>$directorid,
		  					'is_vtc'=>$vtc,
							'note'=>$desc,
							'schoolid'=>$school,
                            'programid'=>$programid
				);
				if($levelid!=''){
					$this->db->where('schlevelid',$levelid)->update('sch_school_level',$data);
				}else{
					$this->db->insert('sch_school_level',$data);
					$levelid=$this->db->insert_id();
				}
		
		$this->do_upload($levelid);
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("school/schoollevel/?save&m=$m&p=$p");
		}
	}
	function do_upload($employee_id)
		{
			if(!file_exists('./assets/upload/school/director/'))
			 {
			    if(mkdir('./assets/upload/school/director/',0755,true))
			    {
			        return true;
			    }
			 }
			
			$config['upload_path'] ='./assets/upload/school/director/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '5000';
			$config['file_name']  = "$employee_id.jpg";
			$config['overwrite']=true;
			$config['file_type']='image/jpg';
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());			
			}
			else
			{				
				 	$config2['image_library'] = 'gd2';
                    $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                    $config2['new_image'] = './assets/upload/school/director/';

                    $config2['maintain_ratio'] = TRUE;
                    $config2['create_thumb'] = TRUE;
                    $config2['thumb_marker'] = FALSE;
                    $config2['width'] = 120;
                    $config2['height'] = 180;
                    $config2['overwrite']=true;
                    $this->load->library('image_lib',$config2); 

                    if ( !$this->image_lib->resize()){
	                	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
					}else{
						//unlink('./assets/upload/employees/'.$year.'/'.$employee_id.'.png');
					}
			}

		}

	function deletes($levelid)
	{
		//$this->db->where('category_id',$category_id);
		//$data=array('status'=>0);
		//$this->db->update('category_tbl',$data);
		//redirect('category/');
		$this->db->where('schlevelid', $levelid);
        $this->db->delete('sch_school_level');
        $m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
        redirect("school/schoollevel/?delete&m=$m&p=$p");
	}
	function searchs(){
		  //----------- Pagination -----------
		   if(isset($_GET['s'])){
			   $y=$_GET['y'];
			   $sch=$_GET['sch'];
			   $data['query']=$this->schoollevels->searchsch_levels($y,$sch);
			   $this->load->view('header');
			   $this->load->view('school/schoollevel/add');
			   $this->load->view('school/schoollevel/view',$data);
			   $this->load->view('footer');
		  }
		  if(!isset($_GET['s'])){
		   $level=$this->input->post('level');
		   $schoolid=$this->input->post('school');
		   $is_vtc=$this->input->post('is_vtc');
		   $m=$this->input->post('m');
		   $p=$this->input->post('p');
		   $this->green->setActiveRole($this->input->post('roleid'));
		   if($m!=''){
		        $this->green->setActiveModule($m);
		   }
		   if($p!=''){
		        $this->green->setActivePage($p); 
		   }

		   $query=$this->schoollevels->searchsch_levels($level,$schoolid,$is_vtc);
		    $i=1;
		    foreach ($query as $row) {
		     echo "		      
			        <tr>
			          <td align=center width=40>$i</td>
			          <td width=170>$row->sch_level</td>
			          <td>$row->last_name $row->first_name</td>
			          <td class='hide'>";if ($row->is_vtc=='0'){
			          			echo "No";
			          		}else{
			          			echo "Yes";
			          		}
			          echo "</td>
			          <td width=170>$row->program</td>
			          <td>$row->name</td>
			          <td align=center width=130>";
			          	  if($this->green->gAction("U")){
			              	echo "<a href='".site_url('school/schoollevel/edits/'.$row->schlevelid)."'><img src='".site_url('../assets/images/icons/edit.png')."' />
			              	</a>"; 
			              }
			              	echo " | ";
			              if($this->green->gAction("D")){
			              	echo "<a><img onclick='deletessch_level(event);' rel='$row->schlevelid' src='".site_url('../assets/images/icons/delete.png')."' />
			              	</a>"; 
			              }
			          echo "</td>
			        </tr>" ;
			        $i++;
		 }
		 // echo "<tr>
   //   			<td colspan='12' id='pgt'>
   //   				<div style='text-align:left'>
   //   					<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
   //   				</div></td>
   //  		   </tr>";
		}
	}
}