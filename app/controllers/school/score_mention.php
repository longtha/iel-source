<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class score_mention extends CI_Controller{

		function __construct(){
			parent::__construct();	
			$this->load->model("school/mod_score_mention","sm");
			$this->load->model('school/schoollevelmodel','schlevels');	
			$this->load->library('pagination');	
			$this->thead=array("No"=>'no',
								"Mention EN"=>'mention_en',	
								"Mention KH"=>'mention_kh',
								"School Level"=>'school_level',								 
								 'Action'=>'action'
								);
			$this->idfield="displanid";					
		}
		function index()
		{	
			$data['tdata']=$this->sm->getscoremention();
			$data['schevels']=$this->schlevels->getsch_level();
			$data['idfield']=$this->idfield;		
			$data['thead']=	$this->thead;
			$data['page_header']="Score Mention";			
			$this->parser->parse('header', $data);
			$this->parser->parse('school/scoremention/score_mention_list', $data);
			$this->parser->parse('footer', $data);
		}

		// validate =========
		function vaidate($menid, $schlevelid, $mention, $mention_kh){
	        $where = '';
	        if($menid - 0 > 0){
	            $where .= "AND m.menid != '{$menid}' ";
	        }
	        
	        $c = $this->db->query("SELECT
	                                    COUNT(*) AS c
	                                FROM
	                                    sch_score_mention AS m
	                                WHERE
	                                    m.schlevelid = '{$schlevelid}' AND m.mention = '{$mention}' AND m.mention_kh = '{$mention_kh}' {$where} ")->row()->c - 0;
	        return $c;
	    }

		// save =========
		function save(){
			$men_id = $this->input->post('men_id');
			$schlevel = $this->input->post('schlevel');
			$arr = $this->input->post('arr');
			$i = 0;

			if(COUNT($arr) > 0){
				foreach ($arr as $row){
					if($schlevel != '' || trim($row['mentionen']) != '' || trim($row['mentionkh']) != ''){
						if($this->vaidate($row['menid'], $schlevel, $row['mentionen'], $row['mentionkh']) > 0){
							$i = 2;
							break;
						}														
					}
				}
			}

			if(COUNT($arr) > 0){
				foreach ($arr as $row){
					if($schlevel != '' || trim($row['mentionen']) != '' || trim($row['mentionkh']) != ''){
						if($i - 0 != 2){
							$data = array('mention' => $row['mentionen'],
											'mention_kh' => $row['mentionkh'],
											'schlevelid' => $schlevel
										);

							if($row['menid'] - 0 > 0){
								$i = $this->db->update('sch_score_mention', $data, array('menid' => $row['menid']));
							}else{
								$i = $this->db->insert('sch_score_mention', $data);
							}
						}														
					}
				}
			}

			header('Content-Type: application/json; charset=utf-8');
        	echo json_encode($i);
		}

		function edit(){
			$men_id=$this->input->post('menid');
			$row=$this->db->where('menid',$men_id)->get('sch_score_mention')->row();
			header("Content-type:text/x-json");
			echo json_encode($row);
		}
		function validate(){
			$men_id=$this->input->post('men_id');
			$men_en=$this->input->post('mentionen');
			$men_kh=$this->input->post('mentionkh');
			$schlevel=$this->input->post('schlevel');
			$this->db->select('count(menid) as count')
					->from('sch_score_mention')
					->where('mention',$men_en)
                    ->where('mention_kh',$men_kh)
					->where('schlevelid',$schlevel);
			if($men_id!='')
				$this->db->where_not_in('menid',$men_id);
            $count=$this->db->get()->row()->count;
			echo $count;
		}
		function delete($menid){
			$this->db->where('menid',$menid)->delete('sch_score_mention');
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("school/score_mention?m=$m&p=$p");
		}
		function search(){
			if(isset($_GET['m'])){
					$schlevelid=$_GET['schlevelid'];
					$m=$_GET['m'];
					$p=$_GET['p'];
					$sort_num=$_GET['s_num'];
					$data['tdata']=$this->sm->search($schlevelid,$sort_num,$m,$p);
					$data['idfield']=$this->idfield;		
					$data['thead']=	$this->thead;
					$data['page_header']="Score Mention";			
					$this->parser->parse('header', $data);
					$this->parser->parse('school/scoremention/score_mention_list', $data);
					$this->parser->parse('footer', $data);
			}else{
					
					$schlevelid=$this->input->post('schlevelid');
					$sort_num=$this->input->post('s_num');
					$i=1;
					$m=$this->input->post('m');
					$p=$this->input->post('p');
					$this->green->setActiveRole($this->input->post('roleid'));
					if($m!=''){
				        $this->green->setActiveModule($m);
				    }
				    if($p!=''){
				        $this->green->setActivePage($p); 
				    }
					foreach ($this->sm->search($schlevelid,$sort_num,$m,$p) as $row) {

						echo "<tr>
								<td>".str_pad($i,2,"0",STR_PAD_LEFT)."</td>
								<td>".$row->mention."</td>
								<td>".$row->mention_kh."</td>
								<td>".$row->sch_level."</td>
								<td class='remove_tag'>";
								if($this->green->gAction("D")){
									echo "<a>
									 		<img rel=".$row->menid." onclick='deletes(event);' src='".site_url('../assets/images/icons/delete.png')."'/>
									 	</a>";
								}
								if($this->green->gAction("U")){
									echo "<a>
									 		<img  rel=".$row->menid." onclick='edit(event);' src='".site_url('../assets/images/icons/edit.png')."'/>
									 	</a>";
								}
							echo "</td>
						</tr>
						 ";
						 $i++;
					}
					echo "<tr class='remove_tag'>
								<td colspan='12' id='pgt'>
									<div style='margin-top:20px; width:10%; float:left;'>
									Display : <select id='sort_num'  onchange='search(event);' style='padding:5px; margin-right:0px;'>";
													$num=10;
													for($i=0;$i<10;$i++){?>
														<option value="<?php echo $num ;?>" <?php if($num==$sort_num) echo 'selected';?> ><?php echo $num;?></option>
														<?php $num+=10;
													}
												echo "</select>
									</div>
									<div style='text-align:center; verticle-align:center; width:90%; float:right;'>
										<ul class='pagination' style='text-align:center'>
										 ".$this->pagination->create_links()."
										</ul>
									</div>

								</td>
							</tr> ";
			}
			
		}		
}