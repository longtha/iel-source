<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_time_table extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        //$this->load->model('school/schoollevelmodel','sl');
        //$this->load->model('school/gradelevelmodel','gm');
        $this->load->model('school/m_time_table', 'tt');
        $this->load->helper(array('form', 'url'));
				$this->load->model('school/schoolinformodel','sch');
  }

	public function index()
	{
    $data['header']="Calendar";
    $this->load->view('header');

    $this->load->view('school/v_time_table', $data);
    $this->load->view('footer');
	}

	
  public function reload(){
      $parameters = array(
          'school_id' => $this->input->post("school"),
          'school_level_id' => $this->input->post("school_level"),
          'academic_year_id' => $this->input->post("academic_year"),
          'grade_level_id' => $this->input->post("grade_level"),
          'class_id' => $this->input->post("school_class")
      );
      $isParameter = $this->input->post("parameter");
      
      $timeTables = $this->tt->reload($parameters, $isParameter);
      
      header("Content-type:text/x-json");
      echo json_encode($timeTables);
      exit();
  }
  
 
  public function filter(){
      $parameters2=json_decode($_POST['data']);
      
      $timetable2 = $this->tt->filter($parameters2);
      
      header("Content-type:text/x-json");
      echo json_encode($timetable2);
  }
 
  public function save() {

    $timeTables = array (
      'school_id' => $this->input->post("school"),
      'school_level_id' => $this->input->post("school-level"),
      'academic_year_id' => $this->input->post("academic-year"),
			'grade_level_id' => $this->input->post("grade-level"),
			'class_id' => $this->input->post("class"),
      'title' => $this->input->post("title"),
      'description' => $this->input->post("note"),
      'attach_file' => 'no file',
      'transaction_date' => date("Y-m-d"),
      'type' => 44,
      'type_no' => $this->green->nextTran(43, "School Calendar"),
      'created_by' => $this->session->userdata('userid')
       
    );

    $upload_file = $this->do_upload("file");

    if (!array_key_exists("error", $upload_file)) {
			$file_type = explode('.', $upload_file["file_name"]);
      $timeTables["attach_file"] = $upload_file["file_name"];
			$timeTables["file_type"] = $file_type[1];

      $result = $this->tt->save($timeTables);

			$result_function = $result == 1 ? "window.parent.reload(1)" : "window.parent.error()";

			echo <<<SCRIPT
							<script type="text/javascript">
								$result_function;
							</script>
SCRIPT;
    }else{
        echo <<<SCRIPT
							<script type="text/javascript">
                                alert("Please check attachment!");
							</script>
SCRIPT;
    }

    exit();
  }

	public function delete() {
		$id = $this->input->post("id");

		$delete = $this->tt->delete($id);

		echo json_encode($delete);
	  exit();
	}


  public function do_upload($file) {
    $config['upload_path']   = './assets/upload/time_tables/';
    $config['allowed_types'] = 'gif|jpg|png|pdf';
    $config['max_size']      = 5120;
    $config['max_width']     = 1024;
    $config['max_height']    = 768;
    $this->load->library('upload', $config);

    $upload_data = array();

    if (!$this->upload->do_upload($file)) {
      $error = array('error' => $this->upload->display_errors());
      $upload_data = $error;
    }

    else {
      $upload_data = $this->upload->data();
    }

    return $upload_data;
  }

	public function getSchoolLevel() {
		$schoolId = $this->input->post("school");

		$schoolLevels = $this->tt->getSchoolLevel($schoolId);

		echo json_encode($schoolLevels);
    exit();
	}
	function get_class() {
	    $school_level = $this->input->post("school_level");
	    $grade_level = $this->input->post("grade_level");
	    $class = $this->tt->callClass($school_level, $grade_level);
	    
	    echo json_encode($class);
	    exit();
	}
}
