<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_calendar extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        //$this->load->model('school/schoollevelmodel','sl');
        //$this->load->model('school/gradelevelmodel','gm');
        $this->load->model('school/m_calendar', 'sc');
        $this->load->helper(array('form', 'url'));
				$this->load->model('school/schoolinformodel','sch');
  }

	public function index()
	{
    $data['header']="Calendar";
    $this->load->view('header');

    $this->load->view('school/v_calendar', $data);
    $this->load->view('footer');
	}

  public function reload() {
		$parameters = array(
			'school_id' => $this->input->post("school"),
			'school_level_id' => $this->input->post("school_level"),
			'academic_year_id' => $this->input->post("academic_year")
		);
		$isParameter = $this->input->post("parameter");

    $calendars = $this->sc->reload($parameters, $isParameter);

    header("Content-type:text/x-json");
    echo json_encode($calendars);
    exit();
  }
  public function filter() {
      
      $parameters2=json_decode($_POST['data']);
      
      $calendar2 = $this->sc->filter($parameters2);
      
      header("Content-type:text/x-json");
      echo json_encode($calendar2);
         
  }
  public function save() {
      
      $yearid=$this->input->post("academic-year");
      
      $sql_get_accademic_year='SELECT yearid, sch_year, from_date, to_date, schoolid, schlevelid, programid 
                                FROM `sch_school_year`
                                WHERE yearid ="'.$yearid.'"';
   
      
      $row_academic=$this->db->query($sql_get_accademic_year)->row();
      
    $calendar = array (
      'school_id' => $this->input->post("school"),
      'school_level_id' => $this->input->post("school-level"),
      'academic_year_id' => $yearid,
      'title' => $this->input->post("title"),
      'description' => $this->input->post("note"),
      'attach_file' => 'no file',
      'transaction_date' => date("Y-m-d"),
      'type' => 43,
      'type_no' => $this->green->nextTran(43, "School Calendar"),
      'created_by' => $this->session->userdata('userid'),
      'from_date'=>$row_academic->from_date,
      'to_date'=>$row_academic->to_date
    );

    $upload_file = $this->do_upload("file");

    if (!array_key_exists("error", $upload_file)) {
			$file_type = explode('.', $upload_file["file_name"]);
            $calendar["attach_file"] = $upload_file["file_name"];
			$calendar["file_type"] = $file_type[1];

      $result = $this->sc->save($calendar);

		
			echo <<<SCRIPT
							<script type="text/javascript">
                                window.parent.reload(1);
							</script>
SCRIPT;
    }else{
        echo <<<SCRIPT
							<script type="text/javascript">
                                alert("Please check attachment!");
							</script>
SCRIPT;
			}

    exit();
  }

	public function delete() {
		$id = $this->input->post("id");

		$delete = $this->sc->delete($id);

		echo json_encode($delete);
	  exit();
	}

  public function getSubject() {
    $schoolLevel = $this->input->post("school_level");
    $programId = $this->input->post("program_id");

    $subject = $this->sc->getSubject($schoolLevel, $programId);

		echo json_encode($subject);
    exit();
  }

  public function do_upload($file) {
    $config['upload_path']   = './assets/upload/school_calendars/';
    $config['allowed_types'] = 'jpg|png|pdf';
    $config['max_size']      = 5120;
    $config['max_width']     = 1024;
    $config['max_height']    = 768;
    $this->load->library('upload', $config);

    $upload_data = array();

    if (!$this->upload->do_upload($file)) {
      $error = array('error' => $this->upload->display_errors());
      $upload_data = $error;
    }

    else {
      $upload_data = $this->upload->data();
    }

    return $upload_data;
  }

	public function getSchoolLevel() {
		$schoolId = $this->input->post("school");

		$schoolLevels = $this->sc->getSchoolLevel($schoolId);

		echo json_encode($schoolLevels);
    exit();
	}
	public function getProgram() {
	
	    $program = $this->sc->getProgram();
	    
	    echo json_encode($program);
	    exit();
	}
}
