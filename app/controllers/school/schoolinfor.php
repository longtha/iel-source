<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class schoolinfor extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/schoolinformodel','schoolinfors');
		$this->load->library('pagination');		
	}
	public function index()
	{
		$this->load->view('header');
		//school/schoolinfor is call view
	
		//for pagination is use $data, if not use $data
		$data['query']=$this->schoolinfors->getpagination();
		$this->load->view('school/schoolinfor/add');
		$this->load->view('school/schoolinfor/view',$data);
		$this->load->view('footer');
	}
	function addnew()
	{
		$this->load->view('header');
		//school/schoolyear is call view
		$this->load->view('school/schoolinfor/schoolinfor/add');
		$this->load->view('footer');	
	}
	function do_upload($id)
	{
		$config['upload_path'] ='./assets/upload/school/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name']  = "$id.png";
		$config['overwrite']=true;
		$config['file_type']='image/png';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());			
		}
		else
		{				
			//$data = array('upload_data' => $this->upload->data());			
			$config2['image_library'] = 'gd2';
                    $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                    $config2['new_image'] = './assets/upload/school/';
                    $config2['maintain_ratio'] = TRUE;
                    $config2['create_thumb'] = TRUE;
                    $config2['thumb_marker'] = '_thumb';
                    $config2['width'] = 120;
                    $config2['height'] = 180;
                    $config2['overwrite']=true;
                    $this->load->library('image_lib',$config2); 

                    if ( !$this->image_lib->resize()){
	                	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
					}else{
						unlink('./assets/upload/school/'.$id.'.png');
						$m='';
						$p='';
						if(isset($_GET['m'])){
					    	$m=$_GET['m'];
					    }
					    if(isset($_GET['p'])){
					        $p=$_GET['p'];
					    }
						redirect("school/schoolinfor/?m=$m&p=$p");
					}	
		}
	}

	function saves()
	{
		$name=$this->input->post('txtname');
		$opendate=date_create($this->input->post('txtdate'));

		$count=$this->schoolinfors->getvalidate($name);
		if ($count!=0){
			//--- exist is variable for check it exist or not ---
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect (site_url("school/schoolinfor/?exist&m=$m&p=$p"));
		}else{
			$data=array(
			  		'name'=>$name,
			  		'address'=>$this->input->post('txtaddress'),
					'contact_person'=>$this->input->post('txtcontactperson'),
					'contact_tel'=>$this->input->post('txtcontacttel'),
					'email'=>$this->input->post('txtemail'),
					'website'=>$this->input->post('txtwebsite'),
					'slogan'=>$this->input->post('txtslogan'),
					'vision'=>$this->input->post('txtvision'),
					'mission'=>$this->input->post("txtmission"),
					'gaol'=>$this->input->post('txtgoal'),
					'open_since'=>date_format($opendate, 'Y-m-d'),
					'is_active'=>$this->input->post('chbactive')

			  );	
			$this->db->insert('sch_school_infor',$data);
			$id=$this->db->insert_id();			
			$this->do_upload($id);
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
				redirect (site_url("school/schoolinfor/?save&m=$m&p=$p"));	
		}
	}
	function edits($school_id)
	{
		$row=$this->schoolinfors->getschinfor_row($school_id);
		$this->load->view('header');
		$data['query']=$row;
		//school/schoolinfor is call view
		//$data1['query']=$this->db->get('sch_subject')->result();
		$this->load->view('school/schoolinfor/edit',$data);
		$data['query']=$this->schoolinfors->getpagination();
		$this->load->view('school/schoolinfor/view',$data);
		$this->load->view('footer');	
	}
	function updates()
	{
		$schoolid=$this->input->post('txtschoolid');
		$name=$this->input->post('txtname');
		$date=date_create($this->input->post('txtdate'));

		$count=$this->schoolinfors->getvalidateup($name, $schoolid);
		
			if ($count!=0){
				$m='';
				$p='';
				if(isset($_GET['m'])){
			    	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }
				redirect (site_url("school/schoolinfor/?exist&m=$m&p=$p"));
			}else{
					$this->db->where('schoolid',$schoolid);

					$data=array(
								'name'=>$name,
								'address'=>$this->input->post('txtaddress'),
								'contact_person'=>$this->input->post('txtcontactperson'),
								'contact_tel'=>$this->input->post('txtcontacttel'),
								'email'=>$this->input->post('txtemail'),
								'website'=>$this->input->post('txtwebsite'),
								'slogan'=>$this->input->post('txtslogan'),
								'is_active'=>$this->input->post('chbactive'),
								'vision'=>$this->input->post('txtvision'),
								'mission'=>$this->input->post('txtmission'),
								'gaol'=>$this->input->post('txtgoal'),
								'open_since'=>date_format($date, 'Y-m-d'),
					);
					$this->db->update('sch_school_infor',$data);
					$this->do_upload($schoolid);
					$m='';
					$p='';
					if(isset($_GET['m'])){
				    	$m=$_GET['m'];
				    }
				    if(isset($_GET['p'])){
				        $p=$_GET['p'];
				    }
					redirect (site_url("school/schoolinfor/?edit&m=$m&p=$p"));
			//echo'true';	
			}
	}
	function deletes($school_id)
	{
		$this->db->where('schoolid',$school_id);
		$data=array('is_active'=>0);
		$this->db->update('sch_school_infor',$data);
		//unlink('./assets/upload/'.$id.'.png');
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
        redirect("school/schoolinfor/?delete&m=$m&p=$p");
	}
	function searchs(){
		  //----------- Pagination -----------
		   if(isset($_GET['s'])){
			   $y=$_GET['y'];
			   //$sch=$_GET['sch'];
			   $data['query']=$this->schoolinfors->searchsch_infors($y);

			   $this->load->view('header');
			   $this->load->view('school/schoolinfor/add');
			   $this->load->view('school/schoolinfor/view',$data);
			   $this->load->view('footer');
		  }

		  if(!isset($_GET['s'])){
		   $name=$this->input->post('schname');
		   $showall=$this->input->post('showall');

		   $m=$this->input->post('m');
		   $p=$this->input->post('p');
		   $this->green->setActiveRole($this->input->post('roleid'));
		   if($m!=''){
		        $this->green->setActiveModule($m);
		   }
		   if($p!=''){
		        $this->green->setActivePage($p); 
		   }

		   $query=$this->schoolinfors->searchsch_infors($name,$showall);
		    $i=1;
		    foreach ($query as $row) {

		    $pendate=date_create($row->open_since);

		     echo "		      
			        <tr>
			          <td align=center width=40>$i</td>
			          <td>$row->name</td>
			          <td width=100>$row->contact_tel</td>
			          <td width=100>$row->email</td>
			          <td width=100>$row->website</td>
			          <td width=100>$row->gaol</td>
			          <td width=100>".date_format($pendate, 'd-m-Y')."</td>
			          <td width=100>$row->contact_person</td>
			          <td>";
				          if ($row->is_active==1){ 
				          			echo 'active';
				          		}else{ 
				          			echo 'inactive';}
				      echo "</td>";
			          echo "<td align=center width=130>";
			          	   if($this->green->gAction("U")){
			              		echo "<a href='".site_url('school/schoolinfor/edits/'.$row->schoolid)."?m=$m&p=$p"."'>
			              		<img src='".site_url('../assets/images/icons/edit.png')."' />
			              		</a> | ";
			              	}
			              	if($this->green->gAction("D")){
			              		echo "<a><img onclick='deletessch_infor(event);' rel='$row->schoolid' src='".site_url('../assets/images/icons/delete.png')."' />
			              			 </a>";
			              	} 
			          echo "</td>
			        </tr>" ;
			        $i++;

		 }
		 //------- Create Pagination --------------
		 // echo "<tr>
   	     //   		<td colspan='12' id='pgt'>
         //   			<div style='text-align:left'>
         //   			<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
         //   		</div></td>
         //  	 </tr>";
         //------- End Pagination -----------------
		}
	}
}