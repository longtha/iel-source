<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class studentpartner extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/studentpartnermodel','spm');
		$this->load->library('pagination');		
	}
	public function index()
	{
		$this->load->view('header');
	
		$data['query']=$this->spm->getpagination();
		$this->load->view('school/studentpartner/add');
		$this->load->view('school/studentpartner/view',$data);
		$this->load->view('footer');
	}
	function addnew()
	{
		$this->load->view('header');
		$this->load->view('school/studentpartner/studentpartner/add');
		$this->load->view('footer');	
	}
	function saves()
	{
		date_default_timezone_set("Asia/Bangkok");
		$cname=$this->input->post('txtcname');
		$cperson=$this->input->post('txtcperson');
		$btype=$this->input->post('txtbtype');
		$pdate=date_create($this->input->post('txtpdate'));
		$tel=$this->input->post('txttel');
		$email=$this->input->post('txtemail');
		$website=$this->input->post('txtwebsite');
		$address=$this->input->post('txtaddress');

		$count=$this->spm->getvalidate($cname);
		if ($count!=0){
			$this->load->view('header');
			$data['exist']="<p style='color:red;'>Your data has already exist...!</p>";
			$data['query']=$this->spm->getpagination();
			$this->load->view('school/studentpartner/add',$data);
			$this->load->view('school/studentpartner/view',$data);
			$this->load->view('footer');
		}else{
			$data=array(
			  		'company_name'=>$cname,
			  		'contact_name'=>$cperson,
			  		'business_type'=>$btype,
			  		'partner_since'=>date_format($pdate, 'Y-m-d'),
			  		'tel'=>$tel,
					'email'=>$email,
					'website'=>$website,
					'address'=>$address,
					'created_by'=>$this->session->userdata('userid'),
					'created_date'=>date('Y-m-d H:i:s'),
					'modified_by'=>$this->session->userdata('userid'),
					'modified_date'=>date('Y-m-d H:i:s'),
			  );	
				$this->db->insert('sch_stud_partner',$data);

				$m='';
				$p='';
				if(isset($_GET['m'])){
			    	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }
				redirect("school/studentpartner/?save&m=$m&p=$p");
		}
	}
	function edits($pid)
	{
		$row=$this->spm->getparnerrow($pid);
		$this->load->view('header');
		$data['query']=$row;

		$this->load->view('school/studentpartner/edit',$data);
		$data['query']=$this->spm->getpagination();
		$this->load->view('school/studentpartner/view',$data);
		$this->load->view('footer');	
	}
	function updates()
	{
		$cname=$this->input->post('txtcname');
		$cperson=$this->input->post('txtcperson');
		$btype=$this->input->post('txtbtype');
		$pdate=date_create($this->input->post('txtpdate'));
		$tel=$this->input->post('txttel');
		$email=$this->input->post('txtemail');
		$website=$this->input->post('txtwebsite');
		$address=$this->input->post('txtaddress');

		$pid=$this->input->post('txtpid');

		$count=$this->spm->getvalidateup($cname,$pid);
		
		if ($count!=0){
			$this->load->view('header');
			$data['exist']="<p style='color:red;'>Your data has already exist...!</p>";
			$data['query']=$this->spm->getpagination();
			$this->load->view('school/studentpartner/add',$data);
			$this->load->view('school/studentpartner/view',$data);
			$this->load->view('footer');
		}else{
				$this->db->where('partnerid',$pid);

				$data=array(
							'company_name'=>$cname,
					  		'contact_name'=>$cperson,
					  		'business_type'=>$btype,
					  		'partner_since'=>date_format($pdate, 'Y-m-d'),
					  		'tel'=>$tel,
							'email'=>$email,
							'website'=>$website,
							'address'=>$address,
							'modified_by'=>$this->session->userdata('userid'),
							'modified_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->update('sch_stud_partner',$data);

				$m='';
				$p='';
				if(isset($_GET['m'])){
			    	$m=$_GET['m'];
			    }
			    if(isset($_GET['p'])){
			        $p=$_GET['p'];
			    }
				redirect("school/studentpartner/?edit&m=$m&p=$p");
		}
	}
	function deletes($pid)
	{
		$this->db->where('partnerid', $pid);
        $this->db->delete('sch_stud_partner');

        $m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
        redirect("school/studentpartner/?delete&m=$m&p=$p");
	}
	function searchs(){
		  //----------- Pagination -----------
		   if(isset($_GET['s'])){
			   $y=$_GET['y'];
			   $data['query']=$this->spm->searchs($y);
			   $this->load->view('header');
			   $this->load->view('school/studentpartner/add');
			   $this->load->view('school/studentpartner/view',$data);
			   $this->load->view('footer');
		  }
		  if(!isset($_GET['s'])){
		    $cname=$this->input->post('cname');

		    $m=$this->input->post('m');
			$p=$this->input->post('p');
			$this->green->setActiveRole($this->input->post('roleid'));
			if($m!=''){
		        $this->green->setActiveModule($m);
		    }
		    if($p!=''){
		        $this->green->setActivePage($p); 
		    }

		   $query=$this->spm->searchs($cname);
		    $i=1;
		    foreach ($query as $row) {
		     	echo "		      
			        <tr>
			          <td align=center width=40>$i</td>
			          <td width=170>$row->company_name</td>
			          <td width=170>$row->contact_name</td>
			          <td>$row->tel</td>
					  <td>$row->email</td>
			          <td>$row->address</td>
			          <td align=center width=130>";
			          	  if($this->green->gAction("U")){
			              	echo "<a href='".site_url('school/studentpartner/edits/'.$row->partnerid)."?m=$m&p=$p"."'><img src='".site_url('../assets/images/icons/edit.png')."' />
			                     </a> |";
			              }
			              if($this->green->gAction("D")){
			                echo "<a><img onclick='deletes(event);' rel='$row->partnerid' src='".site_url('../assets/images/icons/delete.png')."' />
			                	  </a>";
			              }
			          echo "</td>
			        </tr>";
			        $i++;
		    }
		 echo "<tr>
     			<td colspan='7' id='pgt'>
     				<div style='text-align:left'>
     					<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
     				</div></td>
    		   </tr>";
		}
	}
}