<?php if(!defined('BASEPATH'))	exit('No direct script access allowed!');
class Schedule extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('school/Modschedule','sched');
	}
	public function views()
	{
		$datahead['page_header']="Schedule List";
		$this->load->view('header',$datahead);
		$this->load->view('school/timetable/schedule_view');
		$this->load->view('footer');
	}
	public function scheduleprocess(){
		$variables = $_POST['variables'];
		if(!empty($variables['slid']) || $variables['slid']!="")
			$variables['slid']=$variables['slid'];
		else
			$variables['slid']=1;

		$row='';
		$i=1;
		foreach($this->sched->setClassListview($variables['yid'],$variables['slid']) as $rcsa)
		{	

			$row.=$this->sched->scheduleHeader($this->sched->setSchoolName(),$rcsa->class_name,$rcsa->title,$rcsa->year);

			$row.="<table align='center' style='width:100%' border='0'>
						<tbody id='getdata'>
							<tr>
								<td>";
								$row.=$this->sched->getBlockTimeTable($this->sched->setClassInfo($rcsa->classid),$rcsa->classid,$rcsa->year,$rcsa->transno);
						$row.="</td>
							</tr>
						</tbody>
					</table>";
			if(($i%1)==0){
				$row.='<p style="page-break-after: always;"></p>';
			}
			$i++;
		};
		$arr = Array();
		header('Content-type:text/x-json');
		$arr['row']=$row;
		echo json_encode($arr);
		die();
	}
}