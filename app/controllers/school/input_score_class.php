<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require('fpdf/fpdf.php');
// require('fpdf/PDF_HTML.php');

class input_score_class extends CI_Controller {
	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/tearcherclassmodel','tclass');
		$this->load->library('pagination');
	}
	public function index()
	{
		
		//$yearid=$this->session->userdata('year');
		//$data_count = $this->tclass->getteacher('',$yearid);

		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }

		//$data['yearid'] = $this->select_y();
		$data['schoolid'] = $this->select_sch();
		$data['schoolid_lav'] = $this->select_sch_lav();
		$data['exam_type'] = $this->exam_type($id="");
		$this->load->view('header');
		$active_user = $this->green->getActiveUser();
		$data['user_act'] = $active_user;
		$this->load->view('school/score/vinput_score_class',$data);
		$this->load->view('footer');

	}
	function show_report(){
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		$data['schoolid'] = $this->select_sch();
		$data['schoolid_lav'] = $this->select_sch_lav();
		$data['exam_type'] = $this->exam_type($id="");
		$this->load->view('header');
		$this->load->view('school/score/vreport_input_score_gep',$data);
		$this->load->view('footer');
	}
	function show_report_midterm(){
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		$data['schoolid'] = $this->select_sch();
		$data['schoolid_lav'] = $this->select_sch_lav();
		$data['exam_type'] = $this->exam_type($id=1);
		$this->load->view('header');
		$this->load->view('school/score/vreport_mid_term_gep',$data);
		$this->load->view('footer');
	}
	function show_report_term(){
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		$data['schoolid'] = $this->select_sch();
		$data['schoolid_lav'] = $this->select_sch_lav();
		$data['exam_type'] = $this->exam_type($id="");
		$this->load->view('header');
		$this->load->view('school/score/vreport_term_result_gep',$data);
		$this->load->view('footer');
	}
	function select_y(){
		$para_obj  = $this->input->post("para_obj");
		$sql_y = $this->db->query("SELECT
									sch_school_year.yearid,
									sch_school_year.sch_year,
									CONCAT(YEAR(STR_TO_DATE(from_date,'%Y')),'-',YEAR(STR_TO_DATE(to_date,'%Y'))) as st_y
									FROM
									sch_school_year
									WHERE 1=1 AND schlevelid='".$para_obj['schoollavel']."'
									");
		$opt_y = "<option value=''></option>";
		if($sql_y->num_rows() > 0){
			foreach($sql_y->result() as $row_y){
				$opt_y.="<option value='".$row_y->yearid."' st_to_date='".$row_y->st_y."'>".$row_y->sch_year."</option>";
			}
		}
		echo $opt_y;
	}
	function select_sch(){
		$sql_sch = $this->db->query("SELECT
										sch_school_infor.schoolid,
										sch_school_infor.`name`
										FROM
										sch_school_infor
									");
		$opt_sch = "";
		if($sql_sch->num_rows() > 0){
			foreach($sql_sch->result() as $row_sch){
				$opt_sch.="<option value='".$row_sch->schoolid."'>".$row_sch->name."</option>";
			}
		}
		return $opt_sch;
	}
	function select_sch_lav(){
		$sql_sch_schlav = $this->db->query("SELECT
										sch_school_level.schlevelid,
										sch_school_level.sch_level,
										sch_school_level.schoolid
										FROM
										sch_school_level
										WHERE programid=3
										");
		$opt_sch_lav = "<option value=''></option>";
		if($sql_sch_schlav->num_rows() > 0){
			foreach($sql_sch_schlav->result() as $row_sch_l){
				$opt_sch_lav.="<option value='".$row_sch_l->schlevelid."'>".$row_sch_l->sch_level."</option>";
			}
		}
		return $opt_sch_lav;
	}
	function grade_lavel(){
		$para_obj        = $this->input->post("para_obj");
		$sql_gradelav = $this->db->query("SELECT
											sch_grade_level.grade_levelid,
											sch_grade_level.grade_level,
											sch_grade_level.schoolid,
											sch_grade_level.next_grade_level,
											sch_grade_level.schlevelid
											FROM
											sch_grade_level
											WHERE 1=1 AND schoolid='".$para_obj['schoolid']."' 
											AND schlevelid='".$para_obj['schoollavel']."'
										");
		$opt_grade_lavel = "<option value=''></option>";
		if($sql_gradelav->num_rows() > 0){
			foreach($sql_gradelav->result() as $row_gr){
				$opt_grade_lavel.="<option value='".$row_gr->grade_levelid."' att_grandlavel='".$row_gr->grade_level."'>".$row_gr->grade_level."</option>";
			}
		}
		echo $opt_grade_lavel;
	}
	function grade_class(){
		$para_obj        = $this->input->post("para_obj"); 
		$sql_class = $this->db->query("SELECT
											sch_class.classid,
											sch_class.class_name,
											sch_class.grade_levelid,
											sch_class.grade_labelid,
											sch_class.schoolid,
											sch_class.schlevelid,
											sch_class.is_pt
											FROM
											sch_class
											WHERE 1=1 AND schoolid='".$para_obj['schoolid']."' 
											AND schlevelid='".$para_obj['schoollavel']."'
											AND grade_levelid='".$para_obj['gradelavel']."'
										");
		$opt_class = "<option value=''>All</option>";
		if($sql_class->num_rows() > 0){
			foreach($sql_class->result() as $row_class){
				$opt_class.="<option value='".$row_class->classid."' att_class='".$row_class->class_name."' is_pt='".$row_class->is_pt."'>".$row_class->class_name."</option>";
			}
		}
		echo $opt_class;
	}
	function exam_type($id){
		$wh = "";
		if($id != ""){
			$wh = " AND examtypeid=1";
		}
		$sql_exam_type = $this->db->query("SELECT
												sch_student_examtype_gep.examtypeid,
												sch_student_examtype_gep.exam_test,
												sch_student_examtype_gep.maintype,
												sch_student_examtype_gep.is_assissment
												FROM
												sch_student_examtype_gep
												WHERE 1=1 {$wh}
										");
		$opt_exam = "<option value=''></option>";
		if($sql_exam_type->num_rows() > 0){
			foreach($sql_exam_type->result() as $row_exam){
				$opt_exam.="<option value='".$row_exam->examtypeid."' att_label='".$row_exam->exam_test."' is_assigment='".$row_exam->is_assissment."'>".$row_exam->exam_test."</option>";
			}
		}
		return $opt_exam;
	}
	function sub_exam_type(){
		$para_obj        = $this->input->post("para_obj"); 
		$sql_sub_exam = $this->db->query("SELECT
											sch_school_term.termid,
											sch_school_term.term,
											sch_school_term.term_kh,
											sch_school_term.programid,
											sch_school_term.yearid,
											sch_school_term.schoolid,
											sch_school_term.schlevelid,
											sch_school_term.semesterid,
											sch_school_term.start_date,
											sch_school_term.end_date
											FROM
											sch_school_term
											WHERE 1=1
											AND schoolid='".$para_obj['schoolid']."' AND schlevelid='".$para_obj['schoollavel']."'
											AND yearid='".$para_obj ['yearsid']."'
										");
		$opt_sub_exam = "<option value=''></option>";
		if($sql_sub_exam->num_rows() > 0){
			foreach($sql_sub_exam->result() as $row_subexam){
				$opt_sub_exam.="<option value='".$row_subexam->termid."' label_term='".$row_subexam->term."' fdate='".($this->green->convertSQLDate($row_subexam->start_date))."' tdate='".$this->green->convertSQLDate($row_subexam->end_date)."'>".$row_subexam->term."</option>";
			}
		}
		echo $opt_sub_exam;
	}
	function subject_group(){
		$para_obj = $this->input->post("para_obj"); 
		//$is_assigment = $this->input->post("is_assigment");
		$sql_sub  = $this->db->query("SELECT
									sch_subject_type_gep.subj_type_id,
									sch_subject_type_gep.subject_type,
									sch_subject_type_gep.is_class_participation
									FROM
									sch_subject_type_gep
									WHERE 1=1 
									AND schoolid='".$para_obj['schoolid']."' 
									AND schlevelid='".$para_obj['schoollavel']."'
									");
		$opt_subject_group = "<option value=''></option>";
		if($sql_sub->num_rows() > 0){
			foreach($sql_sub->result() as $row_subj_g){
				$opt_subject_group.="<option value='".$row_subj_g->subj_type_id."' is_class_participation='".$row_subj_g->is_class_participation."'>".$row_subj_g->subject_type."</option>";
			}
		}
		echo $opt_subject_group;
	}
	function sub_subject_group(){
		//$subject_group = $this->input->post("subject_group");
		$obj_para     = $this->input->post("obj_para");
		$where_couse = ""; 
		if($obj_para['course'] == 0){
			$where_couse = " AND is_core=1";
		}else{
			$where_couse = " AND is_skill=1";
		}
		$sql_sub_subj  = $this->db->query("SELECT
												sch_subject_gep.subjectid,
												sch_subject_gep.`subject`,
												sch_subject_gep.short_sub

												FROM
												sch_subject_gep
											WHERE 1=1 {$where_couse}
											AND subj_type_id='".$obj_para['subject_group']."'");
		$opt_sub_subject = "<option value=''></option>";
		if($sql_sub_subj->num_rows() > 0){
			foreach($sql_sub_subj->result() as $row_sub_subj){
				$opt_sub_subject.="<option value='".$row_sub_subj->subjectid."' att_label='".$row_sub_subj->short_sub."'>".$row_sub_subj->subject."</option>";
			}
		}
		echo $opt_sub_subject;
	}

	function search_studen(){
		$para_obj = $this->input->post("para_obj"); 
		$is_assigment = isset($para_obj['is_assigment'])?$para_obj['is_assigment']:""; 
		$active_user = $this->input->post("user_active");
		$where = "";
		$where_d = "";
		$where.=" AND vse.schoolid='".$para_obj['schoolid'] ."'";
		$where.=" AND vse.schlevelid='".$para_obj['schoollavel']."'";
		$where.=" AND vse.year='".$para_obj['yearsid']."'";
		$where.=" AND vse.classid='".$para_obj['classid']."'";
		$where.=" AND vse.grade_levelid='".$para_obj['gradelavel']."'";
		
		$w_course = "";
				
		if($para_obj['course'] == 0){
			$w_course .= " AND sch_subject_gep.is_core=1";
		}else{
			$w_course .= " AND sch_subject_gep.is_skill=1";
		}
		$sql_student = "SELECT
								vse.student_num,
								vse.first_name,
								vse.last_name,
								vse.gender,
								vse.studentid,
								vse.class_name
								FROM
								v_student_enroment AS vse
								WHERE 1=1 {$where} ORDER BY vse.studentid ASC";
		
		
		$user_id    = $this->db->query("SELECT is_admin,emp_id FROM sch_user WHERE userid='".$active_user."'")->row();
		$w_user     = "";
		$sql_h_subj = "";
		
		if($user_id->is_admin == 0){ // check user type admin or not admin
			$w_subjid   = "";
			if($is_assigment == 1){ // open one subject
				$w_user.=" AND sch_teacher_subject.subject_id='".$para_obj['subject']."'";
			}
			$w_user.= " AND sch_teacher_subject.teacher_id='".$user_id->emp_id."'";
			// $w_user.= " AND sch_teacher_subject.classid='".$para_obj['classid']."'";
			
			$sql_h_subj = $this->db->query("SELECT
											sch_teacher_subject.subject_id as subjectid,
											sch_teacher_subject.classid,
											sch_subject_gep.max_score,
											sch_subject_gep.`subject`
											FROM
											sch_subject_gep
											INNER JOIN sch_teacher_subject ON sch_teacher_subject.subject_id = sch_subject_gep.subjectid
											WHERE 1=1 {$w_course} {$w_user}
											AND sch_teacher_subject.classid='".$para_obj['classid']."'
											AND sch_subject_gep.subj_type_id='".$para_obj['subject_group']."'
											AND sch_teacher_subject.yearid='".$para_obj['yearsid']."'
											");
		}else{
			$w_subjid   = "";
			if($is_assigment == 1){ // open one subject
				$w_subjid=" AND sch_subject_gep.subjectid='".$para_obj['subject']."'";
			}
			$sql_h_subj = $this->db->query("SELECT
												sch_subject_gep.subjectid,
												sch_subject_gep.`subject`,
												sch_subject_gep.subj_type_id,
												sch_subject_gep.max_score,
												sch_subject_gep.short_sub
												FROM
												sch_subject_gep
												WHERE 1=1 {$w_course}
												AND sch_subject_gep.subj_type_id='".$para_obj['subject_group']."'
												");
		}
		

		$tr = "";
		//$tr_pdf = "";
		//$h_pdf = "";
		$i = 1;
		$h = "";
		$query_student = $this->db->query($sql_student);
		$kkk = "";
		if($query_student->num_rows() > 0){
			foreach($query_student->result() as $row_st){
				$d = "";
				$d_pdf ="";
				$typeno = 0;
				if($sql_h_subj->num_rows() > 0)
				{
					foreach($sql_h_subj->result() as $row_h_s){
						$max_score = $row_h_s->max_score;
						if($i == 1){
							$h.="<th style='text-align:center;'>".$row_h_s->subject."</th>";
							//$h_pdf.="<th style='text-align:center;' width='200' height='30'>".$row_h_s->subject."</th>";
						}
						$sqll = "SELECT
															ssor.score_id,
															ssor.tran_date,
															ssor.typeno,
															ssgd.score_num,
															ssgd.studentid,
															ssgd.subjectid
														FROM
														sch_score_gep_entryed_order as ssor
														INNER JOIN sch_score_gep_entryed_detail as ssgd ON ssor.typeno = ssgd.typeno
														WHERE 1=1
														AND ssor.exam_typeid='".$para_obj['examtype']."'
														AND ssor.course_type = '".$para_obj['course']."'
														AND ssgd.subjectid='".$row_h_s->subjectid."'
														AND ssgd.studentid = '".$row_st->studentid."'
														AND ssor.group_subject='".$para_obj['subject_group']."' 
														AND ssor.tran_date='".$this->green->formatSQLDate($para_obj['date_add'])."'
														AND schoolid='".$para_obj['schoolid']."'
														AND schlevelid='".$para_obj['schoollavel']."'
														AND yearid='".$para_obj['yearsid']."'
														AND gradelevelid='".$para_obj['gradelavel']."'
														AND classid='".$para_obj['classid']."'
														";

						$sql_score = $this->db->query("SELECT
															ssor.score_id,
															ssor.tran_date,
															ssor.typeno,
															ssgd.score_num,
															ssgd.studentid,
															ssgd.subjectid
														FROM
														sch_score_gep_entryed_order as ssor
														INNER JOIN sch_score_gep_entryed_detail as ssgd ON ssor.typeno = ssgd.typeno
														WHERE 1=1
														AND ssor.exam_typeid='".$para_obj['examtype']."'
														AND ssor.course_type = '".$para_obj['course']."'
														AND ssgd.subjectid='".$row_h_s->subjectid."'
														AND ssgd.studentid = '".$row_st->studentid."'
														AND ssor.group_subject='".$para_obj['subject_group']."' 
														AND ssor.tran_date='".$this->green->formatSQLDate($para_obj['date_add'])."'
														AND schoolid='".$para_obj['schoolid']."'
														AND schlevelid='".$para_obj['schoollavel']."'
														AND yearid='".$para_obj['yearsid']."'
														AND gradelevelid='".$para_obj['gradelavel']."'
														AND classid='".$para_obj['classid']."'
														");
						if($sql_score->num_rows() > 0){
							foreach($sql_score->result() as $row_score){
								$d.='<td><input type="text" max_score="'.$max_score.'" id="score" class="form-control score" subj_id="'.$row_h_s->subjectid.'" name="score" value="'.$row_score->score_num.'" style="text-align: center;"></td>';
								
								//$d_pdf.='<td width="200" height="30">'.$row_score->score_num.'</td>';

								$typeno = $row_score->typeno;
							}
						}else{
							$d.='<td><input type="text" max_score="'.$max_score.'" id="score" class="form-control score" name="score" subj_id="'.$row_h_s->subjectid.'" value="0" style="text-align: center;"></td>';
							//$d_pdf.='<td width="200" height="30">0</td>';
						}
					}
				}
				$tr.='<tr>
						<td style="text-align:center;">'.$i.'
							<input type="hidden" name="htypno" id="htypno" class="htypno" value="'.$typeno.'">
							<input type="hidden" name="studentid" id="studentid" class="studentid" value="'.$row_st->studentid.'">
						</td>
					    <td>'.$row_st->last_name.' '.$row_st->first_name.'</td><td>'.ucfirst($row_st->gender).'</td>'.$d.'

					    </tr>';

				// $tr_pdf.='<tr>
				// 			<td width="200" height="30">'.$i.'
								
				// 			</td>
				// 		    <td width="200" height="30">'.$row_st->last_name.' '.$row_st->first_name.'</td>
				// 		    <td width="200" height="30">'.ucfirst($row_st->gender).'</td>'.$d_pdf.'

				// 	    </tr>';


				$i++; 

			}
		}
		// $pdf = new PDF();
		// $pdf->AliasNbPages();
		// $pdf->SetAutoPageBreak(true, 15);
		// $pdf->AddPage();
		// $pdf->SetFont('Arial','B',14);
		
		// $pdf->SetFont('Arial','B',8);
		// $pdf->WriteHTML("<br><br><table border='1' style='width:100%; background-color: red;'><tr>$h_pdf</tr>$tr_pdf</table>");
		// $pdf->SetFont('Arial','B',6);

		// $path = "./assets/upload/testing.pdf";
		// $pdf->Output($path,'F');
		
		//header('location:'.$path);

		//$arr['arr_score'] = $arr_json;
		$arr['arr_hsubj'] = $h;
		$arr['arr_tr'] = $tr;
		echo json_encode($arr);

	}

	public function do_upload($file) {
	    $config['upload_path']   = './assets/upload';
	    $config['allowed_types'] = 'jpg|png|pdf';
	    $config['max_size']      = 5120;
	    $config['max_width']     = 1024;
	    $config['max_height']    = 768;
	    $this->load->library('upload', $config);

	    $upload_data = array();

	    if (!$this->upload->do_upload($file)) {
	      $error = array('error' => $this->upload->display_errors());
	      $upload_data = $error;
	    }

	    else {
	      $upload_data = $this->upload->data();
	    }

	    return $upload_data;
  	}

	function save_data(){
		$obj    = $this->input->post("para_obj"); 
		$arr_student = $this->input->post("arr_student");
		$typeno_tr = $this->input->post("typeno_tr");
		$sql_check = $this->db->query("SELECT COUNT(*) as amt FROM sch_school_term WHERE termid='".$obj['subexamtype']."' AND start_date <= '".$this->green->convertSQLDate($obj['date_add'])."' AND end_date >='".$this->green->convertSQLDate($obj['date_add'])."'")->row()->amt;
		$check_finale = 0;
		if($obj['is_class_participation'] == 0){
			$check_finale = $this->db->query("SELECT COUNT(*) AS count_final 
													FROM sch_score_gep_entryed_order 
													WHERE main_tranno='".$obj['schoollavel']."_".$obj['yearsid']."_".$obj['gradelavel']."_".$obj['classid']."_".$obj['examtype']."_".$obj['subexamtype']."' 
													AND course_type='".$obj['course']."' 
													AND is_class_participate=0
													AND typeno<>'".$typeno_tr."'")->row()->count_final;
		}
		
		$m = 0;
		if($sql_check == 1 AND $check_finale==0)
		{
			$m=1;
			$typeno = "";
			if($typeno_tr != 0){
				//$typeno_del = $tran->row()->typeno;
				$table = array("sch_score_gep_entryed_order","sch_score_gep_entryed_detail");
				$this->db->where("typeno",$typeno_tr);
				$this->db->delete($table);
				$typeno = $typeno_tr;
			}else{
				$typeno = $this->green->nextTran(40,"GEP Score");
			}
			
			if(count($obj) > 0)
			{
					$data = array("type"=>40,
								"typeno"=>$typeno,
								"main_tranno"=>$obj['schoollavel']."_".$obj['yearsid']."_".$obj['gradelavel']."_".$obj['classid']."_".$obj['examtype']."_".$obj['subexamtype'],
								"schoolid"=>$obj['schoolid'],
								"schlevelid"=>$obj['schoollavel'],
								"yearid"=>$obj['yearsid'],
								"gradelevelid"=>$obj['gradelavel'],
								"classid"=>$obj['classid'],
								"exam_typeid"=>$obj['examtype'],
								"termid"=>$obj['subexamtype'],
								"course_type"=>$obj['course'],
								"tran_date"=>$this->green->convertSQLDate($obj['date_add']),
								"create_by"=>$this->session->userdata('userid'),
								"create_date"=>date('Y-m-d H:i:s'),
								"modify_by"=>$this->session->userdata('userid'),
								"modify_date"=>date('Y-m-d H:i:s'),
								"group_subject"=>$obj['subject_group'],
								"is_class_participate"=>$obj['is_class_participation'],
								"is_pt"=>$obj['is_partime']
								);
					$this->db->insert("sch_score_gep_entryed_order",$data);
				if(count($arr_student) > 0){
					foreach($arr_student as $arr_st){
						if(isset($arr_st['score'])?count($arr_st['score']):0 > 0){
							foreach($arr_st['score'] as $arr_sc){
									$data_st = array("studentid"=>$arr_st['studentid'],
													"subjectid"=>$arr_sc['subjectid'],
													"subjecttypeid"=>$obj['subject_group'],
													"score_num"=>$arr_sc['val_score'],
													"type"=>40,
													"typeno"=>$typeno,
													"score_oderid"=>""
													);
									$this->db->insert("sch_score_gep_entryed_detail",$data_st);
							}
						}
						
						
					}
				}
				$arr_gep = array("schoolid"=>$obj['schoolid'],
								"schoollavel"=>$obj['schoollavel'],
								"yearsid"=>$obj['yearsid'],
								"gradelavel"=>$obj['gradelavel'],
								"classid"=>$obj['classid'],
								"examtype"=>$obj['examtype'],
								"termid"=>$obj['subexamtype'],
								"course"=>$obj['course'],
								"is_pt"=>$obj['is_partime']
								);
				$this->green->fn_result_gep($arr_gep);
			}
		}
		else if($check_finale == 1){
			$m = 2;
		}
		echo $m;
	}
	function show_report_list(){
		$para_obj = $this->input->post("para_obj"); 
		$where = "";
		$where_d = "";
		$where.=" AND vse.schoolid   = '".$para_obj['schoolid'] ."'";
		$where.=" AND vse.schlevelid = '".$para_obj['schoollavel']."'";
		$where.=" AND vse.year     = '".$para_obj['yearsid']."'";
		$where.=" AND vse.classid    = '".$para_obj['classid']."'";
		$where.=" AND vse.grade_levelid ='".$para_obj['gradelavel']."'";
		
		$wh_sub_h    = "";
		$wh_sub_tran = "";
		if($para_obj['subject'] != ""){
			$wh_sub_h    = " AND subjectid = '".$para_obj['subject']."'";
			$wh_sub_tran = " AND detail.subjectid ='".$para_obj['subject']."'";
		}
		$w_course = "";
		$wh_c     = "";
		if($para_obj['course'] != ""){
			if($para_obj['course']  == 0){
				$w_course=" AND sch_subject_gep.is_core=1";
				$wh_c=" AND orders.course_type='".$para_obj['course']."'";
			}else{
				$w_course=" AND sch_subject_gep.is_skill=1";
				$wh_c=" AND orders.course_type='".$para_obj['course']."'";
			}
		}
		if($para_obj['date_add'] != ""){
			$wh_c.= " AND orders.tran_date='".$this->green->convertSQLDate($para_obj['date_add'])."'";
		}
		$sql_list = "SELECT
						vse.student_num,
						vse.first_name,
						vse.last_name,
						vse.gender,
						vse.studentid,
						vse.class_name
						FROM
						v_student_enroment AS vse
						WHERE 1=1 {$where}
						-- ORDER BY vse.student_num ASC
						";
		
		$sql_head = $this->db->query("SELECT
										sch_subject_gep.subjectid,
										sch_subject_gep.`subject`,
										sch_subject_gep.is_core,
										sch_subject_gep.is_skill,
										sch_subject_gep.subj_type_id,
										sch_subject_gep.short_sub,
										sch_subject_gep.max_score
										FROM
											sch_subject_gep
										WHERE 1=1 {$w_course} {$wh_sub_h}
										AND subj_type_id='".$para_obj['subject_group']."'");
		$sql_score = "SELECT
							orders.tran_date,
							orders.create_by,
							orders.subjecttypeid,
							orders.is_pt,
							orders.course_type,
							detail.subjectid,
							detail.score_num,
							detail.studentid,
							sch_subject_gep.max_score
							FROM
							sch_score_gep_entryed_order AS orders
							INNER JOIN sch_score_gep_entryed_detail AS detail ON orders.typeno = detail.typeno 
							INNER JOIN sch_subject_gep ON detail.subjectid = sch_subject_gep.subjectid
							AND orders.type = detail.type
							WHERE 1=1 {$wh_c}
							AND orders.schoolid='{$para_obj['schoolid']}' AND orders.schlevelid='{$para_obj['schoollavel']}'
							AND orders.yearid='{$para_obj['yearsid']}' AND orders.gradelevelid='{$para_obj['gradelavel']}' 
							AND orders.classid='{$para_obj['classid']}' AND orders.exam_typeid='{$para_obj['examtype']}' 
							AND orders.termid='{$para_obj['subexamtype']}'
							AND orders.group_subject='{$para_obj['subject_group']}' {$wh_sub_tran}
							ORDER BY detail.score_num DESC
							";
		$result       = "";
		$result_head  = "";
		$result_score = "";
		
		if($sql_head->num_rows() > 0){
			$result_head = $sql_head->result();
		}

		$scor_core  = array();
		$scor_skill = array();
		$score_max_sub = array();
		$teacher_name = "";
		$trandate     = "";
		$select_score = $this->db->query($sql_score);
		if($select_score->num_rows() > 0){
			foreach($select_score->result() as $row_sc){
				$trandate     = $this->green->formatSQLDate($row_sc->tran_date);
				$teacher_name = $this->db->query("SELECT user_name FROM sch_user WHERE userid='".$row_sc->create_by."'")->row()->user_name;
				if($row_sc->course_type == 0){
					if(isset($scor_core[$row_sc->studentid][$row_sc->subjectid]))
					{
						$scor_core[$row_sc->studentid][$row_sc->subjectid] = $scor_core[$row_sc->studentid][$row_sc->subjectid]+$row_sc->score_num;
					}else{
						$scor_core[$row_sc->studentid][$row_sc->subjectid]=$row_sc->score_num;
					}
					
					if(isset($score_max_sub['max_core'][$row_sc->studentid][$row_sc->subjectid])){
						$score_max_sub['max_core'][$row_sc->studentid][$row_sc->subjectid]=$score_max_sub['max_core'][$row_sc->studentid][$row_sc->subjectid]+$row_sc->max_score;
					}else{
						$score_max_sub['max_core'][$row_sc->studentid][$row_sc->subjectid]=$row_sc->max_score;
					}
					if(isset($score_max_sub['comp'][$row_sc->subjectid])){
						if($score_max_sub['comp'][$row_sc->subjectid]<$score_max_sub['max_core'][$row_sc->studentid][$row_sc->subjectid]){
							$score_max_sub['comp'][$row_sc->subjectid]=$score_max_sub['max_core'][$row_sc->studentid][$row_sc->subjectid];
						}
					}else{
						$score_max_sub['comp'][$row_sc->subjectid]=$score_max_sub['max_core'][$row_sc->studentid][$row_sc->subjectid];
					}
				}
				else{
					if(isset($scor_skill[$row_sc->studentid][$row_sc->subjectid])){
						$scor_skill[$row_sc->studentid][$row_sc->subjectid] = $scor_skill[$row_sc->studentid][$row_sc->subjectid]+$row_sc->score_num;
					}else{
						$scor_skill[$row_sc->studentid][$row_sc->subjectid] = $row_sc->score_num;
					}
					
					if(isset($score_max_sub['max_skill'][$row_sc->studentid][$row_sc->subjectid])){
						$score_max_sub['max_skill'][$row_sc->studentid][$row_sc->subjectid]=$score_max_sub['max_skill'][$row_sc->studentid][$row_sc->subjectid]+$row_sc->max_score;
					}else{
						$score_max_sub['max_skill'][$row_sc->studentid][$row_sc->subjectid]=$row_sc->max_score;
					}
					if(isset($score_max_sub['comp_sk'][$row_sc->subjectid])){
						if($score_max_sub['comp_sk'][$row_sc->subjectid]<$score_max_sub['max_skill'][$row_sc->studentid][$row_sc->subjectid]){
							$score_max_sub['comp_sk'][$row_sc->subjectid]=$score_max_sub['max_skill'][$row_sc->studentid][$row_sc->subjectid];
						}
					}else{
						$score_max_sub['comp_sk'][$row_sc->subjectid]=$score_max_sub['max_skill'][$row_sc->studentid][$row_sc->subjectid];
					}
				}
			}
		}
		$query = $this->db->query($sql_list);
		//$arr_list_st = array();
		$arr_rank_v = array();
		$tr_student = "";
		$arr_sort_rank = array();
		if($query->num_rows() > 0){
			$n=1;
			foreach($query->result() as $row_st)
			{
				$td_core = "";
				$td_skill= "";
				$sum_score = 0;
				$sum_score_sub = 0;
				if($sql_head->num_rows() > 0)
				{
					foreach($sql_head->result() as $row_sub)
					{
						if($para_obj['course'] != "")
						{
							if($para_obj['course'] == 0){
								if($row_sub->is_core == 1){
									$core = isset($scor_core[$row_st->studentid][$row_sub->subjectid])?$scor_core[$row_st->studentid][$row_sub->subjectid]:0;
									$max_subj = $row_sub->max_score;
									$total_max = 0;
									if(isset($score_max_sub['comp'][$row_sub->subjectid])){
										$total_max = $score_max_sub['comp'][$row_sub->subjectid];
									}
									if($total_max == 0){
										$total_max = 1;
									}
									$show_score = floor((($core*$max_subj)/$total_max)*100)/100;
									$td_core.= "<td style='text-align:center;'>".$show_score."</td>";
									$sum_score+=$show_score;
									$sum_score_sub+=$row_sub->max_score;
								}
							}else{
								if($row_sub->is_skill == 1){
									$skill = isset($scor_skill[$row_st->studentid][$row_sub->subjectid])?$scor_skill[$row_st->studentid][$row_sub->subjectid]:0;
									$max_subj_sk = $row_sub->max_score;
									$total_max = 0;
									if(isset($score_max_sub['comp_sk'][$row_sub->subjectid])){
										$total_max = $score_max_sub['comp_sk'][$row_sub->subjectid];
									}
									if($total_max == 0){
										$total_max = 1;
									}
									$show_score_sk = floor((($skill*$max_subj_sk)/$total_max)*100)/100;
									$td_skill.= "<td style='text-align:center;'>".$show_score_sk."</td>";
									$sum_score+=$show_score_sk;
									$sum_score_sub+=$row_sub->max_score;
								}
							}
						}else{
							if($row_sub->is_core == 1){
								$core = isset($scor_core[$row_st->studentid][$row_sub->subjectid])?$scor_core[$row_st->studentid][$row_sub->subjectid]:0;
								$max_subj = $row_sub->max_score;
								$total_max = 0;
								if(isset($score_max_sub['comp'][$row_sub->subjectid])){
									$total_max = $score_max_sub['comp'][$row_sub->subjectid];
								}
								if($total_max == 0){
									$total_max = 1;
								}
								$show_score = floor((($core*$max_subj)/$total_max)*100)/100;
								$td_core.= "<td style='text-align:center;'>".$show_score."</td>";
								$sum_score+=$show_score;
								$sum_score_sub+=$row_sub->max_score;
							}
							if($row_sub->is_skill == 1){
								$skill = isset($scor_skill[$row_st->studentid][$row_sub->subjectid])?$scor_skill[$row_st->studentid][$row_sub->subjectid]:0;
								$max_subj_sk = $row_sub->max_score;
								$total_max = 0;
								if(isset($score_max_sub['comp_sk'][$row_sub->subjectid])){
									$total_max = $score_max_sub['comp_sk'][$row_sub->subjectid];
								}
								if($total_max == 0){
									$total_max = 1;
								}
								$show_score_sk = floor((($skill*$max_subj_sk)/$total_max)*100)/100;
								$td_skill.= "<td style='text-align:center;'>".$show_score_sk."</td>";
								$sum_score+=$show_score_sk;
								$sum_score_sub+=$row_sub->max_score;
							}
						}
					}
				}else{
					$td_core.= "<td style='text-align:center;'>&nbsp;</td>";
					$td_skill.= "<td style='text-align:center;'>&nbsp;</td>";
				}
				if($sum_score_sub == 0){
					$sum_score_sub = 1;
				}
				if($para_obj['course'] != ""){
					$result_sco = floor($sum_score*100)/100;
				}else{
					if($para_obj['is_partime'] == 1){
						$result_sco = floor(($sum_score)*100)/100;
					}else{
						$result_sco = floor((($sum_score*100)/$sum_score_sub)*100)/100;
					}
					
					
				}
				
				$arr_rank_v[$row_st->studentid] = $result_sco;
				$sql_mention = $this->db->query("SELECT
												sch_score_mention_gep.schlevelid,
												sch_score_mention_gep.min_score,
												sch_score_mention_gep.max_score,
												sch_score_mention_gep.is_past
												FROM
												sch_score_mention_gep
												WHERE 1=1 AND schlevelid='".$para_obj['schoollavel']."' 
												AND min_score<='".$result_sco."' 
												AND max_score>='".$result_sco."'");
				$res = "";
				if($sql_mention->num_rows() > 0){
					foreach($sql_mention->result() as $row_m){
						if($row_m->is_past == 1){
							$res = "Passed";
						}else{
							$res = "Failed";
						}
					}
				}
				//$arr_sort_rank[$row_st->studentid] = $result_sco;
				if($para_obj['is_partime'] == 0){
					$arr_sort_rank[$row_st->studentid] ="<td>".$row_st->last_name." ".$row_st->first_name."</td>
														<td>".ucfirst($row_st->gender)."</td>".$td_core.$td_skill."<td style='text-align:center;'>".$result_sco."</td>
														<td style='text-align:center;'>".$res."</td><td class='rank_".$row_st->studentid."' style='text-align:center;'></td>
														";
				}else{
					$show_cours = "";
					if($para_obj['course']  == 0){
						$show_cours = $td_core;
					}else{
						$show_cours = $td_skill;
					}
					$arr_sort_rank[$row_st->studentid] = "<td>".$row_st->last_name." ".$row_st->first_name."</td>
															<td>".ucfirst($row_st->gender)."</td>".$show_cours."<td style='text-align:center;'>".$result_sco."</td>
															<td style='text-align:center;'>".$res."</td><td class='rank_".$row_st->studentid."' style='text-align:center;'></td>
															";
				}
				
				$n++;
			}
		}
		arsort($arr_rank_v);
        $arr_n = array();
        $trr = "";
        if(count($arr_rank_v)>0){
        	$ii = 1;
        	$arr_stor = array();
        	$nn = 1;
        	foreach($arr_rank_v as $k=>$v){
        		if(in_array($v,$arr_stor)){
					$arr_n[$k] = $nn;
					$trr.= "<tr><td style='text-align:center;'>".$ii."</td>".$arr_sort_rank[$k]."</tr>";
        		}else{
        			$nn = $ii;
        			$arr_n[$k] = $ii;
        			$trr.= "<tr><tr><td style='text-align:center;'>".$ii."</td>".$arr_sort_rank[$k]."</tr>";
        		}
				$arr_stor[$ii] = $v;
				$ii++;
        	}
        }

		//$arr['student_list']  = $result;
		$arr['subject_head']  = $result_head;
		$arr['teachername']   = $teacher_name;
		$arr['date_examp']    = $trandate;
		//$arr['tr'] = $tr_student;
		$arr['tr'] = $trr;
		$arr['rank_n'] = $arr_n;
		$arr['sort_score'] = rsort($arr_sort_rank,SORT_NUMERIC);
		echo json_encode($arr);


	}


	

	function show_list_term()
	{
		$para_obj = $this->input->post("para_obj"); 
		$todate   = $this->input->post("todate"); 
		$fromdate = $this->input->post("fromdate");
		$where = "";
		$where_d = "";
		$where.=" AND vse.schoolid   = '".$para_obj['schoolid'] ."'";
		$where.=" AND vse.schlevelid = '".$para_obj['schoollavel']."'";
		$where.=" AND vse.year     = '".$para_obj['yearsid']."'";
		$where.=" AND vse.classid    = '".$para_obj['classid']."'";
		$where.=" AND vse.grade_levelid ='".$para_obj['gradelavel']."'";
		$course = $para_obj['course'];
		$wh_course = "";
		if($course == 0){
			$wh_course = " AND is_core=1";
		}else{
			$wh_course = " AND is_skill=1";
		}

		$sql_subexam = $this->db->query("SELECT
											gep_order.type,
											gep_order.typeno,
											gep_order.main_tranno,
											gep_order.classid,
											gep_order.exam_typeid,
											gep_order.tran_date,
											gep_order.subjecttypeid,
											gep_order.group_subject,
											gep_order.is_class_participate,
											gep_det.studentid,
											gep_det.subjectid,
											SUM(gep_det.score_num) AS score,
											COUNT(*) AS amt_count,
											SUM(sch_subject_gep.max_score) AS max_score
											FROM
											sch_score_gep_entryed_order AS gep_order
											INNER JOIN sch_score_gep_entryed_detail AS gep_det ON gep_order.type = gep_det.type AND gep_order.typeno = gep_det.typeno
											INNER JOIN sch_subject_gep ON gep_det.subjectid = sch_subject_gep.subjectid
											WHERE 1=1
											-- AND gep_order.exam_typeid=1
											AND gep_order.course_type = '".$course."'
											AND gep_order.schoolid   = '".$para_obj['schoolid'] ."'
											AND gep_order.schlevelid = '".$para_obj['schoollavel']."'
											AND gep_order.yearid     = '".$para_obj['yearsid']."'
											AND gep_order.gradelevelid ='".$para_obj['gradelavel']."'
											AND gep_order.classid = '".$para_obj['classid']."'
											AND gep_order.tran_date <='".$this->green->convertSQLDate($fromdate)."' 
											AND gep_order.tran_date >='".$this->green->convertSQLDate($todate)."' 
											GROUP BY gep_order.exam_typeid,gep_det.subjectid,gep_det.studentid");
		$arr_score = array();
		$cp_f  = array();
		$cp_m  = array();
		$arr_att  = array();
		$arr_mid  = array();
		$arr_hw   = array();
		$arr_fn   = array();
		$max_cp	  = array();
		$max_att_m = array();
		$max_att_f = array();
		$max_hw_m = array();
		$max_hw_f = array();
		$max_mid = 0;
		if($sql_subexam->num_rows() > 0)
		{
			foreach($sql_subexam->result() as $row_exam)
			{
				if($row_exam->exam_typeid == 1 AND $row_exam->is_class_participate == 0){ // mid-term subject
					$arr_mid[$row_exam->studentid] = (isset($arr_mid[$row_exam->studentid])?($arr_mid[$row_exam->studentid]+$row_exam->score):$row_exam->score);
				
				}else if($row_exam->exam_typeid == 2 && $row_exam->is_class_participate == 0){ // final subject
					$arr_fn[$row_exam->studentid][$row_exam->subjectid] = $row_exam->score;

				}else if($row_exam->is_class_participate == 2)// homework
				{ 
					$arr_hw[$row_exam->exam_typeid][$row_exam->studentid] = (isset($arr_hw[$row_exam->exam_typeid][$row_exam->studentid])?$arr_hw[$row_exam->exam_typeid][$row_exam->studentid]+($row_exam->score):$row_exam->score);
					if($row_exam->exam_typeid == 1){ // mid-term
						$val_hw_m = isset($max_hw_m[$row_exam->exam_typeid][$row_exam->subjectid])?$max_hw_m[$row_exam->exam_typeid][$row_exam->subjectid]:0;
						if($row_exam->max_score > $val_hw_m){
							$max_hw_m[$row_exam->exam_typeid][$row_exam->subjectid]= $row_exam->max_score;
						}
					}else{ // final
						$val_hw_f = isset($max_hw_f[$row_exam->exam_typeid][$row_exam->subjectid])?$max_hw_f[$row_exam->exam_typeid][$row_exam->subjectid]:0;
						if($row_exam->max_score > $val_hw_f){
							$max_hw_f[$row_exam->exam_typeid][$row_exam->subjectid]= $row_exam->max_score;
						}
					}
				}
				else if($row_exam->is_class_participate == 3)// attandance
				{ 
					$arr_att[$row_exam->exam_typeid][$row_exam->studentid] = (isset($arr_att[$row_exam->exam_typeid][$row_exam->studentid])?$arr_att[$row_exam->exam_typeid][$row_exam->studentid]+($row_exam->score):$row_exam->score);
					if($row_exam->exam_typeid == 1){ // mid-term
						$val_att_m = isset($max_att_m[$row_exam->subjectid])?$max_att_m[$row_exam->subjectid]:0;
						if($row_exam->max_score > $val_att_m){
							$max_att_m[$row_exam->subjectid]= $row_exam->max_score;
						}
					}else{ // final
						$val_att_f = isset($max_att_f[$row_exam->subjectid])?$max_att_f[$row_exam->subjectid]:0;
						if($row_exam->max_score > $val_att_f){
							$max_att_f[$row_exam->subjectid]= $row_exam->max_score;
						}
					}	
				}else{ // class participation
					$arr_score[$row_exam->exam_typeid][$row_exam->studentid][$row_exam->subjectid] =isset($arr_score[$row_exam->exam_typeid][$row_exam->studentid][$row_exam->subjectid])?$arr_score[$row_exam->exam_typeid][$row_exam->studentid][$row_exam->subjectid]+$row_exam->score:$row_exam->score;
					if($row_exam->exam_typeid == 1){
						$val_cp_m = isset($cp_m[$row_exam->subjectid])?$cp_m[$row_exam->subjectid]:0;
						if($row_exam->max_score > $val_cp_m){
							$cp_m[$row_exam->subjectid] = $row_exam->max_score;
						}
					}else{
						$val_cp_f = isset($cp_f[$row_exam->subjectid])?$cp_f[$row_exam->subjectid]:0;
						if($row_exam->max_score > $val_cp_f){
							$cp_f[$row_exam->subjectid] = $row_exam->max_score;
						}
					}
				}
				
			}
		}
		$sql_list = "SELECT
						vse.student_num,
						vse.first_name,
						vse.last_name,
						vse.gender,
						vse.studentid,
						vse.class_name
						FROM
						v_student_enroment AS vse
						WHERE 1=1 {$where}
						ORDER BY vse.student_num ASC";
		$result_sql = $this->db->query($sql_list);
		$tr = "";
		$arr_rank_v = array();
		$arr_rank_k = array();
		$arr_sort_rank_final = array();
		if($result_sql->num_rows() > 0)
		{
			$k = 1;
			foreach($result_sql->result() as $row_st)
			{
				
				$sql_subject = $this->db->query("SELECT
												sch_subject_gep.subjectid,
												sch_subject_gep.`subject`,
												sch_subject_gep.short_sub,
												sch_subject_gep.subj_type_id,
												sch_subject_gep.max_score,
												sch_subject_gep.calc_score,
												sch_subject_type_gep.is_group_calc_percent as is_cencent,
												sch_subject_type_gep.is_class_participation as is_cp
												FROM
												sch_subject_type_gep
												INNER JOIN sch_subject_gep ON sch_subject_gep.subj_type_id = sch_subject_type_gep.subj_type_id
												WHERE 1=1 {$wh_course}
												");
				$td_att  = "";
				$td_cp   = "";
				$td_hw   = "";
				$td_fn   = "";
				$td_mid  = 0;
				$sum_att = 0;
				$sum_cp  = 0;
				$sum_hw  = 0;
				$sum_fn  = 0;
				$a = 0;
				$j = 0;
				$m = 0;
				$hw = 1;
				$sum_max_score = 0;
				$percent_att   = 1;
				$percent_cp    = 1;
				$percent_hw    = 1;
				if($sql_subject->num_rows() > 0)
				{
					foreach($sql_subject->result() as $row_id){
						$max_subjech = $row_id->max_score;
						if($row_id->is_cp == 3){ // attandance
							$att_mi = isset($max_att_m[$row_id->subjectid])?$max_att_m[$row_id->subjectid]:1;
							$att_fn = isset($max_att_f[$row_id->subjectid])?$max_att_f[$row_id->subjectid]:1;
							
							$score_att_m  = isset($arr_att[1][$row_st->studentid])?$arr_att[1][$row_st->studentid]:0;
							$score_att_f  = isset($arr_att[2][$row_st->studentid])?$arr_att[2][$row_st->studentid]:0;
							
							$total_m_f  = (($score_att_m*$max_subjech)/$att_mi)+(($score_att_f*$max_subjech)/$att_fn);

							$res_floor  = ($total_m_f/2);
							$result_att = floor($res_floor*100)/100; // round number
							$td_att.="<td style='text-align:center;'>".$result_att."</td>";
							$sum_att     += $result_att;
							$percent_att = $row_id->is_cencent;
							$a++;
						}else if($row_id->is_cp == 1){ // class participation
							$score_m_cp = isset($arr_score[1][$row_st->studentid][$row_id->subjectid])?$arr_score[1][$row_st->studentid][$row_id->subjectid]:0;
							$score_f_cp = isset($arr_score[2][$row_st->studentid][$row_id->subjectid])?$arr_score[2][$row_st->studentid][$row_id->subjectid]:0;
							
							$arr_count_f = isset($cp_f[$row_id->subjectid])?$cp_f[$row_id->subjectid]:1;
							$arr_count_m = isset($cp_m[$row_id->subjectid])?$cp_m[$row_id->subjectid]:1;

							//$result_m    = floor(($score_m_cp/$arr_count_m)*100)/100;
							//$result_f    = floor(($score_f_cp/$arr_count_f)*100)/100;
							$result_m    = ($score_m_cp*$max_subjech)/$arr_count_m;
							$result_f    = ($score_f_cp*$max_subjech)/$arr_count_f;
							
							$total_m_f   = floor((($result_m+$result_f)/2)*100)/100;
							
							$td_cp.="<td style='text-align:center;'>".$total_m_f."</td>";
							$sum_cp   += $total_m_f;
							$percent_cp = $row_id->is_cencent;
							$j++;
						}else if($row_id->is_cp == 2){ // homework
							$hw_mi = isset($max_hw_m[1][$row_id->subjectid])?$max_hw_m[1][$row_id->subjectid]:1;
							$hw_fn = isset($max_hw_f[2][$row_id->subjectid])?$max_hw_f[2][$row_id->subjectid]:1;
							
							$score_hw_m = isset($arr_hw[1][$row_st->studentid])?$arr_hw[1][$row_st->studentid]:0;
							$score_hw_f = isset($arr_hw[2][$row_st->studentid])?$arr_hw[2][$row_st->studentid]:0;

							$total_hw   = (($score_hw_m*$max_subjech/$hw_mi))+(($score_hw_f*$max_subjech/$hw_fn));
							$sum_hw     = floor(($total_hw/2)*100)/100;
							$td_hw.="<td style='text-align:center;'>".$sum_hw."</td>";
							$percent_hw = $row_id->is_cencent;
						}
						else{
							$m++; // mid-term and finale is have subject togather
							$val_fn = isset($arr_fn[$row_st->studentid][$row_id->subjectid])?$arr_fn[$row_st->studentid][$row_id->subjectid]:0;
							
							$toal_cal_fn = floor((($val_fn*$row_id->calc_score)/$max_subjech)*100)/100;

							$td_fn.="<td style='text-align: center;'>".floor(($val_fn*100)/100)."</td>";
							if($course == 0){
								$td_fn.="<td style='text-align: center;'>".$toal_cal_fn."</td>";
							}
							$sum_fn+= $val_fn;
							$sum_max_score+=$row_id->max_score;
						}
						
					}
				}
				
				$sum_mid_term = 0;
				if(count($arr_mid) > 0){
					$sum_mid_term = isset($arr_mid[$row_st->studentid])?$arr_mid[$row_st->studentid]:0;;
				}
				$result_mid_term = floor((($sum_mid_term*15)/100)*100)/100;
				// end mid term
				if($a == 0){
					$a=1;
				}
				if($j == 0){
					$j=1;
				}
				$result_att  = floor(((($sum_att/$a)*$percent_att)/100)*100)/100;
				$result_cp   = floor(((($sum_cp/$j)*$percent_cp)/100)*100)/100;
				$result_hw   = floor((($sum_hw*$percent_hw)/100)*100)/100;
				$result_fn   = floor(($sum_fn*60)/($sum_max_score==0?1:$sum_max_score)*100)/100;
				$overall_score = ($result_att+$result_cp+$result_hw+$result_mid_term+$result_fn)-0;

				$arr_rank_v[$row_st->studentid] = floor($overall_score*100)/100;
				$arr_rank_k[] = floor($overall_score*100)/100;
				$arr_sort_rank_final[$row_st->studentid] ="
																<td>".$row_st->last_name." ".$row_st->first_name."</td>
																<td>".ucfirst($row_st->gender)."</td>
																".$td_att."
																<td style='text-align:center;background-color:#E6E6E6;'>".$result_att."</td>
																".$td_cp."
																<td style='text-align:center;background-color:#E6E6E6;'>".$result_cp."</td>
																".$td_hw."
																<td style='text-align:center;background-color:#E6E6E6;'>".$result_hw."</td>
																<td style='text-align:center;'>".$sum_mid_term."</td>
																<td style='text-align:center;background-color:#E6E6E6;'>".$result_mid_term."</td>
																".$td_fn."
																<td style='text-align:center;background-color:#E6E6E6;'>".$result_fn."</td>
																<td style='text-align:center;background-color:#E6E6E6;'>".$overall_score."</td>
																<td style='text-align:center;' class='rank_".$row_st->studentid."'></td>
															";
			}
		}
		unset($arr_score);
		unset($cp_f);
		unset($cp_m);
		unset($arr_att);
		unset($arr_mid);
		unset($arr_hw);
		unset($arr_fn);
		unset($max_cp);
		unset($max_att_m);
		unset($max_att_f);
		unset($max_hw_m);
		unset($max_hw_f);
		$sql_th = $this->db->query("SELECT
									sch_subject_gep.subjectid,
									sch_subject_gep.`subject`,
									sch_subject_gep.short_sub,
		 							sch_subject_gep.subj_type_id,
		 							sch_subject_gep.max_score,
		 							sch_subject_gep.calc_score,
		 							sch_subject_type_gep.is_group_calc_percent,
									sch_subject_type_gep.is_class_participation AS is_cp
									FROM
									sch_subject_type_gep
									INNER JOIN sch_subject_gep ON sch_subject_gep.subj_type_id = sch_subject_type_gep.subj_type_id
									WHERE 1=1 {$wh_course}
									");
		$th_att    = "";
		$th_cp     = "";
		$th_hw     = "";
		$th_fn     = "";
		$th_num_att= "";
		$th_num_cp = "";
		$th_num_hw = "";
		$th_num_fn = "";
		$cp =1;
		$fn =0;
		$total_percent_cp = 0;
		$total_percent_hw = 0;
		$total_percent_fn = 0;
		$cp_calc_score    = 0;
		$hw_calc_score    = 0;
		$col_hw           = 1;
		$col_fn           = 1;
		if($sql_th->num_rows() > 0){
			foreach($sql_th->result() as $row_h){
				if($row_h->is_cp==3){
					$th_att.='<th style="text-align:center;">'.$row_h->short_sub.'</th>
								<th style="text-align: center;background-color:#E6E6E6;">'.$row_h->is_group_calc_percent.'%</th>';
					$th_num_att='<th>'.$row_h->max_score.'</th><th style="text-align:center;background-color:#E6E6E6;">'.$row_h->calc_score.'</th>';
				}else if($row_h->is_cp==1){
					$th_cp.="<th style='text-align:center;'>".$row_h->short_sub."</th>";
					$th_num_cp.="<th style='text-align:center;'>".$row_h->max_score."</th>";
					$total_percent = $row_h->is_group_calc_percent;
					$cp_calc_score = $row_h->calc_score;
					$cp++;
				}else if($row_h->is_cp==2){
					$th_hw.="<th style='text-align: center;'>".$row_h->short_sub."</th>";
					$th_num_hw.="<th style='text-align: center;'>".$row_h->max_score."</th>";
					$total_percent_hw = $row_h->is_group_calc_percent;
					$hw_calc_score = $row_h->calc_score;
					$col_hw++;
				}else{
					$tr_fn = "";
					$col   = 0;
					if($course == 0){
						$tr_fn.="<th style='text-align: center;'>".$row_h->calc_score."</th>";
						$fn+=2;
						$col = 2;
					}else{
						$tr_fn = "";
						$fn++;
						$col = 0;
					}
					$th_fn.="<th colspan='".$col."' style='text-align: center;'>".$row_h->short_sub."</th>";
					$th_num_fn.="<th style='text-align: center;'>".$row_h->max_score."</th>".$tr_fn;
					
					$total_percent_fn = $row_h->is_group_calc_percent;
					
				}
			}
		}
		$th='<tr>
                <th colspan="3" rowspan="2" style="border-top:1px solid white !important;border-left:1px solid white !important;">&nbsp;</th>
                <th colspan="'.($cp+4).'" style="text-align: center;">One-going Assessment 25%</th>
                <th colspan="2" rowspan="2" style="text-align: center;">Mid-term 15%</th>
                <th colspan="'.($fn+1).'" rowspan="2" style="text-align: center; vertical-align: middle;">Final Exam 60%</th>
                <th rowspan="3" style="text-align: center;">Overall Score 100%</th>
                <th rowspan="4" style="text-align: center; vertical-align: middle;">Rank</th>
            </tr>
            <tr>
                <th colspan="2" style="text-align: center;">Attendance</th>
                <th colspan="'.$cp.'" style="text-align: center;">Class Participation</th>
                <th colspan="'.$col_hw.'" style="text-align: center;">Homework</th> 
            </tr>
            <tr>
                    <th rowspan="2" style="text-align: center; vertical-align: middle;">No</th>
                    <th rowspan="2" style="text-align: center; vertical-align: middle;">Name</th>
                    <th rowspan="2" style="text-align: center; vertical-align: middle;">Sex</th>
                    '.$th_att.'
                    '.$th_cp.'
                    <th style="text-align: center;background-color:#E6E6E6;">'.$total_percent.'%</th>
                    '.$th_hw.'
                    <th style="text-align: center;background-color:#E6E6E6;">'.$total_percent_hw.'%</th>
                    <th style="text-align: center;">Score</th>
                    <th style="text-align: center;background-color:#E6E6E6;">15%</th>
                    '.$th_fn.'
                    <th rowspan="2" style="text-align: center; vertical-align: middle;background-color:#E6E6E6;">'.$total_percent_fn.'%</th>
                </tr>
                <tr>
                    <th>100</th>
                    <th style="text-align:center;background-color:#E6E6E6;">5</th>
                    '.$th_num_cp.'
                    <th style="text-align: center;background-color:#E6E6E6;">'.$total_percent.'</th>
                    '.$th_num_hw.'
                    <th style="text-align: center;background-color:#E6E6E6;">'.$total_percent_hw.'</th>
                    <th style="text-align: center;">100</th>
                    <th style="text-align: center;background-color:#E6E6E6;">15</th>
                    '.$th_num_fn.'
                    <th style="text-align: center;background-color:#E6E6E6;">100</th>
                </tr>';
        $user_id   = $this->session->userdata('userid');
        $username  = $this->db->query("SELECT user_name FROM sch_user WHERE userid='".$user_id."'")->row()->user_name;
        arsort($arr_rank_v);
        $arr_n = array();
        $tr_sort = "";
        $j = 1;
        if(count($arr_rank_v)>0){
        	$ii = 1;
        	$arr_stor = array();
        	$nn = 1;
        	foreach($arr_rank_v as $k=>$v){
        		if(in_array($v,$arr_stor)){
					$arr_n[$k] = $nn;
					//$tr_sort.= $arr_sort_rank_final[$k];
					$tr_sort.= "<tr><td style='text-align:center;'>".($j)."</td>".$arr_sort_rank_final[$k]."</tr>";
        		}else{
        			$nn=$ii;
        			$arr_n[$k] = $ii;
        			//$tr_sort.= $arr_sort_rank_final[$k];
        			$tr_sort.="<tr><td style='text-align:center;'>".($j)."</td>".$arr_sort_rank_final[$k]."</tr>";
        		}
				$arr_stor[$ii] = $v;
				$ii++;
				$j++;
        	}
        }
        $arr_json["thead"] = $th;        
		//$arr_json["tr"] = $tr;
		$arr_json["tr"] = $tr_sort;
		$arr_json['rank_v'] = $arr_rank_v;
		$arr_json['rank_n'] = $arr_n;
		$arr_json['username'] = ucfirst($username);
		echo json_encode($arr_json);
	}
	
}