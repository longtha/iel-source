<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_final_gep extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/tearcherclassmodel','tclass');
		$this->load->library('pagination');		
	}
	public function index()
	{
		$m='';
		$p='';
		if(isset($_GET['m'])){
        	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }

		$data['schoolid'] = $this->select_sch();
		$data['schoolid_lav'] = $this->select_sch_lav();
		// $data['exam_type'] = $this->exam_type($id="");
		$this->load->view('header');
		$this->load->view('school/final_gep/index',$data);
		$this->load->view('footer');
	}
	function show_term(){
		$schoolid    =  $this->input->post("schoolid");
		$schoollavel = $this->input->post("schoollavel");
		$years       = $this->input->post("years");		
		$sql_term    =	$this->db->query("SELECT
												t.termid,
												t.term,
												t.start_date,
												t.end_date AS end_date,
												t.yearid,
												t.schoolid,
												t.programid,
												t.schlevelid
												FROM
													sch_school_term AS t
												WHERE t.yearid='{$years}' 
												AND t.schoolid='{$schoolid}' 
												AND t.schlevelid='{$schoollavel}'
											");
		$opt_term = "";
		if($sql_term->num_rows() > 0){
			foreach($sql_term->result() as $row_term){
				$opt_term.= "<option value='".$row_term->termid."'>".$row_term->term."</option>";
			}
		}
		echo $opt_term;
		die();

	}

	function select_y(){
		$para_obj  = $this->input->post("para_obj");
		$sql_y = $this->db->query("SELECT
									sch_school_year.yearid,
									sch_school_year.sch_year,
									CONCAT(YEAR(STR_TO_DATE(from_date,'%Y')),'-',YEAR(STR_TO_DATE(to_date,'%Y'))) as st_y
									FROM
									sch_school_year
									WHERE 1=1 AND schlevelid='".$para_obj['schoollavel']."'
									");
		$opt_y = "<option value=''></option>";
		if($sql_y->num_rows() > 0){
			foreach($sql_y->result() as $row_y){
				$opt_y.="<option value='".$row_y->yearid."' st_to_date='".$row_y->st_y."'>".$row_y->sch_year."</option>";
			}
		}
		echo $opt_y;
	}

	function select_sch(){
		$sql_sch = $this->db->query("SELECT
										sch_school_infor.schoolid,
										sch_school_infor.`name`
										FROM
										sch_school_infor
									");
		$opt_sch = "";
		if($sql_sch->num_rows() > 0){
			foreach($sql_sch->result() as $row_sch){
				$opt_sch.="<option value='".$row_sch->schoolid."'>".$row_sch->name."</option>";
			}
		}
		return $opt_sch;
	}

	function select_sch_lav(){
		$sql_sch_schlav = $this->db->query("SELECT
										sch_school_level.schlevelid,
										sch_school_level.sch_level,
										sch_school_level.schoolid
										FROM
										sch_school_level
										WHERE programid=3
										");
		$opt_sch_lav = "<option value=''></option>";
		if($sql_sch_schlav->num_rows() > 0){
			foreach($sql_sch_schlav->result() as $row_sch_l){
				$opt_sch_lav.="<option value='".$row_sch_l->schlevelid."'>".$row_sch_l->sch_level."</option>";
			}
		}
		return $opt_sch_lav;
	}

	function grade_lavel(){
		$para_obj        = $this->input->post("para_obj");
		$sql_gradelav = $this->db->query("SELECT
											sch_grade_level.grade_levelid,
											sch_grade_level.grade_level,
											sch_grade_level.schoolid,
											sch_grade_level.next_grade_level,
											sch_grade_level.schlevelid
											FROM
											sch_grade_level
											WHERE 1=1 AND schoolid='".$para_obj['schoolid']."' 
											AND schlevelid='".$para_obj['schoollavel']."'
										");
		$opt_grade_lavel = "<option value=''></option>";
		if($sql_gradelav->num_rows() > 0){
			foreach($sql_gradelav->result() as $row_gr){
				$opt_grade_lavel.="<option value='".$row_gr->grade_levelid."' att_grandlavel='".$row_gr->grade_level."'>".$row_gr->grade_level."</option>";
			}
		}
		echo $opt_grade_lavel;
	}

	function grade_class(){
		$para_obj        = $this->input->post("para_obj"); 
		$sql_class = $this->db->query("SELECT
											sch_class.classid,
											sch_class.class_name,
											sch_class.grade_levelid,
											sch_class.grade_labelid,
											sch_class.schoolid,
											sch_class.schlevelid,
											sch_class.is_pt
											FROM
											sch_class
											WHERE 1=1 AND schoolid='".$para_obj['schoolid']."' 
											AND schlevelid='".$para_obj['schoollavel']."'
											AND grade_levelid='".$para_obj['gradelavel']."'
										");
		$opt_class = "<option value=''>All</option>";
		if($sql_class->num_rows() > 0){
			foreach($sql_class->result() as $row_class){
				$opt_class.="<option value='".$row_class->classid."' att_class='".$row_class->class_name."' is_pt='".$row_class->is_pt."'>".$row_class->class_name."</option>";
			}
		}
		echo $opt_class;
	}

	function search_studen(){
		$para_obj = $this->input->post("para_obj"); 
		$page    = $this->input->post("page");
		$perpage = $this->input->post("perpage");
		$where   = "";
		if($para_obj['student_num'] != ""){
			$where .= " AND vp.student_num LIKE '%".trim($para_obj['student_num'])."%' ";
		}
		if($para_obj['full_name'] != ""){
			$where .= " AND CONCAT(vp.last_name, ' ', vp.first_name) LIKE '%".trim($para_obj['full_name'])."%' ";
		}
		// $where1 = " AND v.programid = '3' ";		
		// $where1 .= " AND v.schoolid = '".$para_obj['schoolid']."' ";
		// $where1 .= " AND v.schlevelid = '".$para_obj['schoollavel']."' ";
		// $where1 .= " AND v.year = '".$para_obj['years']."' ";
		// $where1 .= " AND v.classid = '".$para_obj['classid']."' ";
		// $where1 .= " AND v.grade_levelid = '".$para_obj['gradelavel']."' ";
		// $where1 .= " AND v.grade_levelid = '".$para_obj['termid']."' ";
		if($this->session->userdata('match_con_posid')!='stu'){
		$sql = "SELECT DISTINCT
						vp.student_num,
						vp.first_name,
						vp.last_name,
						CONCAT(vp.last_name,' ',vp.first_name) as fullname,
						vp.first_name_kh,
						vp.last_name_kh,
						CONCAT(vp.last_name_kh,' ',vp.first_name_kh) as fullname_kh,
						vp.gender,
						vp.class_name,
						vp.studentid,
						vp.schoolid,
						vp.`year`,
						vp.classid,
						vp.schlevelid,
						vp.rangelevelid,
						vp.feetypeid,
						vp.programid,
						vp.sch_level,
						vp.rangelevelname,
						vp.program,
						vp.sch_year,
						vp.grade_levelid,
						g.grade_level,
						vp.is_pt,
						tt.term,
						tt.termid,
						tt.yearid,
						CONCAT(YEAR(vp.from_date),'-',YEAR(vp.to_date)) AS ftyear
						FROM
						v_student_enroment AS vp
						INNER JOIN sch_school_term AS tt ON tt.yearid = vp.`year`
						INNER JOIN sch_grade_level as g on vp.grade_levelid = g.grade_levelid
						WHERE 1=1 
						AND vp.schoolid =".($para_obj['schoolid']==''?'NULL':$para_obj['schoolid'])."
						AND vp.programid ='3'
						AND vp.schlevelid ='".$para_obj['schoollavel']."'
						AND vp.grade_levelid ='".$para_obj['gradelavel']."' 
						AND vp.classid ='".$para_obj['classid']."'
						AND vp.year ='".$para_obj['years']."'
						AND tt.termid = '".$para_obj['termid']."'
						{$where}
						ORDER BY vp.studentid ASC";
			}else{
				$sql = "SELECT DISTINCT
						vp.student_num,
						vp.first_name,
						vp.last_name,
						CONCAT(vp.last_name,' ',vp.first_name) as fullname,
						vp.first_name_kh,
						vp.last_name_kh,
						CONCAT(vp.last_name_kh,' ',vp.first_name_kh) as fullname_kh,
						vp.gender,
						vp.class_name,
						vp.studentid,
						vp.schoolid,
						vp.`year`,
						vp.classid,
						vp.schlevelid,
						vp.rangelevelid,
						vp.feetypeid,
						vp.programid,
						vp.sch_level,
						vp.rangelevelname,
						vp.program,
						vp.sch_year,
						vp.grade_levelid,
						g.grade_level,
						vp.is_pt,
						tt.term,
						tt.termid,
						tt.yearid,
						CONCAT(YEAR(vp.from_date),'-',YEAR(vp.to_date)) AS ftyear
						FROM
						v_student_enroment AS vp
						INNER JOIN sch_school_term AS tt ON tt.yearid = vp.`year`
						INNER JOIN sch_grade_level as g on vp.grade_levelid = g.grade_levelid
						WHERE 1=1 AND vp.studentid ='".$this->session->userdata('emp_id')."' AND vp.programid=3
						ORDER BY vp.studentid ASC";
			}

		$paging=$this->green->ajax_pagination(count($this->db->query($sql)->result()),site_url("school/c_final_gep/search_studen"),$perpage,"icon");
		$getlimit=2;
		if($paging['limit']!=''){
			$getlimit = $paging['limit'];
		}
		$runSQL = $this->db->query("$sql LIMIT {$paging['start']},{$getlimit}");
		$tr = '';
		$i = 1;
		if($runSQL->num_rows() > 0)
		{
			foreach ($runSQL->result() as $row) {
				$main_tranno = $row->schoolid.'_'.$row->schlevelid.'_'.$row->year.'_'.$row->grade_levelid.'_'.$row->classid;
				$programid = $row->programid;
				$schlevelid = $row->schlevelid;
				$is_pt = $row->is_pt;
				$rpt_full_path = $row->is_pt - 0 == 0 ? 'c_full_time_final_report' : 'c_part_time_final_report';

				$arr_max_score = array("schoolid"=>$para_obj['schoolid'],"schlevelid"=>$para_obj['schoollavel'],
										"grade_levelid"=>$para_obj['gradelavel'],"classid"=>$para_obj['classid'],
										"yearid"=>$para_obj['years'],"termid"=>$para_obj['termid'],
										"studentid"=>$row->studentid);
				$show_score = $this->green->show_result_score_gep($arr_max_score);
				$tr .= '<tr>
							<td>'.($i++).'</td>
							<td id="student_num">'.$row->student_num.'
								<input type="hidden" id="showyear" value="'.$row->ftyear.'" name="showyear">
							</td>
							<td id="fullname">'.$row->fullname.'</td>
							<td>'.ucwords($row->gender).'</td>
							<td id="classname" data-is_pt="'.$row->is_pt.'">'.$row->class_name.'</td>
							<td>'.$row->term.'</td>
							<td style="text-align:center;">'.$show_score['core'].'</td>
							<td style="text-align:center;">'.$show_score['skill'].'</td>
							<td style="text-align:center;">'.($show_score['core']+$show_score['skill']).'</td>
							<td style="text-align:center;">';
						if($this->green->gAction("C")){ 
						$tr .= '<a href="javascript:void(0)" studentid="'.$row->studentid.'" main_tranno="'.$main_tranno.'" programid="'.$programid.'" is_pt="'.$is_pt.'" schlevelid="'.$schlevelid.'" id="add_comment">
									<img src="'.base_url('assets/images/icons/add.png').'">
								</a>&nbsp;';
							}
						// if($this->green->gAction("P")){ 
							$tr .= '<a href="'.site_url("reports/".$rpt_full_path."/index/?studentid=".$row->studentid."&programid=".$programid."&main_tranno=".$main_tranno."&is_pt=".$is_pt."&termid=".($para_obj['termid']>0?$para_obj['termid']:0)).'" target="_blank">
										<img src="'.base_url('assets/images/icons/a_preview.png').'">
									</a>';
						// }
						$tr .= '
							</td>
						</tr>';
			}
		}
		else{
			$tr .= '<tr>
						<td colspan="9" style="font-weight: bold;text-align: center;">No Results</td>
					</tr>';
		}
		
		//$arr = array('tr' => $tr, 'totalRecord' => $totalRecord, 'totalPage' => $totalPage,"pagenation"=>$paging); 
		$arr = array('tr' => $tr, "pagenation"=>$paging); 
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arr);
	}
	function save_commend_student(){
		$schoolid    = $this->input->post("schoolid");
		$schoollavel = $this->input->post("schoollavel");
		$years       = $this->input->post("years");
		$gradelavel  = $this->input->post("gradelavel");
		$classid     = $this->input->post("classid");
		$termid      = $this->input->post("termid");
		$hstudent_id = $this->input->post("hstudent_id");
		$is_partime  = $this->input->post("is_partime");
		$command     = $this->input->post("command");
		$date_acade  = $this->input->post("date_acade");
		$date_teacher= $this->input->post("date_teacher");
		$academic	= $this->input->post("academic");
		$userid 	= $this->session->userdata('userid');
		$wh_data    =  array("student_id"=>$hstudent_id,
							"schoolid"=>$schoolid,
							"schlevelid"=>$schoollavel,
							"yearid"=>$years,
							"gradelevelid"=>$gradelavel,
							"classid"=>$classid,
							"termid"=>$termid,
							"is_partime"=>$is_partime
						);
		if($command != ""){
			$count_num = $this->db->query("SELECT count(*) AS num_count
											FROM sch_studend_command_gep 
											WHERE 1=1
												AND student_id = {$hstudent_id}
												AND schoolid   = {$schoolid}
												AND schlevelid = {$schoollavel}
												AND yearid     = {$years}
												AND gradelevelid= {$gradelavel}
												AND classid    = {$classid}
												AND termid     = {$termid}
												AND is_partime = {$is_partime}
											")->row();
			if($count_num->num_count > 0){
				$data = array("student_id"=>$hstudent_id,
								"schoolid"=>$schoolid,
								"schlevelid"=>$schoollavel,
								"yearid"=>$years,
								"gradelevelid"=>$gradelavel,
								"classid"=>$classid,
								"termid"=>$termid,
								"command"=>$command,
								"is_partime"=>$is_partime,
								"academicid"=>$academic,
								"date_academic"=>$this->green->convertSQLDate($date_acade),
								"date_teacher"=>$this->green->convertSQLDate($date_teacher),
								"userid"=>$userid
							);
				
				$this->db->UPDATE("sch_studend_command_gep",$data,$wh_data);
			}else{
				$data = array("student_id"=>$hstudent_id,
								"schoolid"=>$schoolid,
								"schlevelid"=>$schoollavel,
								"yearid"=>$years,
								"gradelevelid"=>$gradelavel,
								"classid"=>$classid,
								"termid"=>$termid,
								"command"=>$command,
								"is_partime"=>$is_partime,
								"academicid"=>$academic,
								"date_academic"=>$this->green->convertSQLDate($date_acade),
								"date_teacher"=>$this->green->convertSQLDate($date_teacher),
								"userid"=>$userid
							);
				$this->db->insert("sch_studend_command_gep",$data);	
			}
			
		}else{
			$this->db->delete("sch_studend_command_gep",$wh_data);
		}
		echo "OK";
	}
	function show_command(){
		$schoolid    = $this->input->post("schoolid");
		$schoollavel = $this->input->post("schoollavel");
		$years       = $this->input->post("years");
		$gradelavel  = $this->input->post("gradelavel");
		$classid     = $this->input->post("classid");
		$termid      = $this->input->post("termid");
		$hstudent_id = $this->input->post("hstudent_id");
		$is_partime  = $this->input->post("is_partime");
		$count_num = $this->db->query("SELECT command,student_id,academicid,date_academic,date_teacher,userid
										FROM sch_studend_command_gep 
										WHERE 1=1
											AND student_id = {$hstudent_id}
											AND schoolid   = {$schoolid}
											AND schlevelid = {$schoollavel}
											AND yearid     = {$years}
											AND gradelevelid= {$gradelavel}
											AND classid    = {$classid}
											AND termid     = {$termid}
											AND is_partime = {$is_partime}
										")->row();
		$userid = "";
		if(isset($count_num->userid)){
			$userid = $count_num->userid;
		}else{
			$userid = $this->session->userdata('userid');
		}
		$sql_user = $this->db->query("SELECT
		                                    sch_user.user_name
		                                    FROM
		                                    sch_user
		                                    WHERE sch_user.userid='".$userid."'")->row();

		$username = isset($sql_user->user_name)?$sql_user->user_name:"";

		$arr_comm['command'] = array(
									isset($count_num->command)?$count_num->command:"",
									isset($count_num->student_id)?$count_num->student_id:"",
									isset($count_num->academicid)?$count_num->academicid:"",
									isset($count_num->date_academic)?$this->green->formatSQLDate($count_num->date_academic):date('d-m-Y'),
									isset($count_num->date_teacher)?$this->green->formatSQLDate($count_num->date_teacher):date('d-m-Y'),
									isset($count_num->userid)?$count_num->userid:"",
									"<b>".ucwords($username)."</b>"
									);
		echo json_encode($arr_comm);
	}
}