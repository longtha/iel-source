<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class classes extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('school/classmodel','getclassmode');
        $this->load->model('school/schoollevelmodel','schlevels');
		$this->load->library('pagination');
	}
	public function index()
	{
		$data['levels']=$this->getclassmode->getgradelevel();
		$data['labels']=$this->getclassmode->getgradelabel();
        $data['schevels']=$this->schlevels->getsch_level();
        $this->load->view('header');
		$this->load->view('school/class/add',$data);
		$this->load->view('school/class/view',$data);
		$this->load->view('footer');
	}
	function addnew()
	{
		$this->load->view('header');
		$this->load->view('school/class/add');
		$data['query']=$this->getclassmode->getgradelevel();
		$data['query1']=$this->getclassmode->getgradelabel();
		$this->load->view('school/class/edit',$data);
		$this->load->view('footer');
	}
	function export(){
		$this->load->view('school/class/z_export_to_excel');
	}
	function getvalidate(){
		$schoolid=$this->input->post('schoolid');
		$subjectid=$this->input->post('subjectid');
		$year=$this->input->post('year');
		$count=$this->getclassmode->getvalidate($schoolid, $subjectid, $year);
		if($count>0) echo "true"; else echo "false";

	}
	//---- save when add ----
	function saves()
	{
		date_default_timezone_set("Asia/Bangkok");
		$schoolid=$this->input->post('cboschool');
		$glabelid=$this->input->post('cbogradelabel');
		$classname=$this->input->post('glabel');
		$is_pt=$this->input->post('is_pt');
        $schlevelid=$this->input->post('sclevelid');
        $grade_levelid=$this->input->post('grade_levelid');
        $count=0;

        if(count($classname)>0){
              $i=0;
              foreach($classname as $class){
                  $count=$this->getclassmode->getvalidate($schoolid,$schlevelid,$class);
                  if($class!="" && $count==0){
                      $data=array(
                          'schoolid'=>$schoolid,
                          'grade_levelid'=>$grade_levelid[$i],
                          'grade_labelid'=>$glabelid,
                          'class_name'=>$class,
                          'created_by'=>$this->session->userdata('userid'),
                          'created_date'=>date('Y-m-d H:i:s'),
                          'schlevelid'=>$schlevelid,
                          'is_active'=>1,
                          'is_pt'=>$is_pt,
                          'modified_by'=>$this->session->userdata('userid'),
                          'modified_date'=>date('Y-m-d H:i:s'),
                      );
                      $this->db->insert('sch_class',$data);
                  }
                  $i++;
              }
          }
        $m='';
        $p='';
        if(isset($_GET['m'])){
            $m=$_GET['m'];
        }
        if(isset($_GET['p'])){
            $p=$_GET['p'];
        }
        redirect("school/classes/?save&m=".$m."&p=".$p);
	}

	function preview(){
		$this->load->view('header');
		$data1['query']=$this->getclassmode->getgradelevel();
		$data1['query1']=$this->getclassmode->getgradelabel();
		$data1['query2']=$this->getclassmode->getpagination();
		$data1['query3']=$this->getclassmode->getschoolyearrow($this->session->userdata('year'));
		$data1['query4']=$this->getclassmode->getschoolrow($this->session->userdata('schoolid'));
		$this->load->view('school/class/preview',$data1);

		$this->load->view('footer');
	}

	//---- Use when delete and update----
	function deleteclass($classid){
        $arr=$this->getclassmode->deleteclass($classid);
        echo json_encode($arr);
        exit();
	}
	function search(){
        $start=$this->input->post('startpage');
        $this->getclassmode->search($start);
	}
    function updateclass($classid){
        $json['up']=0;
        if($classid!=""){
            $classname=$this->input->post("classname");
            $idseis_pt=$this->input->post("idseis_pt");

            $up=$this->getclassmode->updateclass($classid,$classname,$idseis_pt);

            if($up){
                $json['up']=1;
            }
        }
        echo json_encode($json);
        exit();
    }

		function getClass() {
			$school_level = $this->input->post("school_level");
			$grade_level = $this->input->post("grade_level");
      $class = $this->getclassmode->callClass($school_level, $grade_level);

			echo json_encode($class);
      exit();
		}
}
