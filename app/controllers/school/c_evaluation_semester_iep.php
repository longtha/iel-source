<?php 
	class C_evaluation_semester_iep extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model("school/m_evaluation_semester_iep","eva");
		}

		function index(){
			$this->load->view('header');
			$this->load->view('reports/iep_reports/v_semester_acadimic_report');
			$this->load->view('footer');
		}

		function add(){
			$this->load->view('header');
			$this->load->view('reports/iep_reports/v_add_evaluation');
			$this->load->view('footer');
		}

		function list_description(){
			$studentid = $this->input->post('studentid');
			$table='';
			$i=1;

			 $cout_student_id = $this->db->query('SELECT COUNT(*) AS studentid FROM sch_evaluation_semester_iep_order WHERE studentid = "'.$studentid.'"')->row()->studentid;
			 
			if($cout_student_id > 0){
				$sql  = $this->eva->list_description();
				foreach($this->db->query($sql['sql'])->result() as $row){
					if($row->seldom == 1){
	                    $ch_sel ='checked';
	                }else{ 
	                    $ch_sel='';				
	                }
	                if($row->sometimes == 1){
	                    $ch_som ='checked';
	                }else{ 
	                    $ch_som='';				
	                }
	                if($row->usually == 1){
	                    $ch_usual='checked';
	                }else{ 
	                    $ch_usual='';				
	                }
	                if($row->consistently == 1){
	                    $ch_cons='checked';
	                }else{ 
	                    $ch_cons='';				
	                }			

					$table.= "<tr>
									<td class='des' attr_des_id=".$row->descrition_id.">".$row->description."</td>
									<td id='scor' class='seldom'><input type='checkbox'  readonly='readonly' $ch_sel id='seldom' class='radio_check ch_only'></td>
									<td id='scor'><input type='checkbox'readonly='readonly' $ch_som id='sometimes' class='radio_check ch_only'></td>
									<td id='scor'><input type='checkbox'readonly='readonly' $ch_usual id='usually' class='radio_check ch_only'></td>
									<td id='scor'><input type='checkbox'readonly='readonly' $ch_cons id='consistently' class='radio_check ch_only'></td>
							  </tr>";	
				}
			}else{ 
				$sql  = $this->eva->list_description();
				foreach($this->db->query($sql['sql'])->result() as $row){
					$table.= "<tr>
								<td class='des' attr_des_id=".$row->description_id.">".$row->description."</td>
								<td id='scor' class='seldom'><input type='checkbox'readonly='readonly'  id='seldom'​ class='radio_check ch_only'></td>
								<td id='scor'><input type='checkbox' id='sometimes'readonly='readonly' class='radio_check ch_only'></td>
								<td id='scor'><input type='checkbox'  id='usually'readonly='readonly' class='radio_check ch_only'></td>
								<td id='scor'><input type='checkbox' id='consistently'readonly='readonly' class='radio_check ch_only'></td>
						  </tr>";
				}	
			}

			header("Content-type:text/x-json");
			$arr['tr_data']=$table;		
			echo json_encode($arr);	
		}

		function save_evaluation(){
			echo $this->eva->save_evaluation();
		}
	}
?>