<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class time extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/timemodel','times');
		$this->load->library('pagination');		
	}
	public function index()
	{
		$this->load->view('header');
		$data['query']=$this->times->getpagination();
		$this->load->view('school/time/add');
		$this->load->view('school/time/view',$data);
		$this->load->view('footer');
	}
	function addnew()
	{
		$this->load->view('header');
		$this->load->view('school/time/time/add');
		$this->load->view('footer');	
	}
	function saves()
	{
		$schoolid=$this->input->post('cboschool');
		$fromtime=$this->input->post('txtfromtime');
		$totime=$this->input->post('txttotime');
		$duration=$this->input->post('txtduration');
		$timerelax=$this->input->post('txtrelax');
		$usefor=$this->input->post('tran');
		$ampm=$this->input->post('cboampm');
		
		$count=$this->times->getvalidate($fromtime, $totime, $usefor, $schoolid,$ampm);

		if ($count!=0){
			$this->load->view('header');
			$data['exist']="<p style='color:red;'>Your data has already exist...!</p>";
			$data['query']=$this->times->getpagination();
			$this->load->view('school/time/add',$data);
			$this->load->view('school/time/view',$data);
			$this->load->view('footer');
		}else{
			$data=array(
			  		'schoolid'=>$schoolid,
			  		'from_time'=>$fromtime,
			  		'to_time'=>$totime,
			  		'duration'=>$duration,
			  		'minute_relax'=>$timerelax,
			  		'for_tran_type'=>$usefor,
			  		'am_pm'=>$ampm,
			  );
			$this->db->insert('sch_time',$data);

			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }

			redirect("school/time/?save&m=$m&p=$p");	
		}
	}
	function edits($timeid)
	{
		$row=$this->times->gettimerow($timeid);
		$this->load->view('header');
		$data['query']=$row;
		$this->load->view('school/time/edit',$data);
		$data1['query']=$this->times->getpagination();
		$this->load->view('school/time/view',$data1);
		$this->load->view('footer');

	}
	function updates()
	{
		$timeid=$this->input->post('txttimeid');
		$schoolid=$this->input->post('cboschool');
		$fromtime=$this->input->post('txtfromtime');
		$totime=$this->input->post('txttotime');
		$duration=$this->input->post('txtduration');
		$timerelax=$this->input->post('txtrelax');
		$usefor=$this->input->post('tran');
		$ampm=$this->input->post('cboampm');

		$count=$this->times->getvalidateup($timeid,$fromtime,$totime,$usefor,$schoolid,$ampm);
		
			if ($count!=0){
				$this->load->view('header');
				$data['exist']="<p style='color:red;'>Your data has already exist...!</p>";
				$data['query']=$this->times->getpagination();
				$this->load->view('school/time/add',$data);
				$this->load->view('school/time/view',$data);
				$this->load->view('footer');
			}else{
					$this->db->where('timeid',$timeid);

					$data=array(
								'schoolid'=>$schoolid,
						  		'from_time'=>$fromtime,
						  		'to_time'=>$totime,
						  		'duration'=>$duration,
						  		'minute_relax'=>$timerelax,
						  		'for_tran_type'=>$usefor,
						  		'am_pm'=>$ampm,
					);
					$this->db->update('sch_time',$data);

			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("school/time/?edit&m=$m&p=$p");
			}
	}
	function deletes($timeid)
	{
		// $this->db->where('grade_levelid',$grade_levelid);
		// $data=array('is_active'=>0);
		// $this->db->update('sch_grade_level',$data);

		$this->db->where('timeid', $timeid);
  		$this->db->delete('sch_time');

  		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }

        redirect("school/time/?delete&m=$m&p=$p");
	}
	function searchs(){
		  //----------- Pagination -----------
		   if(isset($_GET['s'])){
			   $y=$_GET['y'];
			   //$sch=$_GET['sch'];
			   $data['query']=$this->times->searchtime($y);

			   $this->load->view('header');
			   $this->load->view('school/time/add');
			   $this->load->view('school/time/view',$data);
			   $this->load->view('footer');
		  }

		  if(!isset($_GET['s'])){
		   $schoolid=$this->input->post('schoolid');

		   $m=$this->input->post('m');
		   $p=$this->input->post('p');
		   $this->green->setActiveRole($this->input->post('roleid'));
		   if($m!=''){
		        $this->green->setActiveModule($m);
		   }
		   if($p!=''){
		        $this->green->setActivePage($p); 
		   }

		   $query=$this->times->searchtime($schoolid);
		    $i=1;
		    foreach ($query as $row) {
		     echo "		      
			        <tr>
			          <td align=center >$i</td>
			          <td>$row->name</td>
			          <td>".date('H:i',strtotime($row->from_time))."</td>
			          <td>".date('H:i',strtotime($row->to_time))."</td>
			          <td>$row->am_pm</td>
			          <td>".date('H:i',strtotime($row->duration))."</td>
			          <td>$row->minute_relax</td>
			          <td>";
				          if ($row->for_tran_type==1){
				          	echo 'Study';
				          }else if ($row->for_tran_type==2){
				          	echo 'Teach';
				          }else if($row->for_tran_type==3){
				          	echo 'Off work';
				          }
			 echo     "</td>
			          <td align=center>";
			          	  if($this->green->gAction("U")){
			              	echo "<a href='".site_url('school/time/edits/'.$row->timeid)."?m=$m&p=$p"."'>
			              		  <img src='".site_url('../assets/images/icons/edit.png')."' />
			              		  </a> |";
			              }
			              if($this->green->gAction("D")){
			              	echo "<a><img onclick='deletetime(event);' rel='$row->timeid' src='".site_url('../assets/images/icons/delete.png')."' />
			              		  </a>";
			              }
			          echo "</td>
			        </tr>";
			        $i++;
		 }
   //	  echo "<tr>
   //   			<td colspan='12' id='pgt'>
   //   				<div style='text-align:left'>
   //   					<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
   //   				</div></td>
   //  		   </tr>";
		}
	}
}