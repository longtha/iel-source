<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class subjecttype extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/subjecttypemodel','subjecttypes');
        $this->load->model('school/schoolinformodel','sch');
        $this->load->model('school/schoolyearmodel','schyear');
        $this->load->model('school/schoollevelmodel','schlevels');
        $this->load->model('school/modrangelevel','rnglev');
        $this->load->model('school/gradelevelmodel','gradlev');

		$this->load->library('pagination');	
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m = $_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p = $_GET['p'];
	    }	
	     $this->green->setActiveRole($this->session->userdata('roleid'));
		   if($m!=''){
		        $this->green->setActiveModule($m);
		   }
		   if($p!=''){
		        $this->green->setActivePage($p); 
		   }
	}
	public function index()
	{
		$this->load->view('header');

        $data['schevels']=$this->schlevels->getsch_level("1");
        $data['schyears']=$this->schyear->getschoolyear();
        $data['rangelevs']=$this->rnglev->rangelevels();

		$this->load->view('school/subjecttype/add',$data);
		$this->load->view('school/subjecttype/view');
		$this->load->view('footer');
	}
	function addnew()
	{
		$this->load->view('header');
		$this->load->view('school/subjecttype/subjecttype/add');
		$this->load->view('footer');	
	}
	function savesubjecttype()
	{
		$subj_type_id =$this->input->post('subj_type_id');
        $save = $this->subjecttypes->save($subj_type_id);
        header("Content-type:text/x-json");
		$arrJson["res"]=$save;
        echo json_encode($arrJson);
        exit();		
	}
	function editsubjecttype($subj_type_id)
	{
		$row=$this->subjecttypes->getsubjecttyperow($subj_type_id);
		$this->load->view('header');
		$data['query']=$row;
		$data1['query']=$this->db->get('sch_subject_type')->result();

        $data['schyears']=$this->schyear->getschoolyear();
        $data['schevels']=$this->schlevels->getsch_level("1");
        //$data['rangelevs']=$this->rnglev->rangelevels();

        $this->load->view('school/subjecttype/edit',$data);
		$this->load->view('school/subjecttype/view',$data1);
		$this->load->view('footer');	
	}
	
	function deletesubjecttype($subj_type_id)
	{
		$this->db->where('subj_type_id', $subj_type_id);
        $this->db->delete('sch_subject_type');
        $m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
        redirect("school/subjecttype/?delete&m=$m&p=$p");
	}
	function search(){
		
		  //----------- Pagination -----------
		   if(isset($_GET['main_type'])){
		   $subjecttype=$_GET['subjecttype'];
		   $maintype=$_GET['main_type'];

           $schoolid=$_GET('sid');
           $schlevelid=$_GET('slid');
           $rangelevelid=$_GET('rgid');
           $yearid=$_GET('yid');

		   $data['query']=$this->subjecttypes->searchsubjecttype($subjecttype,$maintype,$schoolid,$schlevelid,$yearid,$rangelevelid);
		   $this->load->view('header');
		   $this->load->view('school/subjecttype/add');
		   $this->load->view('school/subjecttype/search',$data);
		   $this->load->view('footer');
		  }
		  if(!isset($_GET['main_type'])){
		   $subjecttype=$this->input->post('subjecttype');
		   $maintype=$this->input->post('maintype');

           $schoolid=$this->input->post('schoolid');
           $schlevelid=$this->input->post('schlevelid');
           $rangelevelid=$this->input->post('rangelevelid');
           $yearid=$this->input->post('yearid');

		   $m=$this->input->post('m');
		   $p=$this->input->post('p');
		   $this->green->setActiveRole($this->input->post('roleid'));
		   if($m!=''){
		        $this->green->setActiveModule($m);
		   }
		   if($p!=''){
		        $this->green->setActivePage($p); 
		   }

		   $query=$this->subjecttypes->searchsubjecttype($subjecttype,$maintype,$schoolid,$schlevelid,$yearid,$rangelevelid);

		    $i=1;
            $arrMo=array("TAE","MoEYS");
		    foreach ($query as $subjecttyperow) {
		     echo "
    		        <tr>
			          <td align=center width=40>$i</td>
			          <td width=170>$subjecttyperow->subject_type</td>
			          <td>$subjecttyperow->note</td>
			          <td>".($subjecttyperow->main_type==1?"ចំណេះដឹងទូទៅភាសាខ្នែរ":"ភាសាបរទេស​ និងជំនាញ")."</td>
			          <td width=170>$subjecttyperow->sch_level</td>
			          <td>".($subjecttyperow->is_group_calc==1?"Yes":"No")."</td>
			          <td>".$subjecttyperow->is_group_calc_percent."</td>
			          <td>".($subjecttyperow->is_class_participation==1?"Yes":"No")."</td>
			          <td>".$subjecttyperow->is_attendance_qty."</td>
			          <td width=170 class='hide'>".$arrMo[$subjecttyperow->is_moeys]."</td>
			          <td align=center width=130>";
			          	  if($this->green->gAction("U")){
			              	echo "<a href='".site_url('school/subjecttype/editsubjecttype/'.$subjecttyperow->subj_type_id)."?m=$m&p=$p"."'>
			              	<img src='".site_url('../assets/images/icons/edit.png')."' />
			              </a> |";
			          	  }
			          	  if($this->green->gAction("D")){
			              	echo "<a onclick='deletesubjecttype(event);' rel='$subjecttyperow->subj_type_id'><img src='".site_url('../assets/images/icons/delete.png')."' />
			              	</a>";
			              } 
			          echo "</td>
			        </tr>";
			        $i++;
		 }
		 echo "<tr>
     			<td colspan='10' id='pgt'>
     				<div style='text-align:left'>
     					<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
     				</div></td>
    		   </tr>";
		}
	}
	public function cgetgradelevels(){	
		$schlevelid = $this->input->post("schlevelid");		
		$option_gradlev ="";
		foreach ($this->gradlev->getgradelevels($schlevelid) as $row){
			$option_gradlev .='<option value="'.$row->grade_levelid.'">'.$row->grade_level.'</option>';
		}		
		header("Content-type:text/x-json");
		$arr_success['gradid'] = $option_gradlev;
		echo json_encode($arr_success);
				
	}
	public function cgetschoolyear(){		
		$schoolid = $this->input->post('schoolid');	
		$sch_program = "";
		$schlevelid = $this->input->post("sch_level");		
		$i =0;		
		$option_year ='';

		foreach ($this->schyear->getschoolyear($schoolid,$sch_program,$schlevelid) as $row){
			$option_year .='<option value="'.$row->yearid.'"'.($row->yearid != $this->session->userdata('year')?"":"selected").'>'.$row->sch_year.'</option>';
		}		
		header("Content-type:text/x-json");
		$arr_success['schyear'] = $option_year;
		echo json_encode($arr_success);
				
	}
}