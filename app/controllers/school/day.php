<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class day extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/daymodel','days');
		$this->load->library('pagination');		
	}
	public function index()
	{
		$this->load->view('header');
		//school/schoolinfor is call view
	
		//for pagination is use $data, if not use $data
		$data['query']=$this->days->getpagination();
		$this->load->view('school/day/add');
		$this->load->view('school/day/view',$data);
		$this->load->view('footer');
	}
	function addnew()
	{
		$this->load->view('header');
		//school/schoolinfor is call view
		$this->load->view('school/gradelabel/gradelabel/add');
		$this->load->view('footer');	
	}
	function saves()
	{
		$day=$this->input->post('day');

		$count=$this->days->getvalidate($day);
		echo $count.'-';
		if ($count!=0){
			echo 'false';
		}else{
			$data=array(
			  		'dayname'=>$day,
			);	
			$this->db->insert('sch_day',$data);

			echo'true';	
		}
	}
	function edits($dayid)
	{
		$row=$this->days->getdayrow($dayid);
		$this->load->view('header');
		$data['query']=$row;
		//school/gradelevel is call view
		//$data1['query']=$this->db->get('sch_subject')->result();
		$this->load->view('school/day/edit',$data);
		$data1['query']=$this->days->getpagination();
		$this->load->view('school/day/view',$data1);
		$this->load->view('footer');

	}
	function updates()
	{
		$dayid=$this->input->post('dayid');
		$day=$this->input->post('day');

		$count=$this->days->getvalidateup($dayid);
			echo $count.'-';
			if ($count!=0){
				echo 'false';
			}else{
					$this->db->where('dayid',$dayid);

					$data=array(
								'dayname'=>$day,
					);
					$this->db->update('sch_day',$data);

			echo'true';
			}
	}
	function deletes($dayid)
	{
		// $this->db->where('grade_levelid',$grade_levelid);
		// $data=array('is_active'=>0);
		// $this->db->update('sch_grade_level',$data);

		$this->db->where('dayid', $dayid);
  		$this->db->delete('sch_day');
        //--- delete is variable for message ---
        redirect('school/day/?delete');
	}
	function searchs(){
		  //----------- Pagination -----------
		   if(isset($_GET['s'])){
			   $y=$_GET['y'];
			   //$sch=$_GET['sch'];
			   $data['query']=$this->days->searchday($y);

			   $this->load->view('header');
			   $this->load->view('school/day/add');
			   $this->load->view('school/day/view',$data);
			   $this->load->view('footer');
		  }

		  if(!isset($_GET['s'])){
		   $day=$this->input->post('day');

		   $query=$this->days->searchday($day);
		    $i=1;
		    foreach ($query as $row) {
		     echo "		      
			        <tr>
			          <td align=center >$i</td>
			          <td>$row->dayname</td>
			          <td align=center width=130>
			              <a href='".site_url('school/day/edits/'.$row->dayid)."'><img src='".site_url('../assets/images/icons/edit.png')."' /></a> |
			              <a><img onclick='deleteday(event);' rel='$row->dayid' src='".site_url('../assets/images/icons/delete.png')."' /></a> 
			          </td>
			        </tr>
			        " ;
			        $i++;
		 }
   //	  echo "<tr>
   //   			<td colspan='12' id='pgt'>
   //   				<div style='text-align:left'>
   //   					<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
   //   				</div></td>
   //  		   </tr>";
		}
	}
}