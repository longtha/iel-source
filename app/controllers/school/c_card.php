<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_card extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('school/modcard', 'c');

        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolyearmodel', 'y');
        // $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');
        // $this->load->model('school/gradelevelmodel', 'g');
        $this->load->model('school/classmodel', 'cl');
	}

	public function index(){
		$this->load->view('header');		
		$this->load->view('school/card/v_card');
		$this->load->view('footer');	
	}

	public function grid(){
    	$grid = $this->c->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $grid;
    }

    public function get_schlevel(){
        $programid = $this->input->post('programid');
        $get_schlevel = $this->c->get_year($programid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel;
    }

    public function get_year(){
        $schlevelid = $this->input->post('schlevelid');
        $get_year = $this->c->get_year($schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    public function get_class(){
        $schlevelid = $this->input->post('schlevelid');       
        $get_class = $this->c->get_class($schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_class;
    }    

}