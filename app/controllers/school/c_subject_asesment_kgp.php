<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class c_subject_asesment_kgp extends CI_Controller{
	protected $thead;
	protected $idfield;
	function __construct() {
	    parent::__construct();
	    $this->load->model("school/m_subject_asesment_percentag_kgp","sapkgp");

	    $this->thead = array("No" => 'no',
	    					"School Level"=>'school_level_id',   					
	    					"Subject Type"=>'subject_type_id',
	    					"Subject"=>'subject_id',
	    					"Assesment Name"=>'assessment_id',
	    					"Max Score"=>'maxscore',
	    					"Percentag (%)"=>'percentage',
	    					"Grade Level"=>'grade_level_id',
	    					"Action"=>'Action'
	                        );
	    $this->idfield = "studentid";
	}
	function show_main_subject(){
		$get_main_subj = $this->sapkgp->getsubjecttype();
		$opt_main_subj = "<option value=''></option>";
		if($get_main_subj->num_rows() > 0){
			foreach($get_main_subj->result() as $r_subj_m){
				$opt_main_subj.= "<option value='".$r_subj_m->subj_type_id."'>".$r_subj_m->subject_type."</option>";
			}
		}
		echo $opt_main_subj;
	}
	function show_subject(){
		$get_subj = $this->sapkgp->getsubject();
		$opt_subj = "<option value=''></option>";
		if($get_subj->num_rows() > 0){
			foreach($get_subj->result() as $r_subj){
				$opt_subj.= "<option value='".$r_subj->subjectid."'>".$r_subj->subject_kh."</option>";
			}
		}
		echo $opt_subj;
	}
	function index() {
	    $data['idfield'] = $this->idfield;
		$data['thead'] = $this->thead;
		$data['school']=$this->sapkgp->show_school();
		$data['program']=$this->sapkgp->show_program();
		//$data['getsubjecttype']=$this->sapkgp->getsubjecttype();
		$data['getschoollavel']=$this->sapkgp->getschoollavel();
	    $this->load->view('header',$data);
	    $this->load->view('school/subjectasesment/v_subject_asesment_kgp',$data);
	    $this->load->view('footer',$data );
	}
	// save -------------------------------------------------------------
	function save(){
		$data_save = $this->sapkgp->save();
		echo $data_save;
	}
	// delete -----------------------------------------------------------
	// function delete(){
	// 	$ass_percent_id = $this->input->post('ass_percent_id');
	// 	if(intval($ass_percent_id) > 0 ) {
	// 	   $this->db->where('ass_percent_id',$ass_percent_id)->delete('sch_subject_assessment_percent_kgp');
	// 	   $msgbox = 'Data deleted!';
	// 	}else{
	// 		$msgbox = 'errorses';
	// 	}
	// 	$arrDel = array();
	// 	header('Content-type:text/x-json');
	// 	$arrDel['success']=$msgbox;
	// 	echo json_encode($arrDel);
	// 	die();
	// }
	//get_schyera ------------------------------------------------------------
	function get_schyera(){
		$schlevel_id = $this->input->post('schlevel_id');
		$get_schyera = $this->sapkgp->get_schyera($schlevel_id);
		header("Content-type:text/x-json");
		echo ($get_schyera);
	}
	
	//getsubject ------------------------------------------------------------
	function getsubject(){
		$subj_type_id = $this->input->post('subj_type_id');
		$getsubject = $this->sapkgp->getsubject($subj_type_id);
		header("Content-type:text/x-json");
		echo ($getsubject);
	}

	//getsubject ------------------------------------------------------------
	function getgradlevels(){
	    $get_gradelevel = $this->sapkgp->gradelevel();
	    $option_grade = "";
	    if($get_gradelevel->num_rows() > 0){
	    	foreach($get_gradelevel->result() as $rgradlevel){
	    		$option_grade.="<option value='".$rgradlevel->grade_levelid."'>".$rgradlevel->grade_level."</option>";
	    		// $tr.="<tr><td><input type='checkbox'class='gradelevelid' attr-id='".$rgradlevel->grade_levelid."' style='margin-left:20px;'>&nbsp;".$rgradlevel->grade_level."</td></tr>";
	    	}
	    }
	    echo $option_grade;
	}

	// showdata -------------------------------------------------
	function showdata(){
		$select_data = $this->db->query("SELECT
											sch_subject_assessment_kgp.assess_id,
											sch_subject_assessment_kgp.assess_name,
											sch_subject_assessment_kgp.percentag,
											sch_subject_assessment_kgp.max_score,
											sch_subject_assessment_kgp.userid,
											sch_subject_assessment_kgp.grade_level_id,
											sch_subject.subject_kh as `subject`,
											sch_subject_type.subject_type,
											sch_school_level.sch_level
											FROM
											sch_subject_assessment_kgp
											INNER JOIN sch_subject ON sch_subject_assessment_kgp.subject_exam_id = sch_subject.subjectid
											INNER JOIN sch_subject_type ON sch_subject_assessment_kgp.group_subj_id = sch_subject_type.subj_type_id
											INNER JOIN sch_school_level ON sch_subject_assessment_kgp.schoollevelid = sch_school_level.schlevelid
											");
		$table = "";
		$jj = 1;
		if($select_data->num_rows() > 0){
			foreach($select_data->result() as $rasm){
				$show_img = "<a href='javascript:void(0)' id='edit_ass' att_edit='".$rasm->assess_id."'><img src='".base_url("/assets/images/icons/edit.png")."'></a>
							<a href='javascript:void(0)' id='delete_ass' att_del='".$rasm->assess_id."'><img src='".base_url("/assets/images/icons/delete.png")."'></a>
							";
				$grade_level = $this->db->query("SELECT
													sch_grade_level.grade_level
													FROM
													sch_grade_level
													WHERE grade_levelid='".$rasm->grade_level_id."'
													");
				$grade_level_name = "";
				if($grade_level->num_rows() > 0){
					foreach($grade_level->result() as $rowg){
						$grade_level_name = $rowg->grade_level;
					}
					
				}
				
				$table.="<tr>
							<td>".$jj."</td>
							<td>".$rasm->sch_level."</td>
							<td>".$rasm->subject_type."</td>
							<td>".$rasm->subject."</td>
							<td>".$rasm->assess_name."</td>
							<td style='text-align:center;'>".$rasm->max_score."</td>
							<td style='text-align:center;'>".$rasm->percentag."</td>
							<td>".$grade_level_name."</td>
							<td style='text-align:center;'>".$show_img."</td>
						</tr>";
				$jj++;
			}
		}
		echo $table;
	}
	function show_edit_assessment(){
		$id_assessment = $this->input->post("id_assessment");
		$qr_order = "SELECT
						sch_subject_assessment_kgp.assess_id,
						sch_subject_assessment_kgp.assess_name,
						sch_subject_assessment_kgp.percentag,
						sch_subject_assessment_kgp.max_score,
						sch_subject_assessment_kgp.schoolid,
						sch_subject_assessment_kgp.programid,
						sch_subject_assessment_kgp.schoollevelid,
						sch_subject_assessment_kgp.grade_level_id,
						sch_subject_assessment_kgp.group_subj_id,
						sch_subject_assessment_kgp.subject_exam_id,
						sch_subject_assessment_kgp.userid
						FROM
						sch_subject_assessment_kgp
						WHERE 1=1 AND assess_id='".$id_assessment."'";
		
		$arr_ord = array();
		$run_qr = $this->db->query($qr_order);
		if($run_qr->num_rows() > 0){
			foreach($run_qr->result() as $vorder){
				$schoolid      = $vorder->schoolid;
				$programid     = $vorder->programid;
				$schoollevelid = $vorder->schoollevelid;
				$grsubject     = $vorder->group_subj_id;
				$assess_id      = $vorder->assess_id;
				$grade_level_id      = $vorder->grade_level_id;
				$assess_name = $vorder->assess_name;
				$percentag     = $vorder->percentag;

				$max_score     = $vorder->max_score;
				$subject_exam_id      = $vorder->subject_exam_id;
				$arr_ord['order'] = array($schoolid,$programid,$schoollevelid,$assess_id,$assess_name,$percentag,$max_score);
				$qr_main_sub = $this->db->query("SELECT subj_type_id,subject_type 
	                                                FROM sch_subject_type
	                                                WHERE 1=1 
	                                                AND schoolid='".$schoolid."'
	                                                AND schlevelid='".$schoollevelid."'
	                                                ");
				
				$opt_main_subj = "<option value=''></option>";
				if($qr_main_sub->num_rows() > 0){
					foreach($qr_main_sub->result() as $r_subj_m){
						$opt_main_subj.= "<option value='".$r_subj_m->subj_type_id."' ".($r_subj_m->subj_type_id==$grsubject?"selected":"").">".$r_subj_m->subject_type."</option>";
					}
				}
				$qr_sub = $this->db->query("SELECT
		                                        sch_subject.subjectid,
		                                        sch_subject.`subject_kh`
		                                        FROM
		                                        sch_subject
		                                        WHERE 1 = 1
		                                        AND schoolid ='".$schoolid ."'
		                                        AND subj_type_id ='".$grsubject."'
		                                        AND programid ='".$programid."'
		                                        AND schlevelid ='".$schoollevelid."'");
		        $opt_subj = "<option value=''></option>";
				if($qr_sub->num_rows() > 0){
					foreach($qr_sub->result() as $r_sub){
						$opt_subj.= "<option value='".$r_sub->subjectid."' ".($r_sub->subjectid==$subject_exam_id?"selected":"").">".$r_sub->subject_kh."</option>";
					}
				}
				$qr_gradlevel = $this->db->query("SELECT
	                                                sch_grade_level.grade_levelid,
	                                                sch_grade_level.grade_level
	                                                FROM
	                                                sch_grade_level
	                                                WHERE schlevelid='".$schoollevelid."' 
	                                                AND schoolid='".$schoolid."'
	                                                ");
				$opt_gradelevel = "";
				if($qr_gradlevel->num_rows() > 0){
			    	foreach($qr_gradlevel->result() as $rgradlevel){
			    		$selected = "";
			    		if($rgradlevel->grade_levelid == $grade_level_id){
			    			$selected = "selected='selected'";
			    		}
			    		$opt_gradelevel.="<option value='".$rgradlevel->grade_levelid."' ".$selected.">".$rgradlevel->grade_level."</option>";
			    		//$tr_gradelevel.="<tr><td><input type='checkbox' class='gradelevelid' attr-id='".$rgradlevel->grade_levelid."' ".$checkbox." style='margin-left:20px;'>&nbsp;".$rgradlevel->grade_level."</td></tr>";
			    	}
			    }	
			}
			$arr_ord['groupsubject'] = $opt_main_subj;
			$arr_ord['subjectexamp'] = $opt_subj;
			$arr_ord['gradlevel']    = $opt_gradelevel;
		}
		echo json_encode($arr_ord);
	}
	function delete_ass(){
		$ass_id = $this->input->post("id_assement");
		$data = array();
		try{
			$sql_check = $this->db->query("SELECT count(*) as amt 
											FROM sch_subject_score_monthly_detail_kgp 
											WHERE subj_assem_id ='".$ass_id."' 
											GROUP BY subj_assem_id");
			if($sql_check->num_rows() > 0){
				$data['data'] = 100;
			}else{
				$this->db->delete("sch_subject_assessment_kgp",array("assess_id"=>$ass_id));
				//$this->db->delete("sch_subject_assessment_detail_kgp",array("subj_assem_id"=>$ass_id));
				$data['data'] = 200;
			}
			
		}catch(Exception $e){
			echo 'Message: ' .$e->getMessage();
		}
		echo json_encode($data);
	}
}