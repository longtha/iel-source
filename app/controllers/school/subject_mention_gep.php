<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subject_mention_gep extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('school/mod_subject_mention_gep', 'sm');
        // $this->load->model('school/program', 'p');
        // $this->load->model('school/schoolyearmodel', 'y');
        $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');
        $this->load->model('school/subjectmodel', 'sub');

        $this->load->model('school/gradelevelmodel', 'g');
	}

	public function index(){	
		$this->load->view('header');		
		$this->load->view('school/subjectmention_gep/v_subjectmention_gep');
		$this->load->view('footer');	
	}    

	public function grid(){
    	$grid = $this->sm->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $grid;
    }

    public function save(){
        $i = $this->sm->save();       
        header('Content-Type: application/json; charset=utf-8');
        echo $i;
    }

    public function edit(){ 
        $examid = $this->input->post('examid') - 0;
        $row = $this->sm->edit($examid);
        header('Content-Type: application/json; charset=utf-8');
        echo $row;      
    }

    public function delete(){   
        $examid = $this->input->post('examid') - 0;
        $i = $this->sm->delete($examid);
        header('Content-Type: application/json; charset=utf-8');
        echo $i;
    }

    public function get_subject(){ 
        $schlevelid = $this->input->post('schlevelid');
        $get_subject = $this->sm->get_subject($schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_subject;      
    }

    public function get_grade(){
        $schlevelid = $this->input->post('schlevelid');
        $get_grade = $this->sm->get_grade($schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_grade;
    }    

}