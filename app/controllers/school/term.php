<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Term extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('school/modterm', 'term');
        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolyearmodel', 'y');
        $this->load->model('school/schoolinformodel', 'info');
        $this->load->model('school/schoollevelmodel', 'level');
        $this->load->model('school/modfeetype', 'f');
        $this->load->model('school/modrangelevel', 'r');
        $this->load->model('school/modsemester', 'se');

        $this->load->library('pagination');
    }

    public function index()
    {
        $this->load->view('header');
        $this->load->view('school/term/index');
        $this->load->view('footer');
    }

    public function save()
    {
        $i = $this->term->save();
        header('Content-Type: application/json; charset=utf-8');
        echo $i;
    }

    public function delete()
    {
        $termid = $this->input->post('termid') - 0;
        $i = $this->term->delete($termid);
        header('Content-Type: application/json; charset=utf-8');
        echo $i;
    }

    public function grid()
    {
        $term = $this->term->grid();
        header('Content-Type: application/json; charset=utf-8');
        echo $term;
    }

    public function edit()
    {
        $termid = $this->input->post('termid') - 0;
        $row = $this->term->edit($termid);

        header('Content-Type: application/json; charset=utf-8');
        echo $row;
    }

    public function get_schlevel()
    {
        $programid = $this->input->post('programid');
        $get_schlevel = $this->term->get_schlevel($programid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel;
    }

    public function get_year()
    {
        $programid = $this->input->post('programid') - 0;
        $schlevelid = $this->input->post('schlevelid') - 0;
        $get_year = $this->term->get_year($programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    public function get_semester()
    {
        $programid = $this->input->post('programid') - 0;
        $schlevelid = $this->input->post('schlevelid') - 0;
        $yearid = $this->input->post('yearid') - 0;

        // semesters($schoolid="",$yearid="",$programid="",$schlevelid,$semesterid="")
        $get_semester = $this->term->get_semester($yearid, $programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_semester;
    }

    public function get_rangelevel()
    {
        $termid = $this->input->post('termid') - 0;
        $schlevelid = $this->input->post('schlevelid');
        $get_rangelevel = $this->term->get_rangelevel($termid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_rangelevel;
    }

    public function get_rangelevel_search()
    {
        $schlevelid = $this->input->post('schlevelid');
        $get_rangelevel_search = $this->term->get_rangelevel_search($schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_rangelevel_search;
    }

    // ===============
    public function get_program_search()
    {
        $programid_search = $this->input->post('programid_search') - 0;
        $get_schlevel_search = $this->term->get_schlevel($programid_search);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel_search;
    }

    public function get_year_search()
    {
        $programid_search = $this->input->post('programid_search') - 0;
        $schlevelid_search = $this->input->post('schlevelid_search') - 0;
        $get_year_search = $this->term->get_year($programid_search, $schlevelid_search);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year_search;
    }


    function jsontermweek($termid)
    {
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($this->term->getCkWeekTerm($termid));
    }

    function show_weeks(){ 
    $termid = $this->input->post('termid');       
       $sql  = $this->term->show_weeks($termid);
       $show_w = "";
       if($sql->num_rows() > 0){
            $show_w = $sql->result();
       }       
        header("Content-type:text/x-json");
        $arr['tr_data']=$show_w;     
        echo json_encode($arr);       
    }

    // public function examtype_iep(){
    //     $sql_examtype = $this->db->query("SELECT
    //                                         sch_student_examtype_iep.examtypeid,
    //                                         sch_student_examtype_iep.exam_test
    //                                         FROM
    //                                         sch_student_examtype_iep
    //                                         ");
    //     return $sql_examtype;
    // }

}