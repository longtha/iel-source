<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rangeslevel extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model('school/schoolinformodel','sch');
        $this->load->model('school/schoollevelmodel','schlevels');
        $this->load->model('school/modrangelevel','rnglev');
        $this->load->model('school/moddayrange','daysrg');
        $this->load->model('school/timemodel','mtime');
        $this->load->model('school/modshift','mshift');
        $this->load->model('school/classmodel','mclass');
    }
	public function index()
	{
        $data['schevels']=$this->schlevels->getsch_level();
        $data['dayrange']=$this->daysrg->dayranges();
        $data['times']=$this->mtime->times();
        $data['shifts']=$this->mshift->shifts();
        $data['classes']=$this->mclass->allclass();
        $this->load->view('header');
		$this->load->view('school/rangelevel/rangeslevel',$data);
		$this->load->view('footer');
	}
	function save()
	{
        $rangelevelid=$this->input->post('rangelevelid');
       if($rangelevelid!=""){
           $this->rnglev->save($rangelevelid);
       }else{
           $this->rnglev->save();
       }

	}
	function delete($rangelevelid){
        $arr=$this->rnglev->delete($rangelevelid);
        echo json_encode($arr);
        exit();
	}
    function  edit($rangelevelid){
        $arr=$this->rnglev->edit($rangelevelid);
        echo json_encode($arr);
        exit();
    }
	function search(){
        $start=$this->input->post('startpage');
        $this->rnglev->search($start);
	}
    function getClass($schlevelid){
        $arr['datas']=$this->mclass->allclass($schlevelid);
        echo json_encode($arr);
        exit();
    }

}