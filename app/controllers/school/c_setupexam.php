<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_setupexam extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('school/modsetupexam', 'e');

        $this->load->model('school/program', 'p');
        $this->load->model('school/schoolyearmodel', 'y');
        $this->load->model('school/schoolinformodel', 'info');                
        $this->load->model('school/schoollevelmodel', 'level');

        $this->load->model('school/gradelevelmodel', 'g');
	}

	public function index(){	
		$this->load->view('header');		
		$this->load->view('school/exam/v_setupexam');
		$this->load->view('footer');	
	}

	public function grid(){
    	$grid = $this->e->grid();
		header('Content-Type: application/json; charset=utf-8');
		echo $grid;
    }

    public function save(){
        $i = $this->e->save();       
        header('Content-Type: application/json; charset=utf-8');
        echo $i;
    }

    public function update(){
        $i = $this->e->update();       
        header('Content-Type: application/json; charset=utf-8');
        echo $i;
    }

    public function edit(){ 
        $examid = $this->input->post('examid') - 0;
        $row = $this->e->edit($examid);
        header('Content-Type: application/json; charset=utf-8');
        echo $row;      
    }

    public function edit_grade(){ 
        $schlevelid = $this->input->post('schlevelid') - 0;
        $edit_grade = $this->e->edit_grade($schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $edit_grade;      
    }

    public function edit_year(){ 
        $schlevelid = $this->input->post('schlevelid') - 0;
        $edit_year = $this->e->edit_year($schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $edit_year;      
    }

    public function delete(){   
        $examid = $this->input->post('examid') - 0;
        $i = $this->e->delete($examid);
        header('Content-Type: application/json; charset=utf-8');
        echo $i;
    }

    public function get_schlevel(){
        $programid = $this->input->post('programid');        
        $get_schlevel = $this->e->get_schlevel($programid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_schlevel;
    }

    public function get_year(){
        $programid = $this->input->post('programid');
        $schlevelid = $this->input->post('schlevelid');
        $get_year = $this->e->get_year($programid, $schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_year;
    }

    public function get_program(){
        $schlevelid = $this->input->post('schlevelid');
        $get_program = $this->e->get_program($schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_program;
    }

    public function get_grade(){
        $schlevelid = $this->input->post('schlevelid');
        $get_grade = $this->e->get_grade($schlevelid);
        header('Content-Type: application/json; charset=utf-8');
        echo $get_grade;
    }    

}