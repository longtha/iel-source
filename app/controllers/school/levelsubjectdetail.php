<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class levelsubjectdetail extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('school/levelsubjectdetailmodel','levelsubjectdetails');
		$this->load->library('pagination');		
	}
	public function index()
	{
		$this->load->view('header');
		//school/schoolinfor is call view
	
		//for pagination is use $data, if not use $data
		$data['query']=$this->levelsubjectdetails->getgradelevel();
		$data['query1']=$this->levelsubjectdetails->getsubject();
		$data['query2']=$this->levelsubjectdetails->getpagination();
		$this->load->view('school/levelsubjectdetail/add',$data);
		//$this->load->view('school/levelsubjectdetail/edit',$data);
		$this->load->view('school/levelsubjectdetail/view',$data);
		$this->load->view('footer');
	}
	function addnew()
	{
		$this->load->view('header');
		//school/schoolinfor is call view
		$this->load->view('school/levelsubjectdetail/add');
		$data['query']=$this->levelsubjectdetails->getgradelevel();
		$this->load->view('school/levelsubjectdetail/edit',$data);
		$this->load->view('footer');	
	}
	function export(){
		$this->load->view('school/levelsubjectdetail/z_export_to_excel');
	}
	function getvalidate(){
		$schoolid=$this->input->post('schoolid');
		$subjectid=$this->input->post('subjectid');
		$year=$this->input->post('year');
		$count=$this->levelsubjectdetails->getvalidate($schoolid, $subjectid, $year);
		if($count>0) echo "true"; else echo "false";
		
	}
	//---- save when add ----
	function saves()
	{
		date_default_timezone_set("Asia/Bangkok");
		$schoolid=$this->input->post('cboschool');
		$glevelid=$this->input->post('grade');
		$subjectid=$this->input->post('cbosubject');
		$year=$this->input->post('cboyear');
		$desc=$this->input->post('txtdesc');
		$count=$this->levelsubjectdetails->getvalidate($schoolid, $subjectid, $year);
		if($count>0){
			$this->load->view('header');
			$data['error']="<p style='color:red;'>Your data has already exist...!</p>";
			$data['query']=$this->levelsubjectdetails->getgradelevel();
			$data['query1']=$this->levelsubjectdetails->getsubject();
			$data['query2']=$this->levelsubjectdetails->getpagination();
			$this->load->view('school/levelsubjectdetail/add',$data);
			$this->load->view('school/levelsubjectdetail/view',$data);
			$this->load->view('footer');
		}else{
			for($i=0;$i<count($glevelid);$i++){
				$data=array(
		  		'schoolid'=>$schoolid,
		  		'grade_levelid'=>$glevelid[$i],
		  		'subjectid'=>$subjectid,
		  		'year'=>$year,
		  		'note'=>$desc,
		  		'created_by'=>$this->session->userdata('userid'),
				'created_date'=>date('Y-m-d H:i:s'),
				'modified_by'=>$this->session->userdata('userid'),
				'modified_date'=>date('Y-m-d H:i:s'),
		  		);	

		  		if($glevelid[$i]>0)
					$this->db->insert('sch_level_subject_detail',$data);
			}
			$m='';
			$p='';
			if(isset($_GET['m'])){
		    	$m=$_GET['m'];
		    }
		    if(isset($_GET['p'])){
		        $p=$_GET['p'];
		    }
			redirect("school/levelsubjectdetail/?save&m=$m&p=$p");
		}
			
	}

//---- save when udpate ----
function savelevelsubject()
{
		date_default_timezone_set("Asia/Bangkok");
		$schoolid=$this->input->post('schoolid');
		$glevelid=$this->input->post('grade');
		$subjectid=$this->input->post('subjectid');
		$year=$this->input->post('yearid');
		$desc=$this->input->post('txtdesc');
		$this->deletelevelsubject($subjectid,$year,$schoolid);
		for($i=0;$i<count($glevelid);$i++){
			$data=array(
	  		'schoolid'=>$schoolid,
	  		'grade_levelid'=>$glevelid[$i],
	  		'subjectid'=>$subjectid,
	  		'year'=>$year,
	  		'note'=>$desc,
	  		'created_by'=>$this->session->userdata('userid'),
			'created_date'=>date('Y-m-d H:i:s'),
	  		'modified_by'=>$this->session->userdata('userid'),
			'modified_date'=>date('Y-m-d H:i:s'),
	  		);	
			$this->db->insert('sch_level_subject_detail',$data);
			}
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("school/levelsubjectdetail/?edit&m=$m&p=$p");
}

	function edits($subjectid,$yearid,$school)
	{
		$row=$this->levelsubjectdetails->editlevelsubject($subjectid,$yearid,$school);
		$this->load->view('header');
		$data['query']=$row;
		$data['data']=(object) array('subjectid'=>$subjectid,'year'=>$yearid,'school'=>$school);
		$data['grade']=$this->levelsubjectdetails->getgradelevel();
		$this->load->view('school/levelsubjectdetail/edit',$data);
		$data1['query']=$this->levelsubjectdetails->getgradelevel();
		$data1['query1']=$this->levelsubjectdetails->getsubject();
		$data1['query2']=$this->levelsubjectdetails->getpagination();
		$this->load->view('school/levelsubjectdetail/view',$data1);
		
		$this->load->view('footer');
	}

	function preview(){
		
		$this->load->view('header');
		$data1['query']=$this->levelsubjectdetails->getgradelevel();
		$data1['query1']=$this->levelsubjectdetails->getsubject();
		$data1['query2']=$this->levelsubjectdetails->getpagination();
		$data1['query3']=$this->levelsubjectdetails->getschoolyearrow($this->session->userdata('year'));
		$data1['query4']=$this->levelsubjectdetails->getschoolrow($this->session->userdata('schoolid'));
		$this->load->view('school/levelsubjectdetail/preview',$data1);
		
		$this->load->view('footer');
	}
	//---- Note use when update we use delete and new insert ----
	function updates()
	{
		$subj_level_id=$this->input->post('subjlevelid');
		$schoolid=$this->input->post('schoolid');
		$glevelid=$this->input->post('glevelid');
		$subjectid=$this->input->post('subjectid');
		$year=$this->input->post('year');
		$desc=$this->input->post('desc');

		$count=$this->levelsubjectdetails->getvalidateup($schoolid, $glevelid, $subjectid, $year, $subj_level_id);

			if ($count!=0){
				echo 'false';
			}else{

				
			    $data=array(
			    			'subj_level_id'=>$subj_level_id,
							'schoolid'=>$schoolid,
					  		'grade_levelid'=>$glevelid,
					  		'subjectid'=>$subjectid,
					  		'year'=>$year,
					  		'note'=>$desc,
				);
				$this->db->where('subj_level_id', $subj_level_id);
				$this->db->update('sch_level_subject_detail',$data);

		  echo 'true';	  
		}
	}

	//---- Use when delete ----
	function deletelevelsubject($subjectid,$yearid,$schoolid){
		$this->db->where('subjectid', $subjectid);
		$this->db->where('year', $yearid);
		$this->db->where('schoolid', $schoolid);
  		$this->db->delete('sch_level_subject_detail');
	}

	//---- use when delete from view -----
	function deletes($subjectid,$yearid,$schoolid){
		$this->db->where('subjectid', $subjectid);
		$this->db->where('year', $yearid);
		$this->db->where('schoolid', $schoolid);
  		$this->db->delete('sch_level_subject_detail');
		
		$m='';
		$p='';
		if(isset($_GET['m'])){
	    	$m=$_GET['m'];
	    }
	    if(isset($_GET['p'])){
	        $p=$_GET['p'];
	    }
		redirect("school/levelsubjectdetail/?delete&m=$m&p=$p");
	}


	function searchs(){
		  //----------- Pagination -----------
		   if(isset($_GET['s'])){
			   $y=$_GET['y'];
			   //$sch=$_GET['sch'];
			   //$data['query']=$this->levelsubjectdetails->searchlevelsubject($y);

		       $data['query']=$this->levelsubjectdetails->getgradelevel();
			   $data['query1']=$this->levelsubjectdetails->getsubject();
			   $data['query2']=$this->levelsubjectdetails->getpagination();
			   $this->load->view('header');
			   $this->load->view('school/levelsubjectdetail/add');
			   $this->load->view('school/levelsubjectdetail/view',$data);
			   $this->load->view('footer');
		  }

		   if(!isset($_GET['s'])){
		   $subject=$this->input->post('subject');
		   $trimester=$this->input->post('trim');
		   $is_eval=$this->input->post('is_eval');

		   $m=$this->input->post('m');
		   $p=$this->input->post('p');
		   $this->green->setActiveRole($this->input->post('roleid'));
		   if($m!=''){
		        $this->green->setActiveModule($m);
		   }
		   if($p!=''){
		        $this->green->setActivePage($p); 
		   }
   

		   foreach ($this->db->get('sch_subject_type')->result() as $sj_type) {
		    	echo "<tr>
		    				<td>$sj_type->subject_type</td>";
		    			  foreach($this->levelsubjectdetails->getgradelevel() as $row){
		    			  	echo "<td></td>";
		    			  }
		    	echo "</tr>";

			    foreach($this->levelsubjectdetails->searchsubjects($sj_type->subj_type_id,$subject,$trimester,$is_eval) as $row1){
			     //$countsub++;
			    	$trim='';
			    	if($row1->is_trimester_sub>0)
			    		$trim='*';
			      echo "
			        <tr>
			          	<td>&nbsp;&nbsp;&nbsp;$row1->subject <span style='color:red'>$trim</span></td>
			          	<td style='display:none'><input type='checkbox' class='checkall'></td>
			         ";
				      // for($i=1;$i<=$countglevel;$i++){
				      // 	
				      // }
			         foreach($this->levelsubjectdetails->getgradelevel() as $row){
			         	if($this->levelsubjectdetails->getlevelsubject($row->grade_levelid, $row1->subjectid,$this->session->userdata('year'),$this->session->userdata('schoolid')) > 0 )
		        			echo "<td><input type='checkbox' checked disabled></td>";
		        		else
		        			echo "<td><input type='checkbox' disabled ></td>";
		        	}


			      echo "<td align=center>";
			      		  if($this->green->gAction("U")){
			              	echo "<a href='".site_url('school/levelsubjectdetail/edits/'.$row1->subjectid."/".$this->session->userdata('year')."/".$this->session->userdata('schoolid'))."?m=$m&p=$p"."'>
			              		  <img src='".site_url('../assets/images/icons/edit.png')."' />
			              		  </a> | ";
			              }
			              if($this->green->gAction("D")){
				            echo "<a><img onclick='delevelsubjectde(event);' rel='$row1->subjectid' 
				              	  src='".site_url('../assets/images/icons/delete.png')."' />
				              	  </a>";
				          }    
			        echo "</td>
			      	</tr>";
			    }
			}
				  //   echo $countsub;
   	  // echo "<tr>
     	// 		<td colspan='12' id='pgt'>
     	// 			<div style='text-align:left'>
     	// 				<ul class='pagination' style='text-align:center'>".$this->pagination->create_links()."</ul>
     	// 			</div></td>
    		//    </tr>";
		}
	}
}