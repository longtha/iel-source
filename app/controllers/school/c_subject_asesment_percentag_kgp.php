<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	class c_subject_asesment_percentag_kgp extends CI_Controller{
	    protected $thead;
		protected $idfield;
	    function __construct() {
	        parent::__construct();
	        $this->load->model("school/m_subject_asesment_percentag_kgp","sapkgp");
	        $this->thead = array("No" => 'no',
	        					"School Level"=>'school_level_id',
	        					"Year"=>'adcademic_year_id',
	        					"Subject Type"=>'subject_type_id',
	        					"Subject"=>'subject_id',
	        					"Grade Level"=>'grade_level_id',
	        					"Assesment Name"=>'assessment_id',
	        					"Max Score"=>'maxscore',
	        					"Percentag (%)"=>'percentage',
	        					"Action"=>'Action'
	                            );
	        $this->idfield = "studentid";
	    }
	    function index() {
	        $data['idfield'] = $this->idfield;
        	$data['thead'] = $this->thead;
        	$data['getsubjecttype']=$this->sapkgp->getsubjecttype();
        	$data['getschoollavel']=$this->sapkgp->getschoollavel();
	        $this->load->view('header',$data);
	        $this->load->view('school/subjectasesment/v_subject_asesment_percentag_kgp',$data);
	        $this->load->view('footer',$data );
	    }
	    // save -------------------------------------------------------------
		function save(){
			$ass_percent_id = $this->input->post('ass_percent_id');
			$schlevel_id = $this->input->post('schlevel_id');
			$ass_percent_id=$this->sapkgp->save($ass_percent_id);
			$arr=array('ass_percent_id'=>$ass_percent_id);
			header("Content-type:text/x-json");
			echo json_encode($arr);
		}
		// delete -----------------------------------------------------------
		function delete(){
			$ass_percent_id = $this->input->post('ass_percent_id');
			if(intval($ass_percent_id) > 0 ) {
			   $this->db->where('ass_percent_id',$ass_percent_id)->delete('sch_subject_assessment_percent_kgp');
			   $msgbox = 'Data deleted!';
			}else{
				$msgbox = 'errorses';
			}
			$arrDel = array();
			header('Content-type:text/x-json');
			$arrDel['success']=$msgbox;
			echo json_encode($arrDel);
			die();
		}
		//get_schyera ------------------------------------------------------------
		function get_schyera(){
			$schlevel_id = $this->input->post('schlevel_id');
			$get_schyera = $this->sapkgp->get_schyera($schlevel_id);
			header("Content-type:text/x-json");
			echo ($get_schyera);
		}
	    //getsubject ------------------------------------------------------------
		function getsubject(){
			$subj_type_id = $this->input->post('subj_type_id');
			$getsubject = $this->sapkgp->getsubject($subj_type_id);
			header("Content-type:text/x-json");
			echo ($getsubject);
		}

		//getsubject ------------------------------------------------------------
	    function getgradlevels(){
	        $schlevelid=$this->input->post('schlevelid');
	        $subj_type_id=$this->input->post('subj_type_id');
	        $subjectid=$this->input->post('subjectid');
	        $html = '';
	        
	        $gradelevel=$this->sapkgp->gradelevel($schlevelid);
	        if(count($gradelevel) > 0){
	        	foreach($gradelevel as $rw_g){
	        		$tr = '';
	        		$percentag = 0;	        		
	        		$get_assessment_name = $this->sapkgp->getasesmentname();
					if(count($get_assessment_name)>0){
						foreach($get_assessment_name as $r_ass_name){
							$percentag = "";
							$maxscore ="";
							$checked = "";
							$ass_percent_id=0;

							$check_row = $this->sapkgp->checkasesment($r_ass_name->assess_id, $rw_g->grade_levelid,$subj_type_id,$subjectid);
							if($check_row->num_rows() > 0){
								$row = $check_row->row();
								if($row->assessment_id > 0){
									$percentag =($row->percentage*100);
									$maxscore = $row->maxscore;
									$checked = "checked";
									$ass_percent_id = $row->ass_percent_id;

								}

							}
							
						$tr .= "<tr class='tr_'>".
		  							"<td  class='cssclass_'>
		  								<input type='checkbox' name='assess_id[]' class='assess_id assess_id_".$rw_g->grade_levelid."' value='".$r_ass_name->assess_id."' ".$checked."><span> - ".$r_ass_name->assess_name_kh."</span>
		  							</td>".
		  							"<td style='width:245px; text-align:center;'>
		  								<input style='width:100%;' onkeypress='return isNumberKey(event);' name='maxscore[]' class='maxscore form-control maxscore_".$rw_g->grade_levelid."' value='".$maxscore."' type='text' >
		  							</td>".
		       						"<td style='width:245px; text-align:center;'>
		       							<input type='hidden' name='ass_percent_id[]' class='ass_percent_id_".$rw_g->grade_levelid."' value='".$ass_percent_id."' >".
		       							"<input style='width:100%;' onkeypress='return isNumberKey(event);' name='percentag[]' class='percentag form-control percentag_".$rw_g->grade_levelid."' value='". $percentag ."' type='text' >
		       						</td>".
			            		"</tr>";
						}
					}

	        		$html .='<tr class="tr">'.
		                        '<td colspan="0" style="width:100px;">
		                        	<input type="checkbox" name="grade_levelid[]" class="grade_levelid grade_'.$rw_g->grade_levelid.'" value="'.$rw_g->grade_levelid.'"> &nbsp; '.$rw_g->grade_level.'
		                        </td>'.
	                 			'<td colspan="3">'.
                                    '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-hover ">'.$tr.'</table>'.
                                '</td>'.
                            '</tr>';
	        	}
	        }	       

	        header("Content-type:text/x-json");
	        echo json_encode($html);
	        exit();
	    }

	    // showdata -------------------------------------------------
		function showdata(){
			$total_display =$this->input->post('sort_num');
			$this->green->setActiveRole($this->input->post('roleid'));
	  		$total_display = $this->input->post('total_display');
	  		$sortby=$this->input->post("sortby");
			$sorttype=$this->input->post("sorttype");
	  		$where='';
			$sortstr="";
		        (isset($sortby) && $sortby!="")?$sortstr.= " ORDER BY `".$sortby."` ".$sorttype:$sortstr="";

			$sqr="SELECT *
			 		FROM
						v_subject_assessment_percent_kgp 
					WHERE 1=1 {$where}";
			
			$total_row = $this->db->query("SELECT COUNT(*) as numrow FROM v_subject_assessment_percent_kgp WHERE 1=1 {$where}")->row()->numrow;
			$getperpage=0;
			if($total_display==''){
				$getperpage=10;
			}else{
				$getperpage=$total_display;
			}
			$paging    = $this->green->ajax_pagination($total_row,site_url()."school/c_subject_asesment_percentag_kgp",$getperpage,"icon");
			if($sortstr!=""){
				$sqr.=$sortstr;
			}
			$getlimit=10;
			if($paging['limit']!=''){
				$getlimit=$paging['limit'];
			}
			$data  = $this->db->query("$sqr limit {$paging['start']}, {$getlimit}")->result();
			$j='';
			$table='';
			$Percentag=0;
				if(count($data) > 0){
					foreach($data as $row){  
						$j++;
						$Percentag = ($row->percentage*100);
						$table.= "<tr>
									<td>".$j."</td>
									<td style='text-align:center;'>".$row->sch_level."</td>
									<td style='text-align:center;'>".$row->sch_year."</td>
									<td style='text-align:center;'>".$row->subject_type."</td>
									<td style='text-align:center;'>".$row->subject_kh."</td>
									<td style='text-align:center;'>".$row->grade_level."</td>
									<td style='text-align:center;'>".$row->assess_name_kh."</td>
									<td style='text-align:center;'>".$row->maxscore."</td>
									<td style='text-align:center;'>".$Percentag."</td>
									<td align='center' class='remove_tag no_wrap'>";
									if($this->green->gAction("D")){
										$table.= "<a data-placement='top' title='Delete'><img rel=".$row->ass_percent_id." onclick='deletedata(event);' src='".base_url('assets/images/icons/delete.png')."'/></a>";
									}
						$table.= " </td>
							 	</tr> ";	
					}
				}else{
					$table.= "<tr>
								<td colspan='9' style='text-align:center;'> 
									We did not find anything to show here... 
								</td>
							</tr>";
				}	

			$arr['data']=$table;
			header('Content-Type:text/x-json');
			$arr['body']=$table;
			$arr['pagination']=$paging;
			echo json_encode($arr); 
			die();
		}

	}